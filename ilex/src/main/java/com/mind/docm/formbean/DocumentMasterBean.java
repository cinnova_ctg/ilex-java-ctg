package com.mind.docm.formbean;

import org.apache.struts.action.ActionForm;

/**
 * The Class DocumentMasterBean.
 */
public class DocumentMasterBean extends ActionForm {

	/** The hmode. */
	String hmode = "";

	/** The document id. */
	String documentId = "";

	/** The view id. */
	String viewId = "";

	/** The filename. */
	String filename = "";

	/** The view ids. */
	String[] viewIds = null;

	/** The MIME. */
	private String MIME = null;

	/** The application. */
	private String application = null;

	/** The size. */
	private String size = null;

	/**
	 * Gets the application.
	 * 
	 * @return the application
	 */
	public String getApplication() {
		return application;
	}

	private byte[] file;

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	/**
	 * Sets the application.
	 * 
	 * @param application
	 *            the new application
	 */
	public void setApplication(String application) {
		this.application = application;
	}

	/**
	 * Gets the mIME.
	 * 
	 * @return the mIME
	 */
	public String getMIME() {
		return MIME;
	}

	/**
	 * Sets the mIME.
	 * 
	 * @param mime
	 *            the new mIME
	 */
	public void setMIME(String mime) {
		MIME = mime;
	}

	/**
	 * Gets the size.
	 * 
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * Sets the size.
	 * 
	 * @param size
	 *            the new size
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * Gets the hmode.
	 * 
	 * @return the hmode
	 */
	public String getHmode() {
		return hmode;
	}

	/**
	 * Sets the hmode.
	 * 
	 * @param hmode
	 *            the new hmode
	 */
	public void setHmode(String hmode) {
		this.hmode = hmode;
	}

	/**
	 * Gets the document id.
	 * 
	 * @return the document id
	 */
	public String getDocumentId() {
		return documentId;
	}

	/**
	 * Sets the document id.
	 * 
	 * @param documentId
	 *            the new document id
	 */
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	/**
	 * Gets the view ids.
	 * 
	 * @return the view ids
	 */
	public String[] getViewIds() {
		return viewIds;
	}

	/**
	 * Sets the view ids.
	 * 
	 * @param viewIds
	 *            the new view ids
	 */
	public void setViewIds(String[] viewIds) {
		this.viewIds = viewIds;
	}

	/**
	 * Gets the view ids.
	 * 
	 * @param i
	 *            the i
	 * 
	 * @return the view ids
	 */
	public String getViewIds(int i) {
		return viewIds[i];
	}

	/**
	 * Gets the view id.
	 * 
	 * @return the view id
	 */
	public String getViewId() {
		return viewId;
	}

	/**
	 * Sets the view id.
	 * 
	 * @param viewId
	 *            the new view id
	 */
	public void setViewId(String viewId) {
		this.viewId = viewId;
	}

	/**
	 * Gets the filename.
	 * 
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Sets the filename.
	 * 
	 * @param filename
	 *            the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}
}
