package com.mind.docm.formbean;

import org.apache.struts.action.ActionForm;

/**
 * The Class ViewManupulationBean.
 */
public class ViewManupulationBean extends ActionForm {

	/** The selected columns. */
	private String[] selectedColumns = null;

	/** The id values. */
	private String[] idValues = null;

	/** The column check value. */
	private String[] columnCheckValue = null;

	/** The selected group columns. */
	private String[] selectedGroupColumns = null;

	/** The selected sort columns. */
	private String[] selectedSortColumns = null;

	/** The selected filter columns. */
	private String[] selectedFilterColumns = null;

	/** The sort dirrection. */
	private String[] sortDirrection = null;

	/** The view name. */
	private String viewName = "";

	/** The filter condition. */
	private String[] filterCondition = null;

	/** The logical condition. */
	private String[] logicalCondition = null;

	/** The view id. */
	private String viewId = "";

	/** The function. */
	private String function = "";

	/** The lib id. */
	private String libId = "";

	/** The filter type. */
	private String[] filterType = null;

	/** The filter id. */
	private String[] filterId = null;

	/** The detail id for sort column. */
	private String[] detailIdForSortColumn = null;

	/** The detail id for group column. */
	private String[] detailIdForGroupColumn = null;

	/** The lib name. */
	private String libName = "";

	/** The field type id. */
	private String[] fieldTypeId = null;

	/**
	 * Gets the detail id for sort column.
	 * 
	 * @return the detail id for sort column
	 */
	public String[] getDetailIdForSortColumn() {
		return detailIdForSortColumn;
	}

	/**
	 * Sets the detail id for sort column.
	 * 
	 * @param detailIdForSortColumn
	 *            the new detail id for sort column
	 */
	public void setDetailIdForSortColumn(String[] detailIdForSortColumn) {
		this.detailIdForSortColumn = detailIdForSortColumn;
	}

	/**
	 * Gets the selected columns.
	 * 
	 * @return the selected columns
	 */
	public String[] getSelectedColumns() {
		return selectedColumns;
	}

	/**
	 * Sets the selected columns.
	 * 
	 * @param selectedColumns
	 *            the new selected columns
	 */
	public void setSelectedColumns(String[] selectedColumns) {
		this.selectedColumns = selectedColumns;
	}

	/**
	 * Gets the selected group columns.
	 * 
	 * @return the selected group columns
	 */
	public String[] getSelectedGroupColumns() {
		return selectedGroupColumns;
	}

	/**
	 * Sets the selected group columns.
	 * 
	 * @param selectedGroupColumns
	 *            the new selected group columns
	 */
	public void setSelectedGroupColumns(String[] selectedGroupColumns) {
		this.selectedGroupColumns = selectedGroupColumns;
	}

	/**
	 * Gets the selected sort columns.
	 * 
	 * @return the selected sort columns
	 */
	public String[] getSelectedSortColumns() {
		return selectedSortColumns;
	}

	/**
	 * Sets the selected sort columns.
	 * 
	 * @param selectedSortColumns
	 *            the new selected sort columns
	 */
	public void setSelectedSortColumns(String[] selectedSortColumns) {
		this.selectedSortColumns = selectedSortColumns;
	}

	/**
	 * Gets the selected filter columns.
	 * 
	 * @return the selected filter columns
	 */
	public String[] getSelectedFilterColumns() {
		return selectedFilterColumns;
	}

	/**
	 * Sets the selected filter columns.
	 * 
	 * @param selectedFilterColumns
	 *            the new selected filter columns
	 */
	public void setSelectedFilterColumns(String[] selectedFilterColumns) {
		this.selectedFilterColumns = selectedFilterColumns;
	}

	/**
	 * Gets the column check value.
	 * 
	 * @return the column check value
	 */
	public String[] getColumnCheckValue() {
		return columnCheckValue;
	}

	/**
	 * Sets the column check value.
	 * 
	 * @param columnCheckValue
	 *            the new column check value
	 */
	public void setColumnCheckValue(String[] columnCheckValue) {
		this.columnCheckValue = columnCheckValue;
	}

	/**
	 * Gets the id values.
	 * 
	 * @return the id values
	 */
	public String[] getIdValues() {
		return idValues;
	}

	/**
	 * Sets the id values.
	 * 
	 * @param idValues
	 *            the new id values
	 */
	public void setIdValues(String[] idValues) {
		this.idValues = idValues;
	}

	/**
	 * Gets the sort dirrection.
	 * 
	 * @return the sort dirrection
	 */
	public String[] getSortDirrection() {
		return sortDirrection;
	}

	/**
	 * Sets the sort dirrection.
	 * 
	 * @param sortDirrection
	 *            the new sort dirrection
	 */
	public void setSortDirrection(String[] sortDirrection) {
		this.sortDirrection = sortDirrection;
	}

	/**
	 * Gets the view name.
	 * 
	 * @return the view name
	 */
	public String getViewName() {
		return viewName;
	}

	/**
	 * Sets the view name.
	 * 
	 * @param viewName
	 *            the new view name
	 */
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	/**
	 * Gets the view id.
	 * 
	 * @return the view id
	 */
	public String getViewId() {
		return viewId;
	}

	/**
	 * Sets the view id.
	 * 
	 * @param viewId
	 *            the new view id
	 */
	public void setViewId(String viewId) {
		this.viewId = viewId;
	}

	/**
	 * Gets the function.
	 * 
	 * @return the function
	 */
	public String getFunction() {
		return function;
	}

	/**
	 * Sets the function.
	 * 
	 * @param function
	 *            the new function
	 */
	public void setFunction(String function) {
		this.function = function;
	}

	/**
	 * Gets the lib id.
	 * 
	 * @return the lib id
	 */
	public String getLibId() {
		return libId;
	}

	/**
	 * Sets the lib id.
	 * 
	 * @param libId
	 *            the new lib id
	 */
	public void setLibId(String libId) {
		this.libId = libId;
	}

	/**
	 * Gets the filter condition.
	 * 
	 * @return the filter condition
	 */
	public String[] getFilterCondition() {
		return filterCondition;
	}

	/**
	 * Sets the filter condition.
	 * 
	 * @param filterCondition
	 *            the new filter condition
	 */
	public void setFilterCondition(String[] filterCondition) {
		this.filterCondition = filterCondition;
	}

	/**
	 * Gets the filter type.
	 * 
	 * @return the filter type
	 */
	public String[] getFilterType() {
		return filterType;
	}

	/**
	 * Sets the filter type.
	 * 
	 * @param filterType
	 *            the new filter type
	 */
	public void setFilterType(String[] filterType) {
		this.filterType = filterType;
	}

	/**
	 * Gets the logical condition.
	 * 
	 * @return the logical condition
	 */
	public String[] getLogicalCondition() {
		return logicalCondition;
	}

	/**
	 * Sets the logical condition.
	 * 
	 * @param logicalCondition
	 *            the new logical condition
	 */
	public void setLogicalCondition(String[] logicalCondition) {
		this.logicalCondition = logicalCondition;
	}

	/**
	 * Gets the filter id.
	 * 
	 * @return the filter id
	 */
	public String[] getFilterId() {
		return filterId;
	}

	/**
	 * Sets the filter id.
	 * 
	 * @param filterId
	 *            the new filter id
	 */
	public void setFilterId(String[] filterId) {
		this.filterId = filterId;
	}

	/**
	 * Gets the detail id for group column.
	 * 
	 * @return the detail id for group column
	 */
	public String[] getDetailIdForGroupColumn() {
		return detailIdForGroupColumn;
	}

	/**
	 * Sets the detail id for group column.
	 * 
	 * @param detailIdForGroupColumn
	 *            the new detail id for group column
	 */
	public void setDetailIdForGroupColumn(String[] detailIdForGroupColumn) {
		this.detailIdForGroupColumn = detailIdForGroupColumn;
	}

	/**
	 * Gets the lib name.
	 * 
	 * @return the lib name
	 */
	public String getLibName() {
		return libName;
	}

	/**
	 * Sets the lib name.
	 * 
	 * @param libName
	 *            the new lib name
	 */
	public void setLibName(String libName) {
		this.libName = libName;
	}

	/**
	 * Gets the field type id.
	 * 
	 * @return the field type id
	 */
	public String[] getFieldTypeId() {
		return fieldTypeId;
	}

	/**
	 * Sets the field type id.
	 * 
	 * @param fieldTypeId
	 *            the new field type id
	 */
	public void setFieldTypeId(String[] fieldTypeId) {
		this.fieldTypeId = fieldTypeId;
	}

}
