package com.mind.docm.formbean;

import org.apache.struts.action.ActionForm;

/**
 * The Class MetaDataListBean.
 */
public class MetaDataListBean extends ActionForm {

	/** The hmode. */
	String hmode = "";

	/**
	 * Gets the hmode.
	 * 
	 * @return the hmode
	 */
	public String getHmode() {
		return hmode;
	}

	/**
	 * Sets the hmode.
	 * 
	 * @param hmode
	 *            the new hmode
	 */
	public void setHmode(String hmode) {
		this.hmode = hmode;
	}

}
