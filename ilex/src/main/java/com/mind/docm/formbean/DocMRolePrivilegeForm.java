package com.mind.docm.formbean;

import org.apache.struts.action.ActionForm;

/**
 * The Class DocMRolePrivilegeForm.
 */
public class DocMRolePrivilegeForm extends ActionForm {

	/** The lo_organization_type_name. */
	private String lo_organization_type_name = null;

	/** The lo_od_name. */
	private String lo_od_name = null;

	/** The cc_fu_group_name. */
	private String cc_fu_group_name = null;

	/** The lo_ro_role_desc. */
	private String lo_ro_role_desc = null;

	/** The lo_ro_id. */
	private String lo_ro_id = null;

	/** The org_type. */
	private String org_type = null;

	/** The org_discipline_id. */
	private String org_discipline_id = null;

	/** The check. */
	private int[] check = null;

	/** The save. */
	private String save = null;

	/** The refresh. */
	private boolean refresh = false;

	/**
	 * Gets the cc_fu_group_name.
	 * 
	 * @return the cc_fu_group_name
	 */
	public String getCc_fu_group_name() {
		return cc_fu_group_name;
	}

	/**
	 * Sets the cc_fu_group_name.
	 * 
	 * @param cc_fu_group_name
	 *            the new cc_fu_group_name
	 */
	public void setCc_fu_group_name(String cc_fu_group_name) {
		this.cc_fu_group_name = cc_fu_group_name;
	}

	/**
	 * Gets the lo_od_name.
	 * 
	 * @return the lo_od_name
	 */
	public String getLo_od_name() {
		return lo_od_name;
	}

	/**
	 * Sets the lo_od_name.
	 * 
	 * @param lo_od_name
	 *            the new lo_od_name
	 */
	public void setLo_od_name(String lo_od_name) {
		this.lo_od_name = lo_od_name;
	}

	/**
	 * Gets the lo_organization_type_name.
	 * 
	 * @return the lo_organization_type_name
	 */
	public String getLo_organization_type_name() {
		return lo_organization_type_name;
	}

	/**
	 * Sets the lo_organization_type_name.
	 * 
	 * @param lo_organization_type_name
	 *            the new lo_organization_type_name
	 */
	public void setLo_organization_type_name(String lo_organization_type_name) {
		this.lo_organization_type_name = lo_organization_type_name;
	}

	/**
	 * Gets the lo_ro_role_desc.
	 * 
	 * @return the lo_ro_role_desc
	 */
	public String getLo_ro_role_desc() {
		return lo_ro_role_desc;
	}

	/**
	 * Sets the lo_ro_role_desc.
	 * 
	 * @param lo_ro_role_desc
	 *            the new lo_ro_role_desc
	 */
	public void setLo_ro_role_desc(String lo_ro_role_desc) {
		this.lo_ro_role_desc = lo_ro_role_desc;
	}

	/**
	 * Gets the save.
	 * 
	 * @return the save
	 */
	public String getSave() {
		return save;
	}

	/**
	 * Sets the save.
	 * 
	 * @param save
	 *            the new save
	 */
	public void setSave(String save) {
		this.save = save;
	}

	/**
	 * Gets the refresh.
	 * 
	 * @return the refresh
	 */
	public boolean getRefresh() {
		return refresh;
	}

	/**
	 * Sets the refresh.
	 * 
	 * @param refresh
	 *            the new refresh
	 */
	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	/**
	 * Gets the org_discipline_id.
	 * 
	 * @return the org_discipline_id
	 */
	public String getOrg_discipline_id() {
		return org_discipline_id;
	}

	/**
	 * Sets the org_discipline_id.
	 * 
	 * @param org_discipline_id
	 *            the new org_discipline_id
	 */
	public void setOrg_discipline_id(String org_discipline_id) {
		this.org_discipline_id = org_discipline_id;
	}

	/**
	 * Gets the org_type.
	 * 
	 * @return the org_type
	 */
	public String getOrg_type() {
		return org_type;
	}

	/**
	 * Sets the org_type.
	 * 
	 * @param org_type
	 *            the new org_type
	 */
	public void setOrg_type(String org_type) {
		this.org_type = org_type;
	}

	/**
	 * Gets the check.
	 * 
	 * @return the check
	 */
	public int[] getCheck() {
		return check;
	}

	/**
	 * Sets the check.
	 * 
	 * @param ix
	 *            the new check
	 */
	public void setCheck(int[] ix) {
		check = ix;
	}

	/**
	 * Gets the lo_ro_id.
	 * 
	 * @return the lo_ro_id
	 */
	public String getLo_ro_id() {
		return lo_ro_id;
	}

	/**
	 * Sets the lo_ro_id.
	 * 
	 * @param lo_ro_id
	 *            the new lo_ro_id
	 */
	public void setLo_ro_id(String lo_ro_id) {
		this.lo_ro_id = lo_ro_id;
	}

}
