package com.mind.docm.formbean;

import org.apache.struts.action.ActionForm;

/**
 * The Class ViewLibraryBean.
 */
public class ViewLibraryBean extends ActionForm {

	/** The hmode. */
	String hmode = "";

	/** The Library id. */
	String LibraryId = "";

	/** The library name. */
	String libraryName = "";

	/** The notify email. */
	String notifyEmail = "";

	/** The notifiable. */
	String notifiable = "";

	/** The doc_identifier_fl_id. */
	String doc_identifier_fl_id = "";

	/**
	 * Gets the doc_identifier_fl_id.
	 * 
	 * @return the doc_identifier_fl_id
	 */
	public String getDoc_identifier_fl_id() {
		return doc_identifier_fl_id;
	}

	/**
	 * Sets the doc_identifier_fl_id.
	 * 
	 * @param doc_identifier_fl_id
	 *            the new doc_identifier_fl_id
	 */
	public void setDoc_identifier_fl_id(String doc_identifier_fl_id) {
		this.doc_identifier_fl_id = doc_identifier_fl_id;
	}

	/**
	 * Gets the notifiable.
	 * 
	 * @return the notifiable
	 */
	public String getNotifiable() {
		return notifiable;
	}

	/**
	 * Sets the notifiable.
	 * 
	 * @param notifiable
	 *            the new notifiable
	 */
	public void setNotifiable(String notifiable) {
		this.notifiable = notifiable;
	}

	/**
	 * Gets the notify email.
	 * 
	 * @return the notify email
	 */
	public String getNotifyEmail() {
		return notifyEmail;
	}

	/**
	 * Sets the notify email.
	 * 
	 * @param notifyEmail
	 *            the new notify email
	 */
	public void setNotifyEmail(String notifyEmail) {
		this.notifyEmail = notifyEmail;
	}

	/**
	 * Gets the library id.
	 * 
	 * @return the library id
	 */
	public String getLibraryId() {
		return LibraryId;
	}

	/**
	 * Sets the library id.
	 * 
	 * @param libraryId
	 *            the new library id
	 */
	public void setLibraryId(String libraryId) {
		LibraryId = libraryId;
	}

	/**
	 * Gets the library name.
	 * 
	 * @return the library name
	 */
	public String getLibraryName() {
		return libraryName;
	}

	/**
	 * Sets the library name.
	 * 
	 * @param libraryName
	 *            the new library name
	 */
	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	/**
	 * Gets the hmode.
	 * 
	 * @return the hmode
	 */
	public String getHmode() {
		return hmode;
	}

	/**
	 * Sets the hmode.
	 * 
	 * @param hmode
	 *            the new hmode
	 */
	public void setHmode(String hmode) {
		this.hmode = hmode;
	}

}
