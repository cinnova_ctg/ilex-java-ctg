package com.mind.docm;

import java.util.Collection;

/**
 * The Class DynamicComboDocM.
 */
public class DynamicComboDocM {

	/** The librarylist. */
	private Collection librarylist = null;

	/**
	 * Gets the librarylist.
	 * 
	 * @return the librarylist
	 */
	public Collection getLibrarylist() {
		return librarylist;
	}

	/**
	 * Sets the librarylist.
	 * 
	 * @param librarylist
	 *            the new librarylist
	 */
	public void setLibrarylist(Collection librarylist) {
		this.librarylist = librarylist;
	}

}
