//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : DocumentRepository.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm;

import com.mind.bean.docm.Library;

/**
 * The Class DocumentRepository.
 */
public class DocumentRepository {

	/** The libraries. */
	private Library[] libraries;

	/**
	 * Gets the libraries.
	 * 
	 * @return the libraries
	 */
	public Library[] getLibraries() {
		return libraries;
	}

	/**
	 * Sets the libraries.
	 * 
	 * @param libraries
	 *            the new libraries
	 */
	public void setLibraries(Library[] libraries) {
		this.libraries = libraries;
	}
}
