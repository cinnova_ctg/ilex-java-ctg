//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : SortElement.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm;

import com.mind.bean.docm.Field;

/**
 * The Class SortElement.
 */
public class SortElement {

	/** The column. */
	public Field column;

	/** The order type. */
	public String orderType;
}
