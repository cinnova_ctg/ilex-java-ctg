package com.mind.docm.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.docm.dao.DocMSeacrhDao;

/**
 * The Class DocumentSearchResultAction.
 */
public class DocumentSearchResultAction extends com.mind.common.IlexAction {

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String sql = (request.getAttribute("resultQuery") == null) ? ""
				: request.getAttribute("resultQuery").toString();

		if (!sql.equals(""))
			request.setAttribute("documentList", DocMSeacrhDao
					.getResultList(sql));
		else
			request.setAttribute("Error", "Error");

		return mapping.findForward("success");
	}
}
