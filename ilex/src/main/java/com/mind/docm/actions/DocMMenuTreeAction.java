package com.mind.docm.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.docm.DocMMenuTree;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.docm.dao.LibraryListDao;

/**
 * The Class DocMMenuTreeAction.
 */
public class DocMMenuTreeAction extends com.mind.common.IlexAction {

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DocMMenuTree menutree = new DocMMenuTree();
		String menustring = "";
		String currState = "";
		String expansionState = "";
		String[] image = null;
		int[][] samelevel_image = null;
		String[] strarr_url = null;
		int[][] samelevel_url = null;
		int[] strarr_level = null;
		int[] strarr_label = null;
		menutree.setImage_width(15);
		menutree.setImage_height(15);
		menutree.setImage_border(0);
		menutree.setTarget("ilexmain");
		HttpSession session = request.getSession(true);
		LibraryListDao libmstDao = new LibraryListDao();

		if (request.getParameter("menucriteria").equals("UM")) {
			image = new String[3];
			image[0] = "docm/images/t_msa.gif";
			image[1] = "docm/images/t_appendix.gif";
			image[2] = "docm/images/t_job.gif";
			menutree.setStrarr_img(image);

			menutree.setStr_headerImg("docm/images/t_msa.gif");
			menutree.setStr_headerLabel("Organization");
			menutree.setStr_headerurl("");

			strarr_level = new int[3];
			strarr_level[0] = 1;
			strarr_level[1] = 3;
			strarr_level[2] = 5;

			menutree.setStrarr_level(strarr_level);

			strarr_label = new int[3];
			strarr_label[0] = 2;
			strarr_label[1] = 4;
			strarr_label[2] = 6;

			menutree.setStrarr_label(strarr_label);

			strarr_url = new String[3];
			strarr_url[0] = "";
			strarr_url[1] = "DocMRolePrivilegeAction.do";
			strarr_url[2] = "";

			menutree.setStrarr_url(strarr_url);

			String strarr_urlparam[][] = {
					{ "1", "3", "5" },
					{ "org_type|1", "org_discipline_id|3~org_type|1",
							"org_discipline_id|3~role_id|5" } };

			menutree.setStrarr_urlparam(strarr_urlparam);
			menutree.setStr_query("select * from lp_user_hierarchy_01");

			menustring = menutree.generateMenu(DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB), "UM");
			currState = menutree.getCurrState();
			expansionState = menutree.getExpansionState();
			request.setAttribute("menustring", menustring);
			request.setAttribute("currState", currState);
			request.setAttribute("expansionState", expansionState);

			request.setAttribute("element", menutree);

			request
					.setAttribute("controller", menutree
							.getDocMMenuController());

			ArrayList libraryList = libmstDao.libraryMasterList();
			request.setAttribute("libraryList", libraryList);
			return (mapping.findForward("leftTree"));
		}

		return null;
	}
}
