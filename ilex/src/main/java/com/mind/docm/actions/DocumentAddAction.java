package com.mind.docm.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.upload.MultipartRequestWrapper;

import com.lowagie.text.DocumentException;
import com.mind.bean.docm.Document;
import com.mind.bean.docm.DocumentAddDTO;
import com.mind.bean.docm.Field;
import com.mind.bean.docm.MetaData;
import com.mind.common.dao.Menu;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.docm.DynamicComboDocM;
import com.mind.docm.client.ClientOperator;
import com.mind.docm.dao.DocumentAddDao;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotMarkDeleteException;
import com.mind.docm.exceptions.CouldNotPhysicallyDeleteException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.DocumentNotExistsException;
import com.mind.docm.formbean.DocumentAddBean;
import com.mind.docm.security.Authentication;
import com.mind.docm.utils.DocumentUtility;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

/**
 * Class DocumentAddAction - This file contains the method to the document
 * add/modify/delete/view. methods - unspecified, metaDataList,
 * metaDataInsert,viewDocument, modifyDocument,moveDocument.
 */
public class DocumentAddAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DocumentAddAction.class);

	/**
	 * unspecified - This method is called first and call metaDataList method of
	 * same Action.
	 * 
	 * @param - As per struts standard.
	 * @return Returns Action Forward.
	 **/
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Pragma", "No-Cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		logger.trace("Class : DocumentMasterAction Method : unspecified ");
		logger.trace("This method is used to show the user listing of the documents in a particular library");
		return this.metaDataList(mapping, form, request, response);
	}

	/**
	 * unspecified - This method is used to get all the necessary metadata list
	 * for a library.
	 * 
	 * @param - As per struts standard.
	 * @return Returns Action Forward.
	 **/
	public ActionForward metaDataList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		DocumentAddBean bean = (DocumentAddBean) form;
		DocumentAddDTO dto = new DocumentAddDTO();
		DocumentAddDao dao = new DocumentAddDao();
		String libId = null;
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		Map<String, Object> map = new HashMap<String, Object>();

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		bean.setSource(((String) request.getAttribute("SourceType") == null ? request
				.getParameter("Source") : (String) request
				.getAttribute("SourceType")));
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error("Not able to copy properties" + e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error("Not able to copy properties" + e);
			e.printStackTrace();
		}
		if (bean.getSource().equalsIgnoreCase("Ilex")) {
			dao.setMenuForEntity(dto, attrmap, paramMap);
			try {
				BeanUtils.copyProperties(bean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			showMenuViewSelector(bean, loginuserid, request);
		}

		libId = (request.getParameter("libId") == null) ? (request
				.getAttribute("libId") == null ? "" : (String) request
				.getAttribute("libId")).toString() : request
				.getParameter("libId");

		if (!libId.equals("")) {
			if (session.getAttribute("userid") == null)
				return (mapping.findForward("SessionExpire")); // Check for
																// session
																// expired
			String libNamne = DocumentUtility.getLibraryName(libId);
			if (!Authentication.getPageSecurity(loginuserid, libNamne,
					"Document Add")) {
				return (mapping.findForward("UnAuthenticate"));
			}
			bean.setLibId(Integer.parseInt(libId));
			ArrayList metaDataFieldList = DocumentAddDao.getAllMetaDataFields(
					dto, attrmap, paramMap, Integer.parseInt(libId));
			request.setAttribute("metadata_field_list", metaDataFieldList);
		}
		dao.populateText("metaDataList", attrmap, paramMap, map, libId, dto);
		WebUtil.copyMapToRequest(request, map);
		request.setAttribute("SourceType", bean.getSource());
		return (mapping.findForward("MetaDataList"));
	}

	/**
	 * unspecified - This method is used to insert,modify and move a document in
	 * database.
	 * 
	 * @param - As per struts standard.
	 * @return Returns Action Forward.
	 **/
	public ActionForward metaDataInsert(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControlledTypeException, CouldNotAddToRepositoryException,
			CouldNotReplaceException, CouldNotCheckDocumentException {
		byte[] filebyte = null;
		Enumeration en = request.getParameterNames();
		DocumentAddBean bean = (DocumentAddBean) form;
		ArrayList al = new ArrayList();
		MetaData mdata = new MetaData();
		int docId = 0;
		Field field = null;
		String move = "";
		ArrayList libNameList = null;
		Document document = new Document();
		DynamicComboDocM dynamicCombo = new DynamicComboDocM();
		DocumentAddDao dao = new DocumentAddDao();
		bean.setSource(((String) request.getAttribute("SourceType") == null ? (request
				.getParameter("Source")) : (String) request
				.getAttribute("SourceType")));
		DocumentAddDTO dto = new DocumentAddDTO();
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		Map<String, Object> map = new HashMap<String, Object>();

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);

		// System.out.println("bean source 1 "+bean.getSource());

		// If file insertion is needed.then set the file bytes and name in
		// document object.

		if (bean.getMyfile().getFileSize() > 0) {
			FormFile file = bean.getMyfile();
			try {
				filebyte = file.getFileData();
				bean.setSize(Integer.toString(bean.getMyfile().getFileSize()));
				bean.setApplication("File Type");
			} catch (Exception e1) {
				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e1);

				if (logger.isDebugEnabled()) {
					logger.debug("metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in catch");
				}
				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e1);
			}
			document.setFileBytes(filebyte);
		}
		document.setFileName(bean.getMyfilename());

		// get all the parameters through enmuration object and set theses
		// metadata fields in document object.
		while (en.hasMoreElements()) {
			String paramName = (String) en.nextElement();
			field = new Field();
			field.setName(paramName);
			if (paramName.equalsIgnoreCase("size")) {
				field.setValue((bean.getSize() == null ? request
						.getParameter(paramName) : bean.getSize()));
			} else if (paramName.equalsIgnoreCase("Application")) {
				field.setValue((bean.getApplication() == null ? request
						.getParameter(paramName) : bean.getApplication()));
			} else {
				field.setValue(request.getParameter(paramName));
			}
			al.add(field);
		}

		mdata.setFields(com.mind.docm.util.DocumentUtility.fieldArray(al));
		document.setMetaData(mdata);

		// If there is no document Id then insert the document object in
		// database and create a new documentId.
		if (request.getParameter("docId") == null) {
			try {
				docId = ClientOperator.explicitAddDocument(document);
				request.setAttribute("Comment",
						"Document Successfully added in DocM System.");
			} catch (DocMFormatException e) {
				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				request.setAttribute("Comment",
						"Compulsary Metadata Fields are not existing.");
				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);
				try {
					BeanUtils.copyProperties(dto, bean);
				} catch (IllegalAccessException e2) {
					logger.error(e2);
					e2.printStackTrace();
				} catch (InvocationTargetException e2) {
					logger.error(e2);
					e2.printStackTrace();
				}
				dao.populateData(DocumentAddDao.getAllMetaDataFields(dto,
						attrmap, paramMap, Integer.parseInt((String) request
								.getParameter("libId"))), map, paramMap);
				WebUtil.copyMapToRequest(request, map);
				try {
					BeanUtils.copyProperties(bean, dto);
				} catch (IllegalAccessException e1) {
					logger.error(e1);
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					logger.error(e1);
					e1.printStackTrace();
				}
				if (!bean.getSource().equals("Ilex"))
					libNameList = DocumentAddDao.getAllLibraryName(true);
				else if (bean.getSource().equals("Ilex"))
					libNameList = DocumentAddDao.getAllLibraryName(false);
				dynamicCombo.setLibrarylist(libNameList);
				request.setAttribute("libNameCombo", dynamicCombo);
				if (request.getParameter("throughMPO") != null) {
					// String url =
					// "MPOAction.do?hmode=mpoSearchResult&through=MPO&type=4&docIdList=";
					String url = "./MPO/MoveToContainer.jsp?poDocId=" + docId
							+ "&devId=" + request.getParameter("devId")
							+ "&docIdList=";
					dispatchJsp(request, response, url);
				} else
					return (mapping.findForward("MetaDataList"));

			} catch (DocumentException e) {
				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				request.setAttribute(
						"Comment",
						"Document Already exists with the same "
								+ DocumentUtility.getLibraryName(request
										.getParameter("libId")) + " ID");
				try {
					BeanUtils.copyProperties(dto, bean);
				} catch (IllegalAccessException e1) {
					logger.error(e1);
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					logger.error(e1);
					e1.printStackTrace();
				}
				dao.populateData(DocumentAddDao.getAllMetaDataFields(dto,
						attrmap, paramMap, Integer.parseInt((String) request
								.getParameter("libId"))), map, paramMap);
				try {
					BeanUtils.copyProperties(bean, dto);
				} catch (IllegalAccessException e1) {
					logger.error(e1);
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					logger.error(e1);
					e1.printStackTrace();
				}
				WebUtil.copyMapToRequest(request, map);
				if (!bean.getSource().equals("Ilex"))
					libNameList = DocumentAddDao.getAllLibraryName(true);
				else if (bean.getSource().equals("Ilex"))
					libNameList = DocumentAddDao.getAllLibraryName(false);
				dynamicCombo.setLibrarylist(libNameList);
				request.setAttribute("libNameCombo", dynamicCombo);
				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);
				if (request.getParameter("throughMPO") != null) {
					// String url =
					// "MPOAction.do?hmode=mpoSearchResult&through=MPO&type=4&docIdList=";
					String url = "./MPO/MoveToContainer.jsp?poDocId=" + docId
							+ "&devId=" + request.getParameter("devId")
							+ "&docIdList=";
					dispatchJsp(request, response, url);
				} else
					return (mapping.findForward("MetaDataList"));
			}
		}

		// If there already exists a document Id then insert the document object
		// in database with the same documentId.
		else if (request.getParameter("docId") != null) {
			try {
				ClientOperator.explicitModifyDocument(
						request.getParameter("docId"), document);
				request.setAttribute("Comment",
						"Document Successfully updated in DocM System.");
			} catch (DocMFormatException e) {
				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);
				request.setAttribute("Comment",
						"Document does not exist in DocM System.");
				return (mapping.findForward("failure"));
			} catch (DocumentNotExistsException e) {
				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				logger.error(
						"metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);
				request.setAttribute("Comment",
						"Document does not exist in DocM System.");
				if (request.getParameter("throughMPO") != null) {
					// String url =
					// "MPOAction.do?hmode=mpoSearchResult&through=MPO&type=4&docIdList=";
					String url = "./MPO/MoveToContainer.jsp?poDocId=" + docId
							+ "&devId=" + request.getParameter("devId")
							+ "&docIdList=";
					dispatchJsp(request, response, url);
				} else
					return (mapping.findForward("failure"));
			}
		}
		// If there user wants to move a document into a different library then
		// call updateDocumentMaster method of dao.
		if (request.getParameter("move") == null)
			move = "";
		else
			move = request.getParameter("move");
		if (move.equals("yes")) {
			DocumentAddDao.updateDocumentMaster(request.getParameter("libId"),
					request.getParameter("docId"));
			request.setAttribute(
					"Comment",
					"Document Successfully moved to Library "
							+ DocumentUtility.getLibraryName(request
									.getParameter("libId")) + ".");
		}
		request.setAttribute("entityType", bean.getEntityType());
		request.setAttribute("entityId", bean.getEntityId());
		request.setAttribute("MSA_ID", bean.getMsaId());
		request.setAttribute("appendixId", bean.getAppendixId());
		request.setAttribute("libId", request.getParameter("libId"));
		request.setAttribute("linkLibName", bean.getLinkLibNameFromIlex());
		request.setAttribute("SourceType", bean.getSource());
		if (logger.isDebugEnabled()) {
			logger.debug("metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - META DATA INSERT "
					+ bean.getSource());
		}
		request.setAttribute(
				"docId",
				(request.getParameter("docId") == null ? Integer
						.toString(docId) : request.getParameter("docId")));
		if (logger.isDebugEnabled()) {
			logger.debug("metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - docID1111"
					+ Integer.toString(docId));
		}
		if (logger.isDebugEnabled()) {
			logger.debug("metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - docID22222"
					+ request.getParameter("docId"));
		}
		String docIdForMpo = request.getParameter("docId") == null ? Integer
				.toString(docId) : request.getParameter("docId");
		if (request.getParameter("throughMPO") != null) {
			// String url =
			// "MPOAction.do?hmode=mpoSearchResult&through=MPO&type=4&docIdList="+docIdForMpo;
			String url = "./MPO/MoveToContainer.jsp?poDocId=" + docIdForMpo
					+ "&devId=" + request.getParameter("devId") + "&docIdList="
					+ docIdForMpo;
			if (logger.isDebugEnabled()) {
				logger.debug("metaDataInsert(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - "
						+ url);
			}
			dispatchJsp(request, response, url);
		} else
			return (mapping.findForward("viewDocument"));

		return null;
	}

	/**
	 * unspecified - This method is used to show the document detail for view or
	 * delete document. get the library name for a libraryId. get the list of
	 * taxonomy group. get the list of metadata fields. get the document Object
	 * for the documentId. get the already linked documentId with the document.
	 * 
	 * @param - As per struts standard.
	 * @return Returns Action Forward for the detail summary for
	 *         DocumentViewModifyDelete jsp..
	 **/
	public ActionForward viewDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		DocumentAddBean bean = (DocumentAddBean) form;
		DocumentAddDTO dto = new DocumentAddDTO();
		DocumentAddDao dao = new DocumentAddDao();
		String docVersion = "";
		String libName = "";
		String libId = "";
		String docId = "";
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		Map<String, Object> map = new HashMap<String, Object>();

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		bean.setSource(((String) request.getAttribute("SourceType") == null ? request
				.getParameter("Source") : (String) request
				.getAttribute("SourceType")));
		request.setAttribute("SourceType", bean.getSource());
		HttpSession session = request.getSession(true);
		String loginuserid = (String) request.getSession().getAttribute(
				"userid");
		String abc = (request.getParameter("abc") == null ? "" : request
				.getParameter("abc"));
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
		// expired
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		if (bean.getSource().equals("Ilex") && !abc.equals("y")) {
			dao.setMenuForEntity(dto, attrmap, paramMap);
			try {
				BeanUtils.copyProperties(bean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			showMenuViewSelector(bean, loginuserid, request);
		}
		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		String linklibName = bean.getLinkLibNameFromIlex();
		libId = (request.getParameter("libId") == null ? (String) request
				.getAttribute("libId") : request.getParameter("libId"));
		docId = (request.getParameter("docId") == null ? (String) request
				.getAttribute("docId") : request.getParameter("docId"));
		libName = DocumentUtility.getLibraryName(libId);
		logger.trace("Class : ViewManipulation Method : deleteDocument ");
		logger.trace("This method is used to delete the document from the library ");

		if (!Authentication.getPageSecurity(loginuserid, libName,
				"Document Delete")) {
			return (mapping.findForward("UnAuthenticate"));
		}
		;
		request.setAttribute("libName", libName);
		request.setAttribute("libId", libId);
		request.setAttribute("docId", docId);
		if (request.getParameter("docVersion") == null)
			docVersion = "";
		else
			docVersion = request.getParameter("docVersion");
		try {
			Document document = ClientOperator.getDocument(docId, docVersion);
			request.setAttribute("document", document);
			bean.setLinkedDocument(DocumentAddDao.getLinkDocId(docId));
			bean.setMyfilename(document.getFileName());
			if (logger.isDebugEnabled()) {
				logger.debug("viewDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - type "
						+ bean.getType());
			}
			DocumentAddDao.propertyUtil(document, bean);

			try {
				BeanUtils.copyProperties(dto, bean);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			dao.populateText("viewDocument", attrmap, paramMap, map, libId, dto);
			try {
				BeanUtils.copyProperties(bean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			WebUtil.copyMapToRequest(request, map);

			String deleteNow = (request.getParameter("delete") == null ? ""
					: request.getParameter("delete"));
			if (bean.getSource().equals("Ilex")) {
				bean.setType(bean
						.getType()
						.equalsIgnoreCase(
								DatabaseUtilityDao
										.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments"))
						|| bean.getType()
								.equalsIgnoreCase(
										DatabaseUtilityDao
												.getKeyValue("ilex.docm.libraryname.msasupportingdocuments"))
						|| bean.getType()
								.equalsIgnoreCase(
										DatabaseUtilityDao
												.getKeyValue("ilex.docm.libraryname.deliverables")) ? linklibName
						: bean.getType());
				bean.setLinkLibNameFromIlex(linklibName);
			}
			bean.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), bean.getJobId()));
			if (DocumentUtility.getFieldsValue(document, "source")
					.equalsIgnoreCase("Ilex")
					&& DocumentUtility.getFieldsValue(document, "AddMethod")
							.equalsIgnoreCase("Implicit")
					&& !bean.getType()
							.equalsIgnoreCase(
									DatabaseUtilityDao
											.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments")) // Appendix
																												// Supporting
																												// Documents
					&& !bean.getType()
							.equalsIgnoreCase(
									DatabaseUtilityDao
											.getKeyValue("ilex.docm.libraryname.msasupportingdocuments"))// MSA
																											// Supporting
																											// Documents
					&& !bean.getType()
							.equalsIgnoreCase(
									DatabaseUtilityDao
											.getKeyValue("ilex.docm.libraryname.deliverables"))// Deliverables
					&& deleteNow.equalsIgnoreCase("DeleteNow")) {
				request.setAttribute("sourceNotValid", "Document with the "
						+ DocumentUtility.getFieldsValue(document, "source")
						+ " source can not be deleted from DocM.");
			}
		} catch (CouldNotCheckDocumentException e) {
			logger.error(
					"viewDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"viewDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
			request.setAttribute("Comment",
					"Could not found document in DocM System.");
			return (mapping.findForward("failure"));
		} catch (DocumentNotExistsException e) {
			logger.error(
					"viewDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"viewDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
			request.setAttribute("Comment",
					"Document does not exist in DocM System.");
			return (mapping.findForward("failure"));
		}
		return (mapping.findForward("viewDocumentDetail"));
	}

	/**
	 * unspecified - This method is used to show the Modify document Page. get
	 * the library name for a libraryId. get the list of taxonomy group. get the
	 * list of metadata fields. get the document Object for the documentId.
	 * 
	 * @param - As per struts standard.
	 * @return Returns Action Forward for the modifying document to
	 *         DocumentViewModifyDelete jsp..
	 **/
	public ActionForward modifyDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("status", request.getParameter("status"));
		request.setAttribute("libraryID", request.getParameter("libId"));
		DocumentAddBean bean = (DocumentAddBean) form;
		DocumentAddDTO dto = new DocumentAddDTO();
		HttpSession session = request.getSession(true);
		DocumentAddDao dao = new DocumentAddDao();
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		Map<String, Object> map = new HashMap<String, Object>();

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		ArrayList libNameList = null;
		String libName = "";
		String libId = "";
		String docId = "";
		String loginuserid = (String) session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
		if (request.getAttribute("SourceType") != null
				&& request.getAttribute("SourceType").toString()
						.equalsIgnoreCase("Ilex")
				&& (request.getAttribute("docId") == null || request
						.getAttribute("docId").toString().equals(""))) {
			return mapping.findForward("error");
		}
		libId = (request.getParameter("libId") == null) ? request
				.getAttribute("libId").toString().trim() : request
				.getParameter("libId");
		docId = (request.getParameter("docId") == null) ? request
				.getAttribute("docId").toString().trim() : request
				.getParameter("docId");
		request.setAttribute("libId", libId);
		request.setAttribute("docId", docId);
		bean.setSource(((String) request.getAttribute("SourceType") == null ? request
				.getParameter("Source") : (String) request
				.getAttribute("SourceType")));
		String abc = (request.getParameter("abc") == null ? "" : request
				.getParameter("abc"));
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		if (bean.getSource().equals("Ilex") && !abc.equals("y")) {
			dao.setMenuForEntity(dto, attrmap, paramMap);
			try {
				BeanUtils.copyProperties(bean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			showMenuViewSelector(bean, loginuserid, request);
		}
		request.setAttribute("SourceType", bean.getSource());
		String linklibName = bean.getLinkLibNameFromIlex();
		if (logger.isDebugEnabled()) {
			logger.debug("modifyDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - bean source modify"
					+ bean.getSource());
		}
		libName = DocumentUtility.getLibraryName(libId);
		if (!Authentication.getPageSecurity(loginuserid, libName,
				"Document Modify")) {
			return (mapping.findForward("UnAuthenticate"));
		}

		dao.populateText("modifyDocument", attrmap, paramMap, map, libId, dto);
		WebUtil.copyMapToRequest(request, map);
		try {
			Document document = ClientOperator.getDocument(docId, "");
			DocumentAddDao.propertyUtil(document, bean);
			request.setAttribute("document", document);

			if (((String) request.getAttribute("SourceType")).equals("Ilex")) {
				bean.setType(bean
						.getType()
						.equalsIgnoreCase(
								DatabaseUtilityDao
										.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments"))
						|| bean.getType()
								.equalsIgnoreCase(
										DatabaseUtilityDao
												.getKeyValue("ilex.docm.libraryname.msasupportingdocuments"))
						|| bean.getType()
								.equalsIgnoreCase(
										DatabaseUtilityDao
												.getKeyValue("ilex.docm.libraryname.deliverables")) ? linklibName
						: bean.getType());
				bean.setLinkLibNameFromIlex(linklibName);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("modifyDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in side ------------------beangetjobid"
						+ bean.getJobId());
			}
			bean.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), bean.getJobId()));
			if (!DocumentUtility.getFieldsValue(document, "source")
					.equalsIgnoreCase(
							(String) request.getAttribute("SourceType"))
					&& !bean.getType()
							.equalsIgnoreCase(
									DatabaseUtilityDao
											.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments"))
					&& !bean.getType()
							.equalsIgnoreCase(
									DatabaseUtilityDao
											.getKeyValue("ilex.docm.libraryname.msasupportingdocuments"))) {
				request.setAttribute("sourceNotValid", "Document with the "
						+ DocumentUtility.getFieldsValue(document, "source")
						+ " source can not be modified from DocM.");
				return (mapping.findForward("viewDocument"));
			}
			DocumentUtility.setFieldsValue(document, "source",
					(String) request.getAttribute("SourceType"));
		} catch (CouldNotCheckDocumentException e) {
			logger.error(
					"modifyDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"modifyDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
			request.setAttribute("Comment",
					"Could not found document in DocM System.");
			return (mapping.findForward("failure"));
		} catch (DocumentNotExistsException e) {
			logger.error(
					"modifyDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"modifyDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
			request.setAttribute("Comment",
					"Document does not exist in DocM System.");
			return (mapping.findForward("failure"));
		}

		// Added by pankaj
		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		// Added by Pankaj
		return (mapping.findForward("viewDocumentDetail"));
	}

	/**
	 * unspecified - This method is used to delete document from docm system.
	 * call the deletedocument method of client operetor class.
	 * 
	 * @param - As per struts standard.
	 * @return Returns Action Forward.
	 **/
	public ActionForward deleteDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		DocumentAddBean bean = (DocumentAddBean) form;
		DocumentAddDao dao = new DocumentAddDao();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) request.getSession().getAttribute(
				"userid");
		String loginUserName = (String) request.getSession().getAttribute(
				"username");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire"));
		if (session.getAttribute("username") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String libId = request.getParameter("libId");
		String libNamne = DocumentUtility.getLibraryName(libId);
		String dispatch = null;
		logger.trace("Class : ViewManipulation Method : deleteDocument ");
		logger.trace("This method is used to delete the document from the library ");
		bean.setSource(((String) request.getAttribute("SourceType") == null ? request
				.getParameter("Source") : (String) request
				.getAttribute("SourceType")));
		request.setAttribute("SourceType", bean.getSource());
		if (!Authentication.getPageSecurity(loginuserid, libNamne,
				"Document Delete")) {
			return (mapping.findForward("UnAuthenticate"));
		}

		try {
			ClientOperator.deleteDocument(request.getParameter("docId"),
					loginUserName);
			request.setAttribute("Comment",
					"Document Successfully deleted from DocM System.");
		} catch (DocumentNotExistsException e) {
			logger.error(
					"deleteDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			request.setAttribute("Comment",
					"Document does not exist in DocM System.");
			logger.error(
					"deleteDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		} catch (CouldNotPhysicallyDeleteException e) {
			logger.error(
					"deleteDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			request.setAttribute("Comment",
					"Document can not be physically deleted from DocM System.");
			logger.error(
					"deleteDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		} catch (CouldNotCheckDocumentException e) {
			logger.error(
					"deleteDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			request.setAttribute("Comment",
					"Document could not found in DocM System.");
			logger.error(
					"deleteDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		} catch (CouldNotMarkDeleteException e) {
			logger.error(
					"deleteDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			request.setAttribute("Comment",
					"Document can not be marke as deleted in DocM System.");
			logger.error(
					"deleteDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		bean.setSource((String) request.getAttribute("SourceType") == null ? bean
				.getSource() : (String) request.getAttribute("SourceType"));
		if (bean.getSource().equals("Ilex")) {
			request.setAttribute("sourceNotValid",
					"Document has been deleted from DocM.");
			String entityType = (request.getParameter("entityType") == null ? ""
					: request.getParameter("entityType"));
			String entityId = (request.getParameter("entityId") == null ? ""
					: request.getParameter("entityId"));
			if (entityType
					.equalsIgnoreCase(DatabaseUtilityDao
							.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments"))
					|| entityType
							.equalsIgnoreCase(DatabaseUtilityDao
									.getKeyValue("ilex.docm.libraryname.msasupportingdocuments"))
					// ||bean.getType().equalsIgnoreCase(DatabaseUtilityDao.getKeyValue("ilex.docm.libraryname.deliverables"))){
					|| entityType.equalsIgnoreCase(DatabaseUtilityDao
							.getKeyValue("ilex.docm.libraryname.deliverables"))) {
				String libName = (request.getParameter("linkLibName") == null ? ""
						: request.getParameter("linkLibName"));
				bean.setMsaId(libName.equalsIgnoreCase("msa") ? entityId : "");
				bean.setAppendixId((libName.equalsIgnoreCase("Appendix") ? entityId
						: ""));
				bean.setJobId((libName.equalsIgnoreCase("Addendum") ? entityId
						: ""));
				bean.setJobId((libName.equalsIgnoreCase("Job") ? entityId : ""));
				String url = "";
				if (libName.equalsIgnoreCase("MSA")) {
					dispatch = "MSAUpdate";
				} else if (libName.equalsIgnoreCase("Appendix")) {
					String msaId = "";
					msaId = IMdao.getMSAId(bean.getAppendixId(),
							getDataSource(request, "ilexnewDB"));
					String appendixStatus = Appendixdao.getAppendixStatus(
							bean.getAppendixId(), msaId,
							getDataSource(request, "ilexnewDB"));
					String appendixType = Appendixdao.getAppendixPrType(
							bean.getAppendixId(),
							getDataSource(request, "ilexnewDB"));
					if (appendixStatus.equalsIgnoreCase("Inwork")) {
						url = "AppendixHeader.do?function=view&fromPage=appendix&appendixid="
								+ bean.getAppendixId();
					} else {
						msaId = IMdao.getMSAId(bean.getAppendixId(),
								getDataSource(request, "ilexnewDB"));
						url = "AppendixUpdateAction.do?firstCall=true&MSA_Id="
								+ msaId;
					}
					dispatchJsp(request, response, url);
				} else if (libName.equalsIgnoreCase("Addendum")) {
					url = "JobDashboardAction.do?hmode=unspecified&jobid="
							+ bean.getJobId();
					dispatchJsp(request, response, url);
				} else if (libName.equalsIgnoreCase("Job")) {
					url = "JobDashboardAction.do?hmode=unspecified&jobid="
							+ bean.getJobId();
					dispatchJsp(request, response, url);
				}
			}
		} else {
			request.setAttribute("libId", request.getParameter("libId"));
			request.setAttribute("docId", request.getParameter("docId"));
			request.setAttribute("docId", DocumentUtility
					.getLibraryName(request.getParameter("libId")));
			dispatch = "DocMaster";
		}
		return (mapping.findForward(dispatch));
	}

	/**
	 * moveDocument - This method is used to move the document to different
	 * library. get all library name for a libraryId. get the list of taxonomy
	 * group. get the list of metadata fields for a library. get the document
	 * Object for the documentId from clientOperator class.
	 * 
	 * @param - As per struts standard.
	 * @return Returns Action Forward for the Moving document to
	 *         DocumentViewModifyDelete jsp..
	 **/
	public ActionForward moveDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		DocumentAddDao dao = new DocumentAddDao();
		DocumentAddBean bean = (DocumentAddBean) form;
		DocumentAddDTO dto = new DocumentAddDTO();
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		Map<String, Object> map = new HashMap<String, Object>();

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		dao.populateText("moveDocument", attrmap, paramMap, map, "", dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		try {
			String doc_id = request.getParameter("docId");
			bean.setSource((request.getParameter("Source") == null ? ""
					: request.getParameter("Source")));
			dao.populateText("moveDocument", attrmap, paramMap, map,
					request.getParameter("libId"), dto);
			try {
				BeanUtils.copyProperties(bean, dto);
			} catch (IllegalAccessException e1) {
				logger.error(e1);
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				logger.error(e1);
				e1.printStackTrace();
			}
			WebUtil.copyMapToRequest(request, map);
			Document document = ClientOperator.getDocument(doc_id, "");
			request.setAttribute("document", document);
			request.setAttribute("docId", doc_id);
			DocumentAddDao.propertyUtil(document, bean);
			request.setAttribute("move", "moveDocument");
			request.setAttribute(
					"SourceType",
					(request.getParameter("Source") == null ? "" : request
							.getParameter("Source")));
		} catch (CouldNotCheckDocumentException e) {
			logger.error(
					"moveDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"moveDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
			request.setAttribute("Comment",
					"Could not found document in DocM System.");
			return (mapping.findForward("failure"));
		} catch (DocumentNotExistsException e) {
			logger.error(
					"moveDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"moveDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
			request.setAttribute("Comment",
					"Document does not exist in DocM System.");
			return (mapping.findForward("failure"));
		}
		return (mapping.findForward("viewDocumentDetail"));
	}

	/**
	 * Show menu view selector.
	 * 
	 * @param docMForm
	 *            the doc m form
	 * @param loginuserid
	 *            the loginuserid
	 * @param request
	 *            the request
	 */
	public void showMenuViewSelector(DocumentAddBean docMForm,
			String loginuserid, HttpServletRequest request) {
		// String
		// msaId=(request.getAttribute("msaId")==null?"":(String)request.getAttribute("msaId"));
		// String
		// appendixId=(request.getAttribute("appendixId")==null?"":(String)request.getAttribute("appendixId"));
		// String
		// jobId=(request.getAttribute("jobId")==null?"":(String)request.getAttribute("jobId"));
		//
		if (docMForm.getType().equalsIgnoreCase("MSA")) {
			docMForm.setMSA_Name(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			request.setAttribute("MSA_Id", docMForm.getMsaId());
			String msaStatus = MSAdao.getMSAStatus(docMForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String msaUploadCheck = MSAdao.getMSAUploadCheck(
					docMForm.getMsaId(), getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
					docMForm.getMsaId(), "", "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);
			request.setAttribute("status", msaStatus);
			request.setAttribute("uploadstatus", msaUploadCheck);
		}

		if (docMForm.getType().equals("Appendix")) {
			docMForm.setAppendixId(docMForm.getAppendixId());
			docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			docMForm.setMSA_Name(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			String appendixStatus = Appendixdao.getAppendixStatus(
					docMForm.getAppendixId(), docMForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			docMForm.setAppendix_Name(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setAppendixStatusChecking(appendixStatus);
			docMForm.setAppendixType(Appendixdao.getAppendixPrType(
					docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));

			if (docMForm.getAppendixStatusChecking().trim()
					.equalsIgnoreCase("Inwork")) {
				String contractDocoumentList = com.mind.dao.PM.Appendixdao
						.getcontractDocoumentMenu(docMForm.getAppendixId(),
								getDataSource(request, "ilexnewDB"));
				request.setAttribute("contractDocMenu", contractDocoumentList);
				String appendixcurrentstatus = Appendixdao.getCurrentstatus(
						docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixcurrentstatus",
						appendixcurrentstatus);
				docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB")));
				request.setAttribute("appendixid", docMForm.getAppendixId());
				request.setAttribute("msaId", docMForm.getMsaId());
				request.setAttribute("nettype", "");

			} else {
				String addendumlist = Appendixdao.getAddendumsIdName(
						docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				String addendumid = com.mind.dao.PM.Addendumdao
						.getLatestAddendumId(docMForm.getAppendixId(),
								getDataSource(request, "ilexnewDB"));
				String appendixType = Appendixdao.getAppendixPrType(
						docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				String list = Menu.getStatus("pm_appendix",
						appendixStatus.charAt(0), docMForm.getMsaId(),
						docMForm.getAppendixId(), "", "", loginuserid,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixStatusList", list);
				request.setAttribute("Appendix_Id", docMForm.getAppendixId());
				request.setAttribute("MSA_Id", docMForm.getMsaId());
				request.setAttribute("appendixStatus", appendixStatus);
				request.setAttribute("latestaddendumid", addendumid);
				request.setAttribute("addendumlist", addendumlist);
				request.setAttribute("appendixType", appendixType);
			}
		}

		if (docMForm.getType().equals("Addendum")) {
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					docMForm.getJobId(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			docMForm.setJob_Name(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), docMForm.getJobId()));
			String jobViewtype = JobDashboarddao.getJobViewType(
					docMForm.getJobId(), getDataSource(request, "ilexnewDB"));
			docMForm.setAppendixId(IMdao.getAppendixId(docMForm.getJobId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendix_Name(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setMSA_Name(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			docMForm.setJob_Name(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), docMForm.getJobId()));

			String list = Menu.getStatus("Addendum", jobStatus.charAt(0), "",
					docMForm.getAppendixId(), docMForm.getJobId(), "",
					loginuserid, getDataSource(request, "ilexnewDB"));
			request.setAttribute("addendumStatusList", list);
			String list_new = Menu.getStatus("Addendum_New", jobStatus.charAt(0), "",
					docMForm.getAppendixId(), docMForm.getJobId(), "",
					loginuserid, getDataSource(request, "ilexnewDB"));
			request.setAttribute("addendumStatusListNew", list_new);
			request.setAttribute("Appendix_Id", docMForm.getAppendixId());
			request.setAttribute("Job_Id", docMForm.getJobId());
			request.setAttribute("job_Status", jobStatus);
			request.setAttribute("jobViewType", jobViewtype);
			Map<String, Object> map;

			if (docMForm.getJobId() != null
					&& !docMForm.getJobId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(docMForm.getJobId(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}
		if (docMForm.getType().equals("PO") || docMForm.getType().equals("WO")) {
			docMForm.setJobId((POWODetaildao.getJobIdFromPoId(
					getDataSource(request, "ilexnewDB"), docMForm.getEntityId())));
			docMForm.setJob_Name(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), docMForm.getJobId()));
			docMForm.setPoId(docMForm.getEntityId());
			docMForm.setWO_ID(docMForm.getEntityId());
			docMForm.setPartner_Name(DocumentAddDao.getPartnerName(
					getDataSource(request, "ilexnewDB"), docMForm.getPoId()));
			docMForm.setAppendixId(IMdao.getAppendixId(docMForm.getJobId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendix_Name(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setMSA_Name(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
		}
		if (docMForm.getType().equals("Supplement")) {
			docMForm.setJobId((POWODetaildao.getJobIdFromPoId(
					getDataSource(request, "ilexnewDB"), docMForm.getEntityId())));
			docMForm.setJob_Name(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), docMForm.getJobId()));
			docMForm.setPartner_Name(DocumentAddDao.getPartnerName(
					getDataSource(request, "ilexnewDB"), docMForm.getPoId()));
			docMForm.setPoId(docMForm.getEntityId());
		}
		if (docMForm.getType().equals("Job")) {
			String[] jobStatusAndType = Jobdao.getProjectJobStatusAndType(
					docMForm.getJobId(), getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			if (logger.isDebugEnabled()) {
				logger.debug("showMenuViewSelector(DocumentAddBean, String, HttpServletRequest) - jobType1 "
						+ jobType1 + " jobstatus " + jobStatus);
			}

			docMForm.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), docMForm.getJobId()));
			docMForm.setAppendixId(IMdao.getAppendixId(docMForm.getJobId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendixType(Appendixdao.getAppendixPrType(
					docMForm.getMsaId(), docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			docMForm.setAppendix_Name(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setMSA_Name(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			docMForm.setJob_Name(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), docMForm.getJobId()));
			if (jobStatus != null) {
				String list = Menu.getStatus("prj_job", jobStatus.charAt(0),
						"", docMForm.getAppendixId(), docMForm.getJobId(), "",
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("prjJobStatusList", list);
				
				String list_new = Menu.getStatus("prj_jobMenuNew", jobStatus.charAt(0),
						"", docMForm.getAppendixId(), docMForm.getJobId(), "",
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("prjJobStatusListMenuNew", list_new);
			}

			request.setAttribute("Appendix_Id", docMForm.getAppendixId());
			request.setAttribute("Job_Id", docMForm.getJobId());
			request.setAttribute("appendixType", docMForm.getAppendixType());
			Map<String, Object> map;

			if (docMForm.getJobId() != null
					&& !docMForm.getJobId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(docMForm.getJobId(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}

		}
	}

	/**
	 * Dispatch jsp.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param url
	 *            the url
	 */
	public void dispatchJsp(HttpServletRequest request,
			HttpServletResponse response, String url) {
		try {
			if (request instanceof MultipartRequestWrapper) {
				request = ((MultipartRequestWrapper) request).getRequest();
			}
			RequestDispatcher dispatcher = request.getRequestDispatcher(url);
			dispatcher.forward(request, response);
		} catch (Exception e) {
			logger.error(
					"dispatchJsp(HttpServletRequest, HttpServletResponse, String)",
					e);

			logger.trace("" + e);
		}
	}

}
