package com.mind.docm.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.docm.dao.LibraryManagerDao;
import com.mind.docm.security.Authentication;

/**
 * The Class LibraryManagerAction.
 */
public class LibraryManagerAction extends com.mind.common.IlexDispatchAction {

	/**
	 * Method :unspecified
	 * 
	 * @purpose This method is called first and call LibraryList method of same
	 *          Action.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authentication.getPageSecurity(loginuserid, "Library",
				"Library List")) {
			return (mapping.findForward("UnAuthenticate"));
		}
		return this.LibraryList(mapping, form, request, response);
	}

	/**
	 * Method :LibraryList
	 * 
	 * @purpose This method is used to get the library list using
	 *          LibraryManagerDao.
	 * @steps 1. Create an object of LibraryManagerDao class. 2. Call the
	 *        libraryList method of LibraryManagerDao. 3. Get an Object of
	 *        ArrayList. 4. Set this Object to request attribute.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward LibraryList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		LibraryManagerDao libManagerDao = new LibraryManagerDao();
		ArrayList libraryList = libManagerDao.libraryList();
		request.setAttribute("libraryList", libraryList);
		return (mapping.findForward("libList"));
	}
}
