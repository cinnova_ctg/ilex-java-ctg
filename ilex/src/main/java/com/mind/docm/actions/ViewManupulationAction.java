/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: <This file contains the whole logic 
 * 							for the library add/modify/delete/view.>
 *
 */

package com.mind.docm.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.ViewManupulationDTO;
import com.mind.docm.dao.ViewManupulationDao;
import com.mind.docm.formbean.ViewManupulationBean;
import com.mind.docm.security.Authentication;
import com.mind.docm.utils.DocumentUtility;
import com.mind.util.WebUtil;

/**
 * Class ViewManupulationAction - This file contains the for the library
 * add/modify/delete/view. methods - addView, insertData, deleteData
 */
public class ViewManupulationAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ViewManupulationAction.class);

	/**
	 * addView - This method helps in add the view..
	 * 
	 * @param - As per struts standard.
	 * @return Returns Action Forward.
	 **/
	public ActionForward addView(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		ViewManupulationDao vmd = new ViewManupulationDao();
		ViewManupulationBean bean = (ViewManupulationBean) form;
		ViewManupulationDTO dto = new ViewManupulationDTO();

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		String libId = request.getParameter("libId");
		String libName = DocumentUtility.getLibraryName(libId);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		logger.trace("Class : ViewManipulation Method : unspecified ");
		logger.trace("This method is used to view/add/update/delete library view details");

		if (request.getParameter("function").equals("add")) {
			if (!Authentication.getPageSecurity(loginuserid, libName,
					"Add View")) {
				return (mapping.findForward("UnAuthenticate"));
			}
		} else if (request.getParameter("function").equals("update")) {
			if (!Authentication.getPageSecurity(loginuserid, libName,
					"Modify View")) {
				return (mapping.findForward("UnAuthenticate"));
			}
		} else if (request.getParameter("function").equals("delete")) {
			if (!Authentication.getPageSecurity(loginuserid, libName,
					"Delete View")) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		// Getting library id for which the view is
		// being created.
		String libraryId = request.getParameter("libId");
		bean.setLibId(libraryId);
		bean.setLibName(libName);

		bean.setFunction(request.getParameter("function"));
		bean.setViewId(request.getParameter("viewId"));
		bean.setViewName(request.getParameter("viewName"));
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		if (bean.getFunction().equalsIgnoreCase("add"))
			vmd.getColumns(map);
		// Getting inserted data for perticular view.
		else if (bean.getFunction().equalsIgnoreCase("update")
				|| bean.getFunction().equalsIgnoreCase("delete")
				|| bean.getFunction().equalsIgnoreCase("view"))
			vmd.fetchInsertedData(map, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		logger.trace("End :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		return (mapping.findForward("viewLibList"));
	}

	/**
	 * insertData - This method helps in inserting data for view.
	 * 
	 * @param - As per struts standered.
	 * @return Returns Action Forward.
	 **/
	public ActionForward insertData(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : insertData)");
		ViewManupulationDao vmd = new ViewManupulationDao();
		ViewManupulationBean bean = (ViewManupulationBean) form;
		ViewManupulationDTO dto = new ViewManupulationDTO();
		String libraryId = request.getParameter("libId");
		bean.setLibId(libraryId);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		vmd.insertData(map, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);

		logger.trace("End :(Class : com.mind.docm.actions.ViewManupulationAction | Method : insertData)");
		return (mapping.findForward("viewLibList"));
	}

	/**
	 * deleteData - This method helps in deleting view.
	 * 
	 * @param - As per struts standered.
	 * @return Returns Action Forward.
	 **/
	public ActionForward deleteData(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : deleteData)");
		ViewManupulationDao vmd = new ViewManupulationDao();
		ViewManupulationBean bean = (ViewManupulationBean) form;
		ViewManupulationDTO dto = new ViewManupulationDTO();
		String libraryId = request.getParameter("libId");
		String libName = request.getParameter("libName");

		bean.setLibId(libraryId);
		bean.setLibName(libName);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		vmd.deleteData(map, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		request.setAttribute("delete", "delete");

		try {
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("ViewLibrary.do?function=unspecified&libId="
							+ bean.getLibId() + "&libName=" + bean.getLibName());
			dispatcher.forward(request, response);
		} catch (Exception e) {
			logger.error(
					"deleteData(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			logger.error(
					"deleteData(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		logger.trace("End :(Class : com.mind.docm.actions.ViewManupulationAction | Method : deleteData)");
		return null;
	}

}
