package com.mind.docm.actions;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.DocMSearchBean;
import com.mind.bean.docm.DynamicComboDocM;
import com.mind.docm.dao.DocMSeacrhDao;
import com.mind.docm.formbean.DocMSearchForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

/**
 * The Class DocumentSearchAction1.
 */
public class DocumentSearchAction1 extends com.mind.common.IlexAction {

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String linkToDoc = null;
		DocMSearchForm docmsearchform = (DocMSearchForm) form;
		DocMSearchBean docMSearchBean = new DocMSearchBean();
		DynamicComboDocM dcdocm = new DynamicComboDocM();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		// For Menu's from Ilex
		String entityType = null;
		String msaId = null;
		String appendixId = null;
		String jobId = null;
		String libName = null;
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);

		// When Searching supplement documents from Ilex regarding any entity
		BeanUtils.copyProperties(docMSearchBean, docmsearchform);
		if (request.getAttribute("searchSupplements") != null) {
			DocMSeacrhDao.setSearchConditionForSupplementsFromIlex(attrmap,
					docMSearchBean);
			BeanUtils.copyProperties(docmsearchform, docMSearchBean);
		}

		// From MPO Page
		if (docmsearchform.getSearchNow() == null) {
			DocMSeacrhDao.setSearchConditionForMpo(paramMap, docMSearchBean);
			BeanUtils.copyProperties(docmsearchform, docMSearchBean);
		}

		if (request.getParameter("linkToDoc") == null
				&& docmsearchform.getLinkToDoc() == null)
			linkToDoc = "";
		else
			linkToDoc = request.getParameter("linkToDoc");
		docmsearchform.setLinkToDoc(linkToDoc);
		docMSearchBean.setLinkToDoc(linkToDoc);

		// When Search button is clicked on the DocumentSearch Page or when
		// searching supplemnts documents in DocM from Ilex
		attrmap = WebUtil.copyRequestToAttributeMap(request);
		Map<String, Object> innerMap = new HashMap<String, Object>();
		Map<String, Object> map;
		if (docmsearchform.getSearchNow() != null) {
			map = DocMSeacrhDao.whenSearchButtonPressed(attrmap, innerMap,
					docMSearchBean, loginuserid);
			BeanUtils.copyProperties(docmsearchform, docMSearchBean);
			WebUtil.copyMapToRequest(request, innerMap);
			WebUtil.copyMapToRequest(request, map);

		}
		DocMSeacrhDao dmsd = new DocMSeacrhDao();
		// Get the libraryList to fill the libraryList combobox on the JSP
		dcdocm.setLiblist(DocMSeacrhDao.getlibraryList());

		map = dmsd.getColumns();
		WebUtil.copyMapToRequest(request, map);
		request.setAttribute("dcdocm", dcdocm);

		// For Menu's from Ilex
		if (request.getAttribute("entityType") != null
				&& (request
						.getAttribute("entityType")
						.toString()
						.trim()
						.equalsIgnoreCase(
								DatabaseUtilityDao
										.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments"))
						|| request
								.getAttribute("entityType")
								.toString()
								.trim()
								.equalsIgnoreCase(
										DatabaseUtilityDao
												.getKeyValue("ilex.docm.libraryname.msasupportingdocuments")) || request
						.getAttribute("entityType")
						.toString()
						.trim()
						.equalsIgnoreCase(
								DatabaseUtilityDao
										.getKeyValue("ilex.docm.libraryname.deliverables")))) {
			attrmap = WebUtil.copyRequestToAttributeMap(request);
			BeanUtils.copyProperties(docMSearchBean, docmsearchform);
			map = DocMSeacrhDao.setIlexMenu(attrmap, docMSearchBean,
					loginuserid, getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(docmsearchform, docMSearchBean);
			WebUtil.copyMapToRequest(request, map);
		} else {
			docmsearchform.setLinkLibNameFromIlex("DocM");
		}
		return mapping.findForward("success");
	}
}
