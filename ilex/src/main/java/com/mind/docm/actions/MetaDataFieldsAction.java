package com.mind.docm.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.MetaData;
import com.mind.docm.dao.MetadataFieldListDao;
import com.mind.docm.security.Authentication;

/**
 * The Class MetaDataFieldsAction.
 */
public class MetaDataFieldsAction extends com.mind.common.IlexDispatchAction {

	/**
	 * Method :unspecified
	 * 
	 * @purpose This method is called first and call MetaDataList method of same
	 *          Action.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authentication.getPageSecurity(loginuserid, "MetaData",
				"MetaData List")) {
			return (mapping.findForward("UnAuthenticate"));
		}

		return this.MetaDataList(mapping, form, request, response);
	}

	/**
	 * Method :MetaDataList
	 * 
	 * @purpose This method is used to get the metadata list using
	 *          MetadataFieldListDao.
	 * @steps 1. Create an object of MetadataFieldListDao class. 2. Call the
	 *        metadataFieldsList method of MetadataFieldListDao. 3. Get an
	 *        Object of ArrayList. 4. Set this Object to request attaribute.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward MetaDataList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MetadataFieldListDao mdlistDao = new MetadataFieldListDao();
		MetaData mdList = mdlistDao.metadataFieldsList();
		request.setAttribute("mdList", mdList);
		return (mapping.findForward("mdList"));
	}

}
