package com.mind.docm.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.ViewLibraryDTO;
import com.mind.docm.dao.LibraryViewDetailDao;
import com.mind.docm.formbean.ViewLibraryBean;
import com.mind.docm.security.Authentication;
import com.mind.docm.utils.DocumentUtility;

/**
 * The Class ViewLibraryAction.
 */
public class ViewLibraryAction extends com.mind.common.IlexDispatchAction {

	/**
	 * Method :unspecified
	 * 
	 * @purpose This method is called first and call ViewLibrary method of same
	 *          Action.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	private static final Logger logger = Logger
			.getLogger(ViewLibraryAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		LibraryViewDetailDao libviewDao = new LibraryViewDetailDao();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		ViewLibraryBean bean = (ViewLibraryBean) form;
		ViewLibraryDTO dto = new ViewLibraryDTO();
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String libId = request.getParameter("libId");
		String libNamne = DocumentUtility.getLibraryName(libId);
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		libviewDao.setLibraryDetail(dto, libId);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		logger.trace("Class : ViewLibraryAction Method : unspecified ");
		logger.trace("This method is used to view library details");

		if (!Authentication.getPageSecurity(loginuserid, libNamne,
				"View Library")) {
			return (mapping.findForward("UnAuthenticate"));
		}

		return this.ViewLibrary(mapping, form, request, response);
	}

	/**
	 * Method :ViewLibrary
	 * 
	 * @purpose This method is used to get the al the view list from
	 *          LibraryViewDetailDao
	 * @steps 1. Create an object of LibraryViewDetailDao class. 2. Call the
	 *        ViewLibrary method of LibraryViewDetailDao. 3. Get an Object of
	 *        ArrayList. 4. Set this Object to request attaribute.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward ViewLibrary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ViewLibraryBean viewLib = (ViewLibraryBean) form;
		LibraryViewDetailDao libviewDao = new LibraryViewDetailDao();
		String lib_Id = request.getParameter("libId");
		String libNamne = DocumentUtility.getLibraryName(lib_Id);
		viewLib.setLibraryId(lib_Id);
		viewLib.setLibraryName(libNamne);
		ArrayList libraryView = libviewDao.libraryViewList(lib_Id);
		request.setAttribute("libraryView", libraryView);
		request.setAttribute("viewLib", viewLib);
		return (mapping.findForward("viewLibList"));
	}
}
