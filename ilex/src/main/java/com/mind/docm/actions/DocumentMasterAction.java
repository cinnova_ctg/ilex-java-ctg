package com.mind.docm.actions;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.DocumentMasterDTO;
import com.mind.common.MultipartRequestWrapper;
import com.mind.docm.DocumentGroupNode;
import com.mind.docm.dao.DocumentHistoryDao;
import com.mind.docm.dao.DocumentMasterDao;
import com.mind.docm.dao.LibraryViewDetailDao;
import com.mind.docm.formbean.DocumentMasterBean;
import com.mind.docm.security.Authentication;
import com.mind.docm.utils.DocumentUtility;
import com.mind.util.ImageInfoUtil;
import com.mind.util.ImageUtils;

/**
 * The Class DocumentMasterAction.
 */
public class DocumentMasterAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DocumentMasterAction.class);
	private static final String MASTER_SITE_IMAGE_MAX_WIDTH = "575";
	private static final String MASTER_SITE_IMAGE_MAX_HEIGHT = "245";

	/**
	 * Method :unspecified
	 * 
	 * @purpose This method is called first and call DocumentList method of same
	 *          Action.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		response.setHeader("Pragma", "No-Cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String tempFunctionName = "";
		String tempFunctionType = "";

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
		// expired
		String libId = request.getParameter("libId");
		String libName = DocumentUtility.getLibraryName(libId);
		logger.trace("Class : DocumentMasterAction Method : unspecified ");
		logger.trace("This method is used to show the user listing of the documents in a particular library");

		if (!Authentication.getPageSecurity(loginuserid, libName,
				"Document List")) {
			return (mapping.findForward("UnAuthenticate"));
		}

		return this.DocumentList(mapping, form, request, response);
	}

	/**
	 * Method :DocumentList
	 * 
	 * @purpose This method is used to get the document list using
	 *          DocumentMasterDao.
	 * @steps 1. Create an object of DocumentMasterDao class. 2. Call the
	 *        documentListList method of DocumentMasterDao. 3. Get an Object of
	 *        ArrayList. 4. Set this Object to request attaribute.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward DocumentList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ArrayList map = null;
		if (request.getParameter("isClick") != null) {
			String fn = request.getParameter("fieldName");
			String fv = request.getParameter("fieldVal");
			String[] fieldNames = fn.split("~");
			String[] fieldValues = fv.split("~");
			map = new ArrayList();
			for (int i = 0; i < fieldNames.length; i++) {
				ArrayList inner = new ArrayList();
				if (!fieldNames[i].trim().equalsIgnoreCase("")
						&& !fieldValues[i].trim().equalsIgnoreCase("")) {
					inner.add(fieldNames[i]);
					inner.add(fieldValues[i]);
				}
				map.add(inner);
			}
			// map.put(request.getParameter("fieldName"),
			// request.getParameter("fieldVal"));
		}
		DocumentMasterBean bean = (DocumentMasterBean) form;
		DocumentMasterDao docMasterDao = new DocumentMasterDao();
		LibraryViewDetailDao libviewDao = new LibraryViewDetailDao();
		String libId = request.getParameter("libId");
		String viewId = request.getParameter("viewId");
		String status = "";
		if (request.getParameter("status") != null)
			status = request.getParameter("status");

		if (viewId == null || viewId.equals("")) {
			long viewIdValue = libviewDao.getDefaultViewId(libId);
			if (viewIdValue > 0)
				viewId = viewIdValue + "";
		}
		bean.setViewId(viewId);
		try {
			DocumentGroupNode[] documentGroupNodes = DocumentGroupNode
					.getChildren(map, libId, viewId, status);
			if (documentGroupNodes != null)
				request.setAttribute("size", documentGroupNodes.length);
			request.setAttribute("documentGroupNodes", documentGroupNodes);
		} catch (Exception e) {
			logger.error(
					"DocumentList(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"DocumentList(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		// if(request.getParameter("isClick")!=null &&
		// !request.getParameter("isClick").trim().equalsIgnoreCase("")){
		// ArrayList libraryList=docMasterDao.documentMasterList(libId,viewId,
		// status);
		// request.setAttribute("libraryList", libraryList);
		// }
		ArrayList libraryView = libviewDao.libraryViewList(libId);
		request.setAttribute("libraryView", libraryView);

		String viewName = libviewDao.getViewName(viewId);
		request.setAttribute("viewName", viewName);

		String libName = DocumentUtility.getLibraryName(libId);
		request.setAttribute("libName", libName);

		if (request.getParameter("isClick") != null) {
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/docm/SubDocumentList.jsp");
			try {
				if (request
						.getClass()
						.getName()
						.equals("org.apache.struts.upload.MultipartRequestWrapper")) {
					logger.debug("Casting MultiparRequestWrapper to custom class");
					request = new MultipartRequestWrapper(request);
				}
				dispatcher.include(request, response);
				response.setContentType("application/xml");
			} catch (Exception e) {
				logger.error(
						"DocumentList(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				logger.error(
						"DocumentList(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);
			}
			return null;

		}
		return (mapping.findForward("docList"));
	}

	/**
	 * Open file.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 */
	public ActionForward openFile(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		DocumentMasterBean bean = (DocumentMasterBean) form;

		logger.trace("Class : DocumentMasterAction Method : openFile ");
		logger.trace("This method is used to show the user the avilable pdf document");

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String tempFunctionName = "";
		String tempFunctionType = "";
		String libId = "";
		String docId = "";
		String fileId = "";
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
		// expired
		if (logger.isDebugEnabled()) {
			logger.debug("openFile(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - =====================++++++++++++++++++++++++"
					+ request.getParameter("fileId"));
		}
		if (request.getParameter("fileId") == null
				|| request.getParameter("fileId").equals("")) {
			docId = request.getParameter("docId");
			fileId = DocumentUtility.getfileIdFromDoc(docId);
		} else {
			fileId = request.getParameter("fileId");
		}
		if (request.getParameter("libId") == null) {
			String lib[] = DocumentHistoryDao.getLibraryIdAndName(docId);
			libId = lib[0];
		} else {
			libId = request.getParameter("libId");
		}
		String libName = DocumentUtility.getLibraryName(libId);

		if (!Authentication.getPageSecurity(loginuserid, libName,
				"Document Download")) {
			return (mapping.findForward("UnAuthenticate"));
		}

		logger.trace("In Dowonloading the Pdf");
		ServletOutputStream outStream = null;
		DocumentMasterDao docMasterDao = new DocumentMasterDao();
		// fileId=request.getParameter("fileId");
		DocumentMasterDTO dto = new DocumentMasterDTO();

		docMasterDao.getFileData(fileId, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		byte[] byteFile = docMasterDao.openFile(fileId);
		response.setContentType("application/" + bean.getMIME());
		String filename = bean.getFilename();
		if (filename.contains("."))
			filename = filename.substring(0, filename.indexOf("."));

		response.setHeader("Cache-Control",
				"must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Content-disposition", "attachment;filename="
				+ filename + "." + bean.getMIME() + ";size=" + bean.getSize());

		// response.setHeader("Content-disposition","attachment;inline=" +
		// "Abc"+";size=" + 100);
		try {
			outStream = response.getOutputStream();
			System.out.println("=====================" + byteFile);
			outStream.write(byteFile);
		} catch (IOException e) {
			logger.error(
					"openFile(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"openFile(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		} finally {
			try {
				outStream.close();
			} catch (IOException e) {
				logger.error(
						"openFile(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				logger.error(
						"openFile(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);
			}
		}
		return null;
	}

	/**
	 * View file.(This function used to view file in jsp.)
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 * @throws IOException
	 * @throws ServletException
	 */
	public void viewFile(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DocumentMasterBean bean = (DocumentMasterBean) form;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String libId = "";
		String docId = "";
		String fileId = "";

		if (request.getParameter("fileId") == null
				|| request.getParameter("fileId").equals("")) {
			docId = request.getParameter("docId");
			fileId = DocumentUtility.getfileIdFromDoc(docId);
		} else {
			fileId = request.getParameter("fileId");
		}
		if (request.getParameter("libId") == null) {
			String lib[] = DocumentHistoryDao.getLibraryIdAndName(docId);
			libId = lib[0];
		} else {
			libId = request.getParameter("libId");
		}
		String libName = DocumentUtility.getLibraryName(libId);

		ServletOutputStream outStream = null;
		DocumentMasterDao docMasterDao = new DocumentMasterDao();
		DocumentMasterDTO dto = new DocumentMasterDTO();

		docMasterDao.getFileData(fileId, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}

		byte[] byteFile = docMasterDao.openFile(fileId);

		String filename = bean.getFilename();
		if (filename.contains("."))
			filename = filename.substring(0, filename.indexOf("."));

		/*
		 * bean.setFile(byteFile); // new Base64
		 * response.getOutputStream().write(byteFile.toString().getBytes());
		 * response.getOutputStream().flush();
		 * response.getOutputStream().close();
		 */

		String checkfilename = getLastThree(bean.getFilename());

		if (checkfilename.equals("peg") || checkfilename.equals("png")
				|| checkfilename.equals("png") || checkfilename.equals("bmp")
				|| checkfilename.equals("gif") || checkfilename.equals("jpg")
				|| checkfilename.equals("psd") || checkfilename.equals("thm")
				|| checkfilename.equals("tif") || checkfilename.equals("yuv")) {
			if (bean.getMIME().equals("jpeg") || bean.getMIME().equals("png")
					|| bean.getMIME().equals("x-png")
					|| bean.getMIME().equals("bmp")
					|| bean.getMIME().equals("gif")
					|| bean.getMIME().equals("jpg")
					|| bean.getMIME().equals("psd")
					|| bean.getMIME().equals("pspimage")
					|| bean.getMIME().equals("thm")
					|| bean.getMIME().equals("tif")
					|| bean.getMIME().equals("yuv")) {

				InputStream inputStream = new ByteArrayInputStream(byteFile);

				BufferedImage originalBufferedImage = ImageIO.read(inputStream);

				BufferedImage convertedBufferdImage = ImageUtils.resize(
						originalBufferedImage, 800, 400, true);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				ImageIO.write(convertedBufferdImage, "png", baos);
				baos.flush();
				byte[] resizedImageInByte = baos.toByteArray();
				baos.close();
				String strInLowerCase = bean.getMIME().toLowerCase();
				bean.setMIME(strInLowerCase);
				if (bean.getMIME().equals("jpeg")
						|| bean.getMIME().equals("png")
						|| bean.getMIME().equals("x-png")
						|| bean.getMIME().equals("bmp")
						|| bean.getMIME().equals("gif")
						|| bean.getMIME().equals("jpg")
						|| bean.getMIME().equals("psd")
						|| bean.getMIME().equals("pspimage")
						|| bean.getMIME().equals("thm")
						|| bean.getMIME().equals("tif")
						|| bean.getMIME().equals("yuv")) {

					response.setContentType("image/" + bean.getMIME());
					response.getOutputStream().write(resizedImageInByte);
					response.getOutputStream().flush();
					response.getOutputStream().close();
					// bean.setFile(resizedImageInByte);

					// response.getOutputStream().write(byteFile);
					// response.getOutputStream().flush();
					// response.getOutputStream().close();
				}

			}
		}
		if (checkfilename.equals("pdf") || bean.getMIME().equals("pdf")) {

			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control",
					"must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");

			response.setContentType("application/pdf");

			byte[] byteFilepdf = docMasterDao.openFile(fileId);
			response.getOutputStream().write(byteFilepdf);
			response.getOutputStream().flush();
			response.getOutputStream().close();

		} else {

			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment;filename="
					+ filename);
			response.getOutputStream().write(byteFile);
			response.getOutputStream().flush();
			response.getOutputStream().close();

		}

	}

	/**
	 * View file.(This function used to view file in jsp.)
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 * @throws IOException
	 * @throws ServletException
	 */
	public void viewResizedFile(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DocumentMasterBean bean = (DocumentMasterBean) form;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String libId = "";
		String docId = "";
		String fileId = "";

		if (request.getParameter("fileId") == null
				|| request.getParameter("fileId").equals("")) {
			docId = request.getParameter("docId");
			fileId = DocumentUtility.getfileIdFromDoc(docId);
		} else {
			fileId = request.getParameter("fileId");
		}
		if (request.getParameter("libId") == null) {
			String lib[] = DocumentHistoryDao.getLibraryIdAndName(docId);
			libId = lib[0];
		} else {
			libId = request.getParameter("libId");
		}
		String libName = DocumentUtility.getLibraryName(libId);

		ServletOutputStream outStream = null;
		DocumentMasterDao docMasterDao = new DocumentMasterDao();
		DocumentMasterDTO dto = new DocumentMasterDTO();

		docMasterDao.getFileData(fileId, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		byte[] byteFile = docMasterDao.openFile(fileId);
		int maxWidth = Integer.parseInt(MASTER_SITE_IMAGE_MAX_WIDTH);
		int maxHeight = Integer.parseInt(MASTER_SITE_IMAGE_MAX_HEIGHT);

		if (!bean.getMIME().equals("pdf")) {
			InputStream inputStream = new ByteArrayInputStream(byteFile);

			BufferedImage originalBufferedImage = ImageIO.read(inputStream);

			BufferedImage convertedBufferdImage = ImageUtils.resize(
					originalBufferedImage, maxWidth, maxHeight, true);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			ImageIO.write(convertedBufferdImage, "jpg", baos);
			baos.flush();
			byte[] resizedImageInByte = baos.toByteArray();
//			Image toolkitImage = Toolkit.getDefaultToolkit().createImage(
//					resizedImageInByte);
			baos.close();
			response.setContentType("image/jpeg");
			response.getOutputStream().write(resizedImageInByte);

			bean.setFile(resizedImageInByte);
		} else {
			response.getOutputStream().write(byteFile);

			bean.setFile(byteFile);
		}

		String filename = bean.getFilename();
		if (filename.contains("."))
			filename = filename.substring(0, filename.indexOf("."));

		response.setHeader("Content-disposition", "filename=" + filename + "."
				+ bean.getMIME());

		response.getOutputStream().flush();
		response.getOutputStream().close();

	}

	/**
	 * View file.(This function used to view file in jsp.)
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 * @throws IOException
	 * @throws ServletException
	 */
	public void getFileDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DocumentMasterBean bean = (DocumentMasterBean) form;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String libId = "";
		String docId = "";
		String fileId = "";

		if (request.getParameter("fileId") == null
				|| request.getParameter("fileId").equals("")) {
			docId = request.getParameter("docId");
			fileId = DocumentUtility.getfileIdFromDoc(docId);
		} else {
			fileId = request.getParameter("fileId");
		}
		if (request.getParameter("libId") == null) {
			String lib[] = DocumentHistoryDao.getLibraryIdAndName(docId);
			libId = lib[0];
		} else {
			libId = request.getParameter("libId");
		}

		DocumentMasterDao docMasterDao = new DocumentMasterDao();
		DocumentMasterDTO dto = new DocumentMasterDTO();

		docMasterDao.getFileData(fileId, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		byte[] byteFile = docMasterDao.openFile(fileId);
		String checkfilename = getLastThree(bean.getFilename());
		String filename = bean.getFilename();
		if (filename.contains("."))
			filename = filename.substring(0, filename.indexOf("."));
		String height = "400";
		String width = "800";
		String mimetype = "jpeg";
		if (bean.getMIME().equals("pdf")) {
			height = "400";
			width = "800";
			mimetype = "pdf";

		} else if (checkfilename.equals("peg") || checkfilename.equals("png")
				|| checkfilename.equals("png") || checkfilename.equals("bmp")
				|| checkfilename.equals("gif") || checkfilename.equals("jpg")
				|| checkfilename.equals("psd") || checkfilename.equals("thm")
				|| checkfilename.equals("tif") || checkfilename.equals("yuv")) {

			if ((bean.getMIME().equals("jpeg") || bean.getMIME().equals("png")
					|| bean.getMIME().equals("x-png")
					|| bean.getMIME().equals("bmp")
					|| bean.getMIME().equals("gif")
					|| bean.getMIME().equals("jpg")
					|| bean.getMIME().equals("psd")
					|| bean.getMIME().equals("pspimage")
					|| bean.getMIME().equals("thm")
					|| bean.getMIME().equals("tif") || bean.getMIME().equals(
					"yuv"))) {
				ImageInfoUtil imageInfoUtil = new ImageInfoUtil(byteFile);
				height = String.valueOf(imageInfoUtil.getHeight());
				width = String.valueOf(imageInfoUtil.getWidth());
				mimetype = "image";
			} else {

				response.setContentType("application/x-download");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + filename);
				response.getOutputStream().write(byteFile);
				response.getOutputStream().flush();
				response.getOutputStream().close();

			}
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("height", height);
		map.put("width", width);
		map.put("mimetype", mimetype);

		JSONObject array = JSONObject.fromObject(map);
		response.getOutputStream().write(array.toString().getBytes());
		response.getOutputStream().flush();
		response.getOutputStream().close();

	}

	// getting last three letter from string
	public String getLastThree(String myString) {
		if (myString.length() > 3)
			return myString.substring(myString.length() - 3);
		else
			return myString;
	}

}
