package com.mind.docm.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.docm.dao.LibraryListDao;

/**
 * The Class LibraryListAction.
 */
public class LibraryListAction extends com.mind.common.IlexDispatchAction {

	/**
	 * Method :unspecified
	 * 
	 * @purpose This method is called first and call LibraryList method of same
	 *          Action.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return this.LibraryList(mapping, form, request, response);
	}

	/**
	 * Method :LibraryList
	 * 
	 * @purpose This method is used to get the library names in library master
	 *          table.
	 * @steps 1. Create an object of LibraryMasterDao class. 2. Call the
	 *        libraryMasterList method of LibraryMasterDao. 3. Get an Object of
	 *        ArrayList. 4. Set this Object to LibraryListBean.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward LibraryList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		LibraryListDao libmstDao = new LibraryListDao();
		ArrayList libraryList = libmstDao.libraryMasterList();
		request.setAttribute("libraryList", libraryList);
		return (mapping.findForward("leftTree"));
	}
}
