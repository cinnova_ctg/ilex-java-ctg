package com.mind.docm.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboUM;
import com.mind.docm.dao.DocMRolePrivilegedao;
import com.mind.docm.formbean.DocMRolePrivilegeForm;

/**
 * The Class DocMRolePrivilegeAction.
 */
public class DocMRolePrivilegeAction extends com.mind.common.IlexAction {

	/**
	 * This method managing privilege for a role in User Manager.
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		/* Page Security Start */
		HttpSession session = request.getSession(true);

		DocMRolePrivilegeForm formdetail = (DocMRolePrivilegeForm) form;

		if (request.getParameter("org_type") != null) {
			formdetail.setOrg_type(request.getParameter("org_type"));
			String org_name = DocMRolePrivilegedao
					.getOrganizationTypeName(formdetail.getOrg_type());
			formdetail.setLo_organization_type_name(org_name);
		}

		if (request.getParameter("org_discipline_id") != null) {
			formdetail.setOrg_discipline_id(request
					.getParameter("org_discipline_id"));
			String org_discipline_name = DocMRolePrivilegedao
					.getOrganizationDisciplineName(formdetail
							.getOrg_discipline_id());
			formdetail.setLo_od_name(org_discipline_name);
		}

		DynamicComboUM dynamiccomboUM = new DynamicComboUM();

		dynamiccomboUM
				.setFunctiongroup(DocMRolePrivilegedao.getFunctionGroup());
		request.setAttribute("dynamiccomboUM", dynamiccomboUM);

		dynamiccomboUM.setRolename(DocMRolePrivilegedao.getRoleName(request
				.getParameter("org_discipline_id")));
		request.setAttribute("dynamiccomboUM", dynamiccomboUM);

		if (formdetail.getRefresh()) {
			if (formdetail.getCc_fu_group_name() != "0") {
				ArrayList UMList = DocMRolePrivilegedao
						.getFunctionType(formdetail.getCc_fu_group_name());
				request.setAttribute("UMList", UMList);
				int numcols = DocMRolePrivilegedao.getNumColumns(formdetail
						.getCc_fu_group_name());
				request.setAttribute("numcols", "" + numcols);
			}

			if (Integer.parseInt(formdetail.getLo_ro_role_desc()) != -1) {
				int roleprivilege[] = DocMRolePrivilegedao.getRolePrivilege(
						formdetail.getCc_fu_group_name(), formdetail
								.getLo_ro_role_desc());
				formdetail.setCheck(roleprivilege);
			}
		}

		if (formdetail.getSave() != null) {
			String indexvalue = "";

			if (formdetail.getCheck() != null) {
				if (formdetail.getCheck().length > 0) {
					int index[] = new int[formdetail.getCheck().length];
					index = formdetail.getCheck();

					for (int i = 0; i < formdetail.getCheck().length; i++) {
						indexvalue = indexvalue + index[i] + ",";
					}
				}
			}
			int retval = DocMRolePrivilegedao.addFunctionType(formdetail
					.getCc_fu_group_name(), formdetail.getLo_ro_role_desc(),
					indexvalue);
			request.setAttribute("retval", "" + retval);

			if (formdetail.getCc_fu_group_name() != "0") {
				ArrayList UMList = DocMRolePrivilegedao
						.getFunctionType(formdetail.getCc_fu_group_name());
				request.setAttribute("UMList", UMList);
				int numcols = DocMRolePrivilegedao.getNumColumns(formdetail
						.getCc_fu_group_name());
				request.setAttribute("numcols", "" + numcols);
			}

			if (Integer.parseInt(formdetail.getLo_ro_role_desc()) != -1) {
				int roleprivilege[] = DocMRolePrivilegedao.getRolePrivilege(
						formdetail.getCc_fu_group_name(), formdetail
								.getLo_ro_role_desc());
				formdetail.setCheck(roleprivilege);
			}
		}
		return (mapping.findForward("success"));
	}
	/* Page Security End */
}
