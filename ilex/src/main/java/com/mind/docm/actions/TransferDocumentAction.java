package com.mind.docm.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.SingletonDocumentTransfer;

/**
 * The Class TransferDocumentAction.
 */
public class TransferDocumentAction extends com.mind.common.IlexAction {

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		if (request.getParameter("synchronize") == null) {
			String type = (request.getParameter("Type") != null) ? request
					.getParameter("Type") : "";
			runThread(type, request);
		} else {
			if (SingletonDocumentTransfer.hasInstance())
				request.setAttribute("inProgress",
						"An existing 'document transfer' is already in progress.");
		}
		return mapping.findForward("success");
	}

	/**
	 * Start thread for contact documents.
	 * 
	 * @param type
	 *            the type
	 * @param request
	 *            the request
	 */
	public void startThreadForContactDocuments(String type,
			HttpServletRequest request) {

		SingletonDocumentTransfer singletonDocumentTransfer = SingletonDocumentTransfer
				.getInstance(type, getDataSource(request, "ilexnewDB"), request);
		if (singletonDocumentTransfer == null) {
			request.setAttribute("inProgress",
					"An existing 'document transfer' is already in progress.");
		} else {
			request.setAttribute("inProgress",
					"The 'document transfer' is now in progress.");
		}
	}

	/**
	 * Start thread for support documents.
	 * 
	 * @param request
	 *            the request
	 */
	public void startThreadForSupportDocuments(HttpServletRequest request) {
		SingletonDocumentTransfer singletonDocumentTransfer = SingletonDocumentTransfer
				.getInstance();
		if (singletonDocumentTransfer == null) {
			request.setAttribute("inProgress",
					"An existing 'document transfer' is already in progress.");
		} else {
			request.setAttribute("inProgress",
					"The 'document transfer' is now in progress.");
		}
	}

	/**
	 * Run thread.
	 * 
	 * @param type
	 *            the type
	 * @param request
	 *            the request
	 */
	public void runThread(String type, HttpServletRequest request) {
		if (type != null && !type.trim().equalsIgnoreCase("")
				&& type.trim().equalsIgnoreCase("support")) {
			startThreadForSupportDocuments(request);
		} else if (type != null && !type.trim().equalsIgnoreCase("")) {
			startThreadForContactDocuments(type, request);
		}
	}

}
