package com.mind.docm.actions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.docm.dao.EntryFieldSetUpDao;
import com.mind.docm.security.Authentication;
import com.mind.docm.utils.DocumentUtility;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class EntryFieldSetUpAction.
 */
public class EntryFieldSetUpAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(EntryFieldSetUpAction.class);

	/**
	 * Method :unspecified
	 * 
	 * @purpose This method is called first and call entryFieldSetUpList method
	 *          of same Action.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return this.entryFieldSetUpList(mapping, form, request, response);
	}

	/**
	 * Method :entryFieldSetUpList
	 * 
	 * @purpose This method is used to get the entry field setup list using
	 *          EntryFieldSetUpDao dao.
	 * @steps 1. Create an object of EntryFieldSetUpDao class. 2. Call the
	 *        entryFieldSetUpList method of EntryFieldSetUpDao. 3. Get an Object
	 *        of ArrayList. 4. Set this Object to request Attribute.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 */
	public ActionForward entryFieldSetUpList(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		EntryFieldSetUpDao entryFieldDao = new EntryFieldSetUpDao();
		String libId = null;
		logger.trace("Class : DocumentMasterAction Method : openFile ");
		logger.trace("This method is used to show the user the avilable pdf document");

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		libId = request.getParameter("libId");
		String libName = DocumentUtility.getLibraryName(libId);

		if (!Authentication.getPageSecurity(loginuserid, libName,
				"Modify Entry Field Setup")) {
			return (mapping.findForward("UnAuthenticate"));
		}

		libId = request.getParameter("libId");
		ArrayList entryField = entryFieldDao.entryFieldSetUpList(libId);
		request.setAttribute("entryField", entryField);
		request.setAttribute("libName", libName);
		ArrayList al = new ArrayList();
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet result = null;
		try {
			st = conn.createStatement();
			String query1 = "select * from dm_taxonomy_group";
			result = DatabaseUtils.getRecord(query1, st);
			while (result.next()) {
				al.add(result.getString("dm_tgrp_desc"));
			}
		} catch (Exception e) {
			logger.error(
					"entryFieldSetUpList(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"entryFieldSetUpList(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		} finally {
			DBUtil.close(result, st);
			DBUtil.close(conn);
		}
		request.setAttribute("txnGROUP", al);
		return (mapping.findForward("entryFieldList"));
	}
}