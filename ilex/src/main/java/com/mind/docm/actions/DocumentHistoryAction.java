package com.mind.docm.actions;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.docm.dao.DocumentHistoryDao;
import com.mind.docm.formbean.DocumentHistoryBean;
import com.mind.docm.security.Authentication;
import com.mind.docm.utils.DocumentUtility;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

/**
 * The Class DocumentHistoryAction.
 */
public class DocumentHistoryAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DocumentHistoryAction.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		DocumentHistoryBean bean = (DocumentHistoryBean) form;
		response.setHeader("Pragma", "No-Cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String documentId = null;
		String entityType = null;
		String entityId = null;
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		documentId = (request.getAttribute("docId") != null) ? request
				.getAttribute("docId").toString() : "";

		if (documentId == null || documentId.trim().equals("")) {
			documentId = request.getParameter("documentId");
		}

		String[] lib = DocumentHistoryDao.getLibraryIdAndName(documentId);
		logger.trace("Class : DocumentHistoryAction Method : unspecified ");
		logger.trace("This method is used to view the history of documents a a certain library");
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - libbbb  "
					+ lib.length + "::00 " + lib[0]);
		}
		if (lib[0] == null) {
			request.setAttribute("Comment",
					"No history available for the document in DocM System.");
			return (mapping.findForward("error"));
		} else if (!Authentication.getPageSecurity(loginuserid, lib[1],
				"View Document History")) {
			return (mapping.findForward("UnAuthenticate"));
		}
		String source = ((String) request.getAttribute("SourceType") == null ? request
				.getParameter("Source") : (String) request
				.getAttribute("SourceType"));
		String libName = DocumentUtility.getLibraryName(lib[0]);

		if (source.equals("Ilex")) {
			if (request.getParameter("entityType") == null
					|| request.getParameter("entityType").equals(""))
				entityType = (request.getAttribute("entityType") == null ? ""
						: (String) request.getAttribute("entityType"));
			else
				entityType = request.getParameter("entityType");

			if (request.getParameter("entityId") == null
					|| request.getParameter("entityId").equals(""))
				entityId = (request.getAttribute("entityId") == null ? ""
						: (String) request.getAttribute("entityId"));
			else
				entityId = request.getParameter("entityId");

			if (logger.isDebugEnabled()) {
				logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In Document History : Type"
						+ entityType);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In Document History : Id "
						+ entityId);
			}

			bean.setType(entityType);

			if (bean.getType().equalsIgnoreCase("MSA")) {
				bean.setMsaId(entityId);
			} else if (bean.getType().equalsIgnoreCase("Appendix")) {
				bean.setAppendixId(entityId);
				if (logger.isDebugEnabled()) {
					logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - entityId:: "
							+ entityId);
				}
				String MSA_ID = (request.getAttribute("MSA_ID") == null ? ""
						: (String) request.getAttribute("MSA_ID"));
				bean.setMsaId(MSA_ID);
			} else if (bean.getType().equalsIgnoreCase("Addendum")) {
				bean.setJobId(entityId);

				String appendixId = (request.getAttribute("appendixId") == null ? request
						.getParameter("appendixId") : (String) request
						.getAttribute("appendixId"));
				bean.setAppendixId(appendixId);
				// Added By Pankaj
				// DatabaseUtilityDao.setMenu(bean.getJobId(), request,
				// loginuserid, getDataSource( request , "ilexnewDB" ));
				// Added By Pankaj
			} else if (isLibrary(bean)) {
				String libraryName = (request.getAttribute("linkLibName") == null ? request
						.getParameter("linkLibName") : (String) request
						.getAttribute("linkLibName"));
				bean.setLinkLibNameFromIlex(libraryName.trim());
				bean.setType(bean.getLinkLibNameFromIlex());
				bean.setMsaId((bean.getType().equalsIgnoreCase("msa") ? entityId
						: ""));
				bean.setAppendixId((bean.getType().equalsIgnoreCase("Appendix") ? entityId
						: ""));
				bean.setJobId((bean.getType().equalsIgnoreCase("Addendum")
						|| bean.getType().equalsIgnoreCase("Job") ? entityId
						: ""));
				request.setAttribute("linkLibName", bean.getType());
				request.setAttribute("entityId", entityId);

				if (bean.getType().equalsIgnoreCase("Job")
						|| bean.getType().equalsIgnoreCase("Addendum")) {
					request.setAttribute("entityType", DatabaseUtilityDao
							.getKeyValue("ilex.docm.libraryname.deliverables"));
					// Added By Pankaj
					// DatabaseUtilityDao.setMenu(entityId, request,
					// loginuserid, getDataSource( request , "ilexnewDB" ));
					// Added By Pankaj
				} else
					request.setAttribute("entityType", entityType);

				if (logger.isDebugEnabled()) {
					logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - bean.getType() :: >>>><<<< "
							+ bean.getType());
				}
			}
			Map<String, Object> map;

			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
			showMenuViewSelector(bean, loginuserid, request);
		}

		request.setAttribute("SourceType", source);
		ArrayList documentHistoryList = DocumentHistoryDao
				.getDocumentHistory(documentId);

		request.setAttribute("libId", lib[0]);
		request.setAttribute("libName", libName);
		request.setAttribute("documentHistoryList", documentHistoryList);
		request.setAttribute("documentHistoryListSize",
				documentHistoryList.size() + "");
		request.setAttribute("msastatus",
				(String) request.getAttribute("msastatus"));

		return (mapping.findForward("documentHistoryList"));
	}

	/**
	 * Open file.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 */
	public ActionForward openFile(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		DocumentMasterAction docMasterAction = new DocumentMasterAction();
		return docMasterAction.openFile(mapping, form, request, response);
	}

	/**
	 * Show menu view selector.
	 * 
	 * @param docMForm
	 *            the doc m form
	 * @param loginuserid
	 *            the loginuserid
	 * @param request
	 *            the request
	 */
	public void showMenuViewSelector(DocumentHistoryBean docMForm,
			String loginuserid, HttpServletRequest request) {
		if (docMForm.getType().equalsIgnoreCase("MSA")) {
			docMForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			request.setAttribute("MSA_Id", docMForm.getMsaId());
			String msaStatus = MSAdao.getMSAStatus(docMForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String msaUploadCheck = MSAdao.getMSAUploadCheck(
					docMForm.getMsaId(), getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
					docMForm.getMsaId(), "", "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);
			request.setAttribute("status", msaStatus);
			request.setAttribute("uploadstatus", msaUploadCheck);
		}

		if (docMForm.getType().equals("Appendix")) {

			docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			String appendixStatus = Appendixdao.getAppendixStatus(
					docMForm.getAppendixId(), docMForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			docMForm.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			docMForm.setAppendixStatusChecking(appendixStatus);
			docMForm.setAppendixType(Appendixdao.getAppendixPrType(
					docMForm.getMsaId(), docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendixTypeId(Appendixdao.getAppendixPrTypeId(
					docMForm.getMsaId(), docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			request.setAttribute("appendixStatus", appendixStatus);

			if (docMForm.getAppendixStatusChecking().trim()
					.equalsIgnoreCase("Inwork")) {
				// System.out.println("In Inwork Appendix");

				String contractDocoumentList = com.mind.dao.PM.Appendixdao
						.getcontractDocoumentMenu(docMForm.getAppendixId(),
								getDataSource(request, "ilexnewDB"));
				request.setAttribute("contractDocMenu", contractDocoumentList);
				String appendixcurrentstatus = Appendixdao.getCurrentstatus(
						docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixcurrentstatus",
						appendixcurrentstatus);
				docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB")));
				request.setAttribute("appendixid", docMForm.getAppendixId());
				request.setAttribute("msaId", docMForm.getMsaId());
				request.setAttribute("nettype", "");

			} else {
				// System.out.println("::: Not an Inwork Appendix :::");

				String addendumlist = Appendixdao.getAddendumsIdName(
						docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				String addendumid = com.mind.dao.PM.Addendumdao
						.getLatestAddendumId(docMForm.getAppendixId(),
								getDataSource(request, "ilexnewDB"));
				String appendixType = Appendixdao.getAppendixPrType(
						docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				docMForm.setAppendixTypeId(Appendixdao.getAppendixPrTypeId(
						docMForm.getMsaId(), docMForm.getAppendixId(),
						getDataSource(request, "ilexnewDB")));
				String list = Menu.getStatus("pm_appendix",
						appendixStatus.charAt(0), docMForm.getMsaId(),
						docMForm.getAppendixId(), "", "", loginuserid,
						getDataSource(request, "ilexnewDB"));

				request.setAttribute("appendixStatusList", list);
				request.setAttribute("Appendix_Id", docMForm.getAppendixId());
				request.setAttribute("MSA_Id", docMForm.getMsaId());
				request.setAttribute("appendixStatus", appendixStatus);
				request.setAttribute("latestaddendumid", addendumid);
				request.setAttribute("addendumlist", addendumlist);
				request.setAttribute("appendixType", appendixType);
			}
		}

		if (docMForm.getType().equals("Addendum")) {
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					docMForm.getJobId(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			docMForm.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), docMForm.getJobId()));
			String jobViewtype = JobDashboarddao.getJobViewType(
					docMForm.getJobId(), getDataSource(request, "ilexnewDB"));
			docMForm.setAppendixId(IMdao.getAppendixId(docMForm.getJobId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			String list = Menu.getStatus("Addendum", jobStatus.charAt(0), "",
					docMForm.getAppendixId(), docMForm.getJobId(), "",
					loginuserid, getDataSource(request, "ilexnewDB"));
			request.setAttribute("addendumStatusList", list);
			String list_new = Menu.getStatus("Addendum_New", jobStatus.charAt(0), "",
					docMForm.getAppendixId(), docMForm.getJobId(), "",
					loginuserid, getDataSource(request, "ilexnewDB"));
			request.setAttribute("addendumStatusListNew", list_new);
			request.setAttribute("Appendix_Id", docMForm.getAppendixId());
			request.setAttribute("Job_Id", docMForm.getJobId());
			request.setAttribute("job_Status", jobStatus);
			request.setAttribute("jobViewType", jobViewtype);
		}

		if (docMForm.getType().equals("Job")) {
			// docMForm.setJobId(request.getAttribute("jobId").toString());
			String[] jobStatusAndType = Jobdao.getProjectJobStatusAndType(
					docMForm.getJobId(), getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			docMForm.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"), docMForm.getJobId()));
			docMForm.setAppendixId(IMdao.getAppendixId(docMForm.getJobId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setMsaId(IMdao.getMSAId(docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendixType(Appendixdao.getAppendixPrType(
					docMForm.getMsaId(), docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendixTypeId(Appendixdao.getAppendixPrTypeId(
					docMForm.getMsaId(), docMForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			docMForm.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					docMForm.getAppendixId()));
			docMForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), docMForm.getMsaId()));
			if (jobStatus != null) {
				String list = Menu.getStatus("prj_job", jobStatus.charAt(0),
						"", docMForm.getAppendixId(), docMForm.getJobId(), "",
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("prjJobStatusList", list);
				
				String list_new = Menu.getStatus("prj_jobMenuNew", jobStatus.charAt(0),
						"", docMForm.getAppendixId(), docMForm.getJobId(), "",
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("prjJobStatusListMenuNew", list_new);
			}
			request.setAttribute("Appendix_Id", docMForm.getAppendixId());
			request.setAttribute("Job_Id", docMForm.getJobId());
			request.setAttribute("appendixType", docMForm.getAppendixType());
		}
	}

	/**
	 * Checks if is library.
	 * 
	 * @param bean
	 *            the bean
	 * 
	 * @return true, if is library
	 */
	public boolean isLibrary(DocumentHistoryBean bean) {
		boolean library = false;
		library = (bean
				.getType()
				.equalsIgnoreCase(
						DatabaseUtilityDao
								.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments"))
				|| bean.getType()
						.equalsIgnoreCase(
								DatabaseUtilityDao
										.getKeyValue("ilex.docm.libraryname.msasupportingdocuments")) || bean
				.getType()
				.equalsIgnoreCase(
						DatabaseUtilityDao
								.getKeyValue("ilex.docm.libraryname.deliverables")));
		return library;
	}
}
