/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is an action class used for performing performing schedule update of appendix/job/activity in project manager.      
 *
 */
package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DispatchScheduleList;
import com.mind.bean.DynamicComboCNSWEB;
import com.mind.common.MultipartRequestWrapper;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.ViewList;
import com.mind.dao.PRM.AddInstallationNotesdao;
import com.mind.dao.PRM.DispatchScheduledao;
import com.mind.formbean.PRM.DispatchScheduleUpdateForm;
import com.mind.formbean.PRM.ScheduleUpdateForm;

public class DispatchScheduleUpdateAction extends
		com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DispatchScheduleUpdateAction.class);

	/**
	 * This method provides functionality to update schedule for
	 * apendix/job/activity.
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		DispatchScheduleUpdateForm dispatchScheduleUpdateForm = (DispatchScheduleUpdateForm) form;
		ArrayList tempList = new ArrayList();
		String scheduleId = "";
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (dispatchScheduleUpdateForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage Project",
					"Update Schedule", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		// set job filter type start
		if (request.getParameter("viewjobtype") != null) {
			dispatchScheduleUpdateForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			dispatchScheduleUpdateForm.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			dispatchScheduleUpdateForm.setOwnerId(""
					+ request.getParameter("ownerId"));
		} else {
			dispatchScheduleUpdateForm.setOwnerId("%");
		}

		if (request.getParameter("nettype") != null) {
			dispatchScheduleUpdateForm.setNettype(""
					+ request.getParameter("nettype"));
		}

		int flag = 0;
		int i = 0;
		int status = 0;
		String page = "";
		String pageid = "";
		String appendixname = null;
		String jobname = null;
		String[] pmschedulelist = new String[2];

		// Add for Installation Notes
		String typeidp = request.getParameter("typeid");
		String typep = request.getParameter("type");
		String ptmp = "";
		String pdtmp = "";
		ArrayList prevdispschedlist = DispatchScheduledao
				.getDispatchScheduleList(typeidp, typep,
						getDataSource(request, "ilexnewDB"));
		int length = DispatchScheduledao.getSize(
				request.getParameter("typeid"),
				getDataSource(request, "ilexnewDB"));
		for (int il = 0; il < length; il++) {
			if (((DispatchScheduleList) prevdispschedlist.get(il))
					.getActstartdate() == null) {
				ptmp = ptmp + "" + ",";
			} else {
				ptmp = ptmp
						+ ((DispatchScheduleList) prevdispschedlist.get(il))
								.getActstartdate()
						+ " "
						+ ((DispatchScheduleList) prevdispschedlist.get(il))
								.getActstartdatehh()
						+ ":"
						+ ((DispatchScheduleList) prevdispschedlist.get(il))
								.getActstartdatemm()
						+ " "
						+ ((DispatchScheduleList) prevdispschedlist.get(il))
								.getActstartdateop() + ",";
			}
			if (((DispatchScheduleList) prevdispschedlist.get(il))
					.getActenddate() == null) {
				pdtmp = pdtmp + "" + ",";
			} else {
				pdtmp = pdtmp
						+ ((DispatchScheduleList) prevdispschedlist.get(il))
								.getActenddate()
						+ " "
						+ ((DispatchScheduleList) prevdispschedlist.get(il))
								.getActenddatehh()
						+ ":"
						+ ((DispatchScheduleList) prevdispschedlist.get(il))
								.getActenddatemm()
						+ " "
						+ ((DispatchScheduleList) prevdispschedlist.get(il))
								.getActenddateop() + ",";
			}
		}
		dispatchScheduleUpdateForm.setPrevactarival(ptmp.split(","));
		dispatchScheduleUpdateForm.setPrevdeparture(pdtmp.split(","));

		if (dispatchScheduleUpdateForm.getSave() != null) {
			String typeid = dispatchScheduleUpdateForm.getTypeid();
			String type = dispatchScheduleUpdateForm.getType().trim();

			for (i = 0; i < dispatchScheduleUpdateForm.getStartdate().length; i++) {
				int z = i + 1;

				if (dispatchScheduleUpdateForm.getScheduleNumber(z).equals("")) {
					dispatchScheduleUpdateForm.setScheduleNumber("0", z);
				}

				if (!dispatchScheduleUpdateForm.getActstartdatehh()
						.equals("  ")
						&& dispatchScheduleUpdateForm.getActstartdatemm(i)
								.equals("  "))
					dispatchScheduleUpdateForm.setActstartdatemm("00", i);

				if (!dispatchScheduleUpdateForm.getActenddatehh().equals("  ")
						&& dispatchScheduleUpdateForm.getActenddatemm(i)
								.equals("  "))
					dispatchScheduleUpdateForm.setActenddatemm("00", i);

				if (!dispatchScheduleUpdateForm.getStartdatehh(i).equals("  ")
						&& dispatchScheduleUpdateForm.getStartdatemm(i).equals(
								"  "))
					dispatchScheduleUpdateForm.setStartdatemm("00", i);

				String actstart = dispatchScheduleUpdateForm.getActstartdate(i);
				if (!dispatchScheduleUpdateForm.getActstartdatehh(i).equals(
						"  "))
					actstart += " "
							+ dispatchScheduleUpdateForm.getActstartdatehh(i)
							+ ":"
							+ dispatchScheduleUpdateForm.getActstartdatemm(i)
							+ " "
							+ dispatchScheduleUpdateForm.getActstartdateop(i);
				dispatchScheduleUpdateForm.setActstartdate(actstart, i);

				String actend = dispatchScheduleUpdateForm.getActenddate(i);
				if (!dispatchScheduleUpdateForm.getActenddatehh(i).equals("  "))
					actend += " "
							+ dispatchScheduleUpdateForm.getActenddatehh(i)
							+ ":"
							+ dispatchScheduleUpdateForm.getActenddatemm(i)
							+ " "
							+ dispatchScheduleUpdateForm.getActenddateop(i);
				dispatchScheduleUpdateForm.setActenddate(actend, i);

				String startdate = dispatchScheduleUpdateForm.getStartdate(i);
				if (!dispatchScheduleUpdateForm.getStartdatehh(i).equals("  "))
					startdate += " "
							+ dispatchScheduleUpdateForm.getStartdatehh(i)
							+ ":"
							+ dispatchScheduleUpdateForm.getStartdatemm(i)
							+ " "
							+ dispatchScheduleUpdateForm.getStartdateop(i);

				if (actstart.equals(" : "))
					actstart = "";
				if (actend.equals(" : "))
					actend = "";
				if (startdate.equals(" : "))
					startdate = "";

				if (dispatchScheduleUpdateForm.getScheduleid() != null)
					;
				else
					dispatchScheduleUpdateForm.setScheduleid("0");

				int retValArray[] = new int[2];

				retValArray = DispatchScheduledao.addScheduleForPRM(
						dispatchScheduleUpdateForm.getScheduleNumber(z),
						typeid, actstart, actend, type, userid, startdate, i,
						getDataSource(request, "ilexnewDB"));
				status = retValArray[0];
				String newScheduleId = String.valueOf(retValArray[1]);

				// if(dispatchScheduleUpdateForm.getScheduleNumber(z).equals("0")){
				// start
				if (dispatchScheduleUpdateForm.getActstartdate(i) != null
						&& !dispatchScheduleUpdateForm.getActstartdate(i)
								.equals("")) {
					String defaultset = "Tech onsite for dispatch " + (z);
					AddInstallationNotesdao.addinstallnotes(typeid, defaultset,
							userid, getDataSource(request, "ilexnewDB"),
							dispatchScheduleUpdateForm.getActstartdate(i),
							dispatchScheduleUpdateForm.getScheduleNumber(z),
							newScheduleId, "S");
				}

				if (dispatchScheduleUpdateForm.getActenddate(i) != null
						&& !dispatchScheduleUpdateForm.getActenddate(i).equals(
								"")) {
					String defaultset = "Tech offsite for dispatch " + (z);
					AddInstallationNotesdao.addinstallnotes(typeid, defaultset,
							userid, getDataSource(request, "ilexnewDB"),
							dispatchScheduleUpdateForm.getActenddate(i),
							dispatchScheduleUpdateForm.getScheduleNumber(z),
							newScheduleId, "E");
				}
				// stop
				// }

			}

			page = dispatchScheduleUpdateForm.getPage();
			pageid = dispatchScheduleUpdateForm.getPageid();
			request.setAttribute("type", type);
			request.setAttribute("page", page);

			dispatchScheduleUpdateForm
					.setChecknetmedxtype(dispatchScheduleUpdateForm
							.getChecknetmedxtype());

			dispatchScheduleUpdateForm.setUpdatemessage("" + status);
			flag = 1;

		}

		else {
			String typeid = request.getParameter("typeid");
			String type = request.getParameter("type");
			page = request.getParameter("page");
			pageid = request.getParameter("pageid");

			/* Delete schedule */
			if (request.getParameter("delete") != null) {

				if (request.getParameter("scheduleId") != null)
					scheduleId = request.getParameter("scheduleId");

				if (request.getParameter("delete").equals("true")) {

					int liztSize = Integer.parseInt(request
							.getParameter("listSize"));

					for (i = liztSize; i < dispatchScheduleUpdateForm
							.getStartdate().length; i++) {
						if (!scheduleId.equals("0"))
							tempList.add(new DispatchScheduleList(
									"0",
									dispatchScheduleUpdateForm.getStartdate(i),
									dispatchScheduleUpdateForm
											.getStartdatehh(i),
									dispatchScheduleUpdateForm
											.getStartdatemm(i),
									dispatchScheduleUpdateForm
											.getStartdateop(i),
									dispatchScheduleUpdateForm
											.getActstartdate(i),
									dispatchScheduleUpdateForm
											.getActstartdatehh(i),
									dispatchScheduleUpdateForm
											.getActstartdatemm(i),
									dispatchScheduleUpdateForm
											.getActstartdateop(i),
									dispatchScheduleUpdateForm.getActenddate(i),
									dispatchScheduleUpdateForm
											.getActenddatehh(i),
									dispatchScheduleUpdateForm
											.getActenddatemm(i),
									dispatchScheduleUpdateForm
											.getActenddateop(i), "0"));
					}

					dispatchScheduleUpdateForm.setDeleteStatus(""
							+ DispatchScheduledao.deleteDispatchSchedule(
									typeid, type,
									request.getParameter("scheduleId"),
									getDataSource(request, "ilexnewDB")));
				}
			}

			ArrayList dispatchScheduleList = DispatchScheduledao
					.getDispatchScheduleList(typeid, type,
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("dispatchScheduleList", dispatchScheduleList);
			request.setAttribute("listSize", dispatchScheduleList.size() + "");
			request.setAttribute("tempList", tempList);
			request.setAttribute("tempListSize", tempList.size() + "");
			dispatchScheduleUpdateForm.setRownum(dispatchScheduleList.size()
					+ "");
			dispatchScheduleUpdateForm.setDeletenum(dispatchScheduleList.size()
					+ "");
			dispatchScheduleUpdateForm.setPage(request.getParameter("page"));
			dispatchScheduleUpdateForm
					.setPageid(request.getParameter("pageid"));
			// Add For Insatallation Notes.
			String tmp = "";
			String dtmp = "";
			for (int j = 0; j < dispatchScheduleList.size(); j++) {
				if (((DispatchScheduleList) dispatchScheduleList.get(j))
						.getActstartdate() == null
						|| ((DispatchScheduleList) dispatchScheduleList.get(j))
								.getActstartdate().equals("")) {
					tmp = tmp + "" + ",";
				} else {
					tmp = tmp
							+ ((DispatchScheduleList) dispatchScheduleList
									.get(j)).getActstartdate() + ",";
				}
				if (((DispatchScheduleList) dispatchScheduleList.get(j))
						.getActenddate() == null) {
					dtmp = dtmp + "" + ",";
				} else {
					dtmp = dtmp
							+ ((DispatchScheduleList) dispatchScheduleList
									.get(j)).getActenddate() + ",";
				}
			}
			dispatchScheduleUpdateForm.setActstartdate(tmp.split(","));
			dispatchScheduleUpdateForm.setActenddate(dtmp.split(","));

			if (type.equals("P")) {
				appendixname = DispatchScheduledao.getAppendixname(typeid,
						getDataSource(request, "ilexnewDB"));
				dispatchScheduleUpdateForm.setAppendixname(appendixname);

				dispatchScheduleUpdateForm.setChecknetmedxtype(ViewList
						.CheckAppendixType(typeid,
								getDataSource(request, "ilexnewDB")));

			} else {
				appendixname = DispatchScheduledao.getAppendixname(pageid,
						getDataSource(request, "ilexnewDB"));
				dispatchScheduleUpdateForm.setAppendixname(appendixname);

				dispatchScheduleUpdateForm.setChecknetmedxtype(ViewList
						.CheckAppendixType(pageid,
								getDataSource(request, "ilexnewDB")));

				jobname = DispatchScheduledao.getJobname(typeid,
						getDataSource(request, "ilexnewDB"));
				dispatchScheduleUpdateForm.setJobname(jobname);
			}

		}
		DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();

		dynamiccomboCNSWEB.setHourname(DynamicComboDao.getHourName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		dynamiccomboCNSWEB.setMinutename(DynamicComboDao.getMinuteName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		dynamiccomboCNSWEB.setOptionname(DynamicComboDao.getOptionName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);
		// For Installaation Notes
		String temp = "";

		if (flag == 1) {
			if (status == 0) {
				String temppath = "PRMJobScheduleInfo";
				request.setAttribute("type", temppath);
				request.setAttribute("appendix_Id",
						dispatchScheduleUpdateForm.getPageid());
				request.setAttribute("viewjobtype",
						dispatchScheduleUpdateForm.getViewjobtype());
				request.setAttribute("ownerId",
						dispatchScheduleUpdateForm.getOwnerId());

				if (dispatchScheduleUpdateForm.getPage().equals("jobdashboard")) {
					request.setAttribute("jobid",
							dispatchScheduleUpdateForm.getTypeid());
					request.setAttribute("tabId",
							(request.getParameter("tabId") == null ? ""
									: request.getParameter("tabId")));
					if (logger.isDebugEnabled()) {
						logger.debug("update(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - "
								+ request.getParameter("tabId")
								+ " >>>> tabid >>> jobid"
								+ dispatchScheduleUpdateForm.getTypeid());
					}
					String url = "JobDashboardAction.do?hmode=unspecified&tabId="
							+ request.getParameter("tabId")
							+ "&jobid="
							+ dispatchScheduleUpdateForm.getTypeid();
					try {
						RequestDispatcher dispatcher = request
								.getRequestDispatcher(url);
						dispatcher.forward(request, response);
					} catch (Exception e) {
						logger.error(
								"update(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
								e);

						if (logger.isDebugEnabled()) {
							logger.debug("update(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - inside exception");
						}
					}
				} else {
					return (mapping.findForward("temppage"));
				}
				// forward = mapping.findForward("success");
			} else {
				forward = mapping.findForward("failure");
			}

		} else {
			forward = mapping.findForward("failure");
		}
		request.setAttribute("scheduleform", dispatchScheduleUpdateForm);
		if (request.getParameter("throughJobDashboard") != null) {
			try {
				RequestDispatcher ds = request
						.getRequestDispatcher("/PRM/DispatchScheduleUpdate.jsp");
				if (request
						.getClass()
						.getName()
						.equals("org.apache.struts.upload.MultipartRequestWrapper")) {
					logger.debug("Casting MultiparRequestWrapper to custom class");
					request = new MultipartRequestWrapper(request);
				}
				ds.include(request, response);
			} catch (Exception e) {
				logger.warn(
						"update(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored",
						e);

			}
		} else
			return forward;
		return null;
	}

	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ScheduleUpdateForm scheduleUpdateForm = (ScheduleUpdateForm) form;

		// set job filter type start
		if (request.getParameter("viewjobtype") != null) {
			scheduleUpdateForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		}

		if (request.getParameter("ownerId") != null) {
			scheduleUpdateForm.setOwnerId("" + request.getParameter("ownerId"));
		}

		if (request.getParameter("nettype") != null) {
			scheduleUpdateForm.setNettype("" + request.getParameter("nettype"));
		}

		String schedulepresent = "false";
		String[] list = new String[4];
		String typeid = request.getParameter("typeid");
		String type = request.getParameter("type");
		// //list=DispatchScheduledao.getSchedule( typeid, type
		// ,getDataSource(request,"ilexnewDB"));
		if (list != null) {
			scheduleUpdateForm.setScheduleid(list[0]);
			scheduleUpdateForm.setType(list[1]);
			scheduleUpdateForm.setTypeid(list[2]);

			if (!list[6].equals("")) {
				scheduleUpdateForm.setActstartdate(list[6].substring(0, 10));
				if (list[6].substring(11, 12).equals(" "))
					scheduleUpdateForm.setActstartdatehh("0"
							+ list[6].substring(12, 13));
				else
					scheduleUpdateForm.setActstartdatehh(list[6].substring(11,
							13));
				scheduleUpdateForm.setActstartdatemm(list[6].substring(14, 16));
				scheduleUpdateForm.setActstartdateop(list[6].substring(23, 25));
			}

			if (!list[7].equals("")) {
				scheduleUpdateForm.setActenddate(list[7].substring(0, 10));
				if (list[7].substring(11, 12).equals(" "))
					scheduleUpdateForm.setActenddatehh("0"
							+ list[7].substring(12, 13));
				else
					scheduleUpdateForm.setActenddatehh(list[7]
							.substring(11, 13));
				scheduleUpdateForm.setActenddatemm(list[7].substring(14, 16));
				scheduleUpdateForm.setActenddateop(list[7].substring(23, 25));
			}

			scheduleUpdateForm.setTotaldays(list[5]);

			if (!list[3].equals("")) {
				scheduleUpdateForm.setStartdate(list[3].substring(0, 10));
				if (list[3].substring(11, 12).equals(" "))
					scheduleUpdateForm.setStartdatehh("0"
							+ list[3].substring(12, 13));
				else
					scheduleUpdateForm
							.setStartdatehh(list[3].substring(11, 13));
				scheduleUpdateForm.setStartdatemm(list[3].substring(14, 16));
				scheduleUpdateForm.setStartdateop(list[3].substring(23, 25));
			}

			if (!list[4].equals("")) {
				scheduleUpdateForm.setPlannedenddate(list[4].substring(0, 10));
				if (list[4].substring(11, 12).equals(" "))
					scheduleUpdateForm.setPlannedenddatehh("0"
							+ list[4].substring(12, 13));
				else
					scheduleUpdateForm.setPlannedenddatehh(list[4].substring(
							11, 13));
				scheduleUpdateForm.setPlannedenddatemm(list[4]
						.substring(14, 16));
				scheduleUpdateForm.setPlannedenddateop(list[4]
						.substring(23, 25));
			}

			/*
			 * if(list[3].charAt(11)==' ')
			 * list[3]=list[3].substring(0,11)+"0"+list[3].substring(12);
			 * if(list[4].charAt(11)==' ')
			 * list[4]=list[4].substring(0,11)+"0"+list[4].substring(12);
			 * if(list[6].charAt(11)==' ')
			 * list[6]=list[6].substring(0,11)+"0"+list[6].substring(12);
			 * if(list[7].charAt(11)==' ')
			 * list[7]=list[7].substring(0,11)+"0"+list[7].substring(12);
			 * 
			 * scheduleUpdateForm.setActstartdate(list[6].substring(0, 10));
			 * scheduleUpdateForm.setActstartdatehh(list[6].substring(11,13));
			 * scheduleUpdateForm.setActstartdatemm(list[6].substring(14,16));
			 * scheduleUpdateForm.setActstartdateop(list[6].substring(23,25));
			 * 
			 * scheduleUpdateForm.setActenddate(list[7].substring(0, 10));
			 * scheduleUpdateForm.setActenddatehh(list[7].substring(11,13));
			 * scheduleUpdateForm.setActenddatemm(list[7].substring(14,16));
			 * scheduleUpdateForm.setActenddateop(list[7].substring(23,25));
			 * 
			 * scheduleUpdateForm.setTotaldays(list[5]);
			 * 
			 * scheduleUpdateForm.setStartdate(list[3].substring(0, 10));
			 * scheduleUpdateForm.setStartdatehh(list[3].substring(11,13));
			 * scheduleUpdateForm.setStartdatemm(list[3].substring(14,16));
			 * scheduleUpdateForm.setStartdateop(list[3].substring(23,25));
			 * 
			 * scheduleUpdateForm.setPlannedenddate(list[4].substring(0, 10));
			 * scheduleUpdateForm.setPlannedenddatehh(list[4].substring(11,13));
			 * scheduleUpdateForm.setPlannedenddatemm(list[4].substring(14,16));
			 * scheduleUpdateForm.setPlannedenddateop(list[4].substring(23,25));
			 */
			schedulepresent = "true";

		}

		request.setAttribute("schedulepresent", schedulepresent);
		request.setAttribute("scheduleform", scheduleUpdateForm);
		forward = mapping.findForward("success");
		return (forward);
	}

}
