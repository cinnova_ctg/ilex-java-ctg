package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AppendixCustomerInformationdao;
import com.mind.formbean.PRM.AppendixCustomerInformationForm;
import com.mind.util.WebUtil;

public class AppendixCustomerInformationAction extends
		com.mind.common.IlexDispatchAction {

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AppendixCustomerInformationForm appendixCustomerInformationForm = (AppendixCustomerInformationForm) form;

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (appendixCustomerInformationForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage Appendix",
					"Custom Fields", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		int flag = 0;
		int statusvalue = -1;
		String typeid = "";
		String type = "";

		String addmessage = "";
		String appendixname = "";
		String appendixid = "";

		String[][] custinfolist = new String[10][2];

		if (request.getParameter("viewjobtype") != null) {
			appendixCustomerInformationForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			appendixCustomerInformationForm.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			appendixCustomerInformationForm.setOwnerId(""
					+ request.getParameter("ownerId"));
		} else {
			appendixCustomerInformationForm.setOwnerId("%");
		}

		/* Start : Appendix Dashboard View Selector Code */

		if (appendixCustomerInformationForm.getJobOwnerOtherCheck() != null
				&& !appendixCustomerInformationForm.getJobOwnerOtherCheck()
						.equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					appendixCustomerInformationForm.getJobOwnerOtherCheck());
		}

		if (request.getParameter("fromPage") != null) {
			appendixCustomerInformationForm.setFromPage(request
					.getParameter("fromPage"));
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId",
					appendixCustomerInformationForm.getAppendixid());
			request.setAttribute("msaId",
					appendixCustomerInformationForm.getMsaId());

			return mapping.findForward("temppage");

		}

		if (appendixCustomerInformationForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < appendixCustomerInformationForm
					.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus
						+ ","
						+ "'"
						+ appendixCustomerInformationForm
								.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus
						+ ","
						+ appendixCustomerInformationForm
								.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (appendixCustomerInformationForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < appendixCustomerInformationForm
					.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner
						+ ","
						+ appendixCustomerInformationForm
								.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner
						+ ","
						+ appendixCustomerInformationForm
								.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					appendixCustomerInformationForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					appendixCustomerInformationForm.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId",
					appendixCustomerInformationForm.getAppendixid());
			request.setAttribute("msaId",
					appendixCustomerInformationForm.getMsaId());

			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				appendixCustomerInformationForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				appendixCustomerInformationForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				appendixCustomerInformationForm.setJobOwnerOtherCheck(session
						.getAttribute("jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				appendixCustomerInformationForm.setJobMonthWeekCheck(session
						.getAttribute("timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(userid,
				appendixCustomerInformationForm.getJobOwnerOtherCheck(),
				"prj_job_new", appendixCustomerInformationForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		if (appendixCustomerInformationForm.getSave() != null) {
			appendixid = appendixCustomerInformationForm.getAppendixid();
			appendixname = AppendixCustomerInformationdao.getAppendixname(
					appendixid, getDataSource(request, "ilexnewDB"));
			appendixCustomerInformationForm.setAppendixname(appendixname);
			appendixCustomerInformationForm.setMsaId(IMdao.getMSAId(appendixid,
					getDataSource(request, "ilexnewDB")));
			String parameterstring = "";
			String parameterstringId = "";

			if (appendixCustomerInformationForm.getCustParameter1() != null) {
				parameterstring = parameterstring
						+ appendixCustomerInformationForm.getCustParameter1();
				parameterstringId = parameterstringId
						+ appendixCustomerInformationForm.getCustParameterId1();
			}

			if (appendixCustomerInformationForm.getCustParameter2() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter2();
				parameterstringId = parameterstringId + "~"
						+ appendixCustomerInformationForm.getCustParameterId2();
			}

			if (appendixCustomerInformationForm.getCustParameter3() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter3();
				parameterstringId = parameterstringId + "~"
						+ appendixCustomerInformationForm.getCustParameterId3();
			}
			// start

			if (appendixCustomerInformationForm.getCustParameter4() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter4();
				parameterstringId = parameterstringId + "~"
						+ appendixCustomerInformationForm.getCustParameterId4();
			}
			if (appendixCustomerInformationForm.getCustParameter5() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter5();
				parameterstringId = parameterstringId + "~"
						+ appendixCustomerInformationForm.getCustParameterId5();
			}
			if (appendixCustomerInformationForm.getCustParameter6() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter6();
				parameterstringId = parameterstringId + "~"
						+ appendixCustomerInformationForm.getCustParameterId6();
			}
			if (appendixCustomerInformationForm.getCustParameter7() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter7();
				parameterstringId = parameterstringId + "~"
						+ appendixCustomerInformationForm.getCustParameterId7();
			}
			if (appendixCustomerInformationForm.getCustParameter8() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter8();
				parameterstringId = parameterstringId + "~"
						+ appendixCustomerInformationForm.getCustParameterId8();
			}
			if (appendixCustomerInformationForm.getCustParameter9() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter9();
				parameterstringId = parameterstringId + "~"
						+ appendixCustomerInformationForm.getCustParameterId9();
			}
			if (appendixCustomerInformationForm.getCustParameter10() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter10();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId10();
			}

			if (appendixCustomerInformationForm.getCustParameter11() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter11();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId11();
			}
			if (appendixCustomerInformationForm.getCustParameter12() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter12();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId12();
			}
			if (appendixCustomerInformationForm.getCustParameter13() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter13();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId13();
			}
			if (appendixCustomerInformationForm.getCustParameter14() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter14();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId14();
			}
			if (appendixCustomerInformationForm.getCustParameter15() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter15();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId15();
			}
			// new
			if (appendixCustomerInformationForm.getCustParameter16() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter16();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId16();
			}
			if (appendixCustomerInformationForm.getCustParameter17() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter17();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId17();
			}
			if (appendixCustomerInformationForm.getCustParameter18() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter18();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId18();
			}
			if (appendixCustomerInformationForm.getCustParameter19() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter19();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId19();
			}
			if (appendixCustomerInformationForm.getCustParameter20() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter20();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId20();
			}
			if (appendixCustomerInformationForm.getCustParameter21() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter21();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId21();
			}
			if (appendixCustomerInformationForm.getCustParameter22() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter22();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId22();
			}
			if (appendixCustomerInformationForm.getCustParameter23() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter23();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId23();
			}
			if (appendixCustomerInformationForm.getCustParameter24() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter24();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId24();
			}
			if (appendixCustomerInformationForm.getCustParameter25() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter25();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId25();
			}
			if (appendixCustomerInformationForm.getCustParameter26() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter26();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId26();
			}
			if (appendixCustomerInformationForm.getCustParameter27() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter27();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId27();
			}
			if (appendixCustomerInformationForm.getCustParameter28() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter28();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId28();
			}
			if (appendixCustomerInformationForm.getCustParameter29() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter29();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId29();
			}
			if (appendixCustomerInformationForm.getCustParameter30() != null) {
				parameterstring = parameterstring + "~"
						+ appendixCustomerInformationForm.getCustParameter30();
				parameterstringId = parameterstringId
						+ "~"
						+ appendixCustomerInformationForm
								.getCustParameterId30();
			}
			// over
			statusvalue = AppendixCustomerInformationdao.addAppendixCustInfo(
					appendixid, parameterstringId, parameterstring, userid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("addmessage", statusvalue + "");

			if (statusvalue == 0) {
				flag = 1;
				String temppath = "PRMJobCustomerAppendixInfo";
				request.setAttribute("type", temppath);
				request.setAttribute("appendix_Id",
						appendixCustomerInformationForm.getAppendixid());
				request.setAttribute("viewjobtype",
						appendixCustomerInformationForm.getViewjobtype());
				request.setAttribute("ownerId",
						appendixCustomerInformationForm.getOwnerId());
				return (mapping.findForward("temppage"));
			} else {
				forward = mapping.findForward("success");
			}

		} else {
			appendixid = request.getParameter("appendixid");
			appendixCustomerInformationForm.setMsaId(IMdao.getMSAId(appendixid,
					getDataSource(request, "ilexnewDB")));
			appendixCustomerInformationForm.setAppendixid(appendixid);
			appendixname = AppendixCustomerInformationdao.getAppendixname(
					appendixid, getDataSource(request, "ilexnewDB"));
			appendixCustomerInformationForm.setAppendixname(appendixname);
			custinfolist = AppendixCustomerInformationdao.getAppendixcustinfo(
					appendixid, getDataSource(request, "ilexnewDB"));
			appendixCustomerInformationForm
					.setCustParameterId1(custinfolist[0][0]);
			appendixCustomerInformationForm
					.setCustParameter1(custinfolist[0][1]);
			appendixCustomerInformationForm
					.setCustParameterId2(custinfolist[1][0]);
			appendixCustomerInformationForm
					.setCustParameter2(custinfolist[1][1]);
			appendixCustomerInformationForm
					.setCustParameterId3(custinfolist[2][0]);
			appendixCustomerInformationForm
					.setCustParameter3(custinfolist[2][1]);
			// change
			appendixCustomerInformationForm
					.setCustParameterId4(custinfolist[3][0]);
			appendixCustomerInformationForm
					.setCustParameter4(custinfolist[3][1]);
			appendixCustomerInformationForm
					.setCustParameterId5(custinfolist[4][0]);
			appendixCustomerInformationForm
					.setCustParameter5(custinfolist[4][1]);
			appendixCustomerInformationForm
					.setCustParameterId6(custinfolist[5][0]);
			appendixCustomerInformationForm
					.setCustParameter6(custinfolist[5][1]);
			appendixCustomerInformationForm
					.setCustParameterId7(custinfolist[6][0]);
			appendixCustomerInformationForm
					.setCustParameter7(custinfolist[6][1]);
			appendixCustomerInformationForm
					.setCustParameterId8(custinfolist[7][0]);
			appendixCustomerInformationForm
					.setCustParameter8(custinfolist[7][1]);
			appendixCustomerInformationForm
					.setCustParameterId9(custinfolist[8][0]);
			appendixCustomerInformationForm
					.setCustParameter9(custinfolist[8][1]);
			appendixCustomerInformationForm
					.setCustParameterId10(custinfolist[9][0]);
			appendixCustomerInformationForm
					.setCustParameter10(custinfolist[9][1]);

			appendixCustomerInformationForm
					.setCustParameterId11(custinfolist[10][0]);
			appendixCustomerInformationForm
					.setCustParameter11(custinfolist[10][1]);
			appendixCustomerInformationForm
					.setCustParameterId12(custinfolist[11][0]);
			appendixCustomerInformationForm
					.setCustParameter12(custinfolist[11][1]);
			appendixCustomerInformationForm
					.setCustParameterId13(custinfolist[12][0]);
			appendixCustomerInformationForm
					.setCustParameter13(custinfolist[12][1]);
			appendixCustomerInformationForm
					.setCustParameterId14(custinfolist[13][0]);
			appendixCustomerInformationForm
					.setCustParameter14(custinfolist[13][1]);
			appendixCustomerInformationForm
					.setCustParameterId15(custinfolist[14][0]);
			appendixCustomerInformationForm
					.setCustParameter15(custinfolist[14][1]);

			// new

			appendixCustomerInformationForm
					.setCustParameterId16(custinfolist[15][0]);
			appendixCustomerInformationForm
					.setCustParameter16(custinfolist[15][1]);
			appendixCustomerInformationForm
					.setCustParameterId17(custinfolist[16][0]);
			appendixCustomerInformationForm
					.setCustParameter17(custinfolist[16][1]);
			appendixCustomerInformationForm
					.setCustParameterId18(custinfolist[17][0]);
			appendixCustomerInformationForm
					.setCustParameter18(custinfolist[17][1]);
			appendixCustomerInformationForm
					.setCustParameterId19(custinfolist[18][0]);
			appendixCustomerInformationForm
					.setCustParameter19(custinfolist[18][1]);
			appendixCustomerInformationForm
					.setCustParameterId20(custinfolist[19][0]);
			appendixCustomerInformationForm
					.setCustParameter20(custinfolist[19][1]);
			appendixCustomerInformationForm
					.setCustParameterId21(custinfolist[20][0]);
			appendixCustomerInformationForm
					.setCustParameter21(custinfolist[20][1]);
			appendixCustomerInformationForm
					.setCustParameterId22(custinfolist[21][0]);
			appendixCustomerInformationForm
					.setCustParameter22(custinfolist[21][1]);
			appendixCustomerInformationForm
					.setCustParameterId23(custinfolist[22][0]);
			appendixCustomerInformationForm
					.setCustParameter23(custinfolist[22][1]);
			appendixCustomerInformationForm
					.setCustParameterId24(custinfolist[23][0]);
			appendixCustomerInformationForm
					.setCustParameter24(custinfolist[23][1]);
			appendixCustomerInformationForm
					.setCustParameterId25(custinfolist[24][0]);
			appendixCustomerInformationForm
					.setCustParameter25(custinfolist[24][1]);
			appendixCustomerInformationForm
					.setCustParameterId26(custinfolist[25][0]);
			appendixCustomerInformationForm
					.setCustParameter26(custinfolist[25][1]);
			appendixCustomerInformationForm
					.setCustParameterId27(custinfolist[26][0]);
			appendixCustomerInformationForm
					.setCustParameter27(custinfolist[26][1]);
			appendixCustomerInformationForm
					.setCustParameterId28(custinfolist[27][0]);
			appendixCustomerInformationForm
					.setCustParameter28(custinfolist[27][1]);
			appendixCustomerInformationForm
					.setCustParameterId29(custinfolist[28][0]);
			appendixCustomerInformationForm
					.setCustParameter29(custinfolist[28][1]);
			appendixCustomerInformationForm
					.setCustParameterId30(custinfolist[29][0]);
			appendixCustomerInformationForm
					.setCustParameter30(custinfolist[29][1]);

			// over

			String contractDocoumentList = com.mind.dao.PM.Appendixdao
					.getcontractDocoumentMenu(appendixid,
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("contractDocMenu", contractDocoumentList);
			String appendixcurrentstatus = Appendixdao.getCurrentstatus(
					appendixid, getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
			request.setAttribute("msaId",
					appendixCustomerInformationForm.getMsaId());
			request.setAttribute("viewjobtype",
					appendixCustomerInformationForm.getViewjobtype());
			request.setAttribute("ownerId",
					appendixCustomerInformationForm.getOwnerId());
			request.setAttribute("appendixid", appendixid);

			forward = mapping.findForward("success");

		}

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(appendixid,
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(appendixid,
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("msaId",
				appendixCustomerInformationForm.getMsaId());
		request.setAttribute("viewjobtype",
				appendixCustomerInformationForm.getViewjobtype());
		request.setAttribute("ownerId",
				appendixCustomerInformationForm.getOwnerId());
		request.setAttribute("appendixid", appendixid);

		request.setAttribute("viewflag", flag + "");

		return (forward);
	}

}
