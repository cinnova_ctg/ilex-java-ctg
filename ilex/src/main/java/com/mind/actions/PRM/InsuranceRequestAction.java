package com.mind.actions.PRM;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.prm.InsuranceRequestBean;
import com.mind.business.PRM.InsuranceRequestConverter;
import com.mind.business.PRM.InsuranceRequestOrganizer;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.StateList;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PRM.InsuranceRequestForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

/**
 * The Class InsuranceRequestAction.
 */
public class InsuranceRequestAction extends com.mind.common.IlexDispatchAction {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Pvsdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		Map<String, Object> map;
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Email Certificate of Insurance Request",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */
		InsuranceRequestForm insuranceForm = (InsuranceRequestForm) form;
		setGeneralFormInfo(insuranceForm, request, session);
		Organizer(insuranceForm, getDataSource(request, "ilexnewDB"));
		map = DatabaseUtilityDao.setMenu(insuranceForm.getJobId(), loginuserid,
				getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		return mapping.findForward("success");
	}

	/**
	 * Send request.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 */
	public ActionForward sendRequest(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginUserId = (String) session.getAttribute("userid");
		Map<String, Object> map;
		if (loginUserId == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		/* Page Security=: End */
		InsuranceRequestForm insuranceForm = (InsuranceRequestForm) form;
		setGeneralFormInfo(insuranceForm, request, session);
		int sentCheck = emailRequest(insuranceForm, request,
				getDataSource(request, "ilexnewDB"));
		if (sentCheck > -1) {
			request.setAttribute("msg", "Email Sent Successfully.");
			ActionForward fwd = new ActionForward();
			fwd.setPath("/JobDashboardAction.do?hmode=unspecified&tabId=&jobid="
					+ insuranceForm.getJobId()
					+ "&appendix_Id="
					+ insuranceForm.getAppendixId() + "&isClicked=");
			return fwd;
		} else {
			request.setAttribute("msg", "Email Sent Failed.");
		}
		map = DatabaseUtilityDao.setMenu(insuranceForm.getJobId(), loginUserId,
				getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		request.setAttribute("sentCheck", sentCheck);
		return mapping.findForward("success");
	}

	/**
	 * Email request.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * @param request
	 *            the request
	 * @param ds
	 *            the ds
	 */
	private int emailRequest(InsuranceRequestForm insuranceForm,
			HttpServletRequest request, DataSource ds) {
		InsuranceRequestBean bean = new InsuranceRequestBean();
		wirteFile(insuranceForm, request, bean);
		ArrayList stateList = StateList.getStateList(ds);
		insuranceForm.setHolderStateList(stateList);
		insuranceForm.setStateList(stateList);
		InsuranceRequestConverter.setInsuranceRequestFromForm(insuranceForm,
				bean);
		int sentCheck = InsuranceRequestOrganizer.sendEmail(bean, request, ds);
		if (sentCheck > -1) {
			InsuranceRequestOrganizer.saveSendAction(bean, ds);
		}
		logger.debug("com.mind.actions.PRM.InsuranceRequestAction.emailRequest() sentCheck:->:"
				+ sentCheck);
		return sentCheck;
	}

	/**
	 * @param insuranceForm
	 * @param request
	 * @param bean
	 */
	private void wirteFile(InsuranceRequestForm insuranceForm,
			HttpServletRequest request, InsuranceRequestBean bean) {
		String path = Util.getTemp() + "/"
				+ insuranceForm.getAdditionalInfo().getFileName();
		if (insuranceForm.getAdditionalInfo().getFileSize() > 0) {
			try {
				Util.writeFile(insuranceForm.getAdditionalInfo().getFileData(),
						path);
				bean.setFileName1(insuranceForm.getAdditionalInfo()
						.getFileName());
				File f = new File(path);
				bean.setFile1(f);
				f.deleteOnExit();
				logger.debug("com.mind.actions.PRM.InsuranceRequestAction.emailRequest() file name-> "
						+ bean.getFileName1());
				logger.debug("com.mind.actions.PRM.InsuranceRequestAction.emailRequest() file path-> "
						+ bean.getFile1().getPath());
			} catch (Exception e) {
				logger.debug("Error on com.mind.actions.PRM.InsuranceRequestAction.wirteFile()::"
						+ e);
			}
		}
	}

	/**
	 * Organizer.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * @param ds
	 *            the ds
	 */
	private void Organizer(InsuranceRequestForm insuranceForm, DataSource ds) {
		InsuranceRequestBean bean = new InsuranceRequestBean();
		InsuranceRequestConverter.setInsuranceRequestFromForm(insuranceForm,
				bean);
		InsuranceRequestOrganizer.organizer(bean, ds);
		InsuranceRequestConverter.setFormFromInsuranceRequest(insuranceForm,
				bean);
	}

	/**
	 * Sets the general form info.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * @param request
	 *            the request
	 */
	private void setGeneralFormInfo(InsuranceRequestForm insuranceForm,
			HttpServletRequest request, HttpSession session) {
		insuranceForm.setLoginUserEmail((String) session
				.getAttribute("useremail"));
		insuranceForm.setLoginUserId((String) session.getAttribute("userid"));
		insuranceForm.setAttention(Util.getKeyValue("attention.name"));
		if (request.getParameter("jobId") != null) {
			insuranceForm.setJobId(request.getParameter("jobId"));
		}
		if (request.getParameter("appendixId") != null) {
			insuranceForm.setAppendixId(request.getParameter("appendixId"));
		}
		if (request.getParameter("msaId") != null) {
			insuranceForm.setMsaId(request.getParameter("msaId"));
		}
		logger.debug("om.mind.actions.PRM.InsuranceRequestAction.setGeneralFormInfo() jobId->"
				+ insuranceForm.getJobId());
		logger.debug("om.mind.actions.PRM.InsuranceRequestAction.setGeneralFormInfo() appendixId->"
				+ insuranceForm.getAppendixId());
		logger.debug("om.mind.actions.PRM.InsuranceRequestAction.setGeneralFormInfo() msaId->"
				+ insuranceForm.getMsaId());
	}

}
