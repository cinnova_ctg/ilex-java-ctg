package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.NewWebTicketdao;
import com.mind.formbean.PRM.NewWebTicketForm;

public class NewWebTicketAction extends com.mind.common.IlexAction {
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	
	{
		
		NewWebTicketForm newWebForm = (NewWebTicketForm) form;
	
			
		/* Page Security Start*/
	    HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if( !Authenticationdao.getPageSecurity( userid , "Manage Project" , "View Web Tickets" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		/* Page Security End*/

		String ticketNumber = "0";
		int deleteTicket = 0;
		if(request.getParameter("ticketNumber") != null) {
			ticketNumber = request.getParameter("ticketNumber");
		}
		
		if(request.getParameter("function") != null && request.getParameter("function").equals("delete")) {
			deleteTicket = NewWebTicketdao.deleteWebTicket(ticketNumber,getDataSource(request, "ilexnewDB")); 
			request.setAttribute("deleteTicket",deleteTicket+"");
		}
			
		ArrayList list = new ArrayList();
		int size=0;

		list = NewWebTicketdao.getWebTicketList(getDataSource(request , "ilexnewDB"));
		request.setAttribute("webticketlist",list);
		size=list.size();
		request.setAttribute("size",size+"");
			
		return (mapping.findForward("success"));
	
	}	
	
}
