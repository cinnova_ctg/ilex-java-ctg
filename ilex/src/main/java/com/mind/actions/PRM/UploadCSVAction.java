package com.mind.actions.PRM;

//import java.util.ArrayList;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.RestClient;
import com.mind.dao.PRM.UploadError;
import com.mind.dao.PRM.UploadSitedao;
import com.mind.formbean.PRM.UploadCSVForm;

/**
 * 
 * @author amitm
 * 
 */

public class UploadCSVAction extends com.mind.common.IlexAction {

	/**
	 * @author amitm
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		UploadCSVForm uploadCSVform = (UploadCSVForm) form;
		String sessionid = request.getRequestedSessionId();
		String fromProposalMg = "No";

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userId = (String) session.getAttribute("userid");

		StringBuffer siredetailfailure = null;
		siredetailfailure = new StringBuffer();
		String siteRow = null;
		String lastRow = "false";
		int retval = -1;
		int i = 0;
		String separator = "~";
		int countTokens = 0;// To count tokens after parsing CSV String on the
							// basis of new line character
		DataSource ds = null;
		ds = getDataSource(request, "ilexnewDB");

		if (request.getParameter("msaid") != null) {
			uploadCSVform.setMsaid((String) request.getParameter("msaid"));
			request.setAttribute("msaid", uploadCSVform.getMsaid());
		}

		if (request.getParameter("fromProposalMg") != null) {
			fromProposalMg = (String) request.getParameter("fromProposalMg");
			request.setAttribute("fromProposalMg", "Yes");
		}

		uploadCSVform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(ds,
				uploadCSVform.getMsaid()));

		/*
		 * if (request.getParameter("appendixid") != null &&
		 * !request.getParameter("appendixid").equals("")) {
		 * uploadCSVform.setAppendixid(request.getParameter("appendixid"));
		 * uploadCSVform.setMsaid(IMdao.getMSAId(uploadCSVform.getAppendixid(),
		 * getDataSource(request, "ilexnewDB")));
		 * uploadCSVform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(ds,
		 * IMdao.getMSAId(uploadCSVform.getAppendixid(), ds))); }
		 */

		if (request.getParameter("pageType") != null)
			uploadCSVform.setPageType(request.getParameter("pageType"));

		if (request.getParameter("siteSearchName") != null)
			uploadCSVform.setSiteSearchName(request
					.getParameter("siteSearchName"));

		if (request.getParameter("siteSearchNumber") != null)
			uploadCSVform.setSiteSearchNumber(request
					.getParameter("siteSearchNumber"));

		if (request.getParameter("siteSearchCity") != null)
			uploadCSVform.setSiteSearchCity(request
					.getParameter("siteSearchCity"));

		if (request.getParameter("siteSearchState") != null)
			uploadCSVform.setSiteSearchState(request
					.getParameter("siteSearchState"));

		if (request.getParameter("siteSearchZip") != null)
			uploadCSVform.setSiteSearchZip(request
					.getParameter("siteSearchZip"));

		if (request.getParameter("siteSearchCountry") != null)
			uploadCSVform.setSiteSearchCountry(request
					.getParameter("siteSearchCountry"));

		if (request.getParameter("siteSearchGroup") != null)
			uploadCSVform.setSiteSearchGroup(request
					.getParameter("siteSearchGroup"));

		if (request.getParameter("siteSearchEndCustomer") != null)
			uploadCSVform.setSiteSearchEndCustomer(request
					.getParameter("siteSearchEndCustomer"));

		if (request.getParameter("siteSearchStatus") != null)
			uploadCSVform.setSiteSearchStatus(request
					.getParameter("siteSearchStatus"));

		if (request.getParameter("lines_per_page") != null)
			uploadCSVform.setLines_per_page(request
					.getParameter("lines_per_page"));

		if (request.getParameter("current_page_no") != null)
			uploadCSVform.setCurrent_page_no(request
					.getParameter("current_page_no"));

		if (request.getParameter("total_page_size") != null)
			uploadCSVform.setTotal_page_size(request
					.getParameter("total_page_size"));

		if (request.getParameter("org_page_no") != null)
			uploadCSVform.setOrg_page_no(request.getParameter("org_page_no"));

		if (request.getParameter("org_lines_per_page") != null)
			uploadCSVform.setOrg_lines_per_page(request
					.getParameter("org_lines_per_page"));

		if (request.getParameter("siteNameForColumn") != null)
			uploadCSVform.setSiteNameForColumn(request
					.getParameter("siteNameForColumn"));

		if (request.getParameter("siteNumberForColumn") != null)
			uploadCSVform.setSiteNumberForColumn(request
					.getParameter("siteNumberForColumn"));

		if (request.getParameter("siteAddressForColumn") != null)
			uploadCSVform.setSiteAddressForColumn(request
					.getParameter("siteAddressForColumn"));

		if (request.getParameter("siteCityForColumn") != null)
			uploadCSVform.setSiteCityForColumn(request
					.getParameter("siteCityForColumn"));

		if (request.getParameter("siteStateForColumn") != null)
			uploadCSVform.setSiteStateForColumn(request
					.getParameter("siteStateForColumn"));

		if (request.getParameter("siteZipForColumn") != null)
			uploadCSVform.setSiteZipForColumn(request
					.getParameter("siteZipForColumn"));

		if (request.getParameter("siteCountryForColumn") != null)
			uploadCSVform.setSiteCountryForColumn(request
					.getParameter("siteCountryForColumn"));

		if (request.getParameter("fromType") != null)
			uploadCSVform.setFromType(request.getParameter("fromType"));

		request.setAttribute("msaid", uploadCSVform.getMsaid());
		// request.setAttribute("appendix_Id", uploadCSVform.getAppendixid());

		if (request.getParameter("viewjobtype") != null) {
			uploadCSVform.setViewjobtype(request.getParameter("viewjobtype"));
		} else {
			uploadCSVform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			uploadCSVform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			uploadCSVform.setOwnerId("%");
		}

		// Added For getting Field Name, format etc from table
		// lp_site_table_definition
		// uploadCSVform.setAppendixname(Jobdao.getAppendixname(ds,
		// uploadCSVform.getAppendixid()));
		// uploadCSVform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(ds,IMdao.getMSAId(uploadCSVform.getAppendixid(),ds)));

		DynamicComboCM MSAEndCustomerList = new DynamicComboCM();
		MSAEndCustomerList.setFirstlevelcatglist(UploadSitedao
				.getMSAEndCustomerList(ds, uploadCSVform.getMsaid()));
		request.setAttribute("MSAEndCustomerList", MSAEndCustomerList);

		/* get the combo lists */
		DynamicComboCM dcTimeZoneList = new DynamicComboCM();
		dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao.gettimezone(ds));
		request.setAttribute("dcTimeZoneList", dcTimeZoneList);

		Collection tableData = new ArrayList();
		tableData = UploadSitedao.getSite_table_definition(ds);
		request.setAttribute("tableData", tableData);
		request.setAttribute("BalnkCSV", "none");// Added if uploaded CSV File
													// is Blank

		if (uploadCSVform.getUpload() != null) {
			byte[] buffer1 = new byte[uploadCSVform.getSitefilename()
					.getFileSize()];
			String sitedata = new String(uploadCSVform.getSitefilename()
					.getFileData());
			String[] dataType = UploadSitedao
					.getuploadfiledatatype(getDataSource(request, "ilexnewDB"));
			String[] dataLength = UploadSitedao
					.getuploadfiledatalength(getDataSource(request, "ilexnewDB"));
			String errors = "";

			ArrayList error = new ArrayList();
			UploadError uploadError = null;

			/*
			 * if (uploadCSVform.getAppend() != null) { statusFlag =
			 * uploadCSVform.getAppend(); } else { statusFlag = "delete"; }
			 */

			StringTokenizer st = new StringTokenizer(sitedata, "\n");
			countTokens = st.countTokens();

			while (st.hasMoreTokens()) {
				siteRow = st.nextToken();
				if (siteRow.contains(",,,,,,,,,,,,,,,,,,,")) {
					continue;
				}
				if (countTokens > 1) {
					if (i == 0) {
						siteRow = st.nextToken();
					}
				} else {
					request.setAttribute("BalnkCSV", "balnkcsv");
					return mapping.findForward("success");
				}
				uploadError = new UploadError();

				if (siteRow.contains("\t"))
					siteRow = siteRow.replace("\t", separator);
				else
					separator = ",";

				uploadError = UploadSitedao.validaterow(siteRow, uploadError,
						i + 1, dataType, dataLength, separator,
						getDataSource(request, "ilexnewDB"));

				if (uploadError.getRowno() != null) {
				} else {

					siteRow = siteRow.replace("'", "|");

					// System.out.println("countTokens::::::::"+countTokens);
					// System.out.println("i::::::::"+i);

					if (countTokens > 2) {
						if (countTokens - 2 == i) // check for last row
							lastRow = "true";
					} else
						lastRow = "true";

					// call Google to get latitude/longitude: Start
					String[] siteDataArray = new String[20];
					String[] siteLatLong = new String[5];
					siteDataArray = siteRow.split(separator);

					// Undefined change which could not be traced to SPR.

					// String stateFullName =
					// Pvsdao.getStateName(siteDataArray[8],ds);
					// siteLatLong =
					// RestClient.getLatitudeLongitude(siteDataArray[6],siteDataArray[7],stateFullName,siteDataArray[10],siteDataArray[9],
					// getDataSource(request, "ilexnewDB"));
					siteLatLong = RestClient.getLatitudeLongitude(
							siteDataArray[6], siteDataArray[7],
							siteDataArray[8], siteDataArray[10],
							siteDataArray[9],
							getDataSource(request, "ilexnewDB"));
					// call Google to get latitude/longitude: End

					retval = UploadSitedao.updateSiteDetail(
							uploadCSVform.getMsaid(), siteRow, userId,
							uploadCSVform.getEndCustomer(), lastRow,
							siteLatLong[3], siteLatLong[2], siteLatLong[0],
							siteLatLong[1],
							getDataSource(request, "ilexnewDB"), separator);
					if (retval == -9001) {
						uploadError.setRowno(i + 1 + "");
						uploadError.setRowerror("Row insert fail.");

						siredetailfailure = siredetailfailure.append(siteRow
								+ "\n");
					}
				}

				if (uploadError.getRowno() != null) {

				} else {
					uploadError.setRowno(i + 1 + "");
					uploadError.setRowerror("success");
				}

				error.add(uploadError);

				i++;
			}

			for (int k = 0; k < error.size(); k++) {
				errors = errors + ((UploadError) error.get(k)).getRowerror()
						+ "\n";
			}

			retval = UploadSitedao.uploadsitehistory(userId, uploadCSVform
					.getSitefilename().getFileName(), errors,
					getDataSource(request, "ilexnewDB"));

			// request.setAttribute("appendixid",
			// uploadCSVform.getAppendixid());
			request.setAttribute("errorlist", error);
			return mapping.findForward("errorpage");
		}

		/*
		 * else { uploadCSVform.setAppend("inactive"); }
		 */

		if (request.getParameter("view_templete") != null) {
			String csvfilepath = request.getRealPath("PRM") + "/Template.txt";

			String filedata = null;
			File file = new File(csvfilepath);

			// FileReader filereader = new FileReader(file);

			FileInputStream fileinputstream = new FileInputStream(file);

			byte[] buffer = new byte[fileinputstream.available()];

			if (fileinputstream.read(buffer) != -1)
				;

			response.setContentType("application/x-msdownload");
			response.setHeader("Content-Disposition",
					"attachment;filename=Template.txt;size=" + buffer.length
							+ "");
			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(buffer);
			outStream.close();
		}

		return mapping.findForward("success");
	}
}
