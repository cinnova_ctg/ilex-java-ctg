package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.DocumentIdNotUpdatedException;
import com.mind.docm.exceptions.EntityDetailsNotFoundException;
import com.mind.docm.exceptions.NotAbleToInsertDocument;
import com.mind.util.WebUtil;
import com.mind.xml.POPdf;
import com.mind.xml.WOPdf;

public class SendPoWoToDocMAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SendPoWoToDocMAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String realPath = WebUtil.getRealPath(request, "/images");
		String fontPath = realPath + "/ARIALN.TTF";
		String fileName = "PoFinal";
		String imageBulletPath = realPath + "/Bullet.gif";
		String imagepath = realPath + "/clogo.jpg";
		String imagepath1 = realPath + "/checkbox.gif";
		String type = "";
		String Id = "";
		String jobId = "";
		byte[] buffer = null;
		String WONwConfigImg = realPath + "/witechfooter.jpg";
		String WONwConfigImgheader = realPath + "/witechheader.jpg";
		String icChrysler = realPath + "/chrysler.jpg";
		String icSnapOn = realPath + "/snap.jpg";
		String icCompuCom = realPath + "/compu.jpg";

		HttpSession session = request.getSession(true);

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		String userid = (String) session.getAttribute("userid");
		if (!Authenticationdao.getPageSecurity(userid, "Manage Partner",
				"Manage PO/WO", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		type = request.getParameter("type") != null ? request
				.getParameter("type") : "";
		Id = request.getParameter("powoid") != null ? request
				.getParameter("powoid") : "";
		jobId = request.getParameter("jobid") != null ? request
				.getParameter("jobid") : "";

		if (type.equals("po")) {
			POPdf po = new POPdf();
			buffer = po.viewPO(Id, fileName, imagepath, fontPath, userid,
					getDataSource(request, "ilexnewDB"), imageBulletPath);
			type = "PO";
			// EntityManager.approveEntity(Id,"PO",buffer);
		} else if (type.equals("wo")) {
			WOPdf wo = new WOPdf();
			buffer = wo.viewwo(Id, imagepath, imagepath1, fontPath, userid,
					getDataSource(request, "ilexnewDB"), WONwConfigImg,
					WONwConfigImgheader, icSnapOn, icChrysler, icCompuCom);
			type = "WO";
			// EntityManager.approveEntity(Id,"WO",buffer);
		}
		try {
			String docId = EntityManager.approveEntity(Id, type, buffer);
		} catch (NotAbleToInsertDocument e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("documentInsertionInDocM", "failed");
		} catch (EntityDetailsNotFoundException e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("EntityDetailsNotFoundException", "failed");
		} catch (ControlledTypeException e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("ControlledTypeException", "failed");
		} catch (DocMFormatException e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("DocMException", "failed");
		} catch (CouldNotAddToRepositoryException e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("CouldNotAddToRepositoryException", "failed");
		} catch (CouldNotReplaceException e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("CouldNotReplaceException", "failed");
		} catch (CouldNotCheckDocumentException e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("CouldNotCheckDocumentException", "failed");
		} catch (DocumentIdNotUpdatedException e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("DocumentIdNotUpdatedException", "failed");
		} catch (Exception e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("Exception", "failed");
		}
		request.setAttribute("jobid", jobId);
		return mapping.findForward("success");
	}

}
