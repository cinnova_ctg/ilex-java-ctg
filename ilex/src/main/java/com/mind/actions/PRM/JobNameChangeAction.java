package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.formbean.PRM.JobNameChangeForm;
import com.mind.util.WebUtil;

public class JobNameChangeAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		JobNameChangeForm jobnamechange = (JobNameChangeForm) form;

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String userid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "Job", "Job Edit",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("appendix_Id") != null) {
			jobnamechange.setAppendixId(request.getParameter("appendix_Id")
					.toString());
			jobnamechange.setMsaId(IMdao.getMSAId(
					jobnamechange.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			jobnamechange.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					jobnamechange.getAppendixId()));
		}

		if (request.getParameter("appendixid") != null) {
			jobnamechange.setAppendixId(request.getParameter("appendixid"));
		}

		if (request.getParameter("Job_Id") != null) {
			jobnamechange.setJobId(request.getParameter("Job_Id").toString());
		}
		if (request.getParameter("viewjobtype") != null) {
			jobnamechange.setViewjobtype(request.getParameter("viewjobtype"));
		}
		if (request.getParameter("ownerId") != null) {
			jobnamechange.setOwnerId(request.getParameter("ownerId"));
		}

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(jobnamechange.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				jobnamechange.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("viewjobtype", jobnamechange.getViewjobtype());
		request.setAttribute("ownerId", jobnamechange.getOwnerId());
		request.setAttribute("appendixid", jobnamechange.getAppendixId());

		if (jobnamechange.getAdd() != null) {
			int changeJobNameFlag = -1;
			changeJobNameFlag = Jobdao.updateJobName(jobnamechange.getJobId(),
					jobnamechange.getJobName().trim(), userid,
					jobnamechange.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("Appendix_Id", jobnamechange.getAppendixId());
			request.setAttribute("msaId", jobnamechange.getMsaId());
			request.setAttribute("changeJobNameFlag", changeJobNameFlag + "");
			if (changeJobNameFlag < 0) {
				return (mapping.findForward("success"));
			} else
				return (mapping.findForward("appendixdashboard"));
		} else
			jobnamechange.setJobName(Jobdao.getJobname(
					jobnamechange.getJobId(),
					getDataSource(request, "ilexnewDB")));

		/* Start : Appendix Dashboard View Selector Code */

		if (jobnamechange.getJobOwnerOtherCheck() != null
				&& !jobnamechange.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					jobnamechange.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", jobnamechange.getAppendixId());
			request.setAttribute("msaId", jobnamechange.getMsaId());
			request.setAttribute("viewjobtype", jobnamechange.getViewjobtype());
			request.setAttribute("ownerId", jobnamechange.getOwnerId());

			return mapping.findForward("temppage");

		}

		if (jobnamechange.getJobSelectedStatus() != null) {
			for (int i = 0; i < jobnamechange.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ jobnamechange.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ jobnamechange.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (jobnamechange.getJobSelectedOwners() != null) {
			for (int j = 0; j < jobnamechange.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ jobnamechange.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ jobnamechange.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					jobnamechange.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					jobnamechange.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", jobnamechange.getAppendixId());
			request.setAttribute("msaId", jobnamechange.getMsaId());
			request.setAttribute("viewjobtype", jobnamechange.getViewjobtype());
			request.setAttribute("ownerId", jobnamechange.getOwnerId());

			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				jobnamechange.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				jobnamechange.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				jobnamechange.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				jobnamechange.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				jobnamechange.getJobOwnerOtherCheck(), "prj_job_new",
				jobnamechange.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		return mapping.findForward("success");
	}
}
