package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboPRM;
import com.mind.common.actions.CheckActivityAction;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PRM.ReportScheduleWindowdao;
import com.mind.dao.PRM.Scheduledao;
import com.mind.formbean.PRM.ReportScheduleWindowForm;

public class ReportScheduleWindowAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ReportScheduleWindowForm reportScheduleWindowForm = (ReportScheduleWindowForm) form;
		ActionForward forward = new ActionForward();
		DynamicComboPRM dcprm = new DynamicComboPRM();

		ArrayList criticalitylist = new ArrayList();// for criticality combo
		ArrayList statuslist = new ArrayList();// for status combo
		ArrayList requestorlist = new ArrayList();// for requestor combo
		ArrayList critdesclist = new ArrayList();// for option arrival combo
		ArrayList yesnolist = new ArrayList();// for MSP/HelpDesk combo
		ArrayList msalist = new ArrayList();// for MSA combo
		ArrayList appendixlist = new ArrayList();// for Appendix combo
		String appendixname = null;
		String jobname = null;
		String lo_ot_id = "0";
		String appendixid = "0";
		String msaid = "0";
		String temp = "";
		String tempstatus = "0";
		String temprequestor = "0";
		String tempcritdesc = "0";
		String tempmsp = "0";
		String temphdesk = "0";
		// String versionid="0";

		if (request.getParameter("typeid") != null) {
			reportScheduleWindowForm.setTypeid(request.getParameter("typeid"));
		} else
			reportScheduleWindowForm.setTypeid("0");

		if (request.getParameter("fromPage") != null) {
			reportScheduleWindowForm.setFromPage(request
					.getParameter("fromPage"));
		}

		if (request.getParameter("report_type") != null)
			reportScheduleWindowForm.setReport_type(request
					.getParameter("report_type"));

		/* Start: Modified By Vishal 05/01/07 */
		reportScheduleWindowForm.setAppendixname(Scheduledao.getAppendixname(
				reportScheduleWindowForm.getTypeid(),
				getDataSource(request, "ilexnewDB")));
		reportScheduleWindowForm.setMsaname(Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"), IMdao.getMSAId(
						reportScheduleWindowForm.getTypeid(),
						getDataSource(request, "ilexnewDB"))));

		if (reportScheduleWindowForm.getReport_type().equals("CSVSummary")
				|| reportScheduleWindowForm.getReport_type().equals(
						"appendixCsvSummary")) {
			reportScheduleWindowForm.setAppendixname(Scheduledao
					.getAppendixname(reportScheduleWindowForm.getTypeid(),
							getDataSource(request, "ilexnewDB")));
			if (!reportScheduleWindowForm.getFromPage().equals("dispatch")) {
				reportScheduleWindowForm.setAppendix(reportScheduleWindowForm
						.getTypeid());
				reportScheduleWindowForm.setMsa(IMdao.getMSAId(
						reportScheduleWindowForm.getTypeid(),
						getDataSource(request, "ilexnewDB")));
				reportScheduleWindowForm.setMsaname(Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						reportScheduleWindowForm.getMsa()));
				reportScheduleWindowForm.setAppendixname(Scheduledao
						.getAppendixname(reportScheduleWindowForm.getTypeid(),
								getDataSource(request, "ilexnewDB")));
			}
		}
		/* End: */

		/* criticality combo start */
		criticalitylist = Scheduledao.getCriticalitylist(getDataSource(request,
				"ilexnewDB"));
		dcprm.setCriticalitylist(criticalitylist);
		request.setAttribute("criticalitycombo", dcprm);
		/* criticality combo end */

		/* status combo start */
		statuslist = ReportScheduleWindowdao.getStatuslist(getDataSource(
				request, "ilexnewDB"));
		dcprm.setJobstatuslist(statuslist);
		request.setAttribute("statuscombo", dcprm);
		/* status combo end */

		/* requestor combo start */
		lo_ot_id = IMdao.getOrganizationTopId(msaid,
				getDataSource(request, "ilexnewDB"));
		dcprm.setRequestorlist(ReportScheduleWindowdao.getOrganizationPOCList(
				lo_ot_id, getDataSource(request, "ilexnewDB")));
		request.setAttribute("requestorcombo", dcprm);
		/* requestor combo end */

		/* criticality desc combo start */
		critdesclist = ReportScheduleWindowdao
				.getCriticalityDescList(getDataSource(request, "ilexnewDB"));
		dcprm.setCritdesclist(critdesclist);
		request.setAttribute("critdesccombo", dcprm);
		/* criticality desc combo end */

		/* MSP/Help Desk combo start */
		yesnolist = ReportScheduleWindowdao.getYesNoList(getDataSource(request,
				"ilexnewDB"));
		dcprm.setYesnolist(yesnolist);
		request.setAttribute("mspcombo", dcprm);
		request.setAttribute("helpdeskcombo", dcprm);
		/* MSP/Help Desk combo end */

		/* MSA combo start */
		msalist = ReportScheduleWindowdao.getMsalist(getDataSource(request,
				"ilexnewDB"));
		dcprm.setMsalist(msalist);
		request.setAttribute("msacombo", dcprm);
		/* MSA combo end */

		/* Appendix Desk combo start */
		appendixlist = ReportScheduleWindowdao.getAppendixlist(
				reportScheduleWindowForm.getMsa(),
				getDataSource(request, "ilexnewDB"));
		dcprm.setAppendixlist(appendixlist);
		request.setAttribute("appendixcombo", dcprm);

		/* Appendix combo end */
		if (reportScheduleWindowForm.getReport_type().equals("CSVSummary")
				|| reportScheduleWindowForm.getReport_type().equals(
						"appendixCsvSummary")) {
			if (reportScheduleWindowForm.getMsp() != null)
				tempmsp = reportScheduleWindowForm.getMsp();

			if (reportScheduleWindowForm.getSubmitreport() != null) {
				request.getSession(false).setAttribute("datasource",
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("MSAid", reportScheduleWindowForm.getMsa());
				request.setAttribute("appendixid",
						reportScheduleWindowForm.getAppendix());
				request.setAttribute("version",
						reportScheduleWindowForm.getVersion());
				request.setAttribute("startdate",
						reportScheduleWindowForm.getStartdate());
				request.setAttribute("enddate",
						reportScheduleWindowForm.getEnddate());
				if (reportScheduleWindowForm.getDateOnSite() != null)
					request.setAttribute("dateOnSite",
							reportScheduleWindowForm.getDateOnSite());
				else
					request.setAttribute("dateOnSite", "");
				if (reportScheduleWindowForm.getDateOffSite() != null)
					request.setAttribute("dateOffSite",
							reportScheduleWindowForm.getDateOffSite());
				else
					request.setAttribute("dateOffSite", "");
				if (reportScheduleWindowForm.getDateComplete() != null)
					request.setAttribute("dateComplete",
							reportScheduleWindowForm.getDateComplete());
				else
					request.setAttribute("dateComplete", "");
				if (reportScheduleWindowForm.getDateClosed() != null)
					request.setAttribute("dateClosed",
							reportScheduleWindowForm.getDateClosed());
				else
					request.setAttribute("dateClosed", "");
				request.setAttribute("msp", tempmsp);

				int activityCount = 0;
				activityCount = CheckActivityAction
						.checkActivitieAppendix(request);
				if (activityCount > 100) {
					request.setAttribute("ActivityError", "ActivityError");
					return (mapping.findForward("success"));
				} else {
					return (mapping.findForward("csvsummary"));
				}
			} else {
				return (mapping.findForward("success"));
			}
		}

		if (reportScheduleWindowForm.getSave() != null) {
			request.getSession(false).setAttribute("datasource",
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("msaname", Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), IMdao.getMSAId(
							reportScheduleWindowForm.getTypeid(),
							getDataSource(request, "ilexnewDB"))));
			request.setAttribute("appendixid",
					reportScheduleWindowForm.getTypeid());
			request.setAttribute("startdate",
					reportScheduleWindowForm.getStartdate());
			request.setAttribute("enddate",
					reportScheduleWindowForm.getEnddate());
			request.setAttribute("report_type",
					reportScheduleWindowForm.getReport_type());

			if (reportScheduleWindowForm.getCriticality() != null) {
				for (int i = 0; i < reportScheduleWindowForm.getCriticality().length; i++) {
					temp = temp + reportScheduleWindowForm.getCriticality(i)
							+ ",";
				}
				temp = temp.substring(0, temp.length() - 1);
			} else {
				temp = "0";
			}

			if (reportScheduleWindowForm.getStatus() != null) {
				tempstatus = reportScheduleWindowForm.getStatus();
			}

			if (reportScheduleWindowForm.getRequestor() != null) {
				temprequestor = reportScheduleWindowForm.getRequestor();
			}

			if (reportScheduleWindowForm.getCritdesc() != null) {
				tempcritdesc = reportScheduleWindowForm.getCritdesc();
			}

			if (reportScheduleWindowForm.getMsp() != null) {
				tempmsp = reportScheduleWindowForm.getMsp();
			}

			if (reportScheduleWindowForm.getHelpdesk() != null) {
				temphdesk = reportScheduleWindowForm.getHelpdesk();
			}

			// request.setAttribute("criticality",reportScheduleWindowForm.getCriticality());
			request.setAttribute("criticality", temp);
			request.setAttribute("status", tempstatus);
			request.setAttribute("requestor", temprequestor);
			request.setAttribute("criticalitydesc", tempcritdesc);
			request.setAttribute("msp", tempmsp);
			request.setAttribute("helpdesk", temphdesk);
			forward = mapping.findForward("report");
		} else {
			forward = mapping.findForward("success");
		}

		return (forward);
	}
}
