package com.mind.actions.PRM;

import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.Activity;
import com.mind.bean.prm.ActivityTabularForm;
import com.mind.bean.prm.JobDashboardBean;
import com.mind.common.EnvironmentSelector;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.common.util.MySqlConnection;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PMT.DeleteJobdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.dao.PRM.NetMedXDashboarddao;
import com.mind.formbean.PRM.JobDashboardForm;

public class JobDashboardAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(JobDashboardAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// Date datenewformat = new Date();
		// SimpleDateFormat formatter = new SimpleDateFormat("H:mm:ss:SSS");
		// String datenewformat = formatter.format(today);
		JobDashboardForm jobdashboardform = (JobDashboardForm) form;
		JobDashboardBean jobdashboardBean = new JobDashboardBean();
		int job_size = 0;
		String activityid = "";
		String sortqueryclause = "";
		String documentpresent = "false";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Project",
				"View Job Dashboard", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("sitelistid") != null
				&& request.getParameter("sitelistid") != "") {
			int siteId = 0;
			int jobid = 0;
			int jobSiteId = 0;
			if (request.getParameter("jobid") != null)
				jobid = Integer.parseInt(request.getParameter("jobid"));

			if (request.getParameter("siteId") != null)
				jobSiteId = Integer.parseInt(request.getParameter("siteId"));

			siteId = Integer.parseInt(request.getParameter("sitelistid"));
			int retval = JobDashboarddao.addNewSite(siteId, jobid, loginuserid,
					getDataSource(request, "ilexnewDB"), jobSiteId);
		}

		if (request.getAttribute("jobid") != null) {
			jobdashboardform.setJobid(request.getAttribute("jobid").toString());
		}

		// for invoice manager back button
		if (request.getParameter("inv_id") != null)
			jobdashboardform.setInv_id(request.getParameter("inv_id"));
		if (request.getParameter("inv_type") != null)
			jobdashboardform.setInv_type(request.getParameter("inv_type"));
		if (request.getParameter("inv_rdofilter") != null)
			jobdashboardform.setInv_rdofilter(request
					.getParameter("inv_rdofilter"));
		if (request.getParameter("invoice_Flag") != null)
			jobdashboardform.setInvoice_Flag(request
					.getParameter("invoice_Flag"));
		if (request.getParameter("invoiceno") != null)
			jobdashboardform.setInvoiceno(request.getParameter("invoiceno"));
		if (request.getParameter("partner_name") != null)
			jobdashboardform.setPartner_name(request
					.getParameter("partner_name"));
		if (request.getParameter("powo_number") != null)
			jobdashboardform
					.setPowo_number(request.getParameter("powo_number"));
		if (request.getParameter("from_date") != null)
			jobdashboardform.setFrom_date(request.getParameter("from_date"));
		if (request.getParameter("to_date") != null)
			jobdashboardform.setTo_date(request.getParameter("to_date"));

		// for dispatch view of netmedx manager back button
		if (request.getParameter("nettype") != null) {
			jobdashboardform.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getParameter("ownerId") != null) {
			jobdashboardform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			jobdashboardform.setOwnerId("%");
		}
		if (request.getParameter("viewjobtype") != null) {
			jobdashboardform.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			jobdashboardform.setViewjobtype("A");
		}

		if (request.getParameter("jobid") != null) {
			jobdashboardform.setJobid(request.getParameter("jobid"));
		}

		if (request.getAttribute("jobid") != null) {
			jobdashboardform.setJobid((String) request.getAttribute("jobid"));
		}

		if (request.getParameter("changeTestIndicator") != null) {
			int jobIndicatorFlag = -1;
			if (request.getParameter("jobIndicator") != null
					&& !request.getParameter("jobIndicator").equals("")) {
				jobIndicatorFlag = JobDashboarddao.changeJobTestIndicator(
						jobdashboardform.getJobid(),
						request.getParameter("jobIndicator"), loginuserid,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("jobIndicatorFlag", jobIndicatorFlag + "");

				if (request.getParameter("jobIndicator")
						.equals("Internal Test")) {
					Connection mySqlConn = null;
					try {
						String url = EnvironmentSelector
								.getBundleString("appendix.detail.changestatus.mysql.url");
						mySqlConn = MySqlConnection.getMySqlConnection(url);
						DeleteJobdao.mySqlDeleteJob(mySqlConn,
								jobdashboardform.getJobid());
					} catch (Exception e) {
						logger.error(
								"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
								e);

						if (logger.isDebugEnabled()) {
							logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Exception caught in creating MySql Connection: "
									+ e);
						}
						logger.error(
								"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
								e);
					} finally {
						if (mySqlConn != null) {
							mySqlConn.close();
						}
					}
				}
			}
		}

		jobdashboardform.setAppendix_id(IMdao.getAppendixId(
				jobdashboardform.getJobid(),
				getDataSource(request, "ilexnewDB")));
		jobdashboardform.setMsa_id(IMdao.getMSAId(
				jobdashboardform.getAppendix_id(),
				getDataSource(request, "ilexnewDB")));
		jobdashboardform.setAppendix_name(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				jobdashboardform.getAppendix_id()));

		if (request.getAttribute("querystring") != null) {
			sortqueryclause = (String) request.getAttribute("querystring");
		}

		if (session != null) {
			if (session.getAttribute("HoldingSortJobDB") != null) {
				sortqueryclause = (String) session
						.getAttribute("HoldingSortJobDB");
			}
		}
		// End

		if (jobdashboardform.getOptactivity() != null) {
			Activity activity = new Activity();

			activity.setJob_Id(jobdashboardform.getJobid());
			activity.setName("oos"); // Out of Scope Activity
			activity.setActivity_Id("0");

			activity.setActivitytypecombo("O");
			activity.setQuantity("1.0");
			activity.setEstimated_materialcost("0.00");
			activity.setEstimated_cnsfieldlaborcost("0.00");
			activity.setEstimated_contractfieldlaborcost("0.00");
			activity.setEstimated_freightcost("0.00");
			activity.setEstimated_travelcost("0.00");
			activity.setEstimated_totalcost("0.00");
			activity.setExtendedprice("0.00");
			activity.setOverheadcost("0.00");
			activity.setListprice("0.00");
			activity.setProformamargin("0.00");
			activity.setEstimated_start_schedule("");
			activity.setEstimated_complete_schedule("");
			activity.setActivity_lib_Id("-1");
			activity.setStatus("A");

			activity.setAddendum_id("0");

			String addSFlag = Activitydao.Addactivity(activity, "IA",
					loginuserid, getDataSource(request, "ilexnewDB"), "");
			String addflag = addSFlag.substring(0, addSFlag.indexOf("|"));
			String Activity_Id = addSFlag.substring(addSFlag.indexOf("|") + 1,
					addSFlag.length());

			request.setAttribute("Activity_Id", "" + Activity_Id);
			request.setAttribute("Job_Id", "" + jobdashboardform.getJobid());
			request.setAttribute("Appendix_Id",
					"" + jobdashboardform.getAppendix_id());
			request.setAttribute("addflag", "" + addflag);
			if (addflag.equals("0"))
				return mapping.findForward("editoos");
		}

		if (request.getAttribute("changestatusflag") != null) {
			String changestatus = (String) request
					.getAttribute("changestatusflag");
			request.setAttribute("changestatusflag", changestatus);
		}

		BeanUtils.copyProperties(jobdashboardBean, jobdashboardform);
		if (ViewList.CheckAppendixType(jobdashboardform.getAppendix_id(),
				getDataSource(request, "ilexnewDB")).equals("1")) {
			request.setAttribute("NetMedX", "NetMedX");
			jobdashboardBean = JobDashboarddao.getNetMedXticketdetails(
					jobdashboardform.getJobid(), jobdashboardBean,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(jobdashboardform, jobdashboardBean);
			jobdashboardform.setInstallationlist(JobDashboarddao
					.getTop3installationnotes(jobdashboardform.getJobid(),
							getDataSource(request, "ilexnewDB")));
			// for yellow folder
			documentpresent = NetMedXDashboarddao.uploadedFilespresent("%",
					jobdashboardform.getJobid(), "Job",
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("documentpresent", documentpresent);

		}

		else {
			jobdashboardBean = JobDashboarddao.getJobdetailedfinancialinfo(
					jobdashboardform.getJobid(), jobdashboardBean,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(jobdashboardform, jobdashboardBean);
			jobdashboardBean = JobDashboarddao.getJobActualCostVGP(
					jobdashboardform.getJobid(), jobdashboardBean,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(jobdashboardform, jobdashboardBean);
			jobdashboardform.setInstallationlist(JobDashboarddao
					.getTop3installationnotes(jobdashboardform.getJobid(),
							getDataSource(request, "ilexnewDB")));
		}

		ArrayList actlist = JobDashboarddao.getActivitytabular(
				jobdashboardform.getJobid(),
				getDataSource(request, "ilexnewDB"), sortqueryclause);
		if (ViewList.CheckAppendixType(jobdashboardform.getAppendix_id(),
				getDataSource(request, "ilexnewDB")).equals("1")) {
			if (actlist.size() > 0) {
				job_size = actlist.size();

				for (int i = 0; i < job_size; i++) {
					activityid = ((ActivityTabularForm) actlist.get(i))
							.getActivity_id();
					((ActivityTabularForm) actlist.get(i))
							.setResourcelist(JobDashboarddao
									.getResourcetabular(activityid,
											getDataSource(request, "ilexnewDB")));
				}
			}
		}

		String[] temp = new String[6];
		temp = Appendixdao.getCustinfo(jobdashboardform.getAppendix_id(),
				jobdashboardform.getJobid(),
				getDataSource(request, "ilexnewDB"));

		jobdashboardform.setCustinfodata1(temp[0]);
		jobdashboardform.setCustinfovalue1(temp[1]);

		jobdashboardform.setCustinfodata2(temp[2]);
		jobdashboardform.setCustinfovalue2(temp[3]);

		jobdashboardform.setCustinfodata3(temp[4]);
		jobdashboardform.setCustinfovalue3(temp[5]);
		// start
		jobdashboardform.setCustinfodata4(temp[6]);
		jobdashboardform.setCustinfovalue4(temp[7]);
		jobdashboardform.setCustinfodata5(temp[8]);
		jobdashboardform.setCustinfovalue5(temp[9]);
		jobdashboardform.setCustinfodata6(temp[10]);
		jobdashboardform.setCustinfovalue6(temp[11]);
		jobdashboardform.setCustinfodata7(temp[12]);
		jobdashboardform.setCustinfovalue7(temp[13]);
		jobdashboardform.setCustinfodata8(temp[14]);
		jobdashboardform.setCustinfovalue8(temp[15]);
		jobdashboardform.setCustinfodata9(temp[16]);
		jobdashboardform.setCustinfovalue9(temp[17]);
		jobdashboardform.setCustinfodata10(temp[18]);
		jobdashboardform.setCustinfovalue10(temp[19]);

		// over
		ArrayList powolist = Appendixdao.getPowoinformation(
				jobdashboardform.getJobid(),
				getDataSource(request, "ilexnewDB"));

		String list = "";

		if (jobdashboardform.getJobtype().equals("Addendum")) {
			list = Menu.getStatus("Addendum", jobdashboardform.getJob_status()
					.charAt(0), "", jobdashboardform.getAppendix_id(),
					jobdashboardform.getJobid(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("prj_job", jobdashboardform.getJob_status()
					.charAt(0), "", jobdashboardform.getAppendix_id(),
					jobdashboardform.getJobid(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));
		}
		request.setAttribute("list", list);

		if (request.getAttribute("copyactivityflag") != null) {
			request.setAttribute("copyactivityflag",
					request.getAttribute("copyactivityflag").toString());
		}

		if (request.getAttribute("oosretval") != null) {
			request.setAttribute("oosretval", request.getAttribute("oosretval"));
		}

		request.setAttribute("actlist", actlist);
		request.setAttribute("powolist", powolist);
		request.setAttribute("job_size", new Integer(job_size));
		request.setAttribute("jobtype", jobdashboardform.getJobtype());
		request.setAttribute("viewType", jobdashboardform.getViewType());
		request.setAttribute("Statuschangedfailed",
				request.getAttribute("Statuschangedfailed") + "");
		return mapping.findForward("successnew");
	}
}
