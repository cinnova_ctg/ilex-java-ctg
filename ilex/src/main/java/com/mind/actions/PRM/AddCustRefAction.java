package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.AddComment;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.formbean.PRM.AddCustRefForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class AddCustRefAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionForward forward = new ActionForward();
		AddCustRefForm addcustrefform = (AddCustRefForm) form;

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");

		String Id = null;
		String Type = null;

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		Map<String, Object> map;

		if (request.getParameter("type") != null) {
			addcustrefform.setType(request.getParameter("type"));
		}

		if (request.getParameter("typeid") != null) {
			addcustrefform.setTypeId(request.getParameter("typeid"));
		}

		if (request.getParameter("viewjobtype") != null) {
			addcustrefform.setViewjobtype(request.getParameter("viewjobtype"));
		}

		if (request.getParameter("ownerId") != null) {
			addcustrefform.setOwnerId(request.getParameter("ownerId"));
		}

		if (request.getParameter("from") != null) {
			addcustrefform.setFrom(request.getParameter("from"));
		}

		request.setAttribute("from", addcustrefform.getFrom());

		if (request.getParameter("appendixid") != null) {
			addcustrefform.setAppendixid(request.getParameter("appendixid"));
		}

		if (addcustrefform.getType().equals("Appendix")) {
			addcustrefform.setName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					addcustrefform.getTypeId()));
		}

		if (addcustrefform.getType().equals("Job")) {
			addcustrefform.setName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"),
					addcustrefform.getTypeId()));
		}
		if (addcustrefform.getJobId() != null
				&& !addcustrefform.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(addcustrefform.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		if (addcustrefform.getAdd() != null) {
			addcustrefform.setUpdatemessage(""
					+ AddComment.addCustomerReference(
							addcustrefform.getTypeId(),
							addcustrefform.getType(),
							addcustrefform.getCustref(), loginuserid,
							getDataSource(request, "ilexnewDB")));

			request.setAttribute("path", "custref");
			request.setAttribute("typeId", addcustrefform.getTypeId());
			request.setAttribute("type", addcustrefform.getType());
			request.setAttribute("viewjobtype", addcustrefform.getViewjobtype());
			request.setAttribute("ownerId", addcustrefform.getOwnerId());
			request.setAttribute("appendix_Id", addcustrefform.getAppendixid());
			forward = mapping.findForward("temppage");
			return (forward);
		}

		if (addcustrefform.getTypeId() != null) {
			addcustrefform.setCustref(AddComment.getCustomerReference(
					addcustrefform.getTypeId(), addcustrefform.getType(),
					getDataSource(request, "ilexnewDB")));
		}

		/* Start : Appendix Dashboard View Selector Code */

		if (request.getParameter("fromPage") != null)
			addcustrefform.setFromPage(request.getParameter("fromPage"));

		if (addcustrefform.getJobOwnerOtherCheck() != null
				&& !addcustrefform.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					addcustrefform.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", addcustrefform.getAppendixid());
			request.setAttribute("msaId", addcustrefform.getMsaId());

			return mapping.findForward("temppage");

		}

		if (addcustrefform.getJobSelectedStatus() != null) {
			for (int i = 0; i < addcustrefform.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ addcustrefform.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ addcustrefform.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (addcustrefform.getJobSelectedOwners() != null) {
			for (int j = 0; j < addcustrefform.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ addcustrefform.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ addcustrefform.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					addcustrefform.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					addcustrefform.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", addcustrefform.getAppendixid());
			request.setAttribute("msaId", addcustrefform.getMsaId());

			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				addcustrefform.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				addcustrefform.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				addcustrefform.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				addcustrefform.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				addcustrefform.getJobOwnerOtherCheck(), "prj_job_new",
				addcustrefform.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(addcustrefform.getAppendixid(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				addcustrefform.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		addcustrefform.setMsaId(IMdao.getMSAId(addcustrefform.getAppendixid(),
				getDataSource(request, "ilexnewDB")));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("msaId", addcustrefform.getMsaId());
		request.setAttribute("viewjobtype", addcustrefform.getViewjobtype());
		request.setAttribute("ownerId", addcustrefform.getOwnerId());
		request.setAttribute("appendixid", addcustrefform.getAppendixid());

		forward = mapping.findForward("success");
		return (forward);
	}
}
