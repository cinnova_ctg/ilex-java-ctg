package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboPRM;
import com.mind.common.LabelValue;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.AppendixMail;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.formbean.PRM.AddContactForm;
import com.mind.util.WebUtil;

public class AddContactAction extends com.mind.common.IlexDispatchAction {

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddContactForm addContactForm = (AddContactForm) form;
		DynamicComboPRM dcprm = new DynamicComboPRM();
		ViewList vlist = new ViewList();

		// Variables added for the View Selector Functioning.
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "msa";
		String msaid = "0";
		String msaname = "";
		int ownerListSize = 0;

		/*** Code added for the View Selecotr ***/
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");

		if (addContactForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage Appendix",
					"Edit Contact Information",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		int flag = 0;
		int statusvalue = -1;
		String typeid = "";
		String type = "";
		String pocid = "";
		String custpocid = "";
		String salespocid = "";
		String businesspocid = "";
		String contractpocid = "";
		String billingpocid = "";
		String addmessage = "";
		String appendixname = "";
		String pocname = "";
		String custpocname = "";
		String salespocname = "";
		String businesspocname = "";
		String contractpocname = "";
		String billingpocname = "";

		String cnsPrjManagerId = "";
		String teamLeadId = "";
		String cnsPrjManagerName = "";
		String teamLeadName = "";

		String[] list = new String[5];
		String[] saleslist = new String[5];
		String[] businesslist = new String[5];
		String[] contractlist = new String[5];
		String[] billinglist = new String[5];
		String[] custlist = new String[5];
		String[] pocsidlist = new String[11];
		ArrayList poclist = new ArrayList();
		ArrayList custpoclist = new ArrayList();
		ArrayList salespoclist = new ArrayList();
		ArrayList businesspoclist = new ArrayList();
		ArrayList contractpoclist = new ArrayList();
		ArrayList billingpoclist = new ArrayList();
		List<LabelValue> cnsPrjManagerList = new ArrayList<LabelValue>();
		List<LabelValue> teamLeadList = new ArrayList<LabelValue>();
		String[] cnsPrjManangerDetail = new String[5];
		String[] teamLeadDetail = new String[5];

		if (request.getParameter("viewjobtype") != null) {
			addContactForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			addContactForm.setViewjobtype("A");
		}
		if (request.getParameter("Type") != null) {
			addContactForm.setFromPage(request.getParameter("Type"));
		}
		request.setAttribute("viewjobtype", addContactForm.getViewjobtype());

		if (request.getParameter("ownerId") != null) {
			addContactForm.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			addContactForm.setOwnerId("%");
		}

		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		if (addContactForm.getSave() != null) {
			typeid = addContactForm.getTypeid();
			type = addContactForm.getType();
			pocid = addContactForm.getCnspoc();
			custpocid = addContactForm.getCustpoc();
			salespocid = addContactForm.getSalespoc();
			businesspocid = addContactForm.getBusinesspoc();
			contractpocid = addContactForm.getContractpoc();
			billingpocid = addContactForm.getBillingpoc();

			cnsPrjManagerId = addContactForm.getCnsPrjManagerPoc();
			teamLeadId = addContactForm.getTeamLeadPoc();

			appendixname = AddContactdao.getAppendixname(typeid,
					getDataSource(request, "ilexnewDB"));
			msaid = IMdao.getMSAId(typeid, getDataSource(request, "ilexnewDB"));
			msaname = com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), msaid);
			addContactForm.setAppendixname(appendixname);
			String currentPcId = AddContactdao.getPocid(typeid,
					getDataSource(request, "ilexnewDB"));
			/* use to check project manager is changed or not for email send */
			statusvalue = AddContactdao.addContact(pocid, custpocid,
					salespocid, billingpocid, businesspocid, contractpocid,
					typeid, userid, cnsPrjManagerId, teamLeadId,
					getDataSource(request, "ilexnewDB"));

			/* Send Mail: Start */
			if (statusvalue == 0) { // - its call when Project Manager is
									// assigned or updated
				/* use to check project manager is changed or not */
				String changedPcId = AddContactdao.getPocid(typeid,
						getDataSource(request, "ilexnewDB"));

				if (!changedPcId.equals(currentPcId)) { // - send mail only on
														// project manager
														// chanage.
					String url = request.getRequestURL().toString()
							.replaceAll(request.getServletPath(), "/");
					AppendixMail.projectManagerUpdateMail(typeid, appendixname,
							msaname, pocid, url, userid,
							getDataSource(request, "ilexnewDB")); // - to send
																	// mail for
																	// assignment/updation
																	// of
																	// Project
																	// manager
				}
			}// - end of if
			/* Send Mail: End */

			request.setAttribute("addmessage", statusvalue + "");
			pocname = AddContactdao.getPocname(pocid,
					getDataSource(request, "ilexnewDB"));
			custpocname = AddContactdao.getPocname(custpocid,
					getDataSource(request, "ilexnewDB"));
			salespocname = AddContactdao.getPocname(salespocid,
					getDataSource(request, "ilexnewDB"));
			businesspocname = AddContactdao.getPocname(businesspocid,
					getDataSource(request, "ilexnewDB"));
			contractpocname = AddContactdao.getPocname(contractpocid,
					getDataSource(request, "ilexnewDB"));
			billingpocname = AddContactdao.getPocname(billingpocid,
					getDataSource(request, "ilexnewDB"));

			cnsPrjManagerName = AddContactdao.getPocname(cnsPrjManagerId,
					getDataSource(request, "ilexnewDB"));
			teamLeadName = AddContactdao.getPocname(teamLeadId,
					getDataSource(request, "ilexnewDB"));

			addContactForm.setPocname(pocname);
			addContactForm.setCustpocname(custpocname);
			addContactForm.setSalespocname(salespocname);
			addContactForm.setBusinesspocname(businesspocname);
			addContactForm.setContractpocname(contractpocname);
			addContactForm.setBillingpocname(billingpocname);

			addContactForm.setCnsPrjManagerPocName(cnsPrjManagerName);
			addContactForm.setTeamLeadPocName(teamLeadName);

			if (addContactForm.getFromPage().equals("Appendix")) {
				request.setAttribute("Appendix_Id", addContactForm.getTypeid());
				request.setAttribute("MSA_Id", IMdao.getMSAId(
						addContactForm.getTypeid(),
						getDataSource(request, "ilexnewDB")));
				forward = mapping.findForward("appendixdetailpage");
			} else {
				if (statusvalue == 0) {
					flag = 1;
					String temppath = "PRMAppendixContact";
					request.setAttribute("type", temppath);
					request.setAttribute("appendix_Id",
							addContactForm.getTypeid());
					request.setAttribute("msaId", IMdao.getMSAId(
							addContactForm.getTypeid(),
							getDataSource(request, "ilexnewDB")));
					return (mapping.findForward("temppage"));

				} else {
					forward = mapping.findForward("success");
				}
			}

		} else {
			typeid = request.getParameter("typeid");
			String temp_typeid = "";
			type = request.getParameter("type");

			// for Menu and View selector: Start
			if (request.getParameter("Type") != null
					&& addContactForm.getFromPage().equals("Appendix")) {

				addContactForm.setName(Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"), typeid));
				addContactForm.setMsaId(IMdao.getMSAId(typeid,
						getDataSource(request, "ilexnewDB")));
				addContactForm.setMsaName(com.mind.dao.PM.Appendixdao
						.getMsaname(getDataSource(request, "ilexnewDB"),
								addContactForm.getMsaId()));
				ownerType = "appendix";
				if (request.getParameter("home") != null) // This is called when
															// reset button is
															// pressed from the
															// view selector.
				{
					addContactForm.setAppendixSelectedStatus(null);
					addContactForm.setAppendixSelectedOwners(null);
					addContactForm.setPrevSelectedStatus("");
					addContactForm.setPrevSelectedOwner("");
					addContactForm.setAppendixOtherCheck(null);
					addContactForm.setMonthWeekCheck(null);

					request.setAttribute("MSA_Id", addContactForm.getMsaId());
					return (mapping.findForward("appendixlistpage"));

				}

				// These two parameter month/week are checked are kept in
				// session. ( View Images)
				if (request.getParameter("month") != null) {
					session.setAttribute("monthAppendix",
							request.getParameter("month").toString());

					session.removeAttribute("weekAppendix");
					addContactForm.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekAppendix",
							request.getParameter("week").toString());

					session.removeAttribute("monthAppendix");
					addContactForm.setMonthWeekCheck("week");
				}

				if (addContactForm.getAppendixOtherCheck() != null) // OtherCheck
																	// box
																	// status is
																	// kept in
																	// session.
					session.setAttribute("appendixOtherCheck", addContactForm
							.getAppendixOtherCheck().toString());

				// When Status Check Boxes are Clicked :::::::::::::::::
				if (addContactForm.getAppendixSelectedStatus() != null) {
					for (int i = 0; i < addContactForm
							.getAppendixSelectedStatus().length; i++) {
						selectedStatus = selectedStatus + "," + "'"
								+ addContactForm.getAppendixSelectedStatus(i)
								+ "'";
						tempStatus = tempStatus + ","
								+ addContactForm.getAppendixSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				// When Owner Check Boxes are Clicked :::::::::::::::::
				if (addContactForm.getAppendixSelectedOwners() != null) {
					for (int j = 0; j < addContactForm
							.getAppendixSelectedOwners().length; j++) {
						selectedOwner = selectedOwner + ","
								+ addContactForm.getAppendixSelectedOwners(j);
						tempOwner = tempOwner + ","
								+ addContactForm.getAppendixSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}

				if (addContactForm.getGo() != null
						&& !addContactForm.getGo().equals("")) {
					session.setAttribute("appendixSelectedOwners",
							selectedOwner);
					session.setAttribute("appSelectedOwners", selectedOwner);
					session.setAttribute("appendixTempOwner", tempOwner);
					session.setAttribute("appendixSelectedStatus",
							selectedStatus);
					session.setAttribute("appSelectedStatus", selectedStatus);
					session.setAttribute("appendixTempStatus", tempStatus);

					if (addContactForm.getMonthWeekCheck() != null) {
						if (addContactForm.getMonthWeekCheck().equals("week")) {
							session.removeAttribute("monthAppendix");
							session.setAttribute("weekAppendix", "0");

						}
						if (addContactForm.getMonthWeekCheck().equals("month")) {
							session.removeAttribute("weekAppendix");
							session.setAttribute("monthAppendix", "0");

						}
						if (addContactForm.getMonthWeekCheck().equals("clear")) {
							session.removeAttribute("monthAppendix");
							session.removeAttribute("weekAppendix");
						}

					}
					request.setAttribute("MSA_Id", addContactForm.getMsaId());
					return (mapping.findForward("appendixlistpage"));
				} else {
					if (session.getAttribute("appendixSelectedOwners") != null) {
						selectedOwner = session.getAttribute(
								"appendixSelectedOwners").toString();
						addContactForm.setAppendixSelectedOwners(session
								.getAttribute("appendixTempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("appendixSelectedStatus") != null) {
						selectedStatus = session.getAttribute(
								"appendixSelectedStatus").toString();
						addContactForm.setAppendixSelectedStatus(session
								.getAttribute("appendixTempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("appendixOtherCheck") != null)
						addContactForm.setAppendixOtherCheck(session
								.getAttribute("appendixOtherCheck").toString());

					if (session.getAttribute("monthAppendix") != null) {
						session.removeAttribute("weekAppendix");
						addContactForm.setMonthWeekCheck("month");
					}
					if (session.getAttribute("weekAppendix") != null) {
						session.removeAttribute("monthAppendix");
						addContactForm.setMonthWeekCheck("week");
					}

				}
				if (!chkOtherOwner) {
					addContactForm.setPrevSelectedStatus(selectedStatus);
					addContactForm.setPrevSelectedOwner(selectedOwner);
				}
				ownerList = MSAdao.getOwnerList(userid,
						addContactForm.getAppendixOtherCheck(), ownerType,
						addContactForm.getMsaId(),
						getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_appendix",
						getDataSource(request, "ilexnewDB"));

				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

				String appendixtype = Appendixdao.getAppendixPrType(
						addContactForm.getMsaId(), typeid,
						getDataSource(request, "ilexnewDB"));
				// request.setAttribute( "changestatusflag" , "true" );
				request.setAttribute("MSA_Id", addContactForm.getMsaId());
				request.setAttribute("Appendix_Id", typeid);
				request.setAttribute("appendixType", appendixtype);

				String appendixStatus = com.mind.dao.PM.Appendixdao
						.getAppendixStatus(addContactForm.getTypeid(),
								addContactForm.getMsaId(),
								getDataSource(request, "ilexnewDB"));
				String addendumlist = com.mind.dao.PM.Appendixdao
						.getAddendumsIdName(addContactForm.getTypeid(),
								getDataSource(request, "ilexnewDB"));
				String addendumid = com.mind.dao.PM.Addendumdao
						.getLatestAddendumId(addContactForm.getTypeid(),
								getDataSource(request, "ilexnewDB"));
				String list1 = Menu.getStatus("pm_appendix",
						appendixStatus.charAt(0), addContactForm.getMsaId(),
						addContactForm.getTypeid(), "", "", userid,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixStatusList", list1);
				request.setAttribute("appendixStatus", appendixStatus);
				request.setAttribute("latestaddendumid", addendumid);
				request.setAttribute("addendumlist", addendumlist);
			}
			// for Menu and View selector: End

			// for AppendixDashboard Menu:start

			if (request.getParameter("fromPage") != null
					&& (addContactForm.getFromPage()
							.equals("AppendixDashBoard") || addContactForm
							.getFromPage().equals("NetMedX"))) {
				/* Start : Appendix Dashboard View Selector Code */

				if (addContactForm.getJobOwnerOtherCheck() != null
						&& !addContactForm.getJobOwnerOtherCheck().equals("")) {
					session.setAttribute("jobOwnerOtherCheck",
							addContactForm.getJobOwnerOtherCheck());
				}

				if (request.getParameter("opendiv") != null) {
					request.setAttribute("opendiv",
							request.getParameter("opendiv"));
				}
				if (request.getParameter("resetList") != null
						&& request.getParameter("resetList")
								.equals("something")) {
					WebUtil.setDefaultAppendixDashboardAttribute(session);
					request.setAttribute("type", "AppendixViewSelector");
					request.setAttribute("appendixId", typeid);
					request.setAttribute("msaId", addContactForm.getMsaId());
					request.setAttribute("viewjobtype",
							addContactForm.getViewjobtype());
					request.setAttribute("ownerId", addContactForm.getOwnerId());

					return mapping.findForward("temppage");

				}

				if (addContactForm.getJobSelectedStatus() != null) {
					for (int i = 0; i < addContactForm.getJobSelectedStatus().length; i++) {
						jobSelectedStatus = jobSelectedStatus + "," + "'"
								+ addContactForm.getJobSelectedStatus(i) + "'";
						jobTempStatus = jobTempStatus + ","
								+ addContactForm.getJobSelectedStatus(i);
					}

					jobSelectedStatus = jobSelectedStatus.substring(1,
							jobSelectedStatus.length());
					jobSelectedStatus = "(" + jobSelectedStatus + ")";
				}

				if (addContactForm.getJobSelectedOwners() != null) {
					for (int j = 0; j < addContactForm.getJobSelectedOwners().length; j++) {
						jobSelectedOwner = jobSelectedOwner + ","
								+ addContactForm.getJobSelectedOwners(j);
						jobTempOwner = jobTempOwner + ","
								+ addContactForm.getJobSelectedOwners(j);
					}
					jobSelectedOwner = jobSelectedOwner.substring(1,
							jobSelectedOwner.length());
					if (jobSelectedOwner.equals("0")) {
						jobSelectedOwner = "%";
					}
					jobSelectedOwner = "(" + jobSelectedOwner + ")";
				}

				if (request.getParameter("showList") != null
						&& request.getParameter("showList").equals("something")) {

					session.setAttribute("jobSelectedOwners", jobSelectedOwner);
					session.setAttribute("jobTempOwner", jobTempOwner);
					session.setAttribute("jobSelectedStatus", jobSelectedStatus);
					session.setAttribute("jobTempStatus", jobTempStatus);
					session.setAttribute("timeFrame",
							addContactForm.getJobMonthWeekCheck());
					session.setAttribute("jobOwnerOtherCheck",
							addContactForm.getJobOwnerOtherCheck());

					request.setAttribute("type", "AppendixViewSelector");
					request.setAttribute("appendixId", typeid);
					request.setAttribute("msaId", addContactForm.getMsaId());
					request.setAttribute("viewjobtype",
							addContactForm.getViewjobtype());
					request.setAttribute("ownerId", addContactForm.getOwnerId());

					return mapping.findForward("temppage");

				} else {
					if (session.getAttribute("jobSelectedOwners") != null) {
						jobSelectedOwner = session.getAttribute(
								"jobSelectedOwners").toString();
						addContactForm.setJobSelectedOwners(session
								.getAttribute("jobTempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("jobSelectedStatus") != null) {
						jobSelectedStatus = session.getAttribute(
								"jobSelectedStatus").toString();
						addContactForm.setJobSelectedStatus(session
								.getAttribute("jobTempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("jobOwnerOtherCheck") != null) {
						addContactForm.setJobOwnerOtherCheck(session
								.getAttribute("jobOwnerOtherCheck").toString());
					}
					if (session.getAttribute("timeFrame") != null) {
						addContactForm.setJobMonthWeekCheck(session
								.getAttribute("timeFrame").toString());
					}
				}

				jobOwnerList = MSAdao.getOwnerList(userid,
						addContactForm.getJobOwnerOtherCheck(), "prj_job_new",
						typeid, getDataSource(request, "ilexnewDB"));
				jobStatusList = MSAdao.getStatusList("prj_job_new",
						getDataSource(request, "ilexnewDB"));

				if (jobOwnerList.size() > 0)
					jobOwnerListSize = jobOwnerList.size();

				request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
				request.setAttribute("jobStatusList", jobStatusList);
				request.setAttribute("jobOwnerList", jobOwnerList);

				String contractDocoumentList = com.mind.dao.PM.Appendixdao
						.getcontractDocoumentMenu(typeid,
								getDataSource(request, "ilexnewDB"));
				request.setAttribute("contractDocMenu", contractDocoumentList);
				String appendixcurrentstatus = Appendixdao.getCurrentstatus(
						typeid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixcurrentstatus",
						appendixcurrentstatus);
				request.setAttribute("viewjobtype",
						addContactForm.getViewjobtype());
				request.setAttribute("ownerId", addContactForm.getOwnerId());
				request.setAttribute("appendixid", typeid);

				/* End : Appendix Dashboard View Selector Code */
			}

			// for AppendixDashboard Menu:end

			if (type.equals("X")) {
				temp_typeid = IMdao.getMSAId(typeid,
						getDataSource(request, "ilexnewDB"));
			} else
				temp_typeid = typeid;

			appendixname = AddContactdao.getAppendixname(typeid,
					getDataSource(request, "ilexnewDB"));
			addContactForm.setAppendixname(appendixname);
			poclist = vlist.getProjectManagerPOC("srProjectManager",
					getDataSource(request, "ilexnewDB")); // - Sr. Project
															// Manager
			salespoclist = vlist.getProjectManagerPOC("sales",
					getDataSource(request, "ilexnewDB")); // - Sales Poc
			custpoclist = vlist.getSitePOC(temp_typeid, type,
					getDataSource(request, "ilexnewDB"));
			businesspoclist = vlist.getSitePOC(temp_typeid, type,
					getDataSource(request, "ilexnewDB"));
			contractpoclist = vlist.getSitePOC(temp_typeid, type,
					getDataSource(request, "ilexnewDB"));
			billingpoclist = vlist.getSitePOC(temp_typeid, type,
					getDataSource(request, "ilexnewDB"));

			cnsPrjManagerList = vlist.getProjectManagerPOC("projectManager",
					getDataSource(request, "ilexnewDB")); // Project Manager
			teamLeadList = vlist.getProjectManagerPOC("teamLead",
					getDataSource(request, "ilexnewDB")); // Team Lead

			if (request.getParameter("Type") != null
					&& addContactForm.getFromPage().equals("Appendix")) {
				request.setAttribute("Appendix_Id", addContactForm.getTypeid());
				request.setAttribute("MSA_Id", IMdao.getMSAId(
						addContactForm.getTypeid(),
						getDataSource(request, "ilexnewDB")));
				forward = mapping.findForward("appendixdetailpage");
			}

			if (poclist.size() > 0) {
				dcprm.setCnspoclist(poclist);

			}

			if (custpoclist.size() > 0) {
				dcprm.setCustpoclist(custpoclist);

			}

			if (salespoclist.size() > 0) {
				dcprm.setSalespoclist(salespoclist);
			}
			if (businesspoclist.size() > 0) {
				dcprm.setBusinesspoclist(businesspoclist);
			}
			if (contractpoclist.size() > 0) {
				dcprm.setContractpoclist(contractpoclist);
			}
			if (billingpoclist.size() > 0) {
				dcprm.setBillingpoclist(billingpoclist);
			}
			if (cnsPrjManagerList.size() > 0) {
				dcprm.setCnsPrjManagerList(cnsPrjManagerList);
			}
			if (teamLeadList.size() > 0) {
				dcprm.setTeamLeadList(teamLeadList);
			}
			pocid = AddContactdao.getPocid(typeid,
					getDataSource(request, "ilexnewDB"));
			custpocid = AddContactdao.getCustpocid(typeid,
					getDataSource(request, "ilexnewDB"));
			pocsidlist = Appendixdao.getAppendixpocdetails(typeid,
					getDataSource(request, "ilexnewDB"));
			salespocid = pocsidlist[8];
			businesspocid = pocsidlist[9];
			contractpocid = pocsidlist[10];
			billingpocid = pocsidlist[11];

			cnsPrjManagerId = AddContactdao.getCnsPrjManagerId(typeid,
					getDataSource(request, "ilexnewDB"));
			teamLeadId = AddContactdao.getTeamLeadId(typeid,
					getDataSource(request, "ilexnewDB"));
			if (pocid == null) {

			} else {

				list = AddContactdao.getPocDetails(pocid,
						getDataSource(request, "ilexnewDB"));
				addContactForm.setCnspoc(pocid);
				addContactForm.setPhone1(list[0]);
				addContactForm.setPhone2(list[1]);
				addContactForm.setEmail1(list[2]);
				addContactForm.setEmail2(list[3]);
				addContactForm.setFax(list[4]);

			}
			if (custpocid == null) {

			} else {

				custlist = AddContactdao.getPocDetails(custpocid,
						getDataSource(request, "ilexnewDB"));
				addContactForm.setCustpoc(custpocid);
				addContactForm.setCustphone1(custlist[0]);
				addContactForm.setCustphone2(custlist[1]);
				addContactForm.setCustemail1(custlist[2]);
				addContactForm.setCustemail2(custlist[3]);
				addContactForm.setCustfax(custlist[4]);

			}

			if (salespocid == null) {

			} else {

				saleslist = AddContactdao.getPocDetails(salespocid,
						getDataSource(request, "ilexnewDB"));
				addContactForm.setSalespoc(salespocid);
				addContactForm.setSpocphone1(saleslist[0]);
				addContactForm.setSpocphone2(saleslist[1]);
				addContactForm.setSpocemail1(saleslist[2]);
				addContactForm.setSpocemail2(saleslist[3]);
				addContactForm.setSpocfax(saleslist[4]);

			}

			if (businesspocid == null) {

			} else {

				businesslist = AddContactdao.getPocDetails(businesspocid,
						getDataSource(request, "ilexnewDB"));

				addContactForm.setBusinesspoc(businesspocid);
				addContactForm.setBusinesspocphone1(businesslist[0]);
				addContactForm.setBusinesspocphone2(businesslist[1]);
				addContactForm.setBusinesspocemail1(businesslist[2]);
				addContactForm.setBusinesspocemail2(businesslist[3]);
				addContactForm.setBusinesspocfax(businesslist[4]);

			}

			if (contractpocid == null) {

			} else {

				contractlist = AddContactdao.getPocDetails(contractpocid,
						getDataSource(request, "ilexnewDB"));

				addContactForm.setContractpoc(contractpocid);
				addContactForm.setContractpocphone1(contractlist[0]);
				addContactForm.setContractpocphone2(contractlist[1]);
				addContactForm.setContractpocemail1(contractlist[2]);
				addContactForm.setContractpocemail2(contractlist[3]);
				addContactForm.setContractpocfax(contractlist[4]);

			}
			if (billingpocid == null) {

			} else {

				billinglist = AddContactdao.getPocDetails(billingpocid,
						getDataSource(request, "ilexnewDB"));

				addContactForm.setBillingpoc(billingpocid);
				addContactForm.setBillingpocphone1(billinglist[0]);
				addContactForm.setBillingpocphone2(billinglist[1]);
				addContactForm.setBillingpocemail1(billinglist[2]);
				addContactForm.setBillingpocemail2(billinglist[3]);
				addContactForm.setBillingpocfax(billinglist[4]);
			}
			if (cnsPrjManagerId == null) {

			} else {

				cnsPrjManangerDetail = AddContactdao.getPocDetails(
						cnsPrjManagerId, getDataSource(request, "ilexnewDB"));

				addContactForm.setCnsPrjManagerPoc(cnsPrjManagerId);
				addContactForm.setCnsPrjManagerPhone1(cnsPrjManangerDetail[0]);
				addContactForm.setCnsPrjManagerPhone2(cnsPrjManangerDetail[1]);
				addContactForm.setCnsPrjManagerMail1(cnsPrjManangerDetail[2]);
				addContactForm.setCnsPrjManagerMail2(cnsPrjManangerDetail[3]);
				addContactForm.setCnsPrjManagerFax(cnsPrjManangerDetail[4]);
			}
			if (teamLeadId == null) {

			} else {

				teamLeadDetail = AddContactdao.getPocDetails(teamLeadId,
						getDataSource(request, "ilexnewDB"));

				addContactForm.setTeamLeadPoc(teamLeadId);
				addContactForm.setTeamLeadPhone1(teamLeadDetail[0]);
				addContactForm.setTeamLeadPhone2(teamLeadDetail[1]);
				addContactForm.setTeamLeadMail1(teamLeadDetail[2]);
				addContactForm.setTeamLeadMail2(teamLeadDetail[3]);
				addContactForm.setTeamLeadFax(teamLeadDetail[4]);
			}

			forward = mapping.findForward("success");

		}

		request.setAttribute("dynamiccombo", dcprm);
		request.setAttribute("typeid", typeid);
		request.setAttribute("type", type);
		request.setAttribute("viewflag", flag + "");

		return (forward);
	}

	public ActionForward refresh(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddContactForm addContactForm = (AddContactForm) form;
		DynamicComboPRM dcprm = new DynamicComboPRM();
		ViewList vlist = new ViewList();
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");

		/*** Code added for the View Selecotr ***/
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		/*
		 * Page Security Start HttpSession session = request.getSession( true );
		 * String userid = (String)session.getAttribute("userid"); if(
		 * addContactForm.getAuthenticate().equals( "" ) ) { if(
		 * !Authenticationdao.getPageSecurity( userid , "Manage Project" ,
		 * "Update Schedule" , getDataSource( request , "ilexnewDB" ) ) ) {
		 * return( mapping.findForward( "UnAuthenticate" ) ); } } Page Security
		 * End
		 */

		int flag = 0;
		int statusvalue = 0;
		String typeid = "";
		String type = "";
		String[] list = new String[5];
		String[] custlist = new String[5];
		String[] saleslist = new String[5];
		String[] businesslist = new String[5];
		String[] contractlist = new String[5];
		String[] billinglist = new String[5];
		ArrayList poclist = new ArrayList();
		ArrayList salespoclist = new ArrayList();
		ArrayList custpoclist = new ArrayList();
		List<LabelValue> cnsPrjManagetList = new ArrayList<LabelValue>();
		List<LabelValue> teamLeadList = new ArrayList<LabelValue>();
		String[] cnsPrjManangerDetail = new String[5];
		String[] teamLeadDetail = new String[5];

		String appendixname = "";
		String temp_typeid = "";

		// Variables added for the View Selector Functioning.
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "msa";
		String msaid = "0";
		int ownerListSize = 0;

		typeid = addContactForm.getTypeid();
		type = addContactForm.getType();
		appendixname = AddContactdao.getAppendixname(typeid,
				getDataSource(request, "ilexnewDB"));
		addContactForm.setAppendixname(appendixname);

		if (request.getParameter("viewjobtype") != null) {
			addContactForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			addContactForm.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			addContactForm.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			addContactForm.setOwnerId("%");
		}

		if (addContactForm.getRefresh().equals("true")) {
			poclist = vlist.getProjectManagerPOC("srProjectManager",
					getDataSource(request, "ilexnewDB"));
			salespoclist = vlist.getProjectManagerPOC("sales",
					getDataSource(request, "ilexnewDB"));

			cnsPrjManagetList = vlist.getProjectManagerPOC("projectManager",
					getDataSource(request, "ilexnewDB")); // Project Manager
			teamLeadList = vlist.getProjectManagerPOC("teamLead",
					getDataSource(request, "ilexnewDB")); // Team Lead

			if (type.equals("X"))
				temp_typeid = IMdao.getMSAId(typeid,
						getDataSource(request, "ilexnewDB"));
			else
				temp_typeid = typeid;

			custpoclist = vlist.getSitePOC(temp_typeid, type,
					getDataSource(request, "ilexnewDB"));

			if (poclist.size() > 0) {
				dcprm.setCnspoclist(poclist);
				dcprm.setSalespoclist(salespoclist);
			}
			if (cnsPrjManagetList.size() > 0) {
				dcprm.setCnsPrjManagerList(cnsPrjManagetList);
			}
			if (teamLeadList.size() > 0) {
				dcprm.setTeamLeadList(teamLeadList);
			}

			if (custpoclist.size() > 0) {
				dcprm.setCustpoclist(custpoclist);
				dcprm.setBusinesspoclist(custpoclist);
				dcprm.setContractpoclist(custpoclist);
				dcprm.setBillingpoclist(custpoclist);
			}
		}
		if (!(addContactForm.getCnspoc().equals("0"))) {

			list = AddContactdao.getPocDetails(addContactForm.getCnspoc(),
					getDataSource(request, "ilexnewDB"));
			addContactForm.setCnspoc(addContactForm.getCnspoc());
			addContactForm.setPhone1(list[0]);
			addContactForm.setPhone2(list[1]);
			addContactForm.setEmail1(list[2]);
			addContactForm.setEmail2(list[3]);
			addContactForm.setFax(list[4]);

		} else {
			addContactForm.setCnspoc("0");
			addContactForm.setPhone1("");
			addContactForm.setPhone2("");
			addContactForm.setEmail1("");
			addContactForm.setEmail2("");
			addContactForm.setFax("");
		}

		if (!(addContactForm.getCustpoc().equals("0"))) {

			custlist = AddContactdao.getPocDetails(addContactForm.getCustpoc(),
					getDataSource(request, "ilexnewDB"));
			addContactForm.setCustpoc(addContactForm.getCustpoc());
			addContactForm.setCustphone1(custlist[0]);
			addContactForm.setCustphone2(custlist[1]);
			addContactForm.setCustemail1(custlist[2]);
			addContactForm.setCustemail2(custlist[3]);
			addContactForm.setCustfax(custlist[4]);

		} else {
			addContactForm.setCustpoc("0");
			addContactForm.setCustphone1("");
			addContactForm.setCustphone2("");
			addContactForm.setCustemail1("");
			addContactForm.setCustemail2("");
			addContactForm.setCustfax("");
		}

		if (!(addContactForm.getSalespoc().equals("0"))) {

			saleslist = AddContactdao.getPocDetails(
					addContactForm.getSalespoc(),
					getDataSource(request, "ilexnewDB"));
			addContactForm.setSalespoc(addContactForm.getSalespoc());
			addContactForm.setSpocphone1(saleslist[0]);
			addContactForm.setSpocphone2(saleslist[1]);
			addContactForm.setSpocemail1(saleslist[2]);
			addContactForm.setSpocemail2(saleslist[3]);
			addContactForm.setSpocfax(saleslist[4]);

		} else {
			addContactForm.setSalespoc("0");
			addContactForm.setSpocphone1("");
			addContactForm.setSpocphone2("");
			addContactForm.setSpocemail1("");
			addContactForm.setSpocemail2("");
			addContactForm.setSpocfax("");
		}

		if (!(addContactForm.getBusinesspoc().equals("0"))) {

			businesslist = AddContactdao.getPocDetails(
					addContactForm.getBusinesspoc(),
					getDataSource(request, "ilexnewDB"));
			addContactForm.setBusinesspoc(addContactForm.getBusinesspoc());
			addContactForm.setBusinesspocphone1(businesslist[0]);
			addContactForm.setBusinesspocphone2(businesslist[1]);
			addContactForm.setBusinesspocemail1(businesslist[2]);
			addContactForm.setBusinesspocemail2(businesslist[3]);
			addContactForm.setBusinesspocfax(businesslist[4]);

		} else {
			addContactForm.setBusinesspoc("0");
			addContactForm.setBusinesspocphone1("");
			addContactForm.setBusinesspocphone2("");
			addContactForm.setBusinesspocemail1("");
			addContactForm.setBusinesspocemail2("");
			addContactForm.setBusinesspocfax("");
		}

		if (!(addContactForm.getBillingpoc().equals("0"))) {

			billinglist = AddContactdao.getPocDetails(
					addContactForm.getBillingpoc(),
					getDataSource(request, "ilexnewDB"));
			addContactForm.setBillingpoc(addContactForm.getBillingpoc());
			addContactForm.setBillingpocphone1(billinglist[0]);
			addContactForm.setBillingpocphone2(billinglist[1]);
			addContactForm.setBillingpocemail1(billinglist[2]);
			addContactForm.setBillingpocemail2(billinglist[3]);
			addContactForm.setBillingpocfax(billinglist[4]);

		} else {
			addContactForm.setBillingpoc("0");
			addContactForm.setBillingpocphone1("");
			addContactForm.setBillingpocphone2("");
			addContactForm.setBillingpocemail1("");
			addContactForm.setBillingpocemail2("");
			addContactForm.setBillingpocfax("");
		}

		if (!(addContactForm.getContractpoc().equals("0"))) {

			contractlist = AddContactdao.getPocDetails(
					addContactForm.getContractpoc(),
					getDataSource(request, "ilexnewDB"));
			addContactForm.setContractpoc(addContactForm.getContractpoc());
			addContactForm.setContractpocphone1(contractlist[0]);
			addContactForm.setContractpocphone2(contractlist[1]);
			addContactForm.setContractpocemail1(contractlist[2]);
			addContactForm.setContractpocemail2(contractlist[3]);
			addContactForm.setContractpocfax(contractlist[4]);

		} else {
			addContactForm.setContractpoc("0");
			addContactForm.setContractpocphone1("");
			addContactForm.setContractpocphone2("");
			addContactForm.setContractpocemail1("");
			addContactForm.setContractpocemail2("");
			addContactForm.setContractpocfax("");
		}

		if (!(addContactForm.getCnsPrjManagerPoc().equals("0"))) {
			cnsPrjManangerDetail = AddContactdao.getPocDetails(
					addContactForm.getCnsPrjManagerPoc(),
					getDataSource(request, "ilexnewDB"));

			addContactForm.setCnsPrjManagerPoc(addContactForm
					.getCnsPrjManagerPoc());
			addContactForm.setCnsPrjManagerPhone1(cnsPrjManangerDetail[0]);
			addContactForm.setCnsPrjManagerPhone2(cnsPrjManangerDetail[1]);
			addContactForm.setCnsPrjManagerMail1(cnsPrjManangerDetail[2]);
			addContactForm.setCnsPrjManagerMail2(cnsPrjManangerDetail[3]);
			addContactForm.setCnsPrjManagerFax(cnsPrjManangerDetail[4]);

		} else {
			addContactForm.setCnsPrjManagerPoc("0");
			addContactForm.setCnsPrjManagerPhone1("");
			addContactForm.setCnsPrjManagerPhone2("");
			addContactForm.setCnsPrjManagerMail1("");
			addContactForm.setCnsPrjManagerMail2("");
			addContactForm.setCnsPrjManagerFax("");
		}
		if (!(addContactForm.getTeamLeadPoc().equals("0"))) {

			teamLeadDetail = AddContactdao.getPocDetails(
					addContactForm.getTeamLeadPoc(),
					getDataSource(request, "ilexnewDB"));

			addContactForm.setTeamLeadPoc(addContactForm.getTeamLeadPoc());
			addContactForm.setTeamLeadPhone1(teamLeadDetail[0]);
			addContactForm.setTeamLeadPhone2(teamLeadDetail[1]);
			addContactForm.setTeamLeadMail1(teamLeadDetail[2]);
			addContactForm.setTeamLeadMail2(teamLeadDetail[3]);
			addContactForm.setTeamLeadFax(teamLeadDetail[4]);

		} else {
			addContactForm.setTeamLeadPoc("0");
			addContactForm.setTeamLeadPhone1("");
			addContactForm.setTeamLeadPhone2("");
			addContactForm.setTeamLeadMail1("");
			addContactForm.setTeamLeadMail2("");
			addContactForm.setTeamLeadFax("");
		}

		if (addContactForm.getFromPage().equals("Appendix")) {

			addContactForm.setName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), typeid));
			addContactForm.setMsaId(IMdao.getMSAId(typeid,
					getDataSource(request, "ilexnewDB")));
			addContactForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					addContactForm.getMsaId()));
			ownerType = "appendix";

			if (request.getParameter("home") != null) // This is called when
														// reset button is
														// pressed from the view
														// selector.
			{
				addContactForm.setAppendixSelectedStatus(null);
				addContactForm.setAppendixSelectedOwners(null);
				addContactForm.setPrevSelectedStatus("");
				addContactForm.setPrevSelectedOwner("");
				addContactForm.setAppendixOtherCheck(null);
				addContactForm.setMonthWeekCheck(null);
				session.removeAttribute("appendixSelectedOwners");
				session.removeAttribute("appendixSelectedStatus");
				session.removeAttribute("appendixTempOwner");
				session.removeAttribute("appendixTempStatus");
				session.removeAttribute("appendixOtherCheck");
				session.removeAttribute("monthAppendix");
				session.removeAttribute("weekAppendix");
				session.removeAttribute("appSelectedOwners");
				session.removeAttribute("appSelectedStatus");
				session.removeAttribute("sessionMSAId");

			}

			// These two parameter month/week are checked are kept in session. (
			// View Images)
			if (request.getParameter("month") != null) {
				session.setAttribute("monthAppendix",
						request.getParameter("month").toString());

				session.removeAttribute("weekAppendix");
				addContactForm.setMonthWeekCheck("month");
			}

			if (request.getParameter("week") != null) {
				session.setAttribute("weekAppendix",
						request.getParameter("week").toString());

				session.removeAttribute("monthAppendix");
				addContactForm.setMonthWeekCheck("week");
			}

			if (addContactForm.getAppendixOtherCheck() != null) // OtherCheck
																// box status is
																// kept in
																// session.
				session.setAttribute("appendixOtherCheck", addContactForm
						.getAppendixOtherCheck().toString());

			// When Status Check Boxes are Clicked :::::::::::::::::
			if (addContactForm.getAppendixSelectedStatus() != null) {
				for (int i = 0; i < addContactForm.getAppendixSelectedStatus().length; i++) {
					selectedStatus = selectedStatus + "," + "'"
							+ addContactForm.getAppendixSelectedStatus(i) + "'";
					tempStatus = tempStatus + ","
							+ addContactForm.getAppendixSelectedStatus(i);
				}

				selectedStatus = selectedStatus.substring(1,
						selectedStatus.length());
				selectedStatus = "(" + selectedStatus + ")";
			}

			// When Owner Check Boxes are Clicked :::::::::::::::::
			if (addContactForm.getAppendixSelectedOwners() != null) {
				for (int j = 0; j < addContactForm.getAppendixSelectedOwners().length; j++) {
					selectedOwner = selectedOwner + ","
							+ addContactForm.getAppendixSelectedOwners(j);
					tempOwner = tempOwner + ","
							+ addContactForm.getAppendixSelectedOwners(j);
				}
				selectedOwner = selectedOwner.substring(1,
						selectedOwner.length());
				if (selectedOwner.equals("0")) {
					chkOtherOwner = true;
					selectedOwner = "%";
				}
				selectedOwner = "(" + selectedOwner + ")";
			}

			if (addContactForm.getGo() != null && addContactForm.getGo() != "") {
				session.setAttribute("appendixSelectedOwners", selectedOwner);
				session.setAttribute("appSelectedOwners", selectedOwner);
				session.setAttribute("appendixTempOwner", tempOwner);
				session.setAttribute("appendixSelectedStatus", selectedStatus);
				session.setAttribute("appSelectedStatus", selectedStatus);
				session.setAttribute("appendixTempStatus", tempStatus);

				if (addContactForm.getMonthWeekCheck() != null) {
					if (addContactForm.getMonthWeekCheck().equals("week")) {
						session.removeAttribute("monthAppendix");
						session.setAttribute("weekAppendix", "0");

					}
					if (addContactForm.getMonthWeekCheck().equals("month")) {
						session.removeAttribute("weekAppendix");
						session.setAttribute("monthAppendix", "0");

					}
					if (addContactForm.getMonthWeekCheck().equals("clear")) {
						session.removeAttribute("monthAppendix");
						session.removeAttribute("weekAppendix");
					}

				}
			} else {
				if (session.getAttribute("appendixSelectedOwners") != null) {
					selectedOwner = session.getAttribute(
							"appendixSelectedOwners").toString();
					addContactForm.setAppendixSelectedOwners(session
							.getAttribute("appendixTempOwner").toString()
							.split(","));
				}
				if (session.getAttribute("appendixSelectedStatus") != null) {
					selectedStatus = session.getAttribute(
							"appendixSelectedStatus").toString();
					addContactForm.setAppendixSelectedStatus(session
							.getAttribute("appendixTempStatus").toString()
							.split(","));
				}
				if (session.getAttribute("appendixOtherCheck") != null)
					addContactForm.setAppendixOtherCheck(session.getAttribute(
							"appendixOtherCheck").toString());

				if (session.getAttribute("monthAppendix") != null) {
					session.removeAttribute("weekAppendix");
					addContactForm.setMonthWeekCheck("month");
				}
				if (session.getAttribute("weekAppendix") != null) {
					session.removeAttribute("monthAppendix");
					addContactForm.setMonthWeekCheck("week");
				}

			}
			if (!chkOtherOwner) {
				addContactForm.setPrevSelectedStatus(selectedStatus);
				addContactForm.setPrevSelectedOwner(selectedOwner);
			}
			ownerList = MSAdao.getOwnerList(userid,
					addContactForm.getAppendixOtherCheck(), ownerType,
					addContactForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			statusList = MSAdao.getStatusList("pm_appendix",
					getDataSource(request, "ilexnewDB"));

			if (ownerList.size() > 0)
				ownerListSize = ownerList.size();

			request.setAttribute("clickShow", request.getParameter("clickShow"));
			request.setAttribute("statuslist", statusList);
			request.setAttribute("ownerlist", ownerList);
			request.setAttribute("ownerListSize", ownerListSize + "");

			String appendixtype = Appendixdao.getAppendixPrType(
					addContactForm.getMsaId(), typeid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("changestatusflag", "true");
			request.setAttribute("MSA_Id", addContactForm.getMsaId());
			request.setAttribute("Appendix_Id", typeid);
			request.setAttribute("appendixType", appendixtype);

			String appendixStatus = com.mind.dao.PM.Appendixdao
					.getAppendixStatus(addContactForm.getTypeid(),
							addContactForm.getMsaId(),
							getDataSource(request, "ilexnewDB"));
			String addendumlist = com.mind.dao.PM.Appendixdao
					.getAddendumsIdName(addContactForm.getTypeid(),
							getDataSource(request, "ilexnewDB"));
			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(addContactForm.getTypeid(),
							getDataSource(request, "ilexnewDB"));
			String list1 = Menu.getStatus("pm_appendix",
					appendixStatus.charAt(0), addContactForm.getMsaId(),
					addContactForm.getTypeid(), "", "", userid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixStatusList", list1);
			request.setAttribute("appendixStatus", appendixStatus);
			request.setAttribute("latestaddendumid", addendumid);
			request.setAttribute("addendumlist", addendumlist);
		}

		// for AppendixDashboard Menu:start

		if (addContactForm.getFromPage().equals("AppendixDashBoard")
				|| addContactForm.getFromPage().equals("NetMedX")) {
			/* Start : Appendix Dashboard View Selector Code */

			if (addContactForm.getJobOwnerOtherCheck() != null
					&& !addContactForm.getJobOwnerOtherCheck().equals("")) {
				session.setAttribute("jobOwnerOtherCheck",
						addContactForm.getJobOwnerOtherCheck());
			}

			if (request.getParameter("opendiv") != null) {
				request.setAttribute("opendiv", request.getParameter("opendiv"));
			}
			if (request.getParameter("resetList") != null
					&& request.getParameter("resetList").equals("something")) {
				WebUtil.setDefaultAppendixDashboardAttribute(session);
				request.setAttribute("type", "AppendixViewSelector");
				request.setAttribute("appendixId", typeid);
				request.setAttribute("msaId", addContactForm.getMsaId());
				request.setAttribute("viewjobtype",
						addContactForm.getViewjobtype());
				request.setAttribute("ownerId", addContactForm.getOwnerId());

				return mapping.findForward("temppage");

			}

			if (addContactForm.getJobSelectedStatus() != null) {
				for (int i = 0; i < addContactForm.getJobSelectedStatus().length; i++) {
					jobSelectedStatus = jobSelectedStatus + "," + "'"
							+ addContactForm.getJobSelectedStatus(i) + "'";
					jobTempStatus = jobTempStatus + ","
							+ addContactForm.getJobSelectedStatus(i);
				}

				jobSelectedStatus = jobSelectedStatus.substring(1,
						jobSelectedStatus.length());
				jobSelectedStatus = "(" + jobSelectedStatus + ")";
			}

			if (addContactForm.getJobSelectedOwners() != null) {
				for (int j = 0; j < addContactForm.getJobSelectedOwners().length; j++) {
					jobSelectedOwner = jobSelectedOwner + ","
							+ addContactForm.getJobSelectedOwners(j);
					jobTempOwner = jobTempOwner + ","
							+ addContactForm.getJobSelectedOwners(j);
				}
				jobSelectedOwner = jobSelectedOwner.substring(1,
						jobSelectedOwner.length());
				if (jobSelectedOwner.equals("0")) {
					jobSelectedOwner = "%";
				}
				jobSelectedOwner = "(" + jobSelectedOwner + ")";
			}

			if (request.getParameter("showList") != null
					&& request.getParameter("showList").equals("something")) {

				session.setAttribute("jobSelectedOwners", jobSelectedOwner);
				session.setAttribute("jobTempOwner", jobTempOwner);
				session.setAttribute("jobSelectedStatus", jobSelectedStatus);
				session.setAttribute("jobTempStatus", jobTempStatus);
				session.setAttribute("timeFrame",
						addContactForm.getJobMonthWeekCheck());
				session.setAttribute("jobOwnerOtherCheck",
						addContactForm.getJobOwnerOtherCheck());

				request.setAttribute("type", "AppendixViewSelector");
				request.setAttribute("appendixId", typeid);
				request.setAttribute("msaId", addContactForm.getMsaId());
				request.setAttribute("viewjobtype",
						addContactForm.getViewjobtype());
				request.setAttribute("ownerId", addContactForm.getOwnerId());

				return mapping.findForward("temppage");

			} else {
				if (session.getAttribute("jobSelectedOwners") != null) {
					jobSelectedOwner = session
							.getAttribute("jobSelectedOwners").toString();
					addContactForm
							.setJobSelectedOwners(session
									.getAttribute("jobTempOwner").toString()
									.split(","));
				}
				if (session.getAttribute("jobSelectedStatus") != null) {
					jobSelectedStatus = session.getAttribute(
							"jobSelectedStatus").toString();
					addContactForm.setJobSelectedStatus(session
							.getAttribute("jobTempStatus").toString()
							.split(","));
				}
				if (session.getAttribute("jobOwnerOtherCheck") != null) {
					addContactForm.setJobOwnerOtherCheck(session.getAttribute(
							"jobOwnerOtherCheck").toString());
				}
				if (session.getAttribute("timeFrame") != null) {
					addContactForm.setJobMonthWeekCheck(session.getAttribute(
							"timeFrame").toString());
				}
			}

			jobOwnerList = MSAdao.getOwnerList(userid,
					addContactForm.getJobOwnerOtherCheck(), "prj_job_new",
					typeid, getDataSource(request, "ilexnewDB"));
			jobStatusList = MSAdao.getStatusList("prj_job_new",
					getDataSource(request, "ilexnewDB"));

			if (jobOwnerList.size() > 0)
				jobOwnerListSize = jobOwnerList.size();

			request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("jobOwnerList", jobOwnerList);

			String contractDocoumentList = com.mind.dao.PM.Appendixdao
					.getcontractDocoumentMenu(typeid,
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("contractDocMenu", contractDocoumentList);
			String appendixcurrentstatus = Appendixdao.getCurrentstatus(typeid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
			request.setAttribute("viewjobtype", addContactForm.getViewjobtype());
			request.setAttribute("ownerId", addContactForm.getOwnerId());
			request.setAttribute("appendixid", typeid);
			/* End : Appendix Dashboard View Selector Code */
		}
		// for AppendixDashboard Menu:end

		addContactForm.setRefresh("false");
		addContactForm.setFunction("add");
		request.setAttribute("typeid", typeid);
		request.setAttribute("type", type);
		request.setAttribute("viewflag", flag + "");
		request.setAttribute("dynamiccombo", dcprm);
		forward = mapping.findForward("success");

		return (forward);
	}

}
