package com.mind.actions.PRM;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.pm.Appendix;
import com.mind.bean.prm.NetMedXDashboardBean;
import com.mind.common.dao.AddComment;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.NetMedXDashboarddao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.formbean.PRM.NetMedXDashboardForm;
import com.mind.util.WebUtil;

public class NetMedXDashboardOldAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		NetMedXDashboardForm netmedxForm = (NetMedXDashboardForm) form;
		NetMedXDashboardBean netmedxBean = new NetMedXDashboardBean();
		ArrayList dispatchavgsummarylist = new ArrayList();
		ArrayList list = new ArrayList();
		ArrayList trendAnalysisList = new ArrayList();
		ArrayList compDispatchList = new ArrayList();
		ArrayList currDispatchSummaryList = new ArrayList();
		ArrayList totalDispatchSummaryList = new ArrayList();
		String sortqueryclause = "";
		String tempjobid = "";
		String[] pocdetails = new String[12];
		String[] custcnspocdetails = new String[5];
		String appendixcurrentstatus = "";

		/*** Code added for the View Selecotr ***/
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		double avg_tempvalue = 0.0;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		String userId = null;
		// String status = null;
		String tempCat = "";
		String[] tempList = null;
		ArrayList initialStateCategory = new ArrayList();
		ArrayList jobOwnerList = new ArrayList();
		Map<String, Object> sessionMap;
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");
		if (netmedxForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(loginuserid,
					"Manage Project", "View Appendix Dashboard",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}

		/* Page Security End */
		if (request.getParameter("appendixid") != null) {
			netmedxForm.setAppendixid(request.getParameter("appendixid"));
		}

		if (request.getAttribute("appendixid") != null)
			netmedxForm.setAppendixid(request.getAttribute("appendixid")
					.toString());

		netmedxForm.setAppendixname(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				netmedxForm.getAppendixid()));

		// set msa id and name in form bean
		netmedxForm.setMsaid(IMdao.getMSAId(netmedxForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")));
		netmedxForm.setMsaname(Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"), netmedxForm.getMsaid()));

		// set job filter type start
		if (request.getParameter("viewjobtype") != null) {
			netmedxForm
					.setViewjobtype("" + request.getParameter("viewjobtype"));
		}

		if (netmedxForm.getViewjobtype() != null) {
		} else {
			netmedxForm.setViewjobtype("All");
		}

		if (netmedxForm.getViewjobtype().equals("")) {
			netmedxForm.setViewjobtype("All");
		}

		// set job filter type end
		if (request.getParameter("nettype") != null) {
			netmedxForm.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getAttribute("nettype") != null) {
			netmedxForm.setNettype("" + request.getAttribute("nettype"));
		}

		if (request.getParameter("from_type") != null) {
			netmedxForm.setFrom_type("" + request.getParameter("from_type"));
		}

		if (netmedxForm.getAppendixid().equals("-1")
				|| netmedxForm.getAppendixid().equals("")) {
			netmedxForm.setAppendixid("-1");
			netmedxForm.setAppendixname("All");
			netmedxForm.setMsaname("All");
		}
		// NetMedX View Selector:Start
		if (netmedxForm.getJobOwnerOtherCheck() != null
				&& !netmedxForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					netmedxForm.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);

		}

		if (netmedxForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < netmedxForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ netmedxForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ netmedxForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (netmedxForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < netmedxForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ netmedxForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ netmedxForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {
			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					netmedxForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					netmedxForm.getJobOwnerOtherCheck());

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				netmedxForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				netmedxForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				netmedxForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				netmedxForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				netmedxForm.getJobOwnerOtherCheck(), "prj_job_new",
				netmedxForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		// NetMedX View Selector:End

		// Don't set in case of ticket search or dispatch view
		if ((!netmedxForm.getAppendixid().equals("-1"))
				|| (!netmedxForm.getViewjobtype().equals("jobSearch"))) {
			appendixcurrentstatus = com.mind.dao.PRM.Appendixdao
					.getCurrentstatus(netmedxForm.getAppendixid(),
							getDataSource(request, "ilexnewDB"));

			request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
			netmedxForm.setStatus(appendixcurrentstatus);

			Appendix appendix = new Appendix();
			appendix = com.mind.dao.PRM.Appendixdao.getAppendix("%",
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			if (netmedxForm.getAppendixid() != null) {
				netmedxForm.setStatusdesc(appendix.getStatus());
				netmedxForm.setAppendix_type(appendix.getAppendixtype());
				netmedxForm.setPending_tickets(appendix.getPendingWebTicket());
				netmedxForm.setToBeSchedule(appendix.getToBeSchedule());
				netmedxForm.setScheduled(appendix.getSchedule());
				netmedxForm.setOverdue(appendix.getOverdue());
			}
			// get Contact Inforamation
			pocdetails = com.mind.dao.PRM.Appendixdao.getAppendixpocdetails(
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			netmedxForm.setAppendixcnspoc(pocdetails[0]);
			netmedxForm.setAppendixcnspocphone(pocdetails[1]);
			netmedxForm.setAppendixbusinesspoc(pocdetails[2]);
			netmedxForm.setAppendixbusinesspocphone(pocdetails[3]);
			netmedxForm.setAppendixcontractpoc(pocdetails[4]);
			netmedxForm.setAppendixcontractpocphone(pocdetails[5]);
			netmedxForm.setAppendixbillingpoc(pocdetails[6]);
			netmedxForm.setAppendixbillingpocphone(pocdetails[7]);

			custcnspocdetails = com.mind.dao.PRM.Appendixdao
					.getAppendixcustcnspocdetails(netmedxForm.getAppendixid(),
							getDataSource(request, "ilexnewDB"));
			netmedxForm.setCnsprojectmgr(custcnspocdetails[0]);
			netmedxForm.setCnsprojectmgrphone(custcnspocdetails[1]);
			netmedxForm.setCustprojectmgr(custcnspocdetails[2]);
			netmedxForm.setCustprojectmgrphone(custcnspocdetails[3]);
			netmedxForm.setCnsprojectmgrId(custcnspocdetails[4]);
		}

		// get ticket/job list
		if (request.getAttribute("querystring") != null) {
			sortqueryclause = (String) request.getAttribute("querystring");
		}

		// Start :Added By Amit For Holdong Sort
		if (session != null) {
			if (session.getAttribute("HoldingSortNetMedx") != null) {
				sortqueryclause = (String) session
						.getAttribute("HoldingSortNetMedx");
			}
		}

		if (request.getParameter("ownerId") != null) {
			netmedxForm.setOwnerId(request.getParameter("ownerId"));
		} else {
			netmedxForm.setOwnerId("All");
		}

		/* Set filters in session */
		if (request.getParameter("changeFilter") != null) {
			if (request.getParameter("changeFilter").equals("Y")) {
				session.setAttribute("ownerId", netmedxForm.getOwnerId());
				session.setAttribute("viewjobtype",
						netmedxForm.getViewjobtype());
			}
		}

		/* Get filters from session */
		if (session.getAttribute("ownerId") != null)
			userId = (String) session.getAttribute("ownerId");

		// if(session.getAttribute("viewjobtype") != null)
		// status = (String)session.getAttribute("viewjobtype");

		initialStateCategory = NetMedXDashboarddao
				.getStateCategoryList(getDataSource(request, "ilexnewDB"));

		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempCat = ((NetMedXDashboardBean) initialStateCategory.get(i))
						.getSiteCountryId();
				((NetMedXDashboardBean) initialStateCategory.get(i))
						.setTempList(SiteManagedao
								.getCategoryWiseStateList(tempCat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}
		request.setAttribute("initialStateCategory", initialStateCategory);

		DynamicComboCM dcJobOwnerList = new DynamicComboCM();
		dcJobOwnerList.setFirstlevelcatglist(com.mind.dao.PRM.Appendixdao
				.getjobOwnerList(netmedxForm.getAppendixid(),
						getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcJobOwnerList", dcJobOwnerList);

		if (netmedxForm.getViewjobtype().equals("jobSearch")) {
			if (request.getParameter("fromLink") != null) {
				list = null;
				request.setAttribute("ticketjoblist", list);
				request.setAttribute("listsize", 0 + "");

				/* Set all search parameters null in session */
				session.setAttribute("job_search_name", null);
				session.setAttribute("job_search_site_number", null);
				session.setAttribute("job_search_city", null);
				session.setAttribute("job_search_state", null);
				session.setAttribute("job_search_owner", null);
				session.setAttribute("job_search_date_from", null);
				session.setAttribute("job_search_date_to", null);
				session.setAttribute("dateOnSite", null);
				session.setAttribute("dateOffSite", null);
				session.setAttribute("dateComplete", null);
				session.setAttribute("dateClosed", null);
				session.setAttribute("job_search", null);
			}

			if (netmedxForm.getJob_search() != null
					&& !netmedxForm.getJob_search().equals("")) {

				request.setAttribute("searchPage", "TicketSearch");
				session.setAttribute("job_search_name",
						netmedxForm.getJob_search_name());
				session.setAttribute("job_search_site_number",
						netmedxForm.getJob_search_site_number());
				session.setAttribute("job_search_city",
						netmedxForm.getJob_search_city());
				session.setAttribute("job_search_state",
						netmedxForm.getJob_search_state());
				session.setAttribute("job_search_owner",
						netmedxForm.getJob_search_owner());
				session.setAttribute("job_search_date_from",
						netmedxForm.getJob_search_date_from());
				session.setAttribute("job_search_date_to",
						netmedxForm.getJob_search_date_to());
				session.setAttribute("dateOnSite", netmedxForm.getDateOnSite());
				session.setAttribute("dateOffSite",
						netmedxForm.getDateOffSite());
				session.setAttribute("dateComplete",
						netmedxForm.getDateComplete());
				session.setAttribute("dateClosed", netmedxForm.getDateClosed());
				session.setAttribute("job_search", netmedxForm.getJob_search());
			}

			else {

				if (session.getAttribute("job_search_name") != null)
					netmedxForm.setJob_search_name(session.getAttribute(
							"job_search_name").toString());

				if (session.getAttribute("job_search_site_number") != null)
					netmedxForm.setJob_search_site_number(session.getAttribute(
							"job_search_site_number").toString());

				if (session.getAttribute("job_search_city") != null)
					netmedxForm.setJob_search_city(session.getAttribute(
							"job_search_city").toString());

				if (session.getAttribute("job_search_state") != null)
					netmedxForm.setJob_search_state(session.getAttribute(
							"job_search_state").toString());

				if (session.getAttribute("job_search_owner") != null)
					netmedxForm.setJob_search_owner(session.getAttribute(
							"job_search_owner").toString());

				if (session.getAttribute("job_search_date_from") != null)
					netmedxForm.setJob_search_date_from(session.getAttribute(
							"job_search_date_from").toString());

				if (session.getAttribute("job_search_date_to") != null)
					netmedxForm.setJob_search_date_to(session.getAttribute(
							"job_search_date_to").toString());

				if (session.getAttribute("dateOnSite") != null)
					netmedxForm.setDateOnSite(session
							.getAttribute("dateOnSite").toString());

				if (session.getAttribute("dateOffSite") != null)
					netmedxForm.setDateOffSite(session.getAttribute(
							"dateOffSite").toString());

				if (session.getAttribute("dateComplete") != null)
					netmedxForm.setDateComplete(session.getAttribute(
							"dateComplete").toString());

				if (session.getAttribute("dateClosed") != null)
					netmedxForm.setDateClosed(session
							.getAttribute("dateClosed").toString());

				if (session.getAttribute("job_search") != null)
					netmedxForm.setJob_search(session
							.getAttribute("job_search").toString());
			}
			if (request.getParameter("fromLink") != null) {
				list = null;
				request.setAttribute("ticketjoblist", list);
				request.setAttribute("listsize", "0");
			} else {
				sessionMap = WebUtil.copySessionToAttributeMap(session);
				list = NetMedXDashboarddao.getJobSearchdetaillist(
						netmedxForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"), sortqueryclause,
						sessionMap);
				request.setAttribute("ticketjoblist", list);
				request.setAttribute("listsize", list.size() + "");
			}

		} else {
			list = NetMedXDashboarddao.getNetMedXJobtabular(
					netmedxForm.getAppendixid(), sortqueryclause,
					jobSelectedStatus, userId,
					getDataSource(request, "ilexnewDB"), jobSelectedOwner,
					netmedxForm.getJobMonthWeekCheck());
			request.setAttribute("ticketjoblist", list);
			request.setAttribute("listsize", list.size() + "");
		}

		if (netmedxForm.getAppendixid().equals("-1"))
			netmedxForm.setCustref("");
		else
			netmedxForm.setCustref(AddComment.getCustomerReference(
					netmedxForm.getAppendixid(), "Appendix",
					getDataSource(request, "ilexnewDB")));

		// Don't set in case of ticket search
		if (!netmedxForm.getViewjobtype().equals("jobSearch")) {
			// get trend analysis list
			trendAnalysisList = NetMedXDashboarddao
					.getNetMedXDispatchTrendAnalysis(
							netmedxForm.getAppendixid(),
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("trendanalysislist", trendAnalysisList);

			// get Complete Dispatched Ticket Summary
			compDispatchList = NetMedXDashboarddao.getCompleteDispatchSummary(
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("compdispatchlist", compDispatchList);

			// get Dispatch Financial Summary
			BeanUtils.copyProperties(netmedxBean, netmedxForm);
			NetMedXDashboarddao.getNetMedXDispatcmFinancialSummary(netmedxBean,
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			NetMedXDashboarddao.getNetMedXDispattotalFinancialSummary(
					netmedxBean, netmedxBean.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(netmedxForm, netmedxBean);

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_request())) / 12;
			netmedxForm.setDfs_avg_mnth_total_request(dfcur
					.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_hd())) / 12;
			netmedxForm.setDfs_avg_mnth_total_hd(dfcur.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_comp())) / 12;
			netmedxForm.setDfs_avg_mnth_total_comp(dfcur.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_inwork())) / 12;
			netmedxForm.setDfs_avg_mnth_total_inwork(dfcur
					.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_cancelled())) / 12;
			netmedxForm.setDfs_avg_mnth_total_cancelled(dfcur
					.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_inwork())) / 12;
			netmedxForm.setDfs_avg_mnth_total_inwork(dfcur
					.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_extprice())) / 12;
			netmedxForm.setDfs_avg_mnth_total_extprice(dfcur
					.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_actcost())) / 12;
			netmedxForm.setDfs_avg_mnth_total_actcost(dfcur
					.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_avgp())) / 12;
			netmedxForm.setDfs_avg_mnth_total_avgp(dfcurmargin
					.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_avgcost())) / 12;
			netmedxForm.setDfs_avg_mnth_total_avgcost(dfcur
					.format(avg_tempvalue));

			avg_tempvalue = (Double.parseDouble(netmedxForm
					.getDfs_ttm_total_avgcharge())) / 12;
			netmedxForm.setDfs_avg_mnth_total_avgcharge(dfcur
					.format(avg_tempvalue));

			netmedxForm.setDfs_avg_mnth_total_avgtimeonsite(NetMedXDashboarddao
					.getAvgDateDiff(netmedxForm
							.getDfs_ttm_total_avgtimeonsite()));
			netmedxForm
					.setDfs_avg_mnth_total_avgtimeonarrive(NetMedXDashboarddao
							.getAvgDateDiff(netmedxForm
									.getDfs_ttm_total_avgtimeonarrive()));
		}
		request.setAttribute("appendixid", netmedxForm.getAppendixid());
		request.setAttribute("nettype", netmedxForm.getNettype());
		request.setAttribute("viewjobtype", netmedxForm.getViewjobtype());
		request.setAttribute("ownerId", netmedxForm.getOwnerId());
		request.setAttribute("loginUserId", loginuserid);

		return (mapping.findForward("success"));
	}

}
