package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.OnlineBiddao;
import com.mind.formbean.PRM.ProjectOnlineBidForm;
import com.mind.util.WebUtil;

public class OnlineBidAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ProjectOnlineBidForm projectOnlineBid = (ProjectOnlineBidForm) form;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Project",
				"Update Project Onlide Bid Status",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		ArrayList projectOnlineBidList = new ArrayList();

		if (request.getParameter("appendixId") != null) {
			projectOnlineBid.setAppendixid(request.getParameter("appendixId"));
			projectOnlineBid.setMsaId(IMdao.getMSAId(
					projectOnlineBid.getAppendixid(),
					getDataSource(request, "ilexnewDB")));
		}

		/* Variable for Appendix Dashboard View Selector: Start */
		ArrayList jobOwnerList = new ArrayList(); // - for list of Job Owner
		ArrayList jobStatusList = new ArrayList(); // - list of job status
		String jobSelectedStatus = ""; // - selected status
		String jobSelectedOwner = ""; // - selected owner
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		/* Variable for Appendix Dashboard View Selector: End */

		if (request.getParameter("ownerId") != null) // - for menu
		{
			projectOnlineBid.setOwnerId(request.getParameter("ownerId"));
		}
		if (request.getParameter("viewjobtype") != null) // - for menu
		{
			projectOnlineBid
					.setViewjobtype(request.getParameter("viewjobtype"));
		}
		if (request.getParameter("fromPage") != null) {
			projectOnlineBid.setFromPage(request.getParameter("fromPage"));
		}

		/* For Menu & View Selector: Start */
		/* Start : Appendix Dashboard View Selector Code */

		if (projectOnlineBid.getJobOwnerOtherCheck() != null
				&& !projectOnlineBid.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					projectOnlineBid.getJobOwnerOtherCheck());
		}
		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", projectOnlineBid.getAppendixid());
			request.setAttribute("msaId", projectOnlineBid.getMsaId());
			return mapping.findForward("temppage");
		}
		if (projectOnlineBid.getJobSelectedStatus() != null) {
			for (int i = 0; i < projectOnlineBid.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ projectOnlineBid.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ projectOnlineBid.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (projectOnlineBid.getJobSelectedOwners() != null) {
			for (int j = 0; j < projectOnlineBid.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ projectOnlineBid.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ projectOnlineBid.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {
			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					projectOnlineBid.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					projectOnlineBid.getJobOwnerOtherCheck());
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", projectOnlineBid.getAppendixid());
			request.setAttribute("msaId", projectOnlineBid.getMsaId());

			return mapping.findForward("temppage"); // - /common/Tempupload.jsp
		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				projectOnlineBid.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				projectOnlineBid.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				projectOnlineBid.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				projectOnlineBid.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}
		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				projectOnlineBid.getJobOwnerOtherCheck(), "prj_job_new",
				projectOnlineBid.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));
		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(projectOnlineBid.getAppendixid(),
						getDataSource(request, "ilexnewDB")); // - for addendum
																// section of
																// menu
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				projectOnlineBid.getAppendixid(),
				getDataSource(request, "ilexnewDB")); // - Appendix current
														// status

		/* Set Parameter for menu and view selector */
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("contractDocMenu", contractDocoumentList);
		request.setAttribute("msaId", projectOnlineBid.getMsaId());
		request.setAttribute("viewjobtype", projectOnlineBid.getViewjobtype());
		request.setAttribute("ownerId", projectOnlineBid.getOwnerId());
		request.setAttribute("appendixid", projectOnlineBid.getAppendixid());
		request.setAttribute("fromPage", request.getParameter("fromPage"));

		/* For Menu & View Selector: End */

		if (projectOnlineBid.getEnableBid() != null
				|| projectOnlineBid.getDisableBid() != null) {
			int setBidValue = -1;
			int retBidFlagStatus = -1;

			if (projectOnlineBid.getEnableBid() != null) {
				setBidValue = 1;
			} else if (projectOnlineBid.getDisableBid() != null) {
				setBidValue = 0;
			}

			retBidFlagStatus = OnlineBiddao.setProjectOnlineBidFlag(
					projectOnlineBid.getAppendixid(), setBidValue,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("retBidFlagStatus", retBidFlagStatus);
		}

		projectOnlineBid.setStatus(String.valueOf(OnlineBiddao
				.getProjectStatusForOnlineBid(projectOnlineBid.getAppendixid(),
						getDataSource(request, "ilexnewDB"))));
		projectOnlineBidList = OnlineBiddao.getProjectOnlineBidInfo(
				projectOnlineBid.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("projectOnlineBidInfo", projectOnlineBidList);
		request.setAttribute("bidInfoSize",
				new Integer(projectOnlineBidList.size()));

		return mapping.findForward("success");
	}
}
