package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.prm.ViewTicketSummaryBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.NewWebTicketdao;
import com.mind.formbean.PRM.ViewTicketSummaryForm;

public class ViewTicketSummaryAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception

	{
		ViewTicketSummaryForm viewTicketSummaryForm = (ViewTicketSummaryForm) form;
		ViewTicketSummaryBean viewTicketSummaryBean = new ViewTicketSummaryBean();
		String ticket_number = null;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");

		if (!Authenticationdao.getPageSecurity(userid, "Manage Project",
				"View Web Tickets", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Page Security End */

		if (request.getParameter("ticket_number") != null) {
			ticket_number = request.getParameter("ticket_number");
		}
		BeanUtils.copyProperties(viewTicketSummaryBean, viewTicketSummaryForm);
		NewWebTicketdao.getWebTicketSummary(ticket_number,
				viewTicketSummaryBean, getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(viewTicketSummaryForm, viewTicketSummaryBean);
		return (mapping.findForward("success"));
	}
}
