package com.mind.actions.PRM;

import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.prm.CustomerRequireBean;
import com.mind.business.PRM.CustomerRequiredDataLogic;
import com.mind.common.LabelValue;
import com.mind.common.MultipartRequestWrapper;
import com.mind.dao.PRM.CustomerRequiredDAO;
import com.mind.formbean.PRM.CustomerRequireForm;

/**
 * The Class CustomerRequireDataAction.
 */
public class CustomerRequireDataAction extends
		com.mind.common.IlexDispatchAction {

	/** The Constant logger. */
	private static final Logger logger = Logger
			.getLogger(CustomerRequireDataAction.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		CustomerRequireForm customerRequireForm = (CustomerRequireForm) form;
		CustomerRequireBean customerRequireBean = new CustomerRequireBean();
		CustomerRequiredDataLogic customerRequiredDataLogic = new CustomerRequiredDataLogic();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		DataSource ds = getDataSource(request, "ilexnewDB");
		customerRequireBean = customerRequiredDataLogic.getCustomerRequireData(
				request.getParameter("jobid"), ds);
		BeanUtils.copyProperties(customerRequireForm, customerRequireBean);
		if (customerRequireBean.getFirstVisitQuestion() == null) {
			customerRequireForm.setFirstVisitQuestion(null);
		}
		if (customerRequireBean.getAddSiteVisitQuestion() == null) {
			customerRequireForm.setAddSiteVisitQuestion(null);
		}
		if (StringUtils.isNotBlank(request.getParameter("jobid"))) {
			customerRequireForm.setJobId(request.getParameter("jobid"));
		}
		if (StringUtils.isNotBlank(request.getParameter("jobStatus"))) {
			customerRequireForm.setJobStatus(request.getParameter("jobStatus"));
		}
		if (StringUtils.isNotBlank(request.getParameter("appendixId"))) {
			customerRequireForm.setAppendixId(request
					.getParameter("appendixId"));
		}
		if (StringUtils.isNotBlank(request.getParameter("msaId"))) {
			customerRequireForm.setAppendixId(request.getParameter("msaId"));
		}
		List<LabelValue> contingentFailVisitList = CustomerRequiredDAO
				.getFailedVisitCategoryList("Contingent", ds);
		List<LabelValue> clientFailVisitList = CustomerRequiredDAO
				.getFailedVisitCategoryList("Client", ds);
		List<LabelValue> lecFailVisitList = CustomerRequiredDAO
				.getFailedVisitCategoryList("LEC", ds);
		List<LabelValue> otherPartyFailVisitList = CustomerRequiredDAO
				.getFailedVisitCategoryList("Other3rdParty", ds);
		customerRequireForm.setContingentList(contingentFailVisitList);
		customerRequireForm.setClientList(clientFailVisitList);
		customerRequireForm.setLecList(lecFailVisitList);
		customerRequireForm.setOtherPartyList(otherPartyFailVisitList);
		return mapping.findForward("success");

	}

	/**
	 * Insert customer required data into database.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @throws Exception
	 *             the exception
	 */
	public void insert(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		CustomerRequireForm customerRequireForm = (CustomerRequireForm) form;
		CustomerRequireBean customerRequireBean = new CustomerRequireBean();
		HttpSession session = request.getSession(true);
		String loginUserid = (String) session.getAttribute("userid");
		BeanUtils.copyProperties(customerRequireBean, customerRequireForm);
		CustomerRequiredDataLogic customerRequiredDataLogic = new CustomerRequiredDataLogic();
		DataSource ds = getDataSource(request, "ilexnewDB");
		String success = customerRequiredDataLogic.insertCustomerRequireData(
				customerRequireBean, loginUserid, ds);
		responce.getOutputStream().write(success.getBytes());
	}

	/**
	 * View customer required data.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		CustomerRequireForm customerRequireForm = (CustomerRequireForm) form;
		CustomerRequireBean customerRequireBean = new CustomerRequireBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (StringUtils.isNotBlank(request.getParameter("jobId"))) {
			customerRequireForm.setJobId(request.getParameter("jobId"));
		}
		DataSource ds = getDataSource(request, "ilexnewDB");
		CustomerRequiredDataLogic customerRequiredDataLogic = new CustomerRequiredDataLogic();
		customerRequireBean = customerRequiredDataLogic.getCustomerRequireData(
				customerRequireForm.getJobId(), ds);
		BeanUtils.copyProperties(customerRequireForm, customerRequireBean);
		RequestDispatcher dispatch = request
				.getRequestDispatcher("/PRM/Job/viewCustomerRequiredData.jsp");

		try {
			if (request.getClass().getName()
					.equals("org.apache.struts.upload.MultipartRequestWrapper")) {
				logger.debug("Casting MultiparRequestWrapper to custom class");
				request = new MultipartRequestWrapper(request);
			}
			dispatch.include(request, responce);
		} catch (Exception e) {
			logger.warn(
					"view(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored",
					e);

		}
		return null;
	}
}
