package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PMT.EditMasterlDao;
import com.mind.dao.PRM.Appendixdao;

public class SiteMenuAction extends com.mind.common.IlexAction{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String appendix = "-1";
		String 	netmedx = "-1";
		String whereclause ="";
		if(request.getParameter("msaid") != null){
			whereclause = "<> 3";
			appendix = Appendixdao.getAppendixCount(request.getParameter("msaid"), whereclause, getDataSource( request , "ilexnewDB" ));
			request.setAttribute("appendix", appendix);
			whereclause = "= 3";
			netmedx = Appendixdao.getAppendixCount(request.getParameter("msaid"), whereclause, getDataSource( request , "ilexnewDB" ));
			request.setAttribute("netmedx", netmedx);
			boolean isvendex = EditMasterlDao.isRawVenedexed(request.getParameter("msaid"), getDataSource( request , "ilexnewDB" ));
			if(isvendex==true) {
				request.setAttribute("viewRawVendex", "viewRawVendex");
			}
			
		}
		
		return mapping.findForward("success");
	}
}
							
				