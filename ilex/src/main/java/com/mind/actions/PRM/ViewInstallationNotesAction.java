package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.ViewInstallationNotesdao;
import com.mind.formbean.PRM.ViewInstallationNotesForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ViewInstallationNotesAction extends
		com.mind.common.IlexDispatchAction {

	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		Map<String, Object> map;
		ViewInstallationNotesForm viewInstallationNotesForm = (ViewInstallationNotesForm) form;

		if (request.getParameter("from") != null) {
			viewInstallationNotesForm.setFrom(request.getParameter("from"));
		}

		if (request.getParameter("nettype") != null) {
			viewInstallationNotesForm.setNettype(""
					+ request.getParameter("nettype"));
		}

		if (request.getParameter("viewjobtype") != null) {
			viewInstallationNotesForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			viewInstallationNotesForm.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			viewInstallationNotesForm.setOwnerId(""
					+ request.getParameter("ownerId"));
		} else {
			viewInstallationNotesForm.setOwnerId("%");
		}

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (viewInstallationNotesForm.getAuthenticate().equals("")) {
			if (viewInstallationNotesForm.getFrom().equals("appendixdashboard")) {
				if (!Authenticationdao.getPageSecurity(userid, "Manage Job",
						"View Installation",
						getDataSource(request, "ilexnewDB"))) {
					return (mapping.findForward("UnAuthenticate"));
				}
			} else {
				// code for security of job
			}
		}
		/* Page Security End */

		int flag = 0;
		int statusvalue = -1;
		String appendixid = "";
		String jobid = "";
		String notes = "";

		String addmessage = "";
		String appendixname = "";
		String jobname = "";
		ArrayList list = new ArrayList();
		appendixid = request.getParameter("appendixid");
		jobid = request.getParameter("jobid");
		appendixname = ViewInstallationNotesdao.getAppendixname(appendixid,
				getDataSource(request, "ilexnewDB"));
		viewInstallationNotesForm.setAppendixname(appendixname);
		jobname = ViewInstallationNotesdao.getJobname(jobid,
				getDataSource(request, "ilexnewDB"));
		viewInstallationNotesForm.setJobname(appendixname);
		list = ViewInstallationNotesdao.getNotesdetaillist(jobid,
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("noteslist", list);
		request.setAttribute("listsize", list.size() + "");

		request.setAttribute("appendixid", appendixid);
		request.setAttribute("appendixname", appendixname);
		request.setAttribute("jobid", jobid);

		if (viewInstallationNotesForm.getFrom() != null
				&& !viewInstallationNotesForm.getFrom().trim()
						.equalsIgnoreCase("")
				&& viewInstallationNotesForm.getFrom().trim()
						.equalsIgnoreCase("jobdashboard")) {
			if (jobid != null && !jobid.trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(jobid, userid,
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);

			}
		}

		forward = mapping.findForward("success");
		return (forward);
	}

}
