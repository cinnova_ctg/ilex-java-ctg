/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is an action class used for performing performing schedule update of appendix/job/activity in project manager.      
 *
 */
package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCNSWEB;
import com.mind.common.ChangeDateFormat;
import com.mind.common.MultipartRequestWrapper;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AddInstallationNotesdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.Scheduledao;
import com.mind.formbean.PRM.ScheduleUpdateForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ScheduleUpdateAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ScheduleUpdateAction.class);

	/**
	 * This method provides functionality to update schedule for
	 * apendix/job/activity.
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ScheduleUpdateForm scheduleUpdateForm = (ScheduleUpdateForm) form;

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		Map<String, Object> map;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (scheduleUpdateForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage Project",
					"Update Schedule", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		// set job filter type start
		if (request.getParameter("viewjobtype") != null) {
			scheduleUpdateForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			scheduleUpdateForm.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			scheduleUpdateForm.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			scheduleUpdateForm.setOwnerId("%");
		}

		if (request.getParameter("pageid") != null) {
			scheduleUpdateForm.setAppendixId(request.getParameter("pageid"));
			scheduleUpdateForm.setMsaId(IMdao.getMSAId(
					scheduleUpdateForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
		}

		if (request.getParameter("nettype") != null) {
			scheduleUpdateForm.setNettype("" + request.getParameter("nettype"));
		}

		int flag = 0;
		int status = 0;
		String page = "";
		String pageid = "";
		String appendixname = null;
		String jobname = null;
		String[] pmschedulelist = new String[2];

		/* Start : Appendix Dashboard View Selector Code */

		if (scheduleUpdateForm.getJobOwnerOtherCheck() != null
				&& !scheduleUpdateForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					scheduleUpdateForm.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId",
					scheduleUpdateForm.getAppendixId());
			request.setAttribute("msaId", scheduleUpdateForm.getMsaId());

			return mapping.findForward("temppage");
		}

		if (scheduleUpdateForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < scheduleUpdateForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ scheduleUpdateForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ scheduleUpdateForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (scheduleUpdateForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < scheduleUpdateForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ scheduleUpdateForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ scheduleUpdateForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					scheduleUpdateForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					scheduleUpdateForm.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId",
					scheduleUpdateForm.getAppendixId());
			request.setAttribute("msaId", scheduleUpdateForm.getMsaId());
			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				scheduleUpdateForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				scheduleUpdateForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				scheduleUpdateForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				scheduleUpdateForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(userid,
				scheduleUpdateForm.getJobOwnerOtherCheck(), "prj_job_new",
				scheduleUpdateForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		/* for installation notes */
		if (scheduleUpdateForm.getSave() != null) {
			String type = scheduleUpdateForm.getType().trim();
			String actstart = scheduleUpdateForm.getActstartdate();
			String actend = scheduleUpdateForm.getActenddate();

			if (!scheduleUpdateForm.getActstartdatehh().equals("  ")
					&& scheduleUpdateForm.getActstartdatemm().equals("  "))
				scheduleUpdateForm.setActstartdatemm("00");

			if (!scheduleUpdateForm.getActenddatehh().equals("  ")
					&& scheduleUpdateForm.getActenddatemm().equals("  "))
				scheduleUpdateForm.setActenddatemm("00");

			if (!scheduleUpdateForm.getStartdatehh().equals("  ")
					&& scheduleUpdateForm.getStartdatemm().equals("  "))
				scheduleUpdateForm.setStartdatemm("00");

			if (!scheduleUpdateForm.getPlannedenddatehh().equals("  ")
					&& scheduleUpdateForm.getPlannedenddatemm().equals("  "))
				scheduleUpdateForm.setPlannedenddatemm("00");

			if (!scheduleUpdateForm.getActstartdatehh().equals("  "))
				actstart += " " + scheduleUpdateForm.getActstartdatehh() + ":"
						+ scheduleUpdateForm.getActstartdatemm() + " "
						+ scheduleUpdateForm.getActstartdateop();

			if (!scheduleUpdateForm.getActenddatehh().equals("  "))
				actend += " " + scheduleUpdateForm.getActenddatehh() + ":"
						+ scheduleUpdateForm.getActenddatemm() + " "
						+ scheduleUpdateForm.getActenddateop();

			String startdate = scheduleUpdateForm.getStartdate();
			if (!scheduleUpdateForm.getStartdatehh().equals("  "))
				startdate += " " + scheduleUpdateForm.getStartdatehh() + ":"
						+ scheduleUpdateForm.getStartdatemm() + " "
						+ scheduleUpdateForm.getStartdateop();

			String enddate = scheduleUpdateForm.getPlannedenddate();
			if (!scheduleUpdateForm.getPlannedenddatehh().equals("  "))
				enddate += " " + scheduleUpdateForm.getPlannedenddatehh() + ":"
						+ scheduleUpdateForm.getPlannedenddatemm() + " "
						+ scheduleUpdateForm.getPlannedenddateop();

			if (actstart.equals(" : "))
				actstart = "";
			if (actend.equals(" : "))
				actend = "";
			if (startdate.equals(" : "))
				startdate = "";
			if (enddate.equals(" : "))
				enddate = "";

			int retValArray[] = new int[2];
			// retValArray =
			// Scheduledao.addScheduleForPRM(scheduleUpdateForm.getScheduleid(),
			// scheduleUpdateForm.getTypeid(), actstart, actend,
			// scheduleUpdateForm.getTotaldays(), type, userid, startdate,
			// enddate, getDataSource(request,"ilexnewDB"));
			status = retValArray[0];
			String newScheduleId = String.valueOf(retValArray[1]);
			/* For Job Reschedule: Start */
			if (scheduleUpdateForm.getRseType() != null
					&& !scheduleUpdateForm.getRseType()
							.equals("Administrative")) {
				String newSchDate = "";
				String hour = "";
				String min = "";
				String date = scheduleUpdateForm.getStartdate();
				if (scheduleUpdateForm.getStartdatehh().equals("  "))
					hour = "00";
				else
					hour = scheduleUpdateForm.getStartdatehh();
				if (scheduleUpdateForm.getStartdatemm().equals("  "))
					min = "00";
				else
					min = scheduleUpdateForm.getStartdatemm();

				newSchDate = date + " " + hour + ":" + min + " "
						+ scheduleUpdateForm.getStartdateop();
				scheduleUpdateForm.setNewSchStart(ChangeDateFormat
						.getDateFormat(newSchDate, "yyyy-MM-dd HH:mm",
								"MM/dd/yyyy hh:mm aaa"));

				Scheduledao.setReschedule(scheduleUpdateForm.getTypeid(),
						scheduleUpdateForm.getRseType(),
						scheduleUpdateForm.getOldSchStart(),
						scheduleUpdateForm.getNewSchStart(),
						scheduleUpdateForm.getRseReason(), userid,
						getDataSource(request, "ilexnewDB"));
			}
			/* For Job Reschedule: End */

			page = scheduleUpdateForm.getPage();
			pageid = scheduleUpdateForm.getPageid();
			request.setAttribute("type", type);
			request.setAttribute("page", page);
			scheduleUpdateForm.setActstartdate(actstart);
			scheduleUpdateForm.setActenddate(actend);

			scheduleUpdateForm.setChecknetmedxtype(scheduleUpdateForm
					.getChecknetmedxtype());

			scheduleUpdateForm.setUpdatemessage("" + status);
			flag = 1;
			// start
			if (type != null
					&& (type.equalsIgnoreCase("J") || type
							.equalsIgnoreCase("IJ"))) { // - no install note for
														// appendix shedule
				if (!scheduleUpdateForm.getActstartdate().equals("")) {
					String defaultset = "Tech onsite";
					AddInstallationNotesdao.addinstallnotes(
							scheduleUpdateForm.getTypeid(), defaultset, userid,
							getDataSource(request, "ilexnewDB"),
							scheduleUpdateForm.getActstartdate(),
							scheduleUpdateForm.getScheduleid(), newScheduleId,
							"S");
				}
				if (!scheduleUpdateForm.getActenddate().equals("")) {
					String defaultset = "Tech offsite";
					AddInstallationNotesdao.addinstallnotes(
							scheduleUpdateForm.getTypeid(), defaultset, userid,
							getDataSource(request, "ilexnewDB"),
							scheduleUpdateForm.getActenddate(),
							scheduleUpdateForm.getScheduleid(), newScheduleId,
							"E");
				}
			}
			// end
		} else {
			String[] list = new String[4];

			String typeid = request.getParameter("typeid");
			String type = request.getParameter("type").trim();
			page = request.getParameter("page");
			pageid = request.getParameter("pageid");
			// Added By pankaj
			if (type != null && type.trim().equalsIgnoreCase("J")) {
				map = DatabaseUtilityDao.setMenu(typeid, userid,
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
			// Added By Pankaj
			list = Scheduledao.getSchedule(typeid, type,
					getDataSource(request, "ilexnewDB"));
			scheduleUpdateForm.setPage(request.getParameter("page"));
			scheduleUpdateForm.setPageid(request.getParameter("pageid"));
			if (type.equals("P")) {
				appendixname = Scheduledao.getAppendixname(typeid,
						getDataSource(request, "ilexnewDB"));
				scheduleUpdateForm.setAppendixname(appendixname);

				scheduleUpdateForm.setChecknetmedxtype(ViewList
						.CheckAppendixType(typeid,
								getDataSource(request, "ilexnewDB")));
			} else {
				appendixname = Scheduledao.getAppendixname(pageid,
						getDataSource(request, "ilexnewDB"));
				scheduleUpdateForm.setAppendixname(appendixname);
				scheduleUpdateForm.setChecknetmedxtype(ViewList
						.CheckAppendixType(pageid,
								getDataSource(request, "ilexnewDB")));

				jobname = Scheduledao.getJobname(typeid,
						getDataSource(request, "ilexnewDB"));
				scheduleUpdateForm.setJobname(jobname);
			}

			if (list[2] != null) {
				scheduleUpdateForm.setScheduleid(list[0]);
				scheduleUpdateForm.setType(list[1].trim());
				scheduleUpdateForm.setTypeid(list[2]);

				/*
				 * if(list[3].charAt(11)==' ' && !list[3].equals(""))
				 * list[3]=list[3].substring(0,11)+"0"+list[3].substring(12);
				 * if(list[4].charAt(11)==' ' && !list[4].equals(""))
				 * list[4]=list[4].substring(0,11)+"0"+list[4].substring(12);
				 * 
				 * if(!list[6].equals("")) { if(list[6].charAt(11)==' ')
				 * list[6]=list[6].substring(0,11)+"0"+list[6].substring(12); }
				 * if(!list[7].equals("")) { if(list[7].charAt(11)==' ')
				 * list[7]=list[7].substring(0,11)+"0"+list[7].substring(12); }
				 */

				if (!list[6].equals("")) {
					scheduleUpdateForm
							.setActstartdate(list[6].substring(0, 10));
					if (list[6].substring(11, 12).equals(" "))
						scheduleUpdateForm.setActstartdatehh("0"
								+ list[6].substring(12, 13));
					else
						scheduleUpdateForm.setActstartdatehh(list[6].substring(
								11, 13));
					scheduleUpdateForm.setActstartdatemm(list[6].substring(14,
							16));
					scheduleUpdateForm.setActstartdateop(list[6].substring(23,
							25));
				}

				if (!list[7].equals("")) {
					scheduleUpdateForm.setActenddate(list[7].substring(0, 10));
					if (list[7].substring(11, 12).equals(" "))
						scheduleUpdateForm.setActenddatehh("0"
								+ list[7].substring(12, 13));
					else
						scheduleUpdateForm.setActenddatehh(list[7].substring(
								11, 13));
					scheduleUpdateForm.setActenddatemm(list[7]
							.substring(14, 16));
					scheduleUpdateForm.setActenddateop(list[7]
							.substring(23, 25));
				}

				scheduleUpdateForm.setTotaldays(list[5]);

				if (!list[3].equals("")) {
					scheduleUpdateForm.setStartdate(list[3].substring(0, 10));
					if (list[3].substring(11, 12).equals(" "))
						scheduleUpdateForm.setStartdatehh("0"
								+ list[3].substring(12, 13));
					else
						scheduleUpdateForm.setStartdatehh(list[3].substring(11,
								13));
					scheduleUpdateForm
							.setStartdatemm(list[3].substring(14, 16));
					scheduleUpdateForm
							.setStartdateop(list[3].substring(23, 25));
				}

				if (!list[4].equals("")) {
					scheduleUpdateForm.setPlannedenddate(list[4].substring(0,
							10));
					if (list[4].substring(11, 12).equals(" "))
						scheduleUpdateForm.setPlannedenddatehh("0"
								+ list[4].substring(12, 13));
					else
						scheduleUpdateForm.setPlannedenddatehh(list[4]
								.substring(11, 13));
					scheduleUpdateForm.setPlannedenddatemm(list[4].substring(
							14, 16));
					scheduleUpdateForm.setPlannedenddateop(list[4].substring(
							23, 25));
				}
			} else {
				scheduleUpdateForm.setScheduleid("0");
				scheduleUpdateForm.setType(type);
				scheduleUpdateForm.setTypeid(typeid);

				// updated by hamid
				pmschedulelist = Scheduledao.getPMSchedule(typeid,
						getDataSource(request, "ilexnewDB"));

				scheduleUpdateForm.setStartdate(pmschedulelist[0]);
				scheduleUpdateForm.setPlannedenddate(pmschedulelist[1]);

				scheduleUpdateForm.setStartdatehh(pmschedulelist[2]);
				scheduleUpdateForm.setStartdatemm(pmschedulelist[3]);
				scheduleUpdateForm.setStartdateop(pmschedulelist[4]);

				scheduleUpdateForm.setPlannedenddatehh(pmschedulelist[5]);
				scheduleUpdateForm.setPlannedenddatemm(pmschedulelist[6]);
				scheduleUpdateForm.setPlannedenddateop(pmschedulelist[7]);

				// updated by hamid
				// scheduleUpdateForm.setStartdate(pmschedulelist[0]);
				// scheduleUpdateForm.setPlannedenddate(pmschedulelist[1]);
				scheduleUpdateForm.setTotaldays("0");
			}
		}
		scheduleUpdateForm.setCurrentSchDate(scheduleUpdateForm.getStartdate());
		DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();
		dynamiccomboCNSWEB.setHourname(DynamicComboDao.getHourName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		dynamiccomboCNSWEB.setMinutename(DynamicComboDao.getMinuteName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		dynamiccomboCNSWEB.setOptionname(DynamicComboDao.getOptionName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		// For Default Instalation Notes Entry

		if (scheduleUpdateForm.getActstartdate() == null) {
			scheduleUpdateForm.setActstartdate("");
		}
		if (scheduleUpdateForm.getActenddate() == null) {
			scheduleUpdateForm.setActenddate("");
		}

		if (flag == 1) {
			if (status == 0) {
				String temppath = "PRMJobScheduleInfo";
				request.setAttribute("type", temppath);
				request.setAttribute("appendix_Id",
						scheduleUpdateForm.getPageid());
				request.setAttribute("viewjobtype",
						scheduleUpdateForm.getViewjobtype());
				request.setAttribute("ownerId", scheduleUpdateForm.getOwnerId());

				if (scheduleUpdateForm.getPage().equals("jobdashboard")) {
					request.setAttribute("jobid",
							scheduleUpdateForm.getTypeid());
					request.setAttribute("tabId",
							(request.getParameter("tabId") == null ? ""
									: request.getParameter("tabId")));
					if (logger.isDebugEnabled()) {
						logger.debug("update(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - "
								+ request.getParameter("tabId")
								+ " >>>> tabid >>> jobid"
								+ scheduleUpdateForm.getTypeid());
					}
					String url = "JobDashboardAction.do?hmode=unspecified&tabId="
							+ request.getParameter("tabId")
							+ "&jobid="
							+ scheduleUpdateForm.getTypeid();
					try {
						RequestDispatcher dispatcher = request
								.getRequestDispatcher(url);
						dispatcher.forward(request, response);
					} catch (Exception e) {
						logger.error(
								"update(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
								e);

						if (logger.isDebugEnabled()) {
							logger.debug("update(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - inside exception");
						}
					}
				} else {
					return (mapping.findForward("temppage"));
				}
			} else {
				forward = mapping.findForward("failure");
			}

		} else {
			forward = mapping.findForward("failure");
		}

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(scheduleUpdateForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				scheduleUpdateForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("appendixid", scheduleUpdateForm.getAppendixId());
		request.setAttribute("msaId", scheduleUpdateForm.getMsaId());
		request.setAttribute("viewjobtype", scheduleUpdateForm.getViewjobtype());
		request.setAttribute("ownerId", scheduleUpdateForm.getOwnerId());
		request.setAttribute("scheduleform", scheduleUpdateForm);
		/*
		 * if( request.getParameter("throughPage") !=null &&
		 * request.getParameter("throughPage").equals( "jobdashboard" ) ) {
		 * request.setAttribute( "tabId" ,
		 * (request.getParameter("tabId")==null?""
		 * :request.getParameter("tabId"))); request.setAttribute( "jobid" ,
		 * scheduleUpdateForm.getTypeid() ); return mapping.findForward(
		 * "jobdashboard" ); }
		 */

		if (request.getParameter("throughJobDashboard") != null) {
			RequestDispatcher ds = request
					.getRequestDispatcher("/PRM/ScheduleUpdate.jsp");
			try {
				if (request
						.getClass()
						.getName()
						.equals("org.apache.struts.upload.MultipartRequestWrapper")) {
					logger.debug("Casting MultiparRequestWrapper to custom class");
					request = new MultipartRequestWrapper(request);
				}
				ds.include(request, response);
			} catch (Exception e) {
				logger.warn(
						"update(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored",
						e);

			}
		} else
			return forward;

		return null;
	}

	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ScheduleUpdateForm scheduleUpdateForm = (ScheduleUpdateForm) form;
		// set job filter type start
		if (request.getParameter("viewjobtype") != null) {
			scheduleUpdateForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		}

		if (request.getParameter("ownerId") != null) {
			scheduleUpdateForm.setOwnerId("" + request.getParameter("ownerId"));
		}

		if (request.getParameter("nettype") != null) {
			scheduleUpdateForm.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getParameter("pageid") != null) {
			scheduleUpdateForm.setAppendixId(request.getParameter("pageid"));
			scheduleUpdateForm.setMsaId(IMdao.getMSAId(
					scheduleUpdateForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
		}

		String schedulepresent = "false";
		String[] list = new String[4];
		String typeid = request.getParameter("typeid");
		String type = request.getParameter("type");
		list = Scheduledao.getSchedule(typeid, type,
				getDataSource(request, "ilexnewDB"));
		if (list != null) {
			scheduleUpdateForm.setScheduleid(list[0]);
			scheduleUpdateForm.setType(list[1].trim());
			scheduleUpdateForm.setTypeid(list[2]);

			if (!list[6].equals("")) {
				scheduleUpdateForm.setActstartdate(list[6].substring(0, 10));
				if (list[6].substring(11, 12).equals(" "))
					scheduleUpdateForm.setActstartdatehh("0"
							+ list[6].substring(12, 13));
				else
					scheduleUpdateForm.setActstartdatehh(list[6].substring(11,
							13));
				scheduleUpdateForm.setActstartdatemm(list[6].substring(14, 16));
				scheduleUpdateForm.setActstartdateop(list[6].substring(23, 25));
			}

			if (!list[7].equals("")) {
				scheduleUpdateForm.setActenddate(list[7].substring(0, 10));
				if (list[7].substring(11, 12).equals(" "))
					scheduleUpdateForm.setActenddatehh("0"
							+ list[7].substring(12, 13));
				else
					scheduleUpdateForm.setActenddatehh(list[7]
							.substring(11, 13));
				scheduleUpdateForm.setActenddatemm(list[7].substring(14, 16));
				scheduleUpdateForm.setActenddateop(list[7].substring(23, 25));
			}

			scheduleUpdateForm.setTotaldays(list[5]);

			if (!list[3].equals("")) {
				scheduleUpdateForm.setStartdate(list[3].substring(0, 10));
				if (list[3].substring(11, 12).equals(" "))
					scheduleUpdateForm.setStartdatehh("0"
							+ list[3].substring(12, 13));
				else
					scheduleUpdateForm
							.setStartdatehh(list[3].substring(11, 13));
				scheduleUpdateForm.setStartdatemm(list[3].substring(14, 16));
				scheduleUpdateForm.setStartdateop(list[3].substring(23, 25));
			}

			if (!list[4].equals("")) {
				scheduleUpdateForm.setPlannedenddate(list[4].substring(0, 10));
				if (list[4].substring(11, 12).equals(" "))
					scheduleUpdateForm.setPlannedenddatehh("0"
							+ list[4].substring(12, 13));
				else
					scheduleUpdateForm.setPlannedenddatehh(list[4].substring(
							11, 13));
				scheduleUpdateForm.setPlannedenddatemm(list[4]
						.substring(14, 16));
				scheduleUpdateForm.setPlannedenddateop(list[4]
						.substring(23, 25));
			}

			/*
			 * if(list[3].charAt(11)==' ')
			 * list[3]=list[3].substring(0,11)+"0"+list[3].substring(12);
			 * if(list[4].charAt(11)==' ')
			 * list[4]=list[4].substring(0,11)+"0"+list[4].substring(12);
			 * if(list[6].charAt(11)==' ')
			 * list[6]=list[6].substring(0,11)+"0"+list[6].substring(12);
			 * if(list[7].charAt(11)==' ')
			 * list[7]=list[7].substring(0,11)+"0"+list[7].substring(12);
			 * 
			 * scheduleUpdateForm.setActstartdate(list[6].substring(0, 10));
			 * scheduleUpdateForm.setActstartdatehh(list[6].substring(11,13));
			 * scheduleUpdateForm.setActstartdatemm(list[6].substring(14,16));
			 * scheduleUpdateForm.setActstartdateop(list[6].substring(23,25));
			 * 
			 * scheduleUpdateForm.setActenddate(list[7].substring(0, 10));
			 * scheduleUpdateForm.setActenddatehh(list[7].substring(11,13));
			 * scheduleUpdateForm.setActenddatemm(list[7].substring(14,16));
			 * scheduleUpdateForm.setActenddateop(list[7].substring(23,25));
			 * 
			 * scheduleUpdateForm.setTotaldays(list[5]);
			 * 
			 * scheduleUpdateForm.setStartdate(list[3].substring(0, 10));
			 * scheduleUpdateForm.setStartdatehh(list[3].substring(11,13));
			 * scheduleUpdateForm.setStartdatemm(list[3].substring(14,16));
			 * scheduleUpdateForm.setStartdateop(list[3].substring(23,25));
			 * 
			 * scheduleUpdateForm.setPlannedenddate(list[4].substring(0, 10));
			 * scheduleUpdateForm.setPlannedenddatehh(list[4].substring(11,13));
			 * scheduleUpdateForm.setPlannedenddatemm(list[4].substring(14,16));
			 * scheduleUpdateForm.setPlannedenddateop(list[4].substring(23,25));
			 */
			schedulepresent = "true";

		}

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(scheduleUpdateForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				scheduleUpdateForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("appendixid", scheduleUpdateForm.getAppendixId());
		request.setAttribute("msaId", scheduleUpdateForm.getMsaId());
		request.setAttribute("viewjobtype", scheduleUpdateForm.getViewjobtype());
		request.setAttribute("ownerId", scheduleUpdateForm.getOwnerId());

		request.setAttribute("schedulepresent", schedulepresent);
		request.setAttribute("scheduleform", scheduleUpdateForm);
		forward = mapping.findForward("success");
		return (forward);
	}

	/**
	 * Discription: This method is used when any job is rescheduled. Its provied
	 * a user interface to lock reschedule action.
	 */
	public ActionForward Reschedule(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		response.setHeader("pragma", "no-cache");// HTTP 1.1
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.addDateHeader("Expires", -1);
		response.setDateHeader("max-age", 0);
		response.setIntHeader("Expires", -1); // - prevents caching at the proxy
												// server
		response.addHeader("cache-Control", "private");

		if (!request.getParameter("hide").equals("hide")) {

			ScheduleUpdateForm scheduleUpdateForm = (ScheduleUpdateForm) form;
			String date = "";
			String hour = "";
			String min = "";
			String aaa = "";
			String startDate = "";
			String jobId = "";
			String currStartDateTime = "";
			boolean check = true;

			if (request.getParameter("date") != null) {
				date = request.getParameter("date");
			}
			if (request.getParameter("hh") != null) {
				hour = request.getParameter("hh");
			}
			if (request.getParameter("mm") != null) {
				min = request.getParameter("mm");
			}
			if (request.getParameter("aa") != null) {
				aaa = request.getParameter("aa");
			}
			if (request.getParameter("jobId") != null) {
				jobId = request.getParameter("jobId");
			}

			startDate = date + " " + hour + ":" + min + " " + aaa;

			/* Get and Set current schedule date */
			currStartDateTime = Scheduledao.checkStartDate(jobId,
					getDataSource(request, "ilexnewDB"));
			scheduleUpdateForm.setOldSchStart(currStartDateTime);

			/* Conver date from MM/dd/yyyy hh:mm aaa to yyyy-MM-dd HH:mm */
			String newStartDateTime = ChangeDateFormat.getDateFormat(startDate,
					"yyyy-MM-dd HH:mm", "MM/dd/yyyy hh:mm aaa");

			if (newStartDateTime.equals(currStartDateTime)) {
				check = false;
			} // - when scheduled start date not changed.

			/* When actual start date exists or job is in Not scheduled state. */
			// else if (currStartDateTime.equals("NoRes")) {
			// check = false;
			// }
			response.setContentType("application/xml");
			if (check) {
				RequestDispatcher rd = request
						.getRequestDispatcher("/PRM/JobReschedule.jsp"); // -
																			// inclued
																			// JobReschedule.jsp
				rd.include(request, response);
			}

		}
		return null;
	}

}
