//ref=bukJobCreationView&ref1=view&Appendix_Id=<%= appendixid %>&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.LoginForm;
import com.mind.bean.prm.BulkJobCreationBean;
import com.mind.common.bean.BulkJobBean;
import com.mind.common.bean.SearchSiteBean;
import com.mind.common.bean.Sortcodelist;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.Logindao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AddendumJobName;
import com.mind.dao.PRM.BulkJobCreationdao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.dao.PRM.UploadError;
import com.mind.formbean.PRM.BulkJobCreationForm;
import com.mind.util.WebUtil;

public class BulkJobCreationAction extends com.mind.common.IlexDispatchAction {

    /**
     * This method will be called when first time Bulk Job Creation Page will
     * open.
     *
     * @author amitm
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public ActionForward bukJobCreationView(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        int sitesSize = 0;
        String fromProposalMg = "No";
        int jobrowcount = 1;

        String isBukJobCreationView = "A";// Flag for during calling
        // bukJobCreationView Message for no
        // sites available
        BulkJobCreationForm bulkjobform = (BulkJobCreationForm) form;

        String prevtempjob_id = "";
        ArrayList addendumjobnamelist = new ArrayList();
        AddendumJobName addendumjobname = null;

        HttpSession session = request.getSession(true);
        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }															// expired
        String loginuserid = (String) session.getAttribute("userid");

		// String appendixid = "";
        // appendixid=(String)request.getParameter("Appendix_Id");
        if (request.getParameter("Appendix_Id") != null) {
            bulkjobform.setAppendix_Id(request.getParameter("Appendix_Id"));
        }

        String id = "";
        String type = "";
        if (request.getParameter("fromProposalMg") != null) {
            fromProposalMg = (String) request.getParameter("fromProposalMg");
            bulkjobform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
                    getDataSource(request, "ilexnewDB"),
                    (String) request.getParameter("MSA_Id")));
            request.setAttribute("fromProposalMg", "Yes");
        }

        if (request.getParameter("viewjobtype") != null) {
            bulkjobform.setViewjobtype(request.getParameter("viewjobtype"));
        } else {
            bulkjobform.setViewjobtype("A");
        }

        if (request.getParameter("ownerId") != null) {
            bulkjobform.setOwnerId("" + request.getParameter("ownerId"));
        } else {
            bulkjobform.setOwnerId("%");
        }

        if (request.getParameter("isBukJobCreationView") != null) {
            isBukJobCreationView = request.getParameter("isBukJobCreationView")
                    .toString();
        }

        if (request.getParameter("JobSaved") != null) {
            String JobSaved = request.getParameter("JobSaved").toString();
        }

        ArrayList initialStateCategory = new ArrayList();
        String tempCat = "";
        String[] tempList = null;
        String check = "";

        bulkjobform.setAppendixname(Jobdao.getAppendixname(
                getDataSource(request, "ilexnewDB"),
                bulkjobform.getAppendix_Id()));
        bulkjobform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
                getDataSource(request, "ilexnewDB"),
                IMdao.getMSAId(bulkjobform.getAppendix_Id(),
                        getDataSource(request, "ilexnewDB"))));
        bulkjobform.setMsa_id(IMdao.getMSAId(bulkjobform.getAppendix_Id(),
                getDataSource(request, "ilexnewDB")));

		// Pvsdao pvsDao= new Pvsdao();
        // if(pvsDao!=null){
        initialStateCategory = BulkJobCreationdao
                .getBulkJobStateList(getDataSource(request, "ilexnewDB"));

        if (initialStateCategory.size() > 0) {
            for (int i = 0; i < initialStateCategory.size(); i++) {
                tempCat = ((BulkJobCreationBean) initialStateCategory.get(i))
                        .getCountryId();
                ((BulkJobCreationBean) initialStateCategory.get(i))
                        .setTempList(SiteManagedao
                                .getCategoryWiseStateList(tempCat, i,
                                        getDataSource(request, "ilexnewDB")));
            }
        }
        request.setAttribute("initialStateCategory", initialStateCategory);

        /*
         * } else{ }
         */
        DynamicComboCM dcGroupList = new DynamicComboCM();
        dcGroupList.setFirstlevelcatglist(DynamicComboDao
                .getGroupNameListSiteSearch(bulkjobform.getMsa_id(),
                        getDataSource(request, "ilexnewDB")));
        request.setAttribute("dcGroupList", dcGroupList);

        DynamicComboCM dcEndCustomerList = new DynamicComboCM();
        dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
                .getSiteEndCustomerList(getDataSource(request, "ilexnewDB"),
                        bulkjobform.getMsa_id(), bulkjobform.getMsaname()));
        request.setAttribute("dcEndCustomerList", dcEndCustomerList);

        ArrayList activityList = BulkJobCreationdao.getActivityList(
                bulkjobform.getAppendix_Id(),
                getDataSource(request, "ilexnewDB"));

        if (activityList.size() > 0) {
            for (int i = 0; i < activityList.size(); i++) {
                if (!prevtempjob_id.equals(((BulkJobBean) activityList.get(i))
                        .getTempjob_id())) {
                    if (prevtempjob_id != "") {
                        addendumjobname = new AddendumJobName();
                        addendumjobname
                                .setAddendumjobname(((BulkJobBean) activityList
                                        .get(i - 1)).getTempjob_name());
                        addendumjobname.setJobrowcount(jobrowcount + "");
                        addendumjobnamelist.add(addendumjobname);
                        jobrowcount = 1;
                    }
                } else {
                    jobrowcount++;
                    if (i == activityList.size() - 1) {
                        addendumjobname = new AddendumJobName();
                        addendumjobname
                                .setAddendumjobname(((BulkJobBean) activityList
                                        .get(i)).getTempjob_name());
                        addendumjobname.setJobrowcount(jobrowcount + "");
                        addendumjobnamelist.add(addendumjobname);
                    }
                }

                prevtempjob_id = ((BulkJobBean) activityList.get(i))
                        .getTempjob_id();
            }

            if (addendumjobnamelist != null) {
                if (addendumjobnamelist.size() > 0) {
                    request.setAttribute("firstdivrownum",
                            ((AddendumJobName) addendumjobnamelist.get(0))
                            .getJobrowcount());
                } else {
                    request.setAttribute("firstdivrownum", 0 + "");
                }
            }
            request.setAttribute("addendumjobnamelist", addendumjobnamelist);
        }

        request.setAttribute("activityList", activityList);
        request.setAttribute("activityListSize",
                new Integer(activityList.size()));

        ArrayList siteDetailLst = new ArrayList();

        siteDetailLst = BulkJobCreationdao.getSiteDetailList("0", "0", "0",
                bulkjobform.getAppendix_Id(), "0", "",
                getDataSource(request, "ilexnewDB"), "0", "0", "0");

        bulkjobform.setSitedetaillist(siteDetailLst);
        // Checking whether after go button Arraylist of site ias comming or not
        if (siteDetailLst != null) {
            if (siteDetailLst.size() > 0) {
                sitesSize = siteDetailLst.size();
            }
        }

		// start For setting Combo For State
        // request.setAttribute( "JobSaved" , "1" );
        request.setAttribute("sitesSize", new Integer(sitesSize));
        request.setAttribute("isBukJobCreationView", isBukJobCreationView);
        request.setAttribute("Appendix_Id", bulkjobform.getAppendix_Id());

        if (request.getParameter("firstdivrownum") != null) {
            String firstdivrownum = request.getParameter("firstdivrownum")
                    .toString();
            request.setAttribute("firstdivrownum", firstdivrownum);
        }
        /*
         * if(request.getParameter( "addendumjobnamelist") != null) { ArrayList
         * addendumjobnamelist1 = new ArrayList(); addendumjobnamelist1 =
         * (ArrayList) request.getParameter("addendumjobnamelist");
         *
         * }
         *
         * if(request.getParameter( "activityList") != null) { Arraylist
         * activityList1 = request.getParameter("activityList"); }
         *
         * if(request.getParameter( "activityListSize") != null) {
         * activityListSize = request.getParameter(
         * "activityListSize").toString(); }
         */
        String contractDocoumentList = com.mind.dao.PM.Appendixdao
                .getcontractDocoumentMenu(bulkjobform.getAppendix_Id(),
                        getDataSource(request, "ilexnewDB"));
        request.setAttribute("contractDocMenu", contractDocoumentList);
        String appendixcurrentstatus = Appendixdao.getCurrentstatus(
                bulkjobform.getAppendix_Id(),
                getDataSource(request, "ilexnewDB"));
        request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
        request.setAttribute("viewjobtype", bulkjobform.getViewjobtype());
        request.setAttribute("ownerId", bulkjobform.getOwnerId());
        request.setAttribute("msaId", bulkjobform.getMsa_id());
        request.setAttribute("appendixid", bulkjobform.getAppendix_Id());

        ActionForward forward = setViewSelector(mapping, bulkjobform, request,
                response);
        return (forward);
    }

    /**
     * This method will be called on clicking Go button for site search.
     *
     * @author amitm
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public ActionForward siteSearch(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String isBukJobCreationView = "B";
        String strCity = null;
        String strValue = null;
        int sitesSize = 0;
        String fromProposalMg = "No";
        Sortcodelist codes = new Sortcodelist();
        String strState = null;
        SearchSiteBean objSearchSiteBean = new SearchSiteBean();
        int jobrowcount = 1;
        String prevtempjob_id = "";
        String orderByQuery = "";

        ArrayList addendumjobnamelist = new ArrayList();
        AddendumJobName addendumjobname = null;

        ArrayList parameter = new ArrayList();
        ArrayList alreturned = new ArrayList();
        StringBuffer sbConcatedString = null;

        BulkJobCreationForm bulkjobform = (BulkJobCreationForm) form;

        String appendixid = "";
        if (request.getParameter("appendix_Id") != null) {
            appendixid = request.getParameter("appendix_Id");
        } else {
            appendixid = bulkjobform.getAppendix_Id();
        }

        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");

        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }															// expired

        if (request.getParameter("fromProposalMg") != null) {
            fromProposalMg = (String) request.getParameter("fromProposalMg");
            bulkjobform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
                    getDataSource(request, "ilexnewDB"),
                    IMdao.getMSAId(appendixid,
                            getDataSource(request, "ilexnewDB"))));
            request.setAttribute("fromProposalMg", "Yes");
        }

        if (request.getParameter("sitecitysearch") != null) {
            bulkjobform.setCity(request.getParameter("sitecitysearch"));
        }

        if (request.getParameter("sitenumbersearch") != null) {
            bulkjobform
                    .setSite_Number(request.getParameter("sitenumbersearch"));
        }

        if (request.getParameter("sitestatesearch") != null) {
            bulkjobform.setSite_state(request.getParameter("sitestatesearch"));
        }

        if (request.getParameter("querystring") != null) {
            orderByQuery = request.getParameter("querystring");
        }

        if (request.getParameter("viewjobtype") != null) {
            bulkjobform.setViewjobtype(request.getParameter("viewjobtype"));
        } else {
            bulkjobform.setViewjobtype("A");
        }

        if (request.getParameter("ownerId") != null) {
            bulkjobform.setOwnerId("" + request.getParameter("ownerId"));
        } else {
            bulkjobform.setOwnerId("%");
        }

        strState = bulkjobform.getSite_state();
        // Here to get MSA Name com.mind.dao.PM.Appendixdao has been used
        bulkjobform.setAppendixname(Jobdao.getAppendixname(
                getDataSource(request, "ilexnewDB"), appendixid));
        bulkjobform
                .setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
                                getDataSource(request, "ilexnewDB"),
                                IMdao.getMSAId(appendixid,
                                        getDataSource(request, "ilexnewDB"))));
        bulkjobform.setMsa_id(IMdao.getMSAId(appendixid,
                getDataSource(request, "ilexnewDB")));

        BulkJobCreationdao objBulkJobCreationdao = new BulkJobCreationdao();

        ArrayList siteDetailLst = new ArrayList();
        siteDetailLst = BulkJobCreationdao.getSiteDetailList(
                bulkjobform.getCity(), bulkjobform.getState(),
                bulkjobform.getMsa_id(), appendixid,
                bulkjobform.getSite_Number(), orderByQuery,
                getDataSource(request, "ilexnewDB"),
                bulkjobform.getBulkJobEndCustomer(), bulkjobform.getBrand(),
                bulkjobform.getCountry());
        bulkjobform.setSitedetaillist(siteDetailLst);

        // Checking whether after go button Arraylist of site ias comming or not
        if (siteDetailLst != null) {

            if (siteDetailLst.size() > 0) {
                sitesSize = siteDetailLst.size();
            }
        }

        // start For setting Combo For State
        ArrayList initialStateCategory = new ArrayList();
        String tempCat = "";
        String[] tempList = null;
        String check = "";

		// Pvsdao pvsDao= new Pvsdao();
        // if(pvsDao!=null){
        initialStateCategory = BulkJobCreationdao
                .getBulkJobStateList(getDataSource(request, "ilexnewDB"));

        if (initialStateCategory.size() > 0) {
            for (int i = 0; i < initialStateCategory.size(); i++) {
                tempCat = ((BulkJobCreationBean) initialStateCategory.get(i))
                        .getCountryId();
                ((BulkJobCreationBean) initialStateCategory.get(i))
                        .setTempList(SiteManagedao
                                .getCategoryWiseStateList(tempCat, i,
                                        getDataSource(request, "ilexnewDB")));
            }
        }
        request.setAttribute("initialStateCategory", initialStateCategory);
        /*
         * dcStateCM.setFirstlevelcatglist(Pvsdao.getStateList(getDataSource(
         * request,"ilexnewDB" ))); request.setAttribute("dcStateCM",dcStateCM);
         *
         * } else{ }
         */
        // End For setting Combo For State

        DynamicComboCM dcGroupList = new DynamicComboCM();
        dcGroupList.setFirstlevelcatglist(DynamicComboDao
                .getGroupNameListSiteSearch(bulkjobform.getMsa_id(),
                        getDataSource(request, "ilexnewDB")));
        request.setAttribute("dcGroupList", dcGroupList);

        DynamicComboCM dcEndCustomerList = new DynamicComboCM();
        dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
                .getSiteEndCustomerList(getDataSource(request, "ilexnewDB"),
                        bulkjobform.getMsa_id(), bulkjobform.getMsaname()));
        request.setAttribute("dcEndCustomerList", dcEndCustomerList);

        ArrayList activityList = BulkJobCreationdao.getActivityList(appendixid,
                getDataSource(request, "ilexnewDB"));

        if (activityList.size() > 0) {
            for (int i = 0; i < activityList.size(); i++) {
                if (!prevtempjob_id.equals(((BulkJobBean) activityList.get(i))
                        .getTempjob_id())) {
                    if (prevtempjob_id != "") {
                        addendumjobname = new AddendumJobName();
                        addendumjobname
                                .setAddendumjobname(((BulkJobBean) activityList
                                        .get(i - 1)).getTempjob_name());
                        addendumjobname.setJobrowcount(jobrowcount + "");
                        addendumjobnamelist.add(addendumjobname);
                        jobrowcount = 1;
                    }
                } else {
                    jobrowcount++;
                    if (i == activityList.size() - 1) {
                        addendumjobname = new AddendumJobName();
                        addendumjobname
                                .setAddendumjobname(((BulkJobBean) activityList
                                        .get(i)).getTempjob_name());
                        addendumjobname.setJobrowcount(jobrowcount + "");
                        addendumjobnamelist.add(addendumjobname);
                    }

                }

                prevtempjob_id = ((BulkJobBean) activityList.get(i))
                        .getTempjob_id();
            }
            if (addendumjobnamelist != null) {
                if (addendumjobnamelist.size() > 0) {
                    request.setAttribute("firstdivrownum",
                            ((AddendumJobName) addendumjobnamelist.get(0))
                            .getJobrowcount());
                } else {
                    request.setAttribute("firstdivrownum", 0 + "");
                }
            }
			// request.setAttribute( "firstdivrownum" , ( ( AddendumJobName
            // )addendumjobnamelist.get(0) ).getJobrowcount() );
            request.setAttribute("addendumjobnamelist", addendumjobnamelist);
        }

        request.setAttribute("activityList", activityList);
        request.setAttribute("activityListSize",
                new Integer(activityList.size()));
        request.setAttribute("sitesSize", new Integer(sitesSize));

        request.setAttribute("isBukJobCreationView", isBukJobCreationView);

        String contractDocoumentList = com.mind.dao.PM.Appendixdao
                .getcontractDocoumentMenu(bulkjobform.getAppendix_Id(),
                        getDataSource(request, "ilexnewDB"));
        request.setAttribute("contractDocMenu", contractDocoumentList);
        String appendixcurrentstatus = Appendixdao.getCurrentstatus(
                bulkjobform.getAppendix_Id(),
                getDataSource(request, "ilexnewDB"));
        request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
        request.setAttribute("viewjobtype", bulkjobform.getViewjobtype());
        request.setAttribute("ownerId", bulkjobform.getOwnerId());
        request.setAttribute("msaId", bulkjobform.getMsa_id());
        request.setAttribute("appendixid", bulkjobform.getAppendix_Id());

        ActionForward forward = setViewSelector(mapping, bulkjobform, request,
                response);
        return (forward);
    }

    /**
     * This method will be called on clicking save button of bulk job creation
     * screen.
     *
     * @author amitm
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public ActionForward saveSiteSearch(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String strCity = null;
        String strValue = null;
        Sortcodelist codes = new Sortcodelist();
        String strState = null;
        SearchSiteBean objSearchSiteBean = new SearchSiteBean();

        ArrayList error = new ArrayList();
        UploadError uploadError = null;
        int sitesSize = 0;

        String isBukJobCreationView = "A";
        ArrayList parameter = new ArrayList();
        ArrayList alreturned = new ArrayList();
        StringBuffer sbConcatedString = null;

        BulkJobCreationForm bulkjobform = (BulkJobCreationForm) form;
        BulkJobCreationBean bulkJobBean = new BulkJobCreationBean();

        String appendixid = "";

        if (request.getParameter("Appendix_Id") != null) {
            appendixid = request.getParameter("Appendix_Id");
        } else {
            appendixid = bulkjobform.getAppendix_Id();
        }

        bulkjobform.setAppendix_Id(appendixid);

        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");

        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }															// expired

        if (request.getParameter("viewjobtype") != null) {
            bulkjobform.setViewjobtype(request.getParameter("viewjobtype"));
        } else {
            bulkjobform.setViewjobtype("A");
        }

        if (request.getParameter("ownerId") != null) {
            bulkjobform.setOwnerId("" + request.getParameter("ownerId"));
        } else {
            bulkjobform.setOwnerId("%");
        }

        strState = bulkjobform.getSite_state();

        // Here to get MSA Name com.mind.dao.PM.Appendixdao has been used
        bulkjobform.setAppendixname(Jobdao.getAppendixname(
                getDataSource(request, "ilexnewDB"), appendixid));
        bulkjobform
                .setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
                                getDataSource(request, "ilexnewDB"),
                                IMdao.getMSAId(appendixid,
                                        getDataSource(request, "ilexnewDB"))));

        bulkjobform.getMsaname();
        bulkjobform.setMsa_id(IMdao.getMSAId(appendixid,
                getDataSource(request, "ilexnewDB")));
        BulkJobCreationdao objBulkJobCreationdao = new BulkJobCreationdao();

        bulkjobform.setSitedetaillist(BulkJobCreationdao.getSiteDetailList("0",
                "0", "0", appendixid, "0", "",
                getDataSource(request, "ilexnewDB"), "0", "0", "0"));

        ArrayList siteDetailLst = new ArrayList();
        siteDetailLst = bulkjobform.getSitedetaillist();
        ArrayList addendumjobnamelist = new ArrayList();
        AddendumJobName addendumjobname = null;
        String prevtempjob_id = "";
        int jobrowcount = 1;

        if (siteDetailLst != null) {
            if (siteDetailLst.size() > 0) {
                sitesSize = siteDetailLst.size();
            }
        }
        bulkjobform.setSite_Number("");
        bulkjobform.setSite_state("");
        bulkjobform.setCity("");

        ArrayList activityList = BulkJobCreationdao.getActivityList(appendixid,
                getDataSource(request, "ilexnewDB"));

        if (activityList.size() > 0) {
            for (int i = 0; i < activityList.size(); i++) {
                if (!prevtempjob_id.equals(((BulkJobBean) activityList.get(i))
                        .getTempjob_id())) {
                    if (prevtempjob_id != "") {
                        addendumjobname = new AddendumJobName();
                        addendumjobname
                                .setAddendumjobname(((BulkJobBean) activityList
                                        .get(i - 1)).getTempjob_name());
                        addendumjobname.setJobrowcount(jobrowcount + "");
                        addendumjobnamelist.add(addendumjobname);
                        jobrowcount = 1;
                    }
                } else {
                    jobrowcount++;
                    if (i == activityList.size() - 1) {
                        addendumjobname = new AddendumJobName();
                        addendumjobname
                                .setAddendumjobname(((BulkJobBean) activityList
                                        .get(i)).getTempjob_name());
                        addendumjobname.setJobrowcount(jobrowcount + "");
                        addendumjobnamelist.add(addendumjobname);
                    }

                }

                prevtempjob_id = ((BulkJobBean) activityList.get(i))
                        .getTempjob_id();
            }
            if (addendumjobnamelist != null) {
                if (addendumjobnamelist.size() > 0) {
                    request.setAttribute("firstdivrownum",
                            ((AddendumJobName) addendumjobnamelist.get(0))
                            .getJobrowcount());
                } else {
                    request.setAttribute("firstdivrownum", 0 + "");
                }
            }
			// request.setAttribute( "firstdivrownum" , ( ( AddendumJobName
            // )addendumjobnamelist.get(0) ).getJobrowcount() );
            request.setAttribute("addendumjobnamelist", addendumjobnamelist);
        }

        request.setAttribute("activityList", activityList);
        request.setAttribute("activityListSize",
                new Integer(activityList.size()));

        ArrayList initialStateCategory = new ArrayList();
        String tempCat = "";
        String[] tempList = null;
        String check = "";
		// start For setting Combo For State

		// / Pvsdao pvsDao= new Pvsdao();
        // if(pvsDao!=null){
        initialStateCategory = BulkJobCreationdao
                .getBulkJobStateList(getDataSource(request, "ilexnewDB"));

        if (initialStateCategory.size() > 0) {
            for (int i = 0; i < initialStateCategory.size(); i++) {
                tempCat = ((BulkJobCreationBean) initialStateCategory.get(i))
                        .getCountryId();
                ((BulkJobCreationBean) initialStateCategory.get(i))
                        .setTempList(SiteManagedao
                                .getCategoryWiseStateList(tempCat, i,
                                        getDataSource(request, "ilexnewDB")));
            }
        }
        request.setAttribute("initialStateCategory", initialStateCategory);

        /*
         * } else{
         *
         * } //End For setting Combo For State
         */
		// ArrayList activityList
        // =BulkJobCreationdao.getActivityList(appendixid,ds);
        DynamicComboCM dcGroupList = new DynamicComboCM();
        dcGroupList.setFirstlevelcatglist(DynamicComboDao
                .getGroupNameListSiteSearch(bulkjobform.getMsa_id(),
                        getDataSource(request, "ilexnewDB")));
        request.setAttribute("dcGroupList", dcGroupList);

        DynamicComboCM dcEndCustomerList = new DynamicComboCM();
        dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
                .getSiteEndCustomerList(getDataSource(request, "ilexnewDB"),
                        bulkjobform.getMsa_id(), bulkjobform.getMsaname()));
        request.setAttribute("dcEndCustomerList", dcEndCustomerList);

        String t1 = "";

        for (int j = 0; j < bulkjobform.getActivity_id().length; j++) {
            t1 = t1 + bulkjobform.getActivity_id(j) + ",";
        }

        StringBuffer sb = new StringBuffer();

        String selectedParam[];
        int returnflag = -1;
        BeanUtils.copyProperties(bulkJobBean, bulkjobform);
        LoginForm formdetail = null;
        String username = request.getRemoteUser();
        String jobtitle;
        String siteId;
        String siteName;
        if (bulkjobform.getSelectedparam() != null) {
            for (int i = 0; i < bulkjobform.getSelectedparam().length; i++) {
                returnflag = BulkJobCreationdao.saveSiteResult(bulkJobBean, i,
                        getDataSource(request, "ilexnewDB"), t1, loginuserid);

                /*
                 * String jobid = BulkJobCreationdao.getJobIdfromName(
                 * bulkjobform.getSelectedparam(i), getDataSource(request,
                 * "ilexnewDB"));
                 */
                siteId = BulkJobCreationdao.getSiteid(
                        bulkjobform.getSelectedparam(i),
                        getDataSource(request, "ilexnewDB"));
                siteName = BulkJobCreationdao.getSitename(
                        bulkjobform.getSelectedparam(i),
                        getDataSource(request, "ilexnewDB"));

                String jobid;

                if (bulkjobform.getPrifix().equals("P")) {

                    if (bulkjobform.getSite_name().trim().equals("N")) {

                        jobtitle = siteName + " " + bulkjobform.getJob_Name();
                    } else {

                        jobtitle = siteId + " " + bulkjobform.getJob_Name();

                    }

                } else {

                    if (bulkjobform.getPrifix().equals("N")) {

                        jobtitle = siteName + " " + bulkjobform.getJob_Name();
                    } else {

                        jobtitle = siteName + " " + siteId;

                    }

                }

                jobid = BulkJobCreationdao.getjobid(jobtitle, getDataSource(request, "ilexnewDB"));

                String requestemail = BulkJobCreationdao.getRequestorEmailByJobId(jobid, getDataSource(request, "ilexnewDB"));

                try {
                    formdetail = Logindao.getUserInfo(username, getDataSource(request, "ilexnewDB"));
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if ((jobid != null && !jobid.equals("")) || (requestemail != null && requestemail.equals(""))) {
                    BulkJobCreationdao.inserTORequestorEmail(jobid, formdetail.getLogin_user_email(), getDataSource(request, "ilexnewDB"));
                }

                uploadError = new UploadError();

                switch (returnflag) {
                    case 0:
                        uploadError.setRowno(i + 1 + "");
                        uploadError.setRowerror("success");
                        error.add(uploadError);
                        break;
                    case -9001:
                        uploadError.setRowno(i + 1 + "");
                        uploadError.setRowerror("Failed to create job.");
                        error.add(uploadError);
                        break;
                    case -9003:
                        uploadError.setRowno(i + 1 + "");
                        uploadError.setRowerror("Insert fail, job already exists.");
                        error.add(uploadError);
                        break;
                    case -9080:
                        uploadError.setRowno(i + 1 + "");
                        uploadError.setRowerror("Failed to copy Site information.");
                        error.add(uploadError);
                        break;
                    case -9081:
                        uploadError.setRowno(i + 1 + "");
                        uploadError.setRowerror("Failed to copy site information.");
                        error.add(uploadError);
                        break;
                    default:
                        uploadError.setRowno(i + 1 + "");
                        uploadError.setRowerror("success");
                        error.add(uploadError);
                }

            }
        }

        /*
         * request.setAttribute("activityList",activityList); String
         * temppath="FormBulkJobCreation"; request.setAttribute( "type" ,
         * temppath ); request.setAttribute( "appendix_Id" ,appendixid );
         * request.setAttribute( "isBukJobCreationView" , isBukJobCreationView);
         * request.setAttribute( "viewjobtype" , bulkjobform.getViewjobtype() );
         * request.setAttribute( "ownerId" , bulkjobform.getOwnerId() );
         * request.setAttribute( "copyactivityflag" , returnflag+"" );
         *
         * Atul start request.setAttribute( "sitesSize" , new Integer(sitesSize)
         * ); request.setAttribute( "isBukJobCreationView" ,
         * isBukJobCreationView); request.setAttribute( "JobSaved" , "1" );
         * //return( mapping.findForward( "createBulkJob" ) );
         */
        request.setAttribute("isBukJobCreationView", isBukJobCreationView);
        request.setAttribute("JobSaved", "1");
        request.setAttribute("viewjobtype", bulkjobform.getViewjobtype());
        request.setAttribute("ownerId", bulkjobform.getOwnerId());
        request.setAttribute("Appendix_Id", appendixid);
        request.setAttribute("msaname", bulkjobform.getMsaname());
        request.setAttribute("errorlist", error);

        session.setAttribute("userid", formdetail.getLogin_user_id());
        session.setAttribute("useremail", formdetail.getLogin_user_email());
        session.setAttribute("username", formdetail.getLogin_user_name());

        ActionForward forward = setViewSelector(mapping, bulkjobform, request,
                response);
        // return( forward );
        return mapping.findForward("bulkerror");
        // return( mapping.findForward( "temppage" ) );
    }

    public ActionForward setViewSelector(ActionMapping mapping,
            BulkJobCreationForm bulkjobform, HttpServletRequest request,
            HttpServletResponse response) {
        /* Start : Appendix Dashboard View Selector Code */
        ArrayList jobOwnerList = new ArrayList();
        ArrayList jobStatusList = new ArrayList();
        String jobSelectedStatus = "";
        String jobSelectedOwner = "";
        String jobTempStatus = "";
        String jobTempOwner = "";
        int jobOwnerListSize = 0;

        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
		// CONDITION CHECK
		/* String username = request.getRemoteUser(); */

        /*
         * session.setAttribute("userid", formdetail.getLogin_user_id());
         * session.setAttribute("useremail", formdetail.getLogin_user_email());
         * session.setAttribute("username", formdetail.getLogin_user_name());
         */
        /* Start : Appendix Dashboard View Selector Code */
        if (bulkjobform.getJobOwnerOtherCheck() != null
                && !bulkjobform.getJobOwnerOtherCheck().equals("")) {
            session.setAttribute("jobOwnerOtherCheck",
                    bulkjobform.getJobOwnerOtherCheck());
        }

        if (request.getParameter("opendiv") != null) {
            request.setAttribute("opendiv", request.getParameter("opendiv"));
        }
        if (request.getParameter("resetList") != null
                && request.getParameter("resetList").equals("something")) {
            WebUtil.setDefaultAppendixDashboardAttribute(session);
            request.setAttribute("type", "AppendixViewSelector");
            request.setAttribute("appendixId", bulkjobform.getAppendix_Id());
            request.setAttribute("msaId", bulkjobform.getMsa_id());

            return mapping.findForward("temppage");

        }

        if (bulkjobform.getJobSelectedStatus() != null) {
            for (int i = 0; i < bulkjobform.getJobSelectedStatus().length; i++) {
                jobSelectedStatus = jobSelectedStatus + "," + "'"
                        + bulkjobform.getJobSelectedStatus(i) + "'";
                jobTempStatus = jobTempStatus + ","
                        + bulkjobform.getJobSelectedStatus(i);
            }

            jobSelectedStatus = jobSelectedStatus.substring(1,
                    jobSelectedStatus.length());
            jobSelectedStatus = "(" + jobSelectedStatus + ")";
        }

        if (bulkjobform.getJobSelectedOwners() != null) {
            for (int j = 0; j < bulkjobform.getJobSelectedOwners().length; j++) {
                jobSelectedOwner = jobSelectedOwner + ","
                        + bulkjobform.getJobSelectedOwners(j);
                jobTempOwner = jobTempOwner + ","
                        + bulkjobform.getJobSelectedOwners(j);
            }
            jobSelectedOwner = jobSelectedOwner.substring(1,
                    jobSelectedOwner.length());
            if (jobSelectedOwner.equals("0")) {
                jobSelectedOwner = "%";
            }
            jobSelectedOwner = "(" + jobSelectedOwner + ")";
        }

        if (request.getParameter("showList") != null
                && request.getParameter("showList").equals("something")) {

            session.setAttribute("jobSelectedOwners", jobSelectedOwner);
            session.setAttribute("jobTempOwner", jobTempOwner);
            session.setAttribute("jobSelectedStatus", jobSelectedStatus);
            session.setAttribute("jobTempStatus", jobTempStatus);
            session.setAttribute("timeFrame",
                    bulkjobform.getJobMonthWeekCheck());
            session.setAttribute("jobOwnerOtherCheck",
                    bulkjobform.getJobOwnerOtherCheck());

            request.setAttribute("type", "AppendixViewSelector");
            request.setAttribute("appendixId", bulkjobform.getAppendix_Id());
            request.setAttribute("msaId", bulkjobform.getMsa_id());

            return mapping.findForward("temppage");

        } else {
            if (session.getAttribute("jobSelectedOwners") != null) {
                jobSelectedOwner = session.getAttribute("jobSelectedOwners")
                        .toString();
                bulkjobform.setJobSelectedOwners(session
                        .getAttribute("jobTempOwner").toString().split(","));
            }
            if (session.getAttribute("jobSelectedStatus") != null) {
                jobSelectedStatus = session.getAttribute("jobSelectedStatus")
                        .toString();
                bulkjobform.setJobSelectedStatus(session
                        .getAttribute("jobTempStatus").toString().split(","));
            }
            if (session.getAttribute("jobOwnerOtherCheck") != null) {
                bulkjobform.setJobOwnerOtherCheck(session.getAttribute(
                        "jobOwnerOtherCheck").toString());
            }
            if (session.getAttribute("timeFrame") != null) {
                bulkjobform.setJobMonthWeekCheck(session.getAttribute(
                        "timeFrame").toString());
            }
        }
        jobOwnerList = MSAdao.getOwnerList(loginuserid,
                bulkjobform.getJobOwnerOtherCheck(), "prj_job_new",
                bulkjobform.getAppendix_Id(),
                getDataSource(request, "ilexnewDB"));
        jobStatusList = MSAdao.getStatusList("prj_job_new",
                getDataSource(request, "ilexnewDB"));

        if (jobOwnerList.size() > 0) {
            jobOwnerListSize = jobOwnerList.size();
        }

        request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
        request.setAttribute("jobStatusList", jobStatusList);
        request.setAttribute("jobOwnerList", jobOwnerList);
        /* End : Appendix Dashboard View Selector Code */
        return (mapping.findForward("createBulkJob"));

    }

}
