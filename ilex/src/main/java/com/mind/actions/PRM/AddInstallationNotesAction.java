package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.AddInstallationNotesdao;
import com.mind.formbean.PRM.AddInstallationNotesForm;

public class AddInstallationNotesAction extends com.mind.common.IlexDispatchAction{

	public ActionForward add (ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception
	{
		
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddInstallationNotesForm addInstallationNotesForm = (AddInstallationNotesForm) form;
		
		if( request.getParameter( "from" ) != null ) {
			addInstallationNotesForm.setFrom( request.getParameter( "from" ) );
		}
		
		if(request.getParameter("nettype") != null)  { 
			addInstallationNotesForm.setNettype(""+request.getParameter("nettype")); 
		}
		
		if(request.getParameter("viewjobtype") != null)  { 
			addInstallationNotesForm.setViewjobtype(""+request.getParameter("viewjobtype")); 
		}
		else
		{
			addInstallationNotesForm.setViewjobtype("A");
		}
		
		if(request.getParameter("ownerId") != null)  { 
			addInstallationNotesForm.setOwnerId(""+request.getParameter("ownerId")); 
		}
		else
		{
			addInstallationNotesForm.setOwnerId("%");
		}
		
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if( addInstallationNotesForm.getAuthenticate().equals( "" ) )
		{
			if( addInstallationNotesForm.getFrom().equals( "appendixdashboard" ) )
			{
				if( !Authenticationdao.getPageSecurity( userid , "Manage Job" , "Add Installation Notes" , getDataSource( request , "ilexnewDB" ) ) ) 
				{
					return( mapping.findForward( "UnAuthenticate" ) );	
				}
			}
			else
			{
				//code for security from jobdashboard
			}
		}
		/* Page Security End*/
		
		int flag=0;
		int statusvalue = -1;
		String appendixid="";
		String jobid="";
		String notes="";
		
		String addmessage="";
		String appendixname="";
		String jobname="";
		
		
		
		
		if(addInstallationNotesForm.getSave()!=null)
		{
			appendixid =addInstallationNotesForm.getAppendixid() ;
			jobid = addInstallationNotesForm.getJobid() ;
			notes = addInstallationNotesForm.getInstallnotes();
			appendixname=AddInstallationNotesdao.getAppendixname(appendixid,getDataSource( request,"ilexnewDB" ));
			addInstallationNotesForm.setAppendixname(appendixname);
			jobname=AddInstallationNotesdao.getJobname(jobid,getDataSource( request,"ilexnewDB" ));
			addInstallationNotesForm.setJobname(jobname);
			//System.out.println("pocid,custpocid,typeid"+pocid+","+custpocid+","+salespocid+","+billingpocid+","+businesspocid+","+contractpocid+","+typeid);
			statusvalue=AddInstallationNotesdao.addinstallnotes(jobid,notes,userid,getDataSource( request,"ilexnewDB" ),"","0","0","");		
			request.setAttribute("addmessage",statusvalue+"");
			
			if(statusvalue==0)
			{
				flag=1;
				String temppath="PRMJobInstallNotes";
				request.setAttribute( "type" , temppath );
				request.setAttribute( "appendix_Id" , addInstallationNotesForm.getAppendixid() );
				request.setAttribute( "viewjobtype" , addInstallationNotesForm.getViewjobtype() );
				request.setAttribute( "ownerId" , addInstallationNotesForm.getOwnerId() );
				
				if( addInstallationNotesForm.getFrom().equals( "appendixdashboard" ) )
				{
					return( mapping.findForward( "temppage" ) );	
				}
				else
				{
					request.setAttribute( "jobid" , addInstallationNotesForm.getJobid() );
					return mapping.findForward( "jobdashboard" );
				}
								
			}
			else
			{
				forward = mapping.findForward("success");
			}
			
		}
		else
		{
			appendixid = request.getParameter("appendixid");
			
			jobid = request.getParameter("jobid");
			appendixname = AddInstallationNotesdao.getAppendixname(appendixid,getDataSource( request,"ilexnewDB" ));
			addInstallationNotesForm.setAppendixname(appendixname);
			jobname = AddInstallationNotesdao.getJobname(jobid,getDataSource( request,"ilexnewDB" ));
			addInstallationNotesForm.setJobname(jobname);
			forward = mapping.findForward("success");
		}
		
		
		request.setAttribute( "appendixname" , appendixname);
		request.setAttribute( "appendixid" , appendixid);
		request.setAttribute( "jobid" , jobid);
		
		request.setAttribute( "viewflag" , flag+"" );	
		
		return (forward);
	}
	
	
}
