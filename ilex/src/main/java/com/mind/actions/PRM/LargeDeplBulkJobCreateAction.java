package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboPRM;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.MPOList;
import com.mind.bean.newjobdb.MasterPurchaseOrder;
import com.mind.bean.prm.BulkJobCreationBean;
import com.mind.common.Util;
import com.mind.common.bean.BulkJobBean;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AddendumJobName;
import com.mind.dao.PRM.AssignOwnerdao;
import com.mind.dao.PRM.BulkJobCreationdao;
import com.mind.formbean.PRM.BulkJobCreationForm;
import com.mind.newjobdb.dao.MPOCopier;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.newjobdb.dao.PurchaseOrderList;
import com.mind.util.WebUtil;

public class LargeDeplBulkJobCreateAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		BulkJobCreationForm bulkjobform = (BulkJobCreationForm) form;
		BulkJobCreationBean bulkjobBean = new BulkJobCreationBean();
		DynamicComboPRM dynamicComboOwner = new DynamicComboPRM();

		int jobrowcount = 1;

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");

		String prevtempjob_id = "";
		ArrayList addendumjobnamelist = new ArrayList();
		AddendumJobName addendumjobname = null;

		if (request.getParameter("Appendix_Id") != null) {
			bulkjobform.setAppendix_Id(request.getParameter("Appendix_Id"));
		}
		String valueFrom = "";
		String ref = "";
		String searchedPartner = "";
		if (request.getParameter("form") != null) {
			valueFrom = request.getParameter("form");
		}
		if (request.getParameter("searchPartner") != null) {
			searchedPartner = request.getParameter("searchPartner");
		}
		if (request.getParameter("ref") != null) {
			ref = request.getParameter("ref");
		}

		if (request.getParameter("fromProposalMg") != null) {
			bulkjobform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					(String) request.getParameter("MSA_Id")));
			request.setAttribute("fromProposalMg", "Yes");
		}
		if (request.getParameter("viewjobtype") != null) {
			bulkjobform.setViewjobtype(request.getParameter("viewjobtype"));
		} else {
			bulkjobform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			bulkjobform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			bulkjobform.setOwnerId("%");
		}

		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		ArrayList schedulerList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		/* Start : View Selector Code */

		if (bulkjobform.getJobOwnerOtherCheck() != null
				&& !bulkjobform.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					bulkjobform.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", bulkjobform.getAppendix_Id());
			request.setAttribute("msaId", bulkjobform.getMsa_id());
			return mapping.findForward("temppage");
		}

		if (bulkjobform.getJobSelectedStatus() != null) {
			for (int i = 0; i < bulkjobform.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ bulkjobform.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ bulkjobform.getJobSelectedStatus(i);
			}
			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (bulkjobform.getJobSelectedOwners() != null) {
			for (int j = 0; j < bulkjobform.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ bulkjobform.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ bulkjobform.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {
			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					bulkjobform.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					bulkjobform.getJobOwnerOtherCheck());
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", bulkjobform.getAppendix_Id());
			request.setAttribute("msaId", bulkjobform.getMsa_id());
			return mapping.findForward("temppage");
		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				bulkjobform.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				bulkjobform.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				bulkjobform.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				bulkjobform.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}
		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				bulkjobform.getJobOwnerOtherCheck(), "prj_job_new",
				bulkjobform.getAppendix_Id(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));
		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();
		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		bulkjobform.setAppendixname(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				bulkjobform.getAppendix_Id()));
		bulkjobform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"),
				IMdao.getMSAId(bulkjobform.getAppendix_Id(),
						getDataSource(request, "ilexnewDB"))));
		bulkjobform.setMsa_id(IMdao.getMSAId(bulkjobform.getAppendix_Id(),
				getDataSource(request, "ilexnewDB")));

		ArrayList activityList = BulkJobCreationdao.getActivityList(
				bulkjobform.getAppendix_Id(),
				getDataSource(request, "ilexnewDB"));
		if (activityList.size() > 0) {
			for (int i = 0; i < activityList.size(); i++) {
				if (!prevtempjob_id.equals(((BulkJobBean) activityList.get(i))
						.getTempjob_id())) {
					if (prevtempjob_id != "") {
						addendumjobname = new AddendumJobName();
						addendumjobname
								.setAddendumjobname(((BulkJobBean) activityList
										.get(i - 1)).getTempjob_name());
						addendumjobname.setJobrowcount(jobrowcount + "");
						addendumjobnamelist.add(addendumjobname);
						jobrowcount = 1;
					}
				} else {
					jobrowcount++;
					if (i == activityList.size() - 1) {
						addendumjobname = new AddendumJobName();
						addendumjobname
								.setAddendumjobname(((BulkJobBean) activityList
										.get(i)).getTempjob_name());
						addendumjobname.setJobrowcount(jobrowcount + "");
						addendumjobnamelist.add(addendumjobname);
					}
				}

				prevtempjob_id = ((BulkJobBean) activityList.get(i))
						.getTempjob_id();
			}

			if (addendumjobnamelist != null) {
				if (addendumjobnamelist.size() > 0)
					request.setAttribute("firstdivrownum",
							((AddendumJobName) addendumjobnamelist.get(0))
									.getJobrowcount());
				else
					request.setAttribute("firstdivrownum", 0 + "");
			}
			request.setAttribute("addendumjobnamelist", addendumjobnamelist);
		}

		request.setAttribute("activityList", activityList);
		request.setAttribute("activityListSize",
				new Integer(activityList.size()));

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(bulkjobform.getAppendix_Id(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		ownerList = AssignOwnerdao.getSchedulerList(
				bulkjobform.getAppendix_Id(), "Job Owner",
				getDataSource(request, "ilexnewDB"));
		schedulerList = AssignOwnerdao.getSchedulerList(
				bulkjobform.getAppendix_Id(), "Scheduler",
				getDataSource(request, "ilexnewDB"));
		if (ownerList.size() > 1) {
			dynamicComboOwner.setAllOwnerName(ownerList);
			request.setAttribute("dyComboPRMOwner", dynamicComboOwner);
		}
		if (schedulerList.size() > 1) {
			dynamicComboOwner.setSchedulerList(schedulerList);
			request.setAttribute("dyComboPRMScheduler", dynamicComboOwner);
		}
		String jobName = null;
		String t1 = "";
		if (bulkjobform.getActivity_id() != null
				&& !searchedPartner.equals("Y")) {
			for (int j = 0; j < bulkjobform.getActivity_id().length; j++) {
				t1 = t1 + bulkjobform.getActivity_id(j) + ",";
			}
			bulkjobform.setFormatedActivityList(t1);
		}

		if (bulkjobform.getSave() != null
				|| (ref.equals("saveSiteSearch") && valueFrom
						.equals("MPOandPartner"))) {
			StringTokenizer stocken = new StringTokenizer(bulkjobform
					.getParagraph_content().toString(), "\n");

			int countTokens = stocken.countTokens();
			if (ref.equals("saveSiteSearch")
					&& valueFrom.equals("MPOandPartner")) {
				t1 = bulkjobform.getFormatedActivityList();
			}
			String siteRow = null;
			int i = 0;
			int j = 0;
			int k = 0;
			String[] siteValuesArr = null;
			String[][] siteInputError = new String[countTokens][2];
			String[][] siteNotFoundError = new String[countTokens][2];
			String checkReturn = "";
			ArrayList siteInputErrorList = new ArrayList();
			ArrayList siteNotFoundErrorList = new ArrayList();
			ArrayList createdJobList = new ArrayList();
			ArrayList notCreatedJobList = new ArrayList();
			int l = 0;
			int m = 0;
			int countjob = 0;
			int countBlank = 0;
			String str = "";
			while (stocken.hasMoreTokens()) {
				if (i != 0)
					siteRow = stocken.nextToken();
				if (countTokens > 0) {
					if (i == 0) {
						siteRow = stocken.nextToken();
					}
				} else {
					request.setAttribute("BalnkTextBox", "balnktextbox");
				}

				siteValuesArr = siteRow.split("\t");

				if (siteRow.equals("\r")
						|| siteRow.split("\t").length == siteRow.length()
						|| (siteRow.length() > 2
								&& siteValuesArr[0].trim().equals("") && siteValuesArr[1]
								.trim().equals(""))) {
					countBlank++;
				} else {
					countjob++;
					int returnflag = -1;
					if ((siteRow.contains("\t") || siteRow.contains("\r"))
							&& (siteValuesArr.length == 2 || siteValuesArr.length == 1)
							&& ((countTokens > 1 && siteRow.contains("\r")) || countTokens == 1)) {
						if (str.equals("")) {
							str = "";
						} else {
							str = str + "\n";
						}
						checkReturn = BulkJobCreationdao.checkDate(
								siteValuesArr, bulkjobform.getAppendix_Id(),
								getDataSource(request, "ilexnewDB"));
						if (checkReturn != null && checkReturn.equals("true")) {
							if (siteValuesArr.length == 2) {
								bulkjobform.setStartDate(siteValuesArr[1]);
								bulkjobform.setSiteNumber(siteValuesArr[0]);
							} else {
								bulkjobform.setStartDate("");
								bulkjobform.setSiteNumber(siteValuesArr[0]);
							}
							bulkjobform.setSite_name("Y");
							BeanUtils.copyProperties(bulkjobBean, bulkjobform);
							returnflag = BulkJobCreationdao.saveSiteResult(
									bulkjobBean, 0,
									getDataSource(request, "ilexnewDB"), t1,
									loginuserid);
							jobName = BulkJobCreationdao
									.getJobName(bulkjobBean);
							if (returnflag == 0) {
								createdJobList.add(l, jobName);
								String jobId = BulkJobCreationdao
										.getJobIdfromName(
												jobName,
												getDataSource(request,
														"ilexnewDB"));
								bulkjobform.setJobId(jobId);
								if (ref.equals("saveSiteSearch")
										&& valueFrom.equals("MPOandPartner")) {
									String pId = "";
									if (bulkjobform.getParnerIdList() != null
											&& !bulkjobform.getParnerIdList()[0]
													.equals("")) {
										pId = bulkjobform.getParnerIdList()[0];
									}
									if (bulkjobform.getMpoIdSelected() != null
											&& !bulkjobform.getMpoIdSelected()
													.equals("")) {
										createNewPOandAssignPartner(
												bulkjobform.getMpoIdSelected(),
												loginuserid,
												bulkjobform,
												pId,
												getDataSource(request,
														"ilexnewDB"));
									}
								}
								l++;
							} else {
								notCreatedJobList.add(m,
										bulkjobform.getSiteNumber() + " "
												+ bulkjobform.getStartDate());
								m++;
								if (!bulkjobform.getStartDate().equals("")) {
									str = str + bulkjobform.getSiteNumber()
											+ "\t" + bulkjobform.getStartDate();
								} else {
									str = str + bulkjobform.getSiteNumber();
								}

							}

						} else if (checkReturn != null
								&& checkReturn.equals("sitenotfound")) {
							if (siteValuesArr.length == 2) {
								siteNotFoundError[k][0] = siteValuesArr[0];
								siteNotFoundError[k][1] = siteValuesArr[1];
							} else {
								siteNotFoundError[k][0] = siteValuesArr[0];
								siteNotFoundError[k][1] = "";
							}
							if (!siteNotFoundError[k][1].equals("")) {
								str = str + siteNotFoundError[k][0] + "\t"
										+ siteNotFoundError[k][1];
							} else {
								str = str + siteNotFoundError[k][0];
							}

							k++;
						} else {
							checkReturn = "inputError";
						}
					} else {
						siteValuesArr = siteRow.split(" ");
						if (siteValuesArr.length > 0) {
							if (siteValuesArr.length == 2) {
								siteInputError[j][0] = siteValuesArr[0] + ""
										+ siteValuesArr[1];
								siteInputError[j][1] = "";
							} else {
								siteInputError[j][0] = siteValuesArr[0];
								siteInputError[j][1] = "";
							}
						} else {
							siteInputError[j][0] = "";
							siteInputError[j][1] = "";
						}

						if (!siteInputError[j][1].equals("")) {
							str = str + siteInputError[j][0] + "\t"
									+ siteInputError[j][1];
						} else {
							str = str + siteInputError[j][0];
						}
						j++;
					}
					if (checkReturn.equals("inputError")) {
						if (siteValuesArr.length == 2) {
							siteInputError[j][0] = siteValuesArr[0];
							siteInputError[j][1] = siteValuesArr[1];
						} else {
							siteInputError[j][0] = siteValuesArr[0];
							siteInputError[j][1] = "";
						}
						if (!siteInputError[j][1].equals("")) {
							str = str + siteInputError[j][0] + "\t"
									+ siteInputError[j][1];
						} else {
							str = str + siteInputError[j][0];
						}
						j++;
					}
				}
				i++;
			}

			siteInputErrorList = BulkJobCreationdao.getValidationError(
					siteInputError, j);
			siteNotFoundErrorList = BulkJobCreationdao.getValidationError(
					siteNotFoundError, k);
			int x = 0;
			int y = 0;
			int z = 0;
			int p = 0;
			if (siteInputErrorList.size() > 0) {
				request.setAttribute("siteInputErrorList", siteInputErrorList);
				x = siteInputErrorList.size();
			}
			request.setAttribute("siteInputErrorListSize",
					siteInputErrorList.size());
			if (siteNotFoundErrorList.size() > 0) {
				request.setAttribute("siteNotFoundErrorList",
						siteNotFoundErrorList);
				y = siteNotFoundErrorList.size();
			}
			request.setAttribute("siteNotFoundErrorListSize",
					siteNotFoundErrorList.size());
			if (createdJobList.size() > 0) {
				request.setAttribute("createdJobList", createdJobList);
				z = createdJobList.size();
			}
			request.setAttribute("createdJobListSize", createdJobList.size());
			if (notCreatedJobList.size() > 0) {
				request.setAttribute("notCreatedJobList", notCreatedJobList);
				p = notCreatedJobList.size();
			}
			request.setAttribute("notCreatedJobListSize",
					notCreatedJobList.size());
			if (x != 1 || y != 1 || z != 1 || p != 1) {
				if (x > 0 || y > 0 || p > 0) {
					request.setAttribute("error", "error");
					if (x + y + p < 15) {
						request.setAttribute("Sizeoffirstdiv", x + y + p + 6);
					} else {
						request.setAttribute("Sizeoffirstdiv", x + y + p + 2);
					}
					bulkjobform.setParagraph_content(str);
				}
			}
			if (createdJobList.size() == countjob) {
				bulkjobform.setParagraph_content("");
				request.setAttribute("Sizeoffirstdiv", 3);
			}
			request.setAttribute("CreateJob", "CreateJob");
		}
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				bulkjobform.getAppendix_Id(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("viewjobtype", bulkjobform.getViewjobtype());
		request.setAttribute("ownerId", bulkjobform.getOwnerId());
		request.setAttribute("msaId", bulkjobform.getMsa_id());
		request.setAttribute("appendixid", bulkjobform.getAppendix_Id());
		request.setAttribute("msaName", bulkjobform.getMsaname());
		request.setAttribute("appendixName", bulkjobform.getAppendixname());
		ArrayList listOfMPO = new ArrayList();
		ArrayList poTypeList = new ArrayList();
		MPOList mpoList = BulkJobCreationdao.getListOfMPO(
				bulkjobform.getAppendix_Id(),
				getDataSource(request, "ilexnewDB"));
		listOfMPO = mpoList.getMasterPurchaseOrders();
		if (!ref.equals("saveSiteSearch") && valueFrom.equals("MPOandPartner")) {
			bulkjobform.setScheduleDate(IlexTimestamp.getSystemDateTime()
					.getDate());
			bulkjobform.setMpoList(listOfMPO);
			setFormFromMasterPurchaseOrders(listOfMPO, bulkjobform);
			poTypeList = BulkJobCreationdao.getPOTypeList();
			bulkjobform.setPoTypeList(poTypeList);

			if (request.getParameter("searchPartner") != null
					&& request.getParameter("searchPartner").equals("Y")) {
				ArrayList pList = BulkJobCreationdao.getSearchedPartnerList(
						bulkjobform.getPartnerName(),
						bulkjobform.getKeyWords(),
						getDataSource(request, "ilexnewDB"));
				bulkjobform.setPartnerList(pList);
				setFormFromPArtnerList(pList, bulkjobform);
				request.setAttribute("partnerSize", pList.size());
			}
			return (mapping.findForward("MPOandPartner"));

		}
		request.setAttribute("mpoListSize", listOfMPO.size() + "");
		return (mapping.findForward("success"));
	}

	public static void setFormFromMasterPurchaseOrders(
			ArrayList<MasterPurchaseOrder> listOfMasterPurchaseOrder,
			BulkJobCreationForm form) {
		String mpoId[] = new String[listOfMasterPurchaseOrder.size()];
		String mpoName[] = new String[listOfMasterPurchaseOrder.size()];
		String masterPOItem[] = new String[listOfMasterPurchaseOrder.size()];
		String estimatedTotalCost[] = new String[listOfMasterPurchaseOrder
				.size()];
		String status[] = new String[listOfMasterPurchaseOrder.size()];
		String isSelectable[] = new String[listOfMasterPurchaseOrder.size()];
		for (int i = 0; i < listOfMasterPurchaseOrder.size(); i++) {

			MasterPurchaseOrder masterPurchaseOrder = listOfMasterPurchaseOrder
					.get(i);
			mpoId[i] = masterPurchaseOrder.getMpoId();
			mpoName[i] = masterPurchaseOrder.getMpoName();
			masterPOItem[i] = masterPurchaseOrder.getMasterPOItem();
			estimatedTotalCost[i] = String.valueOf(masterPurchaseOrder
					.getEstimatedTotalCost());
			status[i] = masterPurchaseOrder.getStatus();
			isSelectable[i] = Util.getStringval(masterPurchaseOrder
					.isSelectable());
		}
		form.setMpoId(mpoId);
		form.setMpoName(mpoName);
		form.setMasterPOItem(masterPOItem);
		form.setEstimatedTotalCost(estimatedTotalCost);
		form.setStatus(status);
		form.setIsSelectable(isSelectable);
	}

	public static void setFormFromPArtnerList(ArrayList list,
			BulkJobCreationForm form) {
		String partnerId[] = new String[list.size()];

		for (int i = 0; i < list.size(); i++) {
			BulkJobCreationBean bulkJobform = (BulkJobCreationBean) list.get(i);
			partnerId[i] = bulkJobform.getPartnerId();
		}
		form.setParnerIdList(partnerId);
	}

	public static void createNewPOandAssignPartner(String mpoId,
			String loginuserid, BulkJobCreationForm form, String partnerId,
			DataSource ds) {
		if (mpoId != null && !mpoId.equals("")) {
			boolean isValid = false;

			String flag = MPOCopier
					.validateCopyToPO(mpoId, form.getJobId(), ds);
			if (flag.equalsIgnoreCase("True"))
				isValid = true;
			if (isValid) {
				String newPoWoId = PurchaseOrderList.copyNewPOFromMPO(
						form.getJobId(), loginuserid, "Add", mpoId, "", "", "",
						"", ds);
				IlexTimestamp deliveryTimeStamp = IlexTimestamp
						.converToTimestamp(form.getDeliverDate());
				IlexTimestamp issueTimeStamp = IlexTimestamp
						.converToTimestamp(form.getScheduleDate());

				// Copying PO info from mpo: start
				PurchaseOrder purchaseOrder = MPOCopier
						.getPurchaseOrderFromMPO(newPoWoId, mpoId,
								form.getJobId(), deliveryTimeStamp,
								issueTimeStamp, ds);
				PurchaseOrder.savePOAuthorizedCostInfo(purchaseOrder,
						loginuserid, ds);
				PurchaseOrder.savePurchaseWorkOrder(purchaseOrder, loginuserid,
						ds);
				PurchaseOrder.savePODocuments(purchaseOrder, loginuserid, ds);
				PurchaseOrder
						.savePODeliverables(purchaseOrder, loginuserid, ds);
				// Copying PO info from mpo: end

				if (newPoWoId != null && !newPoWoId.equals("")
						&& (partnerId != null && !partnerId.equals(""))) {
					PurchaseOrderDAO.upsertPurchaseOrder(form.getJobId(),
							loginuserid, "Update", "", newPoWoId, partnerId,
							"", "", ds);
				}
				if (form.getPoType() != null && !form.getPoType().equals("0")) {
					BulkJobCreationdao.updatePOType(form.getPoType(),
							newPoWoId, loginuserid, ds);
				}
			}

		}
	}
}
