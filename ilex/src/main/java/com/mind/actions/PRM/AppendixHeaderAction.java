/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is an action class used for performing multiple
 * operations like listing of jobs,job dashboard view divided into different sections in project manager.
 *
 */
package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.pm.Appendix;
import com.mind.bean.prm.AppendixHeaderBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.NetMedXDashboarddao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.formbean.PRM.AppendixHeaderForm;
import com.mind.util.WebUtil;

public class AppendixHeaderAction extends com.mind.common.IlexDispatchAction {

	/**
	 * This method provides all functionalities needed for a complete dashboard
	 * view of a appendix in a project manager.
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionForward forward = new ActionForward();
		AppendixHeaderForm appendixHeaderForm = (AppendixHeaderForm) form;
		String userId = null;
		String status = null;
		String tempCat = "";
		ArrayList initialStateCategory = new ArrayList();
		/**
		 * * Code added for the View Selecotr **
		 */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		Map<String, Object> sessionMap;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired
		String loginUserId = (String) session.getAttribute("userid");
		if (appendixHeaderForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(loginUserId,
					"Manage Project", "View Appendix Dashboard",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		String apiAccessValue = "";

		if (request.getParameter("apiAccess") != null) {
			apiAccessValue = request.getParameter("apiAccess");
		}

		// for invoice manager bach button
		if (request.getParameter("inv_id") != null) {
			appendixHeaderForm.setInv_id(request.getParameter("inv_id"));
		}
		if (request.getParameter("inv_type") != null) {
			appendixHeaderForm.setInv_type(request.getParameter("inv_type"));
		}
		if (request.getParameter("inv_rdofilter") != null) {
			appendixHeaderForm.setInv_rdofilter(request
					.getParameter("inv_rdofilter"));
		}
		if (request.getParameter("invoice_Flag") != null) {
			appendixHeaderForm.setInvoice_Flag(request
					.getParameter("invoice_Flag"));
		}
		if (request.getParameter("partner_name") != null) {
			appendixHeaderForm.setPartner_name(request
					.getParameter("partner_name"));
		}
		if (request.getParameter("powo_number") != null) {
			appendixHeaderForm.setPowo_number(request
					.getParameter("powo_number"));
		}
		if (request.getParameter("from_date") != null) {
			appendixHeaderForm.setFrom_date(request.getParameter("from_date"));
		}
		if (request.getParameter("to_date") != null) {
			appendixHeaderForm.setTo_date(request.getParameter("to_date"));
		}
		if (request.getParameter("invoiceno") != null) {
			appendixHeaderForm.setInvoiceno(request.getParameter("invoiceno"));
		}

		ArrayList list = new ArrayList();
		String sortqueryclause = "";
		String appendixname = "";
		String msaname = "";
		String appendixcurrentstatus = "";
		String[] plsummary = new String[12];
		String[] contactsummary = new String[6];
		String[] pocdetails = new String[12];
		String[] custcnspocdetails = new String[5];

		String changeStatusFlag = null;
		String statusValue = null;

		String[] cnsPrjManangerDetail = new String[5];
		String[] teamLeadDetail = new String[5];

		/* Set value for refersh the tree */
		if (request.getAttribute("changestatusflag") != null) {
			if (request.getAttribute("status") != null) {
				statusValue = request.getAttribute("status") + "";
			}

			if (statusValue.equals("C") || statusValue.equals("F")) {
				changeStatusFlag = "true";
			}
		}

		// set job filter type start
		if (request.getParameter("from") != null) {
			appendixHeaderForm.setFrom(request.getParameter("from"));
		}

		if (request.getParameter("fromMyJob") != null) {
			appendixHeaderForm.setFrom(request.getParameter("fromMyJob"));
		}

		// Added to come back from the job edit page
		if (request.getAttribute("Appendix_Id") != null) {
			appendixHeaderForm.setAppendixid(request
					.getAttribute("Appendix_Id").toString());
			request.setAttribute("appendixid",
					appendixHeaderForm.getAppendixid());
		}

		if (request.getAttribute("msaId") != null) {
			appendixHeaderForm.setMsaId(request.getAttribute("msaId")
					.toString());
			session.setAttribute("msaId", request.getAttribute("msaId")
					.toString());
			request.setAttribute("msaId", appendixHeaderForm.getMsaId());
		}
		// end

		if (request.getParameter("viewjobtype") != null) {
			appendixHeaderForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
			// adding 20070903
		} else if (request.getAttribute("viewjobtype") != null) {
			appendixHeaderForm.setViewjobtype(request.getAttribute(
					"viewjobtype").toString());
		} else {
			appendixHeaderForm.setViewjobtype("All");
		}
		// set job filter type end

		if (request.getParameter("appendixid") != null) {
			appendixHeaderForm
					.setAppendixid(request.getParameter("appendixid"));
		}
		// forward to netmedx appendix dashboard

		if (appendixHeaderForm.getAppendixid().equals("-1")) {
			forward = mapping.findForward("netmedxdashboard");
			return (forward);
		}

		if (request.getParameter("ownerId") != null) {
			appendixHeaderForm.setOwnerId(request.getParameter("ownerId"));
			// adding 20070903
			session.setAttribute("ownerId", request.getParameter("ownerId"));
		} else if (request.getAttribute("ownerId") != null) {
			appendixHeaderForm.setOwnerId(request.getAttribute("ownerId")
					.toString());
			session.setAttribute("ownerId", request.getAttribute("ownerId")
					.toString());
		} else {
			appendixHeaderForm.setOwnerId("All");
		}

		if (request.getParameter("msaId") != null
				&& !request.getParameter("msaId").equals("")) {
			appendixHeaderForm.setMsaId(request.getParameter("msaId"));
			session.setAttribute("msaId", request.getParameter("msaId"));
		} else if (session.getAttribute("msaId") != null) {
			appendixHeaderForm.setMsaId(session.getAttribute("msaId")
					.toString());
		}

		String appendixId = "";
		if (appendixHeaderForm.getAppendixid().equals("0")
				&& request.getParameter("searchType") != null) {
			if (request.getParameter("searchType").equals("ticket")) {
				appendixId = Appendixdao.getNetmedxAppendixid(
						appendixHeaderForm.getMsaId(),
						getDataSource(request, "ilexnewDB"));
			}
			appendixHeaderForm.setAppendixid(appendixId);
			request.setAttribute("appendixid",
					appendixHeaderForm.getAppendixid());
		}

		if (request.getParameter("fromPage") != null) {
			appendixHeaderForm.setFromPage(request.getParameter("fromPage"));
			session.setAttribute("fromPage", request.getParameter("fromPage"));
		} else if (session.getAttribute("fromPage") != null) {
			appendixHeaderForm.setFromPage(session.getAttribute("fromPage")
					.toString());
		}

		if (appendixHeaderForm.getAppendixid() != null
				&& !appendixHeaderForm.getAppendixid().equals("")
				&& !appendixHeaderForm.getAppendixid().equals("0")) {
			appendixHeaderForm.setMsaId(IMdao.getMSAId(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB")));
		}

		appendixname = Appendixdao.getAppendixname(
				appendixHeaderForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		appendixHeaderForm.setAppendixname(appendixname);

		if (appendixHeaderForm.getMsaId() != null) {
			msaname = Appendixdao.getMsanameFromMsaID(
					appendixHeaderForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
		} else {
			msaname = Appendixdao.getMsaname(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
		}
		appendixHeaderForm.setMsaname(msaname);

		if (appendixHeaderForm.getMsaId() != null
				&& !appendixHeaderForm.getMsaId().equals("")) {
			DynamicComboCM appendixList = new DynamicComboCM();
			appendixList.setFirstlevelcatglist(Appendixdao.getmsaAppendixList(
					appendixHeaderForm.getMsaId(),
					getDataSource(request, "ilexnewDB")));
			request.setAttribute("appendixList", appendixList);
		}

		// forward to netmedx appendix dashboard
		if (ViewList.CheckAppendixType(appendixHeaderForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")).equals("1")) {
			forward = mapping.findForward("netmedxdashboard");
			return (forward);
		}

		if (!appendixHeaderForm.getViewjobtype().equals("jobSearch")) {
			/* View Selector Code */
			if (appendixHeaderForm.getJobOwnerOtherCheck() != null
					&& !appendixHeaderForm.getJobOwnerOtherCheck().equals("")) {
				session.setAttribute("jobOwnerOtherCheck",
						appendixHeaderForm.getJobOwnerOtherCheck());
			}

			if (request.getParameter("opendiv") != null) {
				request.setAttribute("opendiv", request.getParameter("opendiv"));
			}
			if (request.getParameter("resetList") != null
					&& request.getParameter("resetList").equals("something")) {
				WebUtil.setDefaultAppendixDashboardAttribute(session);

			}

			if (appendixHeaderForm.getJobSelectedStatus() != null) {
				for (int i = 0; i < appendixHeaderForm.getJobSelectedStatus().length; i++) {
					jobSelectedStatus = jobSelectedStatus + "," + "'"
							+ appendixHeaderForm.getJobSelectedStatus(i) + "'";
					jobTempStatus = jobTempStatus + ","
							+ appendixHeaderForm.getJobSelectedStatus(i);
				}

				jobSelectedStatus = jobSelectedStatus.substring(1,
						jobSelectedStatus.length());
				jobSelectedStatus = "(" + jobSelectedStatus + ")";
			}

			if (appendixHeaderForm.getJobSelectedOwners() != null) {
				for (int j = 0; j < appendixHeaderForm.getJobSelectedOwners().length; j++) {
					jobSelectedOwner = jobSelectedOwner + ","
							+ appendixHeaderForm.getJobSelectedOwners(j);
					jobTempOwner = jobTempOwner + ","
							+ appendixHeaderForm.getJobSelectedOwners(j);
				}
				jobSelectedOwner = jobSelectedOwner.substring(1,
						jobSelectedOwner.length());
				if (jobSelectedOwner.equals("0")) {
					jobSelectedOwner = "%";
				}
				jobSelectedOwner = "(" + jobSelectedOwner + ")";
			}

			if (request.getParameter("showList") != null
					&& request.getParameter("showList").equals("something")) {
				session.setAttribute("jobSelectedOwners", jobSelectedOwner);
				session.setAttribute("jobTempOwner", jobTempOwner);
				session.setAttribute("jobSelectedStatus", jobSelectedStatus);
				session.setAttribute("jobTempStatus", jobTempStatus);
				session.setAttribute("timeFrame",
						appendixHeaderForm.getJobMonthWeekCheck());
				session.setAttribute("jobOwnerOtherCheck",
						appendixHeaderForm.getJobOwnerOtherCheck());

			} else {
				if (session.getAttribute("jobSelectedOwners") != null) {
					jobSelectedOwner = session
							.getAttribute("jobSelectedOwners").toString();
					appendixHeaderForm
							.setJobSelectedOwners(session
									.getAttribute("jobTempOwner").toString()
									.split(","));
				}
				if (session.getAttribute("jobSelectedStatus") != null) {
					jobSelectedStatus = session.getAttribute(
							"jobSelectedStatus").toString();
					appendixHeaderForm.setJobSelectedStatus(session
							.getAttribute("jobTempStatus").toString()
							.split(","));
				}
				if (session.getAttribute("jobOwnerOtherCheck") != null) {
					appendixHeaderForm.setJobOwnerOtherCheck(session
							.getAttribute("jobOwnerOtherCheck").toString());
				}
				if (session.getAttribute("timeFrame") != null) {
					appendixHeaderForm.setJobMonthWeekCheck(session
							.getAttribute("timeFrame").toString());
				}
			}

			jobOwnerList = MSAdao.getOwnerList(loginUserId,
					appendixHeaderForm.getJobOwnerOtherCheck(), "prj_job_new",
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			jobStatusList = MSAdao.getStatusList("prj_job_new",
					getDataSource(request, "ilexnewDB"));

			if (jobOwnerList.size() > 0) {
				jobOwnerListSize = jobOwnerList.size();
			}

			request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("jobOwnerList", jobOwnerList);
			/* View Selector Code */
		}
		// Don't set in case of job search
		if (!appendixHeaderForm.getViewjobtype().equals("jobSearch")) {
			// set appendix financial summary
			plsummary = Appendixdao.getPLsummary(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			appendixHeaderForm.setEstimatedcost(plsummary[0]);
			appendixHeaderForm.setExtendedprice(plsummary[1]);
			appendixHeaderForm.setProformamargin(plsummary[2]);
			appendixHeaderForm.setAppendixcnslabor(plsummary[3]);
			appendixHeaderForm.setAppendixfieldlabor(plsummary[4]);
			appendixHeaderForm.setAppendixmaterials(plsummary[5]);
			appendixHeaderForm.setAppendixfreight(plsummary[6]);
			appendixHeaderForm.setAppendixtravel(plsummary[7]);
			appendixHeaderForm.setActualcosts(plsummary[8]);
			appendixHeaderForm.setActualvgpm(plsummary[9]);
			appendixHeaderForm.setActualvgp(plsummary[10]);
			appendixHeaderForm.setPmvipp(plsummary[11]);

			// set appendix contact summary
			contactsummary = Appendixdao.getContactsummary(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			appendixHeaderForm.setPocname(contactsummary[0]);

			if (contactsummary[1] != null) {
				appendixHeaderForm.setPocphone(contactsummary[1]);
			} else {
				appendixHeaderForm.setPocphone("");
			}

			if (contactsummary[3] != null) {
				appendixHeaderForm.setPocemail(contactsummary[3]);
			} else {
				appendixHeaderForm.setPocemail("");
			}

			if (contactsummary[5] != null) {
				appendixHeaderForm.setPocfax(contactsummary[5]);
			} else {
				appendixHeaderForm.setPocfax("");
			}

			appendixHeaderForm.setPocemail(contactsummary[3]);
			appendixHeaderForm.setPocfax(contactsummary[5]);

			// set poc summary
			pocdetails = Appendixdao.getAppendixpocdetails(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			appendixHeaderForm.setAppendixcnspoc(pocdetails[0]);
			appendixHeaderForm.setAppendixcnspocphone(pocdetails[1]);
			appendixHeaderForm.setAppendixbusinesspoc(pocdetails[2]);
			appendixHeaderForm.setAppendixbusinesspocphone(pocdetails[3]);
			appendixHeaderForm.setAppendixcontractpoc(pocdetails[4]);
			appendixHeaderForm.setAppendixcontractpocphone(pocdetails[5]);
			appendixHeaderForm.setAppendixbillingpoc(pocdetails[6]);
			appendixHeaderForm.setAppendixbillingpocphone(pocdetails[7]);

			custcnspocdetails = Appendixdao.getAppendixcustcnspocdetails(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			appendixHeaderForm.setCnsprojectmgr(custcnspocdetails[0]);
			appendixHeaderForm.setCnsprojectmgrphone(custcnspocdetails[1]);
			appendixHeaderForm.setCustprojectmgr(custcnspocdetails[2]);
			appendixHeaderForm.setCustprojectmgrphone(custcnspocdetails[3]);
			appendixHeaderForm.setCnsprojectmgrId(custcnspocdetails[4]);

			// added for project Manager and team lead
			String cnsPrjManagerId = AddContactdao.getCnsPrjManagerId(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			String teamLeadId = AddContactdao.getTeamLeadId(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			if (cnsPrjManagerId != null) {
				cnsPrjManangerDetail = AddContactdao.getPocDetails(
						cnsPrjManagerId, getDataSource(request, "ilexnewDB"));
				String cnsPrjManagerName = AddContactdao.getPocname(
						cnsPrjManagerId, getDataSource(request, "ilexnewDB"));
				appendixHeaderForm.setCnsPrjManagerId(cnsPrjManagerId);
				appendixHeaderForm.setCnsPrjManagerName(cnsPrjManagerName);
				appendixHeaderForm
						.setCnsPrjManagerPhone(cnsPrjManangerDetail[0]);

			}
			if (teamLeadId != null) {
				teamLeadDetail = AddContactdao.getPocDetails(teamLeadId,
						getDataSource(request, "ilexnewDB"));
				String teamLeadName = AddContactdao.getPocname(teamLeadId,
						getDataSource(request, "ilexnewDB"));
				appendixHeaderForm.setTeamLeadId(teamLeadId);
				appendixHeaderForm.setTeamLeadName(teamLeadName);
				appendixHeaderForm.setTeamLeadPhone(teamLeadDetail[0]);

			}

		}

		// appendixcurrentstatus is status code(char)
		appendixcurrentstatus = Appendixdao.getCurrentstatus(
				appendixHeaderForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		appendixHeaderForm.setStatus(appendixcurrentstatus);

		// get appendix info
		Appendix appendix = new Appendix();
		appendix = com.mind.dao.PRM.Appendixdao.getAppendix("%",
				appendixHeaderForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));

		Appendix appendix1 = new Appendix();
		appendix1 = com.mind.dao.PRM.Appendixdao.getjobDates(
				appendixHeaderForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));

		/* For Rescheule */
		appendixHeaderForm.setRseCount(Appendixdao.getResCount(
				appendixHeaderForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")));

		if (appendix.getAppendix_Id() != null) {
			appendixHeaderForm.setStatusdesc(appendix.getStatus());
			appendixHeaderForm.setAppendix_type(appendix.getAppendixtype());
			appendixHeaderForm.setCustref(appendix.getCustref());
			appendixHeaderForm.setJob_inwork(appendix.getInwork_job());
			appendixHeaderForm.setJob_complete(appendix.getComplete_job());
			appendixHeaderForm.setJob_closed(appendix.getClosed_job());
			appendixHeaderForm.setJob_total(appendix.getTotal_job());
			appendixHeaderForm.setEndcust(appendix.getEndcust());
			request.setAttribute("testAppendix", appendix.getTestAppendix());
			appendixHeaderForm.setJob_earliest_start(appendix1
					.getEarliestStart());
			appendixHeaderForm.setJob_earliest_end(appendix1.getEarliestEnd());
			appendixHeaderForm.setJob_latest_start(appendix1.getLatestStart());
			appendixHeaderForm.setJob_latest_end(appendix1.getLatestEnd());
		}

		if (request.getAttribute("querystring") != null) {
			sortqueryclause = (String) request.getAttribute("querystring");
		}

		// Start :Added By Amit For Holdong Sort
		if (session != null) {
			if (session.getAttribute("HoldingSort") != null) {
				sortqueryclause = (String) session.getAttribute("HoldingSort");
			}
		}
		// End

		/* Set filters in session */
		if (request.getParameter("changeFilter") != null) {
			if (request.getParameter("changeFilter").equals("Y")) {
				session.setAttribute("ownerId", appendixHeaderForm.getOwnerId());
				session.setAttribute("viewjobtype",
						appendixHeaderForm.getViewjobtype());
			}
		}

		/* Get filters from session */
		if (session.getAttribute("ownerId") != null) {
			userId = (String) session.getAttribute("ownerId");
		}
		if (session.getAttribute("viewjobtype") != null) {
			status = (String) session.getAttribute("viewjobtype");
		}

		initialStateCategory = Appendixdao.getStateCategoryList(getDataSource(
				request, "ilexnewDB"));

		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempCat = ((AppendixHeaderBean) initialStateCategory.get(i))
						.getSiteCountryId();
				((AppendixHeaderBean) initialStateCategory.get(i))
						.setTempList(SiteManagedao
								.getCategoryWiseStateList(tempCat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}
		request.setAttribute("initialStateCategory", initialStateCategory);

		DynamicComboCM dcJobOwnerList = new DynamicComboCM();
		dcJobOwnerList.setFirstlevelcatglist(Appendixdao
				.getjobOwnerListfromMSA(appendixHeaderForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"),
						appendixHeaderForm.getMsaId()));
		request.setAttribute("dcJobOwnerList", dcJobOwnerList);

		if (appendixHeaderForm.getViewjobtype().equals("jobSearch")) {
			if (request.getParameter("fromLink") != null) {
				list = null;
				request.setAttribute("joblist", list);
				request.setAttribute("listsize", 0 + "");
				session.setAttribute("job_search_name", null);
				session.setAttribute("job_search_site_number", null);
				session.setAttribute("job_search_city", null);
				session.setAttribute("job_search_state", null);
				session.setAttribute("job_search_owner", null);
				session.setAttribute("job_search_date_from", null);
				session.setAttribute("job_search_date_to", null);
				session.setAttribute("dateOnSite", null);
				session.setAttribute("dateOffSite", null);
				session.setAttribute("dateComplete", null);
				session.setAttribute("dateClosed", null);
				session.setAttribute("job_search", null);
			}
			if (appendixHeaderForm.getJob_search() != null
					&& !appendixHeaderForm.getJob_search().equals("")) {
				request.setAttribute("searchPage", "jobSearch");
				session.setAttribute("fromPage",
						appendixHeaderForm.getFromPage());
				session.setAttribute("msaId", appendixHeaderForm.getMsaId());
				session.setAttribute("job_search_name",
						appendixHeaderForm.getJob_search_name());
				session.setAttribute("job_search_site_number",
						appendixHeaderForm.getJob_search_site_number());
				session.setAttribute("job_search_city",
						appendixHeaderForm.getJob_search_city());
				session.setAttribute("job_search_state",
						appendixHeaderForm.getJob_search_state());
				session.setAttribute("job_search_owner",
						appendixHeaderForm.getJob_search_owner());
				session.setAttribute("job_search_date_from",
						appendixHeaderForm.getJob_search_date_from());
				session.setAttribute("job_search_date_to",
						appendixHeaderForm.getJob_search_date_to());
				session.setAttribute("dateOnSite",
						appendixHeaderForm.getDateOnSite());
				session.setAttribute("dateOffSite",
						appendixHeaderForm.getDateOffSite());
				session.setAttribute("dateComplete",
						appendixHeaderForm.getDateComplete());
				session.setAttribute("dateClosed",
						appendixHeaderForm.getDateClosed());
				session.setAttribute("job_search",
						appendixHeaderForm.getJob_search());
			} else {

				if (session.getAttribute("job_search_name") != null) {
					appendixHeaderForm.setJob_search_name(session.getAttribute(
							"job_search_name").toString());
				}

				if (session.getAttribute("job_search_site_number") != null) {
					appendixHeaderForm.setJob_search_site_number(session
							.getAttribute("job_search_site_number").toString());
				}

				if (session.getAttribute("job_search_city") != null) {
					appendixHeaderForm.setJob_search_city(session.getAttribute(
							"job_search_city").toString());
				}

				if (session.getAttribute("job_search_state") != null) {
					appendixHeaderForm.setJob_search_state(session
							.getAttribute("job_search_state").toString());
				}

				if (session.getAttribute("job_search_owner") != null) {
					appendixHeaderForm.setJob_search_owner(session
							.getAttribute("job_search_owner").toString());
				}

				if (session.getAttribute("job_search_date_from") != null) {
					appendixHeaderForm.setJob_search_date_from(session
							.getAttribute("job_search_date_from").toString());
				}

				if (session.getAttribute("job_search_date_to") != null) {
					appendixHeaderForm.setJob_search_date_to(session
							.getAttribute("job_search_date_to").toString());
				}

				if (session.getAttribute("dateOnSite") != null) {
					appendixHeaderForm.setDateOnSite(session.getAttribute(
							"dateOnSite").toString());
				}

				if (session.getAttribute("dateOffSite") != null) {
					appendixHeaderForm.setDateOffSite(session.getAttribute(
							"dateOffSite").toString());
				}

				if (session.getAttribute("dateComplete") != null) {
					appendixHeaderForm.setDateComplete(session.getAttribute(
							"dateComplete").toString());
				}

				if (session.getAttribute("dateClosed") != null) {
					appendixHeaderForm.setDateClosed(session.getAttribute(
							"dateClosed").toString());
				}

				if (session.getAttribute("job_search") != null) {
					appendixHeaderForm.setJob_search(session.getAttribute(
							"job_search").toString());
				}

			}
			if (request.getParameter("fromLink") != null
					|| session.getAttribute("job_search") == null) {
				list = null;
				request.setAttribute("joblist", list);
				request.setAttribute("listsize", 0 + "");
			} else {
				sessionMap = WebUtil.copySessionToAttributeMap(session);
				list = Appendixdao.getJobSearchdetaillist(
						appendixHeaderForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"), sortqueryclause,
						sessionMap, appendixHeaderForm.getMsaId());
				request.setAttribute("joblist", list);
				request.setAttribute("listsize", list.size() + "");
			}
		} else {
			list = Appendixdao.getJobdetaillist(
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"), sortqueryclause,
					jobSelectedStatus, jobSelectedOwner,
					appendixHeaderForm.getJobMonthWeekCheck());
			request.setAttribute("joblist", list);
			request.setAttribute("listsize", list.size() + "");
		}

		if (request.getParameter("bulkuploadflag") != null) {
			request.setAttribute("bulkuploadflag",
					request.getParameter("bulkuploadflag"));
		}

		if (request.getParameter("addendumflag") != null) {
			request.setAttribute("addendumflag",
					request.getParameter("addendumflag"));
		}

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(appendixHeaderForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);

		String addendumid = com.mind.dao.PM.Addendumdao.getLatestAddendumId(
				appendixHeaderForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("latestaddendumid", addendumid);
		request.setAttribute("viewjobtype", appendixHeaderForm.getViewjobtype());
		request.setAttribute("loginUserId", loginUserId);
		request.setAttribute("appendixid", appendixHeaderForm.getAppendixid());
		request.setAttribute("msaId", appendixHeaderForm.getMsaId());
		request.setAttribute("ownerId", appendixHeaderForm.getOwnerId());
		boolean editContactList = Appendixdao.editContactList(loginUserId,
				getDataSource(request, "ilexnewDB"));
		appendixHeaderForm.setEditContactList(editContactList);

		// returned with API Access logic.
		ArrayList<String> userInfo = new ArrayList<>();
		if (appendixHeaderForm.getAppendixid() != null) {

			userInfo = NetMedXDashboarddao.checkPassword(
					appendixHeaderForm.getMsaId(),
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			appendixHeaderForm.setaPIAccessPassword(userInfo.get(0));
			appendixHeaderForm.setApiUserName(userInfo.get(1));
			if (userInfo.get(1).equals("")) {
				appendixHeaderForm.setApiCheckBox("off");
			} else {
				appendixHeaderForm.setApiCheckBox("on");
			}

		}

		if (appendixHeaderForm.getApiAccessChkBox() != null
				&& appendixHeaderForm.getApiAccessChkBox().equals("false")) {

			NetMedXDashboarddao.deletePassword(appendixHeaderForm.getMsaId(),
					appendixHeaderForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			apiAccessValue = "apiAccess";
			appendixHeaderForm.setaPIAccessPassword("");
			appendixHeaderForm.setApiUserName("");
			appendixHeaderForm.setApiCheckBox("off");

		}

		if (appendixHeaderForm.getApiAccessBtn() != null
				&& appendixHeaderForm.getApiAccessBtn().equals("Get Access")
				&& appendixHeaderForm.getApiAccessChkBox() != null
				&& appendixHeaderForm.getApiAccessChkBox().equals("true")) {

			request.setAttribute("searchPage", "TicketSearch");
			session.setAttribute("job_search_site_number",
					appendixHeaderForm.getJob_search_site_number());
			session.setAttribute("job_search_owner",
					appendixHeaderForm.getJob_search_owner());
			appendixHeaderForm.setApiAccessChkBox(appendixHeaderForm
					.getApiAccessChkBox());
			appendixHeaderForm.setApiAccessBtn("Get Access");
			request.setAttribute("apiAccess", "apiAccess");

			String password = new String(NetMedXDashboardAction.generatePswd());

			String userName = "";
			String msaName = appendixHeaderForm.getMsaname().toLowerCase()
					.trim();
			if (msaName.contains(" ")) {
				String[] apiUser = msaName.split(" ");
				userName = apiUser[0].substring(0, 1);
				for (int i = 1; i <= apiUser.length - 1; i++) {
					userName = userName + apiUser[i];
				}
			} else {
				userName = msaName;
			}

			NetMedXDashboarddao.setAPIAccessValue(
					appendixHeaderForm.getMsaId(), userName, password,
					appendixHeaderForm.getAppendixid(), null,
					getDataSource(request, "ilexnewDB"));
			appendixHeaderForm.setaPIAccessPassword(password);
			appendixHeaderForm.setApiUserName(userName);
			appendixHeaderForm.setApiCheckBox("on");
			apiAccessValue = "apiAccess";
		}

		if (apiAccessValue.equals("apiAccess")) {
			return (mapping.findForward("apiAccessSuccess"));
		}

		forward = mapping.findForward("success");

		return (forward);
	}
}
