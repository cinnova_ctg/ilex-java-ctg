package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.prm.SiteManagementBean;
import com.mind.common.Util;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.RestClient;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PRM.SiteManagementForm;

public class SiteManageAction extends com.mind.common.IlexDispatchAction {

	public ActionForward Modify(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		DataSource ds = null;
		ds = getDataSource(request, "ilexnewDB");
		SiteManagementForm siteManage = (SiteManagementForm) form;
		SiteManagementBean siteManageBean = new SiteManagementBean();
		Collection manageStateWiseDetails = new ArrayList();
		ArrayList initialStateCategory = new ArrayList();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String tempCat = "";
		String[] tempList = null;
		String check = "";

		if (request.getParameter("check") != null)
			check = request.getParameter("check");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");

		if (request.getParameter("countRow") != null)
			siteManage.setCountRow(request.getParameter("countRow"));

		if (request.getParameter("siteID") != null)
			siteManage.setSiteID(request.getParameter("siteID"));

		if (request.getAttribute("siteID") != null)
			siteManage.setSiteID(request.getAttribute("siteID") + "");

		if (request.getParameter("ownerId") != null) {
			siteManage.setOwnerId("" + request.getParameter("ownerId"));
		}

		if (request.getParameter("msaid") != null) {
			siteManage.setMsaid(request.getParameter("msaid"));
		}

		if (request.getParameter("siteSearchName") != null)
			siteManage
					.setSiteSearchName(request.getParameter("siteSearchName"));

		if (request.getParameter("pageType") != null)
			siteManage.setPageType(request.getParameter("pageType"));

		if (request.getParameter("siteSearchNumber") != null)
			siteManage.setSiteSearchNumber(request
					.getParameter("siteSearchNumber"));

		if (request.getParameter("siteSearchCity") != null)
			siteManage
					.setSiteSearchCity(request.getParameter("siteSearchCity"));

		if (request.getParameter("siteSearchState") != null)
			siteManage.setSiteSearchState(request
					.getParameter("siteSearchState"));

		if (request.getParameter("siteSearchZip") != null)
			siteManage.setSiteSearchZip(request.getParameter("siteSearchZip"));

		if (request.getParameter("siteSearchCountry") != null)
			siteManage.setSiteSearchCountry(request
					.getParameter("siteSearchCountry"));

		if (request.getParameter("siteSearchGroup") != null)
			siteManage.setSiteSearchGroup(request
					.getParameter("siteSearchGroup"));

		if (request.getParameter("siteSearchEndCustomer") != null)
			siteManage.setSiteSearchEndCustomer(request
					.getParameter("siteSearchEndCustomer"));

		if (request.getParameter("siteSearchStatus") != null)
			siteManage.setSiteSearchStatus(request
					.getParameter("siteSearchStatus"));

		if (request.getParameter("lines_per_page") != null)
			siteManage
					.setLines_per_page(request.getParameter("lines_per_page"));

		if (request.getParameter("current_page_no") != null)
			siteManage.setCurrent_page_no(request
					.getParameter("current_page_no"));

		if (request.getParameter("total_page_size") != null)
			siteManage.setTotal_page_size(request
					.getParameter("total_page_size"));

		if (request.getParameter("org_page_no") != null)
			siteManage.setOrg_page_no(request.getParameter("org_page_no"));

		if (request.getParameter("org_lines_per_page") != null)
			siteManage.setOrg_lines_per_page(request
					.getParameter("org_lines_per_page"));

		if (request.getParameter("siteNameForColumn") != null)
			siteManage.setSiteNameForColumn(request
					.getParameter("siteNameForColumn"));

		if (request.getParameter("siteNumberForColumn") != null)
			siteManage.setSiteNumberForColumn(request
					.getParameter("siteNumberForColumn"));

		if (request.getParameter("siteAddressForColumn") != null)
			siteManage.setSiteAddressForColumn(request
					.getParameter("siteAddressForColumn"));

		if (request.getParameter("siteCityForColumn") != null)
			siteManage.setSiteCityForColumn(request
					.getParameter("siteCityForColumn"));

		if (request.getParameter("siteStateForColumn") != null)
			siteManage.setSiteStateForColumn(request
					.getParameter("siteStateForColumn"));

		if (request.getParameter("siteZipForColumn") != null)
			siteManage.setSiteZipForColumn(request
					.getParameter("siteZipForColumn"));

		if (request.getParameter("siteCountryForColumn") != null)
			siteManage.setSiteCountryForColumn(request
					.getParameter("siteCountryForColumn"));

		if (request.getParameter("pageType") != null) {
			if (request.getParameter("pageType").equals("mastersites")) {
				siteManage.setAppendixid(request.getParameter("appendixid")
						.toString());
				siteManage.setMsaid(request.getParameter("msaid"));
				siteManage.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						ds, request.getParameter("msaid")));
			} else {
				if (request.getParameter("appendixid") != null
						&& !request.getParameter("appendixid").equals("")) {
					siteManage.setAppendixid(request.getParameter("appendixid")
							.toString());
					siteManage.setMsaid(IMdao.getMSAId(
							siteManage.getAppendixid(),
							getDataSource(request, "ilexnewDB")));
					siteManage.setMsaname(com.mind.dao.PM.Appendixdao
							.getMsaname(ds, IMdao.getMSAId(
									siteManage.getAppendixid(), ds)));
					siteManage.setAppendixname(Jobdao.getAppendixname(ds,
							siteManage.getAppendixid()));
				}
			}

		}

		DynamicComboCM dcLonDirec = new DynamicComboCM();
		dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
		request.setAttribute("dcLonDirec", dcLonDirec);

		DynamicComboCM dcLatDirec = new DynamicComboCM();
		dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
		request.setAttribute("dcLatDirec", dcLatDirec);

		DynamicComboCM dcSiteStatus = new DynamicComboCM();
		dcSiteStatus.setFirstlevelcatglist(DynamicComboDao.getsitestatus());
		request.setAttribute("dcSiteStatus", dcSiteStatus);

		DynamicComboCM dcStateCM = new DynamicComboCM();
		Pvsdao pvsDao = new Pvsdao();
		dcStateCM.setFirstlevelcatglist(Pvsdao.getCountryStateList(ds));
		request.setAttribute("dcStateCM", dcStateCM);

		DynamicComboCM dcCountryList = new DynamicComboCM();
		dcCountryList.setFirstlevelcatglist(DynamicComboDao.getCountryList(ds));
		request.setAttribute("dcCountryList", dcCountryList);

		DynamicComboCM dcGroupList = new DynamicComboCM();
		dcGroupList.setFirstlevelcatglist(DynamicComboDao
				.getGroupNameListSiteSearch(siteManage.getMsaid(), ds));
		request.setAttribute("dcGroupList", dcGroupList);

		DynamicComboCM dcEndCustomerList = new DynamicComboCM();
		dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
				.getEndCustomerList(ds, siteManage.getMsaid(),
						siteManage.getMsaname()));
		request.setAttribute("dcEndCustomerList", dcEndCustomerList);

		DynamicComboCM dcTimeZoneList = new DynamicComboCM();
		dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao.gettimezone(ds));
		request.setAttribute("dcTimeZoneList", dcTimeZoneList);

		DynamicComboCM dcCategoryList = new DynamicComboCM();
		dcCategoryList.setFirstlevelcatglist(DynamicComboDao
				.getCategoryList(ds));
		request.setAttribute("dcCategoryList", dcCategoryList);

		DynamicComboCM dcRegionList = new DynamicComboCM();
		dcRegionList.setFirstlevelcatglist(DynamicComboDao.getRegionList(ds));
		request.setAttribute("dcRegionList", dcRegionList);

		initialStateCategory = SiteManagedao
				.getStateCategoryList(getDataSource(request, "ilexnewDB"));

		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempCat = ((SiteManagementBean) initialStateCategory.get(i))
						.getSiteCountryId();
				((SiteManagementBean) initialStateCategory.get(i))
						.setTempList(SiteManagedao
								.getCategoryWiseStateList(tempCat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}
		BeanUtils.copyProperties(siteManageBean, siteManage);
		if (siteManage.getPageType().equals("jobsites")) {
			SiteManagedao
					.getJobSiteInformation(siteManageBean,
							siteManage.getSiteID(),
							getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(siteManage, siteManageBean);
		} else {
			if (siteManage.getRefersh() != null
					&& siteManage.getRefersh().equals("true")) {
				SiteManagedao.getRefershedStates(siteManageBean, check,
						getDataSource(request, "ilexnewDB"));
				BeanUtils.copyProperties(siteManage, siteManageBean);
				siteManage.setRefersh(null);
			} else {
				SiteManagedao.getSiteInformation(siteManageBean,
						siteManage.getSiteID(),
						getDataSource(request, "ilexnewDB"));
				BeanUtils.copyProperties(siteManage, siteManageBean);
			}

		}
		request.setAttribute("initialStateCategory", initialStateCategory);

		// Added this code for the Job Detail Menu.
		if (siteManage.getJobid() != null && siteManage.getFromType() != null) {
			if (!siteManage.getJobid().equals("0")
					&& siteManage.getFromType() != null
					&& siteManage.getFromType().equals("yes")) {
				String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
						siteManage.getJobid(), "%", "%",
						getDataSource(request, "ilexnewDB"));
				String jobStatus = jobStatusAndType[0];
				String jobType1 = jobStatusAndType[1];
				String jobStatusList = "";
				String appendixType = "";

				if (jobType1.equals("Default"))
					jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
							+ siteManage.getJobid() + "&Status='D'\"" + ","
							+ "0";
				if (jobType1.equals("Newjob"))
					jobStatusList = Menu.getStatus("pm_job",
							jobStatus.charAt(0), "", "", siteManage.getJobid(),
							"", loginuserid,
							getDataSource(request, "ilexnewDB"));
				if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
					jobStatusList = "\"Approved\""
							+ ","
							+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
							+ siteManage.getJobid() + "&Status=A\"" + "," + "0";
				if (jobType1.equals("inscopejob")
						&& jobStatus.equals("Approved"))
					jobStatusList = "\"Draft\""
							+ ","
							+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
							+ siteManage.getJobid() + "&Status=D\"" + "," + "0";
				if (jobType1.equals("Addendum"))
					jobStatusList = Menu.getStatus("Addendum",
							jobStatus.charAt(0), "", "", siteManage.getJobid(),
							"", loginuserid,
							getDataSource(request, "ilexnewDB"));

				appendixType = ViewList.getAppendixtypedesc(
						siteManage.getAppendixid(),
						getDataSource(request, "ilexnewDB"));

				request.setAttribute("jobStatus", jobStatus);
				request.setAttribute("job_type", jobType1);
				request.setAttribute("appendixtype", appendixType);
				request.setAttribute("Job_Id", siteManage.getJobid());
				request.setAttribute("Appendix_Id", siteManage.getAppendixid());
				request.setAttribute("jobStatusList", jobStatusList);
				request.setAttribute("addendum_id", "0");

				if (jobType1.equals("Newjob")) {
					request.setAttribute("chkadd", "detailjob");
					request.setAttribute("chkaddendum", "detailjob");
					request.setAttribute("chkaddendumactivity", "View");
				}
				if (jobType1.equals("inscopejob")
						|| jobType1.equals("addendum")) {
					request.setAttribute("chkadd", "inscopejob");
					request.setAttribute("chkaddendum", "inscopejob");
					request.setAttribute("chkaddendumactivity",
							"inscopeactivity");
				}
			}
		}
		// End of Code for Job Detail Menu.

		siteManage.setStateSelect(Util.concatString(siteManage.getState(),
				siteManage.getCountry()));
		forward = mapping.findForward("success");
		return (forward);
	}

	public ActionForward Save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		SiteManagementForm siteManage = (SiteManagementForm) form;
		SiteManagementBean siteManageBean = new SiteManagementBean();
		String appid = null;
		String action = null;
		DataSource ds = null;
		ds = getDataSource(request, "ilexnewDB");
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");

		if (siteManage.getSiteID().equals("0"))
			action = "A";
		else {
			action = "U";
		}

		long[] retValArr = null;
		long retval = 0;

		if (request.getParameter("pageType") != null) {
			if (request.getParameter("pageType").equals("mastersites")) {
				siteManage.setAppendixid(request.getParameter("appendixid")
						.toString());
				siteManage.setMsaid(request.getParameter("msaid"));
				siteManage.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						ds, request.getParameter("msaid")));
			} else {
				if (request.getParameter("appendixid") != null
						&& !request.getParameter("appendixid").equals("")) {
					siteManage.setAppendixid(request.getParameter("appendixid")
							.toString());
					siteManage.setMsaid(IMdao.getMSAId(
							siteManage.getAppendixid(),
							getDataSource(request, "ilexnewDB")));
					siteManage.setMsaname(com.mind.dao.PM.Appendixdao
							.getMsaname(ds, IMdao.getMSAId(
									siteManage.getAppendixid(), ds)));
					siteManage.setAppendixname(Jobdao.getAppendixname(ds,
							siteManage.getAppendixid()));
				}
			}

		}
		String state_name = Pvsdao.getStateName(siteManage.getState(), ds);
		String LatLong[] = RestClient.getLatitudeLongitude(
				siteManage.getSiteAddress(), siteManage.getSiteCity(),
				state_name, siteManage.getCountry(),
				siteManage.getSiteZipCode(),
				getDataSource(request, "ilexnewDB"));
		siteManage.setLatLongAccuracy(LatLong[0]);
		siteManage.setLatLongSource(LatLong[1]);
		siteManage.setLatitude(LatLong[3]);
		siteManage.setLongitude(LatLong[2]);
		BeanUtils.copyProperties(siteManageBean, siteManage);
		retValArr = SiteManagedao.updateSite(siteManageBean, action, userid,
				getDataSource(request, "ilexnewDB"));

		retval = retValArr[0];

		request.setAttribute("message", "" + retval);

		request.setAttribute("siteID", retValArr[1] + "");

		if (request.getParameter("pageType") != null)
			siteManage.setPageType(request.getParameter("pageType"));

		if (request.getParameter("msaid") != null) {
			siteManage.setMsaid(request.getParameter("msaid"));
		}

		if (request.getParameter("siteSearchName") != null)
			siteManage
					.setSiteSearchName(request.getParameter("siteSearchName"));

		if (request.getParameter("siteSearchNumber") != null)
			siteManage.setSiteSearchNumber(request
					.getParameter("siteSearchNumber"));

		if (request.getParameter("siteSearchCity") != null)
			siteManage
					.setSiteSearchCity(request.getParameter("siteSearchCity"));

		if (request.getParameter("siteSearchState") != null)
			siteManage.setSiteSearchState(request
					.getParameter("siteSearchState"));

		if (request.getParameter("siteSearchZip") != null)
			siteManage.setSiteSearchZip(request.getParameter("siteSearchZip"));

		if (request.getParameter("siteSearchCountry") != null)
			siteManage.setSiteSearchCountry(request
					.getParameter("siteSearchCountry"));

		if (request.getParameter("siteSearchGroup") != null)
			siteManage.setSiteSearchGroup(request
					.getParameter("siteSearchGroup"));

		if (request.getParameter("siteSearchEndCustomer") != null)
			siteManage.setSiteSearchEndCustomer(request
					.getParameter("siteSearchEndCustomer"));

		if (request.getParameter("siteSearchStatus") != null)
			siteManage.setSiteSearchStatus(request
					.getParameter("siteSearchStatus"));

		if (request.getParameter("countRow") != null)
			siteManage.setCountRow(request.getParameter("countRow"));

		if (request.getParameter("siteID") != null)
			siteManage.setSiteID(request.getParameter("siteID"));

		if (request.getAttribute("siteID") != null)
			siteManage.setSiteID(request.getAttribute("siteID") + "");

		if (request.getParameter("appendixid") != null) {
			siteManage.setAppendixid(request.getParameter("appendixid"));
			appid = siteManage.getAppendixid();
		}

		if (request.getParameter("lines_per_page") != null)
			siteManage
					.setLines_per_page(request.getParameter("lines_per_page"));

		if (request.getParameter("current_page_no") != null)
			siteManage.setCurrent_page_no(request
					.getParameter("current_page_no"));

		if (request.getParameter("total_page_size") != null)
			siteManage.setTotal_page_size(request
					.getParameter("total_page_size"));

		if (request.getParameter("org_page_no") != null)
			siteManage.setOrg_page_no(request.getParameter("org_page_no"));

		if (request.getParameter("org_lines_per_page") != null)
			siteManage.setOrg_lines_per_page(request
					.getParameter("org_lines_per_page"));

		if (request.getParameter("siteNameForColumn") != null)
			siteManage.setSiteNameForColumn(request
					.getParameter("siteNameForColumn"));

		if (request.getParameter("siteNumberForColumn") != null)
			siteManage.setSiteNumberForColumn(request
					.getParameter("siteNumberForColumn"));

		if (request.getParameter("siteAddressForColumn") != null)
			siteManage.setSiteAddressForColumn(request
					.getParameter("siteAddressForColumn"));

		if (request.getParameter("siteCityForColumn") != null)
			siteManage.setSiteCityForColumn(request
					.getParameter("siteCityForColumn"));

		if (request.getParameter("siteStateForColumn") != null)
			siteManage.setSiteStateForColumn(request
					.getParameter("siteStateForColumn"));

		if (request.getParameter("siteZipForColumn") != null)
			siteManage.setSiteZipForColumn(request
					.getParameter("siteZipForColumn"));

		if (request.getParameter("siteCountryForColumn") != null)
			siteManage.setSiteCountryForColumn(request
					.getParameter("siteCountryForColumn"));

		session.setAttribute("siteList", null);
		request.setAttribute("appendixid", appid);
		// forward = mapping.findForward("backToSiteManage");
		forward = mapping.findForward("siteManagement");
		return (forward);
	}

	public ActionForward Delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		SiteManagementForm siteManage = (SiteManagementForm) form;

		HttpSession session = request.getSession(true);
		String appid = null;
		DataSource ds = null;
		ds = getDataSource(request, "ilexnewDB");
		int retval = 0;
		int linesPerPage = 0;
		int rowSize = 0;

		if (request.getParameter("pageType") != null)
			siteManage.setPageType(request.getParameter("pageType"));

		if (request.getParameter("msaid") != null) {
			siteManage.setMsaid(request.getParameter("msaid"));
		}

		if (request.getParameter("siteSearchName") != null)
			siteManage
					.setSiteSearchName(request.getParameter("siteSearchName"));

		if (request.getParameter("siteSearchNumber") != null)
			siteManage.setSiteSearchNumber(request
					.getParameter("siteSearchNumber"));

		if (request.getParameter("siteSearchCity") != null)
			siteManage
					.setSiteSearchCity(request.getParameter("siteSearchCity"));

		if (request.getParameter("siteSearchState") != null)
			siteManage.setSiteSearchState(request
					.getParameter("siteSearchState"));

		if (request.getParameter("siteSearchZip") != null)
			siteManage.setSiteSearchZip(request.getParameter("siteSearchZip"));

		if (request.getParameter("siteSearchCountry") != null)
			siteManage.setSiteSearchCountry(request
					.getParameter("siteSearchCountry"));

		if (request.getParameter("siteSearchGroup") != null)
			siteManage.setSiteSearchGroup(request
					.getParameter("siteSearchGroup"));

		if (request.getParameter("siteSearchEndCustomer") != null)
			siteManage.setSiteSearchEndCustomer(request
					.getParameter("siteSearchEndCustomer"));

		if (request.getParameter("siteSearchStatus") != null)
			siteManage.setSiteSearchStatus(request
					.getParameter("siteSearchStatus"));

		if (request.getParameter("countRow") != null)
			siteManage.setCountRow(request.getParameter("countRow"));

		if (request.getParameter("siteID") != null)
			siteManage.setSiteID(request.getParameter("siteID"));

		if (request.getParameter("appendixid") != null) {
			siteManage.setAppendixid(request.getParameter("appendixid"));
			appid = siteManage.getAppendixid();
		}

		if (request.getParameter("pageType") != null) {
			if (request.getParameter("pageType").equals("mastersites")) {
				siteManage.setAppendixid(request.getParameter("appendixid")
						.toString());
				siteManage.setMsaid(request.getParameter("msaid"));
				siteManage.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						ds, request.getParameter("msaid")));
			} else {
				if (request.getParameter("appendixid") != null
						&& !request.getParameter("appendixid").equals("")) {
					siteManage.setAppendixid(request.getParameter("appendixid")
							.toString());
					siteManage.setMsaid(IMdao.getMSAId(
							siteManage.getAppendixid(),
							getDataSource(request, "ilexnewDB")));
					siteManage.setMsaname(com.mind.dao.PM.Appendixdao
							.getMsaname(ds, IMdao.getMSAId(
									siteManage.getAppendixid(), ds)));
					siteManage.setAppendixname(Jobdao.getAppendixname(ds,
							siteManage.getAppendixid()));
				}
			}

		}

		if (request.getParameter("lines_per_page") != null) {
			siteManage
					.setLines_per_page(request.getParameter("lines_per_page"));
			linesPerPage = Integer.parseInt(siteManage.getLines_per_page());

		}

		if (request.getParameter("current_page_no") != null)
			siteManage.setCurrent_page_no(request
					.getParameter("current_page_no"));

		if (request.getParameter("total_page_size") != null)
			siteManage.setTotal_page_size(request
					.getParameter("total_page_size"));

		if (request.getParameter("org_page_no") != null)
			siteManage.setOrg_page_no(request.getParameter("org_page_no"));

		if (request.getParameter("org_lines_per_page") != null)
			siteManage.setOrg_lines_per_page(request
					.getParameter("org_lines_per_page"));

		if (request.getParameter("siteNameForColumn") != null)
			siteManage.setSiteNameForColumn(request
					.getParameter("siteNameForColumn"));

		if (request.getParameter("siteNumberForColumn") != null)
			siteManage.setSiteNumberForColumn(request
					.getParameter("siteNumberForColumn"));

		if (request.getParameter("siteAddressForColumn") != null)
			siteManage.setSiteAddressForColumn(request
					.getParameter("siteAddressForColumn"));

		if (request.getParameter("siteCityForColumn") != null)
			siteManage.setSiteCityForColumn(request
					.getParameter("siteCityForColumn"));

		if (request.getParameter("siteStateForColumn") != null)
			siteManage.setSiteStateForColumn(request
					.getParameter("siteStateForColumn"));

		if (request.getParameter("siteZipForColumn") != null)
			siteManage.setSiteZipForColumn(request
					.getParameter("siteZipForColumn"));

		if (request.getParameter("siteCountryForColumn") != null)
			siteManage.setSiteCountryForColumn(request
					.getParameter("siteCountryForColumn"));

		session.setAttribute("siteList", null);
		retval = SiteManagedao.deleteSite(
				Integer.parseInt(siteManage.getSiteID()),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("message", "" + retval);
		request.setAttribute("appendixid", appid);
		forward = mapping.findForward("siteManagement");
		return (forward);
	}

	public ActionForward Add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		SiteManagementForm siteManage = (SiteManagementForm) form;
		SiteManagementBean siteManageBean = new SiteManagementBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String appid = null;
		// Collection manageSiteDetails = new ArrayList();
		int manageSiteSize = 0;
		DataSource ds = null;
		ds = getDataSource(request, "ilexnewDB");
		ArrayList initialStateCategory = new ArrayList();
		String tempcat = "";
		String ticketNumber = "0";

		if (request.getParameter("ticketNumber") != null) {
			ticketNumber = request.getParameter("ticketNumber");
		}

		if (request.getParameter("countRow") != null)
			siteManage.setCountRow(request.getParameter("countRow"));

		if (request.getParameter("pageType") != null) {
			if (request.getParameter("pageType").equals("mastersites")) {
				siteManage.setAppendixid(request.getParameter("appendixid")
						.toString());
				siteManage.setMsaid(request.getParameter("msaid"));
				siteManage.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						ds, request.getParameter("msaid")));
			} else {
				if (request.getParameter("appendixid") != null
						&& !request.getParameter("appendixid").equals("")) {
					siteManage.setAppendixid(request.getParameter("appendixid")
							.toString());
					siteManage.setMsaid(IMdao.getMSAId(
							siteManage.getAppendixid(),
							getDataSource(request, "ilexnewDB")));
					siteManage.setMsaname(com.mind.dao.PM.Appendixdao
							.getMsaname(ds, IMdao.getMSAId(
									siteManage.getAppendixid(), ds)));
					siteManage.setAppendixname(Jobdao.getAppendixname(ds,
							siteManage.getAppendixid()));
				}
			}
		}

		if (request.getParameter("pageType") != null)
			siteManage.setPageType(request.getParameter("pageType"));

		if (request.getParameter("msaid") != null) {
			siteManage.setMsaid(request.getParameter("msaid"));
		}

		if (request.getParameter("siteSearchName") != null)
			siteManage
					.setSiteSearchName(request.getParameter("siteSearchName"));

		if (request.getParameter("siteSearchNumber") != null)
			siteManage.setSiteSearchNumber(request
					.getParameter("siteSearchNumber"));

		if (request.getParameter("siteSearchCity") != null)
			siteManage
					.setSiteSearchCity(request.getParameter("siteSearchCity"));

		if (request.getParameter("siteSearchState") != null)
			siteManage.setSiteSearchState(request
					.getParameter("siteSearchState"));

		if (request.getParameter("siteSearchZip") != null)
			siteManage.setSiteSearchZip(request.getParameter("siteSearchZip"));

		if (request.getParameter("siteSearchCountry") != null)
			siteManage.setSiteSearchCountry(request
					.getParameter("siteSearchCountry"));

		if (request.getParameter("siteSearchGroup") != null)
			siteManage.setSiteSearchGroup(request
					.getParameter("siteSearchGroup"));

		if (request.getParameter("siteSearchEndCustomer") != null)
			siteManage.setSiteSearchEndCustomer(request
					.getParameter("siteSearchEndCustomer"));

		if (request.getParameter("siteSearchStatus") != null)
			siteManage.setSiteSearchStatus(request
					.getParameter("siteSearchStatus"));

		if (request.getParameter("lines_per_page") != null)
			siteManage
					.setLines_per_page(request.getParameter("lines_per_page"));

		if (request.getParameter("current_page_no") != null)
			siteManage.setCurrent_page_no(request
					.getParameter("current_page_no"));

		if (request.getParameter("total_page_size") != null)
			siteManage.setTotal_page_size(request
					.getParameter("total_page_size"));

		if (request.getParameter("org_page_no") != null)
			siteManage.setOrg_page_no(request.getParameter("org_page_no"));

		if (request.getParameter("org_lines_per_page") != null)
			siteManage.setOrg_lines_per_page(request
					.getParameter("org_lines_per_page"));

		if (request.getParameter("siteNameForColumn") != null)
			siteManage.setSiteNameForColumn(request
					.getParameter("siteNameForColumn"));

		if (request.getParameter("siteNumberForColumn") != null)
			siteManage.setSiteNumberForColumn(request
					.getParameter("siteNumberForColumn"));

		if (request.getParameter("siteAddressForColumn") != null)
			siteManage.setSiteAddressForColumn(request
					.getParameter("siteAddressForColumn"));

		if (request.getParameter("siteCityForColumn") != null)
			siteManage.setSiteCityForColumn(request
					.getParameter("siteCityForColumn"));

		if (request.getParameter("siteStateForColumn") != null)
			siteManage.setSiteStateForColumn(request
					.getParameter("siteStateForColumn"));

		if (request.getParameter("siteZipForColumn") != null)
			siteManage.setSiteZipForColumn(request
					.getParameter("siteZipForColumn"));

		if (request.getParameter("siteCountryForColumn") != null)
			siteManage.setSiteCountryForColumn(request
					.getParameter("siteCountryForColumn"));

		if (request.getParameter("jobid") != null) {
			siteManage.setJobid(request.getParameter("jobid"));
			siteManage.setAppendixname(Jobdao.getAppendixname(ds,
					siteManage.getAppendixid()));
			siteManage
					.setJobName(Activitydao.getJobname(
							getDataSource(request, "ilexnewDB"),
							siteManage.getJobid()));
		}
		if (request.getParameter("fromType") != null)
			siteManage.setFromType(request.getParameter("fromType"));

		if (request.getParameter("msaid") != null) {
			siteManage.setMsaid(request.getParameter("msaid"));
			siteManage.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(ds,
					request.getParameter("msaid")));
		}

		DynamicComboCM dcLonDirec = new DynamicComboCM();
		dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
		request.setAttribute("dcLonDirec", dcLonDirec);

		DynamicComboCM dcLatDirec = new DynamicComboCM();
		dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
		request.setAttribute("dcLatDirec", dcLatDirec);

		DynamicComboCM dcSiteStatus = new DynamicComboCM();
		dcSiteStatus.setFirstlevelcatglist(DynamicComboDao.getsitestatus());
		request.setAttribute("dcSiteStatus", dcSiteStatus);

		DynamicComboCM dcStateCM = new DynamicComboCM();
		Pvsdao pvsDao = new Pvsdao();
		dcStateCM.setFirstlevelcatglist(Pvsdao.getCountryStateList(ds));
		request.setAttribute("dcStateCM", dcStateCM);

		DynamicComboCM dcCountryList = new DynamicComboCM();
		dcCountryList.setFirstlevelcatglist(DynamicComboDao.getCountryList(ds));
		request.setAttribute("dcCountryList", dcCountryList);

		DynamicComboCM dcGroupList = new DynamicComboCM();
		dcGroupList.setFirstlevelcatglist(DynamicComboDao
				.getGroupNameListSiteSearch(siteManage.getMsaid(), ds));
		request.setAttribute("dcGroupList", dcGroupList);

		DynamicComboCM dcEndCustomerList = new DynamicComboCM();
		dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
				.getEndCustomerList(ds, siteManage.getMsaid(),
						siteManage.getMsaname()));
		request.setAttribute("dcEndCustomerList", dcEndCustomerList);

		DynamicComboCM dcTimeZoneList = new DynamicComboCM();
		dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao.gettimezone(ds));
		request.setAttribute("dcTimeZoneList", dcTimeZoneList);

		DynamicComboCM dcCategoryList = new DynamicComboCM();
		dcCategoryList.setFirstlevelcatglist(DynamicComboDao
				.getCategoryList(ds));
		request.setAttribute("dcCategoryList", dcCategoryList);

		DynamicComboCM dcRegionList = new DynamicComboCM();
		dcRegionList.setFirstlevelcatglist(DynamicComboDao.getRegionList(ds));
		request.setAttribute("dcRegionList", dcRegionList);

		initialStateCategory = SiteManagedao
				.getStateCategoryList(getDataSource(request, "ilexnewDB"));

		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempcat = ((SiteManagementBean) initialStateCategory.get(i))
						.getSiteCountryId();
				((SiteManagementBean) initialStateCategory.get(i))
						.setTempList(SiteManagedao
								.getCategoryWiseStateList(tempcat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}
		session.setAttribute("siteList", null);
		BeanUtils.copyProperties(siteManageBean, siteManage);
		SiteManagedao.getBlankSite(siteManageBean);
		BeanUtils.copyProperties(siteManage, siteManageBean);

		if (siteManage.getPageType() != null
				&& siteManage.getPageType().equals("webTicket")) {
			if (siteManage.getAppendixid() == null
					&& request.getParameter("appendixid") != null) {
				siteManage.setAppendixid(request.getParameter("appendixid"));
			}
			BeanUtils.copyProperties(siteManageBean, siteManage);
			SiteManagedao.getWebTicketSiteDetails(siteManageBean, ticketNumber,
					ds);
			BeanUtils.copyProperties(siteManage, siteManageBean);
		}

		request.setAttribute("initialStateCategory", initialStateCategory);
		request.setAttribute("appendixid", appid);
		request.setAttribute("AddCase", "Hide");

		// Added this code for the Job Detail Menu.
		if (siteManage.getJobid() != null && siteManage.getFromType() != null) {
			if (!siteManage.getJobid().equals("0")
					&& siteManage.getFromType() != null
					&& siteManage.getFromType().equals("yes")) {
				String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
						siteManage.getJobid(), "%", "%",
						getDataSource(request, "ilexnewDB"));
				String jobStatus = jobStatusAndType[0];
				String jobType1 = jobStatusAndType[1];
				String jobStatusList = "";
				String appendixType = "";

				if (jobType1.equals("Default"))
					jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
							+ siteManage.getJobid() + "&Status='D'\"" + ","
							+ "0";
				if (jobType1.equals("Newjob"))
					jobStatusList = Menu.getStatus("pm_job",
							jobStatus.charAt(0), "", "", siteManage.getJobid(),
							"", loginuserid,
							getDataSource(request, "ilexnewDB"));
				if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
					jobStatusList = "\"Approved\""
							+ ","
							+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
							+ siteManage.getJobid() + "&Status=A\"" + "," + "0";
				if (jobType1.equals("inscopejob")
						&& jobStatus.equals("Approved"))
					jobStatusList = "\"Draft\""
							+ ","
							+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
							+ siteManage.getJobid() + "&Status=D\"" + "," + "0";
				if (jobType1.equals("Addendum"))
					jobStatusList = Menu.getStatus("Addendum",
							jobStatus.charAt(0), "", "", siteManage.getJobid(),
							"", loginuserid,
							getDataSource(request, "ilexnewDB"));

				appendixType = ViewList.getAppendixtypedesc(
						siteManage.getAppendixid(),
						getDataSource(request, "ilexnewDB"));

				request.setAttribute("jobStatus", jobStatus);
				request.setAttribute("job_type", jobType1);
				request.setAttribute("appendixtype", appendixType);
				request.setAttribute("Job_Id", siteManage.getJobid());
				request.setAttribute("Appendix_Id", siteManage.getAppendixid());
				request.setAttribute("jobStatusList", jobStatusList);
				request.setAttribute("addendum_id", "0");

				if (jobType1.equals("Newjob")) {
					request.setAttribute("chkadd", "detailjob");
					request.setAttribute("chkaddendum", "detailjob");
					request.setAttribute("chkaddendumactivity", "View");
				}
				if (jobType1.equals("inscopejob")
						|| jobType1.equals("addendum")) {
					request.setAttribute("chkadd", "inscopejob");
					request.setAttribute("chkaddendum", "inscopejob");
					request.setAttribute("chkaddendumactivity",
							"inscopeactivity");
				}
			}
		}
		// End of Code for Job Detail Menu.

		siteManage.setStateSelect(Util.concatString(siteManage.getState(),
				siteManage.getCountry()));
		forward = mapping.findForward("success");
		return (forward);
	}

	public ActionForward newSite(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		SiteManagementForm siteManage = (SiteManagementForm) form;
		SiteManagementBean siteManageBean = new SiteManagementBean();
		HttpSession session = request.getSession(true);
		String appid = null;
		long[] retValArr = null;
		String action = "A";
		String userid = (String) session.getAttribute("userid");
		int retlatest = 0;

		String state_name = Pvsdao.getStateName(siteManage.getState(),
				getDataSource(request, "ilexnewDB"));
		String LatLong[] = RestClient.getLatitudeLongitude(
				siteManage.getSiteAddress(), siteManage.getSiteCity(),
				state_name, siteManage.getCountry(),
				siteManage.getSiteZipCode(),
				getDataSource(request, "ilexnewDB"));
		siteManage.setLatLongAccuracy(LatLong[0]);
		siteManage.setLatLongSource(LatLong[1]);
		siteManage.setLatitude(LatLong[3]);
		siteManage.setLongitude(LatLong[2]);
		BeanUtils.copyProperties(siteManageBean, siteManage);
		retValArr = SiteManagedao.updateSite(siteManageBean, action, userid,
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("jobid", siteManage.getJobid());
		request.setAttribute("Job_Id", siteManage.getJobid());

		if (siteManage.getFromType() != null) {
			if (siteManage.getFromType().equals("ticketDetail")) {

				request.setAttribute("retVal", retValArr[0] + "");
				request.setAttribute("latestSite", retValArr[1] + "");
				return (mapping.findForward("sitesearch"));
			}
		}

		if (siteManage.getPageType() != null
				&& siteManage.getPageType().equals("webTicket")) {
			request.setAttribute("AddSite", retValArr[0] + "");
			return (mapping.findForward("webTicketPage"));
		}

		if (siteManage.getFromType() != null && siteManage.getFromType() != "")
			forward = mapping.findForward("jobdetail");
		else
			forward = mapping.findForward("jobdashboard");

		return (forward);

	}
}
