package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.prm.SiteHistoryDTO;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.SiteHistorydao;
import com.mind.formbean.PRM.SiteHistoryForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class SiteHistoryAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SiteHistoryForm siteHistoryForm = (SiteHistoryForm) form;
		SiteHistoryDTO siteHistoryDTO = new SiteHistoryDTO();
		ArrayList siteJobList = new ArrayList();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		int sitesJobSize = 0;
		Map<String, Object> map;

		String sortqueryclause = "";

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		/*
		 * if ( request.getAttribute( "querystring" ) != null ) {
		 * sortqueryclause = ( String ) request.getAttribute( "querystring" ); }
		 */

		sortqueryclause = getSortQueryClause(sortqueryclause, session);

		if (request.getParameter("siteid") != null) {
			siteHistoryForm.setSiteId(request.getParameter("siteid"));
		}

		if (request.getParameter("jobid") != null) {
			siteHistoryForm.setJobid(request.getParameter("jobid"));
		}
		if (request.getParameter("Appendix_Id") != null) {
			siteHistoryForm.setAppendixid(request.getParameter("Appendix_Id"));
		}
		if (request.getParameter("ownerId") != null) {
			siteHistoryForm.setOwnerid(request.getParameter("ownerId"));
		}

		if (request.getParameter("viewjobtype") != null) {
			siteHistoryForm.setViewjobtype(request.getParameter("viewjobtype"));
		}
		// System.out.println("sitehistory action owner id is =============="+request.getParameter("ownerId"));

		if (request.getParameter("from") != null) {
			siteHistoryForm.setFrom(request.getParameter("from"));
		} else {
			siteHistoryForm.setFrom(null);
		}
		if (request.getParameter("page") != null) {
			siteHistoryForm.setPage(request.getParameter("page"));
		}
		if (request.getParameter("ref1") != null) {
			siteHistoryForm.setRef1(request.getParameter("ref1"));
		}

		// Following will show the site information for particular job ,comming
		// from site.jsp
		BeanUtils.copyProperties(siteHistoryDTO, siteHistoryForm);
		siteHistoryDTO = SiteHistorydao.getSite(siteHistoryDTO,
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(siteHistoryForm, siteHistoryDTO);

		// Following will show all jobs for the particular site id.
		if (siteHistoryForm.getFrom().equals("netmedx")) {
			siteJobList = SiteHistorydao.getSiteHistoryNetMedx(
					siteHistoryForm.getSiteId(), sortqueryclause,
					getDataSource(request, "ilexnewDB"));
		} else {
			siteJobList = SiteHistorydao.getSiteHistory(
					siteHistoryForm.getSiteId(), sortqueryclause,
					getDataSource(request, "ilexnewDB"));
		}
		if (siteJobList != null) {

			if (siteJobList.size() > 0) {
				sitesJobSize = siteJobList.size();
			}
		}

		request.setAttribute("siteJobList", siteJobList);
		request.setAttribute("sitesJobSize", new Integer(sitesJobSize));

		siteHistoryForm.setAppendixName(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				siteHistoryForm.getAppendixid()));
		siteHistoryForm.setMsaid(IMdao.getMSAId(
				siteHistoryForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")));
		siteHistoryForm
				.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						siteHistoryForm.getMsaid()));

		if (siteHistoryForm.getRef1() != null) {
			if (siteHistoryForm.getRef1().equals("inscopejob")
					|| siteHistoryForm.getRef1().equals("detailjob")) {
				siteHistoryForm.setCheckRef1("true");
			} else
				siteHistoryForm.setCheckRef1("false");
		}

		if (siteHistoryForm.getFrom().equals("site")) {
			siteHistoryForm.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"),
					siteHistoryForm.getJobid()));
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					siteHistoryForm.getJobid(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String jobStatusList = "";
			String appendixType = "";

			if (jobType1.equals("Default"))
				jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
						+ siteHistoryForm.getJobid() + "&Status='D'\"" + ","
						+ "0";
			if (jobType1.equals("Newjob"))
				jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
						"", "", siteHistoryForm.getJobid(), "", loginuserid,
						getDataSource(request, "ilexnewDB"));
			if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
				jobStatusList = "\"Approved\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ siteHistoryForm.getJobid() + "&Status=A\"" + ","
						+ "0";
			if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
				jobStatusList = "\"Draft\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ siteHistoryForm.getJobid() + "&Status=D\"" + ","
						+ "0";
			if (jobType1.equals("Addendum"))
				jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
						"", "", siteHistoryForm.getJobid(), "", loginuserid,
						getDataSource(request, "ilexnewDB"));

			appendixType = ViewList.getAppendixtypedesc(
					siteHistoryForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("jobStatus", jobStatus);
			request.setAttribute("job_type", jobType1);
			request.setAttribute("appendixtype", appendixType);
			request.setAttribute("Job_Id", siteHistoryForm.getJobid());
			request.setAttribute("Appendix_Id", siteHistoryForm.getAppendixid());
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("addendum_id", "0");

			if (jobType1.equals("Newjob")) {
				request.setAttribute("chkadd", "detailjob");
				request.setAttribute("chkaddendum", "detailjob");
				request.setAttribute("chkaddendumactivity", "View");
			}
			if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
				request.setAttribute("chkadd", "inscopejob");
				request.setAttribute("chkaddendum", "inscopejob");
				request.setAttribute("chkaddendumactivity", "inscopeactivity");
			}

		}
		map = DatabaseUtilityDao.setMenu(siteHistoryForm.getJobid(),
				loginuserid, getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		return mapping.findForward("success");
	}

	public String getSortQueryClause(String queryclause, HttpSession session) {
		if (session.getAttribute("sitehist_sort") != null)
			queryclause = (String) session.getAttribute("sitehist_sort");
		return queryclause;
	}
}
