package com.mind.actions.PRM;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.prm.ProjectLevelWorkflowBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.JobNotesDao;
import com.mind.dao.PRM.ProjectLevelWorkflowDao;
import com.mind.formbean.PRM.ProjectLevelWorkflowForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ProjectLevelWorkflowAction extends
		com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ProjectLevelWorkflowAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Appendix",
				"Project Level - Workflow Checklist Items",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */

		ProjectLevelWorkflowForm projectLevelWorkflowForm = (ProjectLevelWorkflowForm) form;
		ProjectLevelWorkflowBean projectLevelWorkflowBean = new ProjectLevelWorkflowBean();
		ProjectLevelWorkflowDao projectLevelWorkflowDao = new ProjectLevelWorkflowDao();
		projectLevelWorkflowForm.setAppendixId((request
				.getParameter("appendixid") == null ? projectLevelWorkflowForm
				.getAppendixId() : request.getParameter("appendixid")));
		projectLevelWorkflowForm.setJobId((String) (request
				.getParameter("jobId") == null ? request.getAttribute("jobid")
				: request.getParameter("jobId")));

		boolean isNetMedX = JobNotesDao.isNetMedx(
				projectLevelWorkflowForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		Map<String, Object> map;
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - isNetMedx  "
					+ isNetMedX);
		}
		if (isNetMedX) {
			request.setAttribute("isNetMedX", "isNetMedX");
			map = DatabaseUtilityDao.setNetMedXMenu(
					projectLevelWorkflowForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		} else {
			map = DatabaseUtilityDao.setAppendixMenu(
					projectLevelWorkflowForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
			try {
				BeanUtils.copyProperties(projectLevelWorkflowBean,
						projectLevelWorkflowForm);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			projectLevelWorkflowDao.getAppendixInformation(
					projectLevelWorkflowBean.getAppendixId(),
					projectLevelWorkflowBean,
					getDataSource(request, "ilexnewDB"));
			try {
				BeanUtils.copyProperties(projectLevelWorkflowForm,
						projectLevelWorkflowBean);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
		}

		ArrayList projectWorkflowItemList = ProjectLevelWorkflowDao
				.fetchPrjCheckListItems(
						new Long(projectLevelWorkflowForm.getAppendixId()),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("projectWorkflowItemList", projectWorkflowItemList);
		return mapping.findForward("Workflow");
	}

	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ProjectLevelWorkflowForm projectLevelWorkflowForm = (ProjectLevelWorkflowForm) form;

		ProjectLevelWorkflowDao projectLevelWorkflowDao = new ProjectLevelWorkflowDao();
		ProjectLevelWorkflowBean projectLevelWorkflowBean = new ProjectLevelWorkflowBean();

		String userId = (String) request.getSession(false).getAttribute(
				"userid");
		// String userName =
		// (String)request.getSession(false).getAttribute("username");
		try {
			BeanUtils.copyProperties(projectLevelWorkflowBean,
					projectLevelWorkflowForm);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		boolean result = projectLevelWorkflowDao.upsertCheckListItems(
				projectLevelWorkflowBean, getDataSource(request, "ilexnewDB"),
				userId);
		if (!result) {
			request.setAttribute("updatedSuccess", "updatedSuccess");
		} else {
			request.setAttribute("updatedFail", "updatedFail");
		}
		boolean isNetMedX = JobNotesDao.isNetMedx(
				projectLevelWorkflowForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		Map<String, Object> map;
		if (isNetMedX) {
			request.setAttribute("isNetMedX", "isNetMedX");
			map = DatabaseUtilityDao.setNetMedXMenu(
					projectLevelWorkflowForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		} else {
			map = DatabaseUtilityDao.setAppendixMenu(
					projectLevelWorkflowForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
			ProjectLevelWorkflowDao.getAppendixInformation(
					projectLevelWorkflowForm.getAppendixId(),
					projectLevelWorkflowBean,
					getDataSource(request, "ilexnewDB"));
			try {
				BeanUtils.copyProperties(projectLevelWorkflowForm,
						projectLevelWorkflowBean);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
		}

		ArrayList projectWorkflowItemList = ProjectLevelWorkflowDao
				.fetchPrjCheckListItems(
						new Long(projectLevelWorkflowForm.getAppendixId()),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("projectWorkflowItemList", projectWorkflowItemList);
		return mapping.findForward("Workflow");
	}

}
