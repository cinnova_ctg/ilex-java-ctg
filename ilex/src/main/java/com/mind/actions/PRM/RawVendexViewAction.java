package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PMT.EditMasterlDao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PRM.RawVendexViewForm;
import com.mind.newjobdb.dao.VendexDAO;

public class RawVendexViewAction extends com.mind.common.IlexDispatchAction {
	
	public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse responce ) {
		

		RawVendexViewForm rawForm = (RawVendexViewForm)form;
		
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		
		if(request.getParameter("msaid") != null) {
			rawForm.setMsaId(request.getParameter("msaid"));
		}	
		if( !Authenticationdao.getPageSecurity( loginuserid , "Manage Job" , "Vendex Raw" , getDataSource( request , "ilexnewDB" ) ) ) {
			return( mapping.findForward( "UnAuthenticate" ) );
			//return( mapping.findForward( "UnAuthenticate" ) ); // - Check for Pager Security.
		}
		setCombo(rawForm,request);
		rawForm.setRadius("50");
		return mapping.findForward("success");
	}
	public ActionForward SearchRawView(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse responce ) {
		RawVendexViewForm rawForm = (RawVendexViewForm)form;
		
		String partnerXml = "";
		String siteXml = "";
		if(request.getParameter("msaid") != null) {
			rawForm.setMsaId(request.getParameter("msaid"));
		}
		ArrayList siteList = new ArrayList();
		ArrayList partnerList = new ArrayList();
		
		partnerList = VendexDAO.getallSitePartnerList(rawForm.getMsaId(),rawForm.getEndCustomer(),rawForm.getZipCode(),rawForm.getRadius(),getDataSource(request,"ilexnewDB"));
		partnerXml=Pvsdao.generateXML(partnerList);
		siteXml= VendexDAO.getSiteListOfMSA(rawForm.getMsaId(),rawForm.getEndCustomer(),rawForm.getZipCode(),rawForm.getRadius(),getDataSource(request,"ilexnewDB"));
		//System.out.println(partnerXml);
		int size = 10;
	
		request.setAttribute("partnerXML",partnerXml);
		request.setAttribute("siteXML",siteXml);
		request.setAttribute("size",size);
		request.setAttribute("searchedResult","searchedResult");
		
		String checkXML = Pvsdao.convertCheckRatingToXML(rawForm.getZipCode(),rawForm.getRadius(),"",getDataSource(request,"ilexnewDB"));
		request.setAttribute("checkXML",checkXML);
		//System.out.println("checkXML - "+request.getAttribute("checkXML"));
		setCombo(rawForm,request);
		return mapping.findForward("success");
	}
	public ActionForward MasterRawVedexView(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse responce ) {
		RawVendexViewForm rawForm = (RawVendexViewForm)form;
		
		ArrayList msaList = EditMasterlDao.getRawVendexList(getDataSource(request,"ilexnewDB"));
		rawForm.setMsaList(msaList);
		
		String endCUstomerMsa[] = EditMasterlDao.getCheckedEndCustomer( getDataSource(request, "ilexnewDB"));
		rawForm.setCheck(endCUstomerMsa);
		
		return mapping.findForward("Masterpage");
	}
	
	public ActionForward MasterRawVedexSave(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse responce ) {
		RawVendexViewForm rawForm = (RawVendexViewForm)form;
		
		String id ="";
		String previousMsaId = "";
		if(rawForm.getCheck()!=null && rawForm.getCheck().length > 0) {
			//previousMsaId = rawForm.getCheck(0).substring(0,rawForm.getCheck(0).indexOf("-"));
			for(int i =0; i<rawForm.getCheck().length; i++ ) {
				if(rawForm.getCheck(i).substring(0,rawForm.getCheck(i).indexOf("^^")).equals(previousMsaId)) {
						id = id + "^^"+rawForm.getCheck(i).substring((rawForm.getCheck(i).indexOf("^^")+2),rawForm.getCheck(i).length());
					//System.out.println(rawForm.getCheck(i).substring((rawForm.getCheck(i).indexOf("^^")+2),rawForm.getCheck(i).length()));
				} else {
					id = id + "^^~"+rawForm.getCheck(i);
				}
				previousMsaId = rawForm.getCheck(i).substring(0,rawForm.getCheck(i).indexOf("^^"));
			}
		}
		id =  id + "^^~";
		//System.out.println("string MSa ID + End Customer--= "+id);
		int result= EditMasterlDao.updateMSAEndCustomer(id,getDataSource(request,"ilexnewDB"));
		if(result==0)
			request.setAttribute("Successfully",result);
		
		ArrayList msaList = EditMasterlDao.getRawVendexList(getDataSource(request,"ilexnewDB"));
		rawForm.setMsaList(msaList);
		
		String endCUstomerMsa[] = EditMasterlDao.getCheckedEndCustomer( getDataSource(request, "ilexnewDB"));
		rawForm.setCheck(endCUstomerMsa);
		//System.out.println("length of checked MSA"+rawForm.getCheck().length);
		
		return mapping.findForward("Masterpage");
	}
	
	public void setCombo(RawVendexViewForm rawForm,HttpServletRequest request) {
		if(request.getParameter("msaid") != null) {
			rawForm.setMsaId(request.getParameter("msaid"));
		}
		rawForm.setEndCustomeList(VendexDAO.getEndCustomerList(rawForm.getMsaId(), getDataSource(request, "ilexnewDB")));
		rawForm.setRadiusList(VendexDAO.getRadius());
		String msaName =  com.mind.dao.PM.Appendixdao.getMsaname(getDataSource( request , "ilexnewDB" ),rawForm.getMsaId());
		boolean isvendex = EditMasterlDao.isRawVenedexed(rawForm.getMsaId(), getDataSource( request , "ilexnewDB" ));
		if(isvendex==true) {
			request.setAttribute("viewRawVendex", "viewRawVendex");
		}
		request.setAttribute("msaName", msaName);
	}

}
