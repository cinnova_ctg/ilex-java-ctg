package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCNSWEB;
import com.mind.bean.prm.POWOBean;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.PartnerSOWAssumptiondao;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PMT.EditMasterlDao;
import com.mind.dao.PRM.POWODetail;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.formbean.PRM.POWOForm;
import com.mind.xml.Domxmltaglist;

public class POWODetailAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionForward forward = new ActionForward();
		POWOForm powoform = (POWOForm) form;
		POWOBean powoBean = new POWOBean();
		int resource_size = 0;
		String deliverbydate = "";
		String appendix_id = "";
		String checkFieldsheet = "0";

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("jobid") != null) {
			powoform.setJobid(request.getParameter("jobid"));
		}

		if (request.getParameter("docMError") != null) {
			request.setAttribute("docMError", "true");
		}

		if (request.getParameter("powoid") != null) {
			powoform.setPowoid(request.getParameter("powoid"));
		}

		if (request.getParameter("powono") != null) {
			powoform.setPowono(request.getParameter("powono"));
		}

		if (request.getParameter("appendixid") != null) {
			appendix_id = request.getParameter("appendixid");
			powoform.setAppendixid(request.getParameter("appendixid"));
		} else {
			appendix_id = IMdao.getAppendixId(powoform.getJobid(),
					getDataSource(request, "ilexnewDB"));
			powoform.setAppendixid(IMdao.getAppendixId(powoform.getJobid(),
					getDataSource(request, "ilexnewDB")));
		}
		if (request.getParameter("firsttime") != null) {
			powoform.setFirsttime(request.getParameter("firsttime"));
		} else
			powoform.setFirsttime("true");

		// for netmedx dashboard back button
		if (request.getParameter("nettype") != null)
			powoform.setNettype("" + request.getParameter("nettype"));
		if (request.getParameter("viewjobtype") != null) {
			powoform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			powoform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			powoform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			powoform.setOwnerId("%");
		}
		request.setAttribute("viewjobtype", powoform.getViewjobtype());

		/* Get default sp instruction */
		String defaultSpecInst = EditMasterlDao.getEmailMessage("pospinst",
				getDataSource(request, "ilexnewDB"));
		if (defaultSpecInst.indexOf("~") >= 0)
			defaultSpecInst = Domxmltaglist.addParameter(defaultSpecInst,
					powoform.getPowoid(), "POSPINST", loginuserid,
					powoform.getJobid(), getDataSource(request, "ilexnewDB"));

		powoform.setDeliverableList(POWODetaildao
				.getDeliverableList(getDataSource(request, "ilexnewDB")));
		powoform.setMasterpritemlist(POWODetaildao
				.getPWOdetailmasterpotype(getDataSource(request, "ilexnewDB")));
		powoform.setTypelist(POWODetaildao.getPWOdetailtype(getDataSource(
				request, "ilexnewDB")));
		powoform.setTermslist(POWODetaildao.getPWOdetailterms(getDataSource(
				request, "ilexnewDB")));
		powoform.setJobname(Activitydao.getJobname(
				getDataSource(request, "ilexnewDB"), powoform.getJobid()));
		powoform.setAppendixname(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"), powoform.getAppendixid()));
		powoform.setAppendixType(ViewList.getAppendixtypedesc(
				powoform.getAppendixid(), getDataSource(request, "ilexnewDB")));

		powoform.setResourcelist(POWODetaildao.getPWOdetailtabular(
				powoform.getJobid(), powoform.getPowoid(),
				powoform.getMasterpoitem(), getDataSource(request, "ilexnewDB")));
		DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();
		dynamiccomboCNSWEB.setHourname(DynamicComboDao.getHourName());
		dynamiccomboCNSWEB.setMinutename(DynamicComboDao.getMinuteName());
		dynamiccomboCNSWEB.setOptionname(DynamicComboDao.getOptionName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		if (powoform.getResource_authorizedunitcost() != null) {
			for (int i = 0; i < powoform.getResource_authorizedunitcost().length; i++) {

				((POWODetail) powoform.getResourcelist().get(i))
						.setResource_authorizedqty(powoform
								.getResource_authorizedqty(i));
				((POWODetail) powoform.getResourcelist().get(i))
						.setResource_authorizedunitcost(powoform
								.getResource_authorizedunitcost(i));
				((POWODetail) powoform.getResourcelist().get(i))
						.setResource_authorizedtotalcost(powoform
								.getResource_authorizedtotalcost(i));
			}
		}
		String savedresources[] = POWODetaildao.getSavedResources(
				powoform.getPowoid(), "resource",
				getDataSource(request, "ilexnewDB"));
		String showonwo[] = POWODetaildao.getSavedResources(
				powoform.getPowoid(), "showonwo",
				getDataSource(request, "ilexnewDB"));
		String dropshipped[] = POWODetaildao.getSavedResources(
				powoform.getPowoid(), "dropshipped",
				getDataSource(request, "ilexnewDB"));

		// Start :Added By Amit For PVS_Supplied
		String pvs_supplied[] = POWODetaildao.getSavedResources(
				powoform.getPowoid(), "pvs_supplied",
				getDataSource(request, "ilexnewDB"));
		// End :Added By Amit

		// changes made by Seema
		String credit_card[] = POWODetaildao.getSavedResources(
				powoform.getPowoid(), "credit_card",
				getDataSource(request, "ilexnewDB"));
		// over

		if (savedresources.length == 0)
			powoform.setFirsttime("true");
		else
			powoform.setFirsttime("false");

		if (powoform.getNext() != null || powoform.getResetsow() != null
				|| powoform.getDefaultSow() != null
				|| powoform.getResetassumption() != null
				|| powoform.getDefaultAssumption() != null
				|| powoform.getResetSPCond() != null
				|| powoform.getResetSPInst() != null) {
			String sowassumption = "";
			String tempSowAssumption = "";
			String tempResourceId = "";
			String prevSOW = powoform.getSow();
			String prevAssumption = powoform.getAssumption();

			if (powoform.getResource_id() != null) {

				String tempid[] = new String[powoform.getResource_id().length];

				/* Get customer SOW */
				if (powoform.getNext() != null
						|| powoform.getDefaultSow() != null) {

					for (int i = 0; i < powoform.getResource_id().length; i++) {
						tempSowAssumption = POWODetaildao.getSowAssumption(
								powoform.getResource_id(i), "sow",
								getDataSource(request, "ilexnewDB"));
						if (!tempSowAssumption.equals(""))
							sowassumption = sowassumption + tempSowAssumption
									+ "\n";
					}
					sowassumption = sowassumption == null ? sowassumption
							: sowassumption.trim();

					powoform.setSow(sowassumption);

					/* For NetMedX PO */
					if (powoform.getNext() != null
							&& powoform.getAppendixType().equals("NetMedX"))
						powoform.setSow(prevSOW);

					/*
					 * Comment for release 1.261 powoform.setSow(
					 * powoform.getProblemdesc() + "\n" + sowassumption );
					 * 
					 * if( powoform.getResetsow() != null ) {
					 * powoform.setSpeccond( powoform.getTempspeccond() ); }
					 */
				}

				if (powoform.getNext() != null
						|| powoform.getResetsow() != null) {

					for (int i = 0; i < powoform.getResource_id().length; i++) {
						tempResourceId = tempResourceId + ","
								+ powoform.getResource_id(i);
					}
					/* Get default partner sow */
					tempResourceId = tempResourceId.substring(1,
							tempResourceId.length());
					if (tempResourceId.length() > 0)
						powoform.setSow(PartnerSOWAssumptiondao
								.getDefaultPartnerSOWAssumption(tempResourceId,
										"prm_act", "sow", "partner",
										getDataSource(request, "ilexnewDB")));

					/* For NetMedX PO */
					if (powoform.getNext() != null
							&& powoform.getAppendixType().equals("NetMedX"))
						powoform.setSow(prevSOW);
				}

				sowassumption = "";
				/* Get customer Assumptions */
				if (powoform.getNext() != null
						|| powoform.getDefaultAssumption() != null) {
					for (int i = 0; i < powoform.getResource_id().length; i++) {
						tempSowAssumption = POWODetaildao.getSowAssumption(
								powoform.getResource_id(i), "",
								getDataSource(request, "ilexnewDB"));
						if (!tempSowAssumption.equals(""))
							sowassumption = sowassumption + tempSowAssumption
									+ "\n";
					}
					powoform.setAssumption(sowassumption);

					/* For NetMedX PO */
					if (powoform.getNext() != null
							&& powoform.getAppendixType().equals("NetMedX"))
						powoform.setAssumption(prevAssumption);

					/*
					 * Comment for release 1.261 if(
					 * powoform.getResetassumption() != null ) {
					 * powoform.setSpeccond( powoform.getTempspeccond() ); }
					 */
				}

				if (powoform.getNext() != null
						|| powoform.getResetassumption() != null) {
					for (int i = 0; i < powoform.getResource_id().length; i++) {
						tempResourceId = tempResourceId + ","
								+ powoform.getResource_id(i);
					}

					/* Get default partner sow */
					tempResourceId = tempResourceId.substring(1,
							tempResourceId.length());
					if (tempResourceId.length() > 0)
						powoform.setAssumption(PartnerSOWAssumptiondao
								.getDefaultPartnerSOWAssumption(tempResourceId,
										"prm_act", "assumption", "partner",
										getDataSource(request, "ilexnewDB")));

					/* For NetMedX PO */
					if (powoform.getNext() != null
							&& powoform.getAppendixType().equals("NetMedX"))
						powoform.setAssumption(prevAssumption);
				}

				if (powoform.getResetSPInst() != null) {
					powoform.setSpecialinstruction(PartnerSOWAssumptiondao
							.getDefaultPartnerSOWAssumption(
									powoform.getAppendixid(), "prm_app", "si",
									"partner",
									getDataSource(request, "ilexnewDB")));

					if ((powoform.getSpecialinstruction().trim()).length() > 0)
						powoform.setSpecialinstruction(powoform
								.getSpecialinstruction().trim()
								+ "\n"
								+ "\n"
								+ defaultSpecInst);
					else
						powoform.setSpecialinstruction(defaultSpecInst);
				}

				if (powoform.getResetSPCond() != null) {
					powoform.setSpeccond(PartnerSOWAssumptiondao
							.getDefaultPartnerSOWAssumption(
									powoform.getAppendixid(), "prm_app", "sc",
									"partner",
									getDataSource(request, "ilexnewDB")));
				}

				for (int i = 0; i < powoform.getResource_id().length; i++) {
					tempid[i] = powoform.getResource_id(i);
				}

			}
			powoform.setFirsttime("false");
		}

		// Start :For Save
		if (powoform.getSave() != null) {
			int checklength = 0;
			if (powoform.getResource_id() != null) {
				checklength = powoform.getResource_id().length;
			}

			int[] index = new int[checklength];
			int k = 0;
			if (checklength > 0) {
				int length = powoform.getResource_tempid().length;
				for (int i = 0; i < checklength; i++) {
					for (int j = 0; j < length; j++) {
						if (powoform.getResource_id(i).equals(
								powoform.getResource_tempid(j))) {
							index[k] = j;
							k++;
						}
					}
				}
			}
			String t1 = "";
			String t2 = "";
			String t3 = "";
			String t4 = "";
			String t5 = "";
			String t6 = "";
			String t7 = "";

			// changes made by Seema
			String t8 = "";
			if (checklength > 0) {
				for (int i = 0; i < index.length; i++) {
					t1 = t1 + powoform.getResource_tempid(index[i]) + ",";
					t2 = t2 + powoform.getResource_authorizedunitcost(index[i])
							+ ",";
					t3 = t3
							+ powoform
									.getResource_authorizedtotalcost(index[i])
							+ ",";
					t6 = t6 + powoform.getResource_authorizedqty(index[i])
							+ ",";
				}
			}
			if (powoform.getResource_showonwo() != null) {

				for (int i = 0; i < powoform.getResource_showonwo().length; i++) {
					t4 = t4 + powoform.getResource_showonwo(i) + ",";
				}
				t4 = t4.substring(0, t4.length() - 1);
			}

			if (powoform.getResource_dropshift() != null) {

				for (int i = 0; i < powoform.getResource_dropshift().length; i++) {
					t5 = t5 + powoform.getResource_dropshift(i) + ",";
				}
				t5 = t5.substring(0, t5.length() - 1);
			}

			// Start :Added By Amit for pvs_supplied
			if (powoform.getPvs_supplied() != null) {
				for (int i = 0; i < powoform.getPvs_supplied().length; i++) {
					t7 = t7 + powoform.getPvs_supplied(i) + ",";
				}
				t7 = t7.substring(0, t7.length() - 1);
			}
			// End :Added By Amit

			// changes made by Seema
			if (powoform.getCredit_card() != null) {
				for (int i = 0; i < powoform.getCredit_card().length; i++) {
					t8 = t8 + powoform.getCredit_card(i) + ",";
				}
				t8 = t8.substring(0, t8.length() - 1);
			}

			// set dates in proper format
			deliverbydate = powoform.getDeliverbydate();
			if (!powoform.getDeliverbydatehh().equals("  "))
				deliverbydate += " " + powoform.getDeliverbydatehh() + ":"
						+ powoform.getDeliverbydatemm() + " "
						+ powoform.getDeliverbydateop();
			BeanUtils.copyProperties(powoBean, powoform);
			int flag = POWODetaildao.updatepowodata(powoBean, t1, t2, t3, t4,
					t5, deliverbydate, loginuserid, t6, t7, t8,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(powoform, powoBean);
			request.setAttribute("jobid", powoform.getJobid());

			request.setAttribute("updateflag", flag + "");
			request.setAttribute("powono", powoform.getPowono());

			return (mapping.findForward("powodashboard"));
		}
		// //End :For Save
		String[] checkMinuteman = POWODetaildao.getMinutemanType(
				powoform.getPowoid(), getDataSource(request, "ilexnewDB"));
		String poPayment = POWODetaildao.getPoPaymentValue(
				powoform.getPowoid(), getDataSource(request, "ilexnewDB"));
		if ((!(powoform.getNext() != null))
				&& (!(powoform.getRefflag() != null))
				&& (!(powoform.getResetsow() != null))
				&& (!(powoform.getDefaultSow() != null))
				&& (!(powoform.getResetassumption() != null))
				&& (!(powoform.getDefaultAssumption() != null))
				&& (!(powoform.getResetSPInst() != null))
				&& (!(powoform.getResetSPCond() != null))) {
			powoform.setResource_id(savedresources);
			powoform.setResource_showonwo(showonwo);
			powoform.setResource_dropshift(dropshipped);

			// Start :Added By Amit For pvs_supplied
			powoform.setPvs_supplied(pvs_supplied);
			// End: Added By Amit For pvs_supplied

			powoform.setCredit_card(credit_card);
			BeanUtils.copyProperties(powoBean, powoform);
			powoBean = POWODetaildao.getPWOdetaildata(powoBean,
					powoform.getPowoid(), powoform.getAppendixType(),
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(powoform, powoBean);
			powoform.setNonContractEstTotal(POWODetaildao
					.getNonContractEstTotalCost(powoform.getPowoid(),
							getDataSource(request, "ilexnewDB")));
			/* Append master special instruction when page is load first time */
			if (powoform.getFirsttime().equals("true"))

				if (powoform.getSpecialinstruction() != null) {
					if ((powoform.getSpecialinstruction().trim()).length() > 0)
						powoform.setSpecialinstruction(powoform
								.getSpecialinstruction().trim()
								+ "\n"
								+ "\n"
								+ defaultSpecInst);
					else
						powoform.setSpecialinstruction(defaultSpecInst);
				} else
					powoform.setSpecialinstruction(defaultSpecInst);

			powoform.setTempspeccond(powoform.getSpeccond());

			if (powoform.getFirsttime().equals("true")
					&& powoform.getType().equals("")) {
				powoform.setChangePoPayment(true);
			}
			if (checkMinuteman[0] != null && checkMinuteman[0].equals("M")) {
				powoform.setType("M");
				powoform.setCheckDefault("true");
				powoform.setPartnerIsMinuteman("yyy");
			}
		}

		if (powoform.getResourcelist() != null) {
			resource_size = powoform.getResourcelist().size();
		}

		powoform.setPartnername(POWODetaildao.getPartnername(
				getDataSource(request, "ilexnewDB"), powoform.getPartnerid()));
		powoform.setPartnerpoclist(POWODetaildao.getPartnerPOC(
				powoform.getPartnerid(), getDataSource(request, "ilexnewDB")));

		if (poPayment != null && !poPayment.equals("N")) {
			request.setAttribute("poPayment", poPayment);
		}

		/* for field sheet */
		checkFieldsheet = POWODetaildao.getCheckFieldSheet(appendix_id,
				getDataSource(request, "ilexnewDB"));

		if (checkFieldsheet.equals("1")
				&& powoform.getFirsttime().equals("true"))
			checkFieldsheet = "1";
		else
			checkFieldsheet = "0";
		request.setAttribute("checkFieldsheet", checkFieldsheet);

		request.setAttribute("resource_size", new Integer(resource_size));
		request.setAttribute("temp_jobid", powoform.getJobid());
		if (checkMinuteman[1].equals("")) {
			request.setAttribute("unSave", "unSave");
			if ((checkMinuteman[0] != null && checkMinuteman[0].equals("M"))
					&& (powoform.getRefbyonchange() != null && powoform
							.getRefbyonchange().equals("false"))) {
				powoform.setMasterpoitem("1");
			}
		}

		return (mapping.findForward("success"));
	}
}
