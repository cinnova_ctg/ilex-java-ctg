package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCNSWEB;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.TicketsPerMsadao;
import com.mind.formbean.PRM.TicketsPerMsaForm;

public class TicketsPerMsaAction extends com.mind.common.IlexDispatchAction
{

	public ActionForward msaticketlist(ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response)throws Exception 
	{
		TicketsPerMsaForm ticketsPerMsaForm = ( TicketsPerMsaForm ) form;
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if( !Authenticationdao.getPageSecurity( userid , "Manage Project" , "View Tickets Per MSA" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		
		/* Page Security End*/
		
		String appendixid = "";
		String msaname = "";
		
		String ref = "";
		ArrayList list = new ArrayList();
		int size=0;

		DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();
		
		dynamiccomboCNSWEB.setMonthname(TicketsPerMsadao.getSixMonth(getDataSource(request , "ilexnewDB")));   
	    request.setAttribute("dynamiccomboCNSWEB",dynamiccomboCNSWEB);
		
		
		
		if(ticketsPerMsaForm.getMonthName() != null) {
				ticketsPerMsaForm.setYearValue(ticketsPerMsaForm.getMonthName().substring(0, 4));
				ticketsPerMsaForm.setMonthValue(TicketsPerMsadao.getMonthName(Integer.parseInt(ticketsPerMsaForm.getMonthName().substring(ticketsPerMsaForm.getMonthName().indexOf(",")+1, ticketsPerMsaForm.getMonthName().length()))));
		}

		else {
				Calendar calender = Calendar.getInstance();
				ticketsPerMsaForm.setMonthValue(TicketsPerMsadao.getMonthName(calender.get(Calendar.MONTH)+1));
				ticketsPerMsaForm.setYearValue(""+calender.get(Calendar.YEAR));
		}	

		list = TicketsPerMsadao.getMsaticketList(ticketsPerMsaForm.getMonthName(), getDataSource(request , "ilexnewDB"));
		request.setAttribute("msaticketlist",list);
		size=list.size();
		request.setAttribute("size",size+"");
		
	
		return( mapping.findForward( "msaticketlist" ) );	
	}

}
