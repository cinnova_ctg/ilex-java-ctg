package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.SiteChecklistdao;
import com.mind.formbean.PRM.SiteChecklistForm;

public class SiteChecklistAction extends com.mind.common.IlexDispatchAction
{

	public ActionForward update(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		SiteChecklistForm siteChecklistForm = (SiteChecklistForm) form;
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if( siteChecklistForm.getAuthenticate().equals( "" ) )
		{
			if(request.getParameter("fromtype")!=null)
			{
				if( request.getParameter( "page" ).equals( "appendixdashboard" ) )
				{
					if(request.getParameter("fromtype").equals("S"))
					{
						if( !Authenticationdao.getPageSecurity( userid , "Manage Job" , "Process Site Checklist" , getDataSource( request , "ilexnewDB" ) ) ) 
						{
							return( mapping.findForward( "UnAuthenticate" ) );	
						}
					}
					else
					{
						if( !Authenticationdao.getPageSecurity( userid , "Manage Job" , "Process QC Checklist" , getDataSource( request , "ilexnewDB" ) ) ) 
						{
							return( mapping.findForward( "UnAuthenticate" ) );	
						}
						
					}
				}
			}
			
		}
		/* Page Security End*/
		
		if(request.getParameter("nettype") != null)  { 
			siteChecklistForm.setNettype(""+request.getParameter("nettype")); 
		}
		
		if(request.getParameter("viewjobtype") != null)  { 
			siteChecklistForm.setViewjobtype(request.getParameter("viewjobtype")); 
		}
		else
		{
			siteChecklistForm.setViewjobtype("A");
		}
		request.setAttribute( "viewjobtype" , siteChecklistForm.getViewjobtype() );
		
		if(request.getParameter("ownerId") != null)  { 
			siteChecklistForm.setOwnerId(""+request.getParameter("ownerId")); 
		}
		else
		{
			siteChecklistForm.setOwnerId("%");
		}
		
		
		ArrayList checklist=new ArrayList();
		ArrayList savedchecklist=new ArrayList();
		String siteid="0";
		String typeid="0";
		String type="";
		String page="";
		String pageid="";
		String sitename="";
		String notfoundmessage="0";
		String saveflag="0";
		int statusvalue=0;
		String[] checkeditems; 
		
		String fromtype="";
		
		
		
		if(siteChecklistForm.getSave()!=null)
		{
			typeid=siteChecklistForm.getTypeid();
			type=siteChecklistForm.getType();
			page=siteChecklistForm.getPage();
			pageid=siteChecklistForm.getPageid();
			fromtype=siteChecklistForm.getFromtype();
			int checklengthi = 0;
			
			if( siteChecklistForm.getSitecheckbox()!=  null )
			{
				 checklengthi = siteChecklistForm.getSitecheckbox().length;
			}
			
			
			//int[] indexi = new int[checklengthi];
			String index="";
			String stringi="";
			for(int i=0;i<checklengthi;i++)
			{
				if(siteChecklistForm.getSitecheckbox(i)!=null)	
				{
					
					//indexi[i] = Integer.parseInt (siteChecklistForm.getSitecheckbox(i));
					index = siteChecklistForm.getSitecheckbox(i);
					
					if(i==(checklengthi-1))
					{
						stringi=stringi+index;
					}
					else
					{
						stringi=stringi+index+",";
					}
				
				}
							
			}
			
			statusvalue=SiteChecklistdao.addChecklistitem(stringi,siteChecklistForm.getSiteid(),siteChecklistForm.getTypeid(),userid,getDataSource( request , "ilexnewDB" ),siteChecklistForm.getFromtype());
			request.setAttribute("addmessage",statusvalue+"");
			siteChecklistForm.getSitecheckbox();
			siteChecklistForm.setTypeid(typeid);
			siteChecklistForm.setType(type);
			siteChecklistForm.setPage(page);
			siteChecklistForm.setPageid(pageid);
			siteChecklistForm.setFromtype(fromtype);
			
			if(statusvalue==0)
			{
				saveflag="1";
				savedchecklist=SiteChecklistdao.getSavedchecklist(stringi,getDataSource( request , "ilexnewDB" ),siteChecklistForm.getFromtype());
				request.setAttribute("savedchecklist",savedchecklist);
				String temppath="PRMJobProcessChecklist";
				request.setAttribute( "type" , temppath );
				request.setAttribute( "appendix_Id" , siteChecklistForm.getPageid() );
				
				if( siteChecklistForm.getPage().equals( "jobdashboard" ) )
				{
					request.setAttribute( "jobid" , siteChecklistForm.getTypeid() );
					return( mapping.findForward( "jobdashboard" ) );
				}
				else
				{
					return( mapping.findForward( "temppage" ) );
				}
				
				
			}
			
			
		}
		else
		{
			
			if(request.getParameter("typeid")!=null)
			{
				typeid=request.getParameter("typeid");
			}
			if(request.getParameter("type")!=null)
			{
				type=request.getParameter("type");
			}
			if(request.getParameter("function")!=null)
			{
				siteChecklistForm.setFunction(request.getParameter("function"));
			}
			if(request.getParameter("page")!=null)
			{
				page=request.getParameter("page");
			}
			if(request.getParameter("pageid")!=null)
			{
				pageid=request.getParameter("pageid");
			}
			if(request.getParameter("fromtype")!=null)
			{
				fromtype=request.getParameter("fromtype");
			}
			siteChecklistForm.setTypeid(typeid);
			siteChecklistForm.setType(type);
			siteChecklistForm.setPage(page);
			siteChecklistForm.setPageid(pageid);
			siteChecklistForm.setFromtype(fromtype);
			if(fromtype.equals("S"))
			{
				siteid=SiteChecklistdao.getSiteid(typeid,getDataSource( request , "ilexnewDB" ));
			}
			else
			{
				//here siteid is partnerid
				siteid=request.getParameter("partnerid");
				
			}
			siteChecklistForm.setSiteid(siteid);
			if(siteid!=null)
			{
				sitename=SiteChecklistdao.getSitename(siteid,getDataSource( request , "ilexnewDB" ),fromtype);
				checklist=SiteChecklistdao.getSitechecklist(siteChecklistForm.getSiteid(),getDataSource( request , "ilexnewDB" ),fromtype,siteChecklistForm.getTypeid());
				
				request.setAttribute("checklist",checklist);
				request.setAttribute("checklistsize",checklist.size()+"");
				
			}
			else
			{
				notfoundmessage="1";
				request.setAttribute("notfoundmessage",notfoundmessage);
				
			}
			siteChecklistForm.setSitename(sitename);
			
			
		}
				
		checkeditems = SiteChecklistdao.getcheckedsiteitem(siteChecklistForm.getSiteid(),siteChecklistForm.getTypeid(),getDataSource( request , "ilexnewDB" ),siteChecklistForm.getFromtype());
		
		
		if(checkeditems != null) {
			if(checkeditems.length>0) {
				siteChecklistForm.setSitecheckbox(checkeditems);
			}
		}
		
		request.setAttribute("typeid",siteChecklistForm.getTypeid());
		request.setAttribute("type",siteChecklistForm.getType());
		request.setAttribute("page",siteChecklistForm.getPage());
		request.setAttribute("pageid",siteChecklistForm.getPageid());
		request.setAttribute("fromtype",siteChecklistForm.getFromtype());
		request.setAttribute("saveflag",saveflag);
		forward = mapping.findForward("success");
		return (forward);
	}
	
}
