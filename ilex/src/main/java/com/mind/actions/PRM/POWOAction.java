package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.PO.dao.PODao;
import com.mind.bean.prm.POWOBean;
import com.mind.bean.pvs.PartnerDetailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.POWOdao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.docm.dao.DocumentMasterDao;
import com.mind.docm.utils.DocumentUtility;
import com.mind.formbean.PRM.POWOForm;
import com.mind.util.WebUtil;
import com.mind.xml.POPdf;
import com.mind.xml.WOPdf;

public class POWOAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = new ActionForward();
		POWOForm powoform = (POWOForm) form;
		ArrayList list = new ArrayList();
		String temppowoid = "";
		String function_type = "";
		String order_type = "";
		String powoid = "";
		String sortqueryclause = "";
		String realPath = WebUtil.getRealPath(request, "/images");

		String fontPath = realPath + "/ARIALN.TTF";
		String fileName = "PoFinal";
		String imageBulletPath = realPath + "/Bullet.gif";

		/* Page Security Start */
		HttpSession session = request.getSession(true);

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		String userid = (String) session.getAttribute("userid");
		if (!Authenticationdao.getPageSecurity(userid, "Manage Partner",
				"Manage PO/WO", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Page Security End */

		if (request.getParameter("partnerid") != null)
			request.setAttribute("partner_id",
					request.getParameter("partnerid"));
		else
			request.setAttribute("partner_id", "0");

		// for invoice manager back button
		if (request.getParameter("inv_id") != null)
			powoform.setInv_id(request.getParameter("inv_id"));
		if (request.getParameter("inv_type") != null)
			powoform.setInv_type(request.getParameter("inv_type"));
		if (request.getParameter("inv_rdofilter") != null)
			powoform.setInv_rdofilter(request.getParameter("inv_rdofilter"));
		if (request.getParameter("invoice_Flag") != null)
			powoform.setInvoice_Flag(request.getParameter("invoice_Flag"));
		if (request.getParameter("invoiceno") != null)
			powoform.setInvoiceNo(request.getParameter("invoiceno"));
		if (request.getParameter("partner_name") != null)
			powoform.setPartner_name(request.getParameter("partner_name"));
		if (request.getParameter("powo_number") != null)
			powoform.setPowo_number(request.getParameter("powo_number"));
		if (request.getParameter("from_date") != null)
			powoform.setFrom_date(request.getParameter("from_date"));
		if (request.getParameter("to_date") != null)
			powoform.setTo_date(request.getParameter("to_date"));

		// for netmedx dashboard back button
		if (request.getParameter("nettype") != null)
			powoform.setNettype("" + request.getParameter("nettype"));
		if (request.getParameter("viewjobtype") != null) {
			powoform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			powoform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			powoform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			powoform.setOwnerId("%");
		}

		// data for view PO/WO
		if (request.getParameter("function_type") != null)
			function_type = request.getParameter("function_type");
		if (request.getParameter("order_type") != null)
			order_type = request.getParameter("order_type");
		if (request.getParameter("powoid") != null)
			powoid = request.getParameter("powoid");
		String imagepath = "";
		if (!powoid.equals("")) {
			boolean checkStatus = POWOdao.checkInternationalPO(powoid,
					getDataSource(request, "ilexnewDB"));
			if (checkStatus == true) {
				imagepath = realPath + "/clogo_international1.jpg";
			} else {
				imagepath = realPath + "/clogo.jpg";
			}
		}
		String imagepath1 = realPath + "/checkbox.gif";
		String WONwConfigImg = realPath + "/witechfooter.jpg";
		String WONwConfigImgheader = realPath + "/witechheader.jpg";

		String icChrysler = realPath + "/chrysler.jpg";
		String icSnapOn = realPath + "/snap.jpg";
		String icCompuCom = realPath + "/compu.jpg";

		// set job id and name in form bean
		if (request.getParameter("jobid") != null) {
			powoform.setJobid(request.getParameter("jobid"));
		} else {
			powoform.setJobid(powoform.getJobid());
		}

		if (request.getAttribute("jobid") != null) {
			powoform.setJobid(request.getParameter("jobid"));
		} else {
			powoform.setJobid(powoform.getJobid());
		}

		powoform.setJobname(Activitydao.getJobname(
				getDataSource(request, "ilexnewDB"), powoform.getJobid()));

		// set appendix id and name in form bean
		powoform.setAppendixid(IMdao.getAppendixId(powoform.getJobid(),
				getDataSource(request, "ilexnewDB")));
		powoform.setAppendixname(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				IMdao.getAppendixId(powoform.getJobid(),
						getDataSource(request, "ilexnewDB"))));

		// get PO list
		/*
		 * if ( request.getAttribute( "querystring" ) != null ) {
		 * sortqueryclause = ( String ) request.getAttribute( "querystring" ); }
		 */

		sortqueryclause = getSortQueryClause(sortqueryclause, session);

		list = POWOdao.getPOWOdetaillist(powoform.getJobid(), sortqueryclause,
				getDataSource(request, "ilexnewDB"));

		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				temppowoid = ((POWOBean) list.get(i)).getPowoid();
				((POWOBean) list.get(i)).setResourcelist(POWOdao
						.getPOWOResourcedetaillist(temppowoid,
								getDataSource(request, "ilexnewDB")));
			}
		}

		if (function_type.equals("view")) {

			byte[] buffer1 = null;
			String fileId = "";
			String status = POWOdao.checkPOStatus(powoid,
					getDataSource(request, "ilexnewDB"));
			fileId = DocumentUtility.getFileId(powoid, order_type);
			if (status != null && !status.equals("0") && fileId != null
					&& !fileId.equals("") && !fileId.equals("0"))
				buffer1 = DocumentMasterDao.openFile(fileId);
			if (order_type.equals("po")) {
				ServletOutputStream outStream = response.getOutputStream();
				POPdf po = new POPdf();
				if (buffer1 == null || buffer1.length <= 0) {

					buffer1 = po.viewPO(powoid, fileName, imagepath, fontPath,
							userid, getDataSource(request, "ilexnewDB"),
							imageBulletPath);
				}
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition",
						"attachment;filename=PO.pdf;size=" + buffer1.length
								+ "");
				outStream.write(buffer1);
				outStream.flush();
				return null;
			}
			if (order_type.equals("wo")) {

				/*********************************
				 * 
				 * 
				 * 
				 * 
				 * 
				 * 
				 * 
				 * 
				 * 
				 * 
				 * ****/
				String partnerId = request.getParameter("partnerid") == null ? "0"
						: request.getParameter("partnerid");

				if (partnerId.equals("0") || partnerId.equals("")) {
					partnerId = Pvsdao.getPartnerForPO(powoid,
							getDataSource(request, "ilexnewDB"));
				}

				PartnerDetailBean partnerDetailBean = new PartnerDetailBean();
				partnerDetailBean.setPartnerId(partnerId);

				Pvsdao.getSearchPartnerDetail(partnerDetailBean, request
						.getRequestURL().toString(),
						getDataSource(request, "ilexnewDB"));

				ServletOutputStream outStream = response.getOutputStream();
				WOPdf wo = new WOPdf();
				if (partnerDetailBean.getImgPath() != null
						&& partnerDetailBean.getImgPath().length() > 0) {
					if (buffer1 == null || buffer1.length <= 0) {
						buffer1 = wo.viewWOWithBadge(powoid, imagepath,
								imagepath1, partnerDetailBean.getImgPath()
										.replace("||||", ""), fontPath, userid,
								getDataSource(request, "ilexnewDB"),
								WONwConfigImg, WONwConfigImgheader, icSnapOn,
								icChrysler, icCompuCom);
					}
				} else {
					if (buffer1 == null || buffer1.length <= 0) {
						buffer1 = wo.viewwo(powoid, imagepath, imagepath1,
								fontPath, userid,
								getDataSource(request, "ilexnewDB"),
								WONwConfigImg, WONwConfigImgheader, icSnapOn,
								icChrysler, icCompuCom);
					}
				}
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition",
						"attachment;filename=WO.pdf;size=" + buffer1.length
								+ "");
				// response.setHeader( "Content-Disposition",
				// "inline;filename=WO.pdf;");
				outStream.write(buffer1);
				outStream.flush();
				// outStream.close();
				return null;
			}
		}

		if (function_type.equals("Send POWO")) {
			request.setAttribute("Job_Id", powoform.getJobid());
			// System.out.println(" Inside POWO Action job id ,type ,powoid : "+powoform.getJobid()+" & "+function_type+" - "+powoid);
			request.setAttribute("order_type", order_type);
			request.setAttribute("from_type", "POWODashboard");
			request.setAttribute("powoid", powoid);
			PODao dao = new PODao();
			List<List<String>> al = dao.featchDocListForMPO(new Integer(powoid)
					.intValue());
			request.setAttribute("docDetailPageList", al);
			request.setAttribute("Type", "" + function_type);
			forward = mapping.findForward("sendPOWO");
			return (forward);
		}

		request.setAttribute("powolist", list);
		request.setAttribute("listsize", list.size() + "");

		request.setAttribute("jobid", powoform.getJobid());

		if (request.getAttribute("updateflag") != null) {
			request.setAttribute("updateflag",
					request.getAttribute("updateflag").toString());
		}

		if (request.getAttribute("powono") != null) {
			request.setAttribute("powono", request.getAttribute("powono")
					.toString());
		}

		return (mapping.findForward("success1"));
	}

	public String getSortQueryClause(String queryclause, HttpSession session) {
		if (session.getAttribute("powo_sort") != null)
			queryclause = (String) session.getAttribute("powo_sort");
		return queryclause;
	}

}