package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PRM.Appendixdao;
import com.mind.formbean.PRM.AppendixJobDetailForm;


public class AppendixJobDetailAction extends com.mind.common.IlexDispatchAction {
	
	public ActionForward view(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AppendixJobDetailForm appendixJobDetailForm = (AppendixJobDetailForm) form;
		ArrayList list = new ArrayList();
		String appendixid="";
		String sortqueryclause = "";
		
		appendixid=request.getParameter("appendixid");
		appendixJobDetailForm.setAppendixid(appendixid);
		request.setAttribute("appendixid",appendixid);
				
		if ( request.getAttribute( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getAttribute( "querystring" );
		}
		
		//appendixJobDetailForm.setType("PRM_Job");
		list=Appendixdao.getJobdetaillist(appendixid,getDataSource(request,"ilexnewDB"),sortqueryclause,"%","%","clear");
		request.setAttribute("joblist",list);
		forward = mapping.findForward("success");
		return (forward);
	}

}
