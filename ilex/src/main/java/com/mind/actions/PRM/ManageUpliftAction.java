package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.prm.ManageUpliftBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.dao.PRM.ManageUpliftdao;
import com.mind.formbean.PRM.ManageUpliftForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ManageUpliftAction extends com.mind.common.IlexDispatchAction {

	public ActionForward addendumUplift(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ManageUpliftForm manageupliftForm = (ManageUpliftForm) form;
		ManageUpliftBean manageupliftBean = new ManageUpliftBean();
		Map<String, Object> map;
		int retval = 0;
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (!Authenticationdao.getPageSecurity(userid, "Manage Job",
				"Manage Uplift", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Page Security End */

		if (request.getParameter("Appendix_Id") != null)
			manageupliftForm.setAppendixId(request.getParameter("Appendix_Id"));

		if (request.getParameter("Job_Id") != null)
			manageupliftForm.setJobId(request.getParameter("Job_Id"));

		if (request.getParameter("ownerId") != null) {
			manageupliftForm.setOwnerId("" + request.getParameter("ownerId"));
		}
		if (request.getParameter("viewjobtype") != null) {
			manageupliftForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		}

		if (manageupliftForm.getSave() != null) {
			retval = ManageUpliftdao.updateUpliftFactor(
					manageupliftForm.getJobId(), "Job",
					manageupliftForm.getUnionUplift(),
					manageupliftForm.getExpedite24Uplift(),
					manageupliftForm.getExpedite48Uplift(),
					manageupliftForm.getAfterHoursUplift(),
					manageupliftForm.getWhUplift(),
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("retval", retval + "");
		}
		BeanUtils.copyProperties(manageupliftBean, manageupliftForm);
		ManageUpliftdao.setAddendumUpliftFactor(manageupliftBean,
				manageupliftForm.getJobId(),
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(manageupliftForm, manageupliftBean);
		manageupliftForm.setJobName(Activitydao.getJobname(
				getDataSource(request, "ilexnewDB"),
				manageupliftForm.getJobId()));
		manageupliftForm.setAppendixName(Activitydao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				manageupliftForm.getJobId()));
		if (manageupliftForm.getJobId() != null
				&& !manageupliftForm.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(manageupliftForm.getJobId(),
					userid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					manageupliftForm.getJobId(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			if (jobStatusAndType[0] != null) {
				request.setAttribute("addendumStatus", jobStatusAndType[0]);
			}
		}
		return (mapping.findForward("addendumUplift"));
	}

	public ActionForward activityUplift(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ManageUpliftForm manageupliftForm = (ManageUpliftForm) form;
		ManageUpliftBean manageUpliftBean = new ManageUpliftBean();
		int retval = 0;
		/* Page Security Start */
		HttpSession session = request.getSession(true);

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (!Authenticationdao.getPageSecurity(userid, "Manage Job",
				"Manage Uplift", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Page Security End */

		if (request.getParameter("Appendix_Id") != null)
			manageupliftForm.setAppendixId(request.getParameter("Appendix_Id"));

		if (request.getParameter("Job_Id") != null)
			manageupliftForm.setJobId(request.getParameter("Job_Id"));

		if (request.getParameter("ownerId") != null) {
			manageupliftForm.setOwnerId("" + request.getParameter("ownerId"));
		}
		if (request.getParameter("viewjobtype") != null) {
			manageupliftForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		}

		if (manageupliftForm.getSave() != null) {
			int checklength = 0;
			int length = 0;
			StringBuffer t0 = new StringBuffer("");
			StringBuffer t1 = new StringBuffer("");
			StringBuffer t2 = new StringBuffer("");
			StringBuffer t3 = new StringBuffer("");
			StringBuffer t4 = new StringBuffer("");
			StringBuffer t5 = new StringBuffer("");

			if (manageupliftForm.getActivity_id() != null) {
				length = manageupliftForm.getActivity_id().length;
			}
			if (manageupliftForm.getActivity_tempid() != null) {
				checklength = manageupliftForm.getActivity_tempid().length;
			}

			int[] index = new int[length];
			String[] act_value = new String[length];
			int k = 0;
			if (length > 0) {
				for (int i = 0; i < length; i++) {
					for (int j = 0; j < checklength; j++) {
						if (manageupliftForm.getActivity_id(i).equals(
								manageupliftForm.getActivity_tempid(j))) {
							index[k] = j;
							k++;
						}
					}
				}
			}
			StringBuffer t = new StringBuffer("");

			if (checklength > 0) {
				for (int i = 0; i < length; i++) {
					t0 = t0.append(
							manageupliftForm.getActivity_tempid(index[i]))
							.append(",");
				}
			}

			if (manageupliftForm.getActivity_id() != null
					&& manageupliftForm.getAtUnionUplift() != null) {
				for (int i = 0; i < manageupliftForm.getAtUnionUplift().length; i++) {
					for (int j = 0; j < manageupliftForm.getActivity_id().length; j++) {
						if (manageupliftForm.getAtUnionUplift(i).equals(
								manageupliftForm.getActivity_id(j)))
							t1 = t1.append(manageupliftForm.getAtUnionUplift(i))
									.append(",");
					}
				}
			}

			if (manageupliftForm.getActivity_id() != null
					&& manageupliftForm.getAtExpedite24Uplift() != null) {
				for (int i = 0; i < manageupliftForm.getAtExpedite24Uplift().length; i++) {
					for (int j = 0; j < manageupliftForm.getActivity_id().length; j++) {
						if (manageupliftForm.getAtExpedite24Uplift(i).equals(
								manageupliftForm.getActivity_id(j)))
							t2 = t2.append(
									manageupliftForm.getAtExpedite24Uplift(i))
									.append(",");
					}
				}
			}

			if (manageupliftForm.getActivity_id() != null
					&& manageupliftForm.getAtExpedite48Uplift() != null) {
				for (int i = 0; i < manageupliftForm.getAtExpedite48Uplift().length; i++) {
					for (int j = 0; j < manageupliftForm.getActivity_id().length; j++) {
						if (manageupliftForm.getAtExpedite48Uplift(i).equals(
								manageupliftForm.getActivity_id(j)))
							t3 = t3.append(
									manageupliftForm.getAtExpedite48Uplift(i))
									.append(",");
					}
				}
			}

			if (manageupliftForm.getActivity_id() != null
					&& manageupliftForm.getAtAfterHoursUplift() != null) {
				for (int i = 0; i < manageupliftForm.getAtAfterHoursUplift().length; i++) {
					for (int j = 0; j < manageupliftForm.getActivity_id().length; j++) {
						if (manageupliftForm.getAtAfterHoursUplift(i).equals(
								manageupliftForm.getActivity_id(j)))
							t4 = t4.append(
									manageupliftForm.getAtAfterHoursUplift(i))
									.append(",");
					}
				}
			}

			if (manageupliftForm.getActivity_id() != null
					&& manageupliftForm.getAtWhUplift() != null) {
				for (int i = 0; i < manageupliftForm.getAtWhUplift().length; i++) {
					for (int j = 0; j < manageupliftForm.getActivity_id().length; j++) {
						if (manageupliftForm.getAtWhUplift(i).equals(
								manageupliftForm.getActivity_id(j)))
							t5 = t5.append(manageupliftForm.getAtWhUplift(i))
									.append(",");
					}
				}
			}

			retval = ManageUpliftdao.applyUpliftFactor(t0.toString(),
					t1.toString(), t2.toString(), t3.toString(), t4.toString(),
					t5.toString(), getDataSource(request, "ilexnewDB"));

			request.setAttribute("retval", retval + "");
		}

		BeanUtils.copyProperties(manageUpliftBean, manageupliftForm);
		ManageUpliftdao.getAppliedUplifts(manageUpliftBean,
				manageupliftForm.getJobId(),
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(manageupliftForm, manageUpliftBean);
		ManageUpliftdao.setUpliftFactor(manageUpliftBean,
				manageupliftForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(manageupliftForm, manageUpliftBean);
		manageupliftForm.setJobName(Activitydao.getJobname(
				getDataSource(request, "ilexnewDB"),
				manageupliftForm.getJobId()));
		manageupliftForm.setAppendixName(Activitydao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				manageupliftForm.getJobId()));
		ArrayList actlist = JobDashboarddao.getActivitytabular(
				manageupliftForm.getJobId(),
				getDataSource(request, "ilexnewDB"), "");
		request.setAttribute("actlist", actlist);
		request.setAttribute("actlistSize", actlist.size() + "");
		if (manageupliftForm.getJobId() != null
				&& !manageupliftForm.getJobId().trim().equalsIgnoreCase("")) {
			Map<String, Object> map = DatabaseUtilityDao.setMenu(
					manageupliftForm.getJobId(), userid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		return (mapping.findForward("activityUplift"));
	}
}
