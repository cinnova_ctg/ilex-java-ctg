package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.pm.Appendix;
import com.mind.bean.prm.NetMedXDashboardBean;
import com.mind.common.dao.AddComment;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.dao.PRM.NetMedXDashboarddao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.formbean.PRM.NetMedXDashboardForm;
import com.mind.util.WebUtil;

public class NetMedXDashboardAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		NetMedXDashboardForm netmedxForm = (NetMedXDashboardForm) form;
		ArrayList list = new ArrayList();
		ArrayList compDispatchList = new ArrayList();
		String sortqueryclause = "";
		String[] pocdetails = new String[12];
		String[] custcnspocdetails = new String[5];
		String appendixcurrentstatus = "";
		Map<String, Object> sessionMap;

		/*** Code added for the View Selecotr ***/
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		String userId = null;
		String tempCat = "";
		ArrayList initialStateCategory = new ArrayList();
		ArrayList jobOwnerList = new ArrayList();

		String[] cnsPrjManangerDetail = new String[5];
		String[] teamLeadDetail = new String[5];

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");
		if (netmedxForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(loginuserid,
					"Manage Project", "View Appendix Dashboard",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}

		/* Page Security End */

		String apiAccessValue = "";
		String fromDate = "";
		String toDate = "";

		if (request.getParameter("fromDate") != null) {
			fromDate = request.getParameter("fromDate");
		}

		if (request.getParameter("toDate") != null) {
			toDate = request.getParameter("toDate");
		}

		if (request.getParameter("apiAccess") != null) {
			apiAccessValue = request.getParameter("apiAccess");
		}

		if (request.getParameter("appendixid") != null) {
			netmedxForm.setAppendixid(request.getParameter("appendixid"));
		}

		if (request.getAttribute("appendixid") != null)
			netmedxForm.setAppendixid(request.getAttribute("appendixid")
					.toString());

		netmedxForm.setAppendixname(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				netmedxForm.getAppendixid()));

		// set msa id and name in form bean
		netmedxForm.setMsaid(IMdao.getMSAId(netmedxForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")));
		netmedxForm.setMsaname(Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"), netmedxForm.getMsaid()));

		// set job filter type start
		if (request.getParameter("viewjobtype") != null) {
			netmedxForm
					.setViewjobtype("" + request.getParameter("viewjobtype"));
		}

		if (netmedxForm.getViewjobtype() != null) {
		} else {
			netmedxForm.setViewjobtype("All");
		}

		if (netmedxForm.getViewjobtype().equals("")) {
			netmedxForm.setViewjobtype("All");
		}

		// set job filter type end
		if (request.getParameter("nettype") != null) {
			netmedxForm.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getAttribute("nettype") != null) {
			netmedxForm.setNettype("" + request.getAttribute("nettype"));
		}

		if (request.getParameter("from_type") != null) {
			netmedxForm.setFrom_type("" + request.getParameter("from_type"));
		}

		if (netmedxForm.getAppendixid().equals("-1")
				|| netmedxForm.getAppendixid().equals("")) {
			netmedxForm.setAppendixid("-1");
			netmedxForm.setAppendixname("All");
			netmedxForm.setMsaname("All");
		}
		// NetMedX View Selector:Start
		if (netmedxForm.getJobOwnerOtherCheck() != null
				&& !netmedxForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					netmedxForm.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);

		}

		if (netmedxForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < netmedxForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ netmedxForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ netmedxForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (netmedxForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < netmedxForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ netmedxForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ netmedxForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {
			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);

			session.setAttribute("timeFrame",
					netmedxForm.getJobMonthWeekCheck());

			session.setAttribute("jobOwnerOtherCheck",
					netmedxForm.getJobOwnerOtherCheck());

		} else {

			session.setAttribute("timeFrame", "month");
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				netmedxForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				netmedxForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				netmedxForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				netmedxForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				netmedxForm.getJobOwnerOtherCheck(), "prj_job_new",
				netmedxForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		// NetMedX View Selector:End

		// Don't set in case of ticket search or dispatch view
		if ((!netmedxForm.getAppendixid().equals("-1"))
				|| (!netmedxForm.getViewjobtype().equals("jobSearch"))) {
			appendixcurrentstatus = com.mind.dao.PRM.Appendixdao
					.getCurrentstatus(netmedxForm.getAppendixid(),
							getDataSource(request, "ilexnewDB"));

			request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
			netmedxForm.setStatus(appendixcurrentstatus);

			Appendix appendix = new Appendix();
			appendix = com.mind.dao.PRM.Appendixdao.getAppendix("%",
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			if (netmedxForm.getAppendixid() != null) {
				netmedxForm.setStatusdesc(appendix.getStatus());
				netmedxForm.setAppendix_type(appendix.getAppendixtype());
				netmedxForm.setPending_tickets(appendix.getPendingWebTicket());
				netmedxForm.setToBeSchedule(appendix.getToBeSchedule());
				netmedxForm.setScheduled(appendix.getSchedule());
				netmedxForm.setOverdue(appendix.getOverdue());
				request.setAttribute("testAppendix", appendix.getTestAppendix());
			}
			// get Contact Information
			pocdetails = com.mind.dao.PRM.Appendixdao.getAppendixpocdetails(
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			netmedxForm.setAppendixcnspoc(pocdetails[0]);
			netmedxForm.setAppendixcnspocphone(pocdetails[1]);
			netmedxForm.setAppendixbusinesspoc(pocdetails[2]);
			netmedxForm.setAppendixbusinesspocphone(pocdetails[3]);
			netmedxForm.setAppendixcontractpoc(pocdetails[4]);
			netmedxForm.setAppendixcontractpocphone(pocdetails[5]);
			netmedxForm.setAppendixbillingpoc(pocdetails[6]);
			netmedxForm.setAppendixbillingpocphone(pocdetails[7]);

			custcnspocdetails = com.mind.dao.PRM.Appendixdao
					.getAppendixcustcnspocdetails(netmedxForm.getAppendixid(),
							getDataSource(request, "ilexnewDB"));
			netmedxForm.setCnsprojectmgr(custcnspocdetails[0]);
			netmedxForm.setCnsprojectmgrphone(custcnspocdetails[1]);
			netmedxForm.setCustprojectmgr(custcnspocdetails[2]);
			netmedxForm.setCustprojectmgrphone(custcnspocdetails[3]);
			netmedxForm.setCnsprojectmgrId(custcnspocdetails[4]);

			// added for project Manager and team lead

			String cnsPrjManagerId = AddContactdao.getCnsPrjManagerId(
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			String teamLeadId = AddContactdao.getTeamLeadId(
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			if (cnsPrjManagerId != null) {
				cnsPrjManangerDetail = AddContactdao.getPocDetails(
						cnsPrjManagerId, getDataSource(request, "ilexnewDB"));
				String cnsPrjManagerName = AddContactdao.getPocname(
						cnsPrjManagerId, getDataSource(request, "ilexnewDB"));
				netmedxForm.setCnsPrjManagerId(cnsPrjManagerId);
				netmedxForm.setCnsPrjManagerName(cnsPrjManagerName);
				netmedxForm.setCnsPrjManagerPhone(cnsPrjManangerDetail[0]);

			}
			if (teamLeadId != null) {
				teamLeadDetail = AddContactdao.getPocDetails(teamLeadId,
						getDataSource(request, "ilexnewDB"));
				String teamLeadName = AddContactdao.getPocname(teamLeadId,
						getDataSource(request, "ilexnewDB"));
				netmedxForm.setTeamLeadId(teamLeadId);
				netmedxForm.setTeamLeadName(teamLeadName);
				netmedxForm.setTeamLeadPhone(teamLeadDetail[0]);

			}

		}

		// get ticket/job list
		if (request.getAttribute("querystring") != null) {
			sortqueryclause = (String) request.getAttribute("querystring");
		}

		// Start :Added By Amit For Holdong Sort
		if (session != null) {
			if (session.getAttribute("HoldingSortNetMedx") != null) {
				sortqueryclause = (String) session
						.getAttribute("HoldingSortNetMedx");
			}
		}

		if (request.getParameter("ownerId") != null) {
			netmedxForm.setOwnerId(request.getParameter("ownerId"));
		} else {
			netmedxForm.setOwnerId("All");
		}

		/* Set filters in session */
		if (request.getParameter("changeFilter") != null) {
			if (request.getParameter("changeFilter").equals("Y")) {
				session.setAttribute("ownerId", netmedxForm.getOwnerId());
				session.setAttribute("viewjobtype",
						netmedxForm.getViewjobtype());
			}
		}

		/* Get filters from session */
		if (session.getAttribute("ownerId") != null)
			userId = (String) session.getAttribute("ownerId");

		initialStateCategory = NetMedXDashboarddao
				.getStateCategoryList(getDataSource(request, "ilexnewDB"));

		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempCat = ((NetMedXDashboardBean) initialStateCategory.get(i))
						.getSiteCountryId();
				((NetMedXDashboardBean) initialStateCategory.get(i))
						.setTempList(SiteManagedao
								.getCategoryWiseStateList(tempCat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}
		request.setAttribute("initialStateCategory", initialStateCategory);

		DynamicComboCM dcJobOwnerList = new DynamicComboCM();
		dcJobOwnerList.setFirstlevelcatglist(com.mind.dao.PRM.Appendixdao
				.getjobOwnerList(netmedxForm.getAppendixid(),
						getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcJobOwnerList", dcJobOwnerList);

		if (netmedxForm.getViewjobtype().equals("jobSearch")) {
			if (request.getParameter("fromLink") != null) {
				list = null;
				request.setAttribute("ticketjoblist", list);
				request.setAttribute("listsize", 0 + "");

				/* Set all search parameters null in session */
				session.setAttribute("job_search_name", null);
				session.setAttribute("job_search_site_number", null);
				session.setAttribute("job_search_city", null);
				session.setAttribute("job_search_state", null);
				session.setAttribute("job_search_owner", null);
				session.setAttribute("job_search_date_from", null);
				session.setAttribute("job_search_date_to", null);
				session.setAttribute("dateOnSite", null);
				session.setAttribute("dateOffSite", null);
				session.setAttribute("dateComplete", null);
				session.setAttribute("dateClosed", null);
				session.setAttribute("job_search", null);
			}
			ArrayList<String> userInfo = new ArrayList<>();
			if (netmedxForm.getAppendixid() != null) {

				userInfo = NetMedXDashboarddao.checkPassword(
						netmedxForm.getMsaid(), netmedxForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"));

				netmedxForm.setaPIAccessPassword(userInfo.get(0));
				netmedxForm.setApiUserName(userInfo.get(1));

			}

			if (netmedxForm.getApiAccessChkBox() != null
					&& netmedxForm.getApiAccessChkBox().equals("false")) {

				NetMedXDashboarddao.deletePassword(netmedxForm.getMsaid(),
						netmedxForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"));
				apiAccessValue = "apiAccess";
				netmedxForm.setaPIAccessPassword("");
				netmedxForm.setApiUserName("");
				netmedxForm.setApiCheckBox("");

			}

			if (netmedxForm.getApiAccessBtn() != null
					&& netmedxForm.getApiAccessBtn().equals("Get Access")
					&& netmedxForm.getApiAccessChkBox() != null
					&& netmedxForm.getApiAccessChkBox().equals("true")) {

				request.setAttribute("searchPage", "TicketSearch");
				session.setAttribute("job_search_site_number",
						netmedxForm.getJob_search_site_number());
				session.setAttribute("job_search_owner",
						netmedxForm.getJob_search_owner());
				netmedxForm
						.setApiAccessChkBox(netmedxForm.getApiAccessChkBox());
				netmedxForm.setApiAccessBtn("Get Access");
				request.setAttribute("apiAccess", "apiAccess");

				String password = new String(generatePswd());

				String userName = "";
				String msaName = netmedxForm.getMsaname().toLowerCase().trim();
				if (msaName.contains(" ")) {
					String[] apiUser = msaName.split(" ");
					userName = apiUser[0].substring(0, 1);
					for (int i = 1; i <= apiUser.length - 1; i++) {
						userName = userName + apiUser[i];
					}
				} else {
					userName = msaName;
				}

				NetMedXDashboarddao.setAPIAccessValue(netmedxForm.getMsaid(),
						userName, password, netmedxForm.getAppendixid(), null,
						getDataSource(request, "ilexnewDB"));
				netmedxForm.setaPIAccessPassword(password);
				netmedxForm.setApiUserName(userName);
				netmedxForm.setApiCheckBox("msg");
				apiAccessValue = "apiAccess";
			}

			else if (netmedxForm.getJob_search() != null
					&& !netmedxForm.getJob_search().equals("")) {

				request.setAttribute("searchPage", "TicketSearch");
				session.setAttribute("job_search_name",
						netmedxForm.getJob_search_name());
				session.setAttribute("job_search_site_number",
						netmedxForm.getJob_search_site_number());
				session.setAttribute("job_search_city",
						netmedxForm.getJob_search_city());
				session.setAttribute("job_search_state",
						netmedxForm.getJob_search_state());
				session.setAttribute("job_search_owner",
						netmedxForm.getJob_search_owner());
				session.setAttribute("job_search_date_from",
						netmedxForm.getJob_search_date_from());
				session.setAttribute("job_search_date_to",
						netmedxForm.getJob_search_date_to());
				session.setAttribute("dateOnSite", netmedxForm.getDateOnSite());
				session.setAttribute("dateOffSite",
						netmedxForm.getDateOffSite());
				session.setAttribute("dateComplete",
						netmedxForm.getDateComplete());
				session.setAttribute("dateClosed", netmedxForm.getDateClosed());
				session.setAttribute("job_search", netmedxForm.getJob_search());
			}

			else {

				if (session.getAttribute("job_search_name") != null)
					netmedxForm.setJob_search_name(session.getAttribute(
							"job_search_name").toString());

				if (session.getAttribute("job_search_site_number") != null)
					netmedxForm.setJob_search_site_number(session.getAttribute(
							"job_search_site_number").toString());

				if (session.getAttribute("job_search_city") != null)
					netmedxForm.setJob_search_city(session.getAttribute(
							"job_search_city").toString());

				if (session.getAttribute("job_search_state") != null)
					netmedxForm.setJob_search_state(session.getAttribute(
							"job_search_state").toString());

				if (session.getAttribute("job_search_owner") != null)
					netmedxForm.setJob_search_owner(session.getAttribute(
							"job_search_owner").toString());

				if (session.getAttribute("job_search_date_from") != null)
					netmedxForm.setJob_search_date_from(session.getAttribute(
							"job_search_date_from").toString());

				if (session.getAttribute("job_search_date_to") != null)
					netmedxForm.setJob_search_date_to(session.getAttribute(
							"job_search_date_to").toString());

				if (session.getAttribute("dateOnSite") != null)
					netmedxForm.setDateOnSite(session
							.getAttribute("dateOnSite").toString());

				if (session.getAttribute("dateOffSite") != null)
					netmedxForm.setDateOffSite(session.getAttribute(
							"dateOffSite").toString());

				if (session.getAttribute("dateComplete") != null)
					netmedxForm.setDateComplete(session.getAttribute(
							"dateComplete").toString());

				if (session.getAttribute("dateClosed") != null)
					netmedxForm.setDateClosed(session
							.getAttribute("dateClosed").toString());

				if (session.getAttribute("job_search") != null)
					netmedxForm.setJob_search(session
							.getAttribute("job_search").toString());
			}
			if (request.getParameter("fromLink") != null) {
				list = null;
				request.setAttribute("ticketjoblist", list);
				request.setAttribute("listsize", "0");
			} else {
				sessionMap = WebUtil.copySessionToAttributeMap(session);
				list = NetMedXDashboarddao.getJobSearchdetaillist(
						netmedxForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"), sortqueryclause,
						sessionMap);
				request.setAttribute("ticketjoblist", list);
				request.setAttribute("listsize", list.size() + "");
			}

		} else {
			list = NetMedXDashboarddao.getNetMedXJobtabularNew(
					netmedxForm.getAppendixid(), sortqueryclause,
					jobSelectedStatus, userId,
					getDataSource(request, "ilexnewDB"), jobSelectedOwner,
					netmedxForm.getJobMonthWeekCheck(), fromDate, toDate);
			request.setAttribute("ticketjoblist", list);
			request.setAttribute("listsize", list.size() + "");
		}

		if (netmedxForm.getAppendixid().equals("-1"))
			netmedxForm.setCustref("");
		else
			netmedxForm.setCustref(AddComment.getCustomerReference(
					netmedxForm.getAppendixid(), "Appendix",
					getDataSource(request, "ilexnewDB")));

		// Don't set in case of ticket search
		if (!netmedxForm.getViewjobtype().equals("jobSearch")) {
			// get Complete Dispatched Ticket Summary
			compDispatchList = NetMedXDashboarddao.getCompleteDispatchSummary(
					netmedxForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("compdispatchlist", compDispatchList);
		}
		request.setAttribute("appendixid", netmedxForm.getAppendixid());

		request.setAttribute("nettype", netmedxForm.getNettype());
		request.setAttribute("viewjobtype", netmedxForm.getViewjobtype());
		request.setAttribute("ownerId", netmedxForm.getOwnerId());
		request.setAttribute("loginUserId", loginuserid);

		boolean editContactList = NetMedXDashboarddao.editContactList(
				loginuserid, getDataSource(request, "ilexnewDB"));
		netmedxForm.setEditContactList(editContactList);
		if (apiAccessValue.equals("apiAccess")) {
			return (mapping.findForward("apiAccessSuccess"));
		}
		return (mapping.findForward("success"));
	}

	private static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
	private static final String NUM = "0123456789";
	private static final String SPL_CHARS = "!@#$%&*";

	public static char[] generatePswd() {

		int minLen = 8;
		int maxLen = 8;
		int noOfCAPSAlpha = 3;
		int noOfDigits = 3;
		int noOfSplChars = 2;
		if (minLen > maxLen)
			throw new IllegalArgumentException("Min. Length > Max. Length!");
		if ((noOfCAPSAlpha + noOfDigits + noOfSplChars) > minLen)
			throw new IllegalArgumentException(
					"Min. Length should be atleast sum of (CAPS, DIGITS, SPL CHARS) Length!");
		Random rnd = new Random();
		int len = rnd.nextInt(maxLen - minLen + 1) + minLen;
		char[] pswd = new char[len];
		int index = 0;
		for (int i = 0; i < noOfCAPSAlpha; i++) {
			index = getNextIndex(rnd, len, pswd);
			pswd[index] = ALPHA_CAPS.charAt(rnd.nextInt(ALPHA_CAPS.length()));
		}
		for (int i = 0; i < noOfDigits; i++) {
			index = getNextIndex(rnd, len, pswd);
			pswd[index] = NUM.charAt(rnd.nextInt(NUM.length()));
		}
		for (int i = 0; i < noOfSplChars; i++) {
			index = getNextIndex(rnd, len, pswd);
			pswd[index] = SPL_CHARS.charAt(rnd.nextInt(SPL_CHARS.length()));
		}
		for (int i = 0; i < len; i++) {
			if (pswd[i] == 0) {
				pswd[i] = ALPHA.charAt(rnd.nextInt(ALPHA.length()));
			}
		}
		return pswd;
	}

	private static int getNextIndex(Random rnd, int len, char[] pswd) {
		int index = rnd.nextInt(len);
		while (pswd[index = rnd.nextInt(len)] != 0)
			;
		return index;
	}

}