package com.mind.actions.PRM;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.prm.SiteManagementBean;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.SiteManagementdao;
import com.mind.formbean.PRM.SiteManagementForm;
import com.mind.util.WebUtil;

public class SiteManagementAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SiteManagementForm siteManagementForm = (SiteManagementForm) form;
		SiteManagementBean siteManageBean = new SiteManagementBean();
		DataSource ds = null;
		ds = getDataSource(request, "ilexnewDB");
		ArrayList siteList = new ArrayList();
		List siteDetails = new ArrayList();
		// add for details check

		String sortqueryclause = "";
		String result = "";
		int count = 0;
		String endCustomer = "";

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		int roleflag = 0;

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire"));
		String userid = (String) session.getAttribute("userid");
		roleflag = SiteManagementdao.getPocRole(userid, ds);
		String username = (String) session.getAttribute("username");

		if (request.getParameter("countRow") != null
				&& !request.getParameter("countRow").equals(""))
			siteManagementForm.setCountRow(request.getParameter("countRow"));
		else
			siteManagementForm.setCountRow("" + 1);

		if (request.getParameter("pageType") != null)
			siteManagementForm.setPageType(request.getParameter("pageType"));

		if (request.getParameter("ownerId") != null) {
			siteManagementForm.setOwnerId(request.getParameter("ownerId"));
		}

		if (request.getParameter("viewjobtype") != null) {
			siteManagementForm.setViewjobtype(request
					.getParameter("viewjobtype"));
		}

		if (request.getParameter("pageType") != null) {
			if (request.getParameter("pageType").equals("mastersites")) {
				siteManagementForm.setAppendixid(request.getParameter(
						"appendixid").toString());
				siteManagementForm.setMsaid(request.getParameter("msaid"));
				siteManagementForm.setMsaname(com.mind.dao.PM.Appendixdao
						.getMsaname(ds, request.getParameter("msaid")));
			} else {
				if (request.getParameter("appendixid") != null
						&& !request.getParameter("appendixid").equals("")) {
					siteManagementForm.setAppendixid(request.getParameter(
							"appendixid").toString());
					siteManagementForm.setMsaid(IMdao.getMSAId(
							siteManagementForm.getAppendixid(),
							getDataSource(request, "ilexnewDB")));
					siteManagementForm.setMsaname(com.mind.dao.PM.Appendixdao
							.getMsaname(ds, IMdao.getMSAId(
									siteManagementForm.getAppendixid(), ds)));
				}
			}

		}

		if (request.getParameter("Psiteflag") != null) {
			String SiteId;
			SiteId = request.getParameter("SiteId");
			if (request.getParameter("add") != null) {
				if (request.getParameter("add").equals("Add")) {

					SiteManagementdao.updateSitePriorityStatus(SiteId, "1", ds);

				} else if (request.getParameter("add").equals("Del")) {

					SiteManagementdao.updateSitePriorityStatus(SiteId, "0", ds);

				}

			}

		}

		if (request.getParameter("initialCheck") != null
				&& request.getParameter("initialCheck").equals("true")) {
			siteManagementForm.setSiteNameForColumn("on");
			siteManagementForm.setSiteNumberForColumn("on");
			siteManagementForm.setSiteAddressForColumn("on");
			siteManagementForm.setSiteCityForColumn("on");
			siteManagementForm.setSiteStateForColumn("on");
			siteManagementForm.setSiteZipForColumn("on");
			siteManagementForm.setSiteCountryForColumn("on");
			siteManagementForm.setSiteSearchStatus("A");
			siteManagementForm.setCboxsite("N");
			endCustomer = SiteManagementdao.getEndCustomer(
					siteManagementForm.getAppendixid(), ds);
			if (endCustomer != null && endCustomer != "")
				siteManagementForm.setSiteSearchEndCustomer(endCustomer);
			BeanUtils.copyProperties(siteManageBean, siteManagementForm);
			siteList = SiteManagementdao.getSiteSearchDetail(siteManageBean,
					sortqueryclause, getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(siteManagementForm, siteManageBean);
			session.setAttribute("siteList", null);

		}
		// check ajax call here for flag status
		if (request.getParameter("cboxsite") != null)
			siteManagementForm.setCboxsite(request.getParameter("cboxsite"));
		if (request.getParameter("siteSearchName") != null)
			siteManagementForm.setSiteSearchName(request
					.getParameter("siteSearchName"));

		if (request.getParameter("siteSearchNumber") != null)
			siteManagementForm.setSiteSearchNumber(request
					.getParameter("siteSearchNumber"));

		if (request.getParameter("siteSearchCity") != null)
			siteManagementForm.setSiteSearchCity(request
					.getParameter("siteSearchCity"));

		if (request.getParameter("siteSearchState") != null)
			siteManagementForm.setSiteSearchState(request
					.getParameter("siteSearchState"));

		if (request.getParameter("siteSearchZip") != null)
			siteManagementForm.setSiteSearchZip(request
					.getParameter("siteSearchZip"));

		if (request.getParameter("siteSearchCountry") != null)
			siteManagementForm.setSiteSearchCountry(request
					.getParameter("siteSearchCountry"));

		if (request.getParameter("siteSearchGroup") != null) {
			siteManagementForm.setSiteSearchGroup(request
					.getParameter("siteSearchGroup"));
		}
		if (request.getParameter("siteSearchEndCustomer") != null) {
			String name = new String("");
			name = request.getParameter("siteSearchEndCustomer");
			name = name.replace('$', '&');
			siteManagementForm.setSiteSearchEndCustomer(name);
		}

		if (request.getParameter("siteSearchStatus") != null)
			siteManagementForm.setSiteSearchStatus(request
					.getParameter("siteSearchStatus"));

		if (request.getParameter("lines_per_page") != null)
			siteManagementForm.setLines_per_page(request
					.getParameter("lines_per_page"));

		if (request.getParameter("current_page_no") != null)
			siteManagementForm.setCurrent_page_no(request
					.getParameter("current_page_no"));

		if (request.getParameter("total_page_size") != null)
			siteManagementForm.setTotal_page_size(request
					.getParameter("total_page_size"));

		if (request.getParameter("org_page_no") != null)
			siteManagementForm.setOrg_page_no(request
					.getParameter("org_page_no"));

		if (request.getParameter("org_lines_per_page") != null)
			siteManagementForm.setOrg_lines_per_page(request
					.getParameter("org_lines_per_page"));

		if (request.getParameter("siteNameForColumn") != null)
			siteManagementForm.setSiteNameForColumn(request
					.getParameter("siteNameForColumn"));

		if (request.getParameter("siteNumberForColumn") != null)
			siteManagementForm.setSiteNumberForColumn(request
					.getParameter("siteNumberForColumn"));

		if (request.getParameter("siteAddressForColumn") != null)
			siteManagementForm.setSiteAddressForColumn(request
					.getParameter("siteAddressForColumn"));

		if (request.getParameter("siteCityForColumn") != null)
			siteManagementForm.setSiteCityForColumn(request
					.getParameter("siteCityForColumn"));

		if (request.getParameter("siteStateForColumn") != null)
			siteManagementForm.setSiteStateForColumn(request
					.getParameter("siteStateForColumn"));

		if (request.getParameter("siteZipForColumn") != null)
			siteManagementForm.setSiteZipForColumn(request
					.getParameter("siteZipForColumn"));

		if (request.getParameter("siteCountryForColumn") != null)
			siteManagementForm.setSiteCountryForColumn(request
					.getParameter("siteCountryForColumn"));

		if (siteManagementForm.getSiteNameForColumn() != null
				&& siteManagementForm.getSiteNameForColumn() != "") {
			count = count + 1;

		}

		if (siteManagementForm.getSiteNumberForColumn() != null
				&& siteManagementForm.getSiteNumberForColumn() != "") {
			count = count + 1;

		}

		if (siteManagementForm.getSiteCityForColumn() != null
				&& siteManagementForm.getSiteCityForColumn() != "") {
			count = count + 1;

		}
		if (siteManagementForm.getSiteCountryForColumn() != null
				&& siteManagementForm.getSiteCountryForColumn() != "") {
			count = count + 1;

		}
		if (siteManagementForm.getSiteAddressForColumn() != null
				&& siteManagementForm.getSiteAddressForColumn() != "") {
			count = count + 1;

		}
		if (siteManagementForm.getSiteStateForColumn() != null
				&& siteManagementForm.getSiteStateForColumn() != "") {
			count = count + 1;

		}
		if (siteManagementForm.getSiteZipForColumn() != null
				&& siteManagementForm.getSiteZipForColumn() != "") {
			count = count + 1;

		}
		siteManagementForm.setCount(count + "");
		request.setAttribute("count", count + "");

		siteManagementForm.setAppendixname(Jobdao.getAppendixname(ds,
				siteManagementForm.getAppendixid()));

		sortqueryclause = getSortQueryClause(sortqueryclause, session);
		BeanUtils.copyProperties(siteManageBean, siteManagementForm);
		if (sortqueryclause != null && sortqueryclause != "") {
			siteList = SiteManagementdao.getSiteSearchDetail(siteManageBean,
					sortqueryclause, getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(siteManagementForm, siteManageBean);
			session.setAttribute("siteList", siteList);
			session.setAttribute("site_sort", null);
		}

		if (siteManagementForm.getPageType().equals("mastersites")) {
			SiteManagementdao.getSiteCount(siteManageBean,
					siteManagementForm.getMsaid(),
					siteManagementForm.getPageType(),
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(siteManagementForm, siteManageBean);
		}
		if (siteManagementForm.getPageType().equals("jobsites")) {
			SiteManagementdao.getSiteCount(siteManageBean,
					siteManagementForm.getAppendixid(),
					siteManagementForm.getPageType(),
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(siteManagementForm, siteManageBean);
		}

		DynamicComboCM dcStatusList = new DynamicComboCM();
		dcStatusList.setFirstlevelcatglist(DynamicComboDao.getstatuslist());
		request.setAttribute("dcStatusList", dcStatusList);

		DynamicComboCM dcEndCustomerList = new DynamicComboCM();
		dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
				.getSiteEndCustomerList(ds, siteManagementForm.getMsaid(),
						siteManagementForm.getMsaname()));
		request.setAttribute("dcEndCustomerList", dcEndCustomerList);

		DynamicComboCM dcGroupList = new DynamicComboCM();
		dcGroupList.setFirstlevelcatglist(DynamicComboDao.getSiteGroupNameList(
				siteManagementForm.getMsaid(), ds));
		request.setAttribute("dcGroupList", dcGroupList);
		BeanUtils.copyProperties(siteManageBean, siteManagementForm);
		if (request.getParameter("search") != null
				&& request.getParameter("search").equals("go")) {

			siteList = SiteManagementdao.getSiteSearchDetail(siteManageBean,
					sortqueryclause, getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(siteManagementForm, siteManageBean);
			session.setAttribute("siteList", siteList);
		}
		BeanUtils.copyProperties(siteManageBean, siteManagementForm);
		if (session.getAttribute("siteList") != null
				|| (session.getAttribute("siteList") != null && (siteManagementForm
						.getFirst() != null
						|| siteManagementForm.getLast() != null
						|| siteManagementForm.getPrevious() != null || siteManagementForm
						.getNext() != null))) {
			if (request.getParameter("search") != null
					&& request.getParameter("search").equals("go")) {
				siteDetails = SiteManagementdao.getSitedisplayDetail(
						(ArrayList) session.getAttribute("siteList"),
						siteManagementForm.getLines_per_page(), "0",
						siteManageBean);
				BeanUtils.copyProperties(siteManagementForm, siteManageBean);
			} else {
				siteDetails = SiteManagementdao
						.getSitedisplayDetail(
								(ArrayList) session.getAttribute("siteList"),
								siteManagementForm.getOrg_lines_per_page(),
								siteManagementForm.getCurrent_page_no(),
								siteManageBean);
				BeanUtils.copyProperties(siteManagementForm, siteManageBean);
				request.setAttribute("count", 7 + "");
			}

		} else {
			siteList = SiteManagementdao.getSiteSearchDetail(siteManageBean,
					sortqueryclause, getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(siteManagementForm, siteManageBean);
			session.setAttribute("siteList", siteList);
			siteDetails = SiteManagementdao.getSitedisplayDetail(
					(ArrayList) session.getAttribute("siteList"),
					siteManagementForm.getOrg_lines_per_page(),
					siteManagementForm.getCurrent_page_no(), siteManageBean);
			BeanUtils.copyProperties(siteManagementForm, siteManageBean);
		}
		request.setAttribute("siteDetails", siteDetails);

		if (siteDetails != null)
			request.setAttribute("siteDetailListSize", siteDetails.size() + "");

		/* Start : Appendix Dashboard View Selector Code */
		if (request.getParameter("fromPage") != null)
			siteManagementForm.setFromPage(request.getParameter("fromPage"));

		if (siteManagementForm.getJobOwnerOtherCheck() != null
				&& !siteManagementForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					siteManagementForm.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId",
					siteManagementForm.getAppendixid());
			request.setAttribute("msaId", siteManagementForm.getMsaid());

			return mapping.findForward("temppage");

		}

		if (siteManagementForm.getJobSelectedStatus() != null
				&& siteManagementForm.getJobSelectedStatus().length != 0) {
			for (int i = 0; i < siteManagementForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ siteManagementForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ siteManagementForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (siteManagementForm.getJobSelectedOwners() != null
				&& siteManagementForm.getJobSelectedStatus().length != 0) {
			for (int j = 0; j < siteManagementForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ siteManagementForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ siteManagementForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					siteManagementForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					siteManagementForm.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId",
					siteManagementForm.getAppendixid());
			request.setAttribute("msaId", siteManagementForm.getMsaid());

			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				siteManagementForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				siteManagementForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				siteManagementForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				siteManagementForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(userid,
				siteManagementForm.getJobOwnerOtherCheck(), "prj_job_new",
				siteManagementForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();
		request.setAttribute("roleflag", roleflag);
		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(siteManagementForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				siteManagementForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("appendixid", siteManagementForm.getAppendixid());
		request.setAttribute("msaId", siteManagementForm.getMsaid());
		request.setAttribute("viewjobtype", siteManagementForm.getViewjobtype());
		request.setAttribute("ownerId", siteManagementForm.getOwnerId());

		String isDownList = request.getParameter("isDownloadList");

		if ("downloadTrue".equals(isDownList)) {

			response.setContentType("text/csv");
			response.setHeader("Content-Disposition",
					"attachment; filename=\"sitesList.csv\"");
			try {
				OutputStream outputStream = response.getOutputStream();
				StringBuilder outputResult = new StringBuilder(
						"Name, Number, Locality Factor, Union Site, Designator, Work Location, Address, City, State, ZIP Code, Country, Direction, Primray Contact Person, Primary Phone Number, Primary Email, Secondary Contact Person, Secondary Phone Number, Secondary Email, Special Condition, Brand\n\n");

				for (int i = 0; i < siteList.size(); i++) {
					SiteManagementBean bean = (SiteManagementBean) siteList
							.get(i);

					outputResult.append("\"" + bean.getSiteName() + "\",\""
							+ bean.getSiteNumber() + "\",\""
							+ bean.getSiteLocalityFactor() + "\",\""
							+ bean.getSiteUnion() + "\",\""
							+ bean.getSiteDesignator() + "\",\""
							+ bean.getSiteWorkLocation() + "\",\""
							+ bean.getSiteAddress() + "\",\""
							+ bean.getSiteCity() + "\",\"" + bean.getState()
							+ "\",\"" + bean.getSiteZipCode() + "\",\""
							+ bean.getCountry() + "\",\""
							+ bean.getSiteDirections() + "\",\""
							+ bean.getSitePrimaryName() + "\",\""
							+ bean.getSitePrimaryPhone() + "\",\""
							+ bean.getSitePrimaryEmail() + "\",\""
							+ bean.getSiteSecondaryName() + "\",\""
							+ bean.getSiteSecondayPhone() + "\",\""
							+ bean.getSiteSecondaryEmail() + "\",\""
							+ bean.getSiteSpecialConditions() + "\",\""
							+ bean.getMsaname() + "\"\n");
				}

				outputStream.write(outputResult.toString().getBytes());
				outputStream.flush();
				outputStream.close();
			} catch (Exception e) {
				System.out.println(e.toString());
			}

		}
		return mapping.findForward("success");
	}

	public String getSortQueryClause(String queryclause, HttpSession session) {
		if (session.getAttribute("site_sort") != null)
			queryclause = (String) session.getAttribute("site_sort");
		return queryclause;
	}
}
