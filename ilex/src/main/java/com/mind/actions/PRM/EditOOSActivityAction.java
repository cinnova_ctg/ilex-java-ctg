package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboPRM;
import com.mind.bean.pm.Activity;
import com.mind.bean.prm.EditOOSActivityBean;
import com.mind.common.Util;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PRM.EditOOSActivitydao;
import com.mind.formbean.PRM.EditOOSActivityForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class EditOOSActivityAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		EditOOSActivityForm oosactivityform = (EditOOSActivityForm) form;
		EditOOSActivityBean oosactivityBean = new EditOOSActivityBean();
		int retval = 0;
		ArrayList methodList = null;
		DynamicComboPRM dcprm = new DynamicComboPRM();
		boolean resourecListPage = false;

		/* Page Security Start */
		Map<String, Object> map;
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		/* Page Security End */

		if (request.getParameter("addOOA") != null)
			oosactivityform.setFlagOOA(request.getParameter("addOOA"));

		if (request.getAttribute("addOOA") != null)
			oosactivityform.setFlagOOA((String) request.getAttribute("addOOA"));

		if (request.getParameter("Appendix_Id") != null)
			oosactivityform.setAppendixId(request.getParameter("Appendix_Id"));

		if (request.getAttribute("Appendix_Id") != null)
			oosactivityform.setAppendixId(""
					+ request.getAttribute("Appendix_Id"));

		if (request.getParameter("Job_Id") != null)
			oosactivityform.setJobId(request.getParameter("Job_Id"));

		if (request.getAttribute("Job_Id") != null)
			oosactivityform.setJobId("" + request.getAttribute("Job_Id"));

		if (request.getParameter("Activity_Id") != null)
			oosactivityform.setActivityId(request.getParameter("Activity_Id"));

		if (request.getAttribute("Activity_Id") != null)
			oosactivityform.setActivityId(""
					+ request.getAttribute("Activity_Id"));

		/* Notification Methods combo start */
		methodList = EditOOSActivitydao
				.getNotificationMethodList(getDataSource(request, "ilexnewDB"));
		dcprm.setNotificationMethodList(methodList);
		request.setAttribute("methodcombo", dcprm);

		/* Notification Methods combo end */
		if (oosactivityform.getJobId() != null
				&& !oosactivityform.getJobId().trim().equalsIgnoreCase("")) {

			map = DatabaseUtilityDao.setMenu(oosactivityform.getJobId(),
					userid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		if (oosactivityform.getSave() != null) {
			/* Add new Out of Scope Activity */

			if (oosactivityform.getActivityId() == null
					|| oosactivityform.getActivityId().equals("")
					|| oosactivityform.getActivityId().equals("0")) {

				Activity activity = setDefaultOOSInfo(oosactivityform);
				String addSFlag = Activitydao.Addactivity(activity, "IA",
						userid, getDataSource(request, "ilexnewDB"), "");
				String addflag = addSFlag.substring(0, addSFlag.indexOf("|"));
				String Activity_Id = addSFlag.substring(
						addSFlag.indexOf("|") + 1, addSFlag.length());

				/* Set OOS flag to Y into added OOS activity */
				if (addflag.equals("0")) {
					Activitydao.setOOSFlag(Activity_Id,
							getDataSource(request, "ilexnewDB"));
				}

				/* When Activity not Added */
				if (!addflag.equals("0")) {
					oosactivityform.setFlagOOA("addOOA");
					request.setAttribute("addOOA", "addOOA");
				} else {
					resourecListPage = true;
				}
				request.setAttribute("addflag", "" + addflag);
				oosactivityform.setActivityId(Activity_Id);
			} // - End of Add OOS Activity

			/* Update OOS Activity */
			if (oosactivityform.getActivityId() != null
					&& !oosactivityform.getActivityId().equals("")
					&& !oosactivityform.getActivityId().equals("0")) {
				retval = EditOOSActivitydao.updateOOSActivity(
						oosactivityform.getJobId(),
						oosactivityform.getActivityId(),
						oosactivityform.getActivityName(),
						oosactivityform.getDateNotified(),
						oosactivityform.getNotificationMethod(),
						oosactivityform.getCustName(),
						oosactivityform.getDateApproved(),
						oosactivityform.getDescription(), userid,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("oosretval", retval + "");
			} // - End of Update OOS activity
		}// - End of Save

		/* Get OSS data */
		if (oosactivityform.getActivityId() != null
				&& !oosactivityform.getActivityId().equals("")
				&& !oosactivityform.getActivityId().equals("0")) {
			BeanUtils.copyProperties(oosactivityBean, oosactivityform);
			EditOOSActivitydao.getOSSActivityData(oosactivityBean,
					oosactivityform.getActivityId(),
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(oosactivityform, oosactivityBean);
		} else {
			setDefaultAttribute(oosactivityform);
		}

		request.setAttribute("function", "view");
		request.setAttribute("jobid", "" + oosactivityform.getJobId());
		request.setAttribute("appendixid", "" + oosactivityform.getAppendixId());

		// request.setAttribute("addflag", ""+request.getAttribute("addflag"));

		if (oosactivityform.getSave() != null && resourecListPage) {
			request.setAttribute("Activity_Id", oosactivityform.getActivityId());
			return (mapping.findForward("resourecListPage"));
		} else if (oosactivityform.getFlagOOA() == null
				&& oosactivityform.getSave() != null) {
			return (mapping.findForward("jobdashboard"));
		} else {
			return (mapping.findForward("success"));
		}
	}

	/**
	 * @param oosactivityform
	 * @return
	 */
	private Activity setDefaultOOSInfo(EditOOSActivityForm oosactivityform) {
		Activity activity = new Activity();
		activity.setJob_Id(oosactivityform.getJobId());
		if (oosactivityform.getActivityName().equalsIgnoreCase(
				"Out of Scope Resources"))
			activity.setName("oos");
		else
			activity.setName(oosactivityform.getActivityName()); // Out of Scope
																	// Activity
		activity.setActivity_Id("0");
		activity.setActivitytypecombo("O");
		activity.setQuantity("1.0");
		activity.setEstimated_materialcost("0.00");
		activity.setEstimated_cnsfieldlaborcost("0.00");
		activity.setEstimated_contractfieldlaborcost("0.00");
		activity.setEstimated_freightcost("0.00");
		activity.setEstimated_travelcost("0.00");
		activity.setEstimated_totalcost("0.00");
		activity.setExtendedprice("0.00");
		activity.setOverheadcost("0.00");
		activity.setListprice("0.00");
		activity.setProformamargin("0.00");
		activity.setEstimated_start_schedule("");
		activity.setEstimated_complete_schedule("");
		activity.setActivity_lib_Id("-1");
		activity.setStatus("A");
		activity.setAddendum_id("0");
		return activity;
	}

	/**
	 * @param oosactivityform
	 */
	private void setDefaultAttribute(EditOOSActivityForm oosactivityform) {
		if (Util.isNullOrBlank(oosactivityform.getActivityName())) {
			oosactivityform.setActivityName("Out of Scope Resources");
		}

		oosactivityform.setDateNotified("");
		oosactivityform.setNotificationMethod("");
		oosactivityform.setCustName("");
		oosactivityform.setDateApproved("");
		oosactivityform.setDescription("");
	}
}
