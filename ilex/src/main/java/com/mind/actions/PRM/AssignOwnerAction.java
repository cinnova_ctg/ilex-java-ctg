package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboPRM;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.common.formbean.EmailForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AssignOwnerdao;
import com.mind.dao.PRM.NewWebTicketdao;
import com.mind.formbean.PRM.AssignOwnerForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class AssignOwnerAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		AssignOwnerForm assignmentOwnerform = (AssignOwnerForm) form;

		ArrayList ownerlist = null;
		int intJobid = 0, intnewOwnerid = 0;
		DynamicComboPRM dynamicComboOwner = new DynamicComboPRM();
		;
		String ownerFName = null;
		String ownerLName = null;
		String ownerDesin = null;
		String msaId = null;
		String msaName = null;
		String messageContent = null;
		String tickeNumber = null;
		String loginuserid = (String) request.getSession(false).getAttribute(
				"userid");
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		Map<String, Object> map;
		EmailBean emailBean = new EmailBean();
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (assignmentOwnerform.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage Project",
					"View Appendix Dashboard",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */
		if (request.getParameter("changeFilter") != null) {
			assignmentOwnerform.setChangeFilter(request
					.getParameter("changeFilter"));
		}

		if (request.getParameter("jobid") != null) {
			assignmentOwnerform.setJobId(request.getParameter("jobid"));
		}
		if (assignmentOwnerform.getJobId() != null
				&& !assignmentOwnerform.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(assignmentOwnerform.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		if (request.getParameter("viewjobtype") != null) {
			assignmentOwnerform.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
			request.setAttribute("viewjobtype",
					request.getParameter("viewjobtype"));
		} else {
			assignmentOwnerform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			assignmentOwnerform
					.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			assignmentOwnerform.setOwnerId("%");
		}

		if (request.getParameter("nettype") != null) {
			assignmentOwnerform
					.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getParameter("appendix_Id") != null) {
			request.setAttribute("appendixid",
					request.getParameter("appendix_Id"));
			assignmentOwnerform.setAppendix_Id(request
					.getParameter("appendix_Id"));
		}

		if (request.getParameter("function") != null) {
			assignmentOwnerform.setFunction(request.getParameter("function"));
		}

		if (request.getParameter("currentOwner") != null) {
			assignmentOwnerform.setOwnerName(request
					.getParameter("currentOwner"));
			assignmentOwnerform.setCurrentScheduler(request
					.getParameter("currentOwner")); // - New Scheduler Combo-Box
		}

		if (request.getParameter("fromPage") != null) {
			assignmentOwnerform.setFromPage(request.getParameter("fromPage"));
		} else {
			assignmentOwnerform.setFromPage("");
		}

		/* For get current Scheduler: Start */
		if (assignmentOwnerform.getJobId() != null) {
			assignmentOwnerform.setCurrentScheduler(AssignOwnerdao
					.getCurrentScheduler(assignmentOwnerform.getJobId(),
							getDataSource(request, "ilexnewDB"))); // - Store
																	// current
																	// Scheduler
		}
		/* For get current Scheduler: End */

		// Added By Amit for getting owner for a appenidx appendix_Id
		if (request.getParameter("from") != null) {
			assignmentOwnerform.setFrom(request.getParameter("from"));
		}
		// End : Added By Amit

		// Start :For Saving

		if (assignmentOwnerform.getSave() != null) {
			if (assignmentOwnerform.getNewOwner() != null) {
				if (Integer.parseInt(assignmentOwnerform.getNewOwner().trim()) != 0) {
					intJobid = Integer.parseInt(assignmentOwnerform.getJobId()
							.trim());
					intnewOwnerid = Integer.parseInt(assignmentOwnerform
							.getNewOwner().trim());
					if (intnewOwnerid != 0) {
						AssignOwnerdao.changeOwner(
								getDataSource(request, "ilexnewDB"), intJobid,
								intnewOwnerid);
					}
				}
			}
			/* For Update Job Scheduler: Start */
			if (assignmentOwnerform.getNewScheduler() != null
					&& !assignmentOwnerform.getNewScheduler().equals("0")) {
				AssignOwnerdao.assignScheduler(assignmentOwnerform.getJobId()
						.trim(), assignmentOwnerform.getNewScheduler(), userid,
						getDataSource(request, "ilexnewDB"));
			}
			if (request.getParameter("fromPage") != null
					&& request.getParameter("fromPage").equals("jobdashboard")) {
				request.setAttribute("jobid", assignmentOwnerform.getJobId()
						.trim()); // - job Dashboard
				return mapping.findForward("jobdashboard");
			}
			/* For Update Job Scheduler: End */

			if (assignmentOwnerform.getFromPage().equals("webTicket")) {
				EmailForm emailform = new EmailForm();
				emailform.setUsername(this.getResources(request).getMessage(
						"common.Email.username"));
				emailform.setUserpassword(this.getResources(request)
						.getMessage("common.Email.userpassword"));
				// emailform.setSmtpservername( this.getResources( request
				// ).getMessage( "common.Email.smtpservername" ) );
				emailform.setSmtpservername(EnvironmentSelector
						.getBundleString("common.Email.smtpservername"));
				emailform.setSmtpserverport(this.getResources(request)
						.getMessage("common.Email.smtpserverport"));

				ownerFName = this.getResources(request).getMessage(
						"common.Email.ownerfname");
				ownerLName = this.getResources(request).getMessage(
						"common.Email.ownerlname");
				ownerDesin = this.getResources(request).getMessage(
						"common.Email.ownerdesin");

				tickeNumber = NewWebTicketdao.getTicketNumber(
						getDataSource(request, "ilexnewDB"),
						assignmentOwnerform.getJobId());
				msaId = IMdao.getMSAId(IMdao.getAppendixId(
						assignmentOwnerform.getJobId(),
						getDataSource(request, "ilexnewDB")),
						getDataSource(request, "ilexnewDB"));
				msaName = Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"), msaId);

				messageContent = "You have been assigned the following ticket: "
						+ tickeNumber
						+ " for "
						+ msaName
						+ ". Please take the following actions:\n\n1)  Receive the ticket per current procedures.\n\n2)  Review and update the ticket in Ilex.  This ticket has been received on-line so some of the required information will already be populated in the ticket.  Update and correct information as needed.\n\nReply to the email when the above actions are complete.\n\n\n"
						+ ownerFName
						+ " "
						+ ownerLName
						+ "\n"
						+ ownerDesin
						+ "\nContingent Network Services, LLC\n4400 Port Union Road\nWest Chester, OH 45011\n(800) 506-9609 x470\n(513) 266-5775 (mobile)\n(513) 860-2105 (facsimile)\nmedius.contingent.com";

				emailform.setCheck_for_fileupload(MSAdao.getfile_upload_flag(
						msaId, getDataSource(request, "ilexnewDB")));
				emailform.setSubject(msaName + ", Ticket # " + tickeNumber);
				emailform.setContent(messageContent);

				emailform.setFrom(NewWebTicketdao.getOwnerEmail(
						getDataSource(request, "ilexnewDB"), ownerFName,
						ownerLName));
				emailform.setFromName(ownerFName + " " + ownerLName);
				emailform.setTo(NewWebTicketdao.getPOCEmail(intnewOwnerid + "",
						getDataSource(request, "ilexnewDB")));
				emailform.setType("webTicket");

				emailform.setCc(NewWebTicketdao.getOwnerEmail(
						getDataSource(request, "ilexnewDB"), ownerFName,
						ownerLName));
				emailform.setBcc("");
				BeanUtils.copyProperties(emailBean, emailform);
				Email.Send(emailBean, getDataSource(request, "ilexnewDB"));

				return (mapping.findForward("webTicketPage"));
			}

			else {
				request.setAttribute("type", "prj_appendix");
				return mapping.findForward("tempPage");
			}
		}
		// End :For Saving

		// Start:Added By Amit For getting all owner for particular appendix.
		// End :

		if (assignmentOwnerform.getFrom() != null) {
			if (assignmentOwnerform.getAppendix_Id() != null)
				ownerlist = AssignOwnerdao.getAllOwnerForAppendix(
						assignmentOwnerform.getAppendix_Id(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				ownerlist = AssignOwnerdao.getAllOwnerForAppendix("%", userid,
						getDataSource(request, "ilexnewDB"));
		} else {
			// ownerlist = AssignOwnerdao.getAllOwnername(getDataSource( request
			// , "ilexnewDB" ));
			ownerlist = AssignOwnerdao.getSchedulerList(
					assignmentOwnerform.getAppendix_Id(), "Job Owner",
					getDataSource(request, "ilexnewDB"));
		}

		dynamicComboOwner.setAllOwnerName(ownerlist);
		request.setAttribute("dyComboPRMOwner", dynamicComboOwner);

		/* For Scheduler: Start */
		assignmentOwnerform.setSchedulerList(AssignOwnerdao.getSchedulerList(
				assignmentOwnerform.getAppendix_Id(), "Scheduler",
				getDataSource(request, "ilexnewDB"))); // - New Scheduler
														// Combo-Box
		/* For Scheduler: End */

		return mapping.findForward("success");
	}
}
