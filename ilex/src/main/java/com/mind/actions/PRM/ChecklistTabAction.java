package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.ChangeStatus;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PRM.ChecklistTabdao;
import com.mind.formbean.PRM.ChecklistTabBean;
import com.mind.formbean.PRM.ChecklistTabForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.util.WebUtil;

public class ChecklistTabAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();

		ChecklistTabForm checklisttabform = (ChecklistTabForm) form;
		ChecklistTabBean checklisttabbean;

		String Job_Id = "";
		int site_checklist_size = 0;
		int partner_checklist_size = 0;
		String activityidlist = "";
		int updateflag = -1;
		int partnerupdateflag = -1;
		int qsize = 0;
		String selectedoptionsid = "";
		int partnerid = 0;
		String siteid = "0";
		String sitename = "";
		// String partnername="";
		int noofpartners = 0;
		String partnernamelist = "";
		String partneridlist = "";
		String type = "";
		String jobCompletedFlag = "";
		Map<String, Object> map;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		/*
		 * if( sitechecklistform.getAuthenticate().equals( "" ) ) {
		 * 
		 * if( !Authenticationdao.getPageSecurity( userid , "Manage Appendix" ,
		 * "QC Checklist" , getDataSource( request , "ilexnewDB" ) ) ) { return(
		 * mapping.findForward( "UnAuthenticate" ) ); }
		 * 
		 * 
		 * }
		 */
		/* Page Security End */
		String tempString = "";
		if (request.getParameter("tempString") != null) {

			tempString = (String) request.getParameter("tempString");
			if (tempString.contains("Type")) {
				int startIndex = tempString.indexOf("=");
				int lastindex = tempString.length();
				type = tempString.substring(startIndex + 1, lastindex);
			}
		} else {
			tempString = "";
		}

		if (request.getParameter("type") != null) {
			type = request.getParameter("type").toString();
		}

		checklisttabform.setType(type);
		request.setAttribute("type", checklisttabform.getType());

		String Status = "";
		if (request.getParameter("Status") != null) {
			Status = request.getParameter("Status");
		}

		checklisttabform.setStatus(Status);
		request.setAttribute("Status", Status);

		String popwin = "0";
		if (request.getParameter("popwin") != null) {
			popwin = (request.getParameter("popwin"));
		}

		request.setAttribute("popwin", popwin);

		if (request.getParameter("checkMsp") != null) {
			checklisttabform.setCheckMsp(request.getParameter("checkMsp"));
		}

		if (request.getParameter("job_id") != null)
			Job_Id = request.getParameter("job_id");

		if (request.getParameter("viewjobtype") != null) {
			checklisttabform
					.setViewjobtype(request.getParameter("viewjobtype"));
		} else {
			checklisttabform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			checklisttabform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			checklisttabform.setOwnerId("%");
		}

		if (request.getParameter("page") != null) {
			checklisttabform.setPage("" + request.getParameter("page"));
		}

		String poId = "";
		if (request.getParameter("poId") != null) {
			poId = request.getParameter("poId");
		}
		/*
		 * if(request.getParameter("partnerid") != null &&
		 * !(request.getParameter("partnerid").equals(""))) {
		 * 
		 * partnerid=Integer.parseInt(request.getParameter("partnerid")+"");
		 * checklisttabform.setPartnerid(request.getParameter("partnerid")); }
		 */

		if (request.getParameter("jobCompleted") != null) {
			jobCompletedFlag = request.getParameter("jobCompleted");
		}

		request.setAttribute("jobCompleted", jobCompletedFlag);

		checklisttabform.setJob_id(Job_Id);

		checklisttabform.setJobname(com.mind.dao.PM.Jobdao.getJobname(Job_Id,
				getDataSource(request, "ilexnewDB")));

		checklisttabform.setAppendix_id(com.mind.dao.IM.IMdao.getAppendixId(
				Job_Id, getDataSource(request, "ilexnewDB")));

		checklisttabform.setAppendixname(com.mind.dao.PRM.Appendixdao
				.getAppendixname(checklisttabform.getAppendix_id(),
						getDataSource(request, "ilexnewDB")));

		checklisttabform.setMsaname(com.mind.dao.PRM.Appendixdao.getMsaname(
				checklisttabform.getAppendix_id(),
				getDataSource(request, "ilexnewDB")));

		String msa_id = IMdao.getMSAId(checklisttabform.getAppendix_id(),
				getDataSource(request, "ilexnewDB"));
		if (request.getParameter("isPO") != null
				&& !request.getParameter("isPO").trim().equals("")
				&& request.getParameter("poId") != null) {
			request.setAttribute("isPO", request.getParameter("isPO"));
			partneridlist = ChecklistTabdao.getPartnersIds(Job_Id, poId,
					getDataSource(request, "ilexnewDB"));
		} else {
			partneridlist = ChecklistTabdao.getPartnersIdlist(Job_Id,
					getDataSource(request, "ilexnewDB"));
		}

		int changestatusflag = -1;

		if (checklisttabform.getJob_id() != null
				&& !checklisttabform.getJob_id().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(checklisttabform.getJob_id(),
					userid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}

		if (checklisttabform.getSavelist() != null) {
			// start

			if (popwin != null) {
				if (popwin.equals("1")) {
					request.setAttribute("changestatusflag", "" + 0);
				}
			}
			// over

			if (checklisttabform.getCheck() != null) {
				for (int i = 0; i < checklisttabform.getCheck().length; i++) {
					if (i == (checklisttabform.getCheck().length - 1)) {
						activityidlist = activityidlist
								+ checklisttabform.getCheck(i);
					} else {
						activityidlist = activityidlist
								+ checklisttabform.getCheck(i) + ",";
					}

				}
			}

			if (request.getParameter("isPO") == null
					|| request.getParameter("isPO").trim().equals("")) {
				updateflag = ChecklistTabdao.addSiteChecklist(activityidlist,
						Job_Id, checklisttabform.getValuesuccess(),
						checklisttabform.getSiteJobCompleted(),
						checklisttabform.getSiteJobProfessional(),
						checklisttabform.getJobSiteFollowUp(),
						checklisttabform.getPocTech(),
						checklisttabform.getPocNumber(),
						checklisttabform.getPocComments(),
						getDataSource(request, "ilexnewDB"));
				if (updateflag == 0) {
					ChecklistTabdao.addSitePocJobRating(Job_Id,
							checklisttabform.getSiteJobCompleted(),
							checklisttabform.getSiteJobProfessional(),
							getDataSource(request, "ilexnewDB"));
				}
			}
			if (request.getParameter("selectedoption") != null) {
				selectedoptionsid = request.getParameter("selectedoption");

				if (selectedoptionsid.length() > 0) {

					selectedoptionsid = selectedoptionsid.substring(0,
							selectedoptionsid.length() - 1);
				}

				// if (checklisttabform.getUserRatingFlag().length() > 0) {
				//
				// checklisttabform.setUserRatingFlag(checklisttabform
				// .getUserRatingFlag().substring(
				// 0,
				// checklisttabform.getUserRatingFlag()
				// .length() - 1));
				// }

				if (selectedoptionsid.length() > 0) {

					checklisttabform.setRecommadTechFutureJobs(checklisttabform
							.getRecommadTechFutureJobs().substring(
									0,
									checklisttabform
											.getRecommadTechFutureJobs()
											.length() - 1));
				}

				// updateflag = ChecklistTabdao.addPartnerQcChecklist(
				// partnerid,selectedoptionsid,userid,getDataSource( request ,
				// "ilexnewDB" ));
				// TODO here for thumbsup jayaraju
				partnerupdateflag = ChecklistTabdao.addPartnerQcChecklist(
						partneridlist, selectedoptionsid, userid,
						checklisttabform.getFirstvisitpartnersuccess(), Job_Id,
						jobCompletedFlag, getDataSource(request, "ilexnewDB"));
				if (partnerupdateflag == 0) {

					List<String[]> list = new ArrayList<String[]>();
					if (partneridlist.length() > 0) {
						String[] partnerArr = partneridlist.split("~");
						// String[] ratingArr = checklisttabform
						// .getUserRatingFlag().split("~");
						String[] comments = checklisttabform
								.getOperationRatingComments().split("~\\|~");
						String[] techFutureArr = checklisttabform
								.getRecommadTechFutureJobs().split("~");
						for (int i = 0; i < partnerArr.length; i++) {

							// list.add(new String[] { partnerArr[i],
							// ratingArr[i], techFutureArr[i], comments[i] });
							list.add(new String[] { partnerArr[i],
									techFutureArr[i], comments[i] });
						}
					}

					ChecklistTabdao.addPartnerRatingQcChecklist(partneridlist,
							selectedoptionsid, list, Job_Id, userid,
							getDataSource(request, "ilexnewDB"));
					ChecklistTabdao.addInternalPartnerRatingQcChecklist(
							partneridlist,
							checklisttabform.getRecommadTechFutureJobs(),
							userid, getDataSource(request, "ilexnewDB"));

				}
			}

			request.setAttribute("updateflag", updateflag + "");
			request.setAttribute("partnerupdateflag", partnerupdateflag + "");

			if (updateflag == 0 && partnerupdateflag == 0) {
				String temppath = "";
				if (checklisttabform.getPage().equals("jobdashboard")) {
					temppath = "PartnerChecklistJobdashboard";
				} else {
					temppath = "PartnerChecklistAppendixdashboard";
				}
				request.setAttribute("type", temppath);
				request.setAttribute("appendix_Id",
						checklisttabform.getAppendix_id());
				request.setAttribute("job_Id", checklisttabform.getJob_id());
				request.setAttribute("viewjobtype",
						checklisttabform.getViewjobtype());
				request.setAttribute("ownerId", checklisttabform.getOwnerId());
				return (mapping.findForward("temppage"));
			}
		}

		if (request.getParameter("checkMsp") != null
				&& request.getParameter("checkMsp").equals("1")) {
			changestatusflag = ChangeStatus.Status(msa_id,
					checklisttabform.getAppendix_id(), Job_Id, "", "",
					checklisttabform.getType(), checklisttabform.getStatus(),
					"", getDataSource(request, "ilexnewDB"), "");
			request.setAttribute("changestatusflag", "" + changestatusflag);
			if (changestatusflag == 0) {
				com.mind.dao.PRM.Jobdao.updateJobOffSiteDate(Job_Id,
						getDataSource(request, "ilexnewDB"));
			}
		}

		ArrayList sitechecklist;
		ArrayList partnerchecklist;

		String[] checkedsitelist;

		/* Added later */
		if (request.getParameter("isPO") != null
				&& !request.getParameter("isPO").trim().equals("")
				&& request.getParameter("poId") != null) {
			noofpartners = ChecklistTabdao.getNoOfPartnersByPO(Job_Id, poId,
					getDataSource(request, "ilexnewDB"));
		} else {
			noofpartners = ChecklistTabdao.getNoOfPartners(Job_Id,
					getDataSource(request, "ilexnewDB"));
		}

		checklisttabform.setNo_of_partners(noofpartners + "");

		if (request.getParameter("tabId") != null) {
			request.setAttribute("tabId", request.getParameter("tabId"));
		}
		if (request.getParameter("ticketType") != null) {
			request.setAttribute("ticketType",
					request.getParameter("ticketType"));
		}
		if (request.getParameter("ticket_id") != null) {
			request.setAttribute("ticket_id", request.getParameter("ticket_id"));
		}

		if (request.getParameter("isActionComplete") != null
				&& !request.getParameter("isActionComplete").trim().equals("")) {
			request.setAttribute("isActionComplete",
					request.getParameter("isActionComplete"));
			// PurchaseOrder.complete(poId, userid, getDataSource( request ,
			// "ilexnewDB" ));
		}
		if (request.getParameter("showJobDB") != null
				&& !request.getParameter("showJobDB").equals("")) {
			if (request.getParameter("isActionComplete") != null
					&& !request.getParameter("isActionComplete").trim()
							.equals("")) {
				request.setAttribute("isActionComplete",
						request.getParameter("isActionComplete"));
				PurchaseOrder.complete(poId, userid,
						getDataSource(request, "ilexnewDB"));
			}
			String url = "/JobDashboardAction.do?hmode=POaction&tabId="
					+ request.getParameter("tabId") + "&isClicked=3&ticket_id="
					+ request.getParameter("ticket_id") + "&ticketType="
					+ request.getParameter("ticketType") + "&appendix_Id="
					+ checklisttabform.getAppendix_id() + "&Job_Id="
					+ checklisttabform.getJob_id();
			ActionForward fwd = new ActionForward();
			fwd.setPath(url);
			return fwd;
		}
		if (request.getParameter("isPO") != null
				&& !request.getParameter("isPO").trim().equals("")) {
			request.setAttribute("isPO", request.getParameter("isPO"));
			partnernamelist = ChecklistTabdao.getPartnerName(Job_Id, poId,
					getDataSource(request, "ilexnewDB"));
		} else {
			partnernamelist = ChecklistTabdao.getPartnersNamelist(Job_Id,
					getDataSource(request, "ilexnewDB"));
		}

		siteid = ChecklistTabdao.getSiteid(Job_Id,
				getDataSource(request, "ilexnewDB"));
		sitename = ChecklistTabdao.getSitename(siteid,
				getDataSource(request, "ilexnewDB"));

		qsize = ChecklistTabdao.getPartnerQCchecklistSize(getDataSource(
				request, "ilexnewDB"));
		checklisttabform.setQuessize(qsize + "");

		checklisttabform.setSelectedoption(ChecklistTabdao.getQuesOptions(
				partneridlist, Job_Id, getDataSource(request, "ilexnewDB")));
		request.setAttribute("selectedOptionSize", checklisttabform
				.getSelectedoption().split("~").length);
		/* Added later */

		sitechecklist = ChecklistTabdao.getActivityList(Job_Id,
				getDataSource(request, "ilexnewDB"));
		partnerchecklist = ChecklistTabdao.getPartnerQCchecklist(getDataSource(
				request, "ilexnewDB"));

		checkedsitelist = ChecklistTabdao.getSavedSiteChecklist(Job_Id,
				getDataSource(request, "ilexnewDB")); // for activity div
														// checkbox check

		checklisttabform.setCheck(checkedsitelist);

		if (sitechecklist.size() > 0) {
			site_checklist_size = sitechecklist.size();
		}
		if (partnerchecklist.size() > 0) {
			partner_checklist_size = partnerchecklist.size();
		}

		String firstvisitsuccesspartner = ChecklistTabdao
				.getFirstvisitsuccessPartner(partneridlist, Job_Id,
						getDataSource(request, "ilexnewDB"));

		checklisttabform.setFirstvisitpartner(firstvisitsuccesspartner);

		// String userRatingRetValues = ChecklistTabdao.getUserRating(
		// partneridlist, Job_Id, getDataSource(request, "ilexnewDB"));
		//
		// checklisttabform.setUserRatingFlag(userRatingRetValues);

		String techFutureRetValues = ChecklistTabdao.getTechFutureRecommad(
				partneridlist, Job_Id, getDataSource(request, "ilexnewDB"));

		checklisttabform.setRecommadTechFutureJobs(techFutureRetValues);

		String oprRattingComments = ChecklistTabdao
				.getOperationRattingComments(partneridlist, Job_Id,
						getDataSource(request, "ilexnewDB"));

		checklisttabform.setOperationRatingComments(oprRattingComments);

		String[] siteQuesList = new String[4];
		siteQuesList = ChecklistTabdao.getFirstvisitsuccessJob(Job_Id,
				getDataSource(request, "ilexnewDB"));
		checklisttabform.setFirstvisitsuccess(siteQuesList[0]);
		checklisttabform.setSiteJobCompleted(siteQuesList[1]);
		checklisttabform.setSiteJobProfessional(siteQuesList[2]);
		checklisttabform.setJobSiteFollowUp(siteQuesList[3]);
		checklisttabform.setPocTech(siteQuesList[4]);
		checklisttabform.setPocNumber(siteQuesList[5]);
		checklisttabform.setPocComments(siteQuesList[6]);

		request.setAttribute("Size", new Integer(site_checklist_size));
		request.setAttribute("partnerchecklistSize", new Integer(
				partner_checklist_size));
		if (request.getParameter("isPO") == null
				|| request.getParameter("isPO").equals("")
				|| request.getParameter("poId") == null) {
			request.setAttribute("sitechecklist", sitechecklist);
		}

		request.setAttribute("partnerchecklist", partnerchecklist);

		// request.setAttribute( "partnerid" , partnerid+"" );
		// request.setAttribute( "partnername" , partnername );
		request.setAttribute("sitename", sitename);
		request.setAttribute("totalpartners",
				checklisttabform.getNo_of_partners());
		request.setAttribute("partnernamelist", partnernamelist);
		request.setAttribute("partneridlist", partneridlist);
		if (!partnernamelist.equals("")) {
			request.setAttribute("partnernamelistsize",
					partnernamelist.split("~").length);
		}
		if (request.getAttribute("NetMedX") != null) {
			request.setAttribute("NetMedX", "NetMedX");
		}

		forward = mapping.findForward("success");
		return (forward);

	}
}
