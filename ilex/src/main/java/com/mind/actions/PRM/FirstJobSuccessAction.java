package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.formbean.PRM.FirstJobSuccessForm;


public class FirstJobSuccessAction extends com.mind.common.IlexAction{
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		FirstJobSuccessForm firstJobSuccessForm = ( FirstJobSuccessForm ) form;
		
		String tempString = "";
		
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Manage Project" , "View Job Dashboard" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		if(request.getParameter("jobid")!=null)
		{
			firstJobSuccessForm.setJobid(request.getParameter("jobid"));
		}
		if(request.getParameter("tempString")!=null)
		{
			
			tempString = (String)request.getParameter("tempString");
			if(tempString.contains("Type"))
			{
				int startIndex = tempString.indexOf("=");
				int lastindex = tempString.length();
				firstJobSuccessForm.setType(tempString.substring(startIndex+1,lastindex ));
			}
		}
		else
		{
			tempString = "";
		}
		if(firstJobSuccessForm.getSubmit()!=null)
		{
			
			request.setAttribute("jobid",firstJobSuccessForm.getJobid());
			request.setAttribute("status",firstJobSuccessForm.getViewjobtype());
			
			request.setAttribute("firstjobsuccess",firstJobSuccessForm.getFirstJobSuccess());
			request.setAttribute("Type",firstJobSuccessForm.getType());
			return mapping.findForward( "tomenuchangestatus" );
		}
	
		firstJobSuccessForm.setFirstJobSuccess("N");
		firstJobSuccessForm.setViewjobtype("F");
		
		return mapping.findForward( "success" );
	}

}
