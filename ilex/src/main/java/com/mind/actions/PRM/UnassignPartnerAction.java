package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.formbean.PRM.UnassignPartnerForm;

public class UnassignPartnerAction extends com.mind.common.IlexAction{

	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		UnassignPartnerForm unassignPartnerForm = (UnassignPartnerForm) form;
		
		String typeid="";
		String pid="";
		String type="";
		int statusvalue=0; 
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		/*if( UnassignPartnerForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Project" , "Assign Partner" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}*/
		/* Page Security End*/
		
		if(request.getParameter("typeid")!=null)
		{
			typeid=request.getParameter("typeid");
		}
		if(request.getParameter("type")!=null)
		{
			type=request.getParameter("type");
		}
		if(request.getParameter("pid")!=null)
		{
			pid=request.getParameter("pid");
		}
		
		request.setAttribute("unassignmessage",statusvalue+"");
		forward=mapping.findForward("success");
		return (forward);
	}




}
