package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.bean.Sow;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.formbean.SOWForm;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ManageStatusAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManageStatusAction.class);

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SOWForm sowform = (SOWForm) form;
		int status_size = 0;

		ArrayList managestatuslist = new ArrayList();
		Map<String, Object> map;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");
		if (sowform.getAuthenticate().equals("")) {

			if (request.getParameter("type") != null) {
				if (request.getParameter("type").equals("prm_appendix")) {
					if (!Authenticationdao.getPageSecurity(loginuserid,
							"Manage Appendix", "Update Status Report",
							getDataSource(request, "ilexnewDB"))) {
						return (mapping.findForward("UnAuthenticate"));
					}
				}
			}

		}
		/* Page Security End */

		if (request.getParameter("id") != null) {
			sowform.setId(request.getParameter("id"));
		}

		if (request.getParameter("type") != null) {
			sowform.setType(request.getParameter("type"));
		}

		if (request.getParameter("ref") != null) {
			sowform.setRef(request.getParameter("ref"));
		}

		if (request.getParameter("viewjobtype") != null) {
			sowform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			sowform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			sowform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			sowform.setOwnerId("%");
		}

		managestatuslist = Appendixdao.getStatusreport(
				getDataSource(request, "ilexnewDB"), sowform.getId(),
				sowform.getType());

		if (managestatuslist.size() > 0) {
			status_size = managestatuslist.size();
		}

		request.setAttribute("managestatuslist", managestatuslist);
		request.setAttribute("Size", status_size + "");

		// By Amit ,Start:Added to get Appendix name on View status Report
		// Screen

		if (sowform.getType().equalsIgnoreCase("prm_appendix")) {
			sowform.setAppindixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), sowform.getId()));

		}
		if (sowform.getType().equalsIgnoreCase("prm_job")) {
			sowform.setJobName(Jobdao.getJobname(sowform.getId(),
					getDataSource(request, "ilexnewDB")));
			if (sowform.getId() != null
					&& !sowform.getId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(sowform.getId(), loginuserid,
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}

		}
		// By Amit ,Start:Added to get Appendix name on View status Report
		// Screen

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(sowform.getId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				sowform.getId(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		sowform.setMsaId(IMdao.getMSAId(sowform.getId(),
				getDataSource(request, "ilexnewDB")));
		request.setAttribute("viewjobtype", sowform.getViewjobtype());
		request.setAttribute("ownerId", sowform.getOwnerId());
		request.setAttribute("msaId", sowform.getMsaId());
		request.setAttribute("appendixid", sowform.getId());

		ActionForward forward = setViewSelector(mapping, sowform, request,
				response);

		return (forward);
	}

	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SOWForm sowform = (SOWForm) form;
		int status_size = 0;
		Map<String, Object> map;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");
		if (sowform.getAuthenticate().equals("")) {

			if (request.getParameter("type") != null) {
				if (request.getParameter("type").equals("prm_appendix")) {
					if (!Authenticationdao.getPageSecurity(loginuserid,
							"Manage Appendix", "View Status Report",
							getDataSource(request, "ilexnewDB"))) {
						return (mapping.findForward("UnAuthenticate"));
					}
				}
			}

		}
		/* Page Security End */
		ArrayList managestatuslist = new ArrayList();

		if (request.getParameter("id") != null) {
			sowform.setId(request.getParameter("id"));
		}

		if (request.getParameter("type") != null) {
			sowform.setType(request.getParameter("type"));
		}

		if (request.getParameter("ref") != null) {
			sowform.setRef(request.getParameter("ref"));
		}

		if (request.getParameter("viewjobtype") != null) {
			sowform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			sowform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			sowform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			sowform.setOwnerId("%");
		}

		managestatuslist = Appendixdao.getStatusreport(
				getDataSource(request, "ilexnewDB"), sowform.getId(),
				sowform.getType());

		if (managestatuslist.size() > 0) {
			status_size = managestatuslist.size();
		}

		request.setAttribute("managestatuslist", managestatuslist);
		request.setAttribute("Size", status_size + "");

		if (sowform.getType().equalsIgnoreCase("prm_appendix")) {
			sowform.setAppindixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), sowform.getId()));

		}
		if (sowform.getType().equalsIgnoreCase("prm_job")) {
			sowform.setJobName(Jobdao.getJobname(sowform.getId(),
					getDataSource(request, "ilexnewDB")));
			if (sowform.getId() != null
					&& !sowform.getId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(sowform.getId(), loginuserid,
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
				if (logger.isDebugEnabled()) {
					logger.debug("view(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - type ------"
							+ sowform.getType());
				}
			}
		}

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(sowform.getId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				sowform.getId(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		sowform.setMsaId(IMdao.getMSAId(sowform.getId(),
				getDataSource(request, "ilexnewDB")));
		request.setAttribute("viewjobtype", sowform.getViewjobtype());
		request.setAttribute("ownerId", sowform.getOwnerId());
		request.setAttribute("msaId", sowform.getMsaId());
		request.setAttribute("appendixid", sowform.getId());

		ActionForward forward = setViewSelector(mapping, sowform, request,
				response);

		return (forward);
	}

	public ActionForward Submit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SOWForm sowform = (SOWForm) form;
		String type = "";
		String Id = "";
		int status_size = 0;
		Sow sow = null;
		String loginuserid = (String) request.getSession(false).getAttribute(
				"userid");

		Id = sowform.getId();
		type = sowform.getType();
		Map<String, Object> map;

		int checklength = 0;

		if (sowform.getCheck() != null) {
			checklength = sowform.getCheck().length;
		}

		int[] index = new int[checklength];
		int k = 0;

		if (checklength > 0) {
			int length = sowform.getSow_Id().length;
			for (int i = 0; i < checklength; i++) {
				for (int j = 0; j < length; j++) {
					if (sowform.getCheck(i).equals(sowform.getSow_Id(j))) {
						index[k] = j;
						k++;
					}
				}
			}
		}

		if (checklength > 0) {
			for (int i = 0; i < index.length; i++) {
				sow = new Sow();

				sow.setSow_Id(sowform.getSow_Id(index[i]));
				sow.setSow(sowform.getSow(index[i]));
				sow.setType(type);

				int updateflag = ActivityLibrarydao.Updatesow(sow,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("updateflag", updateflag + "");
			}
		}

		if (sowform.getNewsow_Id() != null) {
			try {
				sow = new Sow();

				sow.setId(sowform.getId());
				sow.setSow(sowform.getNewsow());

				sow.setType(sowform.getType());

				int addflag = ActivityLibrarydao.addsow(sow,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("addflag", addflag + "");
			} catch (Exception e) {
				logger.error(
						"Submit(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("Submit(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Error Occured"
							+ e);
				}
			}

		}
		sowform.reset(mapping, request);

		sowform.setId(Id);
		sowform.setType(type);
		sowform.setRef("update");

		ArrayList managestatuslist = Appendixdao.getStatusreport(
				getDataSource(request, "ilexnewDB"), sowform.getId(),
				sowform.getType());

		if (managestatuslist.size() > 0) {
			status_size = managestatuslist.size();
		}

		request.setAttribute("managestatuslist", managestatuslist);
		request.setAttribute("Size", "" + status_size);

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(sowform.getId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				sowform.getId(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		sowform.setMsaId(IMdao.getMSAId(sowform.getId(),
				getDataSource(request, "ilexnewDB")));
		request.setAttribute("viewjobtype", sowform.getViewjobtype());
		request.setAttribute("ownerId", sowform.getOwnerId());
		request.setAttribute("msaId", sowform.getMsaId());
		request.setAttribute("appendixid", sowform.getId());

		setViewSelector(mapping, sowform, request, response);
		if (sowform.getType() != null
				&& sowform.getType().trim().equalsIgnoreCase("prm_job")) {
			if (sowform.getId() != null
					&& !sowform.getId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(sowform.getId(), loginuserid,
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}
		return (mapping.findForward("managestatuspage"));
	}

	public ActionForward Delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SOWForm sowform = (SOWForm) form;
		String type = "";
		String Id = "";

		int status_size = 0;
		Sow sow = null;
		String loginuserid = (String) request.getSession(false).getAttribute(
				"userid");

		Id = sowform.getId();
		type = sowform.getType();
		Map<String, Object> map;

		int checklength = 0;

		if (sowform.getCheck() != null) {
			checklength = sowform.getCheck().length;
		}

		int[] index = new int[checklength];
		int k = 0;

		if (checklength > 0) {
			int length = sowform.getSow_Id().length;
			for (int i = 0; i < checklength; i++) {
				for (int j = 0; j < length; j++) {
					if (sowform.getCheck(i).equals(sowform.getSow_Id(j))) {
						index[k] = j;
						k++;
					}
				}
			}
		}

		if (checklength > 0) {
			for (int i = 0; i < index.length; i++) {
				sow = new Sow();

				sow.setSow_Id(sowform.getSow_Id(index[i]));
				sow.setSow(sowform.getSow(index[i]));
				sow.setType(type);

				int deleteflag = ActivityLibrarydao.Deletesow(sow,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("deleteflag", deleteflag + "");
			}
		}

		sowform.reset(mapping, request);

		sowform.setId(Id);
		sowform.setType(type);
		sowform.setRef("update");

		ArrayList managestatuslist = Appendixdao.getStatusreport(
				getDataSource(request, "ilexnewDB"), sowform.getId(),
				sowform.getType());

		if (managestatuslist.size() > 0) {
			status_size = managestatuslist.size();
		}

		request.setAttribute("managestatuslist", managestatuslist);
		request.setAttribute("Size", "" + status_size);

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(sowform.getId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				sowform.getId(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		sowform.setMsaId(IMdao.getMSAId(sowform.getId(),
				getDataSource(request, "ilexnewDB")));
		request.setAttribute("viewjobtype", sowform.getViewjobtype());
		request.setAttribute("ownerId", sowform.getOwnerId());
		request.setAttribute("msaId", sowform.getMsaId());
		request.setAttribute("appendixid", sowform.getId());

		ActionForward forward = setViewSelector(mapping, sowform, request,
				response);
		if (sowform.getType() != null
				&& sowform.getType().trim().equalsIgnoreCase("prm_job")) {
			if (sowform.getId() != null
					&& !sowform.getId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(sowform.getId(), loginuserid,
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}

		return (forward);

	}

	public ActionForward setViewSelector(ActionMapping mapping,
			SOWForm sowform, HttpServletRequest request,
			HttpServletResponse response) {
		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		/* Start : Appendix Dashboard View Selector Code */

		if (sowform.getJobOwnerOtherCheck() != null
				&& !sowform.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					sowform.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", sowform.getId());
			request.setAttribute("msaId", sowform.getMsaId());

			return mapping.findForward("temppage");

		}

		if (sowform.getJobSelectedStatus() != null) {
			for (int i = 0; i < sowform.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ sowform.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ sowform.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (sowform.getJobSelectedOwners() != null) {
			for (int j = 0; j < sowform.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ sowform.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ sowform.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame", sowform.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					sowform.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", sowform.getId());
			request.setAttribute("msaId", sowform.getMsaId());

			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				sowform.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				sowform.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				sowform.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				sowform.setJobMonthWeekCheck(session.getAttribute("timeFrame")
						.toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				sowform.getJobOwnerOtherCheck(), "prj_job_new",
				sowform.getId(), getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */
		return (mapping.findForward("managestatuspage"));

	}

}
