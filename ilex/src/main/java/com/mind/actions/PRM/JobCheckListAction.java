package com.mind.actions.PRM;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.formbean.PRM.JobCheckListForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class JobCheckListAction extends com.mind.common.IlexDispatchAction {
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		JobCheckListForm jobCheckListForm = (JobCheckListForm) form;
		jobCheckListForm.setAppendixId(request.getParameter("appendixid"));
		Map<String, Object> map;
		map = DatabaseUtilityDao.setAppendixMenu(
				jobCheckListForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		return mapping.findForward("jobchecklist");
	}
}
