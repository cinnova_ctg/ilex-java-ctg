package com.mind.actions.PRM;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.prm.JobNotesBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.JobNotesDao;
import com.mind.formbean.PRM.JobNotesForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class JobNotesAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobNotesAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		JobNotesForm jobNotesForm = (JobNotesForm) form;
		JobNotesDao jobNotesDao = new JobNotesDao();
		JobNotesBean jobNotesBean = new JobNotesBean();
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - appendix"
					+ request.getParameter("appendixid"));
		}
		jobNotesForm.setAppendixId(request.getParameter("appendixid"));

		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String userId = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (jobNotesForm.getJobId() != null
				&& !jobNotesForm.getJobId().trim().equalsIgnoreCase("")) {
			if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
					"Job Level - Job Notes",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate")); // - Check for
																// Pager
																// Security.
			}
		} else {
			if (!Authenticationdao.getPageSecurity(loginuserid,
					"Manage Appendix", "Project Level - Job Notes",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate")); // - Check for
																// Pager
																// Security.
			}
		}
		/* Page Security=: End */

		jobNotesForm.setJobId(request.getParameter("jobId"));
		Map<String, Object> map;
		try {
			BeanUtils.copyProperties(jobNotesBean, jobNotesForm);
		} catch (IllegalAccessException e1) {
			logger.error(e1);

			e1.printStackTrace();
		} catch (InvocationTargetException e1) {

			logger.error(e1);
			e1.printStackTrace();
		}
		if (jobNotesForm.getJobId() != null
				&& !jobNotesForm.getJobId().trim().equalsIgnoreCase("")) {
			JobNotesDao.getJobInformation(jobNotesForm.getJobId(),
					jobNotesBean, getDataSource(request, "ilexnewDB"));

			jobNotesDao.fetchJobNotes(jobNotesBean,
					getDataSource(request, "ilexnewDB"));
			try {
				BeanUtils.copyProperties(jobNotesForm, jobNotesBean);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			if (jobNotesForm.getJobId() != null
					&& !jobNotesForm.getJobId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(jobNotesForm.getJobId(),
						userId, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		} else {
			boolean isNetMedX = JobNotesDao.isNetMedx(
					jobNotesForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			if (isNetMedX) {
				request.setAttribute("isNetMedX", "isNetMedX");
				map = DatabaseUtilityDao.setNetMedXMenu(
						jobNotesForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
				request.setAttribute("loginUserId", request.getSession(false)
						.getAttribute("userid"));
			} else {
				map = DatabaseUtilityDao.setAppendixMenu(
						jobNotesForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
			jobNotesDao.fetchProjectNotes(jobNotesBean,
					getDataSource(request, "ilexnewDB"));
			try {
				BeanUtils.copyProperties(jobNotesForm, jobNotesBean);
			} catch (IllegalAccessException e) {
				logger.error(e);// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}

		}

		return mapping.findForward("jobnote");
	}

	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		JobNotesForm jobNotesForm = (JobNotesForm) form;
		JobNotesBean jobNotesBean = new JobNotesBean();

		JobNotesDao jobNotesDao = new JobNotesDao();
		String userId = (String) request.getSession(false).getAttribute(
				"userid");
		String userName = (String) request.getSession(false).getAttribute(
				"username");
		Map<String, Object> map;
		try {
			BeanUtils.copyProperties(jobNotesBean, jobNotesForm);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (jobNotesForm.getJobId() != null
				&& !jobNotesForm.getJobId().trim().equalsIgnoreCase("")) {
			jobNotesDao.manipulateJobNotes(jobNotesBean,
					getDataSource(request, "ilexnewDB"), userId);
			JobNotesDao.getJobInformation(jobNotesForm.getJobId(),
					jobNotesBean, getDataSource(request, "ilexnewDB"));
			try {
				BeanUtils.copyProperties(jobNotesForm, jobNotesBean);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			if (jobNotesForm.getJobId() != null
					&& !jobNotesForm.getJobId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(jobNotesForm.getJobId(),
						userId, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);

			}
		} else {
			jobNotesDao.manipulateNotes(jobNotesBean,
					getDataSource(request, "ilexnewDB"), userId);
			boolean isNetMedX = JobNotesDao.isNetMedx(
					jobNotesForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			if (isNetMedX) {
				request.setAttribute("isNetMedX", "isNetMedX");
				map = DatabaseUtilityDao.setNetMedXMenu(
						jobNotesForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
				request.setAttribute("loginUserId", request.getSession(false)
						.getAttribute("userid"));
			} else {
				map = DatabaseUtilityDao.setAppendixMenu(
						jobNotesForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}

		return mapping.findForward("jobnote");
	}

}
