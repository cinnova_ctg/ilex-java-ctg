package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.CustomerInformationdao;
import com.mind.formbean.PRM.CustomerInformationForm;

public class CustomerInformationAction extends com.mind.common.IlexDispatchAction{

	public ActionForward add (ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		CustomerInformationForm customerInformationForm = (CustomerInformationForm) form;
		
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if( customerInformationForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Job" , "Edit Customer Information" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		int flag=0;
		int statusvalue = -1;
		String typeid="";
		String type="";
		
		String addmessage="";
		String appendixname="";
		String appendixid="";
		String jobname="";
		
		if( request.getParameter( "from" ) != null )
			customerInformationForm.setFrom( request.getParameter( "from" ) );
		
		if(request.getParameter("nettype") != null)  { 
			customerInformationForm.setNettype(""+request.getParameter("nettype")); 
		}
		
		if(request.getParameter("viewjobtype") != null)  { 
			customerInformationForm.setViewjobtype(""+request.getParameter("viewjobtype")); 
		}
		else
		{
			customerInformationForm.setViewjobtype("A");
		}
		request.setAttribute( "viewjobtype" , customerInformationForm.getViewjobtype() );
		
		if(request.getParameter("ownerId") != null)  { 
			customerInformationForm.setOwnerId(""+request.getParameter("ownerId")); 
		}
		else
		{
			customerInformationForm.setOwnerId("%");
		}
		
		String[] custparalist = new String[30];
		
				
		if(customerInformationForm.getSave()!=null)
		{
			typeid =customerInformationForm.getTypeid() ;
			appendixid =customerInformationForm.getAppendixid() ;
			type = customerInformationForm.getType();
			
			appendixname=CustomerInformationdao.getAppendixname(appendixid,getDataSource( request,"ilexnewDB" ));
			customerInformationForm.setAppendixname(appendixname);
			jobname=CustomerInformationdao.getJobname(typeid,getDataSource( request,"ilexnewDB" ));
			customerInformationForm.setJobname(jobname);
			
			
			String parameterstring ="";
			String valuestring  = "";
			
			
			if(customerInformationForm.getAppendixcustinfoid1()!=null)
				 parameterstring = parameterstring+customerInformationForm.getAppendixcustinfoid1();
				
				if(customerInformationForm.getAppendixcustinfoid2()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid2();
				if(customerInformationForm.getAppendixcustinfoid3()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid3();
			 //start
				if(customerInformationForm.getAppendixcustinfoid4()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid4();
				if(customerInformationForm.getAppendixcustinfoid5()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid5();
				if(customerInformationForm.getAppendixcustinfoid6()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid6();
				if(customerInformationForm.getAppendixcustinfoid7()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid7();
				if(customerInformationForm.getAppendixcustinfoid8()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid8();
				if(customerInformationForm.getAppendixcustinfoid9()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid9();
				if(customerInformationForm.getAppendixcustinfoid10()!=null)
					 parameterstring = parameterstring+"~"+customerInformationForm.getAppendixcustinfoid10();
			//over	
			if(customerInformationForm.getCustvalue1()!=null)
				valuestring = valuestring+customerInformationForm.getCustvalue1();
				
			if(customerInformationForm.getCustvalue2()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue2();
			if(customerInformationForm.getCustvalue3()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue3();
			//start
			
			if(customerInformationForm.getCustvalue4()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue4();
			if(customerInformationForm.getCustvalue5()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue5();
			if(customerInformationForm.getCustvalue6()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue6();
			if(customerInformationForm.getCustvalue7()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue7();
			if(customerInformationForm.getCustvalue8()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue8();
			if(customerInformationForm.getCustvalue9()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue9();
			if(customerInformationForm.getCustvalue10()!=null)
				valuestring = valuestring+"~"+customerInformationForm.getCustvalue10();
			//over

			statusvalue=CustomerInformationdao.addCustInfo(typeid,parameterstring,valuestring,userid,getDataSource( request,"ilexnewDB" ));
			request.setAttribute("addmessage",statusvalue+"");
			
			if(statusvalue==0)
			{
				flag=1;
				String temppath="PRMJobCustomerAppendixInfo";
				request.setAttribute( "type" , temppath );
				request.setAttribute( "appendix_Id" , customerInformationForm.getAppendixid() );
				
				if( customerInformationForm.getFrom().equals( "jobdashboard" ) )
				{
					request.setAttribute( "jobid" , customerInformationForm.getTypeid() );
					return( mapping.findForward( "jobdashboard" ) );
				}
				else
				{
					return( mapping.findForward( "temppage" ) );
				}				
			}
			else
			{
				forward = mapping.findForward("success");
			}
			
		}
		else
		{
			
			typeid = request.getParameter("typeid");
			type = request.getParameter("type");
			appendixid = request.getParameter("appendixid");
			customerInformationForm.setAppendixid(appendixid);
			appendixname = CustomerInformationdao.getAppendixname(appendixid,getDataSource( request,"ilexnewDB" ));
			customerInformationForm.setAppendixname(appendixname);
			jobname = CustomerInformationdao.getJobname(typeid,getDataSource( request,"ilexnewDB" ));
			customerInformationForm.setJobname(jobname);
			custparalist=CustomerInformationdao.getCustparameterlist(appendixid,typeid,getDataSource( request,"ilexnewDB" ));
			
			customerInformationForm.setAppendixcustinfoid1(custparalist[0]);
			customerInformationForm.setCustParameter1(custparalist[1]);
			customerInformationForm.setCustvalue1(custparalist[2]);
			customerInformationForm.setAppendixcustinfoid2(custparalist[3]);
			customerInformationForm.setCustParameter2(custparalist[4]);
			customerInformationForm.setCustvalue2(custparalist[5]);
			
			customerInformationForm.setAppendixcustinfoid3(custparalist[6]);
			customerInformationForm.setCustParameter3(custparalist[7]);
			customerInformationForm.setCustvalue3(custparalist[8]);
	//start
			customerInformationForm.setAppendixcustinfoid4(custparalist[9]);
			customerInformationForm.setCustParameter4(custparalist[10]);
			customerInformationForm.setCustvalue4(custparalist[11]);
			customerInformationForm.setAppendixcustinfoid5(custparalist[12]);
			customerInformationForm.setCustParameter5(custparalist[13]);
			customerInformationForm.setCustvalue5(custparalist[14]);
			customerInformationForm.setAppendixcustinfoid6(custparalist[15]);
			customerInformationForm.setCustParameter6(custparalist[16]);
			customerInformationForm.setCustvalue6(custparalist[17]);
			customerInformationForm.setAppendixcustinfoid7(custparalist[18]);
			customerInformationForm.setCustParameter7(custparalist[19]);
			customerInformationForm.setCustvalue7(custparalist[20]);
			customerInformationForm.setAppendixcustinfoid8(custparalist[21]);
			customerInformationForm.setCustParameter8(custparalist[22]);
			customerInformationForm.setCustvalue8(custparalist[23]);
			customerInformationForm.setAppendixcustinfoid9(custparalist[24]);
			customerInformationForm.setCustParameter9(custparalist[25]);
			customerInformationForm.setCustvalue9(custparalist[26]);
			customerInformationForm.setAppendixcustinfoid10(custparalist[27]);
			customerInformationForm.setCustParameter10(custparalist[28]);
			customerInformationForm.setCustvalue10(custparalist[29]);
			
			/* For Snapon: Start */
			boolean checkSnapon = Appendixdao.checkForSnapon(appendixid, getDataSource( request,"ilexnewDB" ));
			if(checkSnapon){
				
				/* Copy texboxes value to other bean variables for Snapon */
				customerInformationForm.setSnCustvalue1( customerInformationForm.getCustvalue1() );
				customerInformationForm.setSnCustvalue2( customerInformationForm.getCustvalue2() );
				customerInformationForm.setSnCustvalue3( customerInformationForm.getCustvalue3() );
				customerInformationForm.setSnCustvalue4( customerInformationForm.getCustvalue4() );
				customerInformationForm.setSnCustvalue5( customerInformationForm.getCustvalue5() );
				customerInformationForm.setSnCustvalue6( customerInformationForm.getCustvalue6() );
				customerInformationForm.setSnCustvalue7( customerInformationForm.getCustvalue7() );
				customerInformationForm.setSnCustvalue8( customerInformationForm.getCustvalue8() );
				customerInformationForm.setSnCustvalue9( customerInformationForm.getCustvalue9() );
				customerInformationForm.setSnCustvalue10( customerInformationForm.getCustvalue10() );
				
				/* Set all texbox default to blank */
				customerInformationForm.setCustvalue1("");
				customerInformationForm.setCustvalue2("");
				customerInformationForm.setCustvalue3("");
				customerInformationForm.setCustvalue4("");
				customerInformationForm.setCustvalue5("");
				customerInformationForm.setCustvalue6("");
				customerInformationForm.setCustvalue7("");
				customerInformationForm.setCustvalue8("");
				customerInformationForm.setCustvalue9("");
				customerInformationForm.setCustvalue10("");
			}
			/* For Snapon: End */
	//end
			request.setAttribute("checkSnapon",checkSnapon);
			
			forward = mapping.findForward("success");
		
		}
		
		request.setAttribute( "typeid" , typeid);
		request.setAttribute( "appendixid" , appendixid);
		request.setAttribute( "type" , type );
		request.setAttribute( "viewflag" , flag+"" );	
		//forward = mapping.findForward("success");
		return (forward);
	}
	
	
	
	
}
