package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.AddComment;
import com.mind.common.dao.Menu;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.formbean.PRM.AddEndCustForm;



public class AddEndCustAction extends com.mind.common.IlexAction
{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		ActionForward forward = new ActionForward();
		AddEndCustForm addendcustform = ( AddEndCustForm ) form;
		DataSource ds = null;
		ds = getDataSource(request, "ilexnewDB");
		
//		Variables added for the View Selector Functioning.
		String opendiv=null;
		ArrayList statusList  = new ArrayList();
		ArrayList ownerList  = new ArrayList();
		String selectedStatus ="";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType= "msa";
		String msaid = "0";
		int ownerListSize = 0; 
		
		
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		String Id = null;
		String Type = null;
			
		if( request.getParameter( "Type" ) != null )
		{
			addendcustform.setFromPage(request.getParameter( "Type" ));
		}
		
		if( request.getParameter( "type" ) != null )
		{
			addendcustform.setType(request.getParameter( "type" ));
		}
		
		if( request.getParameter( "typeid" ) != null )
		{
			addendcustform.setTypeId(request.getParameter( "typeid" ));
		}
		
		if( request.getParameter( "viewjobtype" ) != null )
		{
			addendcustform.setViewjobtype(request.getParameter( "viewjobtype" ));
		}
		
		if( request.getParameter( "ownerId" ) != null )
		{
			addendcustform.setOwnerId(request.getParameter( "ownerId" ));
		}
		
		if( request.getParameter( "from" ) != null )
		{
			addendcustform.setFrom(request.getParameter( "from" ));
		}
		
		request.setAttribute("from", addendcustform.getFrom());
		
		
		if( request.getParameter( "appendixid" ) != null )
		{
			addendcustform.setAppendixid(request.getParameter( "appendixid" ));
			
		}
		if(request.getParameter("opendiv")!=null){
			opendiv=request.getParameter("opendiv");
			request.setAttribute("opendiv",opendiv);
		}
		
		if( addendcustform.getType().equals( "Appendix" ))
		{
			addendcustform.setName( Jobdao.getAppendixname( getDataSource( request , "ilexnewDB" ) , addendcustform.getTypeId() ) );
			addendcustform.setMsaid(IMdao.getMSAId(addendcustform.getTypeId(), getDataSource(request,"ilexnewDB")));
			addendcustform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(ds,IMdao.getMSAId(addendcustform.getTypeId(), ds)));
		}
		
//for Menu and View selector: Start	
		if(request.getParameter( "Type" ) != null && addendcustform.getFromPage().equals( "Appendix" ))
		{
			String abc = null; 
			addendcustform.setName( Jobdao.getAppendixname( getDataSource( request , "ilexnewDB" ) , addendcustform.getTypeId() ) );
			addendcustform.setMsaid(IMdao.getMSAId(addendcustform.getTypeId(), getDataSource(request,"ilexnewDB")));
			addendcustform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(ds,IMdao.getMSAId(addendcustform.getTypeId(), ds)));
			ownerType= "appendix";
			if(request.getParameter("home")!=null)   // This is called when reset button is pressed from the view selector.                       
			{
				addendcustform.setAppendixSelectedStatus(null);
				addendcustform.setAppendixSelectedOwners(null);
				addendcustform.setPrevSelectedStatus("");
				addendcustform.setPrevSelectedOwner("");
				addendcustform.setAppendixOtherCheck(null);
				addendcustform.setMonthWeekCheck(null);
				
				request.setAttribute( "MSA_Id" , addendcustform.getMsaid());
				return( mapping.findForward( "appendixlistpage" ) );
				
			}	
				
			//These two parameter month/week are checked are kept in session.  ( View Images)
			if(request.getParameter("month")!=null){        
				session.setAttribute("monthAppendix",request.getParameter("month").toString());
				
				session.removeAttribute("weekAppendix");
				addendcustform.setMonthWeekCheck("month");
			}
			
			if(request.getParameter("week")!=null){
				session.setAttribute("weekAppendix",request.getParameter("week").toString());
				
				session.removeAttribute("monthAppendix");
				addendcustform.setMonthWeekCheck("week");
			}
			
			if(addendcustform.getAppendixOtherCheck()!=null)  //OtherCheck box status is kept in session.   
				session.setAttribute("appendixOtherCheck",addendcustform.getAppendixOtherCheck().toString());
			
//			When Status Check Boxes are Clicked :::::::::::::::::
			if(addendcustform.getAppendixSelectedStatus()!=null){
				for(int i=0;i<addendcustform.getAppendixSelectedStatus().length;i++)
				{
					selectedStatus = selectedStatus + "," + "'" + addendcustform.getAppendixSelectedStatus(i) + "'" ;
					tempStatus = tempStatus + "," + addendcustform.getAppendixSelectedStatus(i);
				}
				
			selectedStatus = selectedStatus.substring(1, selectedStatus.length());
			selectedStatus = "(" +selectedStatus + ")";		
			}
			
			//When Owner Check Boxes are Clicked :::::::::::::::::
			if(addendcustform.getAppendixSelectedOwners() != null){
				for(int j = 0; j<addendcustform.getAppendixSelectedOwners().length; j++)
				{
					selectedOwner = selectedOwner + "," + addendcustform.getAppendixSelectedOwners(j); 
					tempOwner = tempOwner + "," + addendcustform.getAppendixSelectedOwners(j);
				}
				selectedOwner = selectedOwner.substring(1, selectedOwner.length());
				if(selectedOwner.equals("0")){
					chkOtherOwner = true;
					selectedOwner = "%";
				}
				selectedOwner = "(" +selectedOwner + ")";
			}
			
			
			if(addendcustform.getGo()!= null && !addendcustform.getGo().equals(""))
			{
					session.setAttribute("appendixSelectedOwners",selectedOwner);
					session.setAttribute("appSelectedOwners",selectedOwner);
					session.setAttribute("appendixTempOwner",tempOwner);	
					session.setAttribute("appendixSelectedStatus",selectedStatus);
					session.setAttribute("appSelectedStatus",selectedStatus);
					session.setAttribute("appendixTempStatus",tempStatus);
					
					if(addendcustform.getMonthWeekCheck()!=null)
					{
						if(addendcustform.getMonthWeekCheck().equals("week")){
							session.removeAttribute("monthAppendix");
							session.setAttribute("weekAppendix","0");
							
						}
						if(addendcustform.getMonthWeekCheck().equals("month")){
							session.removeAttribute("weekAppendix");
							session.setAttribute("monthAppendix","0");
							
						}
						if(addendcustform.getMonthWeekCheck().equals("clear")){
							session.removeAttribute("monthAppendix");
							session.removeAttribute("weekAppendix");
						}
							
					}
					request.setAttribute( "MSA_Id" , addendcustform.getMsaid());
					return( mapping.findForward( "appendixlistpage" ) );
			}	
			else
				{
						if(session.getAttribute("appendixSelectedOwners")!=null) {
							selectedOwner = session.getAttribute("appendixSelectedOwners").toString();
							addendcustform.setAppendixSelectedOwners(session.getAttribute("appendixTempOwner").toString().split(","));
						}
						if(session.getAttribute("appendixSelectedStatus")!=null) {
							selectedStatus = session.getAttribute("appendixSelectedStatus").toString();
							addendcustform.setAppendixSelectedStatus(session.getAttribute("appendixTempStatus").toString().split(","));
						}
						if(session.getAttribute("appendixOtherCheck")!=null)
							addendcustform.setAppendixOtherCheck(session.getAttribute("appendixOtherCheck").toString());
						
						if(session.getAttribute("monthAppendix")!=null){
							session.removeAttribute("weekAppendix");
							addendcustform.setMonthWeekCheck("month");
						}
						if(session.getAttribute("weekAppendix")!=null){
							session.removeAttribute("monthAppendix");
							addendcustform.setMonthWeekCheck("week");
						}
						
				}
					if(!chkOtherOwner) {
						addendcustform.setPrevSelectedStatus(selectedStatus);
						addendcustform.setPrevSelectedOwner(selectedOwner);
					}     
					ownerList  = MSAdao.getOwnerList(loginuserid, addendcustform.getAppendixOtherCheck(), ownerType,addendcustform.getMsaid(), getDataSource(request, "ilexnewDB"));
					statusList = MSAdao.getStatusList("pm_appendix",getDataSource(request, "ilexnewDB"));
					
					if(ownerList.size() > 0)
						ownerListSize = ownerList.size();
					
					request.setAttribute("clickShow", request.getParameter("clickShow"));
					request.setAttribute("statuslist", statusList);
					request.setAttribute("ownerlist", ownerList);	
					request.setAttribute("ownerListSize", ownerListSize+"");
			
			String appendixtype = Appendixdao.getAppendixPrType(addendcustform.getMsaid(),addendcustform.getTypeId(),getDataSource(request,"ilexnewDB"));
//			request.setAttribute( "changestatusflag" , "true" );
			request.setAttribute( "MSA_Id" , addendcustform.getMsaid() );
			request.setAttribute( "Appendix_Id" , addendcustform.getTypeId() );
			request.setAttribute( "appendixType" , appendixtype);
			
			String appendixStatus = com.mind.dao.PM.Appendixdao.getAppendixStatus( addendcustform.getTypeId() ,addendcustform.getMsaid(), getDataSource( request , "ilexnewDB" ) );
			String addendumlist = com.mind.dao.PM.Appendixdao.getAddendumsIdName( addendcustform.getTypeId() , getDataSource( request , "ilexnewDB" )  );
			String addendumid = com.mind.dao.PM.Addendumdao.getLatestAddendumId(addendcustform.getTypeId() , getDataSource( request , "ilexnewDB" ) );
			String list = Menu.getStatus( "pm_appendix" , appendixStatus.charAt( 0 ) , addendcustform.getMsaid() , addendcustform.getTypeId(), "" , "" , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "appendixStatusList" , list );
			request.setAttribute("appendixStatus",appendixStatus);
			request.setAttribute( "latestaddendumid" , addendumid );
			request.setAttribute( "addendumlist" , addendumlist );
			
		}
//for Menu and View selector: End
		
		if( addendcustform.getAdd() != null ) {
			
			addendcustform.setUpdatemessage(""+AddComment.addEndCustomer( addendcustform.getTypeId() , addendcustform.getType() , addendcustform.getEndcust() , loginuserid , getDataSource( request , "ilexnewDB" ) ));
			
			if(addendcustform.getFromPage().equals("Appendix")){
				request.setAttribute( "Appendix_Id" ,addendcustform.getTypeId());
				request.setAttribute( "MSA_Id" , IMdao.getMSAId(addendcustform.getTypeId(), getDataSource(request,"ilexnewDB")));
				forward = mapping.findForward("appendixdetailpage");
			}
			else{
				request.setAttribute("path", "endcust");
				request.setAttribute("typeId", addendcustform.getTypeId());
				request.setAttribute("type", addendcustform.getType());
				request.setAttribute("viewjobtype", addendcustform.getViewjobtype());
				request.setAttribute("ownerId", addendcustform.getOwnerId());
				request.setAttribute("appendix_Id", addendcustform.getAppendixid());
				forward = mapping.findForward("temppage");
			}
			return (forward);
		}

		
		if(addendcustform.getTypeId() != null){
			addendcustform.setEndcust( AddComment.getEndCustomer( addendcustform.getTypeId() , addendcustform.getType(), getDataSource( request , "ilexnewDB" ) ));
			
			if(addendcustform.getEndcust()!=null)
			{
				
				if(addendcustform.getEndcust().equals(addendcustform.getMsaname()))
					addendcustform.setCheck("on");
				
			}
			else
			{	
				addendcustform.setEndcust(addendcustform.getMsaname());
				addendcustform.setCheck("on");
			}
		}
		
		
		forward = mapping.findForward("success");
		return (forward);
		}
}



