package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboPRM;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.ReportScheduleWindowdao;
import com.mind.formbean.PRM.ReportScheduleWindowForm;
import com.mind.util.WebUtil;

public class SummaryWindowAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ReportScheduleWindowForm reportScheduleWindowForm = (ReportScheduleWindowForm) form;
		ActionForward forward = new ActionForward();
		ArrayList accountlist = new ArrayList();
		int accoutsummarysize = 0;

		/*** Code added for the View Selecotr ***/
		ArrayList jobStatusList = new ArrayList();
		ArrayList jobOwnerList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		String[] jobSelectedStatus1 = null;
		String[] jobSelectedOwners = null;
		jobSelectedOwners = request.getParameterValues("jobSelectedOwners");
		jobSelectedStatus1 = request.getParameterValues("jobSelectedStatus");

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");

		if (request.getParameter("nettype") != null) {
			reportScheduleWindowForm.setNettype(""
					+ request.getParameter("nettype"));
		}
		if (request.getParameter("ownerId") != null) {
			reportScheduleWindowForm
					.setOwnerId(request.getParameter("ownerId"));
		}

		if (reportScheduleWindowForm.getSave() != null) {
			accountlist = ReportScheduleWindowdao.getAccountList(
					reportScheduleWindowForm.getMsa(),
					reportScheduleWindowForm.getStartdate(),
					reportScheduleWindowForm.getEnddate(),
					getDataSource(request, "ilexnewDB"));

			if (accountlist.size() > 0) {
				accoutsummarysize = accountlist.size();
			}
			request.setAttribute("accountlist", accountlist);
			request.setAttribute("list", "true");
		}

		DynamicComboPRM dcprm = new DynamicComboPRM();
		ArrayList msalist = new ArrayList();// for MSA combo
		/* MSA combo start */
		msalist = ReportScheduleWindowdao.getMsalist(getDataSource(request,
				"ilexnewDB"));

		msalist.set(0, new com.mind.common.LabelValue("ALL", "%"));

		dcprm.setMsalist(msalist);
		request.setAttribute("msacombo", dcprm);

		// NetMedX View Selector:Start
		/* View Selector Code */
		if (reportScheduleWindowForm.getJobOwnerOtherCheck() != null
				&& !reportScheduleWindowForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					reportScheduleWindowForm.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", "-1");
			request.setAttribute("viewjobtype", "ION");
			request.setAttribute("nettype", "dispatch");

			return mapping.findForward("temppage");

		}

		if (reportScheduleWindowForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < reportScheduleWindowForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ reportScheduleWindowForm.getJobSelectedStatus(i)
						+ "'";
				jobTempStatus = jobTempStatus + ","
						+ reportScheduleWindowForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (reportScheduleWindowForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < reportScheduleWindowForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ reportScheduleWindowForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ reportScheduleWindowForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {
			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					reportScheduleWindowForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					reportScheduleWindowForm.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", "-1");
			request.setAttribute("viewjobtype", "ION");
			request.setAttribute("nettype", "dispatch");
			request.setAttribute("ownerId",
					reportScheduleWindowForm.getOwnerId());

			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				reportScheduleWindowForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				reportScheduleWindowForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				reportScheduleWindowForm.setJobOwnerOtherCheck(session
						.getAttribute("jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				reportScheduleWindowForm.setJobMonthWeekCheck(session
						.getAttribute("timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				reportScheduleWindowForm.getJobOwnerOtherCheck(),
				"prj_job_new", "-1", getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* View Selector Code */
		// NetMedX View Selector:End

		forward = mapping.findForward("success");
		request.setAttribute("nettype", reportScheduleWindowForm.getNettype());
		request.setAttribute("ownerId", reportScheduleWindowForm.getOwnerId());
		request.setAttribute("summarysize", new Integer(accoutsummarysize));
		return (forward);

	}

}
