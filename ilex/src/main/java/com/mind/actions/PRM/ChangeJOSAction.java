package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.DynamicComboPRM;
import com.mind.bean.prm.ChangeJOSBean;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.ChangeJOSDao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.formbean.PRM.ChangeJOSForm;
import com.mind.util.WebUtil;

;

public class ChangeJOSAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce)
			throws Exception {

		ChangeJOSForm changeJOSForm = (ChangeJOSForm) form;
		ChangeJOSBean changeJOSBean = new ChangeJOSBean();
		DynamicComboPRM dynamicComboOwner = new DynamicComboPRM();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		ArrayList ownerList = new ArrayList();
		ArrayList schedulerList = new ArrayList();
		ArrayList SearchownerList = new ArrayList();
		ArrayList initialStateCategory = new ArrayList();
		ArrayList jobList = new ArrayList();
		ArrayList searchSchedulerList = new ArrayList();
		String tempCat = "";

		/* Variable for Appendix Dashboard View Selector: Start */
		ArrayList jobOwnerList = new ArrayList(); // - for list of Job Owner
		ArrayList jobStatusList = new ArrayList(); // - list of job status
		String jobSelectedStatus = ""; // - selected status
		String jobSelectedOwner = ""; // - selected owner
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		/* Variable for Appendix Dashboard View Selector: End */

		int countJobInAppendix = ChangeJOSDao.getJobCount(
				changeJOSForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		// System.out.println("in countJob"+countJobInAppendix);
		if (countJobInAppendix > 0) {
			request.setAttribute("Updatepage", countJobInAppendix);
		}
		BeanUtils.copyProperties(changeJOSBean, changeJOSForm);
		if (changeJOSForm.getSearch() != null) {

			jobList = ChangeJOSDao.getJobList(changeJOSBean,
					getDataSource(request, "ilexnewDB"));
			if (jobList.size() > 0) {
				request.setAttribute("jobList", jobList);
				request.setAttribute("joblistsize", jobList.size());
			} else {
				request.setAttribute("NoJobs", "NoJobsFound");
			}
		}
		String saveJobId = "";
		if (changeJOSForm.getUpdate() != null) {
			int retvalue = 0;
			if (changeJOSForm.getJobIdCheck() != null) {
				for (int i = 0; i < changeJOSForm.getJobIdCheck().length; i++) {
					saveJobId = saveJobId + changeJOSForm.getJobIdCheck(i)
							+ ",";
				}
				saveJobId = saveJobId.substring(0, saveJobId.length() - 1);
				retvalue = ChangeJOSDao.upadteJobOwnerScheduler(changeJOSBean,
						saveJobId, getDataSource(request, "ilexnewDB"));
			}
			request.setAttribute("Updated", retvalue);
		}

		SearchownerList = ChangeJOSDao.getJobOwnerList(
				changeJOSForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		schedulerList = ChangeJOSDao.getSchedulerList(
				changeJOSForm.getAppendixid(), "Scheduler",
				getDataSource(request, "ilexnewDB"));
		ownerList = ChangeJOSDao.getSchedulerList(
				changeJOSForm.getAppendixid(), "Job Owner",
				getDataSource(request, "ilexnewDB"));
		searchSchedulerList = ChangeJOSDao.getSchedulerList(
				changeJOSForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));

		if (ownerList.size() > 1) {
			dynamicComboOwner.setAllOwnerName(ownerList);
			request.setAttribute("dyComboPRMOwner", dynamicComboOwner);
		}
		// System.out.println("SIze  "+schedulerList.size());
		if (schedulerList.size() > 1) {
			// System.out.println(schedulerList.size());
			dynamicComboOwner.setSchedulerList(schedulerList);
			request.setAttribute("dyComboPRMScheduler", dynamicComboOwner);
		}

		if (request.getParameter("appendixid") != null) {
			changeJOSForm.setAppendixid((String) request
					.getParameter("appendixid"));
			changeJOSForm.setAppendixName(Appendixdao.getAppendixname(
					changeJOSForm.getAppendixid(),
					getDataSource(request, "ilexnewDB")));
			changeJOSForm.setMsaId(IMdao.getMSAId(
					changeJOSForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"))); // - for menu
			request.setAttribute("Appendix_Id", changeJOSForm.getAppendixid());
		}
		if (request.getParameter("ownerId") != null) // - for menu
		{
			changeJOSForm.setOwnerId(request.getParameter("ownerId"));
		}
		if (request.getParameter("viewjobtype") != null) // - for menu
		{
			changeJOSForm.setViewjobtype(request.getParameter("viewjobtype"));
		}

		DynamicComboPRM dyComboSearchOwner = new DynamicComboPRM();
		dyComboSearchOwner.setAllOwnerName(SearchownerList);
		request.setAttribute("dyComboSearchOwner", dyComboSearchOwner);

		dyComboSearchOwner.setSchedulerList(searchSchedulerList);
		request.setAttribute("dyComboSearchScheduler", dyComboSearchOwner);

		initialStateCategory = ChangeJOSDao.getJobStateList(getDataSource(
				request, "ilexnewDB"));

		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempCat = ((ChangeJOSBean) initialStateCategory.get(i))
						.getCountryId();
				((ChangeJOSBean) initialStateCategory.get(i))
						.setTempList(SiteManagedao
								.getCategoryWiseStateList(tempCat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}
		request.setAttribute("initialStateCategory", initialStateCategory);

		DynamicComboCM dcGroupList = new DynamicComboCM();
		dcGroupList.setFirstlevelcatglist(DynamicComboDao
				.getGroupNameListSiteSearch(changeJOSForm.getMsaId(),
						getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcGroupList", dcGroupList);

		DynamicComboCM dcStatusList = new DynamicComboCM();
		dcStatusList.setStatusList(ChangeJOSDao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcStatusList", dcStatusList);

		DynamicComboCM dcEndCustomerList = new DynamicComboCM();
		dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
				.getSiteEndCustomerList(getDataSource(request, "ilexnewDB"),
						changeJOSForm.getMsaId(), changeJOSForm.getMsaname()));
		request.setAttribute("dcEndCustomerList", dcEndCustomerList);

		request.setAttribute("appendixid", changeJOSForm.getAppendixid());

		/* For Menu & View Selector: Start */

		/* Start : Appendix Dashboard View Selector Code */

		if (changeJOSForm.getJobOwnerOtherCheck() != null
				&& !changeJOSForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					changeJOSForm.getJobOwnerOtherCheck());
		}
		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", changeJOSForm.getAppendixid());
			request.setAttribute("msaId", changeJOSForm.getMsaId());
			return mapping.findForward("temppage");
		}
		if (changeJOSForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < changeJOSForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ changeJOSForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ changeJOSForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}
		if (changeJOSForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < changeJOSForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ changeJOSForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ changeJOSForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}
		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					changeJOSForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					changeJOSForm.getJobOwnerOtherCheck());
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", changeJOSForm.getAppendixid());
			request.setAttribute("msaId", changeJOSForm.getMsaId());

			return mapping.findForward("temppage"); // - /common/Tempupload.jsp
		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				changeJOSForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				changeJOSForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				changeJOSForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				changeJOSForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}
		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				changeJOSForm.getJobOwnerOtherCheck(), "prj_job_new",
				changeJOSForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));
		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(changeJOSForm.getAppendixid(),
						getDataSource(request, "ilexnewDB")); // - for addendum
																// section of
																// menu
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				changeJOSForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")); // - Appendix current
														// status

		/* Set Parameter for menu and view selector */
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("contractDocMenu", contractDocoumentList);
		request.setAttribute("msaId", changeJOSForm.getMsaId());
		request.setAttribute("viewjobtype", changeJOSForm.getViewjobtype());
		request.setAttribute("ownerId", changeJOSForm.getOwnerId());
		request.setAttribute("appendixid", changeJOSForm.getAppendixid());

		/* For Menu & View Selector: End */

		return (mapping.findForward("success"));

	}

}
