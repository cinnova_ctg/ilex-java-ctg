package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.DynamicComboCNSWEB;
import com.mind.bean.prm.TicketDetailBean;
import com.mind.bean.prm.TicketDetailDTO;
import com.mind.common.LabelValue;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.OpenSearchWindowdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.Ticketdao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PRM.TicketDetailForm;
import com.mind.util.WebUtil;

public class TicketDetailAction extends com.mind.common.IlexAction {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        TicketDetailForm ticketdetailForm = (TicketDetailForm) form;
        TicketDetailDTO ticketdetailDTO = new TicketDetailDTO();

        ActionForward forward = new ActionForward();
        int retval = -1;
        int retval1 = -1;
        // String ticket_type = "";
        ArrayList criticalitycatlist = new ArrayList();
        ArrayList initialStateCategory = new ArrayList();
        ArrayList requestorlist = new ArrayList();
        ArrayList productcatlist = new ArrayList();
        String[] prod_list = null;
        String tempdepo_cat = "";
        String depo_product_id = "";
        String tempcat = "";
        String[] tempList = null;
        String appendixId = "";

        /* Start : Appendix Dashboard View Selector Code */
        ArrayList jobOwnerList = new ArrayList();
        ArrayList jobStatusList = new ArrayList();
        String jobSelectedStatus = "";
        String jobSelectedOwner = "";
        String jobTempStatus = "";
        String jobTempOwner = "";
        int jobOwnerListSize = 0;

        /* Page Security Start */
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }															// expired
        String loginuserid = (String) session.getAttribute("userid");
        if (ticketdetailForm.getAuthenticate().equals("")) {
            if (!Authenticationdao.getPageSecurity(loginuserid,
                    "Manage Project", "View Appendix Dashboard",
                    getDataSource(request, "ilexnewDB"))) {
                return (mapping.findForward("UnAuthenticate"));
            }
        }
        /* Page Security End */

        if (request.getParameter("appendixid") != null) {
            ticketdetailForm.setAppendixid(request.getParameter("appendixid"));
            appendixId = request.getParameter("appendixid");
        } else {
            ticketdetailForm.setAppendixid(ticketdetailForm.getAppendixid());
        }

        if (request.getParameter("jobid") != null) {
            ticketdetailForm.setJobid(request.getParameter("jobid"));
        }

        if (request.getParameter("ticket_type") != null) {
            ticketdetailForm.setTicket_type(""
                    + request.getParameter("ticket_type"));
        }

        if (ticketdetailForm.getTicket_type() != null) {
            ticketdetailForm.setTicket_type(ticketdetailForm.getTicket_type());
        } else {
            ticketdetailForm.setTicket_type("");
        }

        if (request.getParameter("nettype") != null) {
            ticketdetailForm.setNettype("" + request.getParameter("nettype"));
        }

        if (request.getParameter("viewjobtype") != null) {
            ticketdetailForm.setViewjobtype(""
                    + request.getParameter("viewjobtype"));
        } else {
            ticketdetailForm.setViewjobtype("A");
        }
        request.setAttribute("viewjobtype", ticketdetailForm.getViewjobtype());

        if (request.getParameter("ownerId") != null) {
            ticketdetailForm.setOwnerId("" + request.getParameter("ownerId"));
        } else {
            ticketdetailForm.setOwnerId("%");
        }

        if (request.getParameter("invoice_Flag") != null) {
            ticketdetailForm.setInvoice_Flag(request
                    .getParameter("invoice_Flag"));
        }
        if (request.getParameter("invoiceno") != null) {
            ticketdetailForm.setInvoiceno(request.getParameter("invoiceno"));
        }
        if (request.getParameter("partner_name") != null) {
            ticketdetailForm.setPartner_name(request
                    .getParameter("partner_name"));
        }
        if (request.getParameter("powo_number") != null) {
            ticketdetailForm
                    .setPowo_number(request.getParameter("powo_number"));
        }
        if (request.getParameter("from_date") != null) {
            ticketdetailForm.setFrom_date(request.getParameter("from_date"));
        }
        if (request.getParameter("to_date") != null) {
            ticketdetailForm.setTo_date(request.getParameter("to_date"));
        }

        ticketdetailForm.setAppendixname(Jobdao.getAppendixname(
                getDataSource(request, "ilexnewDB"),
                ticketdetailForm.getAppendixid()));

        // set msa id and name in form bean
        ticketdetailForm.setMsaid(IMdao.getMSAId(
                ticketdetailForm.getAppendixid(),
                getDataSource(request, "ilexnewDB")));

        if (request.getParameter("msaid") != null) {
            ticketdetailForm.setMsaid(request.getParameter("msaid"));
        }

        /* by hamid for rules start */
        ticketdetailForm.setRules(Ticketdao.getRules(
                ticketdetailForm.getMsaid(),
                getDataSource(request, "ilexnewDB")));
        /* by hamid for rules end */

        /* Start : Appendix Dashboard View Selector Code */
        if (ticketdetailForm.getJobOwnerOtherCheck() != null
                && !ticketdetailForm.getJobOwnerOtherCheck().equals("")) {
            session.setAttribute("jobOwnerOtherCheck",
                    ticketdetailForm.getJobOwnerOtherCheck());
        }

        if (request.getParameter("opendiv") != null) {
            request.setAttribute("opendiv", request.getParameter("opendiv"));
        }
        if (request.getParameter("resetList") != null
                && request.getParameter("resetList").equals("something")) {
            WebUtil.setDefaultAppendixDashboardAttribute(session);
            request.setAttribute("type", "AppendixViewSelector");
            request.setAttribute("appendixId", ticketdetailForm.getAppendixid());
            request.setAttribute("msaId", ticketdetailForm.getMsaid());
            return mapping.findForward("temppage");
        }

        if (ticketdetailForm.getJobSelectedStatus() != null) {
            for (int i = 0; i < ticketdetailForm.getJobSelectedStatus().length; i++) {
                jobSelectedStatus = jobSelectedStatus + "," + "'"
                        + ticketdetailForm.getJobSelectedStatus(i) + "'";
                jobTempStatus = jobTempStatus + ","
                        + ticketdetailForm.getJobSelectedStatus(i);
            }

            jobSelectedStatus = jobSelectedStatus.substring(1,
                    jobSelectedStatus.length());
            jobSelectedStatus = "(" + jobSelectedStatus + ")";
        }

        if (ticketdetailForm.getJobSelectedOwners() != null) {
            for (int j = 0; j < ticketdetailForm.getJobSelectedOwners().length; j++) {
                jobSelectedOwner = jobSelectedOwner + ","
                        + ticketdetailForm.getJobSelectedOwners(j);
                jobTempOwner = jobTempOwner + ","
                        + ticketdetailForm.getJobSelectedOwners(j);
            }
            jobSelectedOwner = jobSelectedOwner.substring(1,
                    jobSelectedOwner.length());
            if (jobSelectedOwner.equals("0")) {
                jobSelectedOwner = "%";
            }
            jobSelectedOwner = "(" + jobSelectedOwner + ")";
        }

        if (request.getParameter("showList") != null
                && request.getParameter("showList").equals("something")) {

            session.setAttribute("jobSelectedOwners", jobSelectedOwner);
            session.setAttribute("jobTempOwner", jobTempOwner);
            session.setAttribute("jobSelectedStatus", jobSelectedStatus);
            session.setAttribute("jobTempStatus", jobTempStatus);
            session.setAttribute("timeFrame",
                    ticketdetailForm.getJobMonthWeekCheck());
            session.setAttribute("jobOwnerOtherCheck",
                    ticketdetailForm.getJobOwnerOtherCheck());

            request.setAttribute("type", "AppendixViewSelector");
            request.setAttribute("appendixId", ticketdetailForm.getAppendixid());
            request.setAttribute("msaId", ticketdetailForm.getMsaid());
            return mapping.findForward("temppage");

        } else {
            if (session.getAttribute("jobSelectedOwners") != null) {
                jobSelectedOwner = session.getAttribute("jobSelectedOwners")
                        .toString();
                ticketdetailForm.setJobSelectedOwners(session
                        .getAttribute("jobTempOwner").toString().split(","));
            }
            if (session.getAttribute("jobSelectedStatus") != null) {
                jobSelectedStatus = session.getAttribute("jobSelectedStatus")
                        .toString();
                ticketdetailForm.setJobSelectedStatus(session
                        .getAttribute("jobTempStatus").toString().split(","));
            }
            if (session.getAttribute("jobOwnerOtherCheck") != null) {
                ticketdetailForm.setJobOwnerOtherCheck(session.getAttribute(
                        "jobOwnerOtherCheck").toString());
            }
            if (session.getAttribute("timeFrame") != null) {
                ticketdetailForm.setJobMonthWeekCheck(session.getAttribute(
                        "timeFrame").toString());
            }
        }
        if (ticketdetailForm.getNettype() != null
                && ticketdetailForm.getNettype().equals("dispatch")) {
            jobOwnerList = MSAdao.getOwnerList(loginuserid,
                    ticketdetailForm.getJobOwnerOtherCheck(), "prj_job_new",
                    "-1", getDataSource(request, "ilexnewDB"));
        } else {
            jobOwnerList = MSAdao.getOwnerList(loginuserid,
                    ticketdetailForm.getJobOwnerOtherCheck(), "prj_job_new",
                    ticketdetailForm.getAppendixid(),
                    getDataSource(request, "ilexnewDB"));
        }
        jobStatusList = MSAdao.getStatusList("prj_job_new",
                getDataSource(request, "ilexnewDB"));

        if (jobOwnerList.size() > 0) {
            jobOwnerListSize = jobOwnerList.size();
        }

        request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
        request.setAttribute("jobStatusList", jobStatusList);
        request.setAttribute("jobOwnerList", jobOwnerList);
        /* End : Appendix Dashboard View Selector Code */

        if (!ticketdetailForm.getTicket_type().equals("saveticket")) {
            if (ticketdetailForm.getDialog_number() != 0) {
                if (!ticketdetailForm.getTicket_type().equals("firstticket")) {
                    ticketdetailForm.setLo_ot_id(IMdao.getOrganizationTopId(
                            ticketdetailForm.getMsaid(),
                            getDataSource(request, "ilexnewDB")));
                    ticketdetailForm.setCompany_name(Appendixdao.getMsaname(
                            getDataSource(request, "ilexnewDB"),
                            ticketdetailForm.getMsaid()));
                }
            } else {
                if (!ticketdetailForm.getTicket_type().equals("firstticket")) {
                    ticketdetailForm.setLo_ot_id(IMdao.getOrganizationTopId(
                            ticketdetailForm.getMsaid(),
                            getDataSource(request, "ilexnewDB")));
                    ticketdetailForm.setCompany_name(Appendixdao.getMsaname(
                            getDataSource(request, "ilexnewDB"),
                            ticketdetailForm.getMsaid()));
                }
            }
        }

        if (ticketdetailForm.getAppendixid().equals("-1")
                || ticketdetailForm.getAppendixid().equals("%")) {
            ticketdetailForm.setAppendixid("0");
            ticketdetailForm.setMsaid("%");
            ticketdetailForm.setAppendixname("");
        }
        DynamicComboCM dcStateCM = new DynamicComboCM();
        dcStateCM.setFirstlevelcatglist(Pvsdao.getStateList(getDataSource(
                request, "ilexnewDB")));
        request.setAttribute("dcStateCM", dcStateCM);

        DynamicComboCM dcLonDirec = new DynamicComboCM();
        dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
        request.setAttribute("dcLonDirec", dcLonDirec);

        DynamicComboCM dcLatDirec = new DynamicComboCM();
        dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
        request.setAttribute("dcLatDirec", dcLatDirec);

        DynamicComboCM dcCountryList = new DynamicComboCM();
        dcCountryList.setFirstlevelcatglist(DynamicComboDao
                .getCountryList(getDataSource(request, "ilexnewDB")));
        request.setAttribute("dcCountryList", dcCountryList);

        DynamicComboCM dcGroupList = new DynamicComboCM();
        dcGroupList.setFirstlevelcatglist(DynamicComboDao
                .getGroupNameListSiteSearch(ticketdetailForm.getMsaid(),
                        getDataSource(request, "ilexnewDB")));
        request.setAttribute("dcGroupList", dcGroupList);

        DynamicComboCM dcEndCustomerList = new DynamicComboCM();
        dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
                .getEndCustomerList(getDataSource(request, "ilexnewDB"),
                        ticketdetailForm.getMsaid(),
                        ticketdetailForm.getCompany_name()));
        request.setAttribute("dcEndCustomerList", dcEndCustomerList);

        DynamicComboCM dcTimeZoneList = new DynamicComboCM();
        dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao
                .gettimezone(getDataSource(request, "ilexnewDB")));
        request.setAttribute("dcTimeZoneList", dcTimeZoneList);

        DynamicComboCM dcCategoryList = new DynamicComboCM();
        dcCategoryList.setFirstlevelcatglist(DynamicComboDao
                .getCategoryList(getDataSource(request, "ilexnewDB")));
        request.setAttribute("dcCategoryList", dcCategoryList);

        DynamicComboCM dcRegionList = new DynamicComboCM();
        dcRegionList.setFirstlevelcatglist(DynamicComboDao
                .getRegionList(getDataSource(request, "ilexnewDB")));
        request.setAttribute("dcRegionList", dcRegionList);

        DynamicComboCM dcSiteStatus = new DynamicComboCM();
        dcSiteStatus.setFirstlevelcatglist(DynamicComboDao.getsitestatus());
        request.setAttribute("dcSiteStatus", dcSiteStatus);

        initialStateCategory = Ticketdao.getStateCategoryList(getDataSource(
                request, "ilexnewDB"));
        if (initialStateCategory.size() > 0) {
            for (int i = 0; i < initialStateCategory.size(); i++) {
                tempcat = ((TicketDetailForm) initialStateCategory.get(i))
                        .getSiteCountryId();
                ((TicketDetailForm) initialStateCategory.get(i))
                        .setTempList(Ticketdao
                                .getCategoryWiseStateList(tempcat, i,
                                        getDataSource(request, "ilexnewDB")));
            }
        }
        request.setAttribute("initialStateCategory", initialStateCategory);

        DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();

        // set combos values
        dynamiccomboCNSWEB.setProblemcategory(DynamicComboDao
                .getProblemCategory(getDataSource(request, "ilexnewDB")));
        dynamiccomboCNSWEB.setCriticalityname(DynamicComboDao
                .getCriticalityName(ticketdetailForm.getMsaid(),
                        getDataSource(request, "ilexnewDB")));
        // set requestor list

        dynamiccomboCNSWEB.setRequestorlist(OpenSearchWindowdao
                .getOrganizationPOCList(ticketdetailForm.getLo_ot_id(),
                        getDataSource(request, "ilexnewDB")));
        dynamiccomboCNSWEB.setHourname(DynamicComboDao.getHourName());
        dynamiccomboCNSWEB.setMinutename(DynamicComboDao.getMinuteName());
        dynamiccomboCNSWEB.setOptionname(DynamicComboDao.getOptionName());
        request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

        // set product and product category combo values
        productcatlist = Ticketdao.getDepoCategoryList(
                ticketdetailForm.getMsaid(),
                getDataSource(request, "ilexnewDB"));

        if (productcatlist.size() > 0) {
            for (int i = 0; i < productcatlist.size(); i++) {
                tempdepo_cat = ((TicketDetailForm) productcatlist.get(i))
                        .getDp_cat_id();
                ((TicketDetailForm) productcatlist.get(i))
                        .setProdList(Ticketdao.getDepoProductList(tempdepo_cat,
                                        getDataSource(request, "ilexnewDB")));
            }
        }

        // for new ticket
        BeanUtils.copyProperties(ticketdetailDTO, ticketdetailForm);
        if (!ticketdetailForm.getTicket_type().equals("saveticket")) {
            if (ticketdetailForm.getDialog_number() == 0) {
                if (ticketdetailForm.getTicket_type().equals("newticket")
                        || ticketdetailForm.getTicket_type().equals(
                                "firstticket")) {
                    ticketdetailDTO = Ticketdao.createTicketNumber(appendixId,
                            ticketdetailDTO,
                            getDataSource(request, "ilexnewDB"));
                    BeanUtils.copyProperties(ticketdetailForm, ticketdetailDTO);
                }
            }
        }
        // get ticket detail data from database
        if (ticketdetailForm.getRefersh() != null) {
            Ticketdao.getRefershedStates(ticketdetailDTO,
                    getDataSource(request, "ilexnewDB"));
            BeanUtils.copyProperties(ticketdetailForm, ticketdetailDTO);

        } else {
            if (ticketdetailForm.getTicket_type().equals("existingticket")) {
                if (!ticketdetailForm.getRefresh()) {
                    ticketdetailDTO = Ticketdao.getTicketDetail(
                            ticketdetailDTO, request.getParameter("ticket_id"),
                            getDataSource(request, "ilexnewDB"));
                    BeanUtils.copyProperties(ticketdetailForm, ticketdetailDTO);

                }
            }
        }

        if (ticketdetailForm.getHelpDeskCheck() != null
                && ticketdetailForm.getHelpDeskCheck().equals("true")) {
            ticketdetailForm.setHelpdesk("Y");
            ticketdetailForm.setCrit_cat_id("1");
        }

        // set criticality and resource level
        criticalitycatlist = Ticketdao.getCriticalityCategorylist(
                ticketdetailForm.getCriticality(),
                getDataSource(request, "ilexnewDB"));
        request.setAttribute("criticalitycatlist", criticalitycatlist);
        request.setAttribute("criticalitycatlistsize",
                "" + criticalitycatlist.size());
        dynamiccomboCNSWEB.setResourcelevel(DynamicComboDao.getResourceLevel(
                ticketdetailForm.getAppendixid(),
                ticketdetailForm.getCrit_cat_id(),
                getDataSource(request, "ilexnewDB")));

        if (ticketdetailForm.getHelpDeskCheck() != null
                && ticketdetailForm.getHelpDeskCheck().equals("true")) {
            for (int n = 0; n < dynamiccomboCNSWEB.getResourcelevel().size(); n++) {
                if ((((LabelValue) ((ArrayList) dynamiccomboCNSWEB
                        .getResourcelevel()).get(n)).getLabel())
                        .equalsIgnoreCase("Systems Engineer")) {
                    ticketdetailForm
                            .setResourcelevel(((LabelValue) ((ArrayList) dynamiccomboCNSWEB
                                    .getResourcelevel()).get(n)).getValue());
                }
            }
        }

        if (ticketdetailForm.getDepot_requisition() != null) {
            for (int i = 0; i < ticketdetailForm.getDepot_requisition().length; i++) {
                depo_product_id = depo_product_id + ","
                        + ticketdetailForm.getDepot_requisition(i);
            }
            depo_product_id = depo_product_id.substring(1,
                    depo_product_id.length());
        } else {
            if (!ticketdetailForm.getRefresh()) {
                ticketdetailForm.setDepot_requisition(Ticketdao
                        .getProductListFromDatabase(
                                request.getParameter("ticket_id"),
                                getDataSource(request, "ilexnewDB")));
            }
        }

        if (ticketdetailForm.getAddquantity() != null
                || !ticketdetailForm.getRefreshprodlist()) {
            ticketdetailForm.setDepoproductlist(Ticketdao
                    .getSelectedDepoProductList(
                            ticketdetailForm.getTicket_id(), depo_product_id,
                            ticketdetailForm.getAddquantity(),
                            getDataSource(request, "ilexnewDB")));
            ticketdetailForm.setDepoproductlistsize(ticketdetailForm
                    .getDepoproductlist().size() + "");
            request.setAttribute("depotlistsize", ticketdetailForm
                    .getDepoproductlist().size() + "");
            if (ticketdetailForm.getAddquantity() != null) {
                if (ticketdetailForm.getAddquantity().equals("")) {
                    if (ticketdetailForm.getDepot_quantity() != null) {
                        for (int i = 0; i < ticketdetailForm
                                .getDepot_quantity().length; i++) {
                            ((TicketDetailBean) ticketdetailForm
                                    .getDepoproductlist().get(i))
                                    .setDepot_quantity(ticketdetailForm
                                            .getDepot_quantity(i));
                        }
                    }
                }
            }
        }

        if (ticketdetailForm.getSave() != null) {
            // set dates in proper format
            if (!ticketdetailForm.getRequested_datehh().equals("  ")
                    && ticketdetailForm.getRequested_datemm().equals("  ")) {
                ticketdetailForm.setRequested_datemm("00");
            }

            if (!ticketdetailForm.getArrivaldatehh().equals("  ")
                    && ticketdetailForm.getArrivaldatemm().equals("  ")) {
                ticketdetailForm.setArrivaldatemm("00");
            }

            if (!ticketdetailForm.getArrivalwindowstartdatehh().equals("  ")
                    && ticketdetailForm.getArrivalwindowstartdatemm().equals(
                            "  ")) {
                ticketdetailForm.setArrivalwindowstartdatemm("00");
            }

            if (!ticketdetailForm.getArrivalwindowenddatehh().equals("  ")
                    && ticketdetailForm.getArrivalwindowenddatemm()
                    .equals("  ")) {
                ticketdetailForm.setArrivalwindowenddatemm("00");
            }

            String requested_date = ticketdetailForm.getRequested_date();
            if (!ticketdetailForm.getRequested_datehh().equals("  ")) {
                requested_date += " " + ticketdetailForm.getRequested_datehh()
                        + ":" + ticketdetailForm.getRequested_datemm() + " "
                        + ticketdetailForm.getRequested_dateop();
            }

            String arrivaldate = ticketdetailForm.getArrivaldate();
            if (!ticketdetailForm.getArrivaldatehh().equals("  ")) {
                arrivaldate += " " + ticketdetailForm.getArrivaldatehh() + ":"
                        + ticketdetailForm.getArrivaldatemm() + " "
                        + ticketdetailForm.getArrivaldateop();
            }

            String arrivalwindowstartdate = ticketdetailForm
                    .getArrivalwindowstartdate();
            if (!ticketdetailForm.getArrivalwindowstartdatehh().equals("  ")) {
                arrivalwindowstartdate += " "
                        + ticketdetailForm.getArrivalwindowstartdatehh() + ":"
                        + ticketdetailForm.getArrivalwindowstartdatemm() + " "
                        + ticketdetailForm.getArrivalwindowstartdateop();
            }

            String arrivalwindowenddate = ticketdetailForm
                    .getArrivalwindowenddate();
            if (!ticketdetailForm.getArrivalwindowenddatehh().equals("  ")) {
                arrivalwindowenddate += " "
                        + ticketdetailForm.getArrivalwindowenddatehh() + ":"
                        + ticketdetailForm.getArrivalwindowenddatemm() + " "
                        + ticketdetailForm.getArrivalwindowenddateop();
            }
            BeanUtils.copyProperties(ticketdetailDTO, ticketdetailForm);
            Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Ticket Detail Action!", "");
            retval = Ticketdao.updateTicketDetail(ticketdetailDTO,
                    requested_date, arrivaldate, arrivalwindowstartdate,
                    arrivalwindowenddate, loginuserid,
                    getDataSource(request, "ilexnewDB"));
            BeanUtils.copyProperties(ticketdetailForm, ticketdetailDTO);
            // ticketdetailForm.setResourcelevel("1032");

            if (ticketdetailForm.getTicket_id() == null
                    || ticketdetailForm.getTicket_id().equals("0")) {
                request.setAttribute("refreshtree", "true");
            }
            // get job id
            String jobTicketIds[] = new String[3];

            jobTicketIds = Ticketdao.getJobIdFromTicketNo(
                    ticketdetailForm.getTicket_number(),
                    getDataSource(request, "ilexnewDB"));
            ticketdetailForm.setTicket_id(jobTicketIds[1]);

            if (ticketdetailForm.getDe_product_id() != null) {
                for (int i = 0; i < ticketdetailForm.getDe_product_id().length; i++) {
                    retval1 = Ticketdao.UpdateDepotQuantity(i,
                            ticketdetailForm.getTicket_id(),
                            ticketdetailForm.getDe_product_id(i),
                            ticketdetailForm.getDepot_quantity(i),
                            getDataSource(request, "ilexnewDB"));
                    request.setAttribute("retval1", "" + retval1);
                }
            }

            request.setAttribute("retval", "" + retval);
            request.setAttribute("type", "netmedxdashboardfromticket");

            if (ticketdetailForm.getNettype().equals("jobdashboard")) {
                request.setAttribute("jobid", ticketdetailForm.getJobid());
                return mapping.findForward("jobdashboard");
            }

            if (ticketdetailForm.getAppendixid().equals("0")) {
                if (ticketdetailForm.getMsaid() != null) {
                    ticketdetailForm.setAppendixid(Ticketdao
                            .getNetMedXAppendixId(ticketdetailForm.getMsaid(),
                                    getDataSource(request, "ilexnewDB")));
                }
            }

            if (ticketdetailForm.getNettype().equals("dispatch")) {
                request.setAttribute("appendix_Id", "-1");
            } else {
                request.setAttribute("appendix_Id",
                        ticketdetailForm.getAppendixid());
            }

            // request.setAttribute("refreshtree","true");
            request.setAttribute("ownerId", ticketdetailForm.getOwnerId());
            request.setAttribute("viejobtype",
                    ticketdetailForm.getViewjobtype());
            request.setAttribute("nettype", ticketdetailForm.getNettype());

            if (ticketdetailForm.getNettype().equals("webTicket")) {
                // return mapping.findForward("webticketpage");
                RequestDispatcher rd = request
                        .getRequestDispatcher("JobDashboardAction.do?function=view&jobid="
                                + ticketdetailForm.getJobid()
                                + "&appendixid="
                                + ticketdetailForm.getAppendixid()
                                + "&nettype=&");
                rd.forward(request, response);
                return null;
            }

            forward = mapping.findForward("netmedxdashboardfromticket");
            return (forward);
        }
        // request.setAttribute("viejobtype",ticketdetailForm.getViewjobtype());
        request.setAttribute("productcatlist", productcatlist);
        request.setAttribute("listsize", productcatlist.size() + "");

        request.setAttribute("appendixid", ticketdetailForm.getAppendixid());
        request.setAttribute("nettype", ticketdetailForm.getNettype());
        request.setAttribute("viewjobtype", ticketdetailForm.getViewjobtype());
        request.setAttribute("ownerId", ticketdetailForm.getOwnerId());
        request.setAttribute("loginUserId", loginuserid);

        return (mapping.findForward("success"));
    }

}
