package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.CostScheduledao;
import com.mind.formbean.PRM.CostScheduleForm;
import com.mind.util.WebUtil;

public class CostScheduleAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		CostScheduleForm costScheduleForm = (CostScheduleForm) form;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		/* Page Security End */

		ArrayList costschedulelist = new ArrayList();

		ArrayList activitylist = new ArrayList();

		if (request.getParameter("appendixid") != null) {
			costScheduleForm.setAppendixid(request.getParameter("appendixid"));
		}

		if (request.getParameter("viewjobtype") != null) {
			costScheduleForm
					.setViewjobtype(request.getParameter("viewjobtype"));
		}
		if (request.getParameter("ownerId") != null) {
			costScheduleForm.setOwnerId(request.getParameter("ownerId"));
		}
		costScheduleForm.setMsaId(IMdao.getMSAId(
				costScheduleForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")));
		costschedulelist = CostScheduledao.getAllcostschedulelist(
				costScheduleForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("costschedulelist", costschedulelist);
		// By Amit Start: To get Appendix Name on schedule view screen
		// bulkjobform.setAppendixname(Jobdao.getAppendixname(ds,appendixid));
		costScheduleForm.setAppendixname(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				costScheduleForm.getAppendixid()));

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(costScheduleForm.getAppendixid(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				costScheduleForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("appendixid", costScheduleForm.getAppendixid());
		request.setAttribute("msaId", costScheduleForm.getMsaId());
		request.setAttribute("viewjobtype", costScheduleForm.getViewjobtype());
		request.setAttribute("ownerId", costScheduleForm.getOwnerId());

		/* Start : Appendix Dashboard View Selector Code */

		if (costScheduleForm.getJobOwnerOtherCheck() != null
				&& !costScheduleForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					costScheduleForm.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", costScheduleForm.getAppendixid());
			request.setAttribute("msaId", costScheduleForm.getMsaId());
			return mapping.findForward("temppage");

		}

		if (costScheduleForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < costScheduleForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ costScheduleForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ costScheduleForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (costScheduleForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < costScheduleForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ costScheduleForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ costScheduleForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					costScheduleForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					costScheduleForm.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", costScheduleForm.getAppendixid());
			request.setAttribute("msaId", costScheduleForm.getMsaId());
			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				costScheduleForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				costScheduleForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				costScheduleForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				costScheduleForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(userid,
				costScheduleForm.getJobOwnerOtherCheck(), "prj_job_new",
				costScheduleForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		forward = mapping.findForward("success");
		return (forward);
	}

}
