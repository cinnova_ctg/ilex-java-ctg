package com.mind.actions.PRM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.prm.DepotRequisitionBean;
import com.mind.bean.prm.TicketDetailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PRM.DepotRequisitionDao;
import com.mind.dao.PRM.Ticketdao;
import com.mind.formbean.PRM.DepotRequisitionForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.util.WebUtil;

public class DepotRequisitionAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DepotRequisitionAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Depot Requisition", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */

		String tempdepo_cat = "";
		String depo_product_id = "";
		DepotRequisitionForm depotRequisitionForm = (DepotRequisitionForm) form;
		ArrayList productcatlist = new ArrayList();
		Map<String, Object> map;

		if (request.getParameter("appendixid") != null) {
			depotRequisitionForm.setAppendixid((request
					.getParameter("appendixid") == null ? depotRequisitionForm
					.getAppendixid() : request.getParameter("appendixid")));
			String appendixId = request.getParameter("appendixid");
		}
		if (request.getParameter("Job_Id") != null) {
			depotRequisitionForm
					.setJobid((request.getParameter("Job_Id") == null ? depotRequisitionForm
							.getJobid() : request.getParameter("Job_Id")));
		}
		depotRequisitionForm.setMsaid(IMdao.getMSAId(
				depotRequisitionForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")));
		if (depotRequisitionForm.getJobid() != null
				&& !depotRequisitionForm.getJobid().trim().equalsIgnoreCase("")) {
			String ticketId = JobDAO.getTicketId(
					depotRequisitionForm.getJobid(),
					getDataSource(request, "ilexnewDB"));
			if (logger.isDebugEnabled()) {
				logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - ticketId-----------"
						+ ticketId + "jobId" + depotRequisitionForm.getJobid());
			}
			if (ticketId != null && !ticketId.equals("")) {
				depotRequisitionForm.setTicket_id(ticketId);
			}
			map = DatabaseUtilityDao.setMenu(depotRequisitionForm.getJobid(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		productcatlist = DepotRequisitionDao.getDepoCategoryList(
				depotRequisitionForm.getMsaid(),
				getDataSource(request, "ilexnewDB"));

		if (productcatlist.size() > 0) {
			for (int i = 0; i < productcatlist.size(); i++) {
				tempdepo_cat = ((DepotRequisitionBean) productcatlist.get(i))
						.getDp_cat_id();

				((DepotRequisitionBean) productcatlist.get(i))
						.setProdList(Ticketdao.getDepoProductList(tempdepo_cat,
								getDataSource(request, "ilexnewDB")));
			}
		}
		if (depotRequisitionForm.getDepot_requisition() != null) {
			for (int i = 0; i < depotRequisitionForm.getDepot_requisition().length; i++) {
				depo_product_id = depo_product_id + ","
						+ depotRequisitionForm.getDepot_requisition()[i];
			}
			depo_product_id = depo_product_id.substring(1,
					depo_product_id.length());
		} else {
			if (!depotRequisitionForm.getRefresh()) {
				depotRequisitionForm.setDepot_requisition(Ticketdao
						.getProductListFromDatabase(
								request.getParameter("ticket_id"),
								getDataSource(request, "ilexnewDB")));
			}
		}
		if (depotRequisitionForm.getAddquantity() != null
				|| !depotRequisitionForm.getRefreshprodlist()) {
			depotRequisitionForm.setDepoproductlist(Ticketdao
					.getSelectedDepoProductList(
							depotRequisitionForm.getTicket_id(),
							depo_product_id,
							depotRequisitionForm.getAddquantity(),
							getDataSource(request, "ilexnewDB")));
			depotRequisitionForm.setDepoproductlistsize(depotRequisitionForm
					.getDepoproductlist().size() + "");
			request.setAttribute("depotlistsize", depotRequisitionForm
					.getDepoproductlist().size() + "");
			if (depotRequisitionForm.getAddquantity() != null) {
				if (depotRequisitionForm.getAddquantity().equals("")) {
					if (depotRequisitionForm.getDepot_quantity() != null) {
						for (int i = 0; i < depotRequisitionForm
								.getDepot_quantity().length; i++)
							((TicketDetailBean) depotRequisitionForm
									.getDepoproductlist().get(i))
									.setDepot_quantity(depotRequisitionForm
											.getDepot_quantity(i));
					}
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - list"
					+ depotRequisitionForm.getProdList());
		}
		request.setAttribute("productcatlist", productcatlist);
		request.setAttribute("depoproductlist",
				depotRequisitionForm.getProdList());
		request.setAttribute("listsize", productcatlist.size() + "");
		return mapping.findForward("success");
	}

	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		DepotRequisitionForm depotRequisitionForm = (DepotRequisitionForm) form;
		if (logger.isDebugEnabled()) {
			logger.debug("save(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in save of  depoy"
					+ depotRequisitionForm.getDe_product_id());
		}

		String loginuserid = (String) request.getSession(false).getAttribute(
				"userid");
		if (depotRequisitionForm.getDe_product_id() != null) {
			for (int i = 0; i < depotRequisitionForm.getDe_product_id().length; i++) {
				Ticketdao.UpdateDepotQuantity(i,
						depotRequisitionForm.getTicket_id(),
						depotRequisitionForm.getDe_product_id()[i],
						depotRequisitionForm.getDepot_quantity()[i],
						getDataSource(request, "ilexnewDB"));
			}
		}

		return unspecified(mapping, depotRequisitionForm, request, response);
	}

}
