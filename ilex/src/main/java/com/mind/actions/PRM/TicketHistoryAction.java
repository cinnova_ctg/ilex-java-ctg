package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.formbean.PRM.TicketHistoryForm;


public class TicketHistoryAction extends com.mind.common.IlexAction
{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		TicketHistoryForm tickethistoryform = ( TicketHistoryForm ) form;
		int history_size = 0;
		
		if( request.getParameter( "jobid" ) != null )
		{
			tickethistoryform.setJobid( request.getParameter( "jobid" ) );
		}
		
		if( request.getParameter( "from" ) != null )
		{
			tickethistoryform.setFrom( request.getParameter( "from" ) );
		}
		
		if(request.getParameter("nettype") != null)  { 
			tickethistoryform.setNettype(""+request.getParameter("nettype")); 
		}
		
		if(request.getParameter("viewjobtype") != null)  { 
			tickethistoryform.setViewjobtype(""+request.getParameter("viewjobtype")); 
		}
		
		if(request.getParameter("ownerId") != null)  { 
			tickethistoryform.setOwnerId(""+request.getParameter("ownerId")); 
		}
		else
		{
			tickethistoryform.setOwnerId("%");
		}
		
		tickethistoryform.setAppendixid( IMdao.getAppendixId( tickethistoryform.getJobid() , getDataSource( request , "ilexnewDB" ) ) );
		tickethistoryform.setJobname( Jobdao.getJobname( tickethistoryform.getJobid() , getDataSource( request , "ilexnewDB" ) ) );
		
		ArrayList tickethistory = JobDashboarddao.getTicketHistory( tickethistoryform.getJobid() , getDataSource( request , "ilexnewDB" ) );
		
		if( tickethistory.size() > 0 )
		{
			history_size = tickethistory.size();
		}
		
		request.setAttribute( "Size" , new Integer ( history_size ) );
		
		request.setAttribute( "tickethistory" , tickethistory );
		
		return mapping.findForward( "success" );
	}
}
