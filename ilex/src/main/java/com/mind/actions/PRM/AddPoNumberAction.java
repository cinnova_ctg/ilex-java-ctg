/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is an action class used for assigning/updating purchase order number and work order number for a job/activity in project manager.      
*
*/
package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.AddPoNumberdao;
import com.mind.formbean.PRM.AddPoNumberForm;



public class AddPoNumberAction extends com.mind.common.IlexDispatchAction{

/** This method provides functionality for adding/updating purchase order number and work order number for a job/activity in a project manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
	public ActionForward add(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddPoNumberForm addPoNumberForm = (AddPoNumberForm) form;
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired 
		
		String userid = (String)session.getAttribute("userid");
		if(!Authenticationdao.getPageSecurity(userid, "Manage Partner", "Add PO/WO Number", getDataSource(request,"ilexnewDB"))) {
			return( mapping.findForward("UnAuthenticate"));	
		}
		/* Page Security End*/
		
		int flag=0;
		int status = 0;
		String typeid="";
		String type="";
		int size=0;
		int checkactivitylength = 0;
		int hidactidlength = 0;
		String ponumber="0";
		String wonumber="0";
		
		
		ArrayList Partnercostlist=new ArrayList();
		ArrayList jobdetaillist=new ArrayList();
				
		if(addPoNumberForm.getSave()!=null)
		{
			String[] list = new String[4];
			typeid = addPoNumberForm.getTypeid();
			addPoNumberForm.setTypeid(typeid);
			request.setAttribute("typeid",typeid);
			type = addPoNumberForm.getType().trim();
			addPoNumberForm.setType(type);
			request.setAttribute("type",type);
			addPoNumberForm.setFunction(addPoNumberForm.getFunction());
			if(type.equals("J")||type.equals("AJ")||type.equals("CJ")||type.equals("IJ"))
			{
				if(addPoNumberForm.getCkboxjob()!=null)
				{	
					
					status=AddPoNumberdao.AddPonumber(typeid,"J",addPoNumberForm.getPonumber(),addPoNumberForm.getWonumber(),userid,getDataSource(request,"ilexnewDB"));
					
				}
				else
				{
					if(addPoNumberForm.getCkboxactivity()!=null) checkactivitylength = addPoNumberForm.getCkboxactivity().length;
					if(addPoNumberForm.getHid_actid()!=null) hidactidlength = addPoNumberForm.getHid_actid().length;
					//System.out.println("---xxxx1-- addPoNumberForm.getHid_actid().length "+addPoNumberForm.getHid_actid().length);
					for(int j = 0; j < hidactidlength; j++) 
					{
						//System.out.println("---xxxx2-- checkactivitylength "+checkactivitylength);
						for(int i=0;i<checkactivitylength;i++)
						{
							//System.out.println("---xxxx3-- addPoNumberForm.getCkboxactivity(i) "+addPoNumberForm.getCkboxactivity(i));
							if(addPoNumberForm.getCkboxactivity(i)!=null)	
							{
								//System.out.println("---xxxx4-- addPoNumberForm.getCkboxactivity(i),addPoNumberForm.getHid_actid(j) "+addPoNumberForm.getCkboxactivity(i)+","+addPoNumberForm.getHid_actid(j));
								if(addPoNumberForm.getCkboxactivity(i).equals(addPoNumberForm.getHid_actid(j)))
								{
									//System.out.println("---xxxx-- "+j+","+addPoNumberForm.getPonumberact(j)+","+addPoNumberForm.getWonumberact(j));
									status=AddPoNumberdao.AddPonumber(addPoNumberForm.getCkboxactivity(i),"JA",addPoNumberForm.getPonumberact(j),addPoNumberForm.getWonumberact(j),userid,getDataSource(request,"ilexnewDB"));
								}
							}
						}
					}
					
				}
								
				jobdetaillist=AddPoNumberdao.getJobDetail(addPoNumberForm.getTypeid(),addPoNumberForm.getType(), getDataSource(request,"ilexnewDB"));
				request.setAttribute("jobdetaillist",jobdetaillist);
				addPoNumberForm.setUpdatemessage(""+status);
				
			}	
			
			
			if(type.equals("A")||type.equals("AA")||type.equals("CA")||type.equals("IA"))
			{
				checkactivitylength = addPoNumberForm.getCkboxactivity().length;
				
				for(int j=0; j<addPoNumberForm.getHid_actid().length; j++) 
				{
					for(int i=0;i<checkactivitylength;i++)
					{
						if(addPoNumberForm.getCkboxactivity(i)!=null)	
						{
							if(addPoNumberForm.getCkboxactivity(i).equals(addPoNumberForm.getHid_actid(j)))
							{
								status=AddPoNumberdao.AddPonumber(addPoNumberForm.getCkboxactivity(i),"A",addPoNumberForm.getPonumberact(j),addPoNumberForm.getWonumberact(j),userid,getDataSource(request,"ilexnewDB"));
							}
						}
					}
				}
				
				addPoNumberForm.setUpdatemessage(""+status);		
				
			}
			Partnercostlist=AddPoNumberdao.getPartnercostlist(addPoNumberForm.getTypeid(),addPoNumberForm.getType(), ponumber, wonumber, getDataSource(request,"ilexnewDB"));
			request.setAttribute("Partnercostlist",Partnercostlist);
			size=Partnercostlist.size();
			request.setAttribute("size",size+"");	
			AddPoNumberdao.checkpartnerassign(request.getParameter("typeid"),request.getParameter("type"), getDataSource(request,"ilexnewDB"));
			addPoNumberForm.setMessage(""+status);
			request.setAttribute("assignmessage",addPoNumberForm.getMessage());

		}
		else
		{
			
			typeid = request.getParameter("typeid");
			addPoNumberForm.setTypeid(typeid);
			request.setAttribute("typeid",typeid);
			type = request.getParameter("type");
			addPoNumberForm.setType(type);
			request.setAttribute("type",type);
			addPoNumberForm.setFunction(request.getParameter("function"));
			
			status=AddPoNumberdao.checkpartnerassign(request.getParameter("typeid"),request.getParameter("type"), getDataSource(request,"ilexnewDB"));
			addPoNumberForm.setMessage(""+status);
			
			request.setAttribute("assignmessage",addPoNumberForm.getMessage());
			//System.out.println("XXXX status po action "+status);
			if(status==0 && (request.getParameter("type").equals("J") ||request.getParameter("type").equals("AJ") ||request.getParameter("type").equals("CJ") ||request.getParameter("type").equals("IJ")))
			{
				jobdetaillist=AddPoNumberdao.getJobDetail(request.getParameter("typeid"),request.getParameter("type"), getDataSource(request,"ilexnewDB"));
				request.setAttribute("jobdetaillist",jobdetaillist);
				ponumber= ( ( AddPoNumberForm )jobdetaillist.get(0) ).getPonumber();
				
				wonumber= ( ( AddPoNumberForm )jobdetaillist.get(0) ).getWonumber();
				
				Partnercostlist=AddPoNumberdao.getPartnercostlist(request.getParameter("typeid"),request.getParameter("type"), ponumber, wonumber, getDataSource(request,"ilexnewDB"));
				request.setAttribute("Partnercostlist",Partnercostlist);
				size=Partnercostlist.size();
				request.setAttribute("size",size+"");	
				
				
			}
			
			if(status==0 && (request.getParameter("type").equals("A") ||request.getParameter("type").equals("AA") ||request.getParameter("type").equals("CA")||request.getParameter("type").equals("IA") ))
			{
				Partnercostlist=AddPoNumberdao.getPartnercostlist(request.getParameter("typeid"),request.getParameter("type"), ponumber, wonumber, getDataSource(request,"ilexnewDB"));
				
				request.setAttribute("Partnercostlist",Partnercostlist);
				
				size=Partnercostlist.size();
				request.setAttribute("size",size+"");	
				
			}
			
		}
		
		
		forward = mapping.findForward("success");
		
		return (forward);
	}


}
