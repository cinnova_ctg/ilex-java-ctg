package com.mind.actions.PRM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.AssignTeamdao;
import com.mind.formbean.PRM.AssignTeamForm;
import com.mind.util.WebUtil;

/**
 * Discription: This class is use to assignment of Appendix team.
 * 
 * @author Vijay Kumar Singh
 * 
 */
public class AssignTeamAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Discription: This method is use to view assigned members and its role.
	 */
	public ActionForward View(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");

		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Appendix",
				"Project Level - Assign Team",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}

		AssignTeamForm assignTeamForm = (AssignTeamForm) form;
		String customerEmail = AssignTeamdao.getCustomerEmail(
				getDataSource(request, "ilexnewDB"),
				assignTeamForm.getAppendixid());
		if (customerEmail != null && !"".equals(customerEmail)) {
			request.setAttribute("customerEmail", customerEmail);
		}
		/* Variable for Appendix Dashboard View Selector: Start */
		ArrayList jobOwnerList = new ArrayList(); // - for list of Job Owner
		ArrayList jobStatusList = new ArrayList(); // - list of job status
		String jobSelectedStatus = ""; // - selected status
		String jobSelectedOwner = ""; // - selected owner
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		/* Variable for Appendix Dashboard View Selector: End */

		if (request.getParameter("appendixid") != null) {
			assignTeamForm.setAppendixid((String) request
					.getParameter("appendixid"));
			assignTeamForm.setAppendixName(Appendixdao.getAppendixname(
					assignTeamForm.getAppendixid(),
					getDataSource(request, "ilexnewDB")));
			assignTeamForm.setMsaId(IMdao.getMSAId(
					assignTeamForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"))); // - for menu
		}
		if (request.getParameter("ownerId") != null) // - for menu
		{
			assignTeamForm.setOwnerId(request.getParameter("ownerId"));
		}
		if (request.getParameter("viewjobtype") != null) // - for menu
		{
			assignTeamForm.setViewjobtype(request.getParameter("viewjobtype"));
		}
		if (request.getParameter("fromPage") != null) {
			assignTeamForm.setFromPage(request.getParameter("fromPage"));
		}

		assignTeamForm.setPocList(AssignTeamdao.getPocList(getDataSource(
				request, "ilexnewDB")));
		assignTeamForm.setRoleList(AssignTeamdao.getRoleList(getDataSource(
				request, "ilexnewDB")));
		assignTeamForm.setAssignTeamList(AssignTeamdao.getAllPocRoleList((String) request.getParameter("appendixid"),
				getDataSource(request, "ilexnewDB")));

		/* For Menu & View Selector: Start */

		/* Start : Appendix Dashboard View Selector Code */

		if (assignTeamForm.getJobOwnerOtherCheck() != null
				&& !assignTeamForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					assignTeamForm.getJobOwnerOtherCheck());
		}
		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", assignTeamForm.getAppendixid());
			request.setAttribute("msaId", assignTeamForm.getMsaId());
			return mapping.findForward("temppage");
		}
		if (assignTeamForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < assignTeamForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ assignTeamForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ assignTeamForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}
		if (assignTeamForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < assignTeamForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ assignTeamForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ assignTeamForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}
		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					assignTeamForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					assignTeamForm.getJobOwnerOtherCheck());
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", assignTeamForm.getAppendixid());
			request.setAttribute("msaId", assignTeamForm.getMsaId());

			return mapping.findForward("temppage"); // - /common/Tempupload.jsp
		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				assignTeamForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				assignTeamForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				assignTeamForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				assignTeamForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}
		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				assignTeamForm.getJobOwnerOtherCheck(), "prj_job_new",
				assignTeamForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));
		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(assignTeamForm.getAppendixid(),
						getDataSource(request, "ilexnewDB")); // - for addendum
																// section of
																// menu
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				assignTeamForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")); // - Appendix current
														// status

		/* Set Parameter for menu and view selector */
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("contractDocMenu", contractDocoumentList);
		request.setAttribute("msaId", assignTeamForm.getMsaId());
		request.setAttribute("viewjobtype", assignTeamForm.getViewjobtype());
		request.setAttribute("ownerId", assignTeamForm.getOwnerId());
		request.setAttribute("appendixid", assignTeamForm.getAppendixid());
		request.setAttribute("fromPage", request.getParameter("fromPage"));

		/* For Menu & View Selector: End */

		return mapping.findForward("success"); // - PRM/AssignTeam.jsp
	}

	/**
	 * Discription: This method is use to assign role to member.
	 */
	public ActionForward Add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");

		AssignTeamForm assignTeamForm = (AssignTeamForm) form;

		/* For Assign Poc: Start */
		String customerEmail = request.getParameter("customerEmail");
		if (customerEmail != null) {
			AssignTeamdao.saveCustomerEmail(
					getDataSource(request, "ilexnewDB"),
					assignTeamForm.getAppendixid(), customerEmail);
			request.setAttribute("customerEmail", customerEmail);
		}
		String[] fname = request.getParameterValues("fname");
		if (fname != null) {
			for (int i = 0; i < fname.length; i++) {
				String jobOwner = request.getParameter("jobOwner[" + i + "]");
				String scheduler = request.getParameter("scheduler[" + i + "]");
				String invGenerator = request.getParameter("invGenerator[" + i
						+ "]");
				String roleId = "";
				if (jobOwner != null && "on".equals(jobOwner)) {
					roleId = "2"; // Jobowner
			int addFlag = AssignTeamdao.assignRole(
							assignTeamForm.getAppendixid(), roleId, fname[i],
							getDataSource(request, "ilexnewDB"));
				} else {
					roleId = "2"; // Jobowner
					int deleteFlag = AssignTeamdao.deleteAssignedRole(
							assignTeamForm.getAppendixid(), roleId, fname[i],
					getDataSource(request, "ilexnewDB"));
				}
				if (scheduler != null && "on".equals(scheduler)) {
					roleId = "3"; // Scheduler
					int addFlag = AssignTeamdao.assignRole(
							assignTeamForm.getAppendixid(), roleId, fname[i],
							getDataSource(request, "ilexnewDB"));
				} else {
					roleId = "3"; // Scheduler
					int deleteFlag = AssignTeamdao.deleteAssignedRole(
							assignTeamForm.getAppendixid(), roleId, fname[i],
							getDataSource(request, "ilexnewDB"));
				}
				if (invGenerator != null && "on".equals(invGenerator)) {
					roleId = "6"; // invoice Generator
					int addFlag = AssignTeamdao.assignRole(
							assignTeamForm.getAppendixid(), roleId, fname[i],
							getDataSource(request, "ilexnewDB"));
				} else {
					roleId = "6"; // invoice Generator
					int deleteFlag = AssignTeamdao.deleteAssignedRole(
							assignTeamForm.getAppendixid(), roleId, fname[i],
							getDataSource(request, "ilexnewDB"));
				}
			}
		}
		/* For Assign Poc: End */

		return mapping.findForward("add"); // - AssignTeam.do?function=View
	}

	/**
	 * Discription: This method is use to delete assigned members and its role.
	 */
	public ActionForward Delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");

		AssignTeamForm assignTeamForm = (AssignTeamForm) form;

		String appendixId = "";
		String roleId = "";
		String pocId = "";

		if (request.getParameter("appendixid") != null) {
			appendixId = request.getParameter("appendixid");
		}
		if (request.getParameter("roleid") != null) {
			roleId = request.getParameter("roleid");
		}
		if (request.getParameter("pocid") != null) {
			pocId = request.getParameter("pocid");
		}

		/* For Delete Assigned Poc: Start */
		if (!roleId.equals("0") || !pocId.equals("0")) {
			int deleteFlag = AssignTeamdao.deleteAssignedRole(appendixId,
					roleId, pocId, getDataSource(request, "ilexnewDB"));
			request.setAttribute("deleteFlag", deleteFlag);
		}
		/* For Delete Assigned Poc: End */

		return mapping.findForward("delete"); // - AssignTeam.do?function=View
	}

	public ActionForward AssignToList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
		String loginuserid = (String) session.getAttribute("userid");
		AssignTeamForm assignTeamForm = (AssignTeamForm) form;
		String username = request.getParameter("pocId");
		String roleId = "2";
		int addFlag = AssignTeamdao.assignRole(assignTeamForm.getAppendixid(),
				roleId, username, getDataSource(request, "ilexnewDB"));
		String customerEmail = request.getParameter("customerEmail");
		if (customerEmail != null) {
			request.setAttribute("customerEmail", customerEmail);
		}
		return mapping.findForward("add");
	}
}
