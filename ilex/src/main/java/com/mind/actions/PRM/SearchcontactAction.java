package com.mind.actions.PRM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.ViewList;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.formbean.PRM.SearchcontactForm;
public class SearchcontactAction extends com.mind.common.IlexAction
{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		SearchcontactForm searchcontactform = ( SearchcontactForm ) form;
		int contactlistsize = 0;
		String partnername=null;
		String checknetmedxappendix="0";
		String partnerid="0";
		
		
		if( request.getParameter( "type" ) != null )
			searchcontactform.setType( request.getParameter( "type" ) );
		
		if( request.getParameter( "appendixid" ) != null )
			searchcontactform.setAppendixid( request.getParameter( "appendixid" ) );
		
		if(searchcontactform.getType().equals("S") || searchcontactform.getType().equals("M"))
			searchcontactform.setType("P");
		
		if(searchcontactform.getType().equals("P"))
		{
			/*if( request.getParameter( "partnername" ) != null )
			{
				partnername=request.getParameter( "partnername" );
				System.out.println("1111111----"+partnername);
				partnername=partnername.substring(0,partnername.indexOf(","));
				System.out.println("partnername "+partnername);
				searchcontactform.setDivsearch( partnername );
			}*/
			if( request.getParameter( "partnerid" ) != null )
			{
				partnerid=request.getParameter( "partnerid" );
				partnername=POWODetaildao.getPartnername( getDataSource( request , "ilexnewDB" ) , partnerid );
				if(partnername!=null && !partnername.equals(""))
				{
					if(partnername.length()>0) {
						if(partnername.indexOf(",")> -1){
							partnername=partnername.substring(0,partnername.indexOf(","));
						}
					}
				}
				
				searchcontactform.setDivsearch( partnername );
			}
		}
		
		checknetmedxappendix=ViewList.CheckAppendixType(searchcontactform.getAppendixid() , getDataSource( request , "ilexnewDB" ));
		
		
		
		if( searchcontactform.getSearch() != null )
		{
			searchcontactform.setContactlist( POWODetaildao.getSearchResult( searchcontactform.getType() , searchcontactform.getOrgsearch() , searchcontactform.getDivsearch() , searchcontactform.getAppendixid() , checknetmedxappendix,getDataSource( request , "ilexnewDB" )  ) );
		
			if( searchcontactform.getContactlist().size() > 0 )
			{
				contactlistsize = searchcontactform.getContactlist().size();
				
			}
		
		}
		
		request.setAttribute( "contactlistsize" , contactlistsize+"" );
		
		
		return mapping.findForward( "success" );
	}
}
