package com.mind.actions.PM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.LaborEditBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.PM.LaborEditForm;

public class LaborEditAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		LaborEditForm laboreditform = (LaborEditForm) form;
		NameId idname = null;
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		String from = "";
		String jobid = "";
		LaborEditBean laborEditBean = new LaborEditBean();

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "Resource",
				"Resource Edit", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("Activity_Id") != null) {
			laboreditform.setActivity_Id(request.getParameter("Activity_Id"));
		}

		// if( request.getParameter( "flag" ) != null )
		// {
		// laboreditform .setFlag( request.getParameter( "flag" ));
		// }

		if (request.getParameter("from") != null
				&& request.getParameter("from") != "") {
			from = request.getParameter("from");
			jobid = request.getParameter("jobid");
			laboreditform.setFromflag(request.getParameter("from"));
			laboreditform.setDashboardid(request.getParameter("jobid"));

			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Labor_Id") != null) {
			laboreditform.setLaborid(request.getParameter("Labor_Id"));
		}

		if (request.getParameter("ref") != null) {
			laboreditform.setChkaddendum(request.getParameter("ref"));
			laboreditform.setRef(request.getParameter("ref"));

		}

		idname = Activitydao.getIdName(laboreditform.getActivity_Id(),
				"Resource", "Appendix", getDataSource(request, "ilexnewDB"));
		laboreditform.setAppendixId(idname.getId());
		laboreditform.setAppendixName(idname.getName());
		laboreditform.setJobName(Jobdao.getoplevelname(
				laboreditform.getActivity_Id(), "Resource",
				getDataSource(request, "ilexnewDB")));
		laboreditform.setJobId(Activitydao.getJobid(
				getDataSource(request, "ilexnewDB"),
				laboreditform.getActivity_Id()));

		idname = Activitydao.getIdName(laboreditform.getJobId(), "Activity",
				"MSA", getDataSource(request, "ilexnewDB"));
		laboreditform.setMsaName(idname.getName());
		laboreditform.setMsaId(idname.getId());
		laboreditform.setActivityName(Activitydao.getActivityname(
				laboreditform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));

		if (laboreditform.getChkaddendum().equals("View")) {
			laboreditform.setLabortype("L");
		}
		if (laboreditform.getChkaddendum().equals("Viewinscope")) {
			laboreditform.setLabortype("IL");
		}

		if (laboreditform.getSave() != null) {

			BeanUtils.copyProperties(laborEditBean, laboreditform);
			int updateflagLabor = Resourcedao.updatelaborresourceNew(
					laborEditBean, userid, getDataSource(request, "ilexnewDB"));
			request.setAttribute("updateflagLabor", "" + updateflagLabor);
			request.setAttribute("from", laboreditform.getFromflag());
			request.setAttribute("jobid", laboreditform.getDashboardid());
			return (mapping.findForward("resourcelistpage"));
		}

		else {
			BeanUtils.copyProperties(laborEditBean, laboreditform);
			Resourcedao.getLaborResourceEdit(laborEditBean, "L", "", request
					.getSession().getId(), "listpage",
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(laboreditform, laborEditBean);
		}

		// For the Material Menu
		String laborType = "";
		String laborStatus = "";
		String laborStatusList = "";

		if (laboreditform.getRef().equalsIgnoreCase("View"))
			laborType = "L";
		else
			laborType = "IL";

		laborStatus = Resourcedao.getResourceStatus(
				laboreditform.getActivity_Id(), laboreditform.getLaborid(),
				laborType, request.getSession().getId(),
				getDataSource(request, "ilexnewDB"));

		if (laboreditform.getRef().equalsIgnoreCase("View")) {
			request.setAttribute("chkaddendum", "View");
			request.setAttribute("chkdetailactivity", "detailactivity");

			if (request.getParameter("from") != null)
				laborStatusList = Menu.getStatus("jobdashboard_resL",
						laborStatus.charAt(0), from,
						request.getParameter("jobid"), "View",
						laboreditform.getLaborid(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				laborStatusList = Menu.getStatus("pm_resL",
						laborStatus.charAt(0), "", "", "",
						laboreditform.getLaborid(), userid,
						getDataSource(request, "ilexnewDB"));

		}

		if (laboreditform.getRef().equalsIgnoreCase("Viewinscope")) {
			request.setAttribute("chkaddendum", "Viewinscope");
			request.setAttribute("chkdetailactivity", "inscopedetailactivity");

			if (request.getParameter("from") != null)
				laborStatusList = Menu.getStatus("jobdashboard_resL",
						laborStatus.charAt(0), from,
						request.getParameter("jobid"), "Viewinscope",
						laboreditform.getLaborid(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				laborStatusList = Menu.getStatus("jobdashboard_resL",
						laborStatus.charAt(0), "", "", "Viewinscope",
						laboreditform.getLaborid(), userid,
						getDataSource(request, "ilexnewDB"));
		}

		request.setAttribute("Activity_Id", laboreditform.getActivity_Id());
		request.setAttribute("Labor_Id", laboreditform.getLaborid());
		request.setAttribute("commonResourceStatus", laborStatus);
		request.setAttribute("laborStatusList", laborStatusList);
		request.setAttribute("checkingRef", laboreditform.getRef());
		request.setAttribute("addendum_id", "0");

		/*
		 * System.out.println("Activity_Id ::: " +
		 * laboreditform.getActivity_Id()); System.out.println("Labor_Id ::: " +
		 * laboreditform.getLaborid()); System.out.println("Labor Status ::: " +
		 * laborStatus); System.out.println("Labor Status list ::: " +
		 * laborStatusList); System.out.println("Addendum Id ::: " + "0");
		 * System.out.println("From  ::::" + from );
		 * System.out.println("job id ::::" + jobid );
		 */

		// End of the Menu Code for Resource Material

		return (mapping.findForward("labortabularedit"));
	}
}
