/*This Action Class extends the base  Struts Action class to perform multiple
 * operations Save,Update, View etc.
 * 
 * Method Name: execute
 * Method Argument: 4 Argument ActionMapping,ActionForm,HttpServletRequest,HttpServletResponse
 * Method Description: MSA Tabular Form with existing MSAs is fetched , default combo are populated with values.
 * see:  MSAListForm class (com.mind.formbean.PM.MSAListForm)
 * 		 MSA class         (com.mind.dao.PM)
 * 15/05/2007  Modify the whole class to see only the list of all MSA's
 */

package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Logindao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.MSAdao;
import com.mind.formbean.PM.AppendixListForm;

public class AppendixUpdateAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AppendixListForm appendixlistform = (AppendixListForm) form;
		boolean chkOtherOwner = false;
		int appendix_size = 0;
		String MSA_Id = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String loginusername = (String) session.getAttribute("username");
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		int ownerListSize = 0;
		Codelist codes = new Codelist();
		ArrayList all_appendices = new ArrayList();
		String sortqueryclause = "";
		String tempStatus = "";
		String tempOwner = "";
		String monthAppendix = null;
		String weekAppendix = null;
		String ownerType = "appendix";
		String defaultvalue = request.getParameter("firstCall");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "Appendix",
				"Appendix List", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		String checkRole = Logindao.checkRoleForUser(loginuserid, "ESA",
				getDataSource(request, "ilexnewDB"));
		byDefaultValue(defaultvalue, session, checkRole);

		if (request.getParameter("home") != null) {
			appendixlistform.setAppendixSelectedStatus(null);
			appendixlistform.setAppendixSelectedOwners(null);
			appendixlistform.setPrevSelectedStatus("");
			appendixlistform.setPrevSelectedOwner("");
			appendixlistform.setAppendixOtherCheck(null);
			appendixlistform.setMonthWeekCheck(null);
			setValuesWhenReset(session, checkRole);
		}

		if (request.getParameter("opendiv") != null)
			request.setAttribute("opendiv", request.getParameter("opendiv")
					.toString());

		if (request.getParameter("month") != null) {
			session.setAttribute("monthAppendix", request.getParameter("month")
					.toString());
			monthAppendix = request.getParameter("month").toString();
			session.removeAttribute("weekAppendix");
			appendixlistform.setMonthWeekCheck("month");
		}

		if (request.getParameter("week") != null) {
			session.setAttribute("weekAppendix", request.getParameter("week")
					.toString());
			weekAppendix = request.getParameter("week").toString();
			session.removeAttribute("monthAppendix");
			appendixlistform.setMonthWeekCheck("week");
		}

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "MSA", "MSA List",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("MSA_Id") != null) {
			MSA_Id = request.getParameter("MSA_Id");
			appendixlistform.setMSA_Id(MSA_Id);
		}
		if (request.getAttribute("MSA_Id") != null) {
			MSA_Id = (String) request.getAttribute("MSA_Id");
			appendixlistform.setMSA_Id(MSA_Id);
		}
		if (request.getParameter("msaId") != null) {
			MSA_Id = request.getParameter("msaId");
			appendixlistform.setMSA_Id(MSA_Id);
		}
		if (request.getAttribute("msaId") != null) {
			MSA_Id = (String) request.getAttribute("msaId");
			appendixlistform.setMSA_Id(MSA_Id);
		}

		if (session.getAttribute("appendix_sort") != null)
			sortqueryclause = (String) session.getAttribute("appendix_sort");

		if (appendixlistform.getAppendixOtherCheck() != null)
			session.setAttribute("appendixOtherCheck", appendixlistform
					.getAppendixOtherCheck().toString());

		// When Status Check Boxes are Clicked :::::::::::::::::
		if (appendixlistform.getAppendixSelectedStatus() != null) {
			for (int i = 0; i < appendixlistform.getAppendixSelectedStatus().length; i++) {
				selectedStatus = selectedStatus + "," + "'"
						+ appendixlistform.getAppendixSelectedStatus(i) + "'";
				tempStatus = tempStatus + ","
						+ appendixlistform.getAppendixSelectedStatus(i);
			}

			selectedStatus = selectedStatus.substring(1,
					selectedStatus.length());
			selectedStatus = "(" + selectedStatus + ")";
		}

		// When Owner Check Boxes are Clicked :::::::::::::::::
		if (appendixlistform.getAppendixSelectedOwners() != null) {
			for (int j = 0; j < appendixlistform.getAppendixSelectedOwners().length; j++) {
				selectedOwner = selectedOwner + ","
						+ appendixlistform.getAppendixSelectedOwners(j);
				tempOwner = tempOwner + ","
						+ appendixlistform.getAppendixSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if (selectedOwner.equals("0")) {
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" + selectedOwner + ")";
		}

		if (appendixlistform.getGo() != null
				&& !appendixlistform.getGo().equals("")) {
			session.setAttribute("appendixSelectedOwners", selectedOwner);
			session.setAttribute("appSelectedOwners", selectedOwner);
			session.setAttribute("sessionMSAId", appendixlistform.getMSA_Id());
			session.setAttribute("appendixTempOwner", tempOwner);
			session.setAttribute("appendixSelectedStatus", selectedStatus);
			session.setAttribute("appSelectedStatus", selectedStatus);
			session.setAttribute("appendixTempStatus", tempStatus);

			if (appendixlistform.getMonthWeekCheck() != null) {
				if (appendixlistform.getMonthWeekCheck().equals("week")) {
					session.removeAttribute("monthAppendix");
					session.setAttribute("weekAppendix", "0");
					weekAppendix = session.getAttribute("weekAppendix")
							.toString();
				}
				if (appendixlistform.getMonthWeekCheck().equals("month")) {
					session.removeAttribute("weekAppendix");
					session.setAttribute("monthAppendix", "0");
					monthAppendix = session.getAttribute("monthAppendix")
							.toString();
				}
				if (appendixlistform.getMonthWeekCheck().equals("clear")) {
					session.removeAttribute("weekAppendix");
					session.removeAttribute("monthAppendix");
					weekAppendix = null;
					monthAppendix = null;
				}

			}
		} else {
			if (session.getAttribute("appendixSelectedOwners") != null) {
				selectedOwner = session.getAttribute("appendixSelectedOwners")
						.toString();
				appendixlistform.setAppendixSelectedOwners(session
						.getAttribute("appendixTempOwner").toString()
						.split(","));
			}
			if (session.getAttribute("appendixSelectedStatus") != null) {
				selectedStatus = session.getAttribute("appendixSelectedStatus")
						.toString();
				appendixlistform.setAppendixSelectedStatus(session
						.getAttribute("appendixTempStatus").toString()
						.split(","));
			}
			if (session.getAttribute("appendixOtherCheck") != null)
				appendixlistform.setAppendixOtherCheck(session.getAttribute(
						"appendixOtherCheck").toString());

			if (session.getAttribute("monthAppendix") != null) {
				session.removeAttribute("weekAppendix");
				monthAppendix = session.getAttribute("monthAppendix")
						.toString();
				appendixlistform.setMonthWeekCheck("month");
			}
			if (session.getAttribute("weekAppendix") != null) {
				session.removeAttribute("monthAppendix");
				weekAppendix = session.getAttribute("weekAppendix").toString();
				appendixlistform.setMonthWeekCheck("week");
			}

		}

		if (!chkOtherOwner) {
			appendixlistform.setPrevSelectedStatus(selectedStatus);
			appendixlistform.setPrevSelectedOwner(selectedOwner);
		}
		ownerList = MSAdao.getOwnerList(loginuserid,
				appendixlistform.getAppendixOtherCheck(), ownerType,
				appendixlistform.getMSA_Id(),
				getDataSource(request, "ilexnewDB"));
		statusList = MSAdao.getStatusList("pm_appendix",
				getDataSource(request, "ilexnewDB"));

		all_appendices = Appendixdao.getAppendixList(
				getDataSource(request, "ilexnewDB"),
				appendixlistform.getMSA_Id(), sortqueryclause, monthAppendix,
				weekAppendix, appendixlistform.getPrevSelectedStatus(),
				appendixlistform.getPrevSelectedOwner());
		if (all_appendices.size() > 0) {
			codes.setAppendixlist(all_appendices);
			appendix_size = all_appendices.size();
		}

		if (ownerList.size() > 0)
			ownerListSize = ownerList.size();

		String msaname = Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"), MSA_Id);
		request.setAttribute("msaname", msaname);

		if (session.getAttribute("monthAppendix") != null
				|| session.getAttribute("weekAppendix") != null
				|| session.getAttribute("appendixSelectedOwners") != null
				|| session.getAttribute("appendixSelectedStatus") != null) {
			request.setAttribute("viewSelectorMessage", "true");
		}
		// request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", statusList);
		request.setAttribute("ownerlist", ownerList);
		request.setAttribute("ownerListSize", ownerListSize + "");
		request.setAttribute("Size", new Integer(appendix_size));
		request.setAttribute("MSA_Id", MSA_Id);
		request.setAttribute("codeslist", codes);

		return (mapping.findForward("appendixtabularpage"));
	}

	public static void byDefaultValue(String defaultvalue, HttpSession session,
			String checkRole) {
		String valueofdefault = defaultvalue;
		if (valueofdefault != null) {
			if (session.getAttribute("appendixSelectedStatus") == null
					&& session.getAttribute("appendixSelectedOwners") == null) {
				setValuesWhenReset(session, checkRole);
			}
		}
	}

	public static void setValuesWhenReset(HttpSession session, String checkRole) {
		session.removeAttribute("appendixSelectedOwners");
		session.removeAttribute("appendixSelectedStatus");
		session.removeAttribute("appendixTempOwner");
		session.removeAttribute("appendixTempStatus");
		session.removeAttribute("appendixOtherCheck");
		session.removeAttribute("monthAppendix");
		session.removeAttribute("weekAppendix");
		session.removeAttribute("appSelectedOwners");
		session.removeAttribute("appSelectedStatus");
		session.removeAttribute("sessionMSAId");
		session.setAttribute("appselectedStatus", "('D','R','P','T','S','A')");
		session.setAttribute("appendixSelectedStatus",
				"('D','R','P','T','S','A')");
		session.setAttribute("appendixTempStatus", "D,R,P,T,S,A");
		if (!checkRole.equals("Y")) {
			session.setAttribute("appselectedOwners", "(" + "%".toString()
					+ ")");
			session.setAttribute("appendixSelectedOwners", "(" + "%".toString()
					+ ")");
			session.setAttribute("appendixTempOwner", "%".toString());
		} else {
			session.setAttribute("appselectedOwners", "("
					+ session.getAttribute("userid").toString() + ")");
			session.setAttribute("appendixSelectedOwners", "("
					+ session.getAttribute("userid").toString() + ")");
			session.setAttribute("appendixTempOwner",
					session.getAttribute("userid").toString());

		}

	}
}
