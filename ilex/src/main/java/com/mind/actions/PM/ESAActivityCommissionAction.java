package com.mind.actions.PM;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.ESAActivityCommissionBean;
import com.mind.business.pm.ESAOrganizer;
import com.mind.formbean.PM.ESAActivityCommissionForm;
import com.mind.util.WebUtil;

public class ESAActivityCommissionAction extends
		com.mind.common.IlexDispatchAction {

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		Map<String, Object> map;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		ESAActivityCommissionForm esaActCommissionForm = (ESAActivityCommissionForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");

		setRequestElements(esaActCommissionForm, request);

		if (esaActCommissionForm.getSave() != null) {
			int retVal = saveCommissionDetails(esaActCommissionForm,
					loginuserid, ds);
			request.setAttribute("saveStatus", String.valueOf(retVal));
		}

		if (esaActCommissionForm.getJobid() != null
				&& !esaActCommissionForm.getJobid().trim().equals("")) {
			map = ESAOrganizer.setCommissionPageMenu(
					esaActCommissionForm.getJobid(), loginuserid, ds);
			WebUtil.copyMapToRequest(request, map);
		}

		setESAActivityCommissionForm(esaActCommissionForm, ds, request);

		return mapping.findForward("commissionManagement");
	}

	/**
	 * Description - Sets the request elements.
	 * 
	 * @param esaActCommissionForm
	 *            - ESA Activity Commission form.
	 * @param request
	 *            - request object.
	 */
	private static void setRequestElements(
			ESAActivityCommissionForm esaActCommissionForm,
			HttpServletRequest request) {
		if (request.getParameter("appendixId") != null) {
			esaActCommissionForm.setAppendixId(request
					.getParameter("appendixId"));
		}

		if (request.getParameter("jobId") != null) {
			esaActCommissionForm.setJobid(request.getParameter("jobId"));
		}

		if (request.getParameter("jobRef") != null) {
			esaActCommissionForm.setJobRef(request.getParameter("jobRef"));
		}
	}

	/**
	 * Description - Sets ESA Activity Commission form.
	 * 
	 * @param esaActCommissionForm
	 *            - ESA Activity Commission form.
	 * @param ds
	 *            - dataSource.
	 * @param request
	 *            - request object.
	 */
	private static void setESAActivityCommissionForm(
			ESAActivityCommissionForm esaActCommissionForm, DataSource ds,
			HttpServletRequest request) {
		ESAActivityCommissionBean esaActCommBean = new ESAActivityCommissionBean();
		esaActCommBean.setAppendixId(esaActCommissionForm.getAppendixId());
		esaActCommBean.setJobid(esaActCommissionForm.getJobid());

		ESAOrganizer.setEsaActivityCommissionBean(esaActCommBean, ds);

		request.setAttribute("esaAgentSize", esaActCommBean.getAgents().size());
		request.setAttribute("activityListSize", esaActCommBean
				.getEsaActivityList().size());

		String jobType = ESAOrganizer.getJobType(
				esaActCommBean.getAppendixId(), esaActCommBean.getJobid(), ds);
		if (!jobType.equals("")) {
			request.setAttribute("jobType", jobType);
		}
		ESAOrganizer.ConvertToCommissionForm(esaActCommissionForm,
				esaActCommBean);
	}

	/**
	 * Save commission form information for each ESA-Activity pair.
	 * 
	 * @param form
	 *            - ESA Activity Commission form.
	 * @param userId
	 *            - login user id
	 * @param ds
	 *            - data source.
	 * 
	 * @return - return code (int).
	 */
	private int saveCommissionDetails(ESAActivityCommissionForm form,
			String userId, DataSource ds) {
		ESAActivityCommissionBean esaActCommBean = new ESAActivityCommissionBean();

		esaActCommBean.setAppendixId(form.getAppendixId());
		esaActCommBean.setJobid(form.getJobid());

		ESAOrganizer.convertFormToBean(form, esaActCommBean);
		int retval = ESAOrganizer
				.saveCommissionBean(esaActCommBean, userId, ds);

		return retval;
	}

}
