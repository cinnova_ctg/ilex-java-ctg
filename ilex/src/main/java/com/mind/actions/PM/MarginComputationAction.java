package com.mind.actions.PM;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.PM.MarginComputationForm;

public class MarginComputationAction extends com.mind.common.IlexAction {
	
	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception
	{
		MarginComputationForm margincomputationform = ( MarginComputationForm ) form;
		
		HttpSession session = request.getSession( true );
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
	
		if(request.getParameter("resourceId") != null)
		{	
			margincomputationform.setResourceName(Resourcedao.getResoucePoolName(request.getParameter("resourceId"), getDataSource(request , "ilexnewDB")));
		}
			
		if( request.getParameter( "estUnitCost" ) != null )
		{
			margincomputationform.setEstUnitCost( request.getParameter( "estUnitCost" ) );
		}
		
		if( request.getParameter( "position" ) != null )
		{
			margincomputationform.setPosition( request.getParameter( "position" ) );
		}
		
		if( request.getParameter( "size" ) != null )
		{
			margincomputationform.setSize( request.getParameter( "size" ) );
			margincomputationform.setLaborResourceSize(null);
			margincomputationform.setMaterialResourceSize(null);
			margincomputationform.setFreightResourceSize(null);
			margincomputationform.setTravelResourceSize(null);
		}
		
		if( request.getParameter( "labor_resource_size" ) != null )
		{
			margincomputationform.setLaborResourceSize( request.getParameter( "labor_resource_size" ) );
			margincomputationform.setMaterialResourceSize(null);
			margincomputationform.setFreightResourceSize(null);
			margincomputationform.setTravelResourceSize(null);
		}
		if( request.getParameter( "material_resource_size" ) != null )
		{
			margincomputationform.setMaterialResourceSize( request.getParameter( "material_resource_size" ) );
			margincomputationform.setTravelResourceSize(null);
			margincomputationform.setFreightResourceSize(null);
			margincomputationform.setLaborResourceSize(null);
		}
		if( request.getParameter( "freight_resource_size" ) != null )
		{
			margincomputationform.setFreightResourceSize( request.getParameter( "freight_resource_size" ) );
			margincomputationform.setMaterialResourceSize(null);
			margincomputationform.setTravelResourceSize(null);
			margincomputationform.setLaborResourceSize(null);
		}
		if( request.getParameter( "travel_resource_size" ) != null )
		{
			margincomputationform.setTravelResourceSize( request.getParameter( "travel_resource_size" ) );
			margincomputationform.setMaterialResourceSize(null);
			margincomputationform.setFreightResourceSize(null);
			margincomputationform.setLaborResourceSize(null);
		}
		return mapping.findForward( "success" );
	}
}
