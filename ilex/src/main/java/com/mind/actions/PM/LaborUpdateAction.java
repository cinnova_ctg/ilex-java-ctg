package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.LaborResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.AM.LaborResourceForm;



public class LaborUpdateAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		String sortqueryclause = "";
		String activityname = "";
		String check_netmedx_appendix="N"; // for transit field
		String Job_Id ="";
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		/*if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			laborresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = laborresourceform.getActivity_Id();
		}
		
		/* fot tramsit start*/
		check_netmedx_appendix = Resourcedao.checkNetmedxAppendix( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "check_netmedx_appendix" , check_netmedx_appendix );
		laborresourceform.setChecknetmedx( check_netmedx_appendix );
		/* fot tramsit end*/
		
		
		if( request.getParameter( "from" ) != null )
		{
			laborresourceform.setFromflag( request.getParameter( "from" ) );
			laborresourceform.setDashboardid( request.getParameter( "jobid" ) );
		
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		Job_Id = Activitydao.getJobid( getDataSource( request , "ilexnewDB" ) , Activity_Id );
		laborresourceform.setJobid( Job_Id );
		
		
		//		added by hamid
		request.setAttribute( "jobid" , Job_Id );
		
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activity_Id );
		laborresourceform.setLaborlist( Resourcedao.getResourcecombolist( "labor" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		laborresourceform.setAddendum_id( "0" );
		laborresourceform.setBacktoactivity( "detailactivity" );
		laborresourceform.setChkaddendum( "View" );
		laborresourceform.setChkaddendumdetail( "View" );
		
		ArrayList laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
		
		/* for transit field start */
		
		String savedtransitresource[] = Resourcedao.getSavedTransitResources( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setChecktransit( savedtransitresource );
		/* for transit field end */
		
		/* for checkbox check start */
		
		String checkTempResources[] = Resourcedao.checkTempResources( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setCheck( checkTempResources );
		/* for checkbox check end */
		
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
			
		}
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setAppendix_id( idname.getId() );
		laborresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		
		laborresourceform.setJobname(Jobdao.getoplevelname(laborresourceform.getActivity_Id() , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		request.setAttribute( "refresh" , "true" );
		
		return( mapping.findForward( "labortabularpage" ) );
	}
	
	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
	
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		String Activity_Id = "";
		LaborResource laborresource = null;
		int resource_size = 0;
		String status = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		ArrayList laborresourcelist = new ArrayList();
		String flag1 = "";
		String flag2 = "";
		String flag3 = "";
		String flag4 = "";
		String activityname = "";
		String check_netmedx_appendix = "N"; // for transit field
		int addflag = 0;
		
		String sortqueryclause = "";
		
		flag1 = laborresourceform.getBacktoactivity();
		flag2 = laborresourceform.getChkaddendum();
		flag3 = laborresourceform.getChkaddendumdetail();
		flag4 = laborresourceform.getAddendum_id();
		NameId idname = null;
		Activity_Id = laborresourceform.getActivity_Id();
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		/*multiple resouce select start end */
		/* fot tramsit start*/
		check_netmedx_appendix = Resourcedao.checkNetmedxAppendix( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "check_netmedx_appendix" , check_netmedx_appendix );
		laborresourceform.setChecknetmedx( check_netmedx_appendix );
		/* fot tramsit end*/
		
		if( laborresourceform.getCheck() !=  null )
		{
			int checklength = laborresourceform.getCheck().length;	
		
			int []index = new int[checklength];
			int k = 0;
			int checklengthtransit = 0;
			int length = 0;
			if( laborresourceform.getLaborid() != null )
			{
				length = laborresourceform.getLaborid().length;			
			}
			
			//int []index = new int[length];
			
			if( laborresourceform.getChecktransit() != null )
			{
				checklengthtransit = laborresourceform.getChecktransit().length;	
							
			}
			
			int t = 0;
			
			if( checklength > 0 )
			{	
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( laborresourceform.getCheck( i ).substring( 1 , laborresourceform.getCheck( i ).length() ).equals( laborresourceform.getLaborid( j ) ) )
						{	
							index[k] = j;
							k++;
							break;
						}
						
						/*if( laborresourceform.getCheck(i).equals( laborresourceform.getLaborid( j ) ) )
						{	
							index[k] = j;
							k++;
						}*/
					}
				}
			}
			
			if( checklength > 0 )                               
			{
				for( int i = 0; i < index.length; i++ )
				{
					laborresource = new LaborResource();
					
					if( laborresourceform.getLaborid( index[i] ).indexOf( "~" ) > 0 )
					{
						laborresource.setLaborid( laborresourceform.getLaborid( index[i] ).substring( 0 , laborresourceform.getLaborid( index[i] ).indexOf( "~" ) ) );
						laborresource.setClr_detail_id( laborresourceform.getLaborid( index[i] ).substring( laborresourceform.getLaborid( index[i] ).indexOf( "~" ) + 1 , laborresourceform.getLaborid( index[i]).length() ) );
					}
					else
					{
						laborresource.setLaborid( laborresourceform.getLaborid ( index[i] ) );
						laborresource.setClr_detail_id( "0" );
					}
					
					laborresource.setLaborcostlibid( laborresourceform.getLaborcostlibid ( index[i] ) );
					
					laborresource.setActivity_Id( laborresourceform.getActivity_Id() );
					
					laborresource.setQuantityhours( laborresourceform.getQuantityhours ( index[i] ) );
					
					laborresource.setPrevquantityhours( laborresourceform.getPrevquantityhours( index[i] ) );
					
					if( laborresourceform.getChkaddendum().equals( "View" ) )
					{
						laborresource.setLabortype( "L" );
					}
					
					if( laborresourceform.getChkaddendum().equals( "ViewAddendum" ) )
					{
						laborresource.setLabortype( "AL" );
					}
					
					if( laborresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
					{
						laborresource.setLabortype( "CL" );
					}
					
					if( laborresourceform.getChkaddendum().equals( "Viewinscope" ) )
					{
						laborresource.setLabortype( "IL" );
					}
					
					laborresource.setEstimatedhourlybasecost( laborresourceform.getEstimatedhourlybasecost( index[i] ) );
					
					laborresource.setProformamargin( laborresourceform.getProformamargin( index[i] ) );
					
					laborresource.setTransit( laborresourceform.getTransit( index[i] ) );
					
					
					/* for multiple resource select start*/
					laborresource.setCnspartnumber( laborresourceform.getCnspartnumber( index[i] ) );
					laborresource.setFlag( laborresourceform.getFlag( index[i] ) );
					if( laborresourceform.getFlag( index[i] ).equals( "P" ) )
					{
						int updateflag = Resourcedao.updatelaborresource( laborresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "updateflag" , ""+updateflag );
					}
					if( laborresourceform.getFlag( index[i] ).equals( "T" ) )
					{
						addflag = Resourcedao.addlaborresource( laborresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "addflag" , ""+addflag );
					}
					/* for multiple resource select end*/		
					/*int updateflag = Resourcedao.updatelaborresource( laborresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );*/
				}
			}
		}
		
		if( laborresourceform.getNewlaborid() != null )
		{
			laborresource = new LaborResource();
			laborresource.setLaborid( laborresourceform.getNewlabornamecombo() );
			laborresource.setActivity_Id( laborresourceform.getActivity_Id() );
			laborresource.setCnspartnumber( laborresourceform.getNewcnspartnumber() );
			laborresource.setQuantityhours( laborresourceform.getNewquantityhours() );
			
			if(laborresourceform.getNewchecktransit()!=null)
			{
				laborresource.setTransit( laborresourceform.getNewtransit() );
			}
			else
			{
				laborresource.setTransit( "N" );
			}
			
			if( laborresourceform.getChkaddendum().equals( "View" ) )
			{
				laborresource.setLabortype( "L" );
			}
			
			if( laborresourceform.getChkaddendum().equals( "ViewAddendum" ) )
			{
				laborresource.setLabortype( "AL" );
			}
			
			if( laborresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
			{
				laborresource.setLabortype( "CL" );
			}
			
			if( laborresourceform.getChkaddendum().equals( "Viewinscope" ) )
			{
				laborresource.setLabortype( "IL" );
			}
			
			
			laborresource.setEstimatedhourlybasecost( laborresourceform.getNewestimatedhourlybasecost() );
			laborresource.setProformamargin( laborresourceform.getNewproformamargin() );
			
			addflag = Resourcedao.addlaborresource( laborresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		
		if( laborresourceform.getFromflag().equals( "jobdashboard" ) || laborresourceform.getFromflag().equals( "jobdashboard_resource" ) )
		{
			request.setAttribute( "jobid" , laborresourceform.getDashboardid() );
			return mapping.findForward( "jobdashboard" );
		}
		
		laborresourceform.reset( mapping , request);
		
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activity_Id );
		
		//laborresourceform.setRef( null );
		laborresourceform.setLaborlist( Resourcedao.getResourcecombolist( "labor" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		laborresourceform.setBacktoactivity( flag1 );
		laborresourceform.setChkaddendum( flag2 );
		laborresourceform.setChkaddendumdetail( flag3 );
		laborresourceform.setAddendum_id( flag4 );
				
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		
		if( laborresourceform.getChkaddendum().equals( "View" ) )
		{
			laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			/* for transit field start */
			String savedtransitresource[] = Resourcedao.getSavedTransitResources( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setChecktransit( savedtransitresource );
			/* for transit field end */
			laborresourceform.setRef( "View" );
		}
		
		if( laborresourceform.getChkaddendum().equals( "Viewinscope" ) )
		{
			
			status = com.mind.dao.PRM.Activitydao.getCurrentstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IL" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			/* for transit field start */
			String savedtransitresource[] = Resourcedao.getSavedTransitResources( Activity_Id , "IL" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setChecktransit( savedtransitresource );
			/* for transit field end */
			laborresourceform.setRef( "Viewinscope" );
		}
		
			
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setAppendix_id( idname.getId() );
		laborresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setJobname(Jobdao.getoplevelname(laborresourceform.getActivity_Id() , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		laborresourceform.setJobid( Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "labortabularpage" ) );
	}
	
	
	
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		int deleteflag = -1;
		String tempflagforref = "";
		ArrayList laborresourcelist = new ArrayList();
		String activityname = "";
		String check_netmedx_appendix = "N"; // for transit field
		HttpSession session = request.getSession( true );
		String sortqueryclause = "";
		String userid = ( String )session.getAttribute( "userid" );
		
		NameId idname = null;
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			laborresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = laborresourceform.getActivity_Id();
		}
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		/* fot tramsit start*/
		check_netmedx_appendix = Resourcedao.checkNetmedxAppendix( Activity_Id , getDataSource( request , "ilexnewDB" ));
		request.setAttribute( "check_netmedx_appendix" , check_netmedx_appendix );
		laborresourceform.setChecknetmedx( check_netmedx_appendix );
		/* fot tramsit end*/
		
		deleteflag = Resourcedao.deleteresource( request.getParameter( "Labor_Id" ), userid, getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activity_Id );
		
		if( request.getParameter( "type" ) != null )
		{
			tempflagforref = request.getParameter( "type" );
		}
		
		if( tempflagforref.equals( "View" ) )
		{
			laborresourceform.setAddendum_id( "0" );
			laborresourceform.setBacktoactivity( "detailactivity" );
			laborresourceform.setChkaddendum( "View" );
			laborresourceform.setChkaddendumdetail( "View" );
			laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			/* for transit field start */
			String savedtransitresource[] = Resourcedao.getSavedTransitResources( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setChecktransit( savedtransitresource );
			/* for transit field end */
			laborresourceform.setRef( "View" );
			
		}
		
		if( tempflagforref.equals( "Viewinscope" ) )
		{
			laborresourceform.setBacktoactivity( "inscopedetailactivity" );
			laborresourceform.setChkaddendum( "Viewinscope" );
			laborresourceform.setChkaddendumdetail( "Viewinscope" );
			laborresourceform.setAddendum_id( "0" );
			laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IL" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			/* for transit field start */
			String savedtransitresource[]=Resourcedao.getSavedTransitResources(Activity_Id , "IL" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setChecktransit(savedtransitresource);
			/* for transit field end */
			laborresourceform.setRef( "Viewinscope" );
		}
		
		laborresourceform.setLaborlist( Resourcedao.getResourcecombolist( "labor" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setAppendix_id( idname.getId() );
		laborresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setJobname(Jobdao.getoplevelname(laborresourceform.getActivity_Id() , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setJobid( Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "labortabularpage" ) );
	}	
	
	
	public ActionForward Viewinscope( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
				
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		String sortqueryclause = "";
		String activityname = "";
		String check_netmedx_appendix="N"; // for transit field
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		String Job_Id ="";
		NameId idname = null;
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			laborresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = laborresourceform.getActivity_Id();
		}
		
		//System.out.println("Labor update Action ZZZZZZ Activity_id "+Activity_Id);
		
		/* fot tramsit start*/
		check_netmedx_appendix = Resourcedao.checkNetmedxAppendix( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "check_netmedx_appendix" , check_netmedx_appendix );
		laborresourceform.setChecknetmedx( check_netmedx_appendix );
		/* fot tramsit end*/
		
		if( request.getParameter( "from" ) != null )
		{
			laborresourceform.setFromflag( request.getParameter( "from" ) );
			laborresourceform.setDashboardid( request.getParameter( "jobid" ) );
		
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		Job_Id = Activitydao.getJobid(getDataSource( request , "ilexnewDB" ) , Activity_Id );
		
		laborresourceform.setJobid( Job_Id );
		
		
		//		added by hamid
		request.setAttribute( "jobid" ,Job_Id);
		
		//Start :Added By Amit
		if( request.getParameter( "fromPage" ) != null )
		{
			laborresourceform.setFromPage( ( String )request.getParameter( "fromPage" ) );
		}
		
		//End : Added By Amit
		
		
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activity_Id );
		laborresourceform.setLaborlist( Resourcedao.getResourcecombolist( "labor" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		laborresourceform.setBacktoactivity( "inscopedetailactivity" );
		laborresourceform.setChkaddendum( "Viewinscope" );
		laborresourceform.setChkaddendumdetail( "Viewinscope" );
		laborresourceform.setAddendum_id( "0" );
		
		ArrayList laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IL" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
		/* for transit field start */
	
		String savedtransitresource[] = Resourcedao.getSavedTransitResources( Activity_Id , "IL" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setChecktransit( savedtransitresource );
		/* for transit field end */
		/* for checkbox check start */
		
		String checkTempResources[] = Resourcedao.checkTempResources( Activity_Id , "IL" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setCheck( checkTempResources );
		/* for checkbox check end */
		
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setAppendix_id( idname.getId() );
		laborresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		status = com.mind.dao.PRM.Activitydao.getCurrentstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setJobname( Jobdao.getoplevelname( laborresourceform.getActivity_Id() , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "labortabularpage" ) );
	}

	
	/**
	 * @ This method for mass approve of labor resources
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward massApprove( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		LaborResource tempresource = null;
		int changestatusflag = -1; 
		String Activitylibrary_Id ="";
		String activityname = "";
		String Activity_Id = "";
		String sortqueryclause = "";
		String typenet = "";
		int resource_size = 0;
		
		String checkforresourcelink = "";
		String status = "";
		
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( userid , "Activity" , "View Resources" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			laborresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = laborresourceform.getActivity_Id();
		}
		
		/*if( request.getParameter( "from" ) != null )
		{
			laborresourceform.setFromflag( request.getParameter( "from" ) );
			laborresourceform.setDashboardid( request.getParameter( "jobid" ) );
			
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}*/
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			
		}
		
			if( laborresourceform.getCheck() !=  null )
			{
				int checklength = laborresourceform.getCheck().length;	 
		
				int []index = new int[checklength];
				int k = 0;
				
				if( checklength > 0 )
				{	int length = laborresourceform.getLaborid().length;
					for( int i = 0; i < checklength; i++ )
					{
						for( int j = 0; j < length; j++ )
						{
							if( laborresourceform.getCheck( i ).substring( 1 , laborresourceform.getCheck( i ).length() ).equals( laborresourceform.getLaborid( j ) ) )
							{	
								index[k] = j;
								k++;
							}
						}
					}
				}
			
				if( checklength > 0 )                               
				{
					for( int i = 0; i < index.length; i++ )
					{
						tempresource = new LaborResource();
						tempresource.setLaborid( laborresourceform.getLaborid( index[i] ) );
						tempresource.setActivity_Id( laborresourceform.getActivity_Id() );
						//materialresource.setMaterialtype( "M" );
						changestatusflag = ChangeStatus.Status( "" , "" , "" , "" , tempresource.getLaborid() , "pm_resL" , "A" , "N" , getDataSource( request , "ilexnewDB" ),"" );
					}
				}
			}
			
			NameId idname = null;
			laborresourceform.setNewquantityhours( "0.0000" );
			laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
			laborresourceform.setNewstatus( "Draft" );
			laborresourceform.setActivity_Id( Activity_Id );
			laborresourceform.setLaborlist( Resourcedao.getResourcecombolist( "labor" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
			
			laborresourceform.setBacktoactivity( "detailactivity" );       //flag for backtoactivity
			laborresourceform.setChkaddendum( "View" );                    //flag for labor , freight , travel tabular
			laborresourceform.setChkaddendumdetail( "View" );              //flag for materialdetail
			laborresourceform.setAddendum_id( "0" );
			
			laborresourceform.setRef( "View" );  
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setAppendix_id( idname.getId() );
			laborresourceform.setAppendixname( idname.getName() );
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setMsa_id( idname.getId() );
			laborresourceform.setMsaname( idname.getName() );
			
			ArrayList laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			
			
			if( laborresourcelist.size() > 0 )
			{
				resource_size = laborresourcelist.size();
			}
			
			
			/* for checkbox check start */
			
			String checkTempResources[] = Resourcedao.checkTempResources( Activity_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setCheck( checkTempResources );
			/* for checkbox check end */
			
			status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			
			request.setAttribute( "act_status" , status );
			request.setAttribute( "activityname" , activityname );
			
			request.setAttribute( "Size" , new Integer ( resource_size ) );
			request.setAttribute( "laborresourcelist" , laborresourcelist );
			
			request.setAttribute( "refresh" , "true" );
			request.setAttribute( "Activity_Id" , Activity_Id );
		
		return( mapping.findForward( "labortabularpage" ) );
	}
	
	
	/**
	 * @ This method is for mass delete of Resource
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward massDelete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		LaborResource tempresource = null;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		int deleteflag = -1;
		String tempflagforref = "";
		ArrayList laborresourcelist = new ArrayList();
		String activityname = "";
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			laborresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = laborresourceform.getActivity_Id();
		}
		//Start :Added By Amit
		if( laborresourceform.getCheck()!=  null )
		{
			
			int checklength = laborresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = laborresourceform.getLaborid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( laborresourceform.getCheck( i ).substring( 1 , laborresourceform.getCheck( i ).length() ).equals( laborresourceform.getLaborid( j ) ) )
						{	
							index[k] = j;
							k++;
						}
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				for( int i = 0; i < index.length; i++ )
				{
					tempresource = new LaborResource();
					tempresource.setLaborid( laborresourceform.getLaborid( ( index[i] ) ) );
					
					deleteflag = Resourcedao.deleteresource( tempresource.getLaborid(), userid,  getDataSource( request , "ilexnewDB" ) );
				}
			}
		}
		//End :
		
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activity_Id );
		
		if( request.getParameter( "type" ) != null )
		{
			tempflagforref = request.getParameter( "type" );
		}
		
		if( tempflagforref.equals( "View" ) )
		{
			laborresourceform.setAddendum_id( "0" );
			laborresourceform.setBacktoactivity( "detailactivity" );
			laborresourceform.setChkaddendum( "View" );
			laborresourceform.setChkaddendumdetail( "View" );
			laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "L" , "%" , sortqueryclause , request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			/* for transit field start */
			String savedtransitresource[] = Resourcedao.getSavedTransitResources( Activity_Id , "L" , "%" , sortqueryclause , request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setChecktransit( savedtransitresource );
			/* for transit field end */
			laborresourceform.setRef( "View" );
		}
		
		if( tempflagforref.equals( "Viewinscope" ) )
		{	
			laborresourceform.setBacktoactivity( "inscopedetailactivity" );
			laborresourceform.setChkaddendum( "Viewinscope" );
			laborresourceform.setChkaddendumdetail( "Viewinscope" );
			laborresourceform.setAddendum_id( "0" );
			laborresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IL" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			/* for transit field start */
			String savedtransitresource[] = Resourcedao.getSavedTransitResources( Activity_Id , "IL" , "%" , sortqueryclause , request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			laborresourceform.setChecktransit( savedtransitresource );
			/* for transit field end */
			laborresourceform.setRef( "Viewinscope" );
		}
		
		laborresourceform.setLaborlist( Resourcedao.getResourcecombolist( "labor" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setAppendix_id( idname.getId() );
		laborresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		laborresourceform.setRef( "View" );  
		
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setJobname( Jobdao.getoplevelname( laborresourceform.getActivity_Id() , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		laborresourceform.setJobid( Activitydao.getJobid( getDataSource( request , "ilexnewDB" ) , Activity_Id ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		request.setAttribute( "refresh" , "true" );
	
		return( mapping.findForward( "labortabularpage" ) );
	}
	
	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "pmresL_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "pmresL_sort" );
		return queryclause;
	}
}

