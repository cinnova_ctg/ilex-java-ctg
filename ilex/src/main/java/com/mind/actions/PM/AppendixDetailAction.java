package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.Appendix;
import com.mind.common.dao.Menu;
import com.mind.common.dao.Upload;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.MSAdao;
import com.mind.formbean.PM.AppendixDetailForm;

public class AppendixDetailAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AppendixDetailForm appendixdetailform = (AppendixDetailForm) form;
		Appendix appendix = new Appendix();
		ArrayList uploadeddocs = new ArrayList();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		// Start of View Selector code//
		// Variables Added to add the common functionality for the View
		// Selector.

		boolean chkOtherOwner = false;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		int ownerListSize = 0;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "appendix";
		String defaultvalue = request.getParameter("firstCall");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Appendix_Id") != null)
			appendixdetailform.setAppendix_Id(request
					.getParameter("Appendix_Id"));

		if (request.getParameter("MSA_Id") != null)
			appendixdetailform.setMsa_id(request.getParameter("MSA_Id"));

		if (request.getAttribute("MSA_Id") != null) {
			appendixdetailform.setMsa_id((String) request
					.getAttribute("MSA_Id"));
			request.setAttribute("MSA_Id", appendixdetailform.getMsa_id());
		}

		if (request.getAttribute("Appendix_Id") != null) {
			appendixdetailform.setAppendix_Id((String) request
					.getAttribute("Appendix_Id"));
			request.setAttribute("Appendix_Id",
					appendixdetailform.getAppendix_Id());
		}

		if (request.getParameter("home") != null) // This is called when reset
													// button is pressed from
													// the view selector.
		{
			appendixdetailform.setAppendixSelectedStatus(null);
			appendixdetailform.setAppendixSelectedOwners(null);
			appendixdetailform.setPrevSelectedStatus("");
			appendixdetailform.setPrevSelectedOwner("");
			appendixdetailform.setAppendixOtherCheck(null);
			appendixdetailform.setMonthWeekCheck(null);
			return (mapping.findForward("appendixtabularpage"));
		}

		if (request.getParameter("opendiv") != null) // This is used to make the
														// div remain open when
														// other check box is
														// checked.
			request.setAttribute("opendiv", request.getParameter("opendiv")
					.toString());

		// These two parameter month/week are checked are kept in session. (
		// View Images)
		if (request.getParameter("month") != null) {
			session.setAttribute("monthAppendix", request.getParameter("month")
					.toString());

			session.removeAttribute("weekAppendix");
			appendixdetailform.setMonthWeekCheck("month");
		}

		if (request.getParameter("week") != null) {
			session.setAttribute("weekAppendix", request.getParameter("week")
					.toString());

			session.removeAttribute("monthAppendix");
			appendixdetailform.setMonthWeekCheck("week");
		}

		if (appendixdetailform.getAppendixOtherCheck() != null) // OtherCheck
																// box status is
																// kept in
																// session.
			session.setAttribute("appendixOtherCheck", appendixdetailform
					.getAppendixOtherCheck().toString());

		// When Status Check Boxes are Clicked :::::::::::::::::
		if (appendixdetailform.getAppendixSelectedStatus() != null) {
			for (int i = 0; i < appendixdetailform.getAppendixSelectedStatus().length; i++) {
				selectedStatus = selectedStatus + "," + "'"
						+ appendixdetailform.getAppendixSelectedStatus(i) + "'";
				tempStatus = tempStatus + ","
						+ appendixdetailform.getAppendixSelectedStatus(i);
			}

			selectedStatus = selectedStatus.substring(1,
					selectedStatus.length());
			selectedStatus = "(" + selectedStatus + ")";
		}

		// When Owner Check Boxes are Clicked :::::::::::::::::
		if (appendixdetailform.getAppendixSelectedOwners() != null) {
			for (int j = 0; j < appendixdetailform.getAppendixSelectedOwners().length; j++) {
				selectedOwner = selectedOwner + ","
						+ appendixdetailform.getAppendixSelectedOwners(j);
				tempOwner = tempOwner + ","
						+ appendixdetailform.getAppendixSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if (selectedOwner.equals("0")) {
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" + selectedOwner + ")";
		}

		if (appendixdetailform.getGo() != null
				&& !appendixdetailform.getGo().equals("")) {
			session.setAttribute("appendixSelectedOwners", selectedOwner);
			session.setAttribute("appSelectedOwners", selectedOwner);
			session.setAttribute("appendixTempOwner", tempOwner);
			session.setAttribute("appendixSelectedStatus", selectedStatus);
			session.setAttribute("appSelectedStatus", selectedStatus);
			session.setAttribute("appendixTempStatus", tempStatus);

			if (appendixdetailform.getMonthWeekCheck() != null) {
				if (appendixdetailform.getMonthWeekCheck().equals("week")) {
					session.removeAttribute("monthAppendix");
					session.setAttribute("weekAppendix", "0");

				}
				if (appendixdetailform.getMonthWeekCheck().equals("month")) {
					session.removeAttribute("weekAppendix");
					session.setAttribute("monthAppendix", "0");

				}
				if (appendixdetailform.getMonthWeekCheck().equals("clear")) {
					session.removeAttribute("weekAppendix");
					session.removeAttribute("monthAppendix");

				}

			}
			return (mapping.findForward("appendixtabularpage"));
		} else {
			if (session.getAttribute("appendixSelectedOwners") != null) {
				selectedOwner = session.getAttribute("appendixSelectedOwners")
						.toString();
				appendixdetailform.setAppendixSelectedOwners(session
						.getAttribute("appendixTempOwner").toString()
						.split(","));
			}
			if (session.getAttribute("appendixSelectedStatus") != null) {
				selectedStatus = session.getAttribute("appendixSelectedStatus")
						.toString();
				appendixdetailform.setAppendixSelectedStatus(session
						.getAttribute("appendixTempStatus").toString()
						.split(","));
			}
			if (session.getAttribute("appendixOtherCheck") != null)
				appendixdetailform.setAppendixOtherCheck(session.getAttribute(
						"appendixOtherCheck").toString());

			if (session.getAttribute("monthAppendix") != null) {
				session.removeAttribute("weekAppendix");
				appendixdetailform.setMonthWeekCheck("month");
			}
			if (session.getAttribute("weekAppendix") != null) {
				session.removeAttribute("monthAppendix");
				appendixdetailform.setMonthWeekCheck("week");
			}

		}

		if (!chkOtherOwner) {
			appendixdetailform.setPrevSelectedStatus(selectedStatus);
			appendixdetailform.setPrevSelectedOwner(selectedOwner);
		}
		ownerList = MSAdao.getOwnerList(loginuserid,
				appendixdetailform.getAppendixOtherCheck(), ownerType,
				appendixdetailform.getMsa_id(),
				getDataSource(request, "ilexnewDB"));
		statusList = MSAdao.getStatusList("pm_appendix",
				getDataSource(request, "ilexnewDB"));
		if (ownerList.size() > 0)
			ownerListSize = ownerList.size();

		request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", statusList);
		request.setAttribute("ownerlist", ownerList);
		request.setAttribute("ownerListSize", ownerListSize + "");

		// End of View Selector Code//

		appendix = Appendixdao.getAppendix("%",
				appendixdetailform.getAppendix_Id(),
				getDataSource(request, "ilexnewDB"));

		if (appendix.getAppendix_Id() != null) {

			appendixdetailform.setMsa_id(appendix.getMsa_Id());
			request.setAttribute("MSA_Id", appendixdetailform.getMsa_id());
			appendixdetailform.setMsaname(Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					appendixdetailform.getMsa_id()));

			appendixdetailform.setAppendix_Id(appendix.getAppendix_Id());

			appendixdetailform.setName(appendix.getName());
			appendixdetailform.setAppendixtype(appendix.getAppendixtype());
			request.setAttribute("appendixType",
					appendixdetailform.getAppendixtype());

			appendixdetailform.setCnsPOC(appendix.getCnsPOC());
			appendixdetailform.setSiteno(appendix.getSiteno());
			appendixdetailform.setStatus(appendix.getStatus());
			appendixdetailform.setEstimatedcost(appendix.getEstimatedcost());
			appendixdetailform.setExtendedprice(appendix.getExtendedprice());
			appendixdetailform.setProformamargin(appendix.getProformamargin());
			appendixdetailform.setCustomerdivision(appendix
					.getCustomerdivision());
			appendixdetailform.setReqplasch(appendix.getReqplasch());
			appendixdetailform.setEffplasch(appendix.getEffplasch());
			appendixdetailform.setSchdays(appendix.getSchdays());

			appendixdetailform.setDraftreviews(appendix.getDraftreviews());
			appendixdetailform
					.setBusinessreviews(appendix.getBusinessreviews());
			appendixdetailform
					.setCustPocbusiness(appendix.getCustPocbusiness());
			appendixdetailform
					.setCustPoccontract(appendix.getCustPoccontract());
			appendixdetailform.setCustPocbilling(appendix.getCustPocbilling());
			appendixdetailform.setLastcomment(appendix.getLastcomment());
			appendixdetailform.setLastchangeby(appendix.getLastchangeby());
			appendixdetailform.setLastchangedate(appendix.getLastchangedate());

			appendixdetailform.setLastappendixmodifiedby(appendix
					.getLastappendixmodifiedby());
			appendixdetailform.setLastappendixmodifieddate(appendix
					.getLastappendixmodifieddate());
			appendixdetailform.setUnionupliftfactor(appendix
					.getUnionupliftfactor());
			appendixdetailform.setMsp(appendix.getMsp());
			appendixdetailform.setTravelAuthorized(appendix
					.getTravelAuthorized());
			appendixdetailform.setCheckAppendix(appendix.getCheckType());
			appendixdetailform.setTestAppendix(appendix.getTestAppendix());
			appendixdetailform.setAppendixTypeBreakout(appendix
					.getAppendixTypeBreakout());

			/* for uploaded files details start */
			appendixdetailform.setFile_upload_check(Upload.getFileUploadcheck(
					"%", appendix.getAppendix_Id(), "Appendix",
					getDataSource(request, "ilexnewDB")));
			/* for uploaded files details start */

			String list = Menu.getStatus("pm_appendix", appendix.getStatus()
					.charAt(0), appendixdetailform.getMsa_id(),
					appendixdetailform.getAppendix_Id(), "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("list", list);

			String list2 = Menu.getStatus("pm_appendix_new", appendix
					.getStatus().charAt(0), appendixdetailform.getMsa_id(),
					appendixdetailform.getAppendix_Id(), "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("new_list", list2);

			/* for uploaded files details start */
			uploadeddocs = Upload.getuploadedfiles("%",
					appendixdetailform.getAppendix_Id(), "Appendix",
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("uploadeddocs", uploadeddocs);
			/* for uploaded files details start */

			if (request.getAttribute("addcommentflag") != null) {
				request.setAttribute("addcommentflag",
						request.getAttribute("addcommentflag"));
			}

			if (request.getAttribute("changestatusflag") != null) {
				request.setAttribute("changestatusflag",
						request.getAttribute("changestatusflag"));
			}

			if (request.getAttribute("uploadflag") != null) {
				request.setAttribute("uploadflag",
						request.getAttribute("uploadflag"));
			}

			if (request.getAttribute("emailflag") != null) {
				request.setAttribute("emailflag",
						request.getAttribute("emailflag"));
			}

			if (request.getAttribute("emailchangestatusflag") != null) {
				request.setAttribute("emailchangestatusflag",
						request.getAttribute("emailchangestatusflag"));
			}

			String addendumlist = Appendixdao.getAddendumsIdName(
					appendixdetailform.getAppendix_Id(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("addendumlist", addendumlist);

			String addendumlist2 = Appendixdao.getAddendumsIdName(
					appendixdetailform.getAppendix_Id(),
					getDataSource(request, "ilexnewDB"), true);
			request.setAttribute("new_addendumlist", addendumlist2);

			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(appendixdetailform.getAppendix_Id(),
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("latestaddendumid", addendumid);

			return (mapping.findForward("appendixdetailpage"));
		} else {
			// request.setAttribute( "changestatusflag" , "true" );
			request.setAttribute("MSA_Id", appendixdetailform.getMsa_id());
			request.setAttribute("appendixType",
					appendixdetailform.getAppendixtype());
			return (mapping.findForward("appendixtabularpage"));
		}

	}

}
