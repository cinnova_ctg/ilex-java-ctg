/**
 * Copyright (C) 2005 MIND All rights reserved. The information contained here
 * in is confidential and proprietary to MIND and forms the part of MIND Project
 * : ILEX Description	: This is an dispatch action class used for managing job
 * and showing job detail in Proposal and Project Manager.
 *
 */
package com.mind.actions.PM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Job;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.formbean.PM.JobDetailForm;

/**
 * Methods : detailjob, detailaddendumjob
 */
public class JobDetailAction extends com.mind.common.IlexDispatchAction {

    /**
     * This method managing appendix job and showing appendix job detail in
     * Proposal Manager.
     *
     * @param mapping Object of ActionMapping
     * @param form Object of ActionForm
     * @param request Object of HttpServletRequest
     * @param response Object of HttpServletResponse
     * @return ActionForward This method returns object of ActionForward.
     */
    public ActionForward detailjob(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        JobDetailForm jobdetailform = (JobDetailForm) form;
        String uploadflag = "";
        Job job = new Job();
        String list = "";
        String menu_list = "";

        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire"));   //Check for session expired
        }
        if (request.getParameter("sitelistid") != null && request.getParameter("sitelistid") != "") {
            int siteId = 0;
            int Job_Id = 0;
            int jobSiteId = 0;

            if (request.getParameter("jobid") != null) {
                Job_Id = Integer.parseInt(request.getParameter("jobid"));
            }

            if (request.getParameter("siteId") != null) {
                jobSiteId = Integer.parseInt(request.getParameter("siteId"));
            }

            siteId = Integer.parseInt(request.getParameter("sitelistid"));
            int retval = JobDashboarddao.addNewSite(siteId, Job_Id, loginuserid, getDataSource(request, "ilexnewDB"), jobSiteId);
        }

        if (request.getAttribute("jobid") != null) {
            jobdetailform.setJob_Id(request.getAttribute("jobid").toString());
        }

        if (request.getParameter("Job_Id") != null) {
            jobdetailform.setJob_Id(request.getParameter("Job_Id"));

            request.setAttribute("Job_Id", jobdetailform.getJob_Id());
        }

        if (request.getAttribute("Job_Id") != null) {
            jobdetailform.setJob_Id((String) request.getAttribute("Job_Id"));
            request.setAttribute("Job_Id", jobdetailform.getJob_Id());
        }

        job = Jobdao.getJob("%", jobdetailform.getJob_Id(), "%", "%", getDataSource(request, "ilexnewDB"));

        jobdetailform.setJob_Id(job.getJob_Id());
        jobdetailform.setAppendix_Id(job.getAppendix_Id());
        request.setAttribute("Appendix_Id", jobdetailform.getAppendix_Id());

        jobdetailform.setName(job.getName());
        jobdetailform.setJob_type(job.getJobtype());

        jobdetailform.setUnion(job.getUnion());

        jobdetailform.setStatus(job.getStatus());
        jobdetailform.setLocality_uplift(job.getLocality_uplift());

        jobdetailform.setEstimated_cnsfieldlaborcost(job.getEstimated_cnsfieldlaborcost());
        jobdetailform.setEstimated_contractfieldlaborcost(job.getEstimated_contractfieldlaborcost());
        jobdetailform.setEstimated_materialcost(job.getEstimated_materialcost());
        jobdetailform.setEstimated_freightcost(job.getEstimated_freightcost());
        jobdetailform.setEstimated_travelcost(job.getEstimated_travelcost());
        jobdetailform.setEstimated_totalcost(job.getEstimated_totalcost());
        jobdetailform.setExtendedprice(job.getExtendedprice());
        jobdetailform.setProformamargin(job.getProformamargin());
        jobdetailform.setSite_name(job.getSite_name());
        jobdetailform.setSite_address(job.getSite_address());
        jobdetailform.setSite_no(job.getSite_number());
        jobdetailform.setSite_city(job.getSite_city());
        jobdetailform.setSite_state(job.getSite_state());
        jobdetailform.setSite_zipcode(job.getSite_zipcode());
        jobdetailform.setSite_POC(job.getSite_POC());
        jobdetailform.setUnion_uplift_factor(job.getUnion_uplift_factor());
        jobdetailform.setLastcomment(job.getLastcomment());
        jobdetailform.setLastchangeby(job.getLastchangeby());
        jobdetailform.setLastchangedate(job.getLastchangedate());
        jobdetailform.setLastjobmodifiedby(job.getLastjobmodifiedby());
        jobdetailform.setLastjobmodifieddate(job.getLastjobmodifieddate());
        jobdetailform.setChkaddendum("detailjob");   // chk addendum flag
        jobdetailform.setChkaddendumactivity("View");   // chk addendum flag for activity
        jobdetailform.setEstimated_start_schedule(job.getEstimated_start_schedule());
        jobdetailform.setEstimated_complete_schedule(job.getEstimated_complete_schedule());
        jobdetailform.setCNS_POC(job.getCNS_POC());
        jobdetailform.setInstallation_POC(job.getInstallation_POC());
        jobdetailform.setMsa_id(job.getMsa_Id());
        jobdetailform.setMsaname(job.getMsaname());

        jobdetailform.setAppendixname(Appendixdao.getAppendixname(getDataSource(request, "ilexnewDB"), jobdetailform.getAppendix_Id()));
        jobdetailform.setCustref(job.getCustref());
        jobdetailform.setAppendixtype(ViewList.getAppendixtypedesc(jobdetailform.getAppendix_Id(), getDataSource(request, "ilexnewDB")));
        int esaCount = Jobdao.getAppendixESACount(jobdetailform.getAppendix_Id(), getDataSource(request, "ilexnewDB"));
        request.setAttribute("esaCount", esaCount + "");

        if (jobdetailform.getJob_type().equals("Default")) {
            menu_list = "<li><a href=\"#?Type=Job&Job_Id=" + jobdetailform.getJob_Id() + "&Status='D'\">Draft</a></li>";
            list = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id=" + jobdetailform.getJob_Id() + "&Status='D'\"" + "," + "0";
        } else {
            menu_list = Menu.getStatus("pm_job_new", job.getStatus().charAt(0), "", "", jobdetailform.getJob_Id(), "", loginuserid, getDataSource(request, "ilexnewDB"));
            list = Menu.getStatus("pm_job", job.getStatus().charAt(0), "", "", jobdetailform.getJob_Id(), "", loginuserid, getDataSource(request, "ilexnewDB"));
        }
        request.setAttribute("list", list);
        request.setAttribute("menu_list", menu_list);

        if (request.getAttribute("uploadflag") != null) {
            request.setAttribute("uploadflag", (String) request.getAttribute("uploadflag"));
        }

        if (request.getAttribute("changestatusflag") != null) {
            request.setAttribute("changestatusflag", request.getAttribute("changestatusflag"));
        }

        return (mapping.findForward("jobdetailpage"));

    }

    /**
     * This method managing addendum job and showing addendum job detail in
     * Project Manager.
     *
     * @param mapping Object of ActionMapping
     * @param form Object of ActionForm
     * @param request Object of HttpServletRequest
     * @param response Object of HttpServletResponse
     * @return ActionForward This method returns object of ActionForward.
     */
    public ActionForward detailaddendumjob(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        JobDetailForm jobdetailform = (JobDetailForm) form;
        String uploadflag = "";
        Job job = new Job();
        String list = "";
        String copyaddendumflag = "";
        String appendixname = "";
        //added by shailja
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");

        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire"));   //Check for session expired
        }		//end

        if (request.getParameter("Job_Id") != null) {
            jobdetailform.setJob_Id(request.getParameter("Job_Id"));
            request.setAttribute("Job_Id", jobdetailform.getJob_Id());
        }

        if (request.getAttribute("Job_Id") != null) {
            jobdetailform.setJob_Id((String) request.getAttribute("Job_Id"));
            request.setAttribute("Job_Id", jobdetailform.getJob_Id());
        }

        if (request.getParameter("addendum_id") != null) {
            jobdetailform.setAddendum_id(request.getParameter("addendum_id"));
        }

        if (request.getAttribute("addendum_id") != null) {
            jobdetailform.setAddendum_id(request.getAttribute("addendum_id").toString());
        }

        //job = Jobdao.getAddendumJob( "%" , jobdetailform.getJob_Id()  , getDataSource( request , "ilexnewDB" ) ); // jobdetailform.getAppendix_Id() is Addendum Id
        job = Jobdao.getJob("%", jobdetailform.getJob_Id(), "AJ", "%", getDataSource(request, "ilexnewDB"));
        jobdetailform.setJob_Id(job.getJob_Id());
        jobdetailform.setAppendix_Id(job.getAppendix_Id());
        request.setAttribute("Appendix_Id", jobdetailform.getAppendix_Id());

        jobdetailform.setName(job.getName());
        jobdetailform.setJob_type(job.getJobtype());

        jobdetailform.setUnion(job.getUnion());

        jobdetailform.setStatus(job.getStatus());
        jobdetailform.setLocality_uplift(job.getLocality_uplift());

        jobdetailform.setEstimated_cnsfieldlaborcost(job.getEstimated_cnsfieldlaborcost());
        jobdetailform.setEstimated_contractfieldlaborcost(job.getEstimated_contractfieldlaborcost());
        jobdetailform.setEstimated_materialcost(job.getEstimated_materialcost());
        jobdetailform.setEstimated_freightcost(job.getEstimated_freightcost());
        jobdetailform.setEstimated_travelcost(job.getEstimated_travelcost());
        jobdetailform.setEstimated_totalcost(job.getEstimated_totalcost());
        jobdetailform.setExtendedprice(job.getExtendedprice());
        jobdetailform.setProformamargin(job.getProformamargin());
        jobdetailform.setSite_name(job.getSite_name());
        jobdetailform.setSite_address(job.getSite_address());
        jobdetailform.setSite_no(job.getSite_number());
        jobdetailform.setSite_city(job.getSite_city());
        jobdetailform.setSite_state(job.getSite_state());
        jobdetailform.setSite_zipcode(job.getSite_zipcode());
        jobdetailform.setSite_POC(job.getSite_POC());
        jobdetailform.setUnion_uplift_factor(job.getUnion_uplift_factor());
        jobdetailform.setLastcomment(job.getLastcomment());
        jobdetailform.setLastchangeby(job.getLastchangeby());
        jobdetailform.setLastchangedate(job.getLastchangedate());
        jobdetailform.setLastjobmodifiedby(job.getLastjobmodifiedby());
        jobdetailform.setLastjobmodifieddate(job.getLastjobmodifieddate());

        jobdetailform.setChkaddendum("detailaddendumjob");   // chk addendum flag
        jobdetailform.setChkaddendumactivity("ViewAddendum");   // chk addendum flag

        jobdetailform.setEstimated_start_schedule(job.getEstimated_start_schedule());
        jobdetailform.setEstimated_complete_schedule(job.getEstimated_complete_schedule());
        jobdetailform.setCNS_POC(job.getCNS_POC());
        jobdetailform.setInstallation_POC(job.getInstallation_POC());
        jobdetailform.setMsa_id(job.getMsa_Id());
        jobdetailform.setMsaname(job.getMsaname());

        jobdetailform.setAppendixname(Appendixdao.getAppendixname(getDataSource(request, "ilexnewDB"), jobdetailform.getAppendix_Id()));
        jobdetailform.setCustref(job.getCustref());

        if (jobdetailform.getStatus().equals("Draft")) {
            list = "\"Approved\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id=" + jobdetailform.getAddendum_id() + "&ref=Fromjoblevel&ref1=detailaddendumjob&Job_Id=" + jobdetailform.getJob_Id() + "&Status=A\"" + "," + "0";
        }

        if (jobdetailform.getStatus().equals("Approved")) {
            list = "\"Draft\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id=" + jobdetailform.getAddendum_id() + "&ref=Fromjoblevel&ref1=detailaddendumjob&Job_Id=" + jobdetailform.getJob_Id() + "&Status=D\"" + "," + "0";
        }

        request.setAttribute("list", list);

        if (request.getAttribute("uploadflag") != null) {
            request.setAttribute("uploadflag", (String) request.getAttribute("uploadflag"));
        }

        if (request.getParameter("copyaddendumflag") != null) {
            request.setAttribute("copyaddendumflag", request.getAttribute("copyaddendumflag"));
        }

        if (request.getParameter("changestatusflag") != null) {
            request.setAttribute("changestatusflag", request.getParameter("changestatusflag"));
        }

        /* by hamid for appendixname setting*/
        appendixname = Jobdao.getAppendixname(getDataSource(request, "ilexnewDB"), com.mind.dao.PRM.Jobdao.getAppendixid(jobdetailform.getJob_Id(), getDataSource(request, "ilexnewDB")));
        request.setAttribute("appendixname", appendixname);
        request.setAttribute("appendixid", com.mind.dao.PRM.Jobdao.getAppendixid(jobdetailform.getJob_Id(), getDataSource(request, "ilexnewDB")));
        /* by hamid*/

        return (mapping.findForward("jobdetailpage"));

    }

    /**
     * This method managing addendum job and showing addendum job detail in
     * Project Manager.
     *
     * @param mapping Object of ActionMapping
     * @param form Object of ActionForm
     * @param request Object of HttpServletRequest
     * @param response Object of HttpServletResponse
     * @return ActionForward This method returns object of ActionForward.
     */
    public ActionForward detailchangeorderjob(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        JobDetailForm jobdetailform = (JobDetailForm) form;
        String uploadflag = "";
        Job job = new Job();
        String list = "";
        String copyaddendumflag = "";
        String appendixname = "";

        if (request.getParameter("Job_Id") != null) {
            jobdetailform.setJob_Id(request.getParameter("Job_Id"));
            request.setAttribute("Job_Id", jobdetailform.getJob_Id());
        }

        if (request.getAttribute("Job_Id") != null) {
            jobdetailform.setJob_Id((String) request.getAttribute("Job_Id"));
            request.setAttribute("Job_Id", jobdetailform.getJob_Id());
        }

        if (request.getParameter("addendum_id") != null) {
            jobdetailform.setAddendum_id(request.getParameter("addendum_id"));
        }

        if (request.getAttribute("addendum_id") != null) {
            jobdetailform.setAddendum_id(request.getAttribute("addendum_id").toString());
        }

        job = Jobdao.getJob("%", jobdetailform.getJob_Id(), "CJ", "%", getDataSource(request, "ilexnewDB"));
        jobdetailform.setJob_Id(job.getJob_Id());
        jobdetailform.setAppendix_Id(job.getAppendix_Id());
        request.setAttribute("Appendix_Id", jobdetailform.getAppendix_Id());

        jobdetailform.setName(job.getName());
        jobdetailform.setJob_type(job.getJobtype());

        jobdetailform.setUnion(job.getUnion());

        jobdetailform.setStatus(job.getStatus());
        jobdetailform.setLocality_uplift(job.getLocality_uplift());

        jobdetailform.setEstimated_cnsfieldlaborcost(job.getEstimated_cnsfieldlaborcost());
        jobdetailform.setEstimated_contractfieldlaborcost(job.getEstimated_contractfieldlaborcost());
        jobdetailform.setEstimated_materialcost(job.getEstimated_materialcost());
        jobdetailform.setEstimated_freightcost(job.getEstimated_freightcost());
        jobdetailform.setEstimated_travelcost(job.getEstimated_travelcost());
        jobdetailform.setEstimated_totalcost(job.getEstimated_totalcost());
        jobdetailform.setExtendedprice(job.getExtendedprice());
        jobdetailform.setProformamargin(job.getProformamargin());
        jobdetailform.setSite_name(job.getSite_name());
        jobdetailform.setSite_address(job.getSite_address());
        jobdetailform.setSite_no(job.getSite_number());
        jobdetailform.setSite_city(job.getSite_city());
        jobdetailform.setSite_state(job.getSite_state());
        jobdetailform.setSite_zipcode(job.getSite_zipcode());
        jobdetailform.setSite_POC(job.getSite_POC());
        jobdetailform.setUnion_uplift_factor(job.getUnion_uplift_factor());
        jobdetailform.setLastcomment(job.getLastcomment());
        jobdetailform.setLastchangeby(job.getLastchangeby());
        jobdetailform.setLastchangedate(job.getLastchangedate());
        jobdetailform.setLastjobmodifiedby(job.getLastjobmodifiedby());
        jobdetailform.setLastjobmodifieddate(job.getLastjobmodifieddate());

        jobdetailform.setChkaddendum("detailchangeorderjob");   // chk addendum flag
        jobdetailform.setChkaddendumactivity("ViewChangeorder");   // chk addendum flag

        jobdetailform.setEstimated_start_schedule(job.getEstimated_start_schedule());
        jobdetailform.setEstimated_complete_schedule(job.getEstimated_complete_schedule());
        jobdetailform.setCNS_POC(job.getCNS_POC());
        jobdetailform.setInstallation_POC(job.getInstallation_POC());
        jobdetailform.setMsa_id(job.getMsa_Id());
        jobdetailform.setMsaname(job.getMsaname());

        jobdetailform.setAppendixname(Appendixdao.getAppendixname(getDataSource(request, "ilexnewDB"), jobdetailform.getAppendix_Id()));
        jobdetailform.setCustref(job.getCustref());

        if (jobdetailform.getStatus().equals("Draft")) {
            list = "\"Approved\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id=" + jobdetailform.getAddendum_id() + "&ref=Fromjoblevel&ref1=detailchangeorderjob&Job_Id=" + jobdetailform.getJob_Id() + "&Status=A\"" + "," + "0";
        }

        if (jobdetailform.getStatus().equals("Approved")) {
            list = "\"Draft\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id=" + jobdetailform.getAddendum_id() + "&ref=Fromjoblevel&ref1=detailchangeorderjob&Job_Id=" + jobdetailform.getJob_Id() + "&Status=D\"" + "," + "0";
        }

        request.setAttribute("list", list);

        if (request.getAttribute("uploadflag") != null) {
            request.setAttribute("uploadflag", (String) request.getAttribute("uploadflag"));
        }

        if (request.getParameter("copyaddendumflag") != null) {
            request.setAttribute("copyaddendumflag", request.getAttribute("copyaddendumflag"));
        }

        if (request.getParameter("changestatusflag") != null) {
            request.setAttribute("changestatusflag", request.getParameter("changestatusflag"));
        }

        /* by hamid for appendixname setting*/
        appendixname = Jobdao.getAppendixname(getDataSource(request, "ilexnewDB"), com.mind.dao.PRM.Jobdao.getAppendixid(jobdetailform.getJob_Id(), getDataSource(request, "ilexnewDB")));
        request.setAttribute("appendixname", appendixname);
        request.setAttribute("appendixid", com.mind.dao.PRM.Jobdao.getAppendixid(jobdetailform.getJob_Id(), getDataSource(request, "ilexnewDB")));
        /* by hamid*/

        return (mapping.findForward("jobdetailpage"));

    }

    public ActionForward inscopejob(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        JobDetailForm jobdetailform = (JobDetailForm) form;
        String uploadflag = "";
        Job job = new Job();
        String list = "";

        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");

        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire"));   //Check for session expired
        }
        if (request.getParameter("Job_Id") != null) {
            jobdetailform.setJob_Id(request.getParameter("Job_Id"));

            request.setAttribute("Job_Id", jobdetailform.getJob_Id());
        }

        if (request.getAttribute("Job_Id") != null) {
            jobdetailform.setJob_Id((String) request.getAttribute("Job_Id"));
            request.setAttribute("Job_Id", jobdetailform.getJob_Id());
        }

        job = Jobdao.getJob("%", jobdetailform.getJob_Id(), "%", "%", getDataSource(request, "ilexnewDB"));

        jobdetailform.setJob_Id(job.getJob_Id());
        jobdetailform.setAppendix_Id(job.getAppendix_Id());
        request.setAttribute("Appendix_Id", jobdetailform.getAppendix_Id());

        jobdetailform.setName(job.getName());
        jobdetailform.setJob_type(job.getJobtype());

        jobdetailform.setUnion(job.getUnion());

        jobdetailform.setStatus(job.getStatus());
        jobdetailform.setLocality_uplift(job.getLocality_uplift());

        jobdetailform.setEstimated_cnsfieldlaborcost(job.getEstimated_cnsfieldlaborcost());
        jobdetailform.setEstimated_contractfieldlaborcost(job.getEstimated_contractfieldlaborcost());
        jobdetailform.setEstimated_materialcost(job.getEstimated_materialcost());
        jobdetailform.setEstimated_freightcost(job.getEstimated_freightcost());
        jobdetailform.setEstimated_travelcost(job.getEstimated_travelcost());
        jobdetailform.setEstimated_totalcost(job.getEstimated_totalcost());
        jobdetailform.setExtendedprice(job.getExtendedprice());
        jobdetailform.setProformamargin(job.getProformamargin());
        jobdetailform.setSite_name(job.getSite_name());
        jobdetailform.setSite_address(job.getSite_address());
        jobdetailform.setSite_no(job.getSite_number());
        jobdetailform.setSite_city(job.getSite_city());
        jobdetailform.setSite_state(job.getSite_state());
        jobdetailform.setSite_zipcode(job.getSite_zipcode());
        jobdetailform.setSite_POC(job.getSite_POC());
        jobdetailform.setUnion_uplift_factor(job.getUnion_uplift_factor());
        jobdetailform.setLastcomment(job.getLastcomment());
        jobdetailform.setLastchangeby(job.getLastchangeby());
        jobdetailform.setLastchangedate(job.getLastchangedate());
        jobdetailform.setLastjobmodifiedby(job.getLastjobmodifiedby());
        jobdetailform.setLastjobmodifieddate(job.getLastjobmodifieddate());
        jobdetailform.setChkaddendum("inscopejob");   // chk addendum flag
        jobdetailform.setChkaddendumactivity("inscopeactivity");   // chk addendum flag for activity
        jobdetailform.setEstimated_start_schedule(job.getEstimated_start_schedule());
        jobdetailform.setEstimated_complete_schedule(job.getEstimated_complete_schedule());
        jobdetailform.setCNS_POC(job.getCNS_POC());
        jobdetailform.setInstallation_POC(job.getInstallation_POC());
        jobdetailform.setMsa_id(job.getMsa_Id());
        jobdetailform.setMsaname(job.getMsaname());
        jobdetailform.setAppendixname(Appendixdao.getAppendixname(getDataSource(request, "ilexnewDB"), jobdetailform.getAppendix_Id()));
        jobdetailform.setCustref(job.getCustref());
        jobdetailform.setAppendixtype(ViewList.getAppendixtypedesc(jobdetailform.getAppendix_Id(), getDataSource(request, "ilexnewDB")));

        /* added by hamid start*/
        if (jobdetailform.getStatus() != null) {
            if (jobdetailform.getStatus().length() > 0) {
                if (jobdetailform.getStatus().equals("Draft")) {
                    list = "\"Approved\"" + "," + "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id=" + jobdetailform.getJob_Id() + "&Status=A\"" + "," + "0";
                }

                if (jobdetailform.getStatus().equals("Approved")) {
                    list = "\"Draft\"" + "," + "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id=" + jobdetailform.getJob_Id() + "&Status=D\"" + "," + "0";
                }
            }
            /*shailja added SPR 397*/
            if (job.getJobtype().equalsIgnoreCase("Addendum")) {
                list = Menu.getStatus("Addendum", job.getStatus().charAt(0), "", "", jobdetailform.getJob_Id(), "", loginuserid, getDataSource(request, "ilexnewDB"));
            }
            /*end */

            //	if(jobdetailform.getStatus().length()>0)
            //	list = Menu.getStatus( "pm_job" , jobdetailform.getStatus().charAt( 0 ) , "" , "" , jobdetailform.getJob_Id() , "" , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
        }
        /* added by hamid end*/

        //list = "\"Approved\"" + "," + "\"#?Type=Job&Job_Id="+jobdetailform.getJob_Id()+"&Status='A'\"" + "," + "0";
        request.setAttribute("list", list);

        if (request.getAttribute("uploadflag") != null) {
            request.setAttribute("uploadflag", (String) request.getAttribute("uploadflag"));
        }

        if (request.getAttribute("changestatusflag") != null) {
            request.setAttribute("changestatusflag", request.getAttribute("changestatusflag"));
        }

        return (mapping.findForward("jobdetailpage"));

    }
}
