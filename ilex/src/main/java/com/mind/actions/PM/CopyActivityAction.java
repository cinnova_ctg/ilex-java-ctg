/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is an dispatch action class used for copying activities from default job to new job .
 *
 */
package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.Activity;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.formbean.PM.ActivityListForm;

public class CopyActivityAction extends com.mind.common.IlexDispatchAction {

    /**
     * This method is used to retrieve activities present under the default job
     *
     * @param mapping Object of ActionMapping
     * @param form Object of ActionForm
     * @param request Object of HttpServletRequest
     * @param response Object of HttpServletResponse
     * @return ActionForward This method returns object of ActionForward.
     */
    public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ActivityListForm activitylistform = (ActivityListForm) form;

        ArrayList all_activities = new ArrayList();
        String Job_Id = "";
        int activity_size = 0;
        String jobname = "";
        String appendixname = "";
        String appendix_Id = "";
        Activity activity = null;
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        boolean checkforoverheadactivity = false;

        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire"));   //Check for session expired
        }
        if (request.getParameter("Appendix_Id") != null) {
            appendix_Id = (String) request.getParameter("Appendix_Id");
            request.setAttribute("Appendix_Id", appendix_Id);
            activitylistform.setAppendixId(request.getParameter("Appendix_Id"));
        }

        if (request.getParameter("Job_Id") != null) {
            Job_Id = request.getParameter("Job_Id");
        }


        /*  This will be used when copy activities from job dashboard through New Activity link  */
        if (request.getParameter("from") != null) {
            appendix_Id = IMdao.getAppendixId(Job_Id, getDataSource(request, "ilexnewDB"));
            request.setAttribute("from", request.getParameter("from"));

            activitylistform.setFromflag(request.getParameter("from"));

        }
        /*-----------------------------------------------------------------------------------------*/

        activitylistform.setJob_Id(Job_Id);
        activitylistform.setJob_type(Jobdao.getJobtype(activitylistform.getJob_Id(), getDataSource(request, "ilexnewDB")));
        activitylistform.setActivity(ActivityLibrarydao.getActivitytype());

        if (activitylistform.getJob_type().equals("Default")) {
            String msa_Id = Activitydao.getmsaid(getDataSource(request, "ilexnewDB"), Job_Id);
            all_activities = Activitydao.getActivityListFromlib(getDataSource(request, "ilexnewDB"), msa_Id, "", Job_Id);
            checkforoverheadactivity = Activitydao.Checkdefaultjoboverhead(getDataSource(request, "ilexnewDB"), Job_Id);
            if (all_activities.size() > 0) {
                activity_size = all_activities.size();

                if (!checkforoverheadactivity) {
                    activity = new Activity();
                    activity.setActivity_Id("0");
                    activity.setName("FIXED OVERHEAD");
                    activity.setActivitytype("Overhead");
                    activity.setActivitytypecombo("V");
                    activity.setQuantity("1.0");
                    activity.setEstimated_materialcost(".00");
                    activity.setEstimated_cnsfieldlaborcost(".00");
                    activity.setEstimated_contractfieldlaborcost(".00");
                    activity.setEstimated_freightcost(".00");
                    activity.setEstimated_travelcost(".00");
                    activity.setEstimated_totalcost(".00");
                    activity.setExtendedprice(".00");
                    activity.setOverheadcost(".00");
                    activity.setListprice(".00");
                    activity.setProformamargin("0.00");
                    activity.setActivity_lib_Id("-1");
                    activity.setStatus("Draft");

                    all_activities.add(activity);

                    activity_size = all_activities.size();
                }

                for (int i = 0; i < activity_size; i++) {
                    ((Activity) all_activities.get(i)).setStatus("Draft");
                }
                request.setAttribute("copylibactivities", "true");
                request.setAttribute("codes", all_activities);

                /*activitylistform.setOverhead_Id( "true" );
                 activitylistform.setOverheadname( "FIXED OVERHEAD" );
                 activitylistform.setOverheadtype( "Overhead" );
                 activitylistform.setOverheadtypecombo( "V" );
                 activitylistform.setOverheadquantity( "1.0" );
                 activitylistform.setOverheadestimated_materialcost( "0.0" );
                 activitylistform.setOverheadestimated_cnsfieldlaborcost( "0.0" );
                 activitylistform.setOverheadestimated_contractfieldlaborcost( "0.0" );
                 activitylistform.setOverheadestimated_freightcost( "0.0" );
                 activitylistform.setOverheadestimated_travelcost( "0.0" );
                 activitylistform.setOverheadestimated_totalcost( "0.0" );
                 activitylistform.setOverheadextendedprice( "0.0" );
                 activitylistform.setOverhead_overheadcost( "0.0" );
                 activitylistform.setOverheadlistprice( "0.0" );
                 activitylistform.setOverheadproformamargin( "0.0" );
                 activitylistform.setOverheadstatus( "Draft" );*/
            }
        } else {

            all_activities = Jobdao.getdefaultjobactivities(getDataSource(request, "ilexnewDB"), appendix_Id, Job_Id);

            if (all_activities.size() > 0) {
                activity_size = all_activities.size();
                request.setAttribute("codes", all_activities);
                request.setAttribute("copydefaultjobactivity", "true");
            }
        }

        jobname = Activitydao.getJobname(getDataSource(request, "ilexnewDB"), Job_Id);
        activitylistform.setActivity_cost_type("A");

        request.setAttribute("Size", new Integer(activity_size));
        request.setAttribute("jobname", jobname);
        appendixname = Activitydao.getAppendixname(getDataSource(request, "ilexnewDB"), Job_Id);
        request.setAttribute("appendixname", appendixname);

        activitylistform.setJobName(Jobdao.getJobname(Job_Id, getDataSource(request, "ilexnewDB")));

        //Code Added to have the Job Detail Page on Copy Activity Page
        activitylistform.setAppendixname(Jobdao.getAppendixname(getDataSource(request, "ilexnewDB"), activitylistform.getAppendixId()));
        activitylistform.setMsa_id(Activitydao.getmsaid(getDataSource(request, "ilexnewDB"), activitylistform.getJob_Id()));
        activitylistform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(getDataSource(request, "ilexnewDB"), activitylistform.getMsa_id()));
        String[] jobStatusAndType = Jobdao.getJobStatusAndType("%", activitylistform.getJob_Id(), "%", "%", getDataSource(request, "ilexnewDB"));
        String jobStatus = jobStatusAndType[0];
        String jobType1 = jobStatusAndType[1];
        String jobStatusList = "";
        String menu_jobStatusList = "";
        String appendixType = "";

        if (jobType1.equals("Default")) {
            menu_jobStatusList = "<li><a href=\"#?Type=Job&Job_Id=" + activitylistform.getJob_Id() + "&Status='D'\">Draft</a></li>";
            jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id=" + activitylistform.getJob_Id() + "&Status='D'\"" + "," + "0";
        } else if (jobType1.equals("Newjob")) {
            menu_jobStatusList = Menu.getStatus("pm_job_new", jobStatus.charAt(0), "", "", activitylistform.getJob_Id(), "", loginuserid, getDataSource(request, "ilexnewDB"));
            jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0), "", "", activitylistform.getJob_Id(), "", loginuserid, getDataSource(request, "ilexnewDB"));
        } else if (jobType1.equals("inscopejob") && jobStatus.equals("Draft")) {
            menu_jobStatusList = "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id=" + activitylistform.getJob_Id() + "&Status=A\">Approved</a></li>";
            jobStatusList = "\"Approved\"" + "," + "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id=" + activitylistform.getJob_Id() + "&Status=A\"" + "," + "0";
        } else if (jobType1.equals("inscopejob") && jobStatus.equals("Draft")) {
            menu_jobStatusList = "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id=" + activitylistform.getJob_Id() + "&Status=D\">Draft</a></li>";
            jobStatusList = "\"Draft\"" + "," + "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id=" + activitylistform.getJob_Id() + "&Status=D\"" + "," + "0";
        } else if (jobType1.equals("Addendum")) {
            menu_jobStatusList = Menu.getStatus("Addendum_New", jobStatus.charAt(0), "", "", activitylistform.getJob_Id(), "", loginuserid, getDataSource(request, "ilexnewDB"));
            jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0), "", "", activitylistform.getJob_Id(), "", loginuserid, getDataSource(request, "ilexnewDB"));
        }

        appendixType = ViewList.getAppendixtypedesc(activitylistform.getAppendixId(), getDataSource(request, "ilexnewDB"));

        request.setAttribute("jobStatus", jobStatus);
        request.setAttribute("job_type", jobType1);
        request.setAttribute("appendixtype", appendixType);
        request.setAttribute("Job_Id", activitylistform.getJob_Id());
        request.setAttribute("Appendix_Id", activitylistform.getAppendixId());
        request.setAttribute("jobStatusList", jobStatusList);
        request.setAttribute("menu_jobStatusList", menu_jobStatusList);
        request.setAttribute("addendum_id", "0");

        if (jobType1.equals("Newjob") || jobType1.equals("Default")) {
            request.setAttribute("chkadd", "detailjob");
            request.setAttribute("chkaddendum", "detailjob");
            request.setAttribute("chkaddendumactivity", "View");
        }
        if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
            request.setAttribute("chkadd", "inscopejob");
            request.setAttribute("chkaddendum", "inscopejob");
            request.setAttribute("chkaddendumactivity", "inscopeactivity");
        }
        //End of Code
        int esaCount = Jobdao.getAppendixESACount(activitylistform.getAppendixId(), getDataSource(request, "ilexnewDB"));
        request.setAttribute("esaCount", esaCount + "");

        return mapping.findForward("copyactivitiespage");
    }

    public ActionForward Submit(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ActivityListForm activitylistform = (ActivityListForm) form;
        Activity activity = null;
        ArrayList all_activities = new ArrayList();
        String Job_Id = "";
        String appendixname = "";
        int activity_size = 0;
        int addflag = -1;
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        String fromflag = "";

        //if( request.getAttribute( "from" ) != null )
        //{
        //fromflag = request.getAttribute( "from" ).toString();
        //}
        if (activitylistform.getFromflag().equals("inscopejob")) {
            fromflag = activitylistform.getFromflag();
        }

        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire"));   //Check for session expired
        }
        String activity_cost_type = activitylistform.getActivity_cost_type();

        if (request.getParameter("Job_Id") != null) {
            Job_Id = request.getParameter("Job_Id");
            activitylistform.setJob_Id(Job_Id);
        } else {
            Job_Id = activitylistform.getJob_Id();
        }

        appendixname = Activitydao.getAppendixname(getDataSource(request, "ilexnewDB"), Job_Id);
        request.setAttribute("appendixname", appendixname);

        if (activitylistform.getCheck() != null) {
            int checklength = activitylistform.getCheck().length;

            int[] index = new int[checklength];
            int k = 0;

            if (checklength > 0) {
                int length = activitylistform.getActivity_Id().length;
                for (int i = 0; i < checklength; i++) {
                    for (int j = 0; j < length; j++) {
                        if (activitylistform.getCheck(i).equals(activitylistform.getActivity_Id(j))) {
                            index[k] = j;
                            k++;
                        }
                    }
                }
            }

            if (checklength > 0) {

                for (int i = 0; i < index.length; i++) {
                    activity = new Activity();
                    activity.setActivity_Id(activitylistform.getActivity_Id(index[i]));
                    activity.setJob_Id(Job_Id);
                    activity.setName(activitylistform.getName(index[i]));
                    activity.setActivitytypecombo(activitylistform.getActivitytypecombo(index[i]));
                    activity.setQuantity(activitylistform.getQuantity(index[i]));

                    activity.setEstimated_materialcost(activitylistform.getEstimated_materialcost(index[i]));
                    activity.setEstimated_cnsfieldlaborcost(activitylistform.getEstimated_cnsfieldlaborcost(index[i]));
                    activity.setEstimated_contractfieldlaborcost(activitylistform.getEstimated_contractfieldlaborcost(index[i]));
                    activity.setEstimated_freightcost(activitylistform.getEstimated_freightcost(index[i]));
                    activity.setEstimated_travelcost(activitylistform.getEstimated_travelcost(index[i]));
                    activity.setEstimated_totalcost(activitylistform.getEstimated_totalcost(index[i]));
                    activity.setExtendedprice(activitylistform.getExtendedprice(index[i]));
                    activity.setOverheadcost(activitylistform.getOverheadcost(index[i]));
                    activity.setListprice(activitylistform.getListprice(index[i]));
                    activity.setProformamargin(activitylistform.getProformamargin(index[i]));
                    activity.setStatus("D");
                    activity.setActivity_lib_Id(activitylistform.getActivity_lib_Id(index[i]));
                    activity.setAddendum_id("0");

                    String addSFlag = Activitydao.Addactivity(activity, "A", loginuserid, getDataSource(request, "ilexnewDB"), fromflag);
                    addflag = Integer.parseInt(addSFlag.substring(0, addSFlag.indexOf("|")));
                }

                request.setAttribute("Id1", Job_Id);
                request.setAttribute("copyactivityflag", "" + addflag);

                if (activitylistform.getFromflag().equals("inscopejob")) {
                    request.setAttribute("copyactivityflag", "" + addflag);
                    request.setAttribute("jobid", Job_Id);
                    return mapping.findForward("jobdashboard");

                }

                if ((activitylistform.getJob_type().equals("Default")) || (activitylistform.getJob_type().equals("Newjob"))) {
                    request.setAttribute("ref", "View");
                }

                if (activitylistform.getJob_type().equals("inscopejob")) {
                    request.setAttribute("ref", "inscopeactivity");
                }

                request.setAttribute("copyActivity", "copyActivity");
                request.setAttribute("type", "jobcopy");
            }

        }

        String appendix_Id = IMdao.getAppendixId(Job_Id, getDataSource(request, "ilexnewDB"));
        int esaCount = Jobdao.getAppendixESACount(appendix_Id, getDataSource(request, "ilexnewDB"));
        request.setAttribute("esaCount", esaCount + "");
        return mapping.findForward("activitytabularpageaftercopy");
    }
}
