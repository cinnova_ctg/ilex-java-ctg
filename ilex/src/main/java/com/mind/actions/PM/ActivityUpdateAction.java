/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is an dispatch action class used for managing activities in Proposal and Project Manager.      
 *
 */
package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.Activity;
import com.mind.common.EnvironmentSelector;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.formbean.PM.ActivityListForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ActivityUpdateAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ActivityUpdateAction.class);

	/**
	 * This method is used to view activities currently present under a job.If
	 * activities are not present then approved activities from Activity Manager
	 * + an overhead activity will be available for the user to select
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActivityListForm activitylistform = (ActivityListForm) form;

		ArrayList all_activities = new ArrayList();
		int activity_size = 0;
		String jobname = "";
		Activity activity = null;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		Map<String, Object> map;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "Job",
				"View Activities", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		String sortqueryclause = "";
		sortqueryclause = getSortQueryClause(sortqueryclause, session);

		if (request.getParameter("Job_Id") != null) {
			activitylistform.setJob_Id(request.getParameter("Job_Id"));
		}

		if (request.getAttribute("Job_Id") != null) {
			activitylistform.setJob_Id(request.getAttribute("Job_Id") + "");
		}

		if (request.getAttribute("from") != null) {
			activitylistform.setFrom(request.getAttribute("from") + "");
			request.setAttribute("from", "" + activitylistform.getFrom());
		}

		if (request.getParameter("from") != null) {
			activitylistform.setFrom(request.getParameter("from"));
			request.setAttribute("from", "" + activitylistform.getFrom());
		}

		if (request.getParameter("ref") != null) {
			if (request.getParameter("ref").equalsIgnoreCase("view")
					|| request.getParameter("ref").equalsIgnoreCase(
							"detailactivity")) {
				activitylistform.setChkaddendum("detailactivity");
			}
			if (request.getParameter("ref").trim()
					.equalsIgnoreCase("inscopeactivity")
					|| request.getParameter("ref").trim()
							.equalsIgnoreCase("inscopedetailactivity")
					|| request.getParameter("ref").trim()
							.equalsIgnoreCase("Submit")) {
				activitylistform.setChkaddendum("inscopedetailactivity");
			}

		} else {
			if (request.getAttribute("ref") != null) {
				activitylistform.setChkaddendum(request.getAttribute("ref")
						.toString());
			}
		}

		if (activitylistform.getCheck() != null
				&& request.getAttribute("copyActivity") == null) {
			List activityDeleteFailed = new ArrayList();
			boolean bidflag = false;
			/*
			 * for(int o=0;o<activitylistform.getCheck().length;o++){
			 * System.out.
			 * println("quantity["+o+"]							::	"+activitylistform.getQuantity
			 * (o));
			 * System.out.println("type["+o+"]								::	"+activitylistform
			 * .getActivitytypecombo (o));
			 * System.out.println("Estimated_materialcost["
			 * +o+"]			::	"+activitylistform.getEstimated_materialcost(o));
			 * System.out.println("Estimated_cnsfieldlaborcost["+o+"]		::	"+
			 * activitylistform.getEstimated_cnsfieldlaborcost(o));
			 * System.out.println
			 * ("Estimated_cnshqlaborcost["+o+"]			::	"+activitylistform
			 * .getEstimated_cnshqlaborcost(o) );
			 * System.out.println("Estimated_contractfieldlaborcost["
			 * +o+"]	::	"+activitylistform
			 * .getEstimated_contractfieldlaborcost(o));
			 * System.out.println("getEstimated_freightcost["
			 * +o+"]			::	"+activitylistform.getEstimated_freightcost(o) );
			 * System
			 * .out.println("getEstimated_travelcost["+o+"]			::	"+activitylistform
			 * .getEstimated_travelcost(o));
			 * System.out.println("Estimated_totalcost["
			 * +o+"]				::	"+activitylistform.getEstimated_totalcost(o) );
			 * System
			 * .out.println("Extendedprice["+o+"]					::	"+activitylistform
			 * .getExtendedprice(o));
			 * System.out.println("Overheadcost["+o+"]						::	"
			 * +activitylistform.getOverheadcost(o));
			 * System.out.println("Listprice["
			 * +o+"]						::	"+activitylistform.getListprice(o) );
			 * System.out.println
			 * ("Proformamargin["+o+"]					::	"+activitylistform
			 * .getProformamargin(o));
			 * System.out.println("Estimated_start_schedule["
			 * +o+"]			::	"+activitylistform.getEstimated_start_schedule(o));
			 * System.out.println("Estimated_complete_schedule["+o+"]		::	"+
			 * activitylistform.getEstimated_complete_schedule(o) ); }
			 */
			int checklength = activitylistform.getCheck().length;

			int[] index = new int[checklength];
			int k = 0;

			if (checklength > 0) {
				int length = activitylistform.getActivity_Id().length;
				for (int i = 0; i < checklength; i++) {
					for (int j = 0; j < length; j++) {
						if (activitylistform.getCheck(i).equals(
								activitylistform.getActivity_Id(j))) {
							index[k] = j;
							k++;
						}
					}
				}
			}

			if (checklength > 0) {
				int updateflag = -1;
				int deleteflag = -1;

				int changestatusflag = 0;
				int changestatusdraftflag = 0;
				for (int i = 0; i < index.length; i++) {
					activity = new Activity();
					activity.setName(activitylistform.getName(index[i]));
					activity.setActivity_Id(activitylistform
							.getActivity_Id(index[i]));
					activity.setJob_Id(activitylistform.getJob_Id());
					activity.setActivitytypecombo(activitylistform
							.getActivitytypecombo(index[i]));
					activity.setQuantity(activitylistform.getQuantity(index[i]));
					activity.setEstimated_materialcost(activitylistform
							.getEstimated_materialcost(index[i]));
					activity.setEstimated_cnsfieldlaborcost(activitylistform
							.getEstimated_cnsfieldlaborcost(index[i]));
					activity.setEstimated_cnshqlaborcost(activitylistform
							.getEstimated_cnshqlaborcost(index[i]));
					activity.setEstimated_contractfieldlaborcost(activitylistform
							.getEstimated_contractfieldlaborcost(index[i]));
					activity.setEstimated_freightcost(activitylistform
							.getEstimated_freightcost(index[i]));
					activity.setEstimated_travelcost(activitylistform
							.getEstimated_travelcost(index[i]));
					activity.setEstimated_totalcost(activitylistform
							.getEstimated_totalcost(index[i]));
					activity.setExtendedprice(activitylistform
							.getExtendedprice(index[i]));
					activity.setOverheadcost(activitylistform
							.getOverheadcost(index[i]));
					activity.setListprice(activitylistform
							.getListprice(index[i]));
					activity.setProformamargin(activitylistform
							.getProformamargin(index[i]));
					activity.setEstimated_start_schedule(activitylistform
							.getEstimated_start_schedule(index[i]));
					activity.setEstimated_complete_schedule(activitylistform
							.getEstimated_complete_schedule(index[i]));
					activity.setAddendum_id(activitylistform.getAddendum_id());

					/*
					 * Following logic is added to check whether Big Flag array
					 * contains the Activity Id If it does the store 1 in the
					 * database else 0
					 */
					if (activitylistform.getBidFlag() != null) {
						for (int j = 0; j < activitylistform.getBidFlag().length; j++) {
							if (activitylistform.getActivity_Id(index[i])
									.equals(activitylistform.getBidFlag(j))) {
								bidflag = true;
							}
						}
					}

					if (bidflag)
						activity.setBidFlag("1");
					else
						activity.setBidFlag("0");
					bidflag = false;

					/*** End ***/

					if (activitylistform.getSave() != null) {
						updateflag = Activitydao.Updateactivity(activity,
								activitylistform.getActivity_cost_type(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("updateflag", "" + updateflag);
					}
					if (activitylistform.getDelete() != null) {

						// mySqlUrl=this.getResources(request).getMessage("appendix.detail.changestatus.mysql.url");
						// System.out.println("mySqlURL---for delete activity---"+mySqlUrl);
						deleteflag = Activitydao
								.deleteactivity(
										activity.getActivity_Id(),
										getDataSource(request, "ilexnewDB"),
										EnvironmentSelector
												.getBundleString("appendix.detail.changestatus.mysql.url"));
						if (deleteflag == -90001) {
							activityDeleteFailed.add(activity.getName());
							request.setAttribute("activityDeletionFailed",
									activityDeleteFailed);
						}
						request.setAttribute("deleteflag", "" + deleteflag);
					}
					if (activitylistform.getApprove() != null) {
						changestatusflag = ChangeStatus.Status("", "", "",
								activity.getActivity_Id(), "", "Activity", "A",
								"N", getDataSource(request, "ilexnewDB"), "");
						request.setAttribute("changestatusflag", ""
								+ changestatusflag);
					}
					if (activitylistform.getDraft() != null) {
						changestatusdraftflag = ChangeStatus.Status("", "", "",
								activity.getActivity_Id(), "", "Activity", "D",
								"N", getDataSource(request, "ilexnewDB"), "");
						request.setAttribute("changestatusdraftflag", ""
								+ changestatusdraftflag);
					}
				}
			}
		}

		request.setAttribute("Job_Id", activitylistform.getJob_Id());
		activitylistform.setRef(request.getParameter("ref"));
		activitylistform.setActivity(ActivityLibrarydao.getActivitytype());

		activitylistform.setAppendixname(Jobdao.getoplevelname(
				activitylistform.getJob_Id(), "Activity",
				getDataSource(request, "ilexnewDB")));
		activitylistform.setJob_type(Jobdao.getJobtype(
				activitylistform.getJob_Id(),
				getDataSource(request, "ilexnewDB")));
		if (activitylistform.getJob_type() != null) {
			if (activitylistform.getJob_Id() != null
					&& !activitylistform.getJob_Id().trim()
							.equalsIgnoreCase("")) {
				if (logger.isDebugEnabled()) {
					logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - set menu");
				}
				map = DatabaseUtilityDao.setMenu(activitylistform.getJob_Id(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}

		all_activities = Activitydao.getActivityList(
				getDataSource(request, "ilexnewDB"),
				activitylistform.getJob_Id(), "%", sortqueryclause, "%");
		activitylistform.setBidFlag(Activitydao.getBidFlag(
				activitylistform.getJob_Id(),
				getDataSource(request, "ilexnewDB")));

		NameId idname = Activitydao.getIdName(activitylistform.getJob_Id(),
				"Activity", "MSA", getDataSource(request, "ilexnewDB"));
		activitylistform.setMsa_id(idname.getId());
		activitylistform.setMsaname(idname.getName());

		if (all_activities.size() > 0) {
			activity_size = all_activities.size();
		}

		activitylistform.setJobName(Activitydao.getJobname(
				getDataSource(request, "ilexnewDB"),
				activitylistform.getJob_Id()));

		if (request.getParameter("copyactivityflag") != null) {
			request.setAttribute("copyactivityflag",
					request.getParameter("copyactivityflag"));
		}
		request.setAttribute("Size", new Integer(activity_size));
		request.setAttribute("codes", all_activities);
		request.setAttribute("jobname", jobname);

		/* customer labor rates */
		String Appendix_Id = "";
		int checkNetmedXappendix = 0;

		Appendix_Id = Activitydao.getAppendixid(
				getDataSource(request, "ilexnewDB"),
				activitylistform.getJob_Id());

		activitylistform.setAppendixid(Appendix_Id);

		checkNetmedXappendix = Activitydao.checkAppendixname(Appendix_Id,
				activitylistform.getJob_Id(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("checknetmedx", checkNetmedXappendix + "");

		/* customer labor rates */

		// Get the Activity Grid Controls
		activitylistform.setActivityGridControl(Activitydao
				.getActivityGridControl(activitylistform.getJob_Id(),
						getDataSource(request, "ilexnewDB")));
		return mapping.findForward("activitytabularpage");
	}

	public String getSortQueryClause(String queryclause, HttpSession session) {
		if (session.getAttribute("activity_sort") != null)
			queryclause = (String) session.getAttribute("activity_sort");
		return queryclause;
	}

}
