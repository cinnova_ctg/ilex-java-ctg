package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.prm.SiteManagementBean;
import com.mind.bean.prm.SiteSearchBean;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.SiteSearchdao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.dao.PRM.SiteManagementdao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PM.SiteSearchForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

public class SiteSearchAction extends com.mind.common.IlexDispatchAction {
	public ActionForward search(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = new ActionForward();
		SiteSearchForm siteSearchForm = (SiteSearchForm) form;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		/* Page Security End */

		ArrayList initialStateCategory = new ArrayList();
		String tempCat = "";
		String[] tempList = null;
		int msa_id = 0;
		int size = 0;
		String endCustomer = "";

		String msa_name = null;

		if (request.getAttribute("retVal") != null) {
			request.setAttribute("retVal", request.getAttribute("retVal") + "");
		}

		ArrayList list = new ArrayList();
		int latestSiteId = 0;

		if (siteSearchForm.getSearch() != null
				&& siteSearchForm.getSearch() != "") {

			if (siteSearchForm.getMsa_id() != null) {
				msa_id = Integer.parseInt(siteSearchForm.getMsa_id());
			}
			list = SiteSearchdao.getSearchsitedetails(msa_id,
					siteSearchForm.getSite_name(),
					siteSearchForm.getSite_number(),
					siteSearchForm.getLo_ot_id(),
					siteSearchForm.getSite_city(),
					siteSearchForm.getSite_state(),
					siteSearchForm.getSite_brand(),
					siteSearchForm.getSite_end_customer(),
					getDataSource(request, "ilexnewDB"), latestSiteId);

			request.setAttribute("sitesearchlist", list);
			size = list.size();
		} else {
			if (request.getParameter("jobid") != null)
				siteSearchForm.setJobid(request.getParameter("jobid"));

			siteSearchForm.setAppendixid(request.getParameter("appendixid"));
			siteSearchForm.setReftype(request.getParameter("reftype"));

			siteSearchForm
					.setLo_ot_id(request.getParameter("lo_ot_id") != null ? request
							.getParameter("lo_ot_id") : "");

			if (request.getAttribute("appendixid") != null) {
				siteSearchForm.setAppendixid(request.getAttribute("appendixid")
						.toString());
			}

			if (request.getAttribute("jobid") != null) {
				siteSearchForm.setJobid(request.getAttribute("jobid")
						.toString());
			}

			if (request.getAttribute("fromType") != null)
				siteSearchForm.setFromType(request.getAttribute("fromType")
						.toString());

			if (request.getParameter("fromType") != null)
				siteSearchForm.setFromType(request.getParameter("fromType")
						.toString());

			if (request.getParameter("siteid") != null)
				siteSearchForm.setSiteid(request.getParameter("siteid")
						.toString());

			if (siteSearchForm.getAppendixid().equals("0"))
				siteSearchForm.setMsa_id(request.getParameter("MSA_Id"));
			else
				siteSearchForm.setMsa_id(SiteSearchdao.getMsaid(
						getDataSource(request, "ilexnewDB"),
						siteSearchForm.getAppendixid())
						+ "");

			endCustomer = SiteManagementdao.getEndCustomer(
					siteSearchForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
			if (endCustomer != null && endCustomer != "")
				siteSearchForm.setSite_end_customer(endCustomer);
		}

		msa_name = Appendixdao.getMsaname(getDataSource(request, "ilexnewDB"),
				"" + msa_id);

		if (msa_id != 0) {
			siteSearchForm.setMsa_name(msa_name);
		} else {
			siteSearchForm.setMsa_name("All");
		}

		if (siteSearchForm.getJobid() != null) {
			siteSearchForm.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"),
					siteSearchForm.getJobid()));
		}
		siteSearchForm
				.setMsa_name(com.mind.dao.PM.Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						siteSearchForm.getMsa_id()));
		siteSearchForm.setAppendixname(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				siteSearchForm.getAppendixid()));

		// Start :For State List
		DynamicComboCM dcStateCM = new DynamicComboCM();
		Pvsdao pvsDao = new Pvsdao();
		if (pvsDao != null) {
			msa_id = Integer.parseInt(siteSearchForm.getMsa_id());
			initialStateCategory = SiteManagedao.getStateCategoryList(""
					+ msa_id, getDataSource(request, "ilexnewDB"));
			if (initialStateCategory.size() > 0) {
				for (int i = 0; i < initialStateCategory.size(); i++) {
					tempCat = ((SiteManagementBean) initialStateCategory.get(i))
							.getSiteCountryId();
					((SiteManagementBean) initialStateCategory.get(i))
							.setTempList(SiteSearchdao.getSiteStateList(
									tempCat, "" + msa_id, i,
									getDataSource(request, "ilexnewDB")));
				}
			}
			request.setAttribute("initialStateCategory", initialStateCategory);
		}
		// End :For State List

		// Get Brand List
		DynamicComboCM dcGroupList = new DynamicComboCM();
		dcGroupList.setFirstlevelcatglist(DynamicComboDao
				.getGroupNameListSiteSearch("" + msa_id,
						getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcGroupList", dcGroupList);

		DynamicComboCM dcEndCustomerList = new DynamicComboCM();
		dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
				.getSiteEndCustomerList(getDataSource(request, "ilexnewDB"),
						siteSearchForm.getMsa_id(),
						siteSearchForm.getMsa_name()));
		request.setAttribute("dcEndCustomerList", dcEndCustomerList);

		if (request.getAttribute("latestSite") != null) {
			latestSiteId = Integer.parseInt(request.getAttribute("latestSite")
					.toString());
			list = SiteSearchdao.getSearchsitedetails(msa_id,
					siteSearchForm.getSite_name(),
					siteSearchForm.getSite_number(),
					siteSearchForm.getLo_ot_id(),
					siteSearchForm.getSite_city(),
					siteSearchForm.getSite_state(),
					siteSearchForm.getSite_brand(),
					siteSearchForm.getSite_end_customer(),
					getDataSource(request, "ilexnewDB"), latestSiteId);
			request.setAttribute("sitesearchlist", list);
			request.setAttribute("latestSiteId", latestSiteId + "");
		}

		request.setAttribute("size", size + "");
		// Code Added for the Job Detail Menu which should be available here
		if (siteSearchForm.getFromType() != null
				&& siteSearchForm.getFromType().equals("yes")) {
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					siteSearchForm.getJobid(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String jobStatusList = "";
			String appendixType = "";

			if (jobType1.equals("Default"))
				jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
						+ siteSearchForm.getJobid() + "&Status='D'\"" + ","
						+ "0";
			if (jobType1.equals("Newjob"))
				jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
						"", "", siteSearchForm.getJobid(), "", loginuserid,
						getDataSource(request, "ilexnewDB"));
			if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
				jobStatusList = "\"Approved\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ siteSearchForm.getJobid() + "&Status=A\"" + "," + "0";
			if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
				jobStatusList = "\"Draft\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ siteSearchForm.getJobid() + "&Status=D\"" + "," + "0";
			if (jobType1.equals("Addendum"))
				jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
						"", "", siteSearchForm.getJobid(), "", loginuserid,
						getDataSource(request, "ilexnewDB"));

			appendixType = ViewList.getAppendixtypedesc(
					siteSearchForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("jobStatus", jobStatus);
			request.setAttribute("job_type", jobType1);
			request.setAttribute("appendixtype", appendixType);
			request.setAttribute("Job_Id", siteSearchForm.getJobid());
			request.setAttribute("Appendix_Id", siteSearchForm.getAppendixid());
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("addendum_id", "0");

			if (jobType1.equals("Newjob")) {
				request.setAttribute("chkadd", "detailjob");
				request.setAttribute("chkaddendum", "detailjob");
				request.setAttribute("chkaddendumactivity", "View");
			}
			if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
				request.setAttribute("chkadd", "inscopejob");
				request.setAttribute("chkaddendum", "inscopejob");
				request.setAttribute("chkaddendumactivity", "inscopeactivity");
			}
		}
		// End of the code for the Job Detail Menu
		// added by pankaj
		SiteSearchBean siteSearchBean = new SiteSearchBean();
		BeanUtils.copyProperties(siteSearchBean, siteSearchForm);
		DatabaseUtilityDao.getJobInformation(siteSearchForm.getJobid(),
				siteSearchBean, getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(siteSearchForm, siteSearchBean);
		// added by pankaj
		forward = mapping.findForward("success");
		return (forward);
	}

	/**
	 * @author amitm
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewAllSites(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = new ActionForward();
		SiteSearchForm siteSearchForm = (SiteSearchForm) form;

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		/* Page Security End */

		if (request.getParameter("MSA_Id") != null) {
			siteSearchForm.setMsa_id((String) request.getParameter("MSA_Id"));
			siteSearchForm.setMsa_name(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					siteSearchForm.getMsa_id()));
		}
		if (request.getParameter("appendixid") != null) {
			siteSearchForm.setAppendixid((String) request
					.getParameter("appendixid"));
			siteSearchForm.setAppendixname(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					siteSearchForm.getAppendixid()));
		}

		int msa_id = 0;
		int size = 0;
		String siteName = "";
		String siteNumber = "";
		String lo_ot_id = "";

		if (siteSearchForm.getSearch() != null) {

			ArrayList list = new ArrayList();

			if (siteSearchForm.getMsa_id() != null) {
				msa_id = Integer.parseInt(siteSearchForm.getMsa_id());
			}
			siteName = siteSearchForm.getSite_name();
			siteNumber = siteSearchForm.getSite_number();
			lo_ot_id = siteSearchForm.getLo_ot_id();
			list = SiteSearchdao.getSearchsitedetails(msa_id, siteName,
					siteNumber, lo_ot_id, getDataSource(request, "ilexnewDB"));

			request.setAttribute("sitesearchlist", list);
			size = list.size();
		} else {

			siteSearchForm.setReftype(request.getParameter("reftype"));
			siteSearchForm
					.setLo_ot_id(request.getParameter("lo_ot_id") != null ? request
							.getParameter("lo_ot_id") : "");
		}

		request.setAttribute("size", size + "");
		forward = mapping.findForward("viewAllSites");
		return (forward);
	}

}
