package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.LaborResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.AM.LaborResourceDetailForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class LaborDetailAction extends com.mind.common.IlexDispatchAction {
	public ActionForward View(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		LaborResourceDetailForm laborresourcedetailform = (LaborResourceDetailForm) form;

		LaborResource laborresource = new LaborResource();
		String Labor_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Labor_Id") != null) {
			Labor_Id = request.getParameter("Labor_Id");
		}

		if (request.getAttribute("Labor_Id") != null) {
			Labor_Id = (String) request.getAttribute("Labor_Id");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			laborresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		laborresourcedetailform.setLaborid(Labor_Id);
		request.setAttribute("Labor_Id", laborresourcedetailform.getLaborid());

		// ArrayList laborresourcelist = Resourcedao.getResourcetabularlist( "%"
		// , "L" , laborresourcedetailform.getLaborid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList laborresourcelist = Resourcedao.getResourcetabularlist(
				laborresourcedetailform.getActivity_Id(), "L",
				laborresourcedetailform.getLaborid(), "", request.getSession()
						.getId(), "", getDataSource(request, "ilexnewDB"));

		laborresource = (LaborResource) laborresourcelist.get(0);

		laborresourcedetailform.setActivity_Id(laborresource.getActivity_Id());
		request.setAttribute("Activity_Id",
				laborresourcedetailform.getActivity_Id());

		laborresourcedetailform.setLaborid(laborresource.getLaborid());
		laborresourcedetailform.setLaborname(laborresource.getLaborname());
		laborresourcedetailform.setLabortype(laborresource.getLabortype());
		laborresourcedetailform.setMinimumquantity(laborresource
				.getMinimumquantity());
		laborresourcedetailform.setCnspartnumber(laborresource
				.getCnspartnumber());
		laborresourcedetailform.setEstimatedtotalcost(laborresource
				.getEstimatedtotalcost());
		laborresourcedetailform.setEstimatedhourlybasecost(laborresource
				.getEstimatedhourlybasecost());

		laborresourcedetailform.setPriceextended(laborresource
				.getPriceextended());
		laborresourcedetailform.setPriceunit(laborresource.getPriceunit());
		laborresourcedetailform.setProformamargin(laborresource
				.getProformamargin());
		laborresourcedetailform.setQuantityhours(laborresource
				.getQuantityhours());
		laborresourcedetailform.setSellablequantity(laborresource
				.getSellablequantity());
		laborresourcedetailform.setStatus(laborresource.getStatus());

		laborresourcedetailform.setChkaddendum("View");
		laborresourcedetailform.setChkdetailactivity("detailactivity");

		laborresourcedetailform.setActivityname(Activitydao.getActivityname(
				laborresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setMsa_id(idname.getId());
		laborresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setAppendix_id(idname.getId());
		laborresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setJob_id(idname.getId());
		laborresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resL", laborresourcedetailform
					.getStatus().charAt(0), from,
					request.getParameter("jobid"), "View",
					laborresourcedetailform.getLaborid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("pm_resL", laborresourcedetailform
					.getStatus().charAt(0), "", "", "", laborresourcedetailform
					.getLaborid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		}
		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				laborresourcedetailform.getLaborid(), "pm_resL");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				laborresourcedetailform.getLaborid(), "pm_resL");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				laborresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				laborresourcedetailform.getChkaddendum());
		// End

		return mapping.findForward("labordetailpage");
	}

	public ActionForward Viewinscope(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		LaborResourceDetailForm laborresourcedetailform = (LaborResourceDetailForm) form;

		LaborResource laborresource = new LaborResource();
		String Labor_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;
		String resourceType = "IL";
		Map<String, Object> map;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Labor_Id") != null) {
			Labor_Id = request.getParameter("Labor_Id");
		}

		if (request.getAttribute("Labor_Id") != null) {
			Labor_Id = (String) request.getAttribute("Labor_Id");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			laborresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		laborresourcedetailform.setLaborid(Labor_Id);
		request.setAttribute("Labor_Id", laborresourcedetailform.getLaborid());

		if (request.getParameter("jobType") != null
				&& request.getParameter("jobType").equals("Newjob")) {
			resourceType = "L";
		}

		// ArrayList laborresourcelist = Resourcedao.getResourcetabularlist( "%"
		// , "IL" , laborresourcedetailform.getLaborid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList laborresourcelist = Resourcedao.getResourcetabularlist(
				laborresourcedetailform.getActivity_Id(), resourceType,
				laborresourcedetailform.getLaborid(), "", request.getSession()
						.getId(), "", getDataSource(request, "ilexnewDB"));

		laborresource = (LaborResource) laborresourcelist.get(0);

		laborresourcedetailform.setActivity_Id(laborresource.getActivity_Id());
		request.setAttribute("Activity_Id",
				laborresourcedetailform.getActivity_Id());

		laborresourcedetailform.setLaborid(laborresource.getLaborid());
		laborresourcedetailform.setLaborname(laborresource.getLaborname());
		laborresourcedetailform.setLabortype(laborresource.getLabortype());
		laborresourcedetailform.setMinimumquantity(laborresource
				.getMinimumquantity());
		laborresourcedetailform.setCnspartnumber(laborresource
				.getCnspartnumber());
		laborresourcedetailform.setEstimatedtotalcost(laborresource
				.getEstimatedtotalcost());
		laborresourcedetailform.setEstimatedhourlybasecost(laborresource
				.getEstimatedhourlybasecost());

		laborresourcedetailform.setPriceextended(laborresource
				.getPriceextended());
		laborresourcedetailform.setPriceunit(laborresource.getPriceunit());
		laborresourcedetailform.setProformamargin(laborresource
				.getProformamargin());
		laborresourcedetailform.setQuantityhours(laborresource
				.getQuantityhours());
		laborresourcedetailform.setSellablequantity(laborresource
				.getSellablequantity());
		laborresourcedetailform.setStatus(laborresource.getStatus());

		laborresourcedetailform.setChkaddendum("Viewinscope");
		laborresourcedetailform.setChkdetailactivity("inscopedetailactivity");
		laborresourcedetailform.setActivityname(Activitydao.getActivityname(
				laborresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setMsa_id(idname.getId());
		laborresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setAppendix_id(idname.getId());
		laborresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setJob_id(idname.getId());
		laborresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resL", laborresourcedetailform
					.getStatus().charAt(0), from,
					request.getParameter("jobid"), "Viewinscope",
					laborresourcedetailform.getLaborid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("jobdashboard_resL", laborresourcedetailform
					.getStatus().charAt(0), "", "", "Viewinscope",
					laborresourcedetailform.getLaborid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		}
		if (request.getParameter("from") != null
				&& !request.getParameter("from").trim().equalsIgnoreCase("")
				&& request.getParameter("from").trim()
						.equalsIgnoreCase("jobdashboard")) {
			if (request.getParameter("jobid") != null
					&& !request.getParameter("jobid").trim()
							.equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(request.getParameter("jobid"),
						loginuserid, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}
		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				laborresourcedetailform.getLaborid(), "pm_resL");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				laborresourcedetailform.getLaborid(), "pm_resL");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				laborresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				laborresourcedetailform.getChkaddendum());
		// End

		return mapping.findForward("labordetailpage");
	}

	public ActionForward ViewAddendum(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		LaborResourceDetailForm laborresourcedetailform = (LaborResourceDetailForm) form;

		LaborResource laborresource = new LaborResource();
		String Labor_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Labor_Id") != null) {
			Labor_Id = request.getParameter("Labor_Id");
		}

		if (request.getAttribute("Labor_Id") != null) {
			Labor_Id = (String) request.getAttribute("Labor_Id");
		}

		if (request.getParameter("addendum_id") != null) {
			laborresourcedetailform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getAttribute("addendum_id") != null) {
			laborresourcedetailform.setAddendum_id(request.getAttribute(
					"addendum_id").toString());
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			laborresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		laborresourcedetailform.setLaborid(Labor_Id);
		request.setAttribute("Labor_Id", laborresourcedetailform.getLaborid());

		// ArrayList laborresourcelist = Resourcedao.getResourcetabularlist( "%"
		// , "AL" , laborresourcedetailform.getLaborid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList laborresourcelist = Resourcedao.getResourcetabularlist(
				laborresourcedetailform.getActivity_Id(), "AL",
				laborresourcedetailform.getLaborid(), "", request.getSession()
						.getId(), "", getDataSource(request, "ilexnewDB"));

		laborresource = (LaborResource) laborresourcelist.get(0);

		laborresourcedetailform.setActivity_Id(laborresource.getActivity_Id());
		request.setAttribute("Activity_Id",
				laborresourcedetailform.getActivity_Id());

		laborresourcedetailform.setLaborid(laborresource.getLaborid());
		laborresourcedetailform.setLaborname(laborresource.getLaborname());
		laborresourcedetailform.setLabortype(laborresource.getLabortype());
		laborresourcedetailform.setMinimumquantity(laborresource
				.getMinimumquantity());
		laborresourcedetailform.setCnspartnumber(laborresource
				.getCnspartnumber());
		laborresourcedetailform.setEstimatedtotalcost(laborresource
				.getEstimatedtotalcost());
		laborresourcedetailform.setEstimatedhourlybasecost(laborresource
				.getEstimatedhourlybasecost());

		laborresourcedetailform.setPriceextended(laborresource
				.getPriceextended());
		laborresourcedetailform.setPriceunit(laborresource.getPriceunit());
		laborresourcedetailform.setProformamargin(laborresource
				.getProformamargin());
		laborresourcedetailform.setQuantityhours(laborresource
				.getQuantityhours());
		laborresourcedetailform.setSellablequantity(laborresource
				.getSellablequantity());
		laborresourcedetailform.setStatus(laborresource.getStatus());

		laborresourcedetailform.setChkaddendum("ViewAddendum");
		laborresourcedetailform.setChkdetailactivity("detailaddendumactivity");
		laborresourcedetailform.setActivityname(Activitydao.getActivityname(
				laborresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setMsa_id(idname.getId());
		laborresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setAppendix_id(idname.getId());
		laborresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setJob_id(idname.getId());
		laborresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resL", laborresourcedetailform
					.getStatus().charAt(0), from,
					request.getParameter("jobid"), "ViewAddendum",
					laborresourcedetailform.getLaborid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			// list = Menu.getStatus( "pm_resL" ,
			// laborresourcedetailform.getStatus().charAt( 0 ) , "" , "" , "" ,
			// laborresourcedetailform.getLaborid() , loginuserid ,
			// getDataSource( request , "ilexnewDB" ) );

			if (laborresourcedetailform.getStatus().equals("Draft")) {
				list = "\"Approved\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ laborresourcedetailform.getAddendum_id()
						+ "&type=pm_resL&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
						+ laborresourcedetailform.getLaborid() + "&Status=A\""
						+ "," + "0";
			}

			if (laborresourcedetailform.getStatus().equals("Approved")) {
				list = "\"Draft\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ laborresourcedetailform.getAddendum_id()
						+ "&type=pm_resL&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
						+ laborresourcedetailform.getLaborid() + "&Status=D\""
						+ "," + "0";
			}
		}

		/*
		 * if( laborresourcedetailform.getStatus().equals( "Draft" ) ) { list =
		 * "\"Approved\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="+
		 * laborresourcedetailform.getAddendum_id()+
		 * "&type=pm_resL&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
		 * +laborresourcedetailform.getLaborid()+"&Status=A\"" + "," + "0"; }
		 * 
		 * if( laborresourcedetailform.getStatus().equals( "Approved" ) ) { list
		 * = "\"Draft\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="+laborresourcedetailform
		 * .getAddendum_id()+
		 * "&type=pm_resL&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
		 * +laborresourcedetailform.getLaborid()+"&Status=D\"" + "," + "0"; }
		 */
		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				laborresourcedetailform.getLaborid(), "pm_resL");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				laborresourcedetailform.getLaborid(), "pm_resL");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);

		return mapping.findForward("labordetailpage");
	}

	public ActionForward ViewChangeorder(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LaborResourceDetailForm laborresourcedetailform = (LaborResourceDetailForm) form;

		LaborResource laborresource = new LaborResource();
		String Labor_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Labor_Id") != null) {
			Labor_Id = request.getParameter("Labor_Id");
		}

		if (request.getAttribute("Labor_Id") != null) {
			Labor_Id = (String) request.getAttribute("Labor_Id");
		}

		if (request.getParameter("addendum_id") != null) {
			laborresourcedetailform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getAttribute("addendum_id") != null) {
			laborresourcedetailform.setAddendum_id(request.getAttribute(
					"addendum_id").toString());
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			laborresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		laborresourcedetailform.setLaborid(Labor_Id);
		request.setAttribute("Labor_Id", laborresourcedetailform.getLaborid());

		// ArrayList laborresourcelist = Resourcedao.getResourcetabularlist( "%"
		// , "CL" , laborresourcedetailform.getLaborid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList laborresourcelist = Resourcedao.getResourcetabularlist(
				laborresourcedetailform.getActivity_Id(), "CL",
				laborresourcedetailform.getLaborid(), "", request.getSession()
						.getId(), "", getDataSource(request, "ilexnewDB"));

		laborresource = (LaborResource) laborresourcelist.get(0);

		laborresourcedetailform.setActivity_Id(laborresource.getActivity_Id());
		request.setAttribute("Activity_Id",
				laborresourcedetailform.getActivity_Id());

		laborresourcedetailform.setLaborid(laborresource.getLaborid());
		laborresourcedetailform.setLaborname(laborresource.getLaborname());
		laborresourcedetailform.setLabortype(laborresource.getLabortype());
		laborresourcedetailform.setMinimumquantity(laborresource
				.getMinimumquantity());
		laborresourcedetailform.setCnspartnumber(laborresource
				.getCnspartnumber());
		laborresourcedetailform.setEstimatedtotalcost(laborresource
				.getEstimatedtotalcost());
		laborresourcedetailform.setEstimatedhourlybasecost(laborresource
				.getEstimatedhourlybasecost());

		laborresourcedetailform.setPriceextended(laborresource
				.getPriceextended());
		laborresourcedetailform.setPriceunit(laborresource.getPriceunit());
		laborresourcedetailform.setProformamargin(laborresource
				.getProformamargin());
		laborresourcedetailform.setQuantityhours(laborresource
				.getQuantityhours());
		laborresourcedetailform.setSellablequantity(laborresource
				.getSellablequantity());
		laborresourcedetailform.setStatus(laborresource.getStatus());

		laborresourcedetailform.setChkaddendum("ViewChangeorder");
		laborresourcedetailform
				.setChkdetailactivity("detailchangeorderactivity");
		laborresourcedetailform.setActivityname(Activitydao.getActivityname(
				laborresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setMsa_id(idname.getId());
		laborresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setAppendix_id(idname.getId());
		laborresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				laborresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		laborresourcedetailform.setJob_id(idname.getId());
		laborresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resL", laborresourcedetailform
					.getStatus().charAt(0), from,
					request.getParameter("jobid"), "ViewChangeorder",
					laborresourcedetailform.getLaborid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			// list = Menu.getStatus( "pm_resL" ,
			// laborresourcedetailform.getStatus().charAt( 0 ) , "" , "" , "" ,
			// laborresourcedetailform.getLaborid() , loginuserid ,
			// getDataSource( request , "ilexnewDB" ) );

			if (laborresourcedetailform.getStatus().equals("Draft")) {
				list = "\"Approved\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ laborresourcedetailform.getAddendum_id()
						+ "&type=pm_resL&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
						+ laborresourcedetailform.getLaborid() + "&Status=A\""
						+ "," + "0";
			}

			if (laborresourcedetailform.getStatus().equals("Approved")) {
				list = "\"Draft\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ laborresourcedetailform.getAddendum_id()
						+ "&type=pm_resL&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
						+ laborresourcedetailform.getLaborid() + "&Status=D\""
						+ "," + "0";
			}
		}

		/*
		 * if( laborresourcedetailform.getStatus().equals( "Draft" ) ) { list =
		 * "\"Approved\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="+
		 * laborresourcedetailform.getAddendum_id()+
		 * "&type=pm_resL&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +laborresourcedetailform.getLaborid()+"&Status=A\"" + "," + "0"; }
		 * 
		 * if( laborresourcedetailform.getStatus().equals( "Approved" ) ) { list
		 * = "\"Draft\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="+laborresourcedetailform
		 * .getAddendum_id()+
		 * "&type=pm_resL&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +laborresourcedetailform.getLaborid()+"&Status=D\"" + "," + "0"; }
		 */

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				laborresourcedetailform.getLaborid(), "pm_resL");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				laborresourcedetailform.getLaborid(), "pm_resL");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);

		request.setAttribute("list", list);

		return mapping.findForward("labordetailpage");
	}
}
