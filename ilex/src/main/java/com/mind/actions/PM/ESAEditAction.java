package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.ESAEditBean;
import com.mind.business.pm.ESAOrganizer;
import com.mind.business.pm.ViewSelector;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.formbean.PM.ESAEditForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

/**
 * The Class ESAEditAction.
 */
public class ESAEditAction extends com.mind.common.IlexDispatchAction {

	/**
	 * View.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * 
	 * @return the action forward
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce) {

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		ESAEditForm esaEditForm = (ESAEditForm) form;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "Appendix",
				"Setup External Sales Agent",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		/* Set Request Object */
		setRequest(esaEditForm, request);
		if (esaEditForm.getSave() != null) {
			saveProjectEsaDetails(esaEditForm, loginuserid,
					getDataSource(request, "ilexnewDB"), request);
		}

		/* Implement Logic. */
		setUpEsaForm(esaEditForm, getDataSource(request, "ilexnewDB"));

		if (request.getParameter("fromPage") != null) {
			request.setAttribute("fromModule", "PRM");
			String forwardPage = generateViewSelectorPRM(session, request,
					esaEditForm, getDataSource(request, "ilexnewDB"));
			if (!forwardPage.equals("")) {
				return (mapping.findForward(forwardPage.toString()));
			}

		} else {

			/* Call View Sector: Start */
			generateViewSelector(session, request, esaEditForm,
					getDataSource(request, "ilexnewDB"));
			if (esaEditForm.getUrl() != null) {
				ActionForward fwd = new ActionForward();
				fwd.setPath(esaEditForm.getUrl());
				return fwd;
			}/* Call View Sector: End */

			/* Call Menu and set into request object: Start */
			HashMap pmMenu = DatabaseUtilityDao.setPMMenu(
					esaEditForm.getAppendixId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixStatusList",
					pmMenu.get("appendixStatusList"));
			request.setAttribute("Appendix_Id", pmMenu.get("Appendix_Id"));
			request.setAttribute("MSA_Id", pmMenu.get("MSA_Id"));
			request.setAttribute("appendixStatus", pmMenu.get("appendixStatus"));
			request.setAttribute("latestaddendumid",
					pmMenu.get("latestaddendumid"));
			request.setAttribute("addendumlist", pmMenu.get("addendumlist"));
			request.setAttribute("msaName", pmMenu.get("msaName"));
			/* Call Menu and set into request object: Start */
		}
		return (mapping.findForward("esaEditPage"));
	}

	/**
	 * Sets the up esa form. Implement Logic for ESA.
	 * 
	 * @param form
	 *            the form
	 * @param ds
	 *            the ds
	 */
	private static void setUpEsaForm(ESAEditForm form, DataSource ds) {
		ESAEditBean esaBean = new ESAEditBean();
		esaBean.setAppendixId(form.getAppendixId());
		ESAOrganizer.setEsaBean(esaBean, ds);
		ESAOrganizer.convertToForm(form, esaBean);
	}

	public static void saveProjectEsaDetails(ESAEditForm form,
			String loginuserid, DataSource ds, HttpServletRequest request) {
		ESAEditBean esaBean = new ESAEditBean();
		esaBean.setAppendixId(form.getAppendixId());
		ESAOrganizer.convertToBean(form, esaBean);
		int esaSaveStatus = ESAOrganizer.saveEsaDetails(esaBean, loginuserid,
				ds);
		request.setAttribute("esaSaveStatus", esaSaveStatus + "");
	}

	/**
	 * Generate view selector for PM. Call setObject(form, session, request,ds )
	 * for set requierd View Selector data into FormBean.
	 * 
	 * @param session
	 *            the session
	 * @param request
	 *            the request
	 * @param form
	 *            the form
	 * @param ds
	 *            the ds
	 */
	private static void generateViewSelector(HttpSession session,
			HttpServletRequest request, ESAEditForm form, DataSource ds) {
		request.setAttribute("appendixId", form.getAppendixId());
		request.setAttribute("msaId", form.getMsaId());
		ViewSelector.setObject(form, session, request, ds);
	}

	/**
	 * Generate view selector for PRM. Call setObject(form, session, request,ds
	 * ) for set requierd View Selector data into FormBean.
	 * 
	 * @param session
	 *            the session
	 * @param request
	 *            the request
	 * @param form
	 *            the form
	 * @param ds
	 *            the ds
	 */
	private static String generateViewSelectorPRM(HttpSession session,
			HttpServletRequest request, ESAEditForm form, DataSource ds) {
		request.setAttribute("appendixId", form.getAppendixId());
		request.setAttribute("msaId", form.getMsaId());
		String forwardPage = "";

		/* Variable for Appendix Dashboard View Selector: Start */
		ArrayList jobOwnerList = new ArrayList(); // - for list of Job Owner
		ArrayList jobStatusList = new ArrayList(); // - list of job status
		String jobSelectedStatus = ""; // - selected status
		String jobSelectedOwner = ""; // - selected owner
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		/* Variable for Appendix Dashboard View Selector: End */

		if (request.getParameter("appendixid") != null) {
			form.setAppendixId((String) request.getParameter("appendixid"));
			form.setAppendixName(Appendixdao.getAppendixname(
					form.getAppendixId(), ds));
			form.setMsaId(IMdao.getMSAId(form.getAppendixId(), ds)); // - for
																		// menu
		}
		if (request.getParameter("ownerId") != null) // - for menu
		{
			form.setOwnerId(request.getParameter("ownerId"));
		}
		if (request.getParameter("viewjobtype") != null) // - for menu
		{
			form.setViewjobtype(request.getParameter("viewjobtype"));
		}
		if (request.getParameter("fromPage") != null) {
			form.setFromPage(request.getParameter("fromPage"));
		}

		/* For Menu & View Selector: Start */

		/* Start : Appendix Dashboard View Selector Code */

		if (form.getJobOwnerOtherCheck() != null
				&& !form.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					form.getJobOwnerOtherCheck());
		}
		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", form.getAppendixId());
			request.setAttribute("msaId", form.getMsaId());
			// return mapping.findForward("temppage");
			forwardPage = "temppage";
			return forwardPage;
		}
		if (form.getJobSelectedStatus() != null) {
			for (int i = 0; i < form.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ form.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ form.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}
		if (form.getJobSelectedOwners() != null) {
			for (int j = 0; j < form.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ form.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ form.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}
		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame", form.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					form.getJobOwnerOtherCheck());
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", form.getAppendixId());
			request.setAttribute("msaId", form.getMsaId());
			forwardPage = "temppage";
			// return mapping.findForward("temppage"); // -
			// /common/Tempupload.jsp
			return forwardPage;

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				form.setJobSelectedOwners(session.getAttribute("jobTempOwner")
						.toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				form.setJobSelectedStatus(session.getAttribute("jobTempStatus")
						.toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				form.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				form.setJobMonthWeekCheck(session.getAttribute("timeFrame")
						.toString());
			}
		}
		jobOwnerList = MSAdao.getOwnerList(
				(String) session.getAttribute("userid"),
				form.getJobOwnerOtherCheck(), "prj_job_new",
				form.getAppendixId(), ds);
		jobStatusList = MSAdao.getStatusList("prj_job_new", ds);
		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(form.getAppendixId(), ds); // - for
																		// addendum
																		// section
																		// of
																		// menu
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				form.getAppendixId(), ds); // - Appendix current status

		/* Set Parameter for menu and view selector */
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("contractDocMenu", contractDocoumentList);
		request.setAttribute("msaId", form.getMsaId());
		request.setAttribute("viewjobtype", form.getViewjobtype());
		request.setAttribute("ownerId", form.getOwnerId());
		request.setAttribute("appendixid", form.getAppendixId());
		request.setAttribute("fromPage", request.getParameter("fromPage"));

		/* For Menu & View Selector: End */
		return forwardPage;
	}

	/**
	 * Sets the request. Set all request paramerter into From Bean.
	 * 
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 */
	private static void setRequest(ESAEditForm form, HttpServletRequest request) {
		if (request.getParameter("appendixId") != null) {
			form.setAppendixId(request.getParameter("appendixId"));
		}

		if (request.getParameter("appendixid") != null) {
			form.setAppendixId(request.getParameter("appendixid"));
		}

		if (request.getParameter("msaId") != null) {
			form.setMsaId(request.getParameter("msaId"));
		}
		if (request.getAttribute("msaId") != null) {
			form.setMsaId((String) request.getAttribute("msaId"));
		}
		if (request.getAttribute("appendixId") != null) {
			form.setAppendixId((String) request.getAttribute("appendixId"));
		}
	}
}
