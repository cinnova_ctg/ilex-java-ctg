package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.TravelResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.AM.TravelResourceForm;

public class TravelUpdateAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		String sortqueryclause = "";
		String activityname = "";
		String Job_Id ="";
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) != null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			travelresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = travelresourceform.getActivity_Id();
		}
		
		if( request.getParameter( "from" ) != null )
		{
			travelresourceform.setFromflag( request.getParameter( "from" ) );
			travelresourceform.setDashboardid( request.getParameter( "jobid" ) );
		
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		Job_Id = Activitydao.getJobid( getDataSource( request , "ilexnewDB" ) , Activity_Id );
		travelresourceform.setJobid( Job_Id );
		
		
		//		added by hamid
		request.setAttribute( "jobid" ,Job_Id);
		travelresourceform.setNewquantity( "0.0000" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setActivity_Id( Activity_Id );
		travelresourceform.setTravellist( Resourcedao.getResourcecombolist( "travel" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		travelresourceform.setBacktoactivity( "detailactivity" );
		travelresourceform.setChkaddendum( "View" );
		travelresourceform.setChkaddendumdetail( "View" );
		travelresourceform.setAddendum_id( "0" );
		
		ArrayList travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "T" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
		
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setAppendix_id( idname.getId() );
		travelresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		
		
		/* for checkbox check start */
		
		String checkTempResources[] = Resourcedao.checkTempResources( Activity_Id , "T" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setCheck( checkTempResources );
		/* for checkbox check end */
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "traveltabularpage" ) );
	}
	
	/*public ActionForward Refresh( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		String Activity_Id = "";
		String newname = "";
		String newnamecombo = "";
		String newtravelid = "";
		int resource_size = 0;
		String status = "";
		int checklength = 0;
		ArrayList temp = new ArrayList();
		String[] checkrefresh = null;
		ArrayList travelresourcelist = new ArrayList();
		String flag1 = "";
		String flag2 = "";
		String flag3 = "";
		String flag4 = "";
		
		String flag5 = "";
		String flag6 = "";
		
		String activityname = "";
		flag1 = travelresourceform.getBacktoactivity();
		flag2 = travelresourceform.getChkaddendum();
		flag3 = travelresourceform.getChkaddendumdetail();
		flag4 = travelresourceform.getAddendum_id();
		
		flag5 = travelresourceform.getFromflag();
		flag6 = travelresourceform.getDashboardid();
		
		
		
		Activity_Id = travelresourceform.getActivity_Id();
		
		newname = travelresourceform.getNewtravelname();
		newnamecombo = travelresourceform.getNewtravelnamecombo();
		newtravelid = travelresourceform.getNewtravelid();
		
		
		
		if( travelresourceform.getCheck()!=  null )
		{
			checklength = travelresourceform.getCheck().length;	 
			checkrefresh = travelresourceform.getCheck();
			
			int []tempindex = new int[checklength];
			int k = 0;
				
			if( checklength > 0 )
			{	int length = travelresourceform.getTravelid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( travelresourceform.getCheck( i ).equals( travelresourceform.getTravelid( j ) ) )
						{	
							tempindex[k] = j;
							k++;
						}
					}
				}
			}	
			
			for( int i = 0; i < checklength; i++ )
			{
				TravelResource tempresource = new TravelResource();
				
				tempresource.setActivity_Id( Activity_Id );
				
				tempresource.setTravelid( travelresourceform.getTravelid( tempindex[i] ) );
				tempresource.setTravelcostlibid( travelresourceform.getTravelcostlibid( tempindex[i] ) );
				
				tempresource.setTravelname( travelresourceform.getTravelname( tempindex[i] ) );
				tempresource.setTraveltype( travelresourceform.getTraveltype( tempindex[i] ) );
				
				tempresource.setCnspartnumber( travelresourceform.getCnspartnumber( tempindex[i] ) );
				tempresource.setEstimatedtotalcost( travelresourceform.getEstimatedtotalcost( tempindex[i] ) );
				tempresource.setEstimatedunitcost( travelresourceform.getEstimatedunitcost( tempindex[i] ) );
				tempresource.setMinimumquantity( travelresourceform.getMinimumquantity( tempindex[i] ) );
				tempresource.setPriceextended( travelresourceform.getPriceextended( tempindex[i] ) );
				tempresource.setPriceunit( travelresourceform.getPriceunit( tempindex[i] ) );
				tempresource.setProformamargin( travelresourceform.getProformamargin( tempindex[i] ) );
				
				tempresource.setQuantity( travelresourceform.getQuantity( tempindex[i] ) );
				tempresource.setPrevquantity( travelresourceform.getPrevquantity( tempindex[i] ) );
				
				tempresource.setSellablequantity( travelresourceform.getSellablequantity( tempindex[i] ) );
				tempresource.setStatus( travelresourceform.getStatus( tempindex[i] ) );
				
				
				temp.add( tempresource );
			}	
		}
		
		
		TravelResource travelresource = Resourcedao.getTravelResource( travelresourceform.getNewtravelnamecombo() , getDataSource( request , "ilexnewDB" ) );
		
		travelresourceform.reset( mapping , request);
		
		travelresourceform.setNewtravelid( newtravelid );
		travelresourceform.setNewtravelname( newname );
		travelresourceform.setNewtravelnamecombo( newnamecombo );
		
		travelresourceform.setNewquantity( "0.0000" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setNewtraveltype( travelresource.getTraveltype() );
		travelresourceform.setNewcnspartnumber( travelresource.getCnspartnumber() );
		
		travelresourceform.setNewestimatedunitcost( travelresource.getEstimatedunitcost() );
		travelresourceform.setNewsellablequantity( travelresource.getSellablequantity() );
		travelresourceform.setNewminimumquantity( travelresource.getMinimumquantity() );
		travelresourceform.setRef( null );
		travelresourceform.setActivity_Id( Activity_Id );
		travelresourceform.setTravellist( Resourcedao.getResourcecombolist( "travel" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );
		
		travelresourceform.setBacktoactivity( flag1 );
		travelresourceform.setChkaddendum( flag2 );
		travelresourceform.setChkaddendumdetail( flag3 );
		travelresourceform.setAddendum_id( flag4 );
		
		travelresourceform.setFromflag( flag5 );
		travelresourceform.setDashboardid( flag6 );
		
		if( travelresourceform.getFromflag().equals( "jobdashboard" ) || travelresourceform.getFromflag().equals( "jobdashboard_resource" ) )
		{
			request.setAttribute( "from" , travelresourceform.getFromflag() );
			request.setAttribute( "jobid" , travelresourceform.getDashboardid() );
			
		}
		
		
		if( travelresourceform.getChkaddendum().equals( "View" ) )
		{
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "T" , "%" , "" ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( travelresourceform.getChkaddendum().equals( "ViewAddendum" ) )
		{
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "AT" , "%" , "" ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( travelresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
		{
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "CT" , "%" , "" ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( travelresourceform.getChkaddendum().equals( "Viewinscope" ) )
		{
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IT" , "%" , "" ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		
		
		if( checklength > 0 )
		{
			if( resource_size > 0 )
			{
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < resource_size; j++ )
					{
						if( ( ( TravelResource )temp.get( i ) ).getTravelid().equals( ( ( TravelResource )travelresourcelist.get( j ) ).getTravelid() ) )
						{
							travelresourcelist.set( j ,  temp.get( i ) );
						}
					}
				}
				travelresourceform.setCheck( checkrefresh );
			}
			
		}
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		
		request.setAttribute( "refresh" , "false" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "traveltabularpage" ) );
	}*/
	
	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		String Activity_Id = "";
		TravelResource travelresource = null;
		int resource_size = 0;
		String status = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		String sortqueryclause = "";
		
		ArrayList travelresourcelist = new ArrayList();
		String flag1 = "";
		String flag2 = "";
		String flag3 = "";
		String flag4 = "";
		String activityname = "";
		int addflag=0;
		NameId idname = null;
		flag1 = travelresourceform.getBacktoactivity();
		flag2 = travelresourceform.getChkaddendum();
		flag3 = travelresourceform.getChkaddendumdetail();
		flag4 = travelresourceform.getAddendum_id();
		
		Activity_Id = travelresourceform.getActivity_Id();
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		if( travelresourceform.getCheck()!=  null )
		{
			int checklength = travelresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = travelresourceform.getTravelid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						
						if( travelresourceform.getCheck( i ).substring( 1 , travelresourceform.getCheck( i ).length() ).equals( travelresourceform.getTravelid( j ) ) )
						{	
							index[k] = j;
							k++;
						}
						
						/*if( travelresourceform.getCheck(i).equals( travelresourceform.getTravelid( j ) ) )
						{	
							index[k] = j;
							k++;
						}*/
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				for( int i = 0; i < index.length; i++ )
				{
					travelresource = new TravelResource();
					travelresource.setTravelid( travelresourceform.getTravelid ( index[i] ) );
					
					travelresource.setTravelcostlibid( travelresourceform.getTravelcostlibid ( index[i] ) );
					
					travelresource.setActivity_Id( travelresourceform.getActivity_Id() );
					
					travelresource.setQuantity( travelresourceform.getQuantity ( index[i] ) );
					
					travelresource.setPrevquantity( travelresourceform.getPrevquantity( index[i] ) );
					
					if( travelresourceform.getChkaddendum().equals( "View" ) )
					{
						travelresource.setTraveltype( "T" );
					}
					
					if( travelresourceform.getChkaddendum().equals( "ViewAddendum" ) )
					{
						travelresource.setTraveltype( "AT" );
					}
					
					if( travelresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
					{
						travelresource.setTraveltype( "CT" );
					}
					
					if( travelresourceform.getChkaddendum().equals( "Viewinscope" ) )
					{
						travelresource.setTraveltype( "IT" );
					}
					
					
					travelresource.setEstimatedunitcost( travelresourceform.getEstimatedunitcost( index[i] ) );
					
					travelresource.setProformamargin( travelresourceform.getProformamargin( index[i] ) );
					/* for multiple resource select start*/
					travelresource.setCnspartnumber( travelresourceform.getCnspartnumber( index[i] ) );
					travelresource.setFlag(travelresourceform.getFlag(index[i]));
					if(travelresourceform.getFlag(index[i]).equals("P"))
					{
						int updateflag = Resourcedao.updatetravelresource( travelresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "updateflag" , ""+updateflag );
					}
					if(travelresourceform.getFlag(index[i]).equals("T"))
					{
						addflag = Resourcedao.addtravelresource( travelresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "addflag" , ""+addflag );
					}
					/* for multiple resource select end*/
					/*int updateflag = Resourcedao.updatetravelresource( travelresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );*/
				}	
			}
		}
		
		if( travelresourceform.getNewtravelid() != null )
		{
			travelresource = new TravelResource();
			travelresource.setTravelid( travelresourceform.getNewtravelnamecombo() );
			travelresource.setActivity_Id( travelresourceform.getActivity_Id() );
			travelresource.setCnspartnumber( travelresourceform.getNewcnspartnumber() );
			travelresource.setQuantity( travelresourceform.getNewquantity() );
			if( travelresourceform.getChkaddendum().equals( "View" ) )
			{
				travelresource.setTraveltype( "T" );
			}
			
			if( travelresourceform.getChkaddendum().equals( "ViewAddendum" ) )
			{
				travelresource.setTraveltype( "AT" );
			}
			
			if( travelresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
			{
				travelresource.setTraveltype( "CT" );
			}
			
			if( travelresourceform.getChkaddendum().equals( "Viewinscope" ) )
			{
				travelresource.setTraveltype( "IT" );
			}
			
			
			travelresource.setEstimatedunitcost( travelresourceform.getNewestimatedunitcost() );
			travelresource.setProformamargin( travelresourceform.getNewproformamargin() );
			
			addflag = Resourcedao.addtravelresource( travelresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		if( travelresourceform.getFromflag().equals( "jobdashboard" ) || travelresourceform.getFromflag().equals( "jobdashboard_resource" ) )
		{
			request.setAttribute( "jobid" , travelresourceform.getDashboardid() );
			return mapping.findForward( "jobdashboard" );
		}
		
		
		travelresourceform.reset( mapping , request );
		
		travelresourceform.setNewquantity( "0.0000" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setActivity_Id( Activity_Id );
		
		//travelresourceform.setRef( null );
		travelresourceform.setTravellist( Resourcedao.getResourcecombolist( "travel" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		travelresourceform.setBacktoactivity( flag1 );
		travelresourceform.setChkaddendum( flag2 );
		travelresourceform.setChkaddendumdetail( flag3 );
				
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		
		
		if( travelresourceform.getChkaddendum().equals( "View" ) )
		{
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "T" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setRef( "View" );
		}
		
		if( travelresourceform.getChkaddendum().equals( "Viewinscope" ) )
		{
			status = com.mind.dao.PRM.Activitydao.getCurrentstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IT" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setRef( "Viewinscope" );
		}
		
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setAppendix_id( idname.getId() );
		travelresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		
		
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setJobid( Activitydao.getJobid( getDataSource( request , "ilexnewDB" ) , Activity_Id ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "traveltabularpage" ) );
	}
	
	
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		int deleteflag = -1;
		String status = "";
		String tempflagforref = "";
		ArrayList travelresourcelist = new ArrayList();
		String activityname = "";
		NameId idname = null;
		
		HttpSession session = request.getSession( true );
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			travelresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = travelresourceform.getActivity_Id();
		}
		
		deleteflag = Resourcedao.deleteresource( request.getParameter( "Travel_Id" ), userid,  getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		travelresourceform.setNewquantity( "0.0000" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setActivity_Id( Activity_Id );
		travelresourceform.setTravellist( Resourcedao.getResourcecombolist( "travel" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		if( request.getParameter( "type" ) != null )
		{
			tempflagforref = request.getParameter( "type" );
		}
		
		if( tempflagforref.equals( "View" ) )
		{
			travelresourceform.setBacktoactivity( "detailactivity" );
			travelresourceform.setChkaddendum( "View" );
			travelresourceform.setChkaddendumdetail( "View" );
			travelresourceform.setAddendum_id( "0" );
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "T" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setRef( "View" );
		}
		
		if( tempflagforref.equals( "Viewinscope" ) )
		{
			travelresourceform.setBacktoactivity( "inscopedetailactivity" );
			travelresourceform.setChkaddendum( "Viewinscope" );
			travelresourceform.setChkaddendumdetail( "Viewinscope" );
			travelresourceform.setAddendum_id( "0" );
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IT" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setRef( "Viewinscope" );
		}
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setAppendix_id( idname.getId() );
		travelresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setJobid( Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "traveltabularpage" ) );
	}
	
	
	public ActionForward Viewinscope( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		String sortqueryclause = "";
		String activityname = "";
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		String Job_Id ="";
		NameId idname = null;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			travelresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = travelresourceform.getActivity_Id();
		}
		
		if( request.getParameter( "from" ) != null )
		{
			travelresourceform.setFromflag( request.getParameter( "from" ) );
			travelresourceform.setDashboardid( request.getParameter( "jobid" ) );
		
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		Job_Id = Activitydao.getJobid( getDataSource( request , "ilexnewDB" ) , Activity_Id );
		travelresourceform.setJobid( Job_Id );
		//		added by hamid
		request.setAttribute( "jobid" , Job_Id );
		
		//	Start :Added By Amit
		if( request.getParameter( "fromPage" ) != null )
		{
			travelresourceform.setFromPage( ( String )request.getParameter( "fromPage" ) );
		}
		
		//End : Added By Amit
		
		travelresourceform.setBacktoactivity( "inscopedetailactivity" );
		travelresourceform.setChkaddendum( "Viewinscope" );
		travelresourceform.setChkaddendumdetail( "Viewinscope" );
		travelresourceform.setAddendum_id( "0" );
		
		
		travelresourceform.setNewquantity( "0.0000" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setActivity_Id( Activity_Id );
		travelresourceform.setTravellist( Resourcedao.getResourcecombolist( "travel" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		ArrayList travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IT" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setAppendix_id( idname.getId() );
		travelresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		
		
		/* for checkbox check start */
		
		String checkTempResources[] = Resourcedao.checkTempResources( Activity_Id , "IT" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setCheck( checkTempResources );
		/* for checkbox check end */
		
		
		status = com.mind.dao.PRM.Activitydao.getCurrentstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "traveltabularpage" ) );
	}
	
	
	/**
	 * @ This method will be called for mass delete.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward massDelete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		TravelResource tempresource = null;
		String Activity_Id = "";
		int resource_size = 0;
		int deleteflag = -1;
		String status = "";
		String tempflagforref = "";
		ArrayList travelresourcelist = new ArrayList();
		String activityname = "";
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			travelresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = travelresourceform.getActivity_Id();
		}
		
		//Start :Added By Amit
		if( travelresourceform.getCheck() !=  null )
		{
			int checklength = travelresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = travelresourceform.getTravelid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( travelresourceform.getCheck( i ).substring( 1 , travelresourceform.getCheck( i ).length() ).equals( travelresourceform.getTravelid( j ) ) )
						{	
							index[k] = j;
							k++;
						}
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				for( int i = 0; i < index.length; i++ )
				{
					tempresource = new TravelResource();
					tempresource.setTravelid( travelresourceform.getTravelid( ( index[i] ) ) );
					
					deleteflag = Resourcedao.deleteresource( tempresource.getTravelid(), userid, getDataSource( request , "ilexnewDB" ) );
				}
			}
		}
		//End :
		
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		travelresourceform.setNewquantity( "0.0000" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setActivity_Id( Activity_Id );
		travelresourceform.setTravellist( Resourcedao.getResourcecombolist( "travel" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		if( request.getParameter( "type" ) != null )
		{
			tempflagforref = request.getParameter( "type" );
		}
		
		if( tempflagforref.equals( "View" ) )
		{
			travelresourceform.setBacktoactivity( "detailactivity" );
			travelresourceform.setChkaddendum( "View" );
			travelresourceform.setChkaddendumdetail( "View" );
			travelresourceform.setAddendum_id( "0" );
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "T" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setRef( "View" );
		}
		
		if( tempflagforref.equals( "Viewinscope" ) )
		{
			travelresourceform.setBacktoactivity( "inscopedetailactivity" );
			travelresourceform.setChkaddendum( "Viewinscope" );
			travelresourceform.setChkaddendumdetail( "Viewinscope" );
			travelresourceform.setAddendum_id( "0" );
			travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IT" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setRef( "Viewinscope" );
		}
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setAppendix_id( idname.getId() );
		travelresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		travelresourceform.setRef( "View" ); 
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setJobid( Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "traveltabularpage" ) );
	}
	
	/**
	 * This method will be invoked for massapprove.
	 * @author amitm
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	
	public ActionForward massApprove( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		TravelResource tempresource = null;
		int changestatusflag = -1; 
		String Activitylibrary_Id ="";
		String activityname = "";
		String Activity_Id = "";
		String sortqueryclause = "";
		String typenet = "";
		int resource_size = 0;
		
		String checkforresourcelink = "";
		String status = "";
		
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( userid , "Activity" , "View Resources" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) != null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			travelresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = travelresourceform.getActivity_Id();
		}
		
		/*if( request.getParameter( "from" ) != null )
		{
			travelresourceform.setFromflag( request.getParameter( "from" ) );
			travelresourceform.setDashboardid( request.getParameter( "jobid" ) );
			
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}*/
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			
		}
		
			if( travelresourceform.getCheck() !=  null )
			{
				int checklength = travelresourceform.getCheck().length;	 
		
				int []index = new int[checklength];
				int k = 0;
				
				if( checklength > 0 )
				{	int length = travelresourceform.getTravelid().length;
					for( int i = 0; i < checklength; i++ )
					{
						for( int j = 0; j < length; j++ )
						{
							if( travelresourceform.getCheck( i ).substring( 1 , travelresourceform.getCheck( i ).length() ).equals( travelresourceform.getTravelid( j ) ) )
							{	
								index[k] = j;
								k++;
							}
						}
					}
				}
			
				if( checklength > 0 )                               
				{
					for( int i = 0; i < index.length; i++ )
					{
						tempresource = new TravelResource();
						tempresource.setTravelid( travelresourceform.getTravelid( index[i] ) );
						tempresource.setActivity_Id( travelresourceform.getActivity_Id() );
						//materialresource.setMaterialtype( "M" );
						changestatusflag = ChangeStatus.Status( "" , "" , "" , "" , tempresource.getTravelid() , "pm_resT" , "A" , "N" , getDataSource( request , "ilexnewDB" ),"" );
					}
				}
			}
			
			NameId idname = null;
			travelresourceform.setNewquantity( "0.0000" );
			travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
			travelresourceform.setNewstatus( "Draft" );
			travelresourceform.setActivity_Id( Activity_Id );
			travelresourceform.setTravellist( Resourcedao.getResourcecombolist( "travel" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
			
			travelresourceform.setBacktoactivity( "detailactivity" );       //flag for backtoactivity
			travelresourceform.setChkaddendum( "View" );                    //flag for labor , freight , travel tabular
			travelresourceform.setChkaddendumdetail( "View" );              //flag for materialdetail
			travelresourceform.setAddendum_id( "0" );
			travelresourceform.setRef( "View" );  
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setAppendix_id( idname.getId() );
			travelresourceform.setAppendixname( idname.getName() );
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setMsa_id( idname.getId() );
			travelresourceform.setMsaname( idname.getName() );
			
			ArrayList travelresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "T" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			
			
			if( travelresourcelist.size() > 0 )
			{
				resource_size = travelresourcelist.size();
			}
			
			
			/* for checkbox check start */
			
			String checkTempResources[] = Resourcedao.checkTempResources(Activity_Id , "T" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			travelresourceform.setCheck( checkTempResources );
			/* for checkbox check end */
			
			status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			
			request.setAttribute( "act_status" , status );
			request.setAttribute( "activityname" , activityname );
			
			request.setAttribute( "Size" , new Integer ( resource_size ) );
			request.setAttribute( "travelresourcelist" , travelresourcelist );
			
			request.setAttribute( "refresh" , "true" );
			request.setAttribute( "Activity_Id" , Activity_Id );
		
		return( mapping.findForward( "traveltabularpage" ) );
	}

	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "pmresT_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "pmresT_sort" );
		return queryclause;
	}
}
