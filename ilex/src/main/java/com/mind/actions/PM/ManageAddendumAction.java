package com.mind.actions.PM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Addendumdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.newjobdb.business.ProjectPlanningNote;

public class ManageAddendumAction extends com.mind.common.IlexAction {
	public ActionForward execute( ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response ) throws Exception 
	{
		String Appendix_Id = null;
		String Viewjobtype = null;
		String OwnerId = null;
		String MsaId = null;
		String from = null;
		String flag = null;
		String retval ="";
		int returnflag = -1;
		String addendumId = "";
		
		HttpSession session = request.getSession( true );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		String userid = ( String )session.getAttribute( "userid" );
		
		if( request.getParameter( "Appendix_Id" ) != null )
		{
			Appendix_Id = request.getParameter( "Appendix_Id" );
		}
		if( request.getParameter( "viewjobtype" ) != null )
		{
			Viewjobtype = request.getParameter( "viewjobtype" );
		}
		if( request.getParameter( "ownerId" ) != null )
		{
			OwnerId = request.getParameter( "ownerId" );
		}
		if( request.getParameter( "from" ) != null )
		{
			from = request.getParameter( "from" );
		}
		
		if( request.getParameter( "flag" ) != null )
		{
			flag = request.getParameter( "flag" );
		}
		
		retval = Addendumdao.AddAddendum( Appendix_Id , flag , from , userid , getDataSource( request , "ilexnewDB" ) );
	
		returnflag = Integer.parseInt(retval.substring(0,retval.indexOf(",")));
		addendumId = retval.substring(retval.indexOf(",")+1, retval.length());
		if(request.getParameter( "flag" )!=null && request.getParameter( "flag" ).trim().equalsIgnoreCase("A"))
		{
			ProjectPlanningNote.copyToJob(getDataSource( request , "ilexnewDB" ),Appendix_Id,addendumId+"",userid);
		}
		request.setAttribute( "type" , from );
		request.setAttribute( "Appendix_Id" , Appendix_Id );
		request.setAttribute( "addendumId" , addendumId );
		request.setAttribute( "copyaddendumflag" , returnflag+"" );
		
		if( from.equalsIgnoreCase( "PM" ) )	
			return( mapping.findForward( "jobtabularpage" ) );
		
		if( from.equalsIgnoreCase( "PRM" ) )
			return( mapping.findForward( "appendixdashboard" ) );
		
		if( from.equalsIgnoreCase( "AddAddendumActivity" ) ) {
		
			String contractDocoumentList = com.mind.dao.PM.Appendixdao.getcontractDocoumentMenu( Appendix_Id , getDataSource( request , "ilexnewDB" )  );
			request.setAttribute("contractDocMenu" , contractDocoumentList );
			String appendixcurrentstatus = Appendixdao.getCurrentstatus(Appendix_Id,getDataSource( request , "ilexnewDB" ));
			request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
			MsaId = IMdao.getMSAId(Appendix_Id, getDataSource(request,"ilexnewDB"));
			request.setAttribute("viewjobtype", Viewjobtype);
			request.setAttribute( "ownerId" , OwnerId);
			request.setAttribute( "msaId" , MsaId);
			request.setAttribute("appendixid",Appendix_Id);
			
			return( mapping.findForward( "addaddendumactivity" ) );
		}	
		
		
	
		return( mapping.findForward( "success" ) );
	}
}
