package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.ResourceFreight;
import com.mind.dao.AM.ResourceLabor;
import com.mind.dao.AM.ResourceMaterial;
import com.mind.dao.AM.ResourceTravel;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.formbean.AM.ResourceListForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.dao.JobContainerDao;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.util.WebUtil;

public class ResourceListAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ResourceListAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ResourceListForm resourcelistform = (ResourceListForm) form;
		if (request.getParameter("through") != null) {
			request.setAttribute("through", request.getParameter("through"));
		}
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		ArrayList materialresourcelist = new ArrayList();
		ArrayList laborresourcelist = new ArrayList();
		ArrayList freightresourcelist = new ArrayList();
		ArrayList travelresourcelist = new ArrayList();
		ResourceMaterial resourcematerial = null;
		ResourceLabor resourcelabor = null;
		ResourceTravel resourcetravel = null;
		ResourceFreight resourcefreight = null;
		NameId idname = null;
		NameId appendixidname = null;
		String sortqueryclause = "";
		String Activitylibrary_Id = "";
		int material_resource_size = 0;
		int labour_resource_size = 0;
		int freight_resource_size = 0;
		int travel_resource_size = 0;
		String activityname = "";
		String typenet = "";
		String type = "";
		String status = "";
		int addflag = 0;
		int updateflag = 0;
		int deleteflag = 0;
		String resourceListType = "";
		String check_netmedx_appendix = "N"; // for transit field
		Map<String, Object> map;

		/*
		 * if(resourcelistform.getMaterialCheck() != null &&
		 * resourcelistform.getMaterialCheck().length > 0){ for(int l=0;
		 * l<resourcelistform.getMaterialCheck().length; l++){
		 * System.out.println
		 * ("MaterialCheck["+l+"]					::	"+resourcelistform.getMaterialCheck
		 * (l));
		 * System.out.println("MaterialQuantity["+l+"]					::	"+resourcelistform
		 * .getMaterialQuantity(l));
		 * System.out.println("MaterialProformamargin["
		 * +l+"]			::	"+resourcelistform.getMaterialProformamargin(l));
		 * System.out
		 * .println("MaterialPriceunit["+l+"]				::	"+resourcelistform.
		 * getMaterialPriceunit(l));
		 * System.out.println("MaterialId["+l+"] 						::	"
		 * +resourcelistform.getMaterialid(l));
		 * System.out.println("MaterialMinimumquantity["
		 * +l+"]			::	"+resourcelistform.getMaterialMinimumquantity(l));
		 * System.out
		 * .println("MaterialName["+l+"] 					::	"+resourcelistform.getMaterialname
		 * (l));
		 * //System.out.println("MaterialManufacturerpartnumber["+l+"] 	::	"
		 * +resourcelistform.getMaterialManufacturerpartnumber(l));
		 * System.out.println
		 * ("MaterialStatus["+l+"] 					::	"+resourcelistform.getMaterialStatus
		 * (l)); System.out.println("MaterialSellablequantity["+l+"] 		::	"+
		 * resourcelistform.getMaterialSellablequantity(l));
		 * System.out.println("MaterialCostlibid["
		 * +l+"]		 		::	"+resourcelistform.getMaterialCostlibid(l));
		 * System.out.println
		 * ("MaterialCnspartnumber["+l+"]		 	::	"+resourcelistform
		 * .getMaterialCnspartnumber(l));
		 * System.out.println("MaterialEstimatedtotalcost["
		 * +l+"]		::	"+resourcelistform.getMaterialEstimatedtotalcost(l));
		 * System
		 * .out.println("MaterialEstimatedunitcost["+l+"]		::	"+resourcelistform
		 * .getMaterialEstimatedunitcost(l));
		 * System.out.println("MaterialPrevquantity["
		 * +l+"]				::	"+resourcelistform.getMaterialPrevquantity(l));
		 * System.out
		 * .println("MaterialPriceextended["+l+"]			::	"+resourcelistform
		 * .getMaterialPriceextended(l)); } }
		 * if(resourcelistform.getLaborCheck() != null &&
		 * resourcelistform.getLaborCheck().length > 0){ for(int l=0;
		 * l<resourcelistform.getLaborCheck().length; l++){
		 * System.out.println("LaborCheck["
		 * +l+"]						::	"+resourcelistform.getLaborCheck(l)); //
		 * System.out.println
		 * ("MaterialQuantity["+l+"]					::	"+resourcelistform.
		 * getLaborQuantity(l));
		 * System.out.println("LaborProformamargin["+l+"]			::	"
		 * +resourcelistform.getLaborProformamargin(l));
		 * System.out.println("LaborPriceunit["
		 * +l+"]				::	"+resourcelistform.getLaborPriceunit(l));
		 * System.out.println
		 * ("LaborId["+l+"] 						::	"+resourcelistform.getLaborid(l));
		 * System.out
		 * .println("LaborMinimumquantity["+l+"]			::	"+resourcelistform
		 * .getLaborMinimumquantity(l));
		 * System.out.println("LaborName["+l+"] 					::	"
		 * +resourcelistform.getLaborname(l));
		 * System.out.println("LaborStatus["+
		 * l+"] 					::	"+resourcelistform.getLaborStatus(l));
		 * System.out.println
		 * ("LaborSellablequantity["+l+"] 		::	"+resourcelistform
		 * .getLaborSellablequantity(l));
		 * System.out.println("LaborCostlibid["+l+
		 * "]		 		::	"+resourcelistform.getLaborcostlibid(l));
		 * System.out.println
		 * ("LaborCnspartnumber["+l+"]		 	::	"+resourcelistform
		 * .getLaborCnspartnumber(l));
		 * System.out.println("LaborEstimatedtotalcost["
		 * +l+"]		::	"+resourcelistform.getLaborEstimatedtotalcost(l));
		 * System.out
		 * .println("LaborEstimatedunitcost["+l+"]		::	"+resourcelistform
		 * .getLaborEstimatedhourlybasecost(l));
		 * System.out.println("LaborPrevquantity["
		 * +l+"]			::	"+resourcelistform.getLaborPrevquantityhours(l));
		 * System.out
		 * .println("LaborPriceextended["+l+"]			::	"+resourcelistform.
		 * getLaborPriceextended(l));
		 * System.out.println("LaborLaborTransit["+l+"]			::	"
		 * +resourcelistform.getLaborTransit(l));
		 * 
		 * } } if(resourcelistform.getTravelCheck() != null &&
		 * resourcelistform.getTravelCheck().length > 0){ for(int l=0;
		 * l<resourcelistform.getTravelCheck().length; l++){
		 * System.out.println("TravelCheck["
		 * +l+"]					::	"+resourcelistform.getTravelCheck(l));
		 * System.out.println
		 * ("TravelQuantity["+l+"]				::	"+resourcelistform.getTravelQuantity
		 * (l));
		 * System.out.println("TravelProformamargin["+l+"]			::	"+resourcelistform
		 * .getTravelProformamargin(l));
		 * System.out.println("TravelPriceunit["+l+
		 * "]				::	"+resourcelistform.getTravelPriceunit(l));
		 * System.out.println
		 * ("TravelId["+l+"] 					::	"+resourcelistform.getTravelid(l));
		 * System.out
		 * .println("TravelMinimumquantity["+l+"]		::	"+resourcelistform
		 * .getTravelMinimumquantity(l));
		 * System.out.println("TravelName["+l+"] 					::	"
		 * +resourcelistform.getTravelname(l));
		 * System.out.println("TravelStatus["
		 * +l+"] 				::	"+resourcelistform.getTravelStatus(l));
		 * System.out.println
		 * ("TravelSellablequantity["+l+"] 		::	"+resourcelistform
		 * .getTravelSellablequantity(l));
		 * System.out.println("TravelCostlibid["+
		 * l+"]		 		::	"+resourcelistform.getTravelcostlibid(l));
		 * System.out.println
		 * ("TravelCnspartnumber["+l+"]		 	::	"+resourcelistform
		 * .getTravelCnspartnumber(l));
		 * System.out.println("TravelEstimatedtotalcost["
		 * +l+"]		::	"+resourcelistform.getTravelEstimatedtotalcost(l));
		 * System.out
		 * .println("TravelEstimatedunitcost["+l+"]		::	"+resourcelistform
		 * .getTravelEstimatedunitcost(l));
		 * System.out.println("TravelPrevquantity["
		 * +l+"]			::	"+resourcelistform.getTravelPrevquantity(l));
		 * System.out.println
		 * ("TravelPriceextended["+l+"]			::	"+resourcelistform
		 * .getTravelPriceextended(l)); } }
		 * if(resourcelistform.getFreightCheck() != null &&
		 * resourcelistform.getFreightCheck().length > 0){ for(int l=0;
		 * l<resourcelistform.getFreightCheck().length; l++){
		 * System.out.println(
		 * "FreightCheck["+l+"]						::	"+resourcelistform.getFreightCheck(l));
		 * System.out.println("FreighQuantity["+l+"]					::	"+resourcelistform.
		 * getFreightQuantity(l));
		 * System.out.println("FreighProformamargin["+l+"]				::	"
		 * +resourcelistform.getFreightProformamargin(l));
		 * System.out.println("FreighPriceunit["
		 * +l+"]					::	"+resourcelistform.getFreightPriceunit(l));
		 * System.out.println
		 * ("FreighId["+l+"] 						::	"+resourcelistform.getFreightid(l));
		 * System
		 * .out.println("FreighMinimumquantity["+l+"]			::	"+resourcelistform
		 * .getFreightMinimumquantity(l));
		 * System.out.println("FreighName["+l+"] 						::	"
		 * +resourcelistform.getFreightname(l));
		 * System.out.println("FreighStatus["
		 * +l+"] 					::	"+resourcelistform.getFreightStatus(l));
		 * System.out.println
		 * ("FreighSellablequantity["+l+"] 			::	"+resourcelistform
		 * .getFreightSellablequantity(l));
		 * System.out.println("FreighCostlibid["
		 * +l+"]		 			::	"+resourcelistform.getFreightcostlibid(l));
		 * System.out.println
		 * ("FreighCnspartnumber["+l+"]		 		::	"+resourcelistform
		 * .getFreightCnspartnumber(l));
		 * System.out.println("FreighEstimatedtotalcost["
		 * +l+"]			::	"+resourcelistform.getFreightEstimatedtotalcost(l));
		 * System
		 * .out.println("FreighEstimatedunitcost["+l+"]			::	"+resourcelistform
		 * .getFreightEstimatedunitcost(l));
		 * System.out.println("FreighPrevquantity["
		 * +l+"]				::	"+resourcelistform.getFreightPrevquantity(l));
		 * System.out.
		 * println("FreightPriceextended["+l+"]				::	"+resourcelistform
		 * .getFreightPriceextended(l)); } }
		 */
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "Activity",
				"View Resources", getDataSource(request, "ilexnewDB")))
			return (mapping.findForward("UnAuthenticate"));

		if (request.getAttribute("tempdeleteflag") != null) {
		} else {
			ActivityLibrarydao.deleteTempResources(
					request.getSession().getId(),
					getDataSource(request, "ilexnewDB"));
		}

		if (resourcelistform.getEdit() != null
				|| request.getAttribute("edit") != null
				|| request.getParameter("editable") != null) {
			resourcelistform.setEdit("editable");
		}

		if (request.getParameter("Activity_Id") != null) {
			resourcelistform
					.setActivity_Id(request.getParameter("Activity_Id"));
		}

		if (request.getAttribute("Activity_Id") != null) {
			resourcelistform.setActivity_Id(request.getAttribute("Activity_Id")
					.toString());
		}

		if (request.getParameter("querystring") != null) {
			sortqueryclause = (request.getParameter("querystring"));
		}

		if (request.getParameter("Activitylibrary_Id") != null) {
			Activitylibrary_Id = request.getParameter("Activitylibrary_Id");
		}

		if (request.getParameter("type") != null) {
			typenet = request.getParameter("type");
		}
		if (request.getParameter("ref") != null) {
			resourcelistform.setChkaddendum(request.getParameter("ref"));
			resourcelistform.setChkaddendumdetail(request.getParameter("ref"));
		}

		if (request.getParameter("Material_Id") != null) {
			resourcelistform
					.setResource_Id(request.getParameter("Material_Id"));
		}

		if (request.getParameter("Labor_Id") != null) {
			resourcelistform.setResource_Id(request.getParameter("Labor_Id"));
		}

		if (request.getParameter("Travel_Id") != null) {
			resourcelistform.setResource_Id(request.getParameter("Travel_Id"));
		}

		if (request.getParameter("Freight_Id") != null) {
			resourcelistform.setResource_Id(request.getParameter("Freight_Id"));
		}

		if (request.getParameter("resourceListType") != null) {
			resourcelistform.setResourceListType(request
					.getParameter("resourceListType"));
		}

		if (request.getParameter("sowaddflag") != null
				&& !request.getParameter("sowaddflag").equals("")) {
			request.setAttribute("sowaddflag",
					request.getParameter("sowaddflag"));
		}

		if (request.getParameter("sowupdateflag") != null
				&& !request.getParameter("sowupdateflag").equals("")) {
			request.setAttribute("sowupdateflag",
					request.getParameter("sowupdateflag"));
		}

		if (request.getParameter("assumptionaddflag") != null
				&& !request.getParameter("assumptionaddflag").equals("")) {
			request.setAttribute("assumptionaddflag",
					request.getParameter("assumptionaddflag"));
		}

		if (request.getParameter("assumptionupdateflag") != null
				&& !request.getParameter("assumptionupdateflag").equals("")) {
			request.setAttribute("assumptionupdateflag",
					request.getParameter("assumptionupdateflag"));
		}

		if (typenet.equals("Delete")) {

			deleteflag = Resourcedao.deleteresource(
					resourcelistform.getResource_Id(), userid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("deleteflag", "" + deleteflag);
		}
		if (request.getParameter("from") != null
				&& request.getParameter("from") != "") {
			resourcelistform.setFromflag(request.getParameter("from"));
			resourcelistform.setDashboardid(request.getParameter("jobid"));

			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}
		/*
		 * if(request.getAttribute( "from" ) != null && request.getAttribute(
		 * "from" ) != "") { resourcelistform.setFromflag(
		 * request.getAttribute("from").toString() );
		 * resourcelistform.setDashboardid( request.getAttribute( "jobid"
		 * ).toString() );
		 * 
		 * request.setAttribute( "from" , request.getAttribute( "from" ) );
		 * request.setAttribute( "jobid" , request.getAttribute( "jobid" ) ); }
		 */

		appendixidname = Activitydao.getIdName(
				resourcelistform.getActivity_Id(), "Resource", "Appendix",
				getDataSource(request, "ilexnewDB"));
		resourcelistform.setAppendix_id(appendixidname.getId());
		resourcelistform.setAppendixName(appendixidname.getName());

		idname = Activitydao.getIdName(resourcelistform.getActivity_Id(),
				"Resource", "MSA", getDataSource(request, "ilexnewDB"));
		resourcelistform.setMsaId(idname.getId());
		resourcelistform.setMsaName(idname.getName());

		resourcelistform.setJobName(Jobdao.getoplevelname(
				resourcelistform.getActivity_Id(), "Resource",
				getDataSource(request, "ilexnewDB")));
		resourcelistform.setJobId(Activitydao.getJobid(
				getDataSource(request, "ilexnewDB"),
				resourcelistform.getActivity_Id()));
		activityname = Activitydao.getActivityname(
				resourcelistform.getActivity_Id(),
				getDataSource(request, "ilexnewDB"));
		resourcelistform.setActivityName(activityname);
		// added by pankaj
		if (resourcelistform.getJobType() == null
				|| resourcelistform.getJobType().trim().equalsIgnoreCase(""))
			resourcelistform.setJobType(JobSetUpDao.getjobType(
					resourcelistform.getJobId(),
					getDataSource(request, "ilexnewDB")));
		String loginuserid = (String) session.getAttribute("userid");

		resourcelistform.setJobStatus(JobContainerDao.getJobStatus(
				resourcelistform.getJobId(),
				getDataSource(request, "ilexnewDB")));
		if (resourcelistform.getJobId() != null
				&& !resourcelistform.getJobId().equals("")) {
			if (TicketDAO.isOnSite(
					Integer.parseInt(resourcelistform.getJobId()),
					getDataSource(request, "ilexnewDB"))) {
				if (logger.isDebugEnabled()) {
					logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - ----in side request set onsite -----------------");
				}
				request.setAttribute("onsite", "onsite");
			}
		}

		if (resourcelistform.getJobType() != null) {
			if (resourcelistform.getJobId() != null
					&& !resourcelistform.getJobId().trim().equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(resourcelistform.getJobId(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}
		// added by pankaj

		status = Activitydao.getActivitystatus(
				resourcelistform.getActivity_Id(),
				getDataSource(request, "ilexnewDB"));

		int checkNetmedXappendix = 0;
		checkNetmedXappendix = Activitydao.checkAppendixname(
				resourcelistform.getAppendix_id(), resourcelistform.getJobId(),
				getDataSource(request, "ilexnewDB"));

		if ((checkNetmedXappendix == 1) && typenet.equals("PM")) {
			type = request.getParameter("type");
			request.setAttribute("type", type);
			request.setAttribute("MSA_Id", resourcelistform.getMsaId());
			// Activitylibrary_Id=request.getParameter("Activitylibrary_Id");
			request.setAttribute("Activitylibrary_Id", Activitylibrary_Id);
			request.setAttribute("appendixId",
					resourcelistform.getAppendix_id());
			request.setAttribute("appendixName",
					resourcelistform.getAppendixName());
			return mapping.findForward("customerlaborratepage");
		}

		if (request.getParameter("resourceListType") != null
				&& request.getParameter("resourceListType").equals("A")) {
			request.setAttribute("allResources", "present");
		}

		// Mass Update/Delete for Material:Start

		/* fot tramsit start */
		check_netmedx_appendix = Resourcedao.checkNetmedxAppendix(
				resourcelistform.getActivity_Id(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("check_netmedx_appendix", check_netmedx_appendix);
		resourcelistform.setChecknetmedx(check_netmedx_appendix);
		/* fot tramsit end */

		if (resourcelistform.getSave() != null
				|| resourcelistform.getDelete() != null) {

			if (resourcelistform.getMaterialCheck() != null) {
				int checklength = resourcelistform.getMaterialCheck().length;

				int[] index = new int[checklength];
				int k = 0;

				if (checklength > 0) {
					int length = resourcelistform.getMaterialid().length;
					for (int i = 0; i < checklength; i++) {
						for (int j = 0; j < length; j++) {

							if (resourcelistform
									.getMaterialCheck(i)
									.substring(
											1,
											resourcelistform
													.getMaterialCheck(i)
													.length())
									.equals(resourcelistform.getMaterialid(j))) {
								index[k] = j;
								k++;
							}
						}
					}
				}

				if (checklength > 0) {
					for (int i = 0; i < index.length; i++) {
						resourcematerial = new ResourceMaterial();
						resourcematerial.setMaterialid(resourcelistform
								.getMaterialid(index[i]));
						resourcematerial.setMaterialCostlibid(resourcelistform
								.getMaterialCostlibid(index[i]));
						resourcematerial.setActivity_Id(resourcelistform
								.getActivity_Id());
						resourcematerial.setQuantity(resourcelistform
								.getMaterialQuantity(index[i]));
						resourcematerial
								.setMaterialPrevquantity(resourcelistform
										.getMaterialPrevquantity(index[i]));

						if (resourcelistform.getChkaddendum().equals("View")
								|| resourcelistform.getChkaddendum()
										.equalsIgnoreCase("detailactivity")) {
							resourcematerial.setMaterialtype("M");
						}

						if (resourcelistform.getChkaddendum().equals(
								"Viewinscope")
								|| resourcelistform.getChkaddendumdetail()
										.equals("inscopedetailactivity")) {
							resourcematerial.setMaterialtype("IM");
							if (resourcelistform.getJobType() != null
									&& resourcelistform.getJobType().equals(
											"Newjob")) {
								resourcematerial.setMaterialtype("M");
							}
						}
						resourcematerial
								.setMaterialEstimatedunitcost(resourcelistform
										.getMaterialEstimatedunitcost(index[i]));
						resourcematerial
								.setMaterialProformamargin(resourcelistform
										.getMaterialProformamargin(index[i]));
						/* for multiple resource select start */
						resourcematerial
								.setMaterialCnspartnumber(resourcelistform
										.getMaterialCnspartnumber(index[i]));
						resourcematerial.setMaterialFlag(resourcelistform
								.getMaterialFlag(index[i]));

						if (resourcelistform.getMaterialFlag(index[i]).equals(
								"P")
								&& resourcelistform.getDelete() == null) {
							updateflag = Resourcedao.updateresourcematerial(
									resourcematerial, userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("updateresourceflag", ""
									+ updateflag);
						}

						if (resourcelistform.getDelete() != null) {
							deleteflag = Resourcedao.deleteresource(
									resourcematerial.getMaterialid(), userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("deleteresourceflag", ""
									+ deleteflag);
						}
					}
				}

				resourcelistform.materialreset(mapping, request);
			}
			// Mass Update/Delete for Material:End

			// Mass Update/Delete for Labor:Start

			if (resourcelistform.getLaborCheck() != null) {
				int checklength = resourcelistform.getLaborCheck().length;

				int[] index = new int[checklength];
				int k = 0;
				int checklengthtransit = 0;
				int length = 0;
				if (resourcelistform.getLaborid() != null) {
					length = resourcelistform.getLaborid().length;
				}

				// int []index = new int[length];

				if (resourcelistform.getLaborChecktransit() != null) {
					checklengthtransit = resourcelistform
							.getLaborChecktransit().length;

				}

				int t = 0;

				if (checklength > 0) {
					for (int i = 0; i < checklength; i++) {
						for (int j = 0; j < length; j++) {
							if (resourcelistform
									.getLaborCheck(i)
									.substring(
											1,
											resourcelistform.getLaborCheck(i)
													.length())
									.equals(resourcelistform.getLaborid(j))) {
								index[k] = j;
								k++;
								break;
							}
						}
					}
				}

				if (checklength > 0) {
					for (int i = 0; i < index.length; i++) {
						resourcelabor = new ResourceLabor();

						if (resourcelistform.getLaborid(index[i]).indexOf("~") > 0) {
							resourcelabor.setLaborid(resourcelistform
									.getLaborid(index[i]).substring(
											0,
											resourcelistform.getLaborid(
													index[i]).indexOf("~")));
							resourcelabor.setClr_detail_id(resourcelistform
									.getLaborid(index[i]).substring(
											resourcelistform.getLaborid(
													index[i]).indexOf("~") + 1,
											resourcelistform.getLaborid(
													index[i]).length()));
						} else {
							resourcelabor.setLaborid(resourcelistform
									.getLaborid(index[i]));
							resourcelabor.setClr_detail_id("0");
						}
						resourcelabor.setLaborcostlibid(resourcelistform
								.getLaborcostlibid(index[i]));
						resourcelabor.setActivity_Id(resourcelistform
								.getActivity_Id());
						resourcelabor.setLaborQuantityhours(resourcelistform
								.getLaborQuantityhours(index[i]));
						resourcelabor
								.setLaborPrevquantityhours(resourcelistform
										.getLaborPrevquantityhours(index[i]));
						if (resourcelistform.getChkaddendum().equals("View")
								|| resourcelistform.getChkaddendum()
										.equalsIgnoreCase("detailactivity")) {
							resourcelabor.setLabortype("L");
						}
						if (resourcelistform.getChkaddendum().equals(
								"ViewAddendum")) {
							resourcelabor.setLabortype("AL");
						}
						if (resourcelistform.getChkaddendum().equals(
								"ViewChangeorder")) {
							resourcelabor.setLabortype("CL");
						}
						if (resourcelistform.getChkaddendum().equals(
								"Viewinscope")
								|| resourcelistform.getChkaddendumdetail()
										.equals("inscopedetailactivity")) {
							resourcelabor.setLabortype("IL");
							if (resourcelistform.getJobType() != null
									&& resourcelistform.getJobType().equals(
											"Newjob")) {
								resourcelabor.setLabortype("L");
							}
						}
						resourcelabor
								.setLaborEstimatedhourlybasecost(resourcelistform
										.getLaborEstimatedhourlybasecost(index[i]));
						resourcelabor.setLaborProformamargin(resourcelistform
								.getLaborProformamargin(index[i]));
						resourcelabor.setLaborTransit(resourcelistform
								.getLaborTransit(index[i]));

						/* for multiple resource select start */
						resourcelabor.setLaborCnspartnumber(resourcelistform
								.getLaborCnspartnumber(index[i]));
						resourcelabor.setLaborFlag(resourcelistform
								.getLaborFlag(index[i]));
						if (resourcelistform.getLaborFlag(index[i]).equals("P")
								&& resourcelistform.getDelete() == null) {
							updateflag = Resourcedao.updateresourcelabor(
									resourcelabor, userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("updateresourceflag", ""
									+ updateflag);
						}
						/* for multiple resource select end */

						if (resourcelistform.getDelete() != null) {

							deleteflag = Resourcedao.deleteresource(
									resourcelabor.getLaborid(), userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("deleteresourceflag", ""
									+ deleteflag);
						}

					}
				}

				resourcelistform.laborreset(mapping, request);

			}

			// Mass Update/Delete for Labor:End

			// Mass Update/Delete for Travel:Start

			if (resourcelistform.getTravelCheck() != null) {
				int checklength = resourcelistform.getTravelCheck().length;

				int[] index = new int[checklength];
				int k = 0;

				if (checklength > 0) {
					int length = resourcelistform.getTravelid().length;
					for (int i = 0; i < checklength; i++) {
						for (int j = 0; j < length; j++) {

							if (resourcelistform
									.getTravelCheck(i)
									.substring(
											1,
											resourcelistform.getTravelCheck(i)
													.length())
									.equals(resourcelistform.getTravelid(j))) {
								index[k] = j;
								k++;
							}

							/*
							 * if( travelresourceform.getCheck(i).equals(
							 * travelresourceform.getTravelid( j ) ) ) {
							 * index[k] = j; k++; }
							 */
						}
					}
				}

				if (checklength > 0) {
					for (int i = 0; i < index.length; i++) {
						resourcetravel = new ResourceTravel();
						resourcetravel.setTravelid(resourcelistform
								.getTravelid(index[i]));
						resourcetravel.setTravelcostlibid(resourcelistform
								.getTravelcostlibid(index[i]));
						resourcetravel.setActivity_Id(resourcelistform
								.getActivity_Id());
						resourcetravel.setTravelQuantity(resourcelistform
								.getTravelQuantity(index[i]));
						resourcetravel.setTravelPrevquantity(resourcelistform
								.getTravelPrevquantity(index[i]));
						if (resourcelistform.getChkaddendum().equals("View")
								|| resourcelistform.getChkaddendum()
										.equalsIgnoreCase("detailactivity")) {
							resourcetravel.setTraveltype("T");
						}
						if (resourcelistform.getChkaddendum().equals(
								"ViewAddendum")) {
							resourcetravel.setTraveltype("AT");
						}
						if (resourcelistform.getChkaddendum().equals(
								"ViewChangeorder")) {
							resourcetravel.setTraveltype("CT");
						}
						if (resourcelistform.getChkaddendum().equals(
								"Viewinscope")
								|| resourcelistform.getChkaddendumdetail()
										.equals("inscopedetailactivity")) {
							resourcetravel.setTraveltype("IT");
							if (resourcelistform.getJobType() != null
									&& resourcelistform.getJobType().equals(
											"Newjob")) {
								resourcetravel.setTraveltype("T");
							}
						}
						resourcetravel
								.setTravelEstimatedunitcost(resourcelistform
										.getTravelEstimatedunitcost(index[i]));
						resourcetravel.setTravelProformamargin(resourcelistform
								.getTravelProformamargin(index[i]));
						/* for multiple resource select start */
						resourcetravel.setTravelCnspartnumber(resourcelistform
								.getTravelCnspartnumber(index[i]));
						resourcetravel.setTravelFlag(resourcelistform
								.getTravelFlag(index[i]));
						if (resourcelistform.getTravelFlag(index[i])
								.equals("P")
								&& resourcelistform.getDelete() == null) {
							updateflag = Resourcedao.updateresourcetravel(
									resourcetravel, userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("updateresourceflag", ""
									+ updateflag);
						}

						if (resourcelistform.getDelete() != null) {

							deleteflag = Resourcedao.deleteresource(
									resourcetravel.getTravelid(), userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("deleteresourceflag", ""
									+ deleteflag);
						}
					}
				}

				resourcelistform.travelreset(mapping, request);

			}

			// Mass Update/Delete for Travel:End

			// Mass Update/Delete for Freight:Start

			if (resourcelistform.getFreightCheck() != null) {
				int checklength = resourcelistform.getFreightCheck().length;

				int[] index = new int[checklength];
				int k = 0;

				if (checklength > 0) {
					int length = resourcelistform.getFreightid().length;
					for (int i = 0; i < checklength; i++) {
						for (int j = 0; j < length; j++) {
							if (resourcelistform
									.getFreightCheck(i)
									.substring(
											1,
											resourcelistform.getFreightCheck(i)
													.length())
									.equals(resourcelistform.getFreightid(j))) {
								index[k] = j;
								k++;
							}
						}
					}
				}

				if (checklength > 0) {
					for (int i = 0; i < index.length; i++) {
						resourcefreight = new ResourceFreight();
						resourcefreight.setFreightid(resourcelistform
								.getFreightid(index[i]));
						resourcefreight.setFreightcostlibid(resourcelistform
								.getFreightcostlibid(index[i]));
						resourcefreight.setActivity_Id(resourcelistform
								.getActivity_Id());
						resourcefreight.setFreightQuantity(resourcelistform
								.getFreightQuantity(index[i]));
						resourcefreight.setFreightPrevquantity(resourcelistform
								.getFreightPrevquantity(index[i]));
						if (resourcelistform.getChkaddendum().equals("View")
								|| resourcelistform.getChkaddendum()
										.equalsIgnoreCase("detailactivity")) {
							resourcefreight.setFreighttype("F");
						}
						if (resourcelistform.getChkaddendum().equals(
								"ViewAddendum")) {
							resourcefreight.setFreighttype("AF");
						}
						if (resourcelistform.getChkaddendum().equals(
								"ViewChangeorder")) {
							resourcefreight.setFreighttype("CF");
						}
						if (resourcelistform.getChkaddendum().equals(
								"Viewinscope")
								|| resourcelistform.getChkaddendumdetail()
										.equals("inscopedetailactivity")) {
							resourcefreight.setFreighttype("IF");
							if (resourcelistform.getJobType() != null
									&& resourcelistform.getJobType().equals(
											"Newjob")) {
								resourcefreight.setFreighttype("F");
							}
						}
						resourcefreight
								.setFreightEstimatedunitcost(resourcelistform
										.getFreightEstimatedunitcost(index[i]));
						resourcefreight
								.setFreightProformamargin(resourcelistform
										.getFreightProformamargin(index[i]));

						/* for multiple resource select start */
						resourcefreight
								.setFreightCnspartnumber(resourcelistform
										.getFreightCnspartnumber(index[i]));
						resourcefreight.setFreightFlag(resourcelistform
								.getFreightFlag(index[i]));
						if (resourcelistform.getFreightFlag(index[i]).equals(
								"P")
								&& resourcelistform.getDelete() == null) {
							updateflag = Resourcedao.updateresourcefreight(
									resourcefreight, userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("updateresourceflag", ""
									+ updateflag);
						}
						if (resourcelistform.getDelete() != null) {

							deleteflag = Resourcedao.deleteresource(
									resourcefreight.getFreightid(), userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("deleteresourceflag", ""
									+ deleteflag);
						}
					}
				}
			}

			resourcelistform.freightreset(mapping, request);
			resourcelistform.setEdit("editable");
		}

		// Mass Update/Delete for Freight:End

		// Added by pankaj
		if (resourcelistform.getChkaddendumdetail().equals("View")
				|| resourcelistform.getChkaddendumdetail().equalsIgnoreCase(
						"detailactivity")) {
			resourcelistform.setChkaddendumdetail("View");
			resourcelistform.setChkaddendum("View");
		}

		// Added by pankaj
		// form job dashboard:Start
		if (resourcelistform.getChkaddendumdetail().equals("Viewinscope")
				|| resourcelistform.getChkaddendumdetail().equals(
						"inscopedetailactivity")) {
			String resourceType = "";
			boolean checkresource = false;
			status = com.mind.dao.PRM.Activitydao.getCurrentstatus(
					resourcelistform.getActivity_Id(),
					getDataSource(request, "ilexnewDB"));
			resourcelistform.setFrom(request.getParameter("from"));
			resourcelistform.setChkaddendumdetail("Viewinscope");

			// Travel Resource:Start
			if (resourcelistform.getResourceListType() != null
					&& (resourcelistform.getResourceListType().equals("T") || resourcelistform
							.getResourceListType().equals("A"))) {

				if (resourcelistform.getJobType() != null
						&& resourcelistform.getJobType().equals("Newjob")) {
					resourceType = "T";
				} else {
					resourceType = "IT";
				}

				resourcelistform.setFromflag(request.getParameter("from"));
				resourcelistform.setDashboardid(request.getParameter("jobid"));
				resourcelistform.setChkaddendumdetail("Viewinscope");
				travelresourcelist = Resourcedao.getResourcelist(
						resourcelistform.getActivity_Id(), resourceType, "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
				travel_resource_size = travelresourcelist.size();

				Iterator iteratorTravel = travelresourcelist.iterator();
				checkresource = false;
				while (iteratorTravel.hasNext()) {
					ResourceTravel resourceTravel = (ResourceTravel) iteratorTravel
							.next();
					if (resourceTravel.getTravelFlag().equals("T")
							&& resourceTravel.getResourceType().equals("T")) {
						addflag = Resourcedao.addResources(
								resourceTravel.getTravelid(),
								resourcelistform.getActivity_Id(),
								resourceTravel.getTravelCnspartnumber(),
								resourceTravel.getTravelQuantity(),
								resourceType,
								resourceTravel.getTravelEstimatedunitcost(),
								resourceTravel.getTravelProformamargin(),
								userid, resourceTravel.getAddendum_id(), "",
								"", getDataSource(request, "ilexnewDB"));
						request.setAttribute("addresourceflag", "" + addflag);
						checkresource = true;
					}
				}
				if (checkresource == true) {
					ActivityLibrarydao.deleteTempResources(request.getSession()
							.getId(), getDataSource(request, "ilexnewDB"));
					travelresourcelist = Resourcedao.getResourcelist(
							resourcelistform.getActivity_Id(), resourceType,
							"%", sortqueryclause, request.getSession().getId(),
							"listpage", getDataSource(request, "ilexnewDB"));
				}
				// travelresourcelist = Resourcedao.getResourcelist(
				// resourcelistform.getActivity_Id() , "IT" , "%" ,
				// sortqueryclause , request.getSession().getId() , "listpage",
				// getDataSource( request , "ilexnewDB" ) );
				if (travelresourcelist.size() > 0)
					travel_resource_size = travelresourcelist.size();
				resourcelistform.setType("pm_resT");
				request.setAttribute("travel_Size", new Integer(
						travel_resource_size));
				request.setAttribute("travelresourcelist", travelresourcelist);
			}
			// Travel Resource:End

			// Matarial Resource:Start
			if (resourcelistform.getResourceListType() != null
					&& (resourcelistform.getResourceListType().equals("M") || resourcelistform
							.getResourceListType().equals("A"))) {

				if (resourcelistform.getJobType() != null
						&& resourcelistform.getJobType().equals("Newjob")) {
					resourceType = "M";
				} else {
					resourceType = "IM";
				}

				resourcelistform.setFromflag(request.getParameter("from"));
				resourcelistform.setDashboardid(request.getParameter("jobid"));
				materialresourcelist = Resourcedao.getResourcelist(
						resourcelistform.getActivity_Id(), resourceType, "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
				Iterator iteratorMaterial = materialresourcelist.iterator();
				checkresource = false;
				while (iteratorMaterial.hasNext()) {
					ResourceMaterial resourceMaterial = (ResourceMaterial) iteratorMaterial
							.next();
					if (resourceMaterial.getMaterialFlag().equals("T")
							&& resourceMaterial.getResourceType().equals("M")) {
						addflag = Resourcedao
								.addResources(
										resourceMaterial.getMaterialid(),
										resourcelistform.getActivity_Id(),
										resourceMaterial
												.getMaterialCnspartnumber(),
										resourceMaterial.getQuantity(),
										resourceType,
										resourceMaterial
												.getMaterialEstimatedunitcost(),
										resourceMaterial
												.getMaterialProformamargin(),
										userid, resourceMaterial
												.getAddendum_id(), "", "",
										getDataSource(request, "ilexnewDB"));
						request.setAttribute("addresourceflag", "" + addflag);
						checkresource = true;
					}
				}
				if (checkresource == true) {
					ActivityLibrarydao.deleteTempResources(request.getSession()
							.getId(), getDataSource(request, "ilexnewDB"));
					materialresourcelist = Resourcedao.getResourcelist(
							resourcelistform.getActivity_Id(), resourceType,
							"%", sortqueryclause, request.getSession().getId(),
							"listpage", getDataSource(request, "ilexnewDB"));
				}
				// materialresourcelist = Resourcedao.getResourcelist(
				// resourcelistform.getActivity_Id() , "IM" , "%" ,
				// sortqueryclause , request.getSession().getId(), "listpage",
				// getDataSource( request , "ilexnewDB" ) );
				if (materialresourcelist.size() > 0)
					material_resource_size = materialresourcelist.size();
				resourcelistform.setType("pm_resM");
				request.setAttribute("material_Size", new Integer(
						material_resource_size));
				request.setAttribute("materialresourcelist",
						materialresourcelist);
			}
			// Matarial Resource:End

			// Freight Resource:Start
			if (resourcelistform.getResourceListType() != null
					&& (resourcelistform.getResourceListType().equals("F") || resourcelistform
							.getResourceListType().equals("A"))) {

				if (resourcelistform.getJobType() != null
						&& resourcelistform.getJobType().equals("Newjob")) {
					resourceType = "F";
				} else {
					resourceType = "IF";
				}

				resourcelistform.setFromflag(request.getParameter("from"));
				resourcelistform.setDashboardid(request.getParameter("jobid"));
				resourcelistform.setChkaddendumdetail("Viewinscope");
				freightresourcelist = Resourcedao.getResourcelist(
						resourcelistform.getActivity_Id(), resourceType, "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
				Iterator iteratorFreight = freightresourcelist.iterator();
				while (iteratorFreight.hasNext()) {
					ResourceFreight resourceFreight = (ResourceFreight) iteratorFreight
							.next();
					if (resourceFreight.getFreightFlag().equals("T")
							&& resourceFreight.getResourceType().equals("F")) {
						addflag = Resourcedao.addResources(
								resourceFreight.getFreightid(),
								resourcelistform.getActivity_Id(),
								resourceFreight.getFreightCnspartnumber(),
								resourceFreight.getFreightQuantity(),
								resourceType,
								resourceFreight.getFreightEstimatedunitcost(),
								resourceFreight.getFreightProformamargin(),
								userid, resourceFreight.getAddendum_id(), "",
								"", getDataSource(request, "ilexnewDB"));
						request.setAttribute("addresourceflag", "" + addflag);
						checkresource = true;
					}
				}
				if (checkresource == true) {
					ActivityLibrarydao.deleteTempResources(request.getSession()
							.getId(), getDataSource(request, "ilexnewDB"));
					freightresourcelist = Resourcedao.getResourcelist(
							resourcelistform.getActivity_Id(), resourceType,
							"%", sortqueryclause, request.getSession().getId(),
							"listpage", getDataSource(request, "ilexnewDB"));
				}
				// freightresourcelist = Resourcedao.getResourcelist(
				// resourcelistform.getActivity_Id(), "IF" , "%" ,
				// sortqueryclause , request.getSession().getId() , "listpage",
				// getDataSource( request , "ilexnewDB" ) );
				if (freightresourcelist.size() > 0)
					freight_resource_size = freightresourcelist.size();
				resourcelistform.setType("pm_resF");
				request.setAttribute("freight_Size", new Integer(
						freight_resource_size));
				request.setAttribute("freightresourcelist", freightresourcelist);
			}
			// Freight Resource:End

			// Labor Resource:Start
			if (resourcelistform.getResourceListType() != null
					&& (resourcelistform.getResourceListType().equals("L") || resourcelistform
							.getResourceListType().equals("A"))) {

				if (resourcelistform.getJobType() != null
						&& resourcelistform.getJobType().equals("Newjob")) {
					resourceType = "L";
				} else {
					resourceType = "IL";
				}

				resourcelistform.setFromflag(request.getParameter("from"));
				resourcelistform.setDashboardid(request.getParameter("jobid"));
				resourcelistform.setChkaddendumdetail("Viewinscope");
				laborresourcelist = Resourcedao.getResourcelist(
						resourcelistform.getActivity_Id(), resourceType, "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
				Iterator iteratorLabor = laborresourcelist.iterator();
				checkresource = false;
				while (iteratorLabor.hasNext()) {
					ResourceLabor resourceLabor = (ResourceLabor) iteratorLabor
							.next();
					if (resourceLabor.getLaborFlag().equals("T")
							&& resourceLabor.getResourceType().equals("L")) {
						addflag = Resourcedao
								.addResources(
										resourceLabor.getLaborid(),
										resourcelistform.getActivity_Id(),
										resourceLabor.getLaborCnspartnumber(),
										resourceLabor.getLaborQuantityhours(),
										resourceType,
										resourceLabor
												.getLaborEstimatedhourlybasecost(),
										resourceLabor.getLaborProformamargin(),
										userid, resourceLabor.getAddendum_id(),
										resourceLabor.getLaborTransit(),
										resourceLabor.getClr_detail_id(),
										getDataSource(request, "ilexnewDB"));
						request.setAttribute("addresourceflag", "" + addflag);
						checkresource = true;
					}
				}
				if (checkresource == true) {
					ActivityLibrarydao.deleteTempResources(request.getSession()
							.getId(), getDataSource(request, "ilexnewDB"));
					laborresourcelist = Resourcedao.getResourcelist(
							resourcelistform.getActivity_Id(), resourceType,
							"%", sortqueryclause, request.getSession().getId(),
							"listpage", getDataSource(request, "ilexnewDB"));
				}
				// laborresourcelist = Resourcedao.getResourcelist(
				// resourcelistform.getActivity_Id(), "IL" , "%" ,
				// sortqueryclause , request.getSession().getId(), "listpage",
				// getDataSource( request , "ilexnewDB" ) );
				if (laborresourcelist.size() > 0) {
					labour_resource_size = laborresourcelist.size();
					getTicketDefaultMinimumHour(resourcelistform,
							getDataSource(request, "ilexnewDB"));
				}
				resourcelistform.setType("pm_resL");
				request.setAttribute("labor_Size", new Integer(
						labour_resource_size));
				request.setAttribute("laborresourcelist", laborresourcelist);
			}
			// Labor Resource:End

			/* for transit field start */

			if (resourcelistform.getJobType() != null
					&& resourcelistform.getJobType().equals("Newjob")) {
				resourceType = "L";
			} else {
				resourceType = "IL";
			}

			String savedtransitresource[] = Resourcedao
					.getSavedTransitResources(
							resourcelistform.getActivity_Id(), resourceType,
							"%", sortqueryclause, request.getSession().getId(),
							getDataSource(request, "ilexnewDB"));
			resourcelistform.setLaborChecktransit(savedtransitresource);
			/* for transit field end */

			/* Check for editable Resourced */
			resourcelistform.setEditableResource(Activitydao.getEditableFlag(
					resourcelistform.getActivity_Id(),
					getDataSource(request, "ilexnewDB")));

			request.setAttribute("act_status", status);
			request.setAttribute("checkingRef",
					resourcelistform.getChkaddendum());
			return mapping.findForward("resourcelistpage");
		}
		// form job dashboard:End

		// From Proposal Manager:Srart
		if (resourcelistform.getChkaddendumdetail().equals("View")
				|| resourcelistform.getChkaddendumdetail().equals(
						"detailactivity")) {
			boolean checkresource = false;

			// Travel List:Start

			if (resourcelistform.getResourceListType() != null
					&& (resourcelistform.getResourceListType().equals("T") || resourcelistform
							.getResourceListType().equals("A"))) {
				travelresourcelist = Resourcedao.getResourcelist(
						resourcelistform.getActivity_Id(), "T", "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
				travel_resource_size = travelresourcelist.size();

				Iterator iteratorTravel = travelresourcelist.iterator();
				checkresource = false;
				while (iteratorTravel.hasNext()) {
					ResourceTravel resourceTravel = (ResourceTravel) iteratorTravel
							.next();
					if (resourceTravel.getTravelFlag().equals("T")
							&& resourceTravel.getResourceType().equals("T")) {
						addflag = Resourcedao.addResources(
								resourceTravel.getTravelid(),
								resourcelistform.getActivity_Id(),
								resourceTravel.getTravelCnspartnumber(),
								resourceTravel.getTravelQuantity(), "T",
								resourceTravel.getTravelEstimatedunitcost(),
								resourceTravel.getTravelProformamargin(),
								userid, resourceTravel.getAddendum_id(), "",
								"", getDataSource(request, "ilexnewDB"));
						request.setAttribute("addresourceflag", "" + addflag);
						checkresource = true;
					}
				}
				if (checkresource == true) {
					ActivityLibrarydao.deleteTempResources(request.getSession()
							.getId(), getDataSource(request, "ilexnewDB"));
					travelresourcelist = Resourcedao.getResourcelist(
							resourcelistform.getActivity_Id(), "T", "%",
							sortqueryclause, request.getSession().getId(),
							"listpage", getDataSource(request, "ilexnewDB"));
				}
				// travelresourcelist = Resourcedao.getResourcelist(
				// resourcelistform.getActivity_Id() , "T" , "%" ,
				// sortqueryclause , request.getSession().getId() , "listpage",
				// getDataSource( request , "ilexnewDB" ) );
				if (travelresourcelist.size() > 0)
					travel_resource_size = travelresourcelist.size();
				resourcelistform.setType("pm_resT");
				request.setAttribute("travel_Size", new Integer(
						travel_resource_size));
				request.setAttribute("travelresourcelist", travelresourcelist);
			}
			// Travel List:End

			// Material List:Start

			if (resourcelistform.getResourceListType() != null
					&& (resourcelistform.getResourceListType().equals("M") || resourcelistform
							.getResourceListType().equals("A"))) {
				materialresourcelist = Resourcedao.getResourcelist(
						resourcelistform.getActivity_Id(), "M", "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
				Iterator iteratorMaterial = materialresourcelist.iterator();
				while (iteratorMaterial.hasNext()) {
					ResourceMaterial resourceMaterial = (ResourceMaterial) iteratorMaterial
							.next();
					checkresource = false;
					if (resourceMaterial.getMaterialFlag().equals("T")
							&& resourceMaterial.getResourceType().equals("M")) {
						addflag = Resourcedao
								.addResources(
										resourceMaterial.getMaterialid(),
										resourcelistform.getActivity_Id(),
										resourceMaterial
												.getMaterialCnspartnumber(),
										resourceMaterial.getQuantity(),
										"M",
										resourceMaterial
												.getMaterialEstimatedunitcost(),
										resourceMaterial
												.getMaterialProformamargin(),
										userid, resourceMaterial
												.getAddendum_id(), "", "",
										getDataSource(request, "ilexnewDB"));
						request.setAttribute("addresourceflag", "" + addflag);
						checkresource = true;
					}
				}
				if (checkresource == true) {
					ActivityLibrarydao.deleteTempResources(request.getSession()
							.getId(), getDataSource(request, "ilexnewDB"));
					materialresourcelist = Resourcedao.getResourcelist(
							resourcelistform.getActivity_Id(), "M", "%",
							sortqueryclause, request.getSession().getId(),
							"listpage", getDataSource(request, "ilexnewDB"));
				}
				// materialresourcelist = Resourcedao.getResourcelist(
				// resourcelistform.getActivity_Id() , "M" , "%" ,
				// sortqueryclause , request.getSession().getId(), "listpage",
				// getDataSource( request , "ilexnewDB" ) );
				if (materialresourcelist.size() > 0)
					material_resource_size = materialresourcelist.size();
				resourcelistform.setType("pm_resM");
				request.setAttribute("material_Size", new Integer(
						material_resource_size));
				request.setAttribute("materialresourcelist",
						materialresourcelist);

			}

			// Material List:End

			// Labor List:Start
			if (resourcelistform.getResourceListType() != null
					&& (resourcelistform.getResourceListType().equals("L") || resourcelistform
							.getResourceListType().equals("A"))) {
				laborresourcelist = Resourcedao.getResourcelist(
						resourcelistform.getActivity_Id(), "L", "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
				Iterator iteratorLabor = laborresourcelist.iterator();
				checkresource = false;
				while (iteratorLabor.hasNext()) {
					ResourceLabor resourceLabor = (ResourceLabor) iteratorLabor
							.next();
					if (resourceLabor.getLaborFlag().equals("T")
							&& resourceLabor.getResourceType().equals("L")) {
						addflag = Resourcedao
								.addResources(
										resourceLabor.getLaborid(),
										resourcelistform.getActivity_Id(),
										resourceLabor.getLaborCnspartnumber(),
										resourceLabor.getLaborQuantityhours(),
										"L",
										resourceLabor
												.getLaborEstimatedhourlybasecost(),
										resourceLabor.getLaborProformamargin(),
										userid, resourceLabor.getAddendum_id(),
										resourceLabor.getLaborTransit(),
										resourceLabor.getClr_detail_id(),
										getDataSource(request, "ilexnewDB"));
						request.setAttribute("addresourceflag", "" + addflag);
						checkresource = true;
					}
				}
				if (checkresource == true) {
					ActivityLibrarydao.deleteTempResources(request.getSession()
							.getId(), getDataSource(request, "ilexnewDB"));
					laborresourcelist = Resourcedao.getResourcelist(
							resourcelistform.getActivity_Id(), "L", "%",
							sortqueryclause, request.getSession().getId(),
							"listpage", getDataSource(request, "ilexnewDB"));
				}
				// laborresourcelist = Resourcedao.getResourcelist(
				// resourcelistform.getActivity_Id(), "L" , "%" ,
				// sortqueryclause , request.getSession().getId(), "listpage",
				// getDataSource( request , "ilexnewDB" ) );
				if (laborresourcelist.size() > 0)
					labour_resource_size = laborresourcelist.size();
				resourcelistform.setType("pm_resL");
				request.setAttribute("labor_Size", new Integer(
						labour_resource_size));
				request.setAttribute("laborresourcelist", laborresourcelist);
			}

			// Labor List:End

			// Freight List:Start

			if (resourcelistform.getResourceListType() != null
					&& (resourcelistform.getResourceListType().equals("F") || resourcelistform
							.getResourceListType().equals("A"))) {
				freightresourcelist = Resourcedao.getResourcelist(
						resourcelistform.getActivity_Id(), "F", "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
				Iterator iteratorFreight = freightresourcelist.iterator();
				checkresource = false;
				while (iteratorFreight.hasNext()) {
					ResourceFreight resourceFreight = (ResourceFreight) iteratorFreight
							.next();
					if (resourceFreight.getFreightFlag().equals("T")
							&& resourceFreight.getResourceType().equals("F")) {
						addflag = Resourcedao.addResources(
								resourceFreight.getFreightid(),
								resourcelistform.getActivity_Id(),
								resourceFreight.getFreightCnspartnumber(),
								resourceFreight.getFreightQuantity(), "F",
								resourceFreight.getFreightEstimatedunitcost(),
								resourceFreight.getFreightProformamargin(),
								userid, resourceFreight.getAddendum_id(), "",
								"", getDataSource(request, "ilexnewDB"));
						request.setAttribute("addresourceflag", "" + addflag);
						checkresource = true;
					}
				}
				if (checkresource == true) {
					ActivityLibrarydao.deleteTempResources(request.getSession()
							.getId(), getDataSource(request, "ilexnewDB"));
					freightresourcelist = Resourcedao.getResourcelist(
							resourcelistform.getActivity_Id(), "F", "%",
							sortqueryclause, request.getSession().getId(),
							"listpage", getDataSource(request, "ilexnewDB"));
				}
				// freightresourcelist = Resourcedao.getResourcelist(
				// resourcelistform.getActivity_Id(), "F" , "%" ,
				// sortqueryclause , request.getSession().getId() , "listpage",
				// getDataSource( request , "ilexnewDB" ) );
				if (freightresourcelist.size() > 0)
					freight_resource_size = freightresourcelist.size();
				resourcelistform.setType("pm_resF");
				request.setAttribute("freight_Size", new Integer(
						freight_resource_size));
				request.setAttribute("freightresourcelist", freightresourcelist);

			}

			// Freight List:End
		}

		/* for transit field start */

		String savedtransitresource[] = Resourcedao.getSavedTransitResources(
				resourcelistform.getActivity_Id(), "L", "%", sortqueryclause,
				request.getSession().getId(),
				getDataSource(request, "ilexnewDB"));
		resourcelistform.setLaborChecktransit(savedtransitresource);
		/* for transit field end */

		/* for checkbox check start */

		request.setAttribute("act_status", status);
		request.setAttribute("checkingRef", resourcelistform.getChkaddendum());
		return mapping.findForward("resourcelistpage");
	}

	// from Project Manager:End

	private void getTicketDefaultMinimumHour(ResourceListForm resourceListForm,
			DataSource ds) {
		String[] ticketDefaultMinimumHour = Resourcedao
				.getTicketDefaultMinimumHour(resourceListForm.getActivity_Id(),
						ds);
		resourceListForm.setTicketMinLaborHour(ticketDefaultMinimumHour[0]);
		resourceListForm.setTicketMinTravelHour(ticketDefaultMinimumHour[1]);
		resourceListForm.setTicketRequestType(ticketDefaultMinimumHour[2]);
	}
}
