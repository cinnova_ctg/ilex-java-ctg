//Detail Page formBean

package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.Appendix;
import com.mind.common.dao.Menu;
import com.mind.common.dao.Upload;
import com.mind.dao.PM.MSA;
import com.mind.dao.PM.MSAdao;
import com.mind.formbean.PM.MSADetailForm;
import com.mind.util.WebUtil;

public class MSADetailAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MSADetailForm msadetailform = (MSADetailForm) form;
		Appendix appendix = new Appendix();
		MSA msa = new MSA();
		String MSA_Id = "";
		ArrayList uploadeddocs = new ArrayList();

		// Variables added for the View Selector Functioning.
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "msa";
		String msaid = "0";
		int ownerListSize = 0;
		String Appendix_Id = "";

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("MSA_Id") != null)
			MSA_Id = request.getParameter("MSA_Id");

		if (request.getAttribute("MSA_Id") != null) {
			MSA_Id = (String) request.getAttribute("MSA_Id");
			request.setAttribute("MSA_Id", MSA_Id);
		}

		if (session.getAttribute("appendixSelectedOwners") != null
				|| session.getAttribute("appendixSelectedStatus") != null)
			request.setAttribute("specialCase", "specialCase");

		if (session.getAttribute("monthAppendix") != null
				|| session.getAttribute("weekAppendix") != null) {
			request.setAttribute("specialCase", "specialCase");
		}

		// Added to refersh the previous Appendix List state::::
		WebUtil.removeAppendixSession(session);

		// This is required for the div to remain open after pressing show.
		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		// The Menu Images are to be set in session respective of one other.
		if (request.getParameter("month") != null) {
			session.setAttribute("monthMSA", request.getParameter("month")
					.toString());
			monthMSA = request.getParameter("month").toString();
			session.removeAttribute("weekMSA");
			msadetailform.setMonthWeekCheck("month");
		}

		if (request.getParameter("week") != null) {
			session.setAttribute("weekMSA", request.getParameter("week")
					.toString());
			weekMSA = request.getParameter("week").toString();
			session.removeAttribute("monthMSA");
			msadetailform.setMonthWeekCheck("week");
		}

		// When the reset button is pressed remove every thing from the session.
		if (request.getParameter("home") != null) {
			msadetailform.setSelectedStatus(null);
			msadetailform.setSelectedOwners(null);
			msadetailform.setPrevSelectedStatus("");
			msadetailform.setPrevSelectedOwner("");
			msadetailform.setOtherCheck(null);
			msadetailform.setMonthWeekCheck(null);
			// MSAdao.setValuesWhenReset(session);
			return (mapping.findForward("msalist"));
		}
		if (request.getAttribute("Appendix_Id") != null) {

			/*
			 * Appendix_Id = Appendixdao.getAppendix("%", Appendix_Id,
			 * getDataSource(request, "ilexnewDB"));
			 */

			request.setAttribute("Appendix_Id", Appendix_Id);
		}

		String appendixtype = appendix.getAppendixtype();
		request.setAttribute("Appendix_Id", Appendix_Id);
		request.setAttribute("appendixType", appendixtype);

		// when the other check box is checked in the view selector.
		if (msadetailform.getOtherCheck() != null)
			session.setAttribute("otherCheck", msadetailform.getOtherCheck()
					.toString());

		if (msadetailform.getSelectedStatus() != null) {
			for (int i = 0; i < msadetailform.getSelectedStatus().length; i++) {
				selectedStatus = selectedStatus + "," + "'"
						+ msadetailform.getSelectedStatus(i) + "'";
				tempStatus = tempStatus + ","
						+ msadetailform.getSelectedStatus(i);
			}

			selectedStatus = selectedStatus.substring(1,
					selectedStatus.length());
			selectedStatus = "(" + selectedStatus + ")";
		}

		if (msadetailform.getSelectedOwners() != null) {
			for (int j = 0; j < msadetailform.getSelectedOwners().length; j++) {
				selectedOwner = selectedOwner + ","
						+ msadetailform.getSelectedOwners(j);
				tempOwner = tempOwner + ","
						+ msadetailform.getSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if (selectedOwner.equals("0")) {
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" + selectedOwner + ")";
		}

		// To set the values in the session when the Show button is Pressed.
		if (msadetailform.getGo() != null && !msadetailform.getGo().equals("")) {
			session.setAttribute("selectedOwners", selectedOwner);
			session.setAttribute("MSASelectedOwners", selectedOwner);
			session.setAttribute("tempOwner", tempOwner);
			session.setAttribute("selectedStatus", selectedStatus);
			session.setAttribute("MSASelectedStatus", selectedStatus);
			session.setAttribute("tempStatus", tempStatus);

			if (msadetailform.getMonthWeekCheck() != null) {
				if (msadetailform.getMonthWeekCheck().equals("week")) {
					session.removeAttribute("monthMSA");
					session.setAttribute("weekMSA", "0");
				}
				if (msadetailform.getMonthWeekCheck().equals("month")) {
					session.removeAttribute("weekMSA");
					session.setAttribute("monthMSA", "0");
				}
				if (msadetailform.getMonthWeekCheck().equals("clear")) {
					session.removeAttribute("monthMSA");
					session.removeAttribute("weekMSA");
				}

			}

			if (session.getAttribute("monthMSA") != null) {
				session.removeAttribute("weekMSA");
				monthMSA = session.getAttribute("monthMSA").toString();
			}
			if (session.getAttribute("weekMSA") != null) {
				session.removeAttribute("monthMSA");
				weekMSA = session.getAttribute("weekMSA").toString();
			}
			return (mapping.findForward("msalist"));
		} else // If not pressed see the values exists in the session or not.
		{
			if (session.getAttribute("selectedOwners") != null) {
				selectedOwner = session.getAttribute("selectedOwners")
						.toString();
				msadetailform.setSelectedOwners(session
						.getAttribute("tempOwner").toString().split(","));
			}
			if (session.getAttribute("selectedStatus") != null) {
				selectedStatus = session.getAttribute("selectedStatus")
						.toString();
				msadetailform.setSelectedStatus(session
						.getAttribute("tempStatus").toString().split(","));
			}
			if (session.getAttribute("otherCheck") != null)
				msadetailform.setOtherCheck(session.getAttribute("otherCheck")
						.toString());
		}

		if (session.getAttribute("monthMSA") != null) {
			session.removeAttribute("weekMSA");
			monthMSA = session.getAttribute("monthMSA").toString();
			msadetailform.setMonthWeekCheck("month");
		}
		if (session.getAttribute("weekMSA") != null) {
			session.removeAttribute("monthMSA");
			weekMSA = session.getAttribute("weekMSA").toString();
			msadetailform.setMonthWeekCheck("week");
		}

		if (!chkOtherOwner) {
			msadetailform.setPrevSelectedStatus(selectedStatus);
			msadetailform.setPrevSelectedOwner(selectedOwner);
		}

		ownerList = MSAdao.getOwnerList(loginuserid,
				msadetailform.getOtherCheck(), ownerType, msaid,
				getDataSource(request, "ilexnewDB"));
		statusList = MSAdao.getStatusList("pm_msa",
				getDataSource(request, "ilexnewDB"));

		if (ownerList.size() > 0)
			ownerListSize = ownerList.size();

		request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", statusList);
		request.setAttribute("ownerlist", ownerList);
		request.setAttribute("ownerListSize", ownerListSize + "");

		msa = MSAdao.getMSA(MSA_Id, getDataSource(request, "ilexnewDB"));

		msadetailform.setMsaId(msa.getMsa_Id());
		msadetailform.setName(msa.getName());
		msadetailform.setMsatype(msa.getMsatype());
		msadetailform.setSalestype(msa.getSalestype());

		msadetailform.setSalesPOC(msa.getSalesPOC());
		msadetailform.setPayterms(msa.getPayterms());
		msadetailform.setPayqualifier(msa.getPayqualifier());

		msadetailform.setFormaldocdate(msa.getFormaldocdate());

		msadetailform.setReqplasch(msa.getReqplasch());
		msadetailform.setEffplasch(msa.getEffplasch());
		msadetailform.setSchdays(msa.getSchdays());

		msadetailform.setDraftreviews(msa.getDraftreviews());
		msadetailform.setBusinessreviews(msa.getBusinessreviews());
		msadetailform.setCustPocbusiness(msa.getCustPocbusiness());
		msadetailform.setCustPoccontract(msa.getCustPoccontract());
		msadetailform.setStatus(msa.getStatus());
		msadetailform.setUnionupliftfactor(msa.getUnionupliftfactor());
		msadetailform.setLastcomment(msa.getLastcomment());
		msadetailform.setLastchangeby(msa.getLastchangeby());
		msadetailform.setLastchangedate(msa.getLastchangedate());
		msadetailform.setFile_upload_check(msa.getFile_upload_check());

		msadetailform.setLastmsamodifieddate(msa.getLastmsamodifieddate());
		msadetailform.setLastmsamodifiedby(msa.getLastmsamodifiedby());

		String list = Menu.getStatus("pm_msa", msa.getStatus().charAt(0),
				msadetailform.getMsaId(), "", "", "", loginuserid,
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("list", list);

		uploadeddocs = Upload.getuploadedfiles("%", MSA_Id, "MSA",
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("uploadeddocs", uploadeddocs);

		if (request.getAttribute("addcommentflag") != null) {
			request.setAttribute("addcommentflag",
					request.getAttribute("addcommentflag"));
		}

		if (request.getAttribute("changestatusflag") != null) {
			request.setAttribute("changestatusflag",
					request.getAttribute("changestatusflag"));
		}

		if (request.getAttribute("uploadflag") != null) {
			request.setAttribute("uploadflag",
					request.getAttribute("uploadflag"));
		}

		if (request.getAttribute("emailflag") != null) {
			request.setAttribute("emailflag", request.getAttribute("emailflag"));
		}

		return (mapping.findForward("success"));
	}
}
