package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.TravelResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.AM.TravelResourceDetailForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class TravelDetailAction extends com.mind.common.IlexDispatchAction {
	public ActionForward View(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		TravelResourceDetailForm travelresourcedetailform = (TravelResourceDetailForm) form;

		TravelResource travelresource = new TravelResource();
		String travel_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Travel_Id") != null) {
			travel_Id = request.getParameter("Travel_Id");
		}

		if (request.getAttribute("Travel_Id") != null) {
			travel_Id = (String) request.getAttribute("Travel_Id");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			travelresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		travelresourcedetailform.setTravelid(travel_Id);
		request.setAttribute("Travel_Id",
				travelresourcedetailform.getTravelid());

		// ArrayList travelresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "T" , travelresourcedetailform.getTravelid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList travelresourcelist = Resourcedao.getResourcetabularlist(
				travelresourcedetailform.getActivity_Id(), "T",
				travelresourcedetailform.getTravelid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		travelresource = (TravelResource) travelresourcelist.get(0);

		travelresourcedetailform
				.setActivity_Id(travelresource.getActivity_Id());
		request.setAttribute("Activity_Id",
				travelresourcedetailform.getActivity_Id());

		travelresourcedetailform.setTravelid(travelresource.getTravelid());
		travelresourcedetailform.setTravelname(travelresource.getTravelname());
		travelresourcedetailform.setTraveltype(travelresource.getTraveltype());
		travelresourcedetailform.setMinimumquantity(travelresource
				.getMinimumquantity());
		travelresourcedetailform.setCnspartnumber(travelresource
				.getCnspartnumber());
		travelresourcedetailform.setEstimatedtotalcost(travelresource
				.getEstimatedtotalcost());
		travelresourcedetailform.setEstimatedunitcost(travelresource
				.getEstimatedunitcost());

		travelresourcedetailform.setPriceextended(travelresource
				.getPriceextended());
		travelresourcedetailform.setPriceunit(travelresource.getPriceunit());
		travelresourcedetailform.setProformamargin(travelresource
				.getProformamargin());
		travelresourcedetailform.setQuantity(travelresource.getQuantity());
		travelresourcedetailform.setSellablequantity(travelresource
				.getSellablequantity());
		travelresourcedetailform.setStatus(travelresource.getStatus());

		travelresourcedetailform.setChkaddendum("View");
		travelresourcedetailform.setChkdetailactivity("detailactivity");
		travelresourcedetailform.setAddendum_id("0");

		travelresourcedetailform.setActivityname(Activitydao.getActivityname(
				travelresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setMsa_id(idname.getId());
		travelresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setAppendix_id(idname.getId());
		travelresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setJob_id(idname.getId());
		travelresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resT", travelresourcedetailform
					.getStatus().charAt(0), from,
					request.getParameter("jobid"), "View",
					travelresourcedetailform.getTravelid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("pm_resT", travelresourcedetailform
					.getStatus().charAt(0), "", "", "",
					travelresourcedetailform.getTravelid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		}
		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				travelresourcedetailform.getTravelid(), "pm_resT");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				travelresourcedetailform.getTravelid(), "pm_resT");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				travelresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				travelresourcedetailform.getChkaddendum());
		// End

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);

		request.setAttribute("list", list);

		return mapping.findForward("traveldetailpage");
	}

	public ActionForward Viewinscope(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		TravelResourceDetailForm travelresourcedetailform = (TravelResourceDetailForm) form;

		TravelResource travelresource = new TravelResource();
		String travel_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";

		int sow_size = 0;
		int assumption_size = 0;
		NameId idname = null;
		String resourceType = "IT";
		Map<String, Object> map;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Travel_Id") != null) {
			travel_Id = request.getParameter("Travel_Id");
		}

		if (request.getAttribute("Travel_Id") != null) {
			travel_Id = (String) request.getAttribute("Travel_Id");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}
		if (request.getParameter("Activity_Id") != null) {
			travelresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		travelresourcedetailform.setTravelid(travel_Id);
		request.setAttribute("Travel_Id",
				travelresourcedetailform.getTravelid());

		if (request.getParameter("jobType") != null
				&& request.getParameter("jobType").equals("Newjob")) {
			resourceType = "T";
		}

		// ArrayList travelresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "IT" , travelresourcedetailform.getTravelid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList travelresourcelist = Resourcedao.getResourcetabularlist(
				travelresourcedetailform.getActivity_Id(), resourceType,
				travelresourcedetailform.getTravelid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		travelresource = (TravelResource) travelresourcelist.get(0);

		travelresourcedetailform
				.setActivity_Id(travelresource.getActivity_Id());
		request.setAttribute("Activity_Id",
				travelresourcedetailform.getActivity_Id());

		travelresourcedetailform.setTravelid(travelresource.getTravelid());
		travelresourcedetailform.setTravelname(travelresource.getTravelname());
		travelresourcedetailform.setTraveltype(travelresource.getTraveltype());
		travelresourcedetailform.setMinimumquantity(travelresource
				.getMinimumquantity());
		travelresourcedetailform.setCnspartnumber(travelresource
				.getCnspartnumber());
		travelresourcedetailform.setEstimatedtotalcost(travelresource
				.getEstimatedtotalcost());
		travelresourcedetailform.setEstimatedunitcost(travelresource
				.getEstimatedunitcost());

		travelresourcedetailform.setPriceextended(travelresource
				.getPriceextended());
		travelresourcedetailform.setPriceunit(travelresource.getPriceunit());
		travelresourcedetailform.setProformamargin(travelresource
				.getProformamargin());
		travelresourcedetailform.setQuantity(travelresource.getQuantity());
		travelresourcedetailform.setSellablequantity(travelresource
				.getSellablequantity());
		travelresourcedetailform.setStatus(travelresource.getStatus());

		travelresourcedetailform.setChkaddendum("Viewinscope");
		travelresourcedetailform.setChkdetailactivity("inscopedetailactivity");

		travelresourcedetailform.setAddendum_id("0");
		travelresourcedetailform.setActivityname(Activitydao.getActivityname(
				travelresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setMsa_id(idname.getId());
		travelresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setAppendix_id(idname.getId());
		travelresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setJob_id(idname.getId());
		travelresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resT", travelresourcedetailform
					.getStatus().charAt(0), from,
					request.getParameter("jobid"), "Viewinscope",
					travelresourcedetailform.getTravelid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("jobdashboard_resT", travelresourcedetailform
					.getStatus().charAt(0), "", "", "Viewinscope",
					travelresourcedetailform.getTravelid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		}

		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				travelresourcedetailform.getTravelid(), "pm_resT");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				travelresourcedetailform.getTravelid(), "pm_resT");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				travelresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				travelresourcedetailform.getChkaddendum());
		// End

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);
		if (request.getParameter("from") != null
				&& !request.getParameter("from").trim().equalsIgnoreCase("")
				&& request.getParameter("from").trim()
						.equalsIgnoreCase("jobdashboard")) {
			if (request.getParameter("jobid") != null
					&& !request.getParameter("jobid").trim()
							.equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(request.getParameter("jobid"),
						loginuserid, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}
		return mapping.findForward("traveldetailpage");
	}

	public ActionForward ViewAddendum(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		TravelResourceDetailForm travelresourcedetailform = (TravelResourceDetailForm) form;

		TravelResource travelresource = new TravelResource();
		String travel_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		int sow_size = 0;
		int assumption_size = 0;
		NameId idname = null;

		String from = "";
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Travel_Id") != null) {
			travel_Id = request.getParameter("Travel_Id");
		}

		if (request.getAttribute("Travel_Id") != null) {
			travel_Id = (String) request.getAttribute("Travel_Id");
		}

		if (request.getParameter("addendum_id") != null) {
			travelresourcedetailform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getAttribute("addendum_id") != null) {
			travelresourcedetailform.setAddendum_id(request.getAttribute(
					"addendum_id").toString());
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}
		if (request.getParameter("Activity_Id") != null) {
			travelresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		travelresourcedetailform.setTravelid(travel_Id);
		request.setAttribute("Travel_Id",
				travelresourcedetailform.getTravelid());

		// ArrayList travelresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "AT" , travelresourcedetailform.getTravelid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList travelresourcelist = Resourcedao.getResourcetabularlist(
				travelresourcedetailform.getActivity_Id(), "AT",
				travelresourcedetailform.getTravelid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		travelresource = (TravelResource) travelresourcelist.get(0);

		travelresourcedetailform
				.setActivity_Id(travelresource.getActivity_Id());
		request.setAttribute("Activity_Id",
				travelresourcedetailform.getActivity_Id());

		travelresourcedetailform.setTravelid(travelresource.getTravelid());
		travelresourcedetailform.setTravelname(travelresource.getTravelname());
		travelresourcedetailform.setTraveltype(travelresource.getTraveltype());
		travelresourcedetailform.setMinimumquantity(travelresource
				.getMinimumquantity());
		travelresourcedetailform.setCnspartnumber(travelresource
				.getCnspartnumber());
		travelresourcedetailform.setEstimatedtotalcost(travelresource
				.getEstimatedtotalcost());
		travelresourcedetailform.setEstimatedunitcost(travelresource
				.getEstimatedunitcost());

		travelresourcedetailform.setPriceextended(travelresource
				.getPriceextended());
		travelresourcedetailform.setPriceunit(travelresource.getPriceunit());
		travelresourcedetailform.setProformamargin(travelresource
				.getProformamargin());
		travelresourcedetailform.setQuantity(travelresource.getQuantity());
		travelresourcedetailform.setSellablequantity(travelresource
				.getSellablequantity());
		travelresourcedetailform.setStatus(travelresource.getStatus());

		travelresourcedetailform.setChkaddendum("ViewAddendum");
		travelresourcedetailform.setChkdetailactivity("detailaddendumactivity");
		travelresourcedetailform.setActivityname(Activitydao.getActivityname(
				travelresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setMsa_id(idname.getId());
		travelresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setAppendix_id(idname.getId());
		travelresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setJob_id(idname.getId());
		travelresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resT", travelresourcedetailform
					.getStatus().charAt(0), from,
					request.getParameter("jobid"), "ViewAddendum",
					travelresourcedetailform.getTravelid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			// list = Menu.getStatus( "pm_resT" ,
			// travelresourcedetailform.getStatus().charAt( 0 ) , "" , "" , "" ,
			// travelresourcedetailform.getTravelid() , loginuserid ,
			// getDataSource( request , "ilexnewDB" ) );
			if (travelresourcedetailform.getStatus().equals("Draft")) {
				list = "\"Approved\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ travelresourcedetailform.getAddendum_id()
						+ "&type=pm_resT&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
						+ travelresourcedetailform.getTravelid()
						+ "&Status=A\"" + "," + "0";
			}

			if (travelresourcedetailform.getStatus().equals("Approved")) {
				list = "\"Draft\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ travelresourcedetailform.getAddendum_id()
						+ "&type=pm_resT&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
						+ travelresourcedetailform.getTravelid()
						+ "&Status=D\"" + "," + "0";
			}
		}

		/*
		 * if( travelresourcedetailform.getStatus().equals( "Draft" ) ) { list =
		 * "\"Approved\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="+
		 * travelresourcedetailform.getAddendum_id()+
		 * "&type=pm_resT&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
		 * +travelresourcedetailform.getTravelid()+"&Status=A\"" + "," + "0"; }
		 * 
		 * if( travelresourcedetailform.getStatus().equals( "Approved" ) ) {
		 * list = "\"Draft\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +travelresourcedetailform.getAddendum_id()+
		 * "&type=pm_resT&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
		 * +travelresourcedetailform.getTravelid()+"&Status=D\"" + "," + "0"; }
		 */

		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				travelresourcedetailform.getTravelid(), "pm_resT");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				travelresourcedetailform.getTravelid(), "pm_resT");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);

		return mapping.findForward("traveldetailpage");
	}

	public ActionForward ViewChangeorder(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		TravelResourceDetailForm travelresourcedetailform = (TravelResourceDetailForm) form;

		TravelResource travelresource = new TravelResource();
		String travel_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";

		int sow_size = 0;
		int assumption_size = 0;
		NameId idname = null;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		/*
		 * if( request.getParameter( "Activity_Id" ) != null ) { activity_Id =
		 * request.getParameter( "Activity_Id" ); }
		 */

		if (request.getParameter("Travel_Id") != null) {
			travel_Id = request.getParameter("Travel_Id");
		}

		if (request.getAttribute("Travel_Id") != null) {
			travel_Id = (String) request.getAttribute("Travel_Id");
		}

		if (request.getParameter("addendum_id") != null) {
			travelresourcedetailform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getAttribute("addendum_id") != null) {
			travelresourcedetailform.setAddendum_id(request.getAttribute(
					"addendum_id").toString());
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}
		if (request.getParameter("Activity_Id") != null) {
			travelresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		travelresourcedetailform.setTravelid(travel_Id);
		request.setAttribute("Travel_Id",
				travelresourcedetailform.getTravelid());

		// ArrayList travelresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "CT" , travelresourcedetailform.getTravelid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList travelresourcelist = Resourcedao.getResourcetabularlist(
				travelresourcedetailform.getActivity_Id(), "CT",
				travelresourcedetailform.getTravelid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));
		travelresource = (TravelResource) travelresourcelist.get(0);

		travelresourcedetailform
				.setActivity_Id(travelresource.getActivity_Id());
		request.setAttribute("Activity_Id",
				travelresourcedetailform.getActivity_Id());

		travelresourcedetailform.setTravelid(travelresource.getTravelid());
		travelresourcedetailform.setTravelname(travelresource.getTravelname());
		travelresourcedetailform.setTraveltype(travelresource.getTraveltype());
		travelresourcedetailform.setMinimumquantity(travelresource
				.getMinimumquantity());
		travelresourcedetailform.setCnspartnumber(travelresource
				.getCnspartnumber());
		travelresourcedetailform.setEstimatedtotalcost(travelresource
				.getEstimatedtotalcost());
		travelresourcedetailform.setEstimatedunitcost(travelresource
				.getEstimatedunitcost());

		travelresourcedetailform.setPriceextended(travelresource
				.getPriceextended());
		travelresourcedetailform.setPriceunit(travelresource.getPriceunit());
		travelresourcedetailform.setProformamargin(travelresource
				.getProformamargin());
		travelresourcedetailform.setQuantity(travelresource.getQuantity());
		travelresourcedetailform.setSellablequantity(travelresource
				.getSellablequantity());
		travelresourcedetailform.setStatus(travelresource.getStatus());

		travelresourcedetailform.setChkaddendum("ViewChangeorder");
		travelresourcedetailform
				.setChkdetailactivity("detailchangeorderactivity");
		travelresourcedetailform.setActivityname(Activitydao.getActivityname(
				travelresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setMsa_id(idname.getId());
		travelresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setAppendix_id(idname.getId());
		travelresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				travelresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		travelresourcedetailform.setJob_id(idname.getId());
		travelresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resT", travelresourcedetailform
					.getStatus().charAt(0), from,
					request.getParameter("jobid"), "ViewChangeorder",
					travelresourcedetailform.getTravelid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			// list = Menu.getStatus( "pm_resT" ,
			// travelresourcedetailform.getStatus().charAt( 0 ) , "" , "" , "" ,
			// travelresourcedetailform.getTravelid() , loginuserid ,
			// getDataSource( request , "ilexnewDB" ) );

			if (travelresourcedetailform.getStatus().equals("Draft")) {
				list = "\"Approved\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ travelresourcedetailform.getAddendum_id()
						+ "&type=pm_resT&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
						+ travelresourcedetailform.getTravelid()
						+ "&Status=A\"" + "," + "0";
			}

			if (travelresourcedetailform.getStatus().equals("Approved")) {
				list = "\"Draft\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ travelresourcedetailform.getAddendum_id()
						+ "&type=pm_resT&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
						+ travelresourcedetailform.getTravelid()
						+ "&Status=D\"" + "," + "0";
			}
		}

		/*
		 * if( travelresourcedetailform.getStatus().equals( "Draft" ) ) { list =
		 * "\"Approved\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="+
		 * travelresourcedetailform.getAddendum_id()+
		 * "&type=pm_resT&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +travelresourcedetailform.getTravelid()+"&Status=A\"" + "," + "0"; }
		 * 
		 * if( travelresourcedetailform.getStatus().equals( "Approved" ) ) {
		 * list = "\"Draft\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +travelresourcedetailform.getAddendum_id()+
		 * "&type=pm_resT&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +travelresourcedetailform.getTravelid()+"&Status=D\"" + "," + "0"; }
		 */

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				travelresourcedetailform.getTravelid(), "pm_resT");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				travelresourcedetailform.getTravelid(), "pm_resT");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				travelresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				travelresourcedetailform.getChkaddendum());
		// End

		request.setAttribute("list", list);

		return mapping.findForward("traveldetailpage");
	}

}
