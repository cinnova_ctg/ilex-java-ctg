package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.JobEditBean;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.formbean.PM.JobEditForm;
import com.mind.newjobdb.actions.Emailtemplate;
import com.mind.newjobdb.business.ProjectPlanningNote;
import com.mind.newjobdb.dao.ProjectWorkflowChecklist;
import com.mind.newjobdb.dao.TicketRequestInfoDAO;
import com.mind.util.WebUtil;

public class JobEditAction extends com.mind.common.IlexAction {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        JobEditForm jobEditForm = (JobEditForm) form;
        // String Appendix_Id = "";
        // String Job_Id = "";
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        String userid = (String) session.getAttribute("userid");
        String username = (String) session.getAttribute("username");
        String loginUserEmail = (String) request.getSession(false)
                .getAttribute("useremail");

        /* Start : Appendix Dashboard View Selector Code */
        ArrayList jobOwnerList = new ArrayList();
        ArrayList jobAppStatusList = new ArrayList();
        String jobSelectedStatus = "";
        String jobSelectedOwner = "";
        String jobTempStatus = "";
        String jobTempOwner = "";
        int jobOwnerListSize = 0;
        JobEditBean jobEditBean = new JobEditBean();

        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }															// expired

        if (!Authenticationdao.getPageSecurity(userid, "Job", "Job Edit",
                getDataSource(request, "ilexnewDB"))) {
            return (mapping.findForward("UnAuthenticate"));
        }

        if (request.getParameter("appendix_Id") != null) {
            jobEditForm.setAppendixId(request.getParameter("appendix_Id")
                    .toString());
        }

        if (request.getParameter("Job_Id") != null) {
            jobEditForm.setJobId(request.getParameter("Job_Id").toString());
        }

        if (request.getParameter("viewjobtype") != null) {
            jobEditForm.setViewjobtype(request.getParameter("viewjobtype"));
        }
        if (request.getParameter("ownerId") != null) {
            jobEditForm.setOwnerId(request.getParameter("ownerId"));
        }

        if (request.getParameter("ref") != null) {
            if (request.getParameter("ref").equalsIgnoreCase("inscopeview")) {
                jobEditForm.setJobType("inscopejob");
            } else if (request.getParameter("ref").equals("detailjob")) {
                jobEditForm.setJobType("Newjob");
            } else {
                jobEditForm.setJobType(request.getParameter("ref").toString());
            }

            jobEditForm.setRef(request.getParameter("ref"));
        }

        jobEditForm.setMsaId(IMdao.getMSAId(jobEditForm.getAppendixId(),
                getDataSource(request, "ilexnewDB")));
        jobEditForm.setAppendixName(Jobdao.getAppendixname(
                getDataSource(request, "ilexnewDB"),
                jobEditForm.getAppendixId()));
        jobEditForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
                getDataSource(request, "ilexnewDB"), jobEditForm.getMsaId()));

        jobEditForm.setRequestorList(TicketRequestInfoDAO.getRequestors(
                new Long(jobEditForm.getMsaId()).longValue(),
                getDataSource(request, "ilexnewDB")));

        if (jobEditForm.getSave() != null) {
            request.setAttribute("ref", jobEditForm.getRef());

            long checkJobId[] = null;
            BeanUtils.copyProperties(jobEditBean, jobEditForm);
            checkJobId = Jobdao.editJob(jobEditBean, loginuserid,
                    getDataSource(request, "ilexnewDB"));
            jobEditBean.setJobId(String.valueOf(checkJobId[1]));
            BeanUtils.copyProperties(jobEditForm, jobEditBean);
            request.setAttribute("editJobFlag", checkJobId[0] + "");

            if (checkJobId[0] == -9003) { // Job with same name already exists
                // for this appendix.
                String contractDocoumentList = com.mind.dao.PM.Appendixdao
                        .getcontractDocoumentMenu(jobEditForm.getAppendixId(),
                                getDataSource(request, "ilexnewDB"));
                request.setAttribute("contractDocMenu", contractDocoumentList);
                String appendixcurrentstatus = Appendixdao.getCurrentstatus(
                        jobEditForm.getAppendixId(),
                        getDataSource(request, "ilexnewDB"));
                request.setAttribute("appendixcurrentstatus",
                        appendixcurrentstatus);

                return (mapping.findForward("success"));
            }

            if (!jobEditForm.getJobId().equals("0")) {
                request.setAttribute("editAction", "U");
            } else {
                request.setAttribute("editAction", "A");
                setWorkflowCheckListPlanningNote(
                        getDataSource(request, "ilexnewDB"),
                        jobEditForm.getAppendixId(), userid, checkJobId[1]);
            }

            request.setAttribute("viewjobtype", jobEditForm.getViewjobtype());
            request.setAttribute("ownerId", jobEditForm.getOwnerId());
            request.setAttribute("Appendix_Id", jobEditForm.getAppendixId());
            request.setAttribute("msaId", jobEditForm.getMsaId());

            String[] creationParam = new String[2];
            creationParam[0] = jobEditForm.getEstimatedStartSchedule();
            creationParam[1] = jobEditForm.getEstimatedCompleteSchedule();

            if (jobEditForm.getJobId() != null
                    && !jobEditForm.getJobId().equals("0")) {

                String emailbody = Emailtemplate.buildJobEmailBody(getDataSource(request, "ilexnewDB"), userid, username, jobEditForm.getMsaName(), jobEditForm.getJobName(), jobEditForm.getJobId(), Emailtemplate.JobType.PROJECTS, Emailtemplate.EmailType.CREATION, AddContactdao.getEscalationContacts(jobEditForm.getAppendixId(), getDataSource(request, "ilexnewDB")), jobEditForm);

                String emailRecipients = Jobdao.getEmailRecipientsByJob(jobEditForm.getJobId(), getDataSource(request, "ilexnewDB"));

                sendEmail(jobEditForm, loginuserid, emailbody, request, loginUserEmail, emailRecipients, getDataSource(request, "ilexnewDB"));
            }
            if (checkJobId[0] == 0
                    && jobEditForm.getRef().equalsIgnoreCase("inscopeview")
                    && !jobEditForm.getJobId().equals("0")) {
                request.setAttribute("jobid", checkJobId[1] + "");
                return (mapping.findForward("jobdashboard"));
            } else if (checkJobId[0] != 0
                    && jobEditForm.getRef().equalsIgnoreCase("inscopeview")
                    && !jobEditForm.getJobId().equals("0")) {
                return (mapping.findForward("appendixdashboard"));
            } else {
                return (mapping.findForward("joblist"));
            }
        }

        if (!jobEditForm.getJobId().equals("0")) {
            jobEditForm.setActionAddUpdate("U");
            BeanUtils.copyProperties(jobEditBean, jobEditForm);
            Jobdao.getJobDetailForEdit(jobEditBean,
                    jobEditForm.getAppendixId(), jobEditForm.getJobId(), "%",
                    "%", getDataSource(request, "ilexnewDB"));
            BeanUtils.copyProperties(jobEditForm, jobEditBean);
        } else {
            jobEditForm.setActionAddUpdate("A");
            jobEditForm.setStatus("Draft");
            jobEditForm.setLocalityUplift("1.0");
            jobEditForm.setUnion("NO");

            if (jobEditForm.getJobType().equals("inscopejob")) {
                jobEditForm.setEstimatedStartSchedule(Jobdao
                        .getCurrDate(getDataSource(request, "ilexnewDB")));
            } else {
                jobEditForm.setEstimatedStartSchedule(Jobdao
                        .getappendixscheduledate(
                                getDataSource(request, "ilexnewDB"),
                                jobEditForm.getAppendixId(), "start"));
                jobEditForm.setEstimatedCompleteSchedule(Jobdao
                        .getappendixscheduledate(
                                getDataSource(request, "ilexnewDB"),
                                jobEditForm.getAppendixId(), "complete"));
            }
            jobEditForm.setCustRef(Jobdao.getAppendixCustRef(
                    getDataSource(request, "ilexnewDB"),
                    jobEditForm.getAppendixId()));
        }

        if (!jobEditForm.getJobId().equals("0")) {
            String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
                    jobEditForm.getJobId(), "%", "%",
                    getDataSource(request, "ilexnewDB"));
            String jobStatus = jobStatusAndType[0];
            String jobType1 = jobStatusAndType[1];
            String jobStatusList = "";
            String appendixType = "";

            if (jobType1.equals("Default")) {
                jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
                        + jobEditForm.getJobId() + "&Status='D'\"" + "," + "0";
            }
            if (jobType1.equals("Newjob")) {
                jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
                        "", "", jobEditForm.getJobId(), "", loginuserid,
                        getDataSource(request, "ilexnewDB"));
            }
            if (jobType1.equals("inscopejob") && jobStatus.equals("Draft")) {
                jobStatusList = "\"Approved\""
                        + ","
                        + "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
                        + jobEditForm.getJobId() + "&Status=A\"" + "," + "0";
            }
            if (jobType1.equals("inscopejob") && jobStatus.equals("Approved")) {
                jobStatusList = "\"Draft\""
                        + ","
                        + "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
                        + jobEditForm.getJobId() + "&Status=D\"" + "," + "0";
            }
            if (jobType1.equals("Addendum")) {
                jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
                        "", "", jobEditForm.getJobId(), "", loginuserid,
                        getDataSource(request, "ilexnewDB"));
            }

            appendixType = ViewList.getAppendixtypedesc(
                    jobEditForm.getAppendixId(),
                    getDataSource(request, "ilexnewDB"));

            request.setAttribute("jobStatus", jobStatus);
            request.setAttribute("job_type", jobType1);
            request.setAttribute("appendixtype", appendixType);
            request.setAttribute("Job_Id", jobEditForm.getJobId());
            request.setAttribute("Appendix_Id", jobEditForm.getAppendixId());
            request.setAttribute("jobStatusList", jobStatusList);
            request.setAttribute("addendum_id", "0");

            if (jobType1.equals("Newjob")) {
                request.setAttribute("chkadd", "detailjob");
                request.setAttribute("chkaddendum", "detailjob");
                request.setAttribute("chkaddendumactivity", "View");
            }
            if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
                request.setAttribute("chkadd", "inscopejob");
                request.setAttribute("chkaddendum", "inscopejob");
                request.setAttribute("chkaddendumactivity", "inscopeactivity");
            }

        }

        if (jobEditForm.getJobId().equals("0")) {
            request.setAttribute("chkaddendum", jobEditForm.getJobType());
        }

        if (jobEditForm.getJobType().equals("inscopejob")) {

            /* Start : Appendix Dashboard View Selector Code */
            if (jobEditForm.getJobOwnerOtherCheck() != null
                    && !jobEditForm.getJobOwnerOtherCheck().equals("")) {
                session.setAttribute("jobOwnerOtherCheck",
                        jobEditForm.getJobOwnerOtherCheck());
            }

            if (request.getParameter("opendiv") != null) {
                request.setAttribute("opendiv", request.getParameter("opendiv"));
            }
            if (request.getParameter("resetList") != null
                    && request.getParameter("resetList").equals("something")) {
                WebUtil.setDefaultAppendixDashboardAttribute(session);
                request.setAttribute("type", "AppendixViewSelector");
                request.setAttribute("appendixId", jobEditForm.getAppendixId());
                request.setAttribute("msaId", jobEditForm.getMsaId());

                return mapping.findForward("temppage");

            }

            if (jobEditForm.getJobSelectedStatus() != null) {
                for (int i = 0; i < jobEditForm.getJobSelectedStatus().length; i++) {
                    jobSelectedStatus = jobSelectedStatus + "," + "'"
                            + jobEditForm.getJobSelectedStatus(i) + "'";
                    jobTempStatus = jobTempStatus + ","
                            + jobEditForm.getJobSelectedStatus(i);
                }

                jobSelectedStatus = jobSelectedStatus.substring(1,
                        jobSelectedStatus.length());
                jobSelectedStatus = "(" + jobSelectedStatus + ")";
            }

            if (jobEditForm.getJobSelectedOwners() != null) {
                for (int j = 0; j < jobEditForm.getJobSelectedOwners().length; j++) {
                    jobSelectedOwner = jobSelectedOwner + ","
                            + jobEditForm.getJobSelectedOwners(j);
                    jobTempOwner = jobTempOwner + ","
                            + jobEditForm.getJobSelectedOwners(j);
                }
                jobSelectedOwner = jobSelectedOwner.substring(1,
                        jobSelectedOwner.length());
                if (jobSelectedOwner.equals("0")) {
                    jobSelectedOwner = "%";
                }
                jobSelectedOwner = "(" + jobSelectedOwner + ")";
            }

            if (request.getParameter("showList") != null
                    && request.getParameter("showList").equals("something")) {

                session.setAttribute("jobSelectedOwners", jobSelectedOwner);
                session.setAttribute("jobTempOwner", jobTempOwner);
                session.setAttribute("jobSelectedStatus", jobSelectedStatus);
                session.setAttribute("jobTempStatus", jobTempStatus);
                session.setAttribute("timeFrame",
                        jobEditForm.getJobMonthWeekCheck());
                session.setAttribute("jobOwnerOtherCheck",
                        jobEditForm.getJobOwnerOtherCheck());

                request.setAttribute("type", "AppendixViewSelector");
                request.setAttribute("appendixId", jobEditForm.getAppendixId());
                request.setAttribute("msaId", jobEditForm.getMsaId());

                return mapping.findForward("temppage");

            } else {
                if (session.getAttribute("jobSelectedOwners") != null) {
                    jobSelectedOwner = session
                            .getAttribute("jobSelectedOwners").toString();
                    jobEditForm
                            .setJobSelectedOwners(session
                                    .getAttribute("jobTempOwner").toString()
                                    .split(","));
                }
                if (session.getAttribute("jobSelectedStatus") != null) {
                    jobSelectedStatus = session.getAttribute(
                            "jobSelectedStatus").toString();
                    jobEditForm.setJobSelectedStatus(session
                            .getAttribute("jobTempStatus").toString()
                            .split(","));
                }
                if (session.getAttribute("jobOwnerOtherCheck") != null) {
                    jobEditForm.setJobOwnerOtherCheck(session.getAttribute(
                            "jobOwnerOtherCheck").toString());
                }
                if (session.getAttribute("timeFrame") != null) {
                    jobEditForm.setJobMonthWeekCheck(session.getAttribute(
                            "timeFrame").toString());
                }
            }

            jobOwnerList = MSAdao.getOwnerList(userid,
                    jobEditForm.getJobOwnerOtherCheck(), "prj_job_new",
                    jobEditForm.getAppendixId(),
                    getDataSource(request, "ilexnewDB"));
            jobAppStatusList = MSAdao.getStatusList("prj_job_new",
                    getDataSource(request, "ilexnewDB"));

            if (jobOwnerList.size() > 0) {
                jobOwnerListSize = jobOwnerList.size();
            }

            request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
            request.setAttribute("jobStatusList", jobAppStatusList);
            request.setAttribute("jobOwnerList", jobOwnerList);
            /* End : Appendix Dashboard View Selector Code */

            String contractDocoumentList = com.mind.dao.PM.Appendixdao
                    .getcontractDocoumentMenu(jobEditForm.getAppendixId(),
                            getDataSource(request, "ilexnewDB"));
            request.setAttribute("contractDocMenu", contractDocoumentList);
            String appendixcurrentstatus = Appendixdao.getCurrentstatus(
                    jobEditForm.getAppendixId(),
                    getDataSource(request, "ilexnewDB"));
            request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
            request.setAttribute("appendixid", jobEditForm.getAppendixId());
            request.setAttribute("msaId", jobEditForm.getMsaId());
            request.setAttribute("viewjobtype", jobEditForm.getViewjobtype());
            request.setAttribute("ownerId", jobEditForm.getOwnerId());
        }

        request.setAttribute("api_url", EnvironmentSelector.getBundleString("api.url"));
        request.setAttribute("api_user", EnvironmentSelector.getBundleString("api.username"));
        request.setAttribute("api_pass", EnvironmentSelector.getBundleString("api.password"));
        request.setAttribute("ilex_php_url", EnvironmentSelector.getBundleString("landingzone.url"));

        return mapping.findForward("success");

    }

    /**
     * @param request
     * @param jobEditForm
     * @param userid
     * @param checkJobId
     */
    public static void setWorkflowCheckListPlanningNote(DataSource ds,
            String appendixId, String userid, long checkJobId) {
        ProjectWorkflowChecklist.copyToJob(ds, appendixId, checkJobId + "",
                userid);
        ProjectPlanningNote.copyToJob(ds, appendixId, checkJobId + "", userid);
    }

    private void sendEmail(JobEditForm bean, String userId, String mailBody, HttpServletRequest request, String loginUserEmail, String emailRecipients, DataSource ds) {
        EmailBean emailform = new EmailBean();

        emailform.setUsername(this.getResources(request).getMessage("common.Email.username"));
        emailform.setUserpassword(this.getResources(request).getMessage("common.Email.userpassword"));

        emailform.setSmtpservername(EnvironmentSelector.getBundleString("common.Email.smtpservername"));
        emailform.setSmtpserverport(this.getResources(request).getMessage("common.Email.smtpserverport"));

        emailform.setFrom(Emailtemplate.SENDER_EMAIL);

        if (emailRecipients.equals("")) {
            emailRecipients = loginUserEmail;
        } else if (!emailRecipients.contains(loginUserEmail)) {
            emailRecipients = loginUserEmail + "," + emailRecipients;
        }

        emailform.setTo(emailRecipients);

        emailform.setSubject("(" + bean.getJobId() + "-" + userId + ") - Job Creation");

        emailform.setContent(mailBody);

        Map<String, String> inlineImages = new HashMap<String, String>();
        String filePath = System.getProperty("catalina.base");
        String filepath = filePath + "//webapps//Ilex//images//ampm_header_v2.png";
        // System.out.println("--File location is " + filePath);
        inlineImages.put("image1", filepath);

        Email.send(emailform, "text/html", mailBody.toString(), inlineImages, ds);
    }
}
