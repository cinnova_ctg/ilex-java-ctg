package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.bean.pm.AppendixEditBean;
import com.mind.common.ViewAllComment;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.PM.AppendixMail;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.ManageUpliftdao;
import com.mind.formbean.PM.AppendixEditForm;

public class AppendixEditAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AppendixEditForm appendixEditForm = (AppendixEditForm) form;
		AppendixEditBean appendixEditBean = new AppendixEditBean();

		boolean refresh = false;
		int msa_size = 0;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String loginusername = (String) session.getAttribute("username");
		String checkforroleassigned = "";
		Collection[] tempPOC = null;
		Codelist codes = new Codelist();
		String sortQueryClause = "";

		// Start of View Selector code//
		// Variables Added to add the common functionality for the View
		// Selector.

		boolean chkOtherOwner = false;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		int ownerListSize = 0;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "appendix";
		String defaultvalue = request.getParameter("firstCall");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "Appendix",
				"Appendix Edit", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("msaId") != null) {
			appendixEditForm.setMsaId(request.getParameter("msaId"));
			appendixEditForm.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					appendixEditForm.getMsaId()));

		}

		if (request.getParameter("appendixId") != null) {
			appendixEditForm.setAppendixId(request.getParameter("appendixId"));
		}

		if (request.getParameter("fromType") != null) {
			appendixEditForm.setFromType(request.getParameter("fromType")
					.toString());
		}

		if (request.getAttribute("querystring") != null) {
			sortQueryClause = (String) request.getAttribute("querystring");
		}

		if (request.getParameter("home") != null) // This is called when reset
													// button is pressed from
													// the view selector.
		{
			appendixEditForm.setAppendixSelectedStatus(null);
			appendixEditForm.setAppendixSelectedOwners(null);
			appendixEditForm.setPrevSelectedStatus("");
			appendixEditForm.setPrevSelectedOwner("");
			appendixEditForm.setAppendixOtherCheck(null);
			appendixEditForm.setMonthWeekCheck(null);
			return (mapping.findForward("appendixlist"));
		}

		if (request.getParameter("opendiv") != null) // This is used to make the
														// div remain open when
														// other check box is
														// checked.
			request.setAttribute("opendiv", request.getParameter("opendiv")
					.toString());

		// These two parameter month/week are checked are kept in session. (
		// View Images)
		if (request.getParameter("month") != null) {
			session.setAttribute("monthAppendix", request.getParameter("month")
					.toString());

			session.removeAttribute("weekAppendix");
			appendixEditForm.setMonthWeekCheck("month");
		}

		if (request.getParameter("week") != null) {
			session.setAttribute("weekAppendix", request.getParameter("week")
					.toString());

			session.removeAttribute("monthAppendix");
			appendixEditForm.setMonthWeekCheck("week");
		}

		if (appendixEditForm.getAppendixOtherCheck() != null) // OtherCheck box
																// status is
																// kept in
																// session.
			session.setAttribute("appendixOtherCheck", appendixEditForm
					.getAppendixOtherCheck().toString());

		// When Status Check Boxes are Clicked :::::::::::::::::
		if (appendixEditForm.getAppendixSelectedStatus() != null) {
			for (int i = 0; i < appendixEditForm.getAppendixSelectedStatus().length; i++) {
				selectedStatus = selectedStatus + "," + "'"
						+ appendixEditForm.getAppendixSelectedStatus(i) + "'";
				tempStatus = tempStatus + ","
						+ appendixEditForm.getAppendixSelectedStatus(i);
			}

			selectedStatus = selectedStatus.substring(1,
					selectedStatus.length());
			selectedStatus = "(" + selectedStatus + ")";
		}

		// When Owner Check Boxes are Clicked :::::::::::::::::
		if (appendixEditForm.getAppendixSelectedOwners() != null) {
			for (int j = 0; j < appendixEditForm.getAppendixSelectedOwners().length; j++) {
				selectedOwner = selectedOwner + ","
						+ appendixEditForm.getAppendixSelectedOwners(j);
				tempOwner = tempOwner + ","
						+ appendixEditForm.getAppendixSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if (selectedOwner.equals("0")) {
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" + selectedOwner + ")";
		}
		if (appendixEditForm.getGo() != null
				&& !appendixEditForm.getGo().equals("")) {
			session.setAttribute("appendixSelectedOwners", selectedOwner);
			session.setAttribute("appSelectedOwners", selectedOwner);
			session.setAttribute("sessionMSAId", appendixEditForm.getMsaId());
			session.setAttribute("appendixTempOwner", tempOwner);
			session.setAttribute("appendixSelectedStatus", selectedStatus);
			session.setAttribute("appSelectedStatus", selectedStatus);
			session.setAttribute("appendixTempStatus", tempStatus);

			if (appendixEditForm.getMonthWeekCheck() != null) {
				if (appendixEditForm.getMonthWeekCheck().equals("week")) {
					session.removeAttribute("monthAppendix");
					session.setAttribute("weekAppendix", "0");

				}
				if (appendixEditForm.getMonthWeekCheck().equals("month")) {
					session.removeAttribute("weekAppendix");
					session.setAttribute("monthAppendix", "0");

				}
				if (appendixEditForm.getMonthWeekCheck().equals("clear")) {
					session.removeAttribute("weekAppendix");
					session.removeAttribute("monthAppendix");

				}

			}
			return (mapping.findForward("appendixlist"));
		} else {
			if (session.getAttribute("appendixSelectedOwners") != null) {
				selectedOwner = session.getAttribute("appendixSelectedOwners")
						.toString();
				appendixEditForm.setAppendixSelectedOwners(session
						.getAttribute("appendixTempOwner").toString()
						.split(","));
			}
			if (session.getAttribute("appendixSelectedStatus") != null) {
				selectedStatus = session.getAttribute("appendixSelectedStatus")
						.toString();
				appendixEditForm.setAppendixSelectedStatus(session
						.getAttribute("appendixTempStatus").toString()
						.split(","));
			}
			if (session.getAttribute("appendixOtherCheck") != null)
				appendixEditForm.setAppendixOtherCheck(session.getAttribute(
						"appendixOtherCheck").toString());

			if (session.getAttribute("monthAppendix") != null) {
				session.removeAttribute("weekAppendix");
				appendixEditForm.setMonthWeekCheck("month");
			}
			if (session.getAttribute("weekAppendix") != null) {
				session.removeAttribute("monthAppendix");
				appendixEditForm.setMonthWeekCheck("week");
			}

		}

		if (!chkOtherOwner) {
			appendixEditForm.setPrevSelectedStatus(selectedStatus);
			appendixEditForm.setPrevSelectedOwner(selectedOwner);
		}
		ownerList = MSAdao.getOwnerList(loginuserid,
				appendixEditForm.getAppendixOtherCheck(), ownerType,
				appendixEditForm.getMsaId(),
				getDataSource(request, "ilexnewDB"));
		statusList = MSAdao.getStatusList("pm_appendix",
				getDataSource(request, "ilexnewDB"));
		if (ownerList.size() > 0)
			ownerListSize = ownerList.size();

		request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", statusList);
		request.setAttribute("ownerlist", ownerList);
		request.setAttribute("ownerListSize", ownerListSize + "");

		if (appendixEditForm.getSave() != null) {
			appendixEditForm.setBusinessReviews("S");
			BeanUtils.copyProperties(appendixEditBean, appendixEditForm);
			int editAppendixFlag = Appendixdao.editAppendix(appendixEditBean,
					loginuserid, getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(appendixEditForm, appendixEditBean);
			if (editAppendixFlag > -1
					&& appendixEditForm.getAppendixId().equals("0")) { // - This
																		// is
																		// use
																		// when
																		// appenix
																		// is
																		// created
				String cnspocid = appendixEditForm.getCnsPOC();
				if (cnspocid == null || cnspocid.equals("")
						|| cnspocid.equals("0"))
					cnspocid = session.getAttribute("userid") + "";
				AppendixMail.appendixCreationMail(
						appendixEditForm.getAppendixName(),
						appendixEditForm.getMsaName(), cnspocid,
						getDataSource(request, "ilexnewDB")); // - to send mail
																// for creation
																// of appendix
			}// - end of if

			if (!appendixEditForm.getAppendixId().equals("0")) {
				request.setAttribute("editAction", "U");
			} else {
				request.setAttribute("editAction", "A");
			}

			request.setAttribute("editAppendixFlag", "" + editAppendixFlag);
			request.setAttribute("MSA_Id", appendixEditForm.getMsaId());

			if (appendixEditForm.getFromType().equals("AppendixDetail")) {
				request.setAttribute("MSA_Id", appendixEditForm.getMsaId());
				request.setAttribute("Appendix_Id",
						appendixEditForm.getAppendixId());
				return (mapping.findForward("appendixdetail"));
			}
			return (mapping.findForward("appendixlist"));
		}
		if (!appendixEditForm.getAppendixId().equals("0")) {
			BeanUtils.copyProperties(appendixEditBean, appendixEditForm);
			appendixEditBean = Appendixdao.getAppendix(appendixEditBean,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(appendixEditForm, appendixEditBean);
			appendixEditForm.setActionUpdateEdit("U");
		} else {

			/* Set default values */
			appendixEditForm.setAppendixType("1");
			appendixEditForm.setUnionUpliftFactor(Appendixdao
					.getMsaUnionUplift(getDataSource(request, "ilexnewDB"),
							appendixEditForm.getMsaId()));
			/* Getting default values of other uplift factors */
			BeanUtils.copyProperties(appendixEditBean, appendixEditForm);
			ManageUpliftdao.setDefaultUpliftFactor(appendixEditBean,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(appendixEditForm, appendixEditBean);

			appendixEditForm.setDraftReviews("S");
			appendixEditForm.setMsp("N");
			// Set the defauly vale for Appedix travel cost is Y
			appendixEditForm.setTravelAuthorized("Y");
			appendixEditForm.setReqPlannedSch(ViewList.getDefaultdate("R",
					getDataSource(request, "ilexnewDB")));
			appendixEditForm.setEffPlannedSch(ViewList.getDefaultdate("E",
					getDataSource(request, "ilexnewDB")));
			appendixEditForm.setSchDays("30");

			checkforroleassigned = ViewList.checkforrole(loginuserid, "ISC",
					getDataSource(request, "ilexnewDB"));

			if (checkforroleassigned.equals("Y")) {
				appendixEditForm.setCnsPOC(loginusername);
			}
			appendixEditForm.setCustPocBusiness("0");
			appendixEditForm.setCustPocContract("0");
			appendixEditForm.setCustPocBilling("0");
			appendixEditForm.setActionUpdateEdit("A");
			appendixEditForm.setCnsPOC(loginuserid);
			appendixEditForm.setTestAppendix("0");
		}

		/* Set combo values */
		codes.setAppendixtypecode(ViewList.getAppendixtype(getDataSource(
				request, "ilexnewDB")));

		codes.setSalesPOCcode(ViewList.getPOCbasedonrole("Appendix",
				getDataSource(request, "ilexnewDB")));
		codes.setCustPOCbusinesscode(ViewList.getPOC("C",
				appendixEditForm.getMsaId(),
				getDataSource(request, "ilexnewDB")));
		codes.setCustPOCcontractcode(codes.getCustPOCbusinesscode());
		codes.setCustPOCbillingcode(codes.getCustPOCbusinesscode());

		codes.setCustomerdivision(ViewList.getCustomerdivision(
				appendixEditForm.getMsaId(),
				getDataSource(request, "ilexnewDB")));

		// added appendix type breakoutList

		codes.setAppendixTypeBreakoutList(ViewList
				.getAppendixTypeBreakoutList(getDataSource(request, "ilexnewDB")));

		request.setAttribute("codeslist", codes);

		ArrayList allComment = new ArrayList();
		allComment = ViewAllComment.Getallcomment(
				appendixEditForm.getAppendixId(), "Appendix",
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("allComment", allComment);

		if (allComment != null)
			request.setAttribute("allCommentSize", "" + allComment.size());

		if (!appendixEditForm.getAppendixId().equals("0")) {

			String appendixStatus = Appendixdao.getAppendixStatus(
					appendixEditForm.getAppendixId(),
					appendixEditForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String addendumlist = Appendixdao.getAddendumsIdName(
					appendixEditForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(appendixEditForm.getAppendixId(),
							getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_appendix",
					appendixStatus.charAt(0), appendixEditForm.getMsaId(),
					appendixEditForm.getAppendixId(), "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixStatusList", list);
			request.setAttribute("Appendix_Id",
					appendixEditForm.getAppendixId());
			request.setAttribute("MSA_Id", appendixEditForm.getMsaId());
			request.setAttribute("appendixStatus", appendixStatus);
			request.setAttribute("latestaddendumid", addendumid);
			request.setAttribute("addendumlist", addendumlist);
			request.setAttribute("appendixType",
					appendixEditForm.getAppendixPrType());
		}

		return (mapping.findForward("success"));
	}
}
