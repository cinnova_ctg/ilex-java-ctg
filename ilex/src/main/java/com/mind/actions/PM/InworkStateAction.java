package com.mind.actions.PM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PM.Appendixdao;
import com.mind.formbean.PM.InworkStateForm;
 
/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 **/

/**
 * 
 *  Class Explanation: 
 *  InformStateAction is the action class for InformState.jsp.
 *  It works as a controller betweent the view and model and provides the  functionality 
 *  of data fetching from the database by calling the method in the respective dao.  
 *     
 **/

public class InworkStateAction extends com.mind.common.IlexAction {
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	{
		ActionErrors actionerror= new ActionErrors();
		ActionForward forward= new ActionForward();//return value
		InworkStateForm informStateForm= (InworkStateForm)form;
		DataSource ds= getDataSource(request,"ilexnewDB");
		int val = -1;
		
		if (informStateForm.getInwork()!=null && informStateForm.getCheck()!=null){
			String []index=informStateForm.getCheck();
			for(int i=0;i<informStateForm.getCheck().length;i++){
				if( index[i]==null ||index[i].equals(""))continue;
				val = Appendixdao.setInworkState(index[i],ds);
			}
		}
		
		informStateForm.setInworkRows(Appendixdao.getInworkState(ds));
		
		if(informStateForm.getInworkRows()!=null){
			request.setAttribute("inworkrows",informStateForm.getInworkRows());
			request.setAttribute( "inworkflag" , val+"" );
			forward= mapping.findForward("success");
		}
		else{
			request.setAttribute( "inworkflag" , val+"" );
			forward= mapping.findForward("blank");
		}
		return forward;
		
	}
}
