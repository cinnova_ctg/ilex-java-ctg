package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.formbean.PM.FormalDocDateForm;
import com.mind.util.WebUtil;

public class FormalDocDateAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		FormalDocDateForm formaldocdateform = (FormalDocDateForm) form;
		String Id = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		// Variables added for the View Selector Functioning.
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "msa";
		String msaid = "0";
		int ownerListSize = 0;

		/* Page Security Start */
		// int userid = Integer.parseInt( ( String )session.getAttribute(
		// "userid" ) );
		String userid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (formaldocdateform.getAuthenticate().equals("")) {

			if (!Authenticationdao.getPageSecurity(userid, "MSA",
					"Set Formal Document Date",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}

		if (request.getParameter("Id") != null) {
			formaldocdateform.setId(request.getParameter("Id"));
			formaldocdateform.setMsaId(request.getParameter("Id"));
		}
		if (request.getParameter("Type") != null) {
			formaldocdateform.setType(request.getParameter("Type"));
		}
		if (request.getParameter("Appendix_Id") != null) {
			formaldocdateform
					.setAppendixId(request.getParameter("Appendix_Id"));
		}
		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		if (formaldocdateform.getSetdate() != null) {
			String id = "";
			if (formaldocdateform.getType() != null
					&& formaldocdateform.getType().equals("Appendix"))
				id = formaldocdateform.getAppendixId();
			else
				id = formaldocdateform.getId();

			int status = MSAdao.setformaldocdate(id,
					formaldocdateform.getFormaldocdate(),
					getDataSource(request, "ilexnewDB"),
					formaldocdateform.getType());

			if (formaldocdateform.getType() != null
					&& formaldocdateform.getType().equals("MSA")) {
				request.setAttribute("MSA_Id", formaldocdateform.getId());
				return (mapping.findForward("msadetailpage"));
			} else {

				request.setAttribute("MSA_Id", formaldocdateform.getId());
				request.setAttribute("Appendix_Id",
						formaldocdateform.getAppendixId());
				return (mapping.findForward("appendixdetailpage"));
			}

		} else {

			if (formaldocdateform.getType().equals("MSA")) {

				formaldocdateform.setName(Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						formaldocdateform.getId()));
				formaldocdateform.setFormaldocdate(MSAdao.getformaldocdatename(
						getDataSource(request, "ilexnewDB"),
						formaldocdateform.getId()));
				ownerType = "msa";
				msaid = "0";

				if (session.getAttribute("appendixSelectedOwners") != null
						|| session.getAttribute("appendixSelectedStatus") != null)
					request.setAttribute("specialCase", "specialCase");

				if (session.getAttribute("monthAppendix") != null
						|| session.getAttribute("weekAppendix") != null) {
					request.setAttribute("specialCase", "specialCase");
				}
				WebUtil.removeAppendixSession(session);

				// when the other check box is checked in the view selector.
				if (formaldocdateform.getOtherCheck() != null)
					session.setAttribute("otherCheck", formaldocdateform
							.getOtherCheck().toString());

				if (formaldocdateform.getSelectedStatus() != null) {
					for (int i = 0; i < formaldocdateform.getSelectedStatus().length; i++) {
						selectedStatus = selectedStatus + "," + "'"
								+ formaldocdateform.getSelectedStatus(i) + "'";
						tempStatus = tempStatus + ","
								+ formaldocdateform.getSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				if (formaldocdateform.getSelectedOwners() != null) {
					for (int j = 0; j < formaldocdateform.getSelectedOwners().length; j++) {
						selectedOwner = selectedOwner + ","
								+ formaldocdateform.getSelectedOwners(j);
						tempOwner = tempOwner + ","
								+ formaldocdateform.getSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}
				// End of the code.

				// The Menu Images are to be set in session respective of one
				// other.
				if (request.getParameter("month") != null) {
					session.setAttribute("monthMSA",
							request.getParameter("month").toString());
					monthMSA = request.getParameter("month").toString();
					session.removeAttribute("weekMSA");
					formaldocdateform.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekMSA", request
							.getParameter("week").toString());
					weekMSA = request.getParameter("week").toString();
					session.removeAttribute("monthMSA");
					formaldocdateform.setMonthWeekCheck("week");
				}

				// When the reset button is pressed remove every thing from the
				// session.
				if (request.getParameter("home") != null) {
					formaldocdateform.setSelectedStatus(null);
					formaldocdateform.setSelectedOwners(null);
					formaldocdateform.setPrevSelectedStatus("");
					formaldocdateform.setPrevSelectedOwner("");
					formaldocdateform.setOtherCheck(null);
					formaldocdateform.setMonthWeekCheck(null);
					// MSAdao.setValuesWhenReset(session);
					return (mapping.findForward("msalistpage"));
				}
				// To set the values in the session when the Show button is
				// Pressed.
				if (formaldocdateform.getGo() != null
						&& !formaldocdateform.getGo().equals("")) {
					session.setAttribute("selectedOwners", selectedOwner);
					session.setAttribute("MSASelectedOwners", selectedOwner);
					session.setAttribute("tempOwner", tempOwner);
					session.setAttribute("selectedStatus", selectedStatus);
					session.setAttribute("MSASelectedStatus", selectedStatus);
					session.setAttribute("tempStatus", tempStatus);

					if (formaldocdateform.getMonthWeekCheck() != null) {
						if (formaldocdateform.getMonthWeekCheck()
								.equals("week")) {
							session.removeAttribute("monthMSA");
							session.setAttribute("weekMSA", "0");
						}
						if (formaldocdateform.getMonthWeekCheck().equals(
								"month")) {
							session.removeAttribute("weekMSA");
							session.setAttribute("monthMSA", "0");
						}
						if (formaldocdateform.getMonthWeekCheck().equals(
								"clear")) {
							session.removeAttribute("monthMSA");
							session.removeAttribute("weekMSA");
						}

					}

					if (session.getAttribute("monthMSA") != null) {
						session.removeAttribute("weekMSA");
						monthMSA = session.getAttribute("monthMSA").toString();
					}
					if (session.getAttribute("weekMSA") != null) {
						session.removeAttribute("monthMSA");
						weekMSA = session.getAttribute("weekMSA").toString();
					}
					return (mapping.findForward("msalistpage"));
				} else // If not pressed see the values exists in the session or
						// not.
				{

					if (session.getAttribute("selectedOwners") != null) {
						selectedOwner = session.getAttribute("selectedOwners")
								.toString();
						formaldocdateform.setSelectedOwners(session
								.getAttribute("tempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("selectedStatus") != null) {
						selectedStatus = session.getAttribute("selectedStatus")
								.toString();
						formaldocdateform.setSelectedStatus(session
								.getAttribute("tempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("otherCheck") != null)
						formaldocdateform.setOtherCheck(session.getAttribute(
								"otherCheck").toString());
				}

				if (session.getAttribute("monthMSA") != null) {
					session.removeAttribute("weekMSA");
					monthMSA = session.getAttribute("monthMSA").toString();
				}
				if (session.getAttribute("weekMSA") != null) {
					session.removeAttribute("monthMSA");
					weekMSA = session.getAttribute("weekMSA").toString();
				}

				if (!chkOtherOwner) {
					formaldocdateform.setPrevSelectedStatus(selectedStatus);
					formaldocdateform.setPrevSelectedOwner(selectedOwner);
				}

				ownerList = MSAdao.getOwnerList(userid,
						formaldocdateform.getOtherCheck(), ownerType, msaid,
						getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_msa",
						getDataSource(request, "ilexnewDB"));

				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

				// End of the code for the View Selector Part
			}

			if (formaldocdateform.getType().equals("Appendix")) {
				formaldocdateform.setName(Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"),
						formaldocdateform.getAppendixId()));
				formaldocdateform.setFormaldocdate(MSAdao
						.getformaldocdateforappendix(
								getDataSource(request, "ilexnewDB"),
								formaldocdateform.getAppendixId()));
				formaldocdateform.setMsaId(IMdao.getMSAId(
						formaldocdateform.getAppendixId(),
						getDataSource(request, "ilexnewDB")));
				formaldocdateform.setMsaName(com.mind.dao.PM.Appendixdao
						.getMsaname(getDataSource(request, "ilexnewDB"),
								formaldocdateform.getMsaId()));
				ownerType = "appendix";

				if (request.getParameter("home") != null) // This is called when
															// reset button is
															// pressed from the
															// view selector.
				{
					formaldocdateform.setAppendixSelectedStatus(null);
					formaldocdateform.setAppendixSelectedOwners(null);
					formaldocdateform.setPrevSelectedStatus("");
					formaldocdateform.setPrevSelectedOwner("");
					formaldocdateform.setAppendixOtherCheck(null);
					formaldocdateform.setMonthWeekCheck(null);
					return (mapping.findForward("appendixlistpage"));
				}

				// These two parameter month/week are checked are kept in
				// session. ( View Images)
				if (request.getParameter("month") != null) {
					session.setAttribute("monthAppendix",
							request.getParameter("month").toString());

					session.removeAttribute("weekAppendix");
					formaldocdateform.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekAppendix",
							request.getParameter("week").toString());

					session.removeAttribute("monthAppendix");
					formaldocdateform.setMonthWeekCheck("week");
				}

				if (formaldocdateform.getAppendixOtherCheck() != null) // OtherCheck
																		// box
																		// status
																		// is
																		// kept
																		// in
																		// session.
					session.setAttribute("appendixOtherCheck",
							formaldocdateform.getAppendixOtherCheck()
									.toString());

				// When Status Check Boxes are Clicked :::::::::::::::::
				if (formaldocdateform.getAppendixSelectedStatus() != null) {
					for (int i = 0; i < formaldocdateform
							.getAppendixSelectedStatus().length; i++) {
						selectedStatus = selectedStatus
								+ ","
								+ "'"
								+ formaldocdateform
										.getAppendixSelectedStatus(i) + "'";
						tempStatus = tempStatus
								+ ","
								+ formaldocdateform
										.getAppendixSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				// When Owner Check Boxes are Clicked :::::::::::::::::
				if (formaldocdateform.getAppendixSelectedOwners() != null) {
					for (int j = 0; j < formaldocdateform
							.getAppendixSelectedOwners().length; j++) {
						selectedOwner = selectedOwner
								+ ","
								+ formaldocdateform
										.getAppendixSelectedOwners(j);
						tempOwner = tempOwner
								+ ","
								+ formaldocdateform
										.getAppendixSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}

				if (formaldocdateform.getGo() != null
						&& !formaldocdateform.getGo().equals("")) {
					session.setAttribute("appendixSelectedOwners",
							selectedOwner);
					session.setAttribute("appSelectedOwners", selectedOwner);
					session.setAttribute("appendixTempOwner", tempOwner);
					session.setAttribute("appendixSelectedStatus",
							selectedStatus);
					session.setAttribute("appSelectedStatus", selectedStatus);
					session.setAttribute("appendixTempStatus", tempStatus);

					if (formaldocdateform.getMonthWeekCheck() != null) {
						if (formaldocdateform.getMonthWeekCheck()
								.equals("week")) {
							session.removeAttribute("monthAppendix");
							session.setAttribute("weekAppendix", "0");

						}
						if (formaldocdateform.getMonthWeekCheck().equals(
								"month")) {
							session.removeAttribute("weekAppendix");
							session.setAttribute("monthAppendix", "0");

						}
						if (formaldocdateform.getMonthWeekCheck().equals(
								"clear")) {
							session.removeAttribute("monthAppendix");
							session.removeAttribute("weekAppendix");
						}

					}
					return (mapping.findForward("appendixlistpage"));
				} else {
					if (session.getAttribute("appendixSelectedOwners") != null) {
						selectedOwner = session.getAttribute(
								"appendixSelectedOwners").toString();
						formaldocdateform.setAppendixSelectedOwners(session
								.getAttribute("appendixTempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("appendixSelectedStatus") != null) {
						selectedStatus = session.getAttribute(
								"appendixSelectedStatus").toString();
						formaldocdateform.setAppendixSelectedStatus(session
								.getAttribute("appendixTempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("appendixOtherCheck") != null)
						formaldocdateform.setAppendixOtherCheck(session
								.getAttribute("appendixOtherCheck").toString());

					if (session.getAttribute("monthAppendix") != null) {
						session.removeAttribute("weekAppendix");
						formaldocdateform.setMonthWeekCheck("month");
					}
					if (session.getAttribute("weekAppendix") != null) {
						session.removeAttribute("monthAppendix");
						formaldocdateform.setMonthWeekCheck("week");
					}

				}
				if (!chkOtherOwner) {
					formaldocdateform.setPrevSelectedStatus(selectedStatus);
					formaldocdateform.setPrevSelectedOwner(selectedOwner);
				}
				ownerList = MSAdao.getOwnerList(userid,
						formaldocdateform.getAppendixOtherCheck(), ownerType,
						formaldocdateform.getMsaId(),
						getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_appendix",
						getDataSource(request, "ilexnewDB"));

				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

			} // End of Appendix
		}// End of the else part

		if (formaldocdateform.getType().equals("MSA")) {
			request.setAttribute("MSA_Id", formaldocdateform.getId());
			String msaStatus = MSAdao.getMSAStatus(formaldocdateform.getId(),
					getDataSource(request, "ilexnewDB"));
			String msaUploadCheck = MSAdao.getMSAUploadCheck(
					formaldocdateform.getId(),
					getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
					formaldocdateform.getId(), "", "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);
			request.setAttribute("status", msaStatus);
			request.setAttribute("uploadstatus", msaUploadCheck);
		}

		if (formaldocdateform.getType().equals("Appendix")) {
			String appendixStatus = Appendixdao.getAppendixStatus(
					formaldocdateform.getAppendixId(),
					formaldocdateform.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String addendumlist = Appendixdao.getAddendumsIdName(
					formaldocdateform.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(formaldocdateform.getAppendixId(),
							getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_appendix",
					appendixStatus.charAt(0), formaldocdateform.getMsaId(),
					formaldocdateform.getAppendixId(), "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixStatusList", list);
			request.setAttribute("Appendix_Id",
					formaldocdateform.getAppendixId());
			request.setAttribute("MSA_Id", formaldocdateform.getMsaId());
			request.setAttribute("appendixStatus", appendixStatus);
			request.setAttribute("latestaddendumid", addendumid);
			request.setAttribute("addendumlist", addendumlist);

		}

		return (mapping.findForward("formaldocdatepage"));
	}
}
