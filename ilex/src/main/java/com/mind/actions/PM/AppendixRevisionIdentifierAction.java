package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.formbean.PM.AppendixRevisionIdentifierForm;
public class AppendixRevisionIdentifierAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward View( ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response ) throws Exception 
	{
		AppendixRevisionIdentifierForm revisionform = ( AppendixRevisionIdentifierForm ) form;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String Appendix_Id = "";
//		Variables Added to add the common functionality for the View Selector.
		
		boolean chkOtherOwner = false;
		ArrayList statusList  = new ArrayList();
		ArrayList ownerList  = new ArrayList();
		String selectedStatus ="";
		String selectedOwner = "";
		int ownerListSize = 0; 
		String tempStatus = "";
		String tempOwner = "";
		String ownerType= "appendix";
		
		
		if( request.getParameter( "Appendix_Id" ) != null )
			revisionform.setId(request.getParameter( "Appendix_Id" ));
		
		if(request.getParameter("opendiv")!=null)    //This is used to make the div remain open when other check box is checked.
			request.setAttribute("opendiv",request.getParameter("opendiv").toString()); 
		
		revisionform.setName(Jobdao.getAppendixname( getDataSource( request,"ilexnewDB"  ), revisionform.getId() ));
		revisionform.setMsaId(IMdao.getMSAId(revisionform.getId(), getDataSource( request , "ilexnewDB" )));
		revisionform.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(getDataSource( request , "ilexnewDB" ),revisionform.getMsaId()));
		
		
		if(revisionform.getSave()!=null && revisionform.getSave()!=""){
			int revisionidentifierflag = Appendixdao.setrevisionsummary( revisionform.getId() , revisionform.getRevisionidentifier() , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute("MSA_Id",revisionform.getMsaId());
			request.setAttribute( "Appendix_Id" , revisionform.getId() );
		
			return mapping.findForward( "appendixdetailpage" );
		}
		
		if(request.getParameter("home")!=null)   // This is called when reset button is pressed from the view selector.                       
		{
			revisionform.setAppendixSelectedStatus(null);
			revisionform.setAppendixSelectedOwners(null);
			revisionform.setPrevSelectedStatus("");
			revisionform.setPrevSelectedOwner("");
			revisionform.setAppendixOtherCheck(null);
			revisionform.setMonthWeekCheck(null);
			session.removeAttribute("appendixSelectedOwners");
			session.removeAttribute("appendixSelectedStatus");
			session.removeAttribute("appendixTempOwner");
			session.removeAttribute("appendixTempStatus");
			session.removeAttribute("appendixOtherCheck");	
			session.removeAttribute("monthAppendix");
			session.removeAttribute("weekAppendix");
			session.removeAttribute("appSelectedOwners");
			session.removeAttribute("appSelectedStatus");
			session.removeAttribute("sessionMSAId");
			
		}	
			
		//These two parameter month/week are checked are kept in session.  ( View Images)
		if(request.getParameter("month")!=null){        
			session.setAttribute("monthAppendix",request.getParameter("month").toString());
			
			session.removeAttribute("weekAppendix");
			revisionform.setMonthWeekCheck("month");
		}
		
		if(request.getParameter("week")!=null){
			session.setAttribute("weekAppendix",request.getParameter("week").toString());
			
			session.removeAttribute("monthAppendix");
			revisionform.setMonthWeekCheck("week");
		}
		
		if(revisionform.getAppendixOtherCheck()!=null)  //OtherCheck box status is kept in session.   
			session.setAttribute("appendixOtherCheck",revisionform.getAppendixOtherCheck().toString());
		
//		When Status Check Boxes are Clicked :::::::::::::::::
		if(revisionform.getAppendixSelectedStatus()!=null){
			for(int i=0;i<revisionform.getAppendixSelectedStatus().length;i++)
			{
				selectedStatus = selectedStatus + "," + "'" + revisionform.getAppendixSelectedStatus(i) + "'" ;
				tempStatus = tempStatus + "," + revisionform.getAppendixSelectedStatus(i);
			}
			
		selectedStatus = selectedStatus.substring(1, selectedStatus.length());
		selectedStatus = "(" +selectedStatus + ")";		
		}
		
		//When Owner Check Boxes are Clicked :::::::::::::::::
		if(revisionform.getAppendixSelectedOwners() != null){
			for(int j = 0; j<revisionform.getAppendixSelectedOwners().length; j++)
			{
				selectedOwner = selectedOwner + "," + revisionform.getAppendixSelectedOwners(j); 
				tempOwner = tempOwner + "," + revisionform.getAppendixSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if(selectedOwner.equals("0")){
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" +selectedOwner + ")";
		}
		
		
		if(revisionform.getGo()!= null && revisionform.getGo()!= "")
		{
				session.setAttribute("appendixSelectedOwners",selectedOwner);
				session.setAttribute("appSelectedOwners",selectedOwner);
				session.setAttribute("appendixTempOwner",tempOwner);	
				session.setAttribute("appendixSelectedStatus",selectedStatus);
				session.setAttribute("appSelectedStatus",selectedStatus);
				session.setAttribute("appendixTempStatus",tempStatus);
				
				if(revisionform.getMonthWeekCheck()!=null)
				{
					if(revisionform.getMonthWeekCheck().equals("week")){
						session.removeAttribute("monthAppendix");
						session.setAttribute("weekAppendix","0");
						
					}
					if(revisionform.getMonthWeekCheck().equals("month")){
						session.removeAttribute("weekAppendix");
						session.setAttribute("monthAppendix","0");
						
					}
						
				}
		}	
		else
			{
					if(session.getAttribute("appendixSelectedOwners")!=null) {
						selectedOwner = session.getAttribute("appendixSelectedOwners").toString();
						revisionform.setAppendixSelectedOwners(session.getAttribute("appendixTempOwner").toString().split(","));
					}
					if(session.getAttribute("appendixSelectedStatus")!=null) {
						selectedStatus = session.getAttribute("appendixSelectedStatus").toString();
						revisionform.setAppendixSelectedStatus(session.getAttribute("appendixTempStatus").toString().split(","));
					}
					if(session.getAttribute("appendixOtherCheck")!=null)
						revisionform.setAppendixOtherCheck(session.getAttribute("appendixOtherCheck").toString());
					
					if(session.getAttribute("monthAppendix")!=null){
						session.removeAttribute("weekAppendix");
						revisionform.setMonthWeekCheck("month");
					}
					if(session.getAttribute("weekAppendix")!=null){
						session.removeAttribute("monthAppendix");
						revisionform.setMonthWeekCheck("week");
					}
					
			}
				if(!chkOtherOwner) {
					revisionform.setPrevSelectedStatus(selectedStatus);
					revisionform.setPrevSelectedOwner(selectedOwner);
				}     
				ownerList  = MSAdao.getOwnerList(loginuserid, revisionform.getAppendixOtherCheck(), ownerType,revisionform.getMsaId(), getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_appendix",getDataSource(request, "ilexnewDB"));
				
				if(ownerList.size() > 0)
					ownerListSize = ownerList.size();
				
				request.setAttribute("clickShow", request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);	
				request.setAttribute("ownerListSize", ownerListSize+"");
				
		String appendixStatus = Appendixdao.getAppendixStatus( revisionform.getId() ,revisionform.getMsaId(), getDataSource( request , "ilexnewDB" ) );
		String addendumlist = Appendixdao.getAddendumsIdName( revisionform.getId() , getDataSource( request , "ilexnewDB" )  );
		String addendumid = com.mind.dao.PM.Addendumdao.getLatestAddendumId(revisionform.getId() , getDataSource( request , "ilexnewDB" ) );
		String list = Menu.getStatus( "pm_appendix" , appendixStatus.charAt( 0 ) , revisionform.getMsaId() , revisionform.getId(), "" , "" , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "appendixStatusList" , list );
		request.setAttribute("Appendix_Id",revisionform.getId());
		request.setAttribute("MSA_Id",revisionform.getMsaId());
		request.setAttribute("appendixStatus",appendixStatus);
		request.setAttribute( "latestaddendumid" , addendumid );
		request.setAttribute( "addendumlist" , addendumlist );
			
		return mapping.findForward( "revisionsummarypage" );
	}
}