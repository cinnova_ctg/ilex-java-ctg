package com.mind.actions.PM;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.bean.pm.ActivityEditBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.formbean.PM.ActivityEditForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ActivityEditAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActivityEditForm activityEditForm = (ActivityEditForm) form;
		ActivityEditBean activityEditBean = new ActivityEditBean();

		Codelist codes = new Codelist();

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "Activity",
				"Activity Edit", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("Job_Id") != null) {
			activityEditForm.setJobId(request.getParameter("Job_Id"));
		}

		if (request.getAttribute("Job_Id") != null) {
			activityEditForm.setJobId(request.getAttribute("Job_Id") + "");
		}

		if (request.getParameter("Activity_Id") != null) {
			activityEditForm
					.setActivity_Id(request.getParameter("Activity_Id"));
		}

		if (request.getParameter("from") != null) {
			activityEditForm.setFrom(request.getParameter("from"));
		}

		if (request.getAttribute("Activity_Id") != null) {
			activityEditForm.setActivity_Id(request.getAttribute("Activity_Id")
					+ "");
		}

		if (request.getParameter("ref") != null) {
			activityEditForm.setRef(request.getParameter("ref"));

			if (request.getParameter("ref").equalsIgnoreCase("view")) {
				activityEditForm.setChkaddendum("detailactivity");
			} else if (request.getParameter("ref").equals("inscopeactivity")) {
				activityEditForm.setChkaddendum("inscopedetailactivity");
			} else
				activityEditForm.setChkaddendum(request.getParameter("ref"));
		}

		request.setAttribute("Job_Id", activityEditForm.getJobId());
		activityEditForm.setJobName(Jobdao.getJobname(
				activityEditForm.getJobId(),
				getDataSource(request, "ilexnewDB")));
		activityEditForm.setAppendixName(Jobdao.getoplevelname(
				activityEditForm.getJobId(), "Activity",
				getDataSource(request, "ilexnewDB")));
		activityEditForm.setAppendixid(Activitydao.getAppendixid(
				getDataSource(request, "ilexnewDB"),
				activityEditForm.getJobId()));

		NameId idname = Activitydao.getIdName(activityEditForm.getJobId(),
				"Activity", "MSA", getDataSource(request, "ilexnewDB"));
		activityEditForm.setMsaName(idname.getName());
		activityEditForm.setMsaId(idname.getId());

		codes.setActivitytypecode(ViewList.getActivitytype(getDataSource(
				request, "ilexnewDB")));
		request.setAttribute("codeslist", codes);
		Map<String, Object> map;

		if (activityEditForm.getSave() != null) {
			request.setAttribute("ref", activityEditForm.getRef());
			request.setAttribute("Job_Id", activityEditForm.getJobId());

			if (activityEditForm.getActivity_Id().equals("0")) {
				BeanUtils.copyProperties(activityEditBean, activityEditForm);
				String addSFlag = Activitydao.addActivity(activityEditBean,
						loginuserid, getDataSource(request, "ilexnewDB"), "");

				int addflag = Integer.parseInt(addSFlag.substring(0,
						addSFlag.indexOf("|")));
				request.setAttribute("addflag", "" + addflag);
			} else {
				BeanUtils.copyProperties(activityEditBean, activityEditForm);
				int updateflag = Activitydao.updateActivity(activityEditBean,
						"A", loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("updateflag", "" + updateflag);

			}

			return (mapping.findForward("activitylist"));

		}

		if (!activityEditForm.getActivity_Id().equals("0")) {
			BeanUtils.copyProperties(activityEditBean, activityEditForm);
			Activitydao.getActivityDetail(activityEditBean,
					getDataSource(request, "ilexnewDB"),
					activityEditBean.getActivity_Id());
			BeanUtils.copyProperties(activityEditForm, activityEditBean);
		}

		else {
			activityEditForm.setQuantity("1.0");
			activityEditForm.setEstimatedMaterialcost("0.00");
			activityEditForm.setEstimatedCnsfieldhqlaborcost("0.00");
			activityEditForm.setEstimatedContractfieldlaborcost("0.00");
			activityEditForm.setEstimatedFreightcost("0.00");
			activityEditForm.setEstimatedTravelcost("0.00");
			activityEditForm.setEstimatedTotalcost("0.00");
			activityEditForm.setExtendedprice("0.00");
			activityEditForm.setOverheadcost("0.00");
			activityEditForm.setListprice("0.00");
			activityEditForm.setProformamargin("0.00");
			activityEditForm.setEstimatedStartSchedule(Activitydao
					.getjobscheduledate(getDataSource(request, "ilexnewDB"),
							activityEditForm.getJobId(), "start"));
			activityEditForm.setEstimatedCompleteSchedule(Activitydao
					.getjobscheduledate(getDataSource(request, "ilexnewDB"),
							activityEditForm.getJobId(), "end"));
			activityEditForm.setNewStatus("Draft");
			activityEditForm.setActivity_Id("0");
			activityEditForm.setAddendum_id("0");
			activityEditForm.setActivity_lib_Id("-1");

			if (activityEditForm.getRef() != null) {
				if (activityEditForm.getRef().equalsIgnoreCase("view")
						|| activityEditForm.getRef().equalsIgnoreCase(
								"detailactivity")) {
					activityEditForm.setActivity_cost_type("A");
				}

				if (activityEditForm.getRef().equalsIgnoreCase(
						"inscopeactivity")
						|| request.getParameter("ref").equalsIgnoreCase(
								"inscopedetailactivity")) {
					activityEditForm.setActivity_cost_type("IA");
					if (activityEditForm.getJobId() != null
							&& !activityEditForm.getJobId().trim()
									.equalsIgnoreCase("")) {
						map = DatabaseUtilityDao.setMenu(
								activityEditForm.getJobId(), loginuserid,
								getDataSource(request, "ilexnewDB"));
						WebUtil.copyMapToRequest(request, map);
					}
				}
			}
		}
		// Code Added to have the Activity Detail Page Menu
		if (!activityEditForm.getActivity_Id().equals("0")) {
			String jobtype = "";
			String activityStatus = "";
			String activityType = "";
			String from = "";
			String jobid = "";
			String activityStatusList = "";
			String MSA_Id = "";
			String MSA_Name = "";
			int checkNetmedXappendix = 0;
			String[] activityStatusAndType = Activitydao.getActivityTypeStatus(
					activityEditForm.getActivity_Id(),
					getDataSource(request, "ilexnewDB"));
			activityStatus = activityStatusAndType[0];
			activityType = activityStatusAndType[1];

			if (request.getParameter("from") != null) {
				from = request.getParameter("from");
				jobid = request.getParameter("jobid");
				request.setAttribute("from", request.getParameter("from"));
				request.setAttribute("jobid", request.getParameter("jobid"));
			}

			jobtype = Jobdao.getJobtype(activityEditForm.getJobId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("jobtype", jobtype);

			if (activityEditForm.getRef().equalsIgnoreCase("view")
					|| activityEditForm.getRef().equalsIgnoreCase(
							"detailactivity")) {
				request.setAttribute("chkaddendum", "detailactivity");
				request.setAttribute("chkaddendumresource", "View");

				if (request.getParameter("from") != null) {
					if (jobtype.equals("Addendum") || jobtype.equals("Default")) {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), from, jobid,
								activityEditForm.getActivity_Id(),
								"detailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					} else {
						activityStatus = JobDashboarddao
								.getActivityprojectstatus(
										activityEditForm.getActivity_Id(),
										getDataSource(request, "ilexnewDB"));
						activityStatusList = Menu.getStatus("prj_act",
								activityStatus.charAt(0), from, jobid,
								activityEditForm.getActivity_Id(),
								"detailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					}
				} else {
					activityStatusList = Menu.getStatus("pm_act",
							activityStatus.charAt(0), "", "",
							activityEditForm.getActivity_Id(),
							"detailactivity", loginuserid,
							getDataSource(request, "ilexnewDB"));

				}

			}

			if (activityEditForm.getRef().equalsIgnoreCase("inscopeactivity")
					|| activityEditForm.getRef().equalsIgnoreCase(
							"inscopedetailactivity")) {
				request.setAttribute("chkaddendum", "inscopedetailactivity");
				request.setAttribute("chkaddendumresource", "Viewinscope");

				if (request.getParameter("from") != null) {

					if (jobtype.equals("Addendum") || jobtype.equals("Default")) {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), from, jobid,
								activityEditForm.getActivity_Id(),
								"inscopedetailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					} else {
						activityStatus = JobDashboarddao
								.getActivityprojectstatus(
										activityEditForm.getActivity_Id(),
										getDataSource(request, "ilexnewDB"));
						activityStatusList = Menu.getStatus("prj_act",
								activityStatus.charAt(0), from, jobid,
								activityEditForm.getActivity_Id(),
								"inscopedetailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					}
				} else {
					activityStatusList = Menu.getStatus("pm_act",
							activityStatus.charAt(0), "", "",
							activityEditForm.getActivity_Id(),
							"inscopedetailactivity", loginuserid,
							getDataSource(request, "ilexnewDB"));

				}

			}

			MSA_Id = Activitydao.getmsaid(getDataSource(request, "ilexnewDB"),
					activityEditForm.getJobId());
			MSA_Name = Activitydao.getMsaname(
					getDataSource(request, "ilexnewDB"), MSA_Id);
			request.setAttribute("MSA_Name", MSA_Name);
			request.setAttribute("MSA_Id", MSA_Id);

			checkNetmedXappendix = Activitydao.checkAppendixname(
					activityEditForm.getAppendixid(),
					activityEditForm.getJobId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("checknetmedx", checkNetmedXappendix + "");
			request.setAttribute("activityStatusList", activityStatusList);
			request.setAttribute("Activity_Id",
					activityEditForm.getActivity_Id());
			request.setAttribute("Job_Id", activityEditForm.getJobId());
			request.setAttribute("jobtype", jobtype);
			request.setAttribute("from", from);
			request.setAttribute("jobid", jobid);
			request.setAttribute("activityStatus", activityStatus);
			request.setAttribute("activityType", activityType);
			request.setAttribute("activityTypeForJob",
					activityEditForm.getChkaddendum());

			if (activityEditForm.getActivity_Id().equals("0")) {
				request.setAttribute("activityTypeForJob",
						activityEditForm.getChkaddendum());

			}

			/*
			 * System.out.println("Ref is :::" + request.getParameter("ref"));
			 * System.out.println("Activity ID ::::" +
			 * activityEditForm.getActivity_Id());
			 * System.out.println("Job ID ::::" + activityEditForm.getJobId() );
			 * System.out.println("Appendix ID ::::" +
			 * activityEditForm.getAppendixid() );
			 * System.out.println("MSA ID ::::" + activityEditForm.getMsaId() );
			 * System.out.println("Job Type ::::" + jobtype );
			 * System.out.println("From  ::::" + from );
			 * System.out.println("job id ::::" + jobid );
			 * System.out.println("Activity Status ::::" + activityStatus );
			 * System.out.println("Activity Type ::::" + activityType);
			 * System.out.println("Activity Status List ::::" +
			 * activityStatusList );
			 * System.out.println("checkNetmedXappendix ::::" +
			 * checkNetmedXappendix );
			 */
			// End of the Code
		}
		if (activityEditForm.getActivity_Id().equals("0")) {
			request.setAttribute("activityTypeForJob",
					activityEditForm.getChkaddendum());
		}

		return (mapping.findForward("success"));
	}

}
