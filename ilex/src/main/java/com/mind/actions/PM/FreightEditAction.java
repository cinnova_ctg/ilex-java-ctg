package com.mind.actions.PM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.FreightEditBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.PM.FreightEditForm;

public class FreightEditAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		FreightEditForm freighteditform = (FreightEditForm) form;
		FreightEditBean freighteditBean = new FreightEditBean();

		NameId idname = null;
		String from = "";
		String jobid = "";

		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "Resource",
				"Resource Edit", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("Activity_Id") != null) {
			freighteditform.setActivity_Id(request.getParameter("Activity_Id"));
		}

		if (request.getParameter("Freight_Id") != null) {
			freighteditform.setFreightid(request.getParameter("Freight_Id"));
		}

		// if( request.getParameter( "flag" ) != null )
		// {
		// freighteditform .setFlag( request.getParameter( "flag" ));
		// }

		if (request.getParameter("from") != null
				&& request.getParameter("from") != "") {
			jobid = request.getParameter("jobid");
			from = request.getParameter("from");
			freighteditform.setFromflag(request.getParameter("from"));
			freighteditform.setDashboardid(request.getParameter("jobid"));

			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("ref") != null) {
			freighteditform.setChkaddendum(request.getParameter("ref"));
			freighteditform.setRef(request.getParameter("ref"));
		}

		idname = Activitydao.getIdName(freighteditform.getActivity_Id(),
				"Resource", "Appendix", getDataSource(request, "ilexnewDB"));
		freighteditform.setAppendixId(idname.getId());
		freighteditform.setAppendixName(idname.getName());
		freighteditform.setJobName(Jobdao.getoplevelname(
				freighteditform.getActivity_Id(), "Resource",
				getDataSource(request, "ilexnewDB")));
		freighteditform.setJobId(Activitydao.getJobid(
				getDataSource(request, "ilexnewDB"),
				freighteditform.getActivity_Id()));

		idname = Activitydao.getIdName(freighteditform.getJobId(), "Activity",
				"MSA", getDataSource(request, "ilexnewDB"));
		freighteditform.setMsaName(idname.getName());
		freighteditform.setMsaId(idname.getId());
		freighteditform.setActivityName(Activitydao.getActivityname(
				freighteditform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));

		if (freighteditform.getChkaddendum().equals("View")) {
			freighteditform.setFreighttype("F");
		}
		if (freighteditform.getChkaddendum().equals("Viewinscope")) {
			freighteditform.setFreighttype("IF");
		}

		if (freighteditform.getSave() != null) {
			BeanUtils.copyProperties(freighteditBean, freighteditform);

			int updateflagFreight = Resourcedao.updatefreightresourceNew(
					freighteditBean, userid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("updateflagFreight", "" + updateflagFreight);
			request.setAttribute("from", freighteditform.getFromflag());
			request.setAttribute("jobid", freighteditform.getDashboardid());
			return (mapping.findForward("resourcelistpage"));
		} else {
			BeanUtils.copyProperties(freighteditBean, freighteditform);
			Resourcedao.getFreightResourceEdit(freighteditBean, "F", "",
					request.getSession().getId(), "listpage",
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(freighteditform, freighteditBean);
		}

		// For the Freight Menu
		String freightType = "";
		String freightStatus = "";
		String freightStatusList = "";

		if (freighteditform.getRef().equalsIgnoreCase("View"))
			freightType = "F";
		else
			freightType = "IF";

		freightStatus = Resourcedao.getResourceStatus(freighteditform
				.getActivity_Id(), freighteditform.getFreightid(), freightType,
				request.getSession().getId(),
				getDataSource(request, "ilexnewDB"));

		if (freighteditform.getRef().equalsIgnoreCase("View")) {
			request.setAttribute("chkaddendum", "View");
			request.setAttribute("chkdetailactivity", "detailactivity");

			if (request.getParameter("from") != null)
				freightStatusList = Menu.getStatus("jobdashboard_resF",
						freightStatus.charAt(0), from,
						request.getParameter("jobid"), "View",
						freighteditform.getFreightid(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				freightStatusList = Menu.getStatus("pm_resF",
						freightStatus.charAt(0), "", "", "",
						freighteditform.getFreightid(), userid,
						getDataSource(request, "ilexnewDB"));

		}

		if (freighteditform.getRef().equalsIgnoreCase("Viewinscope")) {
			request.setAttribute("chkaddendum", "Viewinscope");
			request.setAttribute("chkdetailactivity", "inscopedetailactivity");

			if (request.getParameter("from") != null)
				freightStatusList = Menu.getStatus("jobdashboard_resF",
						freightStatus.charAt(0), from,
						request.getParameter("jobid"), "Viewinscope",
						freighteditform.getFreightid(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				freightStatusList = Menu.getStatus("jobdashboard_resF",
						freightStatus.charAt(0), "", "", "Viewinscope",
						freighteditform.getFreightid(), userid,
						getDataSource(request, "ilexnewDB"));
		}

		request.setAttribute("Activity_Id", freighteditform.getActivity_Id());
		request.setAttribute("Freight_Id", freighteditform.getFreightid());
		request.setAttribute("commonResourceStatus", freightStatus);
		request.setAttribute("freightStatusList", freightStatusList);
		request.setAttribute("checkingRef", freighteditform.getRef());
		request.setAttribute("addendum_id", "0");

		// End of the Menu Code for Resource Freight

		request.setAttribute("checkingRef", freighteditform.getRef());
		return (mapping.findForward("freighttabularedit"));
	}
}
