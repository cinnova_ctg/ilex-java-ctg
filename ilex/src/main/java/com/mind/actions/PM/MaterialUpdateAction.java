package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.MaterialResource;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.AM.MaterialResourceForm;

public class MaterialUpdateAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String checkforresourcelink = "";
		String status = "";
		String sortqueryclause = "";
		String activityname = "";
		String typenet = "";
		
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( userid , "Activity" , "View Resources" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			materialresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = materialresourceform.getActivity_Id();
		}
		
		/*customer labor rates*/
		String Job_Id ="";
		String type ="";
		String MSA_Id ="";
		String Activitylibrary_Id ="";
		String MSA_Name ="";
		String Appendix_Id ="";
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			
		}
		
		if( request.getParameter( "type" ) != null )
		{	
			typenet = request.getParameter( "type" );
			
		}
		
		if( request.getParameter( "from" ) != null )
		{
			materialresourceform.setFromflag( request.getParameter( "from" ) );
			materialresourceform.setDashboardid( request.getParameter( "jobid" ) );
			
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		materialresourceform.setJobname(Jobdao.getoplevelname(materialresourceform.getActivity_Id(), "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		Job_Id = Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id);
		materialresourceform.setJobid(Job_Id);
		
		
		//		added by hamid
		request.setAttribute( "jobid" ,Job_Id);
		
		Appendix_Id = Activitydao.getAppendixid(getDataSource( request , "ilexnewDB" ),Job_Id);
		
		
		//MSA_Id = Activitydao.getmsaid(getDataSource( request , "ilexnewDB" ),Job_Id);
		MSA_Id = IMdao.getMSAId( Appendix_Id, getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setAppendix_id(Appendix_Id);
			
		//MSA_Name = Activitydao.getMsaname(getDataSource( request , "ilexnewDB" ),MSA_Id);
		int checkNetmedXappendix = 0;
		checkNetmedXappendix = Activitydao.checkAppendixname(Appendix_Id, Job_Id,getDataSource( request , "ilexnewDB" ));
		//activityname=Activitydao.getActivityname(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
		
		if( (checkNetmedXappendix==1) && typenet.equals( "PM" ) )
		{	
			
			type=request.getParameter("type");
			request.setAttribute("type",type);
			request.setAttribute( "MSA_Id" , MSA_Id );
			Activitylibrary_Id=request.getParameter("Activitylibrary_Id");
			request.setAttribute( "Activitylibrary_Id" , Activitylibrary_Id );
			
			return mapping.findForward( "customerlaborratepage" );
		}
		/*customer labor rates*/
		else
		{
			NameId idname = null;
			materialresourceform.setNewquantity( "0.0000" );
			materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
			materialresourceform.setNewstatus( "Draft" );
			materialresourceform.setActivity_Id( Activity_Id );
			materialresourceform.setMateriallist( Resourcedao.getResourcecombolist( "material" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
			
			materialresourceform.setBacktoactivity( "detailactivity" );       //flag for backtoactivity
			materialresourceform.setChkaddendum( "View" );                    //flag for labor , freight , travel tabular
			materialresourceform.setChkaddendumdetail( "View" );              //flag for materialdetail
			materialresourceform.setAddendum_id( "0" );
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setAppendix_id( idname.getId() );
			materialresourceform.setAppendixname( idname.getName() );
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setMsa_id( idname.getId() );
			materialresourceform.setMsaname( idname.getName() );
			
			ArrayList materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "M" , "%" , sortqueryclause , request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			
			
			if( materialresourcelist.size() > 0 )
			{
				resource_size = materialresourcelist.size();
			}
			
			/* for checkbox check start */
			
			String checkTempResources[] = Resourcedao.checkTempResources(Activity_Id , "M" , "%" , sortqueryclause ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setCheck(checkTempResources);
			/* for checkbox check end */
			
			status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			
			request.setAttribute( "act_status" , status );
			request.setAttribute( "activityname" , activityname );
			
			request.setAttribute( "Size" , new Integer ( resource_size ) );
			request.setAttribute( "materialresourcelist" , materialresourcelist );
			
			request.setAttribute( "refresh" , "true" );
			request.setAttribute( "Activity_Id" , Activity_Id );
			
			
			return( mapping.findForward( "materialtabularpage" ) );
		}
		
	}
	
	
	/*public ActionForward Refresh( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		  
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		
		String Activity_Id = "";
		String newname = "";
		String newnamecombo = "";
		String newmaterialid = "";
		ArrayList materialresourcelist = new ArrayList();
		String activityname = "";
		
		String flag1 = "";
		String flag2 = "";
		String flag3 = "";
		String flag4 = "";
		String flag5 = "";
		String flag6 = "";
		
		
		flag1 = materialresourceform.getBacktoactivity();       //flag for backtoactivity
		flag2 = materialresourceform.getChkaddendum();                    //flag for labor , freight , travel tabular
		flag3 = materialresourceform.getChkaddendumdetail();
		flag4 = materialresourceform.getAddendum_id();
		flag5 = materialresourceform.getFromflag();
		flag6 = materialresourceform.getDashboardid();
		
		int resource_size = 0;
		Activity_Id = materialresourceform.getActivity_Id();
		int checklength = 0;
		ArrayList temp = new ArrayList();
		String[] checkrefresh = null;
		String status = "";
		newname = materialresourceform.getNewmaterialname();
		newnamecombo = materialresourceform.getNewmaterialnamecombo();
		newmaterialid = materialresourceform.getNewmaterialid();
		
		if( materialresourceform.getCheck()!=  null )
		{
			checklength = materialresourceform.getCheck().length;	 
			checkrefresh = materialresourceform.getCheck();
			
			int []tempindex = new int[checklength];
			int k = 0;
				
			if( checklength > 0 )
			{	int length = materialresourceform.getMaterialid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( materialresourceform.getCheck( i ).equals( materialresourceform.getMaterialid( j ) ) )
						{	
							tempindex[k] = j;
							k++;
						}
					}
				}
			}	
			
			for( int i = 0; i < checklength; i++ )
			{
				MaterialResource tempresource = new MaterialResource();
				
				tempresource.setActivity_Id( Activity_Id );
				
				tempresource.setMaterialid( materialresourceform.getMaterialid( tempindex[i] ) );
				tempresource.setMaterialcostlibid( materialresourceform.getMaterialcostlibid( tempindex[i] ) );
				tempresource.setMaterialname( materialresourceform.getMaterialname( tempindex[i] ) );
				tempresource.setMaterialtype( materialresourceform.getMaterialtype( tempindex[i] ) );
				tempresource.setManufacturername( materialresourceform.getManufacturername( tempindex[i] ) );
				tempresource.setManufacturerpartnumber( materialresourceform.getManufacturerpartnumber( tempindex[i] ) );
				tempresource.setCnspartnumber( materialresourceform.getCnspartnumber( tempindex[i] ) );
				tempresource.setEstimatedtotalcost( materialresourceform.getEstimatedtotalcost( tempindex[i] ) );
				tempresource.setEstimatedunitcost( materialresourceform.getEstimatedunitcost( tempindex[i] ) );
				tempresource.setMinimumquantity( materialresourceform.getMinimumquantity( tempindex[i] ) );
				tempresource.setPriceextended( materialresourceform.getPriceextended( tempindex[i] ) );
				tempresource.setPriceunit( materialresourceform.getPriceunit( tempindex[i] ) );
				tempresource.setProformamargin( materialresourceform.getProformamargin( tempindex[i] ) );
				
				tempresource.setQuantity( materialresourceform.getQuantity( tempindex[i] ) );
				tempresource.setPrevquantity( materialresourceform.getPrevquantity( tempindex[i] ) );
				
				tempresource.setSellablequantity( materialresourceform.getSellablequantity( tempindex[i] ) );
				tempresource.setStatus( materialresourceform.getStatus( tempindex[i] ) );
				
				
				temp.add( tempresource );
			}	
		}	 
			
		MaterialResource materialresource = Resourcedao.getMaterialResource(  materialresourceform.getNewmaterialnamecombo() , getDataSource( request , "ilexnewDB" ) );
		
		materialresourceform.reset( mapping , request);
		
		materialresourceform.setNewmaterialid( newmaterialid );
		materialresourceform.setNewmaterialname( newname );
		materialresourceform.setNewmaterialnamecombo( newnamecombo );
		
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setNewmaterialtype( materialresource.getMaterialtype() );
		materialresourceform.setNewmanufacturername( materialresource.getManufacturername() );
		materialresourceform.setNewmanufacturerpartnumber( materialresource.getManufacturerpartnumber() );
		materialresourceform.setNewcnspartnumber( materialresource.getCnspartnumber() );
		materialresourceform.setNewestimatedunitcost( materialresource.getEstimatedunitcost() );
		materialresourceform.setNewsellablequantity( materialresource.getSellablequantity() );
		materialresourceform.setNewminimumquantity( materialresource.getMinimumquantity() );
		materialresourceform.setRef( null );
		materialresourceform.setActivity_Id( Activity_Id );
		materialresourceform.setMateriallist( Resourcedao.getResourcecombolist( "material" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );
		
		materialresourceform.setBacktoactivity( flag1 );       //flag for backtoactivity
		materialresourceform.setChkaddendum( flag2 );                    //flag for labor , freight , travel tabular
		materialresourceform.setChkaddendumdetail( flag3 );
		materialresourceform.setAddendum_id( flag4 );
		
		materialresourceform.setFromflag( flag5 );
		materialresourceform.setDashboardid( flag6 );
		
		if( materialresourceform.getFromflag().equals( "jobdashboard" ) || materialresourceform.getFromflag().equals( "jobdashboard_resource" ))
		{
			request.setAttribute( "from" , materialresourceform.getFromflag() );
			request.setAttribute( "jobid" , materialresourceform.getDashboardid() );
			
		}
		
		
		if( materialresourceform.getChkaddendum().equals( "View" ) )
		{
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "M" , "%" , "" , request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( materialresourceform.getChkaddendum().equals( "ViewAddendum" ) )
		{
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "AM" , "%" , "" , request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( materialresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
		{
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "CM" , "%" , "" , request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( materialresourceform.getChkaddendum().equals( "Viewinscope" ) )
		{
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IM" , "%" , "" , request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
	
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();	
		}
		
		
		if( checklength > 0 )
		{
			if( resource_size > 0 )
			{
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < resource_size; j++ )
					{
						if( ( ( MaterialResource )temp.get( i ) ).getMaterialid().equals( ( ( MaterialResource )materialresourcelist.get( j ) ).getMaterialid() ) )
						{
							materialresourcelist.set( j ,  temp.get( i ) );
						}
					}
				}
				materialresourceform.setCheck( checkrefresh );
			}
			
		}
		
		materialresourceform.setJobname(Jobdao.getoplevelname(materialresourceform.getActivity_Id(), "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
	
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		
		request.setAttribute( "refresh" , "false" );
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "materialtabularpage" ) );
	}*/
	
	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		if( request.getAttribute( "from" ) != null )
		{
			request.setAttribute( "from" , request.getParameter( "from" ) );
		}
		
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		String sortqueryclause = "";
		String Activity_Id = "";
		MaterialResource materialresource = null;
		int resource_size = 0;
		String status = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		ArrayList materialresourcelist = new ArrayList();
		String flag1 = "";
		String flag2 = "";
		String flag3 = "";
		String flag4 = "";
		NameId idname = null;
		String activityname = "";
		int addflag=0;
		flag1 = materialresourceform.getBacktoactivity();       //flag for backtoactivity
		flag2 = materialresourceform.getChkaddendum();                    //flag for labor , freight , travel tabular
		flag3 = materialresourceform.getChkaddendumdetail();
		flag4 = materialresourceform.getAddendum_id();
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		Activity_Id = materialresourceform.getActivity_Id();
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( materialresourceform.getCheck()!=  null )
		{
			int checklength = materialresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = materialresourceform.getMaterialid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						
						if( materialresourceform.getCheck(i).substring(1,materialresourceform.getCheck(i).length()).equals( materialresourceform.getMaterialid( j )))
						{	
							index[k] = j;
							k++;
						}
						
						/*if( materialresourceform.getCheck(i).equals( materialresourceform.getMaterialid( j ) ) )
						{	
							index[k] = j;
							k++;
						}*/
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				for( int i = 0; i < index.length; i++ )
				{
					materialresource = new MaterialResource();
					materialresource.setMaterialid( materialresourceform.getMaterialid ( index[i] ) );
					
					materialresource.setMaterialcostlibid( materialresourceform.getMaterialcostlibid ( index[i] ) );
					
					materialresource.setActivity_Id( materialresourceform.getActivity_Id() );
					
					materialresource.setQuantity( materialresourceform.getQuantity ( index[i] ) );
					
					materialresource.setPrevquantity( materialresourceform.getPrevquantity( index[i] ) );
					
					if( materialresourceform.getChkaddendum().equals( "View" ) )
					{
						materialresource.setMaterialtype( "M" );
					}
					
					if( materialresourceform.getChkaddendum().equals( "Viewinscope" ) )
					{
						materialresource.setMaterialtype( "IM" );
					}
					
					materialresource.setEstimatedunitcost( materialresourceform.getEstimatedunitcost( index[i] ) );
					
					materialresource.setProformamargin( materialresourceform.getProformamargin( index[i] ) );
					/* for multiple resource select start*/
					materialresource.setCnspartnumber( materialresourceform.getCnspartnumber( index[i] ) );
					materialresource.setFlag( materialresourceform.getFlag( index[i] ) );
					if( materialresourceform.getFlag( index[i] ).equals( "P" ) )
					{
						int updateflag = Resourcedao.updatematerialresource( materialresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "updateflag" , ""+updateflag );
					}
					if( materialresourceform.getFlag( index[i] ).equals( "T" ) )
					{
						addflag = Resourcedao.addmaterialresource( materialresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "addflag" , ""+addflag );
					}
					/* for multiple resource select end*/
			
					/*int updateflag = Resourcedao.updatematerialresource( materialresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );*/
				}
			}
		}
		
		if( materialresourceform.getNewmaterialid() != null )
		{
			materialresource = new MaterialResource();
			materialresource.setMaterialid( materialresourceform.getNewmaterialnamecombo() );
			materialresource.setActivity_Id( materialresourceform.getActivity_Id() );
			materialresource.setCnspartnumber( materialresourceform.getNewcnspartnumber() );
			materialresource.setQuantity( materialresourceform.getNewquantity() );
			if( materialresourceform.getChkaddendum().equals( "View" ) )
			{
				materialresource.setMaterialtype( "M" );
			}
				
			if( materialresourceform.getChkaddendum().equals( "Viewinscope" ) )
			{
				materialresource.setMaterialtype( "IM" );
			}
			
			materialresource.setEstimatedunitcost( materialresourceform.getNewestimatedunitcost() );
			materialresource.setProformamargin( materialresourceform.getNewproformamargin() );
			
			addflag = Resourcedao.addmaterialresource( materialresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		if( materialresourceform.getFromflag().equals( "jobdashboard" ) || materialresourceform.getFromflag().equals( "jobdashboard_resource" ) )
		{
			request.setAttribute( "jobid" , materialresourceform.getDashboardid() );
			return mapping.findForward( "jobdashboard" );
		}
		
		materialresourceform.reset( mapping , request );
		
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setActivity_Id( Activity_Id );
		
		//materialresourceform.setRef( null );
		materialresourceform.setMateriallist( Resourcedao.getResourcecombolist( "material" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		materialresourceform.setBacktoactivity( flag1 );       //flag for backtoactivity
		materialresourceform.setChkaddendum( flag2 );                    //flag for labor , freight , travel tabular
		materialresourceform.setChkaddendumdetail( flag3 );
		materialresourceform.setAddendum_id( flag4 );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setAppendix_id( idname.getId() );
		materialresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		
		if( materialresourceform.getChkaddendum().equals( "View" ) )
		{
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "M" , "%" , sortqueryclause , request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setRef( "View" );
		}
	
		if( materialresourceform.getChkaddendum().equals( "Viewinscope" ) )
		{
			status = com.mind.dao.PRM.Activitydao.getCurrentstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IM" , "%" , sortqueryclause , request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setRef( "Viewinscope" );
		}
		
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
		}
	
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setJobname(Jobdao.getoplevelname(materialresourceform.getActivity_Id(), "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setJobid(Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id));
	
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		
		request.setAttribute( "refresh" , "true" );
	
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "materialtabularpage" ) );
	}
	
	
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		int deleteflag = -1;
		String tempflagforref = "";
		ArrayList materialresourcelist = new ArrayList();
		String activityname = "";
		NameId idname = null;
		String sortqueryclause = "";
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			materialresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = materialresourceform.getActivity_Id();
		}
		
		deleteflag = Resourcedao.deleteresource( request.getParameter( "Material_Id" ), userid,  getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setActivity_Id( Activity_Id );
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		 
		if( request.getParameter( "type" ) != null )
		{
			tempflagforref = request.getParameter( "type" );
		}
		
		if( tempflagforref.equals( "View" ) )
		{
			materialresourceform.setBacktoactivity( "detailactivity" );       //flag for backtoactivity
			materialresourceform.setChkaddendum( "View" );                    //flag for labor , freight , travel tabular
			materialresourceform.setChkaddendumdetail( "View" );              //flag for materialdetail
			materialresourceform.setAddendum_id( "0" );
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "M" , "%" , sortqueryclause , request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setRef( "View" );
		}
		
		if( tempflagforref.equals( "Viewinscope" ) )
		{	
			materialresourceform.setBacktoactivity( "inscopedetailactivity" );       //flag for backtoactivity
			materialresourceform.setChkaddendum( "Viewinscope" );                    //flag for labor , freight , travel
			materialresourceform.setChkaddendumdetail( "Viewinscope" );              //flag for materialdetail
			materialresourceform.setAddendum_id( "0" );
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IM" , "%" , sortqueryclause , request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );	
			materialresourceform.setRef( "Viewinscope" );
		}
		
		materialresourceform.setMateriallist( Resourcedao.getResourcecombolist( "Material" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
		}
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setAppendix_id( idname.getId() );
		materialresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setJobname(Jobdao.getoplevelname(materialresourceform.getActivity_Id(), "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		materialresourceform.setJobid(Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id));
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		request.setAttribute( "refresh" , "true" );
	
		return( mapping.findForward( "materialtabularpage" ) );
	}
	
	public ActionForward Viewinscope( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String checkforresourcelink = "";
		String status = "";
		String sortqueryclause = "";
		String activityname = "";
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		String Job_Id ="";
		NameId idname = null;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( userid , "Activity" , "View Resources" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end
		
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		} */
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			materialresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = materialresourceform.getActivity_Id();
		}
		
		
		if( request.getParameter( "from" ) != null )
		{
			materialresourceform.setFromflag( request.getParameter( "from" ) );
			materialresourceform.setDashboardid( request.getParameter( "jobid" ) );
		
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		Job_Id = Activitydao.getJobid( getDataSource( request , "ilexnewDB" ) , Activity_Id );
		
		materialresourceform.setJobid( Job_Id );
		
		
		//		added by hamid
		request.setAttribute( "jobid" , Job_Id );
		//Start :Added By Amit
		if( request.getParameter( "fromPage" ) != null )
		{
			materialresourceform.setFromPage( ( String )request.getParameter( "fromPage" ) );
		}
		//End : Added By Amit
		
		
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setActivity_Id( Activity_Id );
		
		materialresourceform.setBacktoactivity( "inscopedetailactivity" );       //flag for backtoactivity
		materialresourceform.setChkaddendum( "Viewinscope" );                    //flag for labor , freight , travel
		materialresourceform.setChkaddendumdetail( "Viewinscope" );              //flag for materialdetail
		materialresourceform.setAddendum_id( "0" );
		
		
		materialresourceform.setMateriallist( Resourcedao.getResourcecombolist( "material" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		ArrayList materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IM" , "%" , sortqueryclause , request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
		
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
		}
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setAppendix_id( idname.getId() );
		materialresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		
		/* for checkbox check start */
		
		String checkTempResources[] = Resourcedao.checkTempResources(Activity_Id , "IM" , "%" , sortqueryclause ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setCheck(checkTempResources);
		/* for checkbox check end */
		
		status = com.mind.dao.PRM.Activitydao.getCurrentstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setJobname(Jobdao.getoplevelname(materialresourceform.getActivity_Id(), "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		
		return( mapping.findForward( "materialtabularpage" ) );
	}
	

	/**
	 * This method is for Mass Approve 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward massApprove( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		MaterialResource materialresource = null;
		int changestatusflag = -1; 
		String Activitylibrary_Id ="";
		String activityname = "";
		String Activity_Id = "";
		String sortqueryclause = "";
		String typenet = "";
		int resource_size = 0;
		
		String checkforresourcelink = "";
		String status = "";
		
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( userid , "Activity" , "View Resources" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			materialresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = materialresourceform.getActivity_Id();
		}
		
		/*if( request.getParameter( "from" ) != null )
		{
			materialresourceform.setFromflag( request.getParameter( "from" ) );
			materialresourceform.setDashboardid( request.getParameter( "jobid" ) );
			
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}*/
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			
		}
		
			if( materialresourceform.getCheck()!=  null )
			{
				int checklength = materialresourceform.getCheck().length;	 
		
				int []index = new int[checklength];
				int k = 0;
				
				if( checklength > 0 )
				{	int length = materialresourceform.getMaterialid().length;
					for( int i = 0; i < checklength; i++ )
					{
						for( int j = 0; j < length; j++ )
						{
							if( materialresourceform.getCheck(i).substring(1,materialresourceform.getCheck(i).length()).equals( materialresourceform.getMaterialid( j )))
							{	
								index[k] = j;
								k++;
							}
						}
					}
				}
			
				if( checklength > 0 )                               
				{
					for( int i = 0; i < index.length; i++ )
					{
						materialresource = new MaterialResource();
						materialresource.setMaterialid( materialresourceform.getMaterialid ( index[i] ) );
						materialresource.setActivity_Id( materialresourceform.getActivity_Id() );
						//materialresource.setMaterialtype( "M" );
						changestatusflag = ChangeStatus.Status( "" , "" , "" , "" , materialresource.getMaterialid() , "pm_resM" , "A" ,"N", getDataSource( request , "ilexnewDB" ),"" );
					}
				}
			}
			
			NameId idname = null;
			materialresourceform.setNewquantity( "0.0000" );
			materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
			materialresourceform.setNewstatus( "Draft" );
			materialresourceform.setActivity_Id( Activity_Id );
			materialresourceform.setMateriallist( Resourcedao.getResourcecombolist( "material" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
			
			materialresourceform.setBacktoactivity( "detailactivity" );       //flag for backtoactivity
			materialresourceform.setChkaddendum( "View" );                    //flag for labor , freight , travel tabular
			materialresourceform.setChkaddendumdetail( "View" );              //flag for materialdetail
			materialresourceform.setAddendum_id( "0" );
			materialresourceform.setRef( "View" );  
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setAppendix_id( idname.getId() );
			materialresourceform.setAppendixname( idname.getName() );
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setMsa_id( idname.getId() );
			materialresourceform.setMsaname( idname.getName() );
			
			ArrayList materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "M" , "%" , sortqueryclause , request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			
			if( materialresourcelist.size() > 0 )
			{
				resource_size = materialresourcelist.size();
			}
			
			/* for checkbox check start */
			
			String checkTempResources[] = Resourcedao.checkTempResources( Activity_Id , "M" , "%" , sortqueryclause ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setCheck(checkTempResources);
			/* for checkbox check end */
			
			status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			
			request.setAttribute( "act_status" , status );
			request.setAttribute( "activityname" , activityname );
			
			request.setAttribute( "Size" , new Integer ( resource_size ) );
			request.setAttribute( "materialresourcelist" , materialresourcelist );
			
			request.setAttribute( "refresh" , "true" );
			request.setAttribute( "Activity_Id" , Activity_Id );
		
		return( mapping.findForward( "materialtabularpage" ) );
	}
	
	
	/**
	 * @ This method is for mass delete of Resource
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward massDelete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		MaterialResource materialresource = null;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		int deleteflag = -1;
		String tempflagforref = "";
		ArrayList materialresourcelist = new ArrayList();
		String activityname = "";
		NameId idname = null;
		String sortqueryclause = "";
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			materialresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = materialresourceform.getActivity_Id();
		}
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		//Start :Added By Amit
		if( materialresourceform.getCheck()!=  null )
		{
			
			int checklength = materialresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = materialresourceform.getMaterialid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( materialresourceform.getCheck(i).substring(1,materialresourceform.getCheck(i).length()).equals( materialresourceform.getMaterialid( j )))
						{		
							index[k] = j;
							k++;
						}
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				for( int i = 0; i < index.length; i++ )
				{
					materialresource = new MaterialResource();
					materialresource.setMaterialid( materialresourceform.getMaterialid ( index[i] ) );
					
					deleteflag = Resourcedao.deleteresource( materialresource.getMaterialid(), userid, getDataSource( request , "ilexnewDB" ) );		
				}
			}
		}
		//End :
		
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setActivity_Id( Activity_Id );
		
		if( request.getParameter( "type" ) != null )
		{
			tempflagforref = request.getParameter( "type" );
		}
		
		if( tempflagforref.equals( "View" ) )
		{
			materialresourceform.setBacktoactivity( "detailactivity" );       //flag for backtoactivity
			materialresourceform.setChkaddendum( "View" );                    //flag for labor , freight , travel tabular
			materialresourceform.setChkaddendumdetail( "View" );              //flag for materialdetail
			materialresourceform.setAddendum_id( "0" );
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "M" , "%" , sortqueryclause , request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			materialresourceform.setRef( "View" );
		}
		
		if( tempflagforref.equals( "Viewinscope" ) )
		{	
			materialresourceform.setBacktoactivity( "inscopedetailactivity" );       //flag for backtoactivity
			materialresourceform.setChkaddendum( "Viewinscope" );                    //flag for labor , freight , travel
			materialresourceform.setChkaddendumdetail( "Viewinscope" );              //flag for materialdetail
			materialresourceform.setAddendum_id( "0" );
			materialresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IM" , "%" , sortqueryclause , request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );	
			materialresourceform.setRef( "Viewinscope" );
		}
		
		materialresourceform.setMateriallist( Resourcedao.getResourcecombolist( "Material" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
		}
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setAppendix_id( idname.getId() );
		materialresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		materialresourceform.setRef( "View" );  
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setJobname(Jobdao.getoplevelname(materialresourceform.getActivity_Id(), "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		materialresourceform.setJobid(Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id));
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		request.setAttribute( "refresh" , "true" );
	
		return( mapping.findForward( "materialtabularpage" ) );
	}

	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "pmresM_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "pmresM_sort" );
		return queryclause;
	}
}
