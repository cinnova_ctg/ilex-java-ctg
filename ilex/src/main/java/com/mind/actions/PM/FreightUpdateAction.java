package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.FreightResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.AM.FreightResourceForm;




public class FreightUpdateAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		String sortqueryclause = "";
		String activityname = "";
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		String Job_Id = "";
		NameId idname = null;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			freightresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = freightresourceform.getActivity_Id();
		}
		
		if( request.getParameter( "from" ) != null )
		{
			freightresourceform.setFromflag( request.getParameter( "from" ) );
			freightresourceform.setDashboardid( request.getParameter( "jobid" ) );
		
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		Job_Id = Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id);
		freightresourceform.setJobid( Job_Id );
		
		//		added by hamid
		request.setAttribute( "jobid" ,Job_Id);
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activity_Id );
		freightresourceform.setFreightlist( Resourcedao.getResourcecombolist( "freight" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		freightresourceform.setBacktoactivity( "detailactivity" );
		freightresourceform.setChkaddendum( "View" );
		freightresourceform.setChkaddendumdetail( "View" );
		freightresourceform.setAddendum_id( "0" );
		
		ArrayList freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "F" , "%" , sortqueryclause , request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setAppendix_id( idname.getId() );
		freightresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		
		/* for checkbox check start */
		
		String checkTempResources[] = Resourcedao.checkTempResources( Activity_Id , "F" , "%" , sortqueryclause ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setCheck( checkTempResources );
		/* for checkbox check end */
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		
		return( mapping.findForward( "freighttabularpage" ) );
	}
	
	
	/*public ActionForward Refresh( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		String Activity_Id = "";
		String newname = "";
		String newnamecombo = "";
		String newfreightid = "";
		int resource_size = 0;
		String status = "";
		int checklength = 0;
		ArrayList temp = new ArrayList();
		String[] checkrefresh = null;
		ArrayList freightresourcelist = new ArrayList();
		String flag1 = "";
		String flag2 = "";
		String flag3 = "";
		String flag4 = "";
		String flag5 = "";
		String flag6 = "";
		
		
		
		String activityname = "";
		flag1 = freightresourceform.getBacktoactivity();
		flag2 = freightresourceform.getChkaddendum();
		flag3 = freightresourceform.getChkaddendumdetail();
		flag4 = freightresourceform.getAddendum_id();
		
		flag5 = freightresourceform.getFromflag();
		flag6 = freightresourceform.getDashboardid();
		
		
		
		Activity_Id = freightresourceform.getActivity_Id();
		
		newname = freightresourceform.getNewfreightname();
		newnamecombo = freightresourceform.getNewfreightnamecombo();
		newfreightid = freightresourceform.getNewfreightid();
		
		
		//	Start :Added By Amit
		if(request.getParameter("fromPage")!=null)
		{
			
			freightresourceform.setFromPage((String)request.getParameter("fromPage"));
		}
		
		//End : Added By Amit
		
		
		if( freightresourceform.getCheck()!=  null )
		{
			checklength = freightresourceform.getCheck().length;	 
			checkrefresh = freightresourceform.getCheck();
			
			int []tempindex = new int[checklength];
			int k = 0;
				
			if( checklength > 0 )
			{	int length = freightresourceform.getFreightid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( freightresourceform.getCheck( i ).equals( freightresourceform.getFreightid( j ) ) )
						{	
							tempindex[k] = j;
							k++;
						}
					}
				}
			}	
			
			for( int i = 0; i < checklength; i++ )
			{
				FreightResource tempresource = new FreightResource();
				
				tempresource.setActivity_Id( Activity_Id );
				
				tempresource.setFreightid( freightresourceform.getFreightid( tempindex[i] ) );
				tempresource.setFreightcostlibid( freightresourceform.getFreightcostlibid( tempindex[i] ) );
				
				tempresource.setFreightname( freightresourceform.getFreightname( tempindex[i] ) );
				tempresource.setFreighttype( freightresourceform.getFreighttype( tempindex[i] ) );
				
				tempresource.setCnspartnumber( freightresourceform.getCnspartnumber( tempindex[i] ) );
				tempresource.setEstimatedtotalcost( freightresourceform.getEstimatedtotalcost( tempindex[i] ) );
				tempresource.setEstimatedunitcost( freightresourceform.getEstimatedunitcost( tempindex[i] ) );
				tempresource.setMinimumquantity( freightresourceform.getMinimumquantity( tempindex[i] ) );
				tempresource.setPriceextended( freightresourceform.getPriceextended( tempindex[i] ) );
				tempresource.setPriceunit( freightresourceform.getPriceunit( tempindex[i] ) );
				tempresource.setProformamargin( freightresourceform.getProformamargin( tempindex[i] ) );
				
				tempresource.setQuantity( freightresourceform.getQuantity( tempindex[i] ) );
				tempresource.setPrevquantity( freightresourceform.getPrevquantity( tempindex[i] ) );
				
				tempresource.setSellablequantity( freightresourceform.getSellablequantity( tempindex[i] ) );
				tempresource.setStatus( freightresourceform.getStatus( tempindex[i] ) );
				
				
				temp.add( tempresource );
			}	
		}
		
		FreightResource freightresource = Resourcedao.getFreightResource(  freightresourceform.getNewfreightnamecombo() , getDataSource( request , "ilexnewDB" ) );
		
		freightresourceform.reset( mapping , request);
		
		freightresourceform.setNewfreightid( newfreightid );
		freightresourceform.setNewfreightname( newname );
		freightresourceform.setNewfreightnamecombo( newnamecombo );
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setNewfreighttype( freightresource.getFreighttype() );
		
		freightresourceform.setNewcnspartnumber( freightresource.getCnspartnumber() );
		
		freightresourceform.setNewestimatedunitcost( freightresource.getEstimatedunitcost() );
		freightresourceform.setNewsellablequantity( freightresource.getSellablequantity() );
		freightresourceform.setNewminimumquantity( freightresource.getMinimumquantity() );
		freightresourceform.setRef( null );
		freightresourceform.setActivity_Id( Activity_Id );
		freightresourceform.setFreightlist( Resourcedao.getResourcecombolist( "freight" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );
		
		freightresourceform.setBacktoactivity( flag1 );
		freightresourceform.setChkaddendum( flag2 );
		freightresourceform.setChkaddendumdetail( flag3 );
		freightresourceform.setAddendum_id( flag4 );
		
		freightresourceform.setFromflag( flag5 );
		freightresourceform.setDashboardid( flag6 );
		
		if( freightresourceform.getFromflag().equals( "jobdashboard" ) || freightresourceform.getFromflag().equals( "jobdashboard_resource" ) )
		{
			request.setAttribute( "from" , freightresourceform.getFromflag() );
			request.setAttribute( "jobid" , freightresourceform.getDashboardid() );
			
		}
		
		if( freightresourceform.getChkaddendum().equals( "View" ) )
		{
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "F" , "%" , "" ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( freightresourceform.getChkaddendum().equals( "ViewAddendum" ) )
		{
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "AF" , "%" , "" ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( freightresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
		{
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "CF" , "%" , "" ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		if( freightresourceform.getChkaddendum().equals( "Viewinscope" ) )
		{
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IF" , "%" , "" ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		
		
		if( checklength > 0 )
		{
			if( resource_size > 0 )
			{
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < resource_size; j++ )
					{
						if( ( ( FreightResource )temp.get( i ) ).getFreightid().equals( ( ( FreightResource )freightresourcelist.get( j ) ).getFreightid() ) )
						{
							freightresourcelist.set( j ,  temp.get( i ) );
						}
					}
				}
				freightresourceform.setCheck( checkrefresh );
			}
			
		}
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		request.setAttribute( "refresh" , "false" );

		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "freighttabularpage" ) );
	}*/
	
	
	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		String Activity_Id = "";
		FreightResource freightresource = null;
		int resource_size = 0;
		String status = "";
		
		String sortqueryclause = "";
		
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		ArrayList freightresourcelist = new ArrayList();
		String flag1 = "";
		String flag2 = "";
		String flag3 = "";
		String flag4 = "";
		String activityname = "";
		int addflag = 0;
		NameId idname = null;
		flag1 = freightresourceform.getBacktoactivity();
		flag2 = freightresourceform.getChkaddendum();
		flag3 = freightresourceform.getChkaddendumdetail();
		flag4 = freightresourceform.getAddendum_id();
		
		Activity_Id = freightresourceform.getActivity_Id();
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) != null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
	
		//UPdate and insert code
		
		if( freightresourceform.getCheck()!=  null )
		{
			int checklength = freightresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = freightresourceform.getFreightid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( freightresourceform.getCheck( i ).substring( 1 , freightresourceform.getCheck( i ).length() ).equals( freightresourceform.getFreightid( j ) ) )
						{	
							index[k] = j;
							k++;
						}
						
						/*if( freightresourceform.getCheck(i).equals( freightresourceform.getFreightid( j ) ) )
						{	
							index[k] = j;
							k++;
						}*/
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				for( int i = 0; i < index.length; i++ )
				{
					freightresource = new FreightResource();
					freightresource.setFreightid( freightresourceform.getFreightid ( index[i] ) );
					freightresource.setFreightcostlibid( freightresourceform.getFreightcostlibid ( index[i] ) );
					freightresource.setActivity_Id( freightresourceform.getActivity_Id() );
					freightresource.setQuantity( freightresourceform.getQuantity ( index[i] ) );
					freightresource.setPrevquantity( freightresourceform.getPrevquantity( index[i] ) );
					
					if( freightresourceform.getChkaddendum().equals( "View" ) )
					{
						freightresource.setFreighttype( "F" );
					}
					
					if( freightresourceform.getChkaddendum().equals( "ViewAddendum" ) )
					{
						freightresource.setFreighttype( "AF" );
					}
					
					if( freightresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
					{
						freightresource.setFreighttype( "CF" );
					}
					
					if( freightresourceform.getChkaddendum().equals( "Viewinscope" ) )
					{
						freightresource.setFreighttype( "IF" );
					}
					
					freightresource.setEstimatedunitcost( freightresourceform.getEstimatedunitcost( index[i] ) );
					freightresource.setProformamargin( freightresourceform.getProformamargin( index[i] ) );
					
					/* for multiple resource select start*/
					freightresource.setCnspartnumber( freightresourceform.getCnspartnumber( index[i] ) );
					freightresource.setFlag( freightresourceform.getFlag( index[i] ) );
					if( freightresourceform.getFlag( index[i] ).equals( "P" ) )
					{
						int updateflag = Resourcedao.updatefreightresource( freightresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "updateflag" , ""+updateflag );
					}
					if( freightresourceform.getFlag( index[i] ).equals( "T" ) )
					{
						addflag = Resourcedao.addfreightresource( freightresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "addflag" , ""+addflag );
					}
					/* for multiple resource select end*/
			
					/*int updateflag = Resourcedao.updatefreightresource( freightresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );*/
				}	
			}
		}

		
		if( freightresourceform.getNewfreightid() != null )
		{
			freightresource = new FreightResource();
			freightresource.setFreightid( freightresourceform.getNewfreightnamecombo() );
			freightresource.setActivity_Id( freightresourceform.getActivity_Id() );
			freightresource.setCnspartnumber( freightresourceform.getNewcnspartnumber() );
			freightresource.setQuantity( freightresourceform.getNewquantity() );
			if( freightresourceform.getChkaddendum().equals( "View" ) )
			{
				freightresource.setFreighttype( "F" );
			}
			
			if( freightresourceform.getChkaddendum().equals( "ViewAddendum" ) )
			{
				freightresource.setFreighttype( "AF" );
			}
			
			if( freightresourceform.getChkaddendum().equals( "ViewChangeorder" ) )
			{
				freightresource.setFreighttype( "CF" );
			}
			
			if( freightresourceform.getChkaddendum().equals( "Viewinscope" ) )
			{
				freightresource.setFreighttype( "IF" );
			}
			
			freightresource.setEstimatedunitcost( freightresourceform.getNewestimatedunitcost() );
			freightresource.setProformamargin( freightresourceform.getNewproformamargin() );
			
		
			addflag = Resourcedao.addfreightresource( freightresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		if( freightresourceform.getFromflag().equals( "jobdashboard" ) || freightresourceform.getFromflag().equals( "jobdashboard_resource" ) )
		{
			request.setAttribute( "jobid" , freightresourceform.getDashboardid() );
			return mapping.findForward( "jobdashboard" );
		}
		
		freightresourceform.reset( mapping , request);
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activity_Id );
		
		//freightresourceform.setRef( null );
		freightresourceform.setFreightlist( Resourcedao.getResourcecombolist( "freight" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		freightresourceform.setBacktoactivity( flag1 );
		freightresourceform.setChkaddendum( flag2 );
		freightresourceform.setChkaddendumdetail( flag3 );
		freightresourceform.setAddendum_id( flag4 );
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		
		
		if( freightresourceform.getChkaddendum().equals( "View" ) )
		{
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "F" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setRef( "View" );
		}
		
		if( freightresourceform.getChkaddendum().equals( "Viewinscope" ) )
		{
			status = com.mind.dao.PRM.Activitydao.getCurrentstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IF" , "%" , sortqueryclause , request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setRef( "Viewinscope" );
		}
		
				
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setAppendix_id( idname.getId() );
		freightresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setJobid( Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		request.setAttribute( "refresh" , "true" );
	
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "freighttabularpage" ) );
	}
	
	
	
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		int deleteflag = -1;
		String tempflagforref = "";
		ArrayList freightresourcelist = new ArrayList();
		String activityname = "";
		NameId idname = null;
		
		HttpSession session = request.getSession( true );
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		String userid = ( String )session.getAttribute( "userid" );
		
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			freightresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = freightresourceform.getActivity_Id();
		}
		
		deleteflag = Resourcedao.deleteresource( request.getParameter( "Freight_Id" ) , userid,  getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activity_Id );
		freightresourceform.setFreightlist( Resourcedao.getResourcecombolist( "freight" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		if( request.getParameter( "type" ) != null )
		{
			tempflagforref = request.getParameter( "type" );
		}
		if( tempflagforref.equals( "View" ) )
		{
			freightresourceform.setBacktoactivity( "detailactivity" );
			freightresourceform.setChkaddendum( "View" );
			freightresourceform.setChkaddendumdetail( "View" );
			freightresourceform.setAddendum_id( "0" );
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "F" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setRef( "View" );
		}
		
		if( tempflagforref.equals( "Viewinscope" ) )
		{
			freightresourceform.setBacktoactivity( "inscopedetailactivity" );
			freightresourceform.setChkaddendum( "Viewinscope" );
			freightresourceform.setChkaddendumdetail( "Viewinscope" );
			freightresourceform.setAddendum_id( "0" );
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IF" , "%" , sortqueryclause ,  request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setRef( "Viewinscope" );
		}
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setAppendix_id( idname.getId() );
		freightresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setJobid( Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "freighttabularpage" ) );	
	}	
	
	
	public ActionForward Viewinscope( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		String sortqueryclause = "";
		String activityname = "";
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		String Job_Id ="";
		NameId idname = null;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			freightresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = freightresourceform.getActivity_Id();
		}
		
		
		if( request.getParameter( "from" ) != null )
		{
			freightresourceform.setFromflag( request.getParameter( "from" ) );
			freightresourceform.setDashboardid( request.getParameter( "jobid" ) );
		
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		Job_Id = Activitydao.getJobid( getDataSource( request , "ilexnewDB" ) , Activity_Id );
		freightresourceform.setJobid(Job_Id);
		
		//		added by hamid
		request.setAttribute( "jobid" ,Job_Id);
		//Start :Added By Amit
		if( request.getParameter( "fromPage" )!= null )
		{
			
			freightresourceform.setFromPage( ( String )request.getParameter( "fromPage" ) );
		}
		//End : Added By Amit
		
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activity_Id );
		freightresourceform.setFreightlist( Resourcedao.getResourcecombolist( "freight" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		freightresourceform.setBacktoactivity( "inscopedetailactivity" );
		freightresourceform.setChkaddendum( "Viewinscope" );
		freightresourceform.setChkaddendumdetail( "Viewinscope" );
		freightresourceform.setAddendum_id( "0" );
		
		ArrayList freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IF" , "%" , sortqueryclause , request.getSession().getId(), "listpage", getDataSource( request , "ilexnewDB" ) );
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setAppendix_id( idname.getId() );
		freightresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		/* for checkbox check start */
		
		String checkTempResources[] = Resourcedao.checkTempResources(Activity_Id , "IF" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setCheck(checkTempResources);
		/* for checkbox check end */
		
		status = com.mind.dao.PRM.Activitydao.getCurrentstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		
		return( mapping.findForward( "freighttabularpage" ) );
	}
	
	
	/**
	 * This method wil bre invoked during mass delete
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward massDelete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		FreightResource tempresource = null;
		String Activity_Id = "";
		int resource_size = 0;
		String status = "";
		int deleteflag = -1;
		String tempflagforref = "";
		ArrayList freightresourcelist = new ArrayList();
		String activityname = "";
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			freightresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = freightresourceform.getActivity_Id();
		}
		
		//Start :Added By Amit
		if( freightresourceform.getCheck()!=  null )
		{
			
			int checklength = freightresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = freightresourceform.getFreightid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( freightresourceform.getCheck( i ).substring( 1 , freightresourceform.getCheck( i ).length() ).equals( freightresourceform.getFreightid( j ) ) )
						{	
							index[k] = j;
							k++;
						}
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				
				for( int i = 0; i < index.length; i++ )
				{
					tempresource = new FreightResource();
					tempresource.setFreightid( freightresourceform.getFreightid( ( index[i] ) ));
					
					deleteflag = Resourcedao.deleteresource( tempresource.getFreightid(), userid, getDataSource( request , "ilexnewDB" ) );
				}
			}
		}
		//End :
		
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activity_Id );
		freightresourceform.setFreightlist( Resourcedao.getResourcecombolist( "freight" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		if( request.getParameter( "type" ) != null )
		{
			tempflagforref = request.getParameter( "type" );
		}
		if( tempflagforref.equals( "View" ) )
		{
			freightresourceform.setBacktoactivity( "detailactivity" );
			freightresourceform.setChkaddendum( "View" );
			freightresourceform.setChkaddendumdetail( "View" );
			freightresourceform.setAddendum_id( "0" );
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "F" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setRef( "View" );
		}
		
		if( tempflagforref.equals( "Viewinscope" ) )
		{
			freightresourceform.setBacktoactivity( "inscopedetailactivity" );
			freightresourceform.setChkaddendum( "Viewinscope" );
			freightresourceform.setChkaddendumdetail( "Viewinscope" );
			freightresourceform.setAddendum_id( "0" );
			freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "IF" , "%" , sortqueryclause ,  request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setRef( "Viewinscope" );
		}
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setAppendix_id( idname.getId() );
		freightresourceform.setAppendixname( idname.getName() );
		
		idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		freightresourceform.setRef( "View" );  
		
		status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setJobname( Jobdao.getoplevelname( Activity_Id , "Resource" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setJobid( Activitydao.getJobid(getDataSource( request , "ilexnewDB" ),Activity_Id ) );
		
		
		request.setAttribute( "act_status" , status );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "Activity_Id" , Activity_Id );
		return( mapping.findForward( "freighttabularpage" ) );	
	}	
	
	/**
	 * This method is for massapprove.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	
	public ActionForward massApprove( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		FreightResource tempresource = null;
		int changestatusflag = -1; 
		String Activitylibrary_Id ="";
		String activityname = "";
		String Activity_Id = "";
		String sortqueryclause = "";
		String typenet = "";
		int resource_size = 0;
		
		String checkforresourcelink = "";
		String status = "";
		
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( userid , "Activity" , "View Resources" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) != null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end 
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Activity_Id" ) != null )
		{	
			Activity_Id = request.getParameter( "Activity_Id" );
			freightresourceform.setActivity_Id( Activity_Id );
		}
		else
		{
			Activity_Id = freightresourceform.getActivity_Id();
		}
		
		/*if( request.getParameter( "from" ) != null )
		{
			freightresourceform.setFromflag( request.getParameter( "from" ) );
			freightresourceform.setDashboardid( request.getParameter( "jobid" ) );
			
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}*/
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			
		}
		
			if( freightresourceform.getCheck()!=  null )
			{
				int checklength = freightresourceform.getCheck().length;	 
		
				int []index = new int[checklength];
				int k = 0;
				
				if( checklength > 0 )
				{	int length = freightresourceform.getFreightid().length;
					for( int i = 0; i < checklength; i++ )
					{
						for( int j = 0; j < length; j++ )
						{
							if( freightresourceform.getCheck(i).substring(1,freightresourceform.getCheck(i).length()).equals( freightresourceform.getFreightid( j )))
							{	
								
								index[k] = j;
								k++;
							}
							
						}
					}
				}
			
				if( checklength > 0 )                               
				{
					
					for( int i = 0; i < index.length; i++ )
					{
						tempresource = new FreightResource();
						tempresource.setFreightid( freightresourceform.getFreightid( index[i] ) );
						tempresource.setActivity_Id( freightresourceform.getActivity_Id() );
						//materialresource.setMaterialtype( "M" );
						changestatusflag = ChangeStatus.Status( "" , "" , "" , "" , tempresource.getFreightid() , "pm_resF" , "A" ,"N", getDataSource( request , "ilexnewDB" ),"" );
						
						
					}
				}
			}
			
			NameId idname = null;
			freightresourceform.setNewquantity( "0.0000" );
			freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
			freightresourceform.setNewstatus( "Draft" );
			freightresourceform.setActivity_Id( Activity_Id );
			freightresourceform.setFreightlist( Resourcedao.getResourcecombolist( "freight" , Activity_Id , getDataSource( request , "ilexnewDB" ) ) );	
			
			freightresourceform.setBacktoactivity( "detailactivity" );       //flag for backtoactivity
			freightresourceform.setChkaddendum( "View" );                    //flag for labor , freight , travel tabular
			freightresourceform.setChkaddendumdetail( "View" );              //flag for materialdetail
			freightresourceform.setAddendum_id( "0" );
			freightresourceform.setRef( "View" );  
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "Appendix" , getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setAppendix_id( idname.getId() );
			freightresourceform.setAppendixname( idname.getName() );
			
			idname = Activitydao.getIdName( Activity_Id , "Resource" , "MSA" , getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setMsa_id( idname.getId() );
			freightresourceform.setMsaname( idname.getName() );
			
			ArrayList freightresourcelist = Resourcedao.getResourcetabularlist( Activity_Id , "F" , "%" , sortqueryclause , request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
			
			
			if( freightresourcelist.size() > 0 )
			{
				resource_size = freightresourcelist.size();
			}
			
			/* for checkbox check start */
			
			String checkTempResources[] = Resourcedao.checkTempResources( Activity_Id , "F" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
			freightresourceform.setCheck(checkTempResources);
			/* for checkbox check end */
			
			status = Activitydao.getActivitystatus( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			activityname = Activitydao.getActivityname( Activity_Id , getDataSource( request , "ilexnewDB" ) );
			
			
			request.setAttribute( "act_status" , status );
			request.setAttribute( "activityname" , activityname );
			
			request.setAttribute( "Size" , new Integer ( resource_size ) );
			request.setAttribute( "freightresourcelist" , freightresourcelist );
			
			request.setAttribute( "refresh" , "true" );
			request.setAttribute( "Activity_Id" , Activity_Id );
		
		return( mapping.findForward( "freighttabularpage" ) );
	}
	
	
	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "pmresF_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "pmresF_sort" );
		return queryclause;
	}
}
