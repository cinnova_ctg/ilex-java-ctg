package com.mind.actions.PM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.TravelEditBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.PM.TravelEditForm;

public class TravelEditAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		TravelEditForm traveleditform = (TravelEditForm) form;

		NameId idname = null;
		String from = "";
		String jobid = "";

		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		TravelEditBean travelEditBean = new TravelEditBean();

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "Resource",
				"Resource Edit", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("Activity_Id") != null) {
			traveleditform.setActivity_Id(request.getParameter("Activity_Id"));
		}

		if (request.getParameter("Travel_Id") != null) {
			traveleditform.setTravelid(request.getParameter("Travel_Id"));
		}

		if (request.getParameter("from") != null
				&& request.getParameter("from") != "") {
			jobid = request.getParameter("jobid");
			from = request.getParameter("from");
			traveleditform.setFromflag(request.getParameter("from"));
			traveleditform.setDashboardid(request.getParameter("jobid"));

			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		// if( request.getParameter( "flag" ) != null )
		// {
		// traveleditform .setFlag( request.getParameter( "flag" ));
		// }

		if (request.getParameter("ref") != null) {
			traveleditform.setChkaddendum(request.getParameter("ref"));

		}

		idname = Activitydao.getIdName(traveleditform.getActivity_Id(),
				"Resource", "Appendix", getDataSource(request, "ilexnewDB"));
		traveleditform.setAppendixId(idname.getId());
		traveleditform.setAppendixName(idname.getName());
		traveleditform.setJobName(Jobdao.getoplevelname(
				traveleditform.getActivity_Id(), "Resource",
				getDataSource(request, "ilexnewDB")));
		traveleditform.setJobId(Activitydao.getJobid(
				getDataSource(request, "ilexnewDB"),
				traveleditform.getActivity_Id()));

		idname = Activitydao.getIdName(traveleditform.getJobId(), "Activity",
				"MSA", getDataSource(request, "ilexnewDB"));
		traveleditform.setMsaName(idname.getName());
		traveleditform.setMsaId(idname.getId());
		traveleditform.setActivityName(Activitydao.getActivityname(
				traveleditform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));

		if (traveleditform.getChkaddendum().equals("View")) {
			traveleditform.setTraveltype("T");
		}
		if (traveleditform.getChkaddendum().equals("Viewinscope")) {
			traveleditform.setTraveltype("IT");
		}

		if (traveleditform.getSave() != null) {
			BeanUtils.copyProperties(travelEditBean, traveleditform);
			int updateflagTravel = Resourcedao
					.updatetravelresourceNew(travelEditBean, userid,
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("updateflagTravel", "" + updateflagTravel);
			request.setAttribute("from", traveleditform.getFromflag());
			request.setAttribute("jobid", traveleditform.getDashboardid());
			return (mapping.findForward("resourcelistpage"));
		} else {
			BeanUtils.copyProperties(travelEditBean, traveleditform);
			Resourcedao.getTravelResourceEdit(travelEditBean, traveleditform
					.getTraveltype(), "", request.getSession().getId(),
					"listpage", getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(traveleditform, travelEditBean);
		}

		// For the Travel Menu
		String travelType = "";
		String travelStatus = "";
		String travelStatusList = "";

		if (traveleditform.getRef().equalsIgnoreCase("View"))
			travelType = "T";
		else
			travelType = "IT";

		travelStatus = Resourcedao.getResourceStatus(
				traveleditform.getActivity_Id(), traveleditform.getTravelid(),
				travelType, request.getSession().getId(),
				getDataSource(request, "ilexnewDB"));

		if (traveleditform.getRef().equalsIgnoreCase("View")) {
			request.setAttribute("chkaddendum", "View");
			request.setAttribute("chkdetailactivity", "detailactivity");

			if (request.getParameter("from") != null)
				travelStatusList = Menu.getStatus("jobdashboard_resT",
						travelStatus.charAt(0), from,
						request.getParameter("jobid"), "View",
						traveleditform.getTravelid(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				travelStatusList = Menu.getStatus("pm_resT",
						travelStatus.charAt(0), "", "", "",
						traveleditform.getTravelid(), userid,
						getDataSource(request, "ilexnewDB"));

		}

		if (traveleditform.getRef().equalsIgnoreCase("Viewinscope")) {
			request.setAttribute("chkaddendum", "Viewinscope");
			request.setAttribute("chkdetailactivity", "inscopedetailactivity");

			if (request.getParameter("from") != null)
				travelStatusList = Menu.getStatus("jobdashboard_resT",
						travelStatus.charAt(0), from,
						request.getParameter("jobid"), "Viewinscope",
						traveleditform.getTravelid(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				travelStatusList = Menu.getStatus("jobdashboard_resT",
						travelStatus.charAt(0), "", "", "Viewinscope",
						traveleditform.getTravelid(), userid,
						getDataSource(request, "ilexnewDB"));
		}

		request.setAttribute("Activity_Id", traveleditform.getActivity_Id());
		request.setAttribute("Travel_Id", traveleditform.getTravelid());
		request.setAttribute("commonResourceStatus", travelStatus);
		request.setAttribute("travelStatusList", travelStatusList);
		request.setAttribute("checkingRef", traveleditform.getRef());
		request.setAttribute("addendum_id", "0");

		/*
		 * System.out.println("Activity_Id ::: " +
		 * traveleditform.getActivity_Id()); System.out.println("Travel_Id ::: "
		 * + traveleditform.getTravelid());
		 * System.out.println("Travel Status ::: " + travelStatus);
		 * System.out.println("Travel Status list ::: " + travelStatusList);
		 * System.out.println("Addendum Id ::: " + "0");
		 * System.out.println("From  ::::" + from );
		 * System.out.println("job id ::::" + jobid );
		 */

		// End of the Menu Code for Resource Travel

		return (mapping.findForward("traveltabularedit"));
	}

}
