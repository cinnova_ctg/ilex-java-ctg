/*This Action Class extends the base  Struts Action class to perform multiple
 * operations Save,Update, View etc.
 * 
 * Method Name: execute
 * Method Argument: 4 Argument ActionMapping,ActionForm,HttpServletRequest,HttpServletResponse
 * Method Description: MSA Tabular Form with existing MSAs is fetched , default combo are populated with values.
 * see:  MSAListForm class (com.mind.formbean.PM.MSAListForm)
 * 		 MSA class         (com.mind.dao.PM)
 * 15/05/2007  Modify the whole class to see only the list of all MSA's
 */

package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Logindao;
import com.mind.dao.PM.MSAdao;
import com.mind.formbean.PM.MSAListForm;
import com.mind.util.WebUtil;

public class MSAUpdateAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MSAListForm formdetail = (MSAListForm) form;
		boolean chkOtherOwner = false;
		int msa_size = 0;
		int ownerListSize = 0;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String loginusername = (String) session.getAttribute("username");
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		Codelist codes = new Codelist();
		ArrayList all_MSAs = new ArrayList();
		String sortqueryclause = "";
		String tempStatus = "";
		String tempOwner = "";
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		String ownerType = "msa";
		String msaid = "0";

		if (session.getAttribute("appendixSelectedOwners") != null
				|| session.getAttribute("appendixSelectedStatus") != null) {
			request.setAttribute("specialCase", "specialCase");
		}
		if (session.getAttribute("monthAppendix") != null
				|| session.getAttribute("weekAppendix") != null) {
			request.setAttribute("specialCase", "specialCase");
		}

		// Added to refersh the previous
		session.removeAttribute("appendixSelectedOwners");
		session.removeAttribute("appendixSelectedStatus");
		session.removeAttribute("appendixTempOwner");
		session.removeAttribute("appendixTempStatus");
		session.removeAttribute("appendixOtherCheck");
		session.removeAttribute("monthAppendix");
		session.removeAttribute("weekAppendix");
		session.removeAttribute("appSelectedOwners");
		session.removeAttribute("appSelectedStatus");
		// Appendix List state::::

		if (request.getParameter("home") != null) // Reset Button Pressed
		{

			formdetail.setSelectedStatus(null);
			formdetail.setSelectedOwners(null);
			formdetail.setPrevSelectedStatus("");
			formdetail.setPrevSelectedOwner("");
			formdetail.setOtherCheck(null);
			formdetail.setMonthWeekCheck(null);

			WebUtil.setValuesWhenReset(session);

			// if(session.getAttribute("selectedOwners")!=null) {
			// selectedOwner =
			// session.getAttribute("selectedOwners").toString();
			// formdetail.setSelectedOwners(session.getAttribute("tempOwner").toString().split(","));
			// }
			// if(session.getAttribute("selectedStatus")!=null) {
			// selectedStatus =
			// session.getAttribute("selectedStatus").toString();
			// formdetail.setSelectedStatus(session.getAttribute("tempStatus").toString().split(","));
			// }
		}

		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		if (request.getParameter("month") != null) {
			session.setAttribute("monthMSA", request.getParameter("month")
					.toString());
			monthMSA = request.getParameter("month").toString();
			session.removeAttribute("weekMSA");
			formdetail.setMonthWeekCheck("month");
		}

		if (request.getParameter("week") != null) {
			session.setAttribute("weekMSA", request.getParameter("week")
					.toString());
			weekMSA = request.getParameter("week").toString();
			session.removeAttribute("monthMSA");
			formdetail.setMonthWeekCheck("week");
		}

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
		// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "MSA", "MSA List",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getAttribute("editAction") != null) {
			request.setAttribute("editAction",
					request.getAttribute("editAction"));
		}

		if (request.getAttribute("editMSAFlag") != null)
			request.setAttribute("editMSAFlag",
					request.getAttribute("editMSAFlag"));

		if (session.getAttribute("msa_sort") != null)
			sortqueryclause = (String) session.getAttribute("msa_sort");

		if (formdetail.getOtherCheck() != null)
			session.setAttribute("otherCheck", formdetail.getOtherCheck()
					.toString());

		if (formdetail.getSelectedStatus() != null) {
			for (int i = 0; i < formdetail.getSelectedStatus().length; i++) {
				selectedStatus = selectedStatus + "," + "'"
						+ formdetail.getSelectedStatus(i) + "'";
				tempStatus = tempStatus + "," + formdetail.getSelectedStatus(i);
			}

			selectedStatus = selectedStatus.substring(1,
					selectedStatus.length());
			selectedStatus = "(" + selectedStatus + ")";
		}

		if (formdetail.getSelectedOwners() != null) {
			for (int j = 0; j < formdetail.getSelectedOwners().length; j++) {
				selectedOwner = selectedOwner + ","
						+ formdetail.getSelectedOwners(j);
				tempOwner = tempOwner + "," + formdetail.getSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if (selectedOwner.equals("0")) {
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" + selectedOwner + ")";
		}

		if (formdetail.getGo() != null && !formdetail.getGo().equals("")) {

			session.setAttribute("selectedOwners", selectedOwner);
			session.setAttribute("MSASelectedOwners", selectedOwner);
			session.setAttribute("tempOwner", tempOwner);
			session.setAttribute("selectedStatus", selectedStatus);
			session.setAttribute("MSASelectedStatus", selectedStatus);
			session.setAttribute("tempStatus", tempStatus);

			if (formdetail.getMonthWeekCheck() != null) {
				if (formdetail.getMonthWeekCheck().equals("week")) {
					session.removeAttribute("monthMSA");
					session.setAttribute("weekMSA", "0");
				}
				if (formdetail.getMonthWeekCheck().equals("month")) {
					session.removeAttribute("weekMSA");
					session.setAttribute("monthMSA", "0");
				}
				if (formdetail.getMonthWeekCheck().equals("clear")) {
					session.removeAttribute("monthMSA");
					session.removeAttribute("weekMSA");
					monthMSA = null;
					weekMSA = null;
				}

			}

			if (session.getAttribute("monthMSA") != null) {
				session.removeAttribute("weekMSA");
				monthMSA = session.getAttribute("monthMSA").toString();
			}
			if (session.getAttribute("weekMSA") != null) {
				session.removeAttribute("monthMSA");
				weekMSA = session.getAttribute("weekMSA").toString();
			}

		} else {
			if (session.getAttribute("selectedOwners") != null) {
				selectedOwner = session.getAttribute("selectedOwners")
						.toString();
				formdetail.setSelectedOwners(session.getAttribute("tempOwner")
						.toString().split(","));
			}
			if (session.getAttribute("selectedStatus") != null) {
				selectedStatus = session.getAttribute("selectedStatus")
						.toString();
				formdetail.setSelectedStatus(session.getAttribute("tempStatus")
						.toString().split(","));
			}
			if (session.getAttribute("otherCheck") != null)
				formdetail.setOtherCheck(session.getAttribute("otherCheck")
						.toString());
		}
		if (session.getAttribute("monthMSA") != null) {
			session.removeAttribute("weekMSA");
			monthMSA = session.getAttribute("monthMSA").toString();
			formdetail.setMonthWeekCheck("month");
		}
		if (session.getAttribute("weekMSA") != null) {
			session.removeAttribute("monthMSA");
			weekMSA = session.getAttribute("weekMSA").toString();
			formdetail.setMonthWeekCheck("week");
		}

		if (!chkOtherOwner) {
			formdetail.setPrevSelectedStatus(selectedStatus);
			formdetail.setPrevSelectedOwner(selectedOwner);
		}

		ownerList = MSAdao.getOwnerList(loginuserid,
				formdetail.getOtherCheck(), ownerType, msaid,
				getDataSource(request, "ilexnewDB"));
		statusList = MSAdao.getStatusList("pm_msa",
				getDataSource(request, "ilexnewDB"));
		String whereClause = "";
		if (request.getParameter("filter") != null
				&& !request.getParameter("filter").equals("")) {
			String[] filteredCondition;
			if (!request.getParameter("filter").equals("T-Z")) {
				filteredCondition = request.getParameter("filter").toString()
						.split("-");
				whereClause = " and upper(substring(lo_ot_name ,1,1)) between '"
						+ filteredCondition[0]
						+ "' and '"
						+ filteredCondition[1] + "'";
			} else {
				filteredCondition = request.getParameter("filter").toString()
						.split("-");
				whereClause = " and upper(substring(lo_ot_name ,1,1)) NOT between 'A' and 'S'";
			}
		} else {
			whereClause = " and upper(substring(lo_ot_name ,1,1)) between 'A' and 'D'";
		}
		all_MSAs = MSAdao.getMSAList(getDataSource(request, "ilexnewDB"),
				sortqueryclause, monthMSA, weekMSA,
				formdetail.getPrevSelectedStatus(),
				formdetail.getPrevSelectedOwner(), whereClause);

		if (all_MSAs.size() > 0) {
			codes.setMsalist(all_MSAs);
			msa_size = all_MSAs.size();
		}

		if (ownerList.size() > 0)
			ownerListSize = ownerList.size();

		request.setAttribute(
				"roleExists",
				Logindao.checkRoleForUser(loginuserid, "ESA",
						getDataSource(request, "ilexnewDB")));

		request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", statusList);
		request.setAttribute("ownerlist", ownerList);
		request.setAttribute("ownerListSize", ownerListSize + "");
		request.setAttribute("Size", new Integer(msa_size));
		request.setAttribute("codeslist", codes);

		return (mapping.findForward("success"));
	}
}
