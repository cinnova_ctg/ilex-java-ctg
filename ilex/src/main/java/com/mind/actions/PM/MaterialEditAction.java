package com.mind.actions.PM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.MaterialEditBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.PM.MaterialEditForm;

public class MaterialEditAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MaterialEditForm materialeditform = (MaterialEditForm) form;
		NameId idname = null;
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		String from = "";
		String jobid = "";
		MaterialEditBean materialEditBean = new MaterialEditBean();

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "Resource",
				"Resource Edit", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		if (request.getParameter("Activity_Id") != null) {
			materialeditform
					.setActivity_Id(request.getParameter("Activity_Id"));
		}

		if (request.getParameter("Material_Id") != null) {
			materialeditform.setMaterialid(request.getParameter("Material_Id"));
		}

		// if( request.getParameter( "flag" ) != null ){
		// materialeditform .setFlag( request.getParameter( "flag" ));
		// }

		if (request.getParameter("from") != null
				&& request.getParameter("from") != "") {
			from = request.getParameter("from");
			jobid = request.getParameter("jobid");
			materialeditform.setFromflag(request.getParameter("from"));
			materialeditform.setDashboardid(request.getParameter("jobid"));

			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("ref") != null) {
			materialeditform.setChkaddendum(request.getParameter("ref"));
			materialeditform.setRef(request.getParameter("ref"));
		}

		idname = Activitydao.getIdName(materialeditform.getActivity_Id(),
				"Resource", "Appendix", getDataSource(request, "ilexnewDB"));
		materialeditform.setAppendixId(idname.getId());
		materialeditform.setAppendixName(idname.getName());
		materialeditform.setJobName(Jobdao.getoplevelname(
				materialeditform.getActivity_Id(), "Resource",
				getDataSource(request, "ilexnewDB")));
		materialeditform.setJobId(Activitydao.getJobid(
				getDataSource(request, "ilexnewDB"),
				materialeditform.getActivity_Id()));

		idname = Activitydao.getIdName(materialeditform.getJobId(), "Activity",
				"MSA", getDataSource(request, "ilexnewDB"));
		materialeditform.setMsaName(idname.getName());
		materialeditform.setMsaId(idname.getId());
		materialeditform.setActivityName(Activitydao.getActivityname(
				materialeditform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));

		if (materialeditform.getChkaddendum().equals("View")) {
			materialeditform.setMaterialType("M");
		}
		if (materialeditform.getChkaddendum().equals("Viewinscope")) {
			materialeditform.setMaterialType("IM");
		}

		if (materialeditform.getSave() != null) {
			BeanUtils.copyProperties(materialEditBean, materialeditform);
			int updateflagMaterial = Resourcedao.updatematerialresourceNew(
					materialEditBean, userid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("updateflagMaterial", "" + updateflagMaterial);
			request.setAttribute("from", materialeditform.getFromflag());
			request.setAttribute("jobid", materialeditform.getDashboardid());
			return (mapping.findForward("resourcelistpage"));
		} else {
			BeanUtils.copyProperties(materialEditBean, materialeditform);
			Resourcedao.getMaterialResourceEdit(materialEditBean, "M", "",
					request.getSession().getId(), "listpage",
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(materialeditform, materialEditBean);
		}

		// For the Material Menu
		String materialType = "";
		String materialStatus = "";
		String materialStatusList = "";

		if (materialeditform.getRef().equalsIgnoreCase("View"))
			materialType = "M";
		else
			materialType = "IM";

		materialStatus = Resourcedao.getResourceStatus(materialeditform
				.getActivity_Id(), materialeditform.getMaterialid(),
				materialType, request.getSession().getId(),
				getDataSource(request, "ilexnewDB"));

		if (materialeditform.getRef().equalsIgnoreCase("View")) {
			request.setAttribute("chkaddendum", "View");
			request.setAttribute("chkdetailactivity", "detailactivity");

			if (request.getParameter("from") != null)
				materialStatusList = Menu.getStatus("jobdashboard_resM",
						materialStatus.charAt(0), from,
						request.getParameter("jobid"), "View",
						materialeditform.getMaterialid(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				materialStatusList = Menu.getStatus("pm_resM",
						materialStatus.charAt(0), "", "", "",
						materialeditform.getMaterialid(), userid,
						getDataSource(request, "ilexnewDB"));

		}

		if (materialeditform.getRef().equalsIgnoreCase("Viewinscope")) {
			request.setAttribute("chkaddendum", "Viewinscope");
			request.setAttribute("chkdetailactivity", "inscopedetailactivity");

			if (request.getParameter("from") != null)
				materialStatusList = Menu.getStatus("jobdashboard_resM",
						materialStatus.charAt(0), from,
						request.getParameter("jobid"), "Viewinscope",
						materialeditform.getMaterialid(), userid,
						getDataSource(request, "ilexnewDB"));
			else
				materialStatusList = Menu.getStatus("jobdashboard_resM",
						materialStatus.charAt(0), "", "", "Viewinscope",
						materialeditform.getMaterialid(), userid,
						getDataSource(request, "ilexnewDB"));
		}

		request.setAttribute("Activity_Id", materialeditform.getActivity_Id());
		request.setAttribute("Material_Id", materialeditform.getMaterialid());
		request.setAttribute("commonResourceStatus", materialStatus);
		request.setAttribute("materialStatusList", materialStatusList);
		request.setAttribute("checkingRef", materialeditform.getRef());
		request.setAttribute("addendum_id", "0");

		/*
		 * System.out.println("Activity_Id ::: " +
		 * materialeditform.getActivity_Id());
		 * System.out.println("Material_Id ::: " + materialeditform
		 * .getMaterialid()); System.out.println("Material Status ::: " +
		 * materialStatus); System.out.println("Material Status list ::: " +
		 * materialStatusList); System.out.println("Addendum Id ::: " + "0");
		 * System.out.println("From  ::::" + from );
		 * System.out.println("job id ::::" + jobid );
		 */

		// End of the Menu Code for Resource Material

		return (mapping.findForward("materialtabularedit"));
	}

}
