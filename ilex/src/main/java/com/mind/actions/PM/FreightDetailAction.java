package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.FreightResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.AM.FreightResourceDetailForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class FreightDetailAction extends com.mind.common.IlexDispatchAction {
	public ActionForward View(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		FreightResourceDetailForm freightresourcedetailform = (FreightResourceDetailForm) form;

		FreightResource freightresource = new FreightResource();
		String freight_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Freight_Id") != null) {
			freight_Id = request.getParameter("Freight_Id");
		}

		if (request.getAttribute("Freight_Id") != null) {
			freight_Id = (String) request.getAttribute("Freight_Id");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}
		if (request.getParameter("Activity_Id") != null) {
			freightresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		freightresourcedetailform.setFreightid(freight_Id);
		request.setAttribute("Freight_Id",
				freightresourcedetailform.getFreightid());

		// ArrayList freightresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "F" , freightresourcedetailform.getFreightid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList freightresourcelist = Resourcedao.getResourcetabularlist(
				freightresourcedetailform.getActivity_Id(), "F",
				freightresourcedetailform.getFreightid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		freightresource = (FreightResource) freightresourcelist.get(0);

		freightresourcedetailform.setActivity_Id(freightresource
				.getActivity_Id());
		request.setAttribute("Activity_Id",
				freightresourcedetailform.getActivity_Id());

		freightresourcedetailform.setFreightid(freightresource.getFreightid());
		freightresourcedetailform.setFreightname(freightresource
				.getFreightname());
		freightresourcedetailform.setFreighttype(freightresource
				.getFreighttype());
		freightresourcedetailform.setMinimumquantity(freightresource
				.getMinimumquantity());
		freightresourcedetailform.setCnspartnumber(freightresource
				.getCnspartnumber());
		freightresourcedetailform.setEstimatedtotalcost(freightresource
				.getEstimatedtotalcost());
		freightresourcedetailform.setEstimatedunitcost(freightresource
				.getEstimatedunitcost());

		freightresourcedetailform.setPriceextended(freightresource
				.getPriceextended());
		freightresourcedetailform.setPriceunit(freightresource.getPriceunit());
		freightresourcedetailform.setProformamargin(freightresource
				.getProformamargin());
		freightresourcedetailform.setQuantity(freightresource.getQuantity());
		freightresourcedetailform.setSellablequantity(freightresource
				.getSellablequantity());
		freightresourcedetailform.setStatus(freightresource.getStatus());

		freightresourcedetailform.setChkaddendum("View");
		freightresourcedetailform.setChkdetailactivity("detailactivity");

		freightresourcedetailform.setActivityname(Activitydao.getActivityname(
				freightresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setMsa_id(idname.getId());
		freightresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setAppendix_id(idname.getId());
		freightresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setJob_id(idname.getId());
		freightresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resF",
					freightresourcedetailform.getStatus().charAt(0), from,
					request.getParameter("jobid"), "View",
					freightresourcedetailform.getFreightid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("pm_resF", freightresourcedetailform
					.getStatus().charAt(0), "", "", "",
					freightresourcedetailform.getFreightid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		}

		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				freightresourcedetailform.getFreightid(), "pm_resF");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				freightresourcedetailform.getFreightid(), "pm_resF");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				freightresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				freightresourcedetailform.getChkaddendum());
		// End

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);

		return mapping.findForward("freightdetailpage");
	}

	public ActionForward Viewinscope(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		FreightResourceDetailForm freightresourcedetailform = (FreightResourceDetailForm) form;

		FreightResource freightresource = new FreightResource();
		String freight_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;
		String resourceType = "IF";
		Map<String, Object> map;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Freight_Id") != null) {
			freight_Id = request.getParameter("Freight_Id");
		}

		if (request.getAttribute("Freight_Id") != null) {
			freight_Id = (String) request.getAttribute("Freight_Id");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}
		if (request.getParameter("Activity_Id") != null) {
			freightresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		freightresourcedetailform.setFreightid(freight_Id);
		request.setAttribute("Freight_Id",
				freightresourcedetailform.getFreightid());

		if (request.getParameter("jobType") != null
				&& request.getParameter("jobType").equals("Newjob")) {
			resourceType = "F";
		}

		// ArrayList freightresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "IF" , freightresourcedetailform.getFreightid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList freightresourcelist = Resourcedao.getResourcetabularlist(
				freightresourcedetailform.getActivity_Id(), resourceType,
				freightresourcedetailform.getFreightid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		freightresource = (FreightResource) freightresourcelist.get(0);

		freightresourcedetailform.setActivity_Id(freightresource
				.getActivity_Id());
		request.setAttribute("Activity_Id",
				freightresourcedetailform.getActivity_Id());

		freightresourcedetailform.setFreightid(freightresource.getFreightid());
		freightresourcedetailform.setFreightname(freightresource
				.getFreightname());
		freightresourcedetailform.setFreighttype(freightresource
				.getFreighttype());
		freightresourcedetailform.setMinimumquantity(freightresource
				.getMinimumquantity());
		freightresourcedetailform.setCnspartnumber(freightresource
				.getCnspartnumber());
		freightresourcedetailform.setEstimatedtotalcost(freightresource
				.getEstimatedtotalcost());
		freightresourcedetailform.setEstimatedunitcost(freightresource
				.getEstimatedunitcost());

		freightresourcedetailform.setPriceextended(freightresource
				.getPriceextended());
		freightresourcedetailform.setPriceunit(freightresource.getPriceunit());
		freightresourcedetailform.setProformamargin(freightresource
				.getProformamargin());
		freightresourcedetailform.setQuantity(freightresource.getQuantity());
		freightresourcedetailform.setSellablequantity(freightresource
				.getSellablequantity());
		freightresourcedetailform.setStatus(freightresource.getStatus());

		freightresourcedetailform.setChkaddendum("Viewinscope");
		freightresourcedetailform.setChkdetailactivity("inscopedetailactivity");
		freightresourcedetailform.setActivityname(Activitydao.getActivityname(
				freightresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setMsa_id(idname.getId());
		freightresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setAppendix_id(idname.getId());
		freightresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setJob_id(idname.getId());
		freightresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resF",
					freightresourcedetailform.getStatus().charAt(0), from,
					request.getParameter("jobid"), "Viewinscope",
					freightresourcedetailform.getFreightid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("jobdashboard_resF",
					freightresourcedetailform.getStatus().charAt(0), "", "",
					"Viewinscope", freightresourcedetailform.getFreightid(),
					loginuserid, getDataSource(request, "ilexnewDB"));
		}
		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				freightresourcedetailform.getFreightid(), "pm_resF");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				freightresourcedetailform.getFreightid(), "pm_resF");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				freightresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				freightresourcedetailform.getChkaddendum());
		// End

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);
		if (request.getParameter("from") != null
				&& !request.getParameter("from").trim().equalsIgnoreCase("")
				&& request.getParameter("from").trim()
						.equalsIgnoreCase("jobdashboard")) {
			if (request.getParameter("jobid") != null
					&& !request.getParameter("jobid").trim()
							.equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(request.getParameter("jobid"),
						loginuserid, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}
		return mapping.findForward("freightdetailpage");
	}

	public ActionForward ViewAddendum(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		FreightResourceDetailForm freightresourcedetailform = (FreightResourceDetailForm) form;

		FreightResource freightresource = new FreightResource();
		String freight_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Freight_Id") != null) {
			freight_Id = request.getParameter("Freight_Id");
		}

		if (request.getAttribute("Freight_Id") != null) {
			freight_Id = (String) request.getAttribute("Freight_Id");
		}

		if (request.getParameter("addendum_id") != null) {
			freightresourcedetailform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getAttribute("addendum_id") != null) {
			freightresourcedetailform.setAddendum_id(request.getAttribute(
					"addendum_id").toString());
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}
		if (request.getParameter("Activity_Id") != null) {
			freightresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		freightresourcedetailform.setFreightid(freight_Id);
		request.setAttribute("Freight_Id",
				freightresourcedetailform.getFreightid());

		// ArrayList freightresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "AF" , freightresourcedetailform.getFreightid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList freightresourcelist = Resourcedao.getResourcetabularlist(
				freightresourcedetailform.getActivity_Id(), "AF",
				freightresourcedetailform.getFreightid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		freightresource = (FreightResource) freightresourcelist.get(0);

		freightresourcedetailform.setActivity_Id(freightresource
				.getActivity_Id());
		request.setAttribute("Activity_Id",
				freightresourcedetailform.getActivity_Id());

		freightresourcedetailform.setFreightid(freightresource.getFreightid());
		freightresourcedetailform.setFreightname(freightresource
				.getFreightname());
		freightresourcedetailform.setFreighttype(freightresource
				.getFreighttype());
		freightresourcedetailform.setMinimumquantity(freightresource
				.getMinimumquantity());
		freightresourcedetailform.setCnspartnumber(freightresource
				.getCnspartnumber());
		freightresourcedetailform.setEstimatedtotalcost(freightresource
				.getEstimatedtotalcost());
		freightresourcedetailform.setEstimatedunitcost(freightresource
				.getEstimatedunitcost());

		freightresourcedetailform.setPriceextended(freightresource
				.getPriceextended());
		freightresourcedetailform.setPriceunit(freightresource.getPriceunit());
		freightresourcedetailform.setProformamargin(freightresource
				.getProformamargin());
		freightresourcedetailform.setQuantity(freightresource.getQuantity());
		freightresourcedetailform.setSellablequantity(freightresource
				.getSellablequantity());
		freightresourcedetailform.setStatus(freightresource.getStatus());

		freightresourcedetailform.setChkaddendum("ViewAddendum");
		freightresourcedetailform
				.setChkdetailactivity("detailaddendumactivity");
		freightresourcedetailform.setActivityname(Activitydao.getActivityname(
				freightresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setMsa_id(idname.getId());
		freightresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setAppendix_id(idname.getId());
		freightresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setJob_id(idname.getId());
		freightresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resF",
					freightresourcedetailform.getStatus().charAt(0), from,
					request.getParameter("jobid"), "ViewAddendum",
					freightresourcedetailform.getFreightid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			// list = Menu.getStatus( "pm_resF" ,
			// freightresourcedetailform.getStatus().charAt( 0 ) , "" , "" , ""
			// , freightresourcedetailform.getFreightid() , loginuserid ,
			// getDataSource( request , "ilexnewDB" ) );

			if (freightresourcedetailform.getStatus().equals("Draft")) {
				list = "\"Approved\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ freightresourcedetailform.getAddendum_id()
						+ "&type=pm_resF&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
						+ freightresourcedetailform.getFreightid()
						+ "&Status=A\"" + "," + "0";
			}

			if (freightresourcedetailform.getStatus().equals("Approved")) {
				list = "\"Draft\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ freightresourcedetailform.getAddendum_id()
						+ "&type=pm_resF&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
						+ freightresourcedetailform.getFreightid()
						+ "&Status=D\"" + "," + "0";
			}

		}

		/*
		 * if( freightresourcedetailform.getStatus().equals( "Draft" ) ) { list
		 * = "\"Approved\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +freightresourcedetailform.getAddendum_id()+
		 * "&type=pm_resF&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
		 * +freightresourcedetailform.getFreightid()+"&Status=A\"" + "," + "0";
		 * }
		 * 
		 * if( freightresourcedetailform.getStatus().equals( "Approved" ) ) {
		 * list = "\"Draft\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +freightresourcedetailform.getAddendum_id()+
		 * "&type=pm_resF&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
		 * +freightresourcedetailform.getFreightid()+"&Status=D\"" + "," + "0";
		 * }
		 */

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				freightresourcedetailform.getFreightid(), "pm_resF");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				freightresourcedetailform.getFreightid(), "pm_resF");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);

		request.setAttribute("list", list);

		return mapping.findForward("freightdetailpage");
	}

	public ActionForward ViewChangeorder(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		FreightResourceDetailForm freightresourcedetailform = (FreightResourceDetailForm) form;
		NameId idname = null;
		FreightResource freightresource = new FreightResource();
		String freight_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";

		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Freight_Id") != null) {
			freight_Id = request.getParameter("Freight_Id");
		}

		if (request.getAttribute("Freight_Id") != null) {
			freight_Id = (String) request.getAttribute("Freight_Id");
		}

		if (request.getParameter("addendum_id") != null) {
			freightresourcedetailform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getAttribute("addendum_id") != null) {
			freightresourcedetailform.setAddendum_id(request.getAttribute(
					"addendum_id").toString());
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			freightresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		freightresourcedetailform.setFreightid(freight_Id);
		request.setAttribute("Freight_Id",
				freightresourcedetailform.getFreightid());

		// ArrayList freightresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "CF" , freightresourcedetailform.getFreightid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList freightresourcelist = Resourcedao.getResourcetabularlist(
				freightresourcedetailform.getActivity_Id(), "CF",
				freightresourcedetailform.getFreightid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		freightresource = (FreightResource) freightresourcelist.get(0);

		freightresourcedetailform.setActivity_Id(freightresource
				.getActivity_Id());
		request.setAttribute("Activity_Id",
				freightresourcedetailform.getActivity_Id());

		freightresourcedetailform.setFreightid(freightresource.getFreightid());
		freightresourcedetailform.setFreightname(freightresource
				.getFreightname());
		freightresourcedetailform.setFreighttype(freightresource
				.getFreighttype());
		freightresourcedetailform.setMinimumquantity(freightresource
				.getMinimumquantity());
		freightresourcedetailform.setCnspartnumber(freightresource
				.getCnspartnumber());
		freightresourcedetailform.setEstimatedtotalcost(freightresource
				.getEstimatedtotalcost());
		freightresourcedetailform.setEstimatedunitcost(freightresource
				.getEstimatedunitcost());

		freightresourcedetailform.setPriceextended(freightresource
				.getPriceextended());
		freightresourcedetailform.setPriceunit(freightresource.getPriceunit());
		freightresourcedetailform.setProformamargin(freightresource
				.getProformamargin());
		freightresourcedetailform.setQuantity(freightresource.getQuantity());
		freightresourcedetailform.setSellablequantity(freightresource
				.getSellablequantity());
		freightresourcedetailform.setStatus(freightresource.getStatus());

		freightresourcedetailform.setChkaddendum("ViewChangeorder");
		freightresourcedetailform
				.setChkdetailactivity("detailchangeorderactivity");
		freightresourcedetailform.setActivityname(Activitydao.getActivityname(
				freightresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setMsa_id(idname.getId());
		freightresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setAppendix_id(idname.getId());
		freightresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				freightresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		freightresourcedetailform.setJob_id(idname.getId());
		freightresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resF",
					freightresourcedetailform.getStatus().charAt(0), from,
					request.getParameter("jobid"), "ViewChangeorder",
					freightresourcedetailform.getFreightid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("pm_resF", freightresourcedetailform
					.getStatus().charAt(0), "", "", "",
					freightresourcedetailform.getFreightid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);

			if (freightresourcedetailform.getStatus().equals("Draft")) {
				list = "\"Approved\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ freightresourcedetailform.getAddendum_id()
						+ "&type=pm_resF&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
						+ freightresourcedetailform.getFreightid()
						+ "&Status=A\"" + "," + "0";
			}

			if (freightresourcedetailform.getStatus().equals("Approved")) {
				list = "\"Draft\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ freightresourcedetailform.getAddendum_id()
						+ "&type=pm_resF&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
						+ freightresourcedetailform.getFreightid()
						+ "&Status=D\"" + "," + "0";
			}
		}

		/*
		 * if( freightresourcedetailform.getStatus().equals( "Draft" ) ) { list
		 * = "\"Approved\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +freightresourcedetailform.getAddendum_id()+
		 * "&type=pm_resF&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +freightresourcedetailform.getFreightid()+"&Status=A\"" + "," + "0";
		 * }
		 * 
		 * if( freightresourcedetailform.getStatus().equals( "Approved" ) ) {
		 * list = "\"Draft\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +freightresourcedetailform.getAddendum_id()+
		 * "&type=pm_resF&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +freightresourcedetailform.getFreightid()+"&Status=D\"" + "," + "0";
		 * }
		 */

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				freightresourcedetailform.getFreightid(), "pm_resF");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				freightresourcedetailform.getFreightid(), "pm_resF");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				freightresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				freightresourcedetailform.getChkaddendum());
		// End

		request.setAttribute("list", list);

		return mapping.findForward("freightdetailpage");
	}

}
