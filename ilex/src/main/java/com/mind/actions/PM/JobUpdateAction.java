package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.formbean.PM.JobListForm;


public class JobUpdateAction extends com.mind.common.IlexAction {

	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		JobListForm joblistform = ( JobListForm ) form;
		ArrayList all_jobs = new ArrayList();
		String Appendix_Id = "";
		int job_size = 0;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String loginusername = ( String ) session.getAttribute( "username" );
		Codelist codes = new Codelist(); 
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
			if( !Authenticationdao.getPageSecurity( loginuserid , "Appendix" , "View Jobs" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}

		if(request.getParameter("addendum_id")!=null)
			joblistform.setAddendum_id(request.getParameter("addendum_id"));
		else
			joblistform.setAddendum_id("0");
		
		if(request.getAttribute("viewjobtype")!=null)
			joblistform.setViewjobtype(request.getAttribute("viewjobtype").toString());
		
		if(request.getAttribute("ownerId")!=null)
			joblistform.setOwnerId(request.getAttribute("ownerId").toString());
		
		if(request.getParameter("ref")!=null){
			
			if(request.getParameter("ref").equalsIgnoreCase("View")||request.getParameter("ref").equalsIgnoreCase("Newjob") )
				joblistform.setChkaddendum("detailjob");
			else if(request.getParameter("ref").equalsIgnoreCase("InScopeView"))
				joblistform.setChkaddendum("inscopejob");
			else
				joblistform.setChkaddendum(request.getParameter("ref"));
			
		}else{
				
				if(request.getAttribute("ref")!=null && request.getAttribute("ref").toString().equalsIgnoreCase("Newjob"))
					joblistform.setChkaddendum("detailjob");
				else
					joblistform.setChkaddendum(request.getAttribute("ref").toString());
			}
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
			if( !Authenticationdao.getPageSecurity( loginuserid , "MSA" , "MSA List" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		
		if( request.getParameter( "Appendix_Id" ) != null )
		{
			Appendix_Id = request.getParameter( "Appendix_Id" );
			joblistform.setAppendix_Id( Appendix_Id );
		}
		if( request.getAttribute( "Appendix_Id" ) != null )
		{
			Appendix_Id = request.getAttribute( "Appendix_Id" ).toString();
			joblistform.setAppendix_Id( Appendix_Id );
		}
		
			sortqueryclause =  getSortQueryClause( sortqueryclause , session );
					
			if(joblistform.getChkaddendum().equalsIgnoreCase("detailjob"))
				all_jobs = Jobdao.getJobList( getDataSource( request , "ilexnewDB" ) ,joblistform.getAppendix_Id(), sortqueryclause,"%" , "%" );
			else
				all_jobs = Jobdao.getJobList( getDataSource( request , "ilexnewDB" ) , joblistform.getAppendix_Id() , sortqueryclause, "IJ" , "%" );
			
			joblistform.setMsaname( Jobdao.getoplevelname(joblistform.getAppendix_Id() , "Job" , getDataSource( request , "ilexnewDB" )));
			joblistform.setAppendixname(Jobdao.getAppendixname(getDataSource(request,"ilexnewDB"),joblistform.getAppendix_Id()));
			joblistform.setMSA_Id(IMdao.getMSAId(joblistform.getAppendix_Id(), getDataSource(request,"ilexnewDB")));
		
			if( all_jobs.size() > 0 ){
				codes.setJoblist( all_jobs );
				job_size = all_jobs.size();
			}
			
			String appendixname = Jobdao.getAppendixname( getDataSource( request , "ilexnewDB" ) , Appendix_Id );
			joblistform.setAppendixtype( ViewList.getAppendixtypedesc( joblistform.getAppendix_Id() , getDataSource( request , "ilexnewDB" ) ) );
		
			if( request.getParameter( "addendumflag" ) != null )
				request.setAttribute( "addendumflag" , request.getParameter( "addendumflag" ).toString() );
			
			
			request.setAttribute( "appendixname" ,appendixname);
			request.setAttribute( "Size" , new Integer ( job_size ) );
			request.setAttribute( "codeslist" , codes );
			
		return mapping.findForward( "jobtabularpage" );
	}
	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "job_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "job_sort" );
		return queryclause;
	}	


}
