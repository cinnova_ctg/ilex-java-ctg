package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.pm.Activity;
import com.mind.bean.pm.ActivityListBean;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Addendumdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.AddendumJobName;
import com.mind.formbean.PM.ActivityListForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class CopyAddendumActivityAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActivityListForm activitylistform = (ActivityListForm) form;
		ActivityListBean activitylistBean = new ActivityListBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		ArrayList all_activities = null;
		String dataString = "";
		int returnval = -1;
		boolean checkforoverheadactivity = false;
		Map<String, Object> map;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Appendix_Id") != null) {
			activitylistform.setAppendixid(request.getParameter("Appendix_Id")
					.toString());
		}

		if (request.getParameter("Job_Id") != null) {
			activitylistform.setJob_Id(request.getParameter("Job_Id")
					.toString());
		}

		if (request.getParameter("from") != null) { // either PM or PRM
			activitylistform.setFromflag(request.getParameter("from")
					.toString());
		}

		if (activitylistform.getSave() != null) {
			if (activitylistform.getCheck() != null) {
				int checklength = activitylistform.getCheck().length;

				int[] index = new int[checklength];
				int k = 0;

				if (checklength > 0) {
					int length = activitylistform.getActivity_Id().length;
					for (int i = 0; i < checklength; i++) {
						for (int j = 0; j < length; j++) {
							if (activitylistform.getCheck(i).equals(
									activitylistform.getActivity_Id(j))) {
								index[k] = j;
								k++;
							}
						}
					}
				}

				if (checklength > 0) {
					for (int i = 0; i < index.length; i++) {
						dataString = dataString
								+ activitylistform.getActivity_Id(index[i])
								+ "-"
								+ activitylistform
										.getActivitytypecombo(index[i]) + "-"
								+ activitylistform.getQuantity(index[i]) + "-"
								+ activitylistform.getBidFlag(index[i]) + "-"
								+ activitylistform.getFlag(index[i]) + "~";
					}
					returnval = Addendumdao.CopyAddendumActivities(dataString,
							activitylistform.getJob_Id(),
							activitylistform.getFromflag(), loginuserid,
							getDataSource(request, "ilexnewDB"));
				}
				if (activitylistform.getFromflag().equalsIgnoreCase("PM")) {
					request.setAttribute("copyactivityflag", returnval + "");
					request.setAttribute("type", "pm");
					request.setAttribute("View", "View");
					request.setAttribute("Job_Id", activitylistform.getJob_Id());
					request.setAttribute("copyActivity", "copyActivity");
					return mapping.findForward("activitytabularpage");
				} else {
					request.setAttribute("copyactivityflag", returnval + "");
					request.setAttribute("jobid", activitylistform.getJob_Id());
					ActionForward fwd = new ActionForward();
					fwd.setPath("/JobDashboardAction.do?tabId="
							+ JobDashboardTabType.SETUP_TAB + "&jobid="
							+ activitylistform.getJob_Id() + "&isClicked=");
					return fwd;
					// return mapping.findForward( "jobdashboard" );
				}
			}

		} else {
			int activity_size = 0;
			String prevtempjob_id = "";
			ArrayList addendumjobnamelist = new ArrayList();
			AddendumJobName addendumjobname = null;

			all_activities = new ArrayList();
			BeanUtils.copyProperties(activitylistBean, activitylistform);
			Addendumdao.getLatestAddendumInfo(activitylistBean.getAppendixid(),
					activitylistBean, getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(activitylistform, activitylistBean);

			activitylistform.setJob_type(Jobdao.getJobtype(
					activitylistform.getJob_Id(),
					getDataSource(request, "ilexnewDB")));
			activitylistform.setJobName(Jobdao.getJobname(
					activitylistform.getJob_Id(),
					getDataSource(request, "ilexnewDB")));
			activitylistform.setAppendixname(Appendixdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					activitylistform.getAppendixid()));

			all_activities = Addendumdao.getActivityToCopyList(
					activitylistform.getAppendixid(),
					activitylistform.getJob_Id(),
					getDataSource(request, "ilexnewDB"));
			activitylistform.setActivity(ActivityLibrarydao.getActivitytype());

			checkforoverheadactivity = Activitydao.Checkdefaultjoboverhead(
					getDataSource(request, "ilexnewDB"),
					activitylistform.getJob_Id());

			if (all_activities.size() > 0) {
				for (int i = 0; i < all_activities.size(); i++) {
					if (!prevtempjob_id.equals(((Activity) all_activities
							.get(i)).getTemp_jobid())) {
						addendumjobname = new AddendumJobName();
						addendumjobname
								.setAddendumjobname(((Activity) all_activities
										.get(i)).getJob_name());
						addendumjobnamelist.add(addendumjobname);
					}
					prevtempjob_id = ((Activity) all_activities.get(i))
							.getTemp_jobid();
				}

			}

			if (all_activities.size() > 0) {

				if (checkforoverheadactivity) {
					for (int i = 0; i < all_activities.size(); i++) {
						if (((Activity) all_activities.get(i))
								.getActivitytypecombo().equals("V")) {
							all_activities.remove(i);
						}
					}
				}

				activity_size = all_activities.size();
				request.setAttribute("addendumjobnamelist", addendumjobnamelist);
				request.setAttribute("codes", all_activities);

			}

			if (request.getParameter("addendumflag") != null) {
				request.setAttribute("addendumflag",
						request.getParameter("addendumflag"));
			}
			request.setAttribute("Size", new Integer(activity_size));

			if (activitylistform.getJob_Id() != null
					&& !activitylistform.getJob_Id().trim()
							.equalsIgnoreCase("")) {
				request.setAttribute("tabStatus", "job");
				map = DatabaseUtilityDao.setMenu(activitylistform.getJob_Id(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
		}

		if (activitylistform.getFromflag().equals("PM")) {

			activitylistform.setAppendixname(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					activitylistform.getAppendixid()));
			activitylistform.setMsa_id(Activitydao.getmsaid(
					getDataSource(request, "ilexnewDB"),
					activitylistform.getJob_Id()));
			activitylistform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					activitylistform.getMsa_id()));
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					activitylistform.getJob_Id(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String jobStatusList = "";
			String appendixType = "";

			if (jobType1.equals("Default"))
				jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
						+ activitylistform.getJob_Id() + "&Status='D'\"" + ","
						+ "0";
			if (jobType1.equals("Newjob"))
				jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
						"", "", activitylistform.getJob_Id(), "", loginuserid,
						getDataSource(request, "ilexnewDB"));
			if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
				jobStatusList = "\"Approved\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ activitylistform.getJob_Id() + "&Status=A\"" + ","
						+ "0";
			if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
				jobStatusList = "\"Draft\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ activitylistform.getJob_Id() + "&Status=D\"" + ","
						+ "0";
			if (jobType1.equals("Addendum"))
				jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
						"", "", activitylistform.getJob_Id(), "", loginuserid,
						getDataSource(request, "ilexnewDB"));

			appendixType = ViewList.getAppendixtypedesc(
					activitylistform.getAppendixid(),
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("jobStatus", jobStatus);
			request.setAttribute("job_type", jobType1);
			request.setAttribute("appendixtype", appendixType);
			request.setAttribute("Job_Id", activitylistform.getJob_Id());

			request.setAttribute("Appendix_Id",
					activitylistform.getAppendixid());

			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("addendum_id", "0");

			if (jobType1.equals("Newjob") || jobType1.equals("Default")) {
				request.setAttribute("chkadd", "detailjob");
				request.setAttribute("chkaddendum", "detailjob");
				request.setAttribute("chkaddendumactivity", "View");
			}
			if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
				request.setAttribute("chkadd", "inscopejob");
				request.setAttribute("chkaddendum", "inscopejob");
				request.setAttribute("chkaddendumactivity", "inscopeactivity");
			}

		}
		return (mapping.findForward("success"));
	}
}
