package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.pm.SiteBean;
import com.mind.common.Util;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Job;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PM.SiteForm;

public class SiteAction extends com.mind.common.IlexDispatchAction {
	public ActionForward Add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SiteForm siteform = (SiteForm) form;
		ArrayList initialStateCategory = new ArrayList();
		String tempcat = "";
		String[] tempList = null;
		SiteBean siteBean = new SiteBean();

		Job job = new Job();
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		// for back button on site JSP when ,It comes from Manage Site of
		// Proposal Manager
		if (request.getParameter("fromProposalManag") != null) {
			request.setAttribute("fromType",
					request.getParameter(("fromProposalManag")));
			siteform.setFromType(request.getParameter("fromProposalManag"));
		}
		// for back button on site JSP when ,It comes from Manage Site of
		// Proposal Manager

		if (request.getParameter("Job_Id") != null) {
			siteform.setId2(request.getParameter("Job_Id"));
		}

		if (request.getParameter("Appendix_Id") != null) {
			siteform.setId1(request.getParameter("Appendix_Id"));
			siteform.setAppendixname(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), siteform.getId1()));
		}

		if (request.getParameter("Appendix_Id") != null
				&& !request.getParameter("Appendix_Id").equals("")) {
			siteform.setMsaid(IMdao.getMSAId(siteform.getId1(),
					getDataSource(request, "ilexnewDB")));
			siteform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					IMdao.getMSAId(siteform.getId1(),
							getDataSource(request, "ilexnewDB"))));
			siteform.setAppendixname(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					request.getParameter("Appendix_Id")));
		}

		if (request.getParameter("ref1") != null) {
			siteform.setRef1(request.getParameter("ref1"));
		}

		if (request.getParameter("addendum_id") != null) {
			siteform.setAddendum_id(request.getParameter("addendum_id"));
		}

		if (request.getParameter("nettype") != null) {
			siteform.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getParameter("viewjobtype") != null) {
			siteform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			siteform.setViewjobtype("A");
		}

		request.setAttribute("viewjobtype", siteform.getViewjobtype());

		if (request.getParameter("ownerId") != null) {
			siteform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			siteform.setOwnerId("%");
		}

		if (siteform.getSubmitPage() == null)
			siteform.setSubmitPage("");
		// if( siteform.getSite_name() != null && siteform.getRefersh()==null)

		if (siteform.getSubmitPage().equals("submitPage")) {
			BeanUtils.copyProperties(siteBean, siteform);
			Jobdao.addSite(siteBean, loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("Id1", siteform.getId2());
			request.setAttribute("Id2", siteform.getId1()); // by hamid

			if (siteform.getRef1().equals("fromjobdashboard")) {
				request.setAttribute("jobid", siteform.getId2());
				return mapping.findForward("jobdashboard");
			}

			if (siteform.getRef1().equals("fromprj")) {
				request.setAttribute("type", "PRMJobsite");
			} else {
				request.setAttribute("type", "PMJobsite");
			}

			request.setAttribute("addendum_id", siteform.getAddendum_id());
			request.setAttribute("ref", siteform.getRef1());

			siteform.setSubmitPage(null);
			return mapping.findForward("jobdetailpge");
		}

		else {
			// Start:Added By Amit
			DataSource ds = null;
			DynamicComboCM dcStateCM = new DynamicComboCM();
			ds = getDataSource(request, "ilexnewDB");

			Pvsdao pvsDao = new Pvsdao();
			if (pvsDao != null) {

				dcStateCM.setFirstlevelcatglist(Pvsdao.getStateList(ds));
				request.setAttribute("dcStateCM", dcStateCM);

				initialStateCategory = Jobdao
						.getStateCategoryList(getDataSource(request,
								"ilexnewDB"));

				if (initialStateCategory.size() > 0) {
					for (int i = 0; i < initialStateCategory.size(); i++) {
						tempcat = ((SiteBean) initialStateCategory.get(i))
								.getSiteCountryId();
						((SiteBean) initialStateCategory.get(i))
								.setTempList(SiteManagedao
										.getCategoryWiseStateList(
												tempcat,
												i,
												getDataSource(request,
														"ilexnewDB")));
					}
				}

				DynamicComboCM dcLonDirec = new DynamicComboCM();
				dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
				request.setAttribute("dcLonDirec", dcLonDirec);

				DynamicComboCM dcLatDirec = new DynamicComboCM();
				dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
				request.setAttribute("dcLatDirec", dcLatDirec);

				DynamicComboCM dcCountryList = new DynamicComboCM();
				dcCountryList.setFirstlevelcatglist(DynamicComboDao
						.getCountryList(ds));
				request.setAttribute("dcCountryList", dcCountryList);

				DynamicComboCM dcGroupList = new DynamicComboCM();
				dcGroupList.setFirstlevelcatglist(DynamicComboDao
						.getGroupNameListSiteSearch(siteform.getMsaid(), ds));
				request.setAttribute("dcGroupList", dcGroupList);

				DynamicComboCM dcEndCustomerList = new DynamicComboCM();
				dcEndCustomerList.setFirstlevelcatglist(DynamicComboDao
						.getEndCustomerList(ds, siteform.getMsaid(),
								siteform.getMsaname()));
				request.setAttribute("dcEndCustomerList", dcEndCustomerList);

				DynamicComboCM dcTimeZoneList = new DynamicComboCM();
				dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao
						.gettimezone(ds));
				request.setAttribute("dcTimeZoneList", dcTimeZoneList);

				DynamicComboCM dcCategoryList = new DynamicComboCM();
				dcCategoryList.setFirstlevelcatglist(DynamicComboDao
						.getCategoryList(ds));
				request.setAttribute("dcCategoryList", dcCategoryList);

				DynamicComboCM dcRegionList = new DynamicComboCM();
				dcRegionList.setFirstlevelcatglist(DynamicComboDao
						.getRegionList(ds));
				request.setAttribute("dcRegionList", dcRegionList);

				DynamicComboCM dcSiteStatus = new DynamicComboCM();
				dcSiteStatus.setFirstlevelcatglist(DynamicComboDao
						.getsitestatus());
				request.setAttribute("dcSiteStatus", dcSiteStatus);

			}

			// End: Added By Amit

			/*
			 * if(request.getParameter("page")!=null) {
			 * if(request.getParameter("page").equals("sitesearch")) { String
			 * sitelistid=request.getParameter("sitelistid");
			 * 
			 * siteform = Jobdao.getSearchedSite( siteform,sitelistid ,
			 * getDataSource( request , "ilexnewDB" ) ); } } else { siteform =
			 * Jobdao.getSite( siteform , getDataSource( request , "ilexnewDB" )
			 * ); }
			 */

			request.setAttribute("initialStateCategory", initialStateCategory);

			if (siteform.getRefersh() != null) {
				BeanUtils.copyProperties(siteBean, siteform);
				Jobdao.getRefershedStates(siteBean,
						getDataSource(request, "ilexnewDB"));
				BeanUtils.copyProperties(siteform, siteBean);
			} else {
				BeanUtils.copyProperties(siteBean, siteform);
				siteBean = Jobdao.getSite(siteBean,
						getDataSource(request, "ilexnewDB"));
				BeanUtils.copyProperties(siteform, siteBean);
			}
			if (siteform.getRef1() != null
					&& siteform.getRef1().equals("fromDailyReportPage")) {
				siteform.setDate(request.getParameter("datefrom"));
			}

		}

		// Added to have the job detail menu options.

		String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
				siteform.getId2(), "%", "%",
				getDataSource(request, "ilexnewDB"));
		String jobStatus = jobStatusAndType[0];
		String jobType1 = jobStatusAndType[1];
		String jobStatusList = "";
		String appendixType = "";

		if (jobType1.equals("Default"))
			jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
					+ siteform.getId2() + "&Status='D'\"" + "," + "0";
		if (jobType1.equals("Newjob"))
			jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0), "",
					"", siteform.getId2(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));
		if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
			jobStatusList = "\"Approved\""
					+ ","
					+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
					+ siteform.getId2() + "&Status=A\"" + "," + "0";
		if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
			jobStatusList = "\"Draft\""
					+ ","
					+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
					+ siteform.getId2() + "&Status=D\"" + "," + "0";
		if (jobType1.equals("Addendum"))
			jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0), "",
					"", siteform.getId2(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));

		appendixType = ViewList.getAppendixtypedesc(siteform.getId1(),
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("jobStatus", jobStatus);
		request.setAttribute("job_type", jobType1);
		request.setAttribute("appendixtype", appendixType);
		request.setAttribute("Job_Id", siteform.getId2());
		request.setAttribute("Appendix_Id", siteform.getId1());
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("addendum_id", siteform.getAddendum_id());

		if (jobType1.equals("Newjob")) {
			request.setAttribute("chkadd", "detailjob");
			request.setAttribute("chkaddendum", "detailjob");
			request.setAttribute("chkaddendumactivity", "View");
		}
		if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
			request.setAttribute("chkadd", "inscopejob");
			request.setAttribute("chkaddendum", "inscopejob");
			request.setAttribute("chkaddendumactivity", "inscopeactivity");
		}
		// End: for the job detail page menu

		// End : Added By Amit
		if (siteform.getSite_Id() == null) {

			siteform.setRef(request.getParameter("ref"));
			siteform.setCountry("US");
			siteform.setLocality_uplift("1.0");
			siteform.setUnion_site("N");
			request.setAttribute("appendixid", siteform.getId1());
			request.setAttribute("jobid", siteform.getId2());

			/*
			 * System.out.println("When Forwarding to the Site Search Page");
			 * 
			 * System.out.println("Job ID is ::" + siteform.getId2());
			 * System.out.println("Appendix ID is ::" + siteform.getId1());
			 * System.out.println("Ref is ::" + siteform.getRef());
			 * System.out.println("Ref1 is ::" + siteform.getRef1());
			 * System.out.println("Addendum id is ::" +
			 * siteform.getAddendum_id()); System.out.println("FromType  is ::"
			 * + siteform.getFromType()); System.out.println("NetType is ::" +
			 * siteform.getNettype());
			 */

			return mapping.findForward("sitesearch");
		}

		else {
			siteform.setRef("Update");
		}
		siteform.setPoc(ViewList.getSitePOC(siteform.getId2(), "J",
				getDataSource(request, "ilexnewDB")));

		// By Amit ,Start:To show Appendix Name and Job Name in site Information
		siteform.setAppendixname(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"), siteform.getId1()));
		// job = Jobdao.getJob( "%" , siteform.getId2() , "%" , getDataSource(
		// request , "ilexnewDB" ) );
		siteform.setJobName(Jobdao.getJobname(siteform.getId2(),
				getDataSource(request, "ilexnewDB")));

		// By Amit ,End:To show Appendix Name and Job Name in site Information

		/*
		 * System.out.println("When Forwarding to the Site Edit Page For a Job");
		 * 
		 * System.out.println("Job ID is ::" + siteform.getId2());
		 * System.out.println("Appendix ID is ::" + siteform.getId1());
		 * System.out.println("Ref is ::" + siteform.getRef());
		 * System.out.println("Ref1 is ::" + siteform.getRef1());
		 * System.out.println("Addendum id is ::" + siteform.getAddendum_id());
		 * System.out.println("FromType  is ::" + siteform.getFromType());
		 * System.out.println("NetType is ::" + siteform.getNettype());
		 */
		siteform.setStateSelect(Util.concatString(siteform.getState(),
				siteform.getCountry()));
		return mapping.findForward("sitepage");
	}

	public ActionForward Update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SiteForm siteform = (SiteForm) form;
		SiteBean siteBean = new SiteBean();

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		// Start: By Amit ,to make invisible back button on site JSP when ,It
		// comes from Manage Site of Proposal Manager
		if (request.getParameter("fromProposalManag") != null) {
			request.setAttribute("fromType",
					request.getParameter(("fromProposalManag")));
			siteform.setFromType(request.getParameter("fromProposalManag"));
		}
		// End : : By Amit ,to make invisible back button on site JSP when ,It
		// comes from Manage Site of Proposal Manager
		if (request.getParameter("Job_Id") != null) {
			siteform.setId2(request.getParameter("Job_Id"));
		}

		if (request.getParameter("Appendix_Id") != null) {
			siteform.setId1(request.getParameter("Appendix_Id"));
		}

		if (request.getParameter("ref1") != null) {
			siteform.setRef1(request.getParameter("ref1"));
		}

		if (request.getParameter("nettype") != null) {
			siteform.setNettype("" + request.getParameter("nettype"));
		}
		if (request.getParameter("viewjobtype") != null) {
			siteform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			siteform.setViewjobtype("A");
		}
		request.setAttribute("viewjobtype", siteform.getViewjobtype());

		if (request.getParameter("ownerId") != null) {
			siteform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			siteform.setOwnerId("%");
		}

		if (siteform.getSubmitPage() == null)
			siteform.setSubmitPage("");

		// if( siteform.getSite_name() != null )
		if (siteform.getSubmitPage().equals("submitPage")) {

			BeanUtils.copyProperties(siteBean, siteform);
			int val = Jobdao.updateSite(siteBean, loginuserid,
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("Id1", siteform.getId2());
			request.setAttribute("Id2", siteform.getId1()); // by hamid

			if (siteform.getRef1().equals("fromjobdashboard")) {
				request.setAttribute("jobid", siteform.getId2());
				return mapping.findForward("jobdashboard");
			}
			if (siteform.getRef1() != null
					&& siteform.getRef1().equals("fromDailyReportPage")) {
				request.setAttribute("date", siteform.getDate());
				return mapping.findForward("dailyReportPage");
			}

			if (siteform.getRef1().equals("fromprj")) {
				request.setAttribute("type", "PRMJobsite");
			} else {
				request.setAttribute("type", "PMJobsite");
			}

			request.setAttribute("addendum_id", siteform.getAddendum_id());
			request.setAttribute("ref", siteform.getRef1());
			siteform.setSubmitPage(null);

			return mapping.findForward("jobdetailpge");
		}

		else {
			siteform.setRef(request.getParameter("ref"));
			// Start:Added By Amit
			DataSource ds = null;
			DynamicComboCM dcStateCM = new DynamicComboCM();
			ds = getDataSource(request, "ilexnewDB");

			Pvsdao pvsDao = new Pvsdao();
			if (pvsDao != null) {
				dcStateCM.setFirstlevelcatglist(Pvsdao.getStateList(ds));

				request.setAttribute("dcStateCM", dcStateCM);
			} else {

			}

			// End: Added By Amit
			BeanUtils.copyProperties(siteBean, siteform);
			siteBean = Jobdao.getSite(siteBean,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(siteform, siteBean);
			siteform.setPoc(ViewList.getSitePOC(siteform.getId2(), "J",
					getDataSource(request, "ilexnewDB")));
			return mapping.findForward("sitepage");
		}
	}

	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		SiteForm siteform = (SiteForm) form;
		SiteBean siteBean = new SiteBean();
		BeanUtils.copyProperties(siteBean, siteform);
		Jobdao.deleteSite(siteBean, getDataSource(request, "ilexnewDB"));

		request.setAttribute("Appendix_Id", siteform.getId1());
		return mapping.findForward("jobtabularpge");

	}

}
