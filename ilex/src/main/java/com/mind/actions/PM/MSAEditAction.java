package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.bean.pm.Appendix;
import com.mind.bean.pm.MSAEditBean;
import com.mind.common.ViewAllComment;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.ManageUpliftdao;
import com.mind.formbean.PM.MSAEditForm;
import com.mind.util.WebUtil;

public class MSAEditAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MSAEditForm msaEditForm = (MSAEditForm) form;
		Appendix appendix = new Appendix();
		boolean refresh = false;
		int msa_size = 0;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String loginusername = (String) session.getAttribute("username");
		String checkforroleassigned = "";
		Collection[] tempPOC = null;
		Codelist codes = new Codelist();
		String sortQueryClause = "";

		// Variables added for the View Selector Functioning.
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "msa";
		String msaid = "0";
		int ownerListSize = 0;
		String Appendix_Id = "";
		MSAEditBean msaEditBean = new MSAEditBean();

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, "MSA", "MSA Edit",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (request.getParameter("msaId") != null) {
			msaEditForm.setMsaId(request.getParameter("msaId"));
		} else {
			msaEditForm.setMsaId(msaid);
		}

		if (request.getParameter("fromType") != null) {
			msaEditForm
					.setFromType(request.getParameter("fromType").toString());
		}
		if (request.getAttribute("querystring") != null) {
			sortQueryClause = (String) request.getAttribute("querystring");
		}

		if (session.getAttribute("appendixSelectedOwners") != null
				|| session.getAttribute("appendixSelectedStatus") != null)
			request.setAttribute("specialCase", "specialCase");

		if (session.getAttribute("monthAppendix") != null
				|| session.getAttribute("weekAppendix") != null) {
			request.setAttribute("specialCase", "specialCase");
		}

		// Added to refersh the previous Appendix List state::::
		WebUtil.removeAppendixSession(session);

		// This is required for the div to remain open after pressing show.
		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		// The Menu Images are to be set in session respective of one other.
		if (request.getParameter("month") != null) {
			session.setAttribute("monthMSA", request.getParameter("month")
					.toString());
			monthMSA = request.getParameter("month").toString();
			session.removeAttribute("weekMSA");
			msaEditForm.setMonthWeekCheck("month");
		}

		if (request.getParameter("week") != null) {
			session.setAttribute("weekMSA", request.getParameter("week")
					.toString());
			weekMSA = request.getParameter("week").toString();
			session.removeAttribute("monthMSA");
			msaEditForm.setMonthWeekCheck("week");
		}

		// When the reset button is pressed remove every thing from the session.
		if (request.getParameter("home") != null) {
			msaEditForm.setSelectedStatus(null);
			msaEditForm.setSelectedOwners(null);
			msaEditForm.setPrevSelectedStatus("");
			msaEditForm.setPrevSelectedOwner("");
			msaEditForm.setOtherCheck(null);
			msaEditForm.setMonthWeekCheck(null);
			// MSAdao.setValuesWhenReset(session); added by vishal kapoor
			return (mapping.findForward("msalist"));
		}

		// when the other check box is checked in the view selector.
		if (msaEditForm.getOtherCheck() != null)
			session.setAttribute("otherCheck", msaEditForm.getOtherCheck()
					.toString());

		if (msaEditForm.getSelectedStatus() != null) {
			for (int i = 0; i < msaEditForm.getSelectedStatus().length; i++) {
				selectedStatus = selectedStatus + "," + "'"
						+ msaEditForm.getSelectedStatus(i) + "'";
				tempStatus = tempStatus + ","
						+ msaEditForm.getSelectedStatus(i);
			}

			selectedStatus = selectedStatus.substring(1,
					selectedStatus.length());
			selectedStatus = "(" + selectedStatus + ")";
		}

		if (msaEditForm.getSelectedOwners() != null) {
			for (int j = 0; j < msaEditForm.getSelectedOwners().length; j++) {
				selectedOwner = selectedOwner + ","
						+ msaEditForm.getSelectedOwners(j);
				tempOwner = tempOwner + "," + msaEditForm.getSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if ("0".equals(selectedOwner)) {
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" + selectedOwner + ")";
		}

		// To set the values in the session when the Show button is Pressed.
		if (msaEditForm.getGo() != null && !msaEditForm.getGo().equals("")) {
			session.setAttribute("selectedOwners", selectedOwner);
			session.setAttribute("MSASelectedOwners", selectedOwner);
			session.setAttribute("tempOwner", tempOwner);
			session.setAttribute("selectedStatus", selectedStatus);
			session.setAttribute("MSASelectedStatus", selectedStatus);
			session.setAttribute("tempStatus", tempStatus);

			if (msaEditForm.getMonthWeekCheck() != null) {
				if (msaEditForm.getMonthWeekCheck().equals("week")) {
					session.removeAttribute("monthMSA");
					session.setAttribute("weekMSA", "0");
				}
				if (msaEditForm.getMonthWeekCheck().equals("month")) {
					session.removeAttribute("weekMSA");
					session.setAttribute("monthMSA", "0");
				}
				if (msaEditForm.getMonthWeekCheck().equals("clear")) {
					session.removeAttribute("monthMSA");
					session.removeAttribute("weekMSA");
				}

			}

			if (session.getAttribute("monthMSA") != null) {
				session.removeAttribute("weekMSA");
				monthMSA = session.getAttribute("monthMSA").toString();
			}
			if (session.getAttribute("weekMSA") != null) {
				session.removeAttribute("monthMSA");
				weekMSA = session.getAttribute("weekMSA").toString();
			}
			return (mapping.findForward("msalist"));
		} else // If not pressed see the values exists in the session or not.
		{
			if (session.getAttribute("selectedOwners") != null) {
				selectedOwner = session.getAttribute("selectedOwners")
						.toString();
				msaEditForm.setSelectedOwners(session.getAttribute("tempOwner")
						.toString().split(","));
			}
			if (session.getAttribute("selectedStatus") != null) {
				selectedStatus = session.getAttribute("selectedStatus")
						.toString();
				msaEditForm.setSelectedStatus(session
						.getAttribute("tempStatus").toString().split(","));
			}
			if (session.getAttribute("otherCheck") != null)
				msaEditForm.setOtherCheck(session.getAttribute("otherCheck")
						.toString());
		}

		if (session.getAttribute("monthMSA") != null) {
			session.removeAttribute("weekMSA");
			monthMSA = session.getAttribute("monthMSA").toString();
			msaEditForm.setMonthWeekCheck("month");
		}
		if (session.getAttribute("weekMSA") != null) {
			session.removeAttribute("monthMSA");
			weekMSA = session.getAttribute("weekMSA").toString();
			msaEditForm.setMonthWeekCheck("week");
		}

		if (!chkOtherOwner) {
			msaEditForm.setPrevSelectedStatus(selectedStatus);
			msaEditForm.setPrevSelectedOwner(selectedOwner);
		}

		ownerList = MSAdao.getOwnerList(loginuserid,
				msaEditForm.getOtherCheck(), ownerType, msaid,
				getDataSource(request, "ilexnewDB"));
		statusList = MSAdao.getStatusList("pm_msa",
				getDataSource(request, "ilexnewDB"));

		if (ownerList.size() > 0)
			ownerListSize = ownerList.size();

		request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", statusList);
		request.setAttribute("ownerlist", ownerList);
		request.setAttribute("ownerListSize", ownerListSize + "");

		if (msaEditForm.getSave() != null) {
			BeanUtils.copyProperties(msaEditBean, msaEditForm);
			int editMSAFlag = MSAdao.editMSA(msaEditBean, loginuserid,
					getDataSource(request, "ilexnewDB"));

			if (!"0".equals(msaEditForm.getMsaId())) {
				request.setAttribute("editAction", "U");
			} else {
				request.setAttribute("editAction", "A");
			}

			request.setAttribute("editMSAFlag", "" + editMSAFlag);

			if (msaEditForm.getFromType().equals("MSADetail")) {
				request.setAttribute("MSA_Id", msaEditForm.getMsaId());
				return (mapping.findForward("msadetail"));
			}
			return (mapping.findForward("msalist"));
		}

		if (!"0".equals(msaEditForm.getMsaId())) {
			BeanUtils.copyProperties(msaEditBean, msaEditForm);
			msaEditBean = MSAdao.getMSA(msaEditBean, sortQueryClause,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(msaEditForm, msaEditBean);

			msaEditForm
					.setOrgTopId(IMdao.getOrganizationTopId(
							msaEditForm.getMsaId(),
							getDataSource(request, "ilexnewDB")));

			codes.setPocList(MSAdao.getcustomerPOC(msaEditForm.getOrgTopId(),
					"MSA", getDataSource(request, "ilexnewDB")));

			msaEditForm.setActionAddUpdate("U");
		}

		else {
			msaEditForm.setUnionUpliftFactor(ManageUpliftdao
					.setDefaultUnionUpliftFactor(getDataSource(request,
							"ilexnewDB")));
			msaEditForm.setMsaType("S");
			msaEditForm.setSalesType("D");

			msaEditForm.setPayTerms("Net 30");
			msaEditForm.setPayQualifier("None");

			msaEditForm.setDraftReviews("S");
			msaEditForm.setBusinessReviews("S");

			checkforroleassigned = ViewList.checkforrole(loginuserid, "ISC",
					getDataSource(request, "ilexnewDB"));

			if (checkforroleassigned.equals("Y")) {
				msaEditForm.setSalesPOC(loginuserid);
			}

			msaEditForm.setSchDays("30");

			msaEditForm.setReqPlannedSch(ViewList.getDefaultdate("R",
					getDataSource(request, "ilexnewDB")));
			msaEditForm.setEffPlannedSch(ViewList.getDefaultdate("E",
					getDataSource(request, "ilexnewDB")));

			msaEditForm.setCustPOCBusiness("0");
			msaEditForm.setCustPOCContract("0");
			msaEditForm.setActionAddUpdate("A");
		}

		/* Set combo values */
		codes.setMsatypecode(ViewList.getMsatype(getDataSource(request,
				"ilexnewDB")));
		codes.setSalesPOCcode(ViewList.getPOCbasedonrole("MSA",
				getDataSource(request, "ilexnewDB")));

		codes.setCustPOCbusinesscode(ViewList.getPOC("C", "",
				getDataSource(request, "ilexnewDB")));
		codes.setCustPOCcontractcode(codes.getCustPOCbusinesscode());

		codes.setCustomername(ViewList.getCustomers(getDataSource(request,
				"ilexnewDB")));

		request.setAttribute("codeslist", codes);

		ArrayList allComment = new ArrayList();
		allComment = ViewAllComment.Getallcomment(msaEditForm.getMsaId(),
				"MSA", getDataSource(request, "ilexnewDB"));
		request.setAttribute("allComment", allComment);

		if (allComment != null)
			request.setAttribute("allCommentSize", "" + allComment.size());

		request.setAttribute("MSA_Id", msaEditForm.getMsaId());
		// getting appendix id
		if (request.getAttribute("Appendix_Id") != null) {

			/*
			 * Appendix_Id = Appendixdao.getAppendix("%", Appendix_Id,
			 * getDataSource(request, "ilexnewDB"));
			 */

			request.setAttribute("Appendix_Id", Appendix_Id);
		}

		String appendixtype = appendix.getAppendixtype();
		request.setAttribute("Appendix_Id", Appendix_Id);
		request.setAttribute("appendixType", appendixtype);

		if (!msaEditForm.getMsaId().equals("0")) {
			String msaStatus = MSAdao.getMSAStatus(msaEditForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String msaUploadCheck = MSAdao
					.getMSAUploadCheck(msaEditForm.getMsaId(),
							getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
					msaEditForm.getMsaId(), "", "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);
			request.setAttribute("status", msaStatus);
			request.setAttribute("uploadstatus", msaUploadCheck);
		} else
			request.setAttribute("list", "");

		return (mapping.findForward("success"));
	}
}
