/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is an dispatch action class used for managing activity and showing activity detail in Proposal and Project Manager.      
*
*/

package com.mind.actions.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pm.Activity;
import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.Resourcedao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.dao.PRM.ManageUpliftdao;
import com.mind.formbean.PM.ActivityDetailForm;

/*
 * Methods : detailactivity, detailaddendumactivity
 */

public class ActivityDetailAction extends com.mind.common.IlexDispatchAction
{
/** This method managing appendix activity and showing appendix activity detail in Proposal Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
	
	public ActionForward detailactivity( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		ActivityDetailForm activitydetailform = ( ActivityDetailForm )form;
	
		String Activity_Id = ""; 
		String checkforresourcelink = "";
		String MSA_Id = "0";
		String MSA_Name = "";
		String from = "";
		int uplift_size = 0;
		
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		int sow_size = 0;
		int assumption_size = 0;
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		
		if( request.getParameter( "Activity_Id" ) != null )
		{
			Activity_Id = request.getParameter( "Activity_Id" );
		}
		
		if( request.getAttribute( "Activity_Id" ) != null )
		{
			Activity_Id = ( String ) request.getAttribute( "Activity_Id" );
		}
		
		if( request.getParameter( "from" ) != null )
		{
			from = request.getParameter( "from" );
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		checkforresourcelink = Resourcedao.Checkforlink( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "checkforresourcelink" , checkforresourcelink );
		
		Activity activity = Activitydao.getActivity( Activity_Id , "%" , getDataSource( request , "ilexnewDB" ) );
		
		activitydetailform.setJob_Id( activity.getJob_Id() );
		
		activitydetailform.setJobname( Activitydao.getJobname( getDataSource( request , "ilexnewDB" ) , activitydetailform.getJob_Id() ) );
		
		
		activitydetailform.setJob_type( Jobdao.getJobtype( activity.getJob_Id() , getDataSource( request , "ilexnewDB" ) ) );
		request.setAttribute( "job_type" , activitydetailform.getJob_type() );
		request.setAttribute( "Job_Id" , activitydetailform.getJob_Id() );
		
		activitydetailform.setActivity_Id( activity.getActivity_Id() );
		request.setAttribute( "Activity_Id" , activitydetailform.getActivity_Id() );
		
		activitydetailform.setEstimated_materialcost( activity.getEstimated_materialcost() );
		activitydetailform.setEstimated_cnslaborcost( activity.getEstimated_cnsfieldhqlaborcost() );
		activitydetailform.setEstimated_contractlaborcost( activity.getEstimated_contractfieldlaborcost() );
		activitydetailform.setEstimated_freightcost( activity.getEstimated_freightcost() );
		activitydetailform.setEstimated_travelcost( activity.getEstimated_travelcost() );
		activitydetailform.setEstimated_totalcost( activity.getEstimated_totalcost() );
		activitydetailform.setExtendedprice( activity.getExtendedprice() );
		activitydetailform.setListprice( activity.getListprice() );
		activitydetailform.setQuantity( activity.getQuantity() );
		activitydetailform.setOverheadcost( activity.getOverheadcost() );
		activitydetailform.setProformamargin( activity.getProformamargin() );
		activitydetailform.setLastchangeby( activity.getLastchangeby() ); 
		activitydetailform.setLastchangedate( activity.getLastchangedate() ); 
		activitydetailform.setLastcomment( activity.getLastcomment() );
		activitydetailform.setStatus( activity.getStatus() );
		activitydetailform.setType( activity.getActivitytype() );
		activitydetailform.setName( activity.getName() );
		activitydetailform.setLastactivitymodifiedby( activity.getLastactivitymodifiedby() );
		activitydetailform.setLastactivitymodifieddate( activity.getLastactivitymodifieddate() );
		activitydetailform.setChkaddendum( "detailactivity" );   // chk addendum flag
		activitydetailform.setChkaddendumresource( "View" );		// chk addendum flag
		
		activitydetailform.setEstimated_unitcost(activity.getEstimated_unitcost());
		activitydetailform.setEstimated_unitprice(activity.getEstimated_unitprice());
		
		activitydetailform.setAppendix_id( activity.getAppendix_id() );
		activitydetailform.setAppendixname( activity.getAppendixname() );
		activitydetailform.setMsa_id( activity.getMsa_id() );
		activitydetailform.setMsaname( activity.getMsaname() );
		activitydetailform.setCommissionCost(activity.getCommissionCost());
		activitydetailform.setCommissionRevenue(activity.getCommissionRevenue());
		
		if( request.getParameter( "from" ) != null )
		{
			String jobid = request.getParameter( "jobid" );
			String list = "";
			String list2 = "";
			
			//Addendum Change 1.23
			if(activitydetailform.getJob_type().equals("Addendum") || activitydetailform.getJob_type().equals("Default")) {
				list = Menu.getStatus( "pm_act" , activitydetailform.getStatus().charAt( 0 ) , from , jobid , Activity_Id , "detailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
				list2 = Menu.getStatus( "pm_act_new" , activitydetailform.getStatus().charAt( 0 ) , from , jobid , Activity_Id , "detailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			}
			else {
				activitydetailform.setStatus( JobDashboarddao.getActivityprojectstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) ) );
				list = Menu.getStatus( "prj_act" , activitydetailform.getStatus().charAt( 0 ) , from , jobid , Activity_Id , "detailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
				list2 = Menu.getStatus( "prj_act_new" , activitydetailform.getStatus().charAt( 0 ) , from , jobid , Activity_Id , "detailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			}	
			request.setAttribute( "list" , list );
			request.setAttribute( "new_list" , list2 );
		}
		else
		{
			String list = Menu.getStatus( "pm_act" , activitydetailform.getStatus().charAt( 0 ) , "" , "" , Activity_Id , "detailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "list" , list );
			String list2 = Menu.getStatus( "pm_act_new" , activitydetailform.getStatus().charAt( 0 ) , "" , "" , Activity_Id , "detailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "new_list" , list2 );
		}
		
		
		
		
		MSA_Id = Activitydao.getmsaid(getDataSource( request , "ilexnewDB" ),activitydetailform.getJob_Id());
		request.setAttribute("MSA_Id",MSA_Id);
		MSA_Name = Activitydao.getMsaname(getDataSource( request , "ilexnewDB" ),MSA_Id);
		request.setAttribute("MSA_Name",MSA_Name);
		
		if( request.getAttribute( "changestatusflag" ) != null )
		{
			request.setAttribute( "changestatusflag" , request.getAttribute( "changestatusflag" ) );
		}
		
		
		
		/*customer labor rates*/
		
		String Appendix_Id ="";
		int checkNetmedXappendix = 0;
		
		Appendix_Id = Activitydao.getAppendixid(getDataSource( request , "ilexnewDB" ),activitydetailform.getJob_Id());
		checkNetmedXappendix = Activitydao.checkAppendixname(Appendix_Id, activitydetailform.getJob_Id(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("checknetmedx",checkNetmedXappendix+"");
		
		/*customer labor rates*/
		
		ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , Activity_Id , "Activity" );
		
		if( sowlist.size() > 0 )
		{
			sow_size = sowlist.size();	
		}
		
		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , Activity_Id , "Activity" );
		
		if( assumptionlist.size() > 0 )
		{
			assumption_size = assumptionlist.size();	
		}
		
		request.setAttribute( "sowsize" , ""+sow_size );
		request.setAttribute( "assumptionsize" , ""+assumption_size );
		
		
		request.setAttribute( "sowlist" , sowlist );
		request.setAttribute( "assumptionlist" , assumptionlist );
		
		/* GEt uplift factor list*/
		ArrayList upliftFactorList = ManageUpliftdao.getUpliftFactorList(activitydetailform.getJob_Id(), Activity_Id, "Activity", getDataSource(request, "ilexnewDB"));
		if( upliftFactorList.size() > 0 )
				uplift_size = upliftFactorList.size();	
		
		request.setAttribute("uplift_size", ""+uplift_size);
		request.setAttribute("upliftFactorList", upliftFactorList);
		
		/*customer labor rates*/
		return mapping.findForward( "activitydetailpage" );
	
	}
	
/** This method managing addendum activity and showing addendum activity detail in Project Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/	
		
	
	public ActionForward detailaddendumactivity( ActionMapping mapping , ActionForm form,HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		ActivityDetailForm activitydetailform = ( ActivityDetailForm )form;
	
		String Activity_Id = ""; 
		String checkforresourcelink = "";
		String list = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		int sow_size = 0;
		int assumption_size = 0;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		String from = "";
		String appendixname = "";
		
		if( request.getParameter( "Activity_Id" ) != null )
		{
			Activity_Id = request.getParameter( "Activity_Id" );
		}
		
		if( request.getAttribute( "Activity_Id" ) != null )
		{
			Activity_Id = ( String ) request.getAttribute( "Activity_Id" );
		}
		
		if( request.getParameter( "addendum_id" ) != null )
		{
			activitydetailform.setAddendum_id( request.getParameter( "addendum_id" ) );
		}
		
		if( request.getAttribute( "addendum_id" ) != null )
		{
			activitydetailform.setAddendum_id( request.getAttribute( "addendum_id" ).toString() );
		}
		
		
		if( request.getParameter( "from" ) != null )
		{
			from = request.getParameter( "from" );
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		
		
		
		checkforresourcelink = Resourcedao.Checkforlink( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "checkforresourcelink" , checkforresourcelink );
		
		Activity activity = Activitydao.getActivity( Activity_Id , "%" , getDataSource( request , "ilexnewDB" ) );
		
		activitydetailform.setJob_Id( activity.getJob_Id() );
		
		activitydetailform.setJobname( Activitydao.getJobname( getDataSource( request , "ilexnewDB" ) , activitydetailform.getJob_Id() ) );
		
		
		activitydetailform.setJob_type( Jobdao.getJobtype( activity.getJob_Id() , getDataSource( request , "ilexnewDB" ) ) );
		request.setAttribute( "job_type" , activitydetailform.getJob_type() );
		request.setAttribute( "Job_Id" , activitydetailform.getJob_Id() );
		
		activitydetailform.setActivity_Id( activity.getActivity_Id() );
		request.setAttribute( "Activity_Id" , activitydetailform.getActivity_Id() );
		
		activitydetailform.setEstimated_materialcost( activity.getEstimated_materialcost() );
		activitydetailform.setEstimated_cnslaborcost( activity.getEstimated_cnsfieldhqlaborcost() );
		activitydetailform.setEstimated_contractlaborcost( activity.getEstimated_contractfieldlaborcost() );
		activitydetailform.setEstimated_freightcost( activity.getEstimated_freightcost() );
		activitydetailform.setEstimated_travelcost( activity.getEstimated_travelcost() );
		activitydetailform.setEstimated_totalcost( activity.getEstimated_totalcost() );
		activitydetailform.setExtendedprice( activity.getExtendedprice() );
		activitydetailform.setListprice( activity.getListprice() );
		activitydetailform.setQuantity( activity.getQuantity() );
		activitydetailform.setOverheadcost( activity.getOverheadcost() );
		activitydetailform.setProformamargin( activity.getProformamargin() );
		activitydetailform.setLastchangeby( activity.getLastchangeby() ); 
		activitydetailform.setLastchangedate( activity.getLastchangedate() ); 
		activitydetailform.setLastcomment( activity.getLastcomment() );
		activitydetailform.setStatus( activity.getStatus() );
		activitydetailform.setType( activity.getActivitytype() );
		activitydetailform.setName( activity.getName() );
		
		activitydetailform.setChkaddendum( "detailaddendumactivity" );   // chk addendum flag
		activitydetailform.setChkaddendumresource( "ViewAddendum" );   // chk addendum flag
		
		activitydetailform.setEstimated_unitcost(activity.getEstimated_unitcost());
		activitydetailform.setEstimated_unitprice(activity.getEstimated_unitprice());
		
		activitydetailform.setAppendix_id( activity.getAppendix_id() );
		activitydetailform.setAppendixname( activity.getAppendixname() );
		activitydetailform.setMsa_id( activity.getMsa_id() );
		activitydetailform.setMsaname( activity.getMsaname() );
		activitydetailform.setCommissionCost(activity.getCommissionCost());
		activitydetailform.setCommissionRevenue(activity.getCommissionRevenue());
		
		//String list = Menu.getStatus( "pm_act" , activitydetailform.getStatus().charAt( 0 ) , "" , "" , Activity_Id , "" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
		
		
		if( request.getParameter( "from" ) != null )
		{
			String jobid = request.getParameter( "jobid" );
			
			activitydetailform.setStatus( JobDashboarddao.getActivityprojectstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) ) );
			
			list = Menu.getStatus( "prj_act" , activitydetailform.getStatus().charAt( 0 ) , from , jobid , Activity_Id , "detailaddendumactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "list" , list );
		}
		else
		{
			if( activitydetailform.getStatus().equals( "Draft" ) )
			{
				list = "\"Approved\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id="+activitydetailform.getAddendum_id()+"&ref=Fromactivitylevel&ref1=detailaddendumactivity&Activity_Id="+activitydetailform.getActivity_Id()+"&Status=A\"" + "," + "0";
			}
			
			if( activitydetailform.getStatus().equals( "Approved" ) )
			{
				list = "\"Draft\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id="+activitydetailform.getAddendum_id()+"&ref=Fromactivitylevel&ref1=detailaddendumactivity&Activity_Id="+activitydetailform.getActivity_Id()+"&Status=D\"" + "," + "0";
			}
			
			
			//list = Menu.getStatus( "pm_act" , activitydetailform.getStatus().charAt( 0 ) , "" , "" , Activity_Id , "" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			//request.setAttribute( "list" , list );
		}
		
		
		
		/* by hamid for appendixname setting*/
		
		appendixname=Jobdao.getAppendixname(getDataSource( request , "ilexnewDB" ),com.mind.dao.PRM.Jobdao.getAppendixid(activitydetailform.getJob_Id(),getDataSource( request , "ilexnewDB" )));
		
		
		request.setAttribute("appendixname",appendixname);
		request.setAttribute("appendixid",com.mind.dao.PRM.Jobdao.getAppendixid(activitydetailform.getJob_Id(),getDataSource( request , "ilexnewDB" )));
		/* by hamid*/
		
		
		
		
		
		/*if( activitydetailform.getStatus().equals( "Draft" ) )
		{
			list = "\"Approved\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id="+activitydetailform.getAddendum_id()+"&ref=Fromactivitylevel&ref1=detailaddendumactivity&Activity_Id="+activitydetailform.getActivity_Id()+"&Status=A\"" + "," + "0";
		}
		
		if( activitydetailform.getStatus().equals( "Approved" ) )
		{
			list = "\"Draft\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id="+activitydetailform.getAddendum_id()+"&ref=Fromactivitylevel&ref1=detailaddendumactivity&Activity_Id="+activitydetailform.getActivity_Id()+"&Status=D\"" + "," + "0";
		}*/
		
		if( request.getParameter( "changestatusflag" ) != null )
		{
			request.setAttribute( "changestatusflag" , request.getParameter( "changestatusflag" ) );
		}
		
		ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , Activity_Id , "Activity" );
		
		if( sowlist.size() > 0 )
		{
			sow_size = sowlist.size();	
		}
		
		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , Activity_Id , "Activity" );
		
		if( assumptionlist.size() > 0 )
		{
			assumption_size = assumptionlist.size();	
		}
		
		request.setAttribute( "sowsize" , ""+sow_size );
		request.setAttribute( "assumptionsize" , ""+assumption_size );
		
		
		request.setAttribute( "sowlist" , sowlist );
		request.setAttribute( "assumptionlist" , assumptionlist );
		
		request.setAttribute( "list" , list );

		return mapping.findForward( "activitydetailpage" );
	
	}
	
	
	public ActionForward detailchangeorderactivity( ActionMapping mapping , ActionForm form,HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		ActivityDetailForm activitydetailform = ( ActivityDetailForm )form;
	
		String Activity_Id = ""; 
		String checkforresourcelink = "";
		String list = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		int sow_size = 0;
		int assumption_size = 0;
		String from = "";
		String appendixname = "";
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		
		if( request.getParameter( "Activity_Id" ) != null )
		{
			Activity_Id = request.getParameter( "Activity_Id" );
		}
		
		if( request.getAttribute( "Activity_Id" ) != null )
		{
			Activity_Id = ( String ) request.getAttribute( "Activity_Id" );
		}
		
		if( request.getParameter( "addendum_id" ) != null )
		{
			activitydetailform.setAddendum_id( request.getParameter( "addendum_id" ) );
		}
		
		if( request.getAttribute( "addendum_id" ) != null )
		{
			activitydetailform.setAddendum_id( request.getAttribute( "addendum_id" ).toString() );
		}
		
		
		if( request.getParameter( "from" ) != null )
		{
			from = request.getParameter( "from" );
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		
		checkforresourcelink = Resourcedao.Checkforlink( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "checkforresourcelink" , checkforresourcelink );
		
		Activity activity = Activitydao.getActivity( Activity_Id , "%" , getDataSource( request , "ilexnewDB" ) );
		
		activitydetailform.setJob_Id( activity.getJob_Id() );
		
		activitydetailform.setJobname( Activitydao.getJobname( getDataSource( request , "ilexnewDB" ) , activitydetailform.getJob_Id() ) );
		
		
		activitydetailform.setJob_type( Jobdao.getJobtype( activity.getJob_Id() , getDataSource( request , "ilexnewDB" ) ) );
		request.setAttribute( "job_type" , activitydetailform.getJob_type() );
		request.setAttribute( "Job_Id" , activitydetailform.getJob_Id() );
		
		activitydetailform.setActivity_Id( activity.getActivity_Id() );
		request.setAttribute( "Activity_Id" , activitydetailform.getActivity_Id() );
		
		activitydetailform.setEstimated_materialcost( activity.getEstimated_materialcost() );
		activitydetailform.setEstimated_cnslaborcost( activity.getEstimated_cnsfieldhqlaborcost() );
		activitydetailform.setEstimated_contractlaborcost( activity.getEstimated_contractfieldlaborcost() );
		activitydetailform.setEstimated_freightcost( activity.getEstimated_freightcost() );
		activitydetailform.setEstimated_travelcost( activity.getEstimated_travelcost() );
		activitydetailform.setEstimated_totalcost( activity.getEstimated_totalcost() );
		activitydetailform.setExtendedprice( activity.getExtendedprice() );
		activitydetailform.setListprice( activity.getListprice() );
		activitydetailform.setQuantity( activity.getQuantity() );
		activitydetailform.setOverheadcost( activity.getOverheadcost() );
		activitydetailform.setProformamargin( activity.getProformamargin() );
		activitydetailform.setLastchangeby( activity.getLastchangeby() ); 
		activitydetailform.setLastchangedate( activity.getLastchangedate() ); 
		activitydetailform.setLastcomment( activity.getLastcomment() );
		activitydetailform.setStatus( activity.getStatus() );
		activitydetailform.setType( activity.getActivitytype() );
		activitydetailform.setName( activity.getName() );
		
		activitydetailform.setChkaddendum( "detailchangeorderactivity" );   // chk addendum flag
		activitydetailform.setChkaddendumresource( "ViewChangeorder" );   // chk addendum flag
		
		activitydetailform.setEstimated_unitcost(activity.getEstimated_unitcost());
		activitydetailform.setEstimated_unitprice(activity.getEstimated_unitprice());
		
		activitydetailform.setAppendix_id( activity.getAppendix_id() );
		activitydetailform.setAppendixname( activity.getAppendixname() );
		activitydetailform.setMsa_id( activity.getMsa_id() );
		activitydetailform.setMsaname( activity.getMsaname() );
		activitydetailform.setCommissionCost(activity.getCommissionCost());
		activitydetailform.setCommissionRevenue(activity.getCommissionRevenue());
		
		if( request.getParameter( "from" ) != null )
		{
			String jobid = request.getParameter( "jobid" );
			
			activitydetailform.setStatus( JobDashboarddao.getActivityprojectstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) ) );
			
			list = Menu.getStatus( "prj_act" , activitydetailform.getStatus().charAt( 0 ) , from , jobid , Activity_Id , "detailaddendumactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "list" , list );
		}
		else
		{
			if( activitydetailform.getStatus().equals( "Draft" ) )
			{
				list = "\"Approved\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id="+activitydetailform.getAddendum_id()+"&ref=Fromactivitylevel&ref1=detailchangeorderactivity&Activity_Id="+activitydetailform.getActivity_Id()+"&Status=A\"" + "," + "0";
			}
			
			if( activitydetailform.getStatus().equals( "Approved" ) )
			{
				list = "\"Draft\"" + "," + "\"AddendumChangeStatusAction.do?addendum_id="+activitydetailform.getAddendum_id()+"&ref=Fromactivitylevel&ref1=detailchangeorderactivity&Activity_Id="+activitydetailform.getActivity_Id()+"&Status=D\"" + "," + "0";
			}
			
			//list = Menu.getStatus( "pm_act" , activitydetailform.getStatus().charAt( 0 ) , "" , "" , Activity_Id , "" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			//request.setAttribute( "list" , list );
		}
		
		
		
		/* by hamid for appendixname setting*/
		
		appendixname=Jobdao.getAppendixname(getDataSource( request , "ilexnewDB" ),com.mind.dao.PRM.Jobdao.getAppendixid(activitydetailform.getJob_Id(),getDataSource( request , "ilexnewDB" )));
		
		
		request.setAttribute("appendixname",appendixname);
		request.setAttribute("appendixid",com.mind.dao.PRM.Jobdao.getAppendixid(activitydetailform.getJob_Id(),getDataSource( request , "ilexnewDB" )));
		/* by hamid*/
		
		if( request.getParameter( "changestatusflag" ) != null )
		{
			request.setAttribute( "changestatusflag" , request.getParameter( "changestatusflag" ) );
		}
		
		ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , Activity_Id , "Activity" );
		
		if( sowlist.size() > 0 )
		{
			sow_size = sowlist.size();	
		}
		
		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , Activity_Id , "Activity" );
		
		if( assumptionlist.size() > 0 ) {
			assumption_size = assumptionlist.size();	
		}
		
		request.setAttribute( "sowsize" , ""+sow_size );
		request.setAttribute( "assumptionsize" , ""+assumption_size );
		request.setAttribute( "sowlist" , sowlist );
		request.setAttribute( "assumptionlist" , assumptionlist );
		request.setAttribute( "list" , list );

		return mapping.findForward( "activitydetailpage" );
	
	}
	

	public ActionForward inscopedetailactivity( ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response ) throws Exception 
	{
		ActivityDetailForm activitydetailform = ( ActivityDetailForm )form;
		
		String Activity_Id = ""; 
		String checkforresourcelink = "";
		
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String list = "";
		int sow_size = 0;
		int assumption_size = 0;
		int uplift_size = 0;
		String from = "";
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		
		if( request.getParameter( "Activity_Id" ) != null )
		{
			Activity_Id = request.getParameter( "Activity_Id" );
		}
		
		if( request.getAttribute( "Activity_Id" ) != null )
		{
			Activity_Id = ( String ) request.getAttribute( "Activity_Id" );
		}
		
		if( request.getParameter( "from" ) != null )
		{
			from = request.getParameter( "from" );
			request.setAttribute( "from" , request.getParameter( "from" ) );
			request.setAttribute( "jobid" , request.getParameter( "jobid" ) );
		}
		
		
		checkforresourcelink = Resourcedao.Checkforlink( Activity_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "checkforresourcelink" , checkforresourcelink );
		
		Activity activity = Activitydao.getActivity( Activity_Id , "%" , getDataSource( request , "ilexnewDB" ) );
		
		activitydetailform.setJob_Id( activity.getJob_Id() );
		
		activitydetailform.setJobname( Activitydao.getJobname( getDataSource( request , "ilexnewDB" ) , activitydetailform.getJob_Id() ) );
		
		
		activitydetailform.setJob_type( Jobdao.getJobtype( activity.getJob_Id() , getDataSource( request , "ilexnewDB" ) ) );
		request.setAttribute( "job_type" , activitydetailform.getJob_type() );
		request.setAttribute( "Job_Id" , activitydetailform.getJob_Id() );
		
		activitydetailform.setActivity_Id( activity.getActivity_Id() );
		request.setAttribute( "Activity_Id" , activitydetailform.getActivity_Id() );
		
		activitydetailform.setEstimated_materialcost( activity.getEstimated_materialcost() );
		activitydetailform.setEstimated_cnslaborcost( activity.getEstimated_cnsfieldhqlaborcost() );
		activitydetailform.setEstimated_contractlaborcost( activity.getEstimated_contractfieldlaborcost() );
		activitydetailform.setEstimated_freightcost( activity.getEstimated_freightcost() );
		activitydetailform.setEstimated_travelcost( activity.getEstimated_travelcost() );
		activitydetailform.setEstimated_totalcost( activity.getEstimated_totalcost() );
		activitydetailform.setExtendedprice( activity.getExtendedprice() );
		activitydetailform.setListprice( activity.getListprice() );
		activitydetailform.setQuantity( activity.getQuantity() );
		activitydetailform.setOverheadcost( activity.getOverheadcost() );
		activitydetailform.setProformamargin( activity.getProformamargin() );
		activitydetailform.setLastchangeby( activity.getLastchangeby() ); 
		activitydetailform.setLastchangedate( activity.getLastchangedate() ); 
		activitydetailform.setLastcomment( activity.getLastcomment() );
		activitydetailform.setStatus( activity.getStatus() );
		activitydetailform.setType( activity.getActivitytype() );
		activitydetailform.setName( activity.getName() );
		
		activitydetailform.setChkaddendum( "inscopedetailactivity" );   // chk addendum flag
		activitydetailform.setChkaddendumresource( "Viewinscope" );		// chk addendum flag
		
		activitydetailform.setEstimated_unitcost(activity.getEstimated_unitcost());
		activitydetailform.setEstimated_unitprice(activity.getEstimated_unitprice());
		
		activitydetailform.setAppendix_id( activity.getAppendix_id() );
		activitydetailform.setAppendixname( activity.getAppendixname() );
		activitydetailform.setMsa_id( activity.getMsa_id() );
		activitydetailform.setMsaname( activity.getMsaname() );
		activitydetailform.setCommissionCost(activity.getCommissionCost());
		activitydetailform.setCommissionRevenue(activity.getCommissionRevenue());
		
		if( request.getParameter( "from" ) != null )
		{
			String jobid = request.getParameter( "jobid" );
			// Addendum Change 1.23
			if(activitydetailform.getJob_type().equals("Addendum") || activitydetailform.getJob_type().equals("Default")) {
				list = Menu.getStatus( "pm_act" , activitydetailform.getStatus().charAt( 0 ) , from , jobid , Activity_Id , "inscopedetailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			}	
			else {	
			activitydetailform.setStatus( JobDashboarddao.getActivityprojectstatus( Activity_Id , getDataSource( request , "ilexnewDB" ) ) );
			list = Menu.getStatus( "prj_act" , activitydetailform.getStatus().charAt( 0 ) , from , jobid , Activity_Id , "inscopedetailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			}
			request.setAttribute( "list" , list );
		}
		else
		{
			list = Menu.getStatus( "pm_act" , activitydetailform.getStatus().charAt( 0 ) , "" , "" , Activity_Id , "inscopedetailactivity" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "list" , list );
		}
		request.setAttribute( "list" , list );
		if( request.getAttribute( "changestatusflag" ) != null )
		{
			request.setAttribute( "changestatusflag" , request.getAttribute( "changestatusflag" ) );
		}
		
		String Appendix_Id ="";
		int checkNetmedXappendix = 0;
		
		Appendix_Id = Activitydao.getAppendixid(getDataSource( request , "ilexnewDB" ),activitydetailform.getJob_Id());
		checkNetmedXappendix = Activitydao.checkAppendixname(Appendix_Id, activitydetailform.getJob_Id(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("checknetmedx",checkNetmedXappendix+"");

		ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , Activity_Id , "Activity" );
		
		if( sowlist.size() > 0 )
		{
			sow_size = sowlist.size();	
		}
		
		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , Activity_Id , "Activity" );
		
		if( assumptionlist.size() > 0 )
		{
			assumption_size = assumptionlist.size();	
		}
		
		request.setAttribute( "sowsize" , ""+sow_size );
		request.setAttribute( "assumptionsize" , ""+assumption_size );
		
		request.setAttribute( "sowlist" , sowlist );
		request.setAttribute( "assumptionlist" , assumptionlist );
		
		ArrayList upliftFactorList = ManageUpliftdao.getUpliftFactorList(activitydetailform.getJob_Id(), Activity_Id, "Activity", getDataSource(request, "ilexnewDB"));
		if( upliftFactorList.size() > 0 )
				uplift_size = upliftFactorList.size();	
		
		request.setAttribute("uplift_size", ""+uplift_size);
		request.setAttribute("upliftFactorList", upliftFactorList);
		
		return mapping.findForward( "activitydetailpage" );
	
	}	
}
