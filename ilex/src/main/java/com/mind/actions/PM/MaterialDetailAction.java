package com.mind.actions.PM;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.MaterialResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.formbean.AM.MaterialResourceDetailForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

public class MaterialDetailAction extends com.mind.common.IlexDispatchAction {
	public ActionForward View(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MaterialResourceDetailForm materialresourcedetailform = (MaterialResourceDetailForm) form;

		MaterialResource materialresource = new MaterialResource();
		String material_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Material_Id") != null) {
			material_Id = request.getParameter("Material_Id");
		}

		if (request.getAttribute("Material_Id") != null) {
			material_Id = (String) request.getAttribute("Material_Id");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			materialresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		materialresourcedetailform.setMaterialid(material_Id);
		request.setAttribute("Material_Id",
				materialresourcedetailform.getMaterialid());

		// ArrayList materialresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "M" , materialresourcedetailform.getMaterialid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList materialresourcelist = Resourcedao.getResourcetabularlist(
				materialresourcedetailform.getActivity_Id(), "M",
				materialresourcedetailform.getMaterialid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		materialresource = (MaterialResource) materialresourcelist.get(0);

		materialresourcedetailform.setActivity_Id(materialresource
				.getActivity_Id());

		request.setAttribute("Activity_Id",
				materialresourcedetailform.getActivity_Id());

		materialresourcedetailform.setJob_id(Activitydao.getJobid(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getActivity_Id()));

		materialresourcedetailform.setMaterialid(materialresource
				.getMaterialid());
		materialresourcedetailform.setMaterialname(materialresource
				.getMaterialname());
		materialresourcedetailform.setMaterialtype(materialresource
				.getMaterialtype());
		materialresourcedetailform.setMinimumquantity(materialresource
				.getMinimumquantity());
		materialresourcedetailform.setCnspartnumber(materialresource
				.getCnspartnumber());
		materialresourcedetailform.setEstimatedtotalcost(materialresource
				.getEstimatedtotalcost());
		materialresourcedetailform.setEstimatedunitcost(materialresource
				.getEstimatedunitcost());
		materialresourcedetailform.setManufacturername(materialresource
				.getManufacturername());
		materialresourcedetailform.setManufacturerpartnumber(materialresource
				.getManufacturerpartnumber());
		materialresourcedetailform.setPriceextended(materialresource
				.getPriceextended());
		materialresourcedetailform
				.setPriceunit(materialresource.getPriceunit());
		materialresourcedetailform.setProformamargin(materialresource
				.getProformamargin());
		materialresourcedetailform.setQuantity(materialresource.getQuantity());
		materialresourcedetailform.setSellablequantity(materialresource
				.getSellablequantity());
		materialresourcedetailform.setStatus(materialresource.getStatus());

		materialresourcedetailform.setChkaddendum("View");
		materialresourcedetailform.setChkdetailactivity("detailactivity");

		materialresourcedetailform.setActivityname(Activitydao.getActivityname(
				materialresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setMsa_id(idname.getId());
		materialresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setAppendix_id(idname.getId());
		materialresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setJob_id(idname.getId());
		materialresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resM",
					materialresourcedetailform.getStatus().charAt(0), from,
					request.getParameter("jobid"), "View",
					materialresourcedetailform.getMaterialid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("pm_resM", materialresourcedetailform
					.getStatus().charAt(0), "", "", "",
					materialresourcedetailform.getMaterialid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		}
		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getMaterialid(), "pm_resM");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getMaterialid(), "pm_resM");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				materialresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				materialresourcedetailform.getChkaddendum());
		// End
		return mapping.findForward("materialdetailpage");
	}

	public ActionForward Viewinscope(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MaterialResourceDetailForm materialresourcedetailform = (MaterialResourceDetailForm) form;

		MaterialResource materialresource = new MaterialResource();
		String material_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;
		String resourceType = "IM";
		Map<String, Object> map;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Material_Id") != null) {
			material_Id = request.getParameter("Material_Id");
		}

		if (request.getAttribute("Material_Id") != null) {
			material_Id = (String) request.getAttribute("Material_Id");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			materialresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		materialresourcedetailform.setMaterialid(material_Id);
		request.setAttribute("Material_Id",
				materialresourcedetailform.getMaterialid());

		if (request.getParameter("jobType") != null
				&& request.getParameter("jobType").equals("Newjob")) {
			resourceType = "M";
		}

		// ArrayList materialresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "IM" , materialresourcedetailform.getMaterialid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList materialresourcelist = Resourcedao.getResourcetabularlist(
				materialresourcedetailform.getActivity_Id(), resourceType,
				materialresourcedetailform.getMaterialid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		materialresource = (MaterialResource) materialresourcelist.get(0);

		materialresourcedetailform.setActivity_Id(materialresource
				.getActivity_Id());
		request.setAttribute("Activity_Id",
				materialresourcedetailform.getActivity_Id());

		materialresourcedetailform.setMaterialid(materialresource
				.getMaterialid());
		materialresourcedetailform.setMaterialname(materialresource
				.getMaterialname());
		materialresourcedetailform.setMaterialtype(materialresource
				.getMaterialtype());
		materialresourcedetailform.setMinimumquantity(materialresource
				.getMinimumquantity());
		materialresourcedetailform.setCnspartnumber(materialresource
				.getCnspartnumber());
		materialresourcedetailform.setEstimatedtotalcost(materialresource
				.getEstimatedtotalcost());
		materialresourcedetailform.setEstimatedunitcost(materialresource
				.getEstimatedunitcost());
		materialresourcedetailform.setManufacturername(materialresource
				.getManufacturername());
		materialresourcedetailform.setManufacturerpartnumber(materialresource
				.getManufacturerpartnumber());
		materialresourcedetailform.setPriceextended(materialresource
				.getPriceextended());
		materialresourcedetailform
				.setPriceunit(materialresource.getPriceunit());
		materialresourcedetailform.setProformamargin(materialresource
				.getProformamargin());
		materialresourcedetailform.setQuantity(materialresource.getQuantity());
		materialresourcedetailform.setSellablequantity(materialresource
				.getSellablequantity());
		materialresourcedetailform.setStatus(materialresource.getStatus());

		materialresourcedetailform.setChkaddendum("Viewinscope");
		materialresourcedetailform
				.setChkdetailactivity("inscopedetailactivity");

		materialresourcedetailform.setActivityname(Activitydao.getActivityname(
				materialresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setMsa_id(idname.getId());
		materialresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setAppendix_id(idname.getId());
		materialresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setJob_id(idname.getId());
		materialresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resM",
					materialresourcedetailform.getStatus().charAt(0), from,
					request.getParameter("jobid"), "Viewinscope",
					materialresourcedetailform.getMaterialid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("jobdashboard_resM",
					materialresourcedetailform.getStatus().charAt(0), "", "",
					"Viewinscope", materialresourcedetailform.getMaterialid(),
					loginuserid, getDataSource(request, "ilexnewDB"));
		}
		request.setAttribute("list", list);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getMaterialid(), "pm_resM");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getMaterialid(), "pm_resM");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}
		if (request.getParameter("from") != null
				&& !request.getParameter("from").trim().equalsIgnoreCase("")
				&& request.getParameter("from").trim()
						.equalsIgnoreCase("jobdashboard")) {
			if (request.getParameter("jobid") != null
					&& !request.getParameter("jobid").trim()
							.equalsIgnoreCase("")) {
				map = DatabaseUtilityDao.setMenu(request.getParameter("jobid"),
						loginuserid, getDataSource(request, "ilexnewDB"));
			}
		}
		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);
		// For Resource Menu
		request.setAttribute("commonResourceStatus",
				materialresourcedetailform.getStatus());
		request.setAttribute("checkingRef",
				materialresourcedetailform.getChkaddendum());
		// End

		return mapping.findForward("materialdetailpage");
	}

	public ActionForward ViewAddendum(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MaterialResourceDetailForm materialresourcedetailform = (MaterialResourceDetailForm) form;

		MaterialResource materialresource = new MaterialResource();
		String material_Id = "";
		String activity_Id = "";
		String list = "";

		int sow_size = 0;
		int assumption_size = 0;
		NameId idname = null;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Material_Id") != null) {
			material_Id = request.getParameter("Material_Id");
		}

		if (request.getAttribute("Material_Id") != null) {
			material_Id = (String) request.getAttribute("Material_Id");
		}

		if (request.getParameter("addendum_id") != null) {
			materialresourcedetailform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getAttribute("addendum_id") != null) {
			materialresourcedetailform.setAddendum_id(request.getAttribute(
					"addendum_id").toString());
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			materialresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		materialresourcedetailform.setMaterialid(material_Id);
		request.setAttribute("Material_Id",
				materialresourcedetailform.getMaterialid());

		// ArrayList materialresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "AM" , materialresourcedetailform.getMaterialid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList materialresourcelist = Resourcedao.getResourcetabularlist(
				materialresourcedetailform.getActivity_Id(), "AM",
				materialresourcedetailform.getMaterialid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));

		materialresource = (MaterialResource) materialresourcelist.get(0);

		materialresourcedetailform.setActivity_Id(materialresource
				.getActivity_Id());
		request.setAttribute("Activity_Id",
				materialresourcedetailform.getActivity_Id());

		materialresourcedetailform.setMaterialid(materialresource
				.getMaterialid());
		materialresourcedetailform.setMaterialname(materialresource
				.getMaterialname());
		materialresourcedetailform.setMaterialtype(materialresource
				.getMaterialtype());
		materialresourcedetailform.setMinimumquantity(materialresource
				.getMinimumquantity());
		materialresourcedetailform.setCnspartnumber(materialresource
				.getCnspartnumber());
		materialresourcedetailform.setEstimatedtotalcost(materialresource
				.getEstimatedtotalcost());
		materialresourcedetailform.setEstimatedunitcost(materialresource
				.getEstimatedunitcost());
		materialresourcedetailform.setManufacturername(materialresource
				.getManufacturername());
		materialresourcedetailform.setManufacturerpartnumber(materialresource
				.getManufacturerpartnumber());
		materialresourcedetailform.setPriceextended(materialresource
				.getPriceextended());
		materialresourcedetailform
				.setPriceunit(materialresource.getPriceunit());
		materialresourcedetailform.setProformamargin(materialresource
				.getProformamargin());
		materialresourcedetailform.setQuantity(materialresource.getQuantity());
		materialresourcedetailform.setSellablequantity(materialresource
				.getSellablequantity());
		materialresourcedetailform.setStatus(materialresource.getStatus());

		materialresourcedetailform.setChkaddendum("ViewAddendum");
		materialresourcedetailform
				.setChkdetailactivity("detailaddendumactivity");

		materialresourcedetailform.setActivityname(Activitydao.getActivityname(
				materialresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setMsa_id(idname.getId());
		materialresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setAppendix_id(idname.getId());
		materialresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setJob_id(idname.getId());
		materialresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resM",
					materialresourcedetailform.getStatus().charAt(0), from,
					request.getParameter("jobid"), "ViewAddendum",
					materialresourcedetailform.getMaterialid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			// list = Menu.getStatus( "pm_resM" ,
			// materialresourcedetailform.getStatus().charAt( 0 ) , "" , "" , ""
			// , materialresourcedetailform.getMaterialid() , loginuserid ,
			// getDataSource( request , "ilexnewDB" ) );

			if (materialresourcedetailform.getStatus().equals("Draft")) {
				list = "\"Approved\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ materialresourcedetailform.getAddendum_id()
						+ "&type=pm_resM&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
						+ materialresourcedetailform.getMaterialid()
						+ "&Status=A\"" + "," + "0";
			}

			if (materialresourcedetailform.getStatus().equals("Approved")) {
				list = "\"Draft\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ materialresourcedetailform.getAddendum_id()
						+ "&type=pm_resM&ref=Fromresourcelevel&ref1=ViewAddendum&Id="
						+ materialresourcedetailform.getMaterialid()
						+ "&Status=D\"" + "," + "0";
			}

		}

		/*
		 * if( materialresourcedetailform.getStatus().equals( "Draft" ) ) { list
		 * = "\"Approved\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +materialresourcedetailform.getAddendum_id()+
		 * "&type=pm_resM&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +materialresourcedetailform.getMaterialid()+"&Status=A\"" + "," +
		 * "0"; }
		 * 
		 * if( materialresourcedetailform.getStatus().equals( "Approved" ) ) {
		 * list = "\"Draft\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +materialresourcedetailform.getAddendum_id()+
		 * "&type=pm_resM&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +materialresourcedetailform.getMaterialid()+"&Status=D\"" + "," +
		 * "0"; }
		 */

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getMaterialid(), "pm_resM");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getMaterialid(), "pm_resM");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);

		request.setAttribute("list", list);

		return mapping.findForward("materialdetailpage");
	}

	public ActionForward ViewChangeorder(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		MaterialResourceDetailForm materialresourcedetailform = (MaterialResourceDetailForm) form;

		MaterialResource materialresource = new MaterialResource();
		String material_Id = "";
		String activity_Id = "";
		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String from = "";
		NameId idname = null;
		int sow_size = 0;
		int assumption_size = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("Material_Id") != null) {
			material_Id = request.getParameter("Material_Id");
		}

		if (request.getAttribute("Material_Id") != null) {
			material_Id = (String) request.getAttribute("Material_Id");
		}

		if (request.getParameter("addendum_id") != null) {
			materialresourcedetailform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getAttribute("addendum_id") != null) {
			materialresourcedetailform.setAddendum_id(request.getAttribute(
					"addendum_id").toString());
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
		}

		if (request.getParameter("Activity_Id") != null) {
			materialresourcedetailform.setActivity_Id(request
					.getParameter("Activity_Id"));
		}

		materialresourcedetailform.setMaterialid(material_Id);
		request.setAttribute("Material_Id",
				materialresourcedetailform.getMaterialid());

		// ArrayList materialresourcelist = Resourcedao.getResourcetabularlist(
		// "%" , "CM" , materialresourcedetailform.getMaterialid() , "" ,
		// request.getSession().getId(),getDataSource( request , "ilexnewDB" )
		// );
		ArrayList materialresourcelist = Resourcedao.getResourcetabularlist(
				materialresourcedetailform.getActivity_Id(), "CM",
				materialresourcedetailform.getMaterialid(), "", request
						.getSession().getId(), "",
				getDataSource(request, "ilexnewDB"));
		materialresource = (MaterialResource) materialresourcelist.get(0);

		materialresourcedetailform.setActivity_Id(materialresource
				.getActivity_Id());
		request.setAttribute("Activity_Id",
				materialresourcedetailform.getActivity_Id());

		materialresourcedetailform.setMaterialid(materialresource
				.getMaterialid());
		materialresourcedetailform.setMaterialname(materialresource
				.getMaterialname());
		materialresourcedetailform.setMaterialtype(materialresource
				.getMaterialtype());
		materialresourcedetailform.setMinimumquantity(materialresource
				.getMinimumquantity());
		materialresourcedetailform.setCnspartnumber(materialresource
				.getCnspartnumber());
		materialresourcedetailform.setEstimatedtotalcost(materialresource
				.getEstimatedtotalcost());
		materialresourcedetailform.setEstimatedunitcost(materialresource
				.getEstimatedunitcost());
		materialresourcedetailform.setManufacturername(materialresource
				.getManufacturername());
		materialresourcedetailform.setManufacturerpartnumber(materialresource
				.getManufacturerpartnumber());
		materialresourcedetailform.setPriceextended(materialresource
				.getPriceextended());
		materialresourcedetailform
				.setPriceunit(materialresource.getPriceunit());
		materialresourcedetailform.setProformamargin(materialresource
				.getProformamargin());
		materialresourcedetailform.setQuantity(materialresource.getQuantity());
		materialresourcedetailform.setSellablequantity(materialresource
				.getSellablequantity());
		materialresourcedetailform.setStatus(materialresource.getStatus());

		materialresourcedetailform.setChkaddendum("ViewChangeorder");
		materialresourcedetailform
				.setChkdetailactivity("detailchangeorderactivity");

		materialresourcedetailform.setActivityname(Activitydao.getActivityname(
				materialresourcedetailform.getActivity_Id(),
				getDataSource(request, "ilexnewDB")));
		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource", "MSA",
				getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setMsa_id(idname.getId());
		materialresourcedetailform.setMsaname(idname.getName());

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource",
				"Appendix", getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setAppendix_id(idname.getId());
		materialresourcedetailform.setAppendixname(idname.getName());

		idname = Activitydao.getIdName(
				materialresourcedetailform.getActivity_Id(), "Resource", "Job",
				getDataSource(request, "ilexnewDB"));
		materialresourcedetailform.setJob_id(idname.getId());
		materialresourcedetailform.setJobname(idname.getName());

		if (request.getParameter("from") != null) {
			list = Menu.getStatus("jobdashboard_resM",
					materialresourcedetailform.getStatus().charAt(0), "from",
					request.getParameter("jobid"), "ViewChangeorder",
					materialresourcedetailform.getMaterialid(), loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			// list = Menu.getStatus( "pm_resM" ,
			// materialresourcedetailform.getStatus().charAt( 0 ) , "" , "" , ""
			// , materialresourcedetailform.getMaterialid() , loginuserid ,
			// getDataSource( request , "ilexnewDB" ) );

			if (materialresourcedetailform.getStatus().equals("Draft")) {
				list = "\"Approved\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ materialresourcedetailform.getAddendum_id()
						+ "&type=pm_resM&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
						+ materialresourcedetailform.getMaterialid()
						+ "&Status=A\"" + "," + "0";
			}

			if (materialresourcedetailform.getStatus().equals("Approved")) {
				list = "\"Draft\""
						+ ","
						+ "\"AddendumChangeStatusAction.do?addendum_id="
						+ materialresourcedetailform.getAddendum_id()
						+ "&type=pm_resM&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
						+ materialresourcedetailform.getMaterialid()
						+ "&Status=D\"" + "," + "0";
			}
		}

		/*
		 * if( materialresourcedetailform.getStatus().equals( "Draft" ) ) { list
		 * = "\"Approved\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +materialresourcedetailform.getAddendum_id()+
		 * "&type=pm_resM&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +materialresourcedetailform.getMaterialid()+"&Status=A\"" + "," +
		 * "0"; }
		 * 
		 * if( materialresourcedetailform.getStatus().equals( "Approved" ) ) {
		 * list = "\"Draft\"" + "," +
		 * "\"AddendumChangeStatusAction.do?addendum_id="
		 * +materialresourcedetailform.getAddendum_id()+
		 * "&type=pm_resM&ref=Fromresourcelevel&ref1=ViewChangeorder&Id="
		 * +materialresourcedetailform.getMaterialid()+"&Status=D\"" + "," +
		 * "0"; }
		 */

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getMaterialid(), "pm_resM");

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"),
				materialresourcedetailform.getMaterialid(), "pm_resM");

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		request.setAttribute("sowsize", "" + sow_size);
		request.setAttribute("assumptionsize", "" + assumption_size);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("assumptionlist", assumptionlist);

		request.setAttribute("list", list);

		return mapping.findForward("materialdetailpage");
	}
}
