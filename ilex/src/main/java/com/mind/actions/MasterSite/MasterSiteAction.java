package com.mind.actions.MasterSite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.LabelValue;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.MasterSite.MasterSiteDAO;
import com.mind.formbean.MasterSite.MasterSiteForm;
import com.mind.util.WebUtil;

public class MasterSiteAction extends com.mind.common.IlexDispatchAction {

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		MasterSiteForm masterSiteForm = (MasterSiteForm) form;

		/* Page Security Start */
		HttpSession session = request.getSession(true);

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");

		if (masterSiteForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage PVS",
					"Search Partner", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		masterSiteForm.setSiteMasterClientList(MasterSiteDAO
				.getAllClientsList(getDataSource(request, "ilexnewDB")));

		masterSiteForm.setSiteMasterSitesList(populateSites(masterSiteForm,
				request));

		if (masterSiteForm.getSiteId() == null
				|| masterSiteForm.getSiteId().equals("")) {
			if (masterSiteForm.getSiteName() != null
					&& !masterSiteForm.getSiteName().equals("")) {

				String delemiter = "-,-,-";

				masterSiteForm.setSiteName(masterSiteForm.getSiteName()
						.replace(delemiter, "#"));

				masterSiteForm.setSiteNameLabel(masterSiteForm.getSiteName());

				if (masterSiteForm.getMmId() == null
						|| masterSiteForm.getMmId().equals("")) {
					masterSiteForm.setMmId("0");
				}

				String[] st = MasterSiteDAO.getSiteDetailsBySiteNumber(
						masterSiteForm.getSiteName().trim(),
						masterSiteForm.getMmId(),
						getDataSource(request, "ilexnewDB"));
				if (st != null && st.length > 0) {
					masterSiteForm.setSiteId(st[0]);
					masterSiteForm.setMmId(st[1]);
				} else {
					masterSiteForm.setSiteNameLabel("Site: "
							+ masterSiteForm.getSiteName() + " not found.");
					return mapping.findForward("success");
				}

			} else {
				return mapping.findForward("success");
			}
		}
		String[] siteDetails = MasterSiteDAO
				.getSiteDetails(masterSiteForm.getSiteId(),
						getDataSource(request, "ilexnewDB"));

		String data = null;
		try {
			String myURL = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=site_assets&site_id="
					+ masterSiteForm.getSiteId();
			data = WebUtil.getDataFromWeb(myURL, 9999, "\n");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		String[] dat = null;
		if (data != null && (!data.equals(""))
				&& !data.contains("mysql connection failed")) {
			dat = data.split("~");
		}
		if (dat != null) {
			for (int i = 0; i < dat.length; i++) {
				String[] ar = dat[i].split("\\|");
				masterSiteForm.getAssetsDeployedList().add(ar);
			}
		}

		masterSiteForm.setSiteName(siteDetails[0]);
		masterSiteForm.setSiteNameLabel(siteDetails[0]);
		masterSiteForm.setSiteAddress(siteDetails[1]);
		masterSiteForm.setPhoneNumber(siteDetails[2]);
		masterSiteForm.setPrimaryContactEmail(siteDetails[3]);
		masterSiteForm.setPrimaryContactName(siteDetails[4]);
		masterSiteForm.setMmId(masterSiteForm.getMmId());
		masterSiteForm.setSiteId(masterSiteForm.getSiteId());

		masterSiteForm.setHelpDeskDetailsList(MasterSiteDAO
				.getHelpDeskTicketDetails(masterSiteForm.getSiteId(),
						getDataSource(request, "ilexnewDB")));

		masterSiteForm
				.setJobDetailsList(MasterSiteDAO.getJobDetails(
						masterSiteForm.getSiteId(),
						getDataSource(request, "ilexnewDB")));

		masterSiteForm.setDeliverableDetailsList(MasterSiteDAO
				.getDeliverableDetails(masterSiteForm.getSiteId(),
						getDataSource(request, "ilexnewDB")));

		masterSiteForm
				.setkIPDtlList(MasterSiteDAO.getKPIData(
						masterSiteForm.getSiteId(),
						getDataSource(request, "ilexnewDB")));

		masterSiteForm.setMasterSiteDeviceAttributelst(MasterSiteDAO
				.getDeviceAttributeBysite(masterSiteForm.getSiteId(),
						getDataSource(request, "ilexnewDB")));

		ArrayList<String> docIdList = MasterSiteDAO
				.getDeliverablesImgData(masterSiteForm.getSiteId(),
						getDataSource(request, "ilexnewDB"));
		for (int i = 0; i < docIdList.size(); i++) {
			request.setAttribute("ImgId" + i, docIdList.get(i));
		}

		masterSiteForm.setSiteMasterSitesList(populateSites(masterSiteForm,
				request));

		return mapping.findForward("success");
	}

	public ActionForward advancedSiteSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		MasterSiteForm masterSiteForm = (MasterSiteForm) form;

		/* Page Security Start */
		HttpSession session = request.getSession(true);

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");

		if (masterSiteForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage PVS",
					"Search Partner", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		if (masterSiteForm.getSiteCity() == null) {
			masterSiteForm.setSiteCity("");
		}
		if (masterSiteForm.getSiteState() == null) {
			masterSiteForm.setSiteState("");
		}
		if (masterSiteForm.getSiteZip() == null) {
			masterSiteForm.setSiteZip("");
		}
		if (masterSiteForm.getSiteAdress() == null) {
			masterSiteForm.setSiteAdress("");
		}

		if (masterSiteForm.getSiteCity().equals("")
				&& masterSiteForm.getSiteState().equals("")
				&& masterSiteForm.getSiteZip().equals("")
				&& masterSiteForm.getSiteAdress().equals("")) {
			return mapping.findForward("advSerach");
		}

		ArrayList<String[]> sitesList = MasterSiteDAO.getSitesByAddress(
				masterSiteForm.getSiteCity(), masterSiteForm.getSiteState(),
				masterSiteForm.getSiteZip(), masterSiteForm.getSiteAdress(),
				getDataSource(request, "ilexnewDB"));

		masterSiteForm.setSitesList(sitesList);

		return mapping.findForward("advSerach");
	}

	public ActionForward updatePOByAjax(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String siteName = (String) request.getParameter("siteName");

		String siteSearchType = (String) request.getParameter("searchType");

		List<String> st = null;
		if (siteSearchType.equals("site")) {
			st = MasterSiteDAO.getAllSSiteBySiteNumber(siteName.trim(),
					getDataSource(request, "ilexnewDB"));
		} else {
			st = MasterSiteDAO.getAllSSiteBySiteAddress(siteName.trim(),
					getDataSource(request, "ilexnewDB"));
		}

		JSONArray jsonObject = new JSONArray();
		jsonObject = JSONArray.fromObject(st);
		response.setContentType("text/plain"); // Set content type of the
		// response so that jQuery knows
		// what it can expect.
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonObject.toString());

		return null;
	}

	private Collection populateSites(MasterSiteForm masterSiteForm,
			HttpServletRequest request) {
		if (masterSiteForm.getMmId() != null
				&& !masterSiteForm.getMmId().equals("")) {

			return MasterSiteDAO.getAllSitesByClient(masterSiteForm.getMmId(),
					getDataSource(request, "ilexnewDB"));
		} else {
			/*
			 * Putting empty collection to avoid null pointer exception
			 */
			return new ArrayList<LabelValue>();
		}

	}
}