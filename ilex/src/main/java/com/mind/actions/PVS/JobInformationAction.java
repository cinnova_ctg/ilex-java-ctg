package com.mind.actions.PVS;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pvs.JobInformationBean;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.JobInformationForm;

public class JobInformationAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		JobInformationForm jobInformationForm = (JobInformationForm) form;
		int sitesJobSize = 0;

		ArrayList jobandsitelist = new ArrayList();

		HttpSession session = request.getSession(true);

		// Start :Added By Amit ,Added For maintaining Back on Job Information
		// screen
		if (request.getParameter("temppage") != null) {
			if (session.getAttribute("countersort") != null) {
				session.setAttribute("countersort", "-1");
			}
		} else {

			int counter = -1;
			if (session.getAttribute("countersort") != null) {
				counter = Integer.parseInt((String) session
						.getAttribute("countersort"));
				counter = counter - 1;
				session.setAttribute("countersort", "" + counter);
			} else {
				counter = -1;
			}

		}
		// End

		if (request.getAttribute("from") != null) {
			jobInformationForm.setFrom((String) request.getAttribute("from"));
		} else {
			jobInformationForm.setFrom(null);
		}
		if (request.getParameter("from") != null) {
			jobInformationForm.setFrom(request.getParameter("from"));
		} else {
			jobInformationForm.setFrom(null);
		}

		if (request.getParameter("pid") != null) {
			jobInformationForm.setPartnerid(request.getParameter("pid"));
		}
		if (request.getAttribute("pid") != null) {
			jobInformationForm.setPartnerid((String) request
					.getAttribute("pid"));
		}

		String sortqueryclause = "";

		if (request.getAttribute("sortwindow") == null) {
			sortqueryclause = "order by powo_id ";
			session.setAttribute("jobinfopartner_sort", sortqueryclause);
		}

		/*
		 * if ( request.getAttribute( "querystring" ) != null ) {
		 * sortqueryclause = ( String ) request.getAttribute( "querystring" ); }
		 */
		sortqueryclause = getSortQueryClause(sortqueryclause, session);

		JobInformationBean jobInformationBean = new JobInformationBean();
		BeanUtils.copyProperties(jobInformationBean, jobInformationForm);

		jobInformationBean = Pvsdao.getPartnerInformation(jobInformationBean,
				jobInformationForm.getPartnerid(),
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(jobInformationForm, jobInformationBean);
		jobandsitelist = Pvsdao.getjobInformationbyPartnerId(
				jobInformationForm.getPartnerid(), sortqueryclause,
				getDataSource(request, "ilexnewDB"));

		if (jobandsitelist != null) {

			if (jobandsitelist.size() > 0) {
				sitesJobSize = jobandsitelist.size();
			}
		}

		request.setAttribute("sitesJobSize", new Integer(sitesJobSize));
		request.setAttribute("siteJobList", jobandsitelist);
		request.setAttribute("topPartnerName",
				jobInformationForm.getPartnerType());

		return mapping.findForward("success");

	}

	public String getSortQueryClause(String queryclause, HttpSession session) {
		if (session.getAttribute("jobinfopartner_sort") != null)
			queryclause = (String) session.getAttribute("jobinfopartner_sort");
		return queryclause;
	}

}
