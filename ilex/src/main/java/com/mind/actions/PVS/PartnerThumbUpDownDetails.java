package com.mind.actions.PVS;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pvs.PartnerDetailBean;
import com.mind.common.IlexAction;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.PartnerDetailForm;

public class PartnerThumbUpDownDetails extends IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);

		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired

		String userid = (String) session.getAttribute("userid");
		PartnerDetailForm partnerdetailform = (PartnerDetailForm) form;

		PartnerDetailBean partnerDetailBean = new PartnerDetailBean();
		if (request.getParameter("partnerId") == null
				&& request.getParameter("poId") != null) {
			partnerdetailform.setPartnerId(Pvsdao.getPartnerForPO(
					request.getParameter("poId"),
					getDataSource(request, "ilexnewDB")));
		}

		String upDownFlag = "up";
		if (request.getParameter("upDownFlagVal") != null) {
			upDownFlag = request.getParameter("upDownFlagVal");
		}
		String partnerID = request.getParameter("partnerId") == null ? "0"
				: request.getParameter("partnerId").trim();
		upDownFlag = upDownFlag.equals("up") ? "Y" : "N";
		BeanUtils.copyProperties(partnerDetailBean, partnerdetailform);

		partnerdetailform.setThumbUpDownList(Pvsdao.getThumbUpOrDownComments(
				partnerID, upDownFlag, getDataSource(request, "ilexnewDB")));
		response.setContentType("text/html");
		RequestDispatcher rd = request
				.getRequestDispatcher("/PVS/ThumbUpDownDtlPage.jsp");
		rd.include(request, response);
		return null;

	}
}
