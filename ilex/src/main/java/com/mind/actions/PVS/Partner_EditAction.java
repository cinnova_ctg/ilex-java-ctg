package com.mind.actions.PVS;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.PVSToolList;
import com.mind.bean.PVS_Partner_Edit_TechnicianInfo;
import com.mind.bean.RecRest_History;
import com.mind.bean.pvs.Partner_EditBean;
import com.mind.common.EnvironmentSelector;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.Email;
import com.mind.common.dao.RestClient;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.CoreCompetancyBean;
import com.mind.formbean.PVS.Partner_EditForm;
import com.mind.util.WebUtil;

/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 **/

/**
 * 
 * Class Explanation: Partner_EditAction is the action class for
 * Partner_Edit.jsp. It extends com.mind.common.IlexDispatchActon and its
 * methods are called based on the request parameter"function".
 * 
 **/

public class Partner_EditAction extends com.mind.common.IlexAction {

	private static final Logger logger = Logger
			.getLogger(Partner_EditAction.class);

	/*
	 * This is the ID of competence named CCTV
	 */
	private static final int CCTV_COMPETENCY_ID = 28;
	/*
	 * This is the score of 8 correct answer, if any partner have score greater
	 * then equal to this he must get the CCTV competence check box checked.
	 */
	private static final float COMPETENCY_SCORE = 61.5f;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = new ActionForward();// return value
		Partner_EditForm partner_EditForm = (Partner_EditForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");
		int retvaluedelete = 0;
		String page = "";
		String check = "";
		String url = "";
		ArrayList initialStateCategory = new ArrayList();
		ArrayList LogHistoryList = new ArrayList();
		int retvalueDeleteForMYSQL = -1;
		int retvalueUpdateStatus = -1;
		boolean MySql = false;
		boolean sendMail = false;
		if (null != partner_EditForm.getUploadedDocType()
				&& null != (partner_EditForm.getBrowse())) {

			if (!"".equalsIgnoreCase(partner_EditForm.getBrowse().getFileName()
					.trim())) {
				if ("w9Uploaded".equalsIgnoreCase(partner_EditForm
						.getUploadedDocType())) {
					partner_EditForm.setRdoW9("Y");
					partner_EditForm.setW9Uploaded("Y");
					partner_EditForm.setFileuploadStatus("2");
				} else if ("insuranceDocUploaded"
						.equalsIgnoreCase(partner_EditForm.getUploadedDocType())) {
					partner_EditForm.setIsInsuranceSubmitted("Y");
					partner_EditForm.setIsInsuranceUploaded("Y");
					partner_EditForm.setFileuploadStatus("2");
				} else if ("resumeDocUploaded"
						.equalsIgnoreCase(partner_EditForm.getUploadedDocType())) {
					partner_EditForm.setIsResumeSubmitted("Y");
					partner_EditForm.setIsResumeUploaded("Y");
					partner_EditForm.setFileuploadStatus("2");
				}
				// condition add regarding new changes
				else if ("backgroundCheckUploaded"
						.equalsIgnoreCase(partner_EditForm.getUploadedDocType())) {
					partner_EditForm.setBackgroundCheckSubmitted("Y");
					partner_EditForm.setBackgroundCheckUploaded("Y");
					partner_EditForm.setFileuploadStatus("2");
				}

				else if ("drugScreenUploaded".equalsIgnoreCase(partner_EditForm
						.getUploadedDocType())) {
					partner_EditForm.setDrugScreenCompletionSubmitted("Y");
					partner_EditForm.setDrugScreenCompletionUploaded("Y");
					partner_EditForm.setFileuploadStatus("2");
				}

				// End of changes
			}
		}

		if (request.getParameter("check") != null)
			check = request.getParameter("check");

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (partner_EditForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage PVS",
					"Edit Partner", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		String deleteFileValue = request.getParameter("isDeleteFile");
		if (deleteFileValue != null && deleteFileValue.equals("DeleteFile")) {
			Pvsdao.deleteFileByFileId(request.getParameter("file_id"), ds);
			String fileRemarks = (String) request.getParameter("fileRemarks");
			String pid = (String) request.getParameter("pid");
			int count = Pvsdao.getFileCount(fileRemarks, pid, ds);
			if (count == 0) {
				// update the relevant fields in the database
				Pvsdao.updateFileStatus(pid, fileRemarks, ds);
			}
		}
		String[] arr;

		// case check for insert Recommend
		if (request.getParameter("checkProvidedPid") != null) {

			String listId;
			listId = request.getParameter("checkProvidedPid");
			arr = listId.split(",");
			partner_EditForm.setPid(request.getParameter("Pid"));
			for (int i = 0; i < arr.length; i++) {
				if (!arr[i].equals("")) {

					Pvsdao.checkpartnerInsert(partner_EditForm.getPid(), "1",
							arr[i].equals("") ? "0" : arr[i], null, session
									.getAttribute("userid").toString(), ds);

				}

			}

		}

		// case check for restrict Insert
		if (request.getParameter("checkProvidedPidRI") != null) {

			String listId;
			listId = request.getParameter("checkProvidedPidRI");
			arr = listId.split(",");
			partner_EditForm.setPid(request.getParameter("Pid"));
			for (int i = 0; i < arr.length; i++) {
				if (!arr[i].equals("")) {

					Pvsdao.checkpartnerInsert(partner_EditForm.getPid(), "0",
							arr[i].equals("") ? "0" : arr[i], null, session
									.getAttribute("userid").toString(), ds);

				}

			}

		}

		// Check Case Recommend delete
		if (request.getParameter("checkProvidedPidRD") != null) {
			String listId;
			listId = request.getParameter("checkProvidedPidRD");
			arr = listId.split(",");
			partner_EditForm.setPid(request.getParameter("Pid"));
			for (int i = 0; i < arr.length; i++) {
				if (!arr[i].equals("")) {
					Pvsdao.checkpartnerDelete(partner_EditForm.getPid(), "1",
							arr[i], ds);

				}

			}

		}

		if (request.getParameter("checkProvidedPidRDD") != null) {
			String listId;
			listId = request.getParameter("checkProvidedPidRDD");
			arr = listId.split(",");
			partner_EditForm.setPid(request.getParameter("Pid"));
			for (int i = 0; i < arr.length; i++) {
				if (!arr[i].equals("")) {
					Pvsdao.checkpartnerDelete(partner_EditForm.getPid(), "0",
							arr[i], ds);

				}

			}

		}

		logger.debug("Time Start Check0:");
		partner_EditForm.setPartnerListCount("");

		partner_EditForm.setAllPartnersForRecommendedCol(Pvsdao.getAllPartners(
				1, partner_EditForm.getPid(), ds));

		partner_EditForm.setAllPartnersForRestrictedCol(Pvsdao.getAllPartners(
				0, partner_EditForm.getPid(), ds));

		partner_EditForm.setSelectedPartnersFormRecommendedCol(Pvsdao
				.getRecommendedRestrictedPartner(1, partner_EditForm.getPid(),
						ds));

		partner_EditForm.setSelectedPartnersForRestrictedCol(Pvsdao
				.getRecommendedRestrictedPartner(0, partner_EditForm.getPid(),
						ds));

		int roleCount = Pvsdao.getCurrentUserRoleCountForHRRole(userid, ds);
		request.setAttribute("isHRUser", roleCount);
		logger.debug("Time End Check0:");

		if (request.getParameter("fromtype") != null) {
			if (request.getParameter("fromtype").equalsIgnoreCase("powo")) {
				partner_EditForm.setFromtype(request.getParameter("fromtype"));
				if (request.getParameter("jobid") != null) {
					partner_EditForm.setJobid(request.getParameter("jobid"));
				}
			}
		}

		if (request.getParameter("pid") != null)
			partner_EditForm.setPid(request.getParameter("pid"));
		if (request.getParameter("orgTopName") != null)
			partner_EditForm.setTopPartnerName(request
					.getParameter("orgTopName"));
		if (request.getParameter("function") != null)
			partner_EditForm.setAct(request.getParameter("function"));
		if (request.getParameter("frompage") != null)
			partner_EditForm.setFromPage(request.getParameter("frompage"));
		if (request.getParameter("formPVSSearch") != null)
			partner_EditForm.setFromPVSSearch(request
					.getParameter("formPVSSearch"));

		// START: Setting Partner Registration status in session
		if (request.getParameter("pid") != null) {
			request.getSession(true).setAttribute(
					"partnerRegStatus",
					Pvsdao.checkPartnerRegStatus(request.getParameter("pid"),
							getDataSource(request, "ilexnewDB")));
		}
		// END: Setting Partner Registration status in session

		if (null != request.getParameter("pid")) {

			String status = Pvsdao.getPartnersStatus(
					request.getParameter("pid"), ds);

			if (status.equals("Printed")) {

				partner_EditForm.setPhotoIdentificationID("Y");

			} else {

				partner_EditForm.setPhotoIdentificationID("N");

			}
		}
		if (request.getParameter("func") != null
				&& request.getParameter("func").equals("delete")) {// delete
																	// partner
			int chekPoAssing = Pvsdao.checkPOAssign(partner_EditForm.getPid(),
					ds);
			if (chekPoAssing == 0) { // true when po is not assign.

				url = EnvironmentSelector
						.getBundleString("appendix.detail.changestatus.mysql.url");
				retvalueDeleteForMYSQL = Pvsdao.DeletePartnerInMySql(
						partner_EditForm.getPid(), url);
				if (retvalueDeleteForMYSQL < 0) {
					if (partner_EditForm.getFromPage() != null
							&& partner_EditForm.getFromPage().equals(
									"pvssearch")) {
						request.setAttribute("deletemsgmysql",
								retvalueDeleteForMYSQL + "");
						return (mapping.findForward("pvssearchpage"));
					} else {
						request.setAttribute("deletemsgmysql",
								retvalueDeleteForMYSQL + "");
						MySql = true;
					}
				} else {

					retvaluedelete = Pvsdao.deletePartnerDetail(
							partner_EditForm.getPid(), ds);
					if (retvaluedelete == 0
							&& partner_EditForm.getFromPage() != null
							&& partner_EditForm.getFromPage().equals(
									"pvssearch")) {
						request.setAttribute("pvssearch", "yes");
						request.setAttribute("refreshtree", "true");
						request.setAttribute("partnerDeleted", "yes");
						return (mapping.findForward("pvssearchpage"));
					}
					if (retvaluedelete == 0) {
						page = "searchpage";
						request.setAttribute("type", page);
						request.setAttribute("partnerDeleted", "yes");
						request.setAttribute("refreshtree", "yes");
						return (mapping.findForward("partnerstatistics"));

					} else {
						request.setAttribute("deletemsg", retvaluedelete + "");
						request.setAttribute("PartnerDeleted", "no");
					}
					// Delete from ilex: Start
				}
			}// end chekPoAssing
			else {
				request.setAttribute("deletemsg", chekPoAssing + "");
				request.setAttribute("PartnerDeleted", "no");
				retvaluedelete = chekPoAssing;
			}
		}// end delete partner

		// / Status updated code

		if (request.getParameter("func") != null
				&& request.getParameter("func").equals("Received")) {

			String file_id = (String) request.getParameter("file_id");
			String pid = (String) request.getParameter("pid");
			String Status = "1";
			retvalueUpdateStatus = Pvsdao.setupdateFileStatus(file_id, Status,
					pid, ds);
			if (retvalueUpdateStatus == 1) {
				if (partner_EditForm.getFromPage() != null
						&& partner_EditForm.getFromPage().equals("pvssearch")) {
					request.setAttribute("pvssearch", "yes");
					request.setAttribute("refreshtree", "true");
					request.setAttribute("partnerDeleted", "yes");
					return (mapping.findForward("pvssearchpage"));

				}
			}

		}

		// code Added for Archieved
		if (request.getParameter("func") != null
				&& request.getParameter("func").equals("Archived")) {

			String file_id = (String) request.getParameter("file_id");
			String pid = (String) request.getParameter("pid");
			String Status = "4";
			retvalueUpdateStatus = Pvsdao.setupdateFileStatus(file_id, Status,
					pid, ds);
			if (retvalueUpdateStatus == 1) {
				if (partner_EditForm.getFromPage() != null
						&& partner_EditForm.getFromPage().equals("pvssearch")) {
					request.setAttribute("pvssearch", "yes");
					request.setAttribute("refreshtree", "true");
					request.setAttribute("partnerDeleted", "yes");
					return (mapping.findForward("pvssearchpage"));

				}
			}

		}

		// code Added by Approved
		if (request.getParameter("func") != null
				&& request.getParameter("func").equals("Approved")) {

			String file_id = (String) request.getParameter("file_id");
			String pid = (String) request.getParameter("pid");
			String Status = "2";
			retvalueUpdateStatus = Pvsdao.setupdateFileStatus(file_id, Status,
					pid, ds);
			if (retvalueUpdateStatus == 1) {
				if (partner_EditForm.getFromPage() != null
						&& partner_EditForm.getFromPage().equals("pvssearch")) {
					request.setAttribute("pvssearch", "yes");
					request.setAttribute("refreshtree", "true");
					request.setAttribute("partnerDeleted", "yes");
					return (mapping.findForward("pvssearchpage"));

				}
			}

		}

		// code addded for Denied

		if (request.getParameter("func") != null
				&& request.getParameter("func").equals("Denied")) {

			String file_id = (String) request.getParameter("file_id");
			String pid = (String) request.getParameter("pid");
			String Status = "3";
			retvalueUpdateStatus = Pvsdao.setupdateFileStatus(file_id, Status,
					pid, ds);
			if (retvalueUpdateStatus == 1) {
				if (partner_EditForm.getFromPage() != null
						&& partner_EditForm.getFromPage().equals("pvssearch")) {
					request.setAttribute("pvssearch", "yes");
					request.setAttribute("refreshtree", "true");
					request.setAttribute("partnerDeleted", "yes");
					return (mapping.findForward("pvssearchpage"));

				}
			}

		}

		DynamicComboCM dcStateCM = new DynamicComboCM();
		dcStateCM.setFirstlevelcatglist(Pvsdao.getCountryStateList(ds));
		request.setAttribute("dcStateCM", dcStateCM);

		logger.debug("Time Start Check1:");
		initialStateCategory = Pvsdao.getCountryStateListMap(ds);
		logger.debug("Time End Check1:");

		request.setAttribute("initialStateCategory", initialStateCategory);

		DynamicComboCM dcCorporationType = new DynamicComboCM();
		dcCorporationType.setFirstlevelcatglist(Pvsdao
				.getIncorporationType(getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcCorporationType", dcCorporationType);

		DynamicComboCM dcCountryList = new DynamicComboCM();
		dcCountryList.setFirstlevelcatglist(DynamicComboDao
				.getCountryList(getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcCountryList", dcCountryList);

		List coreCompetency = Pvsdao.getCoreCompetancy(ds);

		List<CoreCompetancyBean> coreCompetencyBeanList = new ArrayList<CoreCompetancyBean>();
		CoreCompetancyBean competencyBean = null;

		String[] dat = null;
		String data = "";

		if (!"0".equals(partner_EditForm.getPid())) {

			String myURL = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=partner_score&partner_id="
					+ (partner_EditForm.getPid());
			data = WebUtil.getDataFromWeb(myURL, 9999, "\n");

			if (data != null && (!data.equals(""))) {
				dat = data.split("\\|");
			}
		}
		int competencyId = 0;
		if (dat == null) {
			dat = new String[0];
		}
		for (int i = 0; i < coreCompetency.size(); i++) {
			LabelValue lb = (LabelValue) coreCompetency.get(i);

			competencyBean = new CoreCompetancyBean();
			competencyBean.setCompetancyId(lb.getValue());
			competencyBean.setCompetancyName(lb.getLabel());
			competencyBean.setMarkDisable("false");// checkbox should not be
													// disabled
			for (int j = 0; j < dat.length; j++) {
				String[] keyVal = dat[j].split(":");
				Float score = Float.parseFloat(keyVal[1]);
				if (keyVal[0].toString().equals(lb.getValue())) {
					if (score < 50) {
						competencyBean.setMarkDisable("true");// checkbox
																// should
						// be disabled
					}

					if (score != 0) {
						score = score / 20;
					}
					competencyBean.setScore(keyVal[1].toString().trim());
					break;
				} else if (keyVal[0].toString().equals("28")) {

					if (score >= Partner_EditAction.COMPETENCY_SCORE) {
						competencyId = Partner_EditAction.CCTV_COMPETENCY_ID;
						break;
					}
				}
			}
			coreCompetencyBeanList.add(competencyBean);
		}

		request.setAttribute("dcCoreCompetancy", coreCompetencyBeanList);

		if (coreCompetencyBeanList.size() > 0)
			request.setAttribute("CompetencySize", new Integer(
					coreCompetencyBeanList.size()));

		if (!"0".equals(partner_EditForm.getPid())) {

			data = WebUtil
					.getDataFromWeb(
							"https://ccc.contingent.com/index.php?cmp=phone_ws&type=partner_schedule&cp_partner_id="
									+ partner_EditForm.getPid(), 9999, "\n");
			if (data != null && !data.contains("mysql connection failed")) {
				String[] sche = data.split("\\|");
				if (sche != null && sche.length > 0) {
					partner_EditForm.setDaysRange(sche[0]);
					partner_EditForm.setTimeRange(sche[1]);
				}
			}
		}
		logger.debug("Time Start Check2:" + new java.util.Date());
		DynamicComboCM dcResourceLevel = new DynamicComboCM();
		dcResourceLevel.setFirstlevelcatglist(Pvsdao.getResourceLevel(ds));
		request.setAttribute("dcResourceLevel", dcResourceLevel);

		DynamicComboCM dcHighestCriticalityAvailable = new DynamicComboCM();
		dcHighestCriticalityAvailable.setFirstlevelcatglist(Pvsdao
				.getHighestCriticallyAvailable(ds));
		request.setAttribute("dcHighestCriticalityAvailable",
				dcHighestCriticalityAvailable);

		DynamicComboCM dcWlCompany = new DynamicComboCM();
		dcWlCompany.setFirstlevelcatglist(Pvsdao.getCompany("W", ds));
		request.setAttribute("dcWlCompany", dcWlCompany);

		DynamicComboCM dcSDCompany = new DynamicComboCM();
		dcSDCompany.setFirstlevelcatglist(Pvsdao.getCompany("SD", ds));
		request.setAttribute("dcSDCompany", dcSDCompany);

		DynamicComboCM dcITCompany = new DynamicComboCM();
		dcITCompany.setFirstlevelcatglist(Pvsdao.getCompany("IT", ds));
		request.setAttribute("dcITCompany", dcITCompany);

		DynamicComboCM dcTACompany = new DynamicComboCM();
		dcTACompany.setFirstlevelcatglist(Pvsdao.getCompany("ITA", ds));
		request.setAttribute("dcTACompany", dcTACompany);

		DynamicComboCM dcCertifications = new DynamicComboCM();
		dcCertifications.setFirstlevelcatglist(Pvsdao.getCertificationlist(ds));
		request.setAttribute("dcCertifications", dcCertifications);

		DynamicComboCM dcPartnerstatus = new DynamicComboCM();
		dcPartnerstatus.setPartnerstatus(Pvsdao.getPartnerstatuslist(ds));
		request.setAttribute("dcPartnerstatus", dcPartnerstatus);

		// Start :Added By Amit For MCSA Version
		DynamicComboCM mcsaVersionCombo = new DynamicComboCM();
		mcsaVersionCombo.setMcsaVersion(Pvsdao.getMsaVersionlist("", ds));
		request.setAttribute("mcsaVersionCombo", mcsaVersionCombo);

		// For Partner Type
		DynamicComboCM dcPartnerType = new DynamicComboCM();
		dcPartnerType.setPartnerType(Pvsdao.getPartner_type(ds));
		request.setAttribute("dcPartnerType", dcPartnerType);
		// End :

		request.setAttribute("equipmentList", Pvsdao.getEquipmentList(ds));
		request.setAttribute("SQL", ds);

		// get user role
		String checkForRoleAssigned = Pvsdao.checkforrole(userid,
				getDataSource(request, "ilexnewDB"));
		partner_EditForm.setResourceDeveloper(checkForRoleAssigned);

		// get if user have CPD or Account role.
		String accountOrCPDRole = Pvsdao.checkforAccountORCPDRole(userid,
				getDataSource(request, "ilexnewDB"));
		partner_EditForm.setAccountingCPDRole(accountOrCPDRole);
		logger.debug("Time End Check2:" + new java.util.Date());

		int deltech = 0;
		String pid = "";
		String tech_id = "";
		String techid_list = "";
		String strArray[] = null;
		Partner_EditBean partner_EditBean = new Partner_EditBean();
		BeanUtils.copyProperties(partner_EditBean, partner_EditForm);

		if (request.getParameter("delete") != null) {
			if (request.getParameter("Pid") != null) {
				partner_EditForm.setPid(request.getParameter("pid"));
				partner_EditBean.setPid(request.getParameter("pid"));
				pid = request.getParameter("Pid");
			}
			if (request.getParameter("tech_id") != null)
				tech_id = request.getParameter("tech_id").toString();
			techid_list = request.getParameter("strdel").toString();

			strArray = techid_list.split("~~");
			for (int i = 0; i < strArray.length; i++) {

				deltech = Pvsdao.deletetechnician(partner_EditBean, pid,
						strArray[i], ds);
			}
			if (deltech == 0)
				request.setAttribute("refreshtree", "true");

		}

		String topPartner[] = new String[3];
		String topPartnerName = "";
		String topPartnerType = "";

		if (partner_EditForm.getPid() != null) {
			if (partner_EditForm.getPid().equals("0")) {
				Pvsdao.getTopLevelPartnerByType(
						partner_EditForm.getTopPartnerName(), topPartner, ds);
			} else {
				Pvsdao.getTopLevelPartner(partner_EditForm.getPid(),
						topPartner, ds);
			}

			partner_EditForm.setOrgTopId(topPartner[2].trim());
			partner_EditForm.setTopPartnerName(topPartner[1].trim());
			partner_EditForm.setTopPartnerType(topPartner[0].trim());

			if ((partner_EditForm.getTopPartnerName().trim())
					.equalsIgnoreCase("Minuteman Partners"))
				topPartnerName = "Minuteman Partner";
			else if ((partner_EditForm.getTopPartnerName().trim())
					.equalsIgnoreCase("Certified Partners"))
				topPartnerName = "Standard Partner";

			request.setAttribute("topPartnerType", topPartnerType);
			request.setAttribute("topPartnerName", topPartnerName);
		}

		int retvalue = -1;
		int retMySqlValue = -1;

		if (partner_EditForm.getSave() != null
				|| partner_EditForm.getSaveContinue() != null) {

			/* Code for Get Latitude and Longitude:start */
			boolean checkLongLat = false;
			String trueforaddress = "";
			String state_name = Pvsdao.getStateName(
					partner_EditForm.getState(), ds);
			String[] LatLong = RestClient.getLatitudeLongitude(
					partner_EditForm.getAddress1(), partner_EditForm.getCity(),
					state_name, partner_EditForm.getCountry(),
					partner_EditForm.getZip(),
					getDataSource(request, "ilexnewDB"));
			partner_EditForm.setLon_degree(null);
			partner_EditForm.setLon_min(null);
			partner_EditForm.setLon_direction(null);
			partner_EditForm.setLat_degree(null);
			partner_EditForm.setLat_min(null);
			partner_EditForm.setLat_direction(null);
			partner_EditForm.setLatLongAccuracy(LatLong[0]);
			partner_EditForm.setLatlon_source(LatLong[1]);
			partner_EditForm.setLatitude(LatLong[3]);
			partner_EditForm.setLongitude(LatLong[2]);
			trueforaddress = LatLong[4];
			BeanUtils.copyProperties(partner_EditBean, partner_EditForm);
			if (partner_EditForm.getAcceptAddress() != null
					&& partner_EditForm.getAcceptAddress().equals("1")) {
				checkLongLat = true;
			} else {
				if (trueforaddress.equals("true")) {// its value is false when
													// accuracy of latitude and
													// longitude according to
													// address is greater then
													// 6.
					checkLongLat = true;
				} else {
					Pvsdao.checkEmptyPhyMail(partner_EditBean);
					BeanUtils
							.copyProperties(partner_EditForm, partner_EditBean);
					request.setAttribute("noLatLong", "noLatLong");
				}
			}
			/* Code for Get Latitude and Longitude:end */
			if (checkLongLat) {

				// flag to check if the partner's contact info/address is
				// modified
				boolean isContactInfoModified = false;
				if (partner_EditForm.getAct().equalsIgnoreCase("Add")) {
					retvalue = Pvsdao.setPartnerDetailGeneralTab(
							partner_EditBean,
							(String) session.getAttribute("userid"),
							partner_EditForm.getAct(), ds);
					BeanUtils
							.copyProperties(partner_EditForm, partner_EditBean);
					if (retvalue == 0) {
						sendMail = true;
					}
				} else if (partner_EditForm.getAct().equalsIgnoreCase("Update")) {
					isContactInfoModified = Pvsdao.isContactInfoModified(
							partner_EditBean, ds);
					// set formfile properties
					if (partner_EditForm.getBrowse() != null) {
						partner_EditBean.setInputStream(partner_EditForm
								.getBrowse().getInputStream());
						partner_EditBean.setFileName(partner_EditForm
								.getBrowse().getFileName());
						partner_EditBean.setFileSize(partner_EditForm
								.getBrowse().getFileSize());
					}

					String[] checkbox1 = (String[]) request
							.getParameterValues("checkBox1");
					String[] checkbox2 = (String[]) request
							.getParameterValues("checkBox2");
					String[] checkbox3 = (String[]) request
							.getParameterValues("checkBox3");
					partner_EditBean.setCheckBox1(checkbox1);
					partner_EditBean.setCheckBox2(checkbox2);
					partner_EditBean.setCheckBox3(checkbox3);
					retvalue = Pvsdao.setPartnerDetails(partner_EditBean,
							(String) session.getAttribute("userid"),
							partner_EditForm.getAct(), ds);
					checkbox1 = getCheckBoxValues(
							partner_EditBean.getCheckBox1(),
							partner_EditBean.getEngName().length);
					checkbox2 = getCheckBoxValues(
							partner_EditBean.getCheckBox2(),
							partner_EditBean.getEngName().length);
					checkbox3 = getCheckBoxValues(
							partner_EditBean.getCheckBox3(),
							partner_EditBean.getEngName().length);
					String[] checkboxDate1 = partner_EditBean
							.getCheckBox1Date();
					String[] checkboxDate2 = partner_EditBean
							.getCheckBox2Date();
					String[] checkboxDate3 = partner_EditBean
							.getCheckBox3Date();
					String technicians_id[] = partner_EditBean.getTech_id();
					String engName[] = partner_EditBean.getEngName();
					for (int i = 0; i < technicians_id.length; i++) {
						try {
							String techId = technicians_id[i];
							String engineerName = engName[i];
							String drugScreenCert = checkbox1[i];
							String criminalBackgroundCert = checkbox2[i];
							String harassmentCert = checkbox3[i];
							String drugScreenCertDate = checkboxDate1[i];
							String criminalBackgroundCertDate = checkboxDate2[i];
							String harassmentCertDate = checkboxDate3[i];
							Pvsdao.saveSecurityCertifications(techId,
									engineerName, drugScreenCert,
									criminalBackgroundCert, harassmentCert,
									drugScreenCertDate,
									criminalBackgroundCertDate,
									harassmentCertDate, ds);
						} catch (Exception ee) {
							ee.printStackTrace();
						}
					}
					BeanUtils
							.copyProperties(partner_EditForm, partner_EditBean);
				}
				if (partner_EditForm.getLatitude() == null
						|| partner_EditForm.getLongitude() == null) {
					request.setAttribute("LatLongNotFound", "LatLongNotFound");
				}

				if (retvalue == 0) {
					request.setAttribute("refreshtree", "true");

					url = EnvironmentSelector
							.getBundleString("appendix.detail.changestatus.mysql.url");
					retMySqlValue = Pvsdao.setPartnerDetailInMySql(
							partner_EditForm.getPid(),
							partner_EditForm.getAct(), "", url, ds);
					request.setAttribute("retMySqlValue", retMySqlValue + "");

					// sending mail to account department
					if (partner_EditForm.getAct().equalsIgnoreCase("Add")
							|| isContactInfoModified) {
						StringBuffer accountContent = new StringBuffer();
						EmailBean emailform = new EmailBean();
						emailform.setUsername(this.getResources(request)
								.getMessage("common.Email.username"));
						emailform.setUserpassword(this.getResources(request)
								.getMessage("common.Email.userpassword"));

						emailform
								.setSmtpservername(EnvironmentSelector
										.getBundleString("common.Email.smtpservername"));

						emailform.setSmtpserverport(this.getResources(request)
								.getMessage("common.Email.smtpserverport"));
						String comEmailPvs = EnvironmentSelector
								.getBundleString("common.Email.for.pvs.manager.accounting.to");

						if (comEmailPvs != null
								&& !"".equals(comEmailPvs.trim())) {
							emailform.setTo(comEmailPvs);
						}

						emailform.setCc("");
						emailform.setBcc("");
						if (partner_EditForm.getAct().equalsIgnoreCase("Add")) {
							emailform.setSubject("Partner Created");
						} else if (partner_EditForm.getAct().equalsIgnoreCase(
								"Update")) {
							emailform.setSubject("Partner Updated");
						}
						emailform
								.setFrom(EnvironmentSelector
										.getBundleString("common.Email.for.pvs.manager.from"));
						accountContent.append(Pvsdao.getAccountMailContent(
								partner_EditBean,
								(String) session.getAttribute("userid"),
								partner_EditForm.getAct(), ds));
						emailform.setContent(accountContent.toString());
						emailform.setType("prm_appendix");
						emailform.setChangeStatus("");
						if (Email.isValidEmailAddress(emailform.getTo())) {
							Email.Send(emailform,
									getDataSource(request, "ilexnewDB"));
						}
					}
					// sending mail to account department over

					// sending mail to resource development manager
					if (!((String) session.getAttribute("userid")).trim()
							.equals(Pvsdao.getResourceDevManagerId(ds).trim())
							&& (request.getParameter("convert") == null)) {
						EmailBean emailform1 = new EmailBean();
						emailform1.setUsername(this.getResources(request)
								.getMessage("common.Email.username"));
						emailform1.setUserpassword(this.getResources(request)
								.getMessage("common.Email.userpassword"));
						emailform1
								.setSmtpservername(EnvironmentSelector
										.getBundleString("common.Email.smtpservername"));
						emailform1.setSmtpserverport(this.getResources(request)
								.getMessage("common.Email.smtpserverport"));
						emailform1.setTo(Pvsdao.getResourceDevManagerEmail(ds));
						emailform1.setCc("");
						emailform1.setBcc("");
						if (partner_EditForm.getAct().equalsIgnoreCase("Add")) {
							emailform1.setSubject("Partner Created");
						} else if (partner_EditForm.getAct().equalsIgnoreCase(
								"Update")) {
							emailform1.setSubject("Partner Updated");
						}
						emailform1
								.setFrom(Pvsdao.getLoginUserEmail(userid, ds));
						emailform1
								.setContent(Pvsdao.getResDevManagerMailContent(
										partner_EditBean,
										(String) session.getAttribute("userid"),
										partner_EditForm.getAct(), ds));
						emailform1.setType("prm_appendix");
						emailform1.setChangeStatus("");
						if (Email.isValidEmailAddress(emailform1.getTo())) {
							Email.Send(emailform1,
									getDataSource(request, "ilexnewDB"));
						}
					}// sending mail to resource development manager over
				}
			}

			// set MinuteMan Partner Payroll id

			if (StringUtils
					.isNotBlank(partner_EditBean.getMinutemanPayrollId())) {
				Pvsdao.setMinutemanPayrollId(partner_EditBean, ds);
			}

		}
		// get Partner Payroll Id
		partner_EditForm.setMinutemanPayrollId(Pvsdao.getMinutePayrollId(
				partner_EditBean.getPid(), ds));

		if (request.getParameter("convert") != null
				&& (request.getParameter("convert").equals("M") || request
						.getParameter("convert").equals("S"))) {
			int retValConMinute = -1;
			char typeOfPartner = 'S';
			if (request.getParameter("convert").equals("M")) {
				typeOfPartner = 'M';
			}
			url = EnvironmentSelector
					.getBundleString("appendix.detail.changestatus.mysql.url");
			retMySqlValue = Pvsdao.setPartnerDetailInMySql(
					partner_EditForm.getPid(), partner_EditForm.getAct(),
					String.valueOf(typeOfPartner), url, ds);
			request.setAttribute("retMySqlValue", retMySqlValue + "");
			if (retMySqlValue == 1) {
				request.setAttribute("refreshtree", "true");

				retValConMinute = Pvsdao.convertToMinuteman(
						partner_EditForm.getPid(), userid, 'I', typeOfPartner,
						ds);
				if (retValConMinute == 0) {
					if (typeOfPartner == 'M') {
						partner_EditForm
								.setTopPartnerName("Minuteman Partners");
						request.setAttribute("topPartnerName",
								"Minuteman Partner");
						request.setAttribute("convert", "Minuteman Partner");
					} else {
						partner_EditForm
								.setTopPartnerName("Certified Partners");
						request.setAttribute("topPartnerName",
								"Standard Partner");
						request.setAttribute("convert", "Standard Partner");
					}
				}

			}
		}

		request.setAttribute("retvalue", retvalue + "");
		BeanUtils.copyProperties(partner_EditBean, partner_EditForm);

		if ((partner_EditForm.getRefersh() != null)
				&& (!partner_EditForm.getRefersh().equals(""))) {
			Pvsdao.getRefershList(partner_EditBean, check, ds);
			BeanUtils.copyProperties(partner_EditForm, partner_EditBean);
		} else {
			if (partner_EditForm.getSave() == null
					|| ((partner_EditForm.getSave() != null || partner_EditForm
							.getSaveContinue() != null) && retvalue == 0)) {
				Pvsdao.getPartnerDetail(partner_EditBean, roleCount, ds);
				BeanUtils.copyProperties(partner_EditForm, partner_EditBean);

				if (!partner_EditForm.getPid().equals("0")) {
					String partnerName = Pvsdao.getPartnername(
							partner_EditForm.getPid(), ds);
					request.setAttribute("partnerName", partnerName);
					if (partner_EditForm.getTopPartnerName() != null
							&& (partner_EditForm.getTopPartnerName().trim())
									.equalsIgnoreCase("Minuteman Partners")) {
						partnerName = partnerName.replaceAll(", ", ",");

						if (partnerName.indexOf(",") >= 0) {
							partner_EditForm.setPartnerLastName(partnerName
									.substring(0, partnerName.indexOf(",")));
							partner_EditForm.setPartnerFirstName(partnerName
									.substring(partnerName.indexOf(",") + 1));
						} else {
							partner_EditForm.setPartnerLastName("");
							partner_EditForm.setPartnerFirstName(partnerName);
						}
						partner_EditForm.setPartnerCandidate("");

					} else if (partner_EditForm.getTopPartnerName() != null
							&& (partner_EditForm.getTopPartnerName().trim())
									.equalsIgnoreCase("Certified Partners")) {
						partner_EditForm.setPartnerCandidate(partnerName);
						partner_EditForm.setPartnerFirstName("");
						partner_EditForm.setPartnerLastName("");
					}
				}
			}
		}

		// RDM E-mail for Add/Re-send mail to primary poc of partner Start
		if ((request.getParameter("reSendRegMail") != null && request
				.getParameter("reSendRegMail").equals("y")) || sendMail) {

			String PartnerEmailContentFile = "";

			/* Get INC file for Partner Creation Email Contents in message body */
			if (partner_EditForm.getTopPartnerName().trim()
					.equals("Certified Partners")) {
				PartnerEmailContentFile = request
						.getRealPath("/PartnerCreateEmailCertified.inc");
			} else if (partner_EditForm.getTopPartnerName().trim()
					.equals("Minuteman Partners")) {
				PartnerEmailContentFile = request
						.getRealPath("/PartnerCreateEmailMinuteman.inc");
			}

			EmailBean emailform2 = new EmailBean();
			emailform2.setUsername(this.getResources(request).getMessage(
					"common.Email.username"));
			emailform2.setUserpassword(this.getResources(request).getMessage(
					"common.Email.userpassword"));
			emailform2.setSmtpservername(EnvironmentSelector
					.getBundleString("common.Email.smtpservername"));
			emailform2.setSmtpserverport(this.getResources(request).getMessage(
					"common.Email.smtpserverport"));
			emailform2.setTo(partner_EditForm.getPrmEmail());
			emailform2.setCc("");
			emailform2.setBcc("");
			emailform2
					.setSubject("New Account Creation on the Contingent Partner Care Center");
			emailform2.setFrom(Pvsdao.getResourceDevManagerEmail(ds));
			emailform2.setContent(Pvsdao.getEmailContent("ResendAdd",
					partner_EditForm.getPid(),
					partner_EditForm.getPrimaryFirstName(),
					partner_EditForm.getPrimaryLastName(),
					partner_EditForm.getTopPartnerName(),
					partner_EditForm.getWebUserName(),
					partner_EditForm.getWebPassword(),
					getDataSource(request, "ilexnewDB"),
					PartnerEmailContentFile));

			/* Get Path for Embedded images in Partner Creation Email. */
			emailform2.setFilename1(request.getRealPath("/images")
					+ "/contingent_logo.gif");
			emailform2.setFilename2(request.getRealPath("/images")
					+ "/button1.gif");
			emailform2.setFilenameA(request.getRealPath("/images")
					+ "/contingent_cp_logo.jpg");
			emailform2.setFilenameB(request.getRealPath("/images")
					+ "/partner_connection.jpg");

			emailform2.setType("partner_create"); // Type is changed from
													// "prm_appendix" to
													// "partner_create".
			emailform2.setChangeStatus("");
			String emailstatus = "";
			if (Email.isValidEmailAddress(emailform2.getTo())) {
				emailstatus = Email.Send(emailform2,
						getDataSource(request, "ilexnewDB"));
			}
			if (!sendMail) {
				if (emailstatus.equals("-90040,-1")) {
					request.setAttribute("emailError", "emailError");
				} else {
					request.setAttribute("emailSent", "emailSent");
				}
			}
		}
		if (request.getParameter("sendUnamePwd") != null
				&& request.getParameter("sendUnamePwd").equals("y")) {
			EmailBean emailform4 = new EmailBean();
			emailform4.setUsername(this.getResources(request).getMessage(
					"common.Email.username"));
			emailform4.setUserpassword(this.getResources(request).getMessage(
					"common.Email.userpassword"));
			emailform4.setSmtpservername(EnvironmentSelector
					.getBundleString("common.Email.smtpservername"));
			emailform4.setSmtpserverport(this.getResources(request).getMessage(
					"common.Email.smtpserverport"));
			emailform4.setTo(partner_EditForm.getPrmEmail());
			emailform4.setCc("");
			emailform4.setBcc(Pvsdao.getBccEmailId());
			emailform4
					.setSubject("Contingent Partner Care Center Account Information");
			emailform4.setFrom(Pvsdao.getResourceDevManagerEmail(ds));
			emailform4.setContent(Pvsdao.getEmailContent("namePass",
					partner_EditForm.getPid(),
					partner_EditForm.getPrimaryFirstName(),
					partner_EditForm.getPrimaryLastName(),
					partner_EditForm.getTopPartnerName(),
					partner_EditForm.getWebUserName(),
					partner_EditForm.getWebPassword(),
					getDataSource(request, "ilexnewDB"), ""));
			emailform4.setType("prm_appendix");
			emailform4.setChangeStatus("");
			String emailstatus = "";
			if (Email.isValidEmailAddress(emailform4.getTo())) {
				emailstatus = Email.Send(emailform4,
						getDataSource(request, "ilexnewDB"));
			}
			if (emailstatus.equals("-90040,-1")) {
				request.setAttribute("emailError", "emailError");
			} else {
				request.setAttribute("emailSent", "emailSent");
			}
		}
		// Re-send mail to primary poc of partner End
		// Add code for changes

		//
		logger.debug("Time Start Check3:" + new java.util.Date());
		String topName = request.getParameter("orgTopName");
		if (topName == null) {
			ArrayList<PVSToolList> lst = new ArrayList<PVSToolList>();

			lst = getToolSet(getDataSource(request, "ilexnewDB"), 0, lst, 0,
					partner_EditForm.getPid());

			logger.debug("Time End Check3:" + new java.util.Date());

			// getting value of other

			String URL = "https://medius.contingent.com/index.php?cmp=phone_ws&type=other_tool_name&cp_partner_id="

					+ (partner_EditForm.getPid());
			String responsedata = WebUtil.getDataFromWeb(URL, 9999, "\n");
			String[] urldata = null;
			if (responsedata != null && (!responsedata.equals(""))) {
				urldata = responsedata.split("\\|");
			}

			if (urldata != null) {
				for (int i = 0; i < lst.size(); i++) {
					PVSToolList lb = (PVSToolList) lst.get(i);
					if (lb.getLabel().equals("Other")) {

						for (int j = 0; j < urldata.length; j++) {
							String keyVal = urldata[j];
							if (keyVal != null) {
								lb.setLabel(keyVal);
								break;
							}

						}

					}

				}

			}

			partner_EditForm.setToolList(lst);
		}
		request.setAttribute("toolList", partner_EditForm.getToolList());
		request.setAttribute("WLCompnay", Pvsdao
				.getCompanyCertificationDetails(partner_EditForm.getPid(), "2",
						ds));
		request.setAttribute("SDCompnay", Pvsdao
				.getCompanyCertificationDetails(partner_EditForm.getPid(), "1",
						ds));
		request.setAttribute("ITCompnay", Pvsdao
				.getCompanyCertificationDetails(partner_EditForm.getPid(), "8",
						ds));
		request.setAttribute("TACompnay", Pvsdao
				.getCompanyCertificationDetails(partner_EditForm.getPid(), "9",
						ds));
		LogHistoryList = Pvsdao.getRecommendedRestrictedPartnerlog(
				partner_EditForm.getPid(), ds);
		ArrayList<String> loghistory = new ArrayList<String>();
		if (LogHistoryList.size() > 0) {
			for (int i = 0; i < LogHistoryList.size(); i++) {
				RecRest_History a = (RecRest_History) LogHistoryList.get(i);
				String name = a.getName();
				String type = a.getType();
				if (type.equals("1")) {

					type = "Recommended";

				} else {

					type = "Restricted";
				}
				String createduser = a.getCreatedUser();
				String date = a.getDate();
				String activity = a.getActivity();
				String result = name + "@" + type + "@" + createduser + "@"
						+ date + "@" + activity;
				loghistory.add(result);

			}

		}

		request.setAttribute("History", loghistory);
		partner_EditForm.setHistorylist(LogHistoryList);

		if (partner_EditForm.getPhyAddress() != null)
			request.setAttribute("phyAddress", partner_EditForm.getPhyAddress());

		if (partner_EditForm.getPh_email() != null)
			request.setAttribute("hr_email", partner_EditForm.getPh_email());

		ArrayList al = (ArrayList) dcResourceLevel.getFirstlevelcatglist();
		ArrayList TechnicianList = Pvsdao.getPartner_technicianInfo(
				partner_EditForm.getPid(), ds);

		boolean tech_present = false;

		for (int i = 0; i < TechnicianList.size(); i++) {
			PVS_Partner_Edit_TechnicianInfo l2 = (PVS_Partner_Edit_TechnicianInfo) TechnicianList
					.get(i);
			tech_present = false;

			for (int j = 0; j < al.size(); j++) {
				LabelValue l1 = (LabelValue) al.get(j);

				if (l1.getValue().equals(l2.getCmboxResourceLevel())) {

					tech_present = true;
					// break;
				}
			}
			if (tech_present == false) {
				al.add(new com.mind.common.LabelValue(l2.getLevelName(), l2
						.getCmboxResourceLevel()));
			}

		}

		request.setAttribute("tech_info", TechnicianList);
		request.setAttribute("tech_info_size", TechnicianList.size() + "");
		request.setAttribute("acceptAddress",
				partner_EditForm.getAcceptAddress());
		request.setAttribute("action", partner_EditForm.getAct());

		if (partner_EditForm.getAct().equalsIgnoreCase("Add")) {

			if (partner_EditForm.getSaveContinue() != null && retvalue == 0) {

				// function that

				forward = mapping.findForward("success");
				request.setAttribute("action", "Update");
			} else {
				if (retvalue == 0
						&& !(request.getParameter("showViewPage") != null && request
								.getParameter("showViewPage").equalsIgnoreCase(
										"y"))) {
					forward = mapping.findForward("viewpage");
				}
				if (request.getParameter("showViewPage") != null
						&& request.getParameter("showViewPage")
								.equalsIgnoreCase("y")) {
					request.setAttribute("view", "view");
					forward = mapping.findForward("viewpage");
				}
				if (retvalue != 0
						&& !(request.getParameter("showViewPage") != null && request
								.getParameter("showViewPage").equalsIgnoreCase(
										"y"))) {

					forward = mapping.findForward("addpartner");
				}
			}
		} else {

			Pvsdao.updatecp_PartnerStatus(partner_EditForm.getPid(),
					getDataSource(request, "ilexnewDB"));

			if (partner_EditForm.getFromPVSSearch() != null
					&& partner_EditForm.getFromPVSSearch().equals(
							"fromPVSSearch")) {

				request.setAttribute("view", "view");
				forward = mapping.findForward("viewpage");
			} else {
				if (partner_EditForm.getFromPVSSearch() != null
						&& partner_EditForm.getFromPVSSearch().equals(
								"fromPVSSearch")) {
					forward = mapping.findForward("viewpage");
				}
				if (retvalue == 0 || retvaluedelete < 0) {
					forward = mapping.findForward("viewpage");
				} else if (request.getParameter("showViewPage") == null) {
					forward = mapping.findForward("success");
				} else if (request.getParameter("showViewPage") != null
						&& request.getParameter("showViewPage").equals("y")) {
					forward = mapping.findForward("viewpage");
				}
			}
			if (retvalueDeleteForMYSQL < 0 && MySql) {
				forward = mapping.findForward("viewpage");
			}
		}
		/* for state combobox only */
		if (competencyId == CCTV_COMPETENCY_ID) {
			String[] st = partner_EditForm.getCmboxCoreCompetency();
			String[] st1 = new String[st.length + 1];
			int i = 0;
			for (; i < st.length; i++) {
				st1[i] = st[i];
			}
			st1[i] = competencyId + "";
			partner_EditForm.setCmboxCoreCompetency(st1);
		}

		partner_EditForm.setStateSelect(Util.concatString(
				partner_EditForm.getState(), partner_EditForm.getCountry()));
		return (forward);
	}

	private ArrayList<PVSToolList> getToolSet(DataSource ds, int parentId,
			ArrayList<PVSToolList> lst1, int width, String partnerid) {

		ArrayList<String[]> lst = Pvsdao.getToolListwithzip(ds, partnerid,
				parentId);

		for (int i = 0; i < lst.size(); i++) {
			String[] st = lst.get(i);
			lst1.add(new PVSToolList(st[0], st[1], st[2], st[3], width));
			if (Pvsdao.getToolListwithzip(ds, partnerid,
					Integer.parseInt(st[3] == null ? "0" : st[3])).size() > 0) {

				getToolSet(ds, Integer.parseInt(st[0]), lst1, width + 20,
						partnerid);

			}
		}
		return lst1;
	}

	private static String[] getCheckBoxValues(String[] checkBox1, int length) {
		String[] checkboxVals = new String[length];
		if (checkBox1 != null) {
			for (String val : checkBox1) {
				checkboxVals[Integer.parseInt(val)] = "1";
			}
		}
		return checkboxVals;
	}
}
