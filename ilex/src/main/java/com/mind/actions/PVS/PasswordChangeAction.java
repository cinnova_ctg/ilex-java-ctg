package com.mind.actions.PVS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.PasswordChangeForm;

public class PasswordChangeAction extends com.mind.common.IlexAction{
	public ActionForward execute(ActionMapping mapping,ActionForm actionform,
			HttpServletRequest request,HttpServletResponse response){
		
		ActionForward forward= new ActionForward();
		PasswordChangeForm form= (PasswordChangeForm) actionform;
		DataSource ds= getDataSource(request,"ilexnewDB");		
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null)
			return( mapping.findForward("SessionExpire"));   //Check for session expired
		int retvalue=0;
		
		String userid = (String)session.getAttribute("userid");		

		form.setPid((String)session.getAttribute("userid"));


		if(form.getSave()!=null){
			if(!form.getOldPassword().equals(Pvsdao.getOldPassword(form.getPid(),ds))){
				request.setAttribute("invalidOldPassword",request);
			}
			else{
				if(Pvsdao.setPassword(form.getPid(),form.getNewPassword(), ds)==1){
					request.setAttribute("changesuccessful",request);	
				}
			}
		}
		
		forward= mapping.findForward("success");
		return forward;
	}
}