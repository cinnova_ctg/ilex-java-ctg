/**
 * 
 */
package com.mind.actions.PVS;

import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pvs.PartnerTypeDetailBean;
import com.mind.common.LabelValue;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.PartnerTypeDetailForm;

/**
 * @author mahmad
 * 
 */
public class PartnerTypeDetailAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);

		PartnerTypeDetailForm partnerTypedetailform = (PartnerTypeDetailForm) form;

		PartnerTypeDetailBean partnerTypeDetailBean = new PartnerTypeDetailBean();

		String partnerID = null;

		if (request.getParameter("partnerId") != null) {
			partnerID = request.getParameter("partnerId").trim();
		}

		BeanUtils.copyProperties(partnerTypeDetailBean, partnerTypedetailform);

		String lastSpeedPayDate = Pvsdao.getLastSpeedPayDate(
				partnerTypeDetailBean, getDataSource(request, "ilexnewDB"));

		ArrayList<LabelValue> returnList = null;

		returnList = Pvsdao.getSpeedPayWorkDetail(partnerTypeDetailBean,
				getDataSource(request, "ilexnewDB"));

		ArrayList<LabelValue> formBean = null;

		if (returnList != null) {
			formBean = new ArrayList<LabelValue>();
			LabelValue singleRowFormdata = null;

			for (int index = 0; index < returnList.size(); index++) {
				singleRowFormdata = new LabelValue();
				BeanUtils.copyProperties(singleRowFormdata,
						returnList.get(index));
				formBean.add(singleRowFormdata);
			}
		}

		partnerTypedetailform.setPartenersDistributionList(formBean);
		partnerTypedetailform.setLastSpeedpayDate(lastSpeedPayDate);

		response.setContentType("application/xml");
		RequestDispatcher rd = request
				.getRequestDispatcher("/PVS/SpeedPayWorkNatureDistribution.jsp");
		rd.include(request, response);
		return null;
		// return mapping.findForward("success");
	}
}
