/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is an action class used for performing multiple
 * operations Add,Delete,view of Incident report of partners in PVS Manager.
 *
 */
package com.mind.actions.PVS;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.common.CreateEmail;
import com.mind.bean.DynamicComboPVS;
import com.mind.bean.pvs.AddIncidentBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDaoPVS;
import com.mind.dao.PM.NameId;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.AddIncidentForm;
import com.mind.util.VelocityUtils;

public class AddIncidentAction extends com.mind.common.IlexDispatchAction {

	/**
	 * This method adds Incident details for a partner in PVS Manager.
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();

		AddIncidentForm addIncidentForm = (AddIncidentForm) form;
		String incidenttype = "Bad";
		String partnerstatus = "";
		// int incidentStatus = 0;
		// String[] tech = request.getParameterValues("technicianname");
		int techId;
		if (addIncidentForm.getTechniciannametxt() != null) {
			if (!addIncidentForm.getTechniciannametxt().equals("")) {

				techId = Pvsdao.savecp_technicianPartner(
						addIncidentForm.getPid(),
						addIncidentForm.getTechniciannametxt(),
						getDataSource(request, "ilexnewDB"));

				addIncidentForm.setTechnicianname(String.valueOf(techId));

			}

		}
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired
		String userid = (String) session.getAttribute("userid");
		addIncidentForm.setAddedBy((String) session.getAttribute("username"));
		if (addIncidentForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage PVS",
					"Manage Incident Report",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		DynamicComboDaoPVS dcomboPVS = new DynamicComboDaoPVS();
		DynamicComboPVS dcpvs = new DynamicComboPVS();
		int statusvalue = 0;
		addIncidentForm.setInid(request.getParameter("did"));
		String note = "";
		int executeElse = 0;
		String partnerTopType = Pvsdao.getTopPartnerType(
				addIncidentForm.getPid(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("topPartnerName", "" + partnerTopType);

		if (request.getParameter("viewjobtype") != null) {
			addIncidentForm.setViewjobtype(request.getParameter("viewjobtype"));
		} else {
			addIncidentForm.setViewjobtype("A");
		}

		if (request.getParameter("jobid") != null) {
			addIncidentForm.setJobid(request.getParameter("jobid"));
		} else {
			addIncidentForm.setJobid("0");
		}

		if (request.getParameter("ownerId") != null) {
			addIncidentForm.setOwnerId(request.getParameter("ownerId"));
		} else {
			addIncidentForm.setOwnerId("%");
		}

		if (request.getParameter("descr") != null) {
			addIncidentForm.setNotes(request.getParameter("descr").toString());
			addIncidentForm.setCkboxother("true");
		}

		if (addIncidentForm.getSave() != null) {
			int checklengthi = 0;
			int checklengtha = 0;
			if (addIncidentForm.getCkboxincident() != null) {
				checklengthi = addIncidentForm.getCkboxincident().length;
			}

			if (addIncidentForm.getCkboxaction() != null) {
				checklengtha = addIncidentForm.getCkboxaction().length;
			}

			int[] indexi = new int[checklengthi];
			String stringi = "";
			for (int i = 0; i < checklengthi; i++) {
				if (addIncidentForm.getCkboxincident(i) != null) {
					indexi[i] = Integer.parseInt(addIncidentForm
							.getCkboxincident(i));

					if (i == (checklengthi - 1)) {
						stringi = stringi + indexi[i];
					} else {
						stringi = stringi + indexi[i] + ",";
					}
				}
			}

			String stringa = "";
			int[] indexa = new int[checklengtha];
			for (int i = 0; i < checklengtha; i++) {
				if (addIncidentForm.getCkboxaction(i) != null) {
					indexa[i] = Integer.parseInt(addIncidentForm
							.getCkboxaction(i));

					if (i == (checklengtha - 1)) {
						stringa = stringa + indexa[i];
					} else {
						stringa = stringa + indexa[i] + ",";
					}

				}
			}

			if (addIncidentForm.getCkboxother() != null) {
				note = addIncidentForm.getNotes();
			}

			if (addIncidentForm.getHours().equals("00")) {
				addIncidentForm.setHours("12");
			}

			String datetime = addIncidentForm.getDate() + " "
					+ addIncidentForm.getHours() + ":"
					+ addIncidentForm.getMinutes() + addIncidentForm.getAm();

			// if (addIncidentForm.getIncidentStatus().equals("Pending")) {
			//
			// incidentStatus = 1;
			// } else if
			// (addIncidentForm.getIncidentStatus().equals("In-Review")) {
			//
			// incidentStatus = 2;
			// } else if
			// (addIncidentForm.getIncidentStatus().equals("Complete")) {
			//
			// incidentStatus = 3;
			// } else {
			//
			// incidentStatus = 4;
			// }
			String userName = ((String) session.getAttribute("username"));
			if (addIncidentForm.getTechnicianname() == null
					|| addIncidentForm.getTechnicianname().equals("")) {

				addIncidentForm.setTechnicianname("0");

			}
			statusvalue = Pvsdao.addIncident(
					addIncidentForm.getTechnicianname(),
					addIncidentForm.getPid(), datetime,
					addIncidentForm.getEndcustomer(),
					addIncidentForm.getIncidentTypeName(),
					addIncidentForm.getNotes(), stringa, userid,
					addIncidentForm.getIncidenttype(), "",
					addIncidentForm.getJobid(), addIncidentForm.getPage(),
					addIncidentForm.getIncidentStatus(),
					Integer.parseInt(addIncidentForm.getIncidentSeverity()),
					userid, getDataSource(request, "ilexnewDB"));
			addIncidentForm.setAddmessage("" + statusvalue);
			request.setAttribute("refreshtree", "true");

			if (statusvalue >= 0) {
				executeElse = 1;
			}

			if (statusvalue > 0) {

				addIncidentForm.setInid("" + statusvalue);
				String[] list = new String[21];
				list = Pvsdao.getIncidentreportdetails(
						addIncidentForm.getInid(),
						getDataSource(request, "ilexnewDB"));
				addIncidentForm.setPartnername(list[1]);
				addIncidentForm.setTechnicianname(list[2]);
				addIncidentForm.setDate(list[3]);
				addIncidentForm.setTime(list[4]);
				addIncidentForm.setEndcustomer(list[5]);
				addIncidentForm.setIncidentlist(list[6]);
				addIncidentForm.setNotes(list[7]);
				addIncidentForm.setActionlist(list[8]);
				addIncidentForm.setIncidenttype(list[9]);
				// addIncidentForm.setPartnerresponse(list[10]);
				addIncidentForm.setJobname(list[11]);
				addIncidentForm.setAppendixname(list[12]);
				addIncidentForm.setMsaname(list[13]);
				addIncidentForm.setAddedBy(list[14]);
				addIncidentForm.setIncidentSeverity(list[15]);
				addIncidentForm.setIncidentStatus(list[16]);
				addIncidentForm.setIncidentDropValue(list[17]);
				addIncidentForm.setNotecpd(list[18]);
				addIncidentForm.setUpdatedBy(list[19]);
				addIncidentForm.setIncidentdrop(list[20]);

				// String incidentStatusValue = "Denied";
				// if (addIncidentForm.equals("1")) {
				// incidentStatusValue = "Pending";
				// } else if (addIncidentForm.getIncidentStatus().equals("2")) {
				// incidentStatusValue = "In-Review";
				// } else if (addIncidentForm.getIncidentStatus().equals("3")) {
				// incidentStatusValue = "Complete";
				// }
				//
				// addIncidentForm.setIncidentStatus(incidentStatusValue);
				// String incidentdropValue = "";
				// if (addIncidentForm.getIncidentdrop().equals("1")) {
				//
				// incidentStatusValue = "Attendance";
				// } else if (addIncidentForm.getIncidentdrop().equals("2")) {
				//
				// incidentStatusValue = "Missing Tools";
				// } else if (addIncidentForm.getIncidentdrop().equals("3")) {
				//
				// incidentStatusValue = "Behavioral/Unprofessional";
				// } else if (addIncidentForm.getIncidentdrop().equals("4")) {
				//
				// incidentStatusValue = "Performance";
				// } else if (addIncidentForm.getIncidentdrop().equals("6")) {
				//
				// incidentStatusValue = "Drug Policy";
				// } else if (addIncidentForm.getIncidentdrop().equals("7")) {
				//
				// incidentStatusValue = "Others";
				// } else if (addIncidentForm.getIncidentdrop().equals("8")) {
				//
				// incidentStatusValue = "Theft";
				// }
				// addIncidentForm.setIncidentdrop(incidentStatusValue);
				String server = request.getServerName() + ":"
						+ request.getServerPort();

				/*
				 * sendEmail(addIncidentForm.getPartnername(),
				 * addIncidentForm.getAddedBy(), getDataSource(request,
				 * "ilexnewDB"));
				 */
				StringBuilder linkToIncidentReport = new StringBuilder(
						"http://");
				linkToIncidentReport.append(server);
				linkToIncidentReport
						.append("/Ilex/AddIncident.do?function=viewParticularIncident&incidentId="
								+ addIncidentForm.getInid()
								+ "&pid="
								+ addIncidentForm.getPid()
								+ "&module=PVS&page=PVS");

				Map<String, String> model = new HashMap<String, String>();
				model.put("partnerName", addIncidentForm.getPartnername());
				model.put("linkToIncidentReport",
						linkToIncidentReport.toString());
				String emaildetail = "";
				String emailContent = VelocityUtils.getMailContent(
						"emailTemplate/incidentReport.vm", model);
				emaildetail = "<table width=990 cellspacing=0 cellpadding=0 border=0>"
						+ "<tbody>"
						+ "<tr><td style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\';word-break: keep-all; \">"
						// + "<span><b>Date: " + "</b>" + requestor + " " + to +
						// "</span><br>"
						+ "<br><span><b>Date: </b>"
						+ addIncidentForm.getDate()
						+ "</span><br>"
						+ "<span><b>Time: </b>"
						+ addIncidentForm.getTime()
						+ "</span><br>"
						+ "<span><b>End Customer: </b>"
						+ addIncidentForm.getEndcustomer() + "</span><br>";
				if (!partnerTopType.equals("Minuteman Partners")) {

					emaildetail = emaildetail
							+ "<span><b>Technician Name: </b>"
							+ addIncidentForm.getTechnicianname() + "</span>"
							+ "<br>";
				}

				// Check Status before sending Email
				String incidentstatusflag = "Denied";
				if (addIncidentForm.getIncidentStatus().equals("1")) {
					incidentstatusflag = "Pending";
				} else if (addIncidentForm.getIncidentStatus().equals("2")) {
					incidentstatusflag = "In-Review";
				} else if (addIncidentForm.getIncidentStatus().equals("3")) {
					incidentstatusflag = "Complete";
				}

				emaildetail = emaildetail + "<span><b>Incident Type: </b>"
						+ addIncidentForm.getIncidentlist() + "</span><br>"
						+ "<span><b>Incident Description: </b>"
						+ addIncidentForm.getNotes() + "</span><br>"
						+ "<span><b>Incident Status: </b>" + incidentstatusflag
						+ "</span><br>" + "<span><b>Incident Severity: </b>"
						+ addIncidentForm.getIncidentSeverity() + "</span><br>"
						+ "<span><b>Added by: </b>"
						+ addIncidentForm.getAddedBy() + "</span><br>"
						+ "</td></tr>" + "<tr><td>&nbsp;</td></tr>"
						+ "</tbody>" + "</table><br><br>";
				// emailContent
				CreateEmail.sendIncidentMail(emailContent + emaildetail,
						userid, getDataSource(request, "ilexnewDB"));

			}
			partnerstatus = Pvsdao.getPartnerStatus(addIncidentForm.getPid(),
					getDataSource(request, "ilexnewDB"));
			addIncidentForm.setPartnerstatus(partnerstatus);
			request.setAttribute("inidvalue", addIncidentForm.getInid());
			request.setAttribute("incidentform", addIncidentForm);
			addIncidentForm.setFlag("1");
		}
		request.setAttribute("totalreports", 1);
		String cpduser = Pvsdao.checkforAccountORCPDRole(userid,
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("cpduser", cpduser);

		if (executeElse == 0) {
			ArrayList ilist = new ArrayList();
			ArrayList alist = new ArrayList();
			ArrayList custlist = new ArrayList();
			ArrayList techlist = new ArrayList();
			ArrayList ampmlist = new ArrayList();

			/* Start: Added By Atul 16/01/2007 */
			String strDate = "";
			String strHours = "";
			String strMin = "";
			String strAm = "";
			Calendar c = Calendar.getInstance();
			strDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE)
					+ "/" + c.get(Calendar.YEAR);
			addIncidentForm.setDate(strDate);

			if (c.get(Calendar.HOUR) < 10) {
				strHours = "0" + c.get(Calendar.HOUR);
			} else {
				strHours = "" + c.get(Calendar.HOUR);
			}

			if (c.get(Calendar.MINUTE) < 10) {
				strMin = "0" + c.get(Calendar.MINUTE);
			} else {
				strMin = "" + c.get(Calendar.MINUTE);
			}

			if (c.get(Calendar.AM_PM) == 1) {
				strAm = "PM";
			} else {
				strAm = "AM";
			}

			addIncidentForm.setHours(strHours);
			addIncidentForm.setMinutes(strMin);
			addIncidentForm.setAm(strAm);
			/* End: Added By Atul 16/01/2007 */

			ampmlist = dcomboPVS.getAmpmlist();
			dcpvs.setAmpmlist(ampmlist);
			String partnername = Pvsdao.getPartnername(
					addIncidentForm.getPid(),
					getDataSource(request, "ilexnewDB"));
			addIncidentForm.setPartnername(partnername);
			ilist = Pvsdao.getIncidentlist(getDataSource(request, "ilexnewDB"));
			dcpvs.setIncidentTypeList(ilist);
			request.setAttribute("ilist", ilist);

			alist = Pvsdao.getActionlist(getDataSource(request, "ilexnewDB"));
			request.setAttribute("alist", alist);
			AddIncidentBean addIncidentBean = new AddIncidentBean();
			BeanUtils.copyProperties(addIncidentBean, addIncidentForm);
			if (addIncidentForm.getPage() != null) {
				if (addIncidentForm.getPage().equals("checklist")) {
					Pvsdao.getCpIncidentQcChecklistDefault(addIncidentBean,
							getDataSource(request, "ilexnewDB"));
					BeanUtils.copyProperties(addIncidentForm, addIncidentBean);
					addIncidentForm
							.setEndcustomer(((NameId) com.mind.dao.PM.Activitydao.getIdName(
									addIncidentForm.getJobid(), "PVS",
									"Customer",
									getDataSource(request, "ilexnewDB")))
									.getId());
				}

			}
			custlist = dcomboPVS.getCustomerlist("C", addIncidentForm.getPid(),
					getDataSource(request, "ilexnewDB"));

			if (custlist.size() > 0) {
				dcpvs.setCustomerlist(custlist);
			}

			techlist = dcomboPVS.getTechnicianlist(addIncidentForm.getPid(),
					getDataSource(request, "ilexnewDB"));
			if (techlist.size() > 0) {
				dcpvs.setTechnicianlist(techlist);
			}
			request.setAttribute("dynamiccombo", dcpvs);

			partnerstatus = Pvsdao.getPartnerStatus(addIncidentForm.getPid(),
					getDataSource(request, "ilexnewDB"));
			addIncidentForm.setPartnerstatus(partnerstatus);
			addIncidentForm.setIncidenttype(incidenttype);
		}

		if (addIncidentForm.getFlag() != null) {
			if (statusvalue >= 0) {
				if (addIncidentForm.getPage().equals("checklist")) {
					request.setAttribute("viewjobtype",
							addIncidentForm.getViewjobtype());
					request.setAttribute("ownerId",
							addIncidentForm.getOwnerId());
					request.setAttribute("type", "qchecklist");
					request.setAttribute("job_Id", addIncidentForm.getJobid());
					forward = mapping.findForward("qcchecklist");
				} else {
					// response.sendRedirect("/Ilex/AddIncident.do?pid="
					// + addIncidentForm.getPid()
					// + "&function=view&page=PVS");
					// return null;
					forward = mapping.findForward("success");
				}
			} else {
				forward = mapping.findForward("failure");
			}
		} else {
			forward = mapping.findForward("failure");
		}
		return (forward);
	}

	/*
	 * public static void sendEmail(String partnerName, String addedBy,
	 * DataSource ds) { EmailBean emailform = new EmailBean();
	 * emailform.setUsername(Util.getKeyValue("common.Email.username"));
	 * emailform
	 * .setUserpassword(Util.getKeyValue("common.Email.userpassword"));
	 * emailform.setSmtpservername(EnvironmentSelector
	 * .getBundleString("common.Email.smtpservername"));
	 * emailform.setSmtpserverport(Util
	 * .getKeyValue("common.Email.smtpserverport"));
	 * emailform.setTo("cpd@contingent.com"); emailform.setCc("");
	 * emailform.setBcc(""); emailform.setSubject("New incident notification");
	 * emailform.setFrom(EnvironmentSelector
	 * .getBundleString("appendix.default.Emailid"));
	 * emailform.setFrom("ilexsupport@contingent.com");
	 * emailform.setContent("text/html"); emailform.setType("partner_create");
	 * emailform.setMessageContent("Partner Name: " + partnerName +
	 * "\n\rAdded By: " + addedBy); if
	 * (Email.isValidEmailAddress(emailform.getTo())) { Email.Send(emailform,
	 * "text/html", ds); }
	 * 
	 * // }
	 */
	/**
	 * This method displays Incident Details for particular incident called From
	 * Email link .
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward viewParticularIncident(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		// int totalreports = 0;
		AddIncidentForm addIncidentForm = (AddIncidentForm) form;
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired
		String userid = (String) session.getAttribute("userid");

		if (addIncidentForm.getAuthenticate().equals("")) {
			if (!Authenticationdao
					.getPageSecurity(userid, "Manage PVS",
							"View Incident Report",
							getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		addIncidentForm.setPid(request.getParameter("pid"));
		addIncidentForm.setInid(request.getParameter("incidentId"));
		request.setAttribute("refreshtree", "true");

		String partnerTopType = Pvsdao.getTopPartnerType(
				addIncidentForm.getPid(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("topPartnerName", "" + partnerTopType);

		String[] list = new String[21];
		list = Pvsdao.getIncidentreportdetails(addIncidentForm.getInid(),
				getDataSource(request, "ilexnewDB"));
		addIncidentForm.setPartnername(list[1]);
		addIncidentForm.setTechnicianname(list[2]);
		addIncidentForm.setDate(list[3]);
		addIncidentForm.setTime(list[4]);
		addIncidentForm.setEndcustomer(list[5]);
		addIncidentForm.setIncidentlist(list[6]);
		addIncidentForm.setNotes(list[7]);
		addIncidentForm.setActionlist(list[8]);
		addIncidentForm.setIncidenttype(list[9]);
		addIncidentForm.setResponse(list[10]);
		addIncidentForm.setJobname(list[11]);
		addIncidentForm.setAppendixname(list[12]);
		addIncidentForm.setMsaname(list[13]);
		addIncidentForm.setAddedBy(list[14]);
		addIncidentForm.setIncidentSeverity(list[15]);
		addIncidentForm.setIncidentStatus(list[16]);
		addIncidentForm.setIncidentDropValue(list[17]);
		addIncidentForm.setNotecpd(list[18]);
		addIncidentForm.setUpdatedBy(list[19]);
		addIncidentForm.setIncidentdrop(list[20]);

		String incidentStatusValue = "Denied";
		if (addIncidentForm.equals("1")) {
			incidentStatusValue = "Pending";
		} else if (addIncidentForm.getIncidentStatus().equals("2")) {
			incidentStatusValue = "In-Review";
		} else if (addIncidentForm.getIncidentStatus().equals("3")) {
			incidentStatusValue = "Complete";
		}
		addIncidentForm.setIncidentStatus(incidentStatusValue);

		request.setAttribute("inidvalue", addIncidentForm.getInid());
		request.setAttribute("incidentform", addIncidentForm);
		request.setAttribute("totalreports", "1");
		addIncidentForm.setFlag("1");
		forward = mapping.findForward("success");

		return (forward);

	}

	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		int totalreports = 0;
		AddIncidentForm addIncidentForm = (AddIncidentForm) form;
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired
		String userid = (String) session.getAttribute("userid");
		if (addIncidentForm.getAuthenticate().equals("")) {
			if (!Authenticationdao
					.getPageSecurity(userid, "Manage PVS",
							"View Incident Report",
							getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		addIncidentForm.setPid(request.getParameter("pid"));

		String partnerTopType = Pvsdao.getTopPartnerType(
				addIncidentForm.getPid(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("topPartnerName", "" + partnerTopType);

		ArrayList list = new ArrayList();
		list = Pvsdao.getAllreportdetails(addIncidentForm.getPid(),
				getDataSource(request, "ilexnewDB"));
		totalreports = list.size();

		String cpduser = Pvsdao.checkforAccountORCPDRole(userid,
				getDataSource(request, "ilexnewDB"));

		// boolean flagUser = Boolean.valueOf(cpduser);

		if (list.size() > 0) {
			request.setAttribute("allreportslist", list);

		}
		request.setAttribute("totalreports", totalreports);
		request.setAttribute("cpduser", cpduser);
		request.setAttribute("incidentform", addIncidentForm);
		request.setAttribute("flag", 1);
		forward = mapping.findForward("success");

		return (forward);

	}

	/**
	 * This method deletes Incident for a partner in PVS Manager.
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddIncidentForm addIncidentForm = (AddIncidentForm) form;
		String deleteids = request.getParameter("did");

		int statusvalue = 0;
		// int totalreports = 0;
		String partnerTopType = Pvsdao.getTopPartnerType(
				addIncidentForm.getPid(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("topPartnerName", "" + partnerTopType);

		statusvalue = Pvsdao.deleteIncident(deleteids,
				getDataSource(request, "ilexnewDB"));

		addIncidentForm.setDeletemessage("" + statusvalue);
		request.setAttribute("refreshtree", "true");
		addIncidentForm.setPid(request.getParameter("pid"));
		// action=2 means incident is deleted after incident is saved.
		// action=1 means incident is deleted after view incident page is
		// directly opened.
		ArrayList list = new ArrayList();
		if (request.getParameter("action").equals("2")) {

			list = Pvsdao.getAllreportdetails(addIncidentForm.getPid(),
					getDataSource(request, "ilexnewDB"));
			// totalreports = list.size();
			if (list.size() > 0) {
				request.setAttribute("allreportslist", list);
			}
		}
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		String cpduser = Pvsdao.checkforAccountORCPDRole(userid,
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("cpduser", cpduser);

		request.setAttribute("incidentform", addIncidentForm);
		request.setAttribute("totalreports", list.size());
		forward = mapping.findForward("success");
		return (forward);

	}

	// update details save ffrom incident
	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();

		AddIncidentForm addIncidentForm = (AddIncidentForm) form;
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired
		String userid = (String) session.getAttribute("userid");
		if (addIncidentForm.getAuthenticate().equals("")) {
			if (!Authenticationdao
					.getPageSecurity(userid, "Manage PVS",
							"View Incident Report",
							getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		addIncidentForm.setPid(request.getParameter("pid"));
		addIncidentForm.setInid(request.getParameter("did"));
		// String[] responselist = request.getParameterValues("response");
		String from = (String) request.getParameter("from");
		// String userid = (String) session.getAttribute("userid");
		String userName = ((String) session.getAttribute("username"));
		String[] values;// =new String[5];
		// String[] partnerresponse;
		// String[] partnerresponseValue = null;
		// String[] cpdnotes;
		String url = "";
		String[] strArrayinner = null;

		String list = "";
		String strArray[] = null;
		if (request.getParameter("str") != null) {
			list = (String) request.getParameter("str");
			strArray = list.split("\\|@\\|");
		}

		/*
		 * System.out.println("String converted to String array");
		 */
		// print elements of String array
		/*
		 * for (int i = 0; i < strArray.length; i++) { strArrayinner =
		 * strArray[i].split("~~"); System.out.println(strArrayinner[i]); }
		 */
		// String jobType = "";
		// int count = 0;
		int listsave = 0;

		String[] IncidentStatusDrop = null;

		// IncidentStatusDrop = addIncidentForm.getIncidentStatusDrop();
		// int incidentStatus = 0;
		// if (IncidentStatusDrop != null) {
		// // incidentStatus =
		// // addIncidentForm.getIncidentStatus(IncidentStatusDrop[i]);
		// }

		// private String[] incidentStatusDrop = null;

		//
		values = addIncidentForm.getChkdelete();
		// partnerresponse = addIncidentForm.getPartnerresponse();
		/*
		 * for (int i = 0; i < partnerresponse.length; i++) {
		 * 
		 * if (!partnerresponse.equals("")) {
		 * 
		 * partnerresponseValue[i] = partnerresponse[i];
		 * 
		 * }
		 * 
		 * }
		 */
		String partnerTopType = Pvsdao.getTopPartnerType(
				addIncidentForm.getPid(), getDataSource(request, "ilexnewDB"));
		request.setAttribute("topPartnerName", "" + partnerTopType);
		// cpdnotes = addIncidentForm.getCpdnotes();

		if (values == null && from.equals("single")) {
			String wordsResponse = request.getParameter("response");
			String wordsNotes = request.getParameter("notecpd");

			listsave = Pvsdao.setDetails(addIncidentForm.getPid(),
					wordsResponse, wordsNotes, addIncidentForm.getInid(),
					addIncidentForm.getIncidentStatus(), userid,
					getDataSource(request, "ilexnewDB"));

			// }
		} else {

			for (int i = 0; i < strArray.length; i++) {
				strArrayinner = strArray[i].split("~~");

				listsave = Pvsdao.setDetails(addIncidentForm.getPid(),
						strArrayinner[1].toString(), strArrayinner[2],
						strArrayinner[0], strArrayinner[3], userid,
						getDataSource(request, "ilexnewDB"));

			}

		}
		request.setAttribute("refreshtree", "true");

		// JobInformationperPartner.do?pid=

		url = "/AddIncident.do?pid=" + addIncidentForm.getPid()
				+ "&function=view&page=PVS";

		// url =
		// "/Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid="
		// + addIncidentForm.getPid();
		ActionForward fwd = new ActionForward();
		fwd.setPath(url);
		return fwd;
	}

	private static String[] removeNullValues(String input[],
			boolean preserveInput) {

		String[] str;
		if (preserveInput) {
			str = new String[input.length];
			System.arraycopy(input, 0, str, 0, input.length);
		} else {
			str = input;
		}

		// Filter values null, empty or with blank spaces
		int p = 0, i = 0;
		for (; i < str.length; i++, p++) {
			str[p] = str[i];
			if (str[i] == null || str[i].isEmpty()
					|| (str[i].startsWith(" ") && str[i].trim().isEmpty()))
				p--;
		}

		// Resize the array
		String[] tmp = new String[p];
		System.arraycopy(str, 0, tmp, 0, p);
		str = null;

		return tmp;
	}

}
