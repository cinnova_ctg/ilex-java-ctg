/**
 * Copyright (C) 2005 MIND All rights reserved. The information contained here
 * in is confidential and proprietary to MIND and forms the part of MIND Project
 * : ILEX Description	:PVS Manager class is used to get the details of the
 * specific partners.
 *
 */
package com.mind.actions.PVS;

import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.pvs.PartnerDetailBean;
import com.mind.common.LabelValue;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.PartnerDetailForm;

public class PartnerDetailAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);

		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired

		PartnerDetailForm partnerdetailform = (PartnerDetailForm) form;

		PartnerDetailBean partnerDetailBean = new PartnerDetailBean();
		if (request.getParameter("partnerId") == null
				&& request.getParameter("poId") != null) {
			partnerdetailform.setPartnerId(Pvsdao.getPartnerForPO(
					request.getParameter("poId"),
					getDataSource(request, "ilexnewDB")));
		}
		BeanUtils.copyProperties(partnerDetailBean, partnerdetailform);
		String checkPartner = Pvsdao.getSearchPartnerDetail(partnerDetailBean,
				request.getRequestURL().toString(),
				getDataSource(request, "ilexnewDB"));

		ArrayList<LabelValue> imagesList = new ArrayList<LabelValue>();
		LabelValue labelValue = null;
		for (int index = 0; index < partnerDetailBean.getImgPath().split(
				"\\|\\|\\|\\|").length; index++) {
			labelValue = new LabelValue();
			labelValue.setLabel(partnerDetailBean.getBadgeName().split(
					"\\|\\|\\|\\|")[index]);
			labelValue.setValue(String.valueOf(partnerDetailBean.getImgPath()
					.split("\\|\\|\\|\\|")[index]));
			imagesList.add(labelValue);
		}

		BeanUtils.copyProperties(partnerdetailform, partnerDetailBean);

		partnerdetailform.setImgPath(partnerDetailBean.getImgPath().split(
				"\\|\\|\\|\\|")[0]);

		partnerdetailform.setImagesList(imagesList);

		if (request.getParameter("partnerId") == null
				&& request.getParameter("poId") != null) {
			request.setAttribute("showPartnerType", "showPartnertype");
		}
		if (!checkPartner.equals("partnerFound")) {
			request.setAttribute("found", "failed");
		}

		response.setContentType("text/html");
		RequestDispatcher rd = request
				.getRequestDispatcher("/PVS/PartnerDetail.jsp");
		rd.include(request, response);
		return null;
	}
}
