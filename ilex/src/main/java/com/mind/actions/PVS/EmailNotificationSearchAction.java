package com.mind.actions.PVS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.EmailNotificationSearchForm;

public class EmailNotificationSearchAction extends com.mind.common.IlexAction
{

	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		EmailNotificationSearchForm emailNotificationSearchForm = ( EmailNotificationSearchForm ) form;
		int contactlistsize = 0;
		
		if( emailNotificationSearchForm.getSearch() != null )
		{
			
			emailNotificationSearchForm.setContactlist( Pvsdao.getPocsearchresult(emailNotificationSearchForm.getPocname() , emailNotificationSearchForm.getDivison() , getDataSource( request , "ilexnewDB" )  )) ;
			
				
		
			if( emailNotificationSearchForm.getContactlist().size() > 0 )
			{
				contactlistsize = emailNotificationSearchForm.getContactlist().size();
				
			}
		
		}
		
	
		
		
		
		request.setAttribute( "contactlistsize" , contactlistsize+"" );
		
		
		return mapping.findForward( "success" );
	}

}
