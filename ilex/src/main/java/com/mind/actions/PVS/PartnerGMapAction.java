package com.mind.actions.PVS;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PVS.Pvsdao;
import com.mind.newjobdb.dao.VendexDAO;
public class PartnerGMapAction extends com.mind.common.IlexAction
{
	
	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception 
	{	
		HttpSession session = request.getSession( true );
		String mapType="";
		String jobId="";
		String partnerId ="";
		String radius="";
		String partnerName="";
		String jobTitle="";
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if(request.getParameter("maptype") != null) {
			mapType=request.getParameter("maptype") ;
		}
		if(request.getParameter("jobId")!=null && !request.getParameter("jobId").equals("") ) {
			jobId =request.getParameter("jobId");
			jobTitle=Pvsdao.getJobTitle(jobId,getDataSource(request,"ilexnewDB"));
			String poXml = VendexDAO.getJobPOnotAssignedtoPartner(request.getParameter("jobId"),getDataSource(request,"ilexnewDB"));
			request.setAttribute("poXml",poXml);
			request.setAttribute("jobTitle",jobTitle);
		}
		if(request.getParameter("partnerId")!=null ) {
			partnerId =request.getParameter("partnerId") ;
		}
		if(request.getParameter("radius")!=null ) {
			radius =request.getParameter("radius") ;
		}
		if(!partnerId.equals("")) {
			ArrayList siteList=new ArrayList();
			ArrayList partnerList=new ArrayList();
			String siteXml="";
			String partnerXml="";
			if(jobId.equals("")) {
				partnerName=Pvsdao.getPartnerName(partnerId,getDataSource(request,"ilexnewDB"));
				request.setAttribute("partnerName",partnerName);
				jobId="0";
			} else {
				request.setAttribute("vendexjobName","vendexjobName");
				
			}
			
			siteList =  Pvsdao.getSiteDeatils(jobId,partnerId,"partnertojob",radius,getDataSource(request,"ilexnewDB"));
			partnerList = Pvsdao.getpartnerDeatilsList(Integer.parseInt(partnerId),getDataSource(request,"ilexnewDB"));
			siteXml=Pvsdao.convertSiteRatingToXML("JOB","","",siteList);
			partnerXml=Pvsdao.convertPartnerRatingToXML(partnerList);
			request.setAttribute("partnerXML",partnerXml);
			request.setAttribute("siteXML",siteXml);
			request.setAttribute("checkXML",session.getAttribute("checkXML"));
			request.setAttribute("centerPartner","centerPartner");
		} else if(mapType.equals("jobtojob")) {
			request.setAttribute("partnerXML",session.getAttribute("partnerXML"));
			request.setAttribute("siteXML",session.getAttribute("siteXML"));
			request.setAttribute("checkXML",session.getAttribute("checkXML"));
			request.setAttribute("vendexMapView","vendexMapView");
		} else if(!jobId.equals("")) {
				request.setAttribute("partnerXML",session.getAttribute("partnerXML"));
				request.setAttribute("siteXML",session.getAttribute("singleSiteXML"));
				request.setAttribute("checkXML",session.getAttribute("checkXML"));
				request.setAttribute("standard","satandard");
			} else {
				request.setAttribute("partnerXML",session.getAttribute("partnerXML"));
				request.setAttribute("siteXML",session.getAttribute("siteXML"));
				request.setAttribute("checkXML",session.getAttribute("checkXML"));
				request.setAttribute("MapView","MapView");
		}
		//System.out.println("check session value "+request.getAttribute("siteXML"));
		//System.out.println("check session value for Partner "+request.getAttribute("partnerXML"));
		//System.out.println("check session value for Partner "+request.getAttribute("checkXML"));
		//System.out.println("check session value for Partner "+request.getAttribute("poXml"));
		return mapping.findForward("success");
	}
}

