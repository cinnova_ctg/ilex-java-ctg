package com.mind.actions.PVS;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.PVSSearchForm;

public class PVSSearchAction extends com.mind.common.IlexAction {
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{

		PVSSearchForm pvssearchForm = (PVSSearchForm)form;
		ArrayList PvsSearchResult=null;
		String sortqueryclause = "";
		String sortqueryclausebydate = "";
		boolean search = false;
		DataSource ds = getDataSource(request, "ilexnewDB");
		HttpSession session = request.getSession( true );
		
		//ParterTypeCombo
		DynamicComboCM dcPartnerType= new DynamicComboCM();
		dcPartnerType.setFirstlevelcatglist(DynamicComboDao.getPartnerTypeCombo());
		request.setAttribute("dcPartnerType",dcPartnerType);
		
		if(request.getAttribute("orgTopName") != null){
			pvssearchForm.setOrgTopName(request.getAttribute("orgTopName")+"");
		}
		if(request.getAttribute("action") != null){
			pvssearchForm.setAct(request.getAttribute("action")+"");
		}
		if(request.getAttribute("startdate") != null){
			pvssearchForm.setStartdate(request.getAttribute("startdate")+"");
		}
		if(request.getAttribute("enddate") != null){
			pvssearchForm.setEnddate(request.getAttribute("enddate")+"");
		}
		if(request.getAttribute("division") != null){
			pvssearchForm.setDivision(request.getAttribute("division")+"");
		}
		if(request.getAttribute("partnerType") != null){
			pvssearchForm.setPartnerType(request.getAttribute("partnerType")+"");
		}
		if(request.getAttribute("fromsort") != null){
			search = true;
		}
		if( session.getAttribute( "pvssearch_sort" ) != null ){
			sortqueryclause = ( String ) session.getAttribute( "pvssearch_sort" );
		}
		if( session.getAttribute( "pvssearch_date_sort" ) != null ){
			sortqueryclausebydate = ( String ) session.getAttribute( "pvssearch_date_sort" );
		}
		
		if(request.getParameter("orgTopName") != null){
			pvssearchForm.setOrgTopName(request.getParameter("orgTopName"));
		}
		
		if(request.getParameter("action") != null){
			pvssearchForm.setAct(request.getParameter("action"));
		}
		if(request.getParameter("firsttime") != null){
			request.setAttribute("firsttime", "firsttime");
		}
		if(request.getAttribute("firsttime") != null || request.getAttribute("refreshtree") !=null){
			search = true;
		}
		if(request.getParameter("clear") != null){
			pvssearchForm.setStartdate(Pvsdao.getDate("startdate",ds));
			pvssearchForm.setEnddate(Pvsdao.getDate("enddate",ds));
		}
		if(pvssearchForm.getAct().equals("resentupdated") || pvssearchForm.getAct().equals("newadded")){
			if(pvssearchForm.getStartdate() == null && pvssearchForm.getEnddate() == null){
				pvssearchForm.setStartdate(Pvsdao.getDate("startdate",ds));
				pvssearchForm.setEnddate(Pvsdao.getDate("enddate",ds));
			}
		}
		
		if(pvssearchForm.getAct().equals("annualreport")){
			if(pvssearchForm.getStartdate() == null && pvssearchForm.getEnddate() == null){
				pvssearchForm.setStartdate(Pvsdao.getAnnualDate("startdate",ds));
				pvssearchForm.setEnddate(Pvsdao.getAnnualDate("enddate",ds));
			}
		}
		
		if(request.getParameter("enablesort")!=null && request.getParameter("enablesort").equals("true")){
			sortqueryclausebydate = "";
			if( session.getAttribute( "pvssearch_date_sort" ) != null ){
				session.removeAttribute( "pvssearch_date_sort" ); 
			}
		}
		
		if(search==true){
			if(pvssearchForm.getDivision() != null && !pvssearchForm.getDivision().equals("")){
				PvsSearchResult = Pvsdao.getPOCResult(pvssearchForm.getDivision()+"%", pvssearchForm.getOrgTopName(),"","","",sortqueryclause,"0",ds);
				if (PvsSearchResult.size() > 0) {
					request.setAttribute("searchPvsResult", PvsSearchResult);
				}
			}
		}
		if(pvssearchForm.getAct() != null && pvssearchForm.getAct().equals("resentupdated")){
			PvsSearchResult = Pvsdao.getPOCResult("", "",pvssearchForm.getStartdate(),pvssearchForm.getEnddate(),"RU",sortqueryclausebydate,pvssearchForm.getPartnerType(),ds);
			if (PvsSearchResult.size() > 0) {
				request.setAttribute("searchPvsResult", PvsSearchResult);
			}
		}
		if(pvssearchForm.getAct() != null && pvssearchForm.getAct().equals("newadded")){
			PvsSearchResult = Pvsdao.getPOCResult("", "",pvssearchForm.getStartdate(),pvssearchForm.getEnddate(),"NA",sortqueryclausebydate,pvssearchForm.getPartnerType(),ds);
			if (PvsSearchResult.size() > 0) {
				request.setAttribute("searchPvsResult", PvsSearchResult);
			}
		}
		
		if(pvssearchForm.getAct() != null && pvssearchForm.getAct().equals("annualreport")){
			PvsSearchResult = Pvsdao.getPOCResult("", "",pvssearchForm.getStartdate(),pvssearchForm.getEnddate(),"ARD",sortqueryclausebydate,pvssearchForm.getPartnerType(),ds);
			if (PvsSearchResult.size() > 0) {
				request.setAttribute("searchPvsResult", PvsSearchResult);
			}
		}
		
		return mapping.findForward("success");
	}
}
