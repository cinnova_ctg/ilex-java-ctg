
/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is an action class used for performing multiple
* operations Add,update of email list.      
*
*/
package com.mind.actions.PVS;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboPVS;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDaoPVS;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.Change_NotificationForm;


public class Change_NotificationAction extends com.mind.common.IlexAction 
{

/** This method manages email notification list of persons for sending a particular document. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
	
	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)		throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		String emaillist="";
		int formid=0;
		int statusvalue=0;
		Change_NotificationForm change_NotificationForm = (Change_NotificationForm) form;
		
		DynamicComboDaoPVS dcomboPVS=new DynamicComboDaoPVS();
		DynamicComboPVS dcpvs = new DynamicComboPVS();

		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if( change_NotificationForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage PVS" , "Change Notification" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/

				
		ArrayList formlist = new ArrayList();
		formlist = dcomboPVS.getFormlist(getDataSource(request,"ilexnewDB"));
		dcpvs.setFormlist(formlist);
		request.setAttribute("dcpvs",dcpvs);
		
		
		if( change_NotificationForm.getRefresh())
		{
			formid=Integer.parseInt(change_NotificationForm.getFormname());
			emaillist=Pvsdao.getEmailnotificationlist(formid,getDataSource(request,"ilexnewDB"));
			change_NotificationForm.setEmaillist(emaillist);
			change_NotificationForm.setRefresh(false);
			
		}
		if(change_NotificationForm.getSave()!=null)
		{
			if(Integer.parseInt(change_NotificationForm.getFormname())!=0)
			{
				statusvalue = Pvsdao.updateEmaillist(Integer.parseInt(change_NotificationForm.getFormname()),change_NotificationForm.getEmaillist(),userid,getDataSource(request,"ilexnewDB"));
				if(change_NotificationForm.getEmaillist()!="")
				{
					change_NotificationForm.setUpdatemessage(""+statusvalue);
				}
				
				
			}
			change_NotificationForm.setSave("null");
			
		}
		
		
		forward=mapping.findForward("success");
		return (forward);

	}
}
