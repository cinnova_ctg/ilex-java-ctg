package com.mind.actions.PVS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.Security_CertificateForm;

public class UploadCertificatesAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String partnerId = request.getParameter("partnerId");
		String technicianId = request.getParameter("technicianId");
		String fileType = request.getParameter("fileType");
		HttpSession session = request.getSession(true);
		String uploadCertificate = request.getParameter("uploadCertificate");
		if (uploadCertificate != null) {
			session.setAttribute("partnerId", partnerId);
			session.setAttribute("technicianId", technicianId);
			session.setAttribute("fileType", fileType);
			return mapping.findForward("success");
		}

		partnerId = (String) session.getAttribute("partnerId");
		technicianId = (String) session.getAttribute("technicianId");
		fileType = (String) session.getAttribute("fileType");
		DataSource ds = getDataSource(request, "ilexnewDB");

		String fileName = "";
		String fileRemarks = "";
		if (fileType != null) {
			if ("DrugScreenCert".equals(fileType)) {
				fileRemarks = "Drug Screen Certification";
			} else if ("CriminalCertificate".equals(fileType)) {
				fileRemarks = "Criminal Background Certification";
			} else if ("DrugScreen".equals(fileType)) {
				fileRemarks = "Drug Screen";
			} else {
				fileRemarks = "Background Check";
			}
		}

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire"));
		String userid = (String) session.getAttribute("userid");

		Security_CertificateForm certificateForm = (Security_CertificateForm) form;

		FormFile file = certificateForm.getCerticateFile();
		byte[] filedata = file.getFileData();
		fileName = file.getFileName();

		if (fileRemarks.contains("Certification")) {
			Pvsdao.saveSecurityFiles(technicianId, partnerId, "Technician",
					fileName, fileRemarks, filedata, userid, "2", ds);
		} else {
			Pvsdao.saveFiles(partnerId, "Partner", fileName, fileRemarks,
					filedata, userid, "2", ds);
		}

		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("File Uploaded");

		return null;

	}

}
