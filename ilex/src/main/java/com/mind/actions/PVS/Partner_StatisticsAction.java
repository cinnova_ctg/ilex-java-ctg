/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is an action class used for displaying partner summary in PVS Manager.      
*
*/
package com.mind.actions.PVS;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;

public class Partner_StatisticsAction extends com.mind.common.IlexAction 
{

	
/** This method displays partner summary in PVS Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/	
	public ActionForward execute( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)	throws Exception 
	{

			ActionErrors errors = new ActionErrors();
			ActionForward forward = new ActionForward();
			
			/* Page Security Start*/
			HttpSession session = request.getSession( true );
			if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
			String userid = (String)session.getAttribute("userid");
			if( !Authenticationdao.getPageSecurity( userid , "Manage PVS" , "View Partner Statistics" , getDataSource( request , "ilexnewDB" ) ) ) 
				{
					return( mapping.findForward( "UnAuthenticate" ) );	
				}
			
			/* Page Security End*/
				
			forward = mapping.findForward( "success" );
			return ( forward );
	}

}
