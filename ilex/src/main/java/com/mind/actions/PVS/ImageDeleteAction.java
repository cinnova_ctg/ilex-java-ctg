package com.mind.actions.PVS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PVS.Pvsdao;

public class ImageDeleteAction extends com.mind.common.IlexAction {
	// private static final Logger logger = Logger
	// .getLogger(ImageDeleteAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String partnerId = request.getParameter("partnerId");

		String message = "";
		int result = Pvsdao.updatePartnerBAdgeStatus(partnerId);
		if (result == 1)
			message = "Badge has been deleted successfully.";
		else
			message = "Badge not deleted, please try again.";

		request.setAttribute("message", message);

		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(message);

		return null;

	}

}
