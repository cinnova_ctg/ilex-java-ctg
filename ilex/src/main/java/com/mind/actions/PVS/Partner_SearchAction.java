/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is an action class used for performing search operation in PVS Manager and
 * search, assign operations in PRM Manager.
 *
 */
package com.mind.actions.PVS;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.DynamicComboPVS;
import com.mind.bean.msp.SiteInfo;
import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.business.PVS.PartnerSearchLogic;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.DynamicComboDaoPVS;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.Partner_SearchForm;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;

public class Partner_SearchAction extends com.mind.common.IlexDispatchAction {

	/**
	 * This method performs search on partners in PVS Manager and PRM Manager.
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward search(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		Partner_SearchForm partner_SearchForm = (Partner_SearchForm) form;
		if (partner_SearchForm.getReAssign() == null) {
			partner_SearchForm
					.setReAssign((request.getParameter("isReassign") != null ? request
							.getParameter("isReassign") : ""));
		}

		DynamicComboDaoPVS dcomboPVS = new DynamicComboDaoPVS();
		DynamicComboPVS dcpvs = new DynamicComboPVS();
		ArrayList initialStateCategory = new ArrayList();
		String tempCat = "";
		// Add for to check site is allocated or not for Gmap vie w purpose
		String jobId = "";

		String sortqueryclause = ",qc_ps_average_rating desc,distance asc";

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired
		String userid = (String) session.getAttribute("userid");

		if (partner_SearchForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage PVS",
					"Search Partner", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		if (request.getParameter("sort") != null) {
			sortqueryclause = getSortQueryClause(sortqueryclause, session);

			partner_SearchForm.setName(request.getParameter("startname"));
			partner_SearchForm.setSearchTerm(request.getParameter("keyword"));
			partner_SearchForm.setPartnerTypeName(request
					.getParameter("partnertype"));
			partner_SearchForm.setMcsaVersion(request.getParameter("mcsavers"));
			String zipCode = request.getParameter("zipcode");
			partner_SearchForm.setZipcode(zipCode);
			partner_SearchForm.setRadius(request.getParameter("radius"));
			partner_SearchForm
					.setMainOffice(request.getParameter("mainoffice"));
			partner_SearchForm.setFieldOffice(request
					.getParameter("fieldoffice"));
			partner_SearchForm
					.setTechniciansZip(request.getParameter("techhi"));
			// partner_SearchForm.setTechnicians(request.getParameter("technician"));
			partner_SearchForm.setCertifications(request
					.getParameter("certification"));
			// partner_SearchForm.setCoreCompetency(request.getParameter("corecomp"));
			partner_SearchForm.setCbox1(request.getParameter("checkbox1"));
			partner_SearchForm.setCbox2(request.getParameter("checkbox2"));
			partner_SearchForm.setPartnerRating(request.getParameter("rating"));
			partner_SearchForm.setSearch(request.getParameter("ref"));
			partner_SearchForm.setCbox3(request.getParameter("checkbox3"));
			if (partner_SearchForm.getSelectedPartnersList() != null) {
				partner_SearchForm.setCheckedPartnerName(Util.getSplitArray(
						partner_SearchForm.getSelectedPartnersList(), ","));
			}
			if (partner_SearchForm.getCoreCompetencyList() != null) {
				partner_SearchForm.setCoreCompetency(Util.getSplitArray(
						partner_SearchForm.getCoreCompetencyList(), ","));
			}

			if (partner_SearchForm.getRecommendedList() != null) {
				partner_SearchForm.setRecommendedArr(Util.getSplitArray(
						partner_SearchForm.getRecommendedList(), ","));
			}

			if (partner_SearchForm.getRestrictedList() != null) {
				partner_SearchForm.setRestrictedArr(Util.getSplitArray(
						partner_SearchForm.getRestrictedList(), ","));
			}
			if (partner_SearchForm.getTechniciansList() != null) {
				partner_SearchForm.setTechnicians(Util.getSplitArray(
						partner_SearchForm.getTechniciansList(), ","));
			}

			if (partner_SearchForm.getToolkitlist() != null) {

				partner_SearchForm.setToolKitArr(Util.getSplitArray(
						partner_SearchForm.getToolkitlist(), ","));
			}

		}

		if (request.getParameter("viewjobtype") != null) {
			partner_SearchForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			partner_SearchForm.setViewjobtype("A");
		}
		if (request.getParameter("typeid") != null
				&& !(request.getParameter("typeid").equals(""))) {
			jobId = request.getParameter("typeid");
		}

		if (request.getParameter("ownerId") != null) {
			partner_SearchForm.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			partner_SearchForm.setOwnerId("%");
		}
		if (request.getParameter("powoid") != null) {
			partner_SearchForm.setPowoId("" + request.getParameter("powoid"));
		}

		if (request.getParameter("appendix_Id") != null) {
			partner_SearchForm.setAppendixId(""
					+ request.getParameter("appendix_Id"));
		}

		if (request.getParameter("tabId") != null) {
			partner_SearchForm.setTabId("" + request.getParameter("tabId"));
		}

		if (partner_SearchForm.getPowoId() == null) {
			partner_SearchForm.setPowoId("0");
		}

		if (request.getParameter("action") != null) {
			partner_SearchForm.setAssign(request.getParameter("action"));
		}

		int flag = 0;
		int type_id = 0;

		if (request.getParameter("formtype") != null) {
			partner_SearchForm.setFormtype(request.getParameter("formtype"));

		} else {
			partner_SearchForm.setFormtype(partner_SearchForm.getFormtype());
		}

		DynamicComboCM dcLatDirec = new DynamicComboCM();
		dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
		request.setAttribute("dcLatDirec", dcLatDirec);

		DynamicComboCM dcPartnerstatus = new DynamicComboCM();
		dcPartnerstatus.setPartnerstatus(Pvsdao
				.getPartnerstatuslist(getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcPartnerstatus", dcPartnerstatus);

		DynamicComboCM dcLonDirec = new DynamicComboCM();
		dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
		request.setAttribute("dcLonDirec", dcLonDirec);

		if (partner_SearchForm.getTypeid() != null
				&& !partner_SearchForm.getTypeid().equals("")) {
			if (request.getParameter("partnerid") != null) // for PO/WO
			// dashboard
			{
				partner_SearchForm.setSelectedpartner(request
						.getParameter("partnerid"));
			}

			flag = Pvsdao.checkassign(partner_SearchForm.getTypeid(),
					partner_SearchForm.getType(),
					getDataSource(request, "ilexnewDB"));
			partner_SearchForm.setAssigncheck("" + flag);
		}

		// ParterTypeCombo
		DynamicComboCM dcPartnerType = new DynamicComboCM();
		dcPartnerType.setFirstlevelcatglist(DynamicComboDao
				.getPartnerTypeCombo());
		request.setAttribute("dcPartnerType", dcPartnerType);

		// Radius Combo
		DynamicComboCM dcRadiusRange = new DynamicComboCM();
		dcRadiusRange.setFirstlevelcatglist(DynamicComboDao.getRadius());
		request.setAttribute("dcRadiusRange", dcRadiusRange);

		// MCSA Version
		DynamicComboCM mcsaVersionCombo = new DynamicComboCM();
		mcsaVersionCombo.setMcsaVersion(Pvsdao.getMsaVersionlist(
				"partnerSearch", getDataSource(request, "ilexnewDB")));
		request.setAttribute("mcsaVersionCombo", mcsaVersionCombo);

		// Core Competency
		DynamicComboCM dcCoreCompetancy = new DynamicComboCM();
		dcCoreCompetancy.setFirstlevelcatglist(Pvsdao
				.getCoreCompetancy(getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcCoreCompetancy", dcCoreCompetancy);

		// Tool Kit
		DynamicComboCM dcToolKit = new DynamicComboCM();

		ArrayList<LabelValue> lst = new ArrayList<LabelValue>();
		lst = getToolSet(getDataSource(request, "ilexnewDB"), 0, lst, "");

		dcToolKit.setToolKitlist(lst);
		request.setAttribute("dcToolKit", dcToolKit);

		DynamicComboCM recommendedComboCM = new DynamicComboCM();
		recommendedComboCM.setRecommendedlist(Pvsdao
				.getRecommendedRestrictedList(1,
						getDataSource(request, "ilexnewDB")));
		request.setAttribute("recommendedComboCM", recommendedComboCM);

		DynamicComboCM restrictedComboCM = new DynamicComboCM();
		restrictedComboCM.setRestrictedlist(Pvsdao
				.getRecommendedRestrictedList(0,
						getDataSource(request, "ilexnewDB")));
		request.setAttribute("restrictedComboCM", restrictedComboCM);

		// Tech Certifications
		DynamicComboCM dcTechCertifications = new DynamicComboCM();
		dcTechCertifications.setFirstlevelcatglist(Pvsdao
				.getCertificationlist(getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcTechCertifications", dcTechCertifications);

		// Certifications By Category Wise
		initialStateCategory = DynamicComboDao
				.getCertificationTypes(getDataSource(request, "ilexnewDB"));
		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempCat = ((Partner_SearchBean) initialStateCategory.get(i))
						.getCertificationsId();
				((Partner_SearchBean) initialStateCategory.get(i))
						.setTempList(DynamicComboDao
								.getCategoryWiseCertifications(tempCat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}
		request.setAttribute("initialStateCategory", initialStateCategory);
		// Changes Start by Yogendra For Adding International Section On partner
		// Search Screen

		partner_SearchForm.setCountryNameValueList(Pvsdao
				.getCountryList(getDataSource(request, "ilexnewDB")));
		if (null != partner_SearchForm.getSelectedCountryName()) {
			partner_SearchForm.setCityNameValueList(Pvsdao.getCityList(
					partner_SearchForm.getSelectedCountryName(),
					getDataSource(request, "ilexnewDB")));
		} else {
			partner_SearchForm.setCityNameValueList(Pvsdao.getCityList("0",
					getDataSource(request, "ilexnewDB")));
		}

		// Changes End by Yogendra For Adding International Section On partner
		// Search Screen
		float latitude = 0;
		float longitude = 0;

		if (partner_SearchForm.getLat_degree() != null
				&& !partner_SearchForm.getLat_degree().equals("")
				&& partner_SearchForm.getLon_degree() != null
				&& !partner_SearchForm.getLon_degree().equals("")) {
			latitude = (Float.parseFloat(partner_SearchForm.getLat_degree()) + (Float
					.parseFloat(partner_SearchForm.getLat_min())) / 60);
			longitude = (Float.parseFloat(partner_SearchForm.getLon_degree()) + (Float
					.parseFloat(partner_SearchForm.getLon_min())) / 60);
		}

		/**/
		if (!partner_SearchForm.getPowoId().equals("0")
				&& !partner_SearchForm.getReAssign().equals("")) {
			String partnerId = Pvsdao.getPartnerForPO(
					partner_SearchForm.getPowoId(),
					getDataSource(request, "ilexnewDB"));
			if (partner_SearchForm.getSelectedpartner() == null) {
				partner_SearchForm.setSelectedpartner(partnerId);
			}
		}

		/**/
		// TODO here for while perform search from PO -Assig/Re-Assign
		if (partner_SearchForm.getSearch() != null) {
			ArrayList list = new ArrayList();
			ArrayList listSite = new ArrayList();
			ArrayList singleSiteList = new ArrayList();
			String zipCode = partner_SearchForm.getZipcode();
			if (zipCode != null) {

				if (zipCode.length() > 5
						&& Character.isDigit(zipCode.charAt(0))) {
					zipCode = zipCode.substring(0, 5);
				}
			}

			request.setAttribute("dcpvs", dcpvs);
			if (partner_SearchForm.getTypeid() != null
					&& !partner_SearchForm.getTypeid().equals("")) {
				type_id = Integer.parseInt(partner_SearchForm.getTypeid());
			}
			if (!jobId.equals("")) {
				listSite = Pvsdao.getSiteDeatils(jobId, "0",
						"multiplejobtojob", partner_SearchForm.getRadius(),
						getDataSource(request, "ilexnewDB"));
				singleSiteList = Pvsdao.getSingleSiteDeatils(jobId,
						getDataSource(request, "ilexnewDB"));
				session.removeAttribute("siteXML");
				session.removeAttribute("singleSiteXML");
				String multipleSiteXML = Pvsdao.convertSiteRatingToXML("JOB",
						partner_SearchForm.getZipcode(),
						partner_SearchForm.getRadius(), listSite);
				String singleSiteXML = Pvsdao.convertSiteRatingToXML("JOB",
						partner_SearchForm.getZipcode(),
						partner_SearchForm.getRadius(), singleSiteList);
				session.setAttribute("siteXML", multipleSiteXML);
				session.setAttribute("singleSiteXML", singleSiteXML);
				request.setAttribute("siteList", listSite);
			}

			String badgeStatus = "";
			if (partner_SearchForm.getCpdBadgeId() != null
					&& partner_SearchForm.getCpdBadgeId().equals("P")) {
				badgeStatus = "LIKE 'Printed'";
			} else if (partner_SearchForm.getCpdBadgeId() != null
					&& partner_SearchForm.getCpdBadgeId().equals("D")) {
				badgeStatus = "LIKE 'Declined'";
			} else if (partner_SearchForm.getCpdBadgeId() != null
					&& partner_SearchForm.getCpdBadgeId().equals("N")) {
				badgeStatus = "LIKE '%'";
			}
			String partnerStatus = request.getParameter("cmboxStatus");

			if (partner_SearchForm.getCbox3() == null) {

				partner_SearchForm.setCbox3("N");

			}
			if (partner_SearchForm.getCboxDrug() == null) {
				partner_SearchForm.setCboxDrug("N");
			}

			list = Pvsdao.getSearchpartnerdetailsWithStatus(
					partner_SearchForm.getName(),
					partner_SearchForm.getSearchTerm(),
					partner_SearchForm.getCheckedPartnerName(),
					partner_SearchForm.getMcsaVersion(), zipCode,
					partner_SearchForm.getRadius(),
					partner_SearchForm.getMainOffice(),
					partner_SearchForm.getFieldOffice(),
					partner_SearchForm.getTechniciansZip(),
					partner_SearchForm.getTechnicians(),
					partner_SearchForm.getCertifications(),
					partner_SearchForm.getCoreCompetency(),
					partner_SearchForm.getCbox1(),
					partner_SearchForm.getCbox2(),
					partner_SearchForm.getPartnerRating(),
					partner_SearchForm.getSecurityCertiftn(), sortqueryclause,
					partner_SearchForm.getAssign(),
					getDataSource(request, "ilexnewDB"),
					partner_SearchForm.getSelectedCountryName(),
					partner_SearchForm.getSelectedCityName(),
					partner_SearchForm.getPowoId(),
					partner_SearchForm.getRecommendedArr(),
					partner_SearchForm.getRestrictedArr(),
					partner_SearchForm.getToolKitArr(), badgeStatus,
					partnerStatus, partner_SearchForm.getCbox3(),
					partner_SearchForm.getCboxDrug());
			String checkXML = Pvsdao.convertCheckRatingToXML(zipCode,
					partner_SearchForm.getRadius(), jobId,
					getDataSource(request, "ilexnewDB"));
			if (!(list.isEmpty())) {

				session.removeAttribute("partnerXML");
				session.removeAttribute("checkXML");
				if (jobId.equals("")) {
					session.removeAttribute("siteXML");
					String siteXMLblank = Pvsdao.convertSiteRatingToXML("",
							zipCode, partner_SearchForm.getRadius(), listSite);
					session.setAttribute("siteXML", siteXMLblank);
				}
				/*
				 * Generate the XML of the first 25 partners to be displayed in
				 * GMAP
				 */
				String xml = Pvsdao.convertPartnerRatingToXML(list);
				session.setAttribute("partnerXML", xml);
				session.setAttribute("checkXML", checkXML);
				request.setAttribute("partnersearchlist", list);
			} else {
				partner_SearchForm.setNorecordmessage("9999");
			}
			partner_SearchForm.setType(partner_SearchForm.getType());

			request.setAttribute("zipCodeValue", zipCode);
		} else {

			if (request.getParameter("typeid") != null) {
				String zipCode = Jobdao.getJobsitezipcode(
						request.getParameter("typeid"),
						getDataSource(request, "ilexnewDB"));

				partner_SearchForm.setZipcode(zipCode);
				SiteInfo siteInfo = JobDAO
						.getSiteInfo(
								Long.valueOf(request.getParameter("typeid"))
										.toString(),
								getDataSource(request, "ilexnewDB"));
				partner_SearchForm.setSiteAddress(siteInfo.getSite_address());
				partner_SearchForm.setSiteCity(siteInfo.getSite_city());
				partner_SearchForm.setSiteState(siteInfo.getSite_state());
				partner_SearchForm.setSiteCountry(siteInfo.getSite_country());
				partner_SearchForm.setSiteLatitude(siteInfo.getSite_latitude());
				partner_SearchForm.setSiteLongitude(siteInfo
						.getSite_longitude());
			}

			request.setAttribute("dcpvs", dcpvs);

			String tempTech[] = { "" };
			String tempCore[] = { "0" };
			partner_SearchForm.setPartnerTypeName("B");
			partner_SearchForm.setMainOffice("Y");
			partner_SearchForm.setFieldOffice("Y");
			partner_SearchForm.setTechniciansZip("Y");
			partner_SearchForm.setCbox1("N");
			partner_SearchForm.setCbox2("Y");
			partner_SearchForm.setCbox3("N");
			partner_SearchForm.setCertifications("0");
			partner_SearchForm.setTechnicians(tempTech);
			partner_SearchForm.setCoreCompetency(tempCore);
			partner_SearchForm.setRecommendedArr(tempCore);
			partner_SearchForm.setRestrictedArr(tempCore);
			partner_SearchForm.setPartnerRating("N");
			partner_SearchForm.setCpdBadgeId("A");
			partner_SearchForm.setRadius("25");
		}

		// List of PartnerType
		// LinkedHashMap used to get the Values in the Insertion order
		LinkedHashMap<String, String> partnerTypeList = new LinkedHashMap<String, String>();
		partnerTypeList.put("M", "Minuteman");
		partnerTypeList.put("SS", "SpeedPay");
		partnerTypeList.put("S", "Certified");
		request.setAttribute("partnerTypeList", partnerTypeList);

		partner_SearchForm.setPartnerTypeList(partnerTypeList);

		request.setAttribute("fromtype", partner_SearchForm.getFormtype());
		// Start:Added By Amit ,To show job name in Assign Partner Screen

		if (request.getParameter("typeid") != null) {
			partner_SearchForm.setJobName(Jobdao.getJobname(
					request.getParameter("typeid"),
					getDataSource(request, "ilexnewDB")));
			// End :By Amit , To show job Name in Assign Partner screen
		}
		// added to get selected value after sorting
		String selectedPartners = Util
				.changeStringToCommaSeperation(partner_SearchForm
						.getCheckedPartnerName());
		partner_SearchForm.setSelectedPartnersList((selectedPartners));
		if (partner_SearchForm.getCoreCompetency() != null) {
			String coreCompetencyList = Util
					.changeStringToCommaSeperation(partner_SearchForm
							.getCoreCompetency());
			partner_SearchForm.setCoreCompetencyList(coreCompetencyList);
		}
		// Recommended and restricted list
		if (partner_SearchForm.getRecommendedArr() != null) {
			String recommendedList = Util
					.changeStringToCommaSeperation(partner_SearchForm
							.getRecommendedArr());
			partner_SearchForm.setRecommendedList(recommendedList);
		}

		if (partner_SearchForm.getRestrictedArr() != null) {
			String restrictedList = Util
					.changeStringToCommaSeperation(partner_SearchForm
							.getRestrictedArr());
			partner_SearchForm.setRestrictedList(restrictedList);
		}

		if (partner_SearchForm.getToolKitArr() != null) {

			String toolkitlist = Util
					.changeStringToCommaSeperation(partner_SearchForm
							.getToolKitArr());
			partner_SearchForm.setToolkitlist(toolkitlist);
		}

		// Recommended and restricted list
		if (partner_SearchForm.getTechnicians() != null) {
			String techniciansList = Util
					.changeStringToCommaSeperation(partner_SearchForm
							.getTechnicians());
			partner_SearchForm.setTechniciansList(techniciansList);
		}
		// CSV Export button viewer status
		String viewerStatus = Pvsdao.getCSVReportViewerStatus(userid,
				getDataSource(request, "ilexnewDB"));
		partner_SearchForm.setCsvExportStatus(viewerStatus);
		forward = mapping.findForward("success");
		return (forward);
	}

	public ActionForward exportToExcel(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Partner_SearchForm partner_SearchForm = (Partner_SearchForm) form;
		String sortqueryclause = ",qc_ps_average_rating desc,distance asc";

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired
		String userid = (String) session.getAttribute("userid");

		if (partner_SearchForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage PVS",
					"Search Partner", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		HSSFWorkbook excelFile = null;
		if (partner_SearchForm.getSearch() != null) {
			ArrayList list = new ArrayList();

			String zipCode = partner_SearchForm.getZipcode();
			if (zipCode != null) {

				if (zipCode.length() > 5
						&& Character.isDigit(zipCode.charAt(0))) {
					zipCode = zipCode.substring(0, 5);
				}
			}
			String badgeStatus = "";
			if (partner_SearchForm.getCpdBadgeId() != null
					&& partner_SearchForm.getCpdBadgeId().equals("P")) {
				badgeStatus = "LIKE 'Printed'";
			} else if (partner_SearchForm.getCpdBadgeId() != null
					&& partner_SearchForm.getCpdBadgeId().equals("D")) {
				badgeStatus = "LIKE 'Declined'";
			} else if (partner_SearchForm.getCpdBadgeId() != null
					&& partner_SearchForm.getCpdBadgeId().equals("N")) {
				badgeStatus = "LIKE '%'";
			}

			String partnerStatus = request.getParameter("cmboxStatus");

			if (partner_SearchForm.getCbox3() == null) {

				partner_SearchForm.setCbox3("N");

			}

			list = Pvsdao.getSearchpartnerdetailsWithStatus(
					partner_SearchForm.getName(),
					partner_SearchForm.getSearchTerm(),
					partner_SearchForm.getCheckedPartnerName(),
					partner_SearchForm.getMcsaVersion(), zipCode,
					partner_SearchForm.getRadius(),
					partner_SearchForm.getMainOffice(),
					partner_SearchForm.getFieldOffice(),
					partner_SearchForm.getTechniciansZip(),
					partner_SearchForm.getTechnicians(),
					partner_SearchForm.getCertifications(),
					partner_SearchForm.getCoreCompetency(),
					partner_SearchForm.getCbox1(),
					partner_SearchForm.getCbox2(),
					partner_SearchForm.getPartnerRating(),
					partner_SearchForm.getSecurityCertiftn(), sortqueryclause,
					partner_SearchForm.getAssign(),
					getDataSource(request, "ilexnewDB"),
					partner_SearchForm.getSelectedCountryName(),
					partner_SearchForm.getSelectedCityName(),
					partner_SearchForm.getPowoId(),
					partner_SearchForm.getRecommendedArr(),
					partner_SearchForm.getRestrictedArr(),
					partner_SearchForm.getToolKitArr(), badgeStatus,
					partnerStatus, partner_SearchForm.getCbox3(),
					partner_SearchForm.getCboxDrug());

			if (!(list.isEmpty())) {
				PartnerSearchLogic partnerSearchLogic = new PartnerSearchLogic();
				Map<Integer, String> coreCompeteancies = Pvsdao
						.getCoreCompetancyWithShortNames(getDataSource(request,
								"ilexnewDB"));

				Map<String, String> coreCompetanciesWithfullName = Pvsdao
						.getCoreCompetancies(getDataSource(request, "ilexnewDB"));
				excelFile = partnerSearchLogic
						.exportPartnerSearchResultToExcel(list,
								coreCompeteancies, coreCompetanciesWithfullName);

			}

		}
		ServletOutputStream outputStream = response.getOutputStream();
		response.setHeader("Cache-Control",
				"must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Content-disposition",
				"attachment;filename=partnerSearchResult.xls");
		excelFile.write(outputStream);
		outputStream.flush();
		outputStream.close();
		return null;

	}

	/**
	 * This method performs assignment of job/activity/resource to partners in
	 * PRM Manager.
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward assign(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String isReassign = (String) request.getSession().getAttribute(
				"isReassign");
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		Partner_SearchForm partner_SearchForm = (Partner_SearchForm) form;
		DynamicComboDaoPVS dcomboPVS = new DynamicComboDaoPVS();
		DynamicComboPVS dcpvs = new DynamicComboPVS();
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
		} // expired
		String userid = (String) session.getAttribute("userid");
		if (partner_SearchForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid, "Manage Project",
					"Assign Partner", getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		if (request.getParameter("viewjobtype") != null) {
			partner_SearchForm.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			partner_SearchForm.setViewjobtype("A");
		}
		request.setAttribute("viewjobtype", partner_SearchForm.getViewjobtype());

		if (partner_SearchForm.getFormtype() != null) {
			partner_SearchForm.setFormtype(partner_SearchForm.getFormtype());
		}

		DynamicComboCM dcLatDirec = new DynamicComboCM();
		dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
		request.setAttribute("dcLatDirec", dcLatDirec);

		DynamicComboCM dcLonDirec = new DynamicComboCM();
		dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
		request.setAttribute("dcLonDirec", dcLonDirec);

		int flag = 0;
		int statusvalue = 0;

		if (partner_SearchForm.getTypeid() != null) {

			flag = Pvsdao.checkassign(partner_SearchForm.getTypeid(),
					partner_SearchForm.getType(),
					getDataSource(request, "ilexnewDB"));
			partner_SearchForm.setAssigncheck("" + flag);

		}
		// System.out.println("------AAAAAAAAAA "+partner_SearchForm.getHiddensave());
		if (partner_SearchForm.getSave() != null
				|| partner_SearchForm.getHiddensave() != null) {
			ArrayList list = new ArrayList();
			ArrayList corecompetencylist = new ArrayList();
			ArrayList certificationlist = new ArrayList();
			ArrayList partnertypelist = new ArrayList();

			corecompetencylist = dcomboPVS.getCompetencylist(getDataSource(
					request, "ilexnewDB"));
			dcpvs.setCompetencylist(corecompetencylist);

			certificationlist = dcomboPVS
					.getPartnerCertificationlist(getDataSource(request,
							"ilexnewDB"));
			dcpvs.setCertificationlist(certificationlist);

			partnertypelist = dcomboPVS.getPartnertypelist(getDataSource(
					request, "ilexnewDB"));
			dcpvs.setPartnertypelist(partnertypelist);
			request.setAttribute("dcpvs", dcpvs);

			String type = partner_SearchForm.getType();
			String pid = partner_SearchForm.getCurrentPartner();
			String typeid = partner_SearchForm.getTypeid();

			request.setAttribute("partnerLatitude",
					request.getParameter("partnerLatitude"));
			request.setAttribute("partnerLongitude",
					request.getParameter("partnerLongitude"));
			request.setAttribute("siteLatitude",
					partner_SearchForm.getSiteLatitude());
			request.setAttribute("siteLongitude",
					partner_SearchForm.getSiteLongitude());
			request.setAttribute("purchaseOrderId",
					partner_SearchForm.getPowoId());

			if (!Util.isNullOrBlank(partner_SearchForm.getReAssign())) {
				int newPoWoId = 0;
				try {
					newPoWoId = PurchaseOrder.reAssign(
							partner_SearchForm.getPowoId(), userid, pid,
							getDataSource(request, "ilexnewDB"));
				} catch (Exception e) {
					System.out
							.println("Error occurred while re-assigning same partner to PO:"
									+ e.getMessage());
				}

				if (newPoWoId > 0) {
					request.setAttribute("purchaseOrderId", newPoWoId);
					String url = "/POAction.do?hmode=unspecified&appendixType=&tabId=0"
							+ "&appendix_Id="
							+ partner_SearchForm.getAppendixId()
							+ "&jobId="
							+ partner_SearchForm.getTypeid()
							+ "&poId="
							+ newPoWoId;
					ActionForward fwd = new ActionForward();
					fwd.setPath(url);
					return fwd;
				} else {
					request.setAttribute("reAssignFail", "reAssignFail");
				}
				// partner_SearchForm.getPowoId(),
				// getDataSource(request,"ilexnewDB"));
			} else {
				statusvalue = Integer.parseInt(PurchaseOrderDAO
						.upsertPurchaseOrder(typeid, userid, "Update", "",
								partner_SearchForm.getPowoId(), pid,
								partner_SearchForm.getTravelDistance(),
								partner_SearchForm.getTravelDuration(),
								getDataSource(request, "ilexnewDB")));
			}
			if (partner_SearchForm.getCurrentPartner() != null) {
				partner_SearchForm.setAssignmessage("" + statusvalue);
			} else {
				partner_SearchForm.setAssignmessage("-9100");
			}

			partner_SearchForm.setType(partner_SearchForm.getType());

		}

		/*
		 * String assignedpartnerid = null; assignedpartnerid =
		 * Pvsdao.getAssignedpartnerid
		 * (partner_SearchForm.getTypeid(),partner_SearchForm
		 * .getType(),getDataSource(request,"ilexnewDB"));
		 * 
		 * if(assignedpartnerid!=null) {
		 * partner_SearchForm.setSelectedpartner(assignedpartnerid); }
		 */
		request.setAttribute("fromtype", partner_SearchForm.getFormtype());

		if (partner_SearchForm.getFormtype().equals("powo")) {
			request.setAttribute("type", "POWODashBoard");
			request.setAttribute("jobid", partner_SearchForm.getTypeid());
			request.setAttribute("tabId", partner_SearchForm.getTabId());
			forward = mapping.findForward("powodashboard");
		} else {
			forward = mapping.findForward("success");
		}
		return (forward);

	}

	public String getSortQueryClause(String queryclause, HttpSession session) {
		if (session.getAttribute("partnerQuery") != null) {
			queryclause = (String) session.getAttribute("partnerQuery");
		}
		return queryclause;
	}

	/*
	 * This method is used to assign the Partner through Ajax at the time of
	 * calculating Distance between PArtner and Site.
	 */
	public ActionForward updatePOByAjax(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Partner_SearchForm partner_SearchForm = (Partner_SearchForm) form;

		HttpSession session = request.getSession(true);
			// return( mapping.findForward("SessionExpire")); //Check for
			// session expired

		String userid = (String) session.getAttribute("userid");

		String poId = request.getParameter("powoId").toString();
		String distance = request.getParameter("distance").toString();
		String duration = request.getParameter("duration").toString();

		PurchaseOrderDAO.updatePOInfo(distance, duration, poId,
				getDataSource(request, "ilexnewDB"));
		return null;
	}

	public ActionForward findCity(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String InternationCountry = (String) request.getParameter("IntCountry");

		ArrayList<LabelValue> cityList = Pvsdao.getCityList(InternationCountry,
				getDataSource(request, "ilexnewDB"));

		String jsonResult = new flexjson.JSONSerializer().serialize(cityList);
		response.setContentType("text/javascript");
		response.getWriter().write(jsonResult);
		return null;
	}

	public ActionForward isPartnerRestricted(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String partnerId = (String) request.getParameter("partnerId");
		String jobId = (String) request.getParameter("jobId");

		boolean val = true;
		try {
			val = Pvsdao.isPartnerRestricted(partnerId, jobId,
					getDataSource(request, "ilexnewDB"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		String jsonResult = new flexjson.JSONSerializer().serialize(val);
		response.setContentType("text/javascript");
		response.getWriter().write(jsonResult);
		return null;
	}

	private ArrayList<LabelValue> getToolSet(DataSource ds, int parentId,
			ArrayList<LabelValue> lst1, String dashes) {

		ArrayList<String[]> lst = Pvsdao.getToolKit(ds, parentId);

		for (int i = 0; i < lst.size(); i++) {
			String[] st = lst.get(i);
			lst1.add(new LabelValue(dashes + st[1], st[0]));
			if (Pvsdao.getToolKit(ds, Integer.parseInt(st[0])).size() > 0) {
				getToolSet(ds, Integer.parseInt(st[0]), lst1, dashes + "-");
			}
		}
		return lst1;
	}
}
