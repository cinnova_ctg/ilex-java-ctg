package com.mind.actions.PVS;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.mind.bean.pvs.McsaDetailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PVS.MCSA_ViewForm;


/**
 * @version 	1.0
 * @author
 */
/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 **/

/**
 * 
 *  Class Explanation: 
 *  MCSA_ViewAction is the action class for MCSA_View.jsp.
 *  It is used for uploading and updating of the MCSA from the client side and database respectively.   
 *     
 */

public class MCSA_ViewAction extends com.mind.common.IlexAction {
	
/** This method is the default method which is executed when the MCSA_View.do action is called.
 *  It is used for uploading and updating of the MCSA from the client side and database respectively.
 * @param  mapping         ActionMapping instance. 
 * @param  form            form bean of the action. 
 * @param  request         reqest Object. 
 * @param  response  	   response Object. 
 * @return ActionForward   Actionforward object for forwarding the request.
 */
	
	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		MCSA_ViewForm mCSA_ViewForm = (MCSA_ViewForm) form;
		DataSource ds= getDataSource( request,"ilexnewDB" );
		ArrayList mcsa_details= null;		
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if( mCSA_ViewForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage PVS" , "Manage MCSA" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		String strDate="";
		Calendar c = Calendar.getInstance();
		strDate = (c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.DATE)+"/"+c.get(Calendar.YEAR);
		
		//if the action is called from the hyperlink
		if(request.getParameter("mcsa_id")!=null){
			mcsa_details= Pvsdao.getMcsa(ds);
			int id=Integer.parseInt((String)request.getParameter("mcsa_id"));
			for (int i=0;i<mcsa_details.size();i++){
				McsaDetailBean mcsaBean= (McsaDetailBean)mcsa_details.get(i);
				if(Integer.parseInt(mcsaBean.getMcsa_id())!=id)
					continue;
				else{
					mCSA_ViewForm.setMcsa(new String(mcsaBean.getMcsa_data()));
					mCSA_ViewForm.setDate(new String(mcsaBean.getMcsa_date()));
					mCSA_ViewForm.setVersion(new String(mcsaBean.getMcsa_version()));
					request.setAttribute("mcsa_details",mcsa_details); 
					forward=mapping.findForward("success");
					return (forward);					
				}// End of if else				
			}//End of For		
		}//End of if
		
		//if the mcsa is uploaded by the Client
		if(mCSA_ViewForm.getBrowse()!=null){
			mCSA_ViewForm.setDate(strDate);
			FormFile ff = mCSA_ViewForm.getBrowse();
			mCSA_ViewForm.setMcsa(new String(ff.getFileData()));
			forward=mapping.findForward("success");
			return (forward);
		}// End of if
		
		
		//if the mcsa is saved then insert the particulars into the database
		if(mCSA_ViewForm.getSave()!=null){
			int spResult=Pvsdao.insertMcsa(null,mCSA_ViewForm.getMcsa(),mCSA_ViewForm.getDate(),
				(String)session.getAttribute("userid"),"a",ds);
			request.setAttribute("savedsuccessfully","");
			}//End of if
		
		
		mcsa_details= Pvsdao.getMcsa(ds); // get all the mcsa details into the arraylist
		if (mcsa_details==null){          //checks whether first time uploaded
			//request.setAttribute("mcsa_upload","true");
			
		}
		else{                             // sets the latest Mcsa as Current Mcsa
			McsaDetailBean mcsaBean= (McsaDetailBean)mcsa_details.get(0);
			mCSA_ViewForm.setMcsa(new String(mcsaBean.getMcsa_data()));
			mCSA_ViewForm.setDate(new String(mcsaBean.getMcsa_date()));
			mCSA_ViewForm.setVersion(new String(mcsaBean.getMcsa_version()));
		}
		
		
		
		
		request.setAttribute("mcsa_details",mcsa_details);	    // for details of all the mcsa into reqeust scope 
		forward=mapping.findForward("success");
		return (forward);
	}
}
