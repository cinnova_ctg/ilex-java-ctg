/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is an action class used for managing privilege for a role in User Manager.      
*
*/


package com.mind.actions.UM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboUM;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.UM.RolePrivilegedao;
import com.mind.formbean.UM.RolePrivilegeForm;


/*
 * Methods : execute
 */

public class RolePrivilegeAction extends com.mind.common.IlexAction {

	
/** This method managing privilege for a role in User Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/	

	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired 
		
		String userid = (String)session.getAttribute("userid");
		if(!Authenticationdao.getPageSecurity(userid, "User Manager", "Manage Security", getDataSource(request,"ilexnewDB"))) {
			return( mapping.findForward("UnAuthenticate"));	
		}
		/* Page Security End*/
		
		
		RolePrivilegeForm formdetail = ( RolePrivilegeForm ) form;
	
		if(request.getParameter("org_type") != null) {
			formdetail.setOrg_type(request.getParameter("org_type"));
			String org_name = RolePrivilegedao.getOrganizationTypeName(formdetail.getOrg_type(),getDataSource( request,"ilexnewDB" ));	
			formdetail.setLo_organization_type_name(org_name);
		}
		
		if(request.getParameter("org_discipline_id") != null) {
			formdetail.setOrg_discipline_id(request.getParameter("org_discipline_id"));
			String org_discipline_name = RolePrivilegedao.getOrganizationDisciplineName(formdetail.getOrg_discipline_id(),getDataSource( request,"ilexnewDB" ));
			formdetail.setLo_od_name(org_discipline_name);
		}
		
			DynamicComboUM dynamiccomboUM = new DynamicComboUM();
			
			dynamiccomboUM.setFunctiongroup(RolePrivilegedao.getFunctionGroup(getDataSource( request, "ilexnewDB" )));       
		    request.setAttribute("dynamiccomboUM",dynamiccomboUM);
			
			dynamiccomboUM.setRolename(RolePrivilegedao.getRoleName(request.getParameter("org_discipline_id"),getDataSource( request, "ilexnewDB" )));       
		    request.setAttribute("dynamiccomboUM",dynamiccomboUM);
			
		
		 if(formdetail.getRefresh()) {
			if(formdetail.getCc_fu_group_name() != "0") {
			ArrayList UMList =  RolePrivilegedao.getFunctionType(formdetail.getCc_fu_group_name(),getDataSource( request,"ilexnewDB" ));  
			request.setAttribute("UMList",UMList);
			int numcols = RolePrivilegedao.getNumColumns(formdetail.getCc_fu_group_name(),getDataSource( request,"ilexnewDB" ));
			request.setAttribute("numcols",""+numcols);
		    }
			
			if(Integer.parseInt(formdetail.getLo_ro_role_desc()) != -1) {
			int roleprivilege[] = RolePrivilegedao.getRolePrivilege(formdetail.getCc_fu_group_name(),formdetail.getLo_ro_role_desc(),getDataSource( request,"ilexnewDB" ));     	
			formdetail.setCheck(roleprivilege);
			}
		 }

		 	if( formdetail.getSave()!= null )
				{
				String indexvalue = ""; 
									
				if(formdetail.getCheck()!= null)
				 {
					if(formdetail.getCheck().length > 0)
					{	
						int index[] = new int[formdetail.getCheck().length];
						index = formdetail.getCheck();
						
						for( int i = 0; i < formdetail.getCheck().length; i++ )
						{
						indexvalue = indexvalue+index[i]+",";
						}
					}
				}
				int retval = RolePrivilegedao.addFunctionType(formdetail.getCc_fu_group_name(), formdetail.getLo_ro_role_desc(), indexvalue, getDataSource( request,"ilexnewDB" ));
				request.setAttribute("retval",""+retval);
								
			    if(formdetail.getCc_fu_group_name() != "0") {
				ArrayList UMList =  RolePrivilegedao.getFunctionType(formdetail.getCc_fu_group_name(),getDataSource( request,"ilexnewDB" ));  
				request.setAttribute("UMList",UMList);
				int numcols = RolePrivilegedao.getNumColumns(formdetail.getCc_fu_group_name(),getDataSource( request,"ilexnewDB" ));
				request.setAttribute("numcols",""+numcols);
			    }
							
				if(Integer.parseInt(formdetail.getLo_ro_role_desc()) != -1) {
				int roleprivilege[] = RolePrivilegedao.getRolePrivilege(formdetail.getCc_fu_group_name(),formdetail.getLo_ro_role_desc(),getDataSource( request,"ilexnewDB" ));     	
				formdetail.setCheck(roleprivilege);
				}
			}
			
		return( mapping.findForward( "success" ) );	
	}
}
