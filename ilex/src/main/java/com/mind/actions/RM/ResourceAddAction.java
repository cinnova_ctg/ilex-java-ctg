/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is an action class used for perform multiple
* operations Add,Update,Delete on 4th level Category(Items) of Resources in Resource Manager.      
*
*/
package com.mind.actions.RM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboRM;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDaoRM;
import com.mind.common.dao.Menu;
import com.mind.dao.RM.RMdao;
import com.mind.formbean.RM.ResourceAddForm;



public class ResourceAddAction extends com.mind.common.IlexDispatchAction
{

	
/** This method adds 4th level Category(Items) for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
	public ActionForward add (
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
		DynamicComboRM dcrm = new DynamicComboRM();
		ResourceAddForm resourceAddForm = (ResourceAddForm) form;
		String[] fieldlist= new String[11];
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		if( resourceAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Resource" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		int statusvalue=0;
		ArrayList criticalitylist= new ArrayList();
		String criticalityname="";
		criticalitylist=RMdao.getCriticalitylist(getDataSource( request,"ilexnewDB" ));
		request.setAttribute("criticalitylist",criticalitylist);
		
		int Checkmanufacturer=0;
		
		if( resourceAddForm.getSave()!= null )
		{
			String restype="";
			String id2="";
			String laborname="";
			String laboridentifier="";
			String status="";
			String sellableqty="";
			String minimumqty="";
			String unit="";
			String mfgpartno="";
			String weight="";
			String basecost="";
			String firstcatg="";
			String secondcatg="";
			String thirdcatg="";
			String statusname="";
			String id3="";
			String criticality="";
			restype=request.getParameter("resourceflag");
			resourceAddForm.setResourceflag(restype);
			request.setAttribute("resourceflag",restype);
			id2=resourceAddForm.getId2();
			laborname=resourceAddForm.getLaborname();
			laboridentifier=resourceAddForm.getLaboridentifier();
			status=resourceAddForm.getStatus();
			sellableqty=resourceAddForm.getSellableqty();
			minimumqty=resourceAddForm.getMinimumqty();
			unit=resourceAddForm.getUnit();
			mfgpartno=resourceAddForm.getMfgpartno();
			weight=resourceAddForm.getWeight();
			basecost=resourceAddForm.getBasecost();
			firstcatg=resourceAddForm.getFirstcatg();
			secondcatg=resourceAddForm.getSecondcatg();
			thirdcatg=resourceAddForm.getThirdcatg();
			if(restype.equals("2"))
			{
				if(resourceAddForm.getCriticality()!=null)
				{
					criticalityname=RMdao.getCriticalityName(resourceAddForm.getCriticality(),getDataSource( request,"ilexnewDB" ));
					resourceAddForm.setCriticalityname(criticalityname);
					resourceAddForm.setCriticality(resourceAddForm.getCriticality());
				}
				statusvalue=RMdao.addItems(0, Integer.parseInt(resourceAddForm.getId2()),laborname,mfgpartno,sellableqty,minimumqty,unit,weight,laboridentifier,status,basecost,Integer.parseInt(userid),"a",resourceAddForm.getCriticality(),getDataSource( request,"ilexnewDB" ));
			}
			else
			{
				statusvalue=RMdao.addItems(0, Integer.parseInt(resourceAddForm.getId2()),laborname,mfgpartno,sellableqty,minimumqty,unit,weight,laboridentifier,status,basecost,Integer.parseInt(userid),"a","0",getDataSource( request,"ilexnewDB" ));
			}
			/*criticalityname=RMdao.getCriticalityName(resourceAddForm.getCriticality(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setCriticalityname(criticalityname);
			resourceAddForm.setCriticality(resourceAddForm.getCriticality());
			statusvalue=RMdao.addItems(0, Integer.parseInt(resourceAddForm.getId2()),laborname,mfgpartno,sellableqty,minimumqty,unit,weight,laboridentifier,status,basecost,Integer.parseInt(userid),"a",resourceAddForm.getCriticality(),getDataSource( request,"ilexnewDB" ));
			*/
			resourceAddForm.setAddmessage(""+statusvalue);
			request.setAttribute("refreshtree","true");
			statusname=RMdao.getStatusName(resourceAddForm.getStatus(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setStatus(statusname);
			id3=RMdao.getResourceid(id2,laborname,laboridentifier,status,sellableqty,minimumqty,unit,basecost,getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setId2(id2);
			resourceAddForm.setId3(id3);
			resourceAddForm.setCnspartno( RMdao.getCnspartno(id3,getDataSource( request,"ilexnewDB" )));
			resourceAddForm.setFlag("1");
			
			// Set created by and updated by
			fieldlist=RMdao.getFieldlist(resourceAddForm.getId3(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setCreatedby(fieldlist[11]);
			resourceAddForm.setCreatedate(fieldlist[12]);
			resourceAddForm.setChangedby(fieldlist[13]);
			resourceAddForm.setChangedate(fieldlist[14]);
			
			request.setAttribute( "resourceform" , resourceAddForm );
			
			//	for checking if manufacturername is like FIELD
			Checkmanufacturer=RMdao.checkManufacturer(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
			request.setAttribute( "Checkmanufacturer" , Checkmanufacturer+"" );
		}
		else
		{
			String restype="";
			String catgname="";
			String mfgname="";
			String subcatg="";
			ArrayList statuslist= new ArrayList();
			
			restype=request.getParameter("f");
			resourceAddForm.setResourceflag(request.getParameter("f"));
			request.setAttribute("resourceflag",restype);
			resourceAddForm.setId(request.getParameter("id"));
			resourceAddForm.setId1(request.getParameter("id1"));
			resourceAddForm.setId2(request.getParameter("id2"));
			resourceAddForm.setFunction(request.getParameter("function"));
			catgname=RMdao.getCategoryName(resourceAddForm.getId(),getDataSource( request,"ilexnewDB" ));
			mfgname=RMdao.getMfgName(resourceAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
			subcatg=RMdao.getThirdcatgName(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setFirstcatg(catgname);
			resourceAddForm.setSecondcatg(mfgname);
			resourceAddForm.setThirdcatg(subcatg);
			statuslist=dcomboRM.getStatuslist("rm",getDataSource( request,"ilexnewDB" ));
			request.setAttribute( "dynamiccombo" , dcrm );
			
			if( statuslist.size()>0) {
				dcrm . setStatuslist(statuslist);
			}
			
			resourceAddForm.setStatus("P");
			request.setAttribute("resourceform",resourceAddForm);
			//default value of criticality
			resourceAddForm.setCriticality("1");
			//	for checking if manufacturername is like FIELD
			Checkmanufacturer=RMdao.checkManufacturer(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
			request.setAttribute( "Checkmanufacturer" , Checkmanufacturer+"" );
		}
		//whether to call same same form or other
		if(resourceAddForm.getFlag()!=null )
		{
			if(statusvalue==0 )
			{
				forward = mapping.findForward("success");
			}
			else 
			{
				ArrayList statuslist1= new ArrayList();
				statuslist1=dcomboRM.getStatuslist("rm",getDataSource( request,"ilexnewDB" ));
				dcrm . setStatuslist(statuslist1);
				request.setAttribute( "dynamiccombo" , dcrm );
				forward = mapping.findForward("failure");
			}
			
		}
		else 
		{
			
			forward = mapping.findForward("failure");
		}
				
		return (forward);

	}
	
	
/** This method updates 4th level Category(Items) for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
	public ActionForward update(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ResourceAddForm resourceAddForm = (ResourceAddForm) form;
		DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
		DynamicComboRM dcrm = new DynamicComboRM();
		Menu menu = new Menu();
		String[] fieldlist= new String[11];
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		if( resourceAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Resource" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		int statusvalue=0;
		int Checkmanufacturer=0;
		
		ArrayList criticalitylist= new ArrayList();
		String criticalityname="";
		criticalitylist=RMdao.getCriticalitylist(getDataSource( request,"ilexnewDB" ));
		request.setAttribute("criticalitylist",criticalitylist);
				
		if( resourceAddForm.getSave()!= null )
		{
			
			String restype="";
			String id="";
			String id1="";
			String id2="";
			String id3="";
			String statusname="";
			restype=request.getParameter("resourceflag");
			
			resourceAddForm.setResourceflag(restype);
			request.setAttribute("resourceflag",restype);
			id=resourceAddForm.getId();
			id1=resourceAddForm.getId1();
			id2=resourceAddForm.getId2();
			id3=resourceAddForm.getId3();
			if(restype.equals("2"))
			{
				statusvalue=RMdao.addItems(Integer.parseInt(resourceAddForm.getId3()), Integer.parseInt(resourceAddForm.getId2()),resourceAddForm.getLaborname(),resourceAddForm.getMfgpartno(),resourceAddForm.getSellableqty(),resourceAddForm.getMinimumqty(),resourceAddForm.getUnit(),resourceAddForm.getWeight(),resourceAddForm.getLaboridentifier(),resourceAddForm.getStatus(),resourceAddForm.getBasecost(),Integer.parseInt(userid),"u",resourceAddForm.getCriticality(),getDataSource( request,"ilexnewDB" ));
			}
			else
			{
				statusvalue=RMdao.addItems(Integer.parseInt(resourceAddForm.getId3()), Integer.parseInt(resourceAddForm.getId2()),resourceAddForm.getLaborname(),resourceAddForm.getMfgpartno(),resourceAddForm.getSellableqty(),resourceAddForm.getMinimumqty(),resourceAddForm.getUnit(),resourceAddForm.getWeight(),resourceAddForm.getLaboridentifier(),resourceAddForm.getStatus(),resourceAddForm.getBasecost(),Integer.parseInt(userid),"u","0",getDataSource( request,"ilexnewDB" ));
			}
			
			resourceAddForm.setUpdatemessage(""+statusvalue);
			request.setAttribute("refreshtree","true");
			resourceAddForm.setLaborname(resourceAddForm.getLaborname());
			resourceAddForm.setLaboridentifier(resourceAddForm.getLaboridentifier());
			resourceAddForm.setStatus(resourceAddForm.getStatus());
			resourceAddForm.setSellableqty(resourceAddForm.getSellableqty());
			resourceAddForm.setMinimumqty(resourceAddForm.getMinimumqty());
			resourceAddForm.setUnit(resourceAddForm.getUnit());
			resourceAddForm.setBasecost(resourceAddForm.getBasecost());
			resourceAddForm.setMfgpartno(resourceAddForm.getMfgpartno());
			resourceAddForm.setCnspartno(resourceAddForm.getCnspartno());
			if(restype.equals("2"))
			{
				if(resourceAddForm.getCriticality()!=null)
				{
					resourceAddForm.setCriticality(resourceAddForm.getCriticality());
					criticalityname=RMdao.getCriticalityName(resourceAddForm.getCriticality(),getDataSource( request,"ilexnewDB" ));
					resourceAddForm.setCriticalityname(criticalityname);
				}
			}
			
			/*resourceAddForm.setCriticality(resourceAddForm.getCriticality());
			criticalityname=RMdao.getCriticalityName(resourceAddForm.getCriticality(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setCriticalityname(criticalityname);*/
			statusname=RMdao.getStatusName(resourceAddForm.getStatus(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setStatus(statusname);
			
//			 Set created by and updated by
			fieldlist=RMdao.getFieldlist(resourceAddForm.getId3(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setCreatedby(fieldlist[11]);
			resourceAddForm.setCreatedate(fieldlist[12]);
			resourceAddForm.setChangedby(fieldlist[13]);
			resourceAddForm.setChangedate(fieldlist[14]);
			
			request.setAttribute( "resourceform" , resourceAddForm );
			
			resourceAddForm.setFlag("1");
			
			//	for checking if manufacturername is like FIELD
			Checkmanufacturer=RMdao.checkManufacturer(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
			request.setAttribute( "Checkmanufacturer" , Checkmanufacturer+"" );
			
			
		}
		else
		{
			
			String restype="";
			ArrayList statuslist= new ArrayList();
			String firstcatg="";
			String secondcatg="";
			String thirdcatg="";
			String criticalityid="";
			restype=request.getParameter("f");
			resourceAddForm.setResourceflag(request.getParameter("f"));
			request.setAttribute("resourceflag",restype);
			resourceAddForm.setId(request.getParameter("id"));
			resourceAddForm.setId1(request.getParameter("id1"));
			resourceAddForm.setId2(request.getParameter("id2"));
			resourceAddForm.setId3(request.getParameter("id3"));
			fieldlist=RMdao.getFieldlist(resourceAddForm.getId3(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setId2(fieldlist[0]);
			resourceAddForm.setLaborname(fieldlist[1]);
			resourceAddForm.setLaboridentifier(fieldlist[2]);
			resourceAddForm.setStatus(fieldlist[3]);
			resourceAddForm.setUpdatecombovalue(fieldlist[3]);
			resourceAddForm.setSellableqty(fieldlist[4]);
			resourceAddForm.setMinimumqty(fieldlist[5]);
			resourceAddForm.setUnit(fieldlist[6]);
			resourceAddForm.setBasecost(fieldlist[7]);
			resourceAddForm.setMfgpartno(fieldlist[8]);
			resourceAddForm.setCnspartno(fieldlist[9]);
			
			if(restype.equals("2"))
			{
				if(fieldlist[10]!=null)
				{
					criticalityid=RMdao.getCriticalityid(fieldlist[10],getDataSource( request,"ilexnewDB" ));
					resourceAddForm.setCriticality(criticalityid);
				}
			}
			/*criticalityid=RMdao.getCriticalityid(fieldlist[10],getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setCriticality(criticalityid);*/
			statuslist=menu.getStatusrm("rm",fieldlist[3],userid,getDataSource( request,"ilexnewDB" ));
			dcrm . setStatuslist(statuslist);
			firstcatg=RMdao.getCategoryName(resourceAddForm.getId(),getDataSource( request,"ilexnewDB" ));
			secondcatg=RMdao.getMfgName(resourceAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
			thirdcatg=RMdao.getThirdcatgName(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setFirstcatg(firstcatg);
			resourceAddForm.setSecondcatg(secondcatg);
			resourceAddForm.setThirdcatg(thirdcatg);
			request.setAttribute( "dynamiccombo" , dcrm );
			
			//	for checking if manufacturername is like FIELD
			Checkmanufacturer=RMdao.checkManufacturer(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
			request.setAttribute( "Checkmanufacturer" , Checkmanufacturer+"" );
			
			
		}
		
		//whether to call same same form or other
		if(resourceAddForm.getFlag()!=null )
		{
			
			if(statusvalue==0 )
			{
				
				forward = mapping.findForward("success");
				
			}
			else 
			{
				ArrayList statuslist1= new ArrayList();
				statuslist1=menu.getStatusrm("rm",resourceAddForm.getUpdatecombovalue(),userid,getDataSource( request,"ilexnewDB" ));
				dcrm . setStatuslist(statuslist1);
				request.setAttribute( "dynamiccombo" , dcrm );
				forward = mapping.findForward("failure");
			}
			
		}
		else 
		{
			
			forward = mapping.findForward("failure");
		}
		
		request.setAttribute( "resourceform" , resourceAddForm );
		
		return (forward);

	}

	
	
/** This method deletes 4th level Category(Items) for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
	public ActionForward delete(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ResourceAddForm resourceAddForm = (ResourceAddForm) form;
		DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
		DynamicComboRM dcrm = new DynamicComboRM();
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		if( resourceAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Resource" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		int statusvalue=0;
		String restype="";
		String firstcatg="";
		String secondcatg="";
		String thirdcatg="";
		String statusname="";
		int Checkmanufacturer=0;
		String[] fieldlist= new String[11];
		ArrayList statuslist= new ArrayList();
		ArrayList criticalitylist= new ArrayList();
		restype=request.getParameter("f");
		resourceAddForm.setResourceflag(request.getParameter("f"));
		request.setAttribute("resourceflag",restype);
		resourceAddForm.setId(request.getParameter("id"));
		resourceAddForm.setId1(request.getParameter("id1"));
		resourceAddForm.setId2(request.getParameter("id2"));
		resourceAddForm.setId3(request.getParameter("id3"));
		statusvalue=RMdao.deleteResourceItem(Integer.parseInt(request.getParameter("id3")),Integer.parseInt(userid),getDataSource( request,"ilexnewDB" ));
		resourceAddForm.setMessage(""+statusvalue);
		request.setAttribute("refreshtree","true");
		firstcatg=RMdao.getCategoryName(resourceAddForm.getId(),getDataSource( request,"ilexnewDB" ));
		secondcatg=RMdao.getMfgName(resourceAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
		thirdcatg=RMdao.getThirdcatgName(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
		resourceAddForm.setFirstcatg(firstcatg);
		resourceAddForm.setSecondcatg(secondcatg);
		resourceAddForm.setThirdcatg(thirdcatg);
		statuslist=dcomboRM.getStatuslist("rm",getDataSource( request,"ilexnewDB" ));
		dcrm . setStatuslist(statuslist);
		//resourceAddForm.setId3("");
		//resourceAddForm.setFunction("add");
		resourceAddForm.setFlag(null); 
		request.setAttribute( "dynamiccombo" , dcrm );
		
		//	for checking if manufacturername is like FIELD
		Checkmanufacturer=RMdao.checkManufacturer(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
		request.setAttribute( "Checkmanufacturer" , Checkmanufacturer+"" );
		
		
		criticalitylist=RMdao.getCriticalitylist(getDataSource( request,"ilexnewDB" ));
		request.setAttribute("criticalitylist",criticalitylist);
		if(statusvalue==0 )
		{
			resourceAddForm.setId3("");
			resourceAddForm.setFunction("add");
			resourceAddForm.setStatus("P");	
			//	default value of criticality
			resourceAddForm.setCriticality("1");
			forward = mapping.findForward("failure");
		}
		else 
		{
			
			fieldlist=RMdao.getFieldlist(resourceAddForm.getId3(),getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setId2(fieldlist[0]);
			resourceAddForm.setLaborname(fieldlist[1]);
			resourceAddForm.setLaboridentifier(fieldlist[2]);
			statusname=RMdao.getStatusName(fieldlist[3],getDataSource( request,"ilexnewDB" ));
			resourceAddForm.setStatus(statusname);
			resourceAddForm.setSellableqty(fieldlist[4]);
			resourceAddForm.setMinimumqty(fieldlist[5]);
			resourceAddForm.setUnit(fieldlist[6]);
			resourceAddForm.setBasecost(fieldlist[7]);
			resourceAddForm.setMfgpartno(fieldlist[8]);
			resourceAddForm.setCnspartno(fieldlist[9]);
			resourceAddForm.setCriticalityname(fieldlist[10]);
			forward = mapping.findForward("success");
		}
		
		request.setAttribute( "resourceform" , resourceAddForm );
		
		return (forward);

	}

	
	
/** This method displays 4th level Category(Items) details for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
	public ActionForward view(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ResourceAddForm resourceAddForm = (ResourceAddForm) form;
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		if( resourceAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "View Resource" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		String restype="";
		String catgname="";
		String mfgname="";
		String subcatg="";
		String statusname="";
		String[] fieldlist= new String[11];
		int Checkmanufacturer=0;
		
		restype=request.getParameter("f");
		resourceAddForm.setResourceflag(restype);
		request.setAttribute("resourceflag",restype);
		resourceAddForm.setId(request.getParameter("id"));
		resourceAddForm.setId1(request.getParameter("id1"));
		resourceAddForm.setId2(request.getParameter("id2"));
		resourceAddForm.setId3(request.getParameter("id3"));
		catgname=RMdao.getCategoryName(resourceAddForm.getId(),getDataSource( request,"ilexnewDB" ));
		mfgname=RMdao.getMfgName(resourceAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
		subcatg=RMdao.getThirdcatgName(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
		resourceAddForm.setFirstcatg(catgname);
		resourceAddForm.setSecondcatg(mfgname);
		resourceAddForm.setThirdcatg(subcatg);
		fieldlist=RMdao.getFieldlist(resourceAddForm.getId3(),getDataSource( request,"ilexnewDB" ));
		resourceAddForm.setId2(fieldlist[0]);
		resourceAddForm.setLaborname(fieldlist[1]);
		resourceAddForm.setLaboridentifier(fieldlist[2]);
		statusname=RMdao.getStatusName(fieldlist[3],getDataSource( request,"ilexnewDB" ));
		resourceAddForm.setStatus(statusname);
		resourceAddForm.setSellableqty(fieldlist[4]);
		resourceAddForm.setMinimumqty(fieldlist[5]);
		resourceAddForm.setUnit(fieldlist[6]);
		resourceAddForm.setBasecost(fieldlist[7]);
		resourceAddForm.setMfgpartno(fieldlist[8]);
		resourceAddForm.setCnspartno(fieldlist[9]);
		resourceAddForm.setCriticalityname(fieldlist[10]);
		resourceAddForm.setCreatedby(fieldlist[11]);
		resourceAddForm.setCreatedate(fieldlist[12]);
		resourceAddForm.setChangedby(fieldlist[13]);
		resourceAddForm.setChangedate(fieldlist[14]);
		request.setAttribute("resourceform",resourceAddForm);
		
		//	for checking if manufacturername is like FIELD
		Checkmanufacturer=RMdao.checkManufacturer(resourceAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
		request.setAttribute( "Checkmanufacturer" , Checkmanufacturer+"" );
		
		
		forward = mapping.findForward("success");
		
		return (forward);

	}

	
}
