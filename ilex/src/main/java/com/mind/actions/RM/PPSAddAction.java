/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is an action class used for perform multiple
* operations Add,Update,Delete on 3rd level Category of Resources in Resource Manager.      
*
*/
package com.mind.actions.RM;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboRM;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDaoRM;
import com.mind.dao.RM.RMdao;
import com.mind.formbean.RM.PPSAddForm;



public class PPSAddAction extends com.mind.common.IlexDispatchAction 
{
	
	
/** This method adds 3rd level categories for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/	
	public ActionForward add(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)	throws Exception 
	{

			ActionErrors errors = new ActionErrors();
			ActionForward forward = new ActionForward();
			DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
			DynamicComboRM dcrm = new DynamicComboRM();
			
			
			PPSAddForm ppsAddForm = (PPSAddForm) form;
			
			/* Page Security Start*/
			HttpSession session = request.getSession( true );
			
			if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
			String userid = (String)session.getAttribute("userid");
			
			if( ppsAddForm.getAuthenticate().equals( "" ) )
			{
				if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage PPStype" , getDataSource( request , "ilexnewDB" ) ) ) 
				{
					return( mapping.findForward( "UnAuthenticate" ) );	
				}
			}
			/* Page Security End*/
			int statusvalue=0;
			
			if( ppsAddForm.getSave()!= null )
			{
				
				String restype="";
				String id="";
				String id1="";
				String firstcatg="";
				String secondcatg="";
				String thirdcatg="";
				String ppsid="";
				restype=request.getParameter("resourceflag");
				ppsAddForm.setResourceflag(restype);
				request.setAttribute("resourceflag",restype);
				id=ppsAddForm.getId();
				id1=ppsAddForm.getId1();
				firstcatg=ppsAddForm.getFirstcatg();
				ppsAddForm.setFirstcatg(firstcatg);
				secondcatg=ppsAddForm.getSecondcatg();
				ppsAddForm.setSecondcatg(secondcatg);
				thirdcatg=ppsAddForm.getPpstype();
				
				if(Integer.parseInt(ppsAddForm.getExistmfg())!=0)
				{
					statusvalue=RMdao.thirdLevelCatg(ppsAddForm.getId1(),ppsAddForm.getExistmfg(),ppsAddForm.getPpstype(),"u",getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setUpdatemessage(""+statusvalue);
					
					request.setAttribute("refreshtree","true");
				}
				else
				{
					statusvalue=RMdao.thirdLevelCatg(ppsAddForm.getId1(),ppsAddForm.getId2(),ppsAddForm.getPpstype(),"a",getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setAddmessage(""+statusvalue);
					
					request.setAttribute("refreshtree","true");
				}
				ppsid=RMdao.getPpsid(ppsAddForm.getId1(),ppsAddForm.getPpstype(),getDataSource( request,"ilexnewDB" ));
				ppsAddForm.setPpstype(thirdcatg);
				ppsAddForm.setId(id);
				ppsAddForm.setId1(id1);
				ppsAddForm.setId2(ppsid);
				request.setAttribute( "id2" , ppsAddForm.getId2() );
				request.setAttribute( "ppsform" , ppsAddForm );
				ppsAddForm.setFlag("1");
				
				
			}
			else
			{
				
				ArrayList tcatglist= new ArrayList();
				if( ppsAddForm.getRefresh())
				{
					String restype="";
					String thirdcatg="";
					String id2="";
					String seccatgname="";
					restype=request.getParameter("resourceflag");
					ppsAddForm.setResourceflag(restype);
					request.setAttribute("resourceflag",restype);
					
					thirdcatg=RMdao.getThirdcatgName(ppsAddForm.getExistmfg(),getDataSource( request,"ilexnewDB" ));
					
					ppsAddForm.setPpstype(thirdcatg);
					
					ppsAddForm.setExistmfg(ppsAddForm.getExistmfg());
					
					ppsAddForm.setId2(ppsAddForm.getExistmfg());
					
					id2= ppsAddForm.getId2();
					
					seccatgname=RMdao.getMfgName(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setSecondcatg(seccatgname);
					
					tcatglist=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					request.setAttribute( "dynamiccombo" , dcrm );
					request.setAttribute("id2",id2);
					request.setAttribute( "ppsform" , ppsAddForm );
					
					if( tcatglist.size()>0)
					{
						
						dcrm .setThirdlevelcatglist(tcatglist);
					}
					ppsAddForm.setFlag(null);
				
				}
				else
				{
					String restype="";
					String category="";
					String mfgname="";
					restype=request.getParameter("f");
					ppsAddForm.setResourceflag(request.getParameter("f"));
					request.setAttribute("resourceflag",restype);
					ppsAddForm.setId(request.getParameter("id"));
					ppsAddForm.setId1(request.getParameter("id1"));
					
					category=RMdao.getCategoryName(ppsAddForm.getId(),getDataSource( request,"ilexnewDB" ));
					mfgname=RMdao.getMfgName(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setFirstcatg(category);
					ppsAddForm.setSecondcatg(mfgname);
					
					tcatglist=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					request.setAttribute( "dynamiccombo" , dcrm );
					if( tcatglist.size()>0)
					{
						
						dcrm . setThirdlevelcatglist(tcatglist);
					}
				}
				request.setAttribute( "ppsform" , ppsAddForm );	
				
			}
			
				
			if(ppsAddForm.getFlag()!=null )
			{
				
				
				if(statusvalue==0 )
				{
					
					forward = mapping.findForward("success");
					
				}
				else 
				{
					ArrayList tcatglist1= new ArrayList();
					tcatglist1=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					dcrm . setThirdlevelcatglist(tcatglist1);
					request.setAttribute( "dynamiccombo" , dcrm );
					
					forward = mapping.findForward("failure");
				}
				
			}
			else 
			{
				
				forward = mapping.findForward("failure");
			}
			
			
			return (forward);

		}
		
		
	
/** This method updates 3rd level categories for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
		public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
		{
			
				ActionErrors errors = new ActionErrors();
				ActionForward forward = new ActionForward();
				DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
				DynamicComboRM dcrm = new DynamicComboRM();
				PPSAddForm ppsAddForm = (PPSAddForm) form;
				
				/* Page Security Start*/
				HttpSession session = request.getSession( true );
				
				if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
				String userid = (String)session.getAttribute("userid");
				
				if( ppsAddForm.getAuthenticate().equals( "" ) )
				{
					if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage PPStype" , getDataSource( request , "ilexnewDB" ) ) ) 
					{
						return( mapping.findForward( "UnAuthenticate" ) );	
					}
				}
				/* Page Security End*/
				int statusvalue=0;
				if( ppsAddForm.getSave()!= null )
				{
					
					String restype="";
					String id="";
					String id1="";
					String firstcatg="";
					String secondcatg="";
					String thirdcatg="";
					String ppsid="";
					restype=request.getParameter("resourceflag");
					ppsAddForm.setResourceflag(restype);
					request.setAttribute("resourceflag",restype);
					id=ppsAddForm.getId();
					id1=ppsAddForm.getId1();
					firstcatg=ppsAddForm.getFirstcatg();
					secondcatg=ppsAddForm.getSecondcatg();
					thirdcatg=ppsAddForm.getPpstype();
					if(Integer.parseInt(ppsAddForm.getExistmfg())!=0)
					{
						statusvalue=RMdao.thirdLevelCatg(ppsAddForm.getId1(),ppsAddForm.getExistmfg(),ppsAddForm.getPpstype(),"u",getDataSource( request,"ilexnewDB" ));
						ppsAddForm.setUpdatemessage(""+statusvalue);
						request.setAttribute("refreshtree","true");
					}
					else
					{
						statusvalue=RMdao.thirdLevelCatg(ppsAddForm.getId1(),ppsAddForm.getId2(),ppsAddForm.getPpstype(),"a",getDataSource( request,"ilexnewDB" ));
						ppsAddForm.setAddmessage(""+statusvalue);
						request.setAttribute("refreshtree","true");
					}
					ppsid=RMdao.getPpsid(ppsAddForm.getId1(),ppsAddForm.getPpstype(),getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setId(id);
					ppsAddForm.setId1(id1);
					ppsAddForm.setId2(ppsid);
					request.setAttribute( "ppsform" , ppsAddForm );
					ppsAddForm.setFlag("1");
					
				}
				else
				{
					
					ArrayList tcatglist= new ArrayList();
					
					if( ppsAddForm.getRefresh())
					{
						String restype="";
						String thirdcatg="";
						String id2="";
						String seccatgname="";
						restype=request.getParameter("resourceflag");
						ppsAddForm.setResourceflag(restype);
						request.setAttribute("resourceflag",restype);
						
						thirdcatg=RMdao.getThirdcatgName(ppsAddForm.getExistmfg(),getDataSource( request,"ilexnewDB" ));
						
						ppsAddForm.setPpstype(thirdcatg);
						
						ppsAddForm.setExistmfg(ppsAddForm.getExistmfg());
						
						ppsAddForm.setId2(ppsAddForm.getExistmfg());
						
						id2= ppsAddForm.getId2();
						
						seccatgname=RMdao.getMfgName(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
						ppsAddForm.setSecondcatg(seccatgname);
						
						tcatglist=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
						request.setAttribute( "dynamiccombo" , dcrm );
						request.setAttribute("id2",id2);
						request.setAttribute( "ppsform" , ppsAddForm );
						
						if( tcatglist.size()>0)
						{
							
							dcrm .setThirdlevelcatglist(tcatglist);
						}
						ppsAddForm.setFlag(null);
					
					}
					else
					{
						String restype="";
						String catgname="";
						String mfgname="";
						String thirdcatg="";
						String id2="";
						ppsAddForm.setId(request.getParameter("id"));
						ppsAddForm.setId1(request.getParameter("id1"));
						ppsAddForm.setId2(request.getParameter("id2"));
						ppsAddForm.setResourceflag(request.getParameter("f"));
						restype=request.getParameter("f");
						request.setAttribute("resourceflag",restype);
						catgname=RMdao.getCategoryName(ppsAddForm.getId(),getDataSource( request,"ilexnewDB" ));
						ppsAddForm.setFirstcatg(catgname);
						mfgname=RMdao.getMfgName(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
						thirdcatg=RMdao.getThirdcatgName(ppsAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
						
						ppsAddForm.setSecondcatg(mfgname);
						ppsAddForm.setPpstype(thirdcatg);
						ppsAddForm.setExistmfg(ppsAddForm.getId2());
						id2= ppsAddForm.getId2();
						request.setAttribute("id2",id2);
						
						tcatglist=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
						request.setAttribute( "dynamiccombo" , dcrm );
						if( tcatglist.size()>0)
						{
							
							dcrm . setThirdlevelcatglist(tcatglist);
						}
						
						
					}
					
					
				}	
				//whether to call same same form or other
				if(ppsAddForm.getFlag()!=null )
				{
					
					if(statusvalue==0 )
					{
						
						forward = mapping.findForward("success");
						
					}
					
					else 
					{
						ArrayList tcatglist1= new ArrayList();
						tcatglist1=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
						dcrm . setThirdlevelcatglist(tcatglist1);
						request.setAttribute( "dynamiccombo" , dcrm );
						
						forward = mapping.findForward("failure");
					}
					
				}
				else 
				{
					
					forward = mapping.findForward("failure");
				}
					
				
				request.setAttribute( "ppsform" , ppsAddForm );
				return (forward);

			}
		
	
/** This method deletes 3rd level categories for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/
		public ActionForward delete(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
		{
			
			Collection subcatglist;
			ActionErrors errors = new ActionErrors();
			ActionForward forward = new ActionForward();
			PPSAddForm ppsAddForm = (PPSAddForm) form;
			DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
			DynamicComboRM dcrm = new DynamicComboRM();
			
			/* Page Security Start*/
			HttpSession session = request.getSession( true );
			
			if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
			String userid = (String)session.getAttribute("userid");
			
			if( ppsAddForm.getAuthenticate().equals( "" ) )
			{
				if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage PPStype" , getDataSource( request , "ilexnewDB" ) ) ) 
				{
					return( mapping.findForward( "UnAuthenticate" ) );	
				}
			}
			/* Page Security End*/
			int statusvalue=0;
			if( ppsAddForm.getSave()!= null )
			{
				String restype="";
				String id="";
				String id1="";
				String firstcatg="";
				String secondcatg="";
				String thirdcatg="";
				String ppsid="";
				restype=request.getParameter("resourceflag");
				ppsAddForm.setResourceflag(restype);
				request.setAttribute("resourceflag",restype);
				id=ppsAddForm.getId();
				id1=ppsAddForm.getId1();
				firstcatg=ppsAddForm.getFirstcatg();
				ppsAddForm.setFirstcatg(firstcatg);
				secondcatg=ppsAddForm.getSecondcatg();
				ppsAddForm.setSecondcatg(secondcatg);
				thirdcatg=ppsAddForm.getPpstype();
				
				if(Integer.parseInt(ppsAddForm.getExistmfg())!=0)
				{
					RMdao.thirdLevelCatg(ppsAddForm.getId1(),ppsAddForm.getExistmfg(),ppsAddForm.getPpstype(),"u",getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setUpdatemessage(""+statusvalue);
					request.setAttribute("refreshtree","true");
				}
				else
				{
					RMdao.thirdLevelCatg(ppsAddForm.getId1(),ppsAddForm.getId2(),ppsAddForm.getPpstype(),"a",getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setAddmessage(""+statusvalue);
					request.setAttribute("refreshtree","true");
				}
				ppsid=RMdao.getPpsid(ppsAddForm.getId1(),ppsAddForm.getPpstype(),getDataSource( request,"ilexnewDB" ));
				ppsAddForm.setPpstype(thirdcatg);
				ppsAddForm.setId(id);
				ppsAddForm.setId1(id1);
				ppsAddForm.setId2(ppsid);
				request.setAttribute("id2",ppsid);
				request.setAttribute( "ppsform" , ppsAddForm );
				ppsAddForm.setFlag("1");
				
				
			}
			else
			{
				ArrayList tcatglist= new ArrayList();
				if( ppsAddForm.getRefresh())
				{
					String restype="";
					String thirdcatg="";
					String id2="";
					String seccatgname="";
					restype=request.getParameter("resourceflag");
					ppsAddForm.setResourceflag(restype);
					request.setAttribute("resourceflag",restype);
					
					thirdcatg=RMdao.getThirdcatgName(ppsAddForm.getExistmfg(),getDataSource( request,"ilexnewDB" ));
					
					ppsAddForm.setPpstype(thirdcatg);
					
					ppsAddForm.setExistmfg(ppsAddForm.getExistmfg());
					
					ppsAddForm.setId2(ppsAddForm.getExistmfg());
					
					id2= ppsAddForm.getId2();
					
					seccatgname=RMdao.getMfgName(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setSecondcatg(seccatgname);
					
					tcatglist=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					request.setAttribute( "dynamiccombo" , dcrm );
					request.setAttribute("id2",id2);
					request.setAttribute( "ppsform" , ppsAddForm );
					
					if( tcatglist.size()>0)
					{
						
						dcrm .setThirdlevelcatglist(tcatglist);
					}
					ppsAddForm.setMessage(null);
					ppsAddForm.setFlag(null);
				}
				else
				{
				
					String restype="";
					String catgname="";
					String mfgname="";
					restype=request.getParameter("f");
					ppsAddForm.setResourceflag(restype);
					request.setAttribute("resourceflag",restype);
					catgname=RMdao.getCategoryName(ppsAddForm.getId(),getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setFirstcatg(catgname);
					
					int status=RMdao.deleteThirdLevelCatg(ppsAddForm.getId2(),getDataSource( request,"ilexnewDB" ));
					request.setAttribute("refreshtree","true");
					mfgname=RMdao.getMfgName(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					ppsAddForm.setSecondcatg(mfgname);
					ppsAddForm.setExistmfg("0");
					ppsAddForm.setPpstype(null);
					tcatglist=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					request.setAttribute( "dynamiccombo" , dcrm );
					if( tcatglist.size()>0)
					{
						
						dcrm . setThirdlevelcatglist(tcatglist);
					}
					
					if((request.getParameter("id2"))!=null)
					{
						ppsAddForm.setMessage(""+status);
					}
					else
					{
						ppsAddForm.setMessage(null);
					}
					
					ppsAddForm.setFlag(null);
					request.setAttribute( "ppsform" , ppsAddForm );
				}
			}
			
			if(ppsAddForm.getFlag()!=null )
			{
				
				
				if(statusvalue==0 )
				{
					
					forward = mapping.findForward("success");
					
				}
				
				else 
				{
					ArrayList tcatglist1= new ArrayList();
					tcatglist1=dcomboRM.getThirdcatglist(ppsAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
					dcrm . setThirdlevelcatglist(tcatglist1);
					request.setAttribute( "dynamiccombo" , dcrm );
					
					forward = mapping.findForward("failure");
				}
				
			}
			else 
			{
	            //	check delete success
					forward = mapping.findForward("failure");
			}
					
			return (forward);
			
	
		}
		
}
