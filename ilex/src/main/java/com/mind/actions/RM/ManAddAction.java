/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is an action class used for perform multiple
* operations Add,Update,Delete on 2nd level Category of Resources in Resource Manager.      
*
*/




package com.mind.actions.RM;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboRM;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDaoRM;
import com.mind.dao.RM.RMdao;
import com.mind.formbean.RM.ManAddForm;


public class ManAddAction extends com.mind.common.IlexDispatchAction
{
	
/** This method adds 2nd level categories for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/	
	public ActionForward add(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		Collection subcatglist;
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ManAddForm manAddForm = (ManAddForm) form;
		DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
		DynamicComboRM dcrm = new DynamicComboRM();
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		if( manAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Manufacturer" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		
		int statusvalue=0;
		
				
		if( manAddForm.getSave()!= null )
		{
			
			String restype="";
			String catg="";
			String subcatg="";
			String id="";
			String subcatid="";
			
			restype=request.getParameter("resourceflag");
			manAddForm.setResourceflag(restype);
			request.setAttribute("resourceflag",restype);
			catg = manAddForm.getCategory();
			manAddForm.setCategory(catg);
			subcatg = manAddForm.getSubcatg();
			id=manAddForm.getId();
			manAddForm.setId(id);
			
			if(Integer.parseInt(manAddForm.getExistcategory())!=0)
			{
				
				statusvalue=RMdao.secondLevelCatg(manAddForm.getId(),manAddForm.getExistcategory(),manAddForm.getSubcatg(),"u",getDataSource( request,"ilexnewDB" ));
				 
				manAddForm.setUpdatemessage(""+statusvalue);
				
				request.setAttribute("refreshtree","true");
			}
			else
			{
				
				statusvalue=RMdao.secondLevelCatg(manAddForm.getId(),manAddForm.getId1(),manAddForm.getSubcatg(),"a",getDataSource( request,"ilexnewDB" ));
				manAddForm.setAddmessage(""+statusvalue);
				
				request.setAttribute("refreshtree","true");
			}
			
			subcatid=RMdao.getMfgid(manAddForm.getId(),manAddForm.getSubcatg(),getDataSource( request,"ilexnewDB" ));
			manAddForm.setId1(subcatid);
			
			request.setAttribute( "manform" , manAddForm );
			manAddForm.setFlag("1");
			request.setAttribute("id1",manAddForm.getId1());
		
			
		}
		else
		{
			ArrayList scatglist= new ArrayList();
			
			
			
			if( manAddForm.getRefresh())
			{
				
				String restype="";
				String subcat="";
				String id1="";
				String catgname="";
				restype=request.getParameter("resourceflag");
				manAddForm.setResourceflag(restype);
				request.setAttribute("resourceflag",restype);
				
				subcat=RMdao.getMfgName(manAddForm.getExistcategory(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setSubcatg(subcat);
				manAddForm.setExistcategory(manAddForm.getExistcategory());
				manAddForm.setId1(manAddForm.getExistcategory());
				id1= manAddForm.getId1();
				
				catgname=RMdao.getCategoryName(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setCategory(catgname);
				
				scatglist=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				request.setAttribute( "dynamiccombo" , dcrm );
				request.setAttribute("id1",id1);
				request.setAttribute( "manform" , manAddForm );
				
				if( scatglist.size()>0)
				{
					
					dcrm . setSecondlevelcatglist(scatglist);
				}
				manAddForm.setFlag(null);
			
			}
			else
			{
				
				String restype="";
				String catgname="";
				restype=request.getParameter("f");
				manAddForm.setResourceflag(request.getParameter("f"));
				request.setAttribute("resourceflag",restype);
				manAddForm.setId(request.getParameter("id"));
				catgname=RMdao.getCategoryName(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setCategory(catgname);
				
				scatglist=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				request.setAttribute( "dynamiccombo" , dcrm );
			
				if( scatglist.size()>0)
				{
					
					dcrm . setSecondlevelcatglist(scatglist);
				}
				request.setAttribute( "manform" , manAddForm );
			}
		
		}
		
		//whether to call same  form or other
		
		
		
		if(manAddForm.getFlag()!=null )
		{
			
			
			if(statusvalue==0 )
			{
				
				forward = mapping.findForward("success");
				
			}
			else 
			{
				ArrayList scatglist1= new ArrayList();
				scatglist1=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				dcrm . setSecondlevelcatglist(scatglist1);
				request.setAttribute( "dynamiccombo" , dcrm );
				
				forward = mapping.findForward("failure");
			}
			
		}
		else 
		{
			
			forward = mapping.findForward("failure");
		}
			
		
		
		
		return (forward);

	}
	
	
	
	
/** This method updates 2nd level categories for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/	
	
	public ActionForward update(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		
		String category1;
		Collection subcatglist;
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ManAddForm manAddForm = (ManAddForm) form;
		DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
		DynamicComboRM dcrm = new DynamicComboRM();
		int statusvalue=0;
		
		
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		if( manAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Manufacturer" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		
		if( manAddForm.getSave()!= null )
		{
			
			String subcatg="";
			String restype="";
			String subcatid="";
			subcatg=manAddForm.getSubcatg();
			
			restype=request.getParameter("resourceflag");
			manAddForm.setResourceflag(restype);
			request.setAttribute("resourceflag",restype);
			if(Integer.parseInt(manAddForm.getExistcategory())!=0)
			{
				statusvalue=RMdao.secondLevelCatg(manAddForm.getId(),manAddForm.getExistcategory(),manAddForm.getSubcatg(),"u",getDataSource( request,"ilexnewDB" ));
				manAddForm.setUpdatemessage(""+statusvalue);
				request.setAttribute("refreshtree","true");
			}
			else
			{
				statusvalue=RMdao.secondLevelCatg(manAddForm.getId(),manAddForm.getId1(),manAddForm.getSubcatg(),"a",getDataSource( request,"ilexnewDB" ));
				manAddForm.setAddmessage(""+statusvalue);
				request.setAttribute("refreshtree","true");
			}
			subcatid=RMdao.getMfgid(manAddForm.getId(),manAddForm.getSubcatg(),getDataSource( request,"ilexnewDB" ));
			manAddForm.setId1(subcatid);
			request.setAttribute( "manform" , manAddForm );
			manAddForm.setFlag("1");
			
		}
		else
		{
			ArrayList scatglist= new ArrayList();
			
			if( manAddForm.getRefresh())
			{
				
				String restype="";
				String subcat="";
				String id1="";
				String catgname="";
				restype=request.getParameter("resourceflag");
				manAddForm.setResourceflag(restype);
				request.setAttribute("resourceflag",restype);
				
				subcat=RMdao.getMfgName(manAddForm.getExistcategory(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setSubcatg(subcat);
				manAddForm.setExistcategory(manAddForm.getExistcategory());
				manAddForm.setId1(manAddForm.getExistcategory());
				id1= manAddForm.getId1();
				catgname=RMdao.getCategoryName(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setCategory(catgname);
				scatglist=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				request.setAttribute( "dynamiccombo" , dcrm );
				request.setAttribute("id1",id1);
				request.setAttribute( "manform" , manAddForm );
				
				if( scatglist.size()>0)
				{
					
					dcrm . setSecondlevelcatglist(scatglist);
				}
				manAddForm.setFlag(null);
			
			}
			else
			{
				
				String restype="";
				String catgname="";
				String mfgname="";
				String id1="";
				manAddForm.setId(request.getParameter("id"));
				manAddForm.setId1(request.getParameter("id1"));
				manAddForm.setResourceflag(request.getParameter("f"));
				restype=request.getParameter("f");
				request.setAttribute("resourceflag",restype);
				catgname=RMdao.getCategoryName(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setCategory(catgname);
				mfgname=RMdao.getMfgName(manAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setSubcatg(mfgname);
				manAddForm.setExistcategory(manAddForm.getId1());
				id1= manAddForm.getId1();
				request.setAttribute("id1",id1);
				
				scatglist=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				request.setAttribute( "dynamiccombo" , dcrm );
				if( scatglist.size()>0)
				{
					
					dcrm . setSecondlevelcatglist(scatglist);
				}
				
			}
			
		}
		//whether to call same  form or other
		if(manAddForm.getFlag()!=null )
		{
			
			
			if(statusvalue==0 )
			{
				
				forward = mapping.findForward("success");
				
			}
			else 
			{
				ArrayList scatglist1= new ArrayList();
				scatglist1=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				dcrm . setSecondlevelcatglist(scatglist1);
				request.setAttribute( "dynamiccombo" , dcrm );
				
				forward = mapping.findForward("failure");
			}
			
		}
		else 
		{
			
			forward = mapping.findForward("failure");
		}
				
		request.setAttribute( "manform" , manAddForm );
		return (forward);

	}
	
	
/** This method deletes 2nd level categories for all resources(material/labor/freight/travel) in Resource Manager. 
* @param mapping   			       Object of ActionMapping
* @param form   			       Object of ActionForm
* @param request   			       Object of HttpServletRequest
* @param response   			   Object of HttpServletResponse
* @return ActionForward            This method returns object of ActionForward.
*/	
	public ActionForward delete(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		Collection subcatglist;
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ManAddForm manAddForm = (ManAddForm) form;
		DynamicComboDaoRM dcomboRM=new DynamicComboDaoRM();
		DynamicComboRM dcrm = new DynamicComboRM();
		int statusvalue=0;
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		if( manAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Manufacturer" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		if( manAddForm.getSave()!= null )
		{
			
			String subcatg="";
			String restype="";
			String subcatid="";
			subcatg=manAddForm.getSubcatg();
			
			restype=request.getParameter("resourceflag");
			manAddForm.setResourceflag(restype);
			request.setAttribute("resourceflag",restype);
			
			if(Integer.parseInt(manAddForm.getExistcategory())!=0)
			{
				
				statusvalue=RMdao.secondLevelCatg(manAddForm.getId(),manAddForm.getExistcategory(),manAddForm.getSubcatg(),"u",getDataSource( request,"ilexnewDB" ));
				manAddForm.setUpdatemessage(""+statusvalue);
				request.setAttribute("refreshtree","true");
			}
			else
			{
				statusvalue=RMdao.secondLevelCatg(manAddForm.getId(),manAddForm.getId1(),manAddForm.getSubcatg(),"a",getDataSource( request,"ilexnewDB" ));
				manAddForm.setAddmessage(""+statusvalue); 
				request.setAttribute("refreshtree","true");
				
			}
			
			subcatid=RMdao.getMfgid(manAddForm.getId(),manAddForm.getSubcatg(),getDataSource( request,"ilexnewDB" ));
			manAddForm.setId1(subcatid);
			
			
			manAddForm.setFlag("1");
		
			request.setAttribute("id1",subcatid);
			request.setAttribute( "manform" , manAddForm );
			
		}
		else
		{
			
			ArrayList scatglist= new ArrayList();
			if( manAddForm.getRefresh())
			{
				String restype="";
				String subcat="";
				String id1="";
				String catgname="";
				restype=request.getParameter("resourceflag");
				manAddForm.setResourceflag(restype);
				request.setAttribute("resourceflag",restype);
				
				subcat=RMdao.getMfgName(manAddForm.getExistcategory(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setSubcatg(subcat);
				manAddForm.setExistcategory(manAddForm.getExistcategory());
				manAddForm.setId1(manAddForm.getExistcategory());
				id1= manAddForm.getId1();
				
				catgname=RMdao.getCategoryName(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setCategory(catgname);
				
				scatglist=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				request.setAttribute( "dynamiccombo" , dcrm );
				request.setAttribute("id1",id1);
				request.setAttribute( "manform" , manAddForm );
				
				if( scatglist.size()>0)
				{
					
					dcrm . setSecondlevelcatglist(scatglist);
				}
				
				manAddForm.setFlag(null);
			}
			else
			{
				
				String restype="";
				String catgname="";
				restype=request.getParameter("f");
				manAddForm.setResourceflag(restype);
				request.setAttribute("resourceflag",restype);
				statusvalue=RMdao.deleteSecondLevelCatg(manAddForm.getId1(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setMessage(""+statusvalue);
				
				request.setAttribute("refreshtree","true");
				catgname=RMdao.getCategoryName(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				manAddForm.setCategory(catgname);
				manAddForm.setExistcategory("0");
				manAddForm.setSubcatg(null);
				scatglist=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
				
				if( scatglist.size()>0)
				{
					
					dcrm . setSecondlevelcatglist(scatglist);
				}
				
								
				manAddForm.setFlag(null);
				request.setAttribute( "manform" , manAddForm );
				request.setAttribute( "dynamiccombo" , dcrm );
			}
		}
		
		
			if(manAddForm.getFlag()!=null )
			{
				
				
				if(statusvalue==0 )
				{
					
					forward = mapping.findForward("success");
					
				}
				else 
				{
					ArrayList scatglist1= new ArrayList();
					scatglist1=dcomboRM.getSubcatglist(manAddForm.getId(),getDataSource( request,"ilexnewDB" ));
					dcrm . setSecondlevelcatglist(scatglist1);
					request.setAttribute( "dynamiccombo" , dcrm );
					
					forward = mapping.findForward("failure");
				}
				
			}
			else 
			{
				//check delete success
						
				forward = mapping.findForward("failure");
			}
			return (forward);

	
	}
	
}

