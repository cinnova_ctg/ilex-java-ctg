/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used for displaying travel resource detail     
*
*/

package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.TravelResource;
import com.mind.formbean.AM.TravelResourceDetailForm;

/*
 * Methods : execute
 */

public class TravelResourceDetailAction extends com.mind.common.IlexAction
{
/** This method populate FreightResourceDetailform formbean with the freight detail of an activity whose data is to be shown
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceDetailForm travelresourcedetailform = ( TravelResourceDetailForm ) form;
		
		TravelResource travelresource = new TravelResource();
		String travel_Id = ""; 
		ArrayList travelresourcelist = new ArrayList();
		String list = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		int sow_size = 0;
		int assumption_size = 0;
		
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		
		if( request.getParameter( "Travel_Id" ) != null )
		{
			travel_Id = request.getParameter( "Travel_Id" );
		}
		
		if( request.getAttribute( "Travel_Id" ) != null )
		{
			travel_Id = ( String ) request.getAttribute( "Travel_Id" );
		}
		
		travelresourcedetailform.setTravelid( travel_Id );
		request.setAttribute( "Travel_Id" , travelresourcedetailform.getTravelid() );
		
		travelresourcelist = ActivityLibrarydao.getMaterialResourcelist( "%" , "T" , travelresourcedetailform.getTravelid() , "" , request.getSession().getId(),"detailpage",getDataSource( request , "ilexnewDB" ) ); 
		
		travelresource = ( TravelResource ) travelresourcelist.get( 0 );
		
		travelresourcedetailform.setActivity_Id( travelresource.getActivity_Id() );
		request.setAttribute( "Activitylibrary_Id" , travelresourcedetailform.getActivity_Id() );  
		
		travelresourcedetailform.setTravelid( travelresource.getTravelid() );
		travelresourcedetailform.setTravelname( travelresource.getTravelname() );
		travelresourcedetailform.setTraveltype( travelresource.getTraveltype() );
		travelresourcedetailform.setMinimumquantity( travelresource.getMinimumquantity() );
		travelresourcedetailform.setCnspartnumber( travelresource.getCnspartnumber() );
		travelresourcedetailform.setEstimatedtotalcost( travelresource.getEstimatedtotalcost() );
		travelresourcedetailform.setEstimatedunitcost( travelresource.getEstimatedunitcost() );
		
		travelresourcedetailform.setPriceextended( travelresource.getPriceextended() );
		travelresourcedetailform.setPriceunit( travelresource.getPriceunit() );
		travelresourcedetailform.setProformamargin( travelresource.getProformamargin() );
		travelresourcedetailform.setQuantity( travelresource.getQuantity() );
		travelresourcedetailform.setSellablequantity( travelresource.getSellablequantity() );
		travelresourcedetailform.setStatus( travelresource.getStatus() );
		
		travelresourcedetailform.setActivity_Id( travelresource.getActivity_Id() );
		travelresourcedetailform.setActivityname( travelresource.getActivity_name() );
		travelresourcedetailform.setMsa_id( travelresource.getMsa_Id() );
		travelresourcedetailform.setMsaname( travelresource.getMsaname() );
		
		
		list = Menu.getStatus( "T" , travelresourcedetailform.getStatus().charAt( 0 ) , "" , "" , "" , travelresourcedetailform.getTravelid() ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "list" , list );
		
		ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , travelresourcedetailform.getTravelid() , "T" );
		
		if( sowlist.size() > 0 )
		{
			sow_size = sowlist.size();	
		}
		
		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , travelresourcedetailform.getTravelid() , "T" );
		
		if( assumptionlist.size() > 0 )
		{
			assumption_size = assumptionlist.size();	
		}
		
		request.setAttribute( "sowsize" , ""+sow_size );
		request.setAttribute( "assumptionsize" , ""+assumption_size );
		
		
		request.setAttribute( "sowlist" , sowlist );
		request.setAttribute( "assumptionlist" , assumptionlist );
		
		
		
		return mapping.findForward( "travelresourcedetailpage" );
	}
}
