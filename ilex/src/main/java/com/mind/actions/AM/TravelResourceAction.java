/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used to manage travel resource of an activity     
*
*/

package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.TravelResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.formbean.AM.TravelResourceForm;

/*
 * Methods : View , Refresh , Submit , Delete
 */

public class TravelResourceAction extends com.mind.common.IlexDispatchAction
{

/** This method is used to list all the travel resources used in an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		String Activitylibrary_Id = "";
		int resource_size = 0;
		ArrayList travelresourcelist = new ArrayList();
		String sortqueryclause = "";
		String status = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String activityname = "";
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		NameId idname = null;
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) != null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			travelresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end
		
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			travelresourceform.setActivity_Id( Activitylibrary_Id );
		}
		else
		{
			Activitylibrary_Id = travelresourceform.getActivity_Id();
		}
		travelresourceform.setNewquantity( "0" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setActivity_Id( Activitylibrary_Id );
		travelresourceform.setTravellist( ActivityLibrarydao.getMateriallist( "travel" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		travelresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "T" , "%" , sortqueryclause , request.getSession().getId() , "listpage", getDataSource( request , "ilexnewDB" ) );
				
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setActivity_Id( idname.getId() );
		travelresourceform.setActivity_name( idname.getName() );
		
		/* for checkbox check start */
		
		String checkTempResources[] = ActivityLibrarydao.checkTempResourcesAM( Activitylibrary_Id , "T" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setCheck( checkTempResources );
		/* for checkbox check end */
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "true" );
		return( mapping.findForward( "travelresourcetabularpage" ) );
	}
	

/** This method is used when the page is refreshed to populate form according to the value selected from the combo 
*	@param mapping   			       object of ActionMapping
*	@param form   			       	   object of ActionForm
*	@param request   			       object of HttpServletRequest
*	@param response   			       object of HttpServletResponse
*	@return ActionForward              This method returns object of ActionForward.
*/	
	public ActionForward Refresh( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		String Activitylibrary_Id = "";
		String newname = "";
		String newnamecombo = "";
		String newtravelid = "";
		int resource_size = 0;
		TravelResource travelresource = new TravelResource();
		int checklength = 0;
		String status = "";
		ArrayList temp = new ArrayList();
		String[] checkrefresh = null;
		ArrayList travelresourcelist = new ArrayList();
		String activityname = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		NameId idname = null;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/* by hamid for back button start*/
		if(request.getParameter("MSA_Id")!=null)
		{
			travelresourceform.setMSA_Id(request.getParameter("MSA_Id"));
		}
		/* by hamid for back button end*/
		
		Activitylibrary_Id = travelresourceform.getActivity_Id();
		
		newname = travelresourceform.getNewtravelname();
		newnamecombo = travelresourceform.getNewtravelnamecombo();
		newtravelid = travelresourceform.getNewtravelid();
		
		
		
		if( travelresourceform.getCheck()!=  null )
		{
			checklength = travelresourceform.getCheck().length;	 
			checkrefresh = travelresourceform.getCheck();
			
			int []tempindex = new int[checklength];
			int k = 0;
				
			if( checklength > 0 )
			{	int length = travelresourceform.getTravelid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( travelresourceform.getCheck( i ).equals( travelresourceform.getTravelid( j ) ) )
						{	
							tempindex[k] = j;
							k++;
						}
					}
				}
			}	
			
			for( int i = 0; i < checklength; i++ )
			{
				TravelResource tempresource = new TravelResource();
				
				tempresource.setActivity_Id( Activitylibrary_Id );
				
				tempresource.setTravelid( travelresourceform.getTravelid( tempindex[i] ) );
				tempresource.setTravelcostlibid( travelresourceform.getTravelcostlibid( tempindex[i] ) );
				
				tempresource.setTravelname( travelresourceform.getTravelname( tempindex[i] ) );
				tempresource.setTraveltype( travelresourceform.getTraveltype( tempindex[i] ) );
				
				tempresource.setCnspartnumber( travelresourceform.getCnspartnumber( tempindex[i] ) );
				tempresource.setEstimatedtotalcost( travelresourceform.getEstimatedtotalcost( tempindex[i] ) );
				tempresource.setEstimatedunitcost( travelresourceform.getEstimatedunitcost( tempindex[i] ) );
				tempresource.setMinimumquantity( travelresourceform.getMinimumquantity( tempindex[i] ) );
				tempresource.setPriceextended( travelresourceform.getPriceextended( tempindex[i] ) );
				tempresource.setPriceunit( travelresourceform.getPriceunit( tempindex[i] ) );
				tempresource.setProformamargin( travelresourceform.getProformamargin( tempindex[i] ) );
				
				tempresource.setQuantity( travelresourceform.getQuantity( tempindex[i] ) );
				tempresource.setPrevquantity( travelresourceform.getPrevquantity( tempindex[i] ) );
				
				tempresource.setSellablequantity( travelresourceform.getSellablequantity( tempindex[i] ) );
				tempresource.setStatus( travelresourceform.getStatus( tempindex[i] ) );
				
				
				temp.add( tempresource );
			}	
		}
		
		
		travelresource = ActivityLibrarydao.getTravelResource( travelresourceform.getNewtravelnamecombo() , getDataSource( request , "ilexnewDB" ) );
		
		travelresourceform.reset( mapping , request);
		
		travelresourceform.setNewtravelid( newtravelid );
		travelresourceform.setNewtravelname( newname );
		travelresourceform.setNewtravelnamecombo( newnamecombo );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setNewtraveltype( travelresource.getTraveltype() );
		travelresourceform.setNewcnspartnumber( travelresource.getCnspartnumber() );
		
		travelresourceform.setNewquantity( "0" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		
		travelresourceform.setNewestimatedunitcost( travelresource.getEstimatedunitcost() );
		travelresourceform.setNewsellablequantity( travelresource.getSellablequantity() );
		travelresourceform.setNewminimumquantity( travelresource.getMinimumquantity() );
		travelresourceform.setRef( null );
		travelresourceform.setActivity_Id( Activitylibrary_Id );
		travelresourceform.setTravellist( ActivityLibrarydao.getMateriallist( "travel" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );
		
		travelresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "T" , "%" , "" , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setActivity_Id( idname.getId() );
		travelresourceform.setActivity_name( idname.getName() );
		
		if( checklength > 0 )
		{
			if( resource_size > 0 )
			{
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < resource_size; j++ )
					{
						if( ( ( TravelResource )temp.get( i ) ).getTravelid().equals( ( ( TravelResource )travelresourcelist.get( j ) ).getTravelid() ) )
						{
							travelresourcelist.set( j ,  temp.get( i ) );
						}
					}
				}
				travelresourceform.setCheck( checkrefresh );
			}
			
		}
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "false" );
		return( mapping.findForward( "travelresourcetabularpage" ) );
	}
	

/** This method is used to add/update travel resource to an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		String Activitylibrary_Id = "";
		TravelResource travelresource = null;
		int resource_size = 0;
		ArrayList travelresourcelist = new ArrayList();
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String status = "";
		String activityname = "";
		int addflag = 0;
		NameId idname = null;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		/* by hamid for back button start*/
		if(request.getParameter("MSA_Id")!=null)
		{
			travelresourceform.setMSA_Id(request.getParameter("MSA_Id"));
		}
		/* by hamid for back button end*/
		Activitylibrary_Id = travelresourceform.getActivity_Id();
		
		if( travelresourceform.getCheck()!=  null )
		{
			int checklength = travelresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = travelresourceform.getTravelid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( travelresourceform.getCheck( i ).substring( 1 , travelresourceform.getCheck( i ).length() ).equals( travelresourceform.getTravelid( j ) ) )
						{	
							index[k] = j;
							k++;
						}
						/*if( travelresourceform.getCheck(i).equals( travelresourceform.getTravelid( j ) ) )
						{	
							index[k] = j;
							k++;
						}*/
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				int updateflag = 0;
				for( int i = 0; i < index.length; i++ )
				{
					travelresource = new TravelResource();
					travelresource.setTravelid( travelresourceform.getTravelid ( index[i] ) );
					travelresource.setTravelcostlibid( travelresourceform.getTravelcostlibid ( index[i] ) );
					travelresource.setActivity_Id( travelresourceform.getActivity_Id() );
					travelresource.setQuantity( travelresourceform.getQuantity ( index[i] ) );
					travelresource.setPrevquantity( travelresourceform.getPrevquantity( index[i] ) );
					travelresource.setTraveltype( "T" );
					travelresource.setEstimatedunitcost( travelresourceform.getEstimatedunitcost( index[i] ) );
					travelresource.setProformamargin( travelresourceform.getProformamargin( index[i] ) );
					
					/* for multiple resource select start*/
					travelresource.setCnspartnumber( travelresourceform.getCnspartnumber( index[i] ) );
					travelresource.setFlag( travelresourceform.getFlag( index[i] ) );
					if( travelresourceform.getFlag( index[i] ).equals( "P" ) )
					{
						updateflag = ActivityLibrarydao.updatetravelresource( travelresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "updateflag" , ""+updateflag );
					}
					if( travelresourceform.getFlag(index[i]).equals( "T" ) )
					{
						updateflag = ActivityLibrarydao.addtravelresource( travelresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "addflag" , ""+addflag );
					}
					/* for multiple resource select end*/
					/*updateflag = ActivityLibrarydao.updatetravelresource( travelresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );*/
				}	
			}
		}
		
		if( travelresourceform.getNewtravelid() != null )
		{
			
			travelresource = new TravelResource();
			travelresource.setTravelid( travelresourceform.getNewtravelnamecombo() );
			travelresource.setActivity_Id( travelresourceform.getActivity_Id() );
			travelresource.setCnspartnumber( travelresourceform.getNewcnspartnumber() );
			travelresource.setQuantity( travelresourceform.getNewquantity() );
			travelresource.setTraveltype( "T" );
			travelresource.setEstimatedunitcost( travelresourceform.getNewestimatedunitcost() );
			travelresource.setProformamargin( travelresourceform.getNewproformamargin() );
			
			addflag = ActivityLibrarydao.addtravelresource( travelresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		travelresourceform.reset( mapping , request );
		
		travelresourceform.setNewquantity( "0" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setActivity_Id( Activitylibrary_Id );
		
		travelresourceform.setRef( null );
		travelresourceform.setTravellist( ActivityLibrarydao.getMateriallist( "travel" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
				
		travelresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "T" , "%" , sortqueryclause , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setActivity_Id( idname.getId() );
		travelresourceform.setActivity_name( idname.getName() );
		
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "activityname" , activityname );
		return( mapping.findForward( "travelresourcetabularpage" ) );
	}
	


/** This method is used to delete travel resource from an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelresourceform = ( TravelResourceForm ) form;
		TravelResource travelresource = null;
		String Activitylibrary_Id = "";
		int resource_size = 0;
		int deleteflag = 0;
		ArrayList travelresourcelist = new ArrayList();
		String status = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String activityname = "";
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		NameId idname = null;
		
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			travelresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end*/
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			travelresourceform.setActivity_Id( Activitylibrary_Id );
		}
		else
		{
			Activitylibrary_Id = travelresourceform.getActivity_Id();
		}
		
		//Start :Added By Amit For Mass delete
		if( request.getParameter( "fromResourceTabular" ) != null )
		{
			if( travelresourceform.getCheck()!=  null )
			{
				
				int checklength = travelresourceform.getCheck().length;	 
		
				int []index = new int[checklength];
				int k = 0;
				
				if( checklength > 0 )
				{	int length = travelresourceform.getTravelid().length;
					for( int i = 0; i < checklength; i++ )
					{
						for( int j = 0; j < length; j++ )
						{
							if( travelresourceform.getCheck( i ).substring( 1 , travelresourceform.getCheck( i ).length() ).equals( travelresourceform.getTravelid( j ) ) )
							{	
								//System.out.println("value of ZZZZ materialresourceform.getCheck(i).substring(1,materialresourceform.getCheck(i).length())"+travelresourceform.getCheck(i).substring(1,travelresourceform.getCheck(i).length()));
								index[k] = j;
								k++;
							}
							
						}
					}
				}
			
				if( checklength > 0 )                               
				{
					
					for( int i = 0; i < index.length; i++ )
					{
						travelresource = new TravelResource();
						travelresource.setTravelid( travelresourceform.getTravelid ( index[i] ) );
						
						deleteflag = ActivityLibrarydao.deleteresource( travelresource.getTravelid() , getDataSource( request , "ilexnewDB" ) );
						
					}
				}
			}
		}
		else
		{
		//End : Added By Amit
		
		deleteflag = ActivityLibrarydao.deleteresource( request.getParameter( "Travel_Id" ) , getDataSource( request , "ilexnewDB" ) );
		}
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		travelresourceform.setNewquantity( "0" );
		travelresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelresourceform.setNewstatus( "Draft" );
		travelresourceform.setActivity_Id( Activitylibrary_Id );
		travelresourceform.setTravellist( ActivityLibrarydao.getMateriallist( "travel" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		travelresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "T" , "%" , sortqueryclause , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		if( travelresourcelist.size() > 0 )
		{
			resource_size = travelresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setMsa_id( idname.getId() );
		travelresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		travelresourceform.setActivity_Id( idname.getId() );
		travelresourceform.setActivity_name( idname.getName() );
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "travelresourcelist" , travelresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "true" );
		
		return( mapping.findForward( "travelresourcetabularpage" ) );
	}
	
	/**
	 * @author amitm
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward MassApprove( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		TravelResourceForm travelResourceForm = ( TravelResourceForm ) form;
		TravelResource travelresource = null;
		String Activitylibrary_Id = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		ArrayList materialresourcelist = new ArrayList();
		String status1  = null,status = null;
		String activityname = "";
		int resource_size = 0,changestatusflag = 0;
		String MSA_Id = ""; 
		String Appendix_Id = "";
		String Job_Id = "";
		int addflag = 0;
		NameId idname = null;
		if( request.getParameter( "Status" ) != null )
		{
			status1 = request.getParameter( "Status" );
		}
		
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		Activitylibrary_Id = travelResourceForm.getActivity_Id();
		
		if( travelResourceForm.getCheck()!=  null )
		{
			int checklength = travelResourceForm.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = travelResourceForm.getTravelid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( travelResourceForm.getCheck( i ).substring( 1 , travelResourceForm.getCheck( i ).length() ).equals( travelResourceForm.getTravelid( j ) ) )
						{	
							
							index[k] = j;
							k++;
						}
						
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				
				for( int i = 0; i < index.length; i++ )
				{
					travelresource = new TravelResource();
					travelresource.setTravelid( travelResourceForm.getTravelid ( index[i] ) );
					travelresource.setActivity_Id( travelResourceForm.getActivity_Id() );
					travelresource.setTraveltype( "T" );
					 changestatusflag = ChangeStatus.Status( MSA_Id , Appendix_Id , Job_Id , travelresource.getActivity_Id() , travelresource.getTravelid() , travelresource.getTraveltype() , status1 ,"N", getDataSource( request , "ilexnewDB" ),"" );
					
				}
			}
		}
		
	
		if( travelResourceForm.getNewtravelid() != null )
		{
			
				travelresource = new TravelResource();
				travelresource.setTravelid( travelResourceForm.getNewtravelnamecombo() );
				travelresource.setActivity_Id( travelResourceForm.getActivity_Id() );
				travelresource.setCnspartnumber( travelResourceForm.getNewcnspartnumber() );
				travelresource.setQuantity( travelResourceForm.getNewquantity() );
				travelresource.setTraveltype( "T" );
				travelresource.setEstimatedunitcost( travelResourceForm.getNewestimatedunitcost() );
				travelresource.setProformamargin( travelResourceForm.getNewproformamargin() );
				
				addflag = ActivityLibrarydao.addtravelresource( travelresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute( "addflag" , ""+addflag );
		}
		
		travelResourceForm.reset( mapping , request);
				
		travelResourceForm.setNewquantity( "0.0000" );
		travelResourceForm.setNewproformamargin( Activitydao.getDefaultproformamargin( "T" , getDataSource( request , "ilexnewDB" ) ) );
		travelResourceForm.setNewstatus( "Draft" );
		travelResourceForm.setActivity_Id( Activitylibrary_Id );
				
		travelResourceForm.setRef( null );
		travelResourceForm.setTravellist( ActivityLibrarydao.getMateriallist( "travel" , Activitylibrary_Id ,  getDataSource( request , "ilexnewDB" ) ) );
		
		materialresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "T" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
		
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		travelResourceForm.setMsa_id( idname.getId() );
		travelResourceForm.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		travelResourceForm.setActivity_Id( idname.getId() );
		travelResourceForm.setActivity_name( idname.getName() );
		
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );					
		
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "travelresourcelist" , materialresourcelist );
		request.setAttribute("changestatusflag",""+changestatusflag);
		
		return( mapping.findForward( "travelresourcetabularpage" ) );
	}
	
	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "amresT_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "amresT_sort" );
		return queryclause;
	}
}
