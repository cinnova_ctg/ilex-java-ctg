/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used for displaying labor resource detail     
*
*/

package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.LaborResource;
import com.mind.formbean.AM.LaborResourceDetailForm;

/*
 * Methods : execute
 */


public class LaborResourceDetailAction extends com.mind.common.IlexAction
{

/** This method populate LaborResourceDetailform formbean with the labor detail of an activity whose data is to be shown
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	
	public ActionForward execute( ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response ) throws Exception 
	{
		LaborResourceDetailForm laborresourcedetailform = ( LaborResourceDetailForm ) form;
		LaborResource laborresource = new LaborResource();
		String Labor_Id = ""; 
		ArrayList laborresourcelist = new ArrayList();
		String list = "";
		
		int sow_size = 0;
		int assumption_size = 0;
		
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		
		if( request.getParameter( "Labor_Id" ) != null )
		{
			Labor_Id = request.getParameter( "Labor_Id" );
		}
		
		if( request.getAttribute( "Labor_Id" ) != null )
		{
			Labor_Id = ( String ) request.getAttribute( "Labor_Id" );
		}
		
		
		laborresourcedetailform.setLaborid( Labor_Id );
		request.setAttribute( "Labor_Id" , laborresourcedetailform.getLaborid() );
		
		laborresourcelist = ActivityLibrarydao.getMaterialResourcelist( "%" , "L" , laborresourcedetailform.getLaborid() , "" , request.getSession().getId(),"detailpage",getDataSource( request , "ilexnewDB" ) ); 
		
		laborresource = ( LaborResource ) laborresourcelist.get( 0 );
		
		laborresourcedetailform.setActivity_Id( laborresource.getActivity_Id() );
		request.setAttribute( "Activitylibrary_Id" , laborresourcedetailform.getActivity_Id() );  
		
		laborresourcedetailform.setLaborid( laborresource.getLaborid() );
		laborresourcedetailform.setLaborname( laborresource.getLaborname() );
		laborresourcedetailform.setLabortype( laborresource.getLabortype() );
		laborresourcedetailform.setMinimumquantity( laborresource.getMinimumquantity() );
		laborresourcedetailform.setCnspartnumber( laborresource.getCnspartnumber() );
		laborresourcedetailform.setEstimatedtotalcost( laborresource.getEstimatedtotalcost() );
		laborresourcedetailform.setEstimatedhourlybasecost( laborresource.getEstimatedhourlybasecost() );
		
		laborresourcedetailform.setPriceextended( laborresource.getPriceextended() );
		laborresourcedetailform.setPriceunit( laborresource.getPriceunit() );
		laborresourcedetailform.setProformamargin( laborresource.getProformamargin() );
		laborresourcedetailform.setQuantityhours( laborresource.getQuantityhours() );
		laborresourcedetailform.setSellablequantity( laborresource.getSellablequantity() );
		laborresourcedetailform.setStatus( laborresource.getStatus() );
		
		laborresourcedetailform.setActivity_Id( laborresource.getActivity_Id() );
		laborresourcedetailform.setActivityname( laborresource.getActivity_name() );
		laborresourcedetailform.setMsa_id( laborresource.getMsa_Id() );
		laborresourcedetailform.setMsaname( laborresource.getMsaname() );
		
		list = Menu.getStatus( "L" , laborresourcedetailform.getStatus().charAt( 0 ) , "" , "" , "" , laborresourcedetailform.getLaborid() ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "list" , list );
		
		
		ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , laborresourcedetailform.getLaborid() , "L" );
		
		if( sowlist.size() > 0 )
		{
			sow_size = sowlist.size();	
		}
		
		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , laborresourcedetailform.getLaborid() , "L" );
		
		if( assumptionlist.size() > 0 )
		{
			assumption_size = assumptionlist.size();	
		}
		
		request.setAttribute( "sowsize" , ""+sow_size );
		request.setAttribute( "assumptionsize" , ""+assumption_size );
		
		
		request.setAttribute( "sowlist" , sowlist );
		request.setAttribute( "assumptionlist" , assumptionlist );
		
		
		return mapping.findForward( "laborresourcedetailpage" );
	}
}
