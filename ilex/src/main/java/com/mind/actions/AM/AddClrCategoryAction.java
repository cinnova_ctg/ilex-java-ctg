package com.mind.actions.AM;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.AddClrCategoryBean;
import com.mind.bean.DynamicComboAM;
import com.mind.common.dao.DynamicComboDaoAM;
import com.mind.dao.AM.AddClrCategorydao;
import com.mind.formbean.AM.AddClrCategoryForm;

public class AddClrCategoryAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward add(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddClrCategoryForm addClrCategoryForm = (AddClrCategoryForm) form;
		DynamicComboDaoAM dcomboAM=new DynamicComboDaoAM();
		DynamicComboAM dcam = new DynamicComboAM();
		ArrayList categorylist= new ArrayList();
		ArrayList criticalitylist= new ArrayList();
		int statusvalue=-1;
		String catdesc[]=new String[2];
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		/*if( addClrCategoryForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Manufacturer" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}*/
		/* Page Security End*/
		
		if(request.getParameter("ref")!=null)
		{
			addClrCategoryForm.setRef(request.getParameter("ref"));
		}
		
		categorylist=dcomboAM.getCategoryNameCombo(getDataSource( request,"ilexnewDB" ));
		dcam .setShortcatlist(categorylist);
	
		criticalitylist = AddClrCategorydao.getCritServicetimelist(addClrCategoryForm.getClrexistingcategory(),getDataSource( request,"ilexnewDB" ));
		
		if( addClrCategoryForm.getSave()!= null )
		{
			String servicetime="";
			String criticalityid="";
			if(criticalitylist.size()>0)
			{
				for(int i=0;i<criticalitylist.size();i++)
				{
					//servicetime=servicetime+ addClrCategoryForm.getClrservicetime(i)+",";
					servicetime=servicetime+ addClrCategoryForm.getClrservicetime(i)+",";
					criticalityid=criticalityid+ ((AddClrCategoryBean)criticalitylist.get(i)).getCriticalityid()+",";
				}
			}
			
			if(addClrCategoryForm.getClrexistingcategory().equals("0"))
			{
			
				if(addClrCategoryForm.getClrcategoryshortname()!=null && !(addClrCategoryForm.getClrcategoryshortname().equals("")))
				{
						statusvalue=AddClrCategorydao.addCategory(addClrCategoryForm.getClrcategory(),addClrCategoryForm.getClrcategoryshortname(), servicetime, criticalityid, getDataSource( request,"ilexnewDB" ));
						 
						addClrCategoryForm.setAddmessage(""+statusvalue);
						criticalitylist = AddClrCategorydao.getCritServicetimelist(addClrCategoryForm.getClrcategoryshortname(),getDataSource( request,"ilexnewDB" ));
				}
			}
			else
			{
				statusvalue=AddClrCategorydao.addCategory(addClrCategoryForm.getClrcategory(),addClrCategoryForm.getClrexistingcategory(),servicetime,criticalityid,getDataSource( request,"ilexnewDB" ));
				 
				addClrCategoryForm.setUpdatemessage(""+statusvalue);
				criticalitylist = AddClrCategorydao.getCritServicetimelist(addClrCategoryForm.getClrexistingcategory(),getDataSource( request,"ilexnewDB" ));
				
			}
			
			categorylist=dcomboAM.getCategoryNameCombo(getDataSource( request,"ilexnewDB" ));
			dcam .setShortcatlist(categorylist);
			
		}
		
		request.setAttribute( "dynamiccategorycombo" , dcam );
		request.setAttribute( "critservicelist" , criticalitylist );
		forward = mapping.findForward("success");
			
		return (forward);

	}
	
	
	public ActionForward delete(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		Collection subcatglist;
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddClrCategoryForm addClrCategoryForm = (AddClrCategoryForm) form;
		DynamicComboDaoAM dcomboAM=new DynamicComboDaoAM();
		DynamicComboAM dcam = new DynamicComboAM();
		ArrayList categorylist= new ArrayList();
		ArrayList criticalitylist= new ArrayList();
		int statusvalue=-1;
		
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		/*if( manAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Manufacturer" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}*/
		/* Page Security End*/
		
		if(request.getParameter("catshortname")!=null)
		{
			statusvalue=AddClrCategorydao.deleteCategory(request.getParameter("catshortname"),getDataSource( request,"ilexnewDB" ));
			addClrCategoryForm.setDeletemessage(""+statusvalue);
			if(statusvalue==0)
			{
				
			}
		}
		
		categorylist=dcomboAM.getCategoryNameCombo(getDataSource( request,"ilexnewDB" ));
		dcam .setShortcatlist(categorylist);
		
		criticalitylist = AddClrCategorydao.getCritServicetimelist("0",getDataSource( request,"ilexnewDB" ));
		
		request.setAttribute( "dynamiccategorycombo" , dcam );
		request.setAttribute( "critservicelist" , criticalitylist );
		addClrCategoryForm.setRef("add");			
		forward = mapping.findForward("success");
					
		return (forward);

	
	}
	
	

}
