package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Appendixdao;

public class ActivitySummaryAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		String  MSA_Id = "";
		String msaname = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String checkformastercopy = "";
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Activity" , "Summary" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		if( request.getParameter( "MSA_Id" ) != null )
		{
			MSA_Id = request.getParameter( "MSA_Id" );
			request.setAttribute( "MSA_Id" , MSA_Id );
		}
		
		msaname = Appendixdao.getMsaname( getDataSource( request , "ilexnewDB" ) , MSA_Id );
		checkformastercopy = ActivityLibrarydao.Checkformasterlibcopy( MSA_Id , getDataSource( request , "ilexnewDB" ) );
		
		ArrayList msaactivitylist =  ActivityLibrarydao.getActivitylibrary( getDataSource( request , "ilexnewDB" ) , MSA_Id , "%" , "order by lm_al_cat_name" );
		
		if( msaactivitylist.size() > 0 )
			request.setAttribute( "msaactivitylist" , msaactivitylist );
		
		request.setAttribute( "checkformastercopy" , checkformastercopy );
		request.setAttribute( "msaname" , msaname ); 
		return mapping.findForward( "activitysummarypage" );
	}
}
