package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.am.ActivityLibrary;
import com.mind.common.LabelValue;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.formbean.AM.MastertolibcopyForm;


public class MastertolibcopyAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward mastertolibcopy( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		MastertolibcopyForm mastertolibcopyform = ( MastertolibcopyForm ) form;
		String MSA_Id = "";
		int masterlib_size = 0;
		ArrayList masterlibactivitylist = new ArrayList();
		String msaname = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Activity" , "View" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		if( request.getParameter( "MSA_Id" ) != null )
		{
			MSA_Id = request.getParameter( "MSA_Id" );
		}
		
		if(request.getParameter("selectedMSAId") != null) {
			mastertolibcopyform.setSelectedMSAId(request.getParameter("selectedMSAId"));
		} else {
			mastertolibcopyform.setSelectedMSAId("0");
		}
		
		//System.out.println("---MSA_Id----"+MSA_Id);
		//System.out.println("---selectedMSAId----"+mastertolibcopyform.getSelectedMSAId());
		
		
		//masterlibactivitylist = ActivityLibrarydao.getMasterlibactivities( MSA_Id , getDataSource( request , "ilexnewDB" ) );
		masterlibactivitylist =  ActivityLibrarydao.getActivitylibrary( getDataSource( request , "ilexnewDB" ) , mastertolibcopyform.getSelectedMSAId() , "%" , "order by lm_al_cat_name, lm_at_title" );
		
		if( masterlibactivitylist.size() > 0 )
		{
			masterlib_size = masterlibactivitylist.size();
			request.setAttribute( "result" , "result" );
		}
		
		mastertolibcopyform.setMsa_Id( MSA_Id );
		msaname = Appendixdao.getMsaname( getDataSource( request , "ilexnewDB" ) , MSA_Id );
		
		ArrayList<LabelValue> msaList = new ArrayList<LabelValue>();
		msaList = ActivityLibrarydao.getActivityCopyMsaList(getDataSource(request, "ilexnewDB"), MSA_Id);
		mastertolibcopyform.setMsaList(msaList);
		String checkformastercopy = ActivityLibrarydao.Checkformasterlibcopy( MSA_Id , getDataSource( request , "ilexnewDB" ) );
		
		request.setAttribute( "Size" , new Integer ( masterlib_size ) );
		request.setAttribute( "masterliblist" , masterlibactivitylist );
		request.setAttribute( "msaname" , msaname );
		request.setAttribute("checkformastercopy", checkformastercopy);
		
		return mapping.findForward( "mastertolibcopypage" );
	}
	
	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		MastertolibcopyForm mastertolibcopyform = ( MastertolibcopyForm ) form;
		ActivityLibrary activity = null;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		int addflag = -1;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		int checklength = 0;
		if( mastertolibcopyform.getCheck()!=  null )
		{
			 checklength = mastertolibcopyform.getCheck().length;	 
		}
		
		
		int []index = new int[checklength];
		int k = 0;
		
		if( checklength > 0 )
		{
			int length = mastertolibcopyform.getActivity_Id().length;
			for( int i = 0; i < checklength; i++ )
			{
				for( int j = 0; j < length; j++ )
				{
					if( mastertolibcopyform.getCheck( i ).equals( mastertolibcopyform.getActivity_Id( j ) ) )
					{	
						index[k] = j;
						k++;
					}
				}
			}
		}
			
		if( checklength > 0 )                              
		{
		
			for( int i = 0; i < index.length; i++ )
			{
				activity = new ActivityLibrary();
				
				activity.setActivity_Id( mastertolibcopyform.getActivity_Id( index[i] ) );
				
				activity.setName( mastertolibcopyform.getName( index[i] ) );
			
				activity.setMsa_Id(  mastertolibcopyform.getMsa_Id() );
				
				activity.setCat_Id( mastertolibcopyform.getCat_Id( index[i] ) );
				
				activity.setActivitytypecombo( mastertolibcopyform.getActivitytypecombo( index[i] ) );
				
				activity.setStatus( "D" );
				activity.setDuration( mastertolibcopyform.getDuration( index[i] ) );
				
				activity.setQuantity( mastertolibcopyform.getQuantity( index[i] ) );
				
				addflag = ActivityLibrarydao.addMastertoactivitylib( activity , loginuserid , getDataSource( request,"ilexnewDB" ) );
				request.setAttribute( "addflag" , ""+addflag );
			}
		}
		
		request.setAttribute( "MSA_Id" , mastertolibcopyform.getMsa_Id() );
		return mapping.findForward( "activitymanagertabularpage" );
	}
	
	public ActionForward showAllMSA(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		MastertolibcopyForm masterLibCopyForm = (MastertolibcopyForm) form;
		String MSA_Id = "";
		String msaname = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Activity" , "View" , getDataSource( request , "ilexnewDB")))	{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		if( request.getParameter( "MSA_Id" ) != null )	{
			MSA_Id = request.getParameter( "MSA_Id" );
		}
		
		masterLibCopyForm.setMsa_Id( MSA_Id );
		msaname = Appendixdao.getMsaname( getDataSource( request , "ilexnewDB" ) , MSA_Id );
		request.setAttribute( "msaname" , msaname );
		
		
		ArrayList<LabelValue> msaList = new ArrayList<LabelValue>();
		msaList = ActivityLibrarydao.getActivityCopyMsaList(getDataSource(request, "ilexnewDB"), MSA_Id);
		masterLibCopyForm.setMsaList(msaList);
		String checkformastercopy = ActivityLibrarydao.Checkformasterlibcopy( MSA_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute("checkformastercopy", checkformastercopy);
		return mapping.findForward("mastertolibcopypage");
	}
}
