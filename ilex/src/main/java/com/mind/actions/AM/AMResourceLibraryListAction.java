package com.mind.actions.AM;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.FreightResource;
import com.mind.dao.AM.LaborResource;
import com.mind.dao.AM.MaterialResource;
import com.mind.dao.AM.ResourceMaterial;
import com.mind.dao.AM.TravelResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.formbean.AM.ResourceListForm;

public class AMResourceLibraryListAction extends
		com.mind.common.IlexDispatchAction {

	/**
	 * This method is used to list all the material resources used in an
	 * activity
	 * 
	 * @param mapping
	 *            object of ActionMapping
	 * @param form
	 *            object of ActionForm
	 * @param request
	 *            object of HttpServletRequest
	 * @param response
	 *            object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// AMResourceListLibraryForm amResourceListLibraryForm =
		// (AMResourceListLibraryForm) form;
		ResourceListForm amResourceListLibraryForm = (ResourceListForm) form;

		ArrayList materialresourcelist = new ArrayList();
		ResourceMaterial resourcematerial = null;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String sortqueryclause = "";
		String Activitylibrary_Id = "";
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		/* multiple resouce select start */
		if (request.getAttribute("tempdeleteflag") != null) {
		} else {
			ActivityLibrarydao.deleteTempResources(
					request.getSession().getId(),
					getDataSource(request, "ilexnewDB"));
		}

		if (request.getParameter("MSA_Id") != null) {
			amResourceListLibraryForm.setMsaId(request.getParameter("MSA_Id"));
		}
		if (request.getParameter("Activitylibrary_Id") != null) {
			Activitylibrary_Id = request.getParameter("Activitylibrary_Id");
			amResourceListLibraryForm.setActivity_Id(Activitylibrary_Id);
		} else {
			Activitylibrary_Id = amResourceListLibraryForm.getActivity_Id();
		}
		sortqueryclause = getSortQueryClause(sortqueryclause, session);

		if (request.getParameter("Activitylibrary_Id") != null) {
			Activitylibrary_Id = request.getParameter("Activitylibrary_Id");
			amResourceListLibraryForm.setActivity_Id(Activitylibrary_Id);
		} else {
			Activitylibrary_Id = amResourceListLibraryForm.getActivity_Id();
		}
		if (amResourceListLibraryForm.getEdit() != null
				|| request.getAttribute("edit") != null
				|| request.getParameter("editable") != null) {
			amResourceListLibraryForm.setEdit("editable");
		}

		materialresourcelist = ActivityLibrarydao.getAMResourceLibList(
				Activitylibrary_Id, "M", "%", sortqueryclause, request
						.getSession().getId(), "listpage",
				getDataSource(request, "ilexnewDB"));
		int resource_size = 0;
		if (materialresourcelist.size() > 0) {
			resource_size = materialresourcelist.size();
		}
		request.setAttribute("material_Size", resource_size);

		ArrayList<TravelResource> travelresourcelist = ActivityLibrarydao
				.getAMResourceLibList(Activitylibrary_Id, "T", "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
		resource_size = 0;
		if (travelresourcelist.size() > 0) {
			resource_size = travelresourcelist.size();
		}
		request.setAttribute("travel_Size", resource_size);

		ArrayList<LaborResource> laborresourcelist = ActivityLibrarydao
				.getAMResourceLibList(Activitylibrary_Id, "L", "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
		resource_size = 0;
		if (laborresourcelist.size() > 0) {
			resource_size = laborresourcelist.size();
		}
		request.setAttribute("labor_Size", resource_size);

		ArrayList<FreightResource> freightresourcelist = ActivityLibrarydao
				.getAMResourceLibList(Activitylibrary_Id, "F", "%",
						sortqueryclause, request.getSession().getId(),
						"listpage", getDataSource(request, "ilexnewDB"));
		resource_size = 0;
		if (freightresourcelist.size() > 0) {
			resource_size = freightresourcelist.size();
		}

		Long creationDate = ActivityLibrarydao.getActivityCreationDate(
				Activitylibrary_Id, getDataSource(request, "ilexnewDB"));

		request.setAttribute("creationDate", creationDate);

		request.setAttribute("freight_Size", resource_size);

		request.setAttribute("materialresourcelist", materialresourcelist);
		request.setAttribute("travelresourcelist", travelresourcelist);
		request.setAttribute("laborresourcelist", laborresourcelist);
		request.setAttribute("freightresourcelist", freightresourcelist);

		NameId idname = Activitydao.getIdName(Activitylibrary_Id, "Library",
				"MSA", getDataSource(request, "ilexnewDB"));
		amResourceListLibraryForm.setMsaId(idname.getId());
		amResourceListLibraryForm.setMsaName(idname.getName());

		idname = Activitydao.getIdName(Activitylibrary_Id, "Library",
				"Activity", getDataSource(request, "ilexnewDB"));
		amResourceListLibraryForm.setActivity_Id(idname.getId());
		amResourceListLibraryForm.setActivityName(idname.getName());

		String status = ActivityLibrarydao.getActivitystatus(
				Activitylibrary_Id, getDataSource(request, "ilexnewDB"));

		request.setAttribute("act_status", status);
		request.setAttribute("Activity_Id", Activitylibrary_Id);

		request.setAttribute("refresh", "true");
		request.setAttribute("activityname",
				amResourceListLibraryForm.getActivityName());

		return mapping.findForward("amResourceLibraryListPage");

	}

	public String getSortQueryClause(String queryclause, HttpSession session) {
		if (session.getAttribute("amresM_sort") != null)
			queryclause = (String) session.getAttribute("amresM_sort");
		return queryclause;
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ResourceListForm amResourceListLibraryForm = (ResourceListForm) form;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String sortqueryclause = "";

		sortqueryclause = getSortQueryClause(sortqueryclause, session);

		String Activitylibrary_Id = "";

		if (request.getParameter("Activitylibrary_Id") != null) {
			Activitylibrary_Id = request.getParameter("Activitylibrary_Id");
			amResourceListLibraryForm.setActivity_Id(Activitylibrary_Id);
		} else {
			Activitylibrary_Id = amResourceListLibraryForm.getActivity_Id();
		}

		/* multiple resouce select start */
		if (request.getAttribute("tempdeleteflag") != null) {
		} else {
			ActivityLibrarydao.deleteTempResources(
					request.getSession().getId(),
					getDataSource(request, "ilexnewDB"));
		}

		if (amResourceListLibraryForm.getMaterialCheck() != null) {

			int checklength = amResourceListLibraryForm.getMaterialCheck().length;

			int[] index = new int[checklength];
			int k = 0;

			if (checklength > 0) {
				int length = amResourceListLibraryForm.getMaterialid().length;
				for (int i = 0; i < checklength; i++) {
					for (int j = 0; j < length; j++) {
						if (amResourceListLibraryForm
								.getMaterialCheck(i)
								.substring(
										1,
										amResourceListLibraryForm
												.getMaterialCheck(i).length())
								.equals(amResourceListLibraryForm
										.getMaterialid(j))) {
							index[k] = j;
							k++;
						}
					}
				}
			}

			if (checklength > 0) {
				int updateflag = 0;
				int deleteflag = 0;
				MaterialResource materialresource = null;
				for (int i = 0; i < index.length; i++) {
					materialresource = new MaterialResource();
					materialresource.setMaterialid(amResourceListLibraryForm
							.getMaterialid(index[i]));
					materialresource
							.setMaterialcostlibid(amResourceListLibraryForm
									.getMaterialCostlibid(index[i]));
					materialresource.setActivity_Id(amResourceListLibraryForm
							.getActivity_Id());
					materialresource.setQuantity(amResourceListLibraryForm
							.getMaterialQuantity(index[i]));
					materialresource.setPrevquantity(amResourceListLibraryForm
							.getMaterialPrevquantity(index[i]));
					materialresource.setMaterialtype("M");
					materialresource
							.setEstimatedunitcost(amResourceListLibraryForm
									.getMaterialEstimatedunitcost(index[i]));
					materialresource
							.setProformamargin(amResourceListLibraryForm
									.getMaterialProformamargin(index[i]));
					/* for multiple resource select start */
					materialresource.setCnspartnumber(amResourceListLibraryForm
							.getMaterialCnspartnumber(index[i]));
					materialresource.setFlag(amResourceListLibraryForm
							.getMaterialFlag(index[i]));
					if (amResourceListLibraryForm.getMaterialFlag(index[i])
							.equals("P")
							&& amResourceListLibraryForm.getDelete() == null) {
						updateflag = ActivityLibrarydao.updateMaterialResource(
								materialresource, loginuserid,
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("updateresourceflag", ""
								+ updateflag);
					} else if (amResourceListLibraryForm.getDelete() != null) {
						deleteflag = ActivityLibrarydao.deleteresource(
								materialresource.getMaterialid(),
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("deleteresourceflag", ""
								+ deleteflag);
					}
				}
			}

		}

		if (amResourceListLibraryForm.getLaborCheck() != null) {
			int checklength = amResourceListLibraryForm.getLaborCheck().length;

			int[] index = new int[checklength];
			int k = 0;

			if (checklength > 0) {
				int length = amResourceListLibraryForm.getLaborid().length;
				for (int i = 0; i < checklength; i++) {
					for (int j = 0; j < length; j++) {
						if (amResourceListLibraryForm
								.getLaborCheck(i)
								.substring(
										1,
										amResourceListLibraryForm
												.getLaborCheck(i).length())
								.equals(amResourceListLibraryForm.getLaborid(j))) {

							index[k] = j;
							k++;
						}
					}
				}
			}

			if (checklength > 0) {
				LaborResource laborresource = null;
				int updateflag = 0;
				int deleteflag = 0;
				for (int i = 0; i < index.length; i++) {
					laborresource = new LaborResource();
					laborresource.setLaborid(amResourceListLibraryForm
							.getLaborid(index[i]));
					laborresource.setLaborcostlibid(amResourceListLibraryForm
							.getLaborcostlibid(index[i]));
					laborresource.setActivity_Id(amResourceListLibraryForm
							.getActivity_Id());
					laborresource.setQuantityhours(amResourceListLibraryForm
							.getLaborQuantityhours(index[i]));
					laborresource
							.setPrevquantityhours(amResourceListLibraryForm
									.getLaborPrevquantityhours(index[i]));
					laborresource.setLabortype("L");
					laborresource
							.setEstimatedhourlybasecost(amResourceListLibraryForm
									.getLaborEstimatedhourlybasecost(index[i]));
					laborresource.setProformamargin(amResourceListLibraryForm
							.getLaborProformamargin(index[i]));

					/* for multiple resource select start */
					laborresource.setCnspartnumber(amResourceListLibraryForm
							.getLaborCnspartnumber(index[i]));
					laborresource.setFlag(amResourceListLibraryForm
							.getLaborFlag(index[i]));
					if (amResourceListLibraryForm.getLaborFlag(index[i])
							.equals("P")
							&& amResourceListLibraryForm.getDelete() == null) {
						updateflag = ActivityLibrarydao.updatelaborresource(
								laborresource, loginuserid,
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("updateresourceflag", ""
								+ updateflag);
					} else if (amResourceListLibraryForm.getDelete() != null) {
						deleteflag = ActivityLibrarydao.deleteresource(
								laborresource.getLaborid(),
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("deleteresourceflag", ""
								+ deleteflag);
					}
				}
			}
		}

		if (amResourceListLibraryForm.getTravelCheck() != null) {

			int checklength = amResourceListLibraryForm.getTravelCheck().length;

			int[] index = new int[checklength];
			int k = 0;

			if (checklength > 0) {
				int length = amResourceListLibraryForm.getTravelid().length;
				for (int i = 0; i < checklength; i++) {
					for (int j = 0; j < length; j++) {
						if (amResourceListLibraryForm
								.getTravelCheck(i)
								.substring(
										1,
										amResourceListLibraryForm
												.getTravelCheck(i).length())
								.equals(amResourceListLibraryForm
										.getTravelid(j))) {
							index[k] = j;
							k++;
						}
					}
				}
			}

			if (checklength > 0) {
				TravelResource travelresource = null;
				int updateflag = 0;
				int deleteflag = 0;
				for (int i = 0; i < index.length; i++) {
					travelresource = new TravelResource();
					travelresource.setTravelid(amResourceListLibraryForm
							.getTravelid(index[i]));
					travelresource.setTravelcostlibid(amResourceListLibraryForm
							.getTravelcostlibid(index[i]));
					travelresource.setActivity_Id(amResourceListLibraryForm
							.getActivity_Id());
					travelresource.setQuantity(amResourceListLibraryForm
							.getTravelQuantity(index[i]));
					travelresource.setPrevquantity(amResourceListLibraryForm
							.getTravelPrevquantity(index[i]));
					travelresource.setTraveltype("T");
					travelresource
							.setEstimatedunitcost(amResourceListLibraryForm
									.getTravelEstimatedunitcost(index[i]));
					travelresource.setProformamargin(amResourceListLibraryForm
							.getTravelProformamargin(index[i]));

					/* for multiple resource select start */
					travelresource.setCnspartnumber(amResourceListLibraryForm
							.getTravelCnspartnumber(index[i]));
					travelresource.setFlag(amResourceListLibraryForm
							.getTravelFlag(index[i]));
					if (amResourceListLibraryForm.getTravelFlag(index[i])
							.equals("P")
							&& amResourceListLibraryForm.getDelete() == null) {
						updateflag = ActivityLibrarydao.updatetravelresource(
								travelresource, loginuserid,
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("updateresourceflag", ""
								+ updateflag);
					} else if (amResourceListLibraryForm.getDelete() != null) {
						deleteflag = ActivityLibrarydao.deleteresource(
								travelresource.getTravelid(),
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("deleteresourceflag", ""
								+ deleteflag);
					}

				}
			}

		}

		if (amResourceListLibraryForm.getFreightCheck() != null) {

			int checklength = amResourceListLibraryForm.getFreightCheck().length;

			int[] index = new int[checklength];
			int k = 0;

			if (checklength > 0) {
				int length = amResourceListLibraryForm.getFreightid().length;
				for (int i = 0; i < checklength; i++) {
					for (int j = 0; j < length; j++) {

						if (amResourceListLibraryForm
								.getFreightCheck(i)
								.substring(
										1,
										amResourceListLibraryForm
												.getFreightCheck(i).length())
								.equals(amResourceListLibraryForm
										.getFreightid(j))) {

							index[k] = j;
							k++;
						}
					}
				}
			}

			if (checklength > 0) {
				FreightResource freightresource = null;
				int updateflag = 0;
				int deleteflag = 0;
				for (int i = 0; i < index.length; i++) {
					freightresource = new FreightResource();
					freightresource.setFreightid(amResourceListLibraryForm
							.getFreightid(index[i]));
					freightresource
							.setFreightcostlibid(amResourceListLibraryForm
									.getFreightcostlibid(index[i]));
					freightresource.setActivity_Id(amResourceListLibraryForm
							.getActivity_Id());
					freightresource.setQuantity(amResourceListLibraryForm
							.getFreightQuantity(index[i]));
					freightresource.setPrevquantity(amResourceListLibraryForm
							.getFreightPrevquantity(index[i]));
					freightresource.setFreighttype("F");
					freightresource
							.setEstimatedunitcost(amResourceListLibraryForm
									.getFreightEstimatedunitcost(index[i]));
					freightresource.setProformamargin(amResourceListLibraryForm
							.getFreightProformamargin(index[i]));
					/* for multiple resource select start */
					freightresource.setCnspartnumber(amResourceListLibraryForm
							.getFreightCnspartnumber(index[i]));
					freightresource.setFlag(amResourceListLibraryForm
							.getFreightFlag(index[i]));
					if (amResourceListLibraryForm.getFreightFlag(index[i])
							.equals("P")
							&& amResourceListLibraryForm.getDelete() == null) {
						updateflag = ActivityLibrarydao.updatefreightresource(
								freightresource, loginuserid,
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("updateresourceflag", ""
								+ updateflag);
					} else if (amResourceListLibraryForm.getDelete() != null) {
						deleteflag = ActivityLibrarydao.deleteresource(
								freightresource.getFreightid(),
								getDataSource(request, "ilexnewDB"));
						request.setAttribute("deleteresourceflag", ""
								+ deleteflag);
					}

				}
			}

		}
		return mapping.findForward("sucess");
	}

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ResourceListForm amResourceListLibraryForm = (ResourceListForm) form;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String sortqueryclause = "";
		String Activitylibrary_Id = "";
		if (request.getParameter("Activitylibrary_Id") != null) {
			Activitylibrary_Id = request.getParameter("Activitylibrary_Id");
			amResourceListLibraryForm.setActivity_Id(Activitylibrary_Id);
		} else {
			Activitylibrary_Id = amResourceListLibraryForm.getActivity_Id();
		}
		sortqueryclause = getSortQueryClause(sortqueryclause, session);

		if (request.getParameter("Activitylibrary_Id") != null) {
			Activitylibrary_Id = request.getParameter("Activitylibrary_Id");
			amResourceListLibraryForm.setActivity_Id(Activitylibrary_Id);
		} else {
			Activitylibrary_Id = amResourceListLibraryForm.getActivity_Id();
		}
		if (amResourceListLibraryForm.getEdit() != null
				|| request.getAttribute("edit") != null
				|| request.getParameter("editable") != null) {
			amResourceListLibraryForm.setEdit("editable");
		}

		sortqueryclause = getSortQueryClause(sortqueryclause, session);
		ArrayList materialresourcelist = new ArrayList();
		ArrayList laborresourcelist = new ArrayList();
		ArrayList freightresourcelist = new ArrayList();
		ArrayList travelresourcelist = new ArrayList();
		boolean checkresource = false;
		int addflag = 0;
		int material_resource_size = 0;
		if (amResourceListLibraryForm.getResourceListType().equals("M")
				|| amResourceListLibraryForm.getResourceListType().equals("A")) {
			materialresourcelist = ActivityLibrarydao.getMaterialResourcelist(
					amResourceListLibraryForm.getActivity_Id(), "M", "%",
					sortqueryclause, request.getSession().getId(), "tempList",
					getDataSource(request, "ilexnewDB"));
			Iterator iteratorMaterial = materialresourcelist.iterator();
			while (iteratorMaterial.hasNext()) {
				MaterialResource resourceMaterial = (MaterialResource) iteratorMaterial
						.next();
				addflag = ActivityLibrarydao.addMaterialResource(
						resourceMaterial,
						amResourceListLibraryForm.getActivity_Id(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("addresourceflag", "" + addflag);
				checkresource = true;
			}
		}
		if (amResourceListLibraryForm.getResourceListType().equals("L")
				|| amResourceListLibraryForm.getResourceListType().equals("A")) {
			materialresourcelist = ActivityLibrarydao.getMaterialResourcelist(
					amResourceListLibraryForm.getActivity_Id(), "L", "%",
					sortqueryclause, request.getSession().getId(), "tempList",
					getDataSource(request, "ilexnewDB"));
			Iterator iteratorMaterial = materialresourcelist.iterator();
			while (iteratorMaterial.hasNext()) {
				LaborResource resourceLabor = (LaborResource) iteratorMaterial
						.next();
				addflag = ActivityLibrarydao.addLaborresource(resourceLabor,
						amResourceListLibraryForm.getActivity_Id(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("addresourceflag", "" + addflag);
				checkresource = true;
			}
		}
		if (amResourceListLibraryForm.getResourceListType().equals("T")
				|| amResourceListLibraryForm.getResourceListType().equals("A")) {
			materialresourcelist = ActivityLibrarydao.getMaterialResourcelist(
					amResourceListLibraryForm.getActivity_Id(), "T", "%",
					sortqueryclause, request.getSession().getId(), "tempList",
					getDataSource(request, "ilexnewDB"));
			Iterator iteratorMaterial = materialresourcelist.iterator();
			while (iteratorMaterial.hasNext()) {
				TravelResource resourceTravel = (TravelResource) iteratorMaterial
						.next();
				addflag = ActivityLibrarydao.addtravelresource(resourceTravel,
						amResourceListLibraryForm.getActivity_Id(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("addresourceflag", "" + addflag);
				checkresource = true;
			}
		}
		if (amResourceListLibraryForm.getResourceListType().equals("F")
				|| amResourceListLibraryForm.getResourceListType().equals("A")) {
			materialresourcelist = ActivityLibrarydao.getMaterialResourcelist(
					amResourceListLibraryForm.getActivity_Id(), "F", "%",
					sortqueryclause, request.getSession().getId(), "tempList",
					getDataSource(request, "ilexnewDB"));
			Iterator iteratorMaterial = materialresourcelist.iterator();
			while (iteratorMaterial.hasNext()) {
				FreightResource resourceFreight = (FreightResource) iteratorMaterial
						.next();
				addflag = ActivityLibrarydao.addfreightresource(
						resourceFreight,
						amResourceListLibraryForm.getActivity_Id(),
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("addresourceflag", "" + addflag);
				checkresource = true;
			}
		}
		if (checkresource == true) {
			ActivityLibrarydao.deleteTempResources(
					request.getSession().getId(),
					getDataSource(request, "ilexnewDB"));
		}
		// materialresourcelist = Resourcedao.getResourcelist(
		// amResourceListLibraryForm.getActivity_Id() , "M" , "%" ,
		// sortqueryclause , request.getSession().getId(), "listpage",
		// getDataSource( request , "ilexnewDB" ) );

		return mapping.findForward("sucess");
	}
}
