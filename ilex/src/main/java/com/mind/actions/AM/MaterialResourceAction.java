/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used to manage material resource of an activity     
*
*/

package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.MaterialResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.formbean.AM.MaterialResourceForm;

public class MaterialResourceAction extends com.mind.common.IlexDispatchAction
{
	
/** This method is used to list all the material resources used in an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		String Activitylibrary_Id = "";
		int resource_size = 0;
		ArrayList materialresourcelist = new ArrayList();
		String sortqueryclause = "";
		String status = "";
		String activityname = "";
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			materialresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		
		/* by hamid for back button end
		
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			materialresourceform.setActivity_Id( Activitylibrary_Id );
		}
		else
		{
			Activitylibrary_Id = materialresourceform.getActivity_Id();
		}
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setActivity_Id( Activitylibrary_Id );
		materialresourceform.setMateriallist( ActivityLibrarydao.getMateriallist( "material" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		materialresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "M" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
			
		}
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setActivity_Id( idname.getId() );
		materialresourceform.setActivity_name( idname.getName() );
		
		/* for checkbox check start */
		String checkTempResources[] = ActivityLibrarydao.checkTempResourcesAM( Activitylibrary_Id , "M" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setCheck(checkTempResources);
		/* for checkbox check end */
		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request,"ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ) );
		
		request.setAttribute("act_status",status );
		request.setAttribute("Activity_Id",Activitylibrary_Id );
		
		request.setAttribute("refresh","true" );
		request.setAttribute("activityname", activityname );
		
		return( mapping.findForward( "materialresourcetabularpage" ) );
	}
	

/** This method is used when the page is refreshed to populate form according to the value selected from the combo 
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
	public ActionForward Refresh( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		String Activitylibrary_Id = "";
		String newname = "";
		String newnamecombo = "";
		String newmaterialid = "";
		int resource_size = 0;
		MaterialResource materialresource = new MaterialResource();
		ArrayList materialresourcelist = new ArrayList();
		Activitylibrary_Id = materialresourceform.getActivity_Id();
		int checklength = 0;
		ArrayList temp = new ArrayList();
		String[] checkrefresh = null;
		String status = "";
		String activityname = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		NameId idname = null;
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/* by hamid for back button start*/
		if(request.getParameter("MSA_Id")!=null)
		{
			materialresourceform.setMSA_Id(request.getParameter("MSA_Id"));
		}
		/* by hamid for back button end*/
		
		newname = materialresourceform.getNewmaterialname();
		newnamecombo = materialresourceform.getNewmaterialnamecombo();
		newmaterialid = materialresourceform.getNewmaterialid();
		
		if( materialresourceform.getCheck()!=  null )
		{
			checklength = materialresourceform.getCheck().length;	 
			checkrefresh = materialresourceform.getCheck();
			
			int []tempindex = new int[checklength];
			int k = 0;
				
			if( checklength > 0 )
			{	int length = materialresourceform.getMaterialid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( materialresourceform.getCheck( i ).equals( materialresourceform.getMaterialid( j ) ) )
						{	
							tempindex[k] = j;
							k++;
						}
					}
				}
			}	
			
			for( int i = 0; i < checklength; i++ )
			{
				MaterialResource tempresource = new MaterialResource();
				
				tempresource.setActivity_Id( Activitylibrary_Id );
				
				tempresource.setMaterialid( materialresourceform.getMaterialid( tempindex[i] ) );
				tempresource.setMaterialcostlibid( materialresourceform.getMaterialcostlibid( tempindex[i] ) );
				tempresource.setMaterialname( materialresourceform.getMaterialname( tempindex[i] ) );
				tempresource.setMaterialtype( materialresourceform.getMaterialtype( tempindex[i] ) );
				tempresource.setManufacturername( materialresourceform.getManufacturername( tempindex[i] ) );
				tempresource.setManufacturerpartnumber( materialresourceform.getManufacturerpartnumber( tempindex[i] ) );
				tempresource.setCnspartnumber( materialresourceform.getCnspartnumber( tempindex[i] ) );
				tempresource.setEstimatedtotalcost( materialresourceform.getEstimatedtotalcost( tempindex[i] ) );
				tempresource.setEstimatedunitcost( materialresourceform.getEstimatedunitcost( tempindex[i] ) );
				tempresource.setMinimumquantity( materialresourceform.getMinimumquantity( tempindex[i] ) );
				tempresource.setPriceextended( materialresourceform.getPriceextended( tempindex[i] ) );
				tempresource.setPriceunit( materialresourceform.getPriceunit( tempindex[i] ) );
				tempresource.setProformamargin( materialresourceform.getProformamargin( tempindex[i] ) );
				
				tempresource.setQuantity( materialresourceform.getQuantity( tempindex[i] ) );
				tempresource.setPrevquantity( materialresourceform.getPrevquantity( tempindex[i] ) );
				
				tempresource.setSellablequantity( materialresourceform.getSellablequantity( tempindex[i] ) );
				tempresource.setStatus( materialresourceform.getStatus( tempindex[i] ) );
				
				
				temp.add( tempresource );
			}	
		}	 
			
		materialresource = ActivityLibrarydao.getMaterialResource(  materialresourceform.getNewmaterialnamecombo() , getDataSource( request , "ilexnewDB" ) );
		
		materialresourceform.reset( mapping , request);
		
		materialresourceform.setNewmaterialid( newmaterialid );
	
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		
		
		materialresourceform.setNewmaterialname( newname );
		materialresourceform.setNewmaterialnamecombo( newnamecombo );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setNewmaterialtype( materialresource.getMaterialtype() );
		materialresourceform.setNewmanufacturername( materialresource.getManufacturername() );
		materialresourceform.setNewmanufacturerpartnumber( materialresource.getManufacturerpartnumber() );
		materialresourceform.setNewcnspartnumber( materialresource.getCnspartnumber() );
		materialresourceform.setNewestimatedunitcost( materialresource.getEstimatedunitcost() );
		materialresourceform.setNewsellablequantity( materialresource.getSellablequantity() );
		materialresourceform.setNewminimumquantity( materialresource.getMinimumquantity() );
		materialresourceform.setRef( null );
		materialresourceform.setActivity_Id( Activitylibrary_Id );
		materialresourceform.setMateriallist( ActivityLibrarydao.getMateriallist( "material" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );
		
		materialresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "M" , "%" , "" , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();	
		}
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setActivity_Id( idname.getId() );
		materialresourceform.setActivity_name( idname.getName() );
		
		if( checklength > 0 )
		{
			if( resource_size > 0 )
			{
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < resource_size; j++ )
					{
						if( ( ( MaterialResource )temp.get( i ) ).getMaterialid().equals( ( ( MaterialResource )materialresourcelist.get( j ) ).getMaterialid() ) )
						{
							materialresourcelist.set( j ,  temp.get( i ) );
						}
					}
				}
				materialresourceform.setCheck( checkrefresh );
			}
			
		}
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
				
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "false" );
		return( mapping.findForward( "materialresourcetabularpage" ) );
	}
	

/** This method is used to add/update material resource to an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		String Activitylibrary_Id = "";
		MaterialResource materialresource = null;
		int resource_size = 0;
		ArrayList materialresourcelist = new ArrayList();
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String status = "";
		String activityname = "";
		int addflag = 0;
		NameId idname = null;
		
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		Activitylibrary_Id = materialresourceform.getActivity_Id();
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) != null ) 
		{
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			materialresourceform.setMSA_Id(request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end*/
		
		if( materialresourceform.getCheck() !=  null )
		{
			int checklength = materialresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = materialresourceform.getMaterialid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( materialresourceform.getCheck( i ).substring( 1 , materialresourceform.getCheck( i ).length() ).equals( materialresourceform.getMaterialid( j ) ) )
						{	
							//System.out.println("value of jjjjjjjmaterialresourceform.getCheck(i).substring(1,materialresourceform.getCheck(i).length())"+materialresourceform.getCheck(i).substring(1,materialresourceform.getCheck(i).length()));
							index[k] = j;
							k++;
						}
						
						/*if( materialresourceform.getCheck(i).equals( materialresourceform.getMaterialid( j ) ) )
						{	
							index[k] = j;
							k++;
						}*/
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				int updateflag = 0;
				for( int i = 0; i < index.length; i++ )
				{
					materialresource = new MaterialResource();
					materialresource.setMaterialid( materialresourceform.getMaterialid ( index[i] ) );
					materialresource.setMaterialcostlibid( materialresourceform.getMaterialcostlibid ( index[i] ) );
					materialresource.setActivity_Id( materialresourceform.getActivity_Id() );
					materialresource.setQuantity( materialresourceform.getQuantity ( index[i] ) );
					materialresource.setPrevquantity( materialresourceform.getPrevquantity( index[i] ) );
					materialresource.setMaterialtype( "M" );
					materialresource.setEstimatedunitcost( materialresourceform.getEstimatedunitcost( index[i] ) );
					materialresource.setProformamargin( materialresourceform.getProformamargin( index[i] ) );
					/* for multiple resource select start*/
					materialresource.setCnspartnumber( materialresourceform.getCnspartnumber( index[i] ) );
					materialresource.setFlag( materialresourceform.getFlag( index[i] ) );
					if( materialresourceform.getFlag( index[i] ).equals( "P" ) )
					{
						updateflag = ActivityLibrarydao.updateMaterialResource( materialresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "updateflag" , ""+updateflag );
					}
					if( materialresourceform.getFlag( index[i] ).equals( "T" ) )
					{
						addflag = ActivityLibrarydao.addMaterialResource( materialresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "addflag" , ""+addflag );
					}
					/* for multiple resource select end*/
					
					/*updateflag = ActivityLibrarydao.updatematerialresource( materialresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );*/
				}
			}
		}
		
		if( materialresourceform.getNewmaterialid() != null )
		{
			
			materialresource = new MaterialResource();
			materialresource.setMaterialid( materialresourceform.getNewmaterialnamecombo() );
			materialresource.setActivity_Id( materialresourceform.getActivity_Id() );
			materialresource.setCnspartnumber( materialresourceform.getNewcnspartnumber() );
			materialresource.setQuantity( materialresourceform.getNewquantity() );
			materialresource.setMaterialtype( "M" );
			materialresource.setEstimatedunitcost( materialresourceform.getNewestimatedunitcost() );
			materialresource.setProformamargin( materialresourceform.getNewproformamargin() );
			
			addflag = ActivityLibrarydao.addMaterialResource( materialresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		materialresourceform.reset( mapping , request);
		
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setActivity_Id( Activitylibrary_Id );
		
		materialresourceform.setRef( null );
		materialresourceform.setMateriallist( ActivityLibrarydao.getMateriallist( "material" , Activitylibrary_Id ,  getDataSource( request , "ilexnewDB" ) ) );	
		
		materialresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "M" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
		}
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setActivity_Id( idname.getId() );
		materialresourceform.setActivity_name( idname.getName() );
		
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "activityname" , activityname );
		
		return( mapping.findForward( "materialresourcetabularpage" ) );
	}
	

/** This method is used to delete material resource from an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
	
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		MaterialResource materialresource = null;
		String Activitylibrary_Id = "";
		int resource_size = 0;
		int deleteflag = 0;
		ArrayList materialresourcelist = new ArrayList();
		String status = "";
		String activityname = "";
		int addflag = 0;
		NameId idname = null;
		
		String sortqueryclause = "";
		
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			materialresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end*/
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			materialresourceform.setActivity_Id( Activitylibrary_Id );
		}
		else
		{
			Activitylibrary_Id = materialresourceform.getActivity_Id();
		}
		
		//Start :Added By Amit For Mass delete
		if( request.getParameter( "fromResourceTabular" ) != null )
		{
			if( materialresourceform.getCheck()!=  null )
			{
				
				int checklength = materialresourceform.getCheck().length;	 
		
				int []index = new int[checklength];
				int k = 0;
				
				if( checklength > 0 )
				{	int length = materialresourceform.getMaterialid().length;
					for( int i = 0; i < checklength; i++ )
					{
						for( int j = 0; j < length; j++ )
						{
							if( materialresourceform.getCheck( i ).substring( 1 , materialresourceform.getCheck( i ).length() ).equals( materialresourceform.getMaterialid( j ) ) )
							{	
								index[k] = j;
								k++;
							}
						}
					}
				}
			
				if( checklength > 0 )                               
				{
					
					for( int i = 0; i < index.length; i++ )
					{
						materialresource = new MaterialResource();
						materialresource.setMaterialid( materialresourceform.getMaterialid ( index[i] ) );
						
						deleteflag = ActivityLibrarydao.deleteresource( materialresource.getMaterialid() , getDataSource( request , "ilexnewDB" ) );
						
					}
				}
			}
		}
		else
		{
		//End : Added By Amit
			deleteflag = ActivityLibrarydao.deleteresource( request.getParameter( "Material_Id" ) , getDataSource( request , "ilexnewDB" ) );
		}
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setActivity_Id( Activitylibrary_Id );
		materialresourceform.setMateriallist( ActivityLibrarydao.getMateriallist( "material" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		materialresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "M" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setActivity_Id( idname.getId() );
		materialresourceform.setActivity_name( idname.getName() );
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		
		request.setAttribute( "refresh" , "true" );
		
		request.setAttribute( "activityname" , activityname );
		
		return( mapping.findForward( "materialresourcetabularpage" ) );
	}
	
	
	/**
	 * @author amitm
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward MassApprove( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		MaterialResourceForm materialresourceform = ( MaterialResourceForm ) form;
		MaterialResource materialresource = null;
		String Activitylibrary_Id = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		ArrayList materialresourcelist = new ArrayList();
		String status1  = null,status = null;
		String activityname = "";
		int resource_size = 0,changestatusflag = 0;
		String MSA_Id = ""; 
		String Appendix_Id = "";
		String Job_Id = "";
		int addflag = 0;
		NameId idname = null;
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Status" ) != null )
		{
			status1 = request.getParameter( "Status" );
		}
		
		
		
		Activitylibrary_Id = materialresourceform.getActivity_Id();
		
		if( materialresourceform.getCheck() !=  null )
		{
			int checklength = materialresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = materialresourceform.getMaterialid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( materialresourceform.getCheck( i ).substring( 1 , materialresourceform.getCheck( i ).length() ).equals( materialresourceform.getMaterialid( j ) ) )
						{	
							index[k] = j;
							k++;
						}
						
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				
				for( int i = 0; i < index.length; i++ )
				{
					materialresource = new MaterialResource();
					materialresource.setMaterialid( materialresourceform.getMaterialid ( index[i] ) );
					materialresource.setActivity_Id( materialresourceform.getActivity_Id() );
					materialresource.setMaterialtype( "M" );
					 changestatusflag = ChangeStatus.Status( MSA_Id , Appendix_Id , Job_Id , materialresource.getActivity_Id() , materialresource.getMaterialid() , materialresource.getMaterialtype() , status1 ,"N", getDataSource( request , "ilexnewDB" ) ,"");
					
				}
			}
		}
		
	
		if( materialresourceform.getNewmaterialid() != null )
		{
			
				materialresource = new MaterialResource();
				materialresource.setMaterialid( materialresourceform.getNewmaterialnamecombo() );
				materialresource.setActivity_Id( materialresourceform.getActivity_Id() );
				materialresource.setCnspartnumber( materialresourceform.getNewcnspartnumber() );
				materialresource.setQuantity( materialresourceform.getNewquantity() );
				materialresource.setMaterialtype( "M" );
				materialresource.setEstimatedunitcost( materialresourceform.getNewestimatedunitcost() );
				materialresource.setProformamargin( materialresourceform.getNewproformamargin() );
				
				addflag = ActivityLibrarydao.addMaterialResource( materialresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute( "addflag" , ""+addflag );
		}
		
		materialresourceform.reset( mapping , request);
		
		materialresourceform.setNewquantity( "0.0000" );
		materialresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "M" , getDataSource( request , "ilexnewDB" ) ) );
		materialresourceform.setNewstatus( "Draft" );
		materialresourceform.setActivity_Id( Activitylibrary_Id );
		
		materialresourceform.setRef( null );
		materialresourceform.setMateriallist( ActivityLibrarydao.getMateriallist( "material" , Activitylibrary_Id ,  getDataSource( request , "ilexnewDB" ) ) );

		materialresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "M" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
		
		if( materialresourcelist.size() > 0 )
		{
			resource_size = materialresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setMsa_id( idname.getId() );
		materialresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		materialresourceform.setActivity_Id( idname.getId() );
		materialresourceform.setActivity_name( idname.getName() );
		
		
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );					
		
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "materialresourcelist" , materialresourcelist );
		request.setAttribute("changestatusflag",""+changestatusflag);
		
		return( mapping.findForward( "materialresourcetabularpage" ) );
	}
	
	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "amresM_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "amresM_sort" );
		return queryclause;
	}
	
}
