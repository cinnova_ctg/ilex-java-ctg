/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used for displaying activity detail     
*
*/
package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.am.ActivityLibrary;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.formbean.AM.ActivityLibraryDetailForm;

/*
 * Methods : execute
 */
public class ActivityLibraryDetailAction extends com.mind.common.IlexAction
{
	
/** This method populate activitylibrarydetailform formbean with the activity detail whose data is to be shown
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
		
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{ 
		ActivityLibraryDetailForm activitylibrarydetailform = ( ActivityLibraryDetailForm ) form;
		String Activitylibrary_Id = "";
		String MSA_Id = "";
		
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String loginuserid = (String)session.getAttribute("userid");
		if( activitylibrarydetailform.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( loginuserid , "Activity" , "View Activity Detail" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		String list = "";
		ActivityLibrary activitylibrary = new ActivityLibrary();
		String status = "";
		int sow_size = 0;
		int assumption_size = 0;
		
		/* Check the status updated or not */
		if( request.getAttribute( "changestatusflag" ) != null ) {
			request.setAttribute( "changestatusflag",( String ) request.getAttribute( "changestatusflag" ));
		}
		
		
		if( request.getParameter( "MSA_Id" ) != null )
		{
			MSA_Id = request.getParameter( "MSA_Id" );
			request.setAttribute( "MSA_Id" , request.getParameter( "MSA_Id" ) );
		}
		
		if( request.getAttribute( "MSA_Id" ) != null )
		{
			MSA_Id = ( String )request.getAttribute( "MSA_Id" );
			request.setAttribute( "MSA_Id" , ( String )request.getAttribute( "MSA_Id" ) );
		}
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			request.setAttribute( "Activitylibrary_Id" , request.getParameter( "Activitylibrary_Id" ) );
		}
		
		if( request.getAttribute( "Activitylibrary_Id" ) != null )
		{
			Activitylibrary_Id = ( String )request.getAttribute( "Activitylibrary_Id" );
			request.setAttribute( "Activitylibrary_Id" , ( String ) request.getAttribute( "Activitylibrary_Id" ) );
		}
		
		/*customer labor rates*/
		int checkNetmedXmsa = 0;
		
		if(MSA_Id != null )
		{
			if (! MSA_Id.equals(""))
			{
				checkNetmedXmsa = ActivityLibrarydao.checkCategoryname(MSA_Id, getDataSource( request , "ilexnewDB" ));	
			}
		}
		
		if(checkNetmedXmsa==1)
		{	
			String type="";
			type=request.getParameter("type");
			request.setAttribute("type",type);
			request.setAttribute( "MSA_Id" , MSA_Id );
			request.setAttribute( "Activitylibrary_Id" , Activitylibrary_Id );
			
			return mapping.findForward( "customerlaborratepage" );
		}
		/*customer labor rates*/
		else
		{
			activitylibrary = ActivityLibrarydao.getActivity( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
			
			activitylibrarydetailform.setActivity_Id( activitylibrary.getActivity_Id() );
			activitylibrarydetailform.setMsa_Id( activitylibrary.getMsa_Id() );
			request.setAttribute( "MSA_Id" , activitylibrarydetailform.getMsa_Id() );
			
			activitylibrarydetailform.setActivityname( activitylibrary.getName() );
			activitylibrarydetailform.setActivitytype( activitylibrary.getActivitytype() );
			activitylibrarydetailform.setEstimatedcnslabourcost( activitylibrary.getEstimatedcnslabourcost());
			activitylibrarydetailform.setEstimatedcontractlabourcost( activitylibrary.getEstimatedcontractlabourcost() );
			activitylibrarydetailform.setEstimatedfreightcost( activitylibrary.getEstimatedfreightcost() );
			activitylibrarydetailform.setEstimatedmaterialcost( activitylibrary.getEstimatedmaterialcost() );
			activitylibrarydetailform.setEstimatedtotalcost( activitylibrary.getEstimatedtotalcost() );
			activitylibrarydetailform.setEstimatedtravelcost( activitylibrary.getEstimatedtravelcost() );
			activitylibrarydetailform.setExtendedprice( activitylibrary.getExtendedprice() );
			activitylibrarydetailform.setProformamargin( activitylibrary.getProformamargin() );
			activitylibrarydetailform.setQuantity( activitylibrary.getQuantity() );
			activitylibrarydetailform.setSow( activitylibrary.getSow() );
			activitylibrarydetailform.setAssumption( activitylibrary.getAssumption() );
			activitylibrarydetailform.setStatus( activitylibrary.getStatus() );
			activitylibrarydetailform.setMsaname( activitylibrary.getMsaname() );
			
			list = Menu.getStatus( "am" , activitylibrarydetailform.getStatus().charAt( 0 ) , "" , "" , Activitylibrary_Id , "" ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
			
			
			ArrayList materiallist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "M" , "%" , "" , request.getSession().getId(),"detailpage",getDataSource( request , "ilexnewDB" ) );
			ArrayList laborlist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "L" , "%" , "" , request.getSession().getId(),"detailpage",getDataSource( request , "ilexnewDB" ) );
			ArrayList freightlist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "F" , "%" , "" , request.getSession().getId(),"detailpage",getDataSource( request , "ilexnewDB" ) );
			ArrayList travellist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "T" , "%" , "" , request.getSession().getId(),"detailpage",getDataSource( request , "ilexnewDB" ) );
			
			request.setAttribute( "list" , list );
			
			
			if( materiallist.size() > 0 )
			{
				request.setAttribute( "materiallist" , materiallist );
			}
			if( laborlist.size() > 0 )
			{
				request.setAttribute( "laborlist" , laborlist );
			}
			if( freightlist.size() > 0 )
			{
				request.setAttribute( "freightlist" , freightlist );
			}
			if( travellist.size() > 0 )
			{
				request.setAttribute( "travellist" , travellist );
			}
			
			
			ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , activitylibrarydetailform.getActivity_Id() , "am" );
			
			if( sowlist.size() > 0 )
			{
				sow_size = sowlist.size();	
			}
			
			ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , activitylibrarydetailform.getActivity_Id() , "am" );
			
			if( assumptionlist.size() > 0 )
			{
				assumption_size = assumptionlist.size();	
			}
			
			request.setAttribute( "sowsize" , ""+sow_size );
			request.setAttribute( "assumptionsize" , ""+assumption_size );
			
			
			request.setAttribute( "sowlist" , sowlist );
			request.setAttribute( "assumptionlist" , assumptionlist );
			
			return mapping.findForward( "activitylibrarydetailpage" );
		}
		
	}	
}
