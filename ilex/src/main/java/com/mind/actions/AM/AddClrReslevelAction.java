package com.mind.actions.AM;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboAM;
import com.mind.common.dao.DynamicComboDaoAM;
import com.mind.dao.AM.AddClrResleveldao;
import com.mind.formbean.AM.AddClrReslevelForm;

public class AddClrReslevelAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward add(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddClrReslevelForm addClrReslevelForm = (AddClrReslevelForm) form;
		DynamicComboDaoAM dcomboAM=new DynamicComboDaoAM();
		DynamicComboAM dcam = new DynamicComboAM();
		ArrayList classlist= new ArrayList();
		ArrayList resourcelist= new ArrayList();
		int statusvalue=-1;
		String resname[]=new String[2];
		
		//System.out.println("11111");
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		/*if( addClrReslevelForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Manufacturer" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}*/
		/* Page Security End*/
		
		if(request.getParameter("ref")!=null)
		{
			addClrReslevelForm.setRef(request.getParameter("ref"));
		}
		
		
		
		if(request.getParameter("refresh")!=null)
		{
			if(request.getParameter("refresh").equals("true"))
			{
				
				if(addClrReslevelForm.getClrresource().equals("0"))
				{
				}
				else
				{
					resname=AddClrResleveldao.getResourcelevelName(addClrReslevelForm.getClrresource(),getDataSource( request,"ilexnewDB" ));
					addClrReslevelForm.setName(resname[0]);
					addClrReslevelForm.setShortname(resname[1]);
				}
				
				addClrReslevelForm.setClrclass("0");
				addClrReslevelForm.setRefresh(null);
				forward = mapping.findForward("success");
			}
			
		}
				
		if( addClrReslevelForm.getSave()!= null )
		{
			if(addClrReslevelForm.getClrclass()!=null)
			{
				if(Integer.parseInt(addClrReslevelForm.getClrclass())!=0)
				{
					statusvalue=AddClrResleveldao.addResLevel(addClrReslevelForm.getClrclass(),addClrReslevelForm.getName(),addClrReslevelForm.getShortname(),0,getDataSource( request,"ilexnewDB" ));
					 
					addClrReslevelForm.setAddmessage(""+statusvalue);
					
				}
			}
			
		}
		classlist=dcomboAM.getClassNameCombo(getDataSource( request,"ilexnewDB" ));
		dcam .setClasslist(classlist);
		request.setAttribute( "dynamicclasscombo" , dcam );
		
		resourcelist=dcomboAM.getResourceNameCombo(getDataSource( request,"ilexnewDB" ));
		dcam .setResourcelist(resourcelist);
		request.setAttribute( "dynamicresoursecombo" , dcam );			
		forward = mapping.findForward("success");
			
		return (forward);

	}
	
	public ActionForward delete(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		Collection subcatglist;
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddClrReslevelForm addClrReslevelForm = (AddClrReslevelForm) form;
		DynamicComboDaoAM dcomboAM=new DynamicComboDaoAM();
		DynamicComboAM dcam = new DynamicComboAM();
		ArrayList classlist= new ArrayList();
		ArrayList resourcelist= new ArrayList();
		int statusvalue=-1;
		String resname[]=new String[2];
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid");
		
		/*if( manAddForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid , "Manage Resource" , "Manage Manufacturer" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}*/
		/* Page Security End*/
		
		if(request.getParameter("resid")!=null)
		{
			statusvalue=AddClrResleveldao.deleteResLevel(request.getParameter("resid"),getDataSource( request,"ilexnewDB" ));
			addClrReslevelForm.setDeletemessage(""+statusvalue);
			if(statusvalue==0)
			{
				addClrReslevelForm.setName(null);
				addClrReslevelForm.setShortname(null);
			}
		}
		classlist=dcomboAM.getClassNameCombo(getDataSource( request,"ilexnewDB" ));
		dcam .setClasslist(classlist);
		request.setAttribute( "dynamicclasscombo" , dcam );
		
		resourcelist=dcomboAM.getResourceNameCombo(getDataSource( request,"ilexnewDB" ));
		dcam .setResourcelist(resourcelist);
		request.setAttribute( "dynamicresoursecombo" , dcam );
		
		
		addClrReslevelForm.setRef("add");			
		forward = mapping.findForward("success");
					
		return (forward);

	
	}
	
	
	
	

}
