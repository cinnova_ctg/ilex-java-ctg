/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used for displaying freight resource detail     
*
*/


package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.FreightResource;
import com.mind.formbean.AM.FreightResourceDetailForm;


/*
 * Methods : execute
 */

public class FreightResourceDetailAction extends com.mind.common.IlexAction
{

/** This method populate FreightResourceDetailform formbean with the freight detail of an activity whose data is to be shown
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		FreightResourceDetailForm freightresourcedetailform = ( FreightResourceDetailForm ) form;
		FreightResource freightresource = new FreightResource();
		String freight_Id = ""; 
		ArrayList freightresourcelist = new ArrayList();
		String list = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		int sow_size = 0;
		int assumption_size = 0;
		
		
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		
		if( request.getParameter( "Freight_Id" ) != null )
		{
			freight_Id = request.getParameter( "Freight_Id" );
		}
		
		if( request.getAttribute( "Freight_Id" ) != null )
		{
			freight_Id = ( String ) request.getAttribute( "Freight_Id" );
		}
		
		freightresourcedetailform.setFreightid( freight_Id );
		request.setAttribute( "Freight_Id" , freightresourcedetailform.getFreightid() );
		
		freightresourcelist = ActivityLibrarydao.getMaterialResourcelist( "%" , "F" , freightresourcedetailform.getFreightid() , "" , request.getSession().getId(),"detailpage",getDataSource( request , "ilexnewDB" ) ); 
		
		freightresource = ( FreightResource ) freightresourcelist.get( 0 );
		
		freightresourcedetailform.setActivity_Id( freightresource.getActivity_Id() );
		request.setAttribute( "Activitylibrary_Id" , freightresourcedetailform.getActivity_Id() );  
		
		freightresourcedetailform.setFreightid( freightresource.getFreightid() );
		freightresourcedetailform.setFreightname( freightresource.getFreightname() );
		freightresourcedetailform.setFreighttype( freightresource.getFreighttype() );
		freightresourcedetailform.setMinimumquantity( freightresource.getMinimumquantity() );
		freightresourcedetailform.setCnspartnumber( freightresource.getCnspartnumber() );
		freightresourcedetailform.setEstimatedtotalcost( freightresource.getEstimatedtotalcost() );
		freightresourcedetailform.setEstimatedunitcost( freightresource.getEstimatedunitcost() );
		
		freightresourcedetailform.setPriceextended( freightresource.getPriceextended() );
		freightresourcedetailform.setPriceunit( freightresource.getPriceunit() );
		freightresourcedetailform.setProformamargin( freightresource.getProformamargin() );
		freightresourcedetailform.setQuantity( freightresource.getQuantity() );
		freightresourcedetailform.setSellablequantity( freightresource.getSellablequantity() );
		freightresourcedetailform.setStatus( freightresource.getStatus() );
		
		freightresourcedetailform.setActivity_Id( freightresource.getActivity_Id() );
		freightresourcedetailform.setActivityname( freightresource.getActivity_name() );
		freightresourcedetailform.setMsa_id( freightresource.getMsa_Id() );
		freightresourcedetailform.setMsaname( freightresource.getMsaname() );
		
		list = Menu.getStatus( "F" , freightresourcedetailform.getStatus().charAt( 0 ) , "" , "" , "" , freightresourcedetailform.getFreightid() ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "list" , list );
		
		ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , freightresourcedetailform.getFreightid() , "F" );
		
		if( sowlist.size() > 0 )
		{
			sow_size = sowlist.size();	
		}
		
		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , freightresourcedetailform.getFreightid() , "F" );
		
		if( assumptionlist.size() > 0 )
		{
			assumption_size = assumptionlist.size();	
		}
		
		request.setAttribute( "sowsize" , ""+sow_size );
		request.setAttribute( "assumptionsize" , ""+assumption_size );
		
		
		request.setAttribute( "sowlist" , sowlist );
		request.setAttribute( "assumptionlist" , assumptionlist );
		
		return mapping.findForward( "freightresourcedetailpage" );
	}
}
