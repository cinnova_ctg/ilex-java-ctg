package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.dao.AM.CustomerLaborRatesdao;
import com.mind.dao.PM.Activitydao;
import com.mind.formbean.AM.CustomerLaborRatesForm;


public class CustomerLaborRatesAction extends com.mind.common.IlexDispatchAction
{

	public ActionForward add ( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		CustomerLaborRatesForm cLRForm = (CustomerLaborRatesForm) form;
		ArrayList CLRlist = new ArrayList();
		ArrayList resourcelevellist = new ArrayList();
		ArrayList nooftechengglist=new ArrayList();
		String[] clrdetail=new String[6];
		
		ArrayList getTechEngtypes = new ArrayList();
		String technos="0";
		String enggnos="0";
		String customerid=null;
		String customername="";
		String netmedxcatgid="0";
		String Activitylibrary_Id="0";
		String list = "";
		String type = "";
		String MSA_Id = "";
		String Job_Id = "";
		String Appendix_Id = "";
		String sptype = "";
		String currentstatus = "";
		int statusvalue=0;
		int editflag=0;
		int totaltechenggs=0;
		int totalresources=0;
		String catid="0";
		/* for adding extra fields in CLR Form start*/
		int categoriespercriticality=0;
		int totalcategories=0;
		/* for adding extra fields in CLR Form end*/
		
		String jobName = "";
		
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		/* for security */
		String temp_fu_type = "";
		String temp_fu_name = "";
		if(request.getParameter( "type" ) != null) 
		{
			if(request.getParameter( "type" ).equals("PM"))
			{ 										//security para for Send PO - WO 
				temp_fu_type = "Manage Customer Labor Rates"; 
				temp_fu_name = "Resource";
			}
			
			if( request.getParameter( "type" ).equals( "AM" ))
			{
				temp_fu_type = "View Customer Labor Rates"; 									//will be changed later use for testing
				temp_fu_name = "Labor";
			}
			if( request.getParameter( "type" ).equals( "AM_EDIT" ) )
			{
				temp_fu_type = "Add Customer Labor Rates"; 									//will be changed later use for testing
				temp_fu_name = "Labor";
			}
		}
		
		cLRForm.setRefclr(request.getParameter("refclr"));
		
		/* Page Security Start*/
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		if( cLRForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid, temp_fu_name,temp_fu_type,  getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		
		if(request.getParameter("appendixId") != null) {
			cLRForm.setShowAppendixId(request.getParameter("appendixId"));
		}
		
		if(request.getParameter("appendixName") != null ){
			cLRForm.setShowAppendixName(request.getParameter("appendixName"));
		}
		
		if(request.getAttribute("from") != null) {
			cLRForm.setFromPage(request.getAttribute("from").toString());
		}
		
		if(cLRForm.getFromPage() == null) {
			cLRForm.setFromPage("");
		}
		
		/* for adding extra fields in CLR Form start*/
		categoriespercriticality=CustomerLaborRatesdao.getCategoriesPerCriticality(getDataSource( request , "ilexnewDB" ));
		request.setAttribute("categoriespercriticality",categoriespercriticality+"");
		
		totalcategories=CustomerLaborRatesdao.getTotalCategories(getDataSource( request , "ilexnewDB" ));
		request.setAttribute("totalcategories",totalcategories+"");
		
		/* for adding extra fields in CLR Form end*/
		netmedxcatgid=CustomerLaborRatesdao.getNetmedxcatgid(getDataSource( request , "ilexnewDB" ));
		request.setAttribute("netmedxcatgid",netmedxcatgid);
		
		if(cLRForm.getSave()!=null)
		{
			type=cLRForm.getType();
			request.setAttribute("type",type);
						
			MSA_Id=cLRForm.getMsa_Id();
			request.setAttribute("MSA_Id",MSA_Id);
			
			Activitylibrary_Id=cLRForm.getActivitylibrary_Id();
			request.setAttribute("Activitylibrary_Id",Activitylibrary_Id);
			
			customerid = cLRForm.getCustomerid();
			customername = cLRForm.getCustomername();
			
			cLRForm.setType(type);
			cLRForm.setMsa_Id(MSA_Id);
			cLRForm.setActivitylibrary_Id(Activitylibrary_Id);
			cLRForm.setCustomerid(customerid);
			cLRForm.setCustomername(customername);
						
			totalresources=CustomerLaborRatesdao.gettotalresources(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
			request.setAttribute("totalresources",totalresources+"");
			
			String[][] laborrates = new String[totalcategories][30];
			String[][] detaillistid=new String[totalcategories][30];
			String[][] selecttedresourceid=new String[totalcategories][30];
						
			for(int i=0;i<totalcategories;i++)
			{
				if(totalresources >= 1)
				{
					detaillistid[i][0]=cLRForm.getDetailid0(i);
					selecttedresourceid[i][0]=cLRForm.getResourceid0(i);
					laborrates[i][0]=cLRForm.getClrrate0(i);
				}
				if(totalresources >= 2)
				{
					detaillistid[i][1]=cLRForm.getDetailid1(i);
					selecttedresourceid[i][1]=cLRForm.getResourceid1(i);
					laborrates[i][1]=cLRForm.getClrrate1(i);
				}
				if(totalresources >= 3)
				{
					detaillistid[i][2]=cLRForm.getDetailid2(i);
					selecttedresourceid[i][2]=cLRForm.getResourceid2(i);
					laborrates[i][2]=cLRForm.getClrrate2(i);
					
				}
				if(totalresources >= 4)
				{
					detaillistid[i][3]=cLRForm.getDetailid3(i);
					selecttedresourceid[i][3]=cLRForm.getResourceid3(i);
					laborrates[i][3]=cLRForm.getClrrate3(i);
				}
				if(totalresources >= 5)
				{
					detaillistid[i][4]=cLRForm.getDetailid4(i);
					selecttedresourceid[i][4]=cLRForm.getResourceid4(i);
					laborrates[i][4]=cLRForm.getClrrate4(i);
				}
				if(totalresources >= 6)
				{
					detaillistid[i][5]=cLRForm.getDetailid5(i);
					selecttedresourceid[i][5]=cLRForm.getResourceid5(i);
					laborrates[i][5]=cLRForm.getClrrate5(i);
				}
				if(totalresources >= 7)
				{
					detaillistid[i][6]=cLRForm.getDetailid6(i);
					selecttedresourceid[i][6]=cLRForm.getResourceid6(i);
					laborrates[i][6]=cLRForm.getClrrate6(i);
				}
				if(totalresources >= 8)
				{
					detaillistid[i][7]=cLRForm.getDetailid7(i);
					selecttedresourceid[i][7]=cLRForm.getResourceid7(i);
					laborrates[i][7]=cLRForm.getClrrate7(i);
				}
				if(totalresources >= 9)
				{
					detaillistid[i][8]=cLRForm.getDetailid8(i);
					selecttedresourceid[i][8]=cLRForm.getResourceid8(i);
					laborrates[i][8]=cLRForm.getClrrate8(i);
				}
				if(totalresources >= 10)
				{
					detaillistid[i][9]=cLRForm.getDetailid9(i);
					selecttedresourceid[i][9]=cLRForm.getResourceid9(i);
					laborrates[i][9]=cLRForm.getClrrate9(i);
				}
				if(totalresources >= 11)
				{
					detaillistid[i][10]=cLRForm.getDetailid10(i);
					selecttedresourceid[i][10]=cLRForm.getResourceid10(i);
					laborrates[i][10]=cLRForm.getClrrate10(i);
				}
				if(totalresources >= 12)
				{
					detaillistid[i][11]=cLRForm.getDetailid11(i);
					selecttedresourceid[i][11]=cLRForm.getResourceid11(i);
					laborrates[i][11]=cLRForm.getClrrate11(i);
				}
				if(totalresources >= 13)
				{
					detaillistid[i][12]=cLRForm.getDetailid12(i);
					selecttedresourceid[i][12]=cLRForm.getResourceid12(i);
					laborrates[i][12]=cLRForm.getClrrate12(i);
				}
				if(totalresources >= 14)
				{
					detaillistid[i][13]=cLRForm.getDetailid13(i);
					selecttedresourceid[i][13]=cLRForm.getResourceid13(i);
					laborrates[i][13]=cLRForm.getClrrate13(i);
				}
				if(totalresources >= 15)
				{
					detaillistid[i][14]=cLRForm.getDetailid14(i);
					selecttedresourceid[i][14]=cLRForm.getResourceid14(i);
					laborrates[i][14]=cLRForm.getClrrate14(i);
				}
				if(totalresources >= 16)
				{
					detaillistid[i][15]=cLRForm.getDetailid15(i);
					selecttedresourceid[i][15]=cLRForm.getResourceid15(i);
					laborrates[i][15]=cLRForm.getClrrate15(i);
				}
				if(totalresources >= 17)
				{
					detaillistid[i][16]=cLRForm.getDetailid16(i);
					selecttedresourceid[i][16]=cLRForm.getResourceid16(i);
					laborrates[i][16]=cLRForm.getClrrate16(i);
				}
				if(totalresources >= 18)
				{
					detaillistid[i][17]=cLRForm.getDetailid17(i);
					selecttedresourceid[i][17]=cLRForm.getResourceid17(i);
					laborrates[i][17]=cLRForm.getClrrate17(i);
				}
				if(totalresources >= 19)
				{
					detaillistid[i][18]=cLRForm.getDetailid18(i);
					selecttedresourceid[i][18]=cLRForm.getResourceid18(i);
					laborrates[i][18]=cLRForm.getClrrate18(i);
				}
				if(totalresources >= 20)
				{
					detaillistid[i][19]=cLRForm.getDetailid19(i);
					selecttedresourceid[i][19]=cLRForm.getResourceid19(i);
					laborrates[i][19]=cLRForm.getClrrate19(i);
				}
				if(totalresources >= 21)
				{
					detaillistid[i][20]=cLRForm.getDetailid20(i);
					selecttedresourceid[i][20]=cLRForm.getResourceid20(i);
					laborrates[i][20]=cLRForm.getClrrate20(i);
				}
				if(totalresources >= 22)
				{
					detaillistid[i][21]=cLRForm.getDetailid21(i);
					selecttedresourceid[i][21]=cLRForm.getResourceid21(i);
					laborrates[i][21]=cLRForm.getClrrate21(i);
				}
				if(totalresources >= 23)
				{
					detaillistid[i][22]=cLRForm.getDetailid22(i);
					selecttedresourceid[i][22]=cLRForm.getResourceid22(i);
					laborrates[i][22]=cLRForm.getClrrate22(i);
				}
				if(totalresources >= 24)
				{
					detaillistid[i][23]=cLRForm.getDetailid23(i);
					selecttedresourceid[i][23]=cLRForm.getResourceid23(i);
					laborrates[i][23]=cLRForm.getClrrate23(i);
				}
				if(totalresources >= 25)
				{
					detaillistid[i][24]=cLRForm.getDetailid24(i);
					selecttedresourceid[i][24]=cLRForm.getResourceid24(i);
					laborrates[i][24]=cLRForm.getClrrate24(i);
				}
				if(totalresources >= 26)
				{
					detaillistid[i][25]=cLRForm.getDetailid25(i);
					selecttedresourceid[i][25]=cLRForm.getResourceid25(i);
					laborrates[i][25]=cLRForm.getClrrate25(i);
				}
				if(totalresources >= 27)
				{
					detaillistid[i][26]=cLRForm.getDetailid26(i);
					selecttedresourceid[i][26]=cLRForm.getResourceid26(i);
					laborrates[i][26]=cLRForm.getClrrate26(i);
				}
				if(totalresources >= 28)
				{
					detaillistid[i][27]=cLRForm.getDetailid27(i);
					selecttedresourceid[i][27]=cLRForm.getResourceid27(i);
					laborrates[i][27]=cLRForm.getClrrate27(i);
				}
				if(totalresources >= 29)
				{
					detaillistid[i][28]=cLRForm.getDetailid28(i);
					selecttedresourceid[i][28]=cLRForm.getResourceid28(i);
					laborrates[i][28]=cLRForm.getClrrate28(i);
				}
				if(totalresources >= 30)
				{
					detaillistid[i][29]=cLRForm.getDetailid29(i);
					selecttedresourceid[i][29]=cLRForm.getResourceid29(i);
					laborrates[i][29]=cLRForm.getClrrate29(i);
				}
			}
			getTechEngtypes=CustomerLaborRatesdao.getTechEngtypes(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
			request.setAttribute("TechEngtypeslist",getTechEngtypes);
			
			resourcelevellist=CustomerLaborRatesdao.getResourcelevellist(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
			request.setAttribute("resourcelevellist",resourcelevellist);
			
			String stringi="";
			
			for(int j=0;j<totalcategories;j++)
			{
				for(int i=0;i<(totalresources);i++)
				{
					stringi=stringi+detaillistid[j][i]+"|"+selecttedresourceid[j][i]+"|"+laborrates[j][i]+",";
				}
				
				if(j==(totalcategories-1))
				{
					stringi=stringi.substring(0,stringi.length()-1);
				}
			}
			
			if(type.equals("AM")||type.equals("AM_EDIT"))
			{
				//statusvalue=CustomerLaborRatesdao.updateCLRates(null,stringi,Float.parseFloat(cLRForm.getMinimumfee()),Float.parseFloat(cLRForm.getTravel()),Float.parseFloat(cLRForm.getTravelmax()),Float.parseFloat(cLRForm.getDiscount()),cLRForm.getEffectivedate(),cLRForm.getRules(),"A",getDataSource( request , "ilexnewDB" ));
				statusvalue=CustomerLaborRatesdao.updateCLRates(null,stringi,Float.parseFloat(cLRForm.getMinimumfee()),cLRForm.getTravel(),Float.parseFloat(cLRForm.getTravelmax()),Float.parseFloat(cLRForm.getDiscount()),cLRForm.getEffectivedate(),cLRForm.getRules(),"A", null, getDataSource( request , "ilexnewDB" ));
				clrdetail=CustomerLaborRatesdao.getclrdetail(null,getDataSource( request , "ilexnewDB" ));
				cLRForm.setMinimumfee(clrdetail[0]);
				cLRForm.setTravel(clrdetail[1]);
				cLRForm.setTravelmax(clrdetail[2]);
				cLRForm.setDiscount(clrdetail[3]);
				cLRForm.setEffectivedate(clrdetail[4]);
				cLRForm.setRules(clrdetail[5]);
			}
			else
			{
				//statusvalue=CustomerLaborRatesdao.updateCLRates(MSA_Id,stringi,Float.parseFloat(cLRForm.getMinimumfee()),Float.parseFloat(cLRForm.getTravel()),Float.parseFloat(cLRForm.getTravelmax()),Float.parseFloat(cLRForm.getDiscount()),cLRForm.getEffectivedate(),cLRForm.getRules(),"A",getDataSource( request , "ilexnewDB" ));
				statusvalue=CustomerLaborRatesdao.updateCLRates(MSA_Id,stringi,Float.parseFloat(cLRForm.getMinimumfee()),cLRForm.getTravel(),Float.parseFloat(cLRForm.getTravelmax()),Float.parseFloat(cLRForm.getDiscount()),cLRForm.getEffectivedate(),cLRForm.getRules(),"A", cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
				clrdetail=CustomerLaborRatesdao.getclrdetail(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
				cLRForm.setMinimumfee(clrdetail[0]);
				cLRForm.setTravel(clrdetail[1]);
				cLRForm.setTravelmax(clrdetail[2]);
				cLRForm.setDiscount(clrdetail[3]);
				cLRForm.setEffectivedate(clrdetail[4]);
				cLRForm.setRules(clrdetail[5]);
			}
			
			request.setAttribute("addmessage",statusvalue+"");
			if(type.equals("AM"))
			{
				currentstatus=CustomerLaborRatesdao.getActivitylibstatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				list = Menu.getStatus( "AM" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute("list",list);
				sptype="a";
				
			}
			if(type.equals("AM_EDIT"))
			{
				currentstatus=CustomerLaborRatesdao.getActivitylibstatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				list = Menu.getStatus( "AM_EDIT" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute("list",list);
				sptype="a";
				
			}
			if(type.equals("PM"))
			{
				currentstatus=CustomerLaborRatesdao.getActivitystatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				
				list = Menu.getStatus( "PM" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute( "list" , list );
				Job_Id=CustomerLaborRatesdao.getJobid(getDataSource( request , "ilexnewDB" ),Activitylibrary_Id);
				jobName = Activitydao.getJobname( getDataSource( request , "ilexnewDB" ) , Job_Id );
				
				request.setAttribute( "Job_Id" , Job_Id );
				request.setAttribute("jobName", jobName);
				sptype="p";
				
			}
			
			//CLRlist = CustomerLaborRatesdao.getCLRates(MSA_Id,"0",totalresources,getDataSource( request , "ilexnewDB" ));
			/* for discounted rates  start*/
			if(type.equals("AM")||type.equals("AM_EDIT"))
			{
				CLRlist = CustomerLaborRatesdao.getDiscountedCLRates(null,"0",totalresources,getDataSource( request , "ilexnewDB" ));
			}
			else
			{
				CLRlist = CustomerLaborRatesdao.getDiscountedCLRates(cLRForm.getCustomerid(),"0",totalresources,getDataSource( request , "ilexnewDB" ));
			}
			
			/* for discounted rates  end*/
			request.setAttribute("ratelist",CLRlist);
			request.setAttribute("listsize",CLRlist.size()+"");
			editflag=1;
			request.setAttribute("editflag",editflag+"");
		}
		else
		{
		
			Activitylibrary_Id=request.getParameter("Activitylibrary_Id");
			cLRForm.setActivitylibrary_Id(Activitylibrary_Id);
			request.setAttribute("Activitylibrary_Id",Activitylibrary_Id);

			MSA_Id=request.getParameter("MSA_Id");
			cLRForm.setMsa_Id(MSA_Id);
			request.setAttribute("MSA_Id",MSA_Id);
			
			type=request.getParameter("type");
			cLRForm.setType(type);
			
//			totalresources=CustomerLaborRatesdao.gettotalresources(MSA_Id,getDataSource( request , "ilexnewDB" ));
//			request.setAttribute("totalresources",totalresources+"");
						
			if(request.getParameter("type").equals("AM"))
			{
				cLRForm.setCustomerid(MSA_Id);
				customername=CustomerLaborRatesdao.getMsaname(getDataSource( request , "ilexnewDB" ),MSA_Id);
				cLRForm.setCustomername(customername);
				currentstatus=CustomerLaborRatesdao.getActivitylibstatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				list = Menu.getStatus( "AM" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute("list",list);
								
				sptype="a";
				editflag=1;
				
			}
			if(request.getParameter("type").equals("PM"))
			{
				//Activitylibrary_Id is Activity_Id here
				//customerid is appendix_id but customername is msaname
				Job_Id=CustomerLaborRatesdao.getJobid(getDataSource( request , "ilexnewDB" ),Activitylibrary_Id);
				jobName = Activitydao.getJobname( getDataSource( request , "ilexnewDB" ) , Job_Id );
				customerid=CustomerLaborRatesdao.getAppendixid(getDataSource( request , "ilexnewDB" ),Job_Id);
				MSA_Id=CustomerLaborRatesdao.getMsaid(getDataSource( request , "ilexnewDB" ),customerid);
				cLRForm.setCustomerid(customerid);
				customername=CustomerLaborRatesdao.getMsaname(getDataSource( request , "ilexnewDB" ),MSA_Id);
				cLRForm.setCustomername(customername);
				currentstatus=CustomerLaborRatesdao.getActivitystatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				list = Menu.getStatus( "PM" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute( "list" , list );
				
				sptype="p";
				
			}
			
			if(request.getParameter("type").equals("AM_EDIT"))
			{
				//customerid=CustomerLaborRatesdao.getNetmedxorgid(getDataSource( request , "ilexnewDB" ));
				cLRForm.setCustomerid(MSA_Id);
				//customername=CustomerLaborRatesdao.getOrganizationtype(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
				customername=CustomerLaborRatesdao.getMsaname(getDataSource( request , "ilexnewDB" ),MSA_Id);
				cLRForm.setCustomername(customername);
				currentstatus=CustomerLaborRatesdao.getActivitylibstatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				list = Menu.getStatus( "AM_EDIT" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute("list",list);
				
				
				sptype="a";
				
			}
			
			totalresources=CustomerLaborRatesdao.gettotalresources(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
			request.setAttribute("totalresources",totalresources+"");
			
			if(request.getParameter("type").equals("AM")||request.getParameter("type").equals("AM_EDIT"))
			{
				
				if(request.getParameter("type").equals("AM"))
				{
					
					CLRlist = CustomerLaborRatesdao.getDiscountedCLRates(null,"0",totalresources,getDataSource( request , "ilexnewDB" ));
				}
				else
				{
					
					CLRlist = CustomerLaborRatesdao.getCLRates(null,"0",totalresources,getDataSource( request , "ilexnewDB" ));
				}
				
				clrdetail=CustomerLaborRatesdao.getclrdetail(null,getDataSource( request , "ilexnewDB" ));
				cLRForm.setMinimumfee(clrdetail[0]);
				cLRForm.setTravel(clrdetail[1]);
				cLRForm.setTravelmax(clrdetail[2]);
				cLRForm.setDiscount(clrdetail[3]);
				cLRForm.setEffectivedate(clrdetail[4]);
				cLRForm.setRules(clrdetail[5]);
			}
			else
			{
				CLRlist = CustomerLaborRatesdao.getCLRates(cLRForm.getCustomerid(),"0",totalresources,getDataSource( request , "ilexnewDB" ));
				clrdetail=CustomerLaborRatesdao.getclrdetail(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
				cLRForm.setMinimumfee(clrdetail[0]);
				cLRForm.setTravel(clrdetail[1]);
				cLRForm.setTravelmax(clrdetail[2]);
				cLRForm.setDiscount(clrdetail[3]);
				cLRForm.setEffectivedate(clrdetail[4]);
				cLRForm.setRules(clrdetail[5]);
			}
					
			request.setAttribute( "Job_Id" , Job_Id );
			request.setAttribute("ratelist",CLRlist);
			request.setAttribute("listsize",CLRlist.size()+"");
			request.setAttribute("editflag",editflag+"");
			request.setAttribute("jobName", jobName);
		}
		
		if(statusvalue!=0)
		{
			editflag=0;
			request.setAttribute("editflag",editflag+"");
		}
		
		getTechEngtypes=CustomerLaborRatesdao.getTechEngtypes(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("TechEngtypeslist",getTechEngtypes);
		
		resourcelevellist=CustomerLaborRatesdao.getResourcelevellist(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("resourcelevellist",resourcelevellist);
		
		/* for link field start */
		int showlink=0;
		request.setAttribute("showlink",showlink+"");
		/* for link field end */
		forward=mapping.findForward("success");
		return (forward);

	}
	
	
	
	public ActionForward delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		CustomerLaborRatesForm cLRForm = (CustomerLaborRatesForm) form;
		ArrayList CLRlist = new ArrayList();
		ArrayList resourcelevellist = new ArrayList();
		ArrayList nooftechengglist=new ArrayList();
		String[] clrdetail=new String[6];
		
		
		ArrayList getTechEngtypes = new ArrayList();
		String technos="0";
		String enggnos="0";
		String customerid="";
		String customername="";
		String netmedxcatgid="0";
		String Activitylibrary_Id="0";
		String list = "";
		String type = "";
		String MSA_Id = "";
		String Job_Id = "";
		String Appendix_Id = "";
		String sptype = "";
		String currentstatus = "";
		int statusvalue=0;
		int editflag=0;
		int totaltechenggs=0;
		int totalresources=0;
		String catid="0";
		String jobName = "";
		
		
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		/* for security */
		String temp_fu_type = "";
		String temp_fu_name = "";
		if(request.getParameter( "type" ) != null) 
		{
			if(request.getParameter( "type" ).equals("PM"))
			{ 										//security para for Send PO - WO 
				temp_fu_type = "Manage Customer Labor Rates"; 
				temp_fu_name = "Resource";
			}
			
			if( request.getParameter( "type" ).equals( "AM" ))
			{
				temp_fu_type = "View Customer Labor Rates"; 									//will be changed later use for testing
				temp_fu_name = "Labor";
				
			}
			if( request.getParameter( "type" ).equals( "AM_EDIT" ) )
			{
				temp_fu_type = "Add Customer Labor Rates"; 									//will be changed later use for testing
				temp_fu_name = "Labor";
				
			}
		}
		
		
		/* Page Security Start*/
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*if( cLRForm.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( userid, temp_fu_name,temp_fu_type,  getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}*/
		/* Page Security End*/
		
		
		Activitylibrary_Id=request.getParameter("Activitylibrary_Id");
		cLRForm.setActivitylibrary_Id(Activitylibrary_Id);
		request.setAttribute("Activitylibrary_Id",Activitylibrary_Id);
		MSA_Id=request.getParameter("MSA_Id");
		cLRForm.setMsa_Id(MSA_Id);
		request.setAttribute("MSA_Id",MSA_Id);
		type=request.getParameter("type");
		cLRForm.setType(type);
		request.setAttribute("type",type);
		
		
		//Activitylibrary_Id is Activity_Id here
		//customerid is appendix_id but customername is msaname
		Job_Id=CustomerLaborRatesdao.getJobid(getDataSource( request , "ilexnewDB" ),Activitylibrary_Id);
		jobName = Activitydao.getJobname( getDataSource( request , "ilexnewDB" ) , Job_Id );
		customerid=CustomerLaborRatesdao.getAppendixid(getDataSource( request , "ilexnewDB" ),Job_Id);
		MSA_Id=CustomerLaborRatesdao.getMsaid(getDataSource( request , "ilexnewDB" ),customerid);
		cLRForm.setCustomerid(customerid);
		customername=CustomerLaborRatesdao.getMsaname(getDataSource( request , "ilexnewDB" ),MSA_Id);
		cLRForm.setCustomername(customername);
		currentstatus=CustomerLaborRatesdao.getActivitystatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
		list = Menu.getStatus( "PM" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "list" , list );
		
		sptype="p";
		
		
		//statusvalue=CustomerLaborRatesdao.updateCLRates(MSA_Id,null,0,0,0,0,null,null,"D",getDataSource( request , "ilexnewDB" ));
		statusvalue=CustomerLaborRatesdao.updateCLRates(MSA_Id,null,0,null,0,0,null,null,"D",cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		
		//CLRlist = CustomerLaborRatesdao.getCLRates(MSA_Id,"0",getDataSource( request , "ilexnewDB" ));
		clrdetail=CustomerLaborRatesdao.getclrdetail(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		cLRForm.setMinimumfee(clrdetail[0]);
		cLRForm.setTravel(clrdetail[1]);
		cLRForm.setTravelmax(clrdetail[2]);
		cLRForm.setDiscount(clrdetail[3]);
		cLRForm.setEffectivedate(clrdetail[4]);
		cLRForm.setRules(clrdetail[5]);
		
		totalresources=CustomerLaborRatesdao.gettotalresources(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("totalresources",totalresources+"");
		
		if(statusvalue==0)
		{
			CLRlist = CustomerLaborRatesdao.getCLRates(null,"0",totalresources,getDataSource( request , "ilexnewDB" ));
		}
		else
		{
			CLRlist = CustomerLaborRatesdao.getCLRates(cLRForm.getCustomerid(),"0",totalresources,getDataSource( request , "ilexnewDB" ));
		}
		
		
	
		netmedxcatgid=CustomerLaborRatesdao.getNetmedxcatgid(getDataSource( request , "ilexnewDB" ));
		request.setAttribute("netmedxcatgid",netmedxcatgid);
		request.setAttribute( "Job_Id" , Job_Id );
		request.setAttribute("ratelist",CLRlist);
		request.setAttribute("listsize",CLRlist.size()+"");
		request.setAttribute("editflag","0");
		request.setAttribute("deletemessage",statusvalue+"");
		request.setAttribute("jobName", jobName);

		
		
		getTechEngtypes=CustomerLaborRatesdao.getTechEngtypes(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("TechEngtypeslist",getTechEngtypes);

		resourcelevellist=CustomerLaborRatesdao.getResourcelevellist(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("resourcelevellist",resourcelevellist);
		
		cLRForm.setRefclr("add");
		/* for link field start */
		int showlink=0;
		request.setAttribute("showlink",showlink+"");
		/* for link field end */
		forward=mapping.findForward("success");
		return (forward);
	}
	

	public ActionForward viewDiscountRates ( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		CustomerLaborRatesForm cLRForm = (CustomerLaborRatesForm) form;
		ArrayList CLRlist = new ArrayList();
		ArrayList resourcelevellist = new ArrayList();
		ArrayList nooftechengglist=new ArrayList();
		String[] clrdetail=new String[6];
		
		
		ArrayList getTechEngtypes = new ArrayList();
		String technos="0";
		String enggnos="0";
		String customerid="";
		String customername="";
		String netmedxcatgid="0";
		String Activitylibrary_Id="0";
		String list = "";
		String type = "";
		String MSA_Id = "";
		String Job_Id = "";
		String Appendix_Id = "";
		String sptype = "";
		String currentstatus = "";
		//int statusvalue=0;
		int editflag=0;
		int totaltechenggs=0;
		int totalresources=0;
		String catid="0";
		/* for adding extra fields in CLR Form start*/
		int categoriespercriticality=0;
		int totalcategories=0;
		/* for adding extra fields in CLR Form end*/
		
		String jobName = "";
		
		HttpSession session = request.getSession( true );
		String userid = ( String )session.getAttribute( "userid" );
		
		cLRForm.setRefclr(request.getParameter("refclr"));
			
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
			
		
		/* for adding extra fields in CLR Form start*/
		categoriespercriticality=CustomerLaborRatesdao.getCategoriesPerCriticality(getDataSource( request , "ilexnewDB" ));
		request.setAttribute("categoriespercriticality",categoriespercriticality+"");
		
		totalcategories=CustomerLaborRatesdao.getTotalCategories(getDataSource( request , "ilexnewDB" ));
		request.setAttribute("totalcategories",totalcategories+"");
		
		/* for adding extra fields in CLR Form end*/
		
		netmedxcatgid=CustomerLaborRatesdao.getNetmedxcatgid(getDataSource( request , "ilexnewDB" ));
		request.setAttribute("netmedxcatgid",netmedxcatgid);
		
			type=cLRForm.getType();
			request.setAttribute("type",type);
						
			MSA_Id=cLRForm.getMsa_Id();
			request.setAttribute("MSA_Id",MSA_Id);
			
			Activitylibrary_Id=cLRForm.getActivitylibrary_Id();
			request.setAttribute("Activitylibrary_Id",Activitylibrary_Id);
			
			customerid = cLRForm.getCustomerid();
			customername = cLRForm.getCustomername();
			
			cLRForm.setType(type);
			cLRForm.setMsa_Id(MSA_Id);
			cLRForm.setActivitylibrary_Id(Activitylibrary_Id);
			cLRForm.setCustomerid(customerid);
			cLRForm.setCustomername(customername);
						
			totalresources=CustomerLaborRatesdao.gettotalresources(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
			request.setAttribute("totalresources",totalresources+"");
			
			
			String[][] laborrates = new String[totalcategories][30];
			String[][] detaillistid=new String[totalcategories][30];
			String[][] selecttedresourceid=new String[totalcategories][30];
			
			for(int i=0;i<totalcategories;i++)
			{
				if(totalresources >= 1)
				{
					detaillistid[i][0]=cLRForm.getDetailid0(i);
					selecttedresourceid[i][0]=cLRForm.getResourceid0(i);
					laborrates[i][0]=cLRForm.getClrrate0(i);
					
				}
				
				if(totalresources >= 2)
				{
					detaillistid[i][1]=cLRForm.getDetailid1(i);
					selecttedresourceid[i][1]=cLRForm.getResourceid1(i);
					laborrates[i][1]=cLRForm.getClrrate1(i);
					
				}
				
				if(totalresources >= 3)
				{
					detaillistid[i][2]=cLRForm.getDetailid2(i);
					selecttedresourceid[i][2]=cLRForm.getResourceid2(i);
					laborrates[i][2]=cLRForm.getClrrate2(i);
					
				}
				
				if(totalresources >= 4)
				{
					detaillistid[i][3]=cLRForm.getDetailid3(i);
					selecttedresourceid[i][3]=cLRForm.getResourceid3(i);
					laborrates[i][3]=cLRForm.getClrrate3(i);
					
				}
				
				if(totalresources >= 5)
				{
					detaillistid[i][4]=cLRForm.getDetailid4(i);
					selecttedresourceid[i][4]=cLRForm.getResourceid4(i);
					laborrates[i][4]=cLRForm.getClrrate4(i);
					
				}
				
				if(totalresources >= 6)
				{
					detaillistid[i][5]=cLRForm.getDetailid5(i);
					selecttedresourceid[i][5]=cLRForm.getResourceid5(i);
					laborrates[i][5]=cLRForm.getClrrate5(i);
					
				}
				
				if(totalresources >= 7)
				{
					detaillistid[i][6]=cLRForm.getDetailid6(i);
					selecttedresourceid[i][6]=cLRForm.getResourceid6(i);
					laborrates[i][6]=cLRForm.getClrrate6(i);
					
				}
				
				if(totalresources >= 8)
				{
					detaillistid[i][7]=cLRForm.getDetailid7(i);
					selecttedresourceid[i][7]=cLRForm.getResourceid7(i);
					laborrates[i][7]=cLRForm.getClrrate7(i);
					
				}
				
				if(totalresources >= 9)
				{
					detaillistid[i][8]=cLRForm.getDetailid8(i);
					selecttedresourceid[i][8]=cLRForm.getResourceid8(i);
					laborrates[i][8]=cLRForm.getClrrate8(i);
					
				}
				
				if(totalresources >= 10)
				{
					detaillistid[i][9]=cLRForm.getDetailid9(i);
					selecttedresourceid[i][9]=cLRForm.getResourceid9(i);
					laborrates[i][9]=cLRForm.getClrrate9(i);
					
				}
				
				if(totalresources >= 11)
				{
					detaillistid[i][10]=cLRForm.getDetailid10(i);
					selecttedresourceid[i][10]=cLRForm.getResourceid10(i);
					laborrates[i][10]=cLRForm.getClrrate10(i);
					
				}
				
				if(totalresources >= 12)
				{
					detaillistid[i][11]=cLRForm.getDetailid11(i);
					selecttedresourceid[i][11]=cLRForm.getResourceid11(i);
					laborrates[i][11]=cLRForm.getClrrate11(i);
					
				}
				
				if(totalresources >= 13)
				{
					detaillistid[i][12]=cLRForm.getDetailid12(i);
					selecttedresourceid[i][12]=cLRForm.getResourceid12(i);
					laborrates[i][12]=cLRForm.getClrrate12(i);
					
				}
				
				if(totalresources >= 14)
				{
					detaillistid[i][13]=cLRForm.getDetailid13(i);
					selecttedresourceid[i][13]=cLRForm.getResourceid13(i);
					laborrates[i][13]=cLRForm.getClrrate13(i);
					
				}
				
				if(totalresources >= 15)
				{
					detaillistid[i][14]=cLRForm.getDetailid14(i);
					selecttedresourceid[i][14]=cLRForm.getResourceid14(i);
					laborrates[i][14]=cLRForm.getClrrate14(i);
					
				}
				
				if(totalresources >= 16)
				{
					detaillistid[i][15]=cLRForm.getDetailid15(i);
					selecttedresourceid[i][15]=cLRForm.getResourceid15(i);
					laborrates[i][15]=cLRForm.getClrrate15(i);
					
				}
				if(totalresources >= 17)
				{
					detaillistid[i][16]=cLRForm.getDetailid16(i);
					selecttedresourceid[i][16]=cLRForm.getResourceid16(i);
					laborrates[i][16]=cLRForm.getClrrate16(i);
					
				}
				if(totalresources >= 18)
				{
					detaillistid[i][17]=cLRForm.getDetailid17(i);
					selecttedresourceid[i][17]=cLRForm.getResourceid17(i);
					laborrates[i][17]=cLRForm.getClrrate17(i);
					
				}
				if(totalresources >= 19)
				{
					detaillistid[i][18]=cLRForm.getDetailid18(i);
					selecttedresourceid[i][18]=cLRForm.getResourceid18(i);
					laborrates[i][18]=cLRForm.getClrrate18(i);
					
				}
				if(totalresources >= 20)
				{
					detaillistid[i][19]=cLRForm.getDetailid19(i);
					selecttedresourceid[i][19]=cLRForm.getResourceid19(i);
					laborrates[i][19]=cLRForm.getClrrate19(i);
					
				}
				if(totalresources >= 21)
				{
					detaillistid[i][20]=cLRForm.getDetailid20(i);
					selecttedresourceid[i][20]=cLRForm.getResourceid20(i);
					laborrates[i][20]=cLRForm.getClrrate20(i);
					
				}
				if(totalresources >= 22)
				{
					detaillistid[i][21]=cLRForm.getDetailid21(i);
					selecttedresourceid[i][21]=cLRForm.getResourceid21(i);
					laborrates[i][21]=cLRForm.getClrrate21(i);
					
				}
				if(totalresources >= 23)
				{
					detaillistid[i][22]=cLRForm.getDetailid22(i);
					selecttedresourceid[i][22]=cLRForm.getResourceid22(i);
					laborrates[i][22]=cLRForm.getClrrate22(i);
					
				}
				if(totalresources >= 24)
				{
					detaillistid[i][23]=cLRForm.getDetailid23(i);
					selecttedresourceid[i][23]=cLRForm.getResourceid23(i);
					laborrates[i][23]=cLRForm.getClrrate23(i);
					
				}
				if(totalresources >= 25)
				{
					detaillistid[i][24]=cLRForm.getDetailid24(i);
					selecttedresourceid[i][24]=cLRForm.getResourceid24(i);
					laborrates[i][24]=cLRForm.getClrrate24(i);
					
				}
				if(totalresources >= 26)
				{
					detaillistid[i][25]=cLRForm.getDetailid25(i);
					selecttedresourceid[i][25]=cLRForm.getResourceid25(i);
					laborrates[i][25]=cLRForm.getClrrate25(i);
					
				}
				if(totalresources >= 27)
				{
					detaillistid[i][26]=cLRForm.getDetailid26(i);
					selecttedresourceid[i][26]=cLRForm.getResourceid26(i);
					laborrates[i][26]=cLRForm.getClrrate26(i);
					
				}
				if(totalresources >= 28)
				{
					detaillistid[i][27]=cLRForm.getDetailid27(i);
					selecttedresourceid[i][27]=cLRForm.getResourceid27(i);
					laborrates[i][27]=cLRForm.getClrrate27(i);
					
				}
				if(totalresources >= 29)
				{
					detaillistid[i][28]=cLRForm.getDetailid28(i);
					selecttedresourceid[i][28]=cLRForm.getResourceid28(i);
					laborrates[i][28]=cLRForm.getClrrate28(i);
					
				}
				if(totalresources >= 30)
				{
					detaillistid[i][29]=cLRForm.getDetailid29(i);
					selecttedresourceid[i][29]=cLRForm.getResourceid29(i);
					laborrates[i][29]=cLRForm.getClrrate29(i);
					
				}
				
			}
			
			getTechEngtypes=CustomerLaborRatesdao.getTechEngtypes(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
			request.setAttribute("TechEngtypeslist",getTechEngtypes);
			
			
			
			resourcelevellist=CustomerLaborRatesdao.getResourcelevellist(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
			request.setAttribute("resourcelevellist",resourcelevellist);
			
			String stringi="";
			
			for(int j=0;j<totalcategories;j++)
			{
				for(int i=0;i<(totalresources);i++)
				{
					stringi=stringi+detaillistid[j][i]+"|"+selecttedresourceid[j][i]+"|"+laborrates[j][i]+",";
				}
				
				
				if(j==(totalcategories-1))
				{
					stringi=stringi.substring(0,stringi.length()-1);
				}
				
			}
			
			if(type.equals("AM")||type.equals("AM_EDIT"))
			{
				//statusvalue=CustomerLaborRatesdao.updateCLRates(null,stringi,Float.parseFloat(cLRForm.getMinimumfee()),Float.parseFloat(cLRForm.getTravel()),Float.parseFloat(cLRForm.getTravelmax()),Float.parseFloat(cLRForm.getDiscount()),cLRForm.getEffectivedate(),cLRForm.getRules(),"A",getDataSource( request , "ilexnewDB" ));
				//statusvalue=CustomerLaborRatesdao.updateCLRates(null,stringi,Float.parseFloat(cLRForm.getMinimumfee()),cLRForm.getTravel(),Float.parseFloat(cLRForm.getTravelmax()),Float.parseFloat(cLRForm.getDiscount()),cLRForm.getEffectivedate(),cLRForm.getRules(),"A",getDataSource( request , "ilexnewDB" ));
				clrdetail=CustomerLaborRatesdao.getclrdetail(null,getDataSource( request , "ilexnewDB" ));
				cLRForm.setMinimumfee(clrdetail[0]);
				cLRForm.setTravel(clrdetail[1]);
				cLRForm.setTravelmax(clrdetail[2]);
				cLRForm.setDiscount(clrdetail[3]);
				cLRForm.setEffectivedate(clrdetail[4]);
				cLRForm.setRules(clrdetail[5]);
			}
			else
			{
				//statusvalue=CustomerLaborRatesdao.updateCLRates(MSA_Id,stringi,Float.parseFloat(cLRForm.getMinimumfee()),Float.parseFloat(cLRForm.getTravel()),Float.parseFloat(cLRForm.getTravelmax()),Float.parseFloat(cLRForm.getDiscount()),cLRForm.getEffectivedate(),cLRForm.getRules(),"A",getDataSource( request , "ilexnewDB" ));
				//statusvalue=CustomerLaborRatesdao.updateCLRates(MSA_Id,stringi,Float.parseFloat(cLRForm.getMinimumfee()),cLRForm.getTravel(),Float.parseFloat(cLRForm.getTravelmax()),Float.parseFloat(cLRForm.getDiscount()),cLRForm.getEffectivedate(),cLRForm.getRules(),"A",getDataSource( request , "ilexnewDB" ));
				clrdetail=CustomerLaborRatesdao.getclrdetail(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
				cLRForm.setMinimumfee(clrdetail[0]);
				cLRForm.setTravel(clrdetail[1]);
				cLRForm.setTravelmax(clrdetail[2]);
				cLRForm.setDiscount(clrdetail[3]);
				cLRForm.setEffectivedate(clrdetail[4]);
				cLRForm.setRules(clrdetail[5]);
			}
			
			
			if(type.equals("AM"))
			{
				currentstatus=CustomerLaborRatesdao.getActivitylibstatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				list = Menu.getStatus( "AM" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute("list",list);
				sptype="a";
				
			}
			if(type.equals("AM_EDIT"))
			{
				currentstatus=CustomerLaborRatesdao.getActivitylibstatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				list = Menu.getStatus( "AM_EDIT" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute("list",list);
				sptype="a";
				
			}
			if(type.equals("PM"))
			{
				currentstatus=CustomerLaborRatesdao.getActivitystatus(Activitylibrary_Id,getDataSource( request , "ilexnewDB" ));
				
				list = Menu.getStatus( "PM" ,currentstatus.charAt( 0 ) , MSA_Id , "" , Activitylibrary_Id , "" ,  userid , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute( "list" , list );
				Job_Id=CustomerLaborRatesdao.getJobid(getDataSource( request , "ilexnewDB" ),Activitylibrary_Id);
				jobName = Activitydao.getJobname( getDataSource( request , "ilexnewDB" ) , Job_Id );
				request.setAttribute( "Job_Id" , Job_Id );
				request.setAttribute("jobName", jobName);
				sptype="p";
				
			}
			
			//CLRlist = CustomerLaborRatesdao.getCLRates(MSA_Id,"0",totalresources,getDataSource( request , "ilexnewDB" ));
			/* for discounted rates  start*/
			if(type.equals("AM")||type.equals("AM_EDIT"))
			{
				CLRlist = CustomerLaborRatesdao.getDiscountedCLRates(null,"0",totalresources,getDataSource( request , "ilexnewDB" ));
			}
			else
			{
				CLRlist = CustomerLaborRatesdao.getDiscountedCLRates(cLRForm.getCustomerid(),"0",totalresources,getDataSource( request , "ilexnewDB" ));
			}
			
			/* for discounted rates  end*/
			request.setAttribute("ratelist",CLRlist);
			request.setAttribute("listsize",CLRlist.size()+"");
			editflag=1;
			request.setAttribute("editflag",editflag+"");
		
		
		
		
		getTechEngtypes=CustomerLaborRatesdao.getTechEngtypes(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("TechEngtypeslist",getTechEngtypes);
		
		resourcelevellist=CustomerLaborRatesdao.getResourcelevellist(cLRForm.getCustomerid(),getDataSource( request , "ilexnewDB" ));
		request.setAttribute("resourcelevellist",resourcelevellist);
		
		cLRForm.setRefclr("add");
		forward=mapping.findForward("success");
		int showlink=1;
		request.setAttribute("showlink",showlink+"");
		return (forward);

	}
	

}
