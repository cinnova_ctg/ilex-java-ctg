/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used to manage freight resource of an activity     
*
*/

package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.FreightResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.formbean.AM.FreightResourceForm;

/*
 * Methods : View , Refresh , Submit , Delete
 */


public class FreightResourceAction extends com.mind.common.IlexDispatchAction
{
	
/** This method is used to list all the freight resources used in an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		String Activitylibrary_Id = "";
		int resource_size = 0;
		String sortqueryclause = "";
		ArrayList freightresourcelist = new ArrayList();
		String status = "";
		String activityname = "";
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
			
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			freightresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end
		
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			freightresourceform.setActivity_Id( Activitylibrary_Id );
		}
		else
		{
			Activitylibrary_Id = freightresourceform.getActivity_Id();
		}
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activitylibrary_Id );
		freightresourceform.setFreightlist( ActivityLibrarydao.getMateriallist( "freight" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		freightresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "F" , "%" , sortqueryclause ,request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setActivity_Id( idname.getId() );
		freightresourceform.setActivity_name( idname.getName() );
		
		/* for checkbox check start */
		
		String checkTempResources[] = ActivityLibrarydao.checkTempResourcesAM( Activitylibrary_Id , "F" , "%" , sortqueryclause ,  request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setCheck( checkTempResources );
		/* for checkbox check end */
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "true" );
		return( mapping.findForward( "freightresourcetabularpage" ) );
	}
	

	
/** This method is used when the page is refreshed to populate form according to the value selected from the combo 
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/		

	public ActionForward Refresh( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		String Activitylibrary_Id = "";
		String newname = "";
		String newnamecombo = "";
		String newfreightid = "";
		int resource_size = 0;
		String status = "";
		FreightResource freightresource = new FreightResource();
		int checklength = 0;
		ArrayList temp = new ArrayList();
		String[] checkrefresh = null;
		ArrayList freightresourcelist = new ArrayList();
		String activityname = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		NameId idname = null;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/* by hamid for back button start*/
		if(request.getParameter("MSA_Id")!=null)
		{
			freightresourceform.setMSA_Id(request.getParameter("MSA_Id"));
		}
		/* by hamid for back button end*/
		Activitylibrary_Id = freightresourceform.getActivity_Id();
		
		newname = freightresourceform.getNewfreightname();
		newnamecombo = freightresourceform.getNewfreightnamecombo();
		newfreightid = freightresourceform.getNewfreightid();
		
		if( freightresourceform.getCheck()!=  null )
		{
			checklength = freightresourceform.getCheck().length;	 
			checkrefresh = freightresourceform.getCheck();
			
			int []tempindex = new int[checklength];
			int k = 0;
				
			if( checklength > 0 )
			{	int length = freightresourceform.getFreightid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( freightresourceform.getCheck( i ).equals( freightresourceform.getFreightid( j ) ) )
						{	
							tempindex[k] = j;
							k++;
						}
					}
				}
			}	
			
			for( int i = 0; i < checklength; i++ )
			{
				FreightResource tempresource = new FreightResource();
				
				tempresource.setActivity_Id( Activitylibrary_Id );
				
				tempresource.setFreightid( freightresourceform.getFreightid( tempindex[i] ) );
				tempresource.setFreightcostlibid( freightresourceform.getFreightcostlibid( tempindex[i] ) );
				
				tempresource.setFreightname( freightresourceform.getFreightname( tempindex[i] ) );
				tempresource.setFreighttype( freightresourceform.getFreighttype( tempindex[i] ) );
				
				tempresource.setCnspartnumber( freightresourceform.getCnspartnumber( tempindex[i] ) );
				tempresource.setEstimatedtotalcost( freightresourceform.getEstimatedtotalcost( tempindex[i] ) );
				tempresource.setEstimatedunitcost( freightresourceform.getEstimatedunitcost( tempindex[i] ) );
				tempresource.setMinimumquantity( freightresourceform.getMinimumquantity( tempindex[i] ) );
				tempresource.setPriceextended( freightresourceform.getPriceextended( tempindex[i] ) );
				tempresource.setPriceunit( freightresourceform.getPriceunit( tempindex[i] ) );
				tempresource.setProformamargin( freightresourceform.getProformamargin( tempindex[i] ) );
				
				tempresource.setQuantity( freightresourceform.getQuantity( tempindex[i] ) );
				tempresource.setPrevquantity( freightresourceform.getPrevquantity( tempindex[i] ) );
				
				tempresource.setSellablequantity( freightresourceform.getSellablequantity( tempindex[i] ) );
				tempresource.setStatus( freightresourceform.getStatus( tempindex[i] ) );
				
				
				temp.add( tempresource );
			}	
		}
		
		freightresource = ActivityLibrarydao.getFreightResource(  freightresourceform.getNewfreightnamecombo() , getDataSource( request , "ilexnewDB" ) );
		
		freightresourceform.reset( mapping , request);
		
		freightresourceform.setNewfreightid( newfreightid );
		freightresourceform.setNewfreightname( newname );
		freightresourceform.setNewfreightnamecombo( newnamecombo );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setNewfreighttype( freightresource.getFreighttype() );
		
		freightresourceform.setNewcnspartnumber( freightresource.getCnspartnumber() );
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		
		freightresourceform.setNewestimatedunitcost( freightresource.getEstimatedunitcost() );
		freightresourceform.setNewsellablequantity( freightresource.getSellablequantity() );
		freightresourceform.setNewminimumquantity( freightresource.getMinimumquantity() );
		freightresourceform.setRef( null );
		freightresourceform.setActivity_Id( Activitylibrary_Id );
		freightresourceform.setFreightlist( ActivityLibrarydao.getMateriallist( "freight" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );
		
		freightresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "F" , "%" , "" , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setActivity_Id( idname.getId() );
		freightresourceform.setActivity_name( idname.getName() );
		
		if( checklength > 0 )
		{
			if( resource_size > 0 )
			{
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < resource_size; j++ )
					{
						if( ( ( FreightResource )temp.get( i ) ).getFreightid().equals( ( ( FreightResource )freightresourcelist.get( j ) ).getFreightid() ) )
						{
							freightresourcelist.set( j ,  temp.get( i ) );
						}
					}
				}
				freightresourceform.setCheck( checkrefresh );
			}
			
		}
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
	
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "false" );
		return( mapping.findForward( "freightresourcetabularpage" ) );
	}
	

	
/** This method is used to add/update freight resource to an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		String Activitylibrary_Id = "";
		FreightResource freightresource = null;
		int resource_size = 0;
		ArrayList freightresourcelist = new ArrayList();
		String status = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String activityname = "";
		int addflag = 0;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		NameId idname = null;
		
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
			
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		/* by hamid for back button start*/
		if(request.getParameter("MSA_Id")!=null)
		{
			freightresourceform.setMSA_Id(request.getParameter("MSA_Id"));
		}
		/* by hamid for back button end*/
		
		Activitylibrary_Id = freightresourceform.getActivity_Id();
		
		if( freightresourceform.getCheck()!=  null )
		{
			int checklength = freightresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = freightresourceform.getFreightid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						
						if( freightresourceform.getCheck( i ).substring( 1 , freightresourceform.getCheck( i ).length() ).equals( freightresourceform.getFreightid( j ) ) )
						{	
							
							index[k] = j;
							k++;
						}
						/*if( freightresourceform.getCheck(i).equals( freightresourceform.getFreightid( j ) ) )
						{	
							index[k] = j;
							k++;
						}*/
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				int updateflag = 0;
				for( int i = 0; i < index.length; i++ )
				{
					freightresource = new FreightResource();
					freightresource.setFreightid( freightresourceform.getFreightid ( index[i] ) );
					freightresource.setFreightcostlibid( freightresourceform.getFreightcostlibid ( index[i] ) );
					freightresource.setActivity_Id( freightresourceform.getActivity_Id() );
					freightresource.setQuantity( freightresourceform.getQuantity ( index[i] ) );
					freightresource.setPrevquantity( freightresourceform.getPrevquantity( index[i] ) );
					freightresource.setFreighttype( "F" );
					freightresource.setEstimatedunitcost( freightresourceform.getEstimatedunitcost( index[i] ) );
					freightresource.setProformamargin( freightresourceform.getProformamargin( index[i] ) );
					/* for multiple resource select start*/
					freightresource.setCnspartnumber( freightresourceform.getCnspartnumber( index[i] ) );
					freightresource.setFlag( freightresourceform.getFlag( index[i] ) );
					if( freightresourceform.getFlag( index[i] ).equals( "P" ) )
					{
						updateflag = ActivityLibrarydao.updatefreightresource( freightresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "updateflag" , ""+updateflag );
					}
					if( freightresourceform.getFlag( index[i] ).equals( "T" ) )
					{
						updateflag = ActivityLibrarydao.addfreightresource( freightresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "addflag" , ""+addflag );
					}
					/* for multiple resource select end*/
					/*updateflag = ActivityLibrarydao.updatefreightresource( freightresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );*/
				}	
			}
		}

		
		if( freightresourceform.getNewfreightid() != null )
		{
			
			freightresource = new FreightResource();
			freightresource.setFreightid( freightresourceform.getNewfreightnamecombo() );
			freightresource.setActivity_Id( freightresourceform.getActivity_Id() );
			freightresource.setCnspartnumber( freightresourceform.getNewcnspartnumber() );
			freightresource.setQuantity( freightresourceform.getNewquantity() );
			freightresource.setFreighttype( "F" );
			freightresource.setEstimatedunitcost( freightresourceform.getNewestimatedunitcost() );
			freightresource.setProformamargin( freightresourceform.getNewproformamargin() );
			
			addflag = ActivityLibrarydao.addfreightresource( freightresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		freightresourceform.reset( mapping , request);
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activitylibrary_Id );
		
		freightresourceform.setRef( null );
		freightresourceform.setFreightlist( ActivityLibrarydao.getMateriallist( "freight" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		
		freightresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "F" , "%" , sortqueryclause , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setActivity_Id( idname.getId() );
		freightresourceform.setActivity_name( idname.getName() );
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "activityname" , activityname );
		return( mapping.findForward( "freightresourcetabularpage" ) );
	}
	
	
/** This method is used to delete freight resource from an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/		
	
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		FreightResource freightresource = null;
		String Activitylibrary_Id = "";
		int resource_size = 0;
		int deleteflag = 0;
		ArrayList freightresourcelist = new ArrayList();
		String status = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String activityname = "";
		NameId idname = null;
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		String sortqueryclause = "";
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			freightresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end*/
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			freightresourceform.setActivity_Id( Activitylibrary_Id );
		}
		else
		{
			Activitylibrary_Id = freightresourceform.getActivity_Id();
		}
		

		if( request.getParameter( "fromResourceTabular" )!= null )
		{
			if( freightresourceform.getCheck()!=  null )
			{
				
				int checklength = freightresourceform.getCheck().length;	 
		
				int []index = new int[checklength];
				int k = 0;
				
				if( checklength > 0 )
				{	int length = freightresourceform.getFreightid().length;
					for( int i = 0; i < checklength; i++ )
					{
						for( int j = 0; j < length; j++ )
						{
							if( freightresourceform.getCheck( i ).substring( 1 , freightresourceform.getCheck( i ).length() ).equals( freightresourceform.getFreightid( j ) ) )
							{	
								
								index[k] = j;
								k++;
							}
							
						}
					}
				}
			
				if( checklength > 0 )                               
				{
					
					for( int i = 0; i < index.length; i++ )
					{
						freightresource = new FreightResource();
						freightresource.setFreightid( freightresourceform.getFreightid ( index[i] ) );
						
						deleteflag = ActivityLibrarydao.deleteresource( freightresource.getFreightid() , getDataSource( request , "ilexnewDB" ) );
						
					}
				}
			}
		}
		else
		{

			deleteflag = ActivityLibrarydao.deleteresource( request.getParameter( "Freight_Id" ) , getDataSource( request , "ilexnewDB" ) );
		}
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activitylibrary_Id );
		freightresourceform.setFreightlist( ActivityLibrarydao.getMateriallist( "freight" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		freightresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "F" , "%" , sortqueryclause , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setActivity_Id( idname.getId() );
		freightresourceform.setActivity_name( idname.getName() );
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "true" );
		
		return( mapping.findForward( "freightresourcetabularpage" ) );	
	}		
	
	/**
	 * @author amitm
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward MassApprove( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		FreightResourceForm freightresourceform = ( FreightResourceForm ) form;
		FreightResource freightresource = null;
		String Activitylibrary_Id = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		ArrayList freightresourcelist = new ArrayList();
		String status1  = null,status = null;
		String activityname = "";
		int resource_size = 0,changestatusflag = 0;
		String MSA_Id = ""; 
		String Appendix_Id = "";
		String Job_Id = "";
		int addflag = 0;
		NameId idname = null;
		
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if( request.getParameter( "Status" ) != null )
		{
			status1 = request.getParameter( "Status" );
		}
		
		Activitylibrary_Id = freightresourceform.getActivity_Id();
		
		if( freightresourceform.getCheck()!=  null )
		{
			int checklength = freightresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = freightresourceform.getFreightid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( freightresourceform.getCheck( i ).substring( 1 , freightresourceform.getCheck( i ).length() ).equals( freightresourceform.getFreightid( j ) ) )
						{	
							
							index[k] = j;
							k++;
						}
						
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				
				for( int i = 0; i < index.length; i++ )
				{
					freightresource = new FreightResource();
					freightresource.setFreightid( freightresourceform.getFreightid ( index[i] ) );
					freightresource.setActivity_Id( freightresourceform.getActivity_Id() );
					freightresource.setFreighttype( "F" );
					 changestatusflag = ChangeStatus.Status( MSA_Id , Appendix_Id , Job_Id , freightresource.getActivity_Id() , freightresource.getFreightid() , freightresource.getFreighttype() , status1 ,"N", getDataSource( request , "ilexnewDB" ),"" );
				}
			}
		}
		
	
		if( freightresourceform.getNewfreightid() != null )
		{
			
			freightresource = new FreightResource();
			freightresource.setFreightid( freightresourceform.getNewfreightnamecombo() );
			freightresource.setActivity_Id( freightresourceform.getActivity_Id() );
			freightresource.setCnspartnumber( freightresourceform.getNewcnspartnumber() );
			freightresource.setQuantity( freightresourceform.getNewquantity() );
			freightresource.setFreighttype( "F" );
			freightresource.setEstimatedunitcost( freightresourceform.getNewestimatedunitcost() );
			freightresource.setProformamargin( freightresourceform.getNewproformamargin() );
			
			addflag = ActivityLibrarydao.addfreightresource( freightresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		freightresourceform.reset( mapping , request);
				
		freightresourceform.setNewquantity( "0.0000" );
		freightresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "F" , getDataSource( request , "ilexnewDB" ) ) );
		freightresourceform.setNewstatus( "Draft" );
		freightresourceform.setActivity_Id( Activitylibrary_Id );
				
		freightresourceform.setRef( null );
		freightresourceform.setFreightlist( ActivityLibrarydao.getMateriallist( "freight" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		
		freightresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "F" , "%" , sortqueryclause , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		if( freightresourcelist.size() > 0 )
		{
			resource_size = freightresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setMsa_id( idname.getId() );
		freightresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		freightresourceform.setActivity_Id( idname.getId() );
		freightresourceform.setActivity_name( idname.getName() );
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );					
		
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "freightresourcelist" , freightresourcelist );
		request.setAttribute("changestatusflag",""+changestatusflag);
		
		return( mapping.findForward( "freightresourcetabularpage" ) );
	}

	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "amresF_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "amresF_sort" );
		return queryclause;
	}
}
