/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used to manage category of an activity     
*
*/

package com.mind.actions.AM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.formbean.AM.ManageCategoryForm;


/*
 * Methods : View , Submit,Delete
 */

public class ManageCategoryAction extends com.mind.common.IlexDispatchAction
{
/** This method is used to list all the categories 
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		ManageCategoryForm managecategoryform = ( ManageCategoryForm ) form;
		int category_size = 0;
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String loginuserid = (String)session.getAttribute("userid");
		if( managecategoryform.getAuthenticate().equals( "" ) )
		{
			if( !Authenticationdao.getPageSecurity( loginuserid , "Category" , "View Category" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		
		managecategoryform.setExistingcategory( ActivityLibrarydao.getActivitycategorylist( getDataSource( request , "ilexnewDB" ) ) );
		
		
		request.setAttribute( "category_list" , managecategoryform.getExistingcategory() );
		
		if(request.getParameter("add")!=null)
		{
			if(request.getParameter("add").equals("add"))
			{
				managecategoryform.reset( mapping , request );
			}
		}
		//managecategoryform.reset( mapping , request );
		managecategoryform.setRef("View");
		return( mapping.findForward( "managecategorypage" ) );
	}
	

	/** This method is used to add/update  category
	* @param mapping						object of ActionMapping
	* @param form   			       	   	object of ActionForm
	* @param request   			       		object of HttpServletRequest
	* @param response   			       	object of HttpServletResponse
	* @return ActionForward              	This method returns object of ActionForward.
	*/
		public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
		{
			ManageCategoryForm managecategoryform = ( ManageCategoryForm ) form;
			HttpSession session = request.getSession( true );
			String loginuserid = ( String ) session.getAttribute( "userid" );
			String intialcategory="";
			//System.out.println("XXXXinside submit");
			
			
			if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
			
			
			//System.out.println("XXXXmanagecategoryform.getCategory_id()"+managecategoryform.getCategory_id());
			
			intialcategory=managecategoryform.getCategory_id();
			if(managecategoryform.getCategory_id().equals("0"))
			{
				managecategoryform.setCategory_id( managecategoryform.getCategory_id() );
			}
			else
			{
				String temp = managecategoryform.getCategory_id();
				temp = temp.substring( 0 , managecategoryform.getCategory_id().indexOf( "!" ) );
				managecategoryform.setCategory_id( temp );
			}
			
			if(managecategoryform.getCategory_id()!=null)
			{
				if(Integer.parseInt(managecategoryform.getCategory_id())!=0)
				{
					int updateflag = ActivityLibrarydao.updateCategory( managecategoryform.getCategory_id() , managecategoryform.getCategory_name() , "U" , getDataSource( request , "ilexnewDB" ) ) ;
					request.setAttribute( "updateflag" , updateflag+"");
					
				}
				else
				{
					
					int addflag = ActivityLibrarydao.addCategory( managecategoryform.getCategory_name() , getDataSource( request , "ilexnewDB" ) ) ;
					request.setAttribute( "addflag" , addflag+"");
					
				}
			}
			
			
			
						
			managecategoryform.setExistingcategory( ActivityLibrarydao.getActivitycategorylist( getDataSource( request , "ilexnewDB" ) ) );
			
			managecategoryform.setCategory_id(intialcategory);
			
						
			request.setAttribute( "category_list" , managecategoryform.getExistingcategory() );
			
			managecategoryform.setRef("View");
			return( mapping.findForward( "managecategorypage" ) );
		}
	
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		ManageCategoryForm managecategoryform = ( ManageCategoryForm ) form;
		
		HttpSession session = request.getSession( true );
		
		String loginuserid = ( String ) session.getAttribute( "userid" );
		int category_size = 0;
		int deleteflag = 0;
		String temp = managecategoryform.getCategory_id();
		temp = temp.substring( 0 , managecategoryform.getCategory_id().indexOf( "!" ) );
		
		managecategoryform.setCategory_id( temp );
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Category" , "Delete" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		
		deleteflag = ActivityLibrarydao.updateCategory( managecategoryform.getCategory_id() , managecategoryform.getCategory_name() , "D" , getDataSource( request , "ilexnewDB" ) ) ;
		request.setAttribute( "deleteflag" , deleteflag+"");
		
		managecategoryform.reset( mapping , request );
		
		managecategoryform.setExistingcategory( ActivityLibrarydao.getActivitycategorylist( getDataSource( request , "ilexnewDB" ) ) );
	
		request.setAttribute( "category_list" , managecategoryform.getExistingcategory() );
				
		managecategoryform.setRef("View");
		return( mapping.findForward( "managecategorypage" ) );
	}
	
	
}
