/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used for managing activities present in a MSA     
*
*/


package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.am.ActivityLibrary;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.formbean.AM.ManageActivityForm;

/*
 * Methods : execute
 */

public class ManageActivityAction extends com.mind.common.IlexAction 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ManageActivityAction.class);

/** This method is used to manage activity library of an MSA 
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response) throws Exception 
	{
		ManageActivityForm activityform = ( ManageActivityForm ) form;
			
		ActivityLibrary activityLibrary = new ActivityLibrary();
		String MSA_Id = "" , activitytype = "" , status1 = "";
		int activity_size = 0;
		ActivityLibrary activity = null;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String msaname = "" , msaName = "";
		ArrayList activitylibrarylist = new ArrayList();
		String category_Id = "";
		String category_name = "";
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Activity" , "View" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		if( request.getParameter( "MSA_Id" ) != null )
		{
			MSA_Id = request.getParameter( "MSA_Id" );
			activityform.setMsa_Id( MSA_Id );
			request.setAttribute( "MSA_Id" , activityform.getMsa_Id() );
		}	
		
		if( request.getAttribute( "MSA_Id" ) != null )
		{
			MSA_Id = ( String ) request.getAttribute( "MSA_Id" );
			activityform.setMsa_Id( MSA_Id );
			request.setAttribute( "MSA_Id" , activityform.getMsa_Id() );
		}
		
		if( request.getParameter( "msa_Id" ) != null )
		{
			MSA_Id = request.getParameter( "msa_Id" );
			activityform.setMsa_Id( MSA_Id );
			request.setAttribute( "MSA_Id" , activityform.getMsa_Id() );
		}
			if( request.getParameter( "category_Id" ) != null )
			{
				category_Id = request.getParameter( "category_Id" );
				activityform.setCategory_Id( category_Id );
				request.setAttribute( "category_Id" , activityform.getCategory_Id() );
			}
			
			if( request.getAttribute( "category_Id" ) != null )
			{
				category_Id = ( String ) request.getAttribute( "category_Id" );
				activityform.setCategory_Id( category_Id );
				request.setAttribute( "category_Id" , activityform.getCategory_Id() );
			}
		
		
		
		if( category_Id == "" )
		{
			request.setAttribute( "flag" , "all" );
		}
		else
		{
			category_name = ActivityLibrarydao.getCategoryname( category_Id , getDataSource(request,"ilexnewDB" ) );
			activityform.setNewcategorytype(category_name);
			request.setAttribute("categoeyName", category_name);
		}
		//Start: Added By Amit for mass delete
		if( request.getParameter( "Type" ) != null )
		{
			activitytype = request.getParameter( "Type" );
		}
		if( request.getParameter( "Status" ) != null )
		{
			status1 = request.getParameter( "Status" );
		}
		if( activityform.getDelete() != null )
		{
			int deleteflag = 0;
			
				if( activityform.getCheck()!=  null )
				{
					int checklength = 0;
					if( activityform.getCheck()!=  null )
					{
						 checklength = activityform.getCheck().length;	 
					}
					
					int []index = new int[checklength];
					int k = 0;
					
					if( checklength > 0 )
					{	int length = activityform.getActivity_Id().length;
						for( int i = 0; i < checklength; i++ )
						{
							for( int j = 0; j < length; j++ )
							{
								if( activityform.getCheck(i).equals( activityform.getActivity_Id( j ) ) )
								{	
									index[k] = j;
									k++;
								}
							}
						}
					}
				
					if( checklength > 0 )                               
					{
						for( int i = 0; i < k; i++ )
						{
							activityLibrary = new ActivityLibrary();
							activityLibrary.setActivity_Id( activityform.getActivity_Id ( index[i] ) );
							
							 deleteflag = ActivityLibrarydao.deleteactivity( activityLibrary.getActivity_Id() , getDataSource( request , "ilexnewDB" ) );
							
							// msaName = Appendixdao.getMsanameFromMsaID(activityform.getMsa_Id(),getDataSource( request , "ilexnewDB" ));
						}
					}
				}
				request.setAttribute( "msaName" , msaName );
				request.setAttribute( "deleteflag" , ""+deleteflag );
			}
			
		
		//End : Added By Amit
		//Start :Added By Amit For Mass Approve
		if( activityform.getApprove() != null )
		{
			int changestatusflag = 0;
			int approvedcount = 0;
			int notapprovedcount = 0;
		
				if( activityform.getCheck() !=  null )
				{
					int checklength = 0;
					if( activityform.getCheck()!=  null )
					{
						 checklength = activityform.getCheck().length; 
					}
					
					int []index = new int[checklength];
					int k = 0;
					
					if( checklength > 0 )
					{	int length = activityform.getActivity_Id().length;
						for( int i = 0; i < checklength; i++ )
						{
							for( int j = 0; j < length; j++ )
							{
								if( activityform.getCheck(i).equals( activityform.getActivity_Id( j ) ) )
								{	
									index[k] = j;
									k++;
								}
							}
						}
					}
				
					if( checklength > 0 )                               
					{
						for( int i = 0; i < k; i++ )
						{
							activityLibrary = new ActivityLibrary();
							activityLibrary.setActivity_Id( activityform.getActivity_Id ( index[i] ) );
							
							changestatusflag = ChangeStatus.Status( "" , "" , "" , activityLibrary.getActivity_Id() , "" , activitytype , status1 ,"N", getDataSource( request , "ilexnewDB" ),"" );
							
							if(changestatusflag==0){
								approvedcount = approvedcount + 1;
							}
							if(changestatusflag!=0){
								notapprovedcount = notapprovedcount + 1;
								
							}
						}
						if(approvedcount == 0 && notapprovedcount > 0)
							request.setAttribute("Approved","false");
						else if(approvedcount > 0 && notapprovedcount == 0)
							request.setAttribute("Approved","true");
						else 
							request.setAttribute("Approved","some");
															
						}
					}
				
				request.setAttribute( "msaName", msaName );
	//			request.setAttribute( "changestatusflag" , ""+changestatusflag );
	//			request.setAttribute("checkforapprovedactivites" ,""+approvedcount);
	//			request.setAttribute("checkfornotapprovedactivites" ,""+notapprovedcount);
			}
		//End :Added By Amit For Mass Approve
		
		if( activityform.getUpdate() != null )
		{
			int checklength = 0;
			if( activityform.getCheck()!=  null )
			{
				 checklength = activityform.getCheck().length;	 
				 
			}
			
			int []index = new int[checklength];
			int k = 0;
			if( checklength > 0 )
			{	int length = activityform.getActivity_Id().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( activityform.getCheck(i).equals( activityform.getActivity_Id( j ) ) )
						{	
							index[k] = j;
							
							k++;
						}
					}
				}
			}
			
			/* Used for updation 
			 * 
			 */
			
		
			if( checklength > 0 )                               
			{
				int updateflag = 0;
				for( int i = 0; i < k; i++ )
				{
					activity = new ActivityLibrary();
					activity.setActivity_Id( activityform.getActivity_Id ( index[i] ) );
					
					activity.setMsa_Id( activityform.getMsa_Id() );
					
					
					activity.setName( activityform.getName( index[i] ) );
					
					activity.setActivitytypecombo( activityform.getActivitytypecombo(  index[i] ) );
					activity.setCategory_type( activityform.getCategory_typecombo( index[i] ) );
					
					updateflag = ActivityLibrarydao.updateActivity( activity , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );
				}	
			}
			
			
			if( activityform.getNewactivity_Id() != null )        
			{
				int addflag = 0;
				try
				{
					activity = new ActivityLibrary();
					activity.setName( activityform.getNewname() );
					activity.setMsa_Id( activityform.getMsa_Id() );
					
					activity.setActivitytypecombo( activityform.getNewactivitytypecombo() );
					if( category_Id == "" )
					{
						activity.setCategory_type( activityform.getNewcategorytypecombo() );
						category_Id = activityform.getNewcategorytypecombo();
					}
					else
						activity.setCategory_type( category_Id );
					
					
					addflag = ActivityLibrarydao.addActivity( activity , loginuserid , getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "addflag" , addflag+"");
				}
				catch( Exception e )
				{
					logger.error("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)", e);

					if (logger.isDebugEnabled()) {
						logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Error Occured " + e);
					}
				}
					
			}
			activityform.reset( mapping , request );
			
			activityform.setMsa_Id( MSA_Id );
			activityform.setCategory_Id( category_Id );
			activityform.setNewcategorytype( category_name );
			activityform.setNewquantity( "1" );
			activityform.setNewestimatedmaterialcost( "0" );
			activityform.setNewestimatedcnslabourcost( "0" );
			activityform.setNewestimatedcontractlabourcost( "0" );
			activityform.setNewestimatedfreightcost( "0" );
			
			activityform.setNewestimatedtravelcost( "0" );
			activityform.setNewestimatedtotalcost( "0" );
			activityform.setNewextendedprice( "0" );
			activityform.setNewproformamargin( "0" );
			activityform.setNewstatus( "Draft" );
			
			activityform.setActivity( ActivityLibrarydao.getActivitytype() );
			activityform.setCategory( ActivityLibrarydao.getActivitycategorylist_01(getDataSource( request , "ilexnewDB" ) ) );
			
			msaname = Appendixdao.getMsaname( getDataSource( request , "ilexnewDB" ) , MSA_Id );
			request.setAttribute( "msaname" , msaname ); 
			
			activitylibrarylist = ActivityLibrarydao.getActivitylibrary( getDataSource( request , "ilexnewDB" ) , MSA_Id , category_Id , sortqueryclause );
			
			if( activitylibrarylist.size() > 0 )
			{
				activity_size = activitylibrarylist.size();
			}
			
			request.setAttribute( "Size" , new Integer ( activity_size ) );
			request.setAttribute( "activitylist" , activitylibrarylist );
			String checkformastercopy = ActivityLibrarydao.Checkformasterlibcopy( MSA_Id , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute("checkformastercopy", checkformastercopy);
			return ( mapping.findForward( "manageactivitypage" ) );
		}
		
		else
		{	
				
				/*if ( request.getAttribute( "querystring" ) != null )
				{
					sortqueryclause = ( String ) request.getAttribute( "querystring" );
				}*/
			
				activityform.setNewquantity( "1" );
				activityform.setNewestimatedmaterialcost( "0" );
				activityform.setNewestimatedcnslabourcost( "0" );
				activityform.setNewestimatedcontractlabourcost( "0" );
				activityform.setNewestimatedfreightcost( "0" );
				
				activityform.setNewestimatedtravelcost( "0" );
				activityform.setNewestimatedtotalcost( "0" );
				activityform.setNewextendedprice( "0" );
				activityform.setNewproformamargin( "0" );
				activityform.setNewstatus( "Draft" );
				
				activityform.setActivity( ActivityLibrarydao.getActivitytype() );
				activityform.setCategory( ActivityLibrarydao.getActivitycategorylist_01(getDataSource( request , "ilexnewDB" ) ) );
				
				msaname = Appendixdao.getMsaname( getDataSource( request , "ilexnewDB" ) , MSA_Id );
				
				activityform.setMsaname(msaname);
				request.setAttribute( "msaname" , msaname ); 
				
				String mappingForward = "";
				if(request.getParameter("ref") != null && request.getParameter("ref").equals("addActivity")) {
					mappingForward = "addactivitypage";
				} else {
					activitylibrarylist = ActivityLibrarydao.getActivitylibrary( getDataSource( request , "ilexnewDB" ) , MSA_Id , category_Id , sortqueryclause );
					request.setAttribute( "activitylist" , activitylibrarylist );
					mappingForward = "manageactivitypage";
					
				}
				
				if( activitylibrarylist.size() > 0 )
				{
					activity_size = activitylibrarylist.size();
				}
				
				request.setAttribute( "Size" , new Integer ( activity_size ) );
				
				if( request.getAttribute( "deleteflag" ) != null )
				{
					request.setAttribute( "deleteflag" , ( String ) request.getAttribute( "deleteflag" ) );
				}
				
				
				if( request.getAttribute( "addflag" ) != null )
				{
					request.setAttribute( "addflag",(String)request.getAttribute("addflag") );
				}
				String checkformastercopy = ActivityLibrarydao.Checkformasterlibcopy( MSA_Id , getDataSource( request , "ilexnewDB" ) );
				request.setAttribute("checkformastercopy", checkformastercopy);
				return ( mapping.findForward(mappingForward) );
		}
	}
	
	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "am_sort" ) != null )
			queryclause = ( String ) session.getAttribute("am_sort");
		if(queryclause.equals("")) {
			queryclause = " order by lm_al_cat_name ";
		} else {
			queryclause = queryclause.substring(9,queryclause.length());
			queryclause = " order by lm_al_cat_name ,"+queryclause;
		}
		return queryclause;
	}
}
