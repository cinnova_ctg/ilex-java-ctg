/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used to manage labor resource of an activity     
*
*/


package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.ChangeStatus;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.LaborResource;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.NameId;
import com.mind.formbean.AM.LaborResourceForm;



/*
 * Methods : View , Refresh , Submit , Delete
 */


public class LaborResourceAction extends com.mind.common.IlexDispatchAction
{
/** This method is used to list all the labor resources used in an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	
	public ActionForward View( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		String Activitylibrary_Id = "";
		int resource_size = 0;
		ArrayList laborresourcelist = new ArrayList();
		String sortqueryclause = "";
		String status = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String activityname = "";
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		NameId idname = null;
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) != null ) 
		{
			
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			laborresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end
		if ( request.getParameter( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getParameter( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			laborresourceform.setActivity_Id( Activitylibrary_Id );
		}
		else
		{
			Activitylibrary_Id = laborresourceform.getActivity_Id();
		}
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activitylibrary_Id );
		laborresourceform.setLaborlist( ActivityLibrarydao.getMateriallist( "labor" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		laborresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "L" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
		
		
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setActivity_Id( idname.getId() );
		laborresourceform.setActivity_name( idname.getName() );
		
		
		/* for checkbox check start */
		
		String checkTempResources[] = ActivityLibrarydao.checkTempResourcesAM( Activitylibrary_Id , "L" , "%" , sortqueryclause ,  request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setCheck( checkTempResources );
		/* for checkbox check end */
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		
		request.setAttribute( "act_status" , status );
		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "activityname" , activityname );
		return( mapping.findForward( "laborresourcetabularpage" ) );
	}
	

	
/** This method is used when the page is refreshed to populate form according to the value selected from the combo 
* @param mapping   			       object of ActionMapping
* @param form   			       	   object of ActionForm
* @param request   			       object of HttpServletRequest
* @param response   			       object of HttpServletResponse
* @return ActionForward              This method returns object of ActionForward.
*/	

	
	public ActionForward Refresh( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		String Activitylibrary_Id = "";
		String newname = "";
		String newnamecombo = "";
		String newlaborid = "";
		int resource_size = 0;
		ArrayList laborresourcelist = new ArrayList();
		int checklength = 0;
		String status = "";
		String activityname = "";
		NameId idname = null;
		ArrayList temp = new ArrayList();
		String[] checkrefresh = null;
		LaborResource laborresource = new LaborResource();
		Activitylibrary_Id = laborresourceform.getActivity_Id();
		
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
			
		}
		else
		{
			ActivityLibrarydao.deleteTempResources(request.getSession().getId(),getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		/* by hamid for back button start*/
		if(request.getParameter("MSA_Id")!=null)
		{
			laborresourceform.setMSA_Id(request.getParameter("MSA_Id"));
		}
		/* by hamid for back button end*/
		
		newname = laborresourceform.getNewlaborname();
		newnamecombo = laborresourceform.getNewlabornamecombo();
		newlaborid = laborresourceform.getNewlaborid();
		
		
		
		if( laborresourceform.getCheck()!=  null )
		{
			checklength = laborresourceform.getCheck().length;	 
			checkrefresh = laborresourceform.getCheck();
			
			int []tempindex = new int[checklength];
			int k = 0;
				
			if( checklength > 0 )
			{	int length = laborresourceform.getLaborid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( laborresourceform.getCheck( i ).equals( laborresourceform.getLaborid( j ) ) )
						{	
							tempindex[k] = j;
							k++;
						}
					}
				}
			}	
			
			for( int i = 0; i < checklength; i++ )
			{
				LaborResource tempresource = new LaborResource();
				
				tempresource.setActivity_Id( Activitylibrary_Id );
				
				tempresource.setLaborid( laborresourceform.getLaborid( tempindex[i] ) );
				tempresource.setLaborcostlibid( laborresourceform.getLaborcostlibid( tempindex[i] ) );
				tempresource.setLaborname( laborresourceform.getLaborname( tempindex[i] ) );
				tempresource.setLabortype( laborresourceform.getLabortype( tempindex[i] ) );
				
				tempresource.setCnspartnumber( laborresourceform.getCnspartnumber( tempindex[i] ) );
				tempresource.setEstimatedtotalcost( laborresourceform.getEstimatedtotalcost( tempindex[i] ) );
				tempresource.setEstimatedhourlybasecost( laborresourceform.getEstimatedhourlybasecost( tempindex[i] ) );
				tempresource.setMinimumquantity( laborresourceform.getMinimumquantity( tempindex[i] ) );
				tempresource.setPriceextended( laborresourceform.getPriceextended( tempindex[i] ) );
				tempresource.setPriceunit( laborresourceform.getPriceunit( tempindex[i] ) );
				tempresource.setProformamargin( laborresourceform.getProformamargin( tempindex[i] ) );
				tempresource.setQuantityhours( laborresourceform.getQuantityhours( tempindex[i] ) );
				tempresource.setPrevquantityhours( laborresourceform.getPrevquantityhours( tempindex[i] ) );
				tempresource.setSellablequantity( laborresourceform.getSellablequantity( tempindex[i] ) );
				tempresource.setStatus( laborresourceform.getStatus( tempindex[i] ) );
				
				
				temp.add( tempresource );
			}	
		}	 
		
		laborresource = ActivityLibrarydao.getLaborResource(  laborresourceform.getNewlabornamecombo() , getDataSource( request , "ilexnewDB" ) );
		
		laborresourceform.reset( mapping , request);
		
		laborresourceform.setNewlaborid( newlaborid );
		
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		
		laborresourceform.setNewlaborname( newname );
		laborresourceform.setNewlabornamecombo( newnamecombo );
		laborresourceform.setNewlabortype( laborresource.getLabortype() );
		
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setNewcnspartnumber( laborresource.getCnspartnumber() );
		laborresourceform.setNewestimatedhourlybasecost( laborresource.getEstimatedhourlybasecost() );
		laborresourceform.setNewsellablequantity( laborresource.getSellablequantity() );
		laborresourceform.setNewminimumquantity( laborresource.getMinimumquantity() );
		
		laborresourceform.setRef( null );
		laborresourceform.setActivity_Id( Activitylibrary_Id );
		laborresourceform.setLaborlist( ActivityLibrarydao.getMateriallist( "labor" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );
		
		laborresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "L" , "%" , "" , request.getSession().getId(),"listpage",getDataSource( request , "ilexnewDB" ) );
		
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setActivity_Id( idname.getId() );
		laborresourceform.setActivity_name( idname.getName() );
		
		if( checklength > 0 )
		{
			if( resource_size > 0 )
			{
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < resource_size; j++ )
					{
						if( ( ( LaborResource )temp.get( i ) ).getLaborid().equals( ( ( LaborResource )laborresourcelist.get( j ) ).getLaborid() ) )
						{
							laborresourcelist.set( j ,  temp.get( i ) );
						}
					}
				}
				laborresourceform.setCheck( checkrefresh );
			}
			
		}
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "refresh" , "false" );
		request.setAttribute( "activityname" , activityname );
		
		return( mapping.findForward( "laborresourcetabularpage" ) );
	}
	

/** This method is used to add/update labor resource to an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
	
	public ActionForward Submit( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		String Activitylibrary_Id = "";
		LaborResource laborresource = null;
		int resource_size = 0;
		ArrayList laborresourcelist = new ArrayList();
		String status = "";
		String activityname = "";
		int addflag = 0;
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		/*multiple resouce select start*/
		if( request.getAttribute( "tempdeleteflag" ) !=null ) 
		{
			
		}
		else
		{
			ActivityLibrarydao.deleteTempResources( request.getSession().getId() , getDataSource( request , "ilexnewDB" ) );
		}
		/*multiple resouce select start end */
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			laborresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end*/
		Activitylibrary_Id = laborresourceform.getActivity_Id();
		
		if( laborresourceform.getCheck() !=  null )
		{
			int checklength = laborresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = laborresourceform.getLaborid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( laborresourceform.getCheck( i ).substring( 1 , laborresourceform.getCheck( i ).length() ).equals( laborresourceform.getLaborid( j ) ) )
						{	
							
							index[k] = j;
							k++;
						}
						
						/*if( laborresourceform.getCheck(i).equals( laborresourceform.getLaborid( j ) ) )
						{	
							index[k] = j;
							k++;
						}*/
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				int updateflag = 0;
				for( int i = 0; i < index.length; i++ )
				{
					laborresource = new LaborResource();
					laborresource.setLaborid( laborresourceform.getLaborid ( index[i] ) );
					laborresource.setLaborcostlibid( laborresourceform.getLaborcostlibid ( index[i] ) );
					laborresource.setActivity_Id( laborresourceform.getActivity_Id() );
					laborresource.setQuantityhours( laborresourceform.getQuantityhours ( index[i] ) );
					laborresource.setPrevquantityhours( laborresourceform.getPrevquantityhours( index[i] ) );
					laborresource.setLabortype( "L" );
					laborresource.setEstimatedhourlybasecost( laborresourceform.getEstimatedhourlybasecost( index[i] ) );
					laborresource.setProformamargin( laborresourceform.getProformamargin( index[i] ) );
			
					/* for multiple resource select start*/
					laborresource.setCnspartnumber( laborresourceform.getCnspartnumber( index[i] ) );
					laborresource.setFlag(laborresourceform.getFlag(index[i]));
					if(laborresourceform.getFlag(index[i]).equals("P"))
					{
						updateflag = ActivityLibrarydao.updatelaborresource( laborresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "updateflag" , ""+updateflag );
					}
					if(laborresourceform.getFlag(index[i]).equals("T"))
					{
						addflag = ActivityLibrarydao.addLaborresource( laborresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
						request.setAttribute( "addflag" , ""+addflag );
					}
					/* for multiple resource select end*/
					/*updateflag = ActivityLibrarydao.updatelaborresource( laborresource , loginuserid ,  getDataSource( request , "ilexnewDB" ) );
					request.setAttribute( "updateflag" , ""+updateflag );*/
				}
			}
		}
		
	
		if( laborresourceform.getNewlaborid() != null )
		{
			
			laborresource = new LaborResource();
			laborresource.setLaborid( laborresourceform.getNewlabornamecombo() );
			laborresource.setActivity_Id( laborresourceform.getActivity_Id() );
			laborresource.setCnspartnumber( laborresourceform.getNewcnspartnumber() );
			laborresource.setQuantityhours( laborresourceform.getNewquantityhours() );
			laborresource.setLabortype( "L" );
			laborresource.setEstimatedhourlybasecost( laborresourceform.getNewestimatedhourlybasecost() );
			laborresource.setProformamargin( laborresourceform.getNewproformamargin() );
			
			addflag = ActivityLibrarydao.addLaborresource( laborresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		laborresourceform.reset( mapping , request);
		
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activitylibrary_Id );
		
		laborresourceform.setRef( null );
		laborresourceform.setLaborlist( ActivityLibrarydao.getMateriallist( "labor" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		laborresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "L" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
		
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setActivity_Id( idname.getId() );
		laborresourceform.setActivity_name( idname.getName() );
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );
		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "true" );
		return( mapping.findForward( "laborresourcetabularpage" ) );
	}
	

/** This method is used to delete labor resource from an activity
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/	
	
	public ActionForward Delete( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{	
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		String Activitylibrary_Id = "";
		int resource_size = 0;
		int deleteflag = 0;
		ArrayList laborresourcelist = new ArrayList();
		String status = "";
		String activityname = "";
		LaborResource laborresource = null;
		NameId idname = null;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		/* by hamid for back button start*/
		if( request.getParameter( "MSA_Id" ) != null )
		{
			laborresourceform.setMSA_Id( request.getParameter( "MSA_Id" ) );
		}
		/* by hamid for back button end*/
		
		if( request.getParameter( "Activitylibrary_Id" ) != null )
		{	
			Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );
			laborresourceform.setActivity_Id( Activitylibrary_Id );
		}
		else
		{
			Activitylibrary_Id = laborresourceform.getActivity_Id();
		}
		
//		Start :Added By Amit For Mass delete
		if(request.getParameter("fromResourceTabular")!=null)
		{
			if( laborresourceform.getCheck()!=  null )
			{
				
				int checklength = laborresourceform.getCheck().length;	 
		
				int []index = new int[checklength];
				int k = 0;
				
				if( checklength > 0 )
				{	int length = laborresourceform.getLaborid().length;
					for( int i = 0; i < checklength; i++ )
					{
						for( int j = 0; j < length; j++ )
						{
							if( laborresourceform.getCheck( i ).substring( 1 , laborresourceform.getCheck( i ).length() ).equals( laborresourceform.getLaborid( j ) ) )
							{	
								
								index[k] = j;
								k++;
							}
						}
					}
				}
			
				if( checklength > 0 )                               
				{
					
					for( int i = 0; i < index.length; i++ )
					{
						laborresource = new LaborResource();
						laborresource.setLaborid( laborresourceform.getLaborid ( index[i] ) );
						
						deleteflag = ActivityLibrarydao.deleteresource( laborresource.getLaborid() , getDataSource( request , "ilexnewDB" ) );
					}
				}
			}
		}
		else
		{
		//End : Added By Amit
			deleteflag = ActivityLibrarydao.deleteresource( request.getParameter( "Labor_Id" ) , getDataSource( request , "ilexnewDB" ) );
		}
		request.setAttribute( "deleteflag" , ""+deleteflag );
		
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activitylibrary_Id );
		laborresourceform.setLaborlist( ActivityLibrarydao.getMateriallist( "labor" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );	
		
		laborresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "L" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
		
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setActivity_Id( idname.getId() );
		laborresourceform.setActivity_name( idname.getName() );
		
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		
		request.setAttribute( "act_status" , status );		
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "refresh" , "true" );
		return( mapping.findForward( "laborresourcetabularpage" ) );
	}
	
	/**
	 * @author amitm
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward MassApprove( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		LaborResourceForm laborresourceform = ( LaborResourceForm ) form;
		LaborResource laborresource = null;
		String Activitylibrary_Id = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		ArrayList laborresourcelist = new ArrayList();
		String status1  = null,status = null;
		String activityname = "";
		int resource_size = 0,changestatusflag = 0;
		String MSA_Id = ""; 
		String Appendix_Id = "";
		String Job_Id = "";
		int addflag = 0;
		NameId idname = null;
		String sortqueryclause = "";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		if(request.getParameter( "Status" ) != null )
		{
			status1 = request.getParameter( "Status" );
		}
		
		Activitylibrary_Id = laborresourceform.getActivity_Id();
		
		if( laborresourceform.getCheck() !=  null )
		{
			int checklength = laborresourceform.getCheck().length;	 
	
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{	int length = laborresourceform.getLaborid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( laborresourceform.getCheck( i ).substring( 1 , laborresourceform.getCheck( i ).length() ).equals( laborresourceform.getLaborid( j ) ) )
						{	
							index[k] = j;
							k++;
						}
					}
				}
			}
		
			if( checklength > 0 )                               
			{
				for( int i = 0; i < index.length; i++ )
				{
					laborresource = new LaborResource();
					laborresource.setLaborid( laborresourceform.getLaborid ( index[i] ) );
					laborresource.setActivity_Id( laborresourceform.getActivity_Id() );
					laborresource.setLabortype( "L" );
					 changestatusflag = ChangeStatus.Status( MSA_Id , Appendix_Id , Job_Id , laborresource.getActivity_Id() , laborresource.getLaborid() , laborresource.getLabortype() , status1 ,"N", getDataSource( request , "ilexnewDB" ) ,"");
					
				}
			}
		}
		
		if( laborresourceform.getNewlaborid() != null )
		{
			laborresource = new LaborResource();
			laborresource.setLaborid( laborresourceform.getNewlabornamecombo() );
			laborresource.setActivity_Id( laborresourceform.getActivity_Id() );
			laborresource.setCnspartnumber( laborresourceform.getNewcnspartnumber() );
			laborresource.setQuantityhours( laborresourceform.getNewquantityhours() );
			laborresource.setLabortype( "L" );
			laborresource.setEstimatedhourlybasecost( laborresourceform.getNewestimatedhourlybasecost() );
			laborresource.setProformamargin( laborresourceform.getNewproformamargin() );
			
			addflag = ActivityLibrarydao.addLaborresource( laborresource , loginuserid , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "addflag" , ""+addflag );
		}
		
		laborresourceform.reset( mapping , request);
				
		laborresourceform.setNewquantityhours( "0.0000" );
		laborresourceform.setNewproformamargin( Activitydao.getDefaultproformamargin( "L" , getDataSource( request , "ilexnewDB" ) ) );
		laborresourceform.setNewstatus( "Draft" );
		laborresourceform.setActivity_Id( Activitylibrary_Id );
				
		laborresourceform.setRef( null );
		laborresourceform.setLaborlist( ActivityLibrarydao.getMateriallist( "labor" , Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) ) );

		laborresourcelist = ActivityLibrarydao.getMaterialResourcelist( Activitylibrary_Id , "L" , "%" , sortqueryclause , request.getSession().getId() , "listpage" , getDataSource( request , "ilexnewDB" ) );
			
			
		if( laborresourcelist.size() > 0 )
		{
			resource_size = laborresourcelist.size();
		}
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "MSA" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setMsa_id( idname.getId() );
		laborresourceform.setMsaname( idname.getName() );
		
		idname = Activitydao.getIdName( Activitylibrary_Id , "Library" , "Activity" , getDataSource( request , "ilexnewDB" ) );
		laborresourceform.setActivity_Id( idname.getId() );
		laborresourceform.setActivity_name( idname.getName() );
		
		
		status = ActivityLibrarydao.getActivitystatus( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		activityname = ActivityLibrarydao.getActivitylibraryname( Activitylibrary_Id , getDataSource( request , "ilexnewDB" ) );
		request.setAttribute( "act_status" , status );					
		
		request.setAttribute( "refresh" , "true" );
		request.setAttribute( "Size" , new Integer ( resource_size ) );
		request.setAttribute( "activityname" , activityname );
		request.setAttribute( "Activity_Id" , Activitylibrary_Id );
		request.setAttribute( "laborresourcelist" , laborresourcelist );
		request.setAttribute("changestatusflag",""+changestatusflag);
		
		return( mapping.findForward( "laborresourcetabularpage" ) );
	}

	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "amresL_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "amresL_sort" );
		return queryclause;
	}
}

