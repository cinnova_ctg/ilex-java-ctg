/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: An action class used for displaying material resource detail     
*
*/

package com.mind.actions.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.AM.MaterialResource;
import com.mind.formbean.AM.MaterialResourceDetailForm;

/*
 * Methods : execute
 */


public class MaterialResourceDetailAction extends com.mind.common.IlexAction
{
	
/** This method populate MaterialResourceDetailform formbean with the material detail of an activity whose data is to be shown
* @param mapping						object of ActionMapping
* @param form   			       	   	object of ActionForm
* @param request   			       		object of HttpServletRequest
* @param response   			       	object of HttpServletResponse
* @return ActionForward              	This method returns object of ActionForward.
*/
	public ActionForward execute( ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response ) throws Exception 
	{
		MaterialResourceDetailForm materialresourcedetailform = ( MaterialResourceDetailForm ) form;
		ArrayList materialresourcelist = new ArrayList();
		MaterialResource materialresource = new MaterialResource();
		String material_Id = ""; 
		String list = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		
		int sow_size = 0;
		int assumption_size = 0;
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		
		if( request.getParameter( "Material_Id" ) != null )
		{
			material_Id = request.getParameter( "Material_Id" );
		}
		
		if( request.getAttribute( "Material_Id" ) != null )
		{
			material_Id = ( String ) request.getAttribute( "Material_Id" );
		}
		
		materialresourcedetailform.setMaterialid( material_Id );
		request.setAttribute( "Material_Id" , materialresourcedetailform.getMaterialid() );
		
		materialresourcelist = ActivityLibrarydao.getMaterialResourcelist( "%" , "M" , materialresourcedetailform.getMaterialid() , "" , request.getSession().getId(),"detailpage",getDataSource( request , "ilexnewDB" ) ); 
		
		materialresource = ( MaterialResource ) materialresourcelist.get( 0 );
		
		materialresourcedetailform.setActivity_Id( materialresource.getActivity_Id() );
		request.setAttribute( "Activitylibrary_Id" , materialresourcedetailform.getActivity_Id() );  
		
		materialresourcedetailform.setMaterialid( materialresource.getMaterialid() );
		materialresourcedetailform.setMaterialname( materialresource.getMaterialname() );
		materialresourcedetailform.setMaterialtype( materialresource.getMaterialtype() );
		materialresourcedetailform.setMinimumquantity( materialresource.getMinimumquantity() );
		materialresourcedetailform.setCnspartnumber( materialresource.getCnspartnumber() );
		materialresourcedetailform.setEstimatedtotalcost( materialresource.getEstimatedtotalcost() );
		materialresourcedetailform.setEstimatedunitcost( materialresource.getEstimatedunitcost() );
		materialresourcedetailform.setManufacturername( materialresource.getManufacturername() );
		materialresourcedetailform.setManufacturerpartnumber( materialresource.getManufacturerpartnumber() );
		materialresourcedetailform.setPriceextended( materialresource.getPriceextended() );
		materialresourcedetailform.setPriceunit( materialresource.getPriceunit() );
		materialresourcedetailform.setProformamargin( materialresource.getProformamargin() );
		materialresourcedetailform.setQuantity( materialresource.getQuantity() );
		materialresourcedetailform.setSellablequantity( materialresource.getSellablequantity() );
		materialresourcedetailform.setStatus( materialresource.getStatus() );
		
		materialresourcedetailform.setActivity_Id( materialresource.getActivity_Id() );
		materialresourcedetailform.setActivityname( materialresource.getActivity_name() );
		materialresourcedetailform.setMsa_id( materialresource.getMsa_Id() );
		materialresourcedetailform.setMsaname( materialresource.getMsaname() );
		
		list = Menu.getStatus( "M" , materialresourcedetailform.getStatus().charAt( 0 ) , "" , "" , "" , materialresourcedetailform.getMaterialid() ,  loginuserid , getDataSource( request , "ilexnewDB" ) );
		
		request.setAttribute( "list" , list );
		
		
		ArrayList sowlist = ActivityLibrarydao.getSowlist( getDataSource( request , "ilexnewDB" ) , materialresourcedetailform.getMaterialid() , "M" );
		
		if( sowlist.size() > 0 )
		{
			sow_size = sowlist.size();	
		}
		
		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist( getDataSource( request , "ilexnewDB" ) , materialresourcedetailform.getMaterialid() , "M" );
		
		if( assumptionlist.size() > 0 )
		{
			assumption_size = assumptionlist.size();	
		}
		
		request.setAttribute( "sowsize" , ""+sow_size );
		request.setAttribute( "assumptionsize" , ""+assumption_size );
		
		
		request.setAttribute( "sowlist" , sowlist );
		request.setAttribute( "assumptionlist" , assumptionlist );
		
		
		
		return mapping.findForward( "materialresourcedetailpage" );
	}
}
