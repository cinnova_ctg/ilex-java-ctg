package com.mind.actions.IM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.Util;
import com.mind.dao.IM.IMdao;
import com.mind.formbean.IM.InvoiceJobDetailForm;
/**
 * @version 	1.0
 * @author
 */
public class InvoiceJobSearchAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,	HttpServletResponse response)throws Exception
	{
		
		InvoiceJobDetailForm formdetail = (InvoiceJobDetailForm) form;
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		ArrayList list = new ArrayList();
		
		if(!Util.isNullOrBlank(request.getParameter("searchType"))){
			formdetail.setSearchType(request.getParameter("searchType"));
		}
		if(formdetail.getSearch() != null ){
			list = IMdao.getJobSearchList( formdetail.getPowo_number(),
						formdetail.getInvoiceno(),
						formdetail.getPartner_name(),
						formdetail.getCustomer_name(),
						formdetail.getFrom_date(),
						formdetail.getTo_date(),
						getDataSource(request,"ilexnewDB"));
			request.setAttribute("joblist", list);
		}
		request.setAttribute("joblistlength", ""+list.size());
		
		return( mapping.findForward("success"));
	}
}
