package com.mind.actions.IM;

import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.im.InvoiceJobDetailDTO;
import com.mind.formbean.IM.InvoiceJobDetailForm;
import com.mind.newjobdb.dao.JobDAO;

public class JobTicketInfoAction extends com.mind.common.IlexAction {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();

        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }															// expired

        String userid = (String) session.getAttribute("userid");
        InvoiceJobDetailForm formdetail = (InvoiceJobDetailForm) form;
        InvoiceJobDetailDTO formdetailDTO = new InvoiceJobDetailDTO();

        if (request.getParameter("jobId") != null
                && !request.getParameter("jobId").equals("")) {
            BeanUtils.copyProperties(formdetailDTO, formdetail);
            JobDAO.setJobTicketInfo(formdetailDTO,
                    request.getParameter("jobId"),
                    getDataSource(request, "ilexnewDB"));
            BeanUtils.copyProperties(formdetail, formdetailDTO);
            ArrayList resList = JobDAO.getInvoiceActResourceList(
                    formdetail.getTypeJob(), request.getParameter("jobId"),
                    getDataSource(request, "ilexnewDB"));

            request.setAttribute("resList", resList);
            ArrayList scheduleList = JobDAO.getScheduledDetail(
                    request.getParameter("jobId"),
                    getDataSource(request, "ilexnewDB"));
            request.setAttribute("scheduleList", scheduleList);
        }

        response.setContentType("text/html");
        RequestDispatcher rd = request.getRequestDispatcher("/IM/InvoioceJobInfo.jsp");
        rd.include(request, response);
        return null;
        // return mapping.findForward("success");
    }
}
