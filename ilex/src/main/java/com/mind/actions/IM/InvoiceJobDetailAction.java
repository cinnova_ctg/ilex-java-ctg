package com.mind.actions.IM;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCNSWEB;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.formbean.IM.InvoiceJobDetailForm;
import com.mind.xml.IRPdf;

/**
 * @version 1.0
 * @author
 */
public class InvoiceJobDetailAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();

		InvoiceJobDetailForm formdetail = (InvoiceJobDetailForm) form;
		String titlemsaname = "";
		String titleappendixname = "";
		String problem_summary = null;

		String sortqueryclause = "";
		ArrayList list = new ArrayList();
		String str_temp_sec = "";
		int checkjoblength = 0;
		int retval = -1;
		String function_type = formdetail.getFunction_type();

		String s = request.getParameter("getGPImportId");
		if (s != null && !s.equals("")) {
			IMdao.setGPImportStatusToComplete(s,
					getDataSource(request, "ilexnewDB"));
			return null;
		}

		if (function_type == null || function_type.equals("filteredJob")) {
			str_temp_sec = "View Invoice Request"; // When comes thru left menu
			function_type = "";
		}
		// Security Start
		if (function_type.equals("viewReport"))
			str_temp_sec = "View Report";
		if (function_type.equals("viewIR"))
			str_temp_sec = "View Invoice Request";
		if (function_type.equals("markClosed"))
			str_temp_sec = "Mark Closed";
		if (function_type.equals("Invoice Request"))
			str_temp_sec = "Send Invoice Request";
		if (function_type.equals("bulkClosed"))
			str_temp_sec = "Mark Closed";

		// Start:Added By Amit
		if (request.getParameter("problem_summary") != null) {
			problem_summary = request.getParameter("problem_summary");
		}

		// End :

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		String userid = (String) session.getAttribute("userid");

		if (!Authenticationdao.getPageSecurity(userid, "Manage Invoice",
				str_temp_sec, getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Security End */
		if (request.getParameter("nettype") != null) {
			formdetail.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getParameter("viewjobtype") != null) {
			formdetail.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			formdetail.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			formdetail.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			formdetail.setOwnerId("%");
		}

		if (request.getAttribute("id") != null)
			formdetail.setId("" + request.getAttribute("id"));
		else
			formdetail.setId(request.getParameter("id"));

		if (request.getAttribute("type") != null)
			formdetail.setType("" + request.getAttribute("type"));
		else
			formdetail.setType(request.getParameter("type"));

		if (request.getParameter("rdofilter") != null) {
			formdetail.setRdofilter("" + request.getParameter("rdofilter"));
		}

		if (request.getParameter("toggle") != null)
			formdetail.setToggle("" + request.getParameter("toggle"));

		if (request.getParameter("invoice_Flag") != null)
			formdetail.setInvoice_Flag(""
					+ request.getParameter("invoice_Flag"));

		if (request.getParameter("partner_name") != null)
			formdetail.setPartner_name(request.getParameter("partner_name"));

		if (request.getParameter("powo_number") != null)
			formdetail.setPowo_number(request.getParameter("powo_number"));

		if (request.getParameter("from_date") != null)
			formdetail.setFrom_date(request.getParameter("from_date"));

		if (request.getParameter("to_date") != null)
			formdetail.setTo_date(request.getParameter("to_date"));

		if (request.getParameter("invoiceno") != null)
			formdetail.setInvoiceno(request.getParameter("invoiceno"));

		if (request.getParameter("exportStatus") != null)
			formdetail.setExportStatus(request.getParameter("exportStatus"));
		else
			formdetail.setExportStatus("%");

		// Added

		if (formdetail.getType().equals("MSA"))
			titlemsaname = Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), formdetail.getId());

		if (formdetail.getType().equals("Appendix")) {
			titlemsaname = Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					IMdao.getMSAId(formdetail.getId(),
							getDataSource(request, "ilexnewDB")));
			titleappendixname = Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), formdetail.getId());
		}

		if (formdetail.getType().equals("MSA")
				|| formdetail.getType().equals("Appendix"))
			formdetail.setInvoice_Flag("True");

		formdetail.setTitleappendixname(titleappendixname);
		formdetail.setTitlemsaname(titlemsaname);

		sortqueryclause = getSortQueryClause(sortqueryclause, session);

		String imagepath = request.getRealPath("images") + "/clogo.jpg";

		if (function_type != null) {
			String indexvalue = "";
			if (formdetail.getCheck() != null) {
				if (formdetail.getCheck().length > 0) {
					String index[] = new String[formdetail.getCheck().length];
					index = formdetail.getCheck();

					for (int i = 0; i < formdetail.getCheck().length; i++) {
						indexvalue = indexvalue + index[i] + ",";
					}
					indexvalue = indexvalue.substring(0,
							indexvalue.length() - 1);
				}
			}

			if (function_type.equals("viewIR")) {
				ServletOutputStream outStream = response.getOutputStream();
				byte[] buffer1 = null;

				IRPdf ir = new IRPdf();
				buffer1 = ir.viewIR(indexvalue, imagepath,
						getDataSource(request, "ilexnewDB"));
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition",
						"attachment;filename=IR.pdf;size=" + buffer1.length
								+ "");
				outStream.write(buffer1);
				outStream.close();
			}

			if (function_type.equals("markClosed")) {

				checkjoblength = formdetail.getCheck().length;
				for (int j = 0; j < formdetail.getHid_jobid().length; j++) {
					for (int i = 0; i < checkjoblength; i++) {
						if (formdetail.getCheck(i).equals(
								formdetail.getHid_jobid(j))) {

							retval = IMdao.UpdateInvoiceJob(
									formdetail.getCheck(i),
									formdetail.getInvoice_no(j),
									formdetail.getAssociated_date(j),
									formdetail.getCustomerref(j), userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("retval", "" + retval);
						}
					}
				}

				request.setAttribute("retval", "" + retval);
			}

			if (function_type.equals("bulkClosed")) {

				checkjoblength = formdetail.getCheck().length;
				for (int j = 0; j < formdetail.getHid_jobid().length; j++) {
					for (int i = 0; i < checkjoblength; i++) {
						if (formdetail.getCheck(i).equals(
								formdetail.getHid_jobid(j))) {

							retval = IMdao.UpdateInvoiceJob(
									formdetail.getCheck(i),
									formdetail.getBulkInvoiceNo(),
									formdetail.getBulkInvoiceDate(),
									formdetail.getCustomerref(j), userid,
									getDataSource(request, "ilexnewDB"));
							request.setAttribute("retval", "" + retval);
						}
					}
				}

				request.setAttribute("retval", "" + retval);
			}

			if (function_type.equals("Invoice Request")) {
				request.setAttribute("id", formdetail.getId());
				request.setAttribute("jobids", "" + indexvalue);
				request.setAttribute("Type", "" + function_type);
				request.setAttribute("invtype1", formdetail.getType());
				request.setAttribute("invtype2", formdetail.getRdofilter());
				forward = mapping.findForward("sendIR");

				return (forward);
			}
		}

		if (formdetail.getPartner_name() == null)
			formdetail.setPartner_name("");

		if (formdetail.getPowo_number() == null)
			formdetail.setPowo_number("");

		if (formdetail.getFrom_date() == null)
			formdetail.setFrom_date("");

		if (formdetail.getTo_date() == null)
			formdetail.setTo_date("");

		if (formdetail.getInvoiceno() == null)
			formdetail.setInvoiceno("");

		if (request.getParameter("toggle") != null
				&& formdetail.getToggle().equals("true")) {
			list = null;
			request.setAttribute("joblistlength", "" + 0);
			request.setAttribute("id", formdetail.getId());
			request.setAttribute("type", formdetail.getType());
			String url = "/InvoiceJobSearch.do?searchType=Invoice";
			ActionForward fwd = new ActionForward();
			fwd.setPath(url);
			return fwd;
		} else {

			list = IMdao.getJobDetailList(formdetail.getId(),
					formdetail.getType(), sortqueryclause,
					formdetail.getRdofilter(), formdetail.getPartner_name(),
					formdetail.getPowo_number(), formdetail.getFrom_date(),
					formdetail.getTo_date(), formdetail.getInvoiceno(),
					formdetail.getInvoice_Flag(), formdetail.getExportStatus(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("joblist", list);
			request.setAttribute("joblistlength", "" + list.size());
			request.setAttribute("id", formdetail.getId());
			request.setAttribute("type", formdetail.getType());
		}

		DynamicComboCNSWEB dcExportStatusList = new DynamicComboCNSWEB();
		dcExportStatusList.setExportStatusList(DynamicComboDao
				.getExportStatusList(getDataSource(request, "ilexnewDB")));
		request.setAttribute("dcExportStatusList", dcExportStatusList);

		forward = mapping.findForward("success");
		return (forward);
	}

	public String getSortQueryClause(String queryclause, HttpSession session) {
		if (session.getAttribute("inv_sort") != null)
			queryclause = (String) session.getAttribute("inv_sort");
		return queryclause;
	}
}
