package com.mind.actions.IM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.im.InvoiceJobCommissionPOBean;
import com.mind.business.IM.InvoiceJobDelegate;
import com.mind.formbean.IM.InvoiceJobCommissionPOForm;

public class InvoiceJobCommissionPOAction extends
		com.mind.common.IlexDispatchAction {
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		InvoiceJobCommissionPOForm invJobCommPOForm = (InvoiceJobCommissionPOForm) form;
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (invJobCommPOForm.getGo() != null) {
			populateClosedJobList(invJobCommPOForm, request,
					getDataSource(request, "ilexnewDB"));
		} else if (invJobCommPOForm.getCompletePOs() != null) {
			completPOsForListedJobs(invJobCommPOForm, request, loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			setFormDefaultValues(invJobCommPOForm,
					getDataSource(request, "ilexnewDB"));
		}

		return mapping.findForward("success");
	}

	/**
	 * Description - Populates invoiced closed jobs. This is called when user
	 * clicks on 'Go' button on 'Commission PO Completion' page under Accounting
	 * Manager.
	 * 
	 * @param invJobCommPOForm
	 *            - instance of InvoiceJobCommissionPOForm class.
	 * @param request
	 *            - instance of HttpServletRequest class.
	 * @param ds
	 *            - instance of DataSource.
	 */
	private void populateClosedJobList(
			InvoiceJobCommissionPOForm invJobCommPOForm,
			HttpServletRequest request, DataSource ds) {
		InvoiceJobCommissionPOBean invJobCommPoBean = new InvoiceJobCommissionPOBean();
		invJobCommPoBean.setInvoiceNo(invJobCommPOForm.getActualInvoiceNo());
		invJobCommPoBean.setPaymentReceiveDate(invJobCommPOForm
				.getPaymentReceiveDate());
		invJobCommPoBean.setJobId(invJobCommPOForm.getJobId());
		InvoiceJobDelegate.setClosedInvoiceJobListToBean(invJobCommPoBean, ds);

		request.setAttribute("closedJobList",
				invJobCommPoBean.getInvoicedJobs());
		request.setAttribute("closedJobListSize", invJobCommPoBean
				.getInvoicedJobs().size());
		invJobCommPOForm.setInvoiceNo(invJobCommPoBean.getInvoiceNo());
		invJobCommPOForm.setPaymentReceiveDate(invJobCommPoBean
				.getPaymentReceiveDate());
		invJobCommPOForm.setInvoicedJobs(invJobCommPoBean.getInvoicedJobs());
		invJobCommPOForm.setTotalRevenue(invJobCommPoBean.getTotalRevenue());
		invJobCommPOForm.setActualInvoiceNo(invJobCommPoBean.getInvoiceNo());

	}

	/**
	 * Description - Sets default values for InvoiceJobCommissionPOForm form.
	 * 
	 * @param invJobCommPOForm
	 *            - InvoiceJobCommissionPOForm form.
	 * @param ds
	 *            - data source.
	 */
	private void setFormDefaultValues(
			InvoiceJobCommissionPOForm invJobCommPOForm, DataSource ds) {
		InvoiceJobCommissionPOBean invJobCommPoBean = new InvoiceJobCommissionPOBean();

		InvoiceJobDelegate.setDefaultPaymentreceivedDate(invJobCommPoBean, ds);
		invJobCommPOForm.setInvoiceNo(invJobCommPoBean.getInvoiceNo());
		invJobCommPOForm.setPaymentReceiveDate(invJobCommPoBean
				.getPaymentReceiveDate());
		invJobCommPOForm.setInvoicedJobs(invJobCommPoBean.getInvoicedJobs());
		invJobCommPOForm.setTotalRevenue(invJobCommPoBean.getTotalRevenue());
		invJobCommPOForm.setActualInvoiceNo(invJobCommPoBean.getInvoiceNo());

	}

	/**
	 * Description - Complete Commission PO(s) for listed complete jobs that are
	 * retrieved based on Invoice number.
	 * 
	 * @param invJobCommPOForm
	 *            - InvoiceJobCommissionPOForm form.
	 * @param request
	 *            - request object.
	 * @param loginuserid
	 *            - login user id.
	 * @param ds
	 *            - data source.
	 */
	private void completPOsForListedJobs(
			InvoiceJobCommissionPOForm invJobCommPOForm,
			HttpServletRequest request, String loginuserid, DataSource ds) {
		InvoiceJobCommissionPOBean invJobCommPoBean = new InvoiceJobCommissionPOBean();
		invJobCommPoBean.setInvoiceNo(invJobCommPOForm.getActualInvoiceNo());
		invJobCommPoBean.setPaymentReceiveDate(invJobCommPOForm
				.getPaymentReceiveDate());
		invJobCommPoBean.setJobId(invJobCommPOForm.getJobId());

		invJobCommPoBean.setInvoiceNo(invJobCommPOForm.getActualInvoiceNo());

		int invoiceStatus = InvoiceJobDelegate.invoiceCommissionPOsForJobs(
				invJobCommPoBean, loginuserid, ds);
		request.setAttribute("invoiceFlagSuccess",
				String.valueOf(invoiceStatus));

		InvoiceJobDelegate.setClosedInvoiceJobListToBean(invJobCommPoBean, ds);
		request.setAttribute("closedJobList",
				invJobCommPoBean.getInvoicedJobs());
		request.setAttribute("closedJobListSize", invJobCommPoBean
				.getInvoicedJobs().size());
		invJobCommPOForm.setInvoiceNo(invJobCommPoBean.getInvoiceNo());
		invJobCommPOForm.setPaymentReceiveDate(invJobCommPoBean
				.getPaymentReceiveDate());
		invJobCommPOForm.setInvoicedJobs(invJobCommPoBean.getInvoicedJobs());
		invJobCommPOForm.setTotalRevenue(invJobCommPoBean.getTotalRevenue());
		invJobCommPOForm.setActualInvoiceNo(invJobCommPoBean.getInvoiceNo());

	}
}
