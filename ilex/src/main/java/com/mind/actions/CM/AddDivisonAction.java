package com.mind.actions.CM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.cm.AddDivisonBean;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.DynamicComboDaoCM;
import com.mind.dao.CM.CustomerManagerDao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.CM.AddDivisonForm;

/**
 * @version 	1.0
 * @author
 */
/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 **/
/**
 * 
 * Class Explanation: AddDivsionAction is the action class for AddDivison.jsp.
 * It extends com.mind.common.IlexDispatchActon and its methods are called based
 * on the request parameter"function".
 * 
 */

public class AddDivisonAction extends com.mind.common.IlexDispatchAction {
	/**
	 * add method is called by dispatch action class on the basis of the
	 * parameter value of 'function'. It is used for adding and updation of the
	 * division.
	 * 
	 * @param mapping
	 *            ActionMapping instance.
	 * @param form
	 *            form bean of the action.
	 * @param request
	 *            reqest object.
	 * @param response
	 *            responseObject.
	 * @return ActionForward Actionforward object for forwarding the request.
	 */
	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddDivisonForm addDivisonForm = (AddDivisonForm) form;
		AddDivisonBean addDivisonBean = new AddDivisonBean();
		DataSource ds = getDataSource(request, "ilexnewDB");

		DynamicComboDaoCM dcomboCM = new DynamicComboDaoCM();
		DynamicComboCM dccm = new DynamicComboCM();

		ArrayList initialStateCategory = new ArrayList();
		String tempcat = "";
		String[] tempList = null;
		String check = "";
		if (request.getParameter("countrychange") != null)
			check = request.getParameter("countrychange");

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (addDivisonForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid,
					"Manage Customer/CNS/PVS", "Add/Update Division", ds)) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		if (request.getParameter("org_type") != null)
			addDivisonForm.setOrg_type(request.getParameter("org_type"));

		if (request.getParameter("org_discipline_id") != null)
			addDivisonForm.setOrg_discipline_id(request
					.getParameter("org_discipline_id"));
		// 20070901

		if (request.getParameter("org_division_id") != null) {
			if (!addDivisonForm.getOrg_type().equalsIgnoreCase("P")) {
				addDivisonForm.setElement(request
						.getParameter("org_division_id"));
			} else {
				addDivisonForm.setElement(null);
			}
		}

		request.setAttribute("dccm", dccm);
		String[] add_content;

		if (addDivisonForm.getElement() == null
				|| addDivisonForm.getElement().equals("")
				|| Integer.parseInt(addDivisonForm.getElement()) == 0) {
			request.setAttribute("element1", new Integer(0));
			if (addDivisonForm.getCountrychange() == null) {
				addDivisonForm.setCountry("US");
			}

			// CustomerManagerDao.getRefershedStates(addDivisonForm,check,getDataSource(request,
			// "ilexnewDB"));
		} else {
			request.setAttribute("element1",
					new Integer(Integer.parseInt(addDivisonForm.getElement())));
		}
		int statusvalue;
		int flag = 0;
		int customer_id = Integer.parseInt(request.getParameter(
				"org_discipline_id").trim());

		if (addDivisonForm.getSave() != null
				|| addDivisonForm.getConMinuteman() != null) {// Executed if the
																// user press
																// the save
																// button
			if (addDivisonForm.getElement().equals("")
					|| Integer.parseInt(addDivisonForm.getElement()) == 0) {// For
																			// adding
																			// the
																			// a
																			// new
																			// Location/Division

				if (!addDivisonForm.getOrg_type().equals("P"))
					addDivisonForm.setCmboxCoreCompetency(null);

				statusvalue = CustomerManagerDao.addSecondLevelCatg(null,
						String.valueOf(customer_id),
						addDivisonForm.getDivison_name(), null,
						addDivisonForm.getFirstaddressline(),
						addDivisonForm.getSecondaddressline(),
						addDivisonForm.getCity(), addDivisonForm.getState(),
						addDivisonForm.getZip(), addDivisonForm.getCountry(),
						"a", addDivisonForm.getWebsite(),
						addDivisonForm.getConMinuteman(),
						addDivisonForm.getLat_deg(),
						addDivisonForm.getLat_min(),
						addDivisonForm.getLat_direction(),
						addDivisonForm.getLon_deg(),
						addDivisonForm.getLon_min(),
						addDivisonForm.getLon_direction(),
						addDivisonForm.getTimezone(),
						addDivisonForm.getLatlonFound(),
						addDivisonForm.getCmboxCoreCompetency(), userid, "I",
						addDivisonForm.getAccept_address(),
						addDivisonForm.getLatitude(),
						addDivisonForm.getLongitude(),
						addDivisonForm.getLatLongAccuracy(), ds);
				switch (statusvalue) {
				case -9001:
					flag = 1;
					break;
				case -9002:
					flag = 1;
					break;
				case -9003:
					flag = 1;
					break;
				case 0:
					flag = 2;
					break;
				case -9010:
					flag = 2;
					break;
				default:
					flag = 1;
					break;
				}

				addDivisonForm.setAddmessage("" + statusvalue);

				if (statusvalue == 0) {

					request.setAttribute(
							"element1",
							new Integer(CustomerManagerDao.getDivisonid(
									addDivisonForm.getDivison_name(),
									addDivisonForm.getCity(),
									addDivisonForm.getState(),
									addDivisonForm.getZip(), customer_id, ds)));
					addDivisonForm.setElement(CustomerManagerDao.getDivisonid(
							addDivisonForm.getDivison_name(),
							addDivisonForm.getCity(),
							addDivisonForm.getState(), addDivisonForm.getZip(),
							customer_id, ds));

				}
				request.setAttribute("refreshtree", "true");

			} else {

				add_content = CustomerManagerDao.getDivisonInfo(
						addDivisonForm.getElement(), ds);

				if (!addDivisonForm.getOrg_type().equals("P"))
					addDivisonForm.setCmboxCoreCompetency(null);

				statusvalue = CustomerManagerDao.addSecondLevelCatg(
						addDivisonForm.getElement(),
						String.valueOf(customer_id),
						addDivisonForm.getDivison_name(), add_content[0],
						addDivisonForm.getFirstaddressline(),
						addDivisonForm.getSecondaddressline(),
						addDivisonForm.getCity(), addDivisonForm.getState(),
						addDivisonForm.getZip(), addDivisonForm.getCountry(),
						"u", addDivisonForm.getWebsite(),
						addDivisonForm.getConMinuteman(),
						addDivisonForm.getLat_deg(),
						addDivisonForm.getLat_min(),
						addDivisonForm.getLat_direction(),
						addDivisonForm.getLon_deg(),
						addDivisonForm.getLon_min(),
						addDivisonForm.getLon_direction(),
						addDivisonForm.getTimezone(),
						addDivisonForm.getLatlonFound(),
						addDivisonForm.getCmboxCoreCompetency(), userid, "I",
						addDivisonForm.getAccept_address(),
						addDivisonForm.getLatitude(),
						addDivisonForm.getLongitude(),
						addDivisonForm.getLatLongAccuracy(), ds);
				// addDivisonForm.setState(CustomerManagerDao.getStateName(addDivisonForm.getState(),ds));
				addDivisonForm.setState(addDivisonForm.getState());
				switch (statusvalue) {
				case -9001:
					flag = 1;
					break;
				case -9002:
					flag = 1;
					break;
				case -9003:
					flag = 1;
					break;
				case 0:
					flag = 2;
					break;
				case -9010:
					flag = 2;
					break;
				default:
					flag = 1;
					break;
				}
				addDivisonForm.setUpdatemessage("" + statusvalue);
				request.setAttribute("refreshtree", "true");
			}
		} else// The Page is Refreshed
		{
			if (addDivisonForm.getRefresh() != null) {
				addDivisonForm.setWebsite(addDivisonForm.getWebsite());
				addDivisonForm.setAddmessage(null);
			}
		}

		/* If division is not saved */
		if (flag == 1)
			addDivisonForm.setElement(null);

		if ((addDivisonForm.getElement() != null)) {
			if (addDivisonForm.getCountrychange() != null) {
			} else
				addDivisonForm.setDivison_name(CustomerManagerDao
						.getDivisonName(addDivisonForm.getElement(), ds));

			if (addDivisonForm.getCountrychange() != null
					&& !addDivisonForm.getCountrychange().equals("")) {
				BeanUtils.copyProperties(addDivisonBean, addDivisonForm);
				CustomerManagerDao.getRefershedStates(addDivisonBean, check,
						getDataSource(request, "ilexnewDB"));
				BeanUtils.copyProperties(addDivisonForm, addDivisonBean);
				addDivisonForm.setCountrychange(null);
			} else {
				add_content = CustomerManagerDao.getDivisonInfo(
						addDivisonForm.getElement(), ds);
				addDivisonForm.setFirstaddressline(add_content[1]);
				addDivisonForm.setSecondaddressline(add_content[2]);
				addDivisonForm.setCity(add_content[3]);
				addDivisonForm.setState(add_content[4]);
				addDivisonForm.setZip(add_content[5]);
				addDivisonForm.setCountry(add_content[6]);
				if (add_content[7] != null && !add_content[7].equals(""))
					addDivisonForm.setCmboxCoreCompetency(add_content[7]
							.split(","));
				else
					addDivisonForm.setCmboxCoreCompetency(null);

				addDivisonForm.setTimezone(add_content[8]);

				addDivisonForm.setLat_deg(add_content[10]);
				addDivisonForm.setLat_min(add_content[11]);
				addDivisonForm.setLat_direction(add_content[12]);
				addDivisonForm.setLon_deg(add_content[13]);
				addDivisonForm.setLon_min(add_content[14]);
				addDivisonForm.setLon_direction(add_content[15]);

				addDivisonForm.setAccept_address(add_content[16]);
				addDivisonForm.setLatitude(add_content[17]);
				addDivisonForm.setLongitude(add_content[18]);
				addDivisonForm.setLatLongAccuracy(add_content[19]);
				addDivisonForm.setLatlonFound(add_content[20]);

			}
			addDivisonForm.setWebsite(CustomerManagerDao.getWebsite(
					addDivisonForm.getElement(), ds));
			if (addDivisonForm.getUpdatemessage() != null) {

				addDivisonForm.setState(addDivisonForm.getState());
			}
			if (addDivisonForm.getAddmessage() != null)
				addDivisonForm.setState(addDivisonForm.getState());
		}

		DynamicComboCM dcTimeZoneList = new DynamicComboCM();
		dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao.gettimezone(ds));
		request.setAttribute("dcTimeZoneList", dcTimeZoneList);

		DynamicComboCM dcCountryList = new DynamicComboCM();
		dcCountryList.setFirstlevelcatglist(DynamicComboDao.getCountryList(ds));
		request.setAttribute("dcCountryList", dcCountryList);

		DynamicComboCM dcLatDirec = new DynamicComboCM();
		dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
		request.setAttribute("dcLatDirec", dcLatDirec);

		DynamicComboCM dcLonDirec = new DynamicComboCM();
		dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
		request.setAttribute("dcLonDirec", dcLonDirec);

		DynamicComboCM dcCoreCompetancy = new DynamicComboCM();
		dcCoreCompetancy.setFirstlevelcatglist(Pvsdao.getCoreCompetancy(ds));
		request.setAttribute("dcCoreCompetancy", dcCoreCompetancy);

		DynamicComboCM dcCategoryList = new DynamicComboCM();
		dcCategoryList.setFirstlevelcatglist(DynamicComboDao
				.getCategoryList(ds));
		request.setAttribute("dcCategoryList", dcCategoryList);

		DynamicComboCM dcRegionList = new DynamicComboCM();
		dcRegionList.setFirstlevelcatglist(DynamicComboDao.getRegionList(ds));
		request.setAttribute("dcRegionList", dcRegionList);

		initialStateCategory = CustomerManagerDao
				.getStateCategoryList(getDataSource(request, "ilexnewDB"));

		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempcat = ((AddDivisonBean) initialStateCategory.get(i))
						.getSiteCountryId();
				((AddDivisonBean) initialStateCategory.get(i))
						.setTempList(SiteManagedao
								.getCategoryWiseStateList(tempcat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}

		request.setAttribute("initialStateCategory", initialStateCategory);

		dccm.setSecondlevelcatglist(DynamicComboDaoCM.getSecondcatglist(
				request.getParameter("org_discipline_id"), ds));
		DynamicComboCM dcStateCM = new DynamicComboCM();
		dcStateCM.setFirstlevelcatglist(Pvsdao.getStateList(ds));
		request.setAttribute("dcStateCM", dcStateCM);

		if (addDivisonForm.getElement() == null
				|| Integer.parseInt(addDivisonForm.getElement()) == 0) {
			request.setAttribute("element1", new Integer(0));
			// addDivisonForm.setCountry("US");
		}

		addDivisonForm.setCustname(CustomerManagerDao.getCustomerName(""
				+ customer_id, (String) request.getParameter("org_type"), ds));
		request.setAttribute("customer_id", new Integer(customer_id));
		request.setAttribute("conMinuteman", addDivisonForm.getConMinuteman());

		if (addDivisonForm.getOrg_type().equalsIgnoreCase("P")
				&& addDivisonForm.getSave() == null
				&& addDivisonForm.getRefresh() == null
				&& request.getParameter("type") == null) {
			request.setAttribute("org", "" + addDivisonForm.getOrg_type());
			request.setAttribute(
					"search_type",
					CustomerManagerDao.getCustomerName(
							request.getParameter("org_discipline_id"), ds));
			request.setAttribute("org_discipline_id",
					addDivisonForm.getOrg_discipline_id());
			request.setAttribute("divPage", "y");
			forward = mapping.findForward("search");
			return forward;
		}

		flag = 1;

		/* Get time zone name */
		addDivisonForm.setTimeZoneName(CustomerManagerDao.getTimeZoneName(
				"division", addDivisonForm.getElement(),
				getDataSource(request, "ilexnewDB")));
		/* For State combox */
		addDivisonForm.setStateSelect(Util.concatString(
				addDivisonForm.getState(), addDivisonForm.getCountry()));

		if (flag == 1)
			forward = mapping.findForward("success");
		else
			forward = mapping.findForward("view");
		return (forward);

	}

	/**
	 * delete method is called by dispatch action class on the basis of the
	 * parameter value of 'function'. It is used for deletion of the division.
	 * 
	 * @param mapping
	 *            ActionMapping instance.
	 * @param form
	 *            form bean of the action.
	 * @param request
	 *            reqest object.
	 * @param response
	 *            responseObject.
	 * @return ActionForward Actionforward object for forwarding the request.
	 */

	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();

		DataSource ds = getDataSource(request, "ilexnewDB");
		ArrayList initialStateCategory = new ArrayList();
		AddDivisonForm addDivisonForm = (AddDivisonForm) form;
		addDivisonForm.reset(mapping, request);
		String tempcat = "";

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (addDivisonForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid,
					"Manage Customer/CNS/PVS", "Delete Division", ds)) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		if (request.getParameter("org_type") != null)
			addDivisonForm.setOrg_type(request.getParameter("org_type"));

		if (request.getParameter("org_discipline_id") != null)
			addDivisonForm.setOrg_discipline_id(request
					.getParameter("org_discipline_id"));

		DynamicComboDaoCM dcomboCM = new DynamicComboDaoCM();
		DynamicComboCM dccm = new DynamicComboCM();

		request.setAttribute("dccm", dccm);
		String[] add_content;

		request.setAttribute("element1", new Integer(0));

		int statusvalue;
		int flag = 0;

		int customer_id = Integer.parseInt(request.getParameter(
				"org_discipline_id").trim());
		addDivisonForm.setCustname(CustomerManagerDao.getCustomerName(""
				+ customer_id, (String) request.getParameter("org_type"), ds));
		request.setAttribute("customer_id", new Integer(customer_id));

		statusvalue = CustomerManagerDao.deleteSecondLevelCatg(
				(String) request.getParameter("element"), ds);
		addDivisonForm.setDeletemessage("" + statusvalue);

		addDivisonForm.setFunction("add");
		request.setAttribute("refreshtree", "true");

		dccm.setSecondlevelcatglist(DynamicComboDaoCM.getSecondcatglist(
				request.getParameter("org_discipline_id"), ds));

		// DynamicComboCM dcStateCM= new DynamicComboCM();
		// dcStateCM.setFirstlevelcatglist(Pvsdao.getStateList(ds));
		// request.setAttribute("dcStateCM",dcStateCM);
		initialStateCategory = CustomerManagerDao
				.getStateCategoryList(getDataSource(request, "ilexnewDB"));

		if (initialStateCategory.size() > 0) {
			for (int i = 0; i < initialStateCategory.size(); i++) {
				tempcat = ((AddDivisonBean) initialStateCategory.get(i))
						.getSiteCountryId();
				((AddDivisonBean) initialStateCategory.get(i))
						.setTempList(SiteManagedao
								.getCategoryWiseStateList(tempcat, i,
										getDataSource(request, "ilexnewDB")));
			}
		}

		request.setAttribute("initialStateCategory", initialStateCategory);
		// System.out.println("List  "+dcStateCM.getFirstlevelcatglist());
		DynamicComboCM dcCountryList = new DynamicComboCM();
		dcCountryList.setFirstlevelcatglist(DynamicComboDao.getCountryList(ds));
		request.setAttribute("dcCountryList", dcCountryList);

		DynamicComboCM dcLatDirec = new DynamicComboCM();
		dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
		request.setAttribute("dcLatDirec", dcLatDirec);

		DynamicComboCM dcLonDirec = new DynamicComboCM();
		dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
		request.setAttribute("dcLonDirec", dcLonDirec);

		DynamicComboCM dcCategoryList = new DynamicComboCM();
		dcCategoryList.setFirstlevelcatglist(DynamicComboDao
				.getCategoryList(ds));
		request.setAttribute("dcCategoryList", dcCategoryList);

		DynamicComboCM dcRegionList = new DynamicComboCM();
		dcRegionList.setFirstlevelcatglist(DynamicComboDao.getRegionList(ds));
		request.setAttribute("dcRegionList", dcRegionList);

		DynamicComboCM dcTimeZoneList = new DynamicComboCM();
		dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao.gettimezone(ds));
		request.setAttribute("dcTimeZoneList", dcTimeZoneList);

		DynamicComboCM dcCoreCompetancy = new DynamicComboCM();
		dcCoreCompetancy.setFirstlevelcatglist(Pvsdao.getCoreCompetancy(ds));
		request.setAttribute("dcCoreCompetancy", dcCoreCompetancy);

		addDivisonForm.setCountry("US");

		forward = mapping.findForward("success");
		return (forward);
	}

}
