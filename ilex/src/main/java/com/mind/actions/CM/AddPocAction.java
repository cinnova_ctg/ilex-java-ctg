package com.mind.actions.CM;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import net.sf.json.JSONArray;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.cm.AddDivisonBean;
import com.mind.bean.cm.AddPocBean;
import com.mind.common.IlexDispatchAction;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.CM.CustomerManagerDao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.formbean.CM.AddPocForm;

/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 *
 **/
/**
 * Class Explanation: AddPocAction is the action class for AddPoc.jsp. It
 * extends IlexDispatchAction and its methods are called based on the request
 * parameter"function".
 *
 */
public class AddPocAction extends IlexDispatchAction {

    /**
     * add method is called by dispatch action class on the basis of the
     * parameter value of 'function'. It is used for addition of the Poc.
     *
     * @param mapping ActionMapping instance.
     * @param form form bean of the action.
     * @param request reqest object.
     * @param response responseObject.
     * @return ActionForward Actionforward object for forwarding the request.
     */
    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();
        AddPocForm addPocForm = (AddPocForm) form;
        AddPocBean addPocBean = new AddPocBean();
        DataSource ds = getDataSource(request, "ilexnewDB");

        String[] division_address = null; // it will contain the content of
        // addresses
        String indexvalue = ""; // used for role assignment
        String statusvalue1 = ""; // return value from SP
        int statusvalue = 0;// used for storage of return value of Stored
        // Procedure
        int chkrole = 0;
        String updatedrole = "";
        String lo_pc_ad_id = null;// used for assigning the address id
        int flag = 0;// used for determination of action forward
        String address_details[] = {null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null};// temporay
        // storage
        // for
        // address
        // details
        ArrayList initialStateCategory = new ArrayList();
        String tempcat = "";

        /* Page Security Start */
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }		// expired
        String userid = (String) session.getAttribute("userid");

        if (addPocForm.getAuthenticate().equals("")) {
            if (!Authenticationdao.getPageSecurity(userid,
                    "Manage Customer/CNS/PVS", "Add/Update POC", ds)) {
                return (mapping.findForward("UnAuthenticate"));
            }
        }
        /* Page Security End */

        if (addPocForm.getLo_pc_id() == null) {
            addPocForm.setLo_pc_id("0");
            addPocForm.setEmpStatus("1");
        }

        /* get the combo lists */
        DynamicComboCM dcTimeZoneList = new DynamicComboCM();
        dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao.gettimezone(ds));
        request.setAttribute("dcTimeZoneList", dcTimeZoneList);

        DynamicComboCM dcCountryList = new DynamicComboCM();
        dcCountryList.setFirstlevelcatglist(DynamicComboDao.getCountryList(ds));
        request.setAttribute("dcCountryList", dcCountryList);

        DynamicComboCM dcLatDirec = new DynamicComboCM();
        dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
        request.setAttribute("dcLatDirec", dcLatDirec);

        DynamicComboCM dcLonDirec = new DynamicComboCM();
        dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
        request.setAttribute("dcLonDirec", dcLonDirec);

        DynamicComboCM dcCategoryList = new DynamicComboCM();
        dcCategoryList.setFirstlevelcatglist(DynamicComboDao
                .getCategoryList(ds));
        request.setAttribute("dcCategoryList", dcCategoryList);

        DynamicComboCM dcRegionList = new DynamicComboCM();
        dcRegionList.setFirstlevelcatglist(DynamicComboDao.getRegionList(ds));
        request.setAttribute("dcRegionList", dcRegionList);

        DynamicComboCM dcClassification = new DynamicComboCM();
        dcClassification.setFirstlevelcatglist(DynamicComboDao
                .getClassification(ds));
        request.setAttribute("dcClassification", dcClassification);

        DynamicComboCM dcCNSPOCTitles = new DynamicComboCM();
        dcCNSPOCTitles.setFirstlevelcatglist(DynamicComboDao
                .getCNSPOCTitles(ds));
        request.setAttribute("dcCNSPOCTitles", dcCNSPOCTitles);

        initialStateCategory = CustomerManagerDao
                .getStateCategoryList(getDataSource(request, "ilexnewDB"));

        if (initialStateCategory.size() > 0) {
            for (int i = 0; i < initialStateCategory.size(); i++) {
                tempcat = ((AddDivisonBean) initialStateCategory.get(i))
                        .getSiteCountryId();
                ((AddDivisonBean) initialStateCategory.get(i))
                        .setTempList(SiteManagedao
                                .getCategoryWiseStateList(tempcat, i,
                                        getDataSource(request, "ilexnewDB")));
            }
        }

        request.setAttribute("initialStateCategory", initialStateCategory);

        /* get country, region and category */
        if (addPocForm.getRefreshState() != null) {
            if (addPocForm.getRefreshState().equals("true")) {
                BeanUtils.copyProperties(addPocBean, addPocForm);
                CustomerManagerDao.getRefreshState(addPocBean,
                        getDataSource(request, "ilexnewDB"));
                BeanUtils.copyProperties(addPocForm, addPocBean);
            }
        }

        // setting the name of type of customer and its division/location
        addPocForm.setOrg_name(CustomerManagerDao.getCustomerName(
                addPocForm.getOrg_discipline_id(), ds));
        addPocForm.setDivision_name(CustomerManagerDao.getDivisionName(
                addPocForm.getDivision_id(), ds));

        // setting the address of division and attaching it to the request
        // attribute
        division_address = CustomerManagerDao.getDivisonInfo(
                addPocForm.getDivision_id(), ds);

        // division_address[4]=(CustomerManagerDao.getStateName(division_address[4],ds));
        request.setAttribute("division_address", division_address);
        if (addPocForm.getFirstname() == null
                || addPocForm.getFirstname().length() == 0) {
            addPocForm.setCountry("US");
        }

        if (addPocForm.getSave() != null) { // if the button save is clicked
            if (addPocForm.getCkboxUseDivison() != null) { // if PoC is using
                // Division address
                lo_pc_ad_id = division_address[0];
                addPocForm.setFirstaddressline(division_address[1]);
                addPocForm.setSecondaddressline(division_address[2]);
                addPocForm.setCity(division_address[3]);
                addPocForm.setState(division_address[4]);
                addPocForm.setZip(division_address[5]);
                addPocForm.setCountry(division_address[6]);
                addPocForm.setTimeZoneId(division_address[8]);
                address_details[6] = division_address[10];
                address_details[7] = division_address[11];
                address_details[8] = division_address[12];
                address_details[9] = division_address[13];
                address_details[10] = division_address[14];
                address_details[11] = division_address[15];
            } else {
                lo_pc_ad_id = null; // otherwise set address_id to null
                address_details[0] = addPocForm.getFirstaddressline();
                address_details[1] = addPocForm.getSecondaddressline();
                address_details[2] = addPocForm.getCity();
                address_details[3] = addPocForm.getState();
                address_details[4] = addPocForm.getZip();
                address_details[5] = addPocForm.getCountry();
                address_details[6] = addPocForm.getLat_deg();
                address_details[7] = addPocForm.getLat_min();
                address_details[8] = addPocForm.getLat_direction();
                address_details[9] = addPocForm.getLon_deg();
                address_details[10] = addPocForm.getLon_min();
                address_details[11] = addPocForm.getLon_direction();
                // address_details[12] = addPocForm.getCategory_id();
                // addPocFormaddress_details[13] = addPocForm.getRegion_id();
                address_details[12] = addPocForm.getTimeZoneId();
            }

            if (addPocForm.getCheck() != null
                    && addPocForm.getRole_assign() != null) { // if PoC has
                // assigned some
                // role

                if (addPocForm.getCheck().length > 0) {
                    String index[] = new String[addPocForm.getCheck().length];
                    index = addPocForm.getCheck();

                    for (int i = 0; i < addPocForm.getCheck().length; i++) {
                        indexvalue = indexvalue + index[i] + ",";
                    }
                }
            }

            if (addPocForm.getLo_pc_id().equals("")
                    || addPocForm.getLo_pc_id().equals("0")) {// For Adding a  // new Poc
                // record

                statusvalue1 = CustomerManagerDao.setThirdLevelCatg(null,
                        addPocForm.getRole_assign(), indexvalue, null,
                        lo_pc_ad_id, null, addPocForm.getDivision_id(),
                        addPocForm.getFirstname(), null,
                        addPocForm.getLastname(), addPocForm.getTitle(),
                        addPocForm.getPhone1(), addPocForm.getPhone2(),
                        addPocForm.getFax(), addPocForm.getEmail1(),
                        addPocForm.getEmail2(), addPocForm.getMobile(),
                        address_details[0], address_details[1],
                        address_details[2], address_details[3],
                        address_details[4], address_details[5],
                        address_details[6], address_details[7],
                        address_details[8], address_details[9],
                        address_details[10], address_details[11],
                        address_details[12], addPocForm.getUser_name(),
                        addPocForm.getPassword(), addPocForm.getLatlonFound(),
                        "a", addPocForm.getPvsContact(),
                        addPocForm.getClassificationId(),
                        addPocForm.getEmpStatus(), userid, ds,
                        addPocForm.getRank(), addPocForm.getInvoiceApproval());

                statusvalue = Integer.parseInt(statusvalue1.substring(0,
                        statusvalue1.indexOf(",")));
                statusvalue1 = statusvalue1.substring(
                        statusvalue1.indexOf(",") + 1, statusvalue1.length());
                chkrole = Integer.parseInt(statusvalue1.substring(0,
                        statusvalue1.indexOf(",")));
                updatedrole = statusvalue1.substring(
                        statusvalue1.indexOf(",") + 1,
                        statusvalue1.lastIndexOf(","));
                updatedrole = statusvalue1.substring(
                        statusvalue1.indexOf(",") + 1,
                        statusvalue1.lastIndexOf(","));
                // Changes Made to Get the User Name Dynamic Base Upon the
                // Condition User is Active User or Not
                String userName = statusvalue1.substring(
                        statusvalue1.lastIndexOf(",") + 1,
                        statusvalue1.length());
                addPocForm.setUser_name(userName);
                if (statusvalue == 0) {// if addition is successful then set the
                    // new lo_pc_id

                    addPocForm.setLo_pc_id(CustomerManagerDao.getPocId(
                            addPocForm.getDivision_id(),
                            addPocForm.getFirstname(),
                            addPocForm.getLastname(), ds));
                    flag = 1;
                }

                addPocForm.setLo_pc_ad_id(CustomerManagerDao.getPocaddressid(
                        addPocForm.getDivision_id(), addPocForm.getFirstname(),
                        addPocForm.getLastname(), ds));
                addPocForm.setAddmessage("" + statusvalue);
                request.setAttribute("chkrole", "" + chkrole);
                request.setAttribute("refreshtree", "true");
            } else {// For Updation of Poc record

                addPocForm
                        .setLo_pc_ad_id(CustomerManagerDao.getPocaddressid(
                                        addPocForm.getOrg_discipline_id(),
                                        addPocForm.getFirstname(),
                                        addPocForm.getLastname(), ds));
                String chkbox = "N";
                if (addPocForm.getCkboxUseDivison() != null) {
                    chkbox = "Y";
                }

                statusvalue1 = CustomerManagerDao.setThirdLevelCatg(
                        addPocForm.getLo_pc_id(), addPocForm.getRole_assign(),
                        indexvalue, chkbox, lo_pc_ad_id, null,
                        addPocForm.getDivision_id(), addPocForm.getFirstname(),
                        null, addPocForm.getLastname(), addPocForm.getTitle(),
                        addPocForm.getPhone1(), addPocForm.getPhone2(),
                        addPocForm.getFax(), addPocForm.getEmail1(),
                        addPocForm.getEmail2(), addPocForm.getMobile(),
                        address_details[0], address_details[1],
                        address_details[2], address_details[3],
                        address_details[4], address_details[5],
                        address_details[6], address_details[7],
                        address_details[8], address_details[9],
                        address_details[10], address_details[11],
                        address_details[12], addPocForm.getUser_name(),
                        addPocForm.getPassword(), addPocForm.getLatlonFound(),
                        "u", addPocForm.getPvsContact(),
                        addPocForm.getClassificationId(),
                        addPocForm.getEmpStatus(), userid, ds,
                        addPocForm.getRank(), addPocForm.getInvoiceApproval());

                statusvalue = Integer.parseInt(statusvalue1.substring(0,
                        statusvalue1.indexOf(",")));
                statusvalue1 = statusvalue1.substring(
                        statusvalue1.indexOf(",") + 1, statusvalue1.length());
                chkrole = Integer.parseInt(statusvalue1.substring(0,
                        statusvalue1.indexOf(",")));
                updatedrole = statusvalue1.substring(
                        statusvalue1.indexOf(",") + 1,
                        statusvalue1.lastIndexOf(","));

                addPocForm.setUpdatemessage("" + statusvalue);
                if (statusvalue == 0) {// if updation is successful then go to
                    // view
                    flag = 1;
                }
                request.setAttribute("chkrole", "" + chkrole);
                request.setAttribute("refreshtree", "true");
            }
            //  populates headcount report
            // System.out.println(" ------------- ");------------
            Collection lst = dcClassification.getFirstlevelcatglist();
            String deptName = "";
            for (Iterator iterator = lst.iterator(); iterator.hasNext();) {
                LabelValue type = (LabelValue) iterator.next();
                if (type.getValue().equals(addPocForm.getClassificationId())) {
                    deptName = type.getLabel();
                    break;
                }
            }

            BeanUtils.copyProperties(addPocBean, addPocForm);
            if ("N".equals(addPocForm.getOrg_type())) {
                String[] hcDtl = CustomerManagerDao.getHeadCountIdByUserId(
                        addPocForm.getLo_pc_id(), ds);

                // if (!addPocForm.getEmpStatus().equals("4")) {
                // int counter = 0;
                if (hcDtl == null) {

                    CustomerManagerDao.addIntoHeadCount(deptName, addPocBean,
                            ds);

                } else {

                    if (!hcDtl[1].contains(deptName)
                            || !hcDtl[2].contains(addPocForm.getRank()) || !hcDtl[4].contains(addPocForm.getTitle())) {
                        // String[] ar = hcDtl.split(",");// 0 index is Hc_Id
                        CustomerManagerDao.updateHeadCountTerminationDate(
                                hcDtl[0], ds);
                        CustomerManagerDao.addIntoHeadCount(deptName,
                                addPocBean, ds);
                    }

                    if (addPocForm.getEmpStatus().equals("0")) {
                        // String[] ar = hcDtl.split(",");// 0 index is Hc_Id
                        CustomerManagerDao.updateHeadCountTerminationDate(
                                hcDtl[0], ds);
                    }
					// }

                    // String[] arr = hcDtl.split(",");
					/*
                     * if status is not termination then modify status for all
                     * records in store table.
                     */
                    if (!addPocForm.getEmpStatus().equals("0")
                            && !addPocForm.getEmpStatus().equals(hcDtl[3])) {

                        CustomerManagerDao.updateHeadCountEmpStatus(
                                addPocForm.getLo_pc_id(),
                                addPocForm.getEmpStatus(), ds);

                    }
                }

            }

        }

        // if the PoC is having it own address
        if (addPocForm.getCkboxUseDivison() == null) {
            request.setAttribute("a", "visible");
            request.setAttribute("b", "hidden");
        } else { // if the PoC is using divisin/location address

            request.setAttribute("b", "visible");
            request.setAttribute("a", "hidden");

        }

        // if Poc have not assigned any role
        if (addPocForm.getRole_assign() == null) {
            request.setAttribute("c", "hidden");
            request.setAttribute("d", "visible");
        } else {// if Poc have assigned some role
            request.setAttribute("d", "hidden");
            request.setAttribute("c", "visible");
        }

        ArrayList RoleList = CustomerManagerDao.getRoleName(
                request.getParameter("org_type"), ds);
        request.setAttribute("RoleList", RoleList);

        /* Get time zone name */
        addPocForm.setTimeZoneName(CustomerManagerDao.getTimeZoneName("poc",
                addPocForm.getLo_pc_id(), getDataSource(request, "ilexnewDB")));
        addPocForm.setStateSelect(Util.concatString(addPocForm.getState(),
                addPocForm.getCountry()));
        /* get rank list */
        List<String> rankList = new ArrayList<String>();
        rankList = DynamicComboDao.getRankList(ds,
                addPocForm.getClassificationId());
        request.setAttribute("rankList", rankList);

        /* make use division checked for new CNS contact */
        if (addPocForm.getOrg_type().equals("N")
                && addPocForm.getFirstname() == null) {
            addPocForm.setCkboxUseDivison("true");
            request.setAttribute("b", "visible");
            request.setAttribute("a", "hidden");
        }
        if (flag == 1) {
            if (!indexvalue.equals("")) {
                request.setAttribute("roleview",
                        CustomerManagerDao.Rolename(updatedrole, ds));
            }
            if (addPocForm.getCkboxUseDivison() != null) {
                addPocForm.setState(division_address[4]);
            } else {
                // addPocForm.setState(CustomerManagerDao.getStateName(
                // addPocForm.getState(),ds));
                addPocForm.setState(addPocForm.getState());
            }
            request.setAttribute("org_type", addPocForm.getOrg_type());

            request.setAttribute("loPcId", addPocForm.getLo_pc_id());
            request.setAttribute("empStatus", addPocForm.getEmpStatus());
            request.setAttribute("orgName", addPocForm.getOrg_name());

            // addPocForm.setCkboxUseDivison("false");
            forward = mapping.findForward("view");
        } else {
            if (addPocForm.getOrg_type().trim().equals("P")) {
                request.setAttribute("b", "visible");
                addPocForm.setCkboxUseDivison("false");
            }
            forward = mapping.findForward("success");
        }
        return (forward);
    }

    /**
     * update method is called by dispatch action class on the basis of the
     * parameter value of 'function'. It is used for updation of the Poc.
     *
     * @param mapping ActionMapping instance.
     * @param form form bean of the action.
     * @param request request object.
     * @param response responseObject.
     * @return ActionForward Actionforward object for forwarding the request.
     */
    public ActionForward update(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();
        AddPocForm addPocForm = (AddPocForm) form;
        AddPocBean addPocBean = new AddPocBean();
        DataSource ds = getDataSource(request, "ilexnewDB");

        String[] division_address = null;// temporay storage for address details
        ArrayList initialStateCategory = new ArrayList();
        String tempcat = "";

        /* Page Security Start */
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }		// expired
        String userid = (String) session.getAttribute("userid");
        if (addPocForm.getAuthenticate().equals("")) {
            if (!Authenticationdao.getPageSecurity(userid,
                    "Manage Customer/CNS/PVS", "Add/Update POC", ds)) {
                return (mapping.findForward("UnAuthenticate"));
            }
        }
        /* Page Security End */

        if (request.getParameter("lo_pc_id") != null) {
            addPocForm.setLo_pc_id(request.getParameter("lo_pc_id"));
        }

        /* get the combo lists */
        DynamicComboCM dcTimeZoneList = new DynamicComboCM();
        dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao.gettimezone(ds));
        request.setAttribute("dcTimeZoneList", dcTimeZoneList);

        DynamicComboCM dcCountryList = new DynamicComboCM();
        dcCountryList.setFirstlevelcatglist(DynamicComboDao.getCountryList(ds));
        request.setAttribute("dcCountryList", dcCountryList);

        DynamicComboCM dcLatDirec = new DynamicComboCM();
        dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
        request.setAttribute("dcLatDirec", dcLatDirec);

        DynamicComboCM dcLonDirec = new DynamicComboCM();
        dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
        request.setAttribute("dcLonDirec", dcLonDirec);

        DynamicComboCM dcCategoryList = new DynamicComboCM();
        dcCategoryList.setFirstlevelcatglist(DynamicComboDao
                .getCategoryList(ds));
        request.setAttribute("dcCategoryList", dcCategoryList);

        DynamicComboCM dcRegionList = new DynamicComboCM();
        dcRegionList.setFirstlevelcatglist(DynamicComboDao.getRegionList(ds));
        request.setAttribute("dcRegionList", dcRegionList);

        DynamicComboCM dcClassification = new DynamicComboCM();
        dcClassification.setFirstlevelcatglist(DynamicComboDao
                .getClassification(ds));
        request.setAttribute("dcClassification", dcClassification);

        DynamicComboCM dcCNSPOCTitles = new DynamicComboCM();
        dcCNSPOCTitles.setFirstlevelcatglist(DynamicComboDao
                .getCNSPOCTitles(ds));
        request.setAttribute("dcCNSPOCTitles", dcCNSPOCTitles);

        initialStateCategory = CustomerManagerDao
                .getStateCategoryList(getDataSource(request, "ilexnewDB"));

        if (initialStateCategory.size() > 0) {
            for (int i = 0; i < initialStateCategory.size(); i++) {
                tempcat = ((AddDivisonBean) initialStateCategory.get(i))
                        .getSiteCountryId();
                ((AddDivisonBean) initialStateCategory.get(i))
                        .setTempList(SiteManagedao
                                .getCategoryWiseStateList(tempcat, i,
                                        getDataSource(request, "ilexnewDB")));
            }
        }

        request.setAttribute("initialStateCategory", initialStateCategory);

        // setting the name of type of customer and its division/location
        addPocForm.setOrg_name(CustomerManagerDao.getCustomerName(
                addPocForm.getOrg_discipline_id(), ds));
        addPocForm.setDivision_name(CustomerManagerDao.getDivisionName(
                addPocForm.getDivision_id(), ds));

        // setting the address of division and attaching it to the request
        // attribute
        division_address = CustomerManagerDao.getDivisonInfo(
                addPocForm.getDivision_id(), ds);

        // division_address[4]=(CustomerManagerDao.getStateName(division_address[4],ds));
        request.setAttribute("division_address", division_address);
        BeanUtils.copyProperties(addPocBean, addPocForm);

        CustomerManagerDao.getPocdetails(addPocBean,
                request.getParameter("lo_pc_id"), ds);
        BeanUtils.copyProperties(addPocForm, addPocBean);

        /* Get time zone name */
        addPocForm.setTimeZoneName(CustomerManagerDao.getTimeZoneName("poc",
                addPocForm.getLo_pc_id(), getDataSource(request, "ilexnewDB")));
        addPocForm.setClassificationId(CustomerManagerDao.getClassification(
                addPocForm.getLo_pc_id(), getDataSource(request, "ilexnewDB")));
        addPocForm.setEmpStatus(CustomerManagerDao.getEmpStatus(
                addPocForm.getLo_pc_id(), getDataSource(request, "ilexnewDB")));
        addPocForm.setStateSelect(Util.concatString(addPocForm.getState(),
                addPocForm.getCountry()));
        /* get rank list */
        List<String> rankList = new ArrayList<String>();
        rankList = DynamicComboDao.getRankList(ds,
                addPocForm.getClassificationId());
        request.setAttribute("rankList", rankList);
        ArrayList RoleList = CustomerManagerDao.getRoleName(
                request.getParameter("org_type"), ds);
        request.setAttribute("RoleList", RoleList);

        addPocForm.setCheck(CustomerManagerDao.getPocRoleArray(
                addPocForm.getLo_pc_id(), ds));

        if (addPocForm.getLo_pc_ad_id() == null) {
            if (addPocForm.getFirstaddressline() == null
                    || addPocForm.getFirstaddressline().length() == 0) {
                addPocForm.setCountry("US");
            }
            request.setAttribute("a", "visible");
            request.setAttribute("b", "hidden");
        } else { // if the PoC is using division/location address
            if (addPocForm.getFirstaddressline() == null
                    || addPocForm.getFirstaddressline().length() == 0) {
                addPocForm.setCountry("US");
            }
            addPocForm.setCkboxUseDivison("true");
            request.setAttribute("b", "visible");
            request.setAttribute("a", "hidden");
        }

        // if Poc have not assigned any role
        if (addPocForm.getCheck() == null) {
            request.setAttribute("c", "hidden");
            request.setAttribute("d", "visible");
        } else {// if Poc have assigned some role
            addPocForm.setRole_assign("true");
            request.setAttribute("d", "hidden");
            request.setAttribute("c", "visible");
        }
        addPocForm.setFunction("add");

        if (addPocForm.getOrg_type().trim().equals("P")) {
            addPocForm.setCkboxUseDivison("false");
            request.setAttribute("b", "visible");
        }

        forward = mapping.findForward("success");
        return (forward);

    }

    /**
     * delete method is called by dispatch action class on the basis of the
     * parameter value of 'function'. It is used for deletion of the Poc.
     *
     * @param mapping ActionMapping instance.
     * @param form form bean of the action.
     * @param request request object.
     * @param response responseObject.
     * @return ActionForward Actionforward object for forwarding the request.
     */
    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String[] division_address = null; // it will contain the content of
        // addresses
        int statusvalue = 0;// used for storage of return value of Stored
        // Procedure
        String address_details[] = {null, null, null, null, null, null};// temporay
        // storage
        // for
        // address
        // details
        String org_type, division_id, org_discipline_id, org_name, division_name, lo_pc_id;
        DataSource ds = getDataSource(request, "ilexnewDB");

        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();
        AddPocForm addPocForm = (AddPocForm) form;
        ArrayList initialStateCategory = new ArrayList();
        String tempcat = "";
        /* Page Security Start */
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userid") == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
        }		// expired
        String userid = (String) session.getAttribute("userid");
        if (addPocForm.getAuthenticate().equals("")) {
            if (!Authenticationdao.getPageSecurity(userid,
                    "Manage Customer/CNS/PVS", "Delete POC", ds)) {
                return (mapping.findForward("UnAuthenticate"));
            }
        }
        /* Page Security End */

        // setting the name of type of customer and its division/location
        org_type = addPocForm.getOrg_type();
        division_id = addPocForm.getDivision_id();
        org_discipline_id = addPocForm.getOrg_discipline_id();
        org_name = CustomerManagerDao.getCustomerName(
                addPocForm.getOrg_discipline_id(), ds);
        division_name = CustomerManagerDao.getDivisionName(
                addPocForm.getDivision_id(), ds);
        lo_pc_id = addPocForm.getLo_pc_id();

        // setting the address of division and attaching it to the request
        // attribute
        division_address = CustomerManagerDao.getDivisonInfo(
                addPocForm.getDivision_id(), ds);
        // division_address[4]=(CustomerManagerDao.getStateName(division_address[4],ds));

        request.setAttribute("division_address", division_address);

        statusvalue = CustomerManagerDao
                .deletePoc(addPocForm.getLo_pc_id(), ds);

        addPocForm.reset(mapping, request);

        addPocForm.setDeletemessage("" + statusvalue);
        addPocForm.setOrg_type(org_type);
        addPocForm.setDivision_id(division_id);
        addPocForm.setOrg_discipline_id(org_discipline_id);
        addPocForm.setOrg_name(org_name);
        addPocForm.setDivision_name(division_name);
        addPocForm.setFunction("add");

        request.setAttribute("a", "visible");
        request.setAttribute("b", "hidden");
        request.setAttribute("c", "hidden");
        request.setAttribute("d", "visible");

        ArrayList RoleList = CustomerManagerDao.getRoleName(
                request.getParameter("org_type"), ds);
        request.setAttribute("RoleList", RoleList);

        /* get the combo lists */
        DynamicComboCM dcTimeZoneList = new DynamicComboCM();
        dcTimeZoneList.setFirstlevelcatglist(DynamicComboDao.gettimezone(ds));
        request.setAttribute("dcTimeZoneList", dcTimeZoneList);

        DynamicComboCM dcCountryList = new DynamicComboCM();
        dcCountryList.setFirstlevelcatglist(DynamicComboDao.getCountryList(ds));
        request.setAttribute("dcCountryList", dcCountryList);

        DynamicComboCM dcLatDirec = new DynamicComboCM();
        dcLatDirec.setFirstlevelcatglist(DynamicComboDao.getlatdirec());
        request.setAttribute("dcLatDirec", dcLatDirec);

        DynamicComboCM dcLonDirec = new DynamicComboCM();
        dcLonDirec.setFirstlevelcatglist(DynamicComboDao.getlondirec());
        request.setAttribute("dcLonDirec", dcLonDirec);

        DynamicComboCM dcCategoryList = new DynamicComboCM();
        dcCategoryList.setFirstlevelcatglist(DynamicComboDao
                .getCategoryList(ds));
        request.setAttribute("dcCategoryList", dcCategoryList);

        DynamicComboCM dcRegionList = new DynamicComboCM();
        dcRegionList.setFirstlevelcatglist(DynamicComboDao.getRegionList(ds));
        request.setAttribute("dcRegionList", dcRegionList);

        DynamicComboCM dcClassification = new DynamicComboCM();
        dcClassification.setFirstlevelcatglist(DynamicComboDao
                .getClassification(ds));
        request.setAttribute("dcClassification", dcClassification);

        DynamicComboCM dcCNSPOCTitles = new DynamicComboCM();
        dcCNSPOCTitles.setFirstlevelcatglist(DynamicComboDao
                .getCNSPOCTitles(ds));
        request.setAttribute("dcCNSPOCTitles", dcCNSPOCTitles);
        /* added for CNS contact deletion */

        List<String> rankList = new ArrayList<String>();
        request.setAttribute("rankList", rankList);
        addPocForm.setRank("");
        addPocForm.setInvoiceApproval("Approval Required");
        addPocForm.setClassificationId("");
        initialStateCategory = CustomerManagerDao
                .getStateCategoryList(getDataSource(request, "ilexnewDB"));

        if (initialStateCategory.size() > 0) {
            for (int i = 0; i < initialStateCategory.size(); i++) {
                tempcat = ((AddDivisonBean) initialStateCategory.get(i))
                        .getSiteCountryId();
                ((AddDivisonBean) initialStateCategory.get(i))
                        .setTempList(SiteManagedao
                                .getCategoryWiseStateList(tempcat, i,
                                        getDataSource(request, "ilexnewDB")));
            }
        }

        request.setAttribute("initialStateCategory", initialStateCategory);
        addPocForm.setCountry("US");
        addPocForm.setLo_pc_id("0");
        request.setAttribute("refreshtree", "true");

        if (addPocForm.getOrg_type().trim().equals("P")) {
            addPocForm.setCkboxUseDivison("false");
            request.setAttribute("b", "visible");
        }
        addPocForm.setStateSelect(Util.concatString(addPocForm.getState(),
                addPocForm.getCountry()));
        forward = mapping.findForward("success");
        return (forward);
    }

    /**
     * Gets the rank list.
     *
     * @param mapping the mapping
     * @param form the form
     * @param request the request
     * @param response the response
     * @return the rank list
     * @throws Exception the exception
     */
    public void getRankList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        DataSource ds = getDataSource(request, "ilexnewDB");
        List<String> rankList = DynamicComboDao.getRankList(ds,
                request.getParameter("id"));
        JSONArray jsonObj = JSONArray.fromObject(rankList);
        response.getOutputStream().write(jsonObj.toString().getBytes());

    }
}
