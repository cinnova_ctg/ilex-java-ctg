package com.mind.actions.CM;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDaoCM;
import com.mind.dao.CM.CustomerManagerDao;
import com.mind.formbean.CM.AddTopLevelForm;

/**
 * @version 	1.0
 * @author
 */

/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 **/

/**
 * 
 * Class Explanation: AddTopLevelAction is the action class for
 * AddTopLevelAction.jsp. It extends com.mind.common.IlexDispatchActon and its
 * methods are called based on the request parameter"function".
 * 
 */
public class AddTopLevelAction extends com.mind.common.IlexDispatchAction {

	/**
	 * add method is called by dispatch action class on the basis of the
	 * parameter value of 'function'. It is used for addition and updation of
	 * the top level organization.
	 * 
	 * @param mapping
	 *            ActionMapping instance.
	 * @param form
	 *            form bean of the action.
	 * @param request
	 *            Request object.
	 * @param response
	 *            responseObject.
	 * @return ActionForward Actionforward object for forwarding the request.
	 */

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddTopLevelForm addTopLevelForm = (AddTopLevelForm) form;
		DynamicComboCM dccm = new DynamicComboCM();

		DataSource ds = getDataSource(request, "ilexnewDB");

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (addTopLevelForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid,
					"Manage Customer/CNS/PVS", "Add/Update Top Level", ds)) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		String custtype = "";
		request.setAttribute("dccm", dccm);

		if (addTopLevelForm.getElement() == null)
			request.setAttribute("element1", new Integer(0));
		else
			request.setAttribute("element1",
					new Integer(Integer.parseInt(addTopLevelForm.getElement())));
		int statusvalue;
		int flag = 0;

		char type = request.getParameter("org_type").charAt(0);
		switch (type) {
		case 'C':
			custtype = "Customer";
			break;
		case 'N':
			custtype = "CNS";
			break;
		case 'P':
			custtype = "PVS";
			break;
		}
		addTopLevelForm.setOrgcatg(custtype);

		if (addTopLevelForm.getSave() != null) {
			/*
			 * Executed if the user press the save button
			 */
			if (Integer.parseInt(addTopLevelForm.getElement()) == 0) {
				/*
				 * For adding the element execution of the query for addition
				 * 
				 * Get the work center.
				 */
				String workCenter = addTopLevelForm.getWorkCenterId();

				statusvalue = CustomerManagerDao.addFirstLevelCatg(null,
						((String) request.getParameter("org_type")),
						addTopLevelForm.getCustname(), "a", ds, workCenter);
				addTopLevelForm.setAddmessage("" + statusvalue);
				if (statusvalue == 0)
					request.setAttribute(
							"element1",
							new Integer(CustomerManagerDao.getCustomerid(
									(String) request.getParameter("org_type"),
									addTopLevelForm.getCustname(), ds)));
				request.setAttribute("refreshtree", "true");
			} else // For updating the element
			{
				String workCenter = addTopLevelForm.getSource();

				// execution of the query for updation
				statusvalue = CustomerManagerDao.addFirstLevelCatg(
						addTopLevelForm.getElement(),
						((String) request.getParameter("org_type")),
						addTopLevelForm.getCustname(), "u", ds, workCenter);
				addTopLevelForm.setUpdatemessage("" + statusvalue);

				String custn = CustomerManagerDao.getCustomerName(
						addTopLevelForm.getElement(), ds);
				addTopLevelForm.setCustname(custn);

				request.setAttribute("refreshtree", "true");
				request.setAttribute(
						"element1",
						new Integer(Integer.parseInt(addTopLevelForm
								.getElement())));
			}
		} else// Executed if the user has not press the save button
		{

			if (addTopLevelForm.getRefresh() != null) {
				addTopLevelForm.setAddmessage(null);
			}
			String custn = CustomerManagerDao.getCustomerName(
					addTopLevelForm.getElement(), ds);
			addTopLevelForm.setCustname(custn);
		}

		dccm.setFirstlevelcatglist(DynamicComboDaoCM.getFirstcatglist(
				request.getParameter("org_type"), ds));

		dccm.setWorkCenterList(DynamicComboDaoCM.getWrokCenterList(ds));

		flag = 1;

		// Changes made by Seema-15th nov 2006
		if (addTopLevelForm.getSave() == null
				&& addTopLevelForm.getRefresh() == null
				&& request.getParameter("type") == null) {

			if (type == 'C') {
				request.setAttribute("org", "" + type);
				request.setAttribute("search_type", "Customers");
				forward = mapping.findForward("search");
				return forward;
			}

			if (type == 'P') {
				request.setAttribute("org", "" + type);
				request.setAttribute("search_type", "PVS");
				forward = mapping.findForward("search");
				return forward;
			}
		}

		if (flag == 1) {
			forward = mapping.findForward("success");

			return (forward);
		} else {
			forward = mapping.findForward("failure");
			return (forward);
		}

	}

	/**
	 * delete method is called by dispatch action class on the basis of the
	 * parameter value of 'function'. It is used for deletion of the top level
	 * organization.
	 * 
	 * @param mapping
	 *            ActionMapping instance.
	 * @param form
	 *            form bean of the action.
	 * @param request
	 *            Request object.
	 * @param response
	 *            responseObject.
	 * @return ActionForward Actionforward object for forwarding the request.
	 */
	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		AddTopLevelForm addTopLevelForm = (AddTopLevelForm) form;
		DynamicComboCM dccm = new DynamicComboCM();

		DataSource ds = getDataSource(request, "ilexnewDB");

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");
		if (addTopLevelForm.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userid,
					"Manage Customer/CNS/PVS", "Delete Top Level", ds)) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		String custtype = "";

		int statusvalue;
		int flag = 0;

		char type = request.getParameter("org_type").charAt(0);
		switch (type) {
		case 'C':
			custtype = "Customer";
			break;
		case 'N':
			custtype = "CNS";
			break;
		case 'P':
			custtype = "PVS";
			break;
		}

		addTopLevelForm.setOrgcatg(custtype);
		// execution of the query for addition
		if (addTopLevelForm.getRefresh().equals("false")) {
			statusvalue = CustomerManagerDao.deleteFirstLevelCatg(
					addTopLevelForm.getElement(),
					(String) request.getParameter("org_type"), ds);
			addTopLevelForm.setDeletemessage("" + statusvalue);
			if (statusvalue == 0)
				request.setAttribute("element1", new Integer(0));
			else
				request.setAttribute(
						"element1",
						new Integer(Integer.parseInt(addTopLevelForm
								.getElement())));

			request.setAttribute("refreshtree", "true");
		} else {
			addTopLevelForm.setDeletemessage(null);
		}
		String custn = CustomerManagerDao.getCustomerName(
				addTopLevelForm.getElement(), ds);
		addTopLevelForm.setCustname(custn);
		addTopLevelForm.setFunction("add");
		request.setAttribute("dccm", dccm);
		dccm.setFirstlevelcatglist(DynamicComboDaoCM.getFirstcatglist(
				request.getParameter("org_type"), ds));

		dccm.setWorkCenterList(DynamicComboDaoCM.getWrokCenterList(ds));

		flag = 1;
		if (flag == 1) {
			forward = mapping.findForward("success");
			return (forward);
		} else {
			forward = mapping.findForward("failure");
			return (forward);
		}
	}
}
