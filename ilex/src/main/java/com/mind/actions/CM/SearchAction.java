package com.mind.actions.CM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.cm.SearchBean;
import com.mind.dao.CM.CustomerManagerDao;
import com.mind.formbean.CM.SearchForm;

public class SearchAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		SearchForm searchForm = (SearchForm) form;
		SearchBean searchBean = new SearchBean();
		DataSource ds = getDataSource(request, "ilexnewDB");
		if (request.getAttribute("search_type") != null)
			searchForm.setSearchtype((String) request
					.getAttribute("search_type"));

		if (request.getParameter("search_type") != null)
			searchForm.setSearchtype((String) request
					.getParameter("search_type"));

		if (request.getAttribute("org_discipline_id") != null)
			searchForm.setOrg_discipline_id((String) request
					.getAttribute("org_discipline_id"));

		if (request.getParameter("org_discipline_id") != null)
			searchForm.setOrg_discipline_id(request
					.getParameter("org_discipline_id"));

		if (request.getParameter("division_id") != null) {
			searchForm.setOrg(request.getParameter("org_type"));
			searchForm.setDivision_id(request.getParameter("division_id"));
		}

		if (searchForm.getOrg() != null)
			request.setAttribute("org", searchForm.getOrg());

		if (request.getParameter("org_type") != null) {
			searchForm.setOrg((String) request.getParameter("org_type"));
			request.setAttribute("firsttime", "for message");
		}
		if (request.getParameter("org_type") != null
				&& request.getParameter("function") != null)
			if (request.getParameter("org_type").equalsIgnoreCase("P")
					&& request.getParameter("function") != null) {
				searchForm.setOrg_discipline_id(request
						.getParameter("org_discipline_id"));
			}

		/* Get Organization discipline ids */
		if (searchForm.getOrg().equalsIgnoreCase("P")) {
			BeanUtils.copyProperties(searchBean, searchForm);
			CustomerManagerDao.getPartnerId(searchBean, ds);
			BeanUtils.copyProperties(searchForm, searchBean);
		}

		ArrayList searchResult = null, searchPocResult = null;

		if (searchForm.getOrg_discipline_id() == null)
			searchForm.setOrg_discipline_id("0");

		if (searchForm.getDivision() == null)
			searchForm.setOrg_discipline_id("0");

		if (request.getParameter("division_id") == null)
			searchResult = CustomerManagerDao.getSearchResult(
					searchForm.getOrg(), searchForm.getType() + "%",
					searchForm.getDivision() + "%",
					searchForm.getOrg_discipline_id(), ds);
		else
			searchPocResult = CustomerManagerDao.getPOCSearchResult(
					request.getParameter("division_id"), "%%", ds);

		if (searchResult != null) {
			request.setAttribute("searchResult", searchResult);
		}

		if (searchPocResult != null) {
			request.setAttribute("searchPocResult", searchPocResult);
			request.setAttribute("viewPocResult", "true");
		}
		forward = mapping.findForward("success");
		return forward;

	}

}
