package com.mind.actions.PMT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PMT.EditMasterlDao;
import com.mind.formbean.PMT.SwitchJobStatusForm;


public class SwitchJobStatusAction extends com.mind.common.IlexAction {
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		SwitchJobStatusForm switchJobStatusForm = (SwitchJobStatusForm)form;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired

		if( !Authenticationdao.getPageSecurity( loginuserid , "Manage Proposal " , "Switch Close Jobs" , getDataSource( request , "ilexnewDB" ) ) ){
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		Codelist codes = new Codelist();
		String jobId = "0";
		int retVal = -1;
		
		if(switchJobStatusForm.getGo() != null || request.getParameter("switchJob") != null){
			
			if(request.getParameter("switchJob") != null) {
				if(request.getParameter("jobId") != null) {
					jobId = request.getParameter("jobId");
				}
				
				retVal = EditMasterlDao.switchJobStatus(jobId, getDataSource(request, "ilexnewDB"));
				request.setAttribute("jobSwitched", retVal+"");
			}
			
			codes.setJoblist(EditMasterlDao.getDbClosedinvoiceJobs(switchJobStatusForm.getInvoiceNumber().trim(), getDataSource(request, "ilexnewDB")));
			request.setAttribute("codes", codes);
			request.setAttribute("jobListSize", codes.getJoblist().size());
		}
		
		return mapping.findForward("success");
	}
}
