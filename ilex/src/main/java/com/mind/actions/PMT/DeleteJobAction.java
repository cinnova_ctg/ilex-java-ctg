/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* 
*/
package com.mind.actions.PMT;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PMT.DeleteJobdao;
import com.mind.formbean.PMT.DeleteJobForm;

/**
*  Used for searching jobs to be deleted 
* 
* @see 
* 
**/

public class DeleteJobAction extends com.mind.common.IlexAction{
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		DeleteJobForm deletejobform = (DeleteJobForm)form;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String loginusername = ( String ) session.getAttribute( "username" );
		Codelist codes = new Codelist(); 
		ArrayList allJobs = new ArrayList();
		int jobSize=0;
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Manage Proposal " , "Delete Job" , getDataSource( request , "ilexnewDB" ) ) ){
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		if(deletejobform.getJobSearch()!=null){
			allJobs = DeleteJobdao.getAllJobs(deletejobform.getJobKeyword(),getDataSource(request, "ilexnewDB"));
				codes.setJoblist( allJobs );
				jobSize = allJobs.size();
			request.setAttribute( "Size" , new Integer ( jobSize ) );
			request.setAttribute( "codeslist" , codes );
		}
		return mapping.findForward("success");
	}
}
