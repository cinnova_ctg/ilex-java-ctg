package com.mind.actions.PMT;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.Util;
import com.mind.dao.IM.IMdao;
import com.mind.formbean.PMT.ViewPOWOPDFForm;
import com.mind.fw.core.dao.util.DBUtil;

public class ViewPOWOPDFAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// HttpSession session = request.getSession(true);
		ViewPOWOPDFForm viewPOWOPDF = (ViewPOWOPDFForm) form;
		boolean checkNumeric = true;
		if (Util.isNullOrBlank(viewPOWOPDF.getPoId()))
			viewPOWOPDF.setType("P");
		if (viewPOWOPDF.getGo() != null && checkNumeric) {
			try {
				viewPOWOPDF.setPoId(Integer.parseInt(viewPOWOPDF.getPoId())
						+ "");
			} catch (Exception numberFormatException) {
				checkNumeric = false;
				request.setAttribute("isNaN", "false");
				return mapping.findForward("success");
			}
			getData(viewPOWOPDF, getDataSource(request, "ilexnewDB"));
			if (viewPOWOPDF.getPdfBytes() != null
					&& viewPOWOPDF.getPdfBytes().length > 0) {
				writeFile(viewPOWOPDF, request, response);
			} else {
				request.setAttribute("isNaN", "false");
			}
		}

		return mapping.findForward("success");
	}

	private void getData(ViewPOWOPDFForm viewPOWOPDF, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lm_get_powo_pdf_from_web(?, ?) }");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, viewPOWOPDF.getPoId());
			cstmt.setString(3, viewPOWOPDF.getType());
			rs = cstmt.executeQuery();
			if (rs.next()) {
				viewPOWOPDF.setPdfBytes(rs.getBytes("dm_fl_data"));
			}
		} catch (Exception e) {
			logger.error("getDate(ViewPOWOPDFForm, DataSource)", e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
	}// - End of getDate(

	private void writeFile(ViewPOWOPDFForm viewPOWOPDF,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			ServletOutputStream outStream = response.getOutputStream();
			response.setHeader(
					"Content-Disposition",
					"attachment;filename=PO/WO.pdf;size="
							+ viewPOWOPDF.getPdfBytes().length + "");
			outStream.write(viewPOWOPDF.getPdfBytes());
			outStream.flush();
			outStream.close();
		} catch (Exception e) {
			logger.error(e);
		}
	}
}
