package com.mind.actions.PMT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PMT.EditMasterlDao;
import com.mind.formbean.PMT.EditMasterForm;

public class EditMasterAction extends com.mind.common.IlexAction{
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		
		int retval = 0;
		
		EditMasterForm formdetail = (EditMasterForm) form;
		
		/* Page Security Start */
		HttpSession session = request.getSession( true );
		
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired 
		
		String userId = (String)session.getAttribute("userid");
		if(!Authenticationdao.getPageSecurity(userId, "Manage Email", "Edit Email Message", getDataSource(request,"ilexnewDB"))) {
			return( mapping.findForward("UnAuthenticate"));	
		}
		/* Page Security End */
		
		
		if(request.getParameter("type") != null)
			formdetail.setType(request.getParameter("type"));
		
		if(formdetail.getSave() != null) {
			if(formdetail.getType().equals("emailbody") || formdetail.getType().equals("pospinst"))
				retval = EditMasterlDao.updateMessage(formdetail.getMessageContent(), formdetail.getType(), getDataSource(request, "ilexnewDB"));
	//		if(formdetail.getType().equals("poterm"))
	//			retval = EditMasterlDao.updatePOTermCond(formdetail.getType(),formdetail.getMessageContent(), userId, getDataSource(request, "ilexnewDB"));
	
			if(formdetail.getType()!=null)
						retval = EditMasterlDao.updatePOTermCond(formdetail.getType(),formdetail.getMessageContent(), userId, getDataSource(request, "ilexnewDB"));
			
			
		 request.setAttribute("retval", retval+"");
		}
		
		if(formdetail.getType().equals("emailbody") || formdetail.getType().equals("pospinst")) {
			formdetail.setMessageContent(EditMasterlDao.getEmailMessage(formdetail.getType(), getDataSource(request, "ilexnewDB")));
		} else {
	//	if(formdetail.getType().equals("poterm"))
	//		formdetail.setMessageContent(EditMasterlDao.getPOTermCond(getDataSource(request, "ilexnewDB")));
	
		if(formdetail.getType()!=null)
				formdetail.setMessageContent(EditMasterlDao.getPOTermCond(formdetail.getType(),getDataSource(request, "ilexnewDB")));
		}
		
		return( mapping.findForward( "success" ) ); 
	}

}
