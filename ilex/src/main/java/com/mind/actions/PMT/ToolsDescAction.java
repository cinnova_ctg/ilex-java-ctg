package com.mind.actions.PMT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.DynamicComboDocM;
import com.mind.bean.pmt.ToolsDescBean;
import com.mind.dao.PMT.ToolsDescdao;
import com.mind.formbean.PMT.ToolsDescForm;

public class ToolsDescAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ToolsDescAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ToolsDescForm toolsDescForm = (ToolsDescForm) form;
		HttpSession session = request.getSession(true);
		String loginUserid = (String) session.getAttribute("userid");
		int retValue = 0;
		ToolsDescBean toolDescBean = new ToolsDescBean();

		if (request.getParameter("mode") == null) {
			// show in add mode

			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In view default a mode");
			}
		} else if (request.getParameter("mode").equals("view")) {
			if (toolsDescForm.getToolId() != null) {
				ToolsDescdao dao = new ToolsDescdao();
				if (logger.isDebugEnabled()) {
					logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In View Modegggg"
							+ toolsDescForm.getToolId());
				}
				BeanUtils.copyProperties(toolDescBean, toolsDescForm);
				dao.getTool(toolDescBean, toolDescBean.getToolId(),
						getDataSource(request, "ilexnewDB"));
				BeanUtils.copyProperties(toolsDescForm, toolDescBean);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In view mode");
			}
		} else if (request.getParameter("mode").equals("add")) {
			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In add mode");
			}
			BeanUtils.copyProperties(toolDescBean, toolsDescForm);
			retValue = ToolsDescdao.manageTool(toolDescBean, "A", loginUserid,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(toolsDescForm, toolDescBean);
			request.setAttribute("add", retValue);
		} else if (request.getParameter("mode").equals("modify")) {
			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In modify mode");
			}
			BeanUtils.copyProperties(toolsDescForm, toolDescBean);
			retValue = ToolsDescdao.manageTool(toolDescBean, "M", loginUserid,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(toolsDescForm, toolDescBean);

			request.setAttribute("update", retValue);
		} else if (request.getParameter("mode").equals("delete")) {
			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In delete mode");
			}
			BeanUtils.copyProperties(toolsDescForm, toolDescBean);
			retValue = ToolsDescdao.manageTool(toolDescBean, "D", loginUserid,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(toolsDescForm, toolDescBean);
			request.setAttribute("delete", retValue);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Value of Return----"
					+ retValue);
		}
		if (retValue == -1) {
			request.setAttribute("errorOccured", retValue);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Return what---"
					+ toolsDescForm.getDescription());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Return what---"
					+ toolsDescForm.getIsActive());
		}
		DynamicComboDocM dcdocm = new DynamicComboDocM();
		dcdocm.setToolslist(ToolsDescdao.getToolsList(getDataSource(request,
				"ilexnewDB")));
		request.setAttribute("dynamicComboTool", dcdocm);
		return mapping.findForward("success");
	}
}
