package com.mind.actions.PMT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PMT.EditMasterlDao;
import com.mind.formbean.PMT.HideAppendixForm;

public class HideAppendixAction extends com.mind.common.IlexAction{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		HideAppendixForm hideAppendix = (HideAppendixForm)form;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired

		if( !Authenticationdao.getPageSecurity( loginuserid , "Manage Proposal " , "Hide Appendix" , getDataSource( request , "ilexnewDB" ) ) ){
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		Codelist codes = new Codelist();
		codes.setCustomername( EditMasterlDao.getCustomers( getDataSource( request , "ilexnewDB" ) ) );
		int retvalue = -1;
		
		if(hideAppendix.getGo() != null || hideAppendix.getGoHideAppendix() != null) {
			codes.setAppendixlist(EditMasterlDao.getAppendixListForMSA(hideAppendix.getMsaId(), getDataSource(request, "ilexnewDB")));
			request.setAttribute("appSize", new Integer (codes.getAppendixlist().size()));
			
			if(hideAppendix.getGoHideAppendix() != null) {
				String saveAppIds = "";
				
				if(hideAppendix.getCheck() != null) {
					for(int i = 0; i < hideAppendix.getCheck().length; i++) {
						saveAppIds = saveAppIds + hideAppendix.getCheck(i) + ",";
					}
					saveAppIds = saveAppIds.substring(0, saveAppIds.length()-1);
				}
				
				retvalue = EditMasterlDao.setHiddenDataInMasters("appendix", saveAppIds, hideAppendix.getMsaId(), getDataSource(request, "ilexnewDB"));
				request.setAttribute("saveHiddenApp", retvalue+"");
			}
			
			String hiddenAppendices[] = EditMasterlDao.gethiddenDataInMasters("Appendix", hideAppendix.getMsaId(), getDataSource(request, "ilexnewDB"));
			hideAppendix.setCheck(hiddenAppendices);
		}
		request.setAttribute("codes", codes);
		return mapping.findForward("success");
	}
}