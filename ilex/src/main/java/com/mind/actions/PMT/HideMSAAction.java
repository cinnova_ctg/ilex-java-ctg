package com.mind.actions.PMT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.Codelist;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PMT.EditMasterlDao;
import com.mind.formbean.PMT.HideMSAForm;

public class HideMSAAction extends com.mind.common.IlexAction{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
	
		HideMSAForm hideMSA = (HideMSAForm)form;
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Manage Proposal " , "Hide MSA" , getDataSource( request , "ilexnewDB" ) ) ){
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		String saveMsaIds = "";
		
		if(hideMSA.getGoHideMSA() != null) {
		
			if(hideMSA.getCheck() != null) {
				for(int i = 0; i < hideMSA.getCheck().length; i++) {
					saveMsaIds = saveMsaIds + hideMSA.getCheck(i) + ",";
				}
				saveMsaIds = saveMsaIds.substring(0, saveMsaIds.length()-1);
			}
			
			int retvalue = EditMasterlDao.setHiddenDataInMasters("msa", saveMsaIds, "0", getDataSource(request, "ilexnewDB"));
			request.setAttribute("saveMsa", retvalue+"");
		}
		
		Codelist codes = new Codelist();
		codes.setMsalist(EditMasterlDao.getMSAList(getDataSource(request, "ilexnewDB")));
		request.setAttribute("msalist", codes.getMsalist());
		request.setAttribute("msaSize", new Integer (codes.getMsalist().size()));
		
		String hiddenMsa[] = EditMasterlDao.gethiddenDataInMasters("MSA", "0", getDataSource(request, "ilexnewDB"));
		hideMSA.setCheck(hiddenMsa);
		
		return mapping.findForward("success");
	}
}
