package com.mind.actions.PMT;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCM;
import com.mind.bean.DynamicComboPVS;
import com.mind.bean.pmt.EditSiteBean;
import com.mind.common.dao.DynamicComboDaoPVS;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PMT.EditSitedao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.formbean.PMT.EditSiteForm;

public class EditSiteAction extends com.mind.common.IlexAction{
	
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		EditSiteForm editSiteForm = (EditSiteForm) form;
		ArrayList siteList = new ArrayList();
		ArrayList unionsitelist = new ArrayList();
		DynamicComboPVS msalist = new DynamicComboPVS();
		DynamicComboPVS sitenamelist = new DynamicComboPVS();
		DynamicComboDaoPVS dccombo = new DynamicComboDaoPVS();
		
		
		
		EditSiteBean editSiteBean = null;
		int sitesSize = 0;
		String sortqueryclause = "";
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		/*if ( request.getAttribute( "querystring" ) != null )
		{
			sortqueryclause = ( String ) request.getAttribute( "querystring" );
		}*/
		sortqueryclause =  getSortQueryClause( sortqueryclause , session );
		
		
		//Start :Added By Amit For Holding combo state after sorting
		if ( request.getParameter( "msasearchcombo" ) != null )
		{
			editSiteForm.setMsasearchcombo( request.getParameter( "msasearchcombo" ));
			
		}
		if ( request.getParameter( "statesearchcombo" ) != null )
		{
			editSiteForm.setStatesearchcombo(  request.getParameter( "statesearchcombo" ));
		}
		if ( request.getParameter( "flag" ) != null )
		{
			editSiteForm.setFlag( ( String ) request.getParameter( "flag" ));
		}
		if ( request.getParameter( "sitenamesearch" ) != null )
		{
			editSiteForm.setSitenamesearch( ( String ) request.getParameter( "sitenamesearch" ));
		}
		//End
		
		msalist.setMsalist(dccombo.getMSAlist(getDataSource( request , "ilexnewDB" )));
		
		request.setAttribute("msalist",msalist);
		
		
		
		if( editSiteForm.getSubmit() != null )
		{
			int checklength = 0;
			if( editSiteForm.getSiteid()!=  null )
			{
				 checklength = editSiteForm.getSiteid().length;	 
				 
			}
			
			int []index = new int[checklength];
			int k = 0;
			
			if( checklength > 0 )
			{
				int length = editSiteForm.getMastersiteid().length;
				for( int i = 0; i < checklength; i++ )
				{
					for( int j = 0; j < length; j++ )
					{
						if( editSiteForm.getSiteid(i).equals( editSiteForm.getMastersiteid( j ) ) )
						{	
							index[k] = j;
							
							k++;
						}
					}
				}
			}
			
			/* Used for updation 
			 * 
			 */
			if( checklength > 0 )                               
			{
				int updateflag = 0;
				for( int i = 0; i < index.length; i++ )
				{
					editSiteBean = new EditSiteBean();
					editSiteBean.setAddress( editSiteForm.getAddress( index[i] ) );
					editSiteBean.setZipcode( editSiteForm.getZipcode(index[i]) );
					editSiteBean.setSiteName( editSiteForm.getSiteName( index[i] ) );
					editSiteBean.setSiteNumber(editSiteForm.getSiteNumber(index[i]));
					//editSiteBean.setState(editSiteForm.getState(index[i]));
					editSiteBean.setStatecombo(editSiteForm.getStatecombo(index[i]));
					
					editSiteBean.setCity( editSiteForm.getCity( index[i] ) );
					editSiteBean.setCountry( editSiteForm.getCountry(index[i]) );
					editSiteBean.setDesignator( editSiteForm.getDesignator( index[i] ) );
					editSiteBean.setDirections( editSiteForm.getDirections(index[i]) );
					editSiteBean.setLocalityFactor( editSiteForm.getLocalityFactor( index[i] ) );
					editSiteBean.setPrimaryContactPerson( editSiteForm.getPrimaryContactPerson(index[i]) );
					editSiteBean.setPrimaryEmailId( editSiteForm.getPrimaryEmailId( index[i] ) );
					editSiteBean.setPrimaryPhoneNumber( editSiteForm.getPrimaryPhoneNumber(index[i]) );
					editSiteBean.setSecondaryContactPerson( editSiteForm.getSecondaryContactPerson( index[i] ) );
					editSiteBean.setSecondaryEmailId( editSiteForm.getSecondaryEmailId(index[i]) );
					editSiteBean.setSecondaryPhoneNumber( editSiteForm.getSecondaryPhoneNumber( index[i] ) );
					editSiteBean.setSiteid( editSiteForm.getMastersiteid(index[i]) );
					editSiteBean.setWorklocation(editSiteForm.getWorklocation(index[i]));
					editSiteBean.setSpecialCondition(editSiteForm.getSpecialCondition(index[i]));
					//editSiteBean.setUnionSite(editSiteForm.getUnionSite(index[i]));
					editSiteBean.setUnionSitecombo(editSiteForm.getUnionSitecombo(index[i]));
					
					updateflag = EditSitedao.updateSite( editSiteBean ,  getDataSource( request , "ilexnewDB" ) );
					
					request.setAttribute( "updateflag" , ""+updateflag );
				}	
			}
			editSiteForm.setSiteid(null);
		
		}//End Of Submit
		
		
		
		if( request.getParameter( "msasearchcombo" ) != null )
		{
			if( editSiteForm.getMsasearchcombo().equals( "0" ) || editSiteForm.getMsasearchcombo().equals( "A" ) )
			{
				editSiteForm.setMsasearchcombo( "%" );
			}
		}
		else
		{
			editSiteForm.setMsasearchcombo( "" );
		}
		
		if( request.getParameter( "statesearchcombo" ) != null )
		{
			
			if( editSiteForm.getStatesearchcombo().equals( "0" ) || editSiteForm.getStatesearchcombo().equals( "A" ) )
			{
				editSiteForm.setStatesearchcombo( "%" );
			}
		}
		else
		{
			editSiteForm.setStatesearchcombo( "" );
		}
		
		if( request.getParameter( "sitenamesearch" ) != null )
		{
			if( editSiteForm.getSitenamesearch().equals( "" ) || editSiteForm.getSitenamesearch().equals( "A" ) )
			{
				editSiteForm.setSitenamesearch( "%" );
			}
		}
		else
		{
			editSiteForm.setSitenamesearch( "" );
		}
		
		if( request.getParameter( "sitenumbersearch" ) != null )
		{
			if( editSiteForm.getSitenumbersearch().equals( "" ) || editSiteForm.getSitenumbersearch().equals( "A" ) )
			{
				editSiteForm.setSitenumbersearch( "%" );
			}
		}
		else
		{
			editSiteForm.setSitenumbersearch( "" );
		}
		
		
		if( request.getParameter( "sitecitysearch" ) != null )
		{
			if( editSiteForm.getSitecitysearch().equals( "" ) || editSiteForm.getSitecitysearch().equals( "A" ) )
			{
				editSiteForm.setSitecitysearch( "%" );
			}
		}
		else
		{
			editSiteForm.setSitecitysearch( "" );
		}
		
		siteList = EditSitedao.getAllSiteList(editSiteForm.getMsasearchcombo(), editSiteForm.getStatesearchcombo(), editSiteForm.getSitenamesearch(), editSiteForm.getSitenumbersearch(), editSiteForm.getSitecitysearch(), sortqueryclause, getDataSource( request,"ilexnewDB" ));
		
		if(editSiteForm.getSitenamesearch()!=null)
		{
			if(editSiteForm.getSitenamesearch().equals("%"))
			{
				editSiteForm.setSitenamesearch( "" );
			}
		}
		
		if(editSiteForm.getSitenumbersearch()!=null)
		{
			if(editSiteForm.getSitenumbersearch().equals("%"))
			{
				editSiteForm.setSitenumbersearch( "" );
			}
		}
		
		if(editSiteForm.getSitecitysearch()!=null)
		{
			if(editSiteForm.getSitecitysearch().equals("%"))
			{
				editSiteForm.setSitecitysearch( "" );
			}
		}
		
		
		if(siteList!=null){
			
			if(siteList.size()>0)
			{
				sitesSize = siteList.size();
		     }
		}
		
		
		// Start :For State List
		DynamicComboCM dcStateCM= new DynamicComboCM();
		Pvsdao pvsDao= new Pvsdao();
		if(pvsDao!=null)
		{
			dcStateCM.setFirstlevelcatglist(Pvsdao.getStateList(getDataSource( request , "ilexnewDB" )));
			request.setAttribute("dcStateCM",dcStateCM);
		}
		//End :For State List
		
		//Set MSA Name
		if(editSiteForm.getMsasearchcombo().equals("%") || editSiteForm.getMsasearchcombo().equals(""))
			editSiteForm.setMsa_name("All");
		else
			editSiteForm.setMsa_name(Appendixdao.getMsaname(getDataSource( request , "ilexnewDB" ), editSiteForm.getMsasearchcombo()));
		
		//For Union Site
		DynamicComboCM dcStateCMUnionsite= new DynamicComboCM();
		DynamicComboDaoPVS dcpvs = new DynamicComboDaoPVS();
		dcStateCMUnionsite.setUnionSite(dcpvs.getUnionSite());
		request.setAttribute("dcStateCMUnionsite",dcStateCMUnionsite);
	
		
		request.setAttribute("siteList",siteList);
		request.setAttribute("sitesSize",new Integer (sitesSize));
		
		
		return mapping.findForward("success");
	}
	
	public String getSortQueryClause( String queryclause , HttpSession session )
	{
		if( session.getAttribute( "sitemaster_sort" ) != null )
			queryclause = ( String ) session.getAttribute( "sitemaster_sort" );
		return queryclause;
	}
}
