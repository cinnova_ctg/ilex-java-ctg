package com.mind.actions.CNSWEB;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.CNSWEB.NetMedXDispatchdao;
import com.mind.formbean.CNSWEB.CnsLoginForm;
import com.mind.formbean.CNSWEB.CookieForm;



public class CnsLoginAction extends com.mind.common.IlexAction{

	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();

		CnsLoginForm loginForm = (CnsLoginForm) form;
		String username="";
		String password="";
		String repassword="";
		String emailid="";
		boolean cookieexist=false;
		boolean userexist=false;
		String failmessage="";
		String pwdmismatchmessage="";
		HttpSession session = request.getSession( true );
		
		if(loginForm.getSignup()!=null)
		{
			username=loginForm.getUsername();
			loginForm.setUsername(username);
			password=loginForm.getPassword();
			repassword=loginForm.getConfirmpassword();
			emailid=loginForm.getEmail();
			loginForm.setEmail(emailid);
			if(!(password.equals(repassword)))
			{
				loginForm.setPassword("");
				loginForm.setConfirmpassword("");
				loginForm.setPasswordsmismatchmessage("9991");
				
				return( mapping.findForward( "success" ) );
			}
			else
			{
				CookieForm cookieForm = new CookieForm();
				cookieexist=cookieForm.getCookie(request,username,password);
				if(cookieexist)
				{
					return( mapping.findForward( "failure" ) );
				}
				else
				{
					userexist=NetMedXDispatchdao.authenticateuser(username,password,getDataSource( request , "ilexnewDB" ));
					if(userexist)
					{
						cookieForm.setCookie(response,username,password);
						return( mapping.findForward( "failure" ) );
					}
					else
					{
						loginForm.setFailmessage("9992");
						
						return( mapping.findForward( "success" ) );
					}
				}
				
			}
			
		}
		
		return( mapping.findForward( "success" ) );		
	}

}
