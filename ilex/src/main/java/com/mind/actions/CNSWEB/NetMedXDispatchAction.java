package com.mind.actions.CNSWEB;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCNSWEB;
import com.mind.bean.cnsweb.NetMedXDispatchBean;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.CNSWEB.NetMedXDispatchdao;
import com.mind.formbean.CNSWEB.NetMedXDispatchForm;

public class NetMedXDispatchAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		NetMedXDispatchForm formbean = (NetMedXDispatchForm) form;
		NetMedXDispatchBean netMedXDispatchBean = new NetMedXDispatchBean();
		DataSource ds = getDataSource(request, "ilexnewDB");
		HttpSession session = request.getSession(true);

		if ((String) request.getAttribute("user_name") != null) {
			// formbean.setUserName((String)request.getAttribute("user_name"));
		}
		formbean.setUserName(NetMedXDispatchdao.getUsername(
				(String) session.getAttribute("userid"), ds));

		if (formbean.getSave() != null) {
			BeanUtils.copyProperties(netMedXDispatchBean, formbean);
			netMedXDispatchBean.setFileSize(formbean.getFilename()
					.getFileSize());
			netMedXDispatchBean.setInputstream(formbean.getFilename()
					.getInputStream());
			netMedXDispatchBean.setFileSize2(formbean.getFilename2()
					.getFileSize());
			netMedXDispatchBean.setFileSize3(formbean.getFilename3()
					.getFileSize());
			netMedXDispatchBean.setInputstream2(formbean.getFilename2()
					.getInputStream());
			netMedXDispatchBean.setInputstream3(formbean.getFilename3()
					.getInputStream());
			NetMedXDispatchdao.setNetMedXDispatchForm(netMedXDispatchBean, ds);
		} else {
			BeanUtils.copyProperties(netMedXDispatchBean, formbean);
			NetMedXDispatchdao.getuserdetail(formbean.getUserName(),
					netMedXDispatchBean, ds);
		}
		BeanUtils.copyProperties(formbean, netMedXDispatchBean);

		int userid = Integer.parseInt((String) session.getAttribute("userid"));

		DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();

		dynamiccomboCNSWEB.setStatename(DynamicComboDao.getStateName(ds));
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		dynamiccomboCNSWEB.setHourname(DynamicComboDao.getHourName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		dynamiccomboCNSWEB.setMinutename(DynamicComboDao.getMinuteName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		dynamiccomboCNSWEB.setOptionname(DynamicComboDao.getOptionName());
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		dynamiccomboCNSWEB.setCriticalityname(DynamicComboDao
				.getCriticalityName("%", ds));
		request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);

		return (mapping.findForward("success"));
	}
}
