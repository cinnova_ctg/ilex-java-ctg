package com.mind.type;

public enum MSPTaskCategory {

	PRE_QUALIFICATION("Pre-Qualification of Customer Sites (Provisioning).",
			new String[] { "Release Date for Site", "Existing Broadband Type" }),
	ORDER_CIRCUIT("Order Circuit (Provisioning).", new String[] {
			"Ordered Date for Site", "ISP", "Circuit Type" }),
	NOTIFY_CUSTOMER_OF_FOC_INSTALL(
			"Notify Customer of FOC Install Date (Provisioning). To be done each week for following weeks installs",
			new String[] { "FOC Date", "ISP", "Circuit Type" }),
	CONFIRM_CIRCUIT_IS_READY(
			"Confirm Circuit is Ready for Install and Send Configuration Sheet (Provisioning).",
			new String[] { "Order Number", "ISP", "Pro or Self Install" }),
	CONFIGURE_APC(
			"Configure APC, CradlePoint and EVDO for Board Build (MSP).",
			new String[] { "Static IP # (if applicable)", "ISP", "Circuit Type" }),
	CONFIRM_BOARD_BUILD_SPECS("Confirm Board Build Specs (Operations)",
			new String[] { "Modem Type", "ISP", "Circuit Type" }),
	BUILD_BOARD_FOR_STORE_AND_SHIP(
			"Build Board for Store and Ship (Logistics). Enter shipping number in ticket",
			new String[] { "Modem Type", "ISP", "Required Delivery Date" }),
	CONFIRM_RECEIPT_OF_SHIPMENT(
			"Confirm Receipt of Shipment at Store (Operations).", new String[] {
					"Ship Tracking Number", "ISP", "Required Delivery Date" }),
	SCHEDULE_INSTALLATION(
			"Schedule Installation with CNS Technician (Operations).",
			new String[] { "Ship Tracking Number", "FOC Date",
					"Required Delivery Date" }),
	NOTIFY_CUSTOMER_OF_INSTALLS(
			"Notify Customer of Installs Planned for Following Week (Operations).",
			new String[] { "Ship Tracking Number", "Tech Name",
					"Required Delivery Date" }),

	SITE_REQUIRES_REPROVISION(
			"Site Requires Reprovision of Circuit or EVDO (Operations).",
			new String[] { "Order Number", "ISP", "Circuit Type" }),
	SITE_INSTALLED("Site Installed and Completed (Operations).", new String[] {
			"Ship Tracking Number", "Tech Name", "Required Delivery Date" }),
	SITE_MONITORED("Site Monitored in WUG (TAC).", new String[] {
			"Circuit Type", "ISP", "IP Type" }),

	SITE_BILLING_INITIATED("Site Billing Initiated (MSP).", new String[] {
			"Billing Master Account Number", "ISP", "Provider Circuit MRC" });

	private String categoryName;
	private String[] colNames;

	private MSPTaskCategory(String argCategoryName, String[] argColNames) {
		this.categoryName = argCategoryName;
		this.colNames = argColNames;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String[] getColNames() {
		return colNames;
	}

	public void setColNames(String[] colNames) {
		this.colNames = colNames;
	}

	public static boolean displayCategory(String categoryName) {
		for (MSPTaskCategory mtc : MSPTaskCategory.values()) {
			if (mtc.getCategoryName().equals(categoryName)) {
				return true;
			}
		}
		return false;
	}

	public static MSPTaskCategory[] displayAllCategoryNames() {
		return MSPTaskCategory.values();
	}

	public static String[] showColumnNamebyCategory(String categoryName) {
		String[] colNames = null;
		for (MSPTaskCategory mtc : MSPTaskCategory.values()) {
			if (mtc.getCategoryName().equals(categoryName)) {
				colNames = mtc.getColNames();
			}
		}
		return colNames;
	}

	public static void main(String[] args) {
		for (MSPTaskCategory mtc : MSPTaskCategory.values()) {
			System.out.println(mtc.getCategoryName());
		}
	}
}
