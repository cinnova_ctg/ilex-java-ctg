/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: <This file actually helps in generating the report.>
 *
 */

package com.mind.report;

import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.util.JRProperties;

import org.apache.log4j.Logger;

import com.mind.bean.reports.ReportFilterCriteria;
import com.mind.docm.dao.MSA;

/**
 * Class ViewDocument - This class helps in generating PDF reports files.
 * methods - getBytes, getBytes
 **/

public class ViewDocument {
	public byte[] bytes = null;
	public String pdfFileName = null;
	public JasperPrint jasperPrint = null;

	/**
	 * getBytes This method helps in setting the report filter criteria.
	 * 
	 * @param context
	 *            - Servlet context.
	 * @param HttpServletRequest
	 *            - Its the request object.
	 * @param Connection
	 *            - Its the dataBase connection object.
	 * @return Returns byte array.
	 **/

	private static final Logger logger = Logger.getLogger(ViewDocument.class);

	public byte[] getBytes(ServletContext context, HttpServletRequest request,
			Connection conn) throws Exception {
		logger.trace("Start :(Class : com.mind.report.ViewDocument | Method : getBytes)");
		// sets the report filter criteria object.
		MSA msa = new MSA();
		ReportFilterCriteria reportFilterCriteria = new ReportFilterCriteria();
		msa.getAppendix().setEntityId(
				request.getAttribute("appendixid").toString());
		msa.setEntityName((String) request.getAttribute("msaname"));
		reportFilterCriteria.setReportType((String) (request
				.getAttribute("report_type")));
		reportFilterCriteria.setStartDate((String) request
				.getAttribute("startdate"));
		reportFilterCriteria.setEndDate((String) request
				.getAttribute("enddate"));
		reportFilterCriteria.setCriticality(request.getAttribute("criticality")
				.toString());
		reportFilterCriteria.setStatus(request.getAttribute("status")
				.toString());
		reportFilterCriteria.setRequestor(request.getAttribute("requestor")
				.toString());
		reportFilterCriteria.setMsp(request.getAttribute("msp").toString());
		reportFilterCriteria.setArrivalOptions(request.getAttribute(
				"criticalitydesc").toString());
		reportFilterCriteria.setHelpDesk(request.getAttribute("helpdesk")
				.toString());
		bytes = this.getBytes(context, conn, msa, reportFilterCriteria);
		logger.trace("End :(Class : com.mind.report.ViewDocument | Method : getBytes)");
		return bytes;
	}

	/**
	 * getBytes This method overloaded method of the above and it actually
	 * generate the report.
	 * 
	 * @param context
	 *            - Servlet context.
	 * @param Connection
	 *            - Its the dataBase connection object.
	 * @param ReportFilterCriteria
	 *            - report filter criteria object for the report.
	 * @return Returns byte array.
	 **/
	public byte[] getBytes(ServletContext context, Connection conn, MSA msa,
			ReportFilterCriteria reportFilterCriteria) throws Exception {
		logger.trace("Start :(Class : com.mind.report.ViewDocument | Method : getBytes)");
		String propsFile = "com.mind.properties.Report";
		ResourceBundle res = ResourceBundle.getBundle(propsFile);
		String reportName = res.getString("app.rpt_ilex_main");
		String reportPath1 = null;
		if (((String) (reportFilterCriteria.getReportType())).equals("redline")) {
			reportPath1 = context.getRealPath(res
					.getString("app.subrpt_ilex_table"));
			pdfFileName = (String) (reportFilterCriteria.getReportType());
		} else {
			reportPath1 = context.getRealPath(res
					.getString("app.subrpt_detail_ilex_table"));
			pdfFileName = (String) (reportFilterCriteria.getReportType());
		}
		String reportPath2 = context.getRealPath(res
				.getString("app.subrpt_ilex_table2"));
		String reportPath3 = context.getRealPath(res
				.getString("app.subrpt_req_req"));
		String reportPath4 = context.getRealPath(res
				.getString("app.subrpt_req_by_crit"));
		String reportPath5 = context.getRealPath(res
				.getString("app.subrpt_reqes_cric_table"));
		String reportPath6 = context.getRealPath(res
				.getString("app.subrpt_req_state"));
		String reportPath7 = context.getRealPath(res
				.getString("app.subrpt_type_cric_table"));
		String imageLocation = context.getRealPath(res
				.getString("app.img_location"));

		File reportFile = new File(context.getRealPath(reportName));
		JRProperties.setProperty(JRProperties.COMPILER_TEMP_DIR,
				context.getRealPath("/reports/"));
		if (!reportFile.exists())
			throw new JRRuntimeException(
					"File WebappReport.jasper not found. The report design must be compiled first.");

		/*
		 * JasperReport jasperReport = (JasperReport)
		 * JRLoader.loadObject(reportFile.getPath());
		 */

		Map inputParameters = new HashMap();
		// inputParameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		logger.trace("Fills up the Map for the Report generation.");
		inputParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, res);
		inputParameters.put("IMAGE_LOCATION", imageLocation);
		inputParameters.put("APPENDIX_ID", new Integer(msa.getAppendix()
				.getEntityId()));
		inputParameters.put("START_DATE", reportFilterCriteria.getStartDate());
		// System.out.println("startDAte"+reportFilterCriteria.getStartDate());
		// System.out.println("endDAte"+reportFilterCriteria.getEndDate());
		inputParameters.put("END_DATE", reportFilterCriteria.getEndDate());
		inputParameters.put("REPORT_NAME", msa.getEntityName());
		inputParameters.put("SUBREPORT_FILE_NAME1", reportPath1);
		inputParameters.put("SUBREPORT_FILE_NAME2", reportPath2);
		inputParameters.put("SUBREPORT_FILE_NAME3", reportPath3);
		inputParameters.put("SUBREPORT_FILE_NAME4", reportPath4);
		inputParameters.put("SUBREPORT_FILE_NAME5", reportPath5);
		inputParameters.put("SUBREPORT_FILE_NAME6", reportPath6);
		inputParameters.put("SUBREPORT_FILE_NAME7", reportPath7);
		inputParameters.put("CRIT_ID", reportFilterCriteria.getCriticality());
		inputParameters.put("STATUS", reportFilterCriteria.getStatus());
		inputParameters
				.put("REQUESTOR_ID", reportFilterCriteria.getRequestor());
		inputParameters.put("CRIT_DESC",
				reportFilterCriteria.getArrivalOptions());
		inputParameters.put("MSP", reportFilterCriteria.getMsp());
		inputParameters.put("HELP_DESK", reportFilterCriteria.getHelpDesk());
		byte[] bytes = null;

		/*
		 * jasperPrint = JasperFillManager.fillReport(jasperReport,
		 * inputParameters, conn); bytes =
		 * JasperExportManager.exportReportToPdf(jasperPrint); if
		 * (jasperPrint.getPages().size() == 0) {
		 * //System.out.println("method called"); bytes = null; return bytes;
		 * 
		 * }
		 */
		JasperResponse jasperResponse = ReportServlet.compileJasper(
				inputParameters, conn, reportFile.getPath());
		if (jasperResponse == null
				|| jasperResponse.getDocumentPageCount() == 0) {
			bytes = null;
			return bytes;
		}
		logger.trace("End :(Class : com.mind.report.ViewDocument | Method : getBytes)");
		return jasperResponse.getDocumentBytes();
	}
}
