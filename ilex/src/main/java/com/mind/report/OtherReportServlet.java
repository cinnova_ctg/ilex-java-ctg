package com.mind.report;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRProperties;

import org.apache.log4j.Logger;

public class OtherReportServlet extends HttpServlet {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OtherReportServlet.class);
	
	public void doPost(HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request,response);
	}
	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException 
	{
		String propsFile = "com.mind.properties.Report";
		ServletContext context = this.getServletConfig().getServletContext();
		Connection conn = null;
		try 
		{
			ResourceBundle res = ResourceBundle.getBundle(propsFile);
			String reportName = null;
			
			String pdfFileName = null;
			if(((String)(request.getAttribute("report_type"))).equals("accountsummary")) {
				reportName = context.getRealPath(res.getString("app.account"));
			 pdfFileName = (String)(request.getAttribute("report_type"));
			}
			if(((String)(request.getAttribute("report_type"))).equals("ownersummary")) {
				reportName = context.getRealPath(res.getString("app.owner"));
			 pdfFileName = (String)(request.getAttribute("report_type"));
			}
			if(((String)(request.getAttribute("report_type"))).equals("laborhours")) {
				reportName = context.getRealPath(res.getString("app.labor"));
				 pdfFileName = (String)(request.getAttribute("report_type"));
			}
				
		
			String imageLocation = context.getRealPath(res.getString("app.img_location"));
			
			//System.out.println("ssssssss"+   reportName);

			File reportFile =new File(reportName );
			JRProperties.setProperty(
					JRProperties.COMPILER_TEMP_DIR,
					context.getRealPath("/reports/"));
			if (!reportFile.exists())
				throw new JRRuntimeException("File WebappReport.jasper not found. The report design must be compiled first.");

			JasperReport jasperReport =
				(JasperReport) JRLoader.loadObject(reportFile.getPath());
				
			
			Map inputParameters = new HashMap();
			//inputParameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			inputParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE,res);
			inputParameters.put("IMAGE_LOCATION",imageLocation);
			
		
			if(((String)(request.getAttribute("report_type"))).equals("ownersummary")) 
			{
				if( request.getAttribute("requestor")!= null )
				{
					if( request.getAttribute("requestor").toString().equals( "X" ) )
					{
						inputParameters.put( "Owner" , "%" );
					}
					else
					{
						inputParameters.put("Owner",request.getAttribute("requestor").toString());
					}
					
				}
			}
		
			if( ( ( String )(request.getAttribute( "report_type" ) ) ).equals( "ownersummary" ) || ( ( ( String )(request.getAttribute("report_type"))).equals("accountsummary") ) ) 
			{
				if( request.getAttribute( "msaid" )!= null )
				{
					if( request.getAttribute( "msaid").toString().equals( "X") )
					{
						inputParameters.put("MSA_ID","%");
					}
					else
					{
						inputParameters.put("MSA_ID",request.getAttribute("msaid").toString());
					}
				}
				inputParameters.put("START_DATE",request.getAttribute("startdate"));
				inputParameters.put("END_DATE",request.getAttribute("enddate"));
			}
			
			Map imagesMap = new HashMap();
			request.getSession().setAttribute("IMAGES_MAP", imagesMap);
			
			DataSource dataSource = (DataSource) request.getSession(false).getAttribute("datasource");

			byte[] bytes = null;
			
			try {
				conn = dataSource.getConnection();	
			} catch (Exception e) {
				logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("doGet(HttpServletRequest, HttpServletResponse) - Exception Occur At makeConnection " + e);
				}
			}

			JasperPrint jasperPrint =
				JasperFillManager.fillReport(jasperReport, inputParameters, conn);
			bytes = JasperExportManager.exportReportToPdf(jasperPrint);
			if (jasperPrint.getPages().size() == 0) {
				RequestDispatcher rd =
					context.getRequestDispatcher(
						"/Report_Error.jsp");
				rd.forward(request, response);
				return;

			}

			if (bytes != null && bytes.length > 0) {
				ServletOutputStream outStream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment;filename=" +pdfFileName + ".pdf" + ";size=" + bytes.length + "");
				outStream.write(bytes);
				outStream.close();
			} else {
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<head>");
				out.println(
					"<title>JasperReports - Web Application Sample</title>");
				out.println(
					"<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
				out.println("</head>");

				out.println("<body bgcolor=\"white\">");

				out.println("<span class=\"bold\">Empty response.</span>");

				out.println("</body>");
				out.println("</html>");
			}

		} catch (Exception e) {
			logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println(
				"<title>JasperReports </title>");
			out.println(
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
			out.println("</head>");

			out.println("<body bgcolor=\"white\">");

			out.println(
				"<span class=\"bnew\">JasperReports encountered this error :</span>");
			out.println("<pre>");

			e.printStackTrace(out);

			out.println("</pre>");

			out.println("</body>");
			out.println("</html>");

			return;
		}
		
		finally
		{
						
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("doGet(HttpServletRequest, HttpServletResponse) - exception ignored", e);
}
		}
	}

}