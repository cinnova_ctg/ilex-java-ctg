package com.mind.report;

public class JasperResponse {
	private byte[] documentBytes=null;
	private int documentPageCount=0;
	public byte[] getDocumentBytes() {
		return documentBytes;
	}
	public void setDocumentBytes(byte[] documentBytes) {
		this.documentBytes = documentBytes;
	}
	public int getDocumentPageCount() {
		return documentPageCount;
	}
	public void setDocumentPageCount(int documentPageCount) {
		this.documentPageCount = documentPageCount;
	}
	
	
}
	
