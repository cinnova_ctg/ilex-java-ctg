/***********************************************************************************************
 *Copyright (C) 2000 MIND 
 *All rights reserved.
 *The information contained here in is confidential and 
 *proprietary to MIND and forms the part of the MIND 
 
 *CREATED BY				:	Shailja Mishra
 *Date					:	27 August 2001.
 
 *Project				:	Ilex
 *Operating environment	:	Websphere , sql server 2000
 
 *DESCRIPTION			:	*This java file will generate the report from the saved templates.
 *Note					:	It NEEDS ENTRY OF EVERY REPORT IN THE SWITCH CASE WHICH CONVERTS REPORT_ID(AS PER THE SRS) *INTO THE BEAN NAME WHICH WILL CORRESSPONDINGLY FETCH THE REPORT PARAMATERS FOR REPORT API.
 
 *********************************************************************************************/
//File included in the com.mind.report Package
package com.mind.report;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class CSVReport extends HttpServlet {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CSVReport.class);

	public void init(ServletConfig conf) throws ServletException {
		super.init(conf);

	}

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		Connection connect = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		Statement stmt = null;
		String Type = "Appendix";
		String Appendix_name = "";
		String MSAType = "MSA";
		String MSA_name = "";
		String sql = "";
		String Report = "";
		StringBuffer FileName = new StringBuffer("");

		try {

			byte[] buffer1 = null;
			// getting the datasource andgenerating the =CSV report from it.

			StringBuffer hout = new StringBuffer();
			java.util.Date today = new java.util.Date();
			DataSource ds = (DataSource) request.getSession(false)
					.getAttribute("datasource");

			String App_id = request.getAttribute("appendixid").toString();
			String MSA_id = request.getAttribute("MSAid").toString();
			String version_id = (request.getAttribute("version").toString());
			String startDate = (request.getAttribute("startdate").toString());
			String endDate = (request.getAttribute("enddate").toString());
			String dateOnSite = (request.getAttribute("dateOnSite").toString());
			String dateOffSite = (request.getAttribute("dateOffSite")
					.toString());
			String dateComplete = (request.getAttribute("dateComplete")
					.toString());
			String dateClosed = (request.getAttribute("dateClosed").toString());
			String msp = (request.getAttribute("msp").toString());

			int appendix_id = Integer.parseInt(request.getAttribute(
					"appendixid").toString());
			// int MSAid =
			// Integer.parseInt(request.getAttribute("MSAid").toString());
			connect = ds.getConnection();
			stmt = connect.createStatement();
			sql = "select dbo.func_cc_name(" + App_id + ", '" + Type + "')";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				Appendix_name = rs.getString(1);
			}

			if ((version_id).charAt(0) == 'I') {
				Report = "Internal Report-";
			} else {
				sql = "select dbo.func_cc_name(" + MSA_id + ", '" + MSAType
						+ "')";
				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					MSA_name = rs.getString(1);
				}
				Report = MSA_name + "-";
			}
			rs.close();

			/*
			 * System.out.println("appendix_id " + appendix_id);
			 * System.out.println("version_id " + version_id);
			 * System.out.println("startDate " + startDate);
			 * System.out.println("endDate " + endDate);
			 * System.out.println("dateOnSite " + dateOnSite);
			 * System.out.println("dateOffSite " + dateOffSite);
			 * System.out.println("dateComplete " + dateComplete);
			 * System.out.println("dateClosed " + dateClosed);
			 * System.out.println("msp " + msp);
			 */

			cstmt = connect
					.prepareCall("{?=call dbo.rpt_job_activity_csv_01(?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, appendix_id);
			cstmt.setString(3, version_id);
			cstmt.setString(4, startDate);
			cstmt.setString(5, endDate);
			cstmt.setString(6, dateOnSite);
			cstmt.setString(7, dateOffSite);
			cstmt.setString(8, dateComplete);
			cstmt.setString(9, dateClosed);
			cstmt.setString(10, msp);

			rs = cstmt.executeQuery();

			int cnt = 0;

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			String s = formatter.format(today);
			FileName.append(Report + Appendix_name + " " + s + ".xls");
			for (int i = 0; i < FileName.length() - 1; i++) {
				char ch = FileName.charAt(i);
				if (ch == '\\' || ch == '/' || ch == '*' || ch == '?'
						|| ch == '"' || ch == '<' || ch == '>' || ch == '|') {
					FileName = FileName.replace(i, i + 1, " ");
				}
			}
			String fileName = FileName.toString();
			fileName = URLEncoder.encode(fileName, "UTF-8");
			fileName = fileName.replaceAll("\\+", " ");

			if (rs.next()) {
				cnt = (int) Integer.parseInt(rs.getString(1));

				do {

					for (int column = 2; column <= cnt + 1; column++) {

						hout.append("\"" + rs.getString(column) + "\"\t");

					}

					hout.append("\r\n");
				} while (rs.next());
			}

			buffer1 = hout.toString().getBytes();

			if (buffer1 != null) {
				response.setContentType("application/excel");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + fileName + ";size="
								+ buffer1.length + "");
				ServletOutputStream outStream = response.getOutputStream();
				outStream.write(buffer1);
				outStream.flush();
				outStream.close();

			}
		} catch (Exception e) {
			logger.error("service(HttpServletRequest, HttpServletResponse)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("service(HttpServletRequest, HttpServletResponse) - ReportCSV --"
						+ e);
			}
		} finally {
			try {
				if (rs != null)
					rs.close();

				if (cstmt != null)
					cstmt.close();
				if (stmt != null)
					stmt.close();

				if (connect != null)
					connect.close();
			} catch (Exception ex) {
				logger.error(
						"service(HttpServletRequest, HttpServletResponse)", ex);

				if (logger.isDebugEnabled()) {
					logger.debug("service(HttpServletRequest, HttpServletResponse) - exception in closing --"
							+ ex);
				}
			}

		}
	}

}
