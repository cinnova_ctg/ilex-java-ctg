package com.mind.report;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import com.mind.common.IlexConstants;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.MindFWRuntimeException;
import com.mind.fw.util.PropertyFileUtil;
import com.mind.fw.util.crypt.DecryptString;

public class ReportServlet extends HttpServlet {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ReportServlet.class);
	private static ConcurrentHashMap<String, DataSource> dsMap = new ConcurrentHashMap();

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String propsFile = "com.mind.properties.Report";
		ServletContext context = this.getServletConfig().getServletContext();
		Connection conn = null;
		try {
			ResourceBundle res = ResourceBundle.getBundle(propsFile);

			String reportName = res.getString("app.rpt_ilex_main");
			String reportPath1 = null;
			String pdfFileName = null;
			if (((String) (request.getAttribute("report_type")))
					.equals("redline")) {
				reportPath1 = context.getRealPath(res
						.getString("app.subrpt_ilex_table"));
				pdfFileName = (String) (request.getAttribute("report_type"));
			} else {
				reportPath1 = context.getRealPath(res
						.getString("app.subrpt_detail_ilex_table"));
				pdfFileName = (String) (request.getAttribute("report_type"));
			}

			String reportPath2 = context.getRealPath(res
					.getString("app.subrpt_ilex_table2"));
			String reportPath3 = context.getRealPath(res
					.getString("app.subrpt_req_req"));
			String reportPath4 = context.getRealPath(res
					.getString("app.subrpt_req_by_crit"));
			String reportPath5 = context.getRealPath(res
					.getString("app.subrpt_reqes_cric_table"));
			String reportPath6 = context.getRealPath(res
					.getString("app.subrpt_req_state"));
			String reportPath7 = context.getRealPath(res
					.getString("app.subrpt_type_cric_table"));

			String imageLocation = context.getRealPath(res
					.getString("app.img_location"));

			File reportFile = new File(context.getRealPath(reportName));
			// JRProperties.setProperty(
			// JRProperties.COMPILER_TEMP_DIR,
			// context.getRealPath("/reports/"));
			if (!reportFile.exists())
				throw new JRRuntimeException(
						"File WebappReport.jasper not found. The report design must be compiled first.");

			// JasperReport jasperReport =
			// (JasperReport) JRLoader.loadObject(reportFile.getPath());
			// JasperReport jasperReport = JasperCompileManager.compileReport
			// (reportFile.getPath());

			Map inputParameters = new HashMap();
			// inputParameters.put(JRParameter.IS_IGNORE_PAGINATION,
			// Boolean.TRUE);
			inputParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, res);
			inputParameters.put("IMAGE_LOCATION", imageLocation);
			inputParameters.put("APPENDIX_ID", new Integer(request
					.getAttribute("appendixid").toString()));
			inputParameters
					.put("START_DATE", request.getAttribute("startdate"));
			inputParameters.put("END_DATE", request.getAttribute("enddate"));

			inputParameters.put("REPORT_NAME", request.getAttribute("msaname"));
			inputParameters.put("SUBREPORT_FILE_NAME1", reportPath1);
			inputParameters.put("SUBREPORT_FILE_NAME2", reportPath2);
			inputParameters.put("SUBREPORT_FILE_NAME3", reportPath3);
			inputParameters.put("SUBREPORT_FILE_NAME4", reportPath4);
			inputParameters.put("SUBREPORT_FILE_NAME5", reportPath5);
			inputParameters.put("SUBREPORT_FILE_NAME6", reportPath6);
			inputParameters.put("SUBREPORT_FILE_NAME7", reportPath7);

			/* for criticality start */
			inputParameters.put("CRIT_ID", request.getAttribute("criticality")
					.toString());
			inputParameters.put("STATUS", request.getAttribute("status")
					.toString());
			inputParameters.put("REQUESTOR_ID",
					request.getAttribute("requestor").toString());
			inputParameters.put("CRIT_DESC",
					request.getAttribute("criticalitydesc").toString());
			inputParameters.put("MSP", request.getAttribute("msp").toString());
			inputParameters.put("HELP_DESK", request.getAttribute("helpdesk")
					.toString());

			System.out.println("XXXXXX APPENDIX_ID:"
					+ request.getAttribute("appendixid").toString());
			System.out.println("XXXXXX START DATE:"
					+ request.getAttribute("startdate").toString());
			System.out.println("XXXXXX END DATE:"
					+ request.getAttribute("enddate").toString());
			System.out.println("XXXXXX CRIT_ID:"
					+ request.getAttribute("criticality").toString());
			System.out.println("XXXXXX STATUS:"
					+ request.getAttribute("status").toString());
			System.out.println("XXXXXX REQ_ID:"
					+ request.getAttribute("requestor").toString());
			System.out.println("XXXXXX CAT_NAME:"
					+ request.getAttribute("criticalitydesc").toString());
			System.out.println("XXXXXX MSP:"
					+ request.getAttribute("msp").toString());
			System.out.println("XXXXXX HELP_DESK:"
					+ request.getAttribute("helpdesk").toString());

			/* for criticality end */

			Map imagesMap = new HashMap();
			request.getSession().setAttribute("IMAGES_MAP", imagesMap);

			// DataSource dataSource = (DataSource) request.getSession(false)
			// .getAttribute("datasource");

			DataSource dataSource = getDataSource(request, "ilexrptDB");

			String path = reportFile.getPath();
			try {

				// conn = dataSource.getConnection();

				/*
				 * BasicDataSource cds = new BasicDataSource(); try {
				 * 
				 * String dsName="reporting"
				 * 
				 * cds.setDriverClassName(PropertyFileUtil.getAppProperty(dsName
				 * + ".db.driver.name"));
				 * cds.setUsername(PropertyFileUtil.getAppProperty(dsName +
				 * ".db.userid"));
				 * 
				 * String pwd = PropertyFileUtil.getAppProperty(dsName +
				 * ".db.password"); pwd = DecryptString.decryptPassword(pwd);
				 * 
				 * cds.setPassword(pwd);
				 * cds.setUrl(PropertyFileUtil.getAppProperty(dsName +
				 * ".db.url"));
				 * 
				 * cds.setMaxActive(Integer.parseInt(PropertyFileUtil.getAppProperty
				 * ( dsName + ".db.pool.maxActive", "25")));
				 * cds.setMaxIdle(Integer
				 * .parseInt(PropertyFileUtil.getAppProperty( dsName +
				 * ".db.pool.maxIdle", "10")));
				 * cds.setMaxWait(Integer.parseInt(PropertyFileUtil
				 * .getAppProperty( dsName + ".db.pool.maxWait", "6000")));
				 * cds.setValidationQuery(PropertyFileUtil.getAppProperty(dsName
				 * + ".db.pool.validationQuery", "SELECT 1"));
				 */

				// conn =
				// DatabaseUtils.getConnection(DocumentConstants.REPORT_DB);
				conn = dataSource.getConnection();

				/*
				 * Properties prop=new Properties(); FileInputStream in = new
				 * FileInputStream
				 * (System.getProperty("WEB-INF/ilex-app.properties"));
				 * prop.load(in); in.close();
				 * 
				 * String drivers = prop.getProperty("jdbc.drivers"); String
				 * connectionURL = prop.getProperty("jdbc.url"); String username
				 * = prop.getProperty("jdbc.username"); String password =
				 * prop.getProperty("jdbc.password");
				 * 
				 * //Class.forName("com.mysql.jdbc.Driver").newInstance();
				 * Class.forName(drivers);
				 * conn=DriverManager.getConnection(connectionURL
				 * ,username,password);
				 * 
				 * System.out.println("Connection Successful");
				 */

			} catch (Exception e) {
				logger.error("doGet(HttpServletRequest, HttpServletResponse)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("doGet(HttpServletRequest, HttpServletResponse) - Exception Occur At makeConnection ");
				}
			}

			// samplePdf(inputParameters, conn);

			JasperResponse jasperResponse = compileJasper(inputParameters,
					conn, path);

			/*
			 * JasperPrint jasperPrint =
			 * JasperFillManager.fillReport(jasperReport, inputParameters,
			 * conn); bytes =
			 * JasperExportManager.exportReportToPdf(jasperPrint);
			 */

			/*
			 * JasperPrint printer = JasperFillManager.fillReport(getClass()
			 * .getResourceAsStream("HelloWorld.jasper"), inputParameters, new
			 * JREmptyDataSource());
			 * JasperExportManager.exportReportToPdfFile(printer,
			 * "D:/report.pdf"); JasperViewer jv = new JasperViewer(printer);
			 * jv.show();
			 */

			if (jasperResponse == null
					|| jasperResponse.getDocumentPageCount() == 0) {
				RequestDispatcher rd = context
						.getRequestDispatcher("/Report_Error.jsp");
				rd.forward(request, response);
				return;
			}

			if (jasperResponse != null
					&& jasperResponse.getDocumentBytes() != null
					&& jasperResponse.getDocumentBytes().length > 0) {
				ServletOutputStream outStream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + pdfFileName + ".pdf"
								+ ";size="
								+ jasperResponse.getDocumentBytes().length + "");
				outStream.write(jasperResponse.getDocumentBytes());
				outStream.close();
			} else {
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<head>");
				out.println("<title>JasperReports - Web Application Sample</title>");
				out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
				out.println("</head>");

				out.println("<body bgcolor=\"white\">");

				out.println("<span class=\"bold\">Empty response.</span>");

				out.println("</body>");
				out.println("</html>");
			}

		} catch (Exception e) {
			logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("<title>JasperReports </title>");
			out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
			out.println("</head>");

			out.println("<body bgcolor=\"white\">");

			out.println("<span class=\"bnew\">JasperReports encountered this error :</span>");
			out.println("<pre>");

			e.printStackTrace(out);

			out.println("</pre>");

			out.println("</body>");
			out.println("</html>");

			return;
		}

		finally {

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"doGet(HttpServletRequest, HttpServletResponse) - exception ignored",
						e);
			}
		}
	}

	public static JasperResponse compileJasper(Map jasperParameter,
			Connection connection, String path) {
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		JasperResponse jasperResponse = new JasperResponse();
		// Connection connection = establishConnection();
		// HashMap jasperParameter = new HashMap();
		byte[] bytes1 = null;
		try {
			// path =
			// "E:\\29August2014\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\webapps\\Ilex\\reports\\HelloWorld.jrxml";
			File file = new File(path);
			System.out.println("file exist" + file.exists());
			jasperReport = JasperCompileManager.compileReport(path);

			// JasperDesign jasperDesign = JasperManager.loadXmlDesign(path);
			// jasperReport = JasperManager.compileReport(jasperDesign);

			/*
			 * JasperFillManager.fillReport(jasperReport, jasperParameter,
			 * connection);
			 */
			/* System.out.println("Hello"); */

			/*
			 * jasperReport = (JasperReport) JRLoader .loadObject(new File(
			 * "E:\\29August2014\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\webapps\\Ilex\\reports\\rpt_ilex_main.jrxml"
			 * ));
			 */
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					jasperParameter, connection);

			if (jasperReport != null) {
				System.out.println("Report Compilation Successfull");
			}

			/*
			 * logger.debug("Calling fillAndExport to fetch the report " +
			 * jasperPrint); logger.debug("parameterMap=" + jasperParameter +
			 * "\n" + "conn=" + connection + "\n");
			 */

			// JasperExportManager.exportReportToPdfFile(jasperPrint,
			// "C://sample_report.pdf");

			// JasperExportManager.exportReportToPdfFile(jasperPrint,
			// "D://sample_report.pdf");
			// JasperViewer jv = new JasperViewer(jasperPrint);
			/* System.out.println("print viewer"); */

			jasperResponse.setDocumentBytes(JasperExportManager
					.exportReportToPdf(jasperPrint));

			jasperResponse.setDocumentPageCount(jasperPrint.getPages().size());

		} catch (JRException e) {
			logger.error("compileJasper(Map, Connection, String)", e);

		}
		return jasperResponse;
	}

	/*
	 * public static void samplePdf(Map jasperParameter, Connection connection)
	 * {
	 * 
	 * try {
	 * 
	 * String fileName =
	 * "E:\\29August2014\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\webapps\\Ilex\\reports\\HelloWorld.jrxml"
	 * ;
	 * 
	 * String fileNamed =
	 * "E:\\29August2014\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\webapps\\Ilex\\reports\\HelloWorld.pdf"
	 * ;
	 * 
	 * JasperReport jasperReport = null;
	 * 
	 * File file = new File(fileName); System.out.println("file exist" +
	 * file.exists()); // JasperDesign jasperDesign = JRXmlLoader.load(file); //
	 * JRJdtCompiler compiler = new JRJdtCompiler(); // jasperReport =
	 * compiler.compileReport(jasperDesign); // System.out.println("design" +
	 * jasperDesign.getName()); // jasperReport = //
	 * JasperCompileManager.compileReportToFile(fileName); Map parameters = new
	 * HashMap();
	 * 
	 * // String pdfPath = //
	 * triggerContext.getServices().getJasperService().getJasperReportasPDF
	 * (jasperReport,parameters);
	 * 
	 * // jasperDesign = JRXmlLoader.load(fileName); // jasperReport =
	 * JasperCompileManager.compileReport(jasperDesign); // Connection
	 * jdbcConnection = openConnection();
	 * 
	 * JasperReport report = JasperCompileManager .compileReport(
	 * "E:\\29August2014\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\webapps\\Ilex\\reports\\rpt_Ilex_mainjasper"
	 * );
	 * 
	 * JasperPrint print = JasperFillManager.fillReport(report, new
	 * HashMap<String, String>()); // export it! try { File pdf =
	 * File.createTempFile("output.", ".pdf"); } catch (IOException e1) { //
	 * TODO Auto-generated catch block e1.printStackTrace(); }
	 * 
	 * OutputStream output = null; try { output = new FileOutputStream( new
	 * File(
	 * "E:\\29August2014\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\webapps\\Ilex\\reports\\HelloWorld.pdf"
	 * )); } catch (FileNotFoundException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); }
	 * JasperExportManager.exportReportToPdfStream(print, output);
	 * 
	 * jasperReport = JasperCompileManager.compileReport(fileName);
	 * 
	 * JasperPrint jasperPrint = JasperFillManager.fillReport( jasperReport,
	 * jasperParameter, new JREmptyDataSource());
	 * 
	 * JasperExportManager.exportReportToHtmlFile(jasperPrint, fileNamed);
	 * 
	 * JasperViewer.viewReport(jasperPrint);
	 * 
	 * 
	 * JasperPrint jasperPrint = JasperFillManager.fillReport( jasperReport,
	 * null, connection);
	 * 
	 * JasperViewer.viewReport(jasperPrint);
	 * 
	 * } catch (JRException e) {
	 * logger.error("compileJasper(Map, Connection, String)", e);
	 * 
	 * }
	 * 
	 * }
	 */
	public DataSource getDataSource(HttpServletRequest argServReq,
			String argDSName) {
		if (argDSName.equals("ilexrptDB")) {
			argDSName = IlexConstants.ILEX_RPT_NAME;
		}
		try {
			return getDataSource(argDSName);
		} catch (Exception e) {

			logger.error("Error getting DataSource.", e);
		}
		return null;
	}

	// get connection

	public static Connection getMySqlConnection(String mySqlUrl)
			throws Exception {
		Connection mySqlConn = null;
		try {
			DataSource ds = DBUtil
					.getDataSource(IlexConstants.CONTINGENT_RPT_NAME);
			mySqlConn = ds.getConnection();
		} catch (Exception e) {
			logger.error("getMySqlConnection(String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMySqlConnection(String) - Error in establishing connection to mySQL.");
			}
		}
		return mySqlConn;
	}

	private static BasicDataSource getCustomDS(String dsName) {
		if (logger.isDebugEnabled()) {
			logger.debug("getCustomDS() - start");
		}
		BasicDataSource cds = new BasicDataSource();
		try {
			cds.setDriverClassName(PropertyFileUtil.getAppProperty(dsName
					+ ".db.driver.name"));
			cds.setUsername(PropertyFileUtil.getAppProperty(dsName
					+ ".db.userid"));

			String pwd = PropertyFileUtil.getAppProperty(dsName
					+ ".db.password");
			pwd = DecryptString.decryptPassword(pwd);

			cds.setPassword(pwd);
			cds.setUrl(PropertyFileUtil.getAppProperty(dsName + ".db.url"));

			cds.setMaxActive(Integer.parseInt(PropertyFileUtil.getAppProperty(
					dsName + ".db.pool.maxActive", "25")));
			cds.setMaxIdle(Integer.parseInt(PropertyFileUtil.getAppProperty(
					dsName + ".db.pool.maxIdle", "10")));
			cds.setMaxWait(Integer.parseInt(PropertyFileUtil.getAppProperty(
					dsName + ".db.pool.maxWait", "6000")));
			cds.setValidationQuery(PropertyFileUtil.getAppProperty(dsName
					+ ".db.pool.validationQuery", "SELECT 1"));
		} catch (Exception e) {
			throw new MindFWRuntimeException(
					"Error creating custom datasource", e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getCustomDS() - end");
		}
		return cds;
	}

	public static DataSource getDataSource(String dsName) {
		if (logger.isDebugEnabled()) {
			logger.debug("getDataSource() - start");
		}
		DataSource ds = (DataSource) dsMap.get(dsName);
		if (ds != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("getDataSource() - end");
			}
			return ds;
		}
		ds = initializeDS(dsName);
		if (logger.isDebugEnabled()) {
			logger.debug("getDataSource() - end");
		}
		return ds;
	}

	// gett
	private static synchronized DataSource initializeDS(String dsName) {
		if (logger.isDebugEnabled()) {
			logger.debug("initializeDS() - start");
		}
		DataSource ds = (DataSource) dsMap.get(dsName);
		if (ds != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("initializeDS() - end");
			}
			return ds;
		}
		ds = new BasicDataSource();

		String jndiName = PropertyFileUtil.getAppProperty(dsName
				+ ".db.jndi.name");
		logger.debug("Initializing DS with JNDI name : " + jndiName);
		if (jndiName != null) {
			ds = getJndiDS(jndiName);
		} else {
			ds = getCustomDS(dsName);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("initializeDS() - end");
		}
		if (dsMap.containsKey(dsName)) {
			dsMap.remove(dsName);
		}
		dsMap.put(dsName, ds);
		return ds;
	}

	private static DataSource getJndiDS(String dsName) {
		if (logger.isDebugEnabled()) {
			logger.debug("getJndiDS() - start");
		}
		DataSource ds = null;
		try {
			InitialContext cxt = new InitialContext();
			if (cxt == null) {
			}
			ds = (DataSource) cxt.lookup(dsName);
			if (logger.isDebugEnabled()) {
				logger.debug("getJndiDS() - end");
			}
		} catch (NamingException ne) {
			throw new MindFWRuntimeException("Error while doing JNDI lookup",
					ne);
		} catch (Exception e) {
			throw new MindFWRuntimeException("fw.ds.jndi.error", e);
		}
		return ds;
	}

}