package com.mind.report;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRProperties;

import org.apache.log4j.Logger;

public class CSVServlet extends HttpServlet {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CSVServlet.class);
	
	public void doPost(HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request,response);
	}
	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		String propsFile = "com.mind.properties.Report";  // resource file name
		ServletContext context = this.getServletConfig().getServletContext();
		Connection conn = null;
		try {
			ResourceBundle res = ResourceBundle.getBundle(propsFile);  //get resourec bundle object
			String reportName = (String)(request.getAttribute("report_type"));
			String imageLocation = context.getRealPath(res.getString("app.img_location"));
			//System.out.println("ds" + (DataSource) request.getSession(false).getAttribute("datasource"));
			//System.out.println("/reports/" + reportName + ".jasper");
			File reportFile =
				new File(
					context.getRealPath( "/reports/" + reportName + ".jasper" ));
			JRProperties.setProperty(
					JRProperties.COMPILER_TEMP_DIR,
					context.getRealPath("/reports/"));
			if (!reportFile.exists())
				throw new JRRuntimeException("File WebappReport.jasper not found. The report design must be compiled first.");

			JasperReport jasperReport =
				(JasperReport) JRLoader.loadObject(reportFile.getPath());
				
			
			Map inputParameters = new HashMap();
			inputParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE,res);
			inputParameters.put("IMAGE_LOCATION",imageLocation);
			inputParameters.put("APPENDIX_ID",new Integer(request.getAttribute("appendixid").toString()));
			

			Map imagesMap = new HashMap();
			request.getSession().setAttribute("IMAGES_MAP", imagesMap);
			
			DataSource dataSource = (DataSource) request.getSession(false).getAttribute("datasource");

			byte[] bytes = null;
			
			try {
				conn = dataSource.getConnection();	
			} catch (Exception e) {
				logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("doGet(HttpServletRequest, HttpServletResponse) - Exception Occur At makeConnection ");
				}
			}

			JasperPrint jasperPrint =
				JasperFillManager.fillReport(jasperReport, inputParameters, conn);

			JRCsvExporter exporter = new JRCsvExporter();
			ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
			exporter.exportReport();
			bytes = xlsReport.toByteArray();
			//System.out.println("No of Pages :" + jasperPrint.getPages().size());
			if (jasperPrint.getPages().size() == 0) {
				RequestDispatcher rd =
					context.getRequestDispatcher(
						"/Report_Error.jsp");
				rd.forward(request, response);
				return;

			}

			if (bytes != null && bytes.length > 0) {
				ServletOutputStream outStream = response.getOutputStream();
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment;filename=" +reportName + ".csv" + ";size=" + bytes.length + "");
				outStream.write(bytes);
				outStream.close();
			} else {
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<head>");
				out.println(
					"<title>JasperReports - Web Application Sample</title>");
				out.println(
					"<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
				out.println("</head>");

				out.println("<body bgcolor=\"white\">");

				out.println("<span class=\"bold\">Empty response.</span>");

				out.println("</body>");
				out.println("</html>");
			}

		} catch (Exception e) {
			logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println(
				"<title>JasperReports </title>");
			out.println(
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
			out.println("</head>");

			out.println("<body bgcolor=\"white\">");

			out.println(
				"<span class=\"bnew\">JasperReports encountered this error :</span>");
			out.println("<pre>");

			e.printStackTrace(out);

			out.println("</pre>");

			out.println("</body>");
			out.println("</html>");

			return;
		}
		
		finally
		{
						
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("doGet(HttpServletRequest, HttpServletResponse) - exception ignored", e);
}
		}
	}

}