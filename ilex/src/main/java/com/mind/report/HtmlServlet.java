package com.mind.report;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRProperties;

import org.apache.log4j.Logger;

public class HtmlServlet extends HttpServlet {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(HtmlServlet.class);
	
	public void doPost(HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request,response);
	}
	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext context = this.getServletConfig().getServletContext();
		Connection conn = null;
		
		try {
			ResourceBundle res = ResourceBundle.getBundle("ilex", request.getLocale());
			String reportName = request.getParameter("reportName");
			String reportPath = context.getRealPath(res.getString("app.jasperfilename"));
			//System.out.println("Report Path" + reportPath);
			//System.out.println("***********report Name*********" + reportName);
			File reportFile =
				new File(
					context.getRealPath("/reports/" + reportName + ".jasper"));
			JRProperties.setProperty(
					JRProperties.COMPILER_TEMP_DIR,
					context.getRealPath("/reports/"));
			//System.out.println("********** HTML ************" + reportFile);
			if (!reportFile.exists())
				throw new JRRuntimeException("File WebappReport.jasper not found. The report design must be compiled first.");

			JasperReport jasperReport =
				(JasperReport) JRLoader.loadObject(reportFile.getPath());

			Map parameters = new HashMap();
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			//parameters.put(JRParameter.REPORT_LOCALE,request.getLocale());
			parameters.put(JRParameter.REPORT_RESOURCE_BUNDLE,res);
			parameters.put("SUBREPORT_FILE_NAME1",reportPath);
			DataSource dataSource = (DataSource) request.getSession(false).getAttribute("DATASOURCE");
			//System.out.println("DataSource is " + dataSource);
			//request.getSession(false).removeAttribute("DATASOURCE");
		//request.removeAttribute("RESBUNDLE");
			byte[] bytes = null;
			
			try {
				conn = dataSource.getConnection();	
			} catch (Exception e) {
				logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("doGet(HttpServletRequest, HttpServletResponse) - Exception Occur At makeConnection ");
				}
			}

			JasperPrint jasperPrint =
				JasperFillManager.fillReport(jasperReport, parameters, conn);
			bytes = JasperExportManager.exportReportToPdf(jasperPrint);
			//System.out.println("No of Pages :" + jasperPrint.getPages().size());
			if (jasperPrint.getPages().size() == 0) {
				RequestDispatcher rd =
					getServletContext().getRequestDispatcher(
						"/Report_Error.jsp");
				rd.forward(request, response);
				return;

			}

			//System.out.println("bytes.length:" + bytes.length);
			if (bytes != null && bytes.length > 0) {
				ServletOutputStream outStream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment;filename=" + "RedLine.pdf" + ";size=" + bytes.length + "");
				outStream.write(bytes);
				outStream.close();
			} else {
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<head>");
				out.println(
					"<title>JasperReports - Web Application Sample</title>");
				out.println(
					"<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
				out.println("</head>");

				out.println("<body bgcolor=\"white\">");

				out.println("<span class=\"bold\">Empty response.</span>");

				out.println("</body>");
				out.println("</html>");
			}

		} catch (Exception e) {
			logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println(
				"<title>JasperReports </title>");
			out.println(
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
			out.println("</head>");

			out.println("<body bgcolor=\"white\">");

			out.println(
				"<span class=\"bnew\">JasperReports encountered this error :</span>");
			out.println("<pre>");

			e.printStackTrace(out);

			out.println("</pre>");

			out.println("</body>");
			out.println("</html>");

			return;
		}
		
		finally
		{
						
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("doGet(HttpServletRequest, HttpServletResponse) - exception ignored", e);
}
		}
	}

}