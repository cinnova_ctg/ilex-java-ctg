package com.mind.RPT.common;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.mind.bean.rpt.XMLTagName;

public class UploadXML {

	/**
	 * Discription: This method parse a XML file and store parsed data to an
	 * ArrayList.
	 * 
	 * @param file
	 *            - XML file
	 * @return ArrayList - list of parsed xml data.
	 */
	public static ArrayList getXMLData(File file) throws Exception {
		/**
		 * Create a instances of DocumentBuilderFactory, DocumentBuilder and
		 * Document. Pass a xml file to document and pass this document to
		 * Element object. Find NO. of all dealer nodes which occure into xml
		 * file and store into NodeList. Store all delear child tags name which
		 * are used to parse into tag Array. Open a for loop for all delear
		 * nodes. Opend another for loop for all delear sub nodes and get nodes
		 * text. All nodes text is store into an ArrayList as a String object.
		 */
		ArrayList list = new ArrayList(); // - to sore parsed data

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		// Document document =
		// builder.parse("C:/Documents and Settings/vijaykumarsingh/Desktop/wiTECHData.xml");
		Document document = builder.parse(file);

		Element root = document.getDocumentElement(); // - load the file
		NodeList nodes = root.getElementsByTagName("dealer"); // - loading the
																// delears tag.

		String[] tag = getTag(); // - get tag name
		boolean add = false; // - for store to list object it will be true.

		/* A for loop of every delear node: Start */
		for (int i = 0, n = nodes.getLength(); i < n; i++) {

			Element elemproduct = (Element) nodes.item(i);
			XMLTagName xmlTagName = new XMLTagName(); // - Create object to
														// store into ArrayList.

			/* A for loop that loops throught the child nodes: Start */
			for (Node node = elemproduct.getFirstChild(); node != null; node = node
					.getNextSibling()) {

				if (node.getNodeType() == Node.ELEMENT_NODE) {

					Element element = (Element) node;
					String tagname = element.getNodeName(); // - current tag
															// name
					String text = getElementText(element); // - current tag text
															// name

					/* get selected node text: Start */
					if (tagname.equals(tag[0])) {
						xmlTagName.setDealernumber(UtilityMethods.isNull(text)); // -
																					// Dealernumber
						add = true;
					} // - end of if

					else if (tagname.equals(tag[1])) {
						String[] netText = new String[7];

						/* When this node has sub nodes */
						if (element.hasChildNodes())
							netText = getNetworkData(element);

						xmlTagName.setStaticgatewayaddress(UtilityMethods
								.isNull(netText[0])); // - Staticgatewayaddress
						xmlTagName.setSubnetmask(UtilityMethods
								.isNull(netText[1])); // - Subnetmask
						xmlTagName.setDefaultgatewayaddress(UtilityMethods
								.isNull(netText[2])); // - Defaultgatewayaddress
						xmlTagName.setPrimarydns(UtilityMethods
								.isNull(netText[3])); // - Primarydns
						xmlTagName.setSecondarydns(UtilityMethods
								.isNull(netText[4])); // - Secondarydns
						xmlTagName.setBroadcastid(UtilityMethods
								.isNull(netText[5])); // - Broadcastid
						xmlTagName.setNetworkid(UtilityMethods
								.isNull(netText[6])); // - Networkid
						add = true;
					} // - end of else if

					else if (tagname.equals(tag[2])) {
						/* When this node has sub nodes */
						if (element.hasChildNodes())
							xmlTagName.setIprange(UtilityMethods
									.isNull(getIPrange(element))); // - Iprange
						add = true;
					}

					else if (tagname.equals(tag[3])) {
						String[] serManagerText = new String[2];

						/* When this node has sub nodes */
						if (element.hasChildNodes())
							serManagerText = getServiceManagerData(element);

						xmlTagName.setSerManagerName(UtilityMethods
								.isNull(serManagerText[0])); // - servicemanager
																// name
						xmlTagName.setSerManagerPhone(UtilityMethods
								.isNull(serManagerText[1]));// - servicemanager
															// phone
						add = true;
					} // - end of else if

					else if (tagname.equals(tag[4])) {
						String[] itAdminText = new String[2];

						/* When this node has sub nodes */
						if (element.hasChildNodes())
							itAdminText = getITAdminData(element);

						xmlTagName.setItAdminName(UtilityMethods
								.isNull(itAdminText[0])); // - itadministrator
															// name
						xmlTagName.setItAdminPhone(UtilityMethods
								.isNull(itAdminText[1]));// - itadministrator
															// phone
						add = true;
					} // - end of else if

					else if (tagname.equals(tag[5])) {
						String pcData = "";
						/* When this node has sub nodes */
						for (Node nnode = element.getFirstChild(); nnode != null; nnode = nnode
								.getNextSibling()) {
							if (nnode.getNodeType() == Node.ELEMENT_NODE) {

								Element nnelement = (Element) nnode;
								String nntagname = nnelement.getNodeName();

								if (nntagname != null
										& nntagname.equals(tag[6])) {
									/* When this node has sub nodes */
									for (Node nnnode = nnelement
											.getFirstChild(); nnnode != null; nnnode = nnnode
											.getNextSibling()) {

										if (nnnode.getNodeType() == Node.ELEMENT_NODE) {
											Element nnnelement = (Element) nnnode;
											String nnntagname = nnnelement
													.getNodeName();
											if (nnntagname != null
													&& nnntagname
															.equals(tag[7])) {
												pcData = pcData
														+ getPCData(nnnelement); // -
																					// pc
																					// tab
																					// data
											}
										}
									} // - end of inner for loop
								}
							}
						} // - end of outer for loop
						xmlTagName.setPcData(UtilityMethods.isNull(pcData
								.trim()));
						add = true;
					} // - end of else if

					else if (tagname.equals(tag[8])) {
						xmlTagName.setOktoconfig(UtilityMethods
								.isNull(getOktoconfig(element))); // -
																	// oktoconfigure
						add = true;
					} // - end of if

					/* get selected node text: End */
				}
			} // - end of if block
			/* A for loop that loops throught the child nodes: Start */

			/* True when any selected node is exist */
			if (add) {
				list.add(xmlTagName);
			}
		}
		/* A for loop of every delear node: End */

		return list;
	}

	/**
	 * Discription: This method return text value of given tag.
	 * 
	 * @param element
	 * @return
	 */
	private static String getElementText(Element element) {
		/**
		 * Open a for loop to get that particular node. Get that node text and
		 * store into sb and return this.
		 */
		StringBuffer sb = new StringBuffer();

		/* A for loop that loops throught the child nodes: Start */
		for (Node node = element.getFirstChild(); node != null; node = node
				.getNextSibling()) {

			if (node.getNodeType() == Node.TEXT_NODE) {

				Text textnode = (Text) node;
				sb.append(textnode.getData());
			}
		}
		/* A for loop that loops throught the child nodes: End */
		return sb.toString().trim();
	}

	/**
	 * Discription: This method get data of network's sub node.
	 * 
	 * @param element
	 *            - Element object
	 */
	private static String[] getNetworkData(Element element) {
		/**
		 * First store all given network's sub node name into tag array. Open a
		 * for loop for all network's sub node, for get all sub nodes text
		 * value. Store all sub's nodes value into netSubTagText array and
		 * return this array.
		 */
		String[] tag = getNetworkTag();
		String[] netSubTagText = new String[7];

		/* A for loop that loops throught the child's child nodes: Start */
		for (Node node = element.getFirstChild(); node != null; node = node
				.getNextSibling()) {

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element nelement = (Element) node;
				String tagname = nelement.getNodeName();
				String text = getElementText(nelement); // - get text value of a
														// tag

				/* get selected node text: Start */
				if (tagname.equals(tag[0]))
					netSubTagText[0] = text;
				else if (tagname.equals(tag[1]))
					netSubTagText[1] = text;
				else if (tagname.equals(tag[2]))
					netSubTagText[2] = text;
				else if (tagname.equals(tag[3]))
					netSubTagText[3] = text;
				else if (tagname.equals(tag[4]))
					netSubTagText[4] = text;
				else if (tagname.equals(tag[5]))
					netSubTagText[5] = text;
				else if (tagname.equals(tag[6]))
					netSubTagText[6] = text;
				/* get selected node text: End */
			}
		}
		/* A for loop that loops throught the child nodes: End */
		return netSubTagText;
	}

	/**
	 * Discription: This method get data of Servicemanager sub node.
	 * 
	 * @param element
	 *            - Element object
	 */
	private static String[] getServiceManagerData(Element element) {
		/**
		 * First store all given servicemanager sub node name into tag array.
		 * Open a for loop for all servicemanager sub node, for get all sub
		 * nodes text value. Store all sub's nodes value into serManagerTagText
		 * array and return this array.
		 */
		String[] tag = getServiceManagerSubTag();
		String[] serManagerTagText = new String[2];

		/* A for loop that loops throught the child's child nodes: Start */
		for (Node node = element.getFirstChild(); node != null; node = node
				.getNextSibling()) {

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element nelement = (Element) node;
				String tagname = nelement.getNodeName();
				String text = getElementText(nelement); // - get text value of a
														// tag

				/* get selected node text: Start */
				if (tagname.equals(tag[0]))
					serManagerTagText[0] = text;
				else if (tagname.equals(tag[1]))
					serManagerTagText[1] = text;
				/* get selected node text: End */
			}
		}
		/* A for loop that loops throught the child nodes: End */
		return serManagerTagText;
	}

	/**
	 * Discription: This method get data of itadmin sub node.
	 * 
	 * @param element
	 *            - Element object
	 */
	private static String[] getITAdminData(Element element) {
		/**
		 * First store all given itadmin sub node name into tag array. Open a
		 * for loop for all itadmin sub node, for get all sub nodes text value.
		 * Store all sub's nodes value into itAdminTagText array and return this
		 * array.
		 */
		String[] tag = getITAdminSubTag();
		String[] itAdminTagText = new String[2];

		/* A for loop that loops throught the child's child nodes: Start */
		for (Node node = element.getFirstChild(); node != null; node = node
				.getNextSibling()) {

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element nelement = (Element) node;
				String tagname = nelement.getNodeName();
				String text = getElementText(nelement); // - get text value of a
														// tag

				/* get selected node text: Start */
				if (tagname.equals(tag[0]))
					itAdminTagText[0] = text;
				else if (tagname.equals(tag[1]))
					itAdminTagText[1] = text;
				/* get selected node text: End */
			}
		}
		/* A for loop that loops throught the child nodes: End */
		return itAdminTagText;
	}

	/**
	 * Dsription: This method concatenate all iprange of a ipaddress tag.
	 * 
	 * @param element
	 *            - Element object
	 * @return String - concatenate all iprange of a ipaddress tag.
	 */
	private static String getIPrange(Element element) {
		/**
		 * First store all attributes name of iprange's sub nodes. Open a for
		 * loop to get attributes value and store into str String seperated by ,
		 * sing. In case of more then on sub nodes, all sub nodes attribute
		 * values are concatenate with str String seperated by ~ sing. e.q
		 * 1rangelow
		 * -1rangehigh~2rangelow-2rangehigh~3rangelow-3rangehigh.........
		 * ...........
		 */
		String[] attribute = getIPAttribute();
		String str = "";

		/* A for loop that loops throught the child's child nodes: Start */
		for (Node node = element.getFirstChild(); node != null; node = node
				.getNextSibling()) {

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element nelement = (Element) node;
				String tagname = nelement.getNodeName();

				/**
				 * iprange is separated by , sign eq. rangelow,rangehigh when
				 * iprange occurs more then one times its seperated by ~ sign
				 */
				// if(!str.equals("")) str = str+"~";

				/* get selected node's attribute text: Start */
				if (tagname.equals("iprange")) {
					str = str + nelement.getAttribute(attribute[0]) + "-"
							+ nelement.getAttribute(attribute[1]) + "~";
				}
				/* get selected node's attribute text: Start */
			}
		}
		/* A for loop that loops throught the child nodes: End */
		return str.trim();
	}

	/**
	 * Discription: This method get data of itmanager sub node.
	 * 
	 * @param element
	 *            - Element object
	 */
	private static String getPCData(Element element) {
		/**
		 * First store all given itmanager sub node name into tag array. Open a
		 * for loop for all itmanager sub node, for get all sub nodes text
		 * value. Store all sub's nodes value into itManagerTagText array and
		 * return this array.
		 */
		String pcData = "";
		String[] tag = getPCSubTag();
		String[] getPCSubTagText = new String[3];

		/* A for loop that loops throught the child's child nodes: Start */
		for (Node node = element.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element nelement = (Element) node;
				String tagname = nelement.getNodeName();
				String text = getElementText(nelement); // - get text value of a
														// tag

				/* get selected node text: Start */
				if (tagname.equals(tag[0]))
					getPCSubTagText[0] = text;
				else if (tagname.equals(tag[1]))
					getPCSubTagText[1] = text;
				else if (tagname.equals(tag[2]))
					getPCSubTagText[2] = text;
				/* get selected node text: End */

			}
		} // - end of outer for
		/* A for loop that loops throught the child nodes: End */
		pcData = getPCSubTagText[0] + "|" + getPCSubTagText[1] + "|"
				+ getPCSubTagText[2] + "~";
		return pcData;
	}

	/**
	 * Discription: This method get data of status sub node.
	 * 
	 * @param element
	 *            - Element object
	 */
	private static String getOktoconfig(Element element) {
		/**
		 * First store all given status sub node name into tag array. Open a for
		 * loop for all status sub node, for get all sub nodes text value. Store
		 * all sub's nodes value into itAdminTagText array and return this
		 * array.
		 */
		String[] tag = getOktoconfigSubTag();
		String oktoconfig = "";

		/* A for loop that loops throught the child's child nodes: Start */
		for (Node node = element.getFirstChild(); node != null; node = node
				.getNextSibling()) {

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element nelement = (Element) node;
				String tagname = nelement.getNodeName();
				String text = getElementText(nelement); // - get text value of a
														// tag

				/* get selected node text: Start */
				if (tagname.equals(tag[0]))
					oktoconfig = text;
				/* get selected node text: End */
			}
		}
		/* A for loop that loops throught the child nodes: End */
		return oktoconfig.trim();
	}

	/**
	 * Discription: This method rerutn tags name form parsing.
	 * 
	 * @return String[] - Tag Name
	 */
	private static String[] getTag() {
		final String tag[] = new String[9];
		tag[0] = "dealernumber";
		tag[1] = "network";
		tag[2] = "ipaddresses";
		tag[3] = "servicemanager";
		tag[4] = "itadministrator";
		tag[5] = "existingequipment";
		tag[6] = "pcs";
		tag[7] = "pc";
		tag[8] = "status";
		return tag;
	}

	/**
	 * Discription: This mehtod return network sub tags name.
	 * 
	 * @return String[] - network subtags name
	 */
	private static String[] getNetworkTag() {
		final String netSubTag[] = new String[7];
		netSubTag[0] = "staticgatewayaddress";
		netSubTag[1] = "subnetmask";
		netSubTag[2] = "defaultgatewayaddress";
		netSubTag[3] = "primarydns";
		netSubTag[4] = "secondarydns";
		netSubTag[5] = "broadcastid";
		netSubTag[6] = "networkid";
		return netSubTag;
	}

	/**
	 * Discription: This method return ipaddress attributes name.
	 * 
	 * @return String[] - Attribute List
	 */
	private static String[] getIPAttribute() {
		final String attribute[] = new String[2];
		attribute[0] = "rangelow";
		attribute[1] = "rangehigh";
		return attribute;
	}

	/**
	 * Discription: This method return service manager sub tab name.
	 * 
	 * @return String[] - Service sub tab name
	 */
	private static String[] getServiceManagerSubTag() {
		final String servicemanager[] = new String[2];
		servicemanager[0] = "name";
		servicemanager[1] = "number";
		return servicemanager;
	}

	/**
	 * Discription: This method return itadministrator sub tab name.
	 * 
	 * @return String[] - itadministrator tab name
	 */
	private static String[] getITAdminSubTag() {
		final String itadministrator[] = new String[2];
		itadministrator[0] = "name";
		itadministrator[1] = "number";
		return itadministrator;
	}

	/**
	 * Discription: This method return pc sub tab name.
	 * 
	 * @return String[] - pc tab name
	 */
	private static String[] getPCSubTag() {
		final String pc[] = new String[5];
		pc[0] = "pcid";
		pc[1] = "systemname";
		pc[2] = "os";
		return pc;
	}

	/**
	 * Discription: This method return Status sub tab name.
	 * 
	 * @return String[] - Status tab name
	 */
	private static String[] getOktoconfigSubTag() {
		final String okToConfig[] = new String[5];
		okToConfig[0] = "oktoconfigure";
		return okToConfig;
	}

	/**
	 * Discription: Use main method for testing only.
	 */
	// public static void main(String args[]){
	// UploadXML uploadXML = new UploadXML();
	// ArrayList list = new ArrayList();
	// FormFile fromfile;
	// File file = new
	// File("C:/Documents and Settings/vijaykumarsingh/Desktop/test.xml");
	// try{
	// list = uploadXML.getXMLData(file);
	// }catch(Exception e){System.out.println(e);}
	// Iterator iterator = list.iterator();
	// while(iterator.hasNext()){
	// XMLTagName xmlTagName = (XMLTagName)iterator.next() ;
	// System.out.println("Dealernumber = "+xmlTagName.getDealernumber());
	// System.out.println("Defaultgatewayaddress = "+xmlTagName.getDefaultgatewayaddress());
	// System.out.println("Primarydns = "+xmlTagName.getPrimarydns());
	// System.out.println("Secondarydns = "+xmlTagName.getSecondarydns());
	// System.out.println("Staticgatewayaddress = "+xmlTagName.getStaticgatewayaddress());
	// System.out.println("Subnetmask = "+xmlTagName.getSubnetmask());
	// System.out.println("Iprange = "+xmlTagName.getIprange());
	// System.out.println("SerManagerName = "+xmlTagName.getSerManagerName());
	// System.out.println("SerManagerPhone = "+xmlTagName.getSerManagerPhone());
	// System.out.println("ItManagerName = "+xmlTagName.getItManagerName());
	// System.out.println("ItManagerPhone = "+xmlTagName.getItManagerPhone());
	// System.out.println("PcData = "+xmlTagName.getPcData());
	// System.out.println("Oktoconfig = "+xmlTagName.getOktoconfig());
	// }
	// System.out.println("list size= "+list.size());
	// }
}
