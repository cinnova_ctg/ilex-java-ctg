package com.mind.RPT.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * Discription: This class is use to change data format.
 * @author vijaykumarsingh
 *
 */
public class ChangeDateFormat{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ChangeDateFormat.class);
	
	
	/**
	 * Discription: This method take date, output date format and input date format.
	 * 				This method change format of date.
	 * @param INPUT_DATE		- Date
	 * @param MY_DATE_FORMAT	- Output Date Format
	 * @param INPUT_DATE_FORMAT	- Input Date Format
	 * @return String			- Date
	 */
	public static String getDateFormat(String INPUT_DATE, String MY_DATE_FORMAT, String INPUT_DATE_FORMAT){
		
		String expDate = "";
		try{
			
			DateFormat  df = new SimpleDateFormat();
			Date d = new SimpleDateFormat(INPUT_DATE_FORMAT).parse(INPUT_DATE);
			expDate = new SimpleDateFormat(MY_DATE_FORMAT).format(d);
//			System.out.println("expDate = "+expDate);

		}
		catch(Exception e){
			logger.error("getDateFormat(String, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDateFormat(String, String, String) - com.mind.RPT.common.ChangeDateFormat.getDayName() " + e);
			}
		}
		return expDate;
	}
	
	public static void main(String args[]){ // for test only
		String dayMonName = ChangeDateFormat.getDateFormat("06/26/2008", "EEEE,MMMM", "MM/dd/yyyy");
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - 1= " + dayMonName);
		}
		String MonYear = ChangeDateFormat.getDateFormat("06/26/2008", "MMMM yy", "MM/dd/yyyy");
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - 2= " + MonYear);
		}
		String mmddyy = ChangeDateFormat.getDateFormat("06/26/2008", "yyyy-MM-dd HH:mm:ss.ms", "MM/dd/yyyy");
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - 3= " + mmddyy);
		}
		String mmddyy_hhmm_aaa = ChangeDateFormat.getDateFormat("06/26/2008 09:29 PM", "yyyy-MM-dd HH:mm", "MM/dd/yyyy hh:mm aaa");
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - 3= " + mmddyy_hhmm_aaa);
		}
		
	}

}
