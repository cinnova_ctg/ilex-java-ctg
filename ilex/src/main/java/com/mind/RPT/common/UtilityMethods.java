package com.mind.RPT.common;

import java.io.FileOutputStream;

import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;

public class UtilityMethods {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UtilityMethods.class);

	/**
	 * Discription: This method is used to write a file into /PM/Viewxml/temp
	 * folder.
	 * 
	 * @param file
	 *            - File name for store.
	 * @param path
	 *            - Path name where this file will be stored.
	 */
	public static void writeFile(FormFile file, String path) throws Exception {
		/**
		 * First convet file to byte[]. Open FileOutputStream and set path for
		 * store. Write byte[] and after that close this FileOutputStream.
		 */
		byte[] buffer = file.getFileData();
		FileOutputStream fw = new FileOutputStream(path);
		fw.write(buffer);
		fw.close();
	}

	/**
	 * Discription: This method check given file has .xml extension or not.
	 * 
	 * @param filename
	 *            - File name(abc.xml)
	 * @return boolean - true/false
	 */
	public static boolean checkFile(String filename) throws Exception {
		/**
		 * Find out last index .xml from given file name. check index = (length
		 * of string - 4) if index = (length of string - 4), it is a vailid .xml
		 * file othewise it's not.
		 */
		boolean check = false;
		try {
			if (filename.lastIndexOf(".xml") == (filename.length() - 4)) {
				check = true;
			}
		} catch (Exception e) {
			logger.warn("checkFile(String) - exception ignored", e);
		}

		return check;
	}

	/**
	 * Discription: This method replace null string to blank string.
	 * 
	 * @param str
	 *            - String
	 * @return String - modify string
	 */
	public static String isNull(String str) {
		try {
			if (str == null) {
				str = "";
			}
		} catch (Exception e) {
			logger.warn("isNull(String) - exception ignored", e);
		}

		return str;
	}
}
