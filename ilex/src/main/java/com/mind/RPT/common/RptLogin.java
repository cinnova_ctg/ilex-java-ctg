package com.mind.RPT.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class Discription: This Servlet class is used for Ilex Authentication.
 * Methods : doGet
 * 
 * @author ILEX
 */
public class RptLogin extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		response.sendRedirect("./main.jsp?rpt=rpt");
	}

}
