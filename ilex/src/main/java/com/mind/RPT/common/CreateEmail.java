package com.mind.RPT.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.MPO.dao.MPODao;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Email;
import com.mind.fw.core.dao.util.DBUtil;

public class CreateEmail {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CreateEmail.class);

	static ResourceBundle rbRPT = ResourceBundle
			.getBundle("com.mind.RPT.properties.ApplicationResourcesRPT");
	/* - Required smtp authentication */
	static ResourceBundle rb = ResourceBundle
			.getBundle("com.mind.properties.ApplicationResources");
	static String username = rb.getString("common.Email.username");
	static String userPassword = rb.getString("common.Email.userpassword");

	static String smtpServerName = EnvironmentSelector
			.getBundleString("common.Email.smtpservername");

	static String smtpServerPort = rb.getString("common.Email.smtpserverport");

	static String fromMailId = rbRPT.getString("rpt.Email.from");
	static String ccMailId = rbRPT.getString("rpt.Email.cc");

	static String incidentEmailTo = EnvironmentSelector
			.getBundleString("incident.email.to");

	public static void CallConfirmCreationMail(String loginUser, String jobID,
			String appendixId, DataSource ds) {

		StringBuffer mailContent = new StringBuffer(); // - for make email
		// content.
		if (logger.isDebugEnabled()) {
			logger.debug("CallConfirmCreationMail(String, String, String, DataSource) - End email Part successfully");
		}
		String[] getMailData = getMailData(loginUser, jobID, ds);
		mailContent.append(getMailData[1] + " " + getMailData[2] + ",<br>");
		mailContent
				.append("You have been confirmed as the point of contact for the Witech installation.  "
						+ "The installation window is scheduled for "
						+ getMailData[4]
						+ " "
						+ "-"
						+ " "
						+ getMailData[6]
						+ ".");
		// mailContent.append( rb.getString("appendix.Email.content1") );
		// mailContent.append( "Thanks,<br>"+cnsPocMailId[0] );

		EmailBean emailform = new EmailBean();
		emailform.setUsername(username);
		emailform.setUserpassword(userPassword);
		emailform.setSmtpservername(smtpServerName);
		emailform.setSmtpserverport(smtpServerPort);

		emailform.setTo(getMailData[3]);
		emailform.setCc(ccMailId + "," + getMailData[5]);
		emailform.setBcc("");

		emailform.setSubject("WiTech Installation Confirmation for "
				+ getMailData[0]);
		emailform.setFrom(fromMailId);
		emailform.setContent(mailContent.toString());
		// emailform.setType( "prm_appendix" );
		// emailform.setChangeStatus( "" );
		Email.Send(emailform, "text/html", ds);
		if (logger.isDebugEnabled()) {
			logger.debug("CallConfirmCreationMail(String, String, String, DataSource) - End email Part successfully");
		}

	}

	public static void sendIncidentMail(String mailContent, String userId,
			DataSource ds) {

		MPODao mpoDao = new MPODao();
		String from = mpoDao.getFromAdd(userId);
		EmailBean emailform = new EmailBean();
		emailform.setUsername(username);
		emailform.setUserpassword(userPassword);
		emailform.setSmtpservername(smtpServerName);
		emailform.setSmtpserverport(smtpServerPort);
		emailform.setTo(incidentEmailTo);
		emailform.setCc("");
		emailform.setBcc("");
		emailform.setFrom(from);
		emailform.setSubject("Incident Report");
		emailform.setContent(mailContent);
		Email.Send(emailform, "text/html", ds);

	}

	public static String[] getMailData(String loginUser, String jobID,
			DataSource ds) {
		String[] getMailData = new String[7];
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String str = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			str = "select lm_si_work_location,lm_si_pri_poc,lm_si_sec_poc,lm_si_pri_email_id, lm_js_planned_start_date from lm_site_detail "
					+ "inner join lm_job on lm_js_si_id=lm_si_id where lm_js_id="
					+ jobID;
			rs = stmt.executeQuery(str);
			if (rs.next()) {

				getMailData[0] = rs.getString("lm_si_work_location");
				getMailData[1] = rs.getString("lm_si_pri_poc");
				getMailData[2] = rs.getString("lm_si_sec_poc");
				getMailData[3] = rs.getString("lm_si_pri_email_id");
			}
			str = "select lo_pc_email1 from lo_poc where lo_pc_id=" + loginUser;
			rs = stmt.executeQuery(str);
			if (rs.next()) {

				getMailData[5] = rs.getString("lo_pc_email1");
			}
			str = "select * from func_site_job_info(" + jobID + ")";
			rs = stmt.executeQuery(str);
			if (rs.next()) {
				getMailData[4] = rs.getString("planned_start_date") + " "
						+ rs.getString("planned_start_time");
				getMailData[6] = rs.getString("planned_start_date") + " "
						+ rs.getString("planned_start_exdended_time");
			}
		} catch (Exception e) {
			logger.error("getMailData(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMailData(String, String, DataSource) - Error ocured in CreateEmail.getMailData() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return getMailData;
	}
}
