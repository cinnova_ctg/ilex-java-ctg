package com.mind.RPT.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class RPTPerformanceDashboardForm extends ActionForm{
	
	private String loginName = null;
	private String jobStatus = null;
	private String count = null;
	private String revenue = null;
	private String aveVGPM = null;
	private ArrayList jobPerformanaceList = null;
	private ArrayList invoiceStatusList = null;
	private String serviceTime = null;

	
	
	
	

	public ArrayList getInvoiceStatusList() {
		return invoiceStatusList;
	}

	public void setInvoiceStatusList(ArrayList invoiceStatusList) {
		this.invoiceStatusList = invoiceStatusList;
	}

	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}

	public ArrayList getJobPerformanaceList() {
		return jobPerformanaceList;
	}

	public void setJobPerformanaceList(ArrayList jobPerformanaceList) {
		this.jobPerformanaceList = jobPerformanaceList;
	}

	public String getAveVGPM() {
		return aveVGPM;
	}

	public void setAveVGPM(String aveVGPM) {
		this.aveVGPM = aveVGPM;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

}
