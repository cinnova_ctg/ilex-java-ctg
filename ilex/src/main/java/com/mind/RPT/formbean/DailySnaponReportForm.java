package com.mind.RPT.formbean;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class DailySnaponReportForm extends ActionForm{
	
	private Collection snCnfJobList = null;
	private Collection snSchJobList = null;
	private String date = null;
	private String go = null;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public Collection getSnCnfJobList() {
		return snCnfJobList;
	}
	public void setSnCnfJobList(Collection snCnfJobList) {
		this.snCnfJobList = snCnfJobList;
	}
	public Collection getSnSchJobList() {
		return snSchJobList;
	}
	public void setSnSchJobList(Collection snSchJobList) {
		this.snSchJobList = snSchJobList;
	}

}
