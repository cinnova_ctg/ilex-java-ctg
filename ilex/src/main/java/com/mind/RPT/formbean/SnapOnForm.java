package com.mind.RPT.formbean;

import java.util.Collection;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class SnapOnForm extends ActionForm{
	
	private FormFile filename = null;
	private String upload = null;
	private Collection info = null;
	
	
	public FormFile getFilename() {
		return filename;
	}
	public void setFilename(FormFile filename) {
		this.filename = filename;
	}
	public String getUpload() {
		return upload;
	}
	public void setUpload(String upload) {
		this.upload = upload;
	}
	public Collection getInfo() {
		return info;
	}
	public void setInfo(Collection info) {
		this.info = info;
	}

}
