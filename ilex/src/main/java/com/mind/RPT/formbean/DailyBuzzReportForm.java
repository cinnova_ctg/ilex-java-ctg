package com.mind.RPT.formbean;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.mind.common.LabelValue;

public class DailyBuzzReportForm extends ActionForm {

	private Collection jobOwnerList = null;
	private Collection schList = null;
	private Collection cnfList = null;
	private Collection onsiteList = null;
	private Collection offsiteList = null;
	private Collection appendixTitleList = null;
	private Collection msaList = null;
	private String jobOwner = null;
	private String appendixName = null;
	private String date = null;
	private String go = null;
	private String srPrjManager;
	private List<LabelValue> srPrjManagerList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		jobOwnerList = null;
		schList = null;
		cnfList = null;
		onsiteList = null;
		offsiteList = null;
		appendixTitleList = null;
		jobOwner = null;
		appendixName = null;
		date = null;
		go = null;
	}

	public Collection getAppendixTitleList() {
		return appendixTitleList;
	}

	public void setAppendixTitleList(Collection appendixTitleList) {
		this.appendixTitleList = appendixTitleList;
	}

	public Collection getCnfList() {
		return cnfList;
	}

	public void setCnfList(Collection cnfList) {
		this.cnfList = cnfList;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getJobOwner() {
		return jobOwner;
	}

	public void setJobOwner(String jobOwner) {
		this.jobOwner = jobOwner;
	}

	public Collection getJobOwnerList() {
		return jobOwnerList;
	}

	public void setJobOwnerList(Collection jobOwnerList) {
		this.jobOwnerList = jobOwnerList;
	}

	public Collection getOffsiteList() {
		return offsiteList;
	}

	public void setOffsiteList(Collection offsiteList) {
		this.offsiteList = offsiteList;
	}

	public Collection getOnsiteList() {
		return onsiteList;
	}

	public void setOnsiteList(Collection onsiteList) {
		this.onsiteList = onsiteList;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public Collection getSchList() {
		return schList;
	}

	public void setSchList(Collection schList) {
		this.schList = schList;
	}

	public Collection getMsaList() {
		return msaList;
	}

	public void setMsaList(Collection msaList) {
		this.msaList = msaList;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getSrPrjManager() {
		return srPrjManager;
	}

	public void setSrPrjManager(String srPrjManager) {
		this.srPrjManager = srPrjManager;
	}

	public List<LabelValue> getSrPrjManagerList() {
		return srPrjManagerList;
	}

	public void setSrPrjManagerList(List<LabelValue> srPrjManagerList) {
		this.srPrjManagerList = srPrjManagerList;
	}
}
