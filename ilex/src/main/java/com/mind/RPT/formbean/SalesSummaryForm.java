package com.mind.RPT.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class SalesSummaryForm extends ActionForm{

	private String toDate = null;
	private String fromDate = null;
	private ArrayList salesSummarylist = null;
	private String go = null;
	
	
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public ArrayList getSalesSummarylist() {
		return salesSummarylist;
	}
	public void setSalesSummarylist(ArrayList salesSummarylist) {
		this.salesSummarylist = salesSummarylist;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	
	

}
