package com.mind.RPT.formbean;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class SnaponCallForm extends ActionForm{
	
	private String jobId = null;
	private String appendixId = null;
	private String pirPhone = null;
	private String call1Status = null;
	private String call1Contact = null;
	private String call2Status = null;
	private String call2Contact = null;
	private String call12Status = null;
	private String call12Contact = null;
	private String escalate = null;
	private String reschedule = null;
	private String dealerName = null;
	private String priContact = null;
	private String save = null;
	private String reset = null;
	private String back = null;
	private String date = null;
	private String occurance = null;
	private String from = null;
	
	private Collection day5Call1 = null;
	private Collection day5Call2 = null;
	private Collection hour24Call = null;
	
	public String getCall12Contact() {
		return call12Contact;
	}
	public void setCall12Contact(String call12Contact) {
		this.call12Contact = call12Contact;
	}
	public String getCall12Status() {
		return call12Status;
	}
	public void setCall12Status(String call12Status) {
		this.call12Status = call12Status;
	}
	public String getCall1Contact() {
		return call1Contact;
	}
	public void setCall1Contact(String call1Contact) {
		this.call1Contact = call1Contact;
	}
	public String getCall1Status() {
		return call1Status;
	}
	public void setCall1Status(String call1Status) {
		this.call1Status = call1Status;
	}
	public String getCall2Contact() {
		return call2Contact;
	}
	public void setCall2Contact(String call2Contact) {
		this.call2Contact = call2Contact;
	}
	public String getCall2Status() {
		return call2Status;
	}
	public void setCall2Status(String call2Status) {
		this.call2Status = call2Status;
	}
	public String getEscalate() {
		return escalate;
	}
	public void setEscalate(String escalate) {
		this.escalate = escalate;
	}
	public String getPirPhone() {
		return pirPhone;
	}
	public void setPirPhone(String pirPhone) {
		this.pirPhone = pirPhone;
	}
	public String getReschedule() {
		return reschedule;
	}
	public void setReschedule(String reschedule) {
		this.reschedule = reschedule;
	}
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public Collection getDay5Call1() {
		return day5Call1;
	}
	public void setDay5Call1(Collection day5Call1) {
		this.day5Call1 = day5Call1;
	}
	public Collection getDay5Call2() {
		return day5Call2;
	}
	public void setDay5Call2(Collection day5Call2) {
		this.day5Call2 = day5Call2;
	}
	public Collection getHour24Call() {
		return hour24Call;
	}
	public void setHour24Call(Collection hour24Call) {
		this.hour24Call = hour24Call;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getPriContact() {
		return priContact;
	}
	public void setPriContact(String priContact) {
		this.priContact = priContact;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getOccurance() {
		return occurance;
	}
	public void setOccurance(String occurance) {
		this.occurance = occurance;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}

}
