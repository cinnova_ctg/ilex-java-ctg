package com.mind.RPT.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class AuditTrailForm extends ActionForm{
	private ArrayList auditTrailList= null;	
	private ArrayList auditTrailList1 = null;
	private ArrayList auditTrailListOperation = null;
	private ArrayList auditTrailListUser = null;
	private ArrayList searchData = null;
	private ArrayList fieldData = null;
	private String tn = null;	//Table Name
	private String oper = null; //Operation Name
	private String fn = null;   // Field Names
	private String users = null;	
	private String auditIDS = null;
	
	public ArrayList getAuditTrailList1() {
		return auditTrailList1;
	}
	public void setAuditTrailList1(ArrayList auditTrailList1) {
		this.auditTrailList1 = auditTrailList1;
	}
	public ArrayList getAuditTrailListOperation() {
		return auditTrailListOperation;
	}
	public void setAuditTrailListOperation(ArrayList auditTrailListOperation) {
		this.auditTrailListOperation = auditTrailListOperation;
	}
	public ArrayList getSearchData() {
		return searchData;
	}
	public void setSearchData(ArrayList searchData) {
		this.searchData = searchData;
	}
	public String getFn() {
		return fn;
	}
	public void setFn(String fn) {
		this.fn = fn;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public String getTn() {
		return tn;
	}
	public void setTn(String tn) {
		this.tn = tn;
	}
	public ArrayList getFieldData() {
		return fieldData;
	}
	public void setFieldData(ArrayList fieldData) {
		this.fieldData = fieldData;
	}
	public ArrayList getAuditTrailList() {
		return auditTrailList;
	}
	public void setAuditTrailList(ArrayList auditTrailList) {
		this.auditTrailList = auditTrailList;
	}
	public ArrayList getAuditTrailListUser() {
		return auditTrailListUser;
	}
	public void setAuditTrailListUser(ArrayList auditTrailListUser) {
		this.auditTrailListUser = auditTrailListUser;
	}
	public String getUsers() {
		return users;
	}
	public void setUsers(String users) {
		this.users = users;
	}
	public String getAuditIDS() {
		return auditIDS;
	}
	public void setAuditIDS(String auditIDS) {
		this.auditIDS = auditIDS;
	}
		
}