package com.mind.RPT.formbean;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class RescheduleReportForm extends ActionForm{
	
	private Collection msaList = null;
	private Collection rescheduleList = null;
	private String appendixId = null;
	private String go = null;
	

	public Collection getMsaList() {
		return msaList;
	}

	public void setMsaList(Collection msaList) {
		this.msaList = msaList;
	}

	public Collection getRescheduleList() {
		return rescheduleList;
	}

	public void setRescheduleList(Collection rescheduleList) {
		this.rescheduleList = rescheduleList;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

}
