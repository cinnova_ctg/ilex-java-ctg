package com.mind.RPT.formbean;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class JobOwnerSummaryForm extends ActionForm{
	
	private String toDate = null;
	private String fromDate = null;
	private String calToDate = null;
	private String calFromDate = null;
	private String jobOwner = null;
	private String appendixName = null;
	private String tabId = null;
	private String go = null;
	private Collection jobOwnerList = null;
	private Collection appendixTitleList = null;
	private Collection appendixList = null;
	private Collection tabInfo = null;
	private Collection msaList = null;
	private Collection projectTeamList = null;
	private Collection schedulerList = null;
	private Collection srProjectManagers = null;
	private String srProjectManagersId = null;
	private String projectTeam = null;
	private String scheduler = null;
	private String msaId = null;
	private String msaName = null;
	private String currMMYY = null;
	private String tabName = null;
	private String cccFromDate = null;
	private String cccToDate = null;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		toDate = null;
		fromDate = null;
		jobOwner = null;
		appendixName = null;
		tabId = null;
		go = null;
		jobOwnerList = null;
		appendixTitleList = null;
		appendixList = null;
		tabInfo = null;
		msaList = null;
		msaId = null;
		msaName = null;
		currMMYY = null;
		tabName = null;
		calToDate= null;
		calFromDate = null;
		projectTeamList = null;
		schedulerList = null;
		projectTeam = null;
		scheduler = null;
	}
	
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Collection getJobOwnerList() {
		return jobOwnerList;
	}
	public void setJobOwnerList(Collection jobOwnerList) {
		this.jobOwnerList = jobOwnerList;
	}
	public String getJobOwner() {
		return jobOwner;
	}
	public void setJobOwner(String jobOwner) {
		this.jobOwner = jobOwner;
	}
	public Collection getAppendixTitleList() {
		return appendixTitleList;
	}
	public void setAppendixTitleList(Collection appendixTitleList) {
		this.appendixTitleList = appendixTitleList;
	}
	public String getTabId() {
		return tabId;
	}
	public void setTabId(String tabId) {
		this.tabId = tabId;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public Collection getAppendixList() {
		return appendixList;
	}
	public void setAppendixList(Collection appendixList) {
		this.appendixList = appendixList;
	}
	public Collection getMsaList() {
		return msaList;
	}
	public void setMsaList(Collection msaList) {
		this.msaList = msaList;
	}
	public String getCurrMMYY() {
		return currMMYY;
	}
	public void setCurrMMYY(String currMMYY) {
		this.currMMYY = currMMYY;
	}
	public Collection getTabInfo() {
		return tabInfo;
	}
	public void setTabInfo(Collection tabInfo) {
		this.tabInfo = tabInfo;
	}
	public String getTabName() {
		return tabName;
	}
	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	public String getCalFromDate() {
		return calFromDate;
	}

	public void setCalFromDate(String calFromDate) {
		this.calFromDate = calFromDate;
	}

	public String getCalToDate() {
		return calToDate;
	}

	public void setCalToDate(String calToDate) {
		this.calToDate = calToDate;
	}

	public Collection getProjectTeamList() {
		return projectTeamList;
	}

	public void setProjectTeamList(Collection projectTeamList) {
		this.projectTeamList = projectTeamList;
	}

	public String getProjectTeam() {
		return projectTeam;
	}

	public void setProjectTeam(String projectTeam) {
		this.projectTeam = projectTeam;
	}

	public String getScheduler() {
		return scheduler;
	}

	public void setScheduler(String scheduler) {
		this.scheduler = scheduler;
	}

	public Collection getSchedulerList() {
		return schedulerList;
	}

	public void setSchedulerList(Collection schedulerList) {
		this.schedulerList = schedulerList;
	}

	public String getCccFromDate() {
		return cccFromDate;
	}

	public void setCccFromDate(String cccFromDate) {
		this.cccFromDate = cccFromDate;
	}

	public String getCccToDate() {
		return cccToDate;
	}

	public void setCccToDate(String cccToDate) {
		this.cccToDate = cccToDate;
	}

	public Collection getSrProjectManagers() {
		return srProjectManagers;
	}

	public void setSrProjectManagers(Collection srProjectManagers) {
		this.srProjectManagers = srProjectManagers;
	}

	public String getSrProjectManagersId() {
		return srProjectManagersId;
	}

	public void setSrProjectManagersId(String srProjectManagersId) {
		this.srProjectManagersId = srProjectManagersId;
	}
	
	
}
