package com.mind.RPT.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class SupportAppendixForm extends ActionForm{
	
	private String toDate = null;
	private String fromDate = null;
	private ArrayList supportAppendixlist = null;
	private String go = null;
	
	
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public ArrayList getSupportAppendixlist() {
		return supportAppendixlist;
	}
	public void setSupportAppendixlist(ArrayList supportAppendixlist) {
		this.supportAppendixlist = supportAppendixlist;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	

}
