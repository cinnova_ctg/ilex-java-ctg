package com.mind.RPT.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.dao.DailySnaponReportdao;
import com.mind.RPT.dao.RPTdao;
import com.mind.RPT.formbean.DailySnaponReportForm;
import com.mind.common.dao.Authenticationdao;

public class DailySnaponReportAction extends com.mind.common.IlexAction{
	
	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   // - Check for session expired
		String userid = (String)session.getAttribute("userid"); // - Login user id
		if( !Authenticationdao.getPageSecurity( userid , "Support" , "Reschedule Report" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		/* Page Security End*/
		
		DailySnaponReportForm dailySnapon = (DailySnaponReportForm)form;
		
		/* Set default range */
		if( request.getAttribute("date") != null ) // - call from com.mind.RPT.Action.SnaponCallAction
			dailySnapon.setDate(request.getAttribute("date").toString());
		
		if( dailySnapon.getDate() == null )
			dailySnapon.setDate( RPTdao.getcurrDate(getDataSource( request , "ilexnewDB" )) );
		
		dailySnapon.setSnCnfJobList(DailySnaponReportdao.getCnfJobList(dailySnapon.getDate(), getDataSource( request , "ilexnewDB" )));
		dailySnapon.setSnSchJobList(DailySnaponReportdao.getSchJobList(dailySnapon.getDate(), getDataSource( request , "ilexnewDB" )));
		
		return mapping.findForward( "success" ); // - /RPT/DailySnaponReport.jsp
	}
}
