package com.mind.RPT.Action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.dao.RPTdao;
import com.mind.RPT.dao.RescheduleReportdao;
import com.mind.RPT.formbean.JobOwnerSummaryForm;
import com.mind.RPT.formbean.RescheduleReportForm;
import com.mind.common.dao.Authenticationdao;

/**
 * Discription:		This class is used for Reschedule Report.
 * @author vijaykumarsingh
 *
 */
public class RescheduleReportAction  extends com.mind.common.IlexAction{
	
	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   // - Check for session expired
		String userid = (String)session.getAttribute("userid"); // - Login user id
		if( !Authenticationdao.getPageSecurity( userid , "Support" , "Reschedule Report" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		/* Page Security End*/
		
		RescheduleReportForm rescheduleReport = (RescheduleReportForm)form;
		
		/* For Project ComboBox: Start*/
		ArrayList msaList = RPTdao.getMsaList(getDataSource( request , "ilexnewDB" )); // - get msa list
		if(msaList.size() > 0){
			for(int i=0; i < msaList.size(); i++){ // - group Appendix title on the basis of MSA title
				String tempid = ( (JobOwnerSummaryForm)msaList.get(i) ).getMsaId();
				// - get all appnedix for every msa
				( (JobOwnerSummaryForm)msaList.get(i) ).setAppendixList(RPTdao.getAppendixTitleList(tempid,i,getDataSource( request , "ilexnewDB" )));
			}// - end of for loop
		}
		rescheduleReport.setMsaList(msaList);
		/* For Project ComboBox: End */
		
		/* Set Reschedule List */
		rescheduleReport.setRescheduleList(RescheduleReportdao.getRescheduleList(rescheduleReport.getAppendixId(),getDataSource( request , "ilexnewDB" )));
		
		return mapping.findForward( "success" ); // - RPT/RescheduleReport.jsp
	}

}
