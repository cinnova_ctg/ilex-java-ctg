package com.mind.RPT.Action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.dao.RPTdao;
import com.mind.RPT.formbean.SupportNetMedXForm;
import com.mind.common.dao.Authenticationdao;

/**
 * Discription: This class is use to generate list for CompuCom Systems, Inc. -> NetMedX Appendix: Appendix C - NetMedX.
 * 				This list is come from live database only. 
 * @author Vijay Kumar Singh
 *
 */
public class SupprortNetMedXListAction extends com.mind.common.IlexAction{

	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   // - Check for session expired
		String userid = (String)session.getAttribute("userid"); // - Login user id
		if( !Authenticationdao.getPageSecurity( userid , "Support" , "L/D Report" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		/* Page Security End*/
		
		SupportNetMedXForm supportnetmedx = ( SupportNetMedXForm )form;
		
		/* List for CompuCom Systems, Inc. -> NetMedX Appendix: Appendix C - NetMedX*/
		ArrayList list = RPTdao.getSupportNetMedX(supportnetmedx.getFromDate(),supportnetmedx.getToDate(),"208");
		supportnetmedx.setSupportNetMedXlist(list);
		
		return mapping.findForward( "success" ); // - SupportNetMedeXList.jsp
	}
}
