package com.mind.RPT.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.dao.RPTdao;
import com.mind.RPT.formbean.RPTPerformanceDashboardForm;
import com.mind.common.dao.Authenticationdao;

/**
 * Class Description: This class is used to retrieve all data for Job Performence Dashboard.
 * Methods : execute
 * @author ILEX
 *
 */
public class RPTPerformanceDashboardAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)	throws Exception {
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   //Check for session expired
		String userid = (String)session.getAttribute("userid"); // Login user id
		if( !Authenticationdao.getPageSecurity( userid , "Performance Dashboard" , "View Performance Dashboard" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		/* Page Security End*/
		
		/* Check User Classification*/
		if(RPTdao.checkUser(userid,getDataSource( request , "ilexnewDB" ))) {
			return (mapping.findForward("blankPage"));
		}		
		/* Check User Classification End */
		
		RPTPerformanceDashboardForm rptdashboardform = (RPTPerformanceDashboardForm) form;
		
		rptdashboardform.setLoginName(RPTdao.getLoginName(userid,getDataSource( request , "ilexnewDB" ))); // - set login user name
		rptdashboardform.setServiceTime(RPTdao.getServiceTimeInterval(userid,getDataSource( request , "ilexnewDB" ))); // - set total service time in month
		rptdashboardform.setJobPerformanaceList(RPTdao.getJobPerformance(userid,getDataSource( request , "ilexnewDB" ))); // - set Job Performance list on the basis of login user id
		rptdashboardform.setInvoiceStatusList(RPTdao.getEInvoiceStatus(userid,getDataSource( request , "ilexnewDB" ))); // - set List of eInvoice status
		
//		Iterator iterator = rptdashboardform.getInvoiceStatusList().iterator();
//		while(iterator.hasNext()){
//			DashboardBean dashboardbean = (DashboardBean)iterator.next() ;
//			System.out.println("1= "+dashboardbean.getColName());
//			System.out.println("2= "+dashboardbean.getCount());
//			System.out.println("3= "+dashboardbean.getRevenue());
//			System.out.println("4= "+dashboardbean.getAveVGPM());
//			System.out.println("");
//		}
		
		return (mapping.findForward("success")); // - return to RPT/RPTPerformanceDashboard.jsp
	}

}
