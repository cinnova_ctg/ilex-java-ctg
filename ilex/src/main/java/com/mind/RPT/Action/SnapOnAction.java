package com.mind.RPT.Action;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.xml.sax.SAXParseException;

import com.mind.RPT.common.UploadXML;
import com.mind.RPT.common.UtilityMethods;
import com.mind.RPT.dao.SnapOndao;
import com.mind.RPT.formbean.SnapOnForm;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;

public class SnapOnAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SnapOnAction.class);

	/*
	 * Discription: This mehtod is used to upload a XML file and send ot the
	 * XMLParser. XMLParser rerurn list of parsed data. Exception: This mehtod
	 * handled two type of Exception 1. FileNotFoundException - When file is not
	 * found for upload. 2. SAXParseException - When XML is not in proper
	 * format.
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired
		String userid = (String) session.getAttribute("userid"); // - Login user
																	// id
		if (!Authenticationdao.getPageSecurity(userid, "Support",
				"Reschedule Report", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Page Security End */

		SnapOnForm snapOnForm = (SnapOnForm) form;

		/* Upload and Parse File: Start */
		if (snapOnForm.getUpload() != null) {
			/**
			 * First check uploaded file is .xml file or not. Set path where
			 * uploaded file will be store. Pass this path into file object.
			 * Call WriteFile method with this path for write file into
			 * application. Make a object of File and load uploaded file. Send
			 * this file to XMLParser. XMLParser return list of parse data. this
			 * list pass to an ArraryList. Delete Uploaded file from
			 * application. This ArrayList data is pass ot dao class for Store
			 * this list into database. Store Uploaded summay.
			 */

			/* Check upladed file has .xml ext */
			if (!UtilityMethods.checkFile(snapOnForm.getFilename()
					.getFileName())) {
				request.setAttribute("Exception", "1000");
				return mapping.findForward("success"); // - /RPT/SnapOn.jsp
			}

			/* Set path to save uploaded file */
			String path = Util.getTemp() + "/"
					+ snapOnForm.getFilename().getFileName();

			ArrayList list = new ArrayList(); // - to store parsed xml data for
												// save into database.
			File file = new File(path);

			try {
				UtilityMethods.writeFile(snapOnForm.getFilename(), path); // -
																			// For
																			// Upload
																			// a
																			// file
				list = UploadXML.getXMLData(file); // - For Parse(DOM Parser)
													// XML file
				// list = com.mind.RPT.common.XMLReader.xmlReader(path); // -
				// For Parse(SAX Parser) XML file

				/* When file not found */
			} catch (FileNotFoundException e) {
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				request.setAttribute("Exception", "1000");

				/* When XML is not in proper format */
			} catch (SAXParseException err) {
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						err);

				String errMessage = "Parsing error at line "
						+ err.getLineNumber() + ". " + err.getMessage();
				request.setAttribute("Exception", "1001");
				request.setAttribute("errMessage", errMessage); // - Messgae for
																// unparsed xml
																// file

				/* Other Exception */
			} catch (Exception e) {
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Exception in com.mind.RPT.Action.SnapOnAction.execute() "
							+ e);
				}

				/* Delete uploaded file */
			} finally {
				file.delete();
			}

			/*
			 * Call setSnapOn for save parsed data to database and collect
			 * summary of uploaded data
			 */
			snapOnForm.setInfo(SnapOndao.setSnapOn(list,
					getDataSource(request, "ilexnewDB")));
		}
		/* Upload and Parse File: End */

		return mapping.findForward("success"); // - /RPT/SnapOn.jsp
	}

}
