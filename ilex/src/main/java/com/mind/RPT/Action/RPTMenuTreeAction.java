package com.mind.RPT.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Class Description: This class is used to open Left Tree Menu of RPT module.
 * Methods:	execute
 * @author ILEX
 *
 */
public class RPTMenuTreeAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)	throws Exception {
		
		return (mapping.findForward("rptmainpage")); // - return to RPT/Left.jsp
	}
	

}
