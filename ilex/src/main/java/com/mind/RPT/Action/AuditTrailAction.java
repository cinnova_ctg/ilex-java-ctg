package com.mind.RPT.Action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.dao.AuditTrailDao;
import com.mind.RPT.formbean.AuditTrailForm;


public class AuditTrailAction extends com.mind.common.IlexAction{

	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   // - Check for session expired
		String userid = (String)session.getAttribute("userid"); // - Login user id	
		/*if( !Authenticationdao.getPageSecurity( userid , "AuditTrail" , "Audit Trail Reports" , getDataSource( request , "ilexnewDb" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}*/
		
		AuditTrailForm auditTrailForm = ( AuditTrailForm )form;
		
		
		ArrayList list = AuditTrailDao.getTableData(getDataSource( request , "ilexnewDB" ));
		auditTrailForm.setAuditTrailList(list);
		request.setAttribute("SelectTable", list);
		
		ArrayList list1 = AuditTrailDao.getTableDataField(auditTrailForm.getTn(),getDataSource( request , "ilexnewDB" ));
		auditTrailForm.setAuditTrailList1(list1);
		request.setAttribute("Field", list1);
		
		ArrayList	listOperation = AuditTrailDao.getTableDataOperation(getDataSource( request , "ilexnewDB" ));
		auditTrailForm.setAuditTrailListOperation(listOperation);
		request.setAttribute("Operation", listOperation);
		
		ArrayList	listUser = AuditTrailDao.getUserNames(getDataSource( request , "ilexnewDB" ));
		auditTrailForm.setAuditTrailListUser(listUser);
		request.setAttribute("users", listUser);
		
		if(request.getParameter("submitButton")!=null){			
		ArrayList slist = AuditTrailDao.getData(auditTrailForm.getTn(),auditTrailForm.getOper(),auditTrailForm.getFn(),auditTrailForm.getUsers(),getDataSource( request , "ilexnewDB" ));
		auditTrailForm.setSearchData(slist);
		request.setAttribute("slist", slist);
		request.setAttribute("auditList",auditTrailForm.getSearchData().size());
		
		ArrayList Flist = AuditTrailDao.getFieldData(auditTrailForm.getTn(),auditTrailForm.getOper(),auditTrailForm.getFn(),auditTrailForm.getUsers(),auditTrailForm.getAuditIDS(),getDataSource( request , "ilexnewDB" ));		
		auditTrailForm.setFieldData(Flist);
		request.setAttribute("Flist", Flist);		
		}		
		request.setAttribute("auditTraillist",auditTrailForm.getAuditTrailList().size());			
		return mapping.findForward( "success" ); 
	}
	
	
		
		
		
		
	
}
