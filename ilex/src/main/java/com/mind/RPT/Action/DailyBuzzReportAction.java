package com.mind.RPT.Action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.dao.DailyBuzzReportdao;
import com.mind.RPT.dao.RPTdao;
import com.mind.RPT.formbean.DailyBuzzReportForm;
import com.mind.bean.rpt.JobOwnerSummaryBean;
import com.mind.common.dao.Authenticationdao;

public class DailyBuzzReportAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired
		String userid = (String) session.getAttribute("userid"); // - Login user
																	// id
		if (!Authenticationdao.getPageSecurity(userid, "Support",
				"Reschedule Report", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		/* Undefined change which could not be traced to SPR. */
		/* Page Security End */
		// if(!(null!=session.getAttribute("requestFrom")))
		// {
		// String parameter=request.getParameter("parm");
		// if(null!=parameter)
		// {
		// System.out.println(parameter);
		// session.setAttribute("requestFrom", parameter);
		// }
		// else
		// {
		// session.setAttribute("requestFrom", "");
		// }
		// }
		DailyBuzzReportForm dailyBuzzReport = (DailyBuzzReportForm) form;

		/* Set default range */
		if (dailyBuzzReport.getDate() == null) {
			dailyBuzzReport.setDate(RPTdao.getcurrDate(getDataSource(request,
					"ilexnewDB")));
			dailyBuzzReport.setAppendixName("0");
			dailyBuzzReport.setJobOwner("0");
			dailyBuzzReport.setSrPrjManager("0");
		}

		/* Get Schedule Job List */
		dailyBuzzReport.setSchList(DailyBuzzReportdao.getSchJobList(
				dailyBuzzReport.getJobOwner(),
				dailyBuzzReport.getAppendixName(), dailyBuzzReport.getDate(),
				dailyBuzzReport.getSrPrjManager(),
				getDataSource(request, "ilexnewDB")));

		/* Get Confirm Job List */
		dailyBuzzReport.setCnfList(DailyBuzzReportdao.getCnfJobList(
				dailyBuzzReport.getJobOwner(),
				dailyBuzzReport.getAppendixName(), dailyBuzzReport.getDate(),
				dailyBuzzReport.getSrPrjManager(),
				getDataSource(request, "ilexnewDB")));

		/* Get Onsite Job List */
		dailyBuzzReport.setOnsiteList(DailyBuzzReportdao.getOnsiteJobList(
				dailyBuzzReport.getJobOwner(),
				dailyBuzzReport.getAppendixName(), dailyBuzzReport.getDate(),
				dailyBuzzReport.getSrPrjManager(),
				getDataSource(request, "ilexnewDB")));

		/* Get Offsite Job List */
		dailyBuzzReport.setOffsiteList(DailyBuzzReportdao.getOffsiteJobList(
				dailyBuzzReport.getJobOwner(),
				dailyBuzzReport.getAppendixName(), dailyBuzzReport.getDate(),
				dailyBuzzReport.getSrPrjManager(),
				getDataSource(request, "ilexnewDB")));

		/* For Project ComboBox: Start */
		ArrayList msaList = RPTdao.getMsaList(getDataSource(request,
				"ilexnewDB")); // - get msa list
		if (msaList.size() > 0) {
			for (int i = 0; i < msaList.size(); i++) { // - group Appendix title
														// on the basis of MSA
														// title
				String tempid = ((JobOwnerSummaryBean) msaList.get(i))
						.getMsaId();
				// - get all appnedix for every msa
				((JobOwnerSummaryBean) msaList.get(i)).setAppendixList(RPTdao
						.getAppendixTitleList(tempid, i,
								getDataSource(request, "ilexnewDB")));
			}// - end of for loop
		}
		dailyBuzzReport.setMsaList(msaList);
		/* For Project ComboBox: End */

		dailyBuzzReport.setJobOwnerList(RPTdao.getJobOwnerList(getDataSource(
				request, "ilexnewDB"))); // - get All job owner list
		// get srProjectManagerList
		dailyBuzzReport.setSrPrjManagerList(RPTdao
				.getSrPrjManagerList(getDataSource(request, "ilexnewDB")));

		return mapping.findForward("success"); // - DailyBuzzReport.jsp
	}

}
