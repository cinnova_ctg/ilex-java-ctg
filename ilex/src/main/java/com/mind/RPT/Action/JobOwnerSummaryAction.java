package com.mind.RPT.Action;

import java.util.ArrayList;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.common.ChangeDateFormat;
import com.mind.RPT.dao.RPTdao;
import com.mind.RPT.formbean.JobOwnerSummaryForm;
import com.mind.bean.rpt.JobOwnerSummaryBean;
import com.mind.common.EnvironmentSelector;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.ViewList;

/**
 * Discription: This class is use to show all tab data of Job Owner Summary.
 * 
 * @author Vijay Kumar Singh
 * 
 */
public class JobOwnerSummaryAction extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired
		String userId = (String) session.getAttribute("userid"); // - Login user
																	// id
		if (!Authenticationdao.getPageSecurity(userId, "Management",
				"Job Owner Summary", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Page Security End */
		JobOwnerSummaryForm jobSummaryForm = (JobOwnerSummaryForm) form;

		/* Undefined change which could not be traced to SPR. */

		// if(!(null!=session.getAttribute("comesFrom")))
		// {
		// String parameter=request.getParameter("parm");
		// if(null!=parameter)
		// {
		// System.out.println(parameter);
		// session.setAttribute("comesFrom", parameter);
		// }
		// else
		// {
		// session.setAttribute("comesFrom", "");
		// }
		// }
		if (jobSummaryForm.getTabName() != null) {
			request.setAttribute("tabname", jobSummaryForm.getTabName()); // -
																			// Tab
																			// Name
		} else {
			request.setAttribute("tabname", "scheduled");
			jobSummaryForm.setTabName("scheduled");
		}
		if (jobSummaryForm.getJobOwner() == null) { // - for default view
			/* set JobOwner to login user id */
			jobSummaryForm.setJobOwner(RPTdao.getActiovOwnerId(userId,
					getDataSource(request, "ilexnewDB")));
		}
		if (jobSummaryForm.getAppendixName() == null) { // - for default view
			jobSummaryForm.setAppendixName("0");
		}
		if (jobSummaryForm.getToDate() == null) { // - for default view
			jobSummaryForm.setToDate("");
		}
		if (jobSummaryForm.getFromDate() == null) { // - for default view
			jobSummaryForm.setFromDate("");
		}

		/* Get List of Project Team */
		jobSummaryForm.setProjectTeamList(RPTdao.getProjectTeam(getDataSource(
				request, "ilexnewDB")));

		/* Get List of Scheduler */
		jobSummaryForm.setSchedulerList(RPTdao.getScheduler(getDataSource(
				request, "ilexnewDB")));

		/* Scheduled Tab: Start */
		if (jobSummaryForm.getTabName().equals("scheduled")) {
			jobSummaryForm.setTabInfo(RPTdao.getJobSummaryScheduledTab(
					jobSummaryForm.getJobOwner(),
					jobSummaryForm.getAppendixName(),
					jobSummaryForm.getFromDate(), jobSummaryForm.getToDate(),
					getDataSource(request, "ilexnewDB")));
		}// - end of scheduled if block
		/* Scheduled Tab: End */

		/* Confirmed Tab: Start */
		else if (jobSummaryForm.getTabName().equals("confirmed")) {
			jobSummaryForm.setTabInfo(RPTdao.getJobSummaryConfirmedTab(
					jobSummaryForm.getJobOwner(),
					jobSummaryForm.getAppendixName(),
					jobSummaryForm.getFromDate(), jobSummaryForm.getToDate(),
					getDataSource(request, "ilexnewDB")));
		}
		/* Confirmed Tab: End */

		/* Onsite Tab: Start */
		else if (jobSummaryForm.getTabName().equals("onsite")) {
			jobSummaryForm.setTabInfo(RPTdao.getJobSummaryOnsiteTab(
					jobSummaryForm.getJobOwner(),
					jobSummaryForm.getAppendixName(),
					jobSummaryForm.getFromDate(), jobSummaryForm.getToDate(),
					getDataSource(request, "ilexnewDB")));
		}// - end of onsite if block
		/* Onsite Tab: End */

		/* Offsite Tab: Start */
		else if (jobSummaryForm.getTabName().equals("offsite")) {
			jobSummaryForm.setTabInfo(RPTdao.getJobSummaryOffsiteTab(
					jobSummaryForm.getJobOwner(),
					jobSummaryForm.getAppendixName(),
					jobSummaryForm.getFromDate(), jobSummaryForm.getToDate(),
					getDataSource(request, "ilexnewDB")));
		}
		/* Offsite Tab: End */

		/* Complete Tab: Start */
		else if (jobSummaryForm.getTabName().equals("complete")) {
			jobSummaryForm.setTabInfo(RPTdao.getJobSummaryCompleteTab(
					jobSummaryForm.getJobOwner(),
					jobSummaryForm.getAppendixName(),
					jobSummaryForm.getFromDate(), jobSummaryForm.getToDate(),
					getDataSource(request, "ilexnewDB")));

		}// - end of complete if block
		/* Offsite Tab: End */

		/* Notschedule Tab: Start */
		else if (jobSummaryForm.getTabName().equals("notscheduled")) {
			jobSummaryForm.setTabInfo(RPTdao.getJobSummaryNotscheduledTab(
					jobSummaryForm.getJobOwner(),
					jobSummaryForm.getAppendixName(),
					jobSummaryForm.getFromDate(), jobSummaryForm.getToDate(),
					getDataSource(request, "ilexnewDB")));
		}// - end of notscheduled if block
		/* Notschedule Tab: End */

		/* CCC Tab: Start */
		else if (jobSummaryForm.getTabName().equals("ccc")) {
			if (jobSummaryForm.getCccToDate().equals("")
					|| jobSummaryForm.getCccFromDate().equals("")) {
				jobSummaryForm.setCccToDate(RPTdao.getToFromDate("toDate",
						getDataSource(request, "ilexnewDB")));
				jobSummaryForm.setCccFromDate(RPTdao.getToFromDate("fromDate",
						getDataSource(request, "ilexnewDB")));
			}

			String url = ""; // - Url of MY SQL for database connection
			// url = this.getResources( request ).getMessage(
			// "appendix.detail.changestatus.mysql.url" );
			url = EnvironmentSelector
					.getBundleString("appendix.detail.changestatus.mysql.url");
			jobSummaryForm.setTabInfo(RPTdao.getCCCTabInfo(url,
					jobSummaryForm.getJobOwner(),
					jobSummaryForm.getCccToDate(),
					jobSummaryForm.getCccFromDate(),
					jobSummaryForm.getAppendixName(),
					getDataSource(request, "ilexnewDB")));
		}// - end of ccc if block
		/* CCC Tab: End */

		/* calendar Tab: Start */
		else if (jobSummaryForm.getTabName().equals("calendar")) {
			String calFromDate = "";
			if (jobSummaryForm.getCalFromDate() == null
					|| jobSummaryForm.getCalFromDate().equals("")) {
				calFromDate = RPTdao.getcurrMonYear(getDataSource(request,
						"ilexnewDB"));
			} else {
				calFromDate = jobSummaryForm.getCalFromDate();
			}

			String[] dates = RPTdao.getDates(calFromDate, "days",
					getDataSource(request, "ilexnewDB")); // - get days range
			String[] daterange = RPTdao.getDateRange(calFromDate,
					getDataSource(request, "ilexnewDB")); // - get date range
			TreeMap calendardata = RPTdao.getcalendarData(calFromDate,
					getDataSource(request, "ilexnewDB")); // - get calendar info

			jobSummaryForm.setCalFromDate(daterange[0]);
			jobSummaryForm.setCalToDate(daterange[daterange.length - 1]);

			/* get data of all current calendar dates. */
			ArrayList list = RPTdao.getDayData(
					jobSummaryForm.getAppendixName(),
					jobSummaryForm.getJobOwner(),
					jobSummaryForm.getCalFromDate(),
					jobSummaryForm.getCalToDate(),
					jobSummaryForm.getSrProjectManagersId(),
					getDataSource(request, "ilexnewDB"));

			jobSummaryForm.setCurrMMYY(ChangeDateFormat.getDateFormat(
					jobSummaryForm.getCalFromDate(), "MMMM yy", "MM/dd/yyyy")); // -
																				// get
																				// current
																				// month
																				// and
																				// current
																				// year

			/* Set Sr Project Manager */
			ViewList vlist = new ViewList();
			jobSummaryForm.setSrProjectManagers(vlist.getProjectManagerPOC(
					"srProjectManager", getDataSource(request, "ilexnewDB")));

			/* Set Arrribute for calendar view */
			request.setAttribute("calendardata", calendardata);
			request.setAttribute("dates", dates);
			request.setAttribute("daterange", daterange);
			request.setAttribute("list", list);
		}
		/* calendar Tab: End */

		jobSummaryForm.setJobOwnerList(RPTdao.getJobOwnerList(getDataSource(
				request, "ilexnewDB"))); // - get All job owner list
		ArrayList msaList = RPTdao.getMsaList(getDataSource(request,
				"ilexnewDB")); // - get msa list

		/* For Project ComboBox: Start */
		if (msaList.size() > 0) {
			for (int i = 0; i < msaList.size(); i++) { // - group Appendix title
														// on the basis of MSA
														// title
				String tempid = ((JobOwnerSummaryBean) msaList.get(i))
						.getMsaId();
				// - get all appnedix for every msa
				((JobOwnerSummaryBean) msaList.get(i)).setAppendixList(RPTdao
						.getAppendixTitleList(tempid, i,
								getDataSource(request, "ilexnewDB")));
			}// - end of for loop
		}
		jobSummaryForm.setMsaList(msaList);
		/* For Project ComboBox: Start */

		if (!jobSummaryForm.getTabName().equals("calendar")) {
			request.setAttribute("tabInfoSize", jobSummaryForm.getTabInfo()
					.size());
		}

		return mapping.findForward("success"); // - return to
												// RPT/JobOwnerSummary.jsp
	}

}
