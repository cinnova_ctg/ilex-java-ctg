package com.mind.RPT.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.common.CreateEmail;
import com.mind.RPT.dao.DailySnaponReportdao;
import com.mind.RPT.formbean.SnaponCallForm;
import com.mind.bean.rpt.SnaponCallBean;
import com.mind.common.dao.Authenticationdao;

/**
 * Discription: This class is used to manage Snap-on Pre-call Tracking - Daily
 * Report.
 * 
 * @author MIND
 */
public class SnaponCallAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SnaponCallAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* Page Security Start */
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired
		String userid = (String) session.getAttribute("userid"); // - Login user
																	// id
		if (!Authenticationdao.getPageSecurity(userid, "Support",
				"Reschedule Report", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Page Security End */

		SnaponCallForm snaponCallForm = (SnaponCallForm) form;
		SnaponCallBean snaponCallBean = new SnaponCallBean();

		/* Add/Update: Start */
		if (snaponCallForm.getSave() != null) {

			BeanUtils.copyProperties(snaponCallBean, snaponCallForm);

			int flag = DailySnaponReportdao.setSnaponCall(snaponCallBean,
					getDataSource(request, "ilexnewDB"));
			BeanUtils.copyProperties(snaponCallForm, snaponCallBean);
			if (flag > -1) {
				request.setAttribute("date", snaponCallForm.getDate());
				// Declare variable for store the boolean value Call1 Confermed
				// Status
				if (logger.isDebugEnabled()) {
					logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in form");
				}
				if (DailySnaponReportdao.isCall1Status(
						snaponCallForm.getJobId(),
						getDataSource(request, "ilexnewDB"))) {
					// System.out.println(" check PC........");
					String[] checkPOCMAILID = CreateEmail.getMailData(userid,
							snaponCallForm.getJobId(),
							getDataSource(request, "ilexnewDB"));
					if (checkPOCMAILID[3] != null
							&& !checkPOCMAILID[3].equals("")) {
						// System.out.println("Email check........");
						CreateEmail.CallConfirmCreationMail(userid,
								snaponCallForm.getJobId(),
								snaponCallForm.getAppendixId(),
								getDataSource(request, "ilexnewDB"));
					} else {
						request.setAttribute("POCMailId", "notExist");
					}
				}
				return mapping.findForward("dailySnaponReport"); // -
																	// /RPT/DailySnaponReport.jsp
			} // - End of Inner If
		} // - End of If
		/* Add/Update: End */

		/* Set Request Parameters */
		if (request.getParameter("jobid") != null)
			snaponCallForm.setJobId(request.getParameter("jobid").toString());

		if (request.getParameter("appendixid") != null)
			snaponCallForm.setAppendixId(request.getParameter("appendixid")
					.toString());

		if (request.getParameter("date") != null)
			snaponCallForm.setDate(request.getParameter("date").toString());

		if (request.getParameter("from") != null)
			snaponCallForm.setFrom(request.getParameter("from").toString());

		/* Fill Combo Boxes */
		snaponCallForm.setDay5Call1(DailySnaponReportdao.getSnCallStatus(
				"5 Day", getDataSource(request, "ilexnewDB")));
		snaponCallForm.setDay5Call2(DailySnaponReportdao.getSnCallStatus(
				"5 Day%", getDataSource(request, "ilexnewDB")));
		snaponCallForm.setHour24Call(DailySnaponReportdao.getSnCallStatus(
				"24 Hour", getDataSource(request, "ilexnewDB")));
		BeanUtils.copyProperties(snaponCallBean, snaponCallForm);
		DailySnaponReportdao.getSnaponCallInfo(snaponCallBean,
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(snaponCallForm, snaponCallBean);

		request.setAttribute("occurance", snaponCallForm.getOccurance()); // -
																			// 1T/2T/3T
																			// -
																			// for
																			// Identify
																			// Occurance
																			// No.

		/* For disable Call 2 Combo box */
		request.setAttribute("call1Status", snaponCallForm.getCall1Status());
		request.setAttribute("call12Status", snaponCallForm.getCall12Status());

		return mapping.findForward("success"); // - RPT/SnaponCall.jsp
	} // - End of execute() mthod

} // - End of Class
