package com.mind.RPT.Action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.dao.RPTdao;
import com.mind.RPT.formbean.SalesSummaryForm;
import com.mind.common.dao.Authenticationdao;

/**
 * Class Discription: This class is used to get State Sales Summary from Database on the basis of given Date range.
 * Methods : execute
 * @author ILEX
 *
 */
public class SalesSummaryAction extends com.mind.common.IlexAction{

	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   // - Check for session expired
		String userid = (String)session.getAttribute("userid"); // - Login user id
		if( !Authenticationdao.getPageSecurity( userid , "Accounting" , "View SalesSummary" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		/* Page Security End*/
		
		SalesSummaryForm salesSummaryform = ( SalesSummaryForm )form;
		ArrayList list = new ArrayList();
		
		if(salesSummaryform.getToDate()==null && salesSummaryform.getFromDate()==null){					// - to get the current quarter
			String[] currentQuarter = RPTdao.getCurrentQuarter(getDataSource( request , "ilexnewDB" )); // - get current quarter and current year
			
			switch(Integer.parseInt(currentQuarter[0])){ 	// - Start Switch: to set current quarter date range.
				case 1:
					salesSummaryform.setToDate("01/01/"+currentQuarter[1]);  // - currentQuarter[1] is current year
					salesSummaryform.setFromDate("03/31/"+currentQuarter[1]);
					break;
				case 2:
					salesSummaryform.setToDate("04/01/"+currentQuarter[1]);
					salesSummaryform.setFromDate("06/30/"+currentQuarter[1]);
					break;
				case 3:
					salesSummaryform.setToDate("07/01/"+currentQuarter[1]);
					salesSummaryform.setFromDate("9/30/"+currentQuarter[1]);
					break;
				case 4:
					salesSummaryform.setToDate("10/01/"+currentQuarter[1]);
					salesSummaryform.setFromDate("12/31/"+currentQuarter[1]);
					break;
				default:
			}
				
		}// End Switch : to set current quarter date range.
		list = RPTdao.getSalesSummary(salesSummaryform.getToDate(),salesSummaryform.getFromDate(),getDataSource( request , "ilexnewDB" )); // - get List of Sales of Summary
		salesSummaryform.setSalesSummarylist(list);// - get List of Sales of Summary
		request.setAttribute("summaySize",salesSummaryform.getSalesSummarylist().size()); // - set size of Sale Summary List
		
		return mapping.findForward( "success" ); // - return to RPT/SalesSummary.jsp
	}
	
}
