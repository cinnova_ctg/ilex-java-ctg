package com.mind.RPT.Action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.dao.RPTdao;
import com.mind.RPT.formbean.SupportAppendixForm;
import com.mind.common.dao.Authenticationdao;

/**
 * Discription: This class is use to generate list for IBM Corporation -> Appendix: Appendix 7 - Qwest CPE Installation Project.
 * 				This list is come from live database only. 
 * @author vijaykumarsingh
 *
 */
public class SupportAppendixListAction extends com.mind.common.IlexAction{

	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   // - Check for session expired
		String userid = (String)session.getAttribute("userid"); // - Login user id
		if( !Authenticationdao.getPageSecurity( userid , "Support" , "IBM Corp Report" , getDataSource( request , "ilexnewDB" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		/* Page Security End*/
		
		SupportAppendixForm supportappendix = ( SupportAppendixForm )form;
		
		/* List for IBM Corporation -> Appendix: Appendix 7 - Qwest CPE Installation Project*/
		ArrayList list = RPTdao.getSupportAppendixList(supportappendix.getFromDate(),supportappendix.getToDate(),"826");
		supportappendix.setSupportAppendixlist(list);
		
		request.setAttribute("supportAppedixList",supportappendix.getSupportAppendixlist().size());
		
		return mapping.findForward( "success" ); // - SupportAppendixList.jsp
	}
}
