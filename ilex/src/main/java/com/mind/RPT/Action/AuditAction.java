package com.mind.RPT.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.RPT.formbean.AuditTrailForm;


public class AuditAction extends com.mind.common.IlexAction{

	public ActionForward execute(ActionMapping mapping,	ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
	{
		 
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if(session.getAttribute("userid") == null) return( mapping.findForward("SessionExpire"));   // - Check for session expired
		String userid = (String)session.getAttribute("userid"); // - Login user id	
		/*if( !Authenticationdao.getPageSecurity( userid , "AuditTrail" , "Audit Trail Reports" , getDataSource( request , "ilexnewDb" ) ) ) 
		{
			return( mapping.findForward( "UnAuthenticate" ) );	
		}*/
		
		AuditTrailForm auditTrailForm = ( AuditTrailForm )form;
		
		//ArrayList list1 = AuditTrailDao.getTableDataField(auditTrailForm.getTn(),getDataSource( request , "ilexnewDB" ));
		//auditTrailForm.setAuditTrailList1(list1);
		//request.setAttribute("xyz", list1);		
		
		//request.setAttribute("auditTraillists",auditTrailForm.getAuditTrailList1().size());			
		return mapping.findForward( "audit" ); 
	}
	
	
		
		
		
		
	
}
