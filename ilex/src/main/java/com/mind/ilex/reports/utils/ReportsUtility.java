/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: <This is a utility file.>
 *
 */

package com.mind.ilex.reports.utils;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * Class ReportsUtility - Utility class contains methods for various uilities.
 * methods - containsData
 **/
public class ReportsUtility {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ReportsUtility.class);

	/**
	 * containsData - This method checks whether the time for running the
	 * scheduler comes or not.
	 * 
	 * @param toHaveRunTime
	 *            - String in which checking is done..
	 * @param nowTime
	 *            - String to be checked.
	 * @return Returns true or false.
	 **/

	public static final String RESOURCE_FILE_NAME_KEY = "com.mind.properties.ApplicationResourcesDocM";

	public static final String RESOURCE_THREAD_KEY = "report.thread.start";

	public static final String THREAD_SLEEPTIME_KEY = "report.scheduler.sleep_time_in_hours";

	public static boolean containsData(String toHaveRunTime, String nowTime) {
		logger.trace("Start :(Class : com.mind.ilex.reports.utils.ReportsUtility |"
				+ " Method : containsData).");

		// checks whether the run time for scheduler is achevied or not

		int toHaveRunValue = 0;
		int nowTimeValue = 0;
		try {
			toHaveRunValue = Integer.parseInt(toHaveRunTime.trim());
			nowTimeValue = Integer.parseInt(nowTime);
			if (toHaveRunValue <= nowTimeValue) {
				// this was supposed to be run at a previous time
				return true;
			}
		} catch (Exception e) {
			logger.error("containsData(String, String)", e);

			if (toHaveRunTime.trim().equalsIgnoreCase(nowTime)) {

				return true;
			}
		}

		logger.trace("End :(Class : com.mind.ilex.reports.utils.ReportsUtility | "
				+ "Method : containsData).");
		return false;
	}

	/**
	 * getResultSetAsList - This method returns the resultset as arraylist.
	 * 
	 * @param ResultSet
	 *            - ResultSet.
	 * @return Returns List.
	 **/
	public static List getResultSetAsList(ResultSet rs) {
		logger.trace("Start :(Class : com.mind.ilex.reports.utils.ReportsUtility | "
				+ "Method : getResultSetAsList).");
		logger.trace("Takes the resultSet as parameter and put it into an arrayList.");
		List list = null;
		try {
			list = new ArrayList();
			int columnCount = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				Map map = new HashMap();
				for (int i = 1; i <= columnCount; i++) {
					map.put(rs.getMetaData().getColumnName(i), rs.getString(i));

				}
				list.add(map);
			}
		} catch (Exception e) {
			logger.error("getResultSetAsList(ResultSet)", e);

			logger.error("getResultSetAsList(ResultSet)", e);
		}
		logger.trace("End :(Class : com.mind.ilex.reports.utils.ReportsUtility | "
				+ "Method : getResultSetAsList).");
		return list;
	}

	public static boolean containsData(String toHaveRunDate, String nowDate,
			String toHaveRunTime, String nowTime) {
		logger.trace("Start :(Class : com.mind.ilex.reports.utils.ReportsUtility |"
				+ " Method : containsData).");

		// checks whether the run time for scheduler is achevied or not

		int toHaveRunDateValue = 0;
		int nowDateValue = 0;
		int toHaveRunTimeValue = 0;
		int nowTimeValue = 0;
		try {
			toHaveRunDateValue = Integer.parseInt(toHaveRunDate.trim());
			nowDateValue = Integer.parseInt(nowDate);
			toHaveRunTimeValue = Integer.parseInt(toHaveRunTime.trim());
			nowTimeValue = Integer.parseInt(nowTime);

			if (toHaveRunDateValue < nowDateValue) {
				// this was supposed to be run at a previous time
				return true;
			}
			if (toHaveRunDateValue == nowDateValue) {
				// this was supposed to be run at a previous time
				if (toHaveRunTimeValue == nowTimeValue) {
					// this was supposed to be run at a previous time
					return true;
				}
			}

		} catch (Exception e) {
			logger.error("containsData(String, String, String, String)", e);

			if (toHaveRunTime.trim().equalsIgnoreCase(nowTime)) {

				return true;
			}
		}

		logger.trace("End :(Class : com.mind.ilex.reports.utils.ReportsUtility | "
				+ "Method : containsData).");
		return false;
	}

}
