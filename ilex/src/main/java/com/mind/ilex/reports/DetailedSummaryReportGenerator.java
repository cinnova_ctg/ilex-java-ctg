//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : DetailedSummaryReportGenerator.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.ilex.reports;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.mind.bean.reports.ReportFilterCriteria;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.dao.MSA;
import com.mind.docm.dao.MSAList;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.DocumentIdNotUpdatedException;
import com.mind.docm.exceptions.EntityDetailsNotFoundException;
import com.mind.docm.exceptions.NotAbleToInsertDocument;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.ilex.reports.reportDao.ReportFilterCriteriaDao;
import com.mind.ilex.reports.reportDao.ReportsDao;
import com.mind.ilex.reports.reportDao.SubmitDocuments;
import com.mind.report.ViewDocument;

/**
 * Class DetailedSummaryReportGenerator - This class is the reportgeneration
 * class for DetailedSummaryReportGenerator. methods runReport, getMSAList,
 * runReportForMSA
 **/

public class DetailedSummaryReportGenerator extends ReportGenerator {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DetailedSummaryReportGenerator.class);

	/**
	 * @name runReport
	 * @purpose This method is used to run the report for the Detailed Summary
	 *          Report For NetMedX (for all the MSAs) typically run 5th of every
	 *          month or based on the properties set into the filter criteria of
	 *          the report.
	 * @steps 1. Report Scheduler typically requests to run this report -
	 *        Detailed Summary Report For NetMedX (for all the MSAs). 2. The
	 *        Report Generator gets the list of all the MSAs. 3. It then runs
	 *        the report for each of these MSAs along with the report filter
	 *        critera set for the report. 4. For each MSa, the report object
	 *        corresponding to the IlexEntity-Report is created. 5. The Report's
	 *        PDf file is generated. 6. Then the EntitiManager is commanded to
	 *        approve the report hence generated. 7. Further process is
	 *        available within the EntitiManager's approveEntity operation.
	 * @param none
	 * @return none
	 * @returnDescription none
	 */
	public Map msaResultMap = new TreeMap();
	public List innerList = null;
	public List outerList = new ArrayList();

	public void runReport(String reportType,
			ReportFilterCriteria reportDateFilterCriteria,
			ServletContext context) {
		MSAList msaList = null;
		msaList = getMSAList();
		msaList = ReportFilterCriteriaDao.getFilteredMSAList(msaList,
				reportType, reportDateFilterCriteria);
		runReport(msaList, reportType, reportDateFilterCriteria, context, null);
	}

	public void runReport(com.mind.docm.dao.MSAList msaList, String reportType,
			ReportFilterCriteria reportDateFilterCriteria,
			ServletContext context, int manual, String userCode) {
		// create ReportFilterCriteria reportDateFilterCriteria
		runReport(msaList, reportType, reportDateFilterCriteria, context,
				userCode);
	}

	public void runReport(com.mind.docm.dao.MSAList msaList, String reportType,
			ReportFilterCriteria reportDateFilterCriteria,
			ServletContext context, String userCode) {
		logger.trace("Start:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator | "
				+ "Method : runReport)");
		SubmitDocuments sd = new SubmitDocuments();
		long startTime = 0;
		long executionTime = 0;
		logger.trace("Call to get the MSA List.");
		startTime = System.currentTimeMillis();
		runReport(msaList, reportDateFilterCriteria, context, userCode);
		executionTime = System.currentTimeMillis() - startTime;
		sd.updateReportScheduleMain(reportDateFilterCriteria, executionTime);
		logger.trace("End:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator | "
				+ "Method : runReport).");
	}

	/**
	 * getMSAList - This method fethes data for MSA.
	 * 
	 * @return Returns list of msa's.
	 **/
	public com.mind.docm.dao.MSAList getMSAList() {
		logger.trace("Start:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator | "
				+ "Method : getMSAList)");
		MSAList msaList = new MSAList();
		ReportsDao rpDao = new ReportsDao();
		int length = rpDao.getMSAlist().size();
		MSA[] msaArray = new MSA[length];
		for (int i = 0; i < length; i++) {
			// sets up the MSA object
			msaArray[i] = (MSA) rpDao.getMSAlist().get(i);
		}
		// sets up the MSAList object
		msaList.setMsaArray(msaArray);
		logger.trace("End:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator | "
				+ "Method : getMSAList)");
		return msaList;
	}

	/**
	 * runReport - Overloaded method for the above run report which is used for
	 * the report generation for the indivdual MSA>
	 * 
	 * @param MSAList
	 *            - Takes the MSA list
	 * @param reportFilterCriteria
	 *            - Report filter criteria for the report.
	 * @return Returns none.
	 **/
	public void runReport(com.mind.docm.dao.MSAList msaList,
			ReportFilterCriteria reportFilterCriteria, ServletContext context,
			String userCode) {
		logger.trace("Start:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator | "
				+ "Method : runReport)");
		Connection conn = ReportsDao.getStaticConnection();
		SubmitDocuments sd = new SubmitDocuments();
		// inserts the information for the reports generated.
		logger.trace("Inserts the information related to the generated report.");
		try {
			String reportId = sd.submitReportInformation(reportFilterCriteria,
					conn);
			if (reportId != null && !reportId.trim().equalsIgnoreCase("")) {
				for (int i = 0; i < msaList.getMsaArray().length; i++) {
					// updates the information for the reports generated.
					try {
						innerList = new ArrayList();
						logger.trace("Updates the information related to the generated report.");
						sd.updateReportInformation(reportId,
								msaList.getMsaArray()[i], reportFilterCriteria,
								i, conn);
						runReportForMSA(reportId, msaList.getMsaArray()[i],
								reportFilterCriteria, context, conn, userCode);

					} catch (NotAbleToInsertDocument e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					} catch (ControlledTypeException e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					} catch (DocMFormatException e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					} catch (CouldNotAddToRepositoryException e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					} catch (CouldNotReplaceException e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					} catch (CouldNotCheckDocumentException e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					} catch (EntityDetailsNotFoundException e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					} catch (DocumentIdNotUpdatedException e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					} catch (SQLException e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
					} catch (Exception e) {
						logger.error(
								"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
								e);

						innerList.add(msaList.getMsaArray()[i].getEntityId()
								+ "-" + msaList.getMsaArray()[i]);
						innerList.add(e.getMessage());
						innerList.add(new Integer(0));
						outerList.add(innerList);
						msaResultMap.put(
								msaList.getMsaArray()[i].getEntityId()
										+ "-"
										+ msaList.getMsaArray()[i]
												.getEntityName(),
								e.getMessage());
						logger.error(e.getMessage());
					}
				}
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			logger.error(
					"runReport(com.mind.ilex.docmanager.MSAList, ReportFilterCriteria, ServletContext, String)",
					e);

			logger.error(e.getMessage());
		} finally {

			DBUtil.close(conn);
		}
		logger.trace("End:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator |"
				+ " Method : runReport)");

	}

	/**
	 * runReportForMSA - This method used for submitting information the report
	 * and also for the report generation.
	 * 
	 * @param reportId
	 *            - Takes the reportID
	 * @param MSA
	 *            - Takes the individual MSA.
	 * @param reportFilterCriteria
	 *            - Report filter criteria for the report.
	 * @return Returns none
	 **/
	public void runReportForMSA(String reportId, MSA msa,
			ReportFilterCriteria reportFilterCriteria, ServletContext context,
			Connection conn, String userCode) throws NotAbleToInsertDocument,
			ControlledTypeException, DocMFormatException,
			CouldNotAddToRepositoryException, CouldNotReplaceException,
			CouldNotCheckDocumentException, EntityDetailsNotFoundException,
			DocumentIdNotUpdatedException, SQLException, Exception {
		logger.trace("Start:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator |"
				+ " Method : runReportForMSA)");
		runReportForMSA(reportId, msa, reportFilterCriteria, context, conn, 0,
				userCode);
		logger.trace("End:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator | "
				+ "Method : runReportForMSA)");

	}

	public void runReportForMSA(String reportId, MSA msa,
			ReportFilterCriteria reportFilterCriteria, ServletContext context,
			Connection conn, int manual, String userCode)
			throws NotAbleToInsertDocument, ControlledTypeException,
			DocMFormatException, CouldNotAddToRepositoryException,
			CouldNotReplaceException, CouldNotCheckDocumentException,
			EntityDetailsNotFoundException, DocumentIdNotUpdatedException,
			SQLException, Exception {
		logger.trace("Start:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator |"
				+ " Method : runReportForMSA)");
		innerList = new ArrayList();
		SubmitDocuments sd = new SubmitDocuments();
		ReportsDao rpDao = new ReportsDao();
		long executionTime = 0;
		long startTime = System.currentTimeMillis();
		// returns the bytes for the generated report.
		logger.trace("get the generated report bytes.");
		byte[] reportBytes = new ViewDocument().getBytes(context, conn, msa,
				reportFilterCriteria);
		if (reportBytes != null && reportBytes.length > 0) {

			// genrates the uniqueKey for the document
			logger.trace("get the unique key for the document.");
			String uniqueKey = rpDao.getUniqueKey(conn, msa, reportId);

			// inserts the information for the document
			if (manual == 0)
				sd.submitDocuments(reportId, msa, reportFilterCriteria,
						uniqueKey, conn, userCode);

			String doc_id = EntityManager.approveEntity(uniqueKey,
					reportFilterCriteria.getReportType(), reportBytes, 0);
			innerList.add(msa.getEntityId() + "-" + msa.getEntityName());
			innerList.add(DatabaseUtilityDao.getKeyValue("document.generation")
					+ doc_id);
			innerList.add(new Integer(1));
			msaResultMap.put(msa.getEntityId() + "-" + msa.getEntityName(),
					DatabaseUtilityDao.getKeyValue("document.generation")
							+ doc_id);
			executionTime = System.currentTimeMillis() - startTime;
			sd.updateReportScheduleDetail(reportId, uniqueKey, executionTime,
					conn, userCode);
		} else {
			innerList.add(msa.getEntityId() + "-" + msa.getEntityName());
			innerList.add(DatabaseUtilityDao
					.getKeyValue("MSA.insufficient.data"));
			innerList.add(new Integer(0));
			msaResultMap.put(msa.getEntityId() + "-" + msa.getEntityName(),
					DatabaseUtilityDao.getKeyValue("MSA.insufficient.data"));
		}
		outerList.add(innerList);
		logger.trace("End:(Class : com.mind.ilex.reports.DetailedSummaryReportGenerator | "
				+ "Method : runReportForMSA)");

	}

}
