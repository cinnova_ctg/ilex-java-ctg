//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : ReRunUtility.java
//  @ Date : 09-July-2008
//  @ Author : Pankaj Chaudhary
//
//

package com.mind.ilex.reports.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.reports.ReportFilterCriteria;
import com.mind.docm.dao.MSA;
import com.mind.docm.dao.MSAList;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.reports.DetailedSummaryReportGenerator;
import com.mind.ilex.reports.reportDao.ReportFilterCriteriaDao;

/**
 * Class ReRunUtility - This class is used for rerunning of scheduler manually
 * for some months. methods reRunReportScheduler, isDataExist
 **/
public class ReRunUtility {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ReRunUtility.class);

	/**
	 * @name reRunReportScheduler
	 * @purpose This method is used to run the report scheduler manually.
	 * @param reportType
	 *            - Takes Report type parameter.
	 * @param MSAList
	 *            - Takes the MSA list
	 * @param month
	 *            - Month for the scheduler to be rerun.
	 * @param year
	 *            - Year for the scheduler to be rerun.
	 * @param request
	 *            - HttpServletRequest Object.
	 * @param DataSource
	 *            - An Object of dataSource.
	 * @return Returns none.
	 * @returnDescription none
	 */
	public List reRunReportScheduler(String reportType, MSAList msaList,
			long month, long year, HttpServletRequest request,
			DataSource dataSource) throws Exception {

		Connection conn = null;
		try {
			Calendar cal = Calendar.getInstance();
			DetailedSummaryReportGenerator dsrg = new DetailedSummaryReportGenerator();

			SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
			ReportFilterCriteriaDao rfcd = new ReportFilterCriteriaDao();
			Date startDate = sf.parse((String.valueOf(month) + "/"
					+ String.valueOf(01) + "/" + String.valueOf(year)));
			cal.setTime(startDate);
			cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
			Date endDate = sf.parse((String.valueOf(month) + "/"
					+ String.valueOf(cal.get(Calendar.DATE)) + "/" + String
					.valueOf(year)));

			MSA[] msaArray = msaList.getMsaArray();
			List newMSAList = new ArrayList();
			MSAList newList = null;
			List outerList = new ArrayList();
			ReportFilterCriteria reportFilterCriteria = rfcd
					.getReportFilterCriteriaForRerun(reportType, startDate,
							endDate, dataSource);
			String userId = (String) request.getSession(false).getAttribute(
					"userid");
			for (int i = 0; i < msaArray.length; i++) {

				String reportId = this.isDataExist(msaArray[i], reportType,
						startDate, endDate, dataSource);
				if (reportId != null && !reportId.trim().equalsIgnoreCase("")) {

					List UpdationMSAList = new ArrayList();
					UpdationMSAList.add(msaArray[i]);
					UpdationMSAList.add(reportId);
					outerList.add(UpdationMSAList);
				} else {
					newMSAList.add(msaArray[i]);
				}

			}
			if (outerList.size() > 0) {
				for (int j = 0; j < outerList.size(); j++) {
					List inner = (ArrayList) outerList.get(j);
					try {
						conn = dataSource.getConnection();
						dsrg.runReportForMSA((String) inner.get(1),
								(MSA) inner.get(0), reportFilterCriteria,
								request.getSession(false).getServletContext(),
								conn, 1, userId);
					} finally {
						DBUtil.close(conn);
					}

				}

			}
			if (newMSAList.size() > 0) {
				newList = new MSAList();
				MSA[] newArray = new MSA[newMSAList.size()];
				for (int k = 0; k < newMSAList.size(); k++) {

					newArray[k] = (MSA) newMSAList.get(k);

				}
				newList.setMsaArray(newArray);
				dsrg.runReport(newList, reportType, reportFilterCriteria,
						request.getSession(false).getServletContext(), 0,
						userId);
			}

			Map returnMap = dsrg.msaResultMap;
			List returnList = dsrg.outerList;
			// Set taskSet = returnMap.entrySet();
			// Iterator taskIterator = taskSet.iterator();
			// while(taskIterator.hasNext())
			// {
			// Map.Entry taskEntry = (Map.Entry)taskIterator.next();
			// System.out.println("taskKey---"+taskEntry.getKey()+"----taskVal----"+taskEntry.getValue());
			// }
			return returnList;
		} finally {
			DBUtil.close(conn);
		}

	}

	/**
	 * @name isDataExist
	 * @purpose This method checks whether the document for the msa for which
	 *          the scheduler is to be rerun exists or not.
	 * @param reportType
	 *            - Takes Report type parameter.
	 * @param startDate
	 *            - Start date for the report parameters.
	 * @param endDate
	 *            - End date for the report parameters.
	 * @param DataSource
	 *            - An Object of dataSource.
	 * @return Returns reportId or null.
	 * @returnDescription none
	 */
	public String isDataExist(MSA msa, String reportType, Date startDate,
			Date endDate, DataSource dataSource) throws Exception {
		Connection conn = null;
		PreparedStatement pStmt = null;
		// Statement stmt = null;
		ResultSet rs = null;
		String report_id = null;
		try {
			SimpleDateFormat sf = new SimpleDateFormat("dd MMM yyyy");
			conn = dataSource.getConnection();
			String sql = "select report_id from report_schedule_detail where iteration_id =? and report_id in"
					+ " \n (select report_id from report_schedule_log where convert(varchar,start_date,106)=?"
					+ " \n and convert(varchar,end_date,106) = ? and report_id in (select report_id from report_schedule_main where report_type=?))";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, msa.getEntityId());
			pStmt.setString(2, sf.format(startDate));
			pStmt.setString(3, sf.format(endDate));
			pStmt.setString(4, reportType);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				report_id = rs.getString("report_id");
				return report_id;
			}
		} catch (Exception e) {
			logger.error("isDataExist(MSA, String, Date, Date, DataSource)", e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(pStmt);
			// DatabaseUtilityDao.closeResultSet(rs);
			DBUtil.close(conn);
		}
		return null;
	}

}
