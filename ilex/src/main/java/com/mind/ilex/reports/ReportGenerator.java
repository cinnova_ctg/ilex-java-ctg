//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : ReportGenerator.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//



package com.mind.ilex.reports;

import com.mind.bean.reports.ReportFilterCriteria;

/**
* Class ReportGenerator - This is base class which helps in generating report and getting and setting report criteria.
* methods - getter, setter
**/
public class ReportGenerator {
	private ReportFilterCriteria reportFilterCriteria;
	public void runReport() {
	
	}
	public ReportFilterCriteria getReportFilterCriteria(String reportType,ReportFilterCriteria dateFilterCriteria) {
		return reportFilterCriteria;
	}
	
	public void setReportFilterCriteria(ReportFilterCriteria reportFilterCriteria) {
		this.reportFilterCriteria = reportFilterCriteria;
	}
}
