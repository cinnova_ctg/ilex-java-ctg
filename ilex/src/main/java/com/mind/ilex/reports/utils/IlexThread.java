/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description : File contains the logic for the running and 
 * 					halting of the scheduler.
 *
 */

package com.mind.ilex.reports.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.mind.bean.reports.ReportFilterCriteria;
import com.mind.docm.exceptions.DateExceedThreadException;
import com.mind.docm.exceptions.FrequencyFormatException;
import com.mind.docm.exceptions.TimeExceedThreadException;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.reports.DetailedSummaryReportGenerator;
import com.mind.ilex.reports.reportDao.ReportFilterCriteriaDao;
import com.mind.ilex.reports.reportDao.ReportsDao;

/**
 * Class IlexThread: This class runs the thread for the scheduler which we
 * create in IlexContextListener class. methods - run, checkFrequencyFormat,
 * validatePastTrack
 */

public class IlexThread implements Runnable {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IlexThread.class);

	public boolean started = true;

	private ServletContext context = null;

	private static long DEFAULT_SLEEP_TIME_IN_HOURS = 1;

	public IlexThread(ServletContext context) {
		logger.trace("Start :(Class : com.mind.ilex.utils.IlexThread | "
				+ "Constructor : IlexThread).");
		logger.trace(" Initializes the context instance variable of the Class.");
		this.context = context;
		logger.trace("End :(Class : com.mind.ilex.utils.IlexThread | "
				+ "Constructor : IlexThread).");
	}

	/**
	 * run - The only method in that class which is mendatory because this class
	 * implements an interface. This method fetchs data from the database on the
	 * basis of which it is decided that whether schedule runs or not and is
	 * also checks for how many time it must be run.
	 * 
	 * @return Return None
	 */
	public void run() {
		logger.trace("Start :(Class : com.mind.ilex.reports.utils.IlexThread | "
				+ "Method : run).");
		logger.trace("This method must be there because this class implements "
				+ "runnable interface.");
		String propsFile = ReportsUtility.RESOURCE_FILE_NAME_KEY;
		ResourceBundle res = ResourceBundle.getBundle(propsFile);
		String sleepTimeInHoursStr = res
				.getString(ReportsUtility.THREAD_SLEEPTIME_KEY);
		logger.trace("sleepTimeInHoursStr" + sleepTimeInHoursStr);
		long sleepTimeInHours = DEFAULT_SLEEP_TIME_IN_HOURS;
		try {
			sleepTimeInHours = Long.parseLong(sleepTimeInHoursStr);
		} catch (Exception e) {
			logger.error("run()", e);

			sleepTimeInHours = DEFAULT_SLEEP_TIME_IN_HOURS;
		}
		logger.trace("sleepTimeInHours=" + sleepTimeInHours);

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		DetailedSummaryReportGenerator dsrg = new DetailedSummaryReportGenerator();
		String dateString;
		String hourString;
		String frequencyData = null;
		String reportType = null;
		String pastTrack = "";
		logger.trace("Thread runs continuously unless the context is destroyed "
				+ "explicitly or server stopped.");
		while (started) {
			try {
				String sql = "SELECT REPORT_TYPE,FREQUENCY,PAST_TRACK FROM REPORT_SCHEDULE_PARAMS";
				conn = ReportsDao.getStaticConnection();
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				List resultSetAsList = ReportsUtility.getResultSetAsList(rs);
				GregorianCalendar calendar = new GregorianCalendar();
				DBUtil.close(stmt);
				DBUtil.close(rs);

				for (int j = 0; j < resultSetAsList.size(); j++) {
					Map map = (HashMap) resultSetAsList.get(j);
					reportType = (String) map.get("REPORT_TYPE");
					frequencyData = (String) map.get("FREQUENCY");
					pastTrack = (String) map.get("PAST_TRACK");
					if (validatePastTrack(pastTrack)) {
						throw new NumberFormatException();
					}

					/*
					 * Frequency field is picked from database and has a
					 * specific format M30H12 means scheduler will run on 30th
					 * date of every month and at 12:00 pm in the afternoon.
					 */
					checkFrequencyFormat(frequencyData);
					dateString = frequencyData.substring(1,
							frequencyData.indexOf('H'));
					hourString = frequencyData.substring(frequencyData
							.indexOf('H') + 1);
					// java.util.Date currentDate = new java.util.Date();
					// int day = currentDate.getDate();
					// int hours = currentDate.getHours();
					int day = calendar.get(Calendar.DATE);
					int hours = calendar.get(Calendar.HOUR_OF_DAY);
					ReportFilterCriteriaDao reportFilterCriteriaDao = new ReportFilterCriteriaDao();
					ReportFilterCriteria reportFilterCriteria = null;

					/**
					 * *********************************************************
					 * ****************************
					 */

					if (calendar.getActualMaximum(Calendar.DATE) < new Integer(
							dateString).intValue()
							&& new Integer(dateString).intValue() < 32) {

						dateString = String.valueOf(calendar
								.getActualMaximum(Calendar.DATE));
					}

					/**
					 * *********************************************************
					 * *****************************
					 */
					// it checks whether that date is achieved when scheduler is
					// to be run
					logger.trace("Here thread checks whether running time of the "
							+ "scheduler has reached or not.");
					if (ReportsUtility.containsData(dateString,
							String.valueOf(day), hourString,
							String.valueOf(hours))) {
						// it checks whether that time is achieved on that
						// perticular date above when scheduler is to be run
						if (ReportsUtility.containsData(dateString,
								String.valueOf(day), hourString,
								String.valueOf(hours))) {

							logger.trace("Here thread checks whether value of the past "
									+ "track variable.");
							logger.trace("if 0 then it will not run scheduler.");
							// it checks whether the past Track field in
							// database is zero or greater than zero.
							if (pastTrack == null
									|| new Integer(pastTrack).intValue() == 0) {

							}
							long executionTime = 0;

							logger.trace(" Here thread checks whether value of the past "
									+ "track variable is greater then zero.");
							logger.trace("If yes then it will try to run the scheduler .");
							if (pastTrack != null
									&& new Integer(pastTrack).intValue() > 0) {
								// now iteration for the value of the past track
								for (int i = new Integer(pastTrack).intValue(); i > 0; i--) {
									long startTime = System.currentTimeMillis();
									// it gets the filter criteria object for
									// report
									logger.trace(" Now thread try to collect the "
											+ "information required to generate report.");
									reportFilterCriteria = reportFilterCriteriaDao
											.getRFCWithPast(reportType, conn,
													String.valueOf(i));
									// it checks whether the report criteria is
									// null or not.
									logger.trace(" It will only run the scheduler if "
											+ "ReportfilterCriteria object is not null.");
									if (reportFilterCriteria != null) {

										logger.trace(" Here it will run the scheduler .");
										dsrg.runReport(reportType,
												reportFilterCriteria,
												this.context);
										logger.trace(" Scheduler run completed.");
										executionTime = executionTime
												+ System.currentTimeMillis()
												- startTime;
									} else
										continue;
								}
							}
						}
					}
				}
			} catch (NumberFormatException e) {
				logger.error("run()", e);

				logger.error("past track format is not good.");
			} catch (FrequencyFormatException e) {
				logger.error("run()", e);

				e.showMessage();
			} catch (TimeExceedThreadException e) {
				logger.error("run()", e);

				e.showMessage();
			} catch (DateExceedThreadException e) {
				logger.error("run()", e);

				e.showMessage();
			} catch (Exception e) {
				logger.error("run()", e);

				logger.error("run()", e);
			} finally {
				DBUtil.close(conn);
				logger.trace("End :(Class : com.mind.ilex.reports.utils.IlexThread | "
						+ "Method : run).");
			}
			try {
				// 1 hour = 1000 millseconds * 60 seconds * 60 minutes
				logger.trace("Now reporting system going to sleep for "
						+ sleepTimeInHours + " hour(s)...");
				Thread.sleep(1000 * 60 * 60 * sleepTimeInHours);
			} catch (Exception e) {
				logger.error("run()", e);

				logger.error("Thread sleeping exception.");
			}
		}

	}

	/**
	 * checkFrequencyFormat - The method checks the input frequency format.
	 * 
	 * @param frequencyData
	 *            - Takes the frequency format.
	 * @return Return new exception
	 */
	public static void checkFrequencyFormat(String frequencyData)
			throws DateExceedThreadException, TimeExceedThreadException,
			FrequencyFormatException {
		logger.trace("Start :(Class : com.mind.ilex.reports.utils.IlexThread | "
				+ "Method : checkFrequencyFormat).");
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[M]\\d\\d?[H]\\d\\d?");
		matcher = pattern.matcher(frequencyData);
		logger.trace("Checks whether the frequency format is per requirement or not.");
		if (matcher.matches()) {
			String dateString = frequencyData.substring(1,
					frequencyData.indexOf('H'));
			String hourString = frequencyData.substring(frequencyData
					.indexOf('H') + 1);
			if (new Integer(dateString).intValue() > 31
					|| new Integer(dateString).intValue() < 1) {
				throw new DateExceedThreadException(dateString);
			}
			if (new Integer(hourString).intValue() > 24) {
				throw new TimeExceedThreadException(hourString);
			}
		} else {
			throw new FrequencyFormatException();
		}
		logger.trace("End :(Class : com.mind.ilex.reports.utils.IlexThread |"
				+ " Method : checkFrequencyFormat).");
	}

	/**
	 * validatePastTrack - The method validates the value of pastTrack.
	 * 
	 * @param frequencyData
	 *            - Takes PastTrack.
	 * @return Return boolean.
	 **/
	public static boolean validatePastTrack(String pastTrack) {
		logger.trace("Start :(Class : com.mind.ilex.reports.utils.IlexThread | "
				+ "Method : validatePastTrack).");
		try {
			new Integer(pastTrack).intValue();
		} catch (Exception e) {
			logger.error("validatePastTrack(String)", e);

			return true;
		}
		logger.trace("End :(Class : com.mind.ilex.reports.utils.IlexThread |"
				+ " Method : validatePastTrack).");
		return false;

	}

}
