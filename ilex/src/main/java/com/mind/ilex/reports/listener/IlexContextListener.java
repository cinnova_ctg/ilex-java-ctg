/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: <Code contains the logic of listening the servletContext event and starts the scheduler.>
 *
 */

package com.mind.ilex.reports.listener;

import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.mind.ilex.reports.utils.IlexThread;
import com.mind.ilex.reports.utils.ReportsUtility;

/**
 * Class IlexContextListener - This is a listner class which listen the
 * servletContext event as the server starts or stop and with respect to that it
 * starts or stop the scheduler. methods - contextInitialized, contextDestroyed
 */

public class IlexContextListener implements ServletContextListener {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(IlexContextListener.class);

	private IlexThread ilexThread = null;

	/**
	 * Method contextInitialized - This method is aumatically called by the
	 * container as the new servletContextEvent happened and it initialized the
	 * new Servlet context.
	 * 
	 * @param ServletContext
	 *            - it gives the information regarding context of the
	 *            application.
	 * @return Return value None
	 */

	public void contextInitialized(ServletContextEvent event) {
		// set the debug level as the application starts

		boolean check = true;
		String isRun = null;
		logger.trace("Start :(Class : com.mind.ilex.listener.IlexContextListener | "
				+ "Method : contextInitialized)");
		logger.trace("This method initializes the Servlet context.");
		logger.trace("Here in this method we create object of our scheduler thread.");
		// gets the servlet context object from the ServletContextEvent
		ServletContext context = event.getServletContext();
		String propsFile = ReportsUtility.RESOURCE_FILE_NAME_KEY;
		ResourceBundle res = ResourceBundle.getBundle(propsFile);
		try {
			isRun = res.getString(ReportsUtility.RESOURCE_THREAD_KEY);
		} catch (Exception e) {
			logger.error("contextInitialized(ServletContextEvent)", e);

			logger.error("contextInitialized(ServletContextEvent)", e);
			check = false;
			logger.error(e.getMessage());
		}
		if (!check)
			isRun = "0";
		logger.trace("Object of Thread is created on the basis of resource file "
				+ "property value.");
		logger.trace("if the value of the property is 1 then only it will be created.");
		if (isRun != null && isRun.trim().equalsIgnoreCase("1")) {
			logger.trace(" Thread Called.");
			ilexThread = new IlexThread(context);
			// creates object for the Thread class.
			Thread thread = new Thread(ilexThread);
			// starts the thread
			thread.start();
		} else {
			logger.error("please turn on the above property.");
		}
		logger.trace("End :(Class : com.mind.ilex.listener.IlexContextListener | "
				+ "Method : contextInitialized)");

	}

	/**
	 * Method contextDestroyed - This method is aumatically called by the
	 * container as the new servletContextEvent happened and it destroy the new
	 * Servlet context.
	 * 
	 * @param ServletContext
	 *            - it gives the information regarding context of the
	 *            application.
	 * @return Return value None
	 */

	// called when context destroyed.
	public void contextDestroyed(ServletContextEvent event) {
		logger.trace("Start :(Class : com.mind.ilex.listener.IlexContextListener | "
				+ "Method : contextDestroyed)");
		logger.trace("This method is automatically called by container when context "
				+ "is destroyed.");
		if (ilexThread != null)
			ilexThread.started = false;
		logger.trace("Scheduler is going to stop.");
		logger.trace("End :(Class : com.mind.ilex.listener.IlexContextListener | "
				+ "Method : contextDestroyed)");
	}
}
