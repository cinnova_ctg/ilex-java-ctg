/**
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is a form bean class used for getting and setting the values of Role Privilege Form.      
*
*/

package com.mind.formbean.UM;

import org.apache.struts.action.ActionForm;


public class RolePrivilegeForm extends ActionForm {
	
	private String lo_organization_type_name = null;
	private String lo_od_name = null;
	private String cc_fu_group_name = null;
	private String lo_ro_role_desc = null;
	private String lo_ro_id = null;
	private String org_type = null;
	private String org_discipline_id = null;
	private int[] check = null;
	private String save = null;
	private boolean refresh=false;
	

	public String getCc_fu_group_name() 
	{
		return cc_fu_group_name;
	}
	public void setCc_fu_group_name ( String cc_fu_group_name ) 
	{
		this.cc_fu_group_name = cc_fu_group_name;
	}
	
	public String getLo_od_name() 
	{
		return lo_od_name;
	}
	public void setLo_od_name ( String lo_od_name ) 
	{
		this.lo_od_name = lo_od_name;
	}
	
	public String getLo_organization_type_name() 
	{
		return lo_organization_type_name;
	}
	public void setLo_organization_type_name ( String lo_organization_type_name ) 
	{
		this.lo_organization_type_name = lo_organization_type_name;
	}
	
	public String getLo_ro_role_desc() 
	{
		return lo_ro_role_desc;
	}
	public void setLo_ro_role_desc ( String lo_ro_role_desc ) 
	{
		this.lo_ro_role_desc = lo_ro_role_desc;
	}
	
	public String getSave() 
	{
		return save;
	}
	public void setSave ( String save ) 
	{
		this.save = save;
	}
	
	public boolean getRefresh() 
	{
		return refresh;
	}

	public void setRefresh ( boolean refresh ) 
	{
		this.refresh = refresh;
	}
	public String getOrg_discipline_id() 
	{
		return org_discipline_id;
	}
	public void setOrg_discipline_id ( String org_discipline_id ) 
	{
		this.org_discipline_id = org_discipline_id;
	}
	public String getOrg_type() 
	{
		return org_type;
	}
	public void setOrg_type ( String org_type ) 
	{
		this.org_type = org_type;
	}
	
	public int[] getCheck()
	{
		return check;
	}

	public void setCheck ( int[] ix )
	{
		check = ix;
	}

	public String getLo_ro_id() 
	{
		return lo_ro_id;
	}
	
	public void setLo_ro_id ( String lo_ro_id ) 
	{
		this.lo_ro_id = lo_ro_id;
	}
	
}
