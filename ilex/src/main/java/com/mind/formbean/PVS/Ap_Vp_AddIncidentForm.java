package com.mind.formbean.PVS;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 17 fields on this form:
 * <ul>
 * <li>ckboxharassment - [your comment here]
 * <li>technicianName - [your comment here]
 * <li>time - [your comment here]
 * <li>date - [your comment here]
 * <li>endCustomer - [your comment here]
 * <li>ckboxmissingtools - [your comment here]
 * <li>ckboxlowerlevel - [your comment here]
 * <li>notes - [your comment here]
 * <li>ckboxlate - [your comment here]
 * <li>ckboxperformance - [your comment here]
 * <li>save - [your comment here]
 * <li>ckboxterminatemcsa - [your comment here]
 * <li>ckboxrestricttechnician - [your comment here]
 * <li>ckboxapology - [your comment here]
 * <li>technician_name - [your comment here]
 * <li>ckboxother - [your comment here]
 * <li>delete - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class Ap_Vp_AddIncidentForm extends ActionForm {

	private String ckboxharassment = null;
	private String technicianName = null;
	private String time = null;
	private String date = null;
	private String endCustomer = null;
	private String ckboxmissingtools = null;
	private String ckboxlate = null;
	private String ckboxlowerlevel = null;
	private String notes = null;
	private String ckboxperformance = null;
	private String ckboxterminatemcsa = null;
	private String save = null;
	private String ckboxapology = null;
	private String ckboxrestricttechnician = null;
	private String technician_name = null;
	private String ckboxother = null;
	private String delete = null;

	/**
	 * Get ckboxharassment
	 * @return String
	 */
	public String getCkboxharassment() {
		return ckboxharassment;
	}

	/**
	 * Set ckboxharassment
	 * @param <code>String</code>
	 */
	public void setCkboxharassment(String c) {
		this.ckboxharassment = c;
	}

	/**
	 * Get technicianName
	 * @return String
	 */
	public String getTechnicianName() {
		return technicianName;
	}

	/**
	 * Set technicianName
	 * @param <code>String</code>
	 */
	public void setTechnicianName(String t) {
		this.technicianName = t;
	}

	/**
	 * Get time
	 * @return String
	 */
	public String getTime() {
		return time;
	}

	/**
	 * Set time
	 * @param <code>String</code>
	 */
	public void setTime(String t) {
		this.time = t;
	}

	/**
	 * Get date
	 * @return String
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Set date
	 * @param <code>String</code>
	 */
	public void setDate(String d) {
		this.date = d;
	}

	/**
	 * Get endCustomer
	 * @return String
	 */
	public String getEndCustomer() {
		return endCustomer;
	}

	/**
	 * Set endCustomer
	 * @param <code>String</code>
	 */
	public void setEndCustomer(String e) {
		this.endCustomer = e;
	}

	/**
	 * Get ckboxmissingtools
	 * @return String
	 */
	public String getCkboxmissingtools() {
		return ckboxmissingtools;
	}

	/**
	 * Set ckboxmissingtools
	 * @param <code>String</code>
	 */
	public void setCkboxmissingtools(String c) {
		this.ckboxmissingtools = c;
	}

	/**
	 * Get ckboxlate
	 * @return String
	 */
	public String getCkboxlate() {
		return ckboxlate;
	}

	/**
	 * Set ckboxlate
	 * @param <code>String</code>
	 */
	public void setCkboxlate(String c) {
		this.ckboxlate = c;
	}

	/**
	 * Get ckboxlowerlevel
	 * @return String
	 */
	public String getCkboxlowerlevel() {
		return ckboxlowerlevel;
	}

	/**
	 * Set ckboxlowerlevel
	 * @param <code>String</code>
	 */
	public void setCkboxlowerlevel(String c) {
		this.ckboxlowerlevel = c;
	}

	/**
	 * Get notes
	 * @return String
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Set notes
	 * @param <code>String</code>
	 */
	public void setNotes(String n) {
		this.notes = n;
	}

	/**
	 * Get ckboxperformance
	 * @return String
	 */
	public String getCkboxperformance() {
		return ckboxperformance;
	}

	/**
	 * Set ckboxperformance
	 * @param <code>String</code>
	 */
	public void setCkboxperformance(String c) {
		this.ckboxperformance = c;
	}

	/**
	 * Get ckboxterminatemcsa
	 * @return String
	 */
	public String getCkboxterminatemcsa() {
		return ckboxterminatemcsa;
	}

	/**
	 * Set ckboxterminatemcsa
	 * @param <code>String</code>
	 */
	public void setCkboxterminatemcsa(String c) {
		this.ckboxterminatemcsa = c;
	}

	/**
	 * Get save
	 * @return String
	 */
	public String getSave() {
		return save;
	}

	/**
	 * Set save
	 * @param <code>String</code>
	 */
	public void setSave(String s) {
		this.save = s;
	}

	/**
	 * Get ckboxapology
	 * @return String
	 */
	public String getCkboxapology() {
		return ckboxapology;
	}

	/**
	 * Set ckboxapology
	 * @param <code>String</code>
	 */
	public void setCkboxapology(String c) {
		this.ckboxapology = c;
	}

	/**
	 * Get ckboxrestricttechnician
	 * @return String
	 */
	public String getCkboxrestricttechnician() {
		return ckboxrestricttechnician;
	}

	/**
	 * Set ckboxrestricttechnician
	 * @param <code>String</code>
	 */
	public void setCkboxrestricttechnician(String c) {
		this.ckboxrestricttechnician = c;
	}

	/**
	 * Get technician_name
	 * @return String
	 */
	public String getTechnician_name() {
		return technician_name;
	}

	/**
	 * Set technician_name
	 * @param <code>String</code>
	 */
	public void setTechnician_name(String t) {
		this.technician_name = t;
	}

	/**
	 * Get ckboxother
	 * @return String
	 */
	public String getCkboxother() {
		return ckboxother;
	}

	/**
	 * Set ckboxother
	 * @param <code>String</code>
	 */
	public void setCkboxother(String c) {
		this.ckboxother = c;
	}

	/**
	 * Get delete
	 * @return String
	 */
	public String getDelete() {
		return delete;
	}

	/**
	 * Set delete
	 * @param <code>String</code>
	 */
	public void setDelete(String d) {
		this.delete = d;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.

		ckboxharassment = null;
		technicianName = null;
		time = null;
		date = null;
		endCustomer = null;
		ckboxmissingtools = null;
		ckboxlate = null;
		ckboxlowerlevel = null;
		notes = null;
		ckboxperformance = null;
		ckboxterminatemcsa = null;
		save = null;
		ckboxapology = null;
		ckboxrestricttechnician = null;
		technician_name = null;
		ckboxother = null;
		delete = null;

	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.

		// if ((field == null) || (field.length() == 0)) {
		//   errors.add("field", new org.apache.struts.action.ActionError("error.field.required"));
		// }
		return errors;

	}
}
