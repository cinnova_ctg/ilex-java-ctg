/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: A formbean containing getter/setter methods for Partner search page
 *
 */
package com.mind.formbean.PVS;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.mind.common.LabelValue;

public class Partner_SearchForm extends ActionForm {

	// For Partner Type List Used in CheckBoxes
	private LinkedHashMap<String, String> partnerTypeList = new LinkedHashMap<String, String>();

	public LinkedHashMap<String, String> getPartnerTypeList() {
		return partnerTypeList;
	}

	public void setPartnerTypeList(LinkedHashMap<String, String> partnerTypeList) {
		this.partnerTypeList = partnerTypeList;
	}

	private String cbox2 = null;
	private String name = null;
	private String cbox1 = null;
	private String cmboxpartnertype = null;
	private String cbox3 = null;
	private String cboxDrug = null;

	private String jobInfo = null;

	private String expiryDate = null;
	private String mcsaVersion = null;

	private String reset = null;
	private String keyword = null;
	private String radius = null;

	private String searchTerm = null;
	private String mainOffice = null;
	private String fieldOffice = null;
	private String[] technicians = null;
	private String enviromentalFacility = null;
	private String keyWords = null;
	private String[] coreCompetency = null;
	private String[] toolKitArr = null;
	private String certifications = null;
	private String techniciansZip = null;
	// New Field Added for Security Certifications Check Box.
	private String securityCertiftn = null;

	private String[] recommendedArr = null;
	private String[] restrictedArr = null;
	private String recommendedList;
	private String restrictedList;
	private String toolkitlist;
	private ArrayList<String> toollist;

	private String cpdBadgeId;

	private String badgePrinted = null;

	private String cmboxStatus = null;

	public String getCmboxStatus() {
		return cmboxStatus;
	}

	public void setCmboxStatus(String cmboxStatus) {
		this.cmboxStatus = cmboxStatus;
	}

	public String getBadgePrinted() {
		return badgePrinted;
	}

	public void setBadgePrinted(String badgePrinted) {
		this.badgePrinted = badgePrinted;
	}

	public String getSecurityCertiftn() {
		return securityCertiftn;
	}

	public void setSecurityCertiftn(String securityCertiftn) {
		this.securityCertiftn = securityCertiftn;
	}

	private String jobName = null;
	private String cmboxcertifications = null;
	private String cmboxcorecompetencies = null;
	private String search = null;
	private String partnername = null;
	private String pid = null;
	private String pcompetency = null;
	private String city = null;
	private String state = null;
	private String zipcode = null;
	private String country = null;
	private String phone = null;
	private String incflag = null;
	private String searchstring = null;
	private String type = null;
	private String selectedpartner = null;
	private String ref = null;
	private String typeid = null;
	private String save = null;
	private String assignmessage = null;
	private String assigncheck = null;
	private String norecordmessage = null;
	private String authenticate = "";
	private String barredflag = null;
	private String excludeinccbx = "Y";
	private String excludebarredcbx = "Y";
	private String unassign = null;
	private String unassignmessage = null;

	private String partnerPriCellPhoneTemp = null;
	private String cityTemp = null;
	private String partnernameTemp = null;

	private String formtype = null; // used to distinguish whether partner
									// search page is open from PVS or PRM or
									// PO/WO

	private String fromprm = null; // used to distinguish whether partner search
									// page is open from PVS or PRM
	private String hiddensave = null; // to set the value of this property
										// through javascript function
										// equivalent to setting save property
	private String viewjobtype = null; // for maintaining filtering status of
										// jobs in appendix dashboard
	private String ownerId = null; // for maintaining filtering status of jobs
									// in appendix dashboard
	private String status = null; // status of the partner in search results

	private String distance = null;

	private String minutmanPartner = null;
	private String certifiedPartner = null;
	private String partnerType = null;

	private String lat_degree = null;
	private String lat_min = null;
	private String lat_direction = null;
	private String lon_degree = null;
	private String lon_min = null;
	private String lon_direction = null;

	private String lat = null;
	private String lon = null;

	private String incTypeFlag = null;
	private String powoId = null;

	private String certificationType = null;
	private String certificationsId = null;
	private ArrayList tempList = null;
	private String partnerTypeName = null;

	public String[] getCheckedPartnerName() {
		return checkedPartnerName;
	}

	public void setCheckedPartnerName(String[] checkedPartnerName) {
		this.checkedPartnerName = checkedPartnerName;
	}

	private String[] checkedPartnerName = { "M" };
	private String currentPartner = null;
	private String partnerRating = null;

	private String partnerAddress1 = null;
	private String partnerAddress2 = null;
	private String partnerPriName = null;
	private String partnerPriPhone = null;
	private String partnerPriCellPhone = null;
	private String partnerPriCellPhonePlain = null;
	private String partnerPriEmail = null;

	private String partnerAvgRating = null;
	private String contractedCount = null;
	private String compeletedCount = null;
	private String quesProfessional = null;
	private String quesOnTime = null;
	private String quesKnowledgeable = null;
	private String quesEquipped = null;
	private String partnerAdpo = null;
	private String partnerLatitude = null;
	private String partnerLongitude = null;
	private String gMap = null;
	private String diversityIndicator = null;
	private String partnerLastUsed = null;
	private String icontype = null;

	private String siteNumber = null;
	private String siteAddress = null;
	private String siteCity = null;
	private String siteLat = null;
	private String siteLong = null;
	private String siteState = null;
	private String siteCountry = null;
	private String Zip = null;

	// Vendex Specific.
	private String siteColor = null; // Color of Site.
	private String bid = null; // Bid price defined by a partner for a job.
	private String vendexMatches = null; // Number of Scheduled jobs that lie
											// within 150 miles radius of
											// location of a partner.
	private String vendexMap = null; // Vendex Map View.
	private String tabId = null;
	private String isClicked = null;
	private String reAssign = null;
	private String msaCustomerName = null;
	private String appendixName = null;
	private String appendixId = null;
	private String assign = null;

	private String travelDistance = null;
	private String travelDuration = null;
	private String siteLatitude = null;
	private String siteLongitude = null;

	// Changes related to show the update date and create date when Last Used
	// date is not present

	private String createDate = null;
	private String updateDate = null;
	private String exportToExcel;

	// added to get back selected value after sorting
	private String selectedPartnersList;
	private String coreCompetencyList;
	private String techniciansList;

	private String cbox4 = null;
	public String getAssign() {
		return assign;
	}

	public void setAssign(String action) {
		this.assign = action;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getMsaCustomerName() {
		return msaCustomerName;
	}

	public void setMsaCustomerName(String msaCustomerName) {
		this.msaCustomerName = msaCustomerName;
	}

	public String getReAssign() {
		return reAssign;
	}

	public void setReAssign(String reAssign) {
		this.reAssign = reAssign;
	}

	public String getIsClicked() {
		return isClicked;
	}

	public void setIsClicked(String isClicked) {
		this.isClicked = isClicked;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteLat() {
		return siteLat;
	}

	public void setSiteLat(String siteLat) {
		this.siteLat = siteLat;
	}

	public String getSiteLong() {
		return siteLong;
	}

	public void setSiteLong(String siteLong) {
		this.siteLong = siteLong;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getZip() {
		return Zip;
	}

	public void setZip(String zip) {
		Zip = zip;
	}

	public String getGMap() {
		return gMap;
	}

	public void setGMap(String map) {
		gMap = map;
	}

	public String getPartnerLatitude() {
		return partnerLatitude;
	}

	public void setPartnerLatitude(String partnerLatitude) {
		this.partnerLatitude = partnerLatitude;
	}

	public String getPartnerLongitude() {
		return partnerLongitude;
	}

	public void setPartnerLongitude(String partnerLongitude) {
		this.partnerLongitude = partnerLongitude;
	}

	public String getPartnerAdpo() {
		return partnerAdpo;
	}

	public void setPartnerAdpo(String partnerAdpo) {
		this.partnerAdpo = partnerAdpo;
	}

	public String getCompeletedCount() {
		return compeletedCount;
	}

	public void setCompeletedCount(String compeletedCount) {
		this.compeletedCount = compeletedCount;
	}

	public String getContractedCount() {
		return contractedCount;
	}

	public void setContractedCount(String contractedCount) {
		this.contractedCount = contractedCount;
	}

	public String getPartnerAvgRating() {
		return partnerAvgRating;
	}

	public void setPartnerAvgRating(String partnerAvgRating) {
		this.partnerAvgRating = partnerAvgRating;
	}

	public String getQuesEquipped() {
		return quesEquipped;
	}

	public void setQuesEquipped(String quesEquipped) {
		this.quesEquipped = quesEquipped;
	}

	public String getQuesKnowledgeable() {
		return quesKnowledgeable;
	}

	public void setQuesKnowledgeable(String quesKnowledgeable) {
		this.quesKnowledgeable = quesKnowledgeable;
	}

	public String getQuesOnTime() {
		return quesOnTime;
	}

	public void setQuesOnTime(String quesOnTime) {
		this.quesOnTime = quesOnTime;
	}

	public String getQuesProfessional() {
		return quesProfessional;
	}

	public void setQuesProfessional(String quesProfessional) {
		this.quesProfessional = quesProfessional;
	}

	public String getPartnerTypeName() {
		return partnerTypeName;
	}

	public void setPartnerTypeName(String partnerTypeName) {
		this.partnerTypeName = partnerTypeName;
	}

	public String getCertificationsId() {
		return certificationsId;
	}

	public void setCertificationsId(String certificationsId) {
		this.certificationsId = certificationsId;
	}

	public String getCertificationType() {
		return certificationType;
	}

	public void setCertificationType(String certificationType) {
		this.certificationType = certificationType;
	}

	public String getPowoId() {
		return powoId;
	}

	public void setPowoId(String powoId) {
		this.powoId = powoId;
	}

	public String getIncTypeFlag() {
		return incTypeFlag;
	}

	public void setIncTypeFlag(String incTypeFlag) {
		this.incTypeFlag = incTypeFlag;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat_degree() {
		return lat_degree;
	}

	public void setLat_degree(String lat_degree) {
		this.lat_degree = lat_degree;
	}

	public String getLat_direction() {
		return lat_direction;
	}

	public void setLat_direction(String lat_direction) {
		this.lat_direction = lat_direction;
	}

	public String getLat_min() {
		return lat_min;
	}

	public void setLat_min(String lat_min) {
		this.lat_min = lat_min;
	}

	public String getLon_degree() {
		return lon_degree;
	}

	public void setLon_degree(String lon_degree) {
		this.lon_degree = lon_degree;
	}

	public String getLon_direction() {
		return lon_direction;
	}

	public void setLon_direction(String lon_direction) {
		this.lon_direction = lon_direction;
	}

	public String getLon_min() {
		return lon_min;
	}

	public void setLon_min(String lon_min) {
		this.lon_min = lon_min;
	}

	public void reset() {
		minutmanPartner = null;
		certifiedPartner = null;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getHiddensave() {
		return hiddensave;
	}

	public void setHiddensave(String hiddensave) {
		this.hiddensave = hiddensave;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getCbox2() {
		return cbox2;
	}

	public void setCbox2(String c) {
		this.cbox2 = c;
	}

	public String getName() {
		return name;
	}

	public void setName(String n) {
		this.name = n;
	}

	public String getCbox1() {
		return cbox1;
	}

	public void setCbox1(String c) {
		this.cbox1 = c;
	}

	public String getCmboxpartnertype() {
		return cmboxpartnertype;
	}

	public void setCmboxpartnertype(String c) {
		this.cmboxpartnertype = c;
	}

	public String getReset() {
		return reset;
	}

	public void setReset(String r) {
		this.reset = r;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String k) {
		this.keyword = k;
	}

	public String getCmboxcertifications() {
		return cmboxcertifications;
	}

	public void setCmboxcertifications(String c) {
		this.cmboxcertifications = c;
	}

	public String getCmboxcorecompetencies() {
		return cmboxcorecompetencies;
	}

	public void setCmboxcorecompetencies(String c) {
		this.cmboxcorecompetencies = c;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String s) {
		this.search = s;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPartnername() {
		return partnername;
	}

	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPcompetency() {
		return pcompetency;
	}

	public void setPcompetency(String pcompetency) {
		this.pcompetency = pcompetency;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getIncflag() {
		return incflag;
	}

	public void setIncflag(String incflag) {
		this.incflag = incflag;
	}

	public String getSearchstring() {
		return searchstring;
	}

	public void setSearchstring(String searchstring) {
		this.searchstring = searchstring;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSelectedpartner() {
		return selectedpartner;
	}

	public void setSelectedpartner(String selectedpartner) {
		this.selectedpartner = selectedpartner;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getTypeid() {
		return typeid;
	}

	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getAssignmessage() {
		return assignmessage;
	}

	public void setAssignmessage(String assignmessage) {
		this.assignmessage = assignmessage;
	}

	public String getAssigncheck() {
		return assigncheck;
	}

	public void setAssigncheck(String assigncheck) {
		this.assigncheck = assigncheck;
	}

	public String getNorecordmessage() {
		return norecordmessage;
	}

	public void setNorecordmessage(String norecordmessage) {
		this.norecordmessage = norecordmessage;
	}

	public String getBarredflag() {
		return barredflag;
	}

	public void setBarredflag(String barredflag) {
		this.barredflag = barredflag;
	}

	public String getExcludebarredcbx() {
		return excludebarredcbx;
	}

	public void setExcludebarredcbx(String excludebarredcbx) {
		this.excludebarredcbx = excludebarredcbx;
	}

	public String getExcludeinccbx() {
		return excludeinccbx;
	}

	public void setExcludeinccbx(String excludeinccbx) {
		this.excludeinccbx = excludeinccbx;
	}

	public String getUnassign() {
		return unassign;
	}

	public void setUnassign(String unassign) {
		this.unassign = unassign;
	}

	public String getUnassignmessage() {
		return unassignmessage;
	}

	public void setUnassignmessage(String unassignmessage) {
		this.unassignmessage = unassignmessage;
	}

	public String getFromprm() {
		return fromprm;
	}

	public void setFromprm(String fromprm) {
		this.fromprm = fromprm;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.

		cbox2 = null;
		name = null;
		cbox1 = null;
		cmboxpartnertype = null;
		reset = null;
		keyword = null;
		radius = null;
		zipcode = null;
		cmboxcertifications = null;
		cmboxcorecompetencies = null;
		search = null;
		partnername = null;
		pid = null;
		pcompetency = null;
		city = null;
		state = null;
		zipcode = null;
		country = null;
		phone = null;
		incflag = null;
		searchstring = null;
		type = null;
		selectedpartner = null;
		currentPartner = null;
		ref = null;
		typeid = null;
		save = null;
		assignmessage = null;
		assigncheck = null;
		norecordmessage = null;
		barredflag = null;
		excludeinccbx = "Y";
		excludebarredcbx = "Y";
		unassign = null;
		unassignmessage = null;
		fromprm = null;
		hiddensave = null;
		keyword = null;
		// minutmanPartner = "on";
		// certifiedPartner = "on";

		// searchTerm = null;
		lat_degree = null;
		lat_min = null;
		lat_direction = null;
		lon_degree = null;
		lon_min = null;
		lon_direction = null;
	}

	public String getFormtype() {
		return formtype;
	}

	public void setFormtype(String formtype) {
		this.formtype = formtype;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getMcsaVersion() {
		return mcsaVersion;
	}

	public void setMcsaVersion(String mcsaVersion) {
		this.mcsaVersion = mcsaVersion;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public String getJobInfo() {
		return jobInfo;
	}

	public void setJobInfo(String jobInfo) {
		this.jobInfo = jobInfo;
	}

	public String getCertifications() {
		return certifications;
	}

	public void setCertifications(String certifications) {
		this.certifications = certifications;
	}

	public String[] getCoreCompetency() {
		return coreCompetency;
	}

	public void setCoreCompetency(String[] coreCompetency) {
		this.coreCompetency = coreCompetency;
	}

	public String getEnviromentalFacility() {
		return enviromentalFacility;
	}

	public void setEnviromentalFacility(String enviromentalFacility) {
		this.enviromentalFacility = enviromentalFacility;
	}

	public String getFieldOffice() {
		return fieldOffice;
	}

	public void setFieldOffice(String fieldOffice) {
		this.fieldOffice = fieldOffice;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public String getMainOffice() {
		return mainOffice;
	}

	public void setMainOffice(String mainOffice) {
		this.mainOffice = mainOffice;
	}

	public String getTechniciansZip() {
		return techniciansZip;
	}

	public void setTechniciansZip(String techniciansZip) {
		this.techniciansZip = techniciansZip;
	}

	public String getCertifiedPartner() {
		return certifiedPartner;
	}

	public void setCertifiedPartner(String certifiedPartner) {
		this.certifiedPartner = certifiedPartner;
	}

	public String getMinutmanPartner() {
		return minutmanPartner;
	}

	public void setMinutmanPartner(String minutmanPartner) {
		this.minutmanPartner = minutmanPartner;
	}

	public ArrayList getTempList() {
		return tempList;
	}

	public void setTempList(ArrayList tempList) {
		this.tempList = tempList;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String[] getTechnicians() {
		return technicians;
	}

	public void setTechnicians(String[] technicians) {
		this.technicians = technicians;
	}

	public String getCurrentPartner() {
		return currentPartner;
	}

	public void setCurrentPartner(String currentPartner) {
		this.currentPartner = currentPartner;
	}

	public String getPartnerRating() {
		return partnerRating;
	}

	public void setPartnerRating(String partnerRating) {
		this.partnerRating = partnerRating;
	}

	public String getPartnerAddress1() {
		return partnerAddress1;
	}

	public void setPartnerAddress1(String partnerAddress1) {
		this.partnerAddress1 = partnerAddress1;
	}

	public String getPartnerAddress2() {
		return partnerAddress2;
	}

	public void setPartnerAddress2(String partnerAddress2) {
		this.partnerAddress2 = partnerAddress2;
	}

	public String getPartnerPriCellPhone() {
		return partnerPriCellPhone;
	}

	public void setPartnerPriCellPhone(String partnerPriCellPhone) {
		this.partnerPriCellPhone = partnerPriCellPhone;
	}

	public String getPartnerPriEmail() {
		return partnerPriEmail;
	}

	public void setPartnerPriEmail(String partnerPriEmail) {
		this.partnerPriEmail = partnerPriEmail;
	}

	public String getPartnerPriName() {
		return partnerPriName;
	}

	public void setPartnerPriName(String partnerPriName) {
		this.partnerPriName = partnerPriName;
	}

	public String getPartnerPriPhone() {
		return partnerPriPhone;
	}

	public void setPartnerPriPhone(String partnerPriPhone) {
		this.partnerPriPhone = partnerPriPhone;
	}

	public String getPartnerLastUsed() {
		return partnerLastUsed;
	}

	public void setPartnerLastUsed(String partnerLastUsed) {
		this.partnerLastUsed = partnerLastUsed;
	}

	public String getIcontype() {
		return icontype;
	}

	public void setIcontype(String icontype) {
		this.icontype = icontype;
	}

	public String getDiversityIndicator() {
		return diversityIndicator;
	}

	public void setDiversityIndicator(String diversityIndicator) {
		this.diversityIndicator = diversityIndicator;
	}

	/* Vendex Specific: Start */
	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getVendexMatches() {
		return vendexMatches;
	}

	public void setVendexMatches(String vendexMatches) {
		this.vendexMatches = vendexMatches;
	}

	public String getVendexMap() {
		return vendexMap;
	}

	public void setVendexMap(String vendexMap) {
		this.vendexMap = vendexMap;
	}

	/* Vendex Specific: End */

	public String getSiteColor() {
		return siteColor;
	}

	public void setSiteColor(String siteColor) {
		this.siteColor = siteColor;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getTravelDistance() {
		return travelDistance;
	}

	public void setTravelDistance(String googleDistance) {
		this.travelDistance = googleDistance;
	}

	public String getTravelDuration() {
		return travelDuration;
	}

	public void setTravelDuration(String googleDuration) {
		this.travelDuration = googleDuration;
	}

	public String getSiteLatitude() {
		return siteLatitude;
	}

	public void setSiteLatitude(String siteLatitude) {
		this.siteLatitude = siteLatitude;
	}

	public String getSiteLongitude() {
		return siteLongitude;
	}

	public void setSiteLongitude(String siteLongitude) {
		this.siteLongitude = siteLongitude;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	// Changes Start by Yogendra For Adding International Section On partner
	// Search Screen

	private String selectedCountryName;
	private String selectedCityName;
	private List<LabelValue> countryNameValueList;
	private List<LabelValue> cityNameValueList;

	public List<LabelValue> getCountryNameValueList() {
		return countryNameValueList;
	}

	public void setCountryNameValueList(List<LabelValue> countryNameValueList) {
		this.countryNameValueList = countryNameValueList;
	}

	public List<LabelValue> getCityNameValueList() {
		return cityNameValueList;
	}

	public void setCityNameValueList(List<LabelValue> cityNameValueList) {
		this.cityNameValueList = cityNameValueList;
	}

	public String getSelectedCityName() {
		return selectedCityName;
	}

	public void setSelectedCityName(String selectedCityName) {
		this.selectedCityName = selectedCityName;
	}

	public String getSelectedCountryName() {
		return selectedCountryName;
	}

	public void setSelectedCountryName(String selectedCountryName) {
		this.selectedCountryName = selectedCountryName;
	}

	// Changes related to show the update date and create date when Last Used
	// date is not present
	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getExportToExcel() {
		return exportToExcel;
	}

	public void setExportToExcel(String exportToExcel) {
		this.exportToExcel = exportToExcel;
	}

	public String getSelectedPartnersList() {
		return selectedPartnersList;
	}

	public void setSelectedPartnersList(String selectedPartnersList) {
		this.selectedPartnersList = selectedPartnersList;
	}

	public String getCoreCompetencyList() {
		return coreCompetencyList;
	}

	public void setCoreCompetencyList(String coreCompetencyList) {
		this.coreCompetencyList = coreCompetencyList;
	}

	public String getTechniciansList() {
		return techniciansList;
	}

	public void setTechniciansList(String techniciansList) {
		this.techniciansList = techniciansList;
	}

	public String getCsvExportStatus() {
		return csvExportStatus;
	}

	public void setCsvExportStatus(String csvExportStatus) {
		this.csvExportStatus = csvExportStatus;
	}

	private String csvExportStatus;

	private String onTimePercentage = null;

	public String getOnTimePercentage() {
		return onTimePercentage;
	}

	public void setOnTimePercentage(String onTimePercentage) {
		this.onTimePercentage = onTimePercentage;
	}

	public String getPartnerPriCellPhonePlain() {
		return partnerPriCellPhonePlain;
	}

	public void setPartnerPriCellPhonePlain(String partnerPriCellPhonePlain) {
		this.partnerPriCellPhonePlain = partnerPriCellPhonePlain;
	}

	public String[] getRecommendedArr() {
		return recommendedArr;
	}

	public void setRecommendedArr(String[] recommendedArr) {
		this.recommendedArr = recommendedArr;
	}

	public String[] getRestrictedArr() {
		return restrictedArr;
	}

	public void setRestrictedArr(String[] restrictedArr) {
		this.restrictedArr = restrictedArr;
	}

	public String getRecommendedList() {
		return recommendedList;
	}

	public void setRecommendedList(String recommendedList) {
		this.recommendedList = recommendedList;
	}

	public String getRestrictedList() {
		return restrictedList;
	}

	public void setRestrictedList(String restrictedList) {
		this.restrictedList = restrictedList;
	}

	public String[] getToolKitArr() {
		return toolKitArr;
	}

	public void setToolKitArr(String[] toolKitArr) {
		this.toolKitArr = toolKitArr;
	}

	public String getCityTemp() {
		return cityTemp;
	}

	public void setCityTemp(String cityTemp) {
		this.cityTemp = cityTemp;
	}

	public String getPartnernameTemp() {
		return partnernameTemp;
	}

	public void setPartnernameTemp(String partnernameTemp) {
		this.partnernameTemp = partnernameTemp;
	}

	public String getPartnerPriCellPhoneTemp() {
		return partnerPriCellPhoneTemp;
	}

	public void setPartnerPriCellPhoneTemp(String partnerPriCellPhoneTemp) {
		this.partnerPriCellPhoneTemp = partnerPriCellPhoneTemp;
	}

	public String getCpdBadgeId() {
		return cpdBadgeId;
	}

	public void setCpdBadgeId(String cpdBadgeId) {
		this.cpdBadgeId = cpdBadgeId;
	}

	public String getToolkitlist() {
		return toolkitlist;
	}

	public void setToolkitlist(String toolkitlist) {
		this.toolkitlist = toolkitlist;
	}

	public ArrayList<String> getToollist() {
		return toollist;
	}

	public void setToollist(ArrayList<String> toollist) {
		this.toollist = toollist;
	}

	public String getCbox3() {
		return cbox3;
	}

	public void setCbox3(String cbox3) {
		this.cbox3 = cbox3;
	}

	public String getCbox4() {
		return cbox4;
	}
	public void setCbox4(String cbox4) {
		this.cbox4 = cbox4;
	}
	public String getCboxDrug() {
		return cboxDrug;
	}
	public void setCboxDrug(String cboxDrug) {
		this.cboxDrug = cboxDrug;
	}
}
