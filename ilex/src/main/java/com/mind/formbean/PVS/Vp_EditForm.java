package com.mind.formbean.PVS;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 76 fields on this form:
 * <ul>
 * <li>secFirstName - [your comment here]
 * <li>displayName - [your comment here]
 * <li>cmboxState - [your comment here]
 * <li>validFromWC - [your comment here]
 * <li>validToWC - [your comment here]
 * <li>projectCode - [your comment here]
 * <li>contactDate - [your comment here]
 * <li>prmEmail - [your comment here]
 * <li>afterHoursEmail - [your comment here]
 * <li>blousesQuantity - [your comment here]
 * <li>mainFax - [your comment here]
 * <li>rdoW9 - [your comment here]
 * <li>secLastName - [your comment here]
 * <li>secExt - [your comment here]
 * <li>validFromIT - [your comment here]
 * <li>validToIT - [your comment here]
 * <li>engZip - [your comment here]
 * <li>cmboxResourceLevel - [your comment here]
 * <li>partnerCandidate - [your comment here]
 * <li>amountAutomobile - [your comment here]
 * <li>secEmail - [your comment here]
 * <li>cmboxPartnerLevel - [your comment here]
 * <li>badgesQuantity - [your comment here]
 * <li>taxR2 - [your comment here]
 * <li>taxR1 - [your comment here]
 * <li>chkboxTools - [your comment here]
 * <li>cmboxContractStatus - [your comment here]
 * <li>secMobilePhone - [your comment here]
 * <li>policyNumber - [your comment here]
 * <li>txareaCertifications - [your comment here]
 * <li>dateInc - [your comment here]
 * <li>rdoMCSA - [your comment here]
 * <li>datePosted - [your comment here]
 * <li>cmboxStatus - [your comment here]
 * <li>amountUmbrella - [your comment here]
 * <li>cmboxCoreCompetency - [your comment here]
 * <li>newPassword - [your comment here]
 * <li>engName - [your comment here]
 * <li>confirmPassword - [your comment here]
 * <li>companyURL - [your comment here]
 * <li>truckLogoQuantity - [your comment here]
 * <li>rdoWorkmens - [your comment here]
 * <li>zipCodeECDF - [your comment here]
 * <li>taxId - [your comment here]
 * <li>stateR2 - [your comment here]
 * <li>zip - [your comment here]
 * <li>stateR1 - [your comment here]
 * <li>validFromTA - [your comment here]
 * <li>validToTA - [your comment here]
 * <li>toolZips - [your comment here]
 * <li>prmExt - [your comment here]
 * <li>prmLastName - [your comment here]
 * <li>mainPhone - [your comment here]
 * <li>rdoUnion - [your comment here]
 * <li>address2 - [your comment here]
 * <li>amountGeneral - [your comment here]
 * <li>address1 - [your comment here]
 * <li>carrier - [your comment here]
 * <li>chkboxEquipped - [your comment here]
 * <li>country - [your comment here]
 * <li>prmPhone - [your comment here]
 * <li>afterHoursPhone - [your comment here]
 * <li>validFromSD - [your comment here]
 * <li>regID - [your comment here]
 * <li>validToSD - [your comment here]
 * <li>prmMobilePhone - [your comment here]
 * <li>prmFirstName - [your comment here]
 * <li>rdoInsurance - [your comment here]
 * <li>chkzipECDF - [your comment here]
 * <li>cmboxHighCriticality - [your comment here]
 * <li>city - [your comment here]
 * <li>programPackage - [your comment here]
 * <li>secPhone - [your comment here]
 * <li>contractDate - [your comment here]
 * <li>companyType - [your comment here]
 * <li>addMore - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class Vp_EditForm extends ActionForm {

	private String secFirstName = null;
	private String[] cmboxState = null;
	private String displayName = null;
	private String validFromWC = null;
	private String validToWC = null;
	private String projectCode = null;
	private String contactDate = null;
	private String prmEmail = null;
	private String afterHoursEmail = null;
	private String blousesQuantity = null;
	private String mainFax = null;
	private String rdoW9 = null;
	private String secLastName = null;
	private String secExt = null;
	private String validFromIT = null;
	private String validToIT = null;
	private String engZip = null;
	private String[] cmboxResourceLevel = null;
	private String partnerCandidate = null;
	private String amountAutomobile = null;
	private String secEmail = null;
	private String[] cmboxPartnerLevel = null;
	private String badgesQuantity = null;
	private String taxR2 = null;
	private String taxR1 = null;
	private String chkboxTools = null;
	private String[] cmboxContractStatus = null;
	private String secMobilePhone = null;
	private String policyNumber = null;
	//private String txareaCertifications = null;
	private String dateInc = null;
	private String rdoMCSA = null;
	private String datePosted = null;
	private String[] cmboxStatus = null;
	private String amountUmbrella = null;
	private String[] cmboxCoreCompetency = null;
	private String engName = null;
	private String newPassword = null;
	private String companyURL = null;
	private String confirmPassword = null;
	private String truckLogoQuantity = null;
	private String rdoWorkmens = null;
	private String zipCodeECDF = null;
	private String taxId = null;
	private String stateR2 = null;
	private String zip = null;
	private String stateR1 = null;
	private String validFromTA = null;
	private String validToTA = null;
	private String toolZips = null;
	private String prmExt = null;
	private String prmLastName = null;
	private String mainPhone = null;
	private String rdoUnion = null;
	private String address2 = null;
	private String amountGeneral = null;
	private String address1 = null;
	private String carrier = null;
	private String chkboxEquipped = null;
	private String country = null;
	private String prmPhone = null;
	private String afterHoursPhone = null;
	private String validFromSD = null;
	private String regID = null;
	private String validToSD = null;
	private String prmMobilePhone = null;
	private String prmFirstName = null;
	private String rdoInsurance = null;
	private String chkzipECDF = null;
	private String[] cmboxHighCriticality = null;
	private String city = null;
	private String programPackage = null;
	private String secPhone = null;
	private String contractDate = null;
	private String companyType = null;
	private String addMore = null;
	private String[] cmboxCertifications = null;

	public String[] getCmboxCertifications() {
		return cmboxCertifications;
	}

	public void setCmboxCertifications(String[] cmboxCertifications) {
		this.cmboxCertifications = cmboxCertifications;
	}

	/**
	 * Get secFirstName
	 * @return String
	 */
	public String getSecFirstName() {
		return secFirstName;
	}

	/**
	 * Set secFirstName
	 * @param <code>String</code>
	 */
	public void setSecFirstName(String s) {
		this.secFirstName = s;
	}

	/**
	 * Get cmboxState
	 * @return String[]
	 */
	public String[] getCmboxState() {
		return cmboxState;
	}

	/**
	 * Set cmboxState
	 * @param <code>String[]</code>
	 */
	public void setCmboxState(String[] c) {
		this.cmboxState = c;
	}

	/**
	 * Get displayName
	 * @return String
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Set displayName
	 * @param <code>String</code>
	 */
	public void setDisplayName(String d) {
		this.displayName = d;
	}

	/**
	 * Get validFromWC
	 * @return String
	 */
	public String getValidFromWC() {
		return validFromWC;
	}

	/**
	 * Set validFromWC
	 * @param <code>String</code>
	 */
	public void setValidFromWC(String v) {
		this.validFromWC = v;
	}

	/**
	 * Get validToWC
	 * @return String
	 */
	public String getValidToWC() {
		return validToWC;
	}

	/**
	 * Set validToWC
	 * @param <code>String</code>
	 */
	public void setValidToWC(String v) {
		this.validToWC = v;
	}

	/**
	 * Get projectCode
	 * @return String
	 */
	public String getProjectCode() {
		return projectCode;
	}

	/**
	 * Set projectCode
	 * @param <code>String</code>
	 */
	public void setProjectCode(String p) {
		this.projectCode = p;
	}

	/**
	 * Get contactDate
	 * @return String
	 */
	public String getContactDate() {
		return contactDate;
	}

	/**
	 * Set contactDate
	 * @param <code>String</code>
	 */
	public void setContactDate(String c) {
		this.contactDate = c;
	}

	/**
	 * Get prmEmail
	 * @return String
	 */
	public String getPrmEmail() {
		return prmEmail;
	}

	/**
	 * Set prmEmail
	 * @param <code>String</code>
	 */
	public void setPrmEmail(String p) {
		this.prmEmail = p;
	}

	/**
	 * Get afterHoursEmail
	 * @return String
	 */
	public String getAfterHoursEmail() {
		return afterHoursEmail;
	}

	/**
	 * Set afterHoursEmail
	 * @param <code>String</code>
	 */
	public void setAfterHoursEmail(String a) {
		this.afterHoursEmail = a;
	}

	/**
	 * Get blousesQuantity
	 * @return String
	 */
	public String getBlousesQuantity() {
		return blousesQuantity;
	}

	/**
	 * Set blousesQuantity
	 * @param <code>String</code>
	 */
	public void setBlousesQuantity(String b) {
		this.blousesQuantity = b;
	}

	/**
	 * Get mainFax
	 * @return String
	 */
	public String getMainFax() {
		return mainFax;
	}

	/**
	 * Set mainFax
	 * @param <code>String</code>
	 */
	public void setMainFax(String m) {
		this.mainFax = m;
	}

	/**
	 * Get rdoW9
	 * @return String
	 */
	public String getRdoW9() {
		return rdoW9;
	}

	/**
	 * Set rdoW9
	 * @param <code>String</code>
	 */
	public void setRdoW9(String r) {
		this.rdoW9 = r;
	}

	/**
	 * Get secLastName
	 * @return String
	 */
	public String getSecLastName() {
		return secLastName;
	}

	/**
	 * Set secLastName
	 * @param <code>String</code>
	 */
	public void setSecLastName(String s) {
		this.secLastName = s;
	}

	/**
	 * Get secExt
	 * @return String
	 */
	public String getSecExt() {
		return secExt;
	}

	/**
	 * Set secExt
	 * @param <code>String</code>
	 */
	public void setSecExt(String s) {
		this.secExt = s;
	}

	/**
	 * Get validFromIT
	 * @return String
	 */
	public String getValidFromIT() {
		return validFromIT;
	}

	/**
	 * Set validFromIT
	 * @param <code>String</code>
	 */
	public void setValidFromIT(String v) {
		this.validFromIT = v;
	}

	/**
	 * Get validToIT
	 * @return String
	 */
	public String getValidToIT() {
		return validToIT;
	}

	/**
	 * Set validToIT
	 * @param <code>String</code>
	 */
	public void setValidToIT(String v) {
		this.validToIT = v;
	}

	/**
	 * Get engZip
	 * @return String
	 */
	public String getEngZip() {
		return engZip;
	}

	/**
	 * Set engZip
	 * @param <code>String</code>
	 */
	public void setEngZip(String e) {
		this.engZip = e;
	}

	/**
	 * Get cmboxResourceLevel
	 * @return String[]
	 */
	public String[] getCmboxResourceLevel() {
		return cmboxResourceLevel;
	}

	/**
	 * Set cmboxResourceLevel
	 * @param <code>String[]</code>
	 */
	public void setCmboxResourceLevel(String[] c) {
		this.cmboxResourceLevel = c;
	}

	/**
	 * Get partnerCandidate
	 * @return String
	 */
	public String getPartnerCandidate() {
		return partnerCandidate;
	}

	/**
	 * Set partnerCandidate
	 * @param <code>String</code>
	 */
	public void setPartnerCandidate(String p) {
		this.partnerCandidate = p;
	}

	/**
	 * Get amountAutomobile
	 * @return String
	 */
	public String getAmountAutomobile() {
		return amountAutomobile;
	}

	/**
	 * Set amountAutomobile
	 * @param <code>String</code>
	 */
	public void setAmountAutomobile(String a) {
		this.amountAutomobile = a;
	}

	/**
	 * Get secEmail
	 * @return String
	 */
	public String getSecEmail() {
		return secEmail;
	}

	/**
	 * Set secEmail
	 * @param <code>String</code>
	 */
	public void setSecEmail(String s) {
		this.secEmail = s;
	}

	/**
	 * Get cmboxPartnerLevel
	 * @return String[]
	 */
	public String[] getCmboxPartnerLevel() {
		return cmboxPartnerLevel;
	}

	/**
	 * Set cmboxPartnerLevel
	 * @param <code>String[]</code>
	 */
	public void setCmboxPartnerLevel(String[] c) {
		this.cmboxPartnerLevel = c;
	}

	/**
	 * Get badgesQuantity
	 * @return String
	 */
	public String getBadgesQuantity() {
		return badgesQuantity;
	}

	/**
	 * Set badgesQuantity
	 * @param <code>String</code>
	 */
	public void setBadgesQuantity(String b) {
		this.badgesQuantity = b;
	}

	/**
	 * Get taxR2
	 * @return String
	 */
	public String getTaxR2() {
		return taxR2;
	}

	/**
	 * Set taxR2
	 * @param <code>String</code>
	 */
	public void setTaxR2(String t) {
		this.taxR2 = t;
	}

	/**
	 * Get taxR1
	 * @return String
	 */
	public String getTaxR1() {
		return taxR1;
	}

	/**
	 * Set taxR1
	 * @param <code>String</code>
	 */
	public void setTaxR1(String t) {
		this.taxR1 = t;
	}

	/**
	 * Get chkboxTools
	 * @return String
	 */
	public String getChkboxTools() {
		return chkboxTools;
	}

	/**
	 * Set chkboxTools
	 * @param <code>String</code>
	 */
	public void setChkboxTools(String c) {
		this.chkboxTools = c;
	}

	/**
	 * Get cmboxContractStatus
	 * @return String[]
	 */
	public String[] getCmboxContractStatus() {
		return cmboxContractStatus;
	}

	/**
	 * Set cmboxContractStatus
	 * @param <code>String[]</code>
	 */
	public void setCmboxContractStatus(String[] c) {
		this.cmboxContractStatus = c;
	}

	/**
	 * Get secMobilePhone
	 * @return String
	 */
	public String getSecMobilePhone() {
		return secMobilePhone;
	}

	/**
	 * Set secMobilePhone
	 * @param <code>String</code>
	 */
	public void setSecMobilePhone(String s) {
		this.secMobilePhone = s;
	}

	/**
	 * Get policyNumber
	 * @return String
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * Set policyNumber
	 * @param <code>String</code>
	 */
	public void setPolicyNumber(String p) {
		this.policyNumber = p;
	}

	
	/**
	 * Get dateInc
	 * @return String
	 */
	public String getDateInc() {
		return dateInc;
	}

	/**
	 * Set dateInc
	 * @param <code>String</code>
	 */
	public void setDateInc(String d) {
		this.dateInc = d;
	}

	/**
	 * Get rdoMCSA
	 * @return String
	 */
	public String getRdoMCSA() {
		return rdoMCSA;
	}

	/**
	 * Set rdoMCSA
	 * @param <code>String</code>
	 */
	public void setRdoMCSA(String r) {
		this.rdoMCSA = r;
	}

	/**
	 * Get datePosted
	 * @return String
	 */
	public String getDatePosted() {
		return datePosted;
	}

	/**
	 * Set datePosted
	 * @param <code>String</code>
	 */
	public void setDatePosted(String d) {
		this.datePosted = d;
	}

	/**
	 * Get cmboxStatus
	 * @return String[]
	 */
	public String[] getCmboxStatus() {
		return cmboxStatus;
	}

	/**
	 * Set cmboxStatus
	 * @param <code>String[]</code>
	 */
	public void setCmboxStatus(String[] c) {
		this.cmboxStatus = c;
	}

	/**
	 * Get amountUmbrella
	 * @return String
	 */
	public String getAmountUmbrella() {
		return amountUmbrella;
	}

	/**
	 * Set amountUmbrella
	 * @param <code>String</code>
	 */
	public void setAmountUmbrella(String a) {
		this.amountUmbrella = a;
	}

	/**
	 * Get cmboxCoreCompetency
	 * @return String[]
	 */
	public String[] getCmboxCoreCompetency() {
		return cmboxCoreCompetency;
	}

	/**
	 * Set cmboxCoreCompetency
	 * @param <code>String[]</code>
	 */
	public void setCmboxCoreCompetency(String[] c) {
		this.cmboxCoreCompetency = c;
	}

	/**
	 * Get engName
	 * @return String
	 */
	public String getEngName() {
		return engName;
	}

	/**
	 * Set engName
	 * @param <code>String</code>
	 */
	public void setEngName(String e) {
		this.engName = e;
	}

	/**
	 * Get newPassword
	 * @return String
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * Set newPassword
	 * @param <code>String</code>
	 */
	public void setNewPassword(String n) {
		this.newPassword = n;
	}

	/**
	 * Get companyURL
	 * @return String
	 */
	public String getCompanyURL() {
		return companyURL;
	}

	/**
	 * Set companyURL
	 * @param <code>String</code>
	 */
	public void setCompanyURL(String c) {
		this.companyURL = c;
	}

	/**
	 * Get confirmPassword
	 * @return String
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * Set confirmPassword
	 * @param <code>String</code>
	 */
	public void setConfirmPassword(String c) {
		this.confirmPassword = c;
	}

	/**
	 * Get truckLogoQuantity
	 * @return String
	 */
	public String getTruckLogoQuantity() {
		return truckLogoQuantity;
	}

	/**
	 * Set truckLogoQuantity
	 * @param <code>String</code>
	 */
	public void setTruckLogoQuantity(String t) {
		this.truckLogoQuantity = t;
	}

	/**
	 * Get rdoWorkmens
	 * @return String
	 */
	public String getRdoWorkmens() {
		return rdoWorkmens;
	}

	/**
	 * Set rdoWorkmens
	 * @param <code>String</code>
	 */
	public void setRdoWorkmens(String r) {
		this.rdoWorkmens = r;
	}

	/**
	 * Get zipCodeECDF
	 * @return String
	 */
	public String getZipCodeECDF() {
		return zipCodeECDF;
	}

	/**
	 * Set zipCodeECDF
	 * @param <code>String</code>
	 */
	public void setZipCodeECDF(String z) {
		this.zipCodeECDF = z;
	}

	/**
	 * Get taxId
	 * @return String
	 */
	public String getTaxId() {
		return taxId;
	}

	/**
	 * Set taxId
	 * @param <code>String</code>
	 */
	public void setTaxId(String t) {
		this.taxId = t;
	}

	/**
	 * Get stateR2
	 * @return String
	 */
	public String getStateR2() {
		return stateR2;
	}

	/**
	 * Set stateR2
	 * @param <code>String</code>
	 */
	public void setStateR2(String s) {
		this.stateR2 = s;
	}

	/**
	 * Get zip
	 * @return String
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * Set zip
	 * @param <code>String</code>
	 */
	public void setZip(String z) {
		this.zip = z;
	}

	/**
	 * Get stateR1
	 * @return String
	 */
	public String getStateR1() {
		return stateR1;
	}

	/**
	 * Set stateR1
	 * @param <code>String</code>
	 */
	public void setStateR1(String s) {
		this.stateR1 = s;
	}

	/**
	 * Get validFromTA
	 * @return String
	 */
	public String getValidFromTA() {
		return validFromTA;
	}

	/**
	 * Set validFromTA
	 * @param <code>String</code>
	 */
	public void setValidFromTA(String v) {
		this.validFromTA = v;
	}

	/**
	 * Get validToTA
	 * @return String
	 */
	public String getValidToTA() {
		return validToTA;
	}

	/**
	 * Set validToTA
	 * @param <code>String</code>
	 */
	public void setValidToTA(String v) {
		this.validToTA = v;
	}

	/**
	 * Get toolZips
	 * @return String
	 */
	public String getToolZips() {
		return toolZips;
	}

	/**
	 * Set toolZips
	 * @param <code>String</code>
	 */
	public void setToolZips(String t) {
		this.toolZips = t;
	}

	/**
	 * Get prmExt
	 * @return String
	 */
	public String getPrmExt() {
		return prmExt;
	}

	/**
	 * Set prmExt
	 * @param <code>String</code>
	 */
	public void setPrmExt(String p) {
		this.prmExt = p;
	}

	/**
	 * Get prmLastName
	 * @return String
	 */
	public String getPrmLastName() {
		return prmLastName;
	}

	/**
	 * Set prmLastName
	 * @param <code>String</code>
	 */
	public void setPrmLastName(String p) {
		this.prmLastName = p;
	}

	/**
	 * Get mainPhone
	 * @return String
	 */
	public String getMainPhone() {
		return mainPhone;
	}

	/**
	 * Set mainPhone
	 * @param <code>String</code>
	 */
	public void setMainPhone(String m) {
		this.mainPhone = m;
	}

	/**
	 * Get rdoUnion
	 * @return String
	 */
	public String getRdoUnion() {
		return rdoUnion;
	}

	/**
	 * Set rdoUnion
	 * @param <code>String</code>
	 */
	public void setRdoUnion(String r) {
		this.rdoUnion = r;
	}

	/**
	 * Get address2
	 * @return String
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * Set address2
	 * @param <code>String</code>
	 */
	public void setAddress2(String a) {
		this.address2 = a;
	}

	/**
	 * Get amountGeneral
	 * @return String
	 */
	public String getAmountGeneral() {
		return amountGeneral;
	}

	/**
	 * Set amountGeneral
	 * @param <code>String</code>
	 */
	public void setAmountGeneral(String a) {
		this.amountGeneral = a;
	}

	/**
	 * Get address1
	 * @return String
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * Set address1
	 * @param <code>String</code>
	 */
	public void setAddress1(String a) {
		this.address1 = a;
	}

	/**
	 * Get carrier
	 * @return String
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * Set carrier
	 * @param <code>String</code>
	 */
	public void setCarrier(String c) {
		this.carrier = c;
	}

	/**
	 * Get chkboxEquipped
	 * @return String
	 */
	public String getChkboxEquipped() {
		return chkboxEquipped;
	}

	/**
	 * Set chkboxEquipped
	 * @param <code>String</code>
	 */
	public void setChkboxEquipped(String c) {
		this.chkboxEquipped = c;
	}

	/**
	 * Get country
	 * @return String
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Set country
	 * @param <code>String</code>
	 */
	public void setCountry(String c) {
		this.country = c;
	}

	/**
	 * Get prmPhone
	 * @return String
	 */
	public String getPrmPhone() {
		return prmPhone;
	}

	/**
	 * Set prmPhone
	 * @param <code>String</code>
	 */
	public void setPrmPhone(String p) {
		this.prmPhone = p;
	}

	/**
	 * Get afterHoursPhone
	 * @return String
	 */
	public String getAfterHoursPhone() {
		return afterHoursPhone;
	}

	/**
	 * Set afterHoursPhone
	 * @param <code>String</code>
	 */
	public void setAfterHoursPhone(String a) {
		this.afterHoursPhone = a;
	}

	/**
	 * Get validFromSD
	 * @return String
	 */
	public String getValidFromSD() {
		return validFromSD;
	}

	/**
	 * Set validFromSD
	 * @param <code>String</code>
	 */
	public void setValidFromSD(String v) {
		this.validFromSD = v;
	}

	/**
	 * Get regID
	 * @return String
	 */
	public String getRegID() {
		return regID;
	}

	/**
	 * Set regID
	 * @param <code>String</code>
	 */
	public void setRegID(String r) {
		this.regID = r;
	}

	/**
	 * Get validToSD
	 * @return String
	 */
	public String getValidToSD() {
		return validToSD;
	}

	/**
	 * Set validToSD
	 * @param <code>String</code>
	 */
	public void setValidToSD(String v) {
		this.validToSD = v;
	}

	/**
	 * Get prmMobilePhone
	 * @return String
	 */
	public String getPrmMobilePhone() {
		return prmMobilePhone;
	}

	/**
	 * Set prmMobilePhone
	 * @param <code>String</code>
	 */
	public void setPrmMobilePhone(String p) {
		this.prmMobilePhone = p;
	}

	/**
	 * Get prmFirstName
	 * @return String
	 */
	public String getPrmFirstName() {
		return prmFirstName;
	}

	/**
	 * Set prmFirstName
	 * @param <code>String</code>
	 */
	public void setPrmFirstName(String p) {
		this.prmFirstName = p;
	}

	/**
	 * Get rdoInsurance
	 * @return String
	 */
	public String getRdoInsurance() {
		return rdoInsurance;
	}

	/**
	 * Set rdoInsurance
	 * @param <code>String</code>
	 */
	public void setRdoInsurance(String r) {
		this.rdoInsurance = r;
	}

	/**
	 * Get chkzipECDF
	 * @return String
	 */
	public String getChkzipECDF() {
		return chkzipECDF;
	}

	/**
	 * Set chkzipECDF
	 * @param <code>String</code>
	 */
	public void setChkzipECDF(String c) {
		this.chkzipECDF = c;
	}

	/**
	 * Get cmboxHighCriticality
	 * @return String[]
	 */
	public String[] getCmboxHighCriticality() {
		return cmboxHighCriticality;
	}

	/**
	 * Set cmboxHighCriticality
	 * @param <code>String[]</code>
	 */
	public void setCmboxHighCriticality(String[] c) {
		this.cmboxHighCriticality = c;
	}

	/**
	 * Get city
	 * @return String
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Set city
	 * @param <code>String</code>
	 */
	public void setCity(String c) {
		this.city = c;
	}

	/**
	 * Get programPackage
	 * @return String
	 */
	public String getProgramPackage() {
		return programPackage;
	}

	/**
	 * Set programPackage
	 * @param <code>String</code>
	 */
	public void setProgramPackage(String p) {
		this.programPackage = p;
	}

	/**
	 * Get secPhone
	 * @return String
	 */
	public String getSecPhone() {
		return secPhone;
	}

	/**
	 * Set secPhone
	 * @param <code>String</code>
	 */
	public void setSecPhone(String s) {
		this.secPhone = s;
	}

	/**
	 * Get contractDate
	 * @return String
	 */
	public String getContractDate() {
		return contractDate;
	}

	/**
	 * Set contractDate
	 * @param <code>String</code>
	 */
	public void setContractDate(String c) {
		this.contractDate = c;
	}

	/**
	 * Get companyType
	 * @return String
	 */
	public String getCompanyType() {
		return companyType;
	}

	/**
	 * Set companyType
	 * @param <code>String</code>
	 */
	public void setCompanyType(String c) {
		this.companyType = c;
	}

	/**
	 * Get addMore
	 * @return String
	 */
	public String getAddMore() {
		return addMore;
	}

	/**
	 * Set addMore
	 * @param <code>String</code>
	 */
	public void setAddMore(String a) {
		this.addMore = a;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.

		secFirstName = null;
		cmboxState = null;
		displayName = null;
		validFromWC = null;
		validToWC = null;
		projectCode = null;
		contactDate = null;
		prmEmail = null;
		afterHoursEmail = null;
		blousesQuantity = null;
		mainFax = null;
		rdoW9 = null;
		secLastName = null;
		secExt = null;
		validFromIT = null;
		validToIT = null;
		engZip = null;
		cmboxResourceLevel = null;
		partnerCandidate = null;
		amountAutomobile = null;
		secEmail = null;
		cmboxPartnerLevel = null;
		badgesQuantity = null;
		taxR2 = null;
		taxR1 = null;
		chkboxTools = null;
		cmboxContractStatus = null;
		secMobilePhone = null;
		policyNumber = null;
		cmboxCertifications = null;
		dateInc = null;
		rdoMCSA = null;
		datePosted = null;
		cmboxStatus = null;
		amountUmbrella = null;
		cmboxCoreCompetency = null;
		engName = null;
		newPassword = null;
		companyURL = null;
		confirmPassword = null;
		truckLogoQuantity = null;
		rdoWorkmens = null;
		zipCodeECDF = null;
		taxId = null;
		stateR2 = null;
		zip = null;
		stateR1 = null;
		validFromTA = null;
		validToTA = null;
		toolZips = null;
		prmExt = null;
		prmLastName = null;
		mainPhone = null;
		rdoUnion = null;
		address2 = null;
		amountGeneral = null;
		address1 = null;
		carrier = null;
		chkboxEquipped = null;
		country = null;
		prmPhone = null;
		afterHoursPhone = null;
		validFromSD = null;
		regID = null;
		validToSD = null;
		prmMobilePhone = null;
		prmFirstName = null;
		rdoInsurance = null;
		chkzipECDF = null;
		cmboxHighCriticality = null;
		city = null;
		programPackage = null;
		secPhone = null;
		contractDate = null;
		companyType = null;
		addMore = null;

	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.

		// if ((field == null) || (field.length() == 0)) {
		//   errors.add("field", new org.apache.struts.action.ActionError("error.field.required"));
		// }
		return errors;

	}
}
