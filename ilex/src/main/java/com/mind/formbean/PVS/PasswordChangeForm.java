package com.mind.formbean.PVS;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;




public class PasswordChangeForm extends ActionForm {
	
	private String pid=null;
	private String oldPassword=null;
	private String newPassword=null;
	private String confirmPassword=null;
	private String save=null;
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request){		
		ActionErrors errors=new ActionErrors();		
		/*if (save==null)return errors;
		
		if( newPassword.equals(oldPassword))
			errors.add("confirmPassword",new ActionError("errors.passwordchange.newpassword.equal"));
		
		if(! newPassword.equals(confirmPassword))
			errors.add("confirmPassword",new ActionError("errors.passwordchange.confirmpassword.notequal"));*/
		return errors;		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		pid=null;
		oldPassword=null;
		newPassword=null;
		confirmPassword=null;
		save=null;		
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

}
