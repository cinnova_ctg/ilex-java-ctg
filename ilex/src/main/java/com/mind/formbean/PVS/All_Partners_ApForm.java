package com.mind.formbean.PVS;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 11 fields on this form:
 * <ul>
 * <li>cbox2 - [your comment here]
 * <li>cbox1 - [your comment here]
 * <li>name - [your comment here]
 * <li>cmboxpartnertype - [your comment here]
 * <li>reset - [your comment here]
 * <li>keyword - [your comment here]
 * <li>radius - [your comment here]
 * <li>cmboxcertifications - [your comment here]
 * <li>zipcode - [your comment here]
 * <li>search - [your comment here]
 * <li>cmboxcorecompetencies - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class All_Partners_ApForm extends ActionForm {

	private String cbox2 = null;
	private String name = null;
	private String cbox1 = null;
	private String[] cmboxpartnertype = null;
	private String reset = null;
	private String keyword = null;
	private String radius = null;
	private String zipcode = null;
	private String[] cmboxcertifications = null;
	private String[] cmboxcorecompetencies = null;
	private String search = null;

	/**
	 * Get cbox2
	 * @return String
	 */
	public String getCbox2() {
		return cbox2;
	}

	/**
	 * Set cbox2
	 * @param <code>String</code>
	 */
	public void setCbox2(String c) {
		this.cbox2 = c;
	}

	/**
	 * Get name
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set name
	 * @param <code>String</code>
	 */
	public void setName(String n) {
		this.name = n;
	}

	/**
	 * Get cbox1
	 * @return String
	 */
	public String getCbox1() {
		return cbox1;
	}

	/**
	 * Set cbox1
	 * @param <code>String</code>
	 */
	public void setCbox1(String c) {
		this.cbox1 = c;
	}

	/**
	 * Get cmboxpartnertype
	 * @return String[]
	 */
	public String[] getCmboxpartnertype() {
		return cmboxpartnertype;
	}

	/**
	 * Set cmboxpartnertype
	 * @param <code>String[]</code>
	 */
	public void setCmboxpartnertype(String[] c) {
		this.cmboxpartnertype = c;
	}

	/**
	 * Get reset
	 * @return String
	 */
	public String getReset() {
		return reset;
	}

	/**
	 * Set reset
	 * @param <code>String</code>
	 */
	public void setReset(String r) {
		this.reset = r;
	}

	/**
	 * Get keyword
	 * @return String
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * Set keyword
	 * @param <code>String</code>
	 */
	public void setKeyword(String k) {
		this.keyword = k;
	}

	/**
	 * Get radius
	 * @return String
	 */
	public String getRadius() {
		return radius;
	}

	/**
	 * Set radius
	 * @param <code>String</code>
	 */
	public void setRadius(String r) {
		this.radius = r;
	}

	/**
	 * Get zipcode
	 * @return String
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * Set zipcode
	 * @param <code>String</code>
	 */
	public void setZipcode(String z) {
		this.zipcode = z;
	}

	/**
	 * Get cmboxcertifications
	 * @return String[]
	 */
	public String[] getCmboxcertifications() {
		return cmboxcertifications;
	}

	/**
	 * Set cmboxcertifications
	 * @param <code>String[]</code>
	 */
	public void setCmboxcertifications(String[] c) {
		this.cmboxcertifications = c;
	}

	/**
	 * Get cmboxcorecompetencies
	 * @return String[]
	 */
	public String[] getCmboxcorecompetencies() {
		return cmboxcorecompetencies;
	}

	/**
	 * Set cmboxcorecompetencies
	 * @param <code>String[]</code>
	 */
	public void setCmboxcorecompetencies(String[] c) {
		this.cmboxcorecompetencies = c;
	}

	/**
	 * Get search
	 * @return String
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * Set search
	 * @param <code>String</code>
	 */
	public void setSearch(String s) {
		this.search = s;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.

		cbox2 = null;
		name = null;
		cbox1 = null;
		cmboxpartnertype = null;
		reset = null;
		keyword = null;
		radius = null;
		zipcode = null;
		cmboxcertifications = null;
		cmboxcorecompetencies = null;
		search = null;

	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.

		// if ((field == null) || (field.length() == 0)) {
		//   errors.add("field", new org.apache.struts.action.ActionError("error.field.required"));
		// }
		return errors;

	}
}
