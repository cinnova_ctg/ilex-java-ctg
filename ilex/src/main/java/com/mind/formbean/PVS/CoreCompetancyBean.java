package com.mind.formbean.PVS;

import org.apache.struts.action.ActionForm;

public class CoreCompetancyBean extends ActionForm {
	private String competancyName;
	private String competancyId;
	private String score;
	private String markDisable;

	public String getCompetancyName() {
		return competancyName;
	}

	public void setCompetancyName(String competancyName) {
		this.competancyName = competancyName;
	}

	public String getCompetancyId() {
		return competancyId;
	}

	public void setCompetancyId(String competancyId) {
		this.competancyId = competancyId;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getMarkDisable() {
		return markDisable;
	}

	public void setMarkDisable(String markDisable) {
		this.markDisable = markDisable;
	}

}
