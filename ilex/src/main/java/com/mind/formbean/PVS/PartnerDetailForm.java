package com.mind.formbean.PVS;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.mind.common.LabelValue;

public class PartnerDetailForm extends ActionForm {

	private String partnerId = null;
	private String partnerName = null;
	private String partnerAddress1 = null;
	private String partnerAddress2 = null;
	private String partnerCity = null;
	private String partnerState = null;
	private String partnerZipCode = null;
	private String partnerPhone = null;
	private String partnerPriName = null;
	private String partnerPriPhone = null;
	private String partnerPriCellPhone = null;
	private String partnerPriEmail = null;
	private String partnerType = null;
	private String partnerCellPhone = null;

	private String badgeName;
	private String status;
	private Date uploadedOn;
	private String uploadedOnString;
	private Date updatedOn;
	private String updatedOnString;
	private String updatedBy;
	private String imgPath;
	private String imgName;

	private String daysRange = null;
	private String timeRange = null;

	private ArrayList<String[]> thumbUpDownList = new ArrayList<String[]>();

	ArrayList<LabelValue> imagesList = new ArrayList<LabelValue>();

	public String getPartnerType() {
		return partnerType;
	}

	public ArrayList<LabelValue> getImagesList() {
		return imagesList;
	}

	public void setImagesList(ArrayList<LabelValue> imagesList) {
		this.imagesList = imagesList;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public void reset(ActionMapping arg0, HttpServletRequest arg1) {
		partnerId = null;
		partnerName = null;
		partnerAddress1 = null;
		partnerAddress2 = null;
		partnerCity = null;
		partnerState = null;
		partnerZipCode = null;
		partnerPhone = null;
		partnerPriName = null;
		partnerPriPhone = null;
		partnerPriCellPhone = null;
		partnerPriEmail = null;
		partnerType = null;
	}

	public String getPartnerAddress1() {
		return partnerAddress1;
	}

	public void setPartnerAddress1(String partnerAddress1) {
		this.partnerAddress1 = partnerAddress1;
	}

	public String getPartnerAddress2() {
		return partnerAddress2;
	}

	public void setPartnerAddress2(String partnerAddress2) {
		this.partnerAddress2 = partnerAddress2;
	}

	public String getPartnerCity() {
		return partnerCity;
	}

	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerPhone() {
		return partnerPhone;
	}

	public void setPartnerPhone(String partnerPhone) {
		this.partnerPhone = partnerPhone;
	}

	public String getPartnerPriCellPhone() {
		return partnerPriCellPhone;
	}

	public void setPartnerPriCellPhone(String partnerPriCellPhone) {
		this.partnerPriCellPhone = partnerPriCellPhone;
	}

	public String getPartnerPriEmail() {
		return partnerPriEmail;
	}

	public void setPartnerPriEmail(String partnerPriEmail) {
		this.partnerPriEmail = partnerPriEmail;
	}

	public String getPartnerPriName() {
		return partnerPriName;
	}

	public void setPartnerPriName(String partnerPriName) {
		this.partnerPriName = partnerPriName;
	}

	public String getPartnerPriPhone() {
		return partnerPriPhone;
	}

	public void setPartnerPriPhone(String partnerPriPhone) {
		this.partnerPriPhone = partnerPriPhone;
	}

	public String getPartnerZipCode() {
		return partnerZipCode;
	}

	public void setPartnerZipCode(String partnerZipCode) {
		this.partnerZipCode = partnerZipCode;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerState() {
		return partnerState;
	}

	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}

	public String getPartnerCellPhone() {
		return partnerCellPhone;
	}

	public void setPartnerCellPhone(String partnerCellPhone) {
		this.partnerCellPhone = partnerCellPhone;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public String getDaysRange() {
		return daysRange;
	}

	public void setDaysRange(String daysRange) {
		this.daysRange = daysRange;
	}

	public String getBadgeName() {
		return badgeName;
	}

	public void setBadgeName(String badgeName) {
		this.badgeName = badgeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public String getUploadedOnString() {
		return uploadedOnString;
	}

	public void setUploadedOnString(String uploadedOnString) {
		this.uploadedOnString = uploadedOnString;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedOnString() {
		return updatedOnString;
	}

	public void setUpdatedOnString(String updatedOnString) {
		this.updatedOnString = updatedOnString;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public ArrayList<String[]> getThumbUpDownList() {
		return thumbUpDownList;
	}

	public void setThumbUpDownList(ArrayList<String[]> thumbUpDownList) {
		this.thumbUpDownList = thumbUpDownList;
	}

}
