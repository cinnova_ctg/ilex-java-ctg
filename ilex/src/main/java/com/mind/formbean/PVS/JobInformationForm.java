package com.mind.formbean.PVS;

import org.apache.struts.action.ActionForm;

public class JobInformationForm extends ActionForm{
	
	
	private String partnerid = null;
	private String customername = null;
	private String sitenumber = null;
	private String sitename = null;
	private String siteaddress = null;
	private String sitePrimarypoc = null;
	private String siteSecondarypoc = null;
	private String owner = null;
	private String jobstatus = null;
	private String appendixName = null;
	private String tempPartnerid = null;
	private String jobName = null;
	private String from = null;
	
	private String partnerName = null;
	private String partnerCity = null;
	private String partnerZipCode = null;
	private String partnerCountry = null;
	private String partnerState = null;
	private String partnerStatus = null;
	private String partnerType=null;
	
	private String partnerPhoneNo = null;
	
	
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobstatus() {
		return jobstatus;
	}
	public void setJobstatus(String jobstatus) {
		this.jobstatus = jobstatus;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getSiteaddress() {
		return siteaddress;
	}
	public void setSiteaddress(String siteaddress) {
		this.siteaddress = siteaddress;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
	public String getSitenumber() {
		return sitenumber;
	}
	public void setSitenumber(String sitenumber) {
		this.sitenumber = sitenumber;
	}
	public String getSitePrimarypoc() {
		return sitePrimarypoc;
	}
	public void setSitePrimarypoc(String sitePrimarypoc) {
		this.sitePrimarypoc = sitePrimarypoc;
	}
	public String getSiteSecondarypoc() {
		return siteSecondarypoc;
	}
	public void setSiteSecondarypoc(String siteSecondarypoc) {
		this.siteSecondarypoc = siteSecondarypoc;
	}
	public String getTempPartnerid() {
		return tempPartnerid;
	}
	public void setTempPartnerid(String tempPartnerid) {
		this.tempPartnerid = tempPartnerid;
	}
	public String getPartnerCity() {
		return partnerCity;
	}
	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}
	public String getPartnerCountry() {
		return partnerCountry;
	}
	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerState() {
		return partnerState;
	}
	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}
	public String getPartnerZipCode() {
		return partnerZipCode;
	}
	public void setPartnerZipCode(String partnerZipCode) {
		this.partnerZipCode = partnerZipCode;
	}
	public String getPartnerStatus() {
		return partnerStatus;
	}
	public void setPartnerStatus(String partnerStatus) {
		this.partnerStatus = partnerStatus;
	}
	public String getPartnerPhoneNo() {
		return partnerPhoneNo;
	}
	public void setPartnerPhoneNo(String partnerPhoneNo) {
		this.partnerPhoneNo = partnerPhoneNo;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

}
