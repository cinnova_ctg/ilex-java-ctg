package com.mind.formbean.PVS;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 * Form bean for a Struts application.
 * Users may access 5 fields on this form:
 * <ul>
 * <li>version - [your comment here]
 * <li>date - [your comment here]
 * <li>mcsa - [your comment here]
 * <li>reset - [your comment here]
 * <li>save - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class MCSA_ViewForm extends ActionForm {

	private String version = null;
	private String mcsa = null;
	private String date = null;
	private String save = null;
	private String reset = null;
	private FormFile browse=null;
	private String upload=null;
	private String authenticate = "";
	
	

	
	public String getVersion() {
		return version;
	}

	
	public void setVersion(String v) {
		this.version = v;
	}

	
	public String getMcsa() {
		return mcsa;
	}

	
	public void setMcsa(String m) {
		this.mcsa = m;
	}

	
	public String getDate() {
		return date;
	}

	
	public void setDate(String d) {
		this.date = d;
	}

	
	public String getSave() {
		return save;
	}

	
	public void setSave(String s) {
		this.save = s;
	}

	
	public String getReset() {
		return reset;
	}

	
	public void setReset(String r) {
		this.reset = r;
	}
	
	

	public String getAuthenticate() {
		return authenticate;
	}


	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}


	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.

		version = null;
		mcsa = null;
		date = null;
		save = null;
		reset = null;
		browse=null;
		upload=null;

	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.

		// if ((field == null) || (field.length() == 0)) {
		//   errors.add("field", new org.apache.struts.action.ActionError("error.field.required"));
		// }
		return errors;

	}

	public String getUpload() {
		return upload;
	}

	public void setUpload(String upload) {
		this.upload = upload;
	}

	public FormFile getBrowse() {
		return browse;
	}

	public void setBrowse(FormFile browse) {		
		this.browse = browse;
	}

}
