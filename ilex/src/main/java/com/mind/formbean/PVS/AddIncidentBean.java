package com.mind.formbean.PVS;

public class AddIncidentBean {

	private String ckboxincidentlist = null;
	private String ckboxactionlist = null;
	private String ckboxincidentvalue = null;
	private String ckboxactionvalue = null;
	
	
	public String getCkboxactionlist() {
		return ckboxactionlist;
	}
	public void setCkboxactionlist(String ckboxactionlist) {
		this.ckboxactionlist = ckboxactionlist;
	}
	public String getCkboxactionvalue() {
		return ckboxactionvalue;
	}
	public void setCkboxactionvalue(String ckboxactionvalue) {
		this.ckboxactionvalue = ckboxactionvalue;
	}
	public String getCkboxincidentlist() {
		return ckboxincidentlist;
	}
	public void setCkboxincidentlist(String ckboxincidentlist) {
		this.ckboxincidentlist = ckboxincidentlist;
	}
	public String getCkboxincidentvalue() {
		return ckboxincidentvalue;
	}
	public void setCkboxincidentvalue(String ckboxincidentvalue) {
		this.ckboxincidentvalue = ckboxincidentvalue;
	}
	
	
	
	
}
