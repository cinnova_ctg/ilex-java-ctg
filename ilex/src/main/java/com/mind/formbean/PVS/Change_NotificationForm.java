package com.mind.formbean.PVS;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 3 fields on this form:
 * <ul>
 * <li>emaillist - [your comment here]
 * <li>reset - [your comment here]
 * <li>save - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class Change_NotificationForm extends ActionForm {

	private String emaillist = null;
	private String save = null;
	private String reset = null;
	private String formname = null;
	private String updatemessage = null;
	private boolean refresh=false;
	private String authenticate = "";
	private String select = null;
	
	public String getAuthenticate() {
		return authenticate;
	}


	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}


	public String getEmaillist() {
		return emaillist;
	}

	
	public void setEmaillist(String e) {
		this.emaillist = e;
	}

	
	public String getSave() {
		return save;
	}

	
	public void setSave(String s) {
		this.save = s;
	}

	
	public String getReset() {
		return reset;
	}

	
	public void setReset(String r) {
		this.reset = r;
	}
	
	public String getFormname() {
		return formname;
	}


	public void setFormname(String formname) {
		this.formname = formname;
	}
	
	public String getUpdatemessage() {
		return updatemessage;
	}


	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}

	
	public boolean getRefresh() {
		return refresh;
	}


	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}


	public void reset(ActionMapping mapping, HttpServletRequest request) {

		

		emaillist = null;
		save = null;
		reset = null;
		formname = null;
		updatemessage = null;
		refresh=false;

	}


	public String getSelect() {
		return select;
	}


	public void setSelect(String select) {
		this.select = select;
	}


	
	
		
}
