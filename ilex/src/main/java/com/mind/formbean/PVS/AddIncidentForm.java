/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: A formbean containing getter/setter methods for Incident add page
 *
 */
package com.mind.formbean.PVS;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AddIncidentForm extends ActionForm {

	private String technicianname = null;
	private String time = null;
	private String date = null;
	private String endcustomer = null;
	private String notes = null;
	private String save = null;
	private String ckboxother = null;
	private String delete = null;
	private String pid = null;
	private String ckboxincidentvalue = null;
	private String ckboxactionvalue = null;
	private String ckboxincidentlist = null;
	private String ckboxactionlist = null;
	private String[] ckboxincident = null;
	private String[] ckboxaction = null;
	private String flag = null;
	private String function = null;
	private String partnername = null;
	private String inid = null;
	private String incidentlist = null;
	private String actionlist = null;
	private String hours = null;
	private String minutes = null;
	private String am = null;
	private String[] chkdelete = null;
	private String temp = null;
	private String addmessage = null;
	private String deletemessage = null;
	private String authenticate = "";
	private String page = null;
	private String incidenttype = null;
	private String partnerstatus = null;
	private String[] partnerresponse = null;
	private String incidentdrop = null;
	private String incidentDropValue = null;
	private String jobid = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String temptext = null;
	private String jobname = null;
	private String appendixname = null;
	private String msaname = null;

	private String addedBy = null;

	private String incidentTypeName = null;
	private String incidentStatus;
	private String incidentSeverity;
	private String[] cpdnotes;
	private String[] item;
	private int number;
	private String response;
	private String notecpd;
	private String updatedBy = null;
	private String techniciannametxt;

	private String[] incidentStatusDrop;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getTechnicianname() {
		return technicianname;
	}

	public void setTechnicianname(String t) {
		this.technicianname = t;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String t) {
		this.time = t;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String d) {
		this.date = d;
	}

	public String getEndcustomer() {
		return endcustomer;
	}

	public void setEndcustomer(String e) {
		this.endcustomer = e;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String n) {
		this.notes = n;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String s) {
		this.save = s;
	}

	public String getCkboxother() {
		return ckboxother;
	}

	public void setCkboxother(String c) {
		this.ckboxother = c;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String d) {
		this.delete = d;
	}

	public String getCkboxincidentvalue() {
		return ckboxincidentvalue;
	}

	public void setCkboxincidentvalue(String ckboxincidentvalue) {
		this.ckboxincidentvalue = ckboxincidentvalue;
	}

	public String getCkboxactionvalue() {
		return ckboxactionvalue;
	}

	public void setCkboxactionvalue(String ckboxactionvalue) {
		this.ckboxactionvalue = ckboxactionvalue;
	}

	public String[] getCkboxaction() {
		return ckboxaction;
	}

	public void setCkboxaction(String[] ckboxaction) {
		this.ckboxaction = ckboxaction;
	}

	public String getCkboxaction(int i) {
		return ckboxaction[i];
	}

	public String[] getCkboxincident() {
		return ckboxincident;
	}

	public String getCkboxincident(int i) {
		return ckboxincident[i];
	}

	public void setCkboxincident(String[] ckboxincident) {
		this.ckboxincident = ckboxincident;
	}

	public String getCkboxactionlist() {
		return ckboxactionlist;
	}

	public void setCkboxactionlist(String ckboxactionlist) {
		this.ckboxactionlist = ckboxactionlist;
	}

	public String getCkboxincidentlist() {
		return ckboxincidentlist;
	}

	public void setCkboxincidentlist(String ckboxincidentlist) {
		this.ckboxincidentlist = ckboxincidentlist;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getPartnername() {
		return partnername;
	}

	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}

	public String getInid() {
		return inid;
	}

	public void setInid(String inid) {
		this.inid = inid;
	}

	public String getActionlist() {
		return actionlist;
	}

	public void setActionlist(String actionlist) {
		this.actionlist = actionlist;
	}

	public String getIncidentlist() {
		return incidentlist;
	}

	public void setIncidentlist(String incidentlist) {
		this.incidentlist = incidentlist;
	}

	public String getAm() {
		return am;
	}

	public void setAm(String am) {
		this.am = am;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}

	public String getMinutes() {
		return minutes;
	}

	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getAddmessage() {
		return addmessage;
	}

	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}

	public String getDeletemessage() {
		return deletemessage;
	}

	public void setDeletemessage(String deletemessage) {
		this.deletemessage = deletemessage;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		technicianname = null;
		time = null;
		date = null;
		endcustomer = null;
		notes = null;
		save = null;
		ckboxother = null;
		delete = null;
		pid = null;
		ckboxincidentvalue = null;
		ckboxincidentvalue = null;
		ckboxincidentlist = null;
		ckboxactionlist = null;
		ckboxincident = null;
		ckboxaction = null;
		flag = null;
		function = null;
		partnername = null;
		inid = null;
		incidentlist = null;
		actionlist = null;
		hours = null;
		minutes = null;
		am = null;
		setChkdelete(null);
		temp = null;
		addmessage = null;
		deletemessage = null;
		page = null;

	}

	public String getIncidenttype() {
		return incidenttype;
	}

	public void setIncidenttype(String incidenttype) {
		this.incidenttype = incidenttype;
	}

	public String getPartnerstatus() {
		return partnerstatus;
	}

	public void setPartnerstatus(String partnerstatus) {
		this.partnerstatus = partnerstatus;
	}

	/*
	 * public String getPartnerresponse() { return partnerresponse; }
	 * 
	 * public void setPartnerresponse(String partnerresponse) {
	 * this.partnerresponse = partnerresponse; }
	 */

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public String getIncidentTypeName() {
		return incidentTypeName;
	}

	public void setIncidentTypeName(String incidentTypeName) {
		this.incidentTypeName = incidentTypeName;
	}

	public String getIncidentStatus() {
		return incidentStatus;
	}

	public void setIncidentStatus(String incidentStatus) {
		this.incidentStatus = incidentStatus;
	}

	public String getIncidentSeverity() {
		return incidentSeverity;
	}

	public void setIncidentSeverity(String incidentSeverity) {
		this.incidentSeverity = incidentSeverity;
	}

	public String getIncidentdrop() {
		return incidentdrop;
	}

	public void setIncidentdrop(String incidentdrop) {
		this.incidentdrop = incidentdrop;
	}

	/*
	 * public String getCpdnotes() { return cpdnotes; }
	 * 
	 * public void setCpdnotes(String cpdnotes) { this.cpdnotes = cpdnotes; }
	 */

	public String[] getItem() {
		return item;
	}

	public void setItem(String[] item) {
		this.item = item;
	}

	public String[] getChkdelete() {
		return chkdelete;
	}

	public void setChkdelete(String[] chkdelete) {
		this.chkdelete = chkdelete;
	}

	public String[] getCpdnotes() {
		return cpdnotes;
	}

	public void setCpdnotes(String[] cpdnotes) {
		this.cpdnotes = cpdnotes;
	}

	public String[] getPartnerresponse() {
		return partnerresponse;
	}

	public void setPartnerresponse(String[] partnerresponse) {
		this.partnerresponse = partnerresponse;
	}

	public String getTemptext() {
		return temptext;
	}

	public void setTemptext(String temptext) {
		this.temptext = temptext;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getNotecpd() {
		return notecpd;
	}

	public void setNotecpd(String notecpd) {
		this.notecpd = notecpd;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getIncidentDropValue() {
		return incidentDropValue;
	}

	public void setIncidentDropValue(String incidentDropValue) {
		this.incidentDropValue = incidentDropValue;
	}

	public String getTechniciannametxt() {
		return techniciannametxt;
	}

	public void setTechniciannametxt(String techniciannametxt) {
		this.techniciannametxt = techniciannametxt;
	}

	public String[] getIncidentStatusDrop() {
		return incidentStatusDrop;
	}

	public void setIncidentStatusDrop(String[] incidentStatusDrop) {
		this.incidentStatusDrop = incidentStatusDrop;
	}

	/*
	 * public void setPartnerresponse(String[] partnerresponse) {
	 * this.partnerresponse = partnerresponse.toString(); }
	 */

}
