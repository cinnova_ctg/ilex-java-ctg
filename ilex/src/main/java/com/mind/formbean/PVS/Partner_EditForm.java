package com.mind.formbean.PVS;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 * Form bean for a Struts application. Users may access 76 fields on this form:
 * <ul>
 * <li>displayName - [your comment here]
 * <li>cmboxState - [your comment here]
 * <li>validFromWC - [your comment here]
 * <li>validToWC - [your comment here]
 * <li>projectCode - [your comment here]
 * <li>contactDate - [your comment here]
 * <li>prmEmail - [your comment here]
 * <li>afterHoursEmail - [your comment here]
 * <li>blousesQuantity - [your comment here]
 * <li>mainFax - [your comment here]
 * <li>rdoW9 - [your comment here]
 * <li>secExt - [your comment here]
 * <li>validFromIT - [your comment here]
 * <li>validToIT - [your comment here]
 * <li>engZip - [your comment here]
 * <li>cmboxResourceLevel - [your comment here]
 * <li>partnerCandidate - [your comment here]
 * <li>amountAutomobile - [your comment here]
 * <li>secEmail - [your comment here]
 * <li>cmboxPartnerLevel - [your comment here]
 * <li>badgesQuantity - [your comment here]
 * <li>taxR2 - [your comment here]
 * <li>taxR1 - [your comment here]
 * <li>chkboxTools - [your comment here]
 * <li>cmboxContractStatus - [your comment here]
 * <li>secMobilePhone - [your comment here]
 * <li>policyNumber - [your comment here]
 * <li>txareaCertifications - [your comment here]
 * <li>dateInc - [your comment here]
 * <li>rdoMCSA - [your comment here]
 * <li>datePosted - [your comment here]
 * <li>cmboxStatus - [your comment here]
 * <li>amountUmbrella - [your comment here]
 * <li>cmboxCoreCompetency - [your comment here]
 * <li>newPassword - [your comment here]
 * <li>engName - [your comment here]
 * <li>confirmPassword - [your comment here]
 * <li>companyURL - [your comment here]
 * <li>truckLogoQuantity - [your comment here]
 * <li>rdoWorkmens - [your comment here]
 * <li>zipCodeECDF - [your comment here]
 * <li>taxId - [your comment here]
 * <li>stateR2 - [your comment here]
 * <li>zip - [your comment here]
 * <li>stateR1 - [your comment here]
 * <li>validFromTA - [your comment here]
 * <li>validToTA - [your comment here]
 * <li>toolZips - [your comment here]
 * <li>mainPhone - [your comment here]
 * <li>rdoUnion - [your comment here]
 * <li>address2 - [your comment here]
 * <li>amountGeneral - [your comment here]
 * <li>address1 - [your comment here]
 * <li>carrier - [your comment here]
 * <li>chkboxEquipped - [your comment here]
 * <li>country - [your comment here]
 * <li>prmPhone - [your comment here]
 * <li>afterHoursPhone - [your comment here]
 * <li>validFromSD - [your comment here]
 * <li>regID - [your comment here]
 * <li>validToSD - [your comment here]
 * <li>prmMobilePhone - [your comment here]
 * <li>rdoInsurance - [your comment here]
 * <li>chkzipECDF - [your comment here]
 * <li>cmboxHighCriticality - [your comment here]
 * <li>city - [your comment here]
 * <li>programPackage - [your comment here]
 * <li>secPhone - [your comment here]
 * <li>contractDate - [your comment here]
 * <li>companyType - [your comment here]
 * <li>addMore - [your comment here]
 * </ul>
 * 
 * @version 1.0
 * @author
 */
public class Partner_EditForm extends ActionForm {
	private String pid = null;

	// Start :Added By Amit
	private String keyWord = null;
	private String mcsaVersion = null;

	private String[] allPartnersForRecommendedArr = null;
	private String[] allPartnersForRestrictedArr = null;
	private String[] selectedPartnersFormRecommendedArr = null;
	private String[] selectedPartnersForRestrictedArr = null;

	private Collection allPartnersForRecommendedCol = null;
	private Collection allPartnersForRestrictedCol = null;
	private Collection selectedPartnersFormRecommendedCol = null;
	private Collection selectedPartnersForRestrictedCol = null;

	private String partnerListCount;

	// End : Added By Amit

	private boolean leadMinuteman;
	private boolean externalSalesAgent;
	private boolean speedPaymentTerms;

	public boolean isSpeedPaymentTerms() {
		return speedPaymentTerms;
	}

	public void setSpeedPaymentTerms(boolean speedPaymentTerms) {
		this.speedPaymentTerms = speedPaymentTerms;
	}

	public boolean isLeadMinuteman() {
		return leadMinuteman;
	}

	public void setLeadMinuteman(boolean leadMinuteman) {
		this.leadMinuteman = leadMinuteman;
	}

	public boolean isExternalSalesAgent() {
		return externalSalesAgent;
	}

	public void setExternalSalesAgent(boolean externalSalesAgent) {
		this.externalSalesAgent = externalSalesAgent;
	}

	private String address_id = null;
	private String save = null;
	private String state = null;
	private String stateSelect = null;

	private String cmboxPrimaryName = null;
	private String cmboxSecondaryName = null;
	private String pri_id = null;
	private String sec_id = null;
	private String orgid = null;
	private String id_address = null;

	private String displayName = null;
	private String[] wlcompany = null;
	private String[] validFromWC = null;
	private String[] validToWC = null;
	private String projectCode = null;
	private String contactDate = null;
	private String prmEmail = null;
	private String[] afterHoursEmail = null;
	private String blousesQuantity = null;
	private String mainFax = null;
	private String rdoW9 = null;
	private String secExt = null;
	private String[] validFromIT = null;
	private String[] validToIT = null;
	private String[] engZip = null;
	private String[] cmboxResourceLevel = null;
	private String partnerCandidate = null;
	private String[] amountAutomobile = null;
	private String secEmail = null;
	private String cmboxPartnerLevel = null;
	private String badgesQuantity = null;
	private String taxR2 = null;
	private String[] taxR1 = null;
	private String[] chkboxTools = null;
	private String cmboxContractStatus = null;
	private String secMobilePhone = null;
	private String[] policyNumber = null;
	// private String[] txareaCertifications = null;
	private String dateInc = null;
	private String rdoMCSA = null;
	private String datePosted = null;
	private String cmboxStatus = null;
	private String[] amountUmbrella = null;
	private String[] cmboxCoreCompetency = null;
	private String[] engName = null;
	private String newPassword = null;
	private String companyURL = null;
	private String confirmPassword = null;
	private String truckLogoQuantity = null;
	private String rdoWorkmens = null;
	private String[] zipCodeECDF = null;
	private String taxId = null;
	private String stateR2 = null;
	private String zip = null;
	private String[] stateR1 = null;
	private String[] validFromTA = null;
	private String[] validToTA = null;
	private String[] toolZips = null;
	private String mainPhone = null;
	private String[] rdoUnion = null;
	private String address2 = null;
	private String[] amountGeneral = null;
	private String address1 = null;
	private String[] carrier = null;
	private String[] chkboxEquipped = null;
	private String country = null;
	private String prmPhone = null;
	private String[] afterHoursPhone = null;
	private String[] validFromSD = null;
	private String regID = null;
	private String[] validToSD = null;
	private String prmMobilePhone = null;
	private String rdoInsurance = null;
	private String[] chkzipECDF = null;
	private String[] cmboxHighCriticality = null;
	private String city = null;
	private String programPackage = null;
	private String secPhone = null;
	private String contractRenewalDate = null;
	private String contractDate = null;
	private String companyType = null;
	private String addMore = null;
	private String vendorID = null;
	private FormFile browse = null;

	private String[] sdcompany = null;
	private String[] itcompany = null;
	private String[] tacompany = null;

	private String[] phyFieldAddress1 = null;
	private String[] phyFieldAddress2 = null;
	private String[] phyFieldState = null;
	private String[] phyFieldZip = null;
	private String[] phyFieldPhone = null;
	private String[] phyFieldFax = null;

	private ArrayList phyAddress = null;
	private ArrayList ph_email = null;
	private String authenticate = "";

	private String[] cmboxCertifications = null;

	private String fromtype = null;
	private String back = null;
	private String jobid = null;
	private ArrayList uploadedfileslist = null;

	// Seema
	private String[] tech_id = null;
	private String deletetech = null;

	private String negotiatedRate = null;

	// to get a List of States group by country id
	private String SiteCountryId = null;
	private String SiteCountryIdName = null;
	private Collection tempList = null;
	private String category = null;
	private String region = null;
	private String refersh = null;

	private String lat_degree = null;
	private String lat_min = null;
	private String lat_direction = null;
	private String lon_degree = null;
	private String lon_min = null;
	private String lon_direction = null;
	private String timezone = null;
	private String latlon_source = null;
	private String incidentReport = null;
	private String orgTopId = null;
	private String topPartnerName = null;
	private String topPartnerType = null;
	private String partnerFirstName = null;
	private String partnerLastName = null;
	private String primaryFirstName = null;
	private String primaryLastName = null;
	private String secondaryFirstName = null;
	private String secondaryLastName = null;
	private String webUserName = null;
	private String webPassword = null;
	private String act = null;
	private String fromPage = null;
	private String saveContinue = null;
	private String duplicatePartnerName = null;
	private String fromPVSSearch = null;
	private String registrationRenewalDate = null;
	private String w9Uploaded = null;
	private String reqRestriction = null;
	private String partnerCreateDate = null;
	private String partnerSource = null;
	private String registered = null;
	private String regDate = null;
	private String signedBy = null;
	private String incidentReportFiled = null;
	private String resourceDeveloper = null;
	private String acceptAddress = null;

	private String accountingCPDRole = null;

	private String partnerUpdateDate = null;
	private String partnerUpdateBy = null;
	private String partnerWebUpdateDate = null;

	private String latitude = null;
	private String longitude = null;
	private String latLongAccuracy = null;
	private ArrayList toolList = null;
	private String[] label = null;
	private String[] LevelName = null;
	private String[] certificationName = null;
	private String[] highCriticalityName = null;
	private String[] resourceLevelName = null;
	private ArrayList technicians = null;
	private ArrayList resourceLevel = null;
	private String[] firstlevelcatglist = null;
	private String[] toolsCheckboxValue = null;
	private String adpo = null;
	private String monthlyNewsletter = null;
	// Created By Lalit goyal for new Changes [spr#103]
	private String isInsuranceSubmitted = null;
	private String isInsuranceUploaded = null;
	private String isResumeSubmitted = null;
	private String isResumeUploaded = null;
	private String backgroundCheckSubmitted = null;
	private String fileuploadStatus = null;
	private String name = null;
	private ArrayList historylist = null;

	private String[] checkBox1 = null;
	private String[] checkBox2 = null;
	private String[] checkBox3 = null;
	private String[] checkBox1Date = null;
	private String[] checkBox2Date = null;
	private String[] checkBox3Date = null;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	private String type = null;
	private String date = null;
	private String createdUser = null;
	private String activity = null;

	public String getBackgroundCheckSubmitted() {
		return backgroundCheckSubmitted;
	}

	public void setBackgroundCheckSubmitted(String backgroundCheckSubmitted) {
		this.backgroundCheckSubmitted = backgroundCheckSubmitted;
	}

	public String getBackgroundCheckUploaded() {
		return backgroundCheckUploaded;
	}

	public void setBackgroundCheckUploaded(String backgroundCheckUploaded) {
		this.backgroundCheckUploaded = backgroundCheckUploaded;
	}

	public String getPhotoIdentificationID() {
		return photoIdentificationID;
	}

	public void setPhotoIdentificationID(String photoIdentificationID) {
		this.photoIdentificationID = photoIdentificationID;
	}

	private String backgroundCheckUploaded = null;
	private String photoIdentificationID = null;

	public String getDrugScreenCompletionSubmitted() {
		return drugScreenCompletionSubmitted;
	}

	public void setDrugScreenCompletionSubmitted(
			String drugScreenCompletionSubmitted) {
		this.drugScreenCompletionSubmitted = drugScreenCompletionSubmitted;
	}

	public String getDrugScreenCompletionUploaded() {
		return drugScreenCompletionUploaded;
	}

	public void setDrugScreenCompletionUploaded(
			String drugScreenCompletionUploaded) {
		this.drugScreenCompletionUploaded = drugScreenCompletionUploaded;
	}

	private String drugScreenCompletionSubmitted = null;
	private String drugScreenCompletionUploaded = null;

	private String uploadedDocType = null;

	private String minutemanPayrollId;

	// add for thumbs up and thumbs down count(Jayaraju).
	private String thumbsUpCount;
	private String thumbsDownCount;

	private String daysRange = null;
	private String timeRange = null;

	   private String vendorSiteCode;
    private String comcastVendorID;
	private String gpVendorId;

	public String getUploadedDocType() {
		return uploadedDocType;
	}

	public void setUploadedDocType(String uploadedDocType) {
		this.uploadedDocType = uploadedDocType;
	}

	public String getIsInsuranceSubmitted() {
		return isInsuranceSubmitted;
	}

	public void setIsInsuranceSubmitted(String isInsuranceSubmitted) {
		this.isInsuranceSubmitted = isInsuranceSubmitted;
	}

	public String getIsInsuranceUploaded() {
		return isInsuranceUploaded;
	}

	public void setIsInsuranceUploaded(String isInsuranceUploaded) {
		this.isInsuranceUploaded = isInsuranceUploaded;
	}

	public String getAdpo() {
		return adpo;
	}

	public void setAdpo(String adpo) {
		this.adpo = adpo;
	}

	public String getMonthlyNewsletter() {
		return monthlyNewsletter;
	}

	public void setMonthlyNewsletter(String monthlyNewsletter) {
		this.monthlyNewsletter = monthlyNewsletter;
	}

	public String[] getToolsCheckboxValue() {
		return toolsCheckboxValue;
	}

	public void setToolsCheckboxValue(String[] toolsCheckboxValue) {
		this.toolsCheckboxValue = toolsCheckboxValue;
	}

	public String[] getFirstlevelcatglist() {
		return firstlevelcatglist;
	}

	public void setFirstlevelcatglist(String[] firstlevelcatglist) {
		this.firstlevelcatglist = firstlevelcatglist;
	}

	public ArrayList getResourceLevel() {
		return resourceLevel;
	}

	public void setResourceLevel(ArrayList resourceLevel) {
		this.resourceLevel = resourceLevel;
	}

	public String getReqRestriction() {
		return reqRestriction;
	}

	public void setReqRestriction(String reqRestriction) {
		this.reqRestriction = reqRestriction;
	}

	public String getLatlon_source() {
		return latlon_source;
	}

	public void setLatlon_source(String latlon_source) {
		this.latlon_source = latlon_source;
	}

	public String getPrimaryFirstName() {
		return primaryFirstName;
	}

	public void setPrimaryFirstName(String primaryFirstName) {
		this.primaryFirstName = primaryFirstName;
	}

	public String getPrimaryLastName() {
		return primaryLastName;
	}

	public void setPrimaryLastName(String primaryLastName) {
		this.primaryLastName = primaryLastName;
	}

	public String getSecondaryFirstName() {
		return secondaryFirstName;
	}

	public void setSecondaryFirstName(String secondaryFirstName) {
		this.secondaryFirstName = secondaryFirstName;
	}

	public String getSecondaryLastName() {
		return secondaryLastName;
	}

	public void setSecondaryLastName(String secondaryLastName) {
		this.secondaryLastName = secondaryLastName;
	}

	public String getPartnerFirstName() {
		return partnerFirstName;
	}

	public void setPartnerFirstName(String partnerFirstName) {
		this.partnerFirstName = partnerFirstName;
	}

	public String getPartnerLastName() {
		return partnerLastName;
	}

	public void setPartnerLastName(String partnerLastName) {
		this.partnerLastName = partnerLastName;
	}

	public String getOrgTopId() {
		return orgTopId;
	}

	public void setOrgTopId(String orgTopId) {
		this.orgTopId = orgTopId;
	}

	public String getIncidentReport() {
		return incidentReport;
	}

	public void setIncidentReport(String incidentReport) {
		this.incidentReport = incidentReport;
	}

	public String getRefersh() {
		return refersh;
	}

	public void setRefersh(String refersh) {
		this.refersh = refersh;
	}

	public String getSiteCountryId() {
		return SiteCountryId;
	}

	public void setSiteCountryId(String siteCountryId) {
		SiteCountryId = siteCountryId;
	}

	public String getSiteCountryIdName() {
		return SiteCountryIdName;
	}

	public void setSiteCountryIdName(String siteCountryIdName) {
		SiteCountryIdName = siteCountryIdName;
	}

	public Collection getTempList() {
		return tempList;
	}

	public void setTempList(Collection tempList) {
		this.tempList = tempList;
	}

	public ArrayList getUploadedfileslist() {
		return uploadedfileslist;
	}

	public void setUploadedfileslist(ArrayList uploadedfileslist) {
		this.uploadedfileslist = uploadedfileslist;
	}

	// changed by Seema
	public String[] getCmboxCertifications() {
		return cmboxCertifications;
	}

	public void setCmboxCertifications(String[] cmboxCertifications) {
		this.cmboxCertifications = cmboxCertifications;
	}

	// added by Seema
	public String getCmboxCertifications(int i) {
		return cmboxCertifications[i];
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.
		pid = null;
		address_id = null;
		save = null;
		state = null;
		cmboxPrimaryName = null;
		cmboxSecondaryName = null;
		orgid = null;
		pri_id = null;
		sec_id = null;
		displayName = null;
		wlcompany = null;
		validFromWC = null;
		validToWC = null;
		projectCode = null;
		contactDate = null;
		prmEmail = null;
		afterHoursEmail = null;
		blousesQuantity = null;
		mainFax = null;
		rdoW9 = null;
		secExt = null;
		validFromIT = null;
		validToIT = null;
		engZip = null;
		cmboxResourceLevel = null;
		partnerCandidate = null;
		amountAutomobile = null;
		secEmail = null;
		cmboxPartnerLevel = null;
		badgesQuantity = null;
		taxR2 = null;
		taxR1 = null;
		chkboxTools = null;
		cmboxContractStatus = null;
		secMobilePhone = null;
		policyNumber = null;
		cmboxCertifications = null;
		dateInc = null;
		rdoMCSA = null;
		datePosted = null;
		cmboxStatus = null;
		amountUmbrella = null;
		cmboxCoreCompetency = null;
		engName = null;
		newPassword = null;
		companyURL = null;
		confirmPassword = null;
		truckLogoQuantity = null;
		rdoWorkmens = null;
		zipCodeECDF = null;
		taxId = null;
		stateR2 = null;
		zip = null;
		stateR1 = null;
		validFromTA = null;
		validToTA = null;
		toolZips = null;
		mainPhone = null;
		rdoUnion = null;
		address2 = null;
		amountGeneral = null;
		address1 = null;
		carrier = null;
		chkboxEquipped = null;
		country = null;
		prmPhone = null;
		afterHoursPhone = null;
		validFromSD = null;
		regID = null;
		validToSD = null;
		prmMobilePhone = null;
		rdoInsurance = null;
		chkzipECDF = null;
		cmboxHighCriticality = null;
		city = null;
		programPackage = null;
		secPhone = null;
		contractRenewalDate = null;
		contractDate = null;
		companyType = null;
		addMore = null;
		vendorID = null;
		browse = null;

		sdcompany = null;
		itcompany = null;
		tacompany = null;

		phyFieldAddress1 = null;
		phyFieldAddress2 = null;
		phyFieldState = null;
		phyFieldZip = null;
		phyFieldPhone = null;
		phyFieldFax = null;

		phyAddress = null;
		ph_email = null;
		tech_id = null;
		deletetech = null;
		negotiatedRate = null;
		SiteCountryId = null;
		SiteCountryIdName = null;
		tempList = null;
		category = null;
		region = null;
		timezone = null;
		refersh = null;
		lat_degree = null;
		lat_min = null;
		lat_direction = null;
		lon_degree = null;
		lon_min = null;
		lon_direction = null;
		incidentReport = null;
		saveContinue = null;
		duplicatePartnerName = null;
		partnerFirstName = null;
		partnerLastName = null;
		primaryFirstName = null;
		primaryLastName = null;
		secondaryFirstName = null;
		secondaryLastName = null;
		webUserName = null;
		webPassword = null;
		keyWord = null;
		fromPVSSearch = null;
		fromPage = null;
		w9Uploaded = null;
		registrationRenewalDate = null;
		reqRestriction = null;
		partnerCreateDate = null;
		partnerSource = null;
		registered = null;
		regDate = null;
		signedBy = null;
		incidentReportFiled = null;
		resourceDeveloper = null;
		acceptAddress = null;
		uploadedfileslist = null;

		partnerUpdateDate = null;
		partnerUpdateBy = null;
		partnerWebUpdateDate = null;
		monthlyNewsletter = null;
		adpo = null;
		isInsuranceSubmitted = null;
		isInsuranceUploaded = null;
		isResumeSubmitted = null;
		isResumeUploaded = null;
		backgroundCheckSubmitted = null;
		backgroundCheckUploaded = null;
		drugScreenCompletionSubmitted = null;
		drugScreenCompletionUploaded = null;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.

		// if ((field == null) || (field.length() == 0)) {
		// errors.add("field", new
		// org.apache.struts.action.ActionError("error.field.required"));
		// }
		return errors;

	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setCmboxPrimaryName(String c) {
		this.cmboxPrimaryName = c;
	}

	public String getCmboxPrimaryName() {
		return cmboxPrimaryName;
	}

	public void setCmboxSecondaryName(String c) {
		this.cmboxSecondaryName = c;
	}

	public String getCmboxSecondaryName() {
		return cmboxSecondaryName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String d) {
		this.displayName = d;
	}

	public String[] getValidFromWC() {
		return validFromWC;
	}

	public void setValidFromWC(String[] v) {
		this.validFromWC = v;
	}

	public String[] getValidToWC() {
		return validToWC;
	}

	public void setValidToWC(String[] v) {
		this.validToWC = v;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String p) {
		this.projectCode = p;
	}

	public String getContactDate() {
		return contactDate;
	}

	public void setContactDate(String c) {
		this.contactDate = c;
	}

	public String getPrmEmail() {
		return prmEmail;
	}

	public void setPrmEmail(String p) {
		this.prmEmail = p;
	}

	public String[] getAfterHoursEmail() {
		return afterHoursEmail;
	}

	public void setAfterHoursEmail(String[] a) {
		this.afterHoursEmail = a;
	}

	public String getBlousesQuantity() {
		return blousesQuantity;
	}

	public void setBlousesQuantity(String b) {
		this.blousesQuantity = b;
	}

	public String getMainFax() {
		return mainFax;
	}

	public void setMainFax(String m) {
		this.mainFax = m;
	}

	public String getRdoW9() {
		return rdoW9;
	}

	public void setRdoW9(String r) {
		this.rdoW9 = r;
	}

	public String getSecExt() {
		return secExt;
	}

	public void setSecExt(String s) {
		this.secExt = s;
	}

	public String[] getValidFromIT() {
		return validFromIT;
	}

	public void setValidFromIT(String[] v) {
		this.validFromIT = v;
	}

	public String[] getValidToIT() {
		return validToIT;
	}

	public void setValidToIT(String[] v) {
		this.validToIT = v;
	}

	public String[] getEngZip() {
		return engZip;
	}

	public void setEngZip(String[] e) {
		this.engZip = e;
	}

	public String[] getCmboxResourceLevel() {
		return cmboxResourceLevel;
	}

	public void setCmboxResourceLevel(String[] c) {
		this.cmboxResourceLevel = c;
	}

	public String getPartnerCandidate() {
		return partnerCandidate;
	}

	public void setPartnerCandidate(String p) {
		this.partnerCandidate = p;
	}

	public String[] getAmountAutomobile() {
		return amountAutomobile;
	}

	public void setAmountAutomobile(String[] a) {
		this.amountAutomobile = a;
	}

	public String getSecEmail() {
		return secEmail;
	}

	public void setSecEmail(String s) {
		this.secEmail = s;
	}

	public String getCmboxPartnerLevel() {
		return cmboxPartnerLevel;
	}

	public void setCmboxPartnerLevel(String c) {
		this.cmboxPartnerLevel = c;
	}

	public String getBadgesQuantity() {
		return badgesQuantity;
	}

	public void setBadgesQuantity(String b) {
		this.badgesQuantity = b;
	}

	public String getTaxR2() {
		return taxR2;
	}

	public void setTaxR2(String t) {
		this.taxR2 = t;
	}

	public String[] getTaxR1() {
		return taxR1;
	}

	public void setTaxR1(String[] t) {
		this.taxR1 = t;
	}

	public String[] getChkboxTools() {
		return chkboxTools;
	}

	public void setChkboxTools(String c[]) {
		this.chkboxTools = c;
	}

	public String getCmboxContractStatus() {
		return cmboxContractStatus;
	}

	public void setCmboxContractStatus(String c) {
		this.cmboxContractStatus = c;
	}

	public String getSecMobilePhone() {
		return secMobilePhone;
	}

	public void setSecMobilePhone(String s) {
		this.secMobilePhone = s;
	}

	public String[] getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String[] p) {
		this.policyNumber = p;
	}

	public String getDateInc() {
		return dateInc;
	}

	public void setDateInc(String d) {
		this.dateInc = d;
	}

	public String getRdoMCSA() {
		return rdoMCSA;
	}

	public void setRdoMCSA(String r) {
		this.rdoMCSA = r;
	}

	public String getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(String d) {
		this.datePosted = d;
	}

	public String getCmboxStatus() {
		return cmboxStatus;
	}

	public void setCmboxStatus(String c) {
		this.cmboxStatus = c;
	}

	public String[] getAmountUmbrella() {
		return amountUmbrella;
	}

	public void setAmountUmbrella(String[] a) {
		this.amountUmbrella = a;
	}

	public String[] getCmboxCoreCompetency() {
		return cmboxCoreCompetency;
	}

	public void setCmboxCoreCompetency(String[] c) {
		this.cmboxCoreCompetency = c;
	}

	public String getCmboxCoreCompetency(int i) {
		return cmboxCoreCompetency[i];
	}

	public String[] getEngName() {
		return engName;
	}

	public void setEngName(String[] e) {
		this.engName = e;
	}

	// added by Seema
	public String getEngName(int i) {
		return engName[i];
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String n) {
		this.newPassword = n;
	}

	public String getCompanyURL() {
		return companyURL;
	}

	public void setCompanyURL(String c) {
		this.companyURL = c;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String c) {
		this.confirmPassword = c;
	}

	public String getTruckLogoQuantity() {
		return truckLogoQuantity;
	}

	public void setTruckLogoQuantity(String t) {
		this.truckLogoQuantity = t;
	}

	public String getRdoWorkmens() {
		return rdoWorkmens;
	}

	public void setRdoWorkmens(String r) {
		this.rdoWorkmens = r;
	}

	public String[] getZipCodeECDF() {
		return zipCodeECDF;
	}

	public void setZipCodeECDF(String[] z) {
		this.zipCodeECDF = z;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String t) {
		this.taxId = t;
	}

	public String getStateR2() {
		return stateR2;
	}

	public void setStateR2(String s) {
		this.stateR2 = s;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String z) {
		this.zip = z;
	}

	public String[] getStateR1() {
		return stateR1;
	}

	public void setStateR1(String[] s) {
		this.stateR1 = s;
	}

	public String[] getValidFromTA() {
		return validFromTA;
	}

	public void setValidFromTA(String[] v) {
		this.validFromTA = v;
	}

	public String[] getValidToTA() {
		return validToTA;
	}

	public void setValidToTA(String[] v) {
		this.validToTA = v;
	}

	public String[] getToolZips() {
		return toolZips;
	}

	public void setToolZips(String[] t) {
		this.toolZips = t;
	}

	public String getMainPhone() {
		return mainPhone;
	}

	public void setMainPhone(String m) {
		this.mainPhone = m;
	}

	public String[] getRdoUnion() {
		return rdoUnion;
	}

	public void setRdoUnion(String[] r) {
		this.rdoUnion = r;

	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String a) {
		this.address2 = a;
	}

	public String[] getAmountGeneral() {
		return amountGeneral;
	}

	public void setAmountGeneral(String[] a) {
		this.amountGeneral = a;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String a) {
		this.address1 = a;
	}

	public String[] getCarrier() {
		return carrier;
	}

	public void setCarrier(String[] c) {
		this.carrier = c;
	}

	public String[] getChkboxEquipped() {
		return chkboxEquipped;
	}

	public void setChkboxEquipped(String[] c) {
		this.chkboxEquipped = c;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String c) {
		this.country = c;
	}

	public String getPrmPhone() {
		return prmPhone;
	}

	public void setPrmPhone(String p) {
		this.prmPhone = p;
	}

	public String[] getAfterHoursPhone() {
		return afterHoursPhone;
	}

	public void setAfterHoursPhone(String[] a) {
		this.afterHoursPhone = a;
	}

	public String[] getValidFromSD() {
		return validFromSD;
	}

	public void setValidFromSD(String[] v) {
		this.validFromSD = v;
	}

	public String getRegID() {
		return regID;
	}

	public void setRegID(String r) {
		this.regID = r;
	}

	public String[] getValidToSD() {
		return validToSD;
	}

	public void setValidToSD(String[] v) {
		this.validToSD = v;
	}

	public String getPrmMobilePhone() {
		return prmMobilePhone;
	}

	public void setPrmMobilePhone(String p) {
		this.prmMobilePhone = p;
	}

	public String getRdoInsurance() {
		return rdoInsurance;
	}

	public void setRdoInsurance(String r) {
		this.rdoInsurance = r;
	}

	public String[] getChkzipECDF() {
		return chkzipECDF;
	}

	public void setChkzipECDF(String[] c) {
		this.chkzipECDF = c;
	}

	public String[] getCmboxHighCriticality() {
		return cmboxHighCriticality;
	}

	public void setCmboxHighCriticality(String[] c) {
		this.cmboxHighCriticality = c;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String c) {
		this.city = c;
	}

	public String getProgramPackage() {
		return programPackage;
	}

	public void setProgramPackage(String p) {
		this.programPackage = p;
	}

	public String getSecPhone() {
		return secPhone;
	}

	public void setSecPhone(String s) {
		this.secPhone = s;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String c) {
		this.contractDate = c;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String c) {
		this.companyType = c;
	}

	public String getAddMore() {
		return addMore;
	}

	public void setAddMore(String a) {
		this.addMore = a;
	}

	public String[] getWlcompany() {
		return wlcompany;
	}

	public void setWlcompany(String[] wlcompany) {
		this.wlcompany = wlcompany;
	}

	public String getContractRenewalDate() {
		return contractRenewalDate;
	}

	public void setContractRenewalDate(String contractRenewalDate) {
		this.contractRenewalDate = contractRenewalDate;
	}

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public String[] getPhyFieldAddress1() {
		return phyFieldAddress1;
	}

	public void setPhyFieldAddress1(String[] phyFieldAddress1) {
		this.phyFieldAddress1 = phyFieldAddress1;
	}

	public String[] getPhyFieldAddress2() {
		return phyFieldAddress2;
	}

	public void setPhyFieldAddress2(String[] phyFieldAddress2) {
		this.phyFieldAddress2 = phyFieldAddress2;
	}

	public String[] getPhyFieldFax() {
		return phyFieldFax;
	}

	public void setPhyFieldFax(String[] phyFieldFax) {
		this.phyFieldFax = phyFieldFax;
	}

	public String[] getPhyFieldPhone() {
		return phyFieldPhone;
	}

	public void setPhyFieldPhone(String[] phyFieldPhone) {
		this.phyFieldPhone = phyFieldPhone;
	}

	public String[] getPhyFieldState() {
		return phyFieldState;
	}

	public void setPhyFieldState(String[] phyFieldState) {
		this.phyFieldState = phyFieldState;
	}

	public String[] getPhyFieldZip() {
		return phyFieldZip;
	}

	public void setPhyFieldZip(String[] phyFieldZip) {
		this.phyFieldZip = phyFieldZip;
	}

	public String[] getItcompany() {
		return itcompany;
	}

	public void setItcompany(String[] itcompany) {
		this.itcompany = itcompany;
	}

	public String[] getSdcompany() {
		return sdcompany;
	}

	public void setSdcompany(String[] sdcompany) {
		this.sdcompany = sdcompany;
	}

	public String[] getTacompany() {
		return tacompany;
	}

	public void setTacompany(String[] tacompany) {
		this.tacompany = tacompany;
	}

	public FormFile getBrowse() {
		return browse;
	}

	public void setBrowse(FormFile browse) {
		this.browse = browse;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getAddress_id() {
		return address_id;
	}

	public void setAddress_id(String address_id) {
		this.address_id = address_id;
	}

	public ArrayList getPhyAddress() {
		return phyAddress;
	}

	public void setPhyAddress(ArrayList phyAddress) {
		this.phyAddress = phyAddress;
	}

	public ArrayList getPh_email() {
		return ph_email;
	}

	public void setPh_email(ArrayList ph_email) {
		this.ph_email = ph_email;
	}

	public String getFromtype() {
		return fromtype;
	}

	public void setFromtype(String fromtype) {
		this.fromtype = fromtype;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getMcsaVersion() {
		return mcsaVersion;
	}

	public void setMcsaVersion(String mcsaVersion) {
		this.mcsaVersion = mcsaVersion;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	// seema

	public String[] getTech_id() {
		return tech_id;
	}

	public void setTech_id(String[] tech_id) {
		this.tech_id = tech_id;
	}

	public String getTech_id(int i) {
		return tech_id[i];
	}

	public String getPri_id() {
		return pri_id;
	}

	public void setPri_id(String pri_id) {
		this.pri_id = pri_id;
	}

	public String getSec_id() {
		return sec_id;
	}

	public void setSec_id(String sec_id) {
		this.sec_id = sec_id;
	}

	// Seema-21/12/2006
	public String getDeletetech() {
		return deletetech;
	}

	public void setDeletetech(String deletetech) {
		this.deletetech = deletetech;
	}

	public String getId_address() {
		return id_address;
	}

	public void setId_address(String id_address) {
		this.id_address = id_address;
	}

	public String getNegotiatedRate() {
		return negotiatedRate;
	}

	public void setNegotiatedRate(String negotiatedRate) {
		this.negotiatedRate = negotiatedRate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getLat_degree() {
		return lat_degree;
	}

	public void setLat_degree(String lat_degree) {
		this.lat_degree = lat_degree;
	}

	public String getLat_direction() {
		return lat_direction;
	}

	public void setLat_direction(String lat_direction) {
		this.lat_direction = lat_direction;
	}

	public String getLat_min() {
		return lat_min;
	}

	public void setLat_min(String lat_min) {
		this.lat_min = lat_min;
	}

	public String getLon_degree() {
		return lon_degree;
	}

	public void setLon_degree(String lon_degree) {
		this.lon_degree = lon_degree;
	}

	public String getLon_direction() {
		return lon_direction;
	}

	public void setLon_direction(String lon_direction) {
		this.lon_direction = lon_direction;
	}

	public String getLon_min() {
		return lon_min;
	}

	public void setLon_min(String lon_min) {
		this.lon_min = lon_min;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getTopPartnerName() {
		return topPartnerName;
	}

	public void setTopPartnerName(String topPartnerName) {
		this.topPartnerName = topPartnerName;
	}

	public String getTopPartnerType() {
		return topPartnerType;
	}

	public void setTopPartnerType(String topPartnerType) {
		this.topPartnerType = topPartnerType;
	}

	public String getWebPassword() {
		return webPassword;
	}

	public void setWebPassword(String webPassword) {
		this.webPassword = webPassword;
	}

	public String getWebUserName() {
		return webUserName;
	}

	public void setWebUserName(String webUserName) {
		this.webUserName = webUserName;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String getSaveContinue() {
		return saveContinue;
	}

	public void setSaveContinue(String saveContinue) {
		this.saveContinue = saveContinue;
	}

	public String getDuplicatePartnerName() {
		return duplicatePartnerName;
	}

	public void setDuplicatePartnerName(String duplicatePartnerName) {
		this.duplicatePartnerName = duplicatePartnerName;
	}

	public String getFromPVSSearch() {
		return fromPVSSearch;
	}

	public void setFromPVSSearch(String fromPVSSearch) {
		this.fromPVSSearch = fromPVSSearch;
	}

	public String getRegistrationRenewalDate() {
		return registrationRenewalDate;
	}

	public void setRegistrationRenewalDate(String registrationRenewalDate) {
		this.registrationRenewalDate = registrationRenewalDate;
	}

	public String getW9Uploaded() {
		return w9Uploaded;
	}

	public void setW9Uploaded(String uploaded) {
		w9Uploaded = uploaded;
	}

	public String getPartnerCreateDate() {
		return partnerCreateDate;
	}

	public void setPartnerCreateDate(String partnerCreateDate) {
		this.partnerCreateDate = partnerCreateDate;
	}

	public String getPartnerSource() {
		return partnerSource;
	}

	public void setPartnerSource(String partnerSource) {
		this.partnerSource = partnerSource;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public String getRegistered() {
		return registered;
	}

	public void setRegistered(String registered) {
		this.registered = registered;
	}

	public String getSignedBy() {
		return signedBy;
	}

	public void setSignedBy(String signedBy) {
		this.signedBy = signedBy;
	}

	public String getIncidentReportFiled() {
		return incidentReportFiled;
	}

	public void setIncidentReportFiled(String incidentReportFiled) {
		this.incidentReportFiled = incidentReportFiled;
	}

	public String getResourceDeveloper() {
		return resourceDeveloper;
	}

	public void setResourceDeveloper(String resourceDeveloper) {
		this.resourceDeveloper = resourceDeveloper;
	}

	public String getAcceptAddress() {
		return acceptAddress;
	}

	public void setAcceptAddress(String acceptAddress) {
		this.acceptAddress = acceptAddress;
	}

	public String getPartnerUpdateBy() {
		return partnerUpdateBy;
	}

	public void setPartnerUpdateBy(String partnerUpdateBy) {
		this.partnerUpdateBy = partnerUpdateBy;
	}

	public String getPartnerUpdateDate() {
		return partnerUpdateDate;
	}

	public void setPartnerUpdateDate(String partnerUpdateDate) {
		this.partnerUpdateDate = partnerUpdateDate;
	}

	public String getPartnerWebUpdateDate() {
		return partnerWebUpdateDate;
	}

	public void setPartnerWebUpdateDate(String partnerWebUpdateDate) {
		this.partnerWebUpdateDate = partnerWebUpdateDate;
	}

	public String getAct() {
		return act;
	}

	public void setAct(String act) {
		this.act = act;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLatLongAccuracy() {
		return latLongAccuracy;
	}

	public void setLatLongAccuracy(String latLongAccuracy) {
		this.latLongAccuracy = latLongAccuracy;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public ArrayList getToolList() {
		return toolList;
	}

	public void setToolList(ArrayList toolList) {
		this.toolList = toolList;
	}

	public String[] getLabel() {
		return label;
	}

	public void setLabel(String[] label) {
		this.label = label;
	}

	public String[] getCertificationName() {
		return certificationName;
	}

	public void setCertificationName(String[] certificationName) {
		this.certificationName = certificationName;
	}

	public String[] getHighCriticalityName() {
		return highCriticalityName;
	}

	public void setHighCriticalityName(String[] highCriticalityName) {
		this.highCriticalityName = highCriticalityName;
	}

	public String[] getLevelName() {
		return LevelName;
	}

	public void setLevelName(String[] levelName) {
		LevelName = levelName;
	}

	public ArrayList getTechnicians() {
		return technicians;
	}

	public void setTechnicians(ArrayList technicians) {
		this.technicians = technicians;
	}

	public String[] getResourceLevelName() {
		return resourceLevelName;
	}

	public void setResourceLevelName(String[] resourceLevelName) {
		this.resourceLevelName = resourceLevelName;
	}

	public String getStateSelect() {
		return stateSelect;
	}

	public void setStateSelect(String stateSelect) {
		this.stateSelect = stateSelect;
	}

	public String getMinutemanPayrollId() {
		return minutemanPayrollId;
	}

	public void setMinutemanPayrollId(String minutemanPayrollId) {
		this.minutemanPayrollId = minutemanPayrollId;
	}

	public String getIsResumeUploaded() {
		return isResumeUploaded;
	}

	public void setIsResumeUploaded(String isResumeUploaded) {
		this.isResumeUploaded = isResumeUploaded;
	}

	public String getIsResumeSubmitted() {
		return isResumeSubmitted;
	}

	public void setIsResumeSubmitted(String isResumeSubmitted) {
		this.isResumeSubmitted = isResumeSubmitted;
	}

	public String getThumbsUpCount() {
		return thumbsUpCount;
	}

	public void setThumbsUpCount(String thumbsUpCount) {
		this.thumbsUpCount = thumbsUpCount;
	}

	public String getThumbsDownCount() {
		return thumbsDownCount;
	}

	public void setThumbsDownCount(String thumbsDownCount) {
		this.thumbsDownCount = thumbsDownCount;
	}

	private String onTimePercentage = null;

	public String getOnTimePercentage() {
		return onTimePercentage;
	}

	public void setOnTimePercentage(String onTimePercentage) {
		this.onTimePercentage = onTimePercentage;
	}

	public String getDaysRange() {
		return daysRange;
	}

	public void setDaysRange(String daysRange) {
		this.daysRange = daysRange;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public Collection getAllPartnersForRecommendedCol() {
		return allPartnersForRecommendedCol;
	}

	public void setAllPartnersForRecommendedCol(
			Collection allPartnersForRecommendedCol) {
		this.allPartnersForRecommendedCol = allPartnersForRecommendedCol;
	}

	public Collection getAllPartnersForRestrictedCol() {
		return allPartnersForRestrictedCol;
	}

	public void setAllPartnersForRestrictedCol(
			Collection allPartnersForRestrictedCol) {
		this.allPartnersForRestrictedCol = allPartnersForRestrictedCol;
	}

	public Collection getSelectedPartnersFormRecommendedCol() {
		return selectedPartnersFormRecommendedCol;
	}

	public void setSelectedPartnersFormRecommendedCol(
			Collection selectedPartnersFormRecommendedCol) {
		this.selectedPartnersFormRecommendedCol = selectedPartnersFormRecommendedCol;
	}

	public Collection getSelectedPartnersForRestrictedCol() {
		return selectedPartnersForRestrictedCol;
	}

	public void setSelectedPartnersForRestrictedCol(
			Collection selectedPartnersForRestrictedCol) {
		this.selectedPartnersForRestrictedCol = selectedPartnersForRestrictedCol;
	}

	public String[] getAllPartnersForRecommendedArr() {
		return allPartnersForRecommendedArr;
	}

	public void setAllPartnersForRecommendedArr(
			String[] allPartnersForRecommendedArr) {
		this.allPartnersForRecommendedArr = allPartnersForRecommendedArr;
	}

	public String[] getAllPartnersForRestrictedArr() {
		return allPartnersForRestrictedArr;
	}

	public void setAllPartnersForRestrictedArr(
			String[] allPartnersForRestrictedArr) {
		this.allPartnersForRestrictedArr = allPartnersForRestrictedArr;
	}

	public String[] getSelectedPartnersFormRecommendedArr() {
		return selectedPartnersFormRecommendedArr;
	}

	public void setSelectedPartnersFormRecommendedArr(
			String[] selectedPartnersFormRecommendedArr) {
		this.selectedPartnersFormRecommendedArr = selectedPartnersFormRecommendedArr;
	}

	public String[] getSelectedPartnersForRestrictedArr() {
		return selectedPartnersForRestrictedArr;
	}

	public void setSelectedPartnersForRestrictedArr(
			String[] selectedPartnersForRestrictedArr) {
		this.selectedPartnersForRestrictedArr = selectedPartnersForRestrictedArr;
	}

	public String getPartnerListCount() {
		return partnerListCount;
	}

	public void setPartnerListCount(String partnerListCount) {
		this.partnerListCount = partnerListCount;
	}

	    public String getVendorSiteCode() {
        return vendorSiteCode;
    }

    public void setVendorSiteCode(String vendorSiteCode) {
        this.vendorSiteCode = vendorSiteCode;
    }

    public String getComcastVendorID() {
        return comcastVendorID;
    }

    public void setComcastVendorID(String comcastVendorID) {
        this.comcastVendorID = comcastVendorID;
    }

	public String getGpVendorId() {
		return gpVendorId;
	}

	public void setGpVendorId(String gpVendorId) {
		this.gpVendorId = gpVendorId;
	}

	public String getAccountingCPDRole() {
		return accountingCPDRole;
	}

	public void setAccountingCPDRole(String accountingCPDRole) {
		this.accountingCPDRole = accountingCPDRole;
	}

	public String getFileuploadStatus() {
		return fileuploadStatus;
	}

	public void setFileuploadStatus(String fileuploadStatus) {
		this.fileuploadStatus = fileuploadStatus;
	}

	public ArrayList getHistorylist() {
		return historylist;
	}

	public void setHistorylist(ArrayList historylist) {
		this.historylist = historylist;
	}

	public String[] getCheckBox1() {
		return checkBox1;
	}
	public void setCheckBox1(String[] checkBox1) {
		this.checkBox1 = checkBox1;
	}
	public String[] getCheckBox2() {
		return checkBox2;
	}
	public void setCheckBox2(String[] checkBox2) {
		this.checkBox2 = checkBox2;
	}
	public String[] getCheckBox3() {
		return checkBox3;
	}
	public void setCheckBox3(String[] checBox3) {
		this.checkBox3 = checBox3;
	}
	public String[] getCheckBox1Date() {
		return checkBox1Date;
	}
	public void setCheckBox1Date(String[] checkBox1Date) {
		this.checkBox1Date = checkBox1Date;
	}
	public String[] getCheckBox2Date() {
		return checkBox2Date;
	}
	public void setCheckBox2Date(String[] checkBox2Date) {
		this.checkBox2Date = checkBox2Date;
	}
	public String[] getCheckBox3Date() {
		return checkBox3Date;
	}
	public void setCheckBox3Date(String[] checkBox3Date) {
		this.checkBox3Date = checkBox3Date;
	}
}
