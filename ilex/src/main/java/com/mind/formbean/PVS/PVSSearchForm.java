package com.mind.formbean.PVS;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class PVSSearchForm extends ActionForm{
	
	private String orgTopName=null;
	private String search=null;
	private String division=null;
	private String act=null;
	private String startdate=null;
	private String enddate=null;
	private String partnerType = null;
	
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		division=null;
		startdate=null;
		enddate=null;
		partnerType=null;
	}
	public String getOrgTopName() {
		return orgTopName;
	}
	public void setOrgTopName(String orgTopName) {
		this.orgTopName = orgTopName;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}

	public String getAct() {
		return act;
	}

	public void setAct(String act) {
		this.act = act;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	

	
}
