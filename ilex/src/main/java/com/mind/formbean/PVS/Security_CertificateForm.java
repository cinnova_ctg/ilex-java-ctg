package com.mind.formbean.PVS;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class Security_CertificateForm extends ActionForm {
	private FormFile certicateFile = null;

	public FormFile getCerticateFile() {
		return certicateFile;
	}

	public void setCerticateFile(FormFile certicateFile) {
		this.certicateFile = certicateFile;
	}
}
