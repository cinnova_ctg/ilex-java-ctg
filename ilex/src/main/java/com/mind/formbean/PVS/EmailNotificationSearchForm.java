package com.mind.formbean.PVS;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class EmailNotificationSearchForm extends ActionForm
{

	private String pocname = null;
	private String divison = null;
	private String emailid = null;
	private String search = null;
	private String close = null;
	private String save = null;
	private String[] poccheckbox = null;
	private ArrayList contactlist = null;
	
	public ArrayList getContactlist() {
		return contactlist;
	}
	public void setContactlist(ArrayList contactlist) {
		this.contactlist = contactlist;
	}
	public String getDivison() {
		return divison;
	}
	public void setDivison(String divison) {
		this.divison = divison;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getPocname() {
		return pocname;
	}
	public void setPocname(String pocname) {
		this.pocname = pocname;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}
	public String[] getPoccheckbox() {
		return poccheckbox;
	}
	public void setPoccheckbox(String[] poccheckbox) {
		this.poccheckbox = poccheckbox;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	

}
