package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class AppendixEditForm extends ActionForm {

	private String msaId = null;

	private String check = null;
	private String appendixId = null;
	private String appendixName = null;
	private String msaName = null;

	private String appendixType = null;
	private String appendixPrType = null;

	private String cnsPOC = null;

	private String customerDivision = null;

	private String siteNo = null;
	private String custRef = null;

	private String estimatedCost = null;
	private String extendedPrice = null;
	private String proformaMargin = null;
	private String reqPlannedSch = null;
	private String effPlannedSch = null;
	private String schDays = null;

	private String draftReviews = null;
	private String businessReviews = null;

	private String custPocBusiness = null;
	private String custPocContract = null;
	private String custPocBilling = null;

	private String status = null;
	private String unionUpliftFactor = null;
	private String expedite24UpliftFactor = null;
	private String expedite48UpliftFactor = null;
	private String afterhoursUpliftFactor = null;
	private String whUpliftFactor = null;

	private String fromType = null;
	private String save = null;
	private String actionUpdateEdit = null;
	private String createInfo = null;
	private String changeInfo = null;

	private String Add = null;
	private String msp = null;
	private String travelAuthorized = null;
	private String testAppendix = null;

	// New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	private String monthWeekCheck = null;
	private String appendixTypeBreakout;

	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public String getAfterhoursUpliftFactor() {
		return afterhoursUpliftFactor;
	}

	public void setAfterhoursUpliftFactor(String afterhoursUpliftFactor) {
		this.afterhoursUpliftFactor = afterhoursUpliftFactor;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getAppendixType() {
		return appendixType;
	}

	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	public String getBusinessReviews() {
		return businessReviews;
	}

	public void setBusinessReviews(String businessReviews) {
		this.businessReviews = businessReviews;
	}

	public String getCheck() {
		return check;
	}

	public void setCheck(String check) {
		this.check = check;
	}

	public String getCnsPOC() {
		return cnsPOC;
	}

	public void setCnsPOC(String cnsPOC) {
		this.cnsPOC = cnsPOC;
	}

	public String getCustomerDivision() {
		return customerDivision;
	}

	public void setCustomerDivision(String customerDivision) {
		this.customerDivision = customerDivision;
	}

	public String getCustPocBilling() {
		return custPocBilling;
	}

	public void setCustPocBilling(String custPocBilling) {
		this.custPocBilling = custPocBilling;
	}

	public String getCustPocBusiness() {
		return custPocBusiness;
	}

	public void setCustPocBusiness(String custPocBusiness) {
		this.custPocBusiness = custPocBusiness;
	}

	public String getCustPocContract() {
		return custPocContract;
	}

	public void setCustPocContract(String custPocContract) {
		this.custPocContract = custPocContract;
	}

	public String getCustRef() {
		return custRef;
	}

	public void setCustRef(String custRef) {
		this.custRef = custRef;
	}

	public String getDraftReviews() {
		return draftReviews;
	}

	public void setDraftReviews(String draftReviews) {
		this.draftReviews = draftReviews;
	}

	public String getEffPlannedSch() {
		return effPlannedSch;
	}

	public void setEffPlannedSch(String effPlannedSch) {
		this.effPlannedSch = effPlannedSch;
	}

	public String getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(String estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public String getExpedite24UpliftFactor() {
		return expedite24UpliftFactor;
	}

	public void setExpedite24UpliftFactor(String expedite24UpliftFactor) {
		this.expedite24UpliftFactor = expedite24UpliftFactor;
	}

	public String getExpedite48UpliftFactor() {
		return expedite48UpliftFactor;
	}

	public void setExpedite48UpliftFactor(String expedite48UpliftFactor) {
		this.expedite48UpliftFactor = expedite48UpliftFactor;
	}

	public String getExtendedPrice() {
		return extendedPrice;
	}

	public void setExtendedPrice(String extendedPrice) {
		this.extendedPrice = extendedPrice;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getProformaMargin() {
		return proformaMargin;
	}

	public void setProformaMargin(String proformaMargin) {
		this.proformaMargin = proformaMargin;
	}

	public String getReqPlannedSch() {
		return reqPlannedSch;
	}

	public void setReqPlannedSch(String reqPlannedSch) {
		this.reqPlannedSch = reqPlannedSch;
	}

	public String getSchDays() {
		return schDays;
	}

	public void setSchDays(String schDays) {
		this.schDays = schDays;
	}

	public String getSiteNo() {
		return siteNo;
	}

	public void setSiteNo(String siteNo) {
		this.siteNo = siteNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUnionUpliftFactor() {
		return unionUpliftFactor;
	}

	public void setUnionUpliftFactor(String unionUpliftFactor) {
		this.unionUpliftFactor = unionUpliftFactor;
	}

	public String getWhUpliftFactor() {
		return whUpliftFactor;
	}

	public void setWhUpliftFactor(String whUpliftFactor) {
		this.whUpliftFactor = whUpliftFactor;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getActionUpdateEdit() {
		return actionUpdateEdit;
	}

	public void setActionUpdateEdit(String action) {
		this.actionUpdateEdit = action;
	}

	public String getChangeInfo() {
		return changeInfo;
	}

	public void setChangeInfo(String changeInfo) {
		this.changeInfo = changeInfo;
	}

	public String getCreateInfo() {
		return createInfo;
	}

	public void setCreateInfo(String createInfo) {
		this.createInfo = createInfo;
	}

	public String getAdd() {
		return Add;
	}

	public void setAdd(String add) {
		Add = add;
	}

	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}

	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}

	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}

	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}

	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}

	public String getAppendixSelectedOwners(int i) {
		return appendixSelectedOwners[i];
	}

	public String getAppendixSelectedStatus(int i) {
		return appendixSelectedStatus[i];
	}

	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String getAppendixPrType() {
		return appendixPrType;
	}

	public void setAppendixPrType(String appendixPrType) {
		this.appendixPrType = appendixPrType;
	}

	public String getMsp() {
		return msp;
	}

	public void setMsp(String msp) {
		this.msp = msp;
	}

	public String getTravelAuthorized() {
		return travelAuthorized;
	}

	public void setTravelAuthorized(String travelAuthorized) {
		this.travelAuthorized = travelAuthorized;
	}

	public String getTestAppendix() {
		return testAppendix;
	}

	public void setTestAppendix(String testAppendix) {
		this.testAppendix = testAppendix;
	}

	/**
	 * @return the appendixTypeBreakout
	 */
	public String getAppendixTypeBreakout() {
		return appendixTypeBreakout;
	}

	/**
	 * @param appendixTypeBreakout
	 *            the appendixTypeBreakout to set
	 */
	public void setAppendixTypeBreakout(String appendixTypeBreakout) {
		this.appendixTypeBreakout = appendixTypeBreakout;
	}

}
