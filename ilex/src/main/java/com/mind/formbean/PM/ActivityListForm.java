package com.mind.formbean.PM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.ActivityGridControl;

public class ActivityListForm extends ActionForm
{
	private String job_Id = null;
	private String job_type = null;
	private String from = null;
	private String chkaddendum = null;
	private String activity_cost_type = null;
	private String[] check = null;
	private String[] activity_Id = null;
	private String[] name = null;
	
	private String[] activity_lib_Id = null;
	
	private String[] activitytype = null;
	private String[] activitytypecombo = null;
	
	private String[] quantity = null;
	private String[] estimated_materialcost = null;
	
	private String[] estimated_cnsfieldlaborcost = null;
	private String[] estimated_cnshqlaborcost = null;
	
	
	private String[] estimated_cnsfieldhqlaborcost = null;
	
	private String[] estimated_contractfieldlaborcost = null;
	private String[] estimated_freightcost = null;
	private String[] estimated_travelcost = null;
	private String[] estimated_totalcost = null;
	
	private String[] extendedprice = null;
	private String[] overheadcost = null;
	private String[] listprice = null;
	private String[] status = null;
	
	private String[] proformamargin = null;
	
	private String[] estimated_start_schedule = null;
	private String[] estimated_complete_schedule = null;
	
	private String overhead_Id = null;
	
	private String overheadname = null;
	private String overheadtype = null;
	private String overheadtypecombo = null;
	
	private String overheadquantity = null;
	private String overheadestimated_materialcost = null;
	
	private String overheadestimated_cnsfieldlaborcost = null;
	private String overheadestimated_contractfieldlaborcost = null;
	private String overheadestimated_freightcost = null;
	private String overheadestimated_travelcost = null;
	private String overheadestimated_totalcost = null;
	
	private String overheadextendedprice = null;
	
	private String overheadproformamargin = null;
	
	private String overhead_overheadcost = null;
	private String overheadlistprice = null;
	private String overheadstatus = null;
	
	private String newactivity_Id = null;
	
	private String newname = null;
	private String newactivitytype = null;
	private String newactivitytypecombo = null;
	
	private String newquantity = null;
	private String newestimated_materialcost = null;
	
	private String newestimated_cnsfieldlaborcost = null;
	private String newestimated_cnshqlaborcost = null;
	
	private String newestimated_contractfieldlaborcost = null;
	private String newestimated_freightcost = null;
	private String newestimated_travelcost = null;
	private String newestimated_totalcost = null;
	
	private String newextendedprice = null;
	
	private String newproformamargin = null;
	private String newestimated_start_schedule = null;
	private String newestimated_complete_schedule = null;
	
	private String newoverheadcost = null;
	private String newlistprice = null;
	private String newstatus = null;
	
	private String fromflag = "";
	
	private String addendum_id = "";
	
	private String save = null;
	
	private String ref = null;
	private ArrayList activity = null;
	
	private String job_estimated_start_schedule = null;
	private String job_estimated_complete_schedule = null;
	
	private String appendixname = null;
	private String appendixType = null; 
	private String jobName = null;
	
	private String appendixid = null;
	
	private String latestaddendum_id = null;
	private String latestaddendum_name = null;
	private String latestaddendum_type = null;
	private String msa_id = null;
	private String msaname = null;
	
	
	private String[] flag = null;
	private String[] job_name = null;
	
	//Added By Amit For Mass Approve and Delete
	private String delete = null;
	private String approve = null;
	private String draft = null;
	//End
	
	private String appendixId = null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	private String[] bidFlag = null;
	private String tabId = null;
	
	private ActivityGridControl activityGridControl  = null;
	
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	
	
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getJob_Id() {
		return job_Id;
	}
	public void setJob_Id( String job_Id ) {
		this.job_Id = job_Id;
	}
	
	public String getJob_type() {
		return job_type;
	}
	public void setJob_type( String job_type ) {
		this.job_type = job_type;
	}
	
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}
	
	public String[] getCheck() {
		return check;
	}
	public void setCheck( String[] check ) {
		this.check = check;
	}
	public String getCheck( int i ) {
		return check[i];
	}
	
	
	
	public String[] getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String[] activity_Id ) {
		this.activity_Id = activity_Id;
	}
	public String getActivity_Id( int i ) {
		return activity_Id[i];
	}
	
	

	public String[] getActivity_lib_Id() {
		return activity_lib_Id;
	}
	public void setActivity_lib_Id( String[] activity_lib_Id ) {
		this.activity_lib_Id = activity_lib_Id;
	}
	public String getActivity_lib_Id( int i ) {
		return activity_lib_Id[i];
	}
	
	
	public String[] getName() {
		return name;
	}
	public void setName( String[] name ) {
		this.name = name;
	}
	public String getName( int i ) {
		return name[i];
	}
	
	
	
	public String[] getActivitytype() {
		return activitytype;
	}
	public void setActivitytype( String[] activitytype ) {
		this.activitytype = activitytype;
	}
	public String getActivitytype( int i ) {
		return activitytype[i];
	}
	
	
	
	public String[] getQuantity() {
		return quantity;
	}
	public void setQuantity( String[] quantity ) {
		this.quantity = quantity;
	}
	public String getQuantity( int i ) {
		return quantity[i];
	}
	
	
	public String[] getActivitytypecombo() {
		return activitytypecombo;
	}
	public void setActivitytypecombo( String[] activitytypecombo ) {
		this.activitytypecombo = activitytypecombo;
	}
	public String getActivitytypecombo( int i ) {
		return activitytypecombo[i];
	}
	
	
	public String[] getEstimated_cnsfieldlaborcost() {
		return estimated_cnsfieldlaborcost;
	}
	public void setEstimated_cnsfieldlaborcost( String[] estimated_cnsfieldlaborcost ) {
		this.estimated_cnsfieldlaborcost = estimated_cnsfieldlaborcost;
	}
	public String getEstimated_cnsfieldlaborcost( int i ) {
		return estimated_cnsfieldlaborcost[i];
	}
	
	
	
	public String[] getEstimated_contractfieldlaborcost() {
		return estimated_contractfieldlaborcost;
	}
	public void setEstimated_contractfieldlaborcost( String[] estimated_contractfieldlaborcost ) {
		this.estimated_contractfieldlaborcost = estimated_contractfieldlaborcost;
	}
	public String getEstimated_contractfieldlaborcost( int i ) {
		return estimated_contractfieldlaborcost[i];
	}
	
	
	
	public String[] getEstimated_freightcost() {
		return estimated_freightcost;
	}
	public void setEstimated_freightcost( String[] estimated_freightcost ) {
		this.estimated_freightcost = estimated_freightcost;
	}
	public String getEstimated_freightcost( int i ) {
		return estimated_freightcost[i];
	}
	
	
	
	public String[] getEstimated_materialcost() {
		return estimated_materialcost;
	}
	public void setEstimated_materialcost( String[] estimated_materialcost ) {
		this.estimated_materialcost = estimated_materialcost;
	}
	public String getEstimated_materialcost( int i ) {
		return estimated_materialcost[i];
	}
	
	
	
	public String[] getEstimated_totalcost() {
		return estimated_totalcost;
	}
	public void setEstimated_totalcost( String[] estimated_totalcost ) {
		this.estimated_totalcost = estimated_totalcost;
	}
	public String getEstimated_totalcost( int i ) {
		return estimated_totalcost[i];
	}
	
	
	
	public String[] getEstimated_travelcost() {
		return estimated_travelcost;
	}
	public void setEstimated_travelcost( String[] estimated_travelcost ) {
		this.estimated_travelcost = estimated_travelcost;
	}
	public String getEstimated_travelcost( int i ) {
		return estimated_travelcost[i];
	}
	
	
	
	public String[] getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice( String[] extendedprice ) {
		this.extendedprice = extendedprice;
	}
	public String getExtendedprice( int i ) {
		return extendedprice[i];
	}
	
	
	
	public String[] getListprice() {
		return listprice;
	}
	public void setListprice( String[] listprice ) {
		this.listprice = listprice;
	}
	public String getListprice( int i ) {
		return listprice[i];
	}
	
	
	
	public String[] getOverheadcost() {
		return overheadcost;
	}
	public void setOverheadcost( String[] overheadcost ) {
		this.overheadcost = overheadcost;
	}
	public String getOverheadcost( int i ) {
		return overheadcost[i];
	}
	
	
	
	public String[] getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String[] proformamargin ) {
		this.proformamargin = proformamargin;
	}
	public String getProformamargin( int i ) {
		return proformamargin[i];
	}
	
	

	public String[] getEstimated_complete_schedule() {
		return estimated_complete_schedule;
	}
	public void setEstimated_complete_schedule( String[] estimated_complete_schedule ) {
		this.estimated_complete_schedule = estimated_complete_schedule;
	}
	public String getEstimated_complete_schedule( int i ) {
		return estimated_complete_schedule[i];
	}
	
	
	
	public String[] getEstimated_start_schedule() {
		return estimated_start_schedule;
	}
	public void setEstimated_start_schedule( String[] estimated_start_schedule ) {
		this.estimated_start_schedule = estimated_start_schedule;
	}
	public String getEstimated_start_schedule( int i ) {
		return estimated_start_schedule[i];
	}
	
	
	public String getOverhead_Id() {
		return overhead_Id;
	}
	public void setOverhead_Id( String overhead_Id ) {
		this.overhead_Id = overhead_Id;
	}
	
	
	public String getOverhead_overheadcost() {
		return overhead_overheadcost;
	}
	public void setOverhead_overheadcost( String overhead_overheadcost ) {
		this.overhead_overheadcost = overhead_overheadcost;
	}
	
	
	public String getOverheadestimated_cnsfieldlaborcost() {
		return overheadestimated_cnsfieldlaborcost;
	}
	public void setOverheadestimated_cnsfieldlaborcost( String overheadestimated_cnsfieldlaborcost ) {
		this.overheadestimated_cnsfieldlaborcost = overheadestimated_cnsfieldlaborcost;
	}
	
	
	public String getOverheadestimated_contractfieldlaborcost() {
		return overheadestimated_contractfieldlaborcost;
	}
	public void setOverheadestimated_contractfieldlaborcost( String overheadestimated_contractfieldlaborcost ) {
		this.overheadestimated_contractfieldlaborcost = overheadestimated_contractfieldlaborcost;
	}
	
	
	public String getOverheadestimated_freightcost() {
		return overheadestimated_freightcost;
	}
	public void setOverheadestimated_freightcost( String overheadestimated_freightcost ) {
		this.overheadestimated_freightcost = overheadestimated_freightcost;
	}
	
	
	public String getOverheadestimated_materialcost() {
		return overheadestimated_materialcost;
	}
	public void setOverheadestimated_materialcost( String overheadestimated_materialcost ) {
		this.overheadestimated_materialcost = overheadestimated_materialcost;
	}
	
	
	public String getOverheadestimated_totalcost() {
		return overheadestimated_totalcost;
	}
	public void setOverheadestimated_totalcost( String overheadestimated_totalcost ) {
		this.overheadestimated_totalcost = overheadestimated_totalcost;
	}
	
	
	public String getOverheadestimated_travelcost() {
		return overheadestimated_travelcost;
	}
	public void setOverheadestimated_travelcost( String overheadestimated_travelcost ) {
		this.overheadestimated_travelcost = overheadestimated_travelcost;
	}
	
	
	public String getOverheadextendedprice() {
		return overheadextendedprice;
	}
	public void setOverheadextendedprice( String overheadextendedprice ) {
		this.overheadextendedprice = overheadextendedprice;
	}
	
	
	public String getOverheadlistprice() {
		return overheadlistprice;
	}
	public void setOverheadlistprice( String overheadlistprice ) {
		this.overheadlistprice = overheadlistprice;
	}
	
	
	public String getOverheadname() {
		return overheadname;
	}
	public void setOverheadname( String overheadname ) {
		this.overheadname = overheadname;
	}
	
	
	public String getOverheadproformamargin() {
		return overheadproformamargin;
	}
	public void setOverheadproformamargin( String overheadproformamargin ) {
		this.overheadproformamargin = overheadproformamargin;
	}
	
	
	public String getOverheadquantity() {
		return overheadquantity;
	}
	public void setOverheadquantity( String overheadquantity ) {
		this.overheadquantity = overheadquantity;
	}
	
	
	public String getOverheadtype() {
		return overheadtype;
	}
	public void setOverheadtype( String overheadtype ) {
		this.overheadtype = overheadtype;
	}
	
	
	public String getOverheadtypecombo() {
		return overheadtypecombo;
	}
	public void setOverheadtypecombo( String overheadtypecombo ) {
		this.overheadtypecombo = overheadtypecombo;
	}
	
	
	public String getNewactivity_Id() {
		return newactivity_Id;
	}
	public void setNewactivity_Id( String newactivity_Id ) {
		this.newactivity_Id = newactivity_Id;
	}
	
	
	public String getNewactivitytype() {
		return newactivitytype;
	}
	public void setNewactivitytype( String newactivitytype ) {
		this.newactivitytype = newactivitytype;
	}
	
	
	public String getNewactivitytypecombo() {
		return newactivitytypecombo;
	}
	public void setNewactivitytypecombo( String newactivitytypecombo ) {
		this.newactivitytypecombo = newactivitytypecombo;
	}
	
	
	public String getNewestimated_cnsfieldlaborcost() {
		return newestimated_cnsfieldlaborcost;
	}
	public void setNewestimated_cnsfieldlaborcost( String newestimated_cnsfieldlaborcost ) {
		this.newestimated_cnsfieldlaborcost = newestimated_cnsfieldlaborcost;
	}
	
	
	public String getNewestimated_contractfieldlaborcost() {
		return newestimated_contractfieldlaborcost;
	}
	public void setNewestimated_contractfieldlaborcost( String newestimated_contractfieldlaborcost ) {
		this.newestimated_contractfieldlaborcost = newestimated_contractfieldlaborcost;
	}
	
	
	public String getNewestimated_freightcost() {
		return newestimated_freightcost;
	}
	public void setNewestimated_freightcost( String newestimated_freightcost ) {
		this.newestimated_freightcost = newestimated_freightcost;
	}
	
	
	public String getNewestimated_totalcost() {
		return newestimated_totalcost;
	}
	public void setNewestimated_totalcost( String newestimated_totalcost ) {
		this.newestimated_totalcost = newestimated_totalcost;
	}
	
	
	public String getNewestimated_travelcost() {
		return newestimated_travelcost;
	}
	public void setNewestimated_travelcost( String newestimated_travelcost ) {
		this.newestimated_travelcost = newestimated_travelcost;
	}
	
	
	public String getNewextendedprice() {
		return newextendedprice;
	}
	public void setNewextendedprice( String newextendedprice ) {
		this.newextendedprice = newextendedprice;
	}
	
	
	public String getNewlistprice() {
		return newlistprice;
	}
	public void setNewlistprice( String newlistprice ) {
		this.newlistprice = newlistprice;
	}
	
	
	public String getNewestimated_materialcost() {
		return newestimated_materialcost;
	}
	public void setNewestimated_materialcost( String newestimated_materialcost ) {
		this.newestimated_materialcost = newestimated_materialcost;
	}
	
	
	public String getNewname() {
		return newname;
	}
	public void setNewname(String newname) {
		this.newname = newname;
	}
	
	
	public String getNewoverheadcost() {
		return newoverheadcost;
	}
	public void setNewoverheadcost( String newoverheadcost ) {
		this.newoverheadcost = newoverheadcost;
	}
	
	
	public String getNewproformamargin() {
		return newproformamargin;
	}
	public void setNewproformamargin( String newproformamargin ) {
		this.newproformamargin = newproformamargin;
	}
	
	
	
	public String getNewestimated_complete_schedule() {
		return newestimated_complete_schedule;
	}
	public void setNewestimated_complete_schedule(
			String newestimated_complete_schedule) {
		this.newestimated_complete_schedule = newestimated_complete_schedule;
	}
	
	
	public String getNewestimated_start_schedule() {
		return newestimated_start_schedule;
	}
	public void setNewestimated_start_schedule( String newestimated_start_schedule ) {
		this.newestimated_start_schedule = newestimated_start_schedule;
	}
	
	
	public String getNewquantity() {
		return newquantity;
	}
	public void setNewquantity( String newquantity ) {
		this.newquantity = newquantity;
	}
	
	
	public String getRef() {
		return ref;
	}
	public void setRef( String ref ) {
		this.ref = ref;
	}
	
	
	public ArrayList getActivity() {
		return activity;
	}
	public void setActivity(ArrayList activity) {
		this.activity = activity;
	}
	
	
	public String getSave() {
		return save;
	}
	public void setSave( String save ) {
		this.save = save;
	}
	
	
	public String getNewstatus() {
		return newstatus;
	}
	public void setNewstatus( String newstatus ) {
		this.newstatus = newstatus;
	}
	
	
	public String getOverheadstatus() {
		return overheadstatus;
	}
	public void setOverheadstatus( String overheadstatus ) {
		this.overheadstatus = overheadstatus;
	}
	
	
	public String[] getStatus() {
		return status;
	}
	public void setStatus( String[] status ) {
		this.status = status;
	}
	public String getStatus( int i ) {
		return status[i];
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}
	
	public String getActivity_cost_type() {
		return activity_cost_type;
	}
	public void setActivity_cost_type(String activity_cost_type) {
		this.activity_cost_type = activity_cost_type;
	}
	
	
	public String getJob_estimated_complete_schedule() {
		return job_estimated_complete_schedule;
	}
	public void setJob_estimated_complete_schedule(
			String job_estimated_complete_schedule) {
		this.job_estimated_complete_schedule = job_estimated_complete_schedule;
	}
	
	
	public String getJob_estimated_start_schedule() {
		return job_estimated_start_schedule;
	}
	public void setJob_estimated_start_schedule( String job_estimated_start_schedule ) {
		this.job_estimated_start_schedule = job_estimated_start_schedule;
	}
	
	
	
	public String[] getEstimated_cnshqlaborcost() {
		return estimated_cnshqlaborcost;
	}
	public void setEstimated_cnshqlaborcost( String[] estimated_cnshqlaborcost ) {
		this.estimated_cnshqlaborcost = estimated_cnshqlaborcost;
	}
	public String getEstimated_cnshqlaborcost( int i ) {
		return estimated_cnshqlaborcost[i];
	}
	
	
	public String getNewestimated_cnshqlaborcost() {
		return newestimated_cnshqlaborcost;
	}
	public void setNewestimated_cnshqlaborcost( String newestimated_cnshqlaborcost ) {
		this.newestimated_cnshqlaborcost = newestimated_cnshqlaborcost;
	}
	
	
	public String[] getEstimated_cnsfieldhqlaborcost() {
		return estimated_cnsfieldhqlaborcost;
	}
	public void setEstimated_cnsfieldhqlaborcost(
			String[] estimated_cnsfieldhqlaborcost) {
		this.estimated_cnsfieldhqlaborcost = estimated_cnsfieldhqlaborcost;
	}
	public String getEstimated_cnsfieldhqlaborcost( int i ) {
		return estimated_cnsfieldhqlaborcost[i];
	}
	
	
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getFromflag() {
		return fromflag;
	}
	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}
	
	public String getLatestaddendum_id() {
		return latestaddendum_id;
	}
	public void setLatestaddendum_id(String latestaddendum_id) {
		this.latestaddendum_id = latestaddendum_id;
	}
	public String getLatestaddendum_name() {
		return latestaddendum_name;
	}
	public void setLatestaddendum_name(String latestaddendum_name) {
		this.latestaddendum_name = latestaddendum_name;
	}
	public String getLatestaddendum_type() {
		return latestaddendum_type;
	}
	public void setLatestaddendum_type(String latestaddendum_type) {
		this.latestaddendum_type = latestaddendum_type;
	}
	
	
	public String[] getFlag() {
		return flag;
	}
	public void setFlag(String[] flag) {
		this.flag = flag;
	}
	public String getFlag( int i ) {
		return flag[i];
	}
	

	public String[] getJob_name() {
		return job_name;
	}
	public void setJob_name(String[] job_name) {
		this.job_name = job_name;
	}
	public String getJob_name( int i ) {
		return job_name[i];
	}
	
	
	public String getMsa_id() {
		return msa_id;
	}
	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		job_Id = null;
		
		check = null;
		activity_Id = null;
		name = null;
		
		activitytype = null;
		activitytypecombo = null;
		quantity = null;
		estimated_materialcost = null;
		
		estimated_cnsfieldlaborcost = null;
		estimated_contractfieldlaborcost = null;
		estimated_freightcost = null;
		estimated_travelcost = null;
		estimated_totalcost = null;
		
		extendedprice = null;
		overheadcost = null;
		listprice = null;
		status = null;
		
		proformamargin = null;
		estimated_start_schedule = null;
		estimated_complete_schedule = null;
		
		overhead_Id = null;
		
		overheadname = null;
		overheadtype = null;
		
		overheadquantity = null;
		overheadestimated_materialcost = null;
		
		overheadestimated_cnsfieldlaborcost = null;
		overheadestimated_contractfieldlaborcost = null;
		overheadestimated_freightcost = null;
		overheadestimated_travelcost = null;
		overheadestimated_totalcost = null;
		
		overheadextendedprice = null;
		
		overheadproformamargin = null;
		
		overhead_overheadcost = null;
		overheadlistprice = null;
		overheadstatus = null;
		
		newactivity_Id = null;
		
		newname = null;
		newactivitytype = null;
		newactivitytypecombo = null;
		
		newquantity = null;
		newestimated_materialcost = null;
		
		newestimated_cnsfieldlaborcost = null;
		newestimated_contractfieldlaborcost = null;
		newestimated_freightcost = null;
		newestimated_travelcost = null;
		newestimated_totalcost = null;
		
		newextendedprice = null;
		
		newproformamargin = null;
		newestimated_start_schedule = null;
		newestimated_complete_schedule = null;
		
		newoverheadcost = null;
		newlistprice = null;
		newstatus = null;
		ref = null;
		activity = null;
		save = null;
		
		addendum_id = null;
		
		job_estimated_start_schedule = null;
		job_estimated_complete_schedule = null;
		
		newestimated_cnshqlaborcost = null;
		estimated_cnshqlaborcost = null;
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
		bidFlag = null;
		
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getApprove() {
		return approve;
	}
	public void setApprove(String approve) {
		this.approve = approve;
	}
	public String getDelete() {
		return delete;
	}
	public void setDelete(String delete) {
		this.delete = delete;
	}
	public String getDraft() {
		return draft;
	}
	public void setDraft(String draft) {
		this.draft = draft;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}

	public String[] getBidFlag() {
		return bidFlag;
	}

	public void setBidFlag(String[] bidFlag) {
		this.bidFlag = bidFlag;
	}
	public String getBidFlag( int i ) {
		return bidFlag[i];
	}

	public String getAppendixType() {
		return appendixType;
	}

	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public ActivityGridControl getActivityGridControl() {
		return activityGridControl;
	}

	public void setActivityGridControl(ActivityGridControl activityGridControl) {
		this.activityGridControl = activityGridControl;
	}
	
}
