package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;


public class MSAListForm extends ActionForm
{	
	private static final long serialVersionUID = 3257009869025589301L;
	private String add = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	private String monthWeekCheck = null;

	

	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getAdd() {
		return add;
	}

	public void setAdd(String add) {
		this.add = add;
	}

	public String[] getSelectedStatus() {
		return selectedStatus;
	}
	public String getSelectedStatus( int i ) {
		return selectedStatus[i];
	}
	
	
	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public String getOtherCheck() {
		return otherCheck;
	}

	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String[] getSelectedOwners() {
		return selectedOwners;
	}
	public String getSelectedOwners( int i ) {
		return selectedOwners[i];
	}
	
	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	
}

