package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class FormalDocDateForm extends ActionForm
{
	private String formaldocdate = null;
	private String id = null;
	private String setdate = null;  
	private String authenticate = "";
	private String name = "";
	private String type ="";
	private String appendixId ="";
	private String msaId ="";
	private String msaName ="";
	
	
	
	//For the View Selector
	private String monthWeekCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	
//	New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String   appendixOtherCheck = null;
	
	
	
	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}
	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}
	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}
	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}
	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}
	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}
	
	public String getAppendixSelectedOwners( int i ) {
		return appendixSelectedOwners[i];
	}
	
	public String getAppendixSelectedStatus( int i ) {
		return appendixSelectedStatus[i];
	}
	
	public String getFormaldocdate() 
	{
		return formaldocdate;
	}
	public void setFormaldocdate( String formaldocdate ) 
	{
		this.formaldocdate = formaldocdate;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId( String id ) {
		this.id = id;
	}
	
	
	public String getSetdate() {
		return setdate;
	}
	public void setSetdate( String setdate ) {
		this.setdate = setdate;
	}
	
	
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate( String authenticate ) {
		this.authenticate = authenticate;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}
	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}
	public String getOtherCheck() {
		return otherCheck;
	}
	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}
	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}
	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}
	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}
	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}
	public String[] getSelectedOwners() {
		return selectedOwners;
	}
	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}
	
	public String getSelectedOwners( int i ) {
		return selectedOwners[i];
	}

	public String getSelectedStatus( int i ) {
		return selectedStatus[i];
	}
	
	public String[] getSelectedStatus() {
		return selectedStatus;
	}
	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}	
}