package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class MaterialEditForm extends ActionForm {
	
	private String activity_Id = null;
	
	private String materialid = null;
	private String materialcostlibid = null;
	private String materialName = null;
	private String materialType = null;
	private String activityName = null;
	private String appendixName = null;
	private String appendixId = null;
	private String jobName = null;
	private String jobId = null;
	
	
	private String manufacturername = null;
	private String manufacturerpartnumber = null;
	private String cnspartnumber = null;
	
	private String quantity = null;
	private String prevquantity = null;
	
	private String estimatedunitcost = null;
	private String estimatedtotalcost = null;
	private String proformamargin = null;
	private String priceunit = null;
	private String priceextended = null;
	
	private String sellablequantity = null;
	private String minimumquantity = null;
	private String status = null;
	private String fromflag = null;
	private String dashboardid = null;
	
	private String addendum_id = null;
	
	private String flag = null; // for resource from temporary table or not
	private String flag_materialid = null; //for setting checkbox checked
	
	private String msaId = null;
	private String msaName = null;
	
	private String save = null;
	private String Chkaddendum = null;
	private String ref = null;
	private String createdBy = null;
	private String createdDate = null;
	private String changedBy = null;
	private String changedDate = null;
	
	
	public String getChangedBy() {
		return changedBy;
	}
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}
	public String getChangedDate() {
		return changedDate;
	}
	public void setChangedDate(String changedDate) {
		this.changedDate = changedDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id(String activity_Id) {
		this.activity_Id = activity_Id;
	}
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}
	public String getCnspartnumber() {
		return cnspartnumber;
	}
	public void setCnspartnumber(String cnspartnumber) {
		this.cnspartnumber = cnspartnumber;
	}
	public String getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost(String estimatedtotalcost) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	public String getEstimatedunitcost() {
		return estimatedunitcost;
	}
	public void setEstimatedunitcost(String estimatedunitcost) {
		this.estimatedunitcost = estimatedunitcost;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getFlag_materialid() {
		return flag_materialid;
	}
	public void setFlag_materialid(String flag_materialid) {
		this.flag_materialid = flag_materialid;
	}
	public String getManufacturername() {
		return manufacturername;
	}
	public void setManufacturername(String manufacturername) {
		this.manufacturername = manufacturername;
	}
	public String getManufacturerpartnumber() {
		return manufacturerpartnumber;
	}
	public void setManufacturerpartnumber(String manufacturerpartnumber) {
		this.manufacturerpartnumber = manufacturerpartnumber;
	}
	public String getMaterialcostlibid() {
		return materialcostlibid;
	}
	public void setMaterialcostlibid(String materialcostlibid) {
		this.materialcostlibid = materialcostlibid;
	}
	public String getMaterialid() {
		return materialid;
	}
	public void setMaterialid(String materialid) {
		this.materialid = materialid;
	}
	public String getMinimumquantity() {
		return minimumquantity;
	}
	public void setMinimumquantity(String minimumquantity) {
		this.minimumquantity = minimumquantity;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getPrevquantity() {
		return prevquantity;
	}
	public void setPrevquantity(String prevquantity) {
		this.prevquantity = prevquantity;
	}
	public String getPriceextended() {
		return priceextended;
	}
	public void setPriceextended(String priceextended) {
		this.priceextended = priceextended;
	}
	public String getPriceunit() {
		return priceunit;
	}
	public void setPriceunit(String priceunit) {
		this.priceunit = priceunit;
	}
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin(String proformamargin) {
		this.proformamargin = proformamargin;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getSellablequantity() {
		return sellablequantity;
	}
	public void setSellablequantity(String sellablequantity) {
		this.sellablequantity = sellablequantity;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
	public String getChkaddendum() {
		return Chkaddendum;
	}
	public void setChkaddendum(String chkaddendum) {
		Chkaddendum = chkaddendum;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getFromflag() {
		return fromflag;
	}
	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}
	public String getDashboardid() {
		return dashboardid;
	}
	public void setDashboardid(String dashboardid) {
		this.dashboardid = dashboardid;
	}
	
	
}
