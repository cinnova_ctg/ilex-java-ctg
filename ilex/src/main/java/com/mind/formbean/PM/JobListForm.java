package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class JobListForm extends ActionForm
{
	
	private String chkaddendum = null;
	private String  addendum_id  = null;
	private String appendix_Id = null;
	private String submit = null;
	private String ref = null;
	private String back = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String msaname = null;
	private String addendumflag = null;
	private String appendixtype = null;

	//New UI Related Changes.
	private String add = null;
	private String otherCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String MSA_Id = null;
	private String appendixname = null;
	
	

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getMSA_Id() {
		return MSA_Id;
	}

	public void setMSA_Id(String id) {
		MSA_Id = id;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String getAddendumflag() {
		return addendumflag;
	}

	public String getAdd() {
		return add;
	}

	public void setAdd(String add) {
		this.add = add;
	}

	public String[] getSelectedOwners() {
		return selectedOwners;
	}
	
	public String getSelectedOwners( int i ) {
		return selectedOwners[i];
	}	

	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}

	public String[] getSelectedStatus() {
		return selectedStatus;
	}
	
	public String getSelectedStatus( int i ) {
		return selectedStatus[i];
	}

	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public void setAddendumflag(String addendumflag) {
		this.addendumflag = addendumflag;
	}

	public String getAppendix_Id() {
		return appendix_Id;
	}

	public void setAppendix_Id(String appendix_Id) {
		this.appendix_Id = appendix_Id;
	}

	public String getAppendixtype() {
		return appendixtype;
	}

	public void setAppendixtype(String appendixtype) {
		this.appendixtype = appendixtype;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public String getChkaddendum() {
		return chkaddendum;
	}

	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getAddendum_id() {
		return addendum_id;
	}

	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}

	public String getOtherCheck() {
		return otherCheck;
	}

	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}
	
}