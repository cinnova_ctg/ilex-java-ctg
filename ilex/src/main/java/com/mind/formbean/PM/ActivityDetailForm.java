package com.mind.formbean.PM;


import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ActivityDetailForm extends ActionForm
{
	private String job_Id = null;
	private String job_type = null;
	private String activity_Id = null;
	private String chkaddendum = null;
	private String chkaddendumresource = null;
	private String name = null;
	private String line_item = null;
	private String type = null;
	
	private String quantity = null;
	
	private String estimated_materialcost = null;
	private String estimated_cnslaborcost = null;
	private String estimated_contractlaborcost = null;
	private String estimated_freightcost = null;
	private String estimated_travelcost = null;
	private String estimated_totalcost = null;
	
	
	private String extendedprice = null;
	private String listprice = null;
	private String overheadcost = null;
	
	private String proformamargin = null;
	
	private String lastcomment = null;
	private String lastchangeby = null;
	private String lastchangedate = null;
	private String status = null;
	
	private String lastactivitymodifiedby = null;
	private String lastactivitymodifieddate = null; 
	
	private String addendum_id = null;
	
	private String estimated_unitcost = null;
	private String estimated_unitprice = null;
	
	private String jobname = null;
	
	private String msa_id = null;
	private String msaname = null;
	
	private String appendix_id = null;
	private String appendixname = null;
	private String commissionCost = null;
	private String commissionRevenue = null;
	
	public String getCommissionCost() {
		return commissionCost;
	}
	public void setCommissionCost(String commissionCost) {
		this.commissionCost = commissionCost;
	}
	public String getCommissionRevenue() {
		return commissionRevenue;
	}
	public void setCommissionRevenue(String commissionRevenue) {
		this.commissionRevenue = commissionRevenue;
	}
	public String getJob_Id() {
		return job_Id;
	}
	public void setJob_Id( String job_Id ) {
		this.job_Id = job_Id;
	}
	
	
	
	public String getJob_type() {
		return job_type;
	}
	public void setJob_type( String job_type ) {
		this.job_type = job_type;
	}
	
	
	
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	
	public String getLine_item() {
		return line_item;
	}
	public void setLine_item( String line_item ) {
		this.line_item = line_item;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	
	
	public String getType() {
		return type;
	}
	public void setType( String type ) {
		this.type = type;
	}
	
	
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity( String quantity ) {
		this.quantity = quantity;
	}
	
	
	public String getEstimated_cnslaborcost() {
		return estimated_cnslaborcost;
	}
	public void setEstimated_cnslaborcost( String estimated_cnslaborcost ) {
		this.estimated_cnslaborcost = estimated_cnslaborcost;
	}
	
	
	public String getEstimated_contractlaborcost() {
		return estimated_contractlaborcost;
	}
	public void setEstimated_contractlaborcost( String estimated_contractlaborcost ) {
		this.estimated_contractlaborcost = estimated_contractlaborcost;
	}
	
	
	public String getEstimated_freightcost() {
		return estimated_freightcost;
	}
	public void setEstimated_freightcost( String estimated_freightcost ) {
		this.estimated_freightcost = estimated_freightcost;
	}
	
	
	public String getEstimated_travelcost() {
		return estimated_travelcost;
	}
	public void setEstimated_travelcost( String estimated_travelcost ) {
		this.estimated_travelcost = estimated_travelcost;
	}
	
	
	public String getEstimated_totalcost() {
		return estimated_totalcost;
	}
	public void setEstimated_totalcost( String estimated_totalcost ) {
		this.estimated_totalcost = estimated_totalcost;
	}
	
	
	
	
	
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String proformamargin ) {
		this.proformamargin = proformamargin;
	}
	
	
	public String getLastcomment() {
		return lastcomment;
	}
	public void setLastcomment( String lastcomment ) {
		this.lastcomment = lastcomment;
	}
	
	
	public String getLastchangeby() {
		return lastchangeby;
	}
	public void setLastchangeby( String lastchangeby ) {
		this.lastchangeby = lastchangeby;
	}
	
	
	public String getLastchangedate() {
		return lastchangedate;
	}
	public void setLastchangedate( String lastchangedate ) {
		this.lastchangedate = lastchangedate;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus( String status ) {
		this.status = status;
	}
	
	
	public String getEstimated_materialcost() {
		return estimated_materialcost;
	}
	public void setEstimated_materialcost( String estimated_materialcost ) {
		this.estimated_materialcost = estimated_materialcost;
	}
	
	
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice( String extendedprice ) {
		this.extendedprice = extendedprice;
	}
	
	
	public String getListprice() {
		return listprice;
	}
	public void setListprice( String listprice ) {
		this.listprice = listprice;
	}
	
	
	public String getOverheadcost() {
		return overheadcost;
	}
	public void setOverheadcost( String overheadcost ) {
		this.overheadcost = overheadcost;
	}
	
	
	
	
	public String getLastactivitymodifiedby() {
		return lastactivitymodifiedby;
	}
	public void setLastactivitymodifiedby(String lastactivitymodifiedby) {
		this.lastactivitymodifiedby = lastactivitymodifiedby;
	}
	public String getLastactivitymodifieddate() {
		return lastactivitymodifieddate;
	}
	public void setLastactivitymodifieddate(String lastactivitymodifieddate) {
		this.lastactivitymodifieddate = lastactivitymodifieddate;
	}
	
	
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}
	public String getChkaddendumresource() {
		return chkaddendumresource;
	}
	public void setChkaddendumresource(String chkaddendumresource) {
		this.chkaddendumresource = chkaddendumresource;
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	
	
	
	public String getEstimated_unitcost() {
		return estimated_unitcost;
	}
	public void setEstimated_unitcost(String estimated_unitcost) {
		this.estimated_unitcost = estimated_unitcost;
	}
	public String getEstimated_unitprice() {
		return estimated_unitprice;
	}
	public void setEstimated_unitprice(String estimated_unitprice) {
		this.estimated_unitprice = estimated_unitprice;
	}
	
	
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	
	public String getMsa_id() {
		return msa_id;
	}
	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	
	
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}
	
	
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	
	
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		job_Id = null;
		activity_Id = null;
		
		name = null;
		line_item = null;
		type = null;
		
		quantity = null;
		
		estimated_materialcost = null;
		estimated_cnslaborcost = null;
		estimated_contractlaborcost = null;
		estimated_freightcost = null;
		estimated_travelcost = null;
		estimated_totalcost = null;
		
		extendedprice = null;
		listprice = null;
		overheadcost = null;
		
		proformamargin = null;
		lastcomment = null;
		lastchangeby = null;
		lastchangedate = null;
		status = null;
		
	}
	
}
