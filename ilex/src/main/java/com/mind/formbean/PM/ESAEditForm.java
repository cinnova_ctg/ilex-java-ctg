package com.mind.formbean.PM;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.pm.ESAList;
import com.mind.common.LabelValue;

public class ESAEditForm extends ActionForm{
	private String agent[] = null;
	private String commission[] = null;
	private String payementTerms[] = null;
	private String effectiveDate[] = null;
	private String save = null;
	private String status[] = null;
	private String esaId[] = null;
	private String esaSeqNo[] = null;
	private String commissionType[] = null;
	private ArrayList<ESAList> esaList = null;
	/*
	 * Start
	 * Following Variables are used for View Selector
	 */
	private String appendixId =null;
	private String msaId =null;
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	private String monthWeekCheck = null;
	private String travelAuthorized = null;
	private String checkAppendix = null;
	private String msaName = null;
	private String appendixName = null;
	private String type =null;
	private String url = null;
	
	/*
	 * End Variable declaration for View Selector
	 */
	
	/*
	 *  Start Variable for PRM View Selector
	 */
		private String jobOwnerOtherCheck = null;
		private String[] jobSelectedStatus = null;
		private String[] jobSelectedOwners = null;
		private String jobMonthWeekCheck = null;
		
		private String ownerId = null;
		private String viewjobtype = null;
		private String fromPage = null;
	/*
	 *  End Variable for PRM View Selector
	 */

	
	// ESA Edit form bean start
	private ArrayList<LabelValue> esaAgentList = null;
	private ArrayList<LabelValue> esaPaymentList = null;
	 
	
	
	public ArrayList<LabelValue> getEsaAgentList() {
		return esaAgentList;
	}
	public void setEsaAgentList(ArrayList<LabelValue> esaAgentList) {
		this.esaAgentList = esaAgentList;
	}
	public ArrayList<LabelValue> getEsaPaymentList() {
		return esaPaymentList;
	}
	public void setEsaPaymentList(ArrayList<LabelValue> esaPaymentList) {
		this.esaPaymentList = esaPaymentList;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}
	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}
	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}
	public String getAppendixSelectedOwners( int i ) {
		return appendixSelectedOwners[i];
	}
	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}
	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}
	public String getAppendixSelectedStatus( int i ) {
		return appendixSelectedStatus[i];
	}
	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}
	public String getCheckAppendix() {
		return checkAppendix;
	}
	public void setCheckAppendix(String checkAppendix) {
		this.checkAppendix = checkAppendix;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}
	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}
	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}
	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}
	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}
	public String getTravelAuthorized() {
		return travelAuthorized;
	}
	public void setTravelAuthorized(String travelAuthorized) {
		this.travelAuthorized = travelAuthorized;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String[] getAgent() {
		return agent;
	}
	public void setAgent(String[] agent) {
		this.agent = agent;
	}
	public String getAgent(int i) {
		return agent[i];
	}
	public String[] getCommission() {
		return commission;
	}
	public void setCommission(String[] commission) {
		this.commission = commission;
	}
	public String getCommission(int i) {
		return commission[i];
	}
	public String[] getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String[] effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getEffectiveDate(int i) {
		return effectiveDate[i];
	}
	public String[] getEsaId() {
		return esaId;
	}
	public void setEsaId(String[] esaId) {
		this.esaId = esaId;
	}
	public String getEsaId(int i) {
		return esaId[i];
	}
	public String[] getEsaSeqNo() {
		return esaSeqNo;
	}
	public void setEsaSeqNo(String[] esaSeqNo) {
		this.esaSeqNo = esaSeqNo;
	}
	public String getEsaSeqNo(int i) {
		return esaSeqNo[i];
	}
	public String[] getPayementTerms() {
		return payementTerms;
	}
	public void setPayementTerms(String[] payementTerms) {
		this.payementTerms = payementTerms;
	}
	public String getPayementTerms(int i) {
		return payementTerms[i];
	}
	public String[] getStatus() {
		return status;
	}
	public void setStatus(String[] status) {
		this.status = status;
	}
	public String getStatus(int i) {
		return status[i];
	}
	public ArrayList<ESAList> getEsaList() {
		return esaList;
	}
	public void setEsaList(ArrayList<ESAList> esaList) {
		this.esaList = esaList;
	}
	public String[] getCommissionType() {
		return commissionType;
	}
	public void setCommissionType(String[] commissionType) {
		this.commissionType = commissionType;
	}
	public String getCommissionType(int i) {
		return commissionType[i];
	}
		public String getFromPage() {
		return fromPage;
	}
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}
	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}
	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}
	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	
	
}
