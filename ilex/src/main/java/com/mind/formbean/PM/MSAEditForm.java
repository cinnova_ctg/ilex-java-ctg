package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class MSAEditForm extends ActionForm {

	private String orgTopId = null;
	private String msaId = null;
	private String msaName = null;
	
	private String msaType = null;
	private String salesType = null;
	private String salesPOC = null;
	private String unionUpliftFactor = null;

	private String payTerms = null;
	private String payQualifier = null;
	
	private String reqPlannedSch = null;
	private String effPlannedSch = null;
	private String schDays = null;
	
	private String draftReviews = null;
	private String businessReviews = null;
	
	private String custPOCBusiness = null;
	private String custPOCContract = null;
	
	private String createInfo = null;
	private String changeInfo = null;
	
	private String save = null;
	private String back = null;
	private String cancel = null;
	private String status = null;
	private String actionAddUpdate = null;
	private String file_upload_check = null;
	private String fromType = null; 

	// For Edit BDm page
	private String curBdm="";
	
//	For the div which should appear in Every Form.
	
	private String monthWeekCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}
	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}
	public String getOtherCheck() {
		return otherCheck;
	}
	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}
	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}
	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}
	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}
	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}
	public String[] getSelectedOwners() {
		return selectedOwners;
	}
	public String getSelectedOwners( int i ) {
		return selectedOwners[i];
	}
	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}
	public String[] getSelectedStatus() {
		return selectedStatus;
	}
	public String getSelectedStatus( int i ) {
		return selectedStatus[i];
	}
	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}
	public String getFromType() {
		return fromType;
	}
	public void setFromType(String fromType) {
		this.fromType = fromType;
	}
	public String getBusinessReviews() {
		return businessReviews;
	}
	public void setBusinessReviews(String businessReviews) {
		this.businessReviews = businessReviews;
	}
	public String getCustPOCBusiness() {
		return custPOCBusiness;
	}
	public void setCustPOCBusiness(String custPOCBusiness) {
		this.custPOCBusiness = custPOCBusiness;
	}
	public String getCustPOCContract() {
		return custPOCContract;
	}
	public void setCustPOCContract(String custPOCContract) {
		this.custPOCContract = custPOCContract;
	}
	public String getDraftReviews() {
		return draftReviews;
	}
	public void setDraftReviews(String draftReviews) {
		this.draftReviews = draftReviews;
	}
	public String getEffPlannedSch() {
		return effPlannedSch;
	}
	public void setEffPlannedSch(String effPlannedSch) {
		this.effPlannedSch = effPlannedSch;
	}
	public String getFile_upload_check() {
		return file_upload_check;
	}
	public void setFile_upload_check(String file_upload_check) {
		this.file_upload_check = file_upload_check;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getMsaType() {
		return msaType;
	}
	public void setMsaType(String msaType) {
		this.msaType = msaType;
	}
	public String getPayQualifier() {
		return payQualifier;
	}
	public void setPayQualifier(String payQualifier) {
		this.payQualifier = payQualifier;
	}
	public String getPayTerms() {
		return payTerms;
	}
	public void setPayTerms(String payTerms) {
		this.payTerms = payTerms;
	}
	public String getReqPlannedSch() {
		return reqPlannedSch;
	}
	public void setReqPlannedSch(String reqPlannedSch) {
		this.reqPlannedSch = reqPlannedSch;
	}
	public String getSalesPOC() {
		return salesPOC;
	}
	public void setSalesPOC(String salesPOC) {
		this.salesPOC = salesPOC;
	}
	public String getSalesType() {
		return salesType;
	}
	public void setSalesType(String salesType) {
		this.salesType = salesType;
	}
	public String getSchDays() {
		return schDays;
	}
	public void setSchDays(String schDays) {
		this.schDays = schDays;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUnionUpliftFactor() {
		return unionUpliftFactor;
	}
	public void setUnionUpliftFactor(String unionUpliftFactor) {
		this.unionUpliftFactor = unionUpliftFactor;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getOrgTopId() {
		return orgTopId;
	}
	public void setOrgTopId(String orgTopId) {
		this.orgTopId = orgTopId;
	}
	public String getChangeInfo() {
		return changeInfo;
	}
	public void setChangeInfo(String changeInfo) {
		this.changeInfo = changeInfo;
	}
	public String getCreateInfo() {
		return createInfo;
	}
	public void setCreateInfo(String createInfo) {
		this.createInfo = createInfo;
	}
	public String getActionAddUpdate() {
		return actionAddUpdate;
	}
	public void setActionAddUpdate(String action) {
		this.actionAddUpdate = action;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getCurBdm() {
		return curBdm;
	}
	public void setCurBdm(String curBdm) {
		this.curBdm = curBdm;
	}
}
