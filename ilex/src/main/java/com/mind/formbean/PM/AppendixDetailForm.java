package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class AppendixDetailForm extends ActionForm {
	private String msa_id = null;
	private String appendix_Id = null;

	private String name = null;
	private String appendixtype = null;
	private String msp = null;

	private String cnsPOC = null;
	private String siteno = null;

	private String customerdivision = null;
	private String estimatedcost = null;
	private String extendedprice = null;
	private String proformamargin = null;

	private String reqplasch = null;
	private String effplasch = null;
	private String schdays = null;

	private String draftreviews = null;
	private String businessreviews = null;

	private String custPocbusiness = null;
	private String custPoccontract = null;
	private String custPocbilling = null;

	private String lastcomment = null;
	private String lastchangeby = null;
	private String lastchangedate = null;

	private String lastappendixmodifiedby = null;
	private String lastappendixmodifieddate = null;

	private String status = null;

	private String unionupliftfactor = null;
	private String file_upload_check = null;

	private String msaname = null;
	private String testAppendix = null;

	// New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String add = null;
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	private String monthWeekCheck = null;
	private String travelAuthorized = null;
	private String checkAppendix = null;
	private String appendixTypeBreakout;

	public String getCheckAppendix() {
		return checkAppendix;
	}

	public void setCheckAppendix(String checkAppendix) {
		this.checkAppendix = checkAppendix;
	}

	public String getTravelAuthorized() {
		return travelAuthorized;
	}

	public void setTravelAuthorized(String travelAuthorized) {
		this.travelAuthorized = travelAuthorized;
	}

	public String getFile_upload_check() {
		return file_upload_check;
	}

	public void setFile_upload_check(String file_upload_check) {
		this.file_upload_check = file_upload_check;
	}

	public String getMsa_id() {
		return msa_id;
	}

	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}

	public String getCustomerdivision() {
		return customerdivision;
	}

	public void setCustomerdivision(String customerdivision) {
		this.customerdivision = customerdivision;
	}

	public String getAppendix_Id() {
		return appendix_Id;
	}

	public void setAppendix_Id(String appendix_Id) {
		this.appendix_Id = appendix_Id;
	}

	public String getAppendixtype() {
		return appendixtype;
	}

	public void setAppendixtype(String appendixtype) {
		this.appendixtype = appendixtype;
	}

	public String getBusinessreviews() {
		return businessreviews;
	}

	public void setBusinessreviews(String businessreviews) {
		this.businessreviews = businessreviews;
	}

	public String getCnsPOC() {
		return cnsPOC;
	}

	public void setCnsPOC(String cnsPOC) {
		this.cnsPOC = cnsPOC;
	}

	public String getCustPocbilling() {
		return custPocbilling;
	}

	public void setCustPocbilling(String custPocbilling) {
		this.custPocbilling = custPocbilling;
	}

	public String getCustPocbusiness() {
		return custPocbusiness;
	}

	public void setCustPocbusiness(String custPocbusiness) {
		this.custPocbusiness = custPocbusiness;
	}

	public String getCustPoccontract() {
		return custPoccontract;
	}

	public void setCustPoccontract(String custPoccontract) {
		this.custPoccontract = custPoccontract;
	}

	public String getDraftreviews() {
		return draftreviews;
	}

	public void setDraftreviews(String draftreviews) {
		this.draftreviews = draftreviews;
	}

	public String getEffplasch() {
		return effplasch;
	}

	public void setEffplasch(String effplasch) {
		this.effplasch = effplasch;
	}

	public String getEstimatedcost() {
		return estimatedcost;
	}

	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}

	public String getExtendedprice() {
		return extendedprice;
	}

	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}

	public String getLastcomment() {
		return lastcomment;
	}

	public void setLastcomment(String lastcomment) {
		this.lastcomment = lastcomment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProformamargin() {
		return proformamargin;
	}

	public void setProformamargin(String proformamargin) {
		this.proformamargin = proformamargin;
	}

	public String getReqplasch() {
		return reqplasch;
	}

	public void setReqplasch(String reqplasch) {
		this.reqplasch = reqplasch;
	}

	public String getSchdays() {
		return schdays;
	}

	public void setSchdays(String schdays) {
		this.schdays = schdays;
	}

	public String getSiteno() {
		return siteno;
	}

	public void setSiteno(String siteno) {
		this.siteno = siteno;
	}

	public String getLastchangeby() {
		return lastchangeby;
	}

	public void setLastchangeby(String lastchangeby) {
		this.lastchangeby = lastchangeby;
	}

	public String getLastchangedate() {
		return lastchangedate;
	}

	public void setLastchangedate(String lastchangedate) {
		this.lastchangedate = lastchangedate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUnionupliftfactor() {
		return unionupliftfactor;
	}

	public void setUnionupliftfactor(String unionupliftfactor) {
		this.unionupliftfactor = unionupliftfactor;
	}

	public String getLastappendixmodifiedby() {
		return lastappendixmodifiedby;
	}

	public void setLastappendixmodifiedby(String lastappendixmodifiedby) {
		this.lastappendixmodifiedby = lastappendixmodifiedby;
	}

	public String getLastappendixmodifieddate() {
		return lastappendixmodifieddate;
	}

	public void setLastappendixmodifieddate(String lastappendixmodifieddate) {
		this.lastappendixmodifieddate = lastappendixmodifieddate;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getAdd() {
		return add;
	}

	public void setAdd(String add) {
		this.add = add;
	}

	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}

	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}

	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}

	public String getAppendixSelectedOwners(int i) {
		return appendixSelectedOwners[i];
	}

	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}

	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}

	public String getAppendixSelectedStatus(int i) {
		return appendixSelectedStatus[i];
	}

	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String getMsp() {
		return msp;
	}

	public void setMsp(String msp) {
		this.msp = msp;
	}

	public String getTestAppendix() {
		return testAppendix;
	}

	public void setTestAppendix(String testAppendix) {
		this.testAppendix = testAppendix;
	}

	/**
	 * @return the appendixTypeBreakout
	 */
	public String getAppendixTypeBreakout() {
		return appendixTypeBreakout;
	}

	/**
	 * @param appendixTypeBreakout
	 *            the appendixTypeBreakout to set
	 */
	public void setAppendixTypeBreakout(String appendixTypeBreakout) {
		this.appendixTypeBreakout = appendixTypeBreakout;
	}

}
