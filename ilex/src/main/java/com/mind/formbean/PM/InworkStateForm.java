package com.mind.formbean.PM;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class InworkStateForm extends ActionForm {
	
	private ArrayList inworkRows=null;
	private String inwork=null;
	private String[] check=null;
	
	public void reset(ActionMapping mapping, HttpServletRequest request){	
		inworkRows=null;	
		inwork=null;
		check=null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors= new ActionErrors();		
		return errors; 
	}

	public String getInwork() {
		return inwork;
	}

	public void setInwork(String inwork) {
		this.inwork = inwork;
	}

	public ArrayList getInworkRows() {
		return inworkRows;
	}

	public void setInworkRows(ArrayList inworkRows) {
		this.inworkRows = inworkRows;
	}

	public String[] getCheck() {
		return check;
	}

	public void setCheck(String[] check) {
		this.check = check;
	}	
}
