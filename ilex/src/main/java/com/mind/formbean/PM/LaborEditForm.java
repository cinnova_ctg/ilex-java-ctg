package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class LaborEditForm extends ActionForm {
	
	private String activity_Id = null;
	private String laborid = null;
	private String laborcostlibid = null;
	private String laborname = null;
	private String labortype = null;
	private String cnspartnumber = null;
	private String quantityhours = null;
	private String prevquantityhours = null;
	private String estimatedhourlybasecost = null;
	private String estimatedtotalcost = null;
	private String proformamargin = null;
	private String priceunit = null;
	private String priceextended = null;
	private String sellablequantity = null;
	private String minimumquantity = null;
	private String status = null;
	private String subcategory = null;
	private String addendum_id = null;
	private String transit = null;
	private String checktransit = null;
	private String activity_name = null;
	private String msaname = null;
	private String chkaddendum = null;
	private String flag = null;
	private String appendixId = null;
	private String AppendixName = null;
	private String jobName = null;
	private String jobId = null;
	private String msaName = null;
	private String msaId = null;
	private String activityName = null;
	private String clr_detail_id = null;
	private String save = null;
	private String fromflag = null;
	private String dashboardid = null;
	private String ref = null;
	private String createdBy = null;
	private String createdDate = null;
	private String changedBy = null;
	private String changedDate = null;
	
	
	
	public String getChangedBy() {
		return changedBy;
	}
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}
	public String getChangedDate() {
		return changedDate;
	}
	public void setChangedDate(String changedDate) {
		this.changedDate = changedDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getDashboardid() {
		return dashboardid;
	}
	public void setDashboardid(String dashboardid) {
		this.dashboardid = dashboardid;
	}
	public String getFromflag() {
		return fromflag;
	}
	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id(String activity_Id) {
		this.activity_Id = activity_Id;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}
	public String getChecktransit() {
		return checktransit;
	}
	public void setChecktransit(String checktransit) {
		this.checktransit = checktransit;
	}
	public String getCnspartnumber() {
		return cnspartnumber;
	}
	public void setCnspartnumber(String cnspartnumber) {
		this.cnspartnumber = cnspartnumber;
	}
	public String getEstimatedhourlybasecost() {
		return estimatedhourlybasecost;
	}
	public void setEstimatedhourlybasecost(String estimatedhourlybasecost) {
		this.estimatedhourlybasecost = estimatedhourlybasecost;
	}
	public String getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost(String estimatedtotalcost) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	public String getLaborcostlibid() {
		return laborcostlibid;
	}
	public void setLaborcostlibid(String laborcostlibid) {
		this.laborcostlibid = laborcostlibid;
	}
	public String getLaborid() {
		return laborid;
	}
	public void setLaborid(String laborid) {
		this.laborid = laborid;
	}
	public String getLaborname() {
		return laborname;
	}
	public void setLaborname(String laborname) {
		this.laborname = laborname;
	}
	public String getLabortype() {
		return labortype;
	}
	public void setLabortype(String labortype) {
		this.labortype = labortype;
	}
	public String getMinimumquantity() {
		return minimumquantity;
	}
	public void setMinimumquantity(String minimumquantity) {
		this.minimumquantity = minimumquantity;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getPrevquantityhours() {
		return prevquantityhours;
	}
	public void setPrevquantityhours(String prevquantityhours) {
		this.prevquantityhours = prevquantityhours;
	}
	public String getPriceextended() {
		return priceextended;
	}
	public void setPriceextended(String priceextended) {
		this.priceextended = priceextended;
	}
	public String getPriceunit() {
		return priceunit;
	}
	public void setPriceunit(String priceunit) {
		this.priceunit = priceunit;
	}
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin(String proformamargin) {
		this.proformamargin = proformamargin;
	}
	public String getQuantityhours() {
		return quantityhours;
	}
	public void setQuantityhours(String quantityhours) {
		this.quantityhours = quantityhours;
	}
	public String getSellablequantity() {
		return sellablequantity;
	}
	public void setSellablequantity(String sellablequantity) {
		this.sellablequantity = sellablequantity;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
	public String getTransit() {
		return transit;
	}
	public void setTransit(String transit) {
		this.transit = transit;
	}
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getAppendixName() {
		return AppendixName;
	}
	public void setAppendixName(String appendixName) {
		AppendixName = appendixName;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getClr_detail_id() {
		return clr_detail_id;
	}
	public void setClr_detail_id(String clr_detail_id) {
		this.clr_detail_id = clr_detail_id;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	
	
	

}
