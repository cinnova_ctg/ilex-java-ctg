package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class MarginComputationForm extends ActionForm {
	
	
	private String resourceName = null;
	private String estUnitCost = null;
	private String targetUnitPrice = null;
	private String newMargin = null;
	private String position = null;
	private String size = null;
	private String materialResourceSize = null;
	private String travelResourceSize = null;
	private String freightResourceSize = null;
	private String laborResourceSize = null;
	
	public String getEstUnitCost() {
		return estUnitCost;
	}
	public void setEstUnitCost(String estUnitCost) {
		this.estUnitCost = estUnitCost;
	}
	public String getNewMargin() {
		return newMargin;
	}
	public void setNewMargin(String newMargin) {
		this.newMargin = newMargin;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getTargetUnitPrice() {
		return targetUnitPrice;
	}
	public void setTargetUnitPrice(String targetUnitPrice) {
		this.targetUnitPrice = targetUnitPrice;
	}
	public String getFreightResourceSize() {
		return freightResourceSize;
	}
	public void setFreightResourceSize(String freightResourceSize) {
		this.freightResourceSize = freightResourceSize;
	}
	public String getLaborResourceSize() {
		return laborResourceSize;
	}
	public void setLaborResourceSize(String laborResourceSize) {
		this.laborResourceSize = laborResourceSize;
	}
	public String getMaterialResourceSize() {
		return materialResourceSize;
	}
	public void setMaterialResourceSize(String materialResourceSize) {
		this.materialResourceSize = materialResourceSize;
	}
	public String getTravelResourceSize() {
		return travelResourceSize;
	}
	public void setTravelResourceSize(String travelResourceSize) {
		this.travelResourceSize = travelResourceSize;
	}
}
