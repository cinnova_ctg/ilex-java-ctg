package com.mind.formbean.PM;

import org.apache.struts.action.ActionForm;

public class AppendixListForm extends ActionForm
{
	private String MSA_Id = null;
	private String newunionupliftfactor = null;
	private String newexpedite24upliftfactor = null;
	private String newexpedite48upliftfactor = null;
	private String newafterhoursupliftfactor = null;
	private String newwhupliftfactor = null;
	
	//New UI Changes 
	private String add = null;
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	private String monthWeekCheck = null;
	
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getAdd() {
		return add;
	}

	public void setAdd(String add) {
		this.add = add;
	}

	

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	

	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}

	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}

	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}
	
	public String getAppendixSelectedOwners( int i ) {
		return appendixSelectedOwners[i];
	}
	

	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}

	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}
	
	public String getAppendixSelectedStatus( int i ) {
		return appendixSelectedStatus[i];
	}
	

	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getMSA_Id() {
		return MSA_Id;
	}

	public void setMSA_Id(String id) {
		MSA_Id = id;
	}

	public String getNewafterhoursupliftfactor() {
		return newafterhoursupliftfactor;
	}

	public void setNewafterhoursupliftfactor(String newafterhoursupliftfactor) {
		this.newafterhoursupliftfactor = newafterhoursupliftfactor;
	}

	public String getNewexpedite24upliftfactor() {
		return newexpedite24upliftfactor;
	}

	public void setNewexpedite24upliftfactor(String newexpedite24upliftfactor) {
		this.newexpedite24upliftfactor = newexpedite24upliftfactor;
	}

	public String getNewexpedite48upliftfactor() {
		return newexpedite48upliftfactor;
	}

	public void setNewexpedite48upliftfactor(String newexpedite48upliftfactor) {
		this.newexpedite48upliftfactor = newexpedite48upliftfactor;
	}

	public String getNewunionupliftfactor() {
		return newunionupliftfactor;
	}

	public void setNewunionupliftfactor(String newunionupliftfactor) {
		this.newunionupliftfactor = newunionupliftfactor;
	}

	public String getNewwhupliftfactor() {
		return newwhupliftfactor;
	}

	public void setNewwhupliftfactor(String newwhupliftfactor) {
		this.newwhupliftfactor = newwhupliftfactor;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}
	
	
	}
