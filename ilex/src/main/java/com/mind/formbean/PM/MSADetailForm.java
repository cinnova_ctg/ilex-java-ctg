package com.mind.formbean.PM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class MSADetailForm extends ActionForm
{
	private static final long serialVersionUID = 3257563997132698416L;
	private String msaId = null;
	private String name = null;
	private String msatype = null;
	private String salestype = null;
	private String salesPOC = null;
	private String payterms = null;
	private String payqualifier = null;
	private String unionupliftfactor = null;
	private String formaldocdate = null;
	private String reqplasch = null;
	private String effplasch = null;
	private String schdays = null;
	private String draftreviews = null;
	private String businessreviews = null;
	private String custPocbusiness = null;
	private String custPoccontract = null;
	private String lastcomment = null;
	private String lastchangeby = null;
	private String lastchangedate = null;
	private String status = null;
	private String file_upload_check = null;
	private String lastmsamodifieddate = null;
	private String lastmsamodifiedby = null; 
	
	
	//For the div which should appear in Every Form.
	
	private String monthWeekCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	

	
	
	
	
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId( String msaId ) {
		this.msaId = msaId;
	}

	public void setName( String name )
	{
			this.name = name;
			
	}
	
	public String getName()
	{
		return name;
	}
	public void setMsatype( String msatype )
	{
			this.msatype = msatype;
	}
	
	public String getMsatype()
	{
		return msatype;
	}
	
	public void setSalestype( String n )
	{
		salestype=n;
	}
	
	public String getSalestype()
	{
		return salestype;
	}
	
	public void setSalesPOC( String n )
	{
		salesPOC = n;
	}
	
	public String getSalesPOC()
	{
		return salesPOC;
	}

	public void setPayterms( String n )
	{
		payterms = n;
	}
	
	public String getPayterms()
	{
		return payterms;
	}
	
	public void setPayqualifier( String n )
	{
		payqualifier = n;
	}
	
	
	public String getUnionupliftfactor() {
		return unionupliftfactor;
	}

	public void setUnionupliftfactor( String unionupliftfactor ) {
		this.unionupliftfactor = unionupliftfactor;
	}

	public String getPayqualifier()
	{
		return payqualifier;
	}
	
	public void setFormaldocdate( String n )
	{
		formaldocdate = n;
	}
	
	public String getFormaldocdate()
	{
		return formaldocdate;
	}
	
	public void setReqplasch( String n )
	{
		reqplasch = n;
	}
	
	public String getReqplasch()
	{
		return reqplasch;
	}

	public void setEffplasch( String n )
	{
		effplasch = n;
	}
	
	public String getEffplasch()
	{
		return effplasch;
	}
	
	public void setSchdays( String n )
	{
		schdays = n;
	}
	
	public String getSchdays()
	{
		return schdays;
	}
	
	public void setDraftreviews( String n )
	{
		draftreviews = n;
	}
	
	public String getDraftreviews()
	{
		return draftreviews;
	}
	
	public void setBusinessreviews( String n )
	{
		businessreviews = n;
	}
	
	public String getBusinessreviews()
	{
		return businessreviews;
	}

	public void setCustPocbusiness( String n )
	{
		custPocbusiness = n;
	}
	
	public String getCustPocbusiness()
	{
		return custPocbusiness;
	}
	
	public void setCustPoccontract( String n )
	{
		custPoccontract = n;
	}
	
	public String getCustPoccontract()
	{
		return custPoccontract;
	}

	public void setStatus( String n )
	{
		status = n;
	}
	
	public String getLastcomment()
	{
		return lastcomment;
	}
	
	public void setLastcomment( String c )
	{
		lastcomment = c;
	}
	
	public String getLastchangeby() {
		return lastchangeby;
	}

	public void setLastchangeby( String lastchangeby ) {
		this.lastchangeby = lastchangeby;
	}

	public String getLastchangedate() {
		return lastchangedate;
	}

	public void setLastchangedate( String lastchangedate ) {
		this.lastchangedate = lastchangedate;
	}

	public String getStatus()
	{
		return status;
	}
	
	
	
	public String getFile_upload_check() {
		return file_upload_check;
	}

	public void setFile_upload_check( String file_upload_check ) {
		this.file_upload_check = file_upload_check;
	}
	
	
	public String getLastmsamodifiedby() {
		return lastmsamodifiedby;
	}

	public void setLastmsamodifiedby( String lastmsamodifiedby ) {
		this.lastmsamodifiedby = lastmsamodifiedby;
	}

	public String getLastmsamodifieddate() {
		return lastmsamodifieddate;
	}

	public void setLastmsamodifieddate( String lastmsamodifieddate ) {
		this.lastmsamodifieddate = lastmsamodifieddate;
	}

	public void reset( ActionMapping mapping , HttpServletRequest request )
	{
		name = null;
		msatype = null;
		salestype = null;
		salesPOC = null;
		payterms = null;
		payqualifier = null;
		formaldocdate = null;
		reqplasch = null;
		effplasch = null;
		schdays = null;
		draftreviews = null;
		businessreviews = null;
		custPocbusiness = null;
		custPoccontract = null;
		lastcomment = null;
		lastchangeby = null;
		lastchangedate = null;
		status = null;
		file_upload_check = null;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getOtherCheck() {
		return otherCheck;
	}

	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String[] getSelectedOwners() {
		return selectedOwners;
	}

	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}
	public String getSelectedOwners( int i ) {
		return selectedOwners[i];
	}

	public String[] getSelectedStatus() {
		return selectedStatus;
	}
	public String getSelectedStatus( int i ) {
		return selectedStatus[i];
	}
	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}
}
