package com.mind.formbean.PM;

import java.util.ArrayList;

import com.mind.bean.pm.ESAActivityList;
import com.mind.bean.pm.ESADetailsPerActivity;
import com.mind.common.LabelValue;
import com.mind.common.ViewSelectorForm;

public class ESAActivityCommissionForm extends ViewSelectorForm {

	private String[] activityId = null;
	private String[] activityName = null;
	private String[] activityQty = null;
	private String[] resourceBasis = null;
	private String[] resRevenue = null;
	
	private String[] esaId1 = null;
	private String[] actCommissionType1 = null;
	private String[] commissionStatus1 = null;
	private String[] commissionRate1 = null;
	private String[] esaActCost1 = null;
	private String[] esaActRevenue1 = null;
	private String[] esaActActiveStatus1 = null;
	
	private String[] esaId2 = null;
	private String[] actCommissionType2 = null;
	private String[] commissionStatus2 = null;
	private String[] commissionRate2 = null;
	private String[] esaActCost2 = null;
	private String[] esaActRevenue2 = null;
	private String[] esaActActiveStatus2 = null;
	
	private String[] esaId3 = null;
	private String[] actCommissionType3 = null;
	private String[] commissionStatus3 = null;
	private String[] commissionRate3 = null;
	private String[] esaActCost3 = null;
	private String[] esaActRevenue3 = null;
	private String[] esaActActiveStatus3 = null;
	
	private ArrayList<LabelValue> commisionType = null;
	private ArrayList<LabelValue> agents = null;
	private ArrayList<ESADetailsPerActivity> esaDetailsPerActivity = null; 
	private ArrayList<ESAActivityList> esaActivityList = null;
	
	//private String appendixId = null;
	private String jobid = null;
	private String save = null;
	private String jobRef = null;
	
	public String getJobRef() {
		return jobRef;
	}

	public void setJobRef(String jobRef) {
		this.jobRef = jobRef;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String[] getActivityId() {
		return activityId;
	}

	public void setActivityId(String[] activityId) {
		this.activityId = activityId;
	}

	public String getActivityId(int i) {
		return activityId[i];
	}
	
	public String[] getActivityName() {
		return activityName;
	}

	public void setActivityName(String[] activityName) {
		this.activityName = activityName;
	}
	
	public String getActivityName(int i) {
		return activityName[i];
	}

	public String[] getActivityQty() {
		return activityQty;
	}

	public void setActivityQty(String[] activityQty) {
		this.activityQty = activityQty;
	}

	public String getActivityQty(int i) {
		return activityQty[i];
	}
	
	public String[] getResourceBasis() {
		return resourceBasis;
	}

	public void setResourceBasis(String[] resourceBasis) {
		this.resourceBasis = resourceBasis;
	}

	public String getResourceBasis(int i) {
		return resourceBasis[i];
	}
	
	public String[] getResRevenue() {
		return resRevenue;
	}

	public void setResRevenue(String[] resRevenue) {
		this.resRevenue = resRevenue;
	}

	public String getResRevenue(int i) {
		return resRevenue[i];
	}
	
	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public ArrayList<ESADetailsPerActivity> getEsaDetailsPerActivity() {
		return esaDetailsPerActivity;
	}

	public void setEsaDetailsPerActivity(
			ArrayList<ESADetailsPerActivity> esaDetailsPerActivity) {
		this.esaDetailsPerActivity = esaDetailsPerActivity;
	}

	public ArrayList<ESAActivityList> getEsaActivityList() {
		return esaActivityList;
	}

	public void setEsaActivityList(ArrayList<ESAActivityList> esaActivityList) {
		this.esaActivityList = esaActivityList;
	}

	public ArrayList<LabelValue> getCommisionType() {
		return commisionType;
	}

	public void setCommisionType(ArrayList<LabelValue> commisionType) {
		this.commisionType = commisionType;
	}

	public ArrayList<LabelValue> getAgents() {
		return agents;
	}

	public void setAgents(ArrayList<LabelValue> agents) {
		this.agents = agents;
	}

	public String[] getActCommissionType1() {
		return actCommissionType1;
	}

	public void setActCommissionType1(String[] actCommissionType1) {
		this.actCommissionType1 = actCommissionType1;
	}

	public String getActCommissionType1(int i) {
		return actCommissionType1[i];
	}
	
	public String[] getActCommissionType2() {
		return actCommissionType2;
	}

	public void setActCommissionType2(String[] actCommissionType2) {
		this.actCommissionType2 = actCommissionType2;
	}
	
	public String getActCommissionType2(int i) {
		return actCommissionType2[i];
	}

	public String[] getActCommissionType3() {
		return actCommissionType3;
	}

	public void setActCommissionType3(String[] actCommissionType3) {
		this.actCommissionType3 = actCommissionType3;
	}

	public String getActCommissionType3(int i) {
		return actCommissionType3[i];
	}
	
	public String[] getCommissionRate1() {
		return commissionRate1;
	}

	public void setCommissionRate1(String[] commissionRate1) {
		this.commissionRate1 = commissionRate1;
	}

	public String getCommissionRate1(int i) {
		return commissionRate1[i];
	}
	
	public String[] getCommissionRate2() {
		return commissionRate2;
	}

	public void setCommissionRate2(String[] commissionRate2) {
		this.commissionRate2 = commissionRate2;
	}

	public String getCommissionRate2(int i) {
		return commissionRate2[i];
	}
	
	public String[] getCommissionRate3() {
		return commissionRate3;
	}

	public void setCommissionRate3(String[] commissionRate3) {
		this.commissionRate3 = commissionRate3;
	}

	public String getCommissionRate3(int i) {
		return commissionRate3[i];
	}
	
	public String[] getCommissionStatus1() {
		return commissionStatus1;
	}

	public void setCommissionStatus1(String[] commissionStatus1) {
		this.commissionStatus1 = commissionStatus1;
	}

	public String getCommissionStatus1(int i) {
		return commissionStatus1[i];
	}
	
	public String[] getCommissionStatus2() {
		return commissionStatus2;
	}

	public void setCommissionStatus2(String[] commissionStatus2) {
		this.commissionStatus2 = commissionStatus2;
	}

	public String getCommissionStatus2(int i) {
		return commissionStatus2[i];
	}
	
	public String[] getCommissionStatus3() {
		return commissionStatus3;
	}

	public void setCommissionStatus3(String[] commissionStatus3) {
		this.commissionStatus3 = commissionStatus3;
	}

	public String getCommissionStatus3(int i) {
		return commissionStatus3[i];
	}
	
	public String[] getEsaActCost1() {
		return esaActCost1;
	}

	public void setEsaActCost1(String[] esaActCost1) {
		this.esaActCost1 = esaActCost1;
	}

	public String getEsaActCost1(int i) {
		return esaActCost1[i];
	}
	
	public String[] getEsaActCost2() {
		return esaActCost2;
	}

	public void setEsaActCost2(String[] esaActCost2) {
		this.esaActCost2 = esaActCost2;
	}

	public String getEsaActCost2(int i) {
		return esaActCost2[i];
	}
	
	public String[] getEsaActCost3() {
		return esaActCost3;
	}

	public void setEsaActCost3(String[] esaActCost3) {
		this.esaActCost3 = esaActCost3;
	}

	public String getEsaActCost3(int i) {
		return esaActCost3[i];
	}
	
	public String[] getEsaActRevenue1() {
		return esaActRevenue1;
	}

	public void setEsaActRevenue1(String[] esaActRevenue1) {
		this.esaActRevenue1 = esaActRevenue1;
	}

	public String getEsaActRevenue1(int i) {
		return esaActRevenue1[i];
	}
	
	public String[] getEsaActRevenue2() {
		return esaActRevenue2;
	}

	public String getEsaActRevenue2(int i) {
		return esaActRevenue2[i];
	}
	public void setEsaActRevenue2(String[] esaActRevenue2) {
		this.esaActRevenue2 = esaActRevenue2;
	}

	public String[] getEsaActRevenue3() {
		return esaActRevenue3;
	}

	public void setEsaActRevenue3(String[] esaActRevenue3) {
		this.esaActRevenue3 = esaActRevenue3;
	}

	public String getEsaActRevenue3(int i) {
		return esaActRevenue3[i];
	}
	
	public String[] getEsaId1() {
		return esaId1;
	}

	public void setEsaId1(String[] esaId1) {
		this.esaId1 = esaId1;
	}

	public String getEsaId1(int i) {
		return esaId1[i];
	}
	
	public String[] getEsaId2() {
		return esaId2;
	}

	public void setEsaId2(String[] esaId2) {
		this.esaId2 = esaId2;
	}

	public String getEsaId2(int i) {
		return esaId2[i];
	}
	
	public String[] getEsaId3() {
		return esaId3;
	}

	public void setEsaId3(String[] esaId3) {
		this.esaId3 = esaId3;
	}

	public String getEsaId3(int i) {
		return esaId3[i];
	}

	public String[] getEsaActActiveStatus1() {
		return esaActActiveStatus1;
	}

	public void setEsaActActiveStatus1(String[] esaActActiveStatus1) {
		this.esaActActiveStatus1 = esaActActiveStatus1;
	}

	public String getEsaActActiveStatus1(int i) {
		return esaActActiveStatus1[i];
	}
	
	public String[] getEsaActActiveStatus2() {
		return esaActActiveStatus2;
	}

	public void setEsaActActiveStatus2(String[] esaActActiveStatus2) {
		this.esaActActiveStatus2 = esaActActiveStatus2;
	}

	public String getEsaActActiveStatus2(int i) {
		return esaActActiveStatus2[i];
	}
	
	public String[] getEsaActActiveStatus3() {
		return esaActActiveStatus3;
	}

	public void setEsaActActiveStatus3(String[] esaActActiveStatus3) {
		this.esaActActiveStatus3 = esaActActiveStatus3;
	}
	
	public String getEsaActActiveStatus3(int i) {
		return esaActActiveStatus3[i];
	}
	
}
