package com.mind.formbean.CNSWEB;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieForm 
{
	
	//returns true if cookie present else return false
	public boolean getCookie( HttpServletRequest request,String cookieName, String password) 
	{
		Cookie[] cookies;
		cookies=request.getCookies();
		for(int i=0; i<cookies.length; i++)
		{
		      Cookie cookie = cookies[i];
		      if (cookieName.equals(cookie.getName()))
			  {
				  if(cookie.getValue().equals(password))
				  {
					  return true;
				  }
					  
				  else
				  {
					  return false;
				  }
					  
			  }
		        
		    }
		    return false;
		
	}

	//sets cookie
	public void setCookie(HttpServletResponse response, String username, String password) 
	{
		Cookie userCookie = new Cookie(username, password);
		response.addCookie(userCookie);
		
	} 
	

}

