package com.mind.formbean.CNSWEB;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class NetMedXDispatchForm extends ActionForm {
	
	private String lo_om_id=null;
	private String userName=null;
	private String lo_site_location=null;
		
	
	private String lo_poc_name = null;
	private String lo_om_division = null;
	private String lo_poc_phone = null;
	private String lo_poc_email = null;
	
	
	
	private String lo_nd_id = null;
	private String lo_nd_send_copy = null;
	private String lo_nd_send_email = null;
	private String lo_nd_po_no = null;
	private String lo_nd_cust_no = null;
	private String lo_nd_arrival_date = null;
	private String lo_nd_arrival_time_hour = null;
	private String lo_nd_arrival_time_minute = null;
	private String lo_nd_arrival_time_option = null;
	private String lo_nd_arrival_win_bw_hour = null;
	private String lo_nd_arrival_win_bw_minute = null;
	private String lo_nd_arrival_win_bw_option = null;
	private String lo_nd_arrival_win_and_hour = null;
	private String lo_nd_arrival_win_and_minute = null;
	private String lo_nd_arrival_win_and_option = null;
	private String lo_nd_call_criticality = null;
	private String lo_nd_item_category = null;
	private String lo_nd_summary = null;
	private String lo_nd_spec_inst = null;
	private String lo_nd_invoice_cust = null;
    
	
	/* Primary Address  */
	
	private String lo_ad_id1 = null;
	private String lo_ad_address11 = null;
	private String lo_ad_city1 = null;
	private String lo_ad_state1 = null;
	private String lo_ad_zip_code1 = null;
	private String lo_ad_country1 = null;
	
	/* Alternate Billing Address */
	
	private String lo_ad_id2 = null;
	private String lo_ad_name2 = null;
	private String lo_ad_address12 = null;
	private String lo_ad_city2 = null;
	private String lo_ad_state2 = null;
	private String lo_ad_zip_code2 = null;
	private String lo_ad_country2 = null;
	
	
	/* POC 1*/
	
	private String lo_nd_pc_id1 = null;
	private String lo_pc_first_name1 = null; 
	private String lo_pc_last_name1 = null;
	private String lo_pc_phone11 = null;
	
	/* POC 2*/
	
	private String lo_nd_pc_id2 = null;
	private String lo_pc_first_name2 = null; 
	private String lo_pc_last_name2 = null;
	private String lo_pc_phone12 = null;
	
	private FormFile filename = null;
	private String save = null;
	
	/* NetMedx: B */
	

	private String lo_cust_name = null;
	private String lo_aegisticket_number = null;	
	private String lo_po_number = null;
	private String lo_custticket_number = null;	
	private String lo_under_warranty = null;

	/* NetMedx: A */
	
	private String lo_Standby = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	
	public void reset(){
		lo_om_id=null;
		userName=null;
		lo_site_location=null;
			
		
		lo_poc_name = null;
		lo_om_division = null;
		lo_poc_phone = null;
		lo_poc_email = null;
		
		
		
		lo_nd_id = null;
		lo_nd_send_copy = null;
		lo_nd_send_email = null;
		lo_nd_po_no = null;
		lo_nd_cust_no = null;
		lo_nd_arrival_date = null;
		lo_nd_arrival_time_hour = null;
		lo_nd_arrival_time_minute = null;
		lo_nd_arrival_time_option = null;
		lo_nd_arrival_win_bw_hour = null;
		lo_nd_arrival_win_bw_minute = null;
		lo_nd_arrival_win_bw_option = null;
		lo_nd_arrival_win_and_hour = null;
		lo_nd_arrival_win_and_minute = null;
		lo_nd_arrival_win_and_option = null;
		lo_nd_call_criticality = null;
		lo_nd_item_category = null;
		lo_nd_summary = null;
		lo_nd_spec_inst = null;
		lo_nd_invoice_cust = null;
	    
		
		/* Primary Address  */
		
		lo_ad_id1 = null;
		lo_ad_address11 = null;
		lo_ad_city1 = null;
		lo_ad_state1 = null;
		lo_ad_zip_code1 = null;
		lo_ad_country1 = null;
		
		/* Alternate Billing Address */
		
		lo_ad_id2 = null;
		lo_ad_name2 = null;
		lo_ad_address12 = null;
		lo_ad_city2 = null;
		lo_ad_state2 = null;
		lo_ad_zip_code2 = null;
		lo_ad_country2 = null;
		
		
		/* POC 1*/
		
		lo_nd_pc_id1 = null;
		lo_pc_first_name1 = null; 
		lo_pc_last_name1 = null;
		lo_pc_phone11 = null;
		
		/* POC 2*/
		
		lo_nd_pc_id2 = null;
		lo_pc_first_name2 = null; 
		lo_pc_last_name2 = null;
		lo_pc_phone12 = null;
		
		filename = null;
		save = null;
		
		/* NetMedx: B */
		

		lo_cust_name = null;
		lo_aegisticket_number = null;	
		lo_po_number = null;
		lo_custticket_number = null;	

		lo_under_warranty = null;

		/* NetMedx: A */
		
		lo_Standby = null;
		filename2 = null;
		filename3 = null;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getLo_ad_address11() {
		return lo_ad_address11;
	}
	public void setLo_ad_address11(String lo_ad_address11) {
		this.lo_ad_address11 = lo_ad_address11;
	}
	public String getLo_ad_address12() {
		return lo_ad_address12;
	}
	public void setLo_ad_address12(String lo_ad_address12) {
		this.lo_ad_address12 = lo_ad_address12;
	}
	public String getLo_ad_city1() {
		return lo_ad_city1;
	}
	public void setLo_ad_city1(String lo_ad_city1) {
		this.lo_ad_city1 = lo_ad_city1;
	}
	public String getLo_ad_city2() {
		return lo_ad_city2;
	}
	public void setLo_ad_city2(String lo_ad_city2) {
		this.lo_ad_city2 = lo_ad_city2;
	}
	public String getLo_ad_country1() {
		return lo_ad_country1;
	}
	public void setLo_ad_country1(String lo_ad_country1) {
		this.lo_ad_country1 = lo_ad_country1;
	}
	public String getLo_ad_country2() {
		return lo_ad_country2;
	}
	public void setLo_ad_country2(String lo_ad_country2) {
		this.lo_ad_country2 = lo_ad_country2;
	}
	public String getLo_ad_id1() {
		return lo_ad_id1;
	}
	public void setLo_ad_id1(String lo_ad_id1) {
		this.lo_ad_id1 = lo_ad_id1;
	}
	public String getLo_ad_id2() {
		return lo_ad_id2;
	}
	public void setLo_ad_id2(String lo_ad_id2) {
		this.lo_ad_id2 = lo_ad_id2;
	}
	public String getLo_ad_state1() {
		return lo_ad_state1;
	}
	public void setLo_ad_state1(String lo_ad_state1) {
		this.lo_ad_state1 = lo_ad_state1;
	}
	public String getLo_ad_state2() {
		return lo_ad_state2;
	}
	public void setLo_ad_state2(String lo_ad_state2) {
		this.lo_ad_state2 = lo_ad_state2;
	}
	public String getLo_ad_zip_code1() {
		return lo_ad_zip_code1;
	}
	public void setLo_ad_zip_code1(String lo_ad_zip_code1) {
		this.lo_ad_zip_code1 = lo_ad_zip_code1;
	}
	public String getLo_ad_zip_code2() {
		return lo_ad_zip_code2;
	}
	public void setLo_ad_zip_code2(String lo_ad_zip_code2) {
		this.lo_ad_zip_code2 = lo_ad_zip_code2;
	}
	public String getLo_nd_arrival_date() {
		return lo_nd_arrival_date;
	}
	public void setLo_nd_arrival_date(String lo_nd_arrival_date) {
		this.lo_nd_arrival_date = lo_nd_arrival_date;
	}
	public String getLo_nd_call_criticality() {
		return lo_nd_call_criticality;
	}
	public void setLo_nd_call_criticality(String lo_nd_call_criticality) {
		this.lo_nd_call_criticality = lo_nd_call_criticality;
	}
	public String getLo_nd_cust_no() {
		return lo_nd_cust_no;
	}
	public void setLo_nd_cust_no(String lo_nd_cust_no) {
		this.lo_nd_cust_no = lo_nd_cust_no;
	}
	public String getLo_nd_id() {
		return lo_nd_id;
	}
	public void setLo_nd_id(String lo_nd_id) {
		this.lo_nd_id = lo_nd_id;
	}
	public String getLo_nd_invoice_cust() {
		return lo_nd_invoice_cust;
	}
	public void setLo_nd_invoice_cust(String lo_nd_invoice_cust) {
		this.lo_nd_invoice_cust = lo_nd_invoice_cust;
	}
	public String getLo_nd_item_category() {
		return lo_nd_item_category;
	}
	public void setLo_nd_item_category(String lo_nd_item_category) {
		this.lo_nd_item_category = lo_nd_item_category;
	}
	public String getLo_nd_pc_id1() {
		return lo_nd_pc_id1;
	}
	public void setLo_nd_pc_id1(String lo_nd_pc_id1) {
		this.lo_nd_pc_id1 = lo_nd_pc_id1;
	}
	public String getLo_nd_pc_id2() {
		return lo_nd_pc_id2;
	}
	public void setLo_nd_pc_id2(String lo_nd_pc_id2) {
		this.lo_nd_pc_id2 = lo_nd_pc_id2;
	}
	public String getLo_nd_po_no() {
		return lo_nd_po_no;
	}
	public void setLo_nd_po_no(String lo_nd_po_no) {
		this.lo_nd_po_no = lo_nd_po_no;
	}
	public String getLo_nd_send_copy() {
		return lo_nd_send_copy;
	}
	public void setLo_nd_send_copy(String lo_nd_send_copy) {
		this.lo_nd_send_copy = lo_nd_send_copy;
	}
	public String getLo_nd_send_email() {
		return lo_nd_send_email;
	}
	public void setLo_nd_send_email(String lo_nd_send_email) {
		this.lo_nd_send_email = lo_nd_send_email;
	}
	public String getLo_nd_spec_inst() {
		return lo_nd_spec_inst;
	}
	public void setLo_nd_spec_inst(String lo_nd_spec_inst) {
		this.lo_nd_spec_inst = lo_nd_spec_inst;
	}
	public String getLo_nd_summary() {
		return lo_nd_summary;
	}
	public void setLo_nd_summary(String lo_nd_summary) {
		this.lo_nd_summary = lo_nd_summary;
	}
	public String getLo_pc_first_name1() {
		return lo_pc_first_name1;
	}
	public void setLo_pc_first_name1(String lo_pc_first_name1) {
		this.lo_pc_first_name1 = lo_pc_first_name1;
	}
	public String getLo_pc_first_name2() {
		return lo_pc_first_name2;
	}
	public void setLo_pc_first_name2(String lo_pc_first_name2) {
		this.lo_pc_first_name2 = lo_pc_first_name2;
	}
	public String getLo_pc_last_name1() {
		return lo_pc_last_name1;
	}
	public void setLo_pc_last_name1(String lo_pc_last_name1) {
		this.lo_pc_last_name1 = lo_pc_last_name1;
	}
	public String getLo_pc_last_name2() {
		return lo_pc_last_name2;
	}
	public void setLo_pc_last_name2(String lo_pc_last_name2) {
		this.lo_pc_last_name2 = lo_pc_last_name2;
	}
	public String getLo_pc_phone11() {
		return lo_pc_phone11;
	}
	public void setLo_pc_phone11(String lo_pc_phone11) {
		this.lo_pc_phone11 = lo_pc_phone11;
	}
	public String getLo_pc_phone12() {
		return lo_pc_phone12;
	}
	public void setLo_pc_phone12(String lo_pc_phone12) {
		this.lo_pc_phone12 = lo_pc_phone12;
	}
	public String getLo_om_division() {
		return lo_om_division;
	}
	public void setLo_om_division(String lo_om_division) {
		this.lo_om_division = lo_om_division;
	}
	public String getLo_poc_email() {
		return lo_poc_email;
	}
	public void setLo_poc_email(String lo_poc_email) {
		this.lo_poc_email = lo_poc_email;
	}
	public String getLo_poc_name() {
		return lo_poc_name;
	}
	public void setLo_poc_name(String lo_poc_name) {
		this.lo_poc_name = lo_poc_name;
	}
	public String getLo_poc_phone() {
		return lo_poc_phone;
	}
	public void setLo_poc_phone(String lo_poc_phone) {
		this.lo_poc_phone = lo_poc_phone;
	}
	public String getLo_ad_name2() {
		return lo_ad_name2;
	}
	public void setLo_ad_name2(String lo_ad_name2) {
		this.lo_ad_name2 = lo_ad_name2;
	}
	public FormFile getFilename() {
		return filename;
	}
	public void setFilename(FormFile filename) {
		this.filename = filename;
	}
	public String getLo_nd_arrival_time_hour() {
		return lo_nd_arrival_time_hour;
	}
	public void setLo_nd_arrival_time_hour(String lo_nd_arrival_time_hour) {
		this.lo_nd_arrival_time_hour = lo_nd_arrival_time_hour;
	}
	public String getLo_nd_arrival_time_minute() {
		return lo_nd_arrival_time_minute;
	}
	public void setLo_nd_arrival_time_minute(String lo_nd_arrival_time_minute) {
		this.lo_nd_arrival_time_minute = lo_nd_arrival_time_minute;
	}
	public String getLo_nd_arrival_time_option() {
		return lo_nd_arrival_time_option;
	}
	public void setLo_nd_arrival_time_option(String lo_nd_arrival_time_option) {
		this.lo_nd_arrival_time_option = lo_nd_arrival_time_option;
	}
	public String getLo_nd_arrival_win_and_hour() {
		return lo_nd_arrival_win_and_hour;
	}
	public void setLo_nd_arrival_win_and_hour(String lo_nd_arrival_win_and_hour) {
		this.lo_nd_arrival_win_and_hour = lo_nd_arrival_win_and_hour;
	}
	public String getLo_nd_arrival_win_and_minute() {
		return lo_nd_arrival_win_and_minute;
	}
	public void setLo_nd_arrival_win_and_minute(String lo_nd_arrival_win_and_minute) {
		this.lo_nd_arrival_win_and_minute = lo_nd_arrival_win_and_minute;
	}
	public String getLo_nd_arrival_win_and_option() {
		return lo_nd_arrival_win_and_option;
	}
	public void setLo_nd_arrival_win_and_option(String lo_nd_arrival_win_and_option) {
		this.lo_nd_arrival_win_and_option = lo_nd_arrival_win_and_option;
	}
	public String getLo_nd_arrival_win_bw_hour() {
		return lo_nd_arrival_win_bw_hour;
	}
	public void setLo_nd_arrival_win_bw_hour(String lo_nd_arrival_win_bw_hour) {
		this.lo_nd_arrival_win_bw_hour = lo_nd_arrival_win_bw_hour;
	}
	public String getLo_nd_arrival_win_bw_minute() {
		return lo_nd_arrival_win_bw_minute;
	}
	public void setLo_nd_arrival_win_bw_minute(String lo_nd_arrival_win_bw_minute) {
		this.lo_nd_arrival_win_bw_minute = lo_nd_arrival_win_bw_minute;
	}
	public String getLo_nd_arrival_win_bw_option() {
		return lo_nd_arrival_win_bw_option;
	}
	public void setLo_nd_arrival_win_bw_option(String lo_nd_arrival_win_bw_option) {
		this.lo_nd_arrival_win_bw_option = lo_nd_arrival_win_bw_option;
	}
	
	public String getLo_aegisticket_number() {
		return lo_aegisticket_number;
	}
	public void setLo_aegisticket_number(String lo_aegisticket_number) {
		this.lo_aegisticket_number = lo_aegisticket_number;
	}
	public String getLo_cust_name() {
		return lo_cust_name;
	}
	public void setLo_cust_name(String lo_cust_name) {
		this.lo_cust_name = lo_cust_name;
	}
	public String getLo_custticket_number() {
		return lo_custticket_number;
	}
	public void setLo_custticket_number(String lo_custticket_number) {
		this.lo_custticket_number = lo_custticket_number;
	}

	public String getLo_po_number() {
		return lo_po_number;
	}
	public void setLo_po_number(String lo_po_number) {
		this.lo_po_number = lo_po_number;
	}

	public String getLo_under_warranty() {
		return lo_under_warranty;
	}
	public void setLo_under_warranty(String lo_under_warranty) {
		this.lo_under_warranty = lo_under_warranty;
	}
	public FormFile getFilename2() {
		return filename2;
	}
	public void setFilename2(FormFile filename2) {
		this.filename2 = filename2;
	}
	public FormFile getFilename3() {
		return filename3;
	}
	public void setFilename3(FormFile filename3) {
		this.filename3 = filename3;
	}
	public String getLo_Standby() {
		return lo_Standby;
	}
	public void setLo_Standby(String lo_Standby) {
		this.lo_Standby = lo_Standby;
	}
	public String getLo_om_id() {
		return lo_om_id;
	}
	public void setLo_om_id(String lo_om_id) {
		this.lo_om_id = lo_om_id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLo_site_location() {
		return lo_site_location;
	}
	public void setLo_site_location(String lo_site_location) {
		this.lo_site_location = lo_site_location;
	}
			
}
