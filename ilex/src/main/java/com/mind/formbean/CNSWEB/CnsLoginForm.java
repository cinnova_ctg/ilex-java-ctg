package com.mind.formbean.CNSWEB;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CnsLoginForm extends ActionForm {

    private String username = null;
    private String password = null;
    private String confirmpassword = null;
    private String email = null;
    private String signup = null;
    private String failmessage = null;
    private String passwordsmismatchmessage = null;
    private String responsepage = null;

    private String token = null;
    private String userid = null;

    public String getConfirmpassword() {
        return confirmpassword;
    }

    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSignup() {
        return signup;
    }

    public void setSignup(String signup) {
        this.signup = signup;
    }

    public String getFailmessage() {
        return failmessage;
    }

    public void setFailmessage(String failmessage) {
        this.failmessage = failmessage;
    }

    public String getPasswordsmismatchmessage() {
        return passwordsmismatchmessage;
    }

    public void setPasswordsmismatchmessage(String passwordsmismatchmessage) {
        this.passwordsmismatchmessage = passwordsmismatchmessage;
    }

    public String getResponsepage() {
        return responsepage;
    }

    public void setResponsepage(String responsepage) {
        this.responsepage = responsepage;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        username = null;
        password = null;
        confirmpassword = null;
        email = null;
        signup = null;
        failmessage = null;
        passwordsmismatchmessage = null;
        responsepage = null;
    }

}
