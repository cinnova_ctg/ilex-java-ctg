package com.mind.formbean.MasterSite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.struts.action.ActionForm;

import com.mind.bean.MasterSite.MasterSiteDeviceAttributeBean;

public class MasterSiteForm extends ActionForm {

	private String siteId;
	private String mmId;

	// Site Detsils
	private String siteName;
	private String siteNameLabel;
	private String siteAddress;
	private String phoneNumber;
	private String primaryContactName;
	private String primaryContactEmail;

	// to search site by address.
	private String siteCity;
	private String siteState;
	private String siteZip;
	private String siteAdress;
	private String searchType;
	// to search site by address.
	private String authenticate = "";

	private ArrayList helpDeskDetailsList = new ArrayList();
	private ArrayList jobDetailsList = new ArrayList();
	private ArrayList deliverableDetailsList = new ArrayList();

	private ArrayList<String[]> sitesList = new ArrayList<String[]>();

	private ArrayList assetsDeployedList = new ArrayList();

	private List<MasterSiteDeviceAttributeBean> masterSiteDeviceAttributelst = new ArrayList<MasterSiteDeviceAttributeBean>();

	private ArrayList<String[]> kIPDtlList = new ArrayList<String[]>();

	private Collection siteMasterClientList = null;
	private Collection siteMasterSitesList = null;

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getMmId() {
		return mmId;
	}

	public void setMmId(String mmId) {
		this.mmId = mmId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPrimaryContactName() {
		return primaryContactName;
	}

	public void setPrimaryContactName(String primaryContactName) {
		this.primaryContactName = primaryContactName;
	}

	public ArrayList getHelpDeskDetailsList() {
		return helpDeskDetailsList;
	}

	public void setHelpDeskDetailsList(ArrayList helpDeskDetailsList) {
		this.helpDeskDetailsList = helpDeskDetailsList;
	}

	public ArrayList getJobDetailsList() {
		return jobDetailsList;
	}

	public void setJobDetailsList(ArrayList jobDetailsList) {
		this.jobDetailsList = jobDetailsList;
	}

	public ArrayList getDeliverableDetailsList() {
		return deliverableDetailsList;
	}

	public void setDeliverableDetailsList(ArrayList deliverableDetailsList) {
		this.deliverableDetailsList = deliverableDetailsList;
	}

	public ArrayList getAssetsDeployedList() {
		return assetsDeployedList;
	}

	public void setAssetsDeployedList(ArrayList assetsDeployedList) {
		this.assetsDeployedList = assetsDeployedList;
	}

	public ArrayList<String[]> getkIPDtlList() {
		return kIPDtlList;
	}

	public void setkIPDtlList(ArrayList<String[]> kIPDtlList) {
		this.kIPDtlList = kIPDtlList;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getPrimaryContactEmail() {
		return primaryContactEmail;
	}

	public void setPrimaryContactEmail(String primaryContactEmail) {
		this.primaryContactEmail = primaryContactEmail;
	}

	public Collection getSiteMasterClientList() {
		return siteMasterClientList;
	}

	public void setSiteMasterClientList(Collection siteMasterClientList) {
		this.siteMasterClientList = siteMasterClientList;
	}

	public Collection getSiteMasterSitesList() {
		return siteMasterSitesList;
	}

	public void setSiteMasterSitesList(Collection siteMasterSitesList) {
		this.siteMasterSitesList = siteMasterSitesList;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteZip() {
		return siteZip;
	}

	public void setSiteZip(String siteZip) {
		this.siteZip = siteZip;
	}

	public String getSiteAdress() {
		return siteAdress;
	}

	public void setSiteAdress(String siteAdress) {
		this.siteAdress = siteAdress;
	}

	public ArrayList<String[]> getSitesList() {
		return sitesList;
	}

	public void setSitesList(ArrayList<String[]> sitesList) {
		this.sitesList = sitesList;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSiteNameLabel() {
		return siteNameLabel;
	}

	public void setSiteNameLabel(String siteNameLabel) {
		this.siteNameLabel = siteNameLabel;
	}

	public List<MasterSiteDeviceAttributeBean> getMasterSiteDeviceAttributelst() {
		return masterSiteDeviceAttributelst;
	}

	public void setMasterSiteDeviceAttributelst(
			List<MasterSiteDeviceAttributeBean> masterSiteDeviceAttributelst) {
		this.masterSiteDeviceAttributelst = masterSiteDeviceAttributelst;
	}
}
