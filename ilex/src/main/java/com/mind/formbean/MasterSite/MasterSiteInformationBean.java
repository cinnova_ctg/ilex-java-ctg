package com.mind.formbean.MasterSite;

import java.util.Date;

public class MasterSiteInformationBean {

	private String jobId;
	private String ticketName;// jobName
	private Date createdDate;
	private String createdDateString;
	private String installNote;
	private String deliverableDesc;
	private int installNoteCount;
	private int deliverableCount;
	private String poWoId;
	private String deliverableId;
	private String appendixId;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getInstallNote() {
		return installNote;
	}

	public void setInstallNote(String installNote) {
		this.installNote = installNote;
	}

	public String getDeliverableDesc() {
		return deliverableDesc;
	}

	public void setDeliverableDesc(String deliverableDesc) {
		this.deliverableDesc = deliverableDesc;
	}

	public int getInstallNoteCount() {
		return installNoteCount;
	}

	public void setInstallNoteCount(int installNoteCount) {
		this.installNoteCount = installNoteCount;
	}

	public int getDeliverableCount() {
		return deliverableCount;
	}

	public void setDeliverableCount(int deliverableCount) {
		this.deliverableCount = deliverableCount;
	}

	public String getPoWoId() {
		return poWoId;
	}

	public void setPoWoId(String poWoId) {
		this.poWoId = poWoId;
	}

	public String getDeliverableId() {
		return deliverableId;
	}

	public void setDeliverableId(String deliverableId) {
		this.deliverableId = deliverableId;
	}

	public String getCreatedDateString() {
		return createdDateString;
	}

	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

}
