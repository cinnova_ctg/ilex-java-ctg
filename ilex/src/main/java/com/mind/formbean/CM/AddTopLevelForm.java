package com.mind.formbean.CM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application. Users may access 3 fields on this form:
 * <ul>
 * <li>custname - [your comment here]
 * <li>reset - [your comment here]
 * <li>save - [your comment here]
 * </ul>
 * 
 * @version 1.0
 * @author
 */
public class AddTopLevelForm extends ActionForm {

	private String textboxdummy = null;
	private String refresh = null;
	private String orgcatg = null;
	private String custname = null;
	private String save = null;
	private String reset = null;
	private String function = null;
	private String element = null;
	private String addmessage = null;
	private String deletemessage = null;
	private String updatemessage = null;
	private String authenticate = "";
	private String type = null;
	private String division = null;
	private String poc = null;
	private String search = null;
	private String source = null;
	private String workCenterId;

	public String getRefresh() {
		return refresh;
	}

	public void setRefresh(String refresh) {
		this.refresh = refresh;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getOrgcatg() {
		return orgcatg;
	}

	public void setOrgcatg(String orgcatg) {
		this.orgcatg = orgcatg;
	}

	public String getCustname() {
		return custname;
	}

	public void setCustname(String c) {
		this.custname = c;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String s) {
		this.save = s;
	}

	public String getReset() {
		return reset;
	}

	public void setReset(String r) {
		this.reset = r;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getAddmessage() {
		return addmessage;
	}

	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}

	public String getDeletemessage() {
		return deletemessage;
	}

	public void setDeletemessage(String message) {
		this.deletemessage = message;
	}

	public String getUpdatemessage() {
		return updatemessage;
	}

	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		textboxdummy = null;
		refresh = null;
		orgcatg = null;
		custname = null;
		save = null;
		reset = null;
		function = null;
		element = null;
		addmessage = null;
		deletemessage = null;
		updatemessage = null;
		type = null;
		division = null;
		poc = null;
		search = null;
	}

	public String getTextboxdummy() {
		return textboxdummy;
	}

	public void setTextboxdummy(String textboxdummy) {
		this.textboxdummy = textboxdummy;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPoc() {
		return poc;
	}

	public void setPoc(String poc) {
		this.poc = poc;
	}

	public String getWorkCenterId() {
		return workCenterId;
	}

	public void setWorkCenterId(String workCenterId) {
		this.workCenterId = workCenterId;
	}

}
