package com.mind.formbean.CM;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 9 fields on this form:
 * <ul>
 * <li>state - [your comment here]
 * <li>secondaddressline - [your comment here]
 * <li>zip - [your comment here]
 * <li>country - [your comment here]
 * <li>divison - [your comment here]
 * <li>city - [your comment here]
 * <li>reset - [your comment here]
 * <li>save - [your comment here]
 * <li>firstaddressline - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class AddDivisonForm extends ActionForm {

	private String state = null;
	private String stateSelect = null; // - for state combobox
	private String secondaddressline = null;
	private String zip = null;
	private String country = null;
	private String divison = null;
	private String city = null;
	private String reset = null;
	private String save = null;
	private String firstaddressline = null;
	
	private String element=null;
	private String custname=null;
	private String addmessage= null;
	private String deletemessage=null;
	private String updatemessage=null;
	private String function=null;
	private String refresh=null;
	private String org_discipline_id=null;
	private String org_type=null;
	private String divison_name=null;
	private String authenticate="";	
	private String type=null;
	private String division=null;
	private String poc=null;
	private String search=null;	
	private String website=null;	
	private String conMinuteman = null;
	private String lat_deg = null;
	private String lat_min = null;
	private String lat_direction = null;
	private String lon_deg = null;
	private String lon_min = null;
	private String lon_direction = null;
	private String region_id = null;
	private String category_id = null;
	private String siteCountryId = null;
	private String siteCountryIdName = null;
	private String countrychange = null;
	private String timezone = null;
	private String timeZoneName = null;
	private Collection tempList = null;
	private String latlonFound = null;
	private String[] cmboxCoreCompetency = null;
	private String accept_address = null;
	private String latitude = null;
	private String longitude = null;
	private String latLongAccuracy = null;
	
	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLatLongAccuracy() {
		return latLongAccuracy;
	}

	public void setLatLongAccuracy(String latLongAccuracy) {
		this.latLongAccuracy = latLongAccuracy;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAccept_address() {
		return accept_address;
	}

	public void setAccept_address(String accept_address) {
		this.accept_address = accept_address;
	}

	public String[] getCmboxCoreCompetency() {
		return cmboxCoreCompetency;
	}

	public void setCmboxCoreCompetency(String[] cmboxCoreCompetency) {
		this.cmboxCoreCompetency = cmboxCoreCompetency;
	}
	
	public String getCmboxCoreCompetency(int i) {
		return cmboxCoreCompetency[i];
	}

		
	public String getLatlonFound() {
		return latlonFound;
	}

	public void setLatlonFound(String latlonFound) {
		this.latlonFound = latlonFound;
	}

	/**
	 * Get state
	 * @return String[]
	 */
	public String getState() {
		return state;
	}

	/**
	 * Set state
	 * @param <code>String[]</code>
	 */
	public void setState(String s) {
		this.state = s;
	}

	/**
	 * Get secondaddressline
	 * @return String
	 */
	public String getSecondaddressline() {
		return secondaddressline;
	}

	/**
	 * Set secondaddressline
	 * @param <code>String</code>
	 */
	public void setSecondaddressline(String s) {
		this.secondaddressline = s;
	}

	/**
	 * Get zip
	 * @return String
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * Set zip
	 * @param <code>String</code>
	 */
	public void setZip(String z) {
		this.zip = z;
	}

	/**
	 * Get country
	 * @return String
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Set country
	 * @param <code>String</code>
	 */
	public void setCountry(String c) {
		this.country = c;
	}

	/**
	 * Get divison
	 * @return String
	 */
	public String getDivison() {
		return divison;
	}

	/**
	 * Set divison
	 * @param <code>String</code>
	 */
	public void setDivison(String d) {
		this.divison = d;
	}

	/**
	 * Get city
	 * @return String
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Set city
	 * @param <code>String</code>
	 */
	public void setCity(String c) {
		this.city = c;
	}

	/**
	 * Get reset
	 * @return String
	 */
	public String getReset() {
		return reset;
	}

	/**
	 * Set reset
	 * @param <code>String</code>
	 */
	public void setReset(String r) {
		this.reset = r;
	}

	/**
	 * Get save
	 * @return String
	 */
	public String getSave() {
		return save;
	}

	/**
	 * Set save
	 * @param <code>String</code>
	 */
	public void setSave(String s) {
		this.save = s;
	}

	/**
	 * Get firstaddressline
	 * @return String
	 */
	public String getFirstaddressline() {
		return firstaddressline;
	}

	/**
	 * Set firstaddressline
	 * @param <code>String</code>
	 */
	public void setFirstaddressline(String f) {
		this.firstaddressline = f;
	}
	
	public String getElement() {
		return element;
	}
	public void setElement(String element) {
		this.element = element;
	}

	public String getCustname(){
		return custname;
	}
	public void setCustname(String custname) {
		this.custname = custname;
	}

	public String getAddmessage() {
		return addmessage;
	}

	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}

	public String getDeletemessage() {
		return deletemessage;
	}

	public void setDeletemessage(String deletemessage) {
		this.deletemessage = deletemessage;
	}

	public String getUpdatemessage() {
		return updatemessage;
	}

	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}
	
	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getRefresh() {
		return refresh;
	}

	public void setRefresh(String refresh) {
		this.refresh = refresh;
	}
	

	public String getOrg_discipline_id() {
		return org_discipline_id;
	}

	public void setOrg_discipline_id(String org_discipline_id) {
		this.org_discipline_id = org_discipline_id;
	}
	
	public String getOrg_type() {
		return org_type;
	}

	public void setOrg_type(String org_type) {
		this.org_type = org_type;
	}

	public String getDivison_name() {
		return divison_name;
	}

	public void setDivison_name(String divison_name) {
		this.divison_name = divison_name;
	}
	
	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// Reset values are provided as samples only. Change as appropriate.
		state = null;
		secondaddressline = null;
		zip = null;
		country = null;
		divison = null;
		city = null;
		reset = null;
		save = null;
		firstaddressline = null;
		element=null;
		custname=null;
		addmessage= null;
		deletemessage=null;
		updatemessage=null;
		function=null;
		refresh=null;
		org_discipline_id=null;
		org_type=null;
		divison_name=null;
		
		type=null;
		division=null;
		poc=null;
		search=null;
		
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.

		// if ((field == null) || (field.length() == 0)) {
		//   errors.add("field", new org.apache.struts.action.ActionError("error.field.required"));
		// }
		return errors;

	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPoc() {
		return poc;
	}

	public void setPoc(String poc) {
		this.poc = poc;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getConMinuteman() {
		return conMinuteman;
	}

	public void setConMinuteman(String conMinuteman) {
		this.conMinuteman = conMinuteman;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getLat_deg() {
		return lat_deg;
	}

	public void setLat_deg(String lat_deg) {
		this.lat_deg = lat_deg;
	}

	public String getLat_direction() {
		return lat_direction;
	}

	public void setLat_direction(String lat_direction) {
		this.lat_direction = lat_direction;
	}

	public String getLat_min() {
		return lat_min;
	}

	public void setLat_min(String lat_min) {
		this.lat_min = lat_min;
	}

	public String getLon_deg() {
		return lon_deg;
	}

	public void setLon_deg(String lon_deg) {
		this.lon_deg = lon_deg;
	}

	public String getLon_direction() {
		return lon_direction;
	}

	public void setLon_direction(String lon_direction) {
		this.lon_direction = lon_direction;
	}

	public String getLon_min() {
		return lon_min;
	}

	public void setLon_min(String lon_min) {
		this.lon_min = lon_min;
	}

	public String getRegion_id() {
		return region_id;
	}

	public void setRegion_id(String region_id) {
		this.region_id = region_id;
	}

	public String getSiteCountryId() {
		return siteCountryId;
	}

	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}
	public Collection getTempList() {
		return tempList;
	}

	public void setTempList(Collection tempList) {
		this.tempList = tempList;
	}

	public String getCountrychange() {
		return countrychange;
	}

	public void setCountrychange(String countrychange) {
		this.countrychange = countrychange;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getTimeZoneName() {
		return timeZoneName;
	}

	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

	public String getSiteCountryIdName() {
		return siteCountryIdName;
	}

	public void setSiteCountryIdName(String siteCountryIdName) {
		this.siteCountryIdName = siteCountryIdName;
	}

	public String getStateSelect() {
		return stateSelect;
	}

	public void setStateSelect(String stateSelect) {
		this.stateSelect = stateSelect;
	}

}
