package com.mind.formbean.IM;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.im.InvoicedJobDetails;

public class InvoiceJobCommissionPOForm extends ActionForm {
	private String invoiceNo = null;
	private String paymentReceiveDate = null;
	private String totalRevenue = null;
	private String go = null;
	private String completePOs = null;
	private ArrayList<InvoicedJobDetails> invoicedJobs = null;
	private String[] jobId = null;
	private String actualInvoiceNo = null;
	
	public String getActualInvoiceNo() {
		return actualInvoiceNo;
	}
	public void setActualInvoiceNo(String actualInvoiceNo) {
		this.actualInvoiceNo = actualInvoiceNo;
	}
	public String[] getJobId() {
		return jobId;
	}
	public void setJobId(String[] jobId) {
		this.jobId = jobId;
	}
	public ArrayList<InvoicedJobDetails> getInvoicedJobs() {
		return invoicedJobs;
	}
	public void setInvoicedJobs(ArrayList<InvoicedJobDetails> invoicedJobs) {
		this.invoicedJobs = invoicedJobs;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getPaymentReceiveDate() {
		return paymentReceiveDate;
	}
	public void setPaymentReceiveDate(String paymentReceiveDate) {
		this.paymentReceiveDate = paymentReceiveDate;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getTotalRevenue() {
		return totalRevenue;
	}
	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}
	public String getCompletePOs() {
		return completePOs;
	}
	public void setCompletePOs(String completePOs) {
		this.completePOs = completePOs;
	}
	
	
}
