package com.mind.formbean.PRM;

import org.apache.struts.action.ActionForm;

public class ChecklistTabForm extends ActionForm {

	private String save = null;
	private String reset = null;

	private String savelist = null;

	private String[] activityid = null;
	private String[] activityname = null;
	private String[] check = null;
	private String appendix_id = null;
	private String job_id = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String msaname = null;
	private String appendixname = null;
	private String jobname = null;
	private String page = null;

	private String[] groupname = null;
	private String[] itemname = null;
	private String[] option = null;
	private String[] optionid = null;
	private String[] optioncheckbox = null;

	private String selectedoption = null;
	private String quessize = "0";
	private String[] optionsperques = null;
	private String partnerid = null;
	private String partnername = null;
	private String no_of_partners = "0";
	private String back = "0";

	private String firstvisitsuccess = null;

	private String firstvisitpartnersuccess = null;

	private String firstvisitpartner = null;
	private String valuesuccess = null;
	private String siteJobCompleted = null;
	private String siteJobProfessional = null;
	private String jobSiteFollowUp = null;
	private String workCompleted = null;
	private String techName = null;
	private String letterOfApology = null;
	private String restrictTech = null;
	private String restrictCertPartner = null;
	private String otherRecomendations = null;
	private String checkMsp = null;
	private String poId = null;

	private String type = null;
	private String status = null;

	private String pocTech;
	private String pocNumber;
	private String pocComments;

	// The below two filed add for recommend this tech for future jobs?
	private String recommadTechFutureJobs;
	private String userRatingFlag;

	private String operationRatingComments;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String[] getActivityid() {
		return activityid;
	}

	public String getActivityid(int i) {
		return activityid[i];
	}

	public void setActivityid(String[] activityid) {
		this.activityid = activityid;
	}

	public String[] getActivityname() {
		return activityname;
	}

	public String getActivityname(int i) {
		return activityname[i];
	}

	public void setActivityname(String[] activityname) {
		this.activityname = activityname;
	}

	public String[] getCheck() {
		return check;
	}

	public String getCheck(int i) {
		return check[i];
	}

	public void setCheck(String[] check) {
		this.check = check;
	}

	public String[] getGroupname() {
		return groupname;
	}

	public String getGroupname(int i) {
		return groupname[i];
	}

	public void setGroupname(String[] groupname) {
		this.groupname = groupname;
	}

	public String[] getItemname() {
		return itemname;
	}

	public String getItemname(int i) {
		return itemname[i];
	}

	public void setItemname(String[] itemname) {
		this.itemname = itemname;
	}

	public String[] getOption() {
		return option;
	}

	public String getOption(int i) {
		return option[i];
	}

	public void setOption(String[] option) {
		this.option = option;
	}

	public String getReset() {
		return reset;
	}

	public void setReset(String reset) {
		this.reset = reset;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getAppendix_id() {
		return appendix_id;
	}

	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String[] getOptionid() {
		return optionid;
	}

	public String getOptionid(int i) {
		return optionid[i];
	}

	public void setOptionid(String[] optionid) {
		this.optionid = optionid;
	}

	public String[] getOptioncheckbox() {
		return optioncheckbox;
	}

	public String getOptioncheckbox(int i) {
		return optioncheckbox[i];
	}

	public void setOptioncheckbox(String[] optioncheckbox) {
		this.optioncheckbox = optioncheckbox;
	}

	public String getSelectedoption() {
		return selectedoption;
	}

	public void setSelectedoption(String selectedoption) {
		this.selectedoption = selectedoption;
	}

	public String getQuessize() {
		return quessize;
	}

	public void setQuessize(String quessize) {
		this.quessize = quessize;
	}

	public String[] getOptionsperques() {
		return optionsperques;
	}

	public void setOptionsperques(String[] optionsperques) {
		this.optionsperques = optionsperques;
	}

	public String getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}

	public String getPartnername() {
		return partnername;
	}

	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}

	public String getNo_of_partners() {
		return no_of_partners;
	}

	public void setNo_of_partners(String no_of_partners) {
		this.no_of_partners = no_of_partners;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public String getFirstvisitsuccess() {
		return firstvisitsuccess;
	}

	public void setFirstvisitsuccess(String firstvisitsuccess) {
		this.firstvisitsuccess = firstvisitsuccess;
	}

	public String getFirstvisitpartnersuccess() {
		return firstvisitpartnersuccess;
	}

	public void setFirstvisitpartnersuccess(String firstvisitpartnersuccess) {
		this.firstvisitpartnersuccess = firstvisitpartnersuccess;
	}

	public String getFirstvisitpartner() {
		return firstvisitpartner;
	}

	public void setFirstvisitpartner(String firstvisitpartner) {
		this.firstvisitpartner = firstvisitpartner;
	}

	public String getValuesuccess() {
		return valuesuccess;
	}

	public void setValuesuccess(String valuesuccess) {
		this.valuesuccess = valuesuccess;
	}

	public String getSavelist() {
		return savelist;
	}

	public void setSavelist(String savelist) {
		this.savelist = savelist;
	}

	public String getSiteJobProfessional() {
		return siteJobProfessional;
	}

	public void setSiteJobProfessional(String siteJobProfessional) {
		this.siteJobProfessional = siteJobProfessional;
	}

	public String getSiteJobCompleted() {
		return siteJobCompleted;
	}

	public void setSiteJobCompleted(String siteJobCompleted) {
		this.siteJobCompleted = siteJobCompleted;
	}

	public String getJobSiteFollowUp() {
		return jobSiteFollowUp;
	}

	public void setJobSiteFollowUp(String jobSiteFollowUp) {
		this.jobSiteFollowUp = jobSiteFollowUp;
	}

	public String getTechName() {
		return techName;
	}

	public void setTechName(String techName) {
		this.techName = techName;
	}

	public String getWorkCompleted() {
		return workCompleted;
	}

	public void setWorkCompleted(String workCompleted) {
		this.workCompleted = workCompleted;
	}

	public String getLetterOfApology() {
		return letterOfApology;
	}

	public void setLetterOfApology(String letterOfApology) {
		this.letterOfApology = letterOfApology;
	}

	public String getOtherRecomendations() {
		return otherRecomendations;
	}

	public void setOtherRecomendations(String otherRecomendations) {
		this.otherRecomendations = otherRecomendations;
	}

	public String getRestrictCertPartner() {
		return restrictCertPartner;
	}

	public void setRestrictCertPartner(String restrictCertPartner) {
		this.restrictCertPartner = restrictCertPartner;
	}

	public String getRestrictTech() {
		return restrictTech;
	}

	public void setRestrictTech(String restrictTech) {
		this.restrictTech = restrictTech;
	}

	public String getCheckMsp() {
		return checkMsp;
	}

	public void setCheckMsp(String checkMsp) {
		this.checkMsp = checkMsp;
	}

	public String getPoId() {
		return poId;
	}

	public void setPoId(String poId) {
		this.poId = poId;
	}

	public String getPocTech() {
		return pocTech;
	}

	public void setPocTech(String pocTech) {
		this.pocTech = pocTech;
	}

	public String getPocNumber() {
		return pocNumber;
	}

	public void setPocNumber(String pocNumber) {
		this.pocNumber = pocNumber;
	}

	public String getRecommadTechFutureJobs() {
		return recommadTechFutureJobs;
	}

	public void setRecommadTechFutureJobs(String recommadTechFutureJobs) {
		this.recommadTechFutureJobs = recommadTechFutureJobs;
	}

	public String getPocComments() {
		return pocComments;
	}

	public void setPocComments(String pocComments) {
		this.pocComments = pocComments;
	}

	public String getUserRatingFlag() {
		return userRatingFlag;
	}

	public void setUserRatingFlag(String userRatingFlag) {
		this.userRatingFlag = userRatingFlag;
	}

	public String getOperationRatingComments() {
		return operationRatingComments;
	}

	public void setOperationRatingComments(String operationRatingComments) {
		this.operationRatingComments = operationRatingComments;
	}

}
