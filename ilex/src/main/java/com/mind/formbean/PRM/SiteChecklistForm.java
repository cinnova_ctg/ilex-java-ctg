package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class SiteChecklistForm extends ActionForm
{
	private String siteid = null;
	//private String checklistitemid = null;
	private String sitename = null;
	private String[] sitecheckbox = null;
	//private String checklistname = null;
	private String nettype = null;
	//private String itemdescription = null;
	private String save= null;
	private String reset = null;
	private String function = null;
	private String typeid = null;
	private String type = null;
	private String[] index = null;
	private String authenticate = "";
	
	private String page = null;
	private String pageid = null;
	private String fromtype = null;
	
	private String viewjobtype = null;
	private String ownerId = null;
	
	
	
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String[] getSitecheckbox() {
		return sitecheckbox;
	}
	public void setSitecheckbox(String[] sitecheckbox) {
		this.sitecheckbox = sitecheckbox;
	}
	public String getSitecheckbox(int i) {
		return sitecheckbox[i];
	}
	public String getSiteid() {
		return siteid;
	}
	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
	
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		siteid = null;
		sitename = null;
		sitecheckbox = null;
		save= null;
		reset = null;
		function = null;
		typeid = null;
		type = null;
		fromtype = null;
		
		
	}
	public String getPageid() {
		return pageid;
	}
	public void setPageid(String pageid) {
		this.pageid = pageid;
	}
	public String getFromtype() {
		return fromtype;
	}
	public void setFromtype(String fromtype) {
		this.fromtype = fromtype;
	}
	public String getNettype() {
		return nettype;
	}
	public void setNettype(String nettype) {
		this.nettype = nettype;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	
	
}
