package com.mind.formbean.PRM;

import org.apache.struts.action.ActionForm;

public class FirstJobSuccessForm extends ActionForm{
	
	private String jobid = null;
	private String firstJobSuccess = null;
	private String submit = null;
	private String viewjobtype = null;
	private String type = null;
	

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getFirstJobSuccess() {
		return firstJobSuccess;
	}

	public void setFirstJobSuccess(String firstJobSuccess) {
		this.firstJobSuccess = firstJobSuccess;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
