package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CustomerInformationForm extends ActionForm{

	private String custParameter1 = null;
	private String custvalue1 = null;
	private String custParameter2 = null;
	private String custvalue2 = null;
	private String custParameter3 = null;
	private String custvalue3 = null;
	private String appendixid = null;
	private String appendixname = null;
	private String save = null;
	private String reset = null;
	private String function = null;
	private String authenticate = "";
	private String typeid = null;
	private String type = null;
	private String back = null;
	private String appendixcustinfoid1 = null;
	private String appendixcustinfoid2 = null;
	private String appendixcustinfoid3= null;
	//start
	private String appendixcustinfoid4= null;
	private String appendixcustinfoid5= null;
	private String appendixcustinfoid6= null;
	private String appendixcustinfoid7= null;
	private String appendixcustinfoid8= null;
	private String appendixcustinfoid9= null;
	private String appendixcustinfoid10= null;
	private String custParameter4 = null;
	private String custvalue4 = null;
	private String custParameter5 = null;
	private String custvalue5 = null;
	private String custParameter6 = null;
	private String custvalue6 = null;
	private String custParameter7 = null;
	private String custvalue7 = null;
	private String custParameter8 = null;
	private String custvalue8 = null;
	private String custParameter9 = null;
	private String custvalue9 = null;
	private String custParameter10 = null;
	private String custvalue10 = null;
	
	private String snCustvalue1 = null;
	private String snCustvalue2 = null;
	private String snCustvalue3 = null;
	private String snCustvalue4 = null;
	private String snCustvalue5 = null;
	private String snCustvalue6 = null;
	private String snCustvalue7 = null;
	private String snCustvalue8 = null;
	private String snCustvalue9 = null;
	private String snCustvalue10 = null;
	
	
	//over
	
	
	private String from = null;
	private String jobname = null;
	private String nettype = null;
	private String viewjobtype = null;
	private String ownerId = null;
	
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getCustParameter1() {
		return custParameter1;
	}
	public void setCustParameter1(String custParameter1) {
		this.custParameter1 = custParameter1;
	}
	public String getCustParameter2() {
		return custParameter2;
	}
	public void setCustParameter2(String custParameter2) {
		this.custParameter2 = custParameter2;
	}
	public String getCustParameter3() {
		return custParameter3;
	}
	public void setCustParameter3(String custParameter3) {
		this.custParameter3 = custParameter3;
	}
	public String getCustvalue1() {
		return custvalue1;
	}
	public void setCustvalue1(String custvalue1) {
		this.custvalue1 = custvalue1;
	}
	public String getCustvalue2() {
		return custvalue2;
	}
	public void setCustvalue2(String custvalue2) {
		this.custvalue2 = custvalue2;
	}
	public String getCustvalue3() {
		return custvalue3;
	}
	public void setCustvalue3(String custvalue3) {
		this.custvalue3 = custvalue3;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		
		custParameter1 = null;
		custvalue1 = null;
		custParameter2 = null;
		custvalue2 = null;
		custParameter3 = null;
		custvalue3 = null;
		appendixid = null;
		appendixname = null;
		save = null;
		function = null;
		back = null;
		
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getAppendixcustinfoid1() {
		return appendixcustinfoid1;
	}
	public void setAppendixcustinfoid1(String appendixcustinfoid1) {
		this.appendixcustinfoid1 = appendixcustinfoid1;
	}
	public String getAppendixcustinfoid2() {
		return appendixcustinfoid2;
	}
	public void setAppendixcustinfoid2(String appendixcustinfoid2) {
		this.appendixcustinfoid2 = appendixcustinfoid2;
	}
	public String getAppendixcustinfoid3() {
		return appendixcustinfoid3;
	}
	public void setAppendixcustinfoid3(String appendixcustinfoid3) {
		this.appendixcustinfoid3 = appendixcustinfoid3;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getNettype() {
		return nettype;
	}
	public void setNettype(String nettype) {
		this.nettype = nettype;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getAppendixcustinfoid10() {
		return appendixcustinfoid10;
	}
	public void setAppendixcustinfoid10(String appendixcustinfoid10) {
		this.appendixcustinfoid10 = appendixcustinfoid10;
	}
	public String getAppendixcustinfoid4() {
		return appendixcustinfoid4;
	}
	public void setAppendixcustinfoid4(String appendixcustinfoid4) {
		this.appendixcustinfoid4 = appendixcustinfoid4;
	}
	public String getAppendixcustinfoid5() {
		return appendixcustinfoid5;
	}
	public void setAppendixcustinfoid5(String appendixcustinfoid5) {
		this.appendixcustinfoid5 = appendixcustinfoid5;
	}
	public String getAppendixcustinfoid6() {
		return appendixcustinfoid6;
	}
	public void setAppendixcustinfoid6(String appendixcustinfoid6) {
		this.appendixcustinfoid6 = appendixcustinfoid6;
	}
	public String getAppendixcustinfoid7() {
		return appendixcustinfoid7;
	}
	public void setAppendixcustinfoid7(String appendixcustinfoid7) {
		this.appendixcustinfoid7 = appendixcustinfoid7;
	}
	public String getAppendixcustinfoid8() {
		return appendixcustinfoid8;
	}
	public void setAppendixcustinfoid8(String appendixcustinfoid8) {
		this.appendixcustinfoid8 = appendixcustinfoid8;
	}
	public String getAppendixcustinfoid9() {
		return appendixcustinfoid9;
	}
	public void setAppendixcustinfoid9(String appendixcustinfoid9) {
		this.appendixcustinfoid9 = appendixcustinfoid9;
	}
	public String getCustParameter10() {
		return custParameter10;
	}
	public void setCustParameter10(String custParameter10) {
		this.custParameter10 = custParameter10;
	}
	public String getCustParameter4() {
		return custParameter4;
	}
	public void setCustParameter4(String custParameter4) {
		this.custParameter4 = custParameter4;
	}
	public String getCustParameter5() {
		return custParameter5;
	}
	public void setCustParameter5(String custParameter5) {
		this.custParameter5 = custParameter5;
	}
	public String getCustParameter6() {
		return custParameter6;
	}
	public void setCustParameter6(String custParameter6) {
		this.custParameter6 = custParameter6;
	}
	public String getCustParameter7() {
		return custParameter7;
	}
	public void setCustParameter7(String custParameter7) {
		this.custParameter7 = custParameter7;
	}
	public String getCustParameter8() {
		return custParameter8;
	}
	public void setCustParameter8(String custParameter8) {
		this.custParameter8 = custParameter8;
	}
	public String getCustParameter9() {
		return custParameter9;
	}
	public void setCustParameter9(String custParameter9) {
		this.custParameter9 = custParameter9;
	}
	public String getCustvalue10() {
		return custvalue10;
	}
	public void setCustvalue10(String custvalue10) {
		this.custvalue10 = custvalue10;
	}
	public String getCustvalue4() {
		return custvalue4;
	}
	public void setCustvalue4(String custvalue4) {
		this.custvalue4 = custvalue4;
	}
	public String getCustvalue5() {
		return custvalue5;
	}
	public void setCustvalue5(String custvalue5) {
		this.custvalue5 = custvalue5;
	}
	public String getCustvalue6() {
		return custvalue6;
	}
	public void setCustvalue6(String custvalue6) {
		this.custvalue6 = custvalue6;
	}
	public String getCustvalue7() {
		return custvalue7;
	}
	public void setCustvalue7(String custvalue7) {
		this.custvalue7 = custvalue7;
	}
	public String getCustvalue8() {
		return custvalue8;
	}
	public void setCustvalue8(String custvalue8) {
		this.custvalue8 = custvalue8;
	}
	public String getCustvalue9() {
		return custvalue9;
	}
	public void setCustvalue9(String custvalue9) {
		this.custvalue9 = custvalue9;
	}
	public String getSnCustvalue1() {
		return snCustvalue1;
	}
	public void setSnCustvalue1(String snCustvalue1) {
		this.snCustvalue1 = snCustvalue1;
	}
	public String getSnCustvalue10() {
		return snCustvalue10;
	}
	public void setSnCustvalue10(String snCustvalue10) {
		this.snCustvalue10 = snCustvalue10;
	}
	public String getSnCustvalue2() {
		return snCustvalue2;
	}
	public void setSnCustvalue2(String snCustvalue2) {
		this.snCustvalue2 = snCustvalue2;
	}
	public String getSnCustvalue3() {
		return snCustvalue3;
	}
	public void setSnCustvalue3(String snCustvalue3) {
		this.snCustvalue3 = snCustvalue3;
	}
	public String getSnCustvalue4() {
		return snCustvalue4;
	}
	public void setSnCustvalue4(String snCustvalue4) {
		this.snCustvalue4 = snCustvalue4;
	}
	public String getSnCustvalue5() {
		return snCustvalue5;
	}
	public void setSnCustvalue5(String snCustvalue5) {
		this.snCustvalue5 = snCustvalue5;
	}
	public String getSnCustvalue6() {
		return snCustvalue6;
	}
	public void setSnCustvalue6(String snCustvalue6) {
		this.snCustvalue6 = snCustvalue6;
	}
	public String getSnCustvalue7() {
		return snCustvalue7;
	}
	public void setSnCustvalue7(String snCustvalue7) {
		this.snCustvalue7 = snCustvalue7;
	}
	public String getSnCustvalue8() {
		return snCustvalue8;
	}
	public void setSnCustvalue8(String snCustvalue8) {
		this.snCustvalue8 = snCustvalue8;
	}
	public String getSnCustvalue9() {
		return snCustvalue9;
	}
	public void setSnCustvalue9(String snCustvalue9) {
		this.snCustvalue9 = snCustvalue9;
	}
	
	
	
	
	
	
}
