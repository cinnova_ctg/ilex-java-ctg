package com.mind.formbean.PRM;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import com.mind.bean.docm.LabelValue;

public class InsuranceRequestForm extends ActionForm {

	private String jobId = null;
	private String appendixId = null;
	private String msaId = null;
	private String loginUserId = null;
	private String loginUserEmail = null;

	private String contactPerson = null;
	private String todayDate = null;
	private String attention = null;
	private String attentionEmailTo = null;
	private String attentionDirect = null;
	private String attentionFax = null;
	private String attentionAdditionalCC = null;
	private String holderName = null;
	private String holderAddress = null;
	private String holderCity = null;
	private String holderState = null;
	private ArrayList<LabelValue> holderStateList = new ArrayList<LabelValue>();
	private String holderZipCode = null;
	private String retName = null;
	private String retEmail = null;
	private String retDirectNumber = null;
	private String retFax = null;
	private String scheduleStart = null;
	private String scheduleEnd = null;
	private String siteNumber = null;
	private String address = null;
	private String city = null;
	private String state = null;
	private ArrayList<LabelValue> stateList = new ArrayList<LabelValue>();
	private String zipCode = null;
	private String insuranceRequirements = null;
	private FormFile additionalInfo = null;
	private String send = null;
	private String copySiteAddress = null;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public FormFile getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(FormFile additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAttentionAdditionalCC() {
		return attentionAdditionalCC;
	}

	public void setAttentionAdditionalCC(String attentionAdditionalCC) {
		this.attentionAdditionalCC = attentionAdditionalCC;
	}

	public String getAttentionDirect() {
		return attentionDirect;
	}

	public void setAttentionDirect(String attentionDirect) {
		this.attentionDirect = attentionDirect;
	}

	public String getAttentionEmailTo() {
		return attentionEmailTo;
	}

	public void setAttentionEmailTo(String attentionEmailTo) {
		this.attentionEmailTo = attentionEmailTo;
	}

	public String getAttentionFax() {
		return attentionFax;
	}

	public void setAttentionFax(String attentionFax) {
		this.attentionFax = attentionFax;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getHolderAddress() {
		return holderAddress;
	}

	public void setHolderAddress(String holderAddress) {
		this.holderAddress = holderAddress;
	}

	public String getHolderCity() {
		return holderCity;
	}

	public void setHolderCity(String holderCity) {
		this.holderCity = holderCity;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getHolderState() {
		return holderState;
	}

	public void setHolderState(String holderState) {
		this.holderState = holderState;
	}

	public String getHolderZipCode() {
		return holderZipCode;
	}

	public void setHolderZipCode(String holderZipCode) {
		this.holderZipCode = holderZipCode;
	}

	public String getInsuranceRequirements() {
		return insuranceRequirements;
	}

	public void setInsuranceRequirements(String insuranceRequirements) {
		this.insuranceRequirements = insuranceRequirements;
	}

	public String getRetDirectNumber() {
		return retDirectNumber;
	}

	public void setRetDirectNumber(String retDierctName) {
		this.retDirectNumber = retDierctName;
	}

	public String getRetEmail() {
		return retEmail;
	}

	public void setRetEmail(String retEmail) {
		this.retEmail = retEmail;
	}

	public String getRetFax() {
		return retFax;
	}

	public void setRetFax(String retFax) {
		this.retFax = retFax;
	}

	public String getRetName() {
		return retName;
	}

	public void setRetName(String retName) {
		this.retName = retName;
	}

	public String getScheduleEnd() {
		return scheduleEnd;
	}

	public void setScheduleEnd(String scheduleEnd) {
		this.scheduleEnd = scheduleEnd;
	}

	public String getScheduleStart() {
		return scheduleStart;
	}

	public void setScheduleStart(String scheduleStart) {
		this.scheduleStart = scheduleStart;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(String todayDate) {
		this.todayDate = todayDate;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAttention() {
		return attention;
	}

	public void setAttention(String attention) {
		this.attention = attention;
	}

	public ArrayList<LabelValue> getHolderStateList() {
		return holderStateList;
	}

	public void setHolderStateList(ArrayList<LabelValue> holderStateList) {
		this.holderStateList = holderStateList;
	}

	public ArrayList<LabelValue> getStateList() {
		return stateList;
	}

	public void setStateList(ArrayList<LabelValue> stateList) {
		this.stateList = stateList;
	}

	public String getCopySiteAddress() {
		return copySiteAddress;
	}

	public void setCopySiteAddress(String copySiteAddress) {
		this.copySiteAddress = copySiteAddress;
	}

	public String getSend() {
		return send;
	}

	public void setSend(String send) {
		this.send = send;
	}

	public String getLoginUserEmail() {
		return loginUserEmail;
	}

	public void setLoginUserEmail(String loginUserEmail) {
		this.loginUserEmail = loginUserEmail;
	}

	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

}
