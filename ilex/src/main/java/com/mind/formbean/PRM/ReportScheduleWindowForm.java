package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ReportScheduleWindowForm extends ActionForm
{

	private String typeid = null;
	private String startdate = null;
	private String enddate = null;
	private String save = null;
	private String reset = null;
	private String back = null;
	private String appendixname = null;
	private String jobname = null;
	private String report_type = null;
	
	//by hamid
	private String[] criticality = null;
	
	//by hamid
	private String status = null;
	
	//by hamid
	private String requestor = null;
	
	private String critdesc = null;
	private String msp = null;
	private String helpdesk = null;
	
	private String msa = null;
	private String appendix = null;
	
	private String msaname = null;

	private String inworkestcost = null;
	private String inworkrevenue = null;
	private String inworkactcost = null;
	private String inworkvgp = null;
	
	private String completeestcost = null;
	private String completerevenue = null;
	private String completeactcost = null;
	private String completevgp = null;
	
	private String closedestcost = null;
	private String closedrevenue = null;
	private String closedactcost = null;
	private String closedvgp = null;
	
	private String incompcloestcost = null;
	private String incompclorevenue = null;
	private String incompcloactcost = null;
	private String incompclovgp = null;
	private String msaid = null;
	
	private String ownerid = null;
	private String ownername = null;

	private String owner = null;
	//Start:Added by Vishal 04/01/2007
	private String internalversion=null;
	private String externalversion=null;
	private String submitreport=null;
	private String version=null;
	private String fromPage=null;
	//End: 
	private String dateOnSite = null;
	private String dateOffSite = null;
	private String dateComplete = null;
	private String dateClosed = null;
	private String nettype = null;
	private String ownerId = null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}
	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}
	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}
	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
	public String getCritdesc() {
		return critdesc;
	}
	public void setCritdesc(String critdesc) {
		this.critdesc = critdesc;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		typeid = null;
		startdate = null;
		enddate = null;
		save = null;
		reset = null;
		back = null;
		appendixname = null;
		jobname = null;
		
		internalversion=null;
		externalversion=null;
		version="I";
		submitreport=null;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getReport_type() {
		return report_type;
	}
	public void setReport_type(String report_type) {
		this.report_type = report_type;
	}
	public String[] getCriticality() {
		return criticality;
	}
	public void setCriticality(String[] criticality) {
		this.criticality = criticality;
	}
	public String getCriticality(int i)
	{
		return criticality[i];
	}
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRequestor() {
		return requestor;
	}
	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}
	public String getHelpdesk() {
		return helpdesk;
	}
	public void setHelpdesk(String helpdesk) {
		this.helpdesk = helpdesk;
	}
	public String getMsp() {
		return msp;
	}
	public void setMsp(String msp) {
		this.msp = msp;
	}
	public String getAppendix() {
		return appendix;
	}
	public void setAppendix(String appendix) {
		this.appendix = appendix;
	}
	public String getMsa() {
		return msa;
	}
	public void setMsa(String msa) {
		this.msa = msa;
	}
	public String getClosedactcost() {
		return closedactcost;
	}
	public void setClosedactcost(String closedactcost) {
		this.closedactcost = closedactcost;
	}
	public String getClosedestcost() {
		return closedestcost;
	}
	public void setClosedestcost(String closedestcost) {
		this.closedestcost = closedestcost;
	}
	public String getClosedrevenue() {
		return closedrevenue;
	}
	public void setClosedrevenue(String closedrevenue) {
		this.closedrevenue = closedrevenue;
	}
	public String getClosedvgp() {
		return closedvgp;
	}
	public void setClosedvgp(String closedvgp) {
		this.closedvgp = closedvgp;
	}
	public String getCompleteactcost() {
		return completeactcost;
	}
	public void setCompleteactcost(String completeactcost) {
		this.completeactcost = completeactcost;
	}
	public String getCompleteestcost() {
		return completeestcost;
	}
	public void setCompleteestcost(String completeestcost) {
		this.completeestcost = completeestcost;
	}
	public String getCompleterevenue() {
		return completerevenue;
	}
	public void setCompleterevenue(String completerevenue) {
		this.completerevenue = completerevenue;
	}
	public String getCompletevgp() {
		return completevgp;
	}
	public void setCompletevgp(String completevgp) {
		this.completevgp = completevgp;
	}
	public String getInworkactcost() {
		return inworkactcost;
	}
	public void setInworkactcost(String inworkactcost) {
		this.inworkactcost = inworkactcost;
	}
	public String getInworkestcost() {
		return inworkestcost;
	}
	public void setInworkestcost(String inworkestcost) {
		this.inworkestcost = inworkestcost;
	}
	public String getInworkrevenue() {
		return inworkrevenue;
	}
	public void setInworkrevenue(String inworkrevenue) {
		this.inworkrevenue = inworkrevenue;
	}
	public String getInworkvgp() {
		return inworkvgp;
	}
	public void setInworkvgp(String inworkvgp) {
		this.inworkvgp = inworkvgp;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getIncompcloactcost() {
		return incompcloactcost;
	}
	public void setIncompcloactcost(String incompcloactcost) {
		this.incompcloactcost = incompcloactcost;
	}
	public String getIncompcloestcost() {
		return incompcloestcost;
	}
	public void setIncompcloestcost(String incompcloestcost) {
		this.incompcloestcost = incompcloestcost;
	}
	public String getIncompclorevenue() {
		return incompclorevenue;
	}
	public void setIncompclorevenue(String incompclorevenue) {
		this.incompclorevenue = incompclorevenue;
	}
	public String getIncompclovgp() {
		return incompclovgp;
	}
	public void setIncompclovgp(String incompclovgp) {
		this.incompclovgp = incompclovgp;
	}
	public String getMsaid() {
		return msaid;
	}
	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}
	public String getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(String ownerid) {
		this.ownerid = ownerid;
	}
	public String getOwnername() {
		return ownername;
	}
	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getExternalversion() {
		return externalversion;
	}
	public void setExternalversion(String externalversion) {
		this.externalversion = externalversion;
	}
	public String getInternalversion() {
		return internalversion;
	}
	public void setInternalversion(String internalversion) {
		this.internalversion = internalversion;
	}
	public String getSubmitreport() {
		return submitreport;
	}
	public void setSubmitreport(String submitreport) {
		this.submitreport = submitreport;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getFromPage() {
		return fromPage;
	}
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}
	public String getDateClosed() {
		return dateClosed;
	}
	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}
	public String getDateComplete() {
		return dateComplete;
	}
	public void setDateComplete(String dateComplete) {
		this.dateComplete = dateComplete;
	}
	public String getDateOffSite() {
		return dateOffSite;
	}
	public void setDateOffSite(String dateOffSite) {
		this.dateOffSite = dateOffSite;
	}
	public String getDateOnSite() {
		return dateOnSite;
	}
	public void setDateOnSite(String dateOnSite) {
		this.dateOnSite = dateOnSite;
	}
	public String getNettype() {
		return nettype;
	}
	public void setNettype(String nettype) {
		this.nettype = nettype;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
}
