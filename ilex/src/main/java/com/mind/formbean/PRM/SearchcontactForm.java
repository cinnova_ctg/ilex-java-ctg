package com.mind.formbean.PRM;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;


public class SearchcontactForm extends ActionForm
{
	private String orgsearch = null;
	private String divsearch = null;
	private String search = null;
	
	private String type = null;
	
	private String[] id = null;
	private String[] org_name = null;
	private String[] org_division = null;
	private String[] city = null;
	private String[] state = null;
	
	private ArrayList contactlist = null;
	
	private String appendixid = null;

	
	public String[] getCity() {
		return city;
	}

	public void setCity( String[] city ) {
		this.city = city;
	}
	public String getCity( int i ) {
		return city[i];
	}
	
	
	public String[] getOrg_division() {
		return org_division;
	}

	public void setOrg_division( String[] org_division ) {
		this.org_division = org_division;
	}
	public String getOrg_division( int i ) {
		return org_division[i];
	}
	
	
	public String[] getOrg_name() {
		return org_name;
	}
	public void setOrg_name(String[] org_name) {
		this.org_name = org_name;
	}
	public String getOrg_name( int i ) {
		return org_name[i];
	}
	
	
	public String[] getState() {
		return state;
	}
	public void setState(String[] state) {
		this.state = state;
	}
	public String getState( int i ) {
		return state[i];
	}

	
	public ArrayList getContactlist() {
		return contactlist;
	}
	public void setContactlist( ArrayList contactlist ) {
		this.contactlist = contactlist;
	}

	
	public String getDivsearch() {
		return divsearch;
	}

	public void setDivsearch( String divsearch ) {
		this.divsearch = divsearch;
	}

	
	public String getOrgsearch() {
		return orgsearch;
	}

	public void setOrgsearch( String orgsearch ) {
		this.orgsearch = orgsearch;
	}

	
	public String getSearch() {
		return search;
	}

	public void setSearch( String search ) {
		this.search = search;
	}

	
	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	
	
	public String[] getId() {
		return id;
	}

	public void setId( String[] id ) {
		this.id = id;
	}
	
	public String getId( int i ) {
		return id[i];
	}

	public String getAppendixid() {
		return appendixid;
	}

	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
