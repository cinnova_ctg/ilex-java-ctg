package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AddContactForm extends ActionForm {

	private String pocid = null;
	private String typeid = null;
	private String type = null;
	private String cnspoc = null;
	private String email1 = null;
	private String email2 = null;
	private String phone1 = null;
	private String phone2 = null;
	private String fax = null;
	private String save = null;
	private String reset = null;
	private String function = null;
	private String authenticate = "";
	private String refresh = null;
	private String pocname = null;
	private String appendixname = null;
	private String custpoc = null;
	private String custemail1 = null;
	private String custemail2 = null;
	private String custphone1 = null;
	private String custphone2 = null;
	private String custfax = null;
	private String custpocname = null;

	private String salespoc = null;
	private String spocemail1 = null;
	private String spocemail2 = null;
	private String spocphone1 = null;
	private String spocphone2 = null;
	private String spocfax = null;

	private String businesspoc = null;
	private String businesspocemail1 = null;
	private String businesspocemail2 = null;
	private String businesspocphone1 = null;
	private String businesspocphone2 = null;
	private String businesspocfax = null;

	private String contractpoc = null;
	private String contractpocemail1 = null;
	private String contractpocemail2 = null;
	private String contractpocphone1 = null;
	private String contractpocphone2 = null;
	private String contractpocfax = null;

	private String billingpoc = null;
	private String billingpocemail1 = null;
	private String billingpocemail2 = null;
	private String billingpocphone1 = null;
	private String billingpocphone2 = null;
	private String billingpocfax = null;

	private String salespocname = null;
	private String businesspocname = null;
	private String contractpocname = null;
	private String billingpocname = null;

	private String back = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String FromPage = null;

	// For the View Selector
	private String monthWeekCheck = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;

	// New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;
	private String msaId = null;
	private String msaName = null;
	private String name = null;

	// For View Selector
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;

	// for poject Mananger and Team lead
	private String cnsPrjManagerPocName = null;
	private String teamLeadPocName = null;

	private String cnsPrjManagerPoc = null;
	private String cnsPrjManagerMail1 = null;
	private String cnsPrjManagerMail2 = null;
	private String cnsPrjManagerPhone1 = null;
	private String cnsPrjManagerPhone2 = null;
	private String cnsPrjManagerFax = null;

	private String teamLeadPoc = null;
	private String teamLeadMail1 = null;
	private String teamLeadMail2 = null;
	private String teamLeadPhone1 = null;
	private String teamLeadPhone2 = null;
	private String teamLeadFax = null;

	public String getCnsPrjManagerPoc() {
		return cnsPrjManagerPoc;
	}

	public void setCnsPrjManagerPoc(String cnsPrjManagerPoc) {
		this.cnsPrjManagerPoc = cnsPrjManagerPoc;
	}

	public String getCnsPrjManagerMail1() {
		return cnsPrjManagerMail1;
	}

	public void setCnsPrjManagerMail1(String cnsPrjManagerMail1) {
		this.cnsPrjManagerMail1 = cnsPrjManagerMail1;
	}

	public String getCnsPrjManagerMail2() {
		return cnsPrjManagerMail2;
	}

	public void setCnsPrjManagerMail2(String cnsPrjManagerMail2) {
		this.cnsPrjManagerMail2 = cnsPrjManagerMail2;
	}

	public String getCnsPrjManagerPhone1() {
		return cnsPrjManagerPhone1;
	}

	public void setCnsPrjManagerPhone1(String cnsPrjManagerPhone1) {
		this.cnsPrjManagerPhone1 = cnsPrjManagerPhone1;
	}

	public String getCnsPrjManagerPhone2() {
		return cnsPrjManagerPhone2;
	}

	public void setCnsPrjManagerPhone2(String cnsPrjManagerPhone2) {
		this.cnsPrjManagerPhone2 = cnsPrjManagerPhone2;
	}

	public String getCnsPrjManagerFax() {
		return cnsPrjManagerFax;
	}

	public void setCnsPrjManagerFax(String cnsPrjManagerFax) {
		this.cnsPrjManagerFax = cnsPrjManagerFax;
	}

	public String getTeamLeadPoc() {
		return teamLeadPoc;
	}

	public void setTeamLeadPoc(String teamLeadPoc) {
		this.teamLeadPoc = teamLeadPoc;
	}

	public String getTeamLeadMail1() {
		return teamLeadMail1;
	}

	public void setTeamLeadMail1(String teamLeadMail1) {
		this.teamLeadMail1 = teamLeadMail1;
	}

	public String getTeamLeadMail2() {
		return teamLeadMail2;
	}

	public void setTeamLeadMail2(String teamLeadMail2) {
		this.teamLeadMail2 = teamLeadMail2;
	}

	public String getTeamLeadPhone1() {
		return teamLeadPhone1;
	}

	public void setTeamLeadPhone1(String teamLeadPhone1) {
		this.teamLeadPhone1 = teamLeadPhone1;
	}

	public String getTeamLeadPhone2() {
		return teamLeadPhone2;
	}

	public void setTeamLeadPhone2(String teamLeadPhone2) {
		this.teamLeadPhone2 = teamLeadPhone2;
	}

	public String getTeamLeadFax() {
		return teamLeadFax;
	}

	public void setTeamLeadFax(String teamLeadFax) {
		this.teamLeadFax = teamLeadFax;
	}

	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String getJobSelectedOwners(int i) {
		return jobSelectedOwners[i];
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}

	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobSelectedStatus(int i) {
		return jobSelectedStatus[i];
	}

	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}

	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}

	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}

	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}

	public String getAppendixSelectedOwners(int i) {
		return appendixSelectedOwners[i];
	}

	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}

	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}

	public String getAppendixSelectedStatus(int i) {
		return appendixSelectedStatus[i];
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getOtherCheck() {
		return otherCheck;
	}

	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String getCustpocname() {
		return custpocname;
	}

	public void setCustpocname(String custpocname) {
		this.custpocname = custpocname;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getCnspoc() {
		return cnspoc;
	}

	public void setCnspoc(String cnspoc) {
		this.cnspoc = cnspoc;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPocid() {
		return pocid;
	}

	public void setPocid(String pocid) {
		this.pocid = pocid;
	}

	public String getReset() {
		return reset;
	}

	public void setReset(String reset) {
		this.reset = reset;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeid() {
		return typeid;
	}

	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}

	public String getRefresh() {
		return refresh;
	}

	public void setRefresh(String refresh) {
		this.refresh = refresh;
	}

	public String getPocname() {
		return pocname;
	}

	public void setPocname(String pocname) {
		this.pocname = pocname;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		pocid = null;
		typeid = null;
		type = null;
		cnspoc = null;
		email1 = null;
		email2 = null;
		phone1 = null;
		phone2 = null;
		fax = null;
		save = null;
		reset = null;
		function = null;
		refresh = null;
		pocname = null;
		appendixname = null;

		custpoc = null;
		custemail1 = null;
		custemail2 = null;
		custphone1 = null;
		custphone2 = null;
		custfax = null;
		custpocname = null;

		salespoc = null;
		spocemail1 = null;
		spocemail2 = null;
		spocphone1 = null;
		spocphone2 = null;
		spocfax = null;

		businesspoc = null;
		businesspocemail1 = null;
		businesspocemail2 = null;
		businesspocphone1 = null;
		businesspocphone2 = null;
		businesspocfax = null;

		contractpoc = null;
		contractpocemail1 = null;
		contractpocemail2 = null;
		contractpocphone1 = null;
		contractpocphone2 = null;
		contractpocfax = null;

		billingpoc = null;
		billingpocemail1 = null;
		billingpocemail2 = null;
		billingpocphone1 = null;
		billingpocphone2 = null;
		billingpocfax = null;

		salespocname = null;
		businesspocname = null;
		contractpocname = null;
		billingpocname = null;

		back = null;

	}

	public String getCustemail1() {
		return custemail1;
	}

	public void setCustemail1(String custemail1) {
		this.custemail1 = custemail1;
	}

	public String getCustemail2() {
		return custemail2;
	}

	public void setCustemail2(String custemail2) {
		this.custemail2 = custemail2;
	}

	public String getCustfax() {
		return custfax;
	}

	public void setCustfax(String custfax) {
		this.custfax = custfax;
	}

	public String getCustphone1() {
		return custphone1;
	}

	public void setCustphone1(String custphone1) {
		this.custphone1 = custphone1;
	}

	public String getCustphone2() {
		return custphone2;
	}

	public void setCustphone2(String custphone2) {
		this.custphone2 = custphone2;
	}

	public String getCustpoc() {
		return custpoc;
	}

	public void setCustpoc(String custpoc) {
		this.custpoc = custpoc;
	}

	public String getBillingpoc() {
		return billingpoc;
	}

	public void setBillingpoc(String billingpoc) {
		this.billingpoc = billingpoc;
	}

	public String getBillingpocemail1() {
		return billingpocemail1;
	}

	public void setBillingpocemail1(String billingpocemail1) {
		this.billingpocemail1 = billingpocemail1;
	}

	public String getBillingpocemail2() {
		return billingpocemail2;
	}

	public void setBillingpocemail2(String billingpocemail2) {
		this.billingpocemail2 = billingpocemail2;
	}

	public String getBillingpocfax() {
		return billingpocfax;
	}

	public void setBillingpocfax(String billingpocfax) {
		this.billingpocfax = billingpocfax;
	}

	public String getBillingpocphone1() {
		return billingpocphone1;
	}

	public void setBillingpocphone1(String billingpocphone1) {
		this.billingpocphone1 = billingpocphone1;
	}

	public String getBillingpocphone2() {
		return billingpocphone2;
	}

	public void setBillingpocphone2(String billingpocphone2) {
		this.billingpocphone2 = billingpocphone2;
	}

	public String getBusinesspoc() {
		return businesspoc;
	}

	public void setBusinesspoc(String businesspoc) {
		this.businesspoc = businesspoc;
	}

	public String getBusinesspocemail1() {
		return businesspocemail1;
	}

	public void setBusinesspocemail1(String businesspocemail1) {
		this.businesspocemail1 = businesspocemail1;
	}

	public String getBusinesspocemail2() {
		return businesspocemail2;
	}

	public void setBusinesspocemail2(String businesspocemail2) {
		this.businesspocemail2 = businesspocemail2;
	}

	public String getBusinesspocfax() {
		return businesspocfax;
	}

	public void setBusinesspocfax(String businesspocfax) {
		this.businesspocfax = businesspocfax;
	}

	public String getBusinesspocphone1() {
		return businesspocphone1;
	}

	public void setBusinesspocphone1(String businesspocphone1) {
		this.businesspocphone1 = businesspocphone1;
	}

	public String getBusinesspocphone2() {
		return businesspocphone2;
	}

	public void setBusinesspocphone2(String businesspocphone2) {
		this.businesspocphone2 = businesspocphone2;
	}

	public String getContractpoc() {
		return contractpoc;
	}

	public void setContractpoc(String contractpoc) {
		this.contractpoc = contractpoc;
	}

	public String getContractpocemail1() {
		return contractpocemail1;
	}

	public void setContractpocemail1(String contractpocemail1) {
		this.contractpocemail1 = contractpocemail1;
	}

	public String getContractpocemail2() {
		return contractpocemail2;
	}

	public void setContractpocemail2(String contractpocemail2) {
		this.contractpocemail2 = contractpocemail2;
	}

	public String getContractpocfax() {
		return contractpocfax;
	}

	public void setContractpocfax(String contractpocfax) {
		this.contractpocfax = contractpocfax;
	}

	public String getContractpocphone1() {
		return contractpocphone1;
	}

	public void setContractpocphone1(String contractpocphone1) {
		this.contractpocphone1 = contractpocphone1;
	}

	public String getContractpocphone2() {
		return contractpocphone2;
	}

	public void setContractpocphone2(String contractpocphone2) {
		this.contractpocphone2 = contractpocphone2;
	}

	public String getSalespoc() {
		return salespoc;
	}

	public void setSalespoc(String salespoc) {
		this.salespoc = salespoc;
	}

	public String getSpocemail1() {
		return spocemail1;
	}

	public void setSpocemail1(String spocemail1) {
		this.spocemail1 = spocemail1;
	}

	public String getSpocemail2() {
		return spocemail2;
	}

	public void setSpocemail2(String spocemail2) {
		this.spocemail2 = spocemail2;
	}

	public String getSpocfax() {
		return spocfax;
	}

	public void setSpocfax(String spocfax) {
		this.spocfax = spocfax;
	}

	public String getSpocphone1() {
		return spocphone1;
	}

	public void setSpocphone1(String spocphone1) {
		this.spocphone1 = spocphone1;
	}

	public String getSpocphone2() {
		return spocphone2;
	}

	public void setSpocphone2(String spocphone2) {
		this.spocphone2 = spocphone2;
	}

	public String getBillingpocname() {
		return billingpocname;
	}

	public void setBillingpocname(String billingpocname) {
		this.billingpocname = billingpocname;
	}

	public String getBusinesspocname() {
		return businesspocname;
	}

	public void setBusinesspocname(String businesspocname) {
		this.businesspocname = businesspocname;
	}

	public String getContractpocname() {
		return contractpocname;
	}

	public void setContractpocname(String contractpocname) {
		this.contractpocname = contractpocname;
	}

	public String getSalespocname() {
		return salespocname;
	}

	public void setSalespocname(String salespocname) {
		this.salespocname = salespocname;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getFromPage() {
		return FromPage;
	}

	public void setFromPage(String fromPage) {
		FromPage = fromPage;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cnsPrjManagerPocName
	 */
	public String getCnsPrjManagerPocName() {
		return cnsPrjManagerPocName;
	}

	/**
	 * @param cnsPrjManagerPocName
	 *            the cnsPrjManagerPocName to set
	 */
	public void setCnsPrjManagerPocName(String cnsPrjManagerPocName) {
		this.cnsPrjManagerPocName = cnsPrjManagerPocName;
	}

	/**
	 * @return the teamLeadPocName
	 */
	public String getTeamLeadPocName() {
		return teamLeadPocName;
	}

	/**
	 * @param teamLeadPocName
	 *            the teamLeadPocName to set
	 */
	public void setTeamLeadPocName(String teamLeadPocName) {
		this.teamLeadPocName = teamLeadPocName;
	}

}
