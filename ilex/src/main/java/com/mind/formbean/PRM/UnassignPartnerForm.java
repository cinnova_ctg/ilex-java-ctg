package com.mind.formbean.PRM;

import org.apache.struts.action.ActionForm;

public class UnassignPartnerForm extends ActionForm{

	private String appendixid = null;
	private String jobid = null;
	private String activityid = null;
	private String authenticate="";
	private String unassignmessage=null;
	public String getActivityid() {
		return activityid;
	}
	public void setActivityid(String activityid) {
		this.activityid = activityid;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getUnassignmessage() {
		return unassignmessage;
	}
	public void setUnassignmessage(String unassignmessage) {
		this.unassignmessage = unassignmessage;
	}
	
}
