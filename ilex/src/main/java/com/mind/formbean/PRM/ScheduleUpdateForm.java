package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ScheduleUpdateForm extends ActionForm{

	private String scheduleid = null;
	private String typeid = null;
	private String type = null;
	private String nettype = null;
	private String startdate = null;
	private String plannedenddate = null;
	
	private String startdatehh = null;
	private String startdatemm = null;
	private String startdateop = null;
	
	private String plannedenddatehh = null;
	private String plannedenddatemm = null;
	private String plannedenddateop = null;
	
	private String actstartdate = null;
	private String actenddate = null;
	
	private String actstartdatehh = null;
	private String actstartdatemm = null;
	private String actstartdateop = null;
	
	private String actenddatehh = null;
	private String actenddatemm = null;
	private String actenddateop = null;
		
	
	private String totaldays = null;
	private String save = null;
	private String reset = null;
	private String function = null;
	private String updatemessage = null;
	private String authenticate = "";
	private String page = null;
	private String pageid = null;
	private String back = null;
	private String appendixname = null;
	private String jobname = null;
	
	private String viewjobtype = null;
	private String ownerId = null;
	private String checknetmedxtype = null;
	private String appendixId = null;
	private String msaId = null;
	
	//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	// - For Job Reschedule
	private String rseReason = null;
	private String rseType = null;
	private String oldSchStart = null;
	private String newSchStart = null;
	private String currentSchDate = null;
		
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	
	
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getPlannedenddate() {
		return plannedenddate;
	}
	public void setPlannedenddate(String plannedenddate) {
		this.plannedenddate = plannedenddate;
	}
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getTotaldays() {
		return totaldays;
	}
	public void setTotaldays(String totaldays) {
		this.totaldays = totaldays;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getScheduleid() {
		return scheduleid;
	}
	public void setScheduleid(String scheduleid) {
		this.scheduleid = scheduleid;
	}
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getUpdatemessage() {
		return updatemessage;
	}
	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		
		scheduleid = null;
		typeid = null;
		type = null;
		startdate = null;
		plannedenddate = null;
		totaldays = null;
		save = null;
		reset = null;
		function = null;
		updatemessage = null;
		back = null;
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getPageid() {
		return pageid;
	}
	public void setPageid(String pageid) {
		this.pageid = pageid;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getActenddate() {
		return actenddate;
	}
	public void setActenddate(String actenddate) {
		this.actenddate = actenddate;
	}
	public String getActenddatehh() {
		return actenddatehh;
	}
	public void setActenddatehh(String actenddatehh) {
		this.actenddatehh = actenddatehh;
	}
	public String getActenddatemm() {
		return actenddatemm;
	}
	public void setActenddatemm(String actenddatemm) {
		this.actenddatemm = actenddatemm;
	}
	public String getActenddateop() {
		return actenddateop;
	}
	public void setActenddateop(String actenddateop) {
		this.actenddateop = actenddateop;
	}
	public String getActstartdate() {
		return actstartdate;
	}
	public void setActstartdate(String actstartdate) {
		this.actstartdate = actstartdate;
	}
	public String getActstartdatehh() {
		return actstartdatehh;
	}
	public void setActstartdatehh(String actstartdatehh) {
		this.actstartdatehh = actstartdatehh;
	}
	public String getActstartdatemm() {
		return actstartdatemm;
	}
	public void setActstartdatemm(String actstartdatemm) {
		this.actstartdatemm = actstartdatemm;
	}
	public String getActstartdateop() {
		return actstartdateop;
	}
	public void setActstartdateop(String actstartdateop) {
		this.actstartdateop = actstartdateop;
	}
	public String getPlannedenddatehh() {
		return plannedenddatehh;
	}
	public void setPlannedenddatehh(String plannedenddatehh) {
		this.plannedenddatehh = plannedenddatehh;
	}
	public String getPlannedenddatemm() {
		return plannedenddatemm;
	}
	public void setPlannedenddatemm(String plannedenddatemm) {
		this.plannedenddatemm = plannedenddatemm;
	}
	public String getPlannedenddateop() {
		return plannedenddateop;
	}
	public void setPlannedenddateop(String plannedenddateop) {
		this.plannedenddateop = plannedenddateop;
	}
	public String getStartdatehh() {
		return startdatehh;
	}
	public void setStartdatehh(String startdatehh) {
		this.startdatehh = startdatehh;
	}
	public String getStartdatemm() {
		return startdatemm;
	}
	public void setStartdatemm(String startdatemm) {
		this.startdatemm = startdatemm;
	}
	public String getStartdateop() {
		return startdateop;
	}
	public void setStartdateop(String startdateop) {
		this.startdateop = startdateop;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getNettype() {
		return nettype;
	}
	public void setNettype(String nettype) {
		this.nettype = nettype;
	}
	public String getChecknetmedxtype() {
		return checknetmedxtype;
	}
	public void setChecknetmedxtype(String checknetmedxtype) {
		this.checknetmedxtype = checknetmedxtype;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	
	public String getRseType() {
		return rseType;
	}

	public void setRseType(String rseType) {
		this.rseType = rseType;
	}

	public String getRseReason() {
		return rseReason;
	}

	public void setRseReason(String rseReason) {
		this.rseReason = rseReason;
	}

	public String getNewSchStart() {
		return newSchStart;
	}

	public void setNewSchStart(String newSchStart) {
		this.newSchStart = newSchStart;
	}

	public String getOldSchStart() {
		return oldSchStart;
	}

	public void setOldSchStart(String oldSchStart) {
		this.oldSchStart = oldSchStart;
	}

	public String getCurrentSchDate() {
		return currentSchDate;
	}

	public void setCurrentSchDate(String currentSchDate) {
		this.currentSchDate = currentSchDate;
	}

	
	
}
