package com.mind.formbean.PRM;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class JobActivityDetailForm extends ActionForm
{
	private String Appendixid = null;
	private String jobid = null;
	private String activityid = null;
	private String activityname = null;
	private String type = null;
	private String quantity = null;
	private String partnerid = null;
	private String partnername= null;
	private String contractlabor = null;
	private String cnslabor= null;
	private String materials = null;
	private String freight = null;
	private String travel = null;
	private String total = null;
	private String extendedprice = null;
	private String pmargin = null;
	private String listprice = null;
	private String estart = null;
	private String ecomplete = null;
	private String status = null;
	
	public String getActivityname() {
		return activityname;
	}
	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}
	public String getAppendixid() {
		return Appendixid;
	}
	public void setAppendixid(String appendixid) {
		Appendixid = appendixid;
	}
	public String getCnslabor() {
		return cnslabor;
	}
	public void setCnslabor(String cnslabor) {
		this.cnslabor = cnslabor;
	}
	public String getContractlabor() {
		return contractlabor;
	}
	public void setContractlabor(String contractlabor) {
		this.contractlabor = contractlabor;
	}
	public String getEcomplete() {
		return ecomplete;
	}
	public void setEcomplete(String ecomplete) {
		this.ecomplete = ecomplete;
	}
	public String getEstart() {
		return estart;
	}
	public void setEstart(String estart) {
		this.estart = estart;
	}
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}
	public String getFreight() {
		return freight;
	}
	public void setFreight(String freight) {
		this.freight = freight;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getListprice() {
		return listprice;
	}
	public void setListprice(String listprice) {
		this.listprice = listprice;
	}
	public String getMaterials() {
		return materials;
	}
	public void setMaterials(String materials) {
		this.materials = materials;
	}
	public String getPartnername() {
		return partnername;
	}
	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}
	public String getPmargin() {
		return pmargin;
	}
	public void setPmargin(String pmargin) {
		this.pmargin = pmargin;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTravel() {
		return travel;
	}
	public void setTravel(String travel) {
		this.travel = travel;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	
	public String getActivityid() {
		return activityid;
	}
	public void setActivityid(String activityid) {
		this.activityid = activityid;
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		
		Appendixid = null;
		jobid = null;
		activityid = null;
		activityname = null;
		type = null;
		quantity = null;
		partnerid = null;
		partnername = null;
		contractlabor = null;
		cnslabor= null;
		materials = null;
		freight = null;
		travel = null;
		total = null;
		extendedprice = null;
		pmargin = null;
		listprice = null;
		estart = null;
		ecomplete = null;
		status = null;

	}
	

}
