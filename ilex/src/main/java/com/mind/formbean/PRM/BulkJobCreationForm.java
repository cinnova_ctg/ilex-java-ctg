package com.mind.formbean.PRM;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class BulkJobCreationForm extends ActionForm{
	
	private String state = null;
	private String stateSelect = null;
	private String country = null;
	private ArrayList sortparam = null;
	private String[] selectedparam = null;
	private String appendixname = null;
	private String msaname = null;
	private String id = "";
	private String id1 = "";
	private String ref = "";
	private String addendum_id = null;
	private String currentstatus = null;
	private String job_Name = null;
	private String job_NewName = null;
	private String site_name = "N";//For Radio Button
	private String site_state = null;
	private String prifix = "P";
	private String sufix = null;
	private String city = null;
	
	private String site_Number = null;
	private String startDate = null;
	private String endDate = null;
	
	private String activity[] = null;
	
	private String temp_activity_id[]=null;
	private String type[] = null;
	private String qty[] = null;
	private String estimated_total_cost[] = null;
	private String extended_sub_total[] = null;
	private String extended_Price[] = null;
	private String proforma_Margin[] = null;
	private String libraryId[]=null;
	private String paragraph_content = null;
	
    /*private String activity = null;
	private String type = null;
	private String qty = null;
	private String estimated_total_cost = null;
	private String extended_sub_total = null;
	private String extended_Price = null;
	private String proforma_Margin = null;*/
	
	
	
	
	private String appendix_Id=null;
	private String[] activity_id = null;
	private String msa_id = null;
	
	private String siteNumber=null;
	private String siteName=null;
	
	
		
	
	private String estimated_Material_Cost = null;
	private String stimated_cnsfieldlaborcost = null;
	private String estimated_contractfieldlaborcost = null;
	private String estimated_freightcost = null;
	private String estimated_travelcost = null;
	
	private String estimated_totalcost = null;
	private String overheadcost = null;
	
	
	
	
	
	private String siteid = null;
	private ArrayList sitedetaillist = null;
	private String viewjobtype = null;
	private String ownerId = null;
	
	private String countryId = null;
	private String countryName = null;
	private String stateCmb = null;
	private ArrayList tempList = null;
	private String bulkJobEndCustomer = null;
	private String brand = null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	private String newOwner=null;
	private String newScheduler=null;
	private String save=null;
	private String createJob=null;
	
	// For Site Error Message
	private String errorrSiteNumber=null;
	private String errorPlannedStartDate=null;
	
	private String custRef = null;
	private String createMPO = null;
	private String jobId = null;
	
	private String deliverDate = null;
	private String scheduleDate = null;
	private String partnerName = null;
	private String keyWords = null;
	private String searchPartner = null;
	private String craeteJobs = null;
	
	private ArrayList mpoList = null;
	private ArrayList partnerList = null;
	
	private String partnerId = null;
	private String namePartner = null;
	private String partnerAddress = null;
	private String partnerCity = null;
	private String partnerState = null;
	private String partnerCountry = null;
	private String partnerZipcode = null;
	private String pocFirstName = null;
	private String pocLastName = null;
	private String partnerType = null;
	
	private String mpoIdSelected = null;
	private String[] mpoId= null;
	private String[] mpoName = null;
	private String[] masterPOItem = null;
	private String[] estimatedTotalCost = null;
	private String[] status =null;
	private String[] isSelectable = null;
	
	private String formatedActivityList = null;
	private String[] parnerIdList = null;
	
	private Collection poTypeList = null;
	private String poType = null;
	
	
	
	public String[] getParnerIdList() {
		return parnerIdList;
	}
	public void setParnerIdList(String[] parnerIdList) {
		this.parnerIdList = parnerIdList;
	}
	public String getFormatedActivityList() {
		return formatedActivityList;
	}
	public void setFormatedActivityList(String formatedActivityList) {
		this.formatedActivityList = formatedActivityList;
	}
	public String getCraeteJobs() {
		return craeteJobs;
	}
	public void setCraeteJobs(String craeteJobs) {
		this.craeteJobs = craeteJobs;
	}
	public String getKeyWords() {
		return keyWords;
	}
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getSearchPartner() {
		return searchPartner;
	}
	public void setSearchPartner(String searchPartner) {
		this.searchPartner = searchPartner;
	}
	public String getDeliverDate() {
		return deliverDate;
	}
	public void setDeliverDate(String deliverDate) {
		this.deliverDate = deliverDate;
	}
	public String getScheduleDate() {
		return scheduleDate;
	}
	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getCreateMPO() {
		return createMPO;
	}
	public void setCreateMPO(String createMPO) {
		this.createMPO = createMPO;
	}
	public String getCustRef() {
		return custRef;
	}
	public void setCustRef(String custRef) {
		this.custRef = custRef;
	}
	public String getErrorrSiteNumber() {
		return errorrSiteNumber;
	}
	public void setErrorrSiteNumber(String errorrSiteNumber) {
		this.errorrSiteNumber = errorrSiteNumber;
	}

	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getNewScheduler() {
		return newScheduler;
	}
	public void setNewScheduler(String newScheduler) {
		this.newScheduler = newScheduler;
	}
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getBulkJobEndCustomer() {
		return bulkJobEndCustomer;
	}

	public void setBulkJobEndCustomer(String bulkJobEndCustomer) {
		this.bulkJobEndCustomer = bulkJobEndCustomer;
	}
	public String getStateCmb() {
		return stateCmb;
	}

	public void setStateCmb(String stateCmb) {
		this.stateCmb = stateCmb;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public ArrayList getTempList() {
		return tempList;
	}

	public void setTempList(ArrayList tempList) {
		this.tempList = tempList;
	}

	public String getJob_Name() {
		return job_Name;
	}

	public void setJob_Name(String job_Name) {
		this.job_Name = job_Name;
	}

	public String getCurrentstatus() {
		return currentstatus;
	}

	public void setCurrentstatus(String currentstatus) {
		this.currentstatus = currentstatus;
	}

	public String getAddendum_id() {
		return addendum_id;
	}

	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(String id1) {
		this.id1 = id1;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String[] getSelectedparam() {
		return selectedparam;
	}

	public void setSelectedparam(String[] selectedparam) {
		this.selectedparam = selectedparam;
	}
	
	public String getSelectedparam( int i ) {
		return selectedparam[i];
	}

	public ArrayList getSortparam() {
		return sortparam;
	}

	public void setSortparam(ArrayList sortparam) {
		this.sortparam = sortparam;
	}

	

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_state() {
		return site_state;
	}

	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}

	public String getPrifix() {
		return prifix;
	}

	public void setPrifix(String prifix) {
		this.prifix = prifix;
	}

	public String getSufix() {
		return sufix;
	}

	public void setSufix(String sufix) {
		this.sufix = sufix;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSiteid() {
		return siteid;
	}

	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}

	public ArrayList getSitedetaillist() {
		return sitedetaillist;
	}

	public void setSitedetaillist(ArrayList sitedetaillist) {
		this.sitedetaillist = sitedetaillist;
	}

	public String getAppendix_Id() {
		return appendix_Id;
	}

	public void setAppendix_Id(String appendix_Id) {
		this.appendix_Id = appendix_Id;
	}

	
	public String getEstimated_contractfieldlaborcost() {
		return estimated_contractfieldlaborcost;
	}

	public void setEstimated_contractfieldlaborcost(
			String estimated_contractfieldlaborcost) {
		this.estimated_contractfieldlaborcost = estimated_contractfieldlaborcost;
	}

	public String getEstimated_freightcost() {
		return estimated_freightcost;
	}

	public void setEstimated_freightcost(String estimated_freightcost) {
		this.estimated_freightcost = estimated_freightcost;
	}

	public String getEstimated_Material_Cost() {
		return estimated_Material_Cost;
	}

	public void setEstimated_Material_Cost(String estimated_Material_Cost) {
		this.estimated_Material_Cost = estimated_Material_Cost;
	}

	public String getEstimated_totalcost() {
		return estimated_totalcost;
	}

	public void setEstimated_totalcost(String estimated_totalcost) {
		this.estimated_totalcost = estimated_totalcost;
	}

	public String getEstimated_travelcost() {
		return estimated_travelcost;
	}

	public void setEstimated_travelcost(String estimated_travelcost) {
		this.estimated_travelcost = estimated_travelcost;
	}

	public String getOverheadcost() {
		return overheadcost;
	}

	public void setOverheadcost(String overheadcost) {
		this.overheadcost = overheadcost;
	}

	public String getStimated_cnsfieldlaborcost() {
		return stimated_cnsfieldlaborcost;
	}

	public void setStimated_cnsfieldlaborcost(String stimated_cnsfieldlaborcost) {
		this.stimated_cnsfieldlaborcost = stimated_cnsfieldlaborcost;
	}

	
	
	public String[] getActivity() {
		return activity;
	}

	public void setActivity(String[] activity) {
		this.activity = activity;
	}
	
	public String getActivity( int i) {
		return activity[i];
	}

	public String getEstimated_total_cost(int i) {
		return estimated_total_cost[i];
	}

	public void setEstimated_total_cost(String[] estimated_total_cost) {
		this.estimated_total_cost = estimated_total_cost;
	}

	public String getExtended_Price(int i) {
		return extended_Price[i];
	}

	public void setExtended_Price(String[] extended_Price) {
		this.extended_Price = extended_Price;
	}

	public String getExtended_sub_total(int i) {
		return extended_sub_total[i];
	}

	public void setExtended_sub_total(String[] extended_sub_total) {
		this.extended_sub_total = extended_sub_total;
	}

	public String getProforma_Margin(int i) {
		return proforma_Margin[i];
	}

	public void setProforma_Margin(String[] proforma_Margin) {
		this.proforma_Margin = proforma_Margin;
	}

	public String getQty(int i) {
		return qty[i];
	}

	public void setQty(String[] qty) {
		this.qty = qty;
	}

	public String getType(int i) {
		return type[i];
	}

	public void setType(String[] type) {
		this.type = type;
	}

	public String[] getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(String[] libraryId) {
		this.libraryId = libraryId;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String[] getTemp_activity_id() {
		return temp_activity_id;
	}

	public void setTemp_activity_id(String[] temp_activity_id) {
		this.temp_activity_id = temp_activity_id;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getMsa_id() {
		return msa_id;
	}

	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String[] getActivity_id() {
		return activity_id;
	}

	public void setActivity_id(String[] activity_id) {
		this.activity_id = activity_id;
	}
	
	public String getActivity_id( int i ) {
		return activity_id[i];
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSite_Number() {
		return site_Number;
	}

	public void setSite_Number(String site_Number) {
		this.site_Number = site_Number;
	}
	public String getNewOwner() {
		return newOwner;
	}
	public void setNewOwner(String newOwner) {
		this.newOwner = newOwner;
	}
	public String getParagraph_content() {
		return paragraph_content;
	}
	public void setParagraph_content(String paragraph_content) {
		this.paragraph_content = paragraph_content;
	}
	public String getErrorPlannedStartDate() {
		return errorPlannedStartDate;
	}
	public void setErrorPlannedStartDate(String errorPlannedStartDate) {
		this.errorPlannedStartDate = errorPlannedStartDate;
	}
	public String getJob_NewName() {
		return job_NewName;
	}
	public void setJob_NewName(String job_NewName) {
		this.job_NewName = job_NewName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateSelect() {
		return stateSelect;
	}
	public void setStateSelect(String stateSelect) {
		this.stateSelect = stateSelect;
	}
	public ArrayList getMpoList() {
		return mpoList;
	}
	public void setMpoList(ArrayList mpoList) {
		this.mpoList = mpoList;
	}
	public String getPartnerAddress() {
		return partnerAddress;
	}
	public void setPartnerAddress(String partnerAddress) {
		this.partnerAddress = partnerAddress;
	}
	public String getPartnerCity() {
		return partnerCity;
	}
	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}
	public String getPartnerCountry() {
		return partnerCountry;
	}
	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public ArrayList getPartnerList() {
		return partnerList;
	}
	public void setPartnerList(ArrayList partnerList) {
		this.partnerList = partnerList;
	}
	public String getPartnerState() {
		return partnerState;
	}
	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}
	public String getPartnerZipcode() {
		return partnerZipcode;
	}
	public void setPartnerZipcode(String partnerZipcode) {
		this.partnerZipcode = partnerZipcode;
	}
	public String getPocFirstName() {
		return pocFirstName;
	}
	public void setPocFirstName(String pocFirstName) {
		this.pocFirstName = pocFirstName;
	}
	public String getPocLastName() {
		return pocLastName;
	}
	public void setPocLastName(String pocLastName) {
		this.pocLastName = pocLastName;
	}
	public String getNamePartner() {
		return namePartner;
	}
	public void setNamePartner(String name) {
		namePartner = name;
	}
	public String[] getEstimatedTotalCost() {
		return estimatedTotalCost;
	}
	public void setEstimatedTotalCost(String[] estimatedTotalCost) {
		this.estimatedTotalCost = estimatedTotalCost;
	}
	public String[] getIsSelectable() {
		return isSelectable;
	}
	public void setIsSelectable(String[] isSelectable) {
		this.isSelectable = isSelectable;
	}
	public String[] getMasterPOItem() {
		return masterPOItem;
	}
	public void setMasterPOItem(String[] masterPOItem) {
		this.masterPOItem = masterPOItem;
	}
	public String[] getMpoId() {
		return mpoId;
	}
	public void setMpoId(String[] mpoId) {
		this.mpoId = mpoId;
	}
	public String[] getMpoName() {
		return mpoName;
	}
	public void setMpoName(String[] mpoName) {
		this.mpoName = mpoName;
	}
	public String[] getStatus() {
		return status;
	}
	public void setStatus(String[] status) {
		this.status = status;
	}
	public String getCreateJob() {
		return createJob;
	}
	public void setCreateJob(String createJob) {
		this.createJob = createJob;
	}
	public String getMpoIdSelected() {
		return mpoIdSelected;
	}
	public void setMpoIdSelected(String mpoIdSelected) {
		this.mpoIdSelected = mpoIdSelected;
	}
	public String getPoType() {
		return poType;
	}
	public void setPoType(String poType) {
		this.poType = poType;
	}
	public Collection getPoTypeList() {
		return poTypeList;
	}
	public void setPoTypeList(Collection poTypeList) {
		this.poTypeList = poTypeList;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}


}
