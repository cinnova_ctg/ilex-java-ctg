package com.mind.formbean.PRM;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AppendixHeaderForm extends ActionForm {

	// private String Appendixid = null;
	private String ownerId = null;
	private String jobname = null;
	private String siteid = null;
	private String sitename = null;
	private String sitepoc = null;
	private String sitephone = null;
	private String custref = null;
	private String endcust = null;

	private String from = null;

	// private String partnerid = null;
	// private String partnername = null;
	private String fieldlabor = null;
	private String cnslabor = null;
	private String materials = null;
	private String freight = null;
	private String travel = null;
	private String total = null;
	// private String extendedprice = null;
	private String pmargin = null;
	private String listprice = null;
	private String estart = null;
	private String ecomplete = null;

	// private String status = null;
	private String type = null;
	private String siteno = null;
	private String sitestate = null;
	private String sitezipcode = null;
	private String siteaddress = null;
	private String sitecity = null;
	private String jobtype = null;

	private String appendixid = null;
	private String appendixname = null;
	private String status = null;
	private String startdate = null;
	private String plannedenddate = null;
	private String estimatedcost = null;
	private String extendedprice = null;
	private String proformamargin = null;
	private String jobid = null;
	private String jobaddendumname = null;

	private String pocname = null;
	private String pocphone = null;
	private String pocemail = null;
	private String pocfax = null;

	private String authenticate = "";
	private String appendixfieldlabor = null;
	private String appendixcnslabor = null;
	private String appendixmaterials = null;
	private String appendixfreight = null;
	private String appendixtravel = null;
	private String msaname = null;
	private String appendixcnspoc = null;
	private String appendixbusinesspoc = null;
	private String appendixcontractpoc = null;
	private String appendixbillingpoc = null;
	private String appendixcnspocphone = null;
	private String appendixbusinesspocphone = null;
	private String appendixcontractpocphone = null;
	private String appendixbillingpocphone = null;
	private String cnsprojectmgrId = null;
	private String cnsprojectmgr = null;
	private String custprojectmgr = null;
	private String cnsprojectmgrphone = null;
	private String custprojectmgrphone = null;
	private String custinfodata1 = null;
	private String custinfovalue1 = null;
	private String custinfodata2 = null;
	private String custinfovalue2 = null;
	private String custinfodata3 = null;
	private String custinfovalue3 = null;
	private String custinfochangedby = null;
	private String custinfochangedate = null;
	private String appendix_type = null;
	// private String numsite = null;
	private String statusdesc = null;
	private String viewjobtype = null;

	private String estarttime = null;
	private String ecompletetime = null;
	private String startdatetime = null;
	private String plannedenddatetime = null;
	private String scope = null;

	private String jobupdateddby = null;
	private String jobupdatedate = null;
	private String jobupdatetime = null;

	private String poid = null;
	private String ponumber = null;
	private String partnername = null;
	private String partnerpocname = null;
	private String partnerpocphone = null;
	private String partnerid = null;
	private Collection powolist = null;

	private String actualcosts = null;
	private String actualvgp = null;
	private String sitesecondarypoc = null;
	private String sitesecondaryphone = null;
	private String sitesecondaryemail = null;
	private String siteprimaryphone = null;
	private String siteprimaryemail = null;

	private String invoice_no = null;
	private String inv_type = null;
	private String inv_id = null;
	private String inv_rdofilter = null;
	private String invoice_Flag = null;
	private String invoiceno = null;
	private String from_date = null;
	private String to_date = null;
	private String powo_number = null;
	private String partner_name = null;

	// for yellow folder show
	private String documentpresent = null;

	private String jobcreatedby = null;
	private Collection installationlist = null;
	private String job_netinstallation_date = "";
	private String job_netinstallation_time = "";

	private String job_closed = null;
	private String job_complete = null;
	private String job_inwork = null;
	private String job_total = null;
	private String job_earliest_start = null;
	private String job_earliest_end = null;
	private String job_latest_start = null;
	private String job_latest_end = null;

	private String actualvgpm = null;
	private String pmvipp = null;

	private String job_search_name = null;
	private String job_search_site_number = null;
	private String job_search_city = null;
	private String job_search_state = null;
	private String job_search_date_from = null;
	private String job_search_date_to = null;
	private String dateOnSite = null;
	private String dateOffSite = null;
	private String dateComplete = null;
	private String dateClosed = null;
	private String job_search_owner = null;
	private String job_search = null;
	private String cancel = null;
	private String rowsize = null;
	private Collection tempList = null;
	private String siteCountryId = null;
	private String siteCountryName = null;
	private String msaId = null;
	private String fromPage = null;

	// For View Selector
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;

	// powo change
	private String powoType = null;

	private String rseCount = null;

	// project Manager and team lead
	private String cnsPrjManagerId;
	private String cnsPrjManagerName;
	private String cnsPrjManagerPhone;

	private String apiAccessBtn = null;
	private String apiAccessChkBox = null;
	private String apiCheckBox = null;
	private String aPIAccessPassword = null;
	private String apiUserName = null;

	public String getApiAccessBtn() {
		return apiAccessBtn;
	}

	public void setApiAccessBtn(String apiAccessBtn) {
		this.apiAccessBtn = apiAccessBtn;
	}

	public String getApiAccessChkBox() {
		return apiAccessChkBox;
	}

	public void setApiAccessChkBox(String apiAccessChkBox) {
		this.apiAccessChkBox = apiAccessChkBox;
	}

	public String getApiCheckBox() {
		return apiCheckBox;
	}

	public void setApiCheckBox(String apiCheckBox) {
		this.apiCheckBox = apiCheckBox;
	}

	public String getaPIAccessPassword() {
		return aPIAccessPassword;
	}

	public void setaPIAccessPassword(String aPIAccessPassword) {
		this.aPIAccessPassword = aPIAccessPassword;
	}

	public String getApiUserName() {
		return apiUserName;
	}

	public void setApiUserName(String apiUserName) {
		this.apiUserName = apiUserName;
	}

	public String getCnsPrjManagerId() {
		return cnsPrjManagerId;
	}

	public void setCnsPrjManagerId(String cnsPrjManagerId) {
		this.cnsPrjManagerId = cnsPrjManagerId;
	}

	public String getCnsPrjManagerName() {
		return cnsPrjManagerName;
	}

	public void setCnsPrjManagerName(String cnsPrjManagerName) {
		this.cnsPrjManagerName = cnsPrjManagerName;
	}

	public String getCnsPrjManagerPhone() {
		return cnsPrjManagerPhone;
	}

	public void setCnsPrjManagerPhone(String cnsPrjManagerPhone) {
		this.cnsPrjManagerPhone = cnsPrjManagerPhone;
	}

	public String getTeamLeadId() {
		return teamLeadId;
	}

	public void setTeamLeadId(String teamLeadId) {
		this.teamLeadId = teamLeadId;
	}

	public String getTeamLeadName() {
		return teamLeadName;
	}

	public void setTeamLeadName(String teamLeadName) {
		this.teamLeadName = teamLeadName;
	}

	public String getTeamLeadPhone() {
		return teamLeadPhone;
	}

	public void setTeamLeadPhone(String teamLeadPhone) {
		this.teamLeadPhone = teamLeadPhone;
	}

	private String teamLeadId;
	private String teamLeadName;
	private String teamLeadPhone;

	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}

	public String getJobSelectedStatus(int i) {
		return jobSelectedStatus[i];
	}

	public String getJobSelectedOwners(int i) {
		return jobSelectedOwners[i];
	}

	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getSiteCountryId() {
		return siteCountryId;
	}

	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}

	public Collection getTempList() {
		return tempList;
	}

	public void setTempList(Collection tempList) {
		this.tempList = tempList;
	}

	public String getRowsize() {
		return rowsize;
	}

	public void setRowsize(String rowsize) {
		this.rowsize = rowsize;
	}

	public String getJob_search_city() {
		return job_search_city;
	}

	public void setJob_search_city(String job_search_city) {
		this.job_search_city = job_search_city;
	}

	public String getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}

	public String getDateComplete() {
		return dateComplete;
	}

	public void setDateComplete(String dateComplete) {
		this.dateComplete = dateComplete;
	}

	public String getDateOffSite() {
		return dateOffSite;
	}

	public void setDateOffSite(String dateOffSite) {
		this.dateOffSite = dateOffSite;
	}

	public String getDateOnSite() {
		return dateOnSite;
	}

	public void setDateOnSite(String dateOnSite) {
		this.dateOnSite = dateOnSite;
	}

	public String getJob_search_date_from() {
		return job_search_date_from;
	}

	public void setJob_search_date_from(String job_search_date_from) {
		this.job_search_date_from = job_search_date_from;
	}

	public String getJob_search_date_to() {
		return job_search_date_to;
	}

	public void setJob_search_date_to(String job_search_date_to) {
		this.job_search_date_to = job_search_date_to;
	}

	public String getJob_search_name() {
		return job_search_name;
	}

	public void setJob_search_name(String job_search_name) {
		this.job_search_name = job_search_name;
	}

	public String getJob_search_site_number() {
		return job_search_site_number;
	}

	public void setJob_search_site_number(String job_search_site_number) {
		this.job_search_site_number = job_search_site_number;
	}

	public String getJob_search_state() {
		return job_search_state;
	}

	public void setJob_search_state(String job_search_state) {
		this.job_search_state = job_search_state;
	}

	public String getActualvgpm() {
		return actualvgpm;
	}

	public void setActualvgpm(String actualvgpm) {
		this.actualvgpm = actualvgpm;
	}

	public String getPmvipp() {
		return pmvipp;
	}

	public void setPmvipp(String pmvipp) {
		this.pmvipp = pmvipp;
	}

	public String getJob_netinstallation_date() {
		return job_netinstallation_date;
	}

	public void setJob_netinstallation_date(String job_netinstallation_date) {
		this.job_netinstallation_date = job_netinstallation_date;
	}

	public String getJob_netinstallation_time() {
		return job_netinstallation_time;
	}

	public void setJob_netinstallation_time(String job_netinstallation_time) {
		this.job_netinstallation_time = job_netinstallation_time;
	}

	public Collection getInstallationlist() {
		return installationlist;
	}

	public void setInstallationlist(Collection installationlist) {
		this.installationlist = installationlist;
	}

	public String getPartnerpocname() {
		return partnerpocname;
	}

	public void setPartnerpocname(String partnerpocname) {
		this.partnerpocname = partnerpocname;
	}

	public String getPartnerpocphone() {
		return partnerpocphone;
	}

	public void setPartnerpocphone(String partnerpocphone) {
		this.partnerpocphone = partnerpocphone;
	}

	public String getPoid() {
		return poid;
	}

	public void setPoid(String poid) {
		this.poid = poid;
	}

	public String getPonumber() {
		return ponumber;
	}

	public void setPonumber(String ponumber) {
		this.ponumber = ponumber;
	}

	public String getAppendixbillingpoc() {
		return appendixbillingpoc;
	}

	public void setAppendixbillingpoc(String appendixbillingpoc) {
		this.appendixbillingpoc = appendixbillingpoc;
	}

	public String getAppendixbusinesspoc() {
		return appendixbusinesspoc;
	}

	public void setAppendixbusinesspoc(String appendixbusinesspoc) {
		this.appendixbusinesspoc = appendixbusinesspoc;
	}

	public String getAppendixcontractpoc() {
		return appendixcontractpoc;
	}

	public void setAppendixcontractpoc(String appendixcontractpoc) {
		this.appendixcontractpoc = appendixcontractpoc;
	}

	public String getAppendixid() {
		return appendixid;
	}

	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPlannedenddate() {
		return plannedenddate;
	}

	public void setPlannedenddate(String plannedenddate) {
		this.plannedenddate = plannedenddate;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEstimatedcost() {
		return estimatedcost;
	}

	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}

	public String getExtendedprice() {
		return extendedprice;
	}

	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}

	public String getProformamargin() {
		return proformamargin;
	}

	public void setProformamargin(String proformamargin) {
		this.proformamargin = proformamargin;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getJobaddendumname() {
		return jobaddendumname;
	}

	public void setJobaddendumname(String jobaddendumname) {
		this.jobaddendumname = jobaddendumname;
	}

	public String getPocemail() {
		return pocemail;
	}

	public void setPocemail(String pocemail) {
		this.pocemail = pocemail;
	}

	public String getPocfax() {
		return pocfax;
	}

	public void setPocfax(String pocfax) {
		this.pocfax = pocfax;
	}

	public String getPocname() {
		return pocname;
	}

	public void setPocname(String pocname) {
		this.pocname = pocname;
	}

	public String getPocphone() {
		return pocphone;
	}

	public void setPocphone(String pocphone) {
		this.pocphone = pocphone;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		jobname = null;
		siteid = null;
		sitename = null;
		sitepoc = null;
		sitephone = null;
		partnerid = null;
		partnername = null;
		fieldlabor = null;
		cnslabor = null;
		materials = null;
		freight = null;
		travel = null;
		total = null;
		// private String extendedprice = null;
		pmargin = null;
		listprice = null;
		estart = null;
		ecomplete = null;
		// private String status = null;
		type = null;

		appendixid = null;
		status = null;
		startdate = null;
		plannedenddate = null;
		appendixname = null;
		estimatedcost = null;
		extendedprice = null;
		proformamargin = null;
		jobid = null;
		jobaddendumname = null;

		pocname = null;
		pocphone = null;
		pocemail = null;
		pocfax = null;
		scope = null;

		actualcosts = null;
		actualvgp = null;
		sitesecondarypoc = null;
		sitesecondaryphone = null;
		sitesecondaryemail = null;
		siteprimaryphone = null;
		siteprimaryemail = null;
		job_closed = null;
		job_complete = null;
		job_inwork = null;
		job_total = null;
		job_earliest_start = null;
		job_earliest_end = null;
		job_latest_start = null;
		job_latest_end = null;

		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;

	}

	public String getCnslabor() {
		return cnslabor;
	}

	public void setCnslabor(String cnslabor) {
		this.cnslabor = cnslabor;
	}

	public String getEcomplete() {
		return ecomplete;
	}

	public void setEcomplete(String ecomplete) {
		this.ecomplete = ecomplete;
	}

	public String getEstart() {
		return estart;
	}

	public void setEstart(String estart) {
		this.estart = estart;
	}

	public String getFieldlabor() {
		return fieldlabor;
	}

	public void setFieldlabor(String fieldlabor) {
		this.fieldlabor = fieldlabor;
	}

	public String getFreight() {
		return freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	public String getListprice() {
		return listprice;
	}

	public void setListprice(String listprice) {
		this.listprice = listprice;
	}

	public String getMaterials() {
		return materials;
	}

	public void setMaterials(String materials) {
		this.materials = materials;
	}

	public String getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}

	public String getPartnername() {
		return partnername;
	}

	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}

	public String getPmargin() {
		return pmargin;
	}

	public void setPmargin(String pmargin) {
		this.pmargin = pmargin;
	}

	public String getSiteid() {
		return siteid;
	}

	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getSitephone() {
		return sitephone;
	}

	public void setSitephone(String sitephone) {
		this.sitephone = sitephone;
	}

	public String getSitepoc() {
		return sitepoc;
	}

	public void setSitepoc(String sitepoc) {
		this.sitepoc = sitepoc;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getTravel() {
		return travel;
	}

	public void setTravel(String travel) {
		this.travel = travel;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getAppendixcnslabor() {
		return appendixcnslabor;
	}

	public void setAppendixcnslabor(String appendixcnslabor) {
		this.appendixcnslabor = appendixcnslabor;
	}

	public String getAppendixfieldlabor() {
		return appendixfieldlabor;
	}

	public void setAppendixfieldlabor(String appendixfieldlabor) {
		this.appendixfieldlabor = appendixfieldlabor;
	}

	public String getAppendixfreight() {
		return appendixfreight;
	}

	public void setAppendixfreight(String appendixfreight) {
		this.appendixfreight = appendixfreight;
	}

	public String getAppendixmaterials() {
		return appendixmaterials;
	}

	public void setAppendixmaterials(String appendixmaterials) {
		this.appendixmaterials = appendixmaterials;
	}

	public String getAppendixtravel() {
		return appendixtravel;
	}

	public void setAppendixtravel(String appendixtravel) {
		this.appendixtravel = appendixtravel;
	}

	public String getSiteno() {
		return siteno;
	}

	public void setSiteno(String siteno) {
		this.siteno = siteno;
	}

	public String getSitestate() {
		return sitestate;
	}

	public void setSitestate(String sitestate) {
		this.sitestate = sitestate;
	}

	public String getSitezipcode() {
		return sitezipcode;
	}

	public void setSitezipcode(String sitezipcode) {
		this.sitezipcode = sitezipcode;
	}

	public String getSiteaddress() {
		return siteaddress;
	}

	public void setSiteaddress(String siteaddress) {
		this.siteaddress = siteaddress;
	}

	public String getSitecity() {
		return sitecity;
	}

	public void setSitecity(String sitecity) {
		this.sitecity = sitecity;
	}

	public String getJobtype() {
		return jobtype;
	}

	public void setJobtype(String jobtype) {
		this.jobtype = jobtype;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getAppendixcnspoc() {
		return appendixcnspoc;
	}

	public void setAppendixcnspoc(String appendixcnspoc) {
		this.appendixcnspoc = appendixcnspoc;
	}

	public String getAppendixbillingpocphone() {
		return appendixbillingpocphone;
	}

	public void setAppendixbillingpocphone(String appendixbillingpocphone) {
		this.appendixbillingpocphone = appendixbillingpocphone;
	}

	public String getAppendixbusinesspocphone() {
		return appendixbusinesspocphone;
	}

	public void setAppendixbusinesspocphone(String appendixbusinesspocphone) {
		this.appendixbusinesspocphone = appendixbusinesspocphone;
	}

	public String getAppendixcnspocphone() {
		return appendixcnspocphone;
	}

	public void setAppendixcnspocphone(String appendixcnspocphone) {
		this.appendixcnspocphone = appendixcnspocphone;
	}

	public String getAppendixcontractpocphone() {
		return appendixcontractpocphone;
	}

	public void setAppendixcontractpocphone(String appendixcontractpocphone) {
		this.appendixcontractpocphone = appendixcontractpocphone;
	}

	public String getCnsprojectmgr() {
		return cnsprojectmgr;
	}

	public void setCnsprojectmgr(String cnsprojectmgr) {
		this.cnsprojectmgr = cnsprojectmgr;
	}

	public String getCnsprojectmgrphone() {
		return cnsprojectmgrphone;
	}

	public void setCnsprojectmgrphone(String cnsprojectmgrphone) {
		this.cnsprojectmgrphone = cnsprojectmgrphone;
	}

	public String getCustprojectmgr() {
		return custprojectmgr;
	}

	public void setCustprojectmgr(String custprojectmgr) {
		this.custprojectmgr = custprojectmgr;
	}

	public String getCustprojectmgrphone() {
		return custprojectmgrphone;
	}

	public void setCustprojectmgrphone(String custprojectmgrphone) {
		this.custprojectmgrphone = custprojectmgrphone;
	}

	public String getCustinfochangedate() {
		return custinfochangedate;
	}

	public void setCustinfochangedate(String custinfochangedate) {
		this.custinfochangedate = custinfochangedate;
	}

	public String getCustinfochangedby() {
		return custinfochangedby;
	}

	public void setCustinfochangedby(String custinfochangedby) {
		this.custinfochangedby = custinfochangedby;
	}

	public String getCustinfodata1() {
		return custinfodata1;
	}

	public void setCustinfodata1(String custinfodata1) {
		this.custinfodata1 = custinfodata1;
	}

	public String getCustinfodata2() {
		return custinfodata2;
	}

	public void setCustinfodata2(String custinfodata2) {
		this.custinfodata2 = custinfodata2;
	}

	public String getCustinfodata3() {
		return custinfodata3;
	}

	public void setCustinfodata3(String custinfodata3) {
		this.custinfodata3 = custinfodata3;
	}

	public String getCustinfovalue1() {
		return custinfovalue1;
	}

	public void setCustinfovalue1(String custinfovalue1) {
		this.custinfovalue1 = custinfovalue1;
	}

	public String getCustinfovalue2() {
		return custinfovalue2;
	}

	public void setCustinfovalue2(String custinfovalue2) {
		this.custinfovalue2 = custinfovalue2;
	}

	public String getCustinfovalue3() {
		return custinfovalue3;
	}

	public void setCustinfovalue3(String custinfovalue3) {
		this.custinfovalue3 = custinfovalue3;
	}

	public String getStatusdesc() {
		return statusdesc;
	}

	public void setStatusdesc(String statusdesc) {
		this.statusdesc = statusdesc;
	}

	public String getAppendix_type() {
		return appendix_type;
	}

	public void setAppendix_type(String appendix_type) {
		this.appendix_type = appendix_type;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getEcompletetime() {
		return ecompletetime;
	}

	public void setEcompletetime(String ecompletetime) {
		this.ecompletetime = ecompletetime;
	}

	public String getEstarttime() {
		return estarttime;
	}

	public void setEstarttime(String estarttime) {
		this.estarttime = estarttime;
	}

	public String getPlannedenddatetime() {
		return plannedenddatetime;
	}

	public void setPlannedenddatetime(String plannedenddatetime) {
		this.plannedenddatetime = plannedenddatetime;
	}

	public String getStartdatetime() {
		return startdatetime;
	}

	public void setStartdatetime(String startdatetime) {
		this.startdatetime = startdatetime;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getJobupdatedate() {
		return jobupdatedate;
	}

	public void setJobupdatedate(String jobupdatedate) {
		this.jobupdatedate = jobupdatedate;
	}

	public String getJobupdateddby() {
		return jobupdateddby;
	}

	public void setJobupdateddby(String jobupdateddby) {
		this.jobupdateddby = jobupdateddby;
	}

	public String getJobupdatetime() {
		return jobupdatetime;
	}

	public void setJobupdatetime(String jobupdatetime) {
		this.jobupdatetime = jobupdatetime;
	}

	public Collection getPowolist() {
		return powolist;
	}

	public void setPowolist(Collection powolist) {
		this.powolist = powolist;
	}

	public String getActualcosts() {
		return actualcosts;
	}

	public void setActualcosts(String actualcosts) {
		this.actualcosts = actualcosts;
	}

	public String getActualvgp() {
		return actualvgp;
	}

	public void setActualvgp(String actualvgp) {
		this.actualvgp = actualvgp;
	}

	public String getSitesecondarypoc() {
		return sitesecondarypoc;
	}

	public void setSitesecondarypoc(String sitesecondarypoc) {
		this.sitesecondarypoc = sitesecondarypoc;
	}

	public String getSiteprimaryemail() {
		return siteprimaryemail;
	}

	public void setSiteprimaryemail(String siteprimaryemail) {
		this.siteprimaryemail = siteprimaryemail;
	}

	public String getSiteprimaryphone() {
		return siteprimaryphone;
	}

	public void setSiteprimaryphone(String siteprimaryphone) {
		this.siteprimaryphone = siteprimaryphone;
	}

	public String getSitesecondaryemail() {
		return sitesecondaryemail;
	}

	public void setSitesecondaryemail(String sitesecondaryemail) {
		this.sitesecondaryemail = sitesecondaryemail;
	}

	public String getSitesecondaryphone() {
		return sitesecondaryphone;
	}

	public void setSitesecondaryphone(String sitesecondaryphone) {
		this.sitesecondaryphone = sitesecondaryphone;
	}

	public String getInv_id() {
		return inv_id;
	}

	public void setInv_id(String inv_id) {
		this.inv_id = inv_id;
	}

	public String getInv_rdofilter() {
		return inv_rdofilter;
	}

	public void setInv_rdofilter(String inv_rdofilter) {
		this.inv_rdofilter = inv_rdofilter;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getInv_type() {
		return inv_type;
	}

	public void setInv_type(String inv_type) {
		this.inv_type = inv_type;
	}

	public String getDocumentpresent() {
		return documentpresent;
	}

	public void setDocumentpresent(String documentpresent) {
		this.documentpresent = documentpresent;
	}

	public String getJobcreatedby() {
		return jobcreatedby;
	}

	public void setJobcreatedby(String jobcreatedby) {
		this.jobcreatedby = jobcreatedby;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getCustref() {
		return custref;
	}

	public void setCustref(String custref) {
		this.custref = custref;
	}

	public String getJob_closed() {
		return job_closed;
	}

	public void setJob_closed(String job_closed) {
		this.job_closed = job_closed;
	}

	public String getJob_complete() {
		return job_complete;
	}

	public void setJob_complete(String job_complete) {
		this.job_complete = job_complete;
	}

	public String getJob_inwork() {
		return job_inwork;
	}

	public void setJob_inwork(String job_inwork) {
		this.job_inwork = job_inwork;
	}

	public String getJob_total() {
		return job_total;
	}

	public void setJob_total(String job_total) {
		this.job_total = job_total;
	}

	public String getJob_earliest_end() {
		return job_earliest_end;
	}

	public void setJob_earliest_end(String job_earliest_end) {
		this.job_earliest_end = job_earliest_end;
	}

	public String getJob_earliest_start() {
		return job_earliest_start;
	}

	public void setJob_earliest_start(String job_earliest_start) {
		this.job_earliest_start = job_earliest_start;
	}

	public String getJob_latest_end() {
		return job_latest_end;
	}

	public void setJob_latest_end(String job_latest_end) {
		this.job_latest_end = job_latest_end;
	}

	public String getJob_latest_start() {
		return job_latest_start;
	}

	public void setJob_latest_start(String job_latest_start) {
		this.job_latest_start = job_latest_start;
	}

	public String getCnsprojectmgrId() {
		return cnsprojectmgrId;
	}

	public void setCnsprojectmgrId(String cnsprojectmgrId) {
		this.cnsprojectmgrId = cnsprojectmgrId;
	}

	public String getFrom_date() {
		return from_date;
	}

	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}

	public String getInvoice_Flag() {
		return invoice_Flag;
	}

	public void setInvoice_Flag(String invoice_Flag) {
		this.invoice_Flag = invoice_Flag;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getPowo_number() {
		return powo_number;
	}

	public void setPowo_number(String powo_number) {
		this.powo_number = powo_number;
	}

	public String getTo_date() {
		return to_date;
	}

	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}

	public String getEndcust() {
		return endcust;
	}

	public void setEndcust(String endcust) {
		this.endcust = endcust;
	}

	public String getJob_search() {
		return job_search;
	}

	public void setJob_search(String job_search) {
		this.job_search = job_search;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getJob_search_owner() {
		return job_search_owner;
	}

	public void setJob_search_owner(String job_search_owner) {
		this.job_search_owner = job_search_owner;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String getSiteCountryName() {
		return siteCountryName;
	}

	public void setSiteCountryName(String siteCountryName) {
		this.siteCountryName = siteCountryName;
	}

	public String getRseCount() {
		return rseCount;
	}

	public void setRseCount(String rseCount) {
		this.rseCount = rseCount;
	}

	public String getPowoType() {
		return powoType;
	}

	public void setPowoType(String powoType) {
		this.powoType = powoType;
	}

	/**
	 * @return the editContactList
	 */
	public boolean isEditContactList() {
		return editContactList;
	}

	/**
	 * @param editContactList
	 *            the editContactList to set
	 */
	public void setEditContactList(boolean editContactList) {
		this.editContactList = editContactList;
	}

	private boolean editContactList;
}
