package com.mind.formbean.PRM;

public class UpliftValue {

	String upliftFactorName = null;
	String upliftFactorValue = null;
	String upliftFactorCost = null;
	
	public String getUpliftFactorCost() {
		return upliftFactorCost;
	}
	public void setUpliftFactorCost(String upliftFactorCost) {
		this.upliftFactorCost = upliftFactorCost;
	}
	public String getUpliftFactorName() {
		return upliftFactorName;
	}
	public void setUpliftFactorName(String upliftFactorName) {
		this.upliftFactorName = upliftFactorName;
	}
	public String getUpliftFactorValue() {
		return upliftFactorValue;
	}
	public void setUpliftFactorValue(String upliftFactorValue) {
		this.upliftFactorValue = upliftFactorValue;
	}
	
}
