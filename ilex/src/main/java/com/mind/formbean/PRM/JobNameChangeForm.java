package com.mind.formbean.PRM;

import org.apache.struts.action.ActionForm;

public class JobNameChangeForm extends ActionForm {
	
	
	private String appendixId = null;
	private String appendixName = null;
	private String msaId = null;
	private String ownerId = null;
	private String viewjobtype = null;
	private String jobName = null; 
	private String jobId = null; 
	private String ref = null;
	private String add = null; 
	private String cancel = null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}
	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}
	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}
	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}	

	
}
