package com.mind.formbean.PRM;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class UploadCSVForm extends ActionForm{
	
	private FormFile sitefilename = null;
	private String upload = null;
	//private String append = null;
	//private String appendixid = null;
	private String msaid = null;
	private String msaname=null;
	//private String appendixname=null;
	private String endCustomer=null;
	
	// Following field to fet format of CSV file
	private String fieldName=null;
	private String dataType=null;
	private String fieldNameLength=null;
	private String fieldNameMandatory=null;
	private String viewjobtype=null;
	private String ownerId=null;
	
	//For Back of Site Management.
	private String siteSearchName=null;
	private String siteSearchNumber=null;
	private String siteSearchCity=null;
	private String siteSearchState=null;
	private String siteSearchZip=null;
	private String siteSearchCountry=null;
	private String siteSearchGroup=null;
	private String siteSearchEndCustomer=null;
	private String siteSearchTimeZone=null;
	private String siteSearchStatus=null;
	
	private String siteNameForColumn="on";
	private String siteNumberForColumn="on";
	private String siteAddressForColumn=null;
	private String siteCityForColumn=null;
	private String siteStateForColumn=null;
	private String siteZipForColumn=null;
	private String siteCountryForColumn=null;
	
	private String lines_per_page= null;       
	private String current_page_no = null;     
	private String total_page_size = null;                                
	private String org_page_no = null;
	private String org_lines_per_page = null;
	private String fromType= null;
	private String pageType= null; 
	
	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public String getCurrent_page_no() {
		return current_page_no;
	}

	public void setCurrent_page_no(String current_page_no) {
		this.current_page_no = current_page_no;
	}

	public String getLines_per_page() {
		return lines_per_page;
	}

	public void setLines_per_page(String lines_per_page) {
		this.lines_per_page = lines_per_page;
	}

	public String getOrg_lines_per_page() {
		return org_lines_per_page;
	}

	public void setOrg_lines_per_page(String org_lines_per_page) {
		this.org_lines_per_page = org_lines_per_page;
	}

	public String getOrg_page_no() {
		return org_page_no;
	}

	public void setOrg_page_no(String org_page_no) {
		this.org_page_no = org_page_no;
	}

	public String getSiteAddressForColumn() {
		return siteAddressForColumn;
	}

	public void setSiteAddressForColumn(String siteAddressForColumn) {
		this.siteAddressForColumn = siteAddressForColumn;
	}

	public String getSiteCityForColumn() {
		return siteCityForColumn;
	}

	public void setSiteCityForColumn(String siteCityForColumn) {
		this.siteCityForColumn = siteCityForColumn;
	}

	public String getSiteCountryForColumn() {
		return siteCountryForColumn;
	}

	public void setSiteCountryForColumn(String siteCountryForColumn) {
		this.siteCountryForColumn = siteCountryForColumn;
	}

	public String getSiteNameForColumn() {
		return siteNameForColumn;
	}

	public void setSiteNameForColumn(String siteNameForColumn) {
		this.siteNameForColumn = siteNameForColumn;
	}

	public String getSiteNumberForColumn() {
		return siteNumberForColumn;
	}

	public void setSiteNumberForColumn(String siteNumberForColumn) {
		this.siteNumberForColumn = siteNumberForColumn;
	}

	public String getSiteSearchCity() {
		return siteSearchCity;
	}

	public void setSiteSearchCity(String siteSearchCity) {
		this.siteSearchCity = siteSearchCity;
	}

	public String getSiteSearchCountry() {
		return siteSearchCountry;
	}

	public void setSiteSearchCountry(String siteSearchCountry) {
		this.siteSearchCountry = siteSearchCountry;
	}

	public String getSiteSearchEndCustomer() {
		return siteSearchEndCustomer;
	}

	public void setSiteSearchEndCustomer(String siteSearchEndCustomer) {
		this.siteSearchEndCustomer = siteSearchEndCustomer;
	}

	public String getSiteSearchGroup() {
		return siteSearchGroup;
	}

	public void setSiteSearchGroup(String siteSearchGroup) {
		this.siteSearchGroup = siteSearchGroup;
	}

	public String getSiteSearchName() {
		return siteSearchName;
	}

	public void setSiteSearchName(String siteSearchName) {
		this.siteSearchName = siteSearchName;
	}

	public String getSiteSearchNumber() {
		return siteSearchNumber;
	}

	public void setSiteSearchNumber(String siteSearchNumber) {
		this.siteSearchNumber = siteSearchNumber;
	}

	public String getSiteSearchState() {
		return siteSearchState;
	}

	public void setSiteSearchState(String siteSearchState) {
		this.siteSearchState = siteSearchState;
	}

	public String getSiteSearchTimeZone() {
		return siteSearchTimeZone;
	}

	public void setSiteSearchTimeZone(String siteSearchTimeZone) {
		this.siteSearchTimeZone = siteSearchTimeZone;
	}

	public String getSiteSearchZip() {
		return siteSearchZip;
	}

	public void setSiteSearchZip(String siteSearchZip) {
		this.siteSearchZip = siteSearchZip;
	}

	public String getSiteStateForColumn() {
		return siteStateForColumn;
	}

	public void setSiteStateForColumn(String siteStateForColumn) {
		this.siteStateForColumn = siteStateForColumn;
	}

	public String getSiteZipForColumn() {
		return siteZipForColumn;
	}

	public void setSiteZipForColumn(String siteZipForColumn) {
		this.siteZipForColumn = siteZipForColumn;
	}

	public String getTotal_page_size() {
		return total_page_size;
	}

	public void setTotal_page_size(String total_page_size) {
		this.total_page_size = total_page_size;
	}

	public FormFile getSitefilename() {
		return sitefilename;
	}

	public void setSitefilename(FormFile sitefilename) {
		this.sitefilename = sitefilename;
	}

	public String getUpload() {
		return upload;
	}

	public void setUpload(String upload) {
		this.upload = upload;
	}
	/*
	public String getAppend() {
		return append;
	}

	public void setAppend( String append ) {
		this.append = append;
	}
	
	public String getAppendixid() {
		return appendixid;
	}

	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	} */

	public String getMsaid() {
		return msaid;
	}

	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldNameLength() {
		return fieldNameLength;
	}

	public void setFieldNameLength(String fieldNameLength) {
		this.fieldNameLength = fieldNameLength;
	}

	public String getFieldNameMandatory() {
		return fieldNameMandatory;
	}

	public void setFieldNameMandatory(String fieldNameMandatory) {
		this.fieldNameMandatory = fieldNameMandatory;
	}
	/*
	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}*/

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getEndCustomer() {
		return endCustomer;
	}

	public void setEndCustomer(String endCustomer) {
		this.endCustomer = endCustomer;
	}

	public String getSiteSearchStatus() {
		return siteSearchStatus;
	}

	public void setSiteSearchStatus(String siteSearchStatus) {
		this.siteSearchStatus = siteSearchStatus;
	}
	
}
