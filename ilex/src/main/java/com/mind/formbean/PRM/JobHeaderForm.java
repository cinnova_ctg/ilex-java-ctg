package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class JobHeaderForm extends ActionForm
{
	//private String Appendixid = null;
	//private String jobid = null;
	private String activityid = null;
	private String activityname = null;
	private String type = null;
	private String quantity = null;
	private String partnerid = null;
	private String partnername= null;
	private String contractlabor = null;
	private String cnslabor= null;
	private String materials = null;
	private String freight = null;
	private String travel = null;
	private String total = null;
	//private String extendedprice = null;
	private String pmargin = null;
	private String listprice = null;
	private String estart = null;
	private String ecomplete = null;
	//private String status = null;
	private String activityaddendumname = null;
	
	
	private String appendixid = null;
	private String appendixname = null;
	private String jobid = null;
	private String jobname = null;
	private String status = null;
	private String startdate = null;
	private String plannedenddate = null;
	private String materialscost = null;
	private String cnslaborcost = null;
	private String fieldlaborcost = null;
	private String freightcost = null;
	private String travelcost = null;
	private String estimatedcost = null;
	private String extendedsubtotal = null;
	private String extendedprice = null;
	private String proformamargin = null;
	private String authenticate="";
	private String partnerassigned=null;
	
	public String getPartnerassigned() {
		return partnerassigned;
	}
	public void setPartnerassigned(String partnerassigned) {
		this.partnerassigned = partnerassigned;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getPlannedenddate() {
		return plannedenddate;
	}
	public void setPlannedenddate(String plannedenddate) {
		this.plannedenddate = plannedenddate;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	
	public String getCnslaborcost() {
		return cnslaborcost;
	}
	public void setCnslaborcost(String cnslaborcost) {
		this.cnslaborcost = cnslaborcost;
	}
	
	public String getExtendedsubtotal() {
		return extendedsubtotal;
	}
	public void setExtendedsubtotal(String extendedsubtotal) {
		this.extendedsubtotal = extendedsubtotal;
	}
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}
	public String getFieldlaborcost() {
		return fieldlaborcost;
	}
	public void setFieldlaborcost(String fieldlaborcost) {
		this.fieldlaborcost = fieldlaborcost;
	}
	public String getFreightcost() {
		return freightcost;
	}
	public void setFreightcost(String freightcost) {
		this.freightcost = freightcost;
	}
	public String getMaterialscost() {
		return materialscost;
	}
	public void setMaterialscost(String materialscost) {
		this.materialscost = materialscost;
	}
	
	public String getEstimatedcost() {
		return estimatedcost;
	}
	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin(String proformamargin) {
		this.proformamargin = proformamargin;
	}
	public String getTravelcost() {
		return travelcost;
	}
	public void setTravelcost(String travelcost) {
		this.travelcost = travelcost;
	}
	public String getActivityaddendumname() {
		return activityaddendumname;
	}
	public void setActivityaddendumname(String activityaddendumname) {
		this.activityaddendumname = activityaddendumname;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		
		activityid = null;
		activityname = null;
		type = null;
		quantity = null;
		partnerid = null;
		partnername= null;
		contractlabor = null;
		cnslabor= null;
		materials = null;
		freight = null;
		travel = null;
		total = null;
		pmargin = null;
		listprice = null;
		estart = null;
		ecomplete = null;
		activityaddendumname = null;
		
		
		appendixid = null;
		appendixname = null;
		jobid = null;
		jobname = null;
		status = null;
		startdate = null;
		plannedenddate = null;
		materialscost = null;
		cnslaborcost = null;
		fieldlaborcost = null;
		freightcost = null;
		travelcost = null;
		estimatedcost = null;
		extendedsubtotal = null;
		extendedprice = null;
		proformamargin = null;
	}
	public String getActivityid() {
		return activityid;
	}
	public void setActivityid(String activityid) {
		this.activityid = activityid;
	}
	public String getActivityname() {
		return activityname;
	}
	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}
	public String getCnslabor() {
		return cnslabor;
	}
	public void setCnslabor(String cnslabor) {
		this.cnslabor = cnslabor;
	}
	public String getContractlabor() {
		return contractlabor;
	}
	public void setContractlabor(String contractlabor) {
		this.contractlabor = contractlabor;
	}
	public String getEcomplete() {
		return ecomplete;
	}
	public void setEcomplete(String ecomplete) {
		this.ecomplete = ecomplete;
	}
	public String getEstart() {
		return estart;
	}
	public void setEstart(String estart) {
		this.estart = estart;
	}
	public String getFreight() {
		return freight;
	}
	public void setFreight(String freight) {
		this.freight = freight;
	}
	public String getListprice() {
		return listprice;
	}
	public void setListprice(String listprice) {
		this.listprice = listprice;
	}
	public String getMaterials() {
		return materials;
	}
	public void setMaterials(String materials) {
		this.materials = materials;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	public String getPartnername() {
		return partnername;
	}
	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}
	public String getPmargin() {
		return pmargin;
	}
	public void setPmargin(String pmargin) {
		this.pmargin = pmargin;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTravel() {
		return travel;
	}
	public void setTravel(String travel) {
		this.travel = travel;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	

}
