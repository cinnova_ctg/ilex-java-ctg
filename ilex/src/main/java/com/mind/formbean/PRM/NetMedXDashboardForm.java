package com.mind.formbean.PRM;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class NetMedXDashboardForm extends ActionForm {

	private String from_type = null;

	private String lo_ot_name = null;
	private String jobid = null;
	private String jobname = null;
	private String appendix_id = null;
	private String appendixid = null;
	private String appendixname = null;
	private String status = null;
	private String msaid = null;
	private String msaname = null;
	private String authenticate = "";
	private String viewjobtype = "";
	private String nettype = "";

	// Added by Amit to maintain owner id page per page
	private String ownerId = "";

	private String cnsprojectmgrId = null;
	private String cnsprojectmgr = null;
	private String cnsprojectmgrphone = null;
	private String custprojectmgr = null;
	private String custprojectmgrphone = null;
	private String appendixbusinesspoc = null;
	private String appendixbusinesspocphone = null;
	private String appendixcontractpoc = null;
	private String appendixcontractpocphone = null;
	private String appendixbillingpoc = null;
	private String appendixbillingpocphone = null;
	private String job_proformamarginvgp = null;
	private String sitescheduleplanned_startdate = null;
	private String sitescheduleplanned_startdatetime = null;
	private String newticket = null;

	private String ticket_id = null;
	private String ticket_number = null;
	private String standby = null;
	private String msp = null;
	private String helpDesk = null;
	private String site_id = null;
	private String site_name = null;
	private String site_number = null;
	private String site_address = null;
	private String site_city = null;
	private String site_zipcode = null;
	private String site_state = null;

	private String request_received_date = null;
	private String request_received_time = null;
	private String resource_level = null;
	private String hourly_cost = null;
	private String hourly_price = null;
	private String hourly_vgp = null;

	private String job_status = null;
	private String invoice_no = null;
	private String jobtype = null;

	private String job_extendedprice = null;
	private String job_estimatedcost = null;

	private String updatedby = null;
	private String updatedtime = null;
	private String updateddate = null;

	private String job_netrequestor = null;
	private String job_netproblemdesc = null;
	private String job_netspecialinstruct = null;
	private String job_netcategory = null;
	private String job_netcriticality = null;

	private Collection installationlist = null;
	private Collection powolist = null;

	private String dfs_curr_mnth_request = null;
	private String dfs_curr_mnth_hd = null;
	private String dfs_curr_mnth_comp = null;
	private String dfs_curr_mnth_inwork = null;
	private String dfs_curr_mnth_cancelled = null;
	private String dfs_curr_mnth_extprice = null;
	private String dfs_curr_mnth_actcost = null;
	private String dfs_curr_mnth_avgp = null;
	private String dfs_curr_mnth_avgcost = null;
	private String dfs_curr_mnth_avgcharge = null;
	private String dfs_curr_mnth_avgtimeonsite = null;
	private String dfs_curr_mnth_avgtimeonarrive = null;

	private String dfs_ttm_total_request = null;
	private String dfs_ttm_total_hd = null;
	private String dfs_ttm_total_comp = null;
	private String dfs_ttm_total_inwork = null;
	private String dfs_ttm_total_cancelled = null;
	private String dfs_ttm_total_extprice = null;
	private String dfs_ttm_total_actcost = null;
	private String dfs_ttm_total_avgp = null;
	private String dfs_ttm_total_avgcost = null;
	private String dfs_ttm_total_avgcharge = null;
	private String dfs_ttm_total_avgtimeonsite = null;
	private String dfs_ttm_total_avgtimeonarrive = null;

	private String dfs_avg_mnth_total_request = null;
	private String dfs_avg_mnth_total_hd = null;
	private String dfs_avg_mnth_total_comp = null;
	private String dfs_avg_mnth_total_inwork = null;
	private String dfs_avg_mnth_total_cancelled = null;
	private String dfs_avg_mnth_total_extprice = null;
	private String dfs_avg_mnth_total_actcost = null;
	private String dfs_avg_mnth_total_avgp = null;
	private String dfs_avg_mnth_total_avgcost = null;
	private String dfs_avg_mnth_total_avgcharge = null;
	private String dfs_avg_mnth_total_avgtimeonsite = null;
	private String dfs_avg_mnth_total_avgtimeonarrive = null;

	// Completed Dispatch Summary
	private String cd_msa_name = null;
	private String cd_cm_count = null;
	private String cd_ttm_count = null;
	private String cd_avg_count = null;

	// Dispatch Trend Analysis Summary
	private String da_category_name = null;
	private String da_category_count = null;
	private String da_state_name = null;
	private String da_state_count = null;
	private String da_requestor_name = null;
	private String da_requestor_count = null;
	private String da_criticality_name = null;
	private String da_criticality_count = null;

	// for yellow folder show
	private String documentpresent = null;
	private String ticketcreatedby = null;

	private String custref = null;
	private String schedule_position = null;
	private int schedule_total = 0;

	private String appendixcnspoc = null;
	private String appendixcnspocphone = null;
	private String appendix_type = null;
	private String statusdesc = null;
	private String pending_tickets = null;
	private String toBeSchedule = null;
	private String scheduled = null;
	private String overdue = null;

	private String job_search_name = null;
	private String job_search_site_number = null;
	private String job_search_city = null;
	private String job_search_state = null;
	private String job_search_date_from = null;
	private String job_search_date_to = null;
	private String dateOnSite = null;
	private String dateOffSite = null;
	private String dateComplete = null;
	private String dateClosed = null;
	private String job_search_owner = null;
	private String job_search = null;
	private String cancel = null;
	private Collection tempList = null;
	private String siteCountryId = null;
	private String siteCountryName = null;

	private String apiAccessBtn = null;
	private String apiAccessChkBox = null;
	private String apiCheckBox = null;
	private String aPIAccessPassword = null;
	private String apiUserName = null;

	// For View Selector
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;

	// private String checkAPIAccessPassword = null;

	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String getJobSelectedOwners(int i) {
		return jobSelectedOwners[i];
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}

	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobSelectedStatus(int i) {
		return jobSelectedStatus[i];
	}

	public String getCustref() {
		return custref;
	}

	public void setCustref(String custref) {
		this.custref = custref;
	}

	public Collection getInstallationlist() {
		return installationlist;
	}

	public void setInstallationlist(Collection installationlist) {
		this.installationlist = installationlist;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getAppendixid() {
		return appendixid;
	}

	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	public String getMsaid() {
		return msaid;
	}

	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getAppendixbillingpoc() {
		return appendixbillingpoc;
	}

	public void setAppendixbillingpoc(String appendixbillingpoc) {
		this.appendixbillingpoc = appendixbillingpoc;
	}

	public String getAppendixbillingpocphone() {
		return appendixbillingpocphone;
	}

	public void setAppendixbillingpocphone(String appendixbillingpocphone) {
		this.appendixbillingpocphone = appendixbillingpocphone;
	}

	public String getAppendixbusinesspoc() {
		return appendixbusinesspoc;
	}

	public void setAppendixbusinesspoc(String appendixbusinesspoc) {
		this.appendixbusinesspoc = appendixbusinesspoc;
	}

	public String getAppendixbusinesspocphone() {
		return appendixbusinesspocphone;
	}

	public void setAppendixbusinesspocphone(String appendixbusinesspocphone) {
		this.appendixbusinesspocphone = appendixbusinesspocphone;
	}

	public String getAppendixcontractpoc() {
		return appendixcontractpoc;
	}

	public void setAppendixcontractpoc(String appendixcontractpoc) {
		this.appendixcontractpoc = appendixcontractpoc;
	}

	public String getAppendixcontractpocphone() {
		return appendixcontractpocphone;
	}

	public void setAppendixcontractpocphone(String appendixcontractpocphone) {
		this.appendixcontractpocphone = appendixcontractpocphone;
	}

	public String getCnsprojectmgr() {
		return cnsprojectmgr;
	}

	public void setCnsprojectmgr(String cnsprojectmgr) {
		this.cnsprojectmgr = cnsprojectmgr;
	}

	public String getCnsprojectmgrphone() {
		return cnsprojectmgrphone;
	}

	public void setCnsprojectmgrphone(String cnsprojectmgrphone) {
		this.cnsprojectmgrphone = cnsprojectmgrphone;
	}

	public String getCustprojectmgr() {
		return custprojectmgr;
	}

	public void setCustprojectmgr(String custprojectmgr) {
		this.custprojectmgr = custprojectmgr;
	}

	public String getCustprojectmgrphone() {
		return custprojectmgrphone;
	}

	public void setCustprojectmgrphone(String custprojectmgrphone) {
		this.custprojectmgrphone = custprojectmgrphone;
	}

	public String getNewticket() {
		return newticket;
	}

	public void setNewticket(String newticket) {
		this.newticket = newticket;
	}

	public String getHourly_cost() {
		return hourly_cost;
	}

	public void setHourly_cost(String hourly_cost) {
		this.hourly_cost = hourly_cost;
	}

	public String getHourly_price() {
		return hourly_price;
	}

	public void setHourly_price(String hourly_price) {
		this.hourly_price = hourly_price;
	}

	public String getHourly_vgp() {
		return hourly_vgp;
	}

	public void setHourly_vgp(String hourly_vgp) {
		this.hourly_vgp = hourly_vgp;
	}

	public String getJob_estimatedcost() {
		return job_estimatedcost;
	}

	public void setJob_estimatedcost(String job_estimatedcost) {
		this.job_estimatedcost = job_estimatedcost;
	}

	public String getJob_extendedprice() {
		return job_extendedprice;
	}

	public void setJob_extendedprice(String job_extendedprice) {
		this.job_extendedprice = job_extendedprice;
	}

	public String getJob_netcategory() {
		return job_netcategory;
	}

	public void setJob_netcategory(String job_netcategory) {
		this.job_netcategory = job_netcategory;
	}

	public String getJob_netproblemdesc() {
		return job_netproblemdesc;
	}

	public void setJob_netproblemdesc(String job_netproblemdesc) {
		this.job_netproblemdesc = job_netproblemdesc;
	}

	public String getJob_netrequestor() {
		return job_netrequestor;
	}

	public void setJob_netrequestor(String job_netrequestor) {
		this.job_netrequestor = job_netrequestor;
	}

	public String getJob_netspecialinstruct() {
		return job_netspecialinstruct;
	}

	public void setJob_netspecialinstruct(String job_netspecialinstruct) {
		this.job_netspecialinstruct = job_netspecialinstruct;
	}

	public String getJob_status() {
		return job_status;
	}

	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getJobtype() {
		return jobtype;
	}

	public void setJobtype(String jobtype) {
		this.jobtype = jobtype;
	}

	public String getMsp() {
		return msp;
	}

	public void setMsp(String msp) {
		this.msp = msp;
	}

	public String getRequest_received_date() {
		return request_received_date;
	}

	public void setRequest_received_date(String request_received_date) {
		this.request_received_date = request_received_date;
	}

	public String getRequest_received_time() {
		return request_received_time;
	}

	public void setRequest_received_time(String request_received_time) {
		this.request_received_time = request_received_time;
	}

	public String getResource_level() {
		return resource_level;
	}

	public void setResource_level(String resource_level) {
		this.resource_level = resource_level;
	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	public String getSite_city() {
		return site_city;
	}

	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_number() {
		return site_number;
	}

	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}

	public String getSite_state() {
		return site_state;
	}

	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}

	public String getSite_zipcode() {
		return site_zipcode;
	}

	public void setSite_zipcode(String site_zipcode) {
		this.site_zipcode = site_zipcode;
	}

	public String getStandby() {
		return standby;
	}

	public void setStandby(String standby) {
		this.standby = standby;
	}

	public String getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}

	public String getTicket_number() {
		return ticket_number;
	}

	public void setTicket_number(String ticket_number) {
		this.ticket_number = ticket_number;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public String getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(String updateddate) {
		this.updateddate = updateddate;
	}

	public String getUpdatedtime() {
		return updatedtime;
	}

	public void setUpdatedtime(String updatedtime) {
		this.updatedtime = updatedtime;
	}

	public String getSite_id() {
		return site_id;
	}

	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}

	public String getJob_netcriticality() {
		return job_netcriticality;
	}

	public void setJob_netcriticality(String job_netcriticality) {
		this.job_netcriticality = job_netcriticality;
	}

	public Collection getPowolist() {
		return powolist;
	}

	public void setPowolist(Collection powolist) {
		this.powolist = powolist;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDa_category_count() {
		return da_category_count;
	}

	public void setDa_category_count(String da_category_count) {
		this.da_category_count = da_category_count;
	}

	public String getDa_category_name() {
		return da_category_name;
	}

	public void setDa_category_name(String da_category_name) {
		this.da_category_name = da_category_name;
	}

	public String getDa_criticality_count() {
		return da_criticality_count;
	}

	public void setDa_criticality_count(String da_criticality_count) {
		this.da_criticality_count = da_criticality_count;
	}

	public String getDa_criticality_name() {
		return da_criticality_name;
	}

	public void setDa_criticality_name(String da_criticality_name) {
		this.da_criticality_name = da_criticality_name;
	}

	public String getDa_requestor_count() {
		return da_requestor_count;
	}

	public void setDa_requestor_count(String da_requestor_count) {
		this.da_requestor_count = da_requestor_count;
	}

	public String getDa_requestor_name() {
		return da_requestor_name;
	}

	public void setDa_requestor_name(String da_requestor_name) {
		this.da_requestor_name = da_requestor_name;
	}

	public String getDa_state_count() {
		return da_state_count;
	}

	public void setDa_state_count(String da_state_count) {
		this.da_state_count = da_state_count;
	}

	public String getDa_state_name() {
		return da_state_name;
	}

	public void setDa_state_name(String da_state_name) {
		this.da_state_name = da_state_name;
	}

	public String getDfs_curr_mnth_actcost() {
		return dfs_curr_mnth_actcost;
	}

	public void setDfs_curr_mnth_actcost(String dfs_curr_mnth_actcost) {
		this.dfs_curr_mnth_actcost = dfs_curr_mnth_actcost;
	}

	public String getDfs_curr_mnth_avgp() {
		return dfs_curr_mnth_avgp;
	}

	public void setDfs_curr_mnth_avgp(String dfs_curr_mnth_avgp) {
		this.dfs_curr_mnth_avgp = dfs_curr_mnth_avgp;
	}

	public String getDfs_curr_mnth_comp() {
		return dfs_curr_mnth_comp;
	}

	public void setDfs_curr_mnth_comp(String dfs_curr_mnth_comp) {
		this.dfs_curr_mnth_comp = dfs_curr_mnth_comp;
	}

	public String getDfs_curr_mnth_extprice() {
		return dfs_curr_mnth_extprice;
	}

	public void setDfs_curr_mnth_extprice(String dfs_curr_mnth_extprice) {
		this.dfs_curr_mnth_extprice = dfs_curr_mnth_extprice;
	}

	public String getDfs_curr_mnth_inwork() {
		return dfs_curr_mnth_inwork;
	}

	public void setDfs_curr_mnth_inwork(String dfs_curr_mnth_inwork) {
		this.dfs_curr_mnth_inwork = dfs_curr_mnth_inwork;
	}

	public String getDfs_curr_mnth_avgcost() {
		return dfs_curr_mnth_avgcost;
	}

	public void setDfs_curr_mnth_avgcost(String dfs_curr_mnth_avgcost) {
		this.dfs_curr_mnth_avgcost = dfs_curr_mnth_avgcost;
	}

	public String getDfs_curr_mnth_avgtimeonarrive() {
		return dfs_curr_mnth_avgtimeonarrive;
	}

	public void setDfs_curr_mnth_avgtimeonarrive(
			String dfs_curr_mnth_avgtimeonarrive) {
		this.dfs_curr_mnth_avgtimeonarrive = dfs_curr_mnth_avgtimeonarrive;
	}

	public String getDfs_curr_mnth_avgtimeonsite() {
		return dfs_curr_mnth_avgtimeonsite;
	}

	public void setDfs_curr_mnth_avgtimeonsite(
			String dfs_curr_mnth_avgtimeonsite) {
		this.dfs_curr_mnth_avgtimeonsite = dfs_curr_mnth_avgtimeonsite;
	}

	public String getDfs_curr_mnth_hd() {
		return dfs_curr_mnth_hd;
	}

	public void setDfs_curr_mnth_hd(String dfs_curr_mnth_hd) {
		this.dfs_curr_mnth_hd = dfs_curr_mnth_hd;
	}

	public String getDfs_curr_mnth_request() {
		return dfs_curr_mnth_request;
	}

	public void setDfs_curr_mnth_request(String dfs_curr_mnth_request) {
		this.dfs_curr_mnth_request = dfs_curr_mnth_request;
	}

	public String getDfs_curr_mnth_avgcharge() {
		return dfs_curr_mnth_avgcharge;
	}

	public void setDfs_curr_mnth_avgcharge(String dfs_curr_mnth_avgcharge) {
		this.dfs_curr_mnth_avgcharge = dfs_curr_mnth_avgcharge;
	}

	public String getDfs_ttm_total_actcost() {
		return dfs_ttm_total_actcost;
	}

	public void setDfs_ttm_total_actcost(String dfs_ttm_total_actcost) {
		this.dfs_ttm_total_actcost = dfs_ttm_total_actcost;
	}

	public String getDfs_ttm_total_avgcharge() {
		return dfs_ttm_total_avgcharge;
	}

	public void setDfs_ttm_total_avgcharge(String dfs_ttm_total_avgcharge) {
		this.dfs_ttm_total_avgcharge = dfs_ttm_total_avgcharge;
	}

	public String getDfs_ttm_total_avgcost() {
		return dfs_ttm_total_avgcost;
	}

	public void setDfs_ttm_total_avgcost(String dfs_ttm_total_avgcost) {
		this.dfs_ttm_total_avgcost = dfs_ttm_total_avgcost;
	}

	public String getDfs_ttm_total_avgp() {
		return dfs_ttm_total_avgp;
	}

	public void setDfs_ttm_total_avgp(String dfs_ttm_total_avgp) {
		this.dfs_ttm_total_avgp = dfs_ttm_total_avgp;
	}

	public String getDfs_ttm_total_avgtimeonarrive() {
		return dfs_ttm_total_avgtimeonarrive;
	}

	public void setDfs_ttm_total_avgtimeonarrive(
			String dfs_ttm_total_avgtimeonarrive) {
		this.dfs_ttm_total_avgtimeonarrive = dfs_ttm_total_avgtimeonarrive;
	}

	public String getDfs_ttm_total_avgtimeonsite() {
		return dfs_ttm_total_avgtimeonsite;
	}

	public void setDfs_ttm_total_avgtimeonsite(
			String dfs_ttm_total_avgtimeonsite) {
		this.dfs_ttm_total_avgtimeonsite = dfs_ttm_total_avgtimeonsite;
	}

	public String getDfs_ttm_total_comp() {
		return dfs_ttm_total_comp;
	}

	public void setDfs_ttm_total_comp(String dfs_ttm_total_comp) {
		this.dfs_ttm_total_comp = dfs_ttm_total_comp;
	}

	public String getDfs_ttm_total_extprice() {
		return dfs_ttm_total_extprice;
	}

	public void setDfs_ttm_total_extprice(String dfs_ttm_total_extprice) {
		this.dfs_ttm_total_extprice = dfs_ttm_total_extprice;
	}

	public String getDfs_ttm_total_hd() {
		return dfs_ttm_total_hd;
	}

	public void setDfs_ttm_total_hd(String dfs_ttm_total_hd) {
		this.dfs_ttm_total_hd = dfs_ttm_total_hd;
	}

	public String getDfs_ttm_total_inwork() {
		return dfs_ttm_total_inwork;
	}

	public void setDfs_ttm_total_inwork(String dfs_ttm_total_inwork) {
		this.dfs_ttm_total_inwork = dfs_ttm_total_inwork;
	}

	public String getDfs_ttm_total_request() {
		return dfs_ttm_total_request;
	}

	public void setDfs_ttm_total_request(String dfs_ttm_total_request) {
		this.dfs_ttm_total_request = dfs_ttm_total_request;
	}

	public String getDfs_avg_mnth_total_actcost() {
		return dfs_avg_mnth_total_actcost;
	}

	public void setDfs_avg_mnth_total_actcost(String dfs_avg_mnth_total_actcost) {
		this.dfs_avg_mnth_total_actcost = dfs_avg_mnth_total_actcost;
	}

	public String getDfs_avg_mnth_total_avgcharge() {
		return dfs_avg_mnth_total_avgcharge;
	}

	public void setDfs_avg_mnth_total_avgcharge(
			String dfs_avg_mnth_total_avgcharge) {
		this.dfs_avg_mnth_total_avgcharge = dfs_avg_mnth_total_avgcharge;
	}

	public String getDfs_avg_mnth_total_avgcost() {
		return dfs_avg_mnth_total_avgcost;
	}

	public void setDfs_avg_mnth_total_avgcost(String dfs_avg_mnth_total_avgcost) {
		this.dfs_avg_mnth_total_avgcost = dfs_avg_mnth_total_avgcost;
	}

	public String getDfs_avg_mnth_total_avgp() {
		return dfs_avg_mnth_total_avgp;
	}

	public void setDfs_avg_mnth_total_avgp(String dfs_avg_mnth_total_avgp) {
		this.dfs_avg_mnth_total_avgp = dfs_avg_mnth_total_avgp;
	}

	public String getDfs_avg_mnth_total_avgtimeonarrive() {
		return dfs_avg_mnth_total_avgtimeonarrive;
	}

	public void setDfs_avg_mnth_total_avgtimeonarrive(
			String dfs_avg_mnth_total_avgtimeonarrive) {
		this.dfs_avg_mnth_total_avgtimeonarrive = dfs_avg_mnth_total_avgtimeonarrive;
	}

	public String getDfs_avg_mnth_total_avgtimeonsite() {
		return dfs_avg_mnth_total_avgtimeonsite;
	}

	public void setDfs_avg_mnth_total_avgtimeonsite(
			String dfs_avg_mnth_total_avgtimeonsite) {
		this.dfs_avg_mnth_total_avgtimeonsite = dfs_avg_mnth_total_avgtimeonsite;
	}

	public String getDfs_avg_mnth_total_comp() {
		return dfs_avg_mnth_total_comp;
	}

	public void setDfs_avg_mnth_total_comp(String dfs_avg_mnth_total_comp) {
		this.dfs_avg_mnth_total_comp = dfs_avg_mnth_total_comp;
	}

	public String getDfs_avg_mnth_total_extprice() {
		return dfs_avg_mnth_total_extprice;
	}

	public void setDfs_avg_mnth_total_extprice(
			String dfs_avg_mnth_total_extprice) {
		this.dfs_avg_mnth_total_extprice = dfs_avg_mnth_total_extprice;
	}

	public String getDfs_avg_mnth_total_hd() {
		return dfs_avg_mnth_total_hd;
	}

	public void setDfs_avg_mnth_total_hd(String dfs_avg_mnth_total_hd) {
		this.dfs_avg_mnth_total_hd = dfs_avg_mnth_total_hd;
	}

	public String getDfs_avg_mnth_total_inwork() {
		return dfs_avg_mnth_total_inwork;
	}

	public void setDfs_avg_mnth_total_inwork(String dfs_avg_mnth_total_inwork) {
		this.dfs_avg_mnth_total_inwork = dfs_avg_mnth_total_inwork;
	}

	public String getDfs_avg_mnth_total_request() {
		return dfs_avg_mnth_total_request;
	}

	public void setDfs_avg_mnth_total_request(String dfs_avg_mnth_total_request) {
		this.dfs_avg_mnth_total_request = dfs_avg_mnth_total_request;
	}

	public String getNettype() {
		return nettype;
	}

	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	public String getAppendix_id() {
		return appendix_id;
	}

	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}

	public String getLo_ot_name() {
		return lo_ot_name;
	}

	public void setLo_ot_name(String lo_ot_name) {
		this.lo_ot_name = lo_ot_name;
	}

	public String getFrom_type() {
		return from_type;
	}

	public void setFrom_type(String from_type) {
		this.from_type = from_type;
	}

	public String getDocumentpresent() {
		return documentpresent;
	}

	public void setDocumentpresent(String documentpresent) {
		this.documentpresent = documentpresent;
	}

	public String getTicketcreatedby() {
		return ticketcreatedby;
	}

	public void setTicketcreatedby(String ticketcreatedby) {
		this.ticketcreatedby = ticketcreatedby;
	}

	public String getCd_avg_count() {
		return cd_avg_count;
	}

	public void setCd_avg_count(String cd_avg_count) {
		this.cd_avg_count = cd_avg_count;
	}

	public String getCd_cm_count() {
		return cd_cm_count;
	}

	public void setCd_cm_count(String cd_cm_count) {
		this.cd_cm_count = cd_cm_count;
	}

	public String getCd_msa_name() {
		return cd_msa_name;
	}

	public void setCd_msa_name(String cd_msa_name) {
		this.cd_msa_name = cd_msa_name;
	}

	public String getCd_ttm_count() {
		return cd_ttm_count;
	}

	public void setCd_ttm_count(String cd_ttm_count) {
		this.cd_ttm_count = cd_ttm_count;
	}

	public String getHelpDesk() {
		return helpDesk;
	}

	public void setHelpDesk(String helpDesk) {
		this.helpDesk = helpDesk;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getDfs_curr_mnth_cancelled() {
		return dfs_curr_mnth_cancelled;
	}

	public void setDfs_curr_mnth_cancelled(String dfs_curr_mnth_cancelled) {
		this.dfs_curr_mnth_cancelled = dfs_curr_mnth_cancelled;
	}

	public String getDfs_ttm_total_cancelled() {
		return dfs_ttm_total_cancelled;
	}

	public void setDfs_ttm_total_cancelled(String dfs_ttm_total_cancelled) {
		this.dfs_ttm_total_cancelled = dfs_ttm_total_cancelled;
	}

	public String getDfs_avg_mnth_total_cancelled() {
		return dfs_avg_mnth_total_cancelled;
	}

	public void setDfs_avg_mnth_total_cancelled(
			String dfs_avg_mnth_total_cancelled) {
		this.dfs_avg_mnth_total_cancelled = dfs_avg_mnth_total_cancelled;
	}

	public String getSchedule_position() {
		return schedule_position;
	}

	public void setSchedule_position(String schedule_position) {
		this.schedule_position = schedule_position;
	}

	public int getSchedule_total() {
		return schedule_total;
	}

	public void setSchedule_total(int schedule_total) {
		this.schedule_total = schedule_total;
	}

	public String getAppendixcnspoc() {
		return appendixcnspoc;
	}

	public void setAppendixcnspoc(String appendixcnspoc) {
		this.appendixcnspoc = appendixcnspoc;
	}

	public String getAppendixcnspocphone() {
		return appendixcnspocphone;
	}

	public void setAppendixcnspocphone(String appendixcnspocphone) {
		this.appendixcnspocphone = appendixcnspocphone;
	}

	public String getAppendix_type() {
		return appendix_type;
	}

	public void setAppendix_type(String appendix_type) {
		this.appendix_type = appendix_type;
	}

	public String getStatusdesc() {
		return statusdesc;
	}

	public void setStatusdesc(String statusdesc) {
		this.statusdesc = statusdesc;
	}

	public String getPending_tickets() {
		return pending_tickets;
	}

	public void setPending_tickets(String pending_tickets) {
		this.pending_tickets = pending_tickets;
	}

	public String getOverdue() {
		return overdue;
	}

	public void setOverdue(String overdue) {
		this.overdue = overdue;
	}

	public String getScheduled() {
		return scheduled;
	}

	public void setScheduled(String scheduled) {
		this.scheduled = scheduled;
	}

	public String getToBeSchedule() {
		return toBeSchedule;
	}

	public void setToBeSchedule(String toBeSchedule) {
		this.toBeSchedule = toBeSchedule;
	}

	public String getCnsprojectmgrId() {
		return cnsprojectmgrId;
	}

	public void setCnsprojectmgrId(String cnsprojectmgrId) {
		this.cnsprojectmgrId = cnsprojectmgrId;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}

	public String getDateComplete() {
		return dateComplete;
	}

	public void setDateComplete(String dateComplete) {
		this.dateComplete = dateComplete;
	}

	public String getDateOffSite() {
		return dateOffSite;
	}

	public void setDateOffSite(String dateOffSite) {
		this.dateOffSite = dateOffSite;
	}

	public String getDateOnSite() {
		return dateOnSite;
	}

	public void setDateOnSite(String dateOnSite) {
		this.dateOnSite = dateOnSite;
	}

	public String getJob_search() {
		return job_search;
	}

	public void setJob_search(String job_search) {
		this.job_search = job_search;
	}

	public String getJob_search_city() {
		return job_search_city;
	}

	public void setJob_search_city(String job_search_city) {
		this.job_search_city = job_search_city;
	}

	public String getJob_search_date_from() {
		return job_search_date_from;
	}

	public void setJob_search_date_from(String job_search_date_from) {
		this.job_search_date_from = job_search_date_from;
	}

	public String getJob_search_date_to() {
		return job_search_date_to;
	}

	public void setJob_search_date_to(String job_search_date_to) {
		this.job_search_date_to = job_search_date_to;
	}

	public String getJob_search_name() {
		return job_search_name;
	}

	public void setJob_search_name(String job_search_name) {
		this.job_search_name = job_search_name;
	}

	public String getJob_search_owner() {
		return job_search_owner;
	}

	public void setJob_search_owner(String job_search_owner) {
		this.job_search_owner = job_search_owner;
	}

	public String getJob_search_site_number() {
		return job_search_site_number;
	}

	public void setJob_search_site_number(String job_search_site_number) {
		this.job_search_site_number = job_search_site_number;
	}

	public String getJob_search_state() {
		return job_search_state;
	}

	public void setJob_search_state(String job_search_state) {
		this.job_search_state = job_search_state;
	}

	public String getSiteCountryId() {
		return siteCountryId;
	}

	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}

	public Collection getTempList() {
		return tempList;
	}

	public void setTempList(Collection tempList) {
		this.tempList = tempList;
	}

	public String getJob_proformamarginvgp() {
		return job_proformamarginvgp;
	}

	public void setJob_proformamarginvgp(String job_proformamarginvgp) {
		this.job_proformamarginvgp = job_proformamarginvgp;
	}

	public String getSitescheduleplanned_startdate() {
		return sitescheduleplanned_startdate;
	}

	public void setSitescheduleplanned_startdate(
			String sitescheduleplanned_startdate) {
		this.sitescheduleplanned_startdate = sitescheduleplanned_startdate;
	}

	public String getSitescheduleplanned_startdatetime() {
		return sitescheduleplanned_startdatetime;
	}

	public void setSitescheduleplanned_startdatetime(
			String sitescheduleplanned_startdatetime) {
		this.sitescheduleplanned_startdatetime = sitescheduleplanned_startdatetime;
	}

	public String getSiteCountryName() {
		return siteCountryName;
	}

	public void setSiteCountryName(String siteCountryName) {
		this.siteCountryName = siteCountryName;
	}

	// project Manager and team lead
	private String cnsPrjManagerId;
	private String cnsPrjManagerName;
	private String cnsPrjManagerPhone;

	public String getCnsPrjManagerId() {
		return cnsPrjManagerId;
	}

	public void setCnsPrjManagerId(String cnsPrjManagerId) {
		this.cnsPrjManagerId = cnsPrjManagerId;
	}

	public String getCnsPrjManagerName() {
		return cnsPrjManagerName;
	}

	public void setCnsPrjManagerName(String cnsPrjManagerName) {
		this.cnsPrjManagerName = cnsPrjManagerName;
	}

	public String getCnsPrjManagerPhone() {
		return cnsPrjManagerPhone;
	}

	public void setCnsPrjManagerPhone(String cnsPrjManagerPhone) {
		this.cnsPrjManagerPhone = cnsPrjManagerPhone;
	}

	public String getTeamLeadId() {
		return teamLeadId;
	}

	public void setTeamLeadId(String teamLeadId) {
		this.teamLeadId = teamLeadId;
	}

	public String getTeamLeadName() {
		return teamLeadName;
	}

	public void setTeamLeadName(String teamLeadName) {
		this.teamLeadName = teamLeadName;
	}

	public String getTeamLeadPhone() {
		return teamLeadPhone;
	}

	public void setTeamLeadPhone(String teamLeadPhone) {
		this.teamLeadPhone = teamLeadPhone;
	}

	/**
	 * @return the editContactList
	 */
	public boolean isEditContactList() {
		return editContactList;
	}

	/**
	 * @param editContactList
	 *            the editContactList to set
	 */
	public void setEditContactList(boolean editContactList) {
		this.editContactList = editContactList;
	}

	public String getApiAccessBtn() {
		return apiAccessBtn;
	}

	public void setApiAccessBtn(String apiAccessBtn) {
		this.apiAccessBtn = apiAccessBtn;
	}

	public String getApiAccessChkBox() {
		return apiAccessChkBox;
	}

	public void setApiAccessChkBox(String apiAccessChkBox) {
		this.apiAccessChkBox = apiAccessChkBox;
	}

	public String getaPIAccessPassword() {
		return aPIAccessPassword;
	}

	public void setaPIAccessPassword(String aPIAccessPassword) {
		this.aPIAccessPassword = aPIAccessPassword;
	}

	// public String getCheckAPIAccessPassword() {
	// return checkAPIAccessPassword;
	// }
	//
	// public void setCheckAPIAccessPassword(String checkAPIAccessPassword) {
	// this.checkAPIAccessPassword = checkAPIAccessPassword;
	// }

	public String getApiUserName() {
		return apiUserName;
	}

	public void setApiUserName(String apiUserName) {
		this.apiUserName = apiUserName;
	}

	public String getApiCheckBox() {
		return apiCheckBox;
	}

	public void setApiCheckBox(String apiCheckBox) {
		this.apiCheckBox = apiCheckBox;
	}

	private String teamLeadId;
	private String teamLeadName;
	private String teamLeadPhone;
	private boolean editContactList;

}
