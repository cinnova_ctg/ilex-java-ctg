package com.mind.formbean.PRM;

import org.apache.struts.action.ActionForm;

public class TicketsPerMsaForm extends ActionForm{

	private String appendixid = null;
	private String msaname = null;
	private String ref = null;
	private String draft = null;
	private String inwork = null;
	private String inwork_extprice = null;
	private String inwork_actcost = null;
	private String inwork_actvgp = null;
	private String complete = null;
	private String complete_extprice = null;
	private String complete_actcost = null;
	private String complete_actvgp = null;
	private String closed = null;
	private String closed_extprice = null;
	private String closed_actcost = null;
	private String closed_actvgp = null;
	private String total = null;
	private String total_extprice = null;
	private String total_actcost = null;
	private String total_actvgp = null;
	private String monthValue = null;
	private String yearValue = null;
	
	private String monthName = null;
		
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getClosed() {
		return closed;
	}
	public void setClosed(String closed) {
		this.closed = closed;
	}
	public String getComplete() {
		return complete;
	}
	public void setComplete(String complete) {
		this.complete = complete;
	}
	public String getDraft() {
		return draft;
	}
	public void setDraft(String draft) {
		this.draft = draft;
	}
	public String getInwork() {
		return inwork;
	}
	public void setInwork(String inwork) {
		this.inwork = inwork;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getClosed_actcost() {
		return closed_actcost;
	}
	public void setClosed_actcost(String closed_actcost) {
		this.closed_actcost = closed_actcost;
	}
	public String getClosed_actvgp() {
		return closed_actvgp;
	}
	public void setClosed_actvgp(String closed_actvgp) {
		this.closed_actvgp = closed_actvgp;
	}
	public String getClosed_extprice() {
		return closed_extprice;
	}
	public void setClosed_extprice(String closed_extprice) {
		this.closed_extprice = closed_extprice;
	}
	public String getComplete_actcost() {
		return complete_actcost;
	}
	public void setComplete_actcost(String complete_actcost) {
		this.complete_actcost = complete_actcost;
	}
	public String getComplete_actvgp() {
		return complete_actvgp;
	}
	public void setComplete_actvgp(String complete_actvgp) {
		this.complete_actvgp = complete_actvgp;
	}
	public String getComplete_extprice() {
		return complete_extprice;
	}
	public void setComplete_extprice(String complete_extprice) {
		this.complete_extprice = complete_extprice;
	}
	public String getInwork_actcost() {
		return inwork_actcost;
	}
	public void setInwork_actcost(String inwork_actcost) {
		this.inwork_actcost = inwork_actcost;
	}
	public String getInwork_actvgp() {
		return inwork_actvgp;
	}
	public void setInwork_actvgp(String inwork_actvgp) {
		this.inwork_actvgp = inwork_actvgp;
	}
	public String getInwork_extprice() {
		return inwork_extprice;
	}
	public void setInwork_extprice(String inwork_extprice) {
		this.inwork_extprice = inwork_extprice;
	}
	public String getTotal_actcost() {
		return total_actcost;
	}
	public void setTotal_actcost(String total_actcost) {
		this.total_actcost = total_actcost;
	}
	public String getTotal_actvgp() {
		return total_actvgp;
	}
	public void setTotal_actvgp(String total_actvgp) {
		this.total_actvgp = total_actvgp;
	}
	public String getTotal_extprice() {
		return total_extprice;
	}
	public void setTotal_extprice(String total_extprice) {
		this.total_extprice = total_extprice;
	}
	public String getMonthName() {
		return monthName;
	}
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}
	public String getMonthValue() {
		return monthValue;
	}
	public void setMonthValue(String monthValue) {
		this.monthValue = monthValue;
	}
	public String getYearValue() {
		return yearValue;
	}
	public void setYearValue(String yearValue) {
		this.yearValue = yearValue;
	}
	
}
