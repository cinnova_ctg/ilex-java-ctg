package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AppendixCustomerInformationForm extends ActionForm{

	private String custParameter1 = null;
	private String custParameter2 = null;
	private String custParameter3 = null;
	private String custParameter4 = null;
	private String custParameter5 = null;
	private String custParameter6 = null;
	private String custParameter7 = null;
	private String custParameter8 = null;
	private String custParameter9 = null;
	private String custParameter10 = null;
	private String custParameter11 = null;
	private String custParameter12 = null;
	private String custParameter13 = null;
	private String custParameter14 = null;
	private String custParameter15 = null;
	
	private String custParameter16 = null;
	private String custParameter17 = null;
	private String custParameter18 = null;
	private String custParameter19 = null;
	private String custParameter20 = null;
	private String custParameter21 = null;
	private String custParameter22 = null;
	private String custParameter23 = null;
	private String custParameter24 = null;
	private String custParameter25 = null;
	private String custParameter26 = null;
	private String custParameter27 = null;
	private String custParameter28 = null;
	private String custParameter29 = null;
	private String custParameter30 = null;
	
	
	
	private String custParameterId1 = null;
	private String custParameterId2 = null;
	private String custParameterId3 = null;
	private String custParameterId4 = null;
	private String custParameterId5 = null;
	private String custParameterId6 = null;
	private String custParameterId7 = null;
	private String custParameterId8 = null;
	private String custParameterId9 = null;
	private String custParameterId10 = null;
	private String custParameterId11 = null;
	private String custParameterId12 = null;
	private String custParameterId13 = null;
	private String custParameterId14 = null;
	private String custParameterId15 = null;
	
	
	private String custParameterId16 = null;
	private String custParameterId17 = null;
	private String custParameterId18 = null;
	private String custParameterId19 = null;
	private String custParameterId20 = null;
	private String custParameterId21 = null;
	private String custParameterId22 = null;
	private String custParameterId23 = null;
	private String custParameterId24 = null;
	private String custParameterId25 = null;
	private String custParameterId26 = null;
	private String custParameterId27 = null;
	private String custParameterId28 = null;
	private String custParameterId29 = null;
	private String custParameterId30 = null;
	
	
	private String custvalue3 = null;
	private String appendixid = null;
	private String msaId = null;
	private String appendixname = null;
	private String save = null;
	private String reset = null;
	private String function = null;
	private String authenticate = "";
	
	private String back = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String fromPage = null;
	
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getCustParameter1() {
		return custParameter1;
	}
	public void setCustParameter1(String custParameter1) {
		this.custParameter1 = custParameter1;
	}
	public String getCustParameter2() {
		return custParameter2;
	}
	public void setCustParameter2(String custParameter2) {
		this.custParameter2 = custParameter2;
	}
	public String getCustParameter3() {
		return custParameter3;
	}
	public void setCustParameter3(String custParameter3) {
		this.custParameter3 = custParameter3;
	}
	
	public String getCustvalue3() {
		return custvalue3;
	}
	public void setCustvalue3(String custvalue3) {
		this.custvalue3 = custvalue3;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		custParameter1 = null;
		custParameter2 = null;
		custParameter3 = null;
		custParameter4 = null;
		custParameter5 = null;
		custParameter6 = null;
		custParameter7 = null;
		custParameter8 = null;
		custParameter9 = null;
		custParameter10 = null;
		custvalue3 = null;
		appendixid = null;
		appendixname = null;
		save = null;
		function = null;
		back = null;
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getCustParameterId1() {
		return custParameterId1;
	}
	public void setCustParameterId1(String custParameterId1) {
		this.custParameterId1 = custParameterId1;
	}
	public String getCustParameterId2() {
		return custParameterId2;
	}
	public void setCustParameterId2(String custParameterId2) {
		this.custParameterId2 = custParameterId2;
	}
	public String getCustParameterId3() {
		return custParameterId3;
	}
	public void setCustParameterId3(String custParameterId3) {
		this.custParameterId3 = custParameterId3;
	}
	public String getCustParameterId10() {
		return custParameterId10;
	}
	public void setCustParameterId10(String custParameterId10) {
		this.custParameterId10 = custParameterId10;
	}
	public String getCustParameterId4() {
		return custParameterId4;
	}
	public void setCustParameterId4(String custParameterId4) {
		this.custParameterId4 = custParameterId4;
	}
	public String getCustParameterId5() {
		return custParameterId5;
	}
	public void setCustParameterId5(String custParameterId5) {
		this.custParameterId5 = custParameterId5;
	}
	public String getCustParameterId6() {
		return custParameterId6;
	}
	public void setCustParameterId6(String custParameterId6) {
		this.custParameterId6 = custParameterId6;
	}
	public String getCustParameterId7() {
		return custParameterId7;
	}
	public void setCustParameterId7(String custParameterId7) {
		this.custParameterId7 = custParameterId7;
	}
	public String getCustParameterId8() {
		return custParameterId8;
	}
	public void setCustParameterId8(String custParameterId8) {
		this.custParameterId8 = custParameterId8;
	}
	public String getCustParameterId9() {
		return custParameterId9;
	}
	public void setCustParameterId9(String custParameterId9) {
		this.custParameterId9 = custParameterId9;
	}
	public String getCustParameter10() {
		return custParameter10;
	}
	public void setCustParameter10(String custParameter10) {
		this.custParameter10 = custParameter10;
	}
	public String getCustParameter4() {
		return custParameter4;
	}
	public void setCustParameter4(String custParameter4) {
		this.custParameter4 = custParameter4;
	}
	public String getCustParameter5() {
		return custParameter5;
	}
	public void setCustParameter5(String custParameter5) {
		this.custParameter5 = custParameter5;
	}
	public String getCustParameter6() {
		return custParameter6;
	}
	public void setCustParameter6(String custParameter6) {
		this.custParameter6 = custParameter6;
	}
	public String getCustParameter7() {
		return custParameter7;
	}
	public void setCustParameter7(String custParameter7) {
		this.custParameter7 = custParameter7;
	}
	public String getCustParameter8() {
		return custParameter8;
	}
	public void setCustParameter8(String custParameter8) {
		this.custParameter8 = custParameter8;
	}
	public String getCustParameter9() {
		return custParameter9;
	}
	public void setCustParameter9(String custParameter9) {
		this.custParameter9 = custParameter9;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String getCustParameter11() {
		return custParameter11;
	}

	public void setCustParameter11(String custParameter11) {
		this.custParameter11 = custParameter11;
	}

	public String getCustParameter12() {
		return custParameter12;
	}

	public void setCustParameter12(String custParameter12) {
		this.custParameter12 = custParameter12;
	}

	public String getCustParameter13() {
		return custParameter13;
	}

	public void setCustParameter13(String custParameter13) {
		this.custParameter13 = custParameter13;
	}

	public String getCustParameter14() {
		return custParameter14;
	}

	public void setCustParameter14(String custParameter14) {
		this.custParameter14 = custParameter14;
	}

	public String getCustParameter15() {
		return custParameter15;
	}

	public void setCustParameter15(String custParameter15) {
		this.custParameter15 = custParameter15;
	}

	public String getCustParameterId11() {
		return custParameterId11;
	}

	public void setCustParameterId11(String custParameterId11) {
		this.custParameterId11 = custParameterId11;
	}

	public String getCustParameterId12() {
		return custParameterId12;
	}

	public void setCustParameterId12(String custParameterId12) {
		this.custParameterId12 = custParameterId12;
	}

	public String getCustParameterId13() {
		return custParameterId13;
	}

	public void setCustParameterId13(String custParameterId13) {
		this.custParameterId13 = custParameterId13;
	}

	public String getCustParameterId14() {
		return custParameterId14;
	}

	public void setCustParameterId14(String custParameterId14) {
		this.custParameterId14 = custParameterId14;
	}

	public String getCustParameterId15() {
		return custParameterId15;
	}

	public void setCustParameterId15(String custParameterId15) {
		this.custParameterId15 = custParameterId15;
	}

	public String getCustParameter16() {
		return custParameter16;
	}

	public void setCustParameter16(String custParameter16) {
		this.custParameter16 = custParameter16;
	}

	public String getCustParameter17() {
		return custParameter17;
	}

	public void setCustParameter17(String custParameter17) {
		this.custParameter17 = custParameter17;
	}

	public String getCustParameter18() {
		return custParameter18;
	}

	public void setCustParameter18(String custParameter18) {
		this.custParameter18 = custParameter18;
	}

	public String getCustParameter19() {
		return custParameter19;
	}

	public void setCustParameter19(String custParameter19) {
		this.custParameter19 = custParameter19;
	}

	public String getCustParameter20() {
		return custParameter20;
	}

	public void setCustParameter20(String custParameter20) {
		this.custParameter20 = custParameter20;
	}

	public String getCustParameter21() {
		return custParameter21;
	}

	public void setCustParameter21(String custParameter21) {
		this.custParameter21 = custParameter21;
	}

	public String getCustParameter22() {
		return custParameter22;
	}

	public void setCustParameter22(String custParameter22) {
		this.custParameter22 = custParameter22;
	}

	public String getCustParameter23() {
		return custParameter23;
	}

	public void setCustParameter23(String custParameter23) {
		this.custParameter23 = custParameter23;
	}

	public String getCustParameter24() {
		return custParameter24;
	}

	public void setCustParameter24(String custParameter24) {
		this.custParameter24 = custParameter24;
	}

	public String getCustParameter25() {
		return custParameter25;
	}

	public void setCustParameter25(String custParameter25) {
		this.custParameter25 = custParameter25;
	}

	public String getCustParameter26() {
		return custParameter26;
	}

	public void setCustParameter26(String custParameter26) {
		this.custParameter26 = custParameter26;
	}

	public String getCustParameter27() {
		return custParameter27;
	}

	public void setCustParameter27(String custParameter27) {
		this.custParameter27 = custParameter27;
	}

	public String getCustParameter28() {
		return custParameter28;
	}

	public void setCustParameter28(String custParameter28) {
		this.custParameter28 = custParameter28;
	}

	public String getCustParameter29() {
		return custParameter29;
	}

	public void setCustParameter29(String custParameter29) {
		this.custParameter29 = custParameter29;
	}

	public String getCustParameter30() {
		return custParameter30;
	}

	public void setCustParameter30(String custParameter30) {
		this.custParameter30 = custParameter30;
	}

	public String getCustParameterId16() {
		return custParameterId16;
	}

	public void setCustParameterId16(String custParameterId16) {
		this.custParameterId16 = custParameterId16;
	}

	public String getCustParameterId17() {
		return custParameterId17;
	}

	public void setCustParameterId17(String custParameterId17) {
		this.custParameterId17 = custParameterId17;
	}

	public String getCustParameterId18() {
		return custParameterId18;
	}

	public void setCustParameterId18(String custParameterId18) {
		this.custParameterId18 = custParameterId18;
	}

	public String getCustParameterId19() {
		return custParameterId19;
	}

	public void setCustParameterId19(String custParameterId19) {
		this.custParameterId19 = custParameterId19;
	}

	public String getCustParameterId20() {
		return custParameterId20;
	}

	public void setCustParameterId20(String custParameterId20) {
		this.custParameterId20 = custParameterId20;
	}

	public String getCustParameterId21() {
		return custParameterId21;
	}

	public void setCustParameterId21(String custParameterId21) {
		this.custParameterId21 = custParameterId21;
	}

	public String getCustParameterId22() {
		return custParameterId22;
	}

	public void setCustParameterId22(String custParameterId22) {
		this.custParameterId22 = custParameterId22;
	}

	public String getCustParameterId23() {
		return custParameterId23;
	}

	public void setCustParameterId23(String custParameterId23) {
		this.custParameterId23 = custParameterId23;
	}

	public String getCustParameterId24() {
		return custParameterId24;
	}

	public void setCustParameterId24(String custParameterId24) {
		this.custParameterId24 = custParameterId24;
	}

	public String getCustParameterId25() {
		return custParameterId25;
	}

	public void setCustParameterId25(String custParameterId25) {
		this.custParameterId25 = custParameterId25;
	}

	public String getCustParameterId26() {
		return custParameterId26;
	}

	public void setCustParameterId26(String custParameterId26) {
		this.custParameterId26 = custParameterId26;
	}

	public String getCustParameterId27() {
		return custParameterId27;
	}

	public void setCustParameterId27(String custParameterId27) {
		this.custParameterId27 = custParameterId27;
	}

	public String getCustParameterId28() {
		return custParameterId28;
	}

	public void setCustParameterId28(String custParameterId28) {
		this.custParameterId28 = custParameterId28;
	}

	public String getCustParameterId29() {
		return custParameterId29;
	}

	public void setCustParameterId29(String custParameterId29) {
		this.custParameterId29 = custParameterId29;
	}

	public String getCustParameterId30() {
		return custParameterId30;
	}

	public void setCustParameterId30(String custParameterId30) {
		this.custParameterId30 = custParameterId30;
	}
}
