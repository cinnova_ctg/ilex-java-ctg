package com.mind.formbean.PRM;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class AssignOwnerForm extends ActionForm{
	
	private String ownerName = null;
	private String changeFilter = null;
	
	private String newOwner = null;
	
	private String ownerId = null;
	private String viewjobtype = null;
	private String nettype = null;
	
	private String appendix_Id = null;
	private String appendixName = "";
	private String function = null;
	
	private String textboxdummy = null;
	
	private String save = null;
	
	private String jobId = null;
	private String jobName = null;
	private String fromPage = null;
	
	private ArrayList newOwnercombo = null;
	private String from = null;
	
	private String authenticate="";
	private String appendixType = "";
	private String msaId = "";
	private String msaName = "";
	/* */
	private String currentScheduler = null;
	private String newScheduler = null;
	private ArrayList schedulerList = null;
	
	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public ArrayList getNewOwnercombo() {
		return newOwnercombo;
	}

	public void setNewOwnercombo(ArrayList newOwnercombo) {
		this.newOwnercombo = newOwnercombo;
	}

	public String getNewOwner() {
		return newOwner;
	}

	public void setNewOwner(String newOwner) {
		this.newOwner = newOwner;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getTextboxdummy() {
		return textboxdummy;
	}

	public void setTextboxdummy(String textboxdummy) {
		this.textboxdummy = textboxdummy;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getAppendix_Id() {
		return appendix_Id;
	}

	public void setAppendix_Id(String appendix_Id) {
		this.appendix_Id = appendix_Id;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getNettype() {
		return nettype;
	}

	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getChangeFilter() {
		return changeFilter;
	}

	public void setChangeFilter(String changeFilter) {
		this.changeFilter = changeFilter;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String getCurrentScheduler() {
		return currentScheduler;
	}

	public void setCurrentScheduler(String currentScheduler) {
		this.currentScheduler = currentScheduler;
	}

	public String getNewScheduler() {
		return newScheduler;
	}

	public void setNewScheduler(String newScheduler) {
		this.newScheduler = newScheduler;
	}

	public ArrayList getSchedulerList() {
		return schedulerList;
	}

	public void setSchedulerList(ArrayList schedulerList) {
		this.schedulerList = schedulerList;
	}

	public String getAppendixType() {
		return appendixType;
	}

	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}



	
}
