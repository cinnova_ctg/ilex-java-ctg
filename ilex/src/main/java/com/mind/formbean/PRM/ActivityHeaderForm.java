package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ActivityHeaderForm extends ActionForm
{
	private String appendixid = null;
	private String appendixname = null;
	private String jobid = null;
	private String jobname = null;
	private String activityid = null;
	private String activityname = null;
	private String status = null;
	private String startdate = null;
	private String plannedenddate = null;
	private String materialscost = null;
	private String cnslaborcost = null;
	private String fieldlaborcost = null;
	private String freightcost = null;
	private String travelcost = null;
	private String estimatedcost = null;
	private String extendedprice = null;
	private String extendedsubtotal = null;
	private String proformamargin = null;
	private String type= null;
	private String resourceaddendumname= null;
	
	
	private String resourceid = null;
	private String resourcename = null;
	private String resourcetype = null;
	private String manufacturername = null;
	private String manufacturerpartno = null;
	private String cnspartno = null;
	private String qty = null;
	private String ecostunit = null;
	private String ecosttotal = null;
	private String epriceunit = null;
	private String epriceextended = null;
	private String deliverdate = null;
	private String qtyhours = null;
	private String unionuplift = null;
	private String ecosthourlybase = null;
	private String authenticate="";
	private String partnerassigned=null;
	private String partnerid=null;
	
	
	
	
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getActivityid() {
		return activityid;
	}
	public void setActivityid(String activityid) {
		this.activityid = activityid;
	}
	public String getActivityname() {
		return activityname;
	}
	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getCnslaborcost() {
		return cnslaborcost;
	}
	public void setCnslaborcost(String cnslaborcost) {
		this.cnslaborcost = cnslaborcost;
	}
	public String getCnspartno() {
		return cnspartno;
	}
	public void setCnspartno(String cnspartno) {
		this.cnspartno = cnspartno;
	}
	public String getDeliverdate() {
		return deliverdate;
	}
	public void setDeliverdate(String deliverdate) {
		this.deliverdate = deliverdate;
	}
	public String getEcosthourlybase() {
		return ecosthourlybase;
	}
	public void setEcosthourlybase(String ecosthourlybase) {
		this.ecosthourlybase = ecosthourlybase;
	}
	public String getEcosttotal() {
		return ecosttotal;
	}
	public void setEcosttotal(String ecosttotal) {
		this.ecosttotal = ecosttotal;
	}
	public String getEcostunit() {
		return ecostunit;
	}
	public void setEcostunit(String ecostunit) {
		this.ecostunit = ecostunit;
	}
	public String getEpriceextended() {
		return epriceextended;
	}
	public void setEpriceextended(String epriceextended) {
		this.epriceextended = epriceextended;
	}
	public String getEpriceunit() {
		return epriceunit;
	}
	public void setEpriceunit(String epriceunit) {
		this.epriceunit = epriceunit;
	}
	public String getEstimatedcost() {
		return estimatedcost;
	}
	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}
	
	public String getExtendedsubtotal() {
		return extendedsubtotal;
	}
	public void setExtendedsubtotal(String extendedsubtotal) {
		this.extendedsubtotal = extendedsubtotal;
	}
	public String getFieldlaborcost() {
		return fieldlaborcost;
	}
	public void setFieldlaborcost(String fieldlaborcost) {
		this.fieldlaborcost = fieldlaborcost;
	}
	public String getFreightcost() {
		return freightcost;
	}
	public void setFreightcost(String freightcost) {
		this.freightcost = freightcost;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getManufacturername() {
		return manufacturername;
	}
	public void setManufacturername(String manufacturername) {
		this.manufacturername = manufacturername;
	}
	public String getManufacturerpartno() {
		return manufacturerpartno;
	}
	public void setManufacturerpartno(String manufacturerpartno) {
		this.manufacturerpartno = manufacturerpartno;
	}
	public String getMaterialscost() {
		return materialscost;
	}
	public void setMaterialscost(String materialscost) {
		this.materialscost = materialscost;
	}
	public String getPlannedenddate() {
		return plannedenddate;
	}
	public void setPlannedenddate(String plannedenddate) {
		this.plannedenddate = plannedenddate;
	}
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin(String proformamargin) {
		this.proformamargin = proformamargin;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getQtyhours() {
		return qtyhours;
	}
	public void setQtyhours(String qtyhours) {
		this.qtyhours = qtyhours;
	}
	
	public String getResourceaddendumname() {
		return resourceaddendumname;
	}
	public void setResourceaddendumname(String resourceaddendumname) {
		this.resourceaddendumname = resourceaddendumname;
	}
	
	
	
	
	public String getResourceid() {
		return resourceid;
	}
	public void setResourceid(String resourceid) {
		this.resourceid = resourceid;
	}
	public String getResourcename() {
		return resourcename;
	}
	public void setResourcename(String resourcename) {
		this.resourcename = resourcename;
	}
	public String getResourcetype() {
		return resourcetype;
	}
	public void setResourcetype(String resourcetype) {
		this.resourcetype = resourcetype;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTravelcost() {
		return travelcost;
	}
	public void setTravelcost(String travelcost) {
		this.travelcost = travelcost;
	}
	public String getUnionuplift() {
		return unionuplift;
	}
	public void setUnionuplift(String unionuplift) {
		this.unionuplift = unionuplift;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		appendixid = null;
		appendixname = null;
		jobid = null;
		jobname = null;
		activityid = null;
		activityname = null;
		status = null;
		startdate = null;
		plannedenddate = null;
		materialscost = null;
		cnslaborcost = null;
		fieldlaborcost = null;
		freightcost = null;
		travelcost = null;
		estimatedcost = null;
		extendedprice = null;
		extendedsubtotal = null;
		proformamargin = null;
		type= null;
		resourceaddendumname= null;
		
		
		resourceid = null;
		resourcename = null;
		resourcetype = null;
		manufacturername = null;
		manufacturerpartno = null;
		cnspartno = null;
		qty = null;
		ecostunit = null;
		ecosttotal = null;
		epriceunit = null;
		epriceextended = null;
		deliverdate = null;
		qtyhours = null;
		unionuplift = null;
		ecosthourlybase = null;
		
	}
	public String getPartnerassigned() {
		return partnerassigned;
	}
	public void setPartnerassigned(String partnerassigned) {
		this.partnerassigned = partnerassigned;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	

}
