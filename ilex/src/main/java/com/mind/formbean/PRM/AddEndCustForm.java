package com.mind.formbean.PRM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class AddEndCustForm extends ActionForm{
	
	private String endcust = null;
	private String ownerId = null;
	private String viewjobtype = null;
	private String typeId = null;
	private String type = null;
	private String fromPage = null;
	private String name = null;
	private String add = null;
	private String cancel = null;
	private String back = null;
	private String updatemessage = null;
	private String from = null;
	private String appendixid = null;
	private String check  = null;
	private String msaid =null;
	private String msaname = null;
	
//	For the View Selector
	private String monthWeekCheck = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	
//	New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String   appendixOtherCheck = null;
	private String msaName = null;
	//private String name = null;
	
	
	
	
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getUpdatemessage() {
		return updatemessage;
	}
	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		updatemessage = null;
		endcust = null;
	}
	public String getEndcust() {
		return endcust;
	}
	public void setEndcust(String endcust) {
		this.endcust = endcust;
	}
	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getMsaid() {
		return msaid;
	}
	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}
	public String getFromPage() {
		return fromPage;
	}
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}
	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}
	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}
	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}
	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}
	public String getAppendixSelectedOwners( int i ) {
		return appendixSelectedOwners[i];
	}
	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}
	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}
	public String getAppendixSelectedStatus( int i ) {
		return appendixSelectedStatus[i];
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}
	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getOtherCheck() {
		return otherCheck;
	}
	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}
	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}
	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}
	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}
	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}
	
}
