package com.mind.formbean.PRM;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class AssignTeamForm extends ActionForm {
	
	
//	For View Selector: Start 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
//	For View Selector: End
	
//	For Menu: Start
	private String ownerId = null;
	private String viewjobtype = null;
	private String msaId = null;
	private String fromPage = null;
//	For Menu: End
	
	private String appendixid = null;
	private String appendixName = null;
	private String pocId = null;
	private Collection assignTeamList = null;
	private String roleId = null;
	private Collection roleList = null;
	private Collection pocList = null;
	private String save = null;
	
	

	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}
	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}
	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}
	
	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}
	public Collection getRoleList() {
		return roleList;
	}
	public void setRoleList(Collection roleList) {
		this.roleList = roleList;
	}
	public Collection getAssignTeamList() {
		return assignTeamList;
	}
	public void setAssignTeamList(Collection assignTeamList) {
		this.assignTeamList = assignTeamList;
	}
	public String getPocId() {
		return pocId;
	}
	public void setPocId(String pocId) {
		this.pocId = pocId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public Collection getPocList() {
		return pocList;
	}
	public void setPocList(Collection pocList) {
		this.pocList = pocList;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getFromPage() {
		return fromPage;
	}
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

}
