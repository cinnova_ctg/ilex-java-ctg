package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class HideMSAForm extends ActionForm{
	
	private String msaId = null;
	private String msaName = null;
	private String msaBDM = null;;
	private String msaStatus = null;
	private String[] check = null;
	private String goHideMSA = null;
	private String cancel = null;
		
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String[] getCheck() {
		return check;
	}
	public void setCheck(String[] check) {
		this.check = check;
	}
	public String getCheck(int i) {
		return check[i];
	}
	public String getGoHideMSA() {
		return goHideMSA;
	}
	public void setGoHideMSA(String goHideMSA) {
		this.goHideMSA = goHideMSA;
	}
	public String getMsaBDM() {
		return msaBDM;
	}
	public void setMsaBDM(String msaBDM) {
		this.msaBDM = msaBDM;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getMsaStatus() {
		return msaStatus;
	}
	public void setMsaStatus(String msaStatus) {
		this.msaStatus = msaStatus;
	}
}