package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class DeleteJobForm extends ActionForm {
	
	private String msaId = null;
	private String appendixId = null;
	private String jobId = null;
	private String jobName = null;
	private String appendixName = null;
	private String msaName = null;
	private String jobStatus = null;
	private String jobOwner = null;
	private String jobScheduleCheck = null;
	private String jobPowoCheck = null;
	private String jobSearch = null;
	private String jobKeyword = null;
	private String jobOrgStatus = null;
	
	public String getJobOrgStatus() {
		return jobOrgStatus;
	}
	public void setJobOrgStatus(String jobOrgStatus) {
		this.jobOrgStatus = jobOrgStatus;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobOwner() {
		return jobOwner;
	}
	public void setJobOwner(String jobOwner) {
		this.jobOwner = jobOwner;
	}
	public String getJobPowoCheck() {
		return jobPowoCheck;
	}
	public void setJobPowoCheck(String jobPowoCheck) {
		this.jobPowoCheck = jobPowoCheck;
	}
	public String getJobScheduleCheck() {
		return jobScheduleCheck;
	}
	public void setJobScheduleCheck(String jobScheduleCheck) {
		this.jobScheduleCheck = jobScheduleCheck;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getJobSearch() {
		return jobSearch;
	}
	public void setJobSearch(String jobSearch) {
		this.jobSearch = jobSearch;
	}
	public String getJobKeyword() {
		return jobKeyword;
	}
	public void setJobKeyword(String jobKeyword) {
		this.jobKeyword = jobKeyword;
	}
	
}
