package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class ToolsDescForm extends ActionForm {
	private String toolId=null;
	private String name = null;
	private String description=null;
	private String isActive = null;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getToolId() {
		return toolId;
	}
	public void setToolId(String toolId) {
		this.toolId = toolId;
	}
	
}
