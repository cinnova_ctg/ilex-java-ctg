package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class CssStandardsForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type = null;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
