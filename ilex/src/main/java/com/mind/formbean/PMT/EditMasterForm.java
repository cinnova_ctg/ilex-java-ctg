package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class EditMasterForm  extends ActionForm {
	
	private String messageContent = null;
	private String save = null;
	private String cancel = null;
	private byte[] termCondData = null;
	private String termCond = null;
	private String type = null;
		

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getTermCond() {
		return termCond;
	}

	public void setTermCond(String termCond) {
		this.termCond = termCond;
	}

	public byte[] getTermCondData() {
		return termCondData;
	}

	public void setTermCondData(byte[] termCondData) {
		this.termCondData = termCondData;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
