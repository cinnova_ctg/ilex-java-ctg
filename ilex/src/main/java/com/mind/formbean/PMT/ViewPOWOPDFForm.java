package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class ViewPOWOPDFForm extends ActionForm {
	
	private String poId = null;
	private String type = null;
	private int requestStatus = -1;
	private String go = null;
	private byte[] pdfBytes = null;
	
	
	public byte[] getPdfBytes() {
		return pdfBytes;
	}
	public void setPdfBytes(byte[] pdfBytes) {
		this.pdfBytes = pdfBytes;
	}
	public String getPoId() {
		return poId;
	}
	public void setPoId(String poId) {
		this.poId = poId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(int requestStatus) {
		this.requestStatus = requestStatus;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}

}
