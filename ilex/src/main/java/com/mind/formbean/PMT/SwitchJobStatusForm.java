package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class SwitchJobStatusForm extends ActionForm{
	private String invoiceNumber = null;
	private String go = null;
	
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
}
