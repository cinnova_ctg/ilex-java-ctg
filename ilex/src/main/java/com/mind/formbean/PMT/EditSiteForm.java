package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class EditSiteForm extends ActionForm{
	
	private String msaid = null;
	private String []siteid = null;
	private String []mastersiteid = null;
	private String submit = null;
	private String sort = null;
	private String []statecombo = null;
	private String []unionSitecombo = null;
	
	private String msa_name = null;
	
	private String []msaName = null;
	private String []siteName = null;
	private String []siteNumber = null;
	private String []localityFactor = null;
	private String []unionSite = null;
	private String []designator = null;
	private String []worklocation = null;
	private String []address = null;
	
	private String []city = null;
	private String []state = null;
	private String []zipcode = null;
	private String []country = null;
	private String []directions = null;
	private String []primaryContactPerson = null;
	private String []secondaryContactPerson = null;
	
	private String []primaryPhoneNumber = null;
	private String []secondaryPhoneNumber = null;
	private String []primaryEmailId = null;
	private String []secondaryEmailId = null;
	private String []specialCondition = null;
	
	private String flag = null; // This flag will decide that at  starting point search screen will have only search combo
								// After search It will have listing of sites which are to be edited. 
	private String msasearchcombo = null; //Search combe for msa
	private String statesearchcombo = null; //search combo for state
	
	
	private String sitenamesearch = null;
	private String sitenumbersearch = null;
	private String sitecitysearch = null;
	private String search = null;
	

	

	public String getSitenumbersearch() {
		return sitenumbersearch;
	}

	public void setSitenumbersearch(String sitenumbersearch) {
		this.sitenumbersearch = sitenumbersearch;
	}

	public String getSort() {
		return sort;
	}

	public String getSitenamesearch() {
		return sitenamesearch;
	}

	public void setSitenamesearch(String sitenamesearch) {
		this.sitenamesearch = sitenamesearch;
	}

	public String getMsasearchcombo() {
		return msasearchcombo;
	}

	public void setMsasearchcombo(String msasearchcombo) {
		this.msasearchcombo = msasearchcombo;
	}

	

	public String getStatesearchcombo() {
		return statesearchcombo;
	}

	public void setStatesearchcombo(String statesearchcombo) {
		this.statesearchcombo = statesearchcombo;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getMsaid() {
		return msaid;
	}

	

	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}

	public String[] getAddress() {
		return address;
	}
	
	public String getAddress(int i) {
		return address[i];
	}

	public void setAddress(String[] address) {
		this.address = address;
	}

	public String[] getCity() {
		return city;
	}
	public String getCity(int i) {
		return city[i];
	}

	public void setCity(String[] city) {
		this.city = city;
	}

	public String[] getCountry() {
		return country;
	}
	public String getCountry(int i) {
		return country[i];
	}

	public void setCountry(String[] country) {
		this.country = country;
	}

	public String[] getDesignator() {
		return designator;
	}
	public String getDesignator(int i) {
		return designator[i];
	}

	public void setDesignator(String[] designator) {
		this.designator = designator;
	}

	public String[] getDirections() {
		return directions;
	}
	public String getDirections(int i) {
		return directions[i];
	}

	public void setDirections(String[] directions) {
		this.directions = directions;
	}

	public String[] getLocalityFactor() {
		return localityFactor;
	}
	public String getLocalityFactor(int i) {
		return localityFactor[i];
	}

	public void setLocalityFactor(String[] localityFactor) {
		this.localityFactor = localityFactor;
	}

	public String[] getPrimaryContactPerson() {
		return primaryContactPerson;
	}
	public String getPrimaryContactPerson(int i) {
		return primaryContactPerson[i];
	}

	public void setPrimaryContactPerson(String[] primaryContactPerson) {
		this.primaryContactPerson = primaryContactPerson;
	}

	public String[] getPrimaryEmailId() {
		return primaryEmailId;
	}
	public String getPrimaryEmailId(int i) {
		return primaryEmailId[i];
	}

	public void setPrimaryEmailId(String[] primaryEmailId) {
		this.primaryEmailId = primaryEmailId;
	}

	public String[] getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}
	public String getPrimaryPhoneNumber(int i) {
		return primaryPhoneNumber[i];
	}

	public void setPrimaryPhoneNumber(String[] primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String[] getSecondaryContactPerson() {
		return secondaryContactPerson;
	}
	public String getSecondaryContactPerson(int i) {
		return secondaryContactPerson[i];
	}

	public void setSecondaryContactPerson(String[] secondaryContactPerson) {
		this.secondaryContactPerson = secondaryContactPerson;
	}

	public String[] getSecondaryEmailId() {
		return secondaryEmailId;
	}
	public String getSecondaryEmailId(int i) {
		return secondaryEmailId[i];
	}

	public void setSecondaryEmailId(String[] secondaryEmailId) {
		this.secondaryEmailId = secondaryEmailId;
	}

	public String[] getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}
	public String getSecondaryPhoneNumber(int i) {
		return secondaryPhoneNumber[i];
	}

	public void setSecondaryPhoneNumber(String[] secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}

	public String[] getSiteNumber() {
		return siteNumber;
	}
	public String getSiteNumber(int i) {
		return siteNumber[i];
	}

	public void setSiteNumber(String[] siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String[] getSpecialCondition() {
		return specialCondition;
	}
	
	public String getSpecialCondition(int i) {
		return specialCondition[i];
	}

	public void setSpecialCondition(String[] specialCondition) {
		this.specialCondition = specialCondition;
	}

	public String[] getState() {
		return state;
	}
	public String getState(int i) {
		return state[i];
	}

	public void setState(String[] state) {
		this.state = state;
	}

	public String[] getUnionSite() {
		return unionSite;
	}
	public String getUnionSite(int i) {
		return unionSite[i];
	}

	public void setUnionSite(String[] unionSite) {
		this.unionSite = unionSite;
	}

	public String[] getWorklocation() {
		return worklocation;
	}
	public String getWorklocation(int i) {
		return worklocation[i];
	}

	public void setWorklocation(String[] worklocation) {
		this.worklocation = worklocation;
	}

	public String[] getZipcode() {
		return zipcode;
	}

	public void setZipcode(String[] zipcode) {
		this.zipcode = zipcode;
	}
	public String getZipcode(int i) {
		return zipcode[i];
	}

	public String[] getMsaName() {
		return msaName;
	}

	public void setMsaName(String[] msaName) {
		this.msaName = msaName;
	}
	public String getMsaName(int i) {
		return msaName[i];
	}

	public String[] getSiteName() {
		return siteName;
	}
	public String getSiteName(int i) {
		return siteName[i];
	}

	public void setSiteName(String[] siteName) {
		this.siteName = siteName;
	}

	public String[] getSiteid() {
		return siteid;
	}
	public String getSiteid(int i) {
		return siteid[i];
	}

	public void setSiteid(String[] siteid) {
		this.siteid = siteid;
	}

	public String[] getMastersiteid() {
		return mastersiteid;
	}
	public String getMastersiteid(int i) {
		return mastersiteid[i];
	}

	public void setMastersiteid(String[] mastersiteid) {
		this.mastersiteid = mastersiteid;
	}

	public String[] getStatecombo() {
		return statecombo;
	}
	public String getStatecombo(int i) {
		return statecombo[i];
	}

	public void setStatecombo(String[] statecombo) {
		this.statecombo = statecombo;
	}

	public String[] getUnionSitecombo() {
		return unionSitecombo;
	}
	
	public String getUnionSitecombo(int i) {
		return unionSitecombo[i];
	}

	public void setUnionSitecombo(String[] unionSitecombo) {
		this.unionSitecombo = unionSitecombo;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getSitecitysearch() {
		return sitecitysearch;
	}

	public void setSitecitysearch(String sitecitysearch) {
		this.sitecitysearch = sitecitysearch;
	}

	public String getMsa_name() {
		return msa_name;
	}

	public void setMsa_name(String msa_name) {
		this.msa_name = msa_name;
	}

	

}
