package com.mind.formbean.PMT;

import org.apache.struts.action.ActionForm;

public class HideAppendixForm extends ActionForm{

	private String msaId = null;
	private String msaName = null;
	private String appendixId = null;;
	private String appendixName = null;
	private String appendixBDM = null;
	private String appendixPrManager = null;
	private String appStatus = "";
	private String[] check = null;
	private String goHideAppendix = null;
	private String cancel = null;
	private String go = null;
	
	public String getAppendixBDM() {
		return appendixBDM;
	}
	public void setAppendixBDM(String appendixBDM) {
		this.appendixBDM = appendixBDM;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getAppendixPrManager() {
		return appendixPrManager;
	}
	public void setAppendixPrManager(String appendixPrManager) {
		this.appendixPrManager = appendixPrManager;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getGoHideAppendix() {
		return goHideAppendix;
	}
	public void setGoHideAppendix(String goHideAppendix) {
		this.goHideAppendix = goHideAppendix;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getAppStatus() {
		return appStatus;
	}
	public void setAppStatus(String appStatus) {
		this.appStatus = appStatus;
	}
	public String[] getCheck() {
		return check;
	}
	public void setCheck(String[] check) {
		this.check = check;
	}
	public String getCheck(int i) {
		return check[i];
	}

}
