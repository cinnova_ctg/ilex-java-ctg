/**
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for PPSAdd(third level category add) page
*
*/
package com.mind.formbean.RM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class PPSAddForm extends ActionForm {

	private String save = null;
	private String reset = null;
	private String ppstype = null;
	private String comboflag=null;
	private String flag=null;
	private String resourceflag=null;
	private String id=null;
	private String id1=null;
	private String id2=null;
	private String firstcatg=null;
	private String secondcatg=null;
	private String existmfg = null;
	private String message=null;
	private String addmessage=null;
	private String updatemessage=null;
	private boolean refresh=false;
	private String authenticate="";
	private String textboxdummy = null;
	
	
	
	public String getAddmessage() {
		return addmessage;
	}

	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}

	public String getUpdatemessage() {
		return updatemessage;
	}

	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean getRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public String getComboflag() {
		return comboflag;
	}

	public void setComboflag(String comboflag) {
		this.comboflag = comboflag;
	}
	public String getExistmfg() {
		return existmfg;
	}

	public void setExistmfg(String existmfg) {
		this.existmfg = existmfg;
	}

	public String getFirstcatg() {
		return firstcatg;
	}

	public void setFirstcatg(String firstcatg) {
		this.firstcatg = firstcatg;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(String id2) {
		this.id2 = id2;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(String id1) {
		this.id1 = id1;
	}

	public String getResourceflag() {
		return resourceflag;
	}

	public void setResourceflag(String resourceflag) {
		this.resourceflag = resourceflag;
	}

	
	
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getSecondcatg() {
		return secondcatg;
	}

	public void setSecondcatg(String secondcatg) {
		this.secondcatg = secondcatg;
	}

	
	public String  getSave() {
		return save;
	}

	
	public void setSave(String s) {
		this.save = s;
	}

	
	public String getReset() {
		return reset;
	}

	
	public void setReset(String r) {
		this.reset = r;
	}

	
	public String getPpstype() {
		return ppstype;
	}

	public void setPpstype(String p) {
		this.ppstype = p;
	}
	
	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getTextboxdummy() {
		return textboxdummy;
	}

	public void setTextboxdummy(String textboxdummy) {
		this.textboxdummy = textboxdummy;
	}



	public void reset(ActionMapping mapping, HttpServletRequest request) {

		
		save = null;
		reset = null;
		ppstype = null;
		firstcatg=null;
		secondcatg=null;
		flag=null;
		resourceflag=null;
		id = null;
		id1 = null;
		id2 = null;
		existmfg = null;
		comboflag=null;
		message=null;
		addmessage=null;
		updatemessage=null;
		refresh=false;
		textboxdummy = null;
		

	}

	
	
		

	
}
