/**
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for ManAdd(second level category add) page
*
*/
package com.mind.formbean.RM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class ManAddForm extends ActionForm {

	private String subcatg = null;
	private String existcategory = null;
	private String save = null;
	private String reset = null;
	private String category = null;
	private String flag = null;
	//private String comboflag=null;
	private String resourceflag=null;
	private String id=null;
	private String id1=null;
	private String message=null;
	private String addmessage=null;
	private String updatemessage=null;
	private boolean refresh=false;
	private String authenticate="";
	private String textboxdummy = null;
	
	
	
	public String getAddmessage() {
		return addmessage;
	}

	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}

	public String getUpdatemessage() {
		return updatemessage;
	}

	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}
	

	public boolean getRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(String id1) {
		this.id1 = id1;
	}
	
	public String getResourceflag() {
		return resourceflag;
	}

	public void setResourceflag(String resourceflag) {
		this.resourceflag = resourceflag;
	}

	
	public String getFlag() {
		return flag;
	}

	public void setFlag(String f) {
		this.flag = f;
	}
	public String getCategory() {
		return category;
	}

	public void setCategory(String c) {
		this.category = c;
	}
	public String getSubcatg() {
		return subcatg;
	}

	public void setSubcatg(String s) {
		this.subcatg = s;
	}

	
	public String getExistcategory() {
		return existcategory;
	}
	
	

	
	public void setExistcategory(String e) {
		this.existcategory = e;
	}

	public String getSave() {
		return save;
	}

	
	public void setSave(String s) {
		this.save = s;
	}

	public String getReset() {
		return reset;
	}

	
	public void setReset(String r) {
		this.reset = r;
	}
	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

		

	public String getTextboxdummy() {
		return textboxdummy;
	}

	public void setTextboxdummy(String textboxdummy) {
		this.textboxdummy = textboxdummy;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		

		subcatg = null;
		existcategory = null;
		save = null;
		reset = null;
		flag = null;
		category = null;
		id = null;
		id1 = null;
		//comboflag=null;
		message=null;
		addmessage=null;
		updatemessage=null;
		refresh=false;
		textboxdummy = null;
	}

	

		


}
