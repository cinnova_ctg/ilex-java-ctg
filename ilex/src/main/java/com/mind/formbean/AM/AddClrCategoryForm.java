package com.mind.formbean.AM;

import org.apache.struts.action.ActionForm;

public class AddClrCategoryForm extends ActionForm 
{
	private String clrexistingcategory = null;
	private String clrcategory = null;
	private String save = null;
	private String reset = null;
	private String clrcategoryshortname = null;
	private String addmessage=null;
	private String ref=null;
	private String authenticate="";
	private String clrservicetime[] = null;
	private String clrcriticality[] = null;
	private String refresh = null;
	private String deletemessage=null;
	private String updatemessage=null;
	
	public String getAddmessage() {
		return addmessage;
	}
	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getClrcategory() {
		return clrcategory;
	}
	
	public void setClrcategory(String clrcategory) {
		this.clrcategory = clrcategory;
	}
	public String getClrcategoryshortname() {
		return clrcategoryshortname;
	}
	public void setClrcategoryshortname(String clrcategoryshortname) {
		this.clrcategoryshortname = clrcategoryshortname;
	}
	public String[] getClrcriticality() {
		return clrcriticality;
	}
	public void setClrcriticality(String[] clrcriticality) {
		this.clrcriticality = clrcriticality;
	}
	public String getClrexistingcategory() {
		return clrexistingcategory;
	}
	public void setClrexistingcategory(String clrexistingcategory) {
		this.clrexistingcategory = clrexistingcategory;
	}
	public String[] getClrservicetime() {
		return clrservicetime;
	}
	public String getClrservicetime(int i) {
		return clrservicetime[i];
	}
	public void setClrservicetime(String[] clrservicetime) {
		this.clrservicetime = clrservicetime;
	}
	public String getDeletemessage() {
		return deletemessage;
	}
	public void setDeletemessage(String deletemessage) {
		this.deletemessage = deletemessage;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getRefresh() {
		return refresh;
	}
	public void setRefresh(String refresh) {
		this.refresh = refresh;
	}
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getUpdatemessage() {
		return updatemessage;
	}
	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}
	

}
