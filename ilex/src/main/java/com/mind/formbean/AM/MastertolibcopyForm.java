package com.mind.formbean.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.mind.common.LabelValue;

public class MastertolibcopyForm extends ActionForm
{
	private String msa_Id = null;
	private String[] cat_Id = null;
	private String[] category_name = null;
	
	private String check[] = null;
	private String activity_Id[] = null;
	private String name[] = null;
	
	private String activitytype[] = null;
	private String activitytypecombo[] = null;
	
	private String quantity[] = null;
	private String duration[] = null;
	
	private String estimatedmaterialcost[] = null;
	private String estimatedcnslabourcost[] = null;
	private String estimatedcontractlabourcost[] = null;
	private String estimatedfreightcost[] = null;
	private String estimatedtravelcost[] = null;
	private String estimatedtotalcost[] = null;
	private String extendedprice[] = null;
	private String proformamargin[] = null;
	private String status[] = null;
	
	private String ref = null;
	private ArrayList<LabelValue> msaList = null;
	private String selectedMSAId = null;
	
	
	public String getSelectedMSAId() {
		return selectedMSAId;
	}
	public void setSelectedMSAId(String selectedMSAId) {
		this.selectedMSAId = selectedMSAId;
	}
	public ArrayList<LabelValue> getMsaList() {
		return msaList;
	}
	public void setMsaList(ArrayList<LabelValue> msaList) {
		this.msaList = msaList;
	}
	public String[] getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String[] activity_Id ) {
		this.activity_Id = activity_Id;
	}
	public String getActivity_Id( int i ) {
		return activity_Id[i];
	}
	
	
	
	public String[] getActivitytype() {
		return activitytype;
	}
	public void setActivitytype( String[] activitytype ) {
		this.activitytype = activitytype;
	}
	public String getActivitytype( int i ) {
		return activitytype[i];
	}
	
	
	
	public String[] getActivitytypecombo() {
		return activitytypecombo;
	}
	public void setActivitytypecombo( String[] activitytypecombo ) {
		this.activitytypecombo = activitytypecombo;
	}
	public String getActivitytypecombo( int i ) {
		return activitytypecombo[i];
	}
	
	
	public String[] getCat_Id() {
		return cat_Id;
	}
	public void setCat_Id( String[] cat_Id ) {
		this.cat_Id = cat_Id;
	}
	public String getCat_Id( int i ) {
		return cat_Id[i];
	}
	
	
	public String[] getCategory_name() {
		return category_name;
	}
	public void setCategory_name( String[] category_name ) {
		this.category_name = category_name;
	}
	public String getCategory_name( int i ) {
		return category_name[i];
	}
	
	
	public String[] getCheck() {
		return check;
	}
	public void setCheck( String[] check ) {
		this.check = check;
	}
	public String getCheck( int i ) {
		return check[i];
	}
	
	
	
	public String[] getDuration() {
		return duration;
	}
	public void setDuration( String[] duration ) {
		this.duration = duration;
	}
	public String getDuration( int i ) {
		return duration[i];
	}
	
	
	
	public String[] getEstimatedcnslabourcost() {
		return estimatedcnslabourcost;
	}
	public void setEstimatedcnslabourcost( String[] estimatedcnslabourcost ) {
		this.estimatedcnslabourcost = estimatedcnslabourcost;
	}
	public String getEstimatedcnslabourcost( int i ) {
		return estimatedcnslabourcost[i];
	}
	
	
	
	public String[] getEstimatedcontractlabourcost() {
		return estimatedcontractlabourcost;
	}
	public void setEstimatedcontractlabourcost( String[] estimatedcontractlabourcost ) {
		this.estimatedcontractlabourcost = estimatedcontractlabourcost;
	}
	public String getEstimatedcontractlabourcost( int i ) {
		return estimatedcontractlabourcost[i];
	}
	
	
	public String[] getEstimatedfreightcost() {
		return estimatedfreightcost;
	}
	public void setEstimatedfreightcost( String[] estimatedfreightcost ) {
		this.estimatedfreightcost = estimatedfreightcost;
	}
	public String getEstimatedfreightcost( int i ) {
		return estimatedfreightcost[i];
	}
	
	
	
	
	public String[] getEstimatedmaterialcost() {
		return estimatedmaterialcost;
	}
	public void setEstimatedmaterialcost( String[] estimatedmaterialcost ) {
		this.estimatedmaterialcost = estimatedmaterialcost;
	}
	public String getEstimatedmaterialcost( int i ) {
		return estimatedmaterialcost[i];
	}
	
	
	
	public String[] getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost( String[] estimatedtotalcost ) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	public String getEstimatedtotalcost( int i ) {
		return estimatedtotalcost[i];
	}
	
	
	public String[] getEstimatedtravelcost() {
		return estimatedtravelcost;
	}
	public void setEstimatedtravelcost( String[] estimatedtravelcost ) {
		this.estimatedtravelcost = estimatedtravelcost;
	}
	public String getEstimatedtravelcost( int i ) {
		return estimatedtravelcost[i];
	}
	
	
	public String[] getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice( String[] extendedprice ) {
		this.extendedprice = extendedprice;
	}
	public String getExtendedprice( int i ) {
		return extendedprice[i];
	}
	
	
	
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id( String msa_Id ) {
		this.msa_Id = msa_Id;
	}
	
	
	
	public String[] getName() {
		return name;
	}
	public void setName( String[] name ) {
		this.name = name;
	}
	public String getName( int i ) {
		return name[i];
	}
	
	
	
	public String[] getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String[] proformamargin ) {
		this.proformamargin = proformamargin;
	}
	public String getProformamargin( int i ) {
		return proformamargin[i];
	}
	
	
	public String[] getQuantity() {
		return quantity;
	}
	public void setQuantity( String[] quantity ) {
		this.quantity = quantity;
	}
	public String getQuantity( int i ) {
		return quantity[i];
	}
	
	
	public String[] getStatus() {
		return status;
	}
	public void setStatus( String[] status ) {
		this.status = status;
	}
	public String getStatus( int i ) {
		return status[i];
	}
	
	
	public String getRef() {
		return ref;
	}
	public void setRef( String ref ) {
		this.ref = ref;
	}
	
	
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		msa_Id = null;
		cat_Id = null;
		category_name = null;
		
		check = null;
		activity_Id = null;
		name = null;
		
		activitytype = null;
		activitytypecombo = null;
		
		quantity = null;
		duration = null;
		
		estimatedmaterialcost = null;
		estimatedcnslabourcost = null;
		estimatedcontractlabourcost = null;
		estimatedfreightcost = null;
		estimatedtravelcost = null;
		estimatedtotalcost = null;
		extendedprice = null;
		proformamargin = null;
		status = null;
		ref = null;
	}
	
}
