/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for labor resource tabular page
*
*/

package com.mind.formbean.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class LaborResourceForm extends ActionForm
{
	
	private String activity_Id = null;
	private String appendix_id = null;
	private String fromPage = null;
	
	private String[] check = null;
	private String[] laborid = null;
	private String[] laborcostlibid = null;
	private String[] laborname = null;
	
	private String[] labortype = null;
	private String[] cnspartnumber = null;
	
	private String[] quantityhours = null;
	private String[] prevquantityhours = null;
	
	private String[] estimatedhourlybasecost = null;
	
	private String[] estimatedtotalcost = null;
	private String[] proformamargin = null;
	private String[] priceunit = null;
	private String[] priceextended = null;
	private String[] status = null;
	private String[] sellablequantity = null;
	private String[] minimumquantity = null;
	private String[] subcategory = null;
	
	
	private String newlaborid = null;
	private String newlaborname = null;
	private String newlabornamecombo = null;
	
	private String newlabortype = null;
	private String newcnspartnumber = null;
	private String newquantityhours = null;
	private String newestimatedhourlybasecost = null;
	
	private String newestimatedtotalcost = null;
	private String newproformamargin = null;
	private String newpriceunit = null;
	private String newpriceextended = null;
	private String newstatus = null;
	private String newsellablequantity = null;
	private String newminimumquantity = null;
	private String newsubcategory = null;
	
	private ArrayList laborlist = null;
	
	private String ref = null;
	private String save = null;
	
	private String chkaddendum = null;
	private String chkaddendumdetail = null;
	private String backtoactivity = null;
	private String addendum_id = null;
	
	
	private String fromflag = null;
	private String dashboardid = null;
	
	//	for back button
	private String MSA_Id = null;
	// for in transit field
	private String[] transit = null;
	private String[] checktransit = null;
	private String newtransit = "N";
	private String newchecktransit = null;
	
	private String[] flag = null; // for resource from temporary table or not
	private String checknetmedx = null;
	
	private String jobname = null;
	private String jobid = null;
	
	
	private String msa_id = null;
	private String msaname = null;
	private String appendixname = null;
	private String activity_name = null;
	
	
	public String[] getFlag() {
		return flag;
	}
	public void setFlag(String[] flag) {
		this.flag = flag;
	}
	public String getFlag( int i ) {
		return flag[i];
	}
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	
	public String[] getCheck() {
		return check;
	}
	public void setCheck( String[] check ) {
		this.check = check;
	}
	public String getCheck( int i ) {
		return check[i];
	}

	
	public String[] getCnspartnumber() {
		return cnspartnumber;
	}
	public void setCnspartnumber( String[] cnspartnumber ) {
		this.cnspartnumber = cnspartnumber;
	}
	public String getCnspartnumber( int i ) {
		return cnspartnumber[i];
	}
	
	
	
	public String[] getEstimatedhourlybasecost() {
		return estimatedhourlybasecost;
	}
	public void setEstimatedhourlybasecost( String[] estimatedhourlybasecost ) {
		this.estimatedhourlybasecost = estimatedhourlybasecost;
	}
	public String getEstimatedhourlybasecost( int i ) {
		return estimatedhourlybasecost[i];
	}
	
	
	
	public String[] getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost( String[] estimatedtotalcost ) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	public String getEstimatedtotalcost( int i ) {
		return estimatedtotalcost[i];
	}
	
	
	
	public String[] getLaborid() {
		return laborid;
	}
	public void setLaborid( String[] laborid ) {
		this.laborid = laborid;
	}
	public String getLaborid( int i ) {
		return laborid[i];
	}
	
	
	public String[] getLaborcostlibid() {
		return laborcostlibid;
	}
	public void setLaborcostlibid( String[] laborcostlibid ) {
		this.laborcostlibid = laborcostlibid;
	}
	public String getLaborcostlibid( int i ) {
		return laborcostlibid[i];
	}
	
	
	public String[] getLaborname() {
		return laborname;
	}
	public void setLaborname( String[] laborname ) {
		this.laborname = laborname;
	}
	public String getLaborname( int i ) {
		return laborname[i];
	}
	
	
	
	public String[] getLabortype() {
		return labortype;
	}
	public void setLabortype( String[] labortype ) {
		this.labortype = labortype;
	}
	public String getLabortype( int i ) {
		return labortype[i];
	}
	
	
	public String[] getMinimumquantity() {
		return minimumquantity;
	}
	public void setMinimumquantity( String[] minimumquantity ) {
		this.minimumquantity = minimumquantity;
	}
	public String getMinimumquantity( int i ) {
		return minimumquantity[i];
	}
	
	
	public String[] getPriceextended() {
		return priceextended;
	}
	public void setPriceextended( String[] priceextended ) {
		this.priceextended = priceextended;
	}
	public String getPriceextended( int i ) {
		return priceextended[i];
	}
	
	
	public String[] getPriceunit() {
		return priceunit;
	}
	public void setPriceunit( String[] priceunit ) {
		this.priceunit = priceunit;
	}
	public String getPriceunit( int i ) {
		return priceunit[i];
	}
	
	
	public String[] getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String[] proformamargin ) {
		this.proformamargin = proformamargin;
	}
	public String getProformamargin( int i) {
		return proformamargin[i];
	}
	
	
	public String[] getQuantityhours() {
		return quantityhours;
	}
	public void setQuantityhours( String[] quantityhours ) {
		this.quantityhours = quantityhours;
	}
	public String getQuantityhours( int i ) {
		return quantityhours[i];
	}
	
	
	public String[] getPrevquantityhours() {
		return prevquantityhours;
	}
	public void setPrevquantityhours( String[] prevquantityhours ) {
		this.prevquantityhours = prevquantityhours;
	}
	public String getPrevquantityhours( int i ) {
		return prevquantityhours[i];
	}
	
	
	
	public String[] getSellablequantity() {
		return sellablequantity;
	}
	public void setSellablequantity( String[] sellablequantity ) {
		this.sellablequantity = sellablequantity;
	}
	public String getSellablequantity( int i ) {
		return sellablequantity[i];
	}
	
	
	public String[] getStatus() {
		return status;
	}
	public void setStatus(String[] status) {
		this.status = status;
	}
	public String getStatus( int i ) {
		return status[i];
	}
	
	
	
	public String[] getSubcategory() {
		return subcategory;
	}
	public void setSubcategory( String[] subcategory ) {
		this.subcategory = subcategory;
	}
	public String getSubcategory( int i ) {
		return subcategory[i];
	}
	
	
	public String getNewcnspartnumber() {
		return newcnspartnumber;
	}
	public void setNewcnspartnumber( String newcnspartnumber ) {
		this.newcnspartnumber = newcnspartnumber;
	}
	
	
	public String getNewestimatedhourlybasecost() {
		return newestimatedhourlybasecost;
	}
	public void setNewestimatedhourlybasecost( String newestimatedhourlybasecost ) {
		this.newestimatedhourlybasecost = newestimatedhourlybasecost;
	}
	
	
	
	public String getNewestimatedtotalcost() {
		return newestimatedtotalcost;
	}
	public void setNewestimatedtotalcost( String newestimatedtotalcost ) {
		this.newestimatedtotalcost = newestimatedtotalcost;
	}
	
	
	public String getNewlaborid() {
		return newlaborid;
	}
	public void setNewlaborid( String newlaborid ) {
		this.newlaborid = newlaborid;
	}
	
	
	public String getNewlaborname() {
		return newlaborname;
	}
	public void setNewlaborname( String newlaborname ) {
		this.newlaborname = newlaborname;
	}
	
	
	public String getNewlabornamecombo() {
		return newlabornamecombo;
	}
	public void setNewlabornamecombo( String newlabornamecombo ) {
		this.newlabornamecombo = newlabornamecombo;
	}
	
	
	public String getNewlabortype() {
		return newlabortype;
	}
	public void setNewlabortype( String newlabortype ) {
		this.newlabortype = newlabortype;
	}
	
	
	public String getNewminimumquantity() {
		return newminimumquantity;
	}
	public void setNewminimumquantity( String newminimumquantity ) {
		this.newminimumquantity = newminimumquantity;
	}
	
	
	public String getNewpriceextended() {
		return newpriceextended;
	}
	public void setNewpriceextended( String newpriceextended ) {
		this.newpriceextended = newpriceextended;
	}
	
	
	public String getNewpriceunit() {
		return newpriceunit;
	}
	public void setNewpriceunit( String newpriceunit ) {
		this.newpriceunit = newpriceunit;
	}
	
	
	public String getNewproformamargin() {
		return newproformamargin;
	}
	public void setNewproformamargin( String newproformamargin ) {
		this.newproformamargin = newproformamargin;
	}
	
	
	public String getNewquantityhours() {
		return newquantityhours;
	}
	public void setNewquantityhours( String newquantityhours ) {
		this.newquantityhours = newquantityhours;
	}
	
	
	public String getNewsellablequantity() {
		return newsellablequantity;
	}
	public void setNewsellablequantity( String newsellablequantity ) {
		this.newsellablequantity = newsellablequantity;
	}
	
	
	public String getNewstatus() {
		return newstatus;
	}
	public void setNewstatus( String newstatus ) {
		this.newstatus = newstatus;
	}
	
	
	
	public String getNewsubcategory() {
		return newsubcategory;
	}
	public void setNewsubcategory( String newsubcategory ) {
		this.newsubcategory = newsubcategory;
	}
	
	
	public ArrayList getLaborlist() {
		return laborlist;
	}
	public void setLaborlist( ArrayList laborlist ) {
		this.laborlist = laborlist;
	}
	
	
	public String getRef() {
		return ref;
	}
	public void setRef( String ref ) {
		this.ref = ref;
	}
	
	
	public String getSave() {
		return save;
	}
	public void setSave( String save ) {
		this.save = save;
	}
	
	
	
	

	public String getBacktoactivity() {
		return backtoactivity;
	}
	public void setBacktoactivity( String backtoactivity ) {
		this.backtoactivity = backtoactivity;
	}
	
	
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum( String chkaddendum ) {
		this.chkaddendum = chkaddendum;
	}
	
	
	public String getChkaddendumdetail() {
		return chkaddendumdetail;
	}
	public void setChkaddendumdetail( String chkaddendumdetail ) {
		this.chkaddendumdetail = chkaddendumdetail;
	}
	
	
	
	
	
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getDashboardid() {
		return dashboardid;
	}
	public void setDashboardid(String dashboardid) {
		this.dashboardid = dashboardid;
	}
	public String getFromflag() {
		return fromflag;
	}
	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}
	
	
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	
	
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	
	
	public String getMsa_id() {
		return msa_id;
	}
	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}
	
	
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
/** This method reset all the fields
* @param mapping						object of ActionMapping
* @param request   			       		object of HttpServletRequest
* @return void              			This method returns object of ActionForward.
*/
	
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		activity_Id = null;
		
		check = null;
		laborid = null;
		laborcostlibid = null;
		laborname = null;
		
		labortype = null;
		cnspartnumber = null;
		quantityhours = null;
		prevquantityhours = null;
		estimatedhourlybasecost = null;
		
		estimatedtotalcost = null;
		proformamargin = null;
		priceunit = null;
		priceextended = null;
		status = null;
		sellablequantity = null;
		minimumquantity = null;
		subcategory = null;
		
		newlaborid = null;
		newlaborname = null;
		newlabornamecombo = null;
		
		newlabortype = null;
		newcnspartnumber = null;
		newquantityhours = null;
		newestimatedhourlybasecost = null;
		
		newestimatedtotalcost = null;
		newproformamargin = null;
		newpriceunit = null;
		newpriceextended = null;
		newstatus = null;
		newsellablequantity = null;
		newminimumquantity = null;
		newsubcategory = null;
		
		laborlist = null;
		
		ref = null;
		save = null;
		
		chkaddendum = null;
		chkaddendumdetail = null;
		backtoactivity = null;
		addendum_id = null;
		
		
	}
	public String getMSA_Id() {
		return MSA_Id;
	}
	public void setMSA_Id(String id) {
		MSA_Id = id;
	}
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}
	public String getFromPage() {
		return fromPage;
	}
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}
	public String[] getChecktransit() {
		return checktransit;
	}
	public void setChecktransit(String[] checktransit) {
		this.checktransit = checktransit;
	}
	public String getChecktransit( int i ) {
		return checktransit[i];
	}
	
	public String getNewchecktransit() {
		return newchecktransit;
	}
	public void setNewchecktransit(String newchecktransit) {
		this.newchecktransit = newchecktransit;
	}
	
	public String getNewtransit() {
		return newtransit;
	}
	public void setNewtransit(String newtransit) {
		this.newtransit = newtransit;
	}
	public String[] getTransit() {
		return transit;
	}
	public void setTransit(String[] transit) {
		this.transit = transit;
	}
	public String getTransit( int i ) {
		return transit[i];
	}
	public String getChecknetmedx() {
		return checknetmedx;
	}
	public void setChecknetmedx(String checknetmedx) {
		this.checknetmedx = checknetmedx;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	
	
}
