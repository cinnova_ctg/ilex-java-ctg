/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for material resource tabular page
*
*/

package com.mind.formbean.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class MaterialResourceForm extends ActionForm
{
	private String activity_Id = null;
	private String appendix_id = null;
	private String fromPage = null;
	
	
	private String[] check = null;
	private String[] materialid = null;
	private String[] materialcostlibid = null;
	
	private String[] materialname = null;
	private String[] materialtype = null;
	
	private String[] manufacturername = null;
	private String[] manufacturerpartnumber = null;
	private String[] cnspartnumber = null;
	
	private String[] quantity = null;
	private String[] prevquantity = null;
	
	private String[] estimatedunitcost = null;
	private String[] estimatedtotalcost = null;
	private String[] proformamargin = null;
	private String[] priceunit = null;
	private String[] priceextended = null;
	private String[] status = null;
	private String[] sellablequantity = null;
	private String[] minimumquantity = null;
	
	
	private String newmaterialid = null;
	private String newmaterialname = null;
	private String newmaterialnamecombo = null;
	private String newmaterialtype = null;
	
	private String newmanufacturername = null;
	private String newmanufacturerpartnumber = null;
	private String newcnspartnumber = null;
	private String newquantity = null;
	private String newestimatedunitcost = null;
	private String newestimatedtotalcost = null;
	private String newproformamargin = null;
	private String newpriceunit = null;
	private String newpriceextended = null;
	
	private String newsellablequantity = null;
	private String newminimumquantity = null;
	
	private String newstatus = null;
	
	private ArrayList materiallist = null;
	
	
	private String chkaddendum = null;
	private String chkaddendumdetail = null;
	private String backtoactivity = null;
	private String addendum_id = null;
	
	private String fromflag = null;
	private String dashboardid = null;
	
	
	private String[] flag = null; // for resource from temporary table or not
	
	private String jobname = null;
	private String jobid = null;
	
	//private ArrayList materialresourcelist = null;
	
	private String ref = null;
	private String save = null;
	
	//for back button
	private String MSA_Id = null;
	
	
	private String msa_id = null;
	private String msaname = null;
	private String appendixname = null;
	
	private String activity_name = null;
	
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	
	public String[] getMaterialid() {
		return materialid;
	}
	public void setMaterialid( String[] materialid ) {
		this.materialid = materialid;
	}
	public String getMaterialid( int i ) {
		return materialid[i];
	}
	
	
	public String[] getMaterialcostlibid() {
		return materialcostlibid;
	}
	public void setMaterialcostlibid( String[] materialcostlibid ) {
		this.materialcostlibid = materialcostlibid;
	}
	public String getMaterialcostlibid( int i ) {
		return materialcostlibid[i];
	}
	
	
	
	public String[] getCnspartnumber() {
		return cnspartnumber;
	}
	public void setCnspartnumber( String[] cnspartnumber ) {
		this.cnspartnumber = cnspartnumber;
	}
	public String getCnspartnumber( int i ) {
		return cnspartnumber[i];
	}
	
	
	public String[] getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost( String[] estimatedtotalcost ) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	public String getEstimatedtotalcost( int i ) {
		return estimatedtotalcost[i];
	}
	
	
	public String[] getEstimatedunitcost() {
		return estimatedunitcost;
	}
	public void setEstimatedunitcost(String[] estimatedunitcost) {
		this.estimatedunitcost = estimatedunitcost;
	}
	public String getEstimatedunitcost( int i ) {
		return estimatedunitcost[i];
	}
	
	
	public String[] getManufacturername() {
		return manufacturername;
	}
	public void setManufacturername(String[] manufacturername) {
		this.manufacturername = manufacturername;
	}
	public String getManufacturername( int i ) {
		return manufacturername[i];
	}
	
	
	public String[] getManufacturerpartnumber() {
		return manufacturerpartnumber;
	}
	public void setManufacturerpartnumber(String[] manufacturerpartnumber) {
		this.manufacturerpartnumber = manufacturerpartnumber;
	}
	public String getManufacturerpartnumber( int i ) {
		return manufacturerpartnumber[i];
	}
	
	
	public String[] getCheck() {
		return check;
	}
	public void setCheck( String[] check ) {
		this.check = check;
	}
	public String getCheck( int i ) {
		return check[i];
	}
	
	
	public String[] getMaterialname() {
		return materialname;
	}
	public void setMaterialname( String[] materialname ) {
		this.materialname = materialname;
	}
	public String getMaterialname( int  i ) {
		return materialname[i];
	}
	
	
	public String[] getMaterialtype() {
		return materialtype;
	}
	public void setMaterialtype( String[] materialtype ) {
		this.materialtype = materialtype;
	}
	public String getMaterialtype( int i ) {
		return materialtype[i];
	}
	
	
	public String[] getPriceextended() {
		return priceextended;
	}
	public void setPriceextended( String[] priceextended ) {
		this.priceextended = priceextended;
	}
	public String getPriceextended( int i ) {
		return priceextended[i];
	}
	
	
	
	public String[] getPriceunit() {
		return priceunit;
	}
	public void setPriceunit( String[] priceunit ) {
		this.priceunit = priceunit;
	}
	public String getPriceunit( int i ) {
		return priceunit[i];
	}
	
	
	public String[] getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String[] proformamargin ) {
		this.proformamargin = proformamargin;
	}
	public String getProformamargin( int i ) {
		return proformamargin[i];
	}
	
	
	public String[] getQuantity() {
		return quantity;
	}
	public void setQuantity( String[] quantity ) {
		this.quantity = quantity;
	}
	public String getQuantity( int i ) {
		return quantity[i];
	}
	

	public String[] getPrevquantity() {
		return prevquantity;
	}
	public void setPrevquantity(String[] prevquantity) {
		this.prevquantity = prevquantity;
	}
	public String getPrevquantity( int i ) {
		return prevquantity[i];
	}
	
	
	
	
	public String[] getStatus() {
		return status;
	}
	public void setStatus( String[] status ) {
		this.status = status;
	}
	public String getStatus( int i ) {
		return status[i];
	}
	
	
	public String[] getMinimumquantity() {
		return minimumquantity;
	}
	public void setMinimumquantity( String[] minimumquantity ) {
		this.minimumquantity = minimumquantity;
	}
	public String getMinimumquantity( int i ) {
		return minimumquantity[i];
	}
	
	
	public String[] getSellablequantity() {
		return sellablequantity;
	}
	public void setSellablequantity( String[] sellablequantity ) {
		this.sellablequantity = sellablequantity;
	}
	public String getSellablequantity( int i ) {
		return sellablequantity[i];
	}
	
	
	public String getNewcnspartnumber() {
		return newcnspartnumber;
	}
	public void setNewcnspartnumber( String newcnspartnumber ) {
		this.newcnspartnumber = newcnspartnumber;
	}
	
	
	public String getNewestimatedtotalcost() {
		return newestimatedtotalcost;
	}
	public void setNewestimatedtotalcost( String newestimatedtotalcost ) {
		this.newestimatedtotalcost = newestimatedtotalcost;
	}
	
	
	public String getNewestimatedunitcost() {
		return newestimatedunitcost;
	}
	public void setNewestimatedunitcost( String newestimatedunitcost ) {
		this.newestimatedunitcost = newestimatedunitcost;
	}
	
	
	public String getNewmanufacturername() {
		return newmanufacturername;
	}
	public void setNewmanufacturername( String newmanufacturername ) {
		this.newmanufacturername = newmanufacturername;
	}
	
	
	public String getNewmanufacturerpartnumber() {
		return newmanufacturerpartnumber;
	}
	public void setNewmanufacturerpartnumber( String newmanufacturerpartnumber ) {
		this.newmanufacturerpartnumber = newmanufacturerpartnumber;
	}
	
	
	public String getNewmaterialid() {
		return newmaterialid;
	}
	public void setNewmaterialid( String newmaterialid ) {
		this.newmaterialid = newmaterialid;
	}
	
	
	public String getNewmaterialname() {
		return newmaterialname;
	}
	public void setNewmaterialname( String newmaterialname ) {
		this.newmaterialname = newmaterialname;
	}
	
	
	public String getNewmaterialnamecombo() {
		return newmaterialnamecombo;
	}
	public void setNewmaterialnamecombo( String newmaterialnamecombo ) {
		this.newmaterialnamecombo = newmaterialnamecombo;
	}
	
	
	public String getNewmaterialtype() {
		return newmaterialtype;
	}
	public void setNewmaterialtype( String newmaterialtype ) {
		this.newmaterialtype = newmaterialtype;
	}
	
	
	public String getNewpriceextended() {
		return newpriceextended;
	}
	public void setNewpriceextended( String newpriceextended ) {
		this.newpriceextended = newpriceextended;
	}
	
	
	public String getNewpriceunit() {
		return newpriceunit;
	}
	public void setNewpriceunit( String newpriceunit ) {
		this.newpriceunit = newpriceunit;
	}
	
	
	public String getNewproformamargin() {
		return newproformamargin;
	}
	public void setNewproformamargin( String newproformamargin ) {
		this.newproformamargin = newproformamargin;
	}
	
	
	public String getNewquantity() {
		return newquantity;
	}
	public void setNewquantity( String newquantity ) {
		this.newquantity = newquantity;
	}
	
	
	public String getNewminimumquantity() {
		return newminimumquantity;
	}
	public void setNewminimumquantity( String newminimumquantity ) {
		this.newminimumquantity = newminimumquantity;
	}
	
	
	public String getNewsellablequantity() {
		return newsellablequantity;
	}
	public void setNewsellablequantity( String newsellablequantity ) {
		this.newsellablequantity = newsellablequantity;
	}
	
	
	public String getNewstatus() {
		return newstatus;
	}
	public void setNewstatus( String newstatus ) {
		this.newstatus = newstatus;
	}
	

	public ArrayList getMateriallist() {
		return materiallist;
	}
	public void setMateriallist( ArrayList materiallist ) {
		this.materiallist = materiallist;
	}
	
	
	/*public ArrayList getMaterialresourcelist() {
		return materialresourcelist;
	}
	public void setMaterialresourcelist( ArrayList materialresourcelist ) {
		this.materialresourcelist = materialresourcelist;
	}*/
	
	
	public String getRef() {
		return ref;
	}
	public void setRef( String ref ) {
		this.ref = ref;
	}
	
	
	public String getSave() {
		return save;
	}
	public void setSave( String save ) {
		this.save = save;
	}
	
	
	public String getBacktoactivity() {
		return backtoactivity;
	}
	public void setBacktoactivity( String backtoactivity ) {
		this.backtoactivity = backtoactivity;
	}
	
	
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum( String chkaddendum ) {
		this.chkaddendum = chkaddendum;
	}
	
	
	public String getChkaddendumdetail() {
		return chkaddendumdetail;
	}
	public void setChkaddendumdetail( String chkaddendumdetail ) {
		this.chkaddendumdetail = chkaddendumdetail;
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	
	
	public String getDashboardid() {
		return dashboardid;
	}
	public void setDashboardid(String dashboardid) {
		this.dashboardid = dashboardid;
	}
	public String getFromflag() {
		return fromflag;
	}
	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}
	

	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	 
	
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	
	
	public String getMsa_id() {
		return msa_id;
	}
	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}
	
	
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
/** This method reset all the fields
* @param mapping						object of ActionMapping
* @param request   			       		object of HttpServletRequest
* @return void              			This method returns object of ActionForward.
*/

	
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		activity_Id = null;
		check = null;
		materialid = null;
		materialcostlibid = null;
		materialname = null;
		materialtype = null;
		
		manufacturername = null;
		manufacturerpartnumber = null;
		cnspartnumber = null;
		quantity = null;
		prevquantity = null;
		estimatedunitcost = null;
		estimatedtotalcost = null;
		proformamargin = null;
		priceunit = null;
		priceextended = null;
		status = null;
		sellablequantity = null;
		minimumquantity = null;
		
		newmaterialid = null;
		newmaterialname = null;
		newmaterialnamecombo = null;
		newmaterialtype = null;
		
		newmanufacturername = null;
		newmanufacturerpartnumber = null;
		newcnspartnumber = null;
		newquantity = null;
		newestimatedunitcost = null;
		newestimatedtotalcost = null;
		newproformamargin = null;
		newpriceunit = null;
		newpriceextended = null;
		newstatus = null;
		
		materiallist = null;
		//materialresourcelist = null;
		ref = null;
		save = null;
		
		chkaddendum = null;
		chkaddendumdetail = null;
		backtoactivity = null;
		addendum_id = null;
	}
	public String getMSA_Id() {
		return MSA_Id;
	}
	public void setMSA_Id(String id) {
		MSA_Id = id;
	}
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}
	public String getFromPage() {
		return fromPage;
	}
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}
	public String[] getFlag() {
		return flag;
	}
	public void setFlag(String[] flag) {
		this.flag = flag;
	}
	
	public String getFlag( int i ) {
		return flag[i];
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
		
	
}
