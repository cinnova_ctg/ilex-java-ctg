/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for activity library detail page
*
*/

package com.mind.formbean.AM;

import org.apache.struts.action.ActionForm;

public class ActivityLibraryDetailForm extends ActionForm
{
	private String activity_Id = null;
	private String msa_Id = null;
	private String activityname = null;
	private String activitytype = null;
	private String quantity = null;
	private String estimatedmaterialcost = null;
	private String estimatedcnslabourcost = null;
	private String estimatedcontractlabourcost = null;
	private String estimatedfreightcost = null;
	private String estimatedtravelcost = null;
	private String estimatedtotalcost = null;
	private String extendedprice = null;
	private String proformamargin = null;
	private String sow = null;
	private String assumption = null;
	private String status = null;
	private String authenticate = "";
	private String msaname = null;
	
	
	
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}
	public String getActivityname() {
		return activityname;
	}
	public void setActivityname( String activityname ) {
		this.activityname = activityname;
	}
	
	
	public String getActivitytype() {
		return activitytype;
	}
	public void setActivitytype( String activitytype ) {
		this.activitytype = activitytype;
	}
	
	
	public String getEstimatedcnslabourcost() {
		return estimatedcnslabourcost;
	}
	public void setEstimatedcnslabourcost( String estimatedcnslabourcost ) {
		this.estimatedcnslabourcost = estimatedcnslabourcost;
	}
	
	
	public String getEstimatedcontractlabourcost() {
		return estimatedcontractlabourcost;
	}
	public void setEstimatedcontractlabourcost( String estimatedcontractlabourcost ) {
		this.estimatedcontractlabourcost = estimatedcontractlabourcost;
	}
	
	
	public String getEstimatedfreightcost() {
		return estimatedfreightcost;
	}
	public void setEstimatedfreightcost( String estimatedfreightcost ) {
		this.estimatedfreightcost = estimatedfreightcost;
	}
	
	
	public String getEstimatedmaterialcost() {
		return estimatedmaterialcost;
	}
	public void setEstimatedmaterialcost( String estimatedmaterialcost ) {
		this.estimatedmaterialcost = estimatedmaterialcost;
	}
	
	
	public String getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost( String estimatedtotalcost ) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	
	
	public String getEstimatedtravelcost() {
		return estimatedtravelcost;
	}
	public void setEstimatedtravelcost( String estimatedtravelcost ) {
		this.estimatedtravelcost = estimatedtravelcost;
	}
	
	
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice( String extendedprice ) {
		this.extendedprice = extendedprice;
	}
	
	
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String proformamargin ) {
		this.proformamargin = proformamargin;
	}
	
	
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity( String quantity )  {
		this.quantity = quantity;
	}
	
	
	public String getSow() {
		return sow;
	}
	public void setSow( String sow ) {
		this.sow = sow;
	}
	
	
	public String getAssumption() {
		return assumption;
	}
	public void setAssumption( String assumption ) {
		this.assumption = assumption;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus( String status ) {
		this.status = status;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	
	
}
