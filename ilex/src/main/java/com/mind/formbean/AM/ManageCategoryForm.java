/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for category page
*
*/


package com.mind.formbean.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ManageCategoryForm extends ActionForm {
	private String category_id = null;
	private String category_name = null;
	private String ref = null;
	private String save = null;
	private String textboxdummy = null;
	private ArrayList existingcategory = null;
	private String authenticate = "";
	
	public String getAuthenticate() {
		return authenticate;
	}
	
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	
	public String getCategory_id() {
		return category_id;
	}
	
	public void setCategory_id( String category_id ) {
		this.category_id = category_id;
	}
	
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name( String category_name ) {
		this.category_name = category_name;
	}
	
	
	public String getRef() {
		return ref;
	}
	public void setRef( String ref ) {
		this.ref = ref;
	}
	
	
	public String getSave() {
		return save;
	}
	public void setSave( String save ) {
		this.save = save;
	}
	
	
	public ArrayList getExistingcategory() {
		return existingcategory;
	}
	public void setExistingcategory( ArrayList existingcategory ) {
		this.existingcategory = existingcategory;
	}
	
	
	
	public String getTextboxdummy() {
		return textboxdummy;
	}
	public void setTextboxdummy( String textboxdummy ) {
		this.textboxdummy = textboxdummy;
	}
	
	/** This method reset all the fields
	* @param mapping						object of ActionMapping
	* @param request   			       		object of HttpServletRequest
	* @return void              			This method returns object of ActionForward.
	*/	
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		category_id = null;
		category_name = null;
		ref = null;
		save = null;
		textboxdummy = null;
	}
}
