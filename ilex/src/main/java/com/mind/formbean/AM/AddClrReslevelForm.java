package com.mind.formbean.AM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AddClrReslevelForm extends ActionForm 
{

	private String clrclass = null;
	private String name = null;
	private String save = null;
	private String reset = null;
	private String shortname = null;
	private String addmessage=null;
	private String ref=null;
	private String authenticate="";
	private String clrresource = null;
	private String refresh = null;
	private String deletemessage=null;
	
	
	
	public String getAddmessage() {
		return addmessage;
	}



	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}



	public String getAuthenticate() {
		return authenticate;
	}



	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}



	public String getClrclass() {
		return clrclass;
	}



	public void setClrclass(String clrclass) {
		this.clrclass = clrclass;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getReset() {
		return reset;
	}



	public void setReset(String reset) {
		this.reset = reset;
	}



	public String getSave() {
		return save;
	}



	public void setSave(String save) {
		this.save = save;
	}



	public String getShortname() {
		return shortname;
	}



	public void setShortname(String shortname) {
		this.shortname = shortname;
	}



	public void reset(ActionMapping mapping, HttpServletRequest request) 
	{

		clrclass = null;
		name = null;
		save = null;
		reset = null;
		shortname = null;
		addmessage=null;
		ref=null;
		clrresource = null;
		refresh = null;
		deletemessage=null;
	}



	public String getRef() {
		return ref;
	}



	public void setRef(String ref) {
		this.ref = ref;
	}



	public String getClrresource() {
		return clrresource;
	}



	public void setClrresource(String clrresource) {
		this.clrresource = clrresource;
	}



	public String getRefresh() {
		return refresh;
	}



	public void setRefresh(String refresh) {
		this.refresh = refresh;
	}



	public String getDeletemessage() {
		return deletemessage;
	}



	public void setDeletemessage(String deletemessage) {
		this.deletemessage = deletemessage;
	}
}
