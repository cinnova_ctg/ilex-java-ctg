package com.mind.formbean.AM;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CustomerLaborRatesForm extends ActionForm {

	private String customername = null;
	private String customerid = null;
	private String minimumfee = null;
	private String travel = null;
	private String travelmax = null;
	private String discount = null;
	private String effectivedate = null;
	private String rules = null;
	private String save = null;
	private String reset = null;
	private String delete = null;
	private String addmessage = null;
	// private String function = null;

	private String[] criticalityname = null;
	private String[] criticalitydesc = null;
	private String[] criticalityid = null;
	private String[] description = null;
	private String[] ppstype = null;
	private String[] techengg0 = null;
	private String[] techengg1 = null;
	private String[] techengg2 = null;
	private String[] techengg3 = null;
	private String[] techengg4 = null;
	private String[] techengg5 = null;
	private String[] techengg6 = null;
	private String[] techengg7 = null;
	private String[] techengg8 = null;
	private String[] techengg9 = null;
	private String[] techenggid0 = null;
	private String[] techenggid1 = null;
	private String[] techenggid2 = null;
	private String[] techenggid3 = null;
	private String[] techenggid4 = null;
	private String[] techenggid5 = null;
	private String[] techenggid6 = null;
	private String[] techenggid7 = null;
	private String[] techenggid8 = null;
	private String[] techenggid9 = null;

	private String nooftechnician = null;
	private String noofengineers = null;
	private String msa_Id = null;
	private String activitylibrary_Id = null;
	private String type = null;

	private String authenticate = "";

	private String[] classname = null;
	private String[] classnums = null;
	private String[] resourcename = null;
	private String[] categoryname = null;
	private String[] categoryshortname = null;
	private String[] servicetime = null;
	private String[] detailid = null;
	private String[] resourceid = null;
	private String[] clrrate = null;
	private String catid = null;

	private Collection reslist = null;

	private String[] clrrate0 = null;
	private String[] clrrate1 = null;
	private String[] clrrate2 = null;
	private String[] clrrate3 = null;
	private String[] clrrate4 = null;
	private String[] clrrate5 = null;
	private String[] clrrate6 = null;
	private String[] clrrate7 = null;
	private String[] clrrate8 = null;
	private String[] clrrate9 = null;
	private String[] clrrate10 = null;
	private String[] clrrate11 = null;
	private String[] clrrate12 = null;
	private String[] clrrate13 = null;
	private String[] clrrate14 = null;
	private String[] clrrate15 = null;
	private String[] clrrate16 = null;
	private String[] clrrate17 = null;
	private String[] clrrate18 = null;
	private String[] clrrate19 = null;
	private String[] clrrate20 = null;
	private String[] clrrate21 = null;
	private String[] clrrate22 = null;
	private String[] clrrate23 = null;
	private String[] clrrate24 = null;
	private String[] clrrate25 = null;
	private String[] clrrate26 = null;
	private String[] clrrate27 = null;
	private String[] clrrate28 = null;
	private String[] clrrate29 = null;
	private String[] clrrate30 = null;

	private String[] detailid0 = null;
	private String[] detailid1 = null;
	private String[] detailid2 = null;
	private String[] detailid3 = null;
	private String[] detailid4 = null;
	private String[] detailid5 = null;
	private String[] detailid6 = null;
	private String[] detailid7 = null;
	private String[] detailid8 = null;
	private String[] detailid9 = null;
	private String[] detailid10 = null;
	private String[] detailid11 = null;
	private String[] detailid12 = null;
	private String[] detailid13 = null;
	private String[] detailid14 = null;
	private String[] detailid15 = null;
	private String[] detailid16 = null;
	private String[] detailid17 = null;
	private String[] detailid18 = null;
	private String[] detailid19 = null;
	private String[] detailid20 = null;
	private String[] detailid21 = null;
	private String[] detailid22 = null;
	private String[] detailid23 = null;
	private String[] detailid24 = null;
	private String[] detailid25 = null;
	private String[] detailid26 = null;
	private String[] detailid27 = null;
	private String[] detailid28 = null;
	private String[] detailid29 = null;
	private String[] detailid30 = null;

	private String[] resourceid0 = null;
	private String[] resourceid1 = null;
	private String[] resourceid2 = null;
	private String[] resourceid3 = null;
	private String[] resourceid4 = null;
	private String[] resourceid5 = null;
	private String[] resourceid6 = null;
	private String[] resourceid7 = null;
	private String[] resourceid8 = null;
	private String[] resourceid9 = null;
	private String[] resourceid10 = null;
	private String[] resourceid11 = null;
	private String[] resourceid12 = null;
	private String[] resourceid13 = null;
	private String[] resourceid14 = null;
	private String[] resourceid15 = null;
	private String[] resourceid16 = null;
	private String[] resourceid17 = null;
	private String[] resourceid18 = null;
	private String[] resourceid19 = null;
	private String[] resourceid20 = null;
	private String[] resourceid21 = null;
	private String[] resourceid22 = null;
	private String[] resourceid23 = null;
	private String[] resourceid24 = null;
	private String[] resourceid25 = null;
	private String[] resourceid26 = null;
	private String[] resourceid27 = null;
	private String[] resourceid28 = null;
	private String[] resourceid29 = null;
	private String[] resourceid30 = null;

	private String[] selectresourceid0 = null;
	private String[] selectresourceid1 = null;
	private String[] selectresourceid2 = null;
	private String[] selectresourceid3 = null;
	private String[] selectresourceid4 = null;
	private String[] selectresourceid5 = null;
	private String[] selectresourceid6 = null;
	private String[] selectresourceid7 = null;
	private String[] selectresourceid8 = null;
	private String[] selectresourceid9 = null;
	private String[] selectresourceid10 = null;
	private String[] selectresourceid11 = null;
	private String[] selectresourceid12 = null;
	private String[] selectresourceid13 = null;
	private String[] selectresourceid14 = null;
	private String[] selectresourceid15 = null;
	private String[] selectresourceid16 = null;
	private String[] selectresourceid17 = null;
	private String[] selectresourceid18 = null;
	private String[] selectresourceid19 = null;
	private String[] selectresourceid20 = null;
	private String[] selectresourceid21 = null;
	private String[] selectresourceid22 = null;
	private String[] selectresourceid23 = null;
	private String[] selectresourceid24 = null;
	private String[] selectresourceid25 = null;
	private String[] selectresourceid26 = null;
	private String[] selectresourceid27 = null;
	private String[] selectresourceid28 = null;
	private String[] selectresourceid29 = null;
	private String[] selectresourceid30 = null;

	private String[] resourcename0 = null;
	private String[] resourcename1 = null;
	private String[] resourcename2 = null;
	private String[] resourcename3 = null;
	private String[] resourcename4 = null;
	private String[] resourcename5 = null;
	private String[] resourcename6 = null;
	private String[] resourcename7 = null;
	private String[] resourcename8 = null;
	private String[] resourcename9 = null;
	private String[] resourcename10 = null;
	private String[] resourcename11 = null;
	private String[] resourcename12 = null;
	private String[] resourcename13 = null;
	private String[] resourcename14 = null;
	private String[] resourcename15 = null;
	private String[] resourcename16 = null;
	private String[] resourcename17 = null;
	private String[] resourcename18 = null;
	private String[] resourcename19 = null;
	private String[] resourcename20 = null;
	private String[] resourcename21 = null;
	private String[] resourcename22 = null;
	private String[] resourcename23 = null;
	private String[] resourcename24 = null;
	private String[] resourcename25 = null;
	private String[] resourcename26 = null;
	private String[] resourcename27 = null;
	private String[] resourcename28 = null;
	private String[] resourcename29 = null;
	private String[] resourcename30 = null;

	private String refclr = null;

	private String showAppendixId = null;
	private String showAppendixName = null;
	private String fromPage = null;

	/* Form Element for New CustomerLabor Rate */
	String editableView = null;

	public String getEditableView() {
		return editableView;
	}

	public void setEditableView(String editableView) {
		this.editableView = editableView;
	}

	String[] dispatchRules0 = null;
	String[] dispatchRules1 = null;
	String[] dispatchRules2 = null;
	String[] dispatchRules3 = null;
	String[] dispatchRules4 = null;

	String[] dispatchRateDiscount = null;
	String[] dispatchRateLabels = null;

	String[] cftLevel0 = null;
	String[] cftLevel1 = null;
	String[] cftLevel2 = null;
	String[] cftLevel3 = null;
	String[] cftLevel4 = null;
	String[] cftLevel5 = null;
	String[] cftLevel6 = null;
	String[] cftLevel7 = null;
	String[] cftLevel8 = null;
	String[] cftLevel9 = null;
	String[] cftLevel10 = null;
	String[] cftLevel11 = null;
	String[] cftLevel12 = null;
	String[] cftLevel13 = null;
	String[] cftLevel14 = null;
	String[] cftLevel15 = null;

	public String[] getCftLevel0() {
		return cftLevel0;
	}

	public void setCftLevel0(String[] cftLevel0) {
		this.cftLevel0 = cftLevel0;
	}

	public String[] getCftLevel1() {
		return cftLevel1;
	}

	public void setCftLevel1(String[] cftLevel1) {
		this.cftLevel1 = cftLevel1;
	}

	public String[] getCftLevel2() {
		return cftLevel2;
	}

	public void setCftLevel2(String[] cftLevel2) {
		this.cftLevel2 = cftLevel2;
	}

	public String[] getCftLevel3() {
		return cftLevel3;
	}

	public void setCftLevel3(String[] cftLevel3) {
		this.cftLevel3 = cftLevel3;
	}

	public String[] getCftLevel4() {
		return cftLevel4;
	}

	public void setCftLevel4(String[] cftLevel4) {
		this.cftLevel4 = cftLevel4;
	}

	public String[] getCftLevel5() {
		return cftLevel5;
	}

	public void setCftLevel5(String[] cftLevel5) {
		this.cftLevel5 = cftLevel5;
	}

	public String[] getCftLevel6() {
		return cftLevel6;
	}

	public void setCftLevel6(String[] cftLevel6) {
		this.cftLevel6 = cftLevel6;
	}

	public String[] getCftLevel7() {
		return cftLevel7;
	}

	public void setCftLevel7(String[] cftLevel7) {
		this.cftLevel7 = cftLevel7;
	}

	public String[] getCftLevel8() {
		return cftLevel8;
	}

	public void setCftLevel8(String[] cftLevel8) {
		this.cftLevel8 = cftLevel8;
	}

	public String[] getCftLevel9() {
		return cftLevel9;
	}

	public void setCftLevel9(String[] cftLevel9) {
		this.cftLevel9 = cftLevel9;
	}

	public String[] getCftLevel10() {
		return cftLevel10;
	}

	public void setCftLevel10(String[] cftLevel10) {
		this.cftLevel10 = cftLevel10;
	}

	public String[] getCftLevel11() {
		return cftLevel11;
	}

	public void setCftLevel11(String[] cftLevel11) {
		this.cftLevel11 = cftLevel11;
	}

	public String[] getCftLevel12() {
		return cftLevel12;
	}

	public void setCftLevel12(String[] cftLevel12) {
		this.cftLevel12 = cftLevel12;
	}

	public String[] getCftLevel13() {
		return cftLevel13;
	}

	public void setCftLevel13(String[] cftLevel13) {
		this.cftLevel13 = cftLevel13;
	}

	public String[] getCftLevel14() {
		return cftLevel14;
	}

	public void setCftLevel14(String[] cftLevel14) {
		this.cftLevel14 = cftLevel14;
	}

	public String[] getCftLevel15() {
		return cftLevel15;
	}

	public void setCftLevel15(String[] cftLevel15) {
		this.cftLevel15 = cftLevel15;
	}

	public String[] getCftLevel16() {
		return cftLevel16;
	}

	public void setCftLevel16(String[] cftLevel16) {
		this.cftLevel16 = cftLevel16;
	}

	public String[] getCftLevel17() {
		return cftLevel17;
	}

	public void setCftLevel17(String[] cftLevel17) {
		this.cftLevel17 = cftLevel17;
	}

	public String[] getCftLevel18() {
		return cftLevel18;
	}

	public void setCftLevel18(String[] cftLevel18) {
		this.cftLevel18 = cftLevel18;
	}

	public String[] getCftLevel19() {
		return cftLevel19;
	}

	public void setCftLevel19(String[] cftLevel19) {
		this.cftLevel19 = cftLevel19;
	}

	public String[] getCftLevel20() {
		return cftLevel20;
	}

	public void setCftLevel20(String[] cftLevel20) {
		this.cftLevel20 = cftLevel20;
	}

	public String[] getCftLevel21() {
		return cftLevel21;
	}

	public void setCftLevel21(String[] cftLevel21) {
		this.cftLevel21 = cftLevel21;
	}

	public String[] getCftLevel22() {
		return cftLevel22;
	}

	public void setCftLevel22(String[] cftLevel22) {
		this.cftLevel22 = cftLevel22;
	}

	public String[] getCftLevel23() {
		return cftLevel23;
	}

	public void setCftLevel23(String[] cftLevel23) {
		this.cftLevel23 = cftLevel23;
	}

	String[] cftLevel16 = null;
	String[] cftLevel17 = null;
	String[] cftLevel18 = null;
	String[] cftLevel19 = null;
	String[] cftLevel20 = null;
	String[] cftLevel21 = null;
	String[] cftLevel22 = null;
	String[] cftLevel23 = null;

	public String[] getDispatchRateDiscount() {
		return dispatchRateDiscount;
	}

	public void setDispatchRateDiscount(String[] dispatchRateDiscount) {
		this.dispatchRateDiscount = dispatchRateDiscount;
	}

	public String[] getDispatchRateLabels() {
		return dispatchRateLabels;
	}

	public void setDispatchRateLabels(String[] dispatchRateLabels) {
		this.dispatchRateLabels = dispatchRateLabels;
	}

	public String[] getDispatchRules1() {
		return dispatchRules1;
	}

	public void setDispatchRules1(String[] dispatchRules1) {
		this.dispatchRules1 = dispatchRules1;
	}

	public String[] getDispatchRules2() {
		return dispatchRules2;
	}

	public void setDispatchRules2(String[] dispatchRules2) {
		this.dispatchRules2 = dispatchRules2;
	}

	public String[] getDispatchRules3() {
		return dispatchRules3;
	}

	public void setDispatchRules3(String[] dispatchRules3) {
		this.dispatchRules3 = dispatchRules3;
	}

	public String[] getDispatchRules4() {
		return dispatchRules4;
	}

	public void setDispatchRules4(String[] dispatchRules4) {
		this.dispatchRules4 = dispatchRules4;
	}

	public String[] getDispatchRules0() {
		return dispatchRules0;
	}

	public void setDispatchRules0(String[] dispatchRules0) {
		this.dispatchRules0 = dispatchRules0;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String[] getClrrate0() {
		return clrrate0;
	}

	public String getClrrate0(int i) {
		return clrrate0[i];
	}

	public void setClrrate0(String[] clrrate0) {
		this.clrrate0 = clrrate0;
	}

	public String[] getClrrate1() {
		return clrrate1;
	}

	public String getClrrate1(int i) {
		return clrrate1[i];
	}

	public void setClrrate1(String[] clrrate1) {
		this.clrrate1 = clrrate1;
	}

	public String[] getClrrate10() {
		return clrrate10;
	}

	public String getClrrate10(int i) {
		return clrrate10[i];
	}

	public void setClrrate10(String[] clrrate10) {
		this.clrrate10 = clrrate10;
	}

	public String[] getClrrate11() {
		return clrrate11;
	}

	public String getClrrate11(int i) {
		return clrrate11[i];
	}

	public void setClrrate11(String[] clrrate11) {
		this.clrrate11 = clrrate11;
	}

	public String[] getClrrate12() {
		return clrrate12;
	}

	public String getClrrate12(int i) {
		return clrrate12[i];
	}

	public void setClrrate12(String[] clrrate12) {
		this.clrrate12 = clrrate12;
	}

	public String[] getClrrate13() {
		return clrrate13;
	}

	public String getClrrate13(int i) {
		return clrrate13[i];
	}

	public void setClrrate13(String[] clrrate13) {
		this.clrrate13 = clrrate13;
	}

	public String[] getClrrate14() {
		return clrrate14;
	}

	public String getClrrate14(int i) {
		return clrrate14[i];
	}

	public void setClrrate14(String[] clrrate14) {
		this.clrrate14 = clrrate14;
	}

	public String[] getClrrate15() {
		return clrrate15;
	}

	public String getClrrate15(int i) {
		return clrrate15[i];
	}

	public void setClrrate15(String[] clrrate15) {
		this.clrrate15 = clrrate15;
	}

	public String[] getClrrate16() {
		return clrrate16;
	}

	public String getClrrate16(int i) {
		return clrrate16[i];
	}

	public void setClrrate16(String[] clrrate16) {
		this.clrrate16 = clrrate16;
	}

	public String[] getClrrate17() {
		return clrrate17;
	}

	public String getClrrate17(int i) {
		return clrrate17[i];
	}

	public void setClrrate17(String[] clrrate17) {
		this.clrrate17 = clrrate17;
	}

	public String[] getClrrate18() {
		return clrrate18;
	}

	public String getClrrate18(int i) {
		return clrrate18[i];
	}

	public void setClrrate18(String[] clrrate18) {
		this.clrrate18 = clrrate18;
	}

	public String[] getClrrate19() {
		return clrrate19;
	}

	public String getClrrate19(int i) {
		return clrrate19[i];
	}

	public void setClrrate19(String[] clrrate19) {
		this.clrrate19 = clrrate19;
	}

	public String[] getClrrate2() {
		return clrrate2;
	}

	public String getClrrate2(int i) {
		return clrrate2[i];
	}

	public void setClrrate2(String[] clrrate2) {
		this.clrrate2 = clrrate2;
	}

	public String[] getClrrate3() {
		return clrrate3;
	}

	public String getClrrate3(int i) {
		return clrrate3[i];
	}

	public void setClrrate3(String[] clrrate3) {
		this.clrrate3 = clrrate3;
	}

	public String[] getClrrate4() {
		return clrrate4;
	}

	public String getClrrate4(int i) {
		return clrrate4[i];
	}

	public void setClrrate4(String[] clrrate4) {
		this.clrrate4 = clrrate4;
	}

	public String[] getClrrate5() {
		return clrrate5;
	}

	public String getClrrate5(int i) {
		return clrrate5[i];
	}

	public void setClrrate5(String[] clrrate5) {
		this.clrrate5 = clrrate5;
	}

	public String[] getClrrate6() {
		return clrrate6;
	}

	public String getClrrate6(int i) {
		return clrrate6[i];
	}

	public void setClrrate6(String[] clrrate6) {
		this.clrrate6 = clrrate6;
	}

	public String[] getClrrate7() {
		return clrrate7;
	}

	public String getClrrate7(int i) {
		return clrrate7[i];
	}

	public void setClrrate7(String[] clrrate7) {
		this.clrrate7 = clrrate7;
	}

	public String[] getClrrate8() {
		return clrrate8;
	}

	public String getClrrate8(int i) {
		return clrrate8[i];
	}

	public void setClrrate8(String[] clrrate8) {
		this.clrrate8 = clrrate8;
	}

	public String[] getClrrate9() {
		return clrrate9;
	}

	public String getClrrate9(int i) {
		return clrrate9[i];
	}

	public void setClrrate9(String[] clrrate9) {
		this.clrrate9 = clrrate9;
	}

	public String[] getDetailid0() {
		return detailid0;
	}

	public String getDetailid0(int i) {
		return detailid0[i];
	}

	public void setDetailid0(String[] detailid0) {
		this.detailid0 = detailid0;
	}

	public String[] getDetailid1() {
		return detailid1;
	}

	public String getDetailid1(int i) {
		return detailid1[i];
	}

	public void setDetailid1(String[] detailid1) {
		this.detailid1 = detailid1;
	}

	public String[] getDetailid10() {
		return detailid10;
	}

	public String getDetailid10(int i) {
		return detailid10[i];
	}

	public void setDetailid10(String[] detailid10) {
		this.detailid10 = detailid10;
	}

	public String[] getDetailid11() {
		return detailid11;
	}

	public String getDetailid11(int i) {
		return detailid11[i];
	}

	public void setDetailid11(String[] detailid11) {
		this.detailid11 = detailid11;
	}

	public String[] getDetailid12() {
		return detailid12;
	}

	public String getDetailid12(int i) {
		return detailid12[i];
	}

	public void setDetailid12(String[] detailid12) {
		this.detailid12 = detailid12;
	}

	public String[] getDetailid13() {
		return detailid13;
	}

	public String getDetailid13(int i) {
		return detailid13[i];
	}

	public void setDetailid13(String[] detailid13) {
		this.detailid13 = detailid13;
	}

	public String[] getDetailid14() {
		return detailid14;
	}

	public String getDetailid14(int i) {
		return detailid14[i];
	}

	public void setDetailid14(String[] detailid14) {
		this.detailid14 = detailid14;
	}

	public String[] getDetailid15() {
		return detailid15;
	}

	public String getDetailid15(int i) {
		return detailid15[i];
	}

	public void setDetailid15(String[] detailid15) {
		this.detailid15 = detailid15;
	}

	public String[] getDetailid16() {
		return detailid16;
	}

	public String getDetailid16(int i) {
		return detailid16[i];
	}

	public void setDetailid16(String[] detailid16) {
		this.detailid16 = detailid16;
	}

	public String[] getDetailid17() {
		return detailid17;
	}

	public String getDetailid17(int i) {
		return detailid17[i];
	}

	public void setDetailid17(String[] detailid17) {
		this.detailid17 = detailid17;
	}

	public String[] getDetailid18() {
		return detailid18;
	}

	public String getDetailid18(int i) {
		return detailid18[i];
	}

	public void setDetailid18(String[] detailid18) {
		this.detailid18 = detailid18;
	}

	public String[] getDetailid19() {
		return detailid19;
	}

	public String getDetailid19(int i) {
		return detailid19[i];
	}

	public void setDetailid19(String[] detailid19) {
		this.detailid19 = detailid19;
	}

	public String[] getDetailid2() {
		return detailid2;
	}

	public String getDetailid2(int i) {
		return detailid2[i];
	}

	public void setDetailid2(String[] detailid2) {
		this.detailid2 = detailid2;
	}

	public String[] getDetailid3() {
		return detailid3;
	}

	public String getDetailid3(int i) {
		return detailid3[i];
	}

	public void setDetailid3(String[] detailid3) {
		this.detailid3 = detailid3;
	}

	public String[] getDetailid4() {
		return detailid4;
	}

	public String getDetailid4(int i) {
		return detailid4[i];
	}

	public void setDetailid4(String[] detailid4) {
		this.detailid4 = detailid4;
	}

	public String[] getDetailid5() {
		return detailid5;
	}

	public String getDetailid5(int i) {
		return detailid5[i];
	}

	public void setDetailid5(String[] detailid5) {
		this.detailid5 = detailid5;
	}

	public String[] getDetailid6() {
		return detailid6;
	}

	public String getDetailid6(int i) {
		return detailid6[i];
	}

	public void setDetailid6(String[] detailid6) {
		this.detailid6 = detailid6;
	}

	public String[] getDetailid7() {
		return detailid7;
	}

	public String getDetailid7(int i) {
		return detailid7[i];
	}

	public void setDetailid7(String[] detailid7) {
		this.detailid7 = detailid7;
	}

	public String[] getDetailid8() {
		return detailid8;
	}

	public String getDetailid8(int i) {
		return detailid8[i];
	}

	public void setDetailid8(String[] detailid8) {
		this.detailid8 = detailid8;
	}

	public String[] getDetailid9() {
		return detailid9;
	}

	public String getDetailid9(int i) {
		return detailid9[i];
	}

	public void setDetailid9(String[] detailid9) {
		this.detailid9 = detailid9;
	}

	public String[] getResourceid0() {
		return resourceid0;
	}

	public String getResourceid0(int i) {
		return resourceid0[i];
	}

	public void setResourceid0(String[] resourceid0) {
		this.resourceid0 = resourceid0;
	}

	public String[] getResourceid1() {
		return resourceid1;
	}

	public String getResourceid1(int i) {
		return resourceid1[i];
	}

	public void setResourceid1(String[] resourceid1) {
		this.resourceid1 = resourceid1;
	}

	public String[] getResourceid10() {
		return resourceid10;
	}

	public String getResourceid10(int i) {
		return resourceid10[i];
	}

	public void setResourceid10(String[] resourceid10) {
		this.resourceid10 = resourceid10;
	}

	public String[] getResourceid11() {
		return resourceid11;
	}

	public String getResourceid11(int i) {
		return resourceid11[i];
	}

	public void setResourceid11(String[] resourceid11) {
		this.resourceid11 = resourceid11;
	}

	public String[] getResourceid12() {
		return resourceid12;
	}

	public String getResourceid12(int i) {
		return resourceid12[i];
	}

	public void setResourceid12(String[] resourceid12) {
		this.resourceid12 = resourceid12;
	}

	public String[] getResourceid13() {
		return resourceid13;
	}

	public String getResourceid13(int i) {
		return resourceid13[i];
	}

	public void setResourceid13(String[] resourceid13) {
		this.resourceid13 = resourceid13;
	}

	public String[] getResourceid14() {
		return resourceid14;
	}

	public String getResourceid14(int i) {
		return resourceid14[i];
	}

	public void setResourceid14(String[] resourceid14) {
		this.resourceid14 = resourceid14;
	}

	public String[] getResourceid15() {
		return resourceid15;
	}

	public String getResourceid15(int i) {
		return resourceid15[i];
	}

	public void setResourceid15(String[] resourceid15) {
		this.resourceid15 = resourceid15;
	}

	public String[] getResourceid16() {
		return resourceid16;
	}

	public String getResourceid16(int i) {
		return resourceid16[i];
	}

	public void setResourceid16(String[] resourceid16) {
		this.resourceid16 = resourceid16;
	}

	public String[] getResourceid17() {
		return resourceid17;
	}

	public String getResourceid17(int i) {
		return resourceid17[i];
	}

	public void setResourceid17(String[] resourceid17) {
		this.resourceid17 = resourceid17;
	}

	public String[] getResourceid18() {
		return resourceid18;
	}

	public String getResourceid18(int i) {
		return resourceid18[i];
	}

	public void setResourceid18(String[] resourceid18) {
		this.resourceid18 = resourceid18;
	}

	public String[] getResourceid19() {
		return resourceid19;
	}

	public String getResourceid19(int i) {
		return resourceid19[i];
	}

	public void setResourceid19(String[] resourceid19) {
		this.resourceid19 = resourceid19;
	}

	public String[] getResourceid2() {
		return resourceid2;
	}

	public String getResourceid2(int i) {
		return resourceid2[i];
	}

	public void setResourceid2(String[] resourceid2) {
		this.resourceid2 = resourceid2;
	}

	public String[] getResourceid3() {
		return resourceid3;
	}

	public String getResourceid3(int i) {
		return resourceid3[i];
	}

	public void setResourceid3(String[] resourceid3) {
		this.resourceid3 = resourceid3;
	}

	public String[] getResourceid4() {
		return resourceid4;
	}

	public String getResourceid4(int i) {
		return resourceid4[i];
	}

	public void setResourceid4(String[] resourceid4) {
		this.resourceid4 = resourceid4;
	}

	public String[] getResourceid5() {
		return resourceid5;
	}

	public String getResourceid5(int i) {
		return resourceid5[i];
	}

	public void setResourceid5(String[] resourceid5) {
		this.resourceid5 = resourceid5;
	}

	public String[] getResourceid6() {
		return resourceid6;
	}

	public String getResourceid6(int i) {
		return resourceid6[i];
	}

	public void setResourceid6(String[] resourceid6) {
		this.resourceid6 = resourceid6;
	}

	public String[] getResourceid7() {
		return resourceid7;
	}

	public String getResourceid7(int i) {
		return resourceid7[i];
	}

	public void setResourceid7(String[] resourceid7) {
		this.resourceid7 = resourceid7;
	}

	public String[] getResourceid8() {
		return resourceid8;
	}

	public String getResourceid8(int i) {
		return resourceid8[i];
	}

	public void setResourceid8(String[] resourceid8) {
		this.resourceid8 = resourceid8;
	}

	public String[] getResourceid9() {
		return resourceid9;
	}

	public String getResourceid9(int i) {
		return resourceid9[i];
	}

	public void setResourceid9(String[] resourceid9) {
		this.resourceid9 = resourceid9;
	}

	public Collection getReslist() {
		return reslist;
	}

	public void setReslist(Collection reslist) {
		this.reslist = reslist;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getAddmessage() {
		return addmessage;
	}

	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}

	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getEffectivedate() {
		return effectivedate;
	}

	public void setEffectivedate(String effectivedate) {
		this.effectivedate = effectivedate;
	}

	public String getMinimumfee() {
		return minimumfee;
	}

	public void setMinimumfee(String minimumfee) {
		this.minimumfee = minimumfee;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getTravel() {
		return travel;
	}

	public void setTravel(String travel) {
		this.travel = travel;
	}

	public String getTravelmax() {
		return travelmax;
	}

	public void setTravelmax(String travelmax) {
		this.travelmax = travelmax;
	}

	public String getReset() {
		return reset;
	}

	public void setReset(String reset) {
		this.reset = reset;
	}

	public String getNoofengineers() {
		return noofengineers;
	}

	public void setNoofengineers(String noofengineers) {
		this.noofengineers = noofengineers;
	}

	public String getNooftechnician() {
		return nooftechnician;
	}

	public void setNooftechnician(String nooftechnician) {
		this.nooftechnician = nooftechnician;
	}

	public String getActivitylibrary_Id() {
		return activitylibrary_Id;
	}

	public void setActivitylibrary_Id(String activitylibrary_Id) {
		this.activitylibrary_Id = activitylibrary_Id;
	}

	public String getMsa_Id() {
		return msa_Id;
	}

	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		customername = null;
		minimumfee = null;
		travel = null;
		travelmax = null;
		discount = null;
		effectivedate = null;
		rules = null;
		save = null;
		reset = null;
		addmessage = null;

	}

	public String[] getCriticalityid() {
		return criticalityid;
	}

	public void setCriticalityid(String[] criticalityid) {
		this.criticalityid = criticalityid;
	}

	public String getCriticalityid(int i) {
		return criticalityid[i];
	}

	public String[] getCriticalityname() {
		return criticalityname;
	}

	public void setCriticalityname(String[] criticalityname) {
		this.criticalityname = criticalityname;
	}

	public String getCriticalityname(int i) {
		return criticalityname[i];
	}

	public String[] getDescription() {
		return description;
	}

	public void setDescription(String[] description) {
		this.description = description;
	}

	public String getDescription(int i) {
		return description[i];
	}

	public String[] getPpstype() {
		return ppstype;
	}

	public void setPpstype(String[] ppstype) {
		this.ppstype = ppstype;
	}

	public String getPpstype(int i) {
		return ppstype[i];
	}

	public String[] getTechengg0() {
		return techengg0;
	}

	public void setTechengg0(String[] techengg0) {
		this.techengg0 = techengg0;
	}

	public String getTechengg0(int i) {
		return techengg0[i];
	}

	public String[] getTechengg1() {
		return techengg1;
	}

	public void setTechengg1(String[] techengg1) {
		this.techengg1 = techengg1;
	}

	public String getTechengg1(int i) {
		return techengg1[i];
	}

	public String[] getTechengg2() {
		return techengg2;
	}

	public void setTechengg2(String[] techengg2) {
		this.techengg2 = techengg2;
	}

	public String getTechengg2(int i) {
		return techengg2[i];
	}

	public String[] getTechengg3() {
		return techengg3;
	}

	public void setTechengg3(String[] techengg3) {
		this.techengg3 = techengg3;
	}

	public String getTechengg3(int i) {
		return techengg3[i];
	}

	public String[] getTechengg4() {
		return techengg4;
	}

	public void setTechengg4(String[] techengg4) {
		this.techengg4 = techengg4;
	}

	public String getTechengg4(int i) {
		return techengg4[i];
	}

	public String[] getTechengg5() {
		return techengg5;
	}

	public void setTechengg5(String[] techengg5) {
		this.techengg5 = techengg5;
	}

	public String getTechengg5(int i) {
		return techengg5[i];
	}

	public String[] getTechengg6() {
		return techengg6;
	}

	public void setTechengg6(String[] techengg6) {
		this.techengg6 = techengg6;
	}

	public String getTechengg6(int i) {
		return techengg6[i];
	}

	public String[] getTechengg7() {
		return techengg7;
	}

	public void setTechengg7(String[] techengg7) {
		this.techengg7 = techengg7;
	}

	public String getTechengg7(int i) {
		return techengg7[i];
	}

	public String[] getTechengg8() {
		return techengg8;
	}

	public void setTechengg8(String[] techengg8) {
		this.techengg8 = techengg8;
	}

	public String getTechengg8(int i) {
		return techengg8[i];
	}

	public String[] getTechengg9() {
		return techengg9;
	}

	public void setTechengg9(String[] techengg9) {
		this.techengg9 = techengg9;
	}

	public String getTechengg9(int i) {
		return techengg9[i];
	}

	public String[] getTechenggid0() {
		return techenggid0;
	}

	public void setTechenggid0(String[] techenggid0) {
		this.techenggid0 = techenggid0;
	}

	public String getTechenggid0(int i) {
		return techenggid0[i];
	}

	public String[] getTechenggid1() {
		return techenggid1;
	}

	public void setTechenggid1(String[] techenggid1) {
		this.techenggid1 = techenggid1;
	}

	public String getTechenggid1(int i) {
		return techenggid1[i];
	}

	public String[] getTechenggid2() {
		return techenggid2;
	}

	public void setTechenggid2(String[] techenggid2) {
		this.techenggid2 = techenggid2;
	}

	public String getTechenggid2(int i) {
		return techenggid2[i];
	}

	public String[] getTechenggid3() {
		return techenggid3;
	}

	public void setTechenggid3(String[] techenggid3) {
		this.techenggid3 = techenggid3;
	}

	public String getTechenggid3(int i) {
		return techenggid3[i];
	}

	public String[] getTechenggid4() {
		return techenggid4;
	}

	public void setTechenggid4(String[] techenggid4) {
		this.techenggid4 = techenggid4;
	}

	public String getTechenggid4(int i) {
		return techenggid4[i];
	}

	public String[] getTechenggid5() {
		return techenggid5;
	}

	public void setTechenggid5(String[] techenggid5) {
		this.techenggid5 = techenggid5;
	}

	public String getTechenggid5(int i) {
		return techenggid5[i];
	}

	public String[] getTechenggid6() {
		return techenggid6;
	}

	public void setTechenggid6(String[] techenggid6) {
		this.techenggid6 = techenggid6;
	}

	public String getTechenggid6(int i) {
		return techenggid6[i];
	}

	public String[] getTechenggid7() {
		return techenggid7;
	}

	public void setTechenggid7(String[] techenggid7) {
		this.techenggid7 = techenggid7;
	}

	public String getTechenggid7(int i) {
		return techenggid7[i];
	}

	public String[] getTechenggid8() {
		return techenggid8;
	}

	public void setTechenggid8(String[] techenggid8) {
		this.techenggid8 = techenggid8;
	}

	public String getTechenggid8(int i) {
		return techenggid8[i];
	}

	public String[] getTechenggid9() {
		return techenggid9;
	}

	public void setTechenggid9(String[] techenggid9) {
		this.techenggid9 = techenggid9;
	}

	public String getTechenggid9(int i) {
		return techenggid9[i];
	}

	public String[] getClassname() {
		return classname;
	}

	public String getClassname(int i) {
		return classname[i];
	}

	public void setClassname(String[] classname) {
		this.classname = classname;
	}

	public String[] getClassnums() {
		return classnums;
	}

	public String getClassnums(int i) {
		return classnums[i];
	}

	public void setClassnums(String[] classnums) {
		this.classnums = classnums;
	}

	public String[] getResourcename() {
		return resourcename;
	}

	public String getResourcename(int i) {
		return resourcename[i];
	}

	public void setResourcename(String[] resourcename) {
		this.resourcename = resourcename;
	}

	public String[] getCategoryname() {
		return categoryname;
	}

	public String getCategoryname(int i) {
		return categoryname[i];
	}

	public void setCategoryname(String[] categoryname) {
		this.categoryname = categoryname;
	}

	public String[] getClrrate() {
		return clrrate;
	}

	public String getClrrate(int i) {
		return clrrate[i];
	}

	public void setClrrate(String[] clrrate) {
		this.clrrate = clrrate;
	}

	public String[] getDetailid() {
		return detailid;
	}

	public String getDetailid(int i) {
		return detailid[i];
	}

	public void setDetailid(String[] detailid) {
		this.detailid = detailid;
	}

	public String[] getResourceid() {
		return resourceid;
	}

	public String getResourceid(int i) {
		return resourceid[i];
	}

	public void setResourceid(String[] resourceid) {
		this.resourceid = resourceid;
	}

	public String[] getServicetime() {
		return servicetime;
	}

	public String getServicetime(int i) {
		return servicetime[i];
	}

	public void setServicetime(String[] servicetime) {
		this.servicetime = servicetime;
	}

	public String getCatid() {
		return catid;
	}

	public void setCatid(String catid) {
		this.catid = catid;
	}

	public String[] getSelectresourceid0() {
		return selectresourceid0;
	}

	public String getSelectresourceid0(int i) {
		return selectresourceid0[i];
	}

	public void setSelectresourceid0(String[] selectresourceid0) {
		this.selectresourceid0 = selectresourceid0;
	}

	public String[] getSelectresourceid1() {
		return selectresourceid1;
	}

	public String getSelectresourceid1(int i) {
		return selectresourceid0[i];
	}

	public void setSelectresourceid1(String[] selectresourceid1) {
		this.selectresourceid1 = selectresourceid1;
	}

	public String[] getSelectresourceid10() {
		return selectresourceid10;
	}

	public String getSelectresourceid10(int i) {
		return selectresourceid10[i];
	}

	public void setSelectresourceid10(String[] selectresourceid10) {
		this.selectresourceid10 = selectresourceid10;
	}

	public String[] getSelectresourceid11() {
		return selectresourceid11;
	}

	public String getSelectresourceid11(int i) {
		return selectresourceid11[i];
	}

	public void setSelectresourceid11(String[] selectresourceid11) {
		this.selectresourceid11 = selectresourceid11;
	}

	public String[] getSelectresourceid12() {
		return selectresourceid12;
	}

	public String getSelectresourceid12(int i) {
		return selectresourceid12[i];
	}

	public void setSelectresourceid12(String[] selectresourceid12) {
		this.selectresourceid12 = selectresourceid12;
	}

	public String[] getSelectresourceid13() {
		return selectresourceid13;
	}

	public String getSelectresourceid13(int i) {
		return selectresourceid13[i];
	}

	public void setSelectresourceid13(String[] selectresourceid13) {
		this.selectresourceid13 = selectresourceid13;
	}

	public String[] getSelectresourceid14() {
		return selectresourceid14;
	}

	public String getSelectresourceid14(int i) {
		return selectresourceid14[i];
	}

	public void setSelectresourceid14(String[] selectresourceid14) {
		this.selectresourceid14 = selectresourceid14;
	}

	public String[] getSelectresourceid15() {
		return selectresourceid15;
	}

	public String getSelectresourceid15(int i) {
		return selectresourceid15[i];
	}

	public void setSelectresourceid15(String[] selectresourceid15) {
		this.selectresourceid15 = selectresourceid15;
	}

	public String[] getSelectresourceid16() {
		return selectresourceid16;
	}

	public String getSelectresourceid16(int i) {
		return selectresourceid16[i];
	}

	public void setSelectresourceid16(String[] selectresourceid16) {
		this.selectresourceid16 = selectresourceid16;
	}

	public String[] getSelectresourceid17() {
		return selectresourceid17;
	}

	public String getSelectresourceid17(int i) {
		return selectresourceid17[i];
	}

	public void setSelectresourceid17(String[] selectresourceid17) {
		this.selectresourceid17 = selectresourceid17;
	}

	public String[] getSelectresourceid18() {
		return selectresourceid18;
	}

	public String getSelectresourceid18(int i) {
		return selectresourceid18[i];
	}

	public void setSelectresourceid18(String[] selectresourceid18) {
		this.selectresourceid18 = selectresourceid18;
	}

	public String[] getSelectresourceid19() {
		return selectresourceid19;
	}

	public String getSelectresourceid19(int i) {
		return selectresourceid19[i];
	}

	public void setSelectresourceid19(String[] selectresourceid19) {
		this.selectresourceid19 = selectresourceid19;
	}

	public String[] getSelectresourceid2() {
		return selectresourceid2;
	}

	public String getSelectresourceid2(int i) {
		return selectresourceid2[i];
	}

	public void setSelectresourceid2(String[] selectresourceid2) {
		this.selectresourceid2 = selectresourceid2;
	}

	public String[] getSelectresourceid3() {
		return selectresourceid3;
	}

	public String getSelectresourceid3(int i) {
		return selectresourceid3[i];
	}

	public void setSelectresourceid3(String[] selectresourceid3) {
		this.selectresourceid3 = selectresourceid3;
	}

	public String[] getSelectresourceid4() {
		return selectresourceid4;
	}

	public String getSelectresourceid4(int i) {
		return selectresourceid4[i];
	}

	public void setSelectresourceid4(String[] selectresourceid4) {
		this.selectresourceid4 = selectresourceid4;
	}

	public String[] getSelectresourceid5() {
		return selectresourceid5;
	}

	public String getSelectresourceid5(int i) {
		return selectresourceid5[i];
	}

	public void setSelectresourceid5(String[] selectresourceid5) {
		this.selectresourceid5 = selectresourceid5;
	}

	public String[] getSelectresourceid6() {
		return selectresourceid6;
	}

	public String getSelectresourceid6(int i) {
		return selectresourceid6[i];
	}

	public void setSelectresourceid6(String[] selectresourceid6) {
		this.selectresourceid6 = selectresourceid6;
	}

	public String[] getSelectresourceid7() {
		return selectresourceid7;
	}

	public String getSelectresourceid7(int i) {
		return selectresourceid7[i];
	}

	public void setSelectresourceid7(String[] selectresourceid7) {
		this.selectresourceid7 = selectresourceid7;
	}

	public String[] getSelectresourceid8() {
		return selectresourceid8;
	}

	public String getSelectresourceid8(int i) {
		return selectresourceid8[i];
	}

	public void setSelectresourceid8(String[] selectresourceid8) {
		this.selectresourceid8 = selectresourceid8;
	}

	public String[] getSelectresourceid9() {
		return selectresourceid9;
	}

	public String getSelectresourceid9(int i) {
		return selectresourceid9[i];
	}

	public void setSelectresourceid9(String[] selectresourceid9) {
		this.selectresourceid9 = selectresourceid9;
	}

	public String[] getResourcename0() {
		return resourcename0;
	}

	public String getResourcename0(int i) {
		return resourcename0[i];
	}

	public void setResourcename0(String[] resourcename0) {
		this.resourcename0 = resourcename0;
	}

	public String[] getResourcename1() {
		return resourcename1;
	}

	public String getResourcename1(int i) {
		return resourcename0[i];
	}

	public void setResourcename1(String[] resourcename1) {
		this.resourcename1 = resourcename1;
	}

	public String[] getResourcename10() {
		return resourcename10;
	}

	public String getResourcename10(int i) {
		return resourcename10[i];
	}

	public void setResourcename10(String[] resourcename10) {
		this.resourcename10 = resourcename10;
	}

	public String[] getResourcename11() {
		return resourcename11;
	}

	public String getResourcename11(int i) {
		return resourcename11[i];
	}

	public void setResourcename11(String[] resourcename11) {
		this.resourcename11 = resourcename11;
	}

	public String[] getResourcename12() {
		return resourcename12;
	}

	public String getResourcename12(int i) {
		return resourcename12[i];
	}

	public void setResourcename12(String[] resourcename12) {
		this.resourcename12 = resourcename12;
	}

	public String[] getResourcename13() {
		return resourcename13;
	}

	public String getResourcename13(int i) {
		return resourcename13[i];
	}

	public void setResourcename13(String[] resourcename13) {
		this.resourcename13 = resourcename13;
	}

	public String[] getResourcename14() {
		return resourcename14;
	}

	public String getResourcename14(int i) {
		return resourcename14[i];
	}

	public void setResourcename14(String[] resourcename14) {
		this.resourcename14 = resourcename14;
	}

	public String[] getResourcename15() {
		return resourcename15;
	}

	public String getResourcename15(int i) {
		return resourcename15[i];
	}

	public void setResourcename15(String[] resourcename15) {
		this.resourcename15 = resourcename15;
	}

	public String[] getResourcename16() {
		return resourcename16;
	}

	public String getResourcename16(int i) {
		return resourcename16[i];
	}

	public void setResourcename16(String[] resourcename16) {
		this.resourcename16 = resourcename16;
	}

	public String[] getResourcename17() {
		return resourcename17;
	}

	public String getResourcename17(int i) {
		return resourcename17[i];
	}

	public void setResourcename17(String[] resourcename17) {
		this.resourcename17 = resourcename17;
	}

	public String[] getResourcename18() {
		return resourcename18;
	}

	public String getResourcename18(int i) {
		return resourcename18[i];
	}

	public void setResourcename18(String[] resourcename18) {
		this.resourcename18 = resourcename18;
	}

	public String[] getResourcename19() {
		return resourcename19;
	}

	public String getResourcename19(int i) {
		return resourcename19[i];
	}

	public void setResourcename19(String[] resourcename19) {
		this.resourcename19 = resourcename19;
	}

	public String[] getResourcename2() {
		return resourcename2;
	}

	public String getResourcename2(int i) {
		return resourcename2[i];
	}

	public void setResourcename2(String[] resourcename2) {
		this.resourcename2 = resourcename2;
	}

	public String[] getResourcename3() {
		return resourcename3;
	}

	public String getResourcename3(int i) {
		return resourcename3[i];
	}

	public void setResourcename3(String[] resourcename3) {
		this.resourcename3 = resourcename3;
	}

	public String[] getResourcename4() {
		return resourcename4;
	}

	public String getResourcename4(int i) {
		return resourcename4[i];
	}

	public void setResourcename4(String[] resourcename4) {
		this.resourcename4 = resourcename4;
	}

	public String[] getResourcename5() {
		return resourcename5;
	}

	public String getResourcename5(int i) {
		return resourcename5[i];
	}

	public void setResourcename5(String[] resourcename5) {
		this.resourcename5 = resourcename5;
	}

	public String[] getResourcename6() {
		return resourcename6;
	}

	public String getResourcename6(int i) {
		return resourcename6[i];
	}

	public void setResourcename6(String[] resourcename6) {
		this.resourcename6 = resourcename6;
	}

	public String[] getResourcename7() {
		return resourcename7;
	}

	public String getResourcename7(int i) {
		return resourcename7[i];
	}

	public void setResourcename7(String[] resourcename7) {
		this.resourcename7 = resourcename7;
	}

	public String[] getResourcename8() {
		return resourcename8;
	}

	public String getResourcename8(int i) {
		return resourcename8[i];
	}

	public void setResourcename8(String[] resourcename8) {
		this.resourcename8 = resourcename8;
	}

	public String[] getResourcename9() {
		return resourcename9;
	}

	public String getResourcename9(int i) {
		return resourcename9[i];
	}

	public void setResourcename9(String[] resourcename9) {
		this.resourcename9 = resourcename9;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	public String getRefclr() {
		return refclr;
	}

	public void setRefclr(String refclr) {
		this.refclr = refclr;
	}

	public String[] getCategoryshortname() {
		return categoryshortname;
	}

	public void setCategoryshortname(String[] categoryshortname) {
		this.categoryshortname = categoryshortname;
	}

	public String[] getCriticalitydesc() {
		return criticalitydesc;
	}

	public void setCriticalitydesc(String[] criticalitydesc) {
		this.criticalitydesc = criticalitydesc;
	}

	public String[] getClrrate20() {
		return clrrate20;
	}

	public String getClrrate20(int i) {
		return clrrate20[i];
	}

	public void setClrrate20(String[] clrrate20) {
		this.clrrate20 = clrrate20;
	}

	public String[] getClrrate21() {
		return clrrate21;
	}

	public String getClrrate21(int i) {
		return clrrate21[i];
	}

	public void setClrrate21(String[] clrrate21) {
		this.clrrate21 = clrrate21;
	}

	public String[] getClrrate22() {
		return clrrate22;
	}

	public String getClrrate22(int i) {
		return clrrate22[i];
	}

	public void setClrrate22(String[] clrrate22) {
		this.clrrate22 = clrrate22;
	}

	public String[] getClrrate23() {
		return clrrate23;
	}

	public String getClrrate23(int i) {
		return clrrate23[i];
	}

	public void setClrrate23(String[] clrrate23) {
		this.clrrate23 = clrrate23;
	}

	public String[] getClrrate24() {
		return clrrate24;
	}

	public String getClrrate24(int i) {
		return clrrate24[i];
	}

	public void setClrrate24(String[] clrrate24) {
		this.clrrate24 = clrrate24;
	}

	public String[] getClrrate25() {
		return clrrate25;
	}

	public String getClrrate25(int i) {
		return clrrate25[i];
	}

	public void setClrrate25(String[] clrrate25) {
		this.clrrate25 = clrrate25;
	}

	public String[] getClrrate26() {
		return clrrate26;
	}

	public String getClrrate26(int i) {
		return clrrate26[i];
	}

	public void setClrrate26(String[] clrrate26) {
		this.clrrate26 = clrrate26;
	}

	public String[] getClrrate27() {
		return clrrate27;
	}

	public String getClrrate27(int i) {
		return clrrate27[i];
	}

	public void setClrrate27(String[] clrrate27) {
		this.clrrate27 = clrrate27;
	}

	public String[] getClrrate28() {
		return clrrate28;
	}

	public String getClrrate28(int i) {
		return clrrate28[i];
	}

	public void setClrrate28(String[] clrrate28) {
		this.clrrate28 = clrrate28;
	}

	public String[] getClrrate29() {
		return clrrate29;
	}

	public String getClrrate29(int i) {
		return clrrate29[i];
	}

	public void setClrrate29(String[] clrrate29) {
		this.clrrate29 = clrrate29;
	}

	public String[] getClrrate30() {
		return clrrate30;
	}

	public String getClrrate30(int i) {
		return clrrate30[i];
	}

	public void setClrrate30(String[] clrrate30) {
		this.clrrate30 = clrrate30;
	}

	public String[] getDetailid20() {
		return detailid20;
	}

	public String getDetailid20(int i) {
		return detailid20[i];
	}

	public void setDetailid20(String[] detailid20) {
		this.detailid20 = detailid20;
	}

	public String[] getDetailid21() {
		return detailid21;
	}

	public String getDetailid21(int i) {
		return detailid21[i];
	}

	public void setDetailid21(String[] detailid21) {
		this.detailid21 = detailid21;
	}

	public String[] getDetailid22() {
		return detailid22;
	}

	public String getDetailid22(int i) {
		return detailid22[i];
	}

	public void setDetailid22(String[] detailid22) {
		this.detailid22 = detailid22;
	}

	public String[] getDetailid23() {
		return detailid23;
	}

	public String getDetailid23(int i) {
		return detailid23[i];
	}

	public void setDetailid23(String[] detailid23) {
		this.detailid23 = detailid23;
	}

	public String[] getDetailid24() {
		return detailid24;
	}

	public String getDetailid24(int i) {
		return detailid24[i];
	}

	public void setDetailid24(String[] detailid24) {
		this.detailid24 = detailid24;
	}

	public String[] getDetailid25() {
		return detailid25;
	}

	public String getDetailid25(int i) {
		return detailid25[i];
	}

	public void setDetailid25(String[] detailid25) {
		this.detailid25 = detailid25;
	}

	public String[] getDetailid26() {
		return detailid26;
	}

	public String getDetailid26(int i) {
		return detailid26[i];
	}

	public void setDetailid26(String[] detailid26) {
		this.detailid26 = detailid26;
	}

	public String[] getDetailid27() {
		return detailid27;
	}

	public String getDetailid27(int i) {
		return detailid27[i];
	}

	public void setDetailid27(String[] detailid27) {
		this.detailid27 = detailid27;
	}

	public String[] getDetailid28() {
		return detailid28;
	}

	public String getDetailid28(int i) {
		return detailid28[i];
	}

	public void setDetailid28(String[] detailid28) {
		this.detailid28 = detailid28;
	}

	public String[] getDetailid29() {
		return detailid29;
	}

	public String getDetailid29(int i) {
		return detailid29[i];
	}

	public void setDetailid29(String[] detailid29) {
		this.detailid29 = detailid29;
	}

	public String[] getDetailid30() {
		return detailid30;
	}

	public String getDetailid30(int i) {
		return detailid30[i];
	}

	public void setDetailid30(String[] detailid30) {
		this.detailid30 = detailid30;
	}

	public String[] getResourceid20() {
		return resourceid20;
	}

	public String getResourceid20(int i) {
		return resourceid20[i];
	}

	public void setResourceid20(String[] resourceid20) {
		this.resourceid20 = resourceid20;
	}

	public String[] getResourceid21() {
		return resourceid21;
	}

	public String getResourceid21(int i) {
		return resourceid21[i];
	}

	public void setResourceid21(String[] resourceid21) {
		this.resourceid21 = resourceid21;
	}

	public String[] getResourceid22() {
		return resourceid22;
	}

	public String getResourceid22(int i) {
		return resourceid22[i];
	}

	public void setResourceid22(String[] resourceid22) {
		this.resourceid22 = resourceid22;
	}

	public String[] getResourceid23() {
		return resourceid23;
	}

	public String getResourceid23(int i) {
		return resourceid23[i];
	}

	public void setResourceid23(String[] resourceid23) {
		this.resourceid23 = resourceid23;
	}

	public String[] getResourceid24() {
		return resourceid24;
	}

	public String getResourceid24(int i) {
		return resourceid24[i];
	}

	public void setResourceid24(String[] resourceid24) {
		this.resourceid24 = resourceid24;
	}

	public String[] getResourceid25() {
		return resourceid25;
	}

	public String getResourceid25(int i) {
		return resourceid25[i];
	}

	public void setResourceid25(String[] resourceid25) {
		this.resourceid25 = resourceid25;
	}

	public String[] getResourceid26() {
		return resourceid26;
	}

	public String getResourceid26(int i) {
		return resourceid26[i];
	}

	public void setResourceid26(String[] resourceid26) {
		this.resourceid26 = resourceid26;
	}

	public String[] getResourceid27() {
		return resourceid27;
	}

	public String getResourceid27(int i) {
		return resourceid27[i];
	}

	public void setResourceid27(String[] resourceid27) {
		this.resourceid27 = resourceid27;
	}

	public String[] getResourceid28() {
		return resourceid28;
	}

	public String getResourceid28(int i) {
		return resourceid28[i];
	}

	public void setResourceid28(String[] resourceid28) {
		this.resourceid28 = resourceid28;
	}

	public String[] getResourceid29() {
		return resourceid29;
	}

	public String getResourceid29(int i) {
		return resourceid29[i];
	}

	public void setResourceid29(String[] resourceid29) {
		this.resourceid29 = resourceid29;
	}

	public String[] getResourceid30() {
		return resourceid30;
	}

	public String getResourceid30(int i) {
		return resourceid30[i];
	}

	public void setResourceid30(String[] resourceid30) {
		this.resourceid30 = resourceid30;
	}

	public String[] getResourcename20() {
		return resourcename20;
	}

	public String getResourcename20(int i) {
		return resourcename20[i];
	}

	public void setResourcename20(String[] resourcename20) {
		this.resourcename20 = resourcename20;
	}

	public String[] getResourcename21() {
		return resourcename21;
	}

	public String getResourcename21(int i) {
		return resourcename21[i];
	}

	public void setResourcename21(String[] resourcename21) {
		this.resourcename21 = resourcename21;
	}

	public String[] getResourcename22() {
		return resourcename22;
	}

	public String getResourcename22(int i) {
		return resourcename22[i];
	}

	public void setResourcename22(String[] resourcename22) {
		this.resourcename22 = resourcename22;
	}

	public String[] getResourcename23() {
		return resourcename23;
	}

	public String getResourcename23(int i) {
		return resourcename23[i];
	}

	public void setResourcename23(String[] resourcename23) {
		this.resourcename23 = resourcename23;
	}

	public String[] getResourcename24() {
		return resourcename24;
	}

	public String getResourcename24(int i) {
		return resourcename24[i];
	}

	public void setResourcename24(String[] resourcename24) {
		this.resourcename24 = resourcename24;
	}

	public String[] getResourcename25() {
		return resourcename25;
	}

	public String getResourcename25(int i) {
		return resourcename25[i];
	}

	public void setResourcename25(String[] resourcename25) {
		this.resourcename25 = resourcename25;
	}

	public String[] getResourcename26() {
		return resourcename26;
	}

	public String getResourcename26(int i) {
		return resourcename26[i];
	}

	public void setResourcename26(String[] resourcename26) {
		this.resourcename26 = resourcename26;
	}

	public String[] getResourcename27() {
		return resourcename27;
	}

	public String getResourcename27(int i) {
		return resourcename27[i];
	}

	public void setResourcename27(String[] resourcename27) {
		this.resourcename27 = resourcename27;
	}

	public String[] getResourcename28() {
		return resourcename28;
	}

	public String getResourcename28(int i) {
		return resourcename28[i];
	}

	public void setResourcename28(String[] resourcename28) {
		this.resourcename28 = resourcename28;
	}

	public String[] getResourcename29() {
		return resourcename29;
	}

	public String getResourcename29(int i) {
		return resourcename29[i];
	}

	public void setResourcename29(String[] resourcename29) {
		this.resourcename29 = resourcename29;
	}

	public String[] getResourcename30() {
		return resourcename30;
	}

	public String getResourcename30(int i) {
		return resourcename30[i];
	}

	public void setResourcename30(String[] resourcename30) {
		this.resourcename30 = resourcename30;
	}

	public String[] getSelectresourceid20() {
		return selectresourceid20;
	}

	public String getSelectresourceid20(int i) {
		return selectresourceid20[i];
	}

	public void setSelectresourceid20(String[] selectresourceid20) {
		this.selectresourceid20 = selectresourceid20;
	}

	public String[] getSelectresourceid21() {
		return selectresourceid21;
	}

	public String getSelectresourceid21(int i) {
		return selectresourceid21[i];
	}

	public void setSelectresourceid21(String[] selectresourceid21) {
		this.selectresourceid21 = selectresourceid21;
	}

	public String[] getSelectresourceid22() {
		return selectresourceid22;
	}

	public String getSelectresourceid22(int i) {
		return selectresourceid22[i];
	}

	public void setSelectresourceid22(String[] selectresourceid22) {
		this.selectresourceid22 = selectresourceid22;
	}

	public String[] getSelectresourceid23() {
		return selectresourceid23;
	}

	public String getSelectresourceid23(int i) {
		return selectresourceid23[i];
	}

	public void setSelectresourceid23(String[] selectresourceid23) {
		this.selectresourceid23 = selectresourceid23;
	}

	public String[] getSelectresourceid24() {
		return selectresourceid24;
	}

	public String getSelectresourceid24(int i) {
		return selectresourceid24[i];
	}

	public void setSelectresourceid24(String[] selectresourceid24) {
		this.selectresourceid24 = selectresourceid24;
	}

	public String[] getSelectresourceid25() {
		return selectresourceid25;
	}

	public String getSelectresourceid25(int i) {
		return selectresourceid25[i];
	}

	public void setSelectresourceid25(String[] selectresourceid25) {
		this.selectresourceid25 = selectresourceid25;
	}

	public String[] getSelectresourceid26() {
		return selectresourceid26;
	}

	public String getSelectresourceid26(int i) {
		return selectresourceid26[i];
	}

	public void setSelectresourceid26(String[] selectresourceid26) {
		this.selectresourceid26 = selectresourceid26;
	}

	public String[] getSelectresourceid27() {
		return selectresourceid27;
	}

	public String getSelectresourceid27(int i) {
		return selectresourceid27[i];
	}

	public void setSelectresourceid27(String[] selectresourceid27) {
		this.selectresourceid27 = selectresourceid27;
	}

	public String[] getSelectresourceid28() {
		return selectresourceid28;
	}

	public String getSelectresourceid28(int i) {
		return selectresourceid28[i];
	}

	public void setSelectresourceid28(String[] selectresourceid28) {
		this.selectresourceid28 = selectresourceid28;
	}

	public String[] getSelectresourceid29() {
		return selectresourceid29;
	}

	public String getSelectresourceid29(int i) {
		return selectresourceid29[i];
	}

	public void setSelectresourceid29(String[] selectresourceid29) {
		this.selectresourceid29 = selectresourceid29;
	}

	public String[] getSelectresourceid30() {
		return selectresourceid30;
	}

	public String getSelectresourceid30(int i) {
		return selectresourceid30[i];
	}

	public void setSelectresourceid30(String[] selectresourceid30) {
		this.selectresourceid30 = selectresourceid30;
	}

	public String getShowAppendixId() {
		return showAppendixId;
	}

	public void setShowAppendixId(String showAppendixId) {
		this.showAppendixId = showAppendixId;
	}

	public String getShowAppendixName() {
		return showAppendixName;
	}

	public void setShowAppendixName(String showAppendixName) {
		this.showAppendixName = showAppendixName;
	}

}
