/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for material resource tabular page
*
*/

package com.mind.formbean.AM;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ResourceListForm extends ActionForm
{
	private String activity_Id = null;
	private String appendix_id = null;
	private String addendum_id = null;
	private String activityName = null;
	private String chkaddendumdetail = null;
	private String msaName = null;
	private String msaId = null;
	private String appendixName = null;
	private String jobId = null;
	private String jobName = null;
	private String type = null;
	private String fromflag = null;
	private String dashboardid = null;
	private String from = null;
	private String resource_Id = null;
	private String chkaddendum = null;
	private String resourceType = null;
	
	private String[] materialid = null;
	private String[] materialCostlibid = null;
	private String[] materialname = null;
	private String[] materialType = null;
	private String[] materialManufacturername = null;
	private String[] materialManufacturerpartnumber = null;
	private String[] materialCnspartnumber = null;
	private String[] materialEstimatedunitcost = null;
	private String[] materialEstimatedtotalcost = null;
	private String[] materialPriceextended = null;
	private String[] materialStatus = null;
	private String[] materialSellablequantity = null;
	private String[] materialMinimumquantity = null;
	private String[] materialCheck= null;
	private String[] materialQuantity = null;
	private String[] materialPrevquantity = null;
	private String[] materialProformamargin = null;
	private String[] materialPriceunit = null;
	private String[] materialFlag = null;
	
	private String[] laborCheck= null;
	private String[] laborid = null;
	private String[] laborcostlibid = null;
	private String[] laborname = null;
	private String[] labortype = null;
	private String[] laborCnspartnumber = null;
	private String[] laborQuantityhours = null;
	private String[] laborPrevquantityhours = null;
	private String[] laborEstimatedhourlybasecost = null;
	private String[] laborEstimatedtotalcost = null;
	private String[] laborProformamargin = null;
	private String[] laborPriceunit = null;
	private String[] laborPriceextended = null;
	private String[] laborSellablequantity = null;
	private String[] laborMinimumquantity = null;
	private String[] laborStatus = null;
	private String[] laborSubcategory = null;
	private String[] laborFlag = null;
	private String[] laborTransit = null;
	private String[] laborChecktransit = null;
	private String[] clr_detail_id = null;
	
	private String[] travelCheck = null;
	private String[] travelid = null;
	private String[] travelcostlibid = null;
	private String[] travelname = null;
	private String[] traveltype = null;
	private String[] travelCnspartnumber = null;
	private String[] travelQuantity = null;
	private String[] travelPrevquantity = null;
	private String[] travelEstimatedunitcost = null;
	private String[] travelEstimatedtotalcost = null;
	private String[] travelProformamargin = null;
	private String[] travelPriceunit = null;
	private String[] travelPriceextended = null;
	private String[] travelSellablequantity = null;
	private String[] travelMinimumquantity = null;
	private String[] travelStatus = null;
	private String[] travelFlag = null;
	
	private String[] freightCheck = null;
	private String[] freightid = null;
	private String[] freightcostlibid = null;
	private String[] freightname = null;
	private String[] freighttype = null;
	private String[] freightCnspartnumber = null;
	private String[] freightQuantity = null;
	private String[] freightPrevquantity = null;
	private String[] freightEstimatedunitcost = null;
	private String[] freightEstimatedtotalcost = null;
	private String[] freightProformamargin = null;
	private String[] freightPriceunit = null;
	private String[] freightPriceextended = null;
	private String[] freightSellablequantity = null;
	private String[] freightMinimumquantity = null;
	private String[] freightStatus = null;
	private String[] freightFlag = null; // for resource from temporary table or not
	private String[] flag_freightid = null; //for setting checkbox checked
	
	
	
	
	private String submit = null;
	private String cancel = null;
	private String delete = null;
	
	private String resourceListType = null;
	private String checknetmedx = null;
	private String jobType = null;
	private String save = null;
	private String edit = null;
	private String editableResource;
	private String jobStatus = null;
	
	private String ticketMinLaborHour;
	private String ticketMinTravelHour;
	private String ticketRequestType;
	
	public String getEdit() {
		return edit;
	}
	public void setEdit(String edit) {
		this.edit = edit;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getChkaddendumdetail() {
		return chkaddendumdetail;
	}
	public void setChkaddendumdetail(String chkaddendumdetail) {
		this.chkaddendumdetail = chkaddendumdetail;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDashboardid() {
		return dashboardid;
	}
	public void setDashboardid(String dashboardid) {
		this.dashboardid = dashboardid;
	}
	public String getFromflag() {
		return fromflag;
	}
	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getResource_Id() {
		return resource_Id;
	}
	public void setResource_Id(String resource_Id) {
		this.resource_Id = resource_Id;
	}
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	
	public String[] getMaterialCheck() {
		return materialCheck;
	}
	public void setMaterialCheck(String[] materialCheck) {
		this.materialCheck = materialCheck;
	}
	public String getMaterialCheck( int i ) {
		return materialCheck[i];
	}
	
	public String[] getMaterialCnspartnumber() {
		return materialCnspartnumber;
	}
	public void setMaterialCnspartnumber(String[] materialCnspartnumber) {
		this.materialCnspartnumber = materialCnspartnumber;
	}
	public String getMaterialCnspartnumber( int i ) {
		return materialCnspartnumber[i];
	}
	
	public String[] getMaterialCostlibid() {
		return materialCostlibid;
	}
	public void setMaterialCostlibid(String[] materialCostlibid) {
		this.materialCostlibid = materialCostlibid;
	}
	public String getMaterialCostlibid( int i ) {
		return materialCostlibid[i];
	}
	
	public String[] getMaterialEstimatedtotalcost() {
		return materialEstimatedtotalcost;
	}
	public void setMaterialEstimatedtotalcost(String[] materialEstimatedtotalcost) {
		this.materialEstimatedtotalcost = materialEstimatedtotalcost;
	}
	public String getMaterialEstimatedtotalcost( int i ) {
		return materialEstimatedtotalcost[i];
	}
	
	public String[] getMaterialEstimatedunitcost() {
		return materialEstimatedunitcost;
	}
	public void setMaterialEstimatedunitcost(String[] materialEstimatedunitcost) {
		this.materialEstimatedunitcost = materialEstimatedunitcost;
	}
	public String getMaterialEstimatedunitcost( int i ) {
		return materialEstimatedunitcost[i];
	}
	
	public String[] getMaterialManufacturername() {
		return materialManufacturername;
	}
	public void setMaterialManufacturername(String[] materialManufacturername) {
		this.materialManufacturername = materialManufacturername;
	}
	public String getMaterialManufacturername( int i ) {
		return materialManufacturername[i];
	}
	
	public String[] getMaterialManufacturerpartnumber() {
		return materialManufacturerpartnumber;
	}
	public void setMaterialManufacturerpartnumber(
			String[] materialManufacturerpartnumber) {
		this.materialManufacturerpartnumber = materialManufacturerpartnumber;
	}
	public String getMaterialManufacturerpartnumber( int i ) {
		return materialManufacturerpartnumber[i];
	}
	
	public String[] getMaterialMinimumquantity() {
		return materialMinimumquantity;
	}
	public void setMaterialMinimumquantity(String[] materialMinimumquantity) {
		this.materialMinimumquantity = materialMinimumquantity;
	}
	public String getMaterialMinimumquantity( int i ) {
		return materialMinimumquantity[i];
	}
	
	public String[] getMaterialname() {
		return materialname;
	}
	public void setMaterialname(String[] materialname) {
		this.materialname = materialname;
	}
	public String getMaterialname( int i ) {
		return materialname[i];
	}
	
	public String[] getMaterialPrevquantity() {
		return materialPrevquantity;
	}
	public void setMaterialPrevquantity(String[] materialPrevquantity) {
		this.materialPrevquantity = materialPrevquantity;
	}
	public String getMaterialPrevquantity( int i ) {
		return materialPrevquantity[i];
	}
	
	public String[] getMaterialPriceunit() {
		return materialPriceunit;
	}
	public void setMaterialPriceunit(String[] materialPriceunit) {
		materialPriceunit = materialPriceunit;
	}
	public String getMaterialPriceunit( int i ) {
		return materialPriceunit[i];
	}
	
	public String[] getMaterialProformamargin() {
		return materialProformamargin;
	}
	public void setMaterialProformamargin(String[] materialProformamargin) {
		this.materialProformamargin = materialProformamargin;
	}
	public String getMaterialProformamargin( int i ) {
		return materialProformamargin[i];
	}
	
	public String[] getMaterialQuantity() {
		return materialQuantity;
	}
	public void setMaterialQuantity(String[] materialQuantity) {
		this.materialQuantity = materialQuantity;
	}
	public String getMaterialQuantity( int i ) {
		return materialQuantity[i];
	}
	
	public String[] getMaterialSellablequantity() {
		return materialSellablequantity;
	}
	public void setMaterialSellablequantity(String[] materialSellablequantity) {
		this.materialSellablequantity = materialSellablequantity;
	}
	public String getMaterialSellablequantity( int i ) {
		return materialSellablequantity[i];
	}
	
	public String[] getMaterialStatus() {
		return materialStatus;
	}
	public void setMaterialStatus(String[] materialStatus) {
		this.materialStatus = materialStatus;
	}
	public String getMaterialStatus( int i ) {
		return materialStatus[i];
	}
	
	public String[] getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String[] materialType) {
		this.materialType = materialType;
	}
	public String getMaterialType( int i ) {
		return materialType[i];
	}
	public String[] getMaterialid() {
		return materialid;
	}
	public void setMaterialid(String[] materialid) {
		this.materialid = materialid;
	}
	public String getMaterialid( int i ) {
		return materialid[i];
	}
	public String[] getMaterialPriceextended() {
		return materialPriceextended;
	}
	public void setMaterialPriceextended(String[] materialPriceextended) {
		this.materialPriceextended = materialPriceextended;
	}
	public String getMaterialPriceextended( int i ) {
		return materialPriceextended[i];
	}
	public String[] getMaterialFlag() {
		return materialFlag;
	}
	public void setMaterialFlag(String[] materialFlag) {
		this.materialFlag = materialFlag;
	}
	public String getMaterialFlag( int i ) {
		return materialFlag[i];
	}
	public String getDelete() {
		return delete;
	}
	public void setDelete(String delete) {
		this.delete = delete;
	}
	public String[] getLaborCheck() {
		return laborCheck;
	}
	public void setLaborCheck(String[] laborCheck) {
		this.laborCheck = laborCheck;
	}
	public String getLaborCheck( int i ) {
		return laborCheck[i];
	}
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String[] getLaborCnspartnumber() {
		return laborCnspartnumber;
	}
	public void setLaborCnspartnumber(String[] laborCnspartnumber) {
		this.laborCnspartnumber = laborCnspartnumber;
	}
	public String getLaborCnspartnumber( int i ) {
		return laborCnspartnumber[i];
	}
	
	public String[] getLaborcostlibid() {
		return laborcostlibid;
	}
	public void setLaborcostlibid(String[] laborcostlibid) {
		this.laborcostlibid = laborcostlibid;
	}
	public String getLaborcostlibid( int i ) {
		return laborcostlibid[i];
	}
	
	public String[] getLaborEstimatedhourlybasecost() {
		return laborEstimatedhourlybasecost;
	}
	public void setLaborEstimatedhourlybasecost(
			String[] laborEstimatedhourlybasecost) {
		this.laborEstimatedhourlybasecost = laborEstimatedhourlybasecost;
	}
	public String getLaborEstimatedhourlybasecost( int i ) {
		return laborEstimatedhourlybasecost[i];
	}
	
	public String[] getLaborEstimatedtotalcost() {
		return laborEstimatedtotalcost;
	}
	public void setLaborEstimatedtotalcost(String[] laborEstimatedtotalcost) {
		this.laborEstimatedtotalcost = laborEstimatedtotalcost;
	}
	public String getLaborEstimatedtotalcost( int i ) {
		return laborEstimatedtotalcost[i];
	}
	
	public String[] getLaborFlag() {
		return laborFlag;
	}
	public void setLaborFlag(String[] laborFlag) {
		this.laborFlag = laborFlag;
	}
	public String getLaborFlag( int i ) {
		return laborFlag[i];
	}
	
	public String[] getLaborid() {
		return laborid;
	}
	public void setLaborid(String[] laborid) {
		this.laborid = laborid;
	}
	public String getLaborid( int i ) {
		return laborid[i];
	}
	
	public String[] getLaborMinimumquantity() {
		return laborMinimumquantity;
	}
	public void setLaborMinimumquantity(String[] laborMinimumquantity) {
		this.laborMinimumquantity = laborMinimumquantity;
	}
	public String getLaborMinimumquantity( int i ) {
		return laborMinimumquantity[i];
	}
	
	public String[] getLaborname() {
		return laborname;
	}
	public void setLaborname(String[] laborname) {
		this.laborname = laborname;
	}
	public String getLaborname( int i ) {
		return laborname[i];
	}
	
	public String[] getLaborPrevquantityhours() {
		return laborPrevquantityhours;
	}
	public void setLaborPrevquantityhours(String[] laborPrevquantityhours) {
		this.laborPrevquantityhours = laborPrevquantityhours;
	}
	public String getLaborPrevquantityhours( int i ) {
		return laborPrevquantityhours[i];
	}
	
	public String[] getLaborPriceextended() {
		return laborPriceextended;
	}
	public void setLaborPriceextended(String[] laborPriceextended) {
		this.laborPriceextended = laborPriceextended;
	}
	public String getLaborPriceextended( int i ) {
		return laborPriceextended[i];
	}
	
	public String[] getLaborPriceunit() {
		return laborPriceunit;
	}
	public void setLaborPriceunit(String[] laborPriceunit) {
		this.laborPriceunit = laborPriceunit;
	}
	public String getLaborPriceunit( int i ) {
		return laborPriceunit[i];
	}
	
	public String[] getLaborProformamargin() {
		return laborProformamargin;
	}
	public void setLaborProformamargin(String[] laborProformamargin) {
		this.laborProformamargin = laborProformamargin;
	}
	public String getLaborProformamargin( int i ) {
		return laborProformamargin[i];
	}
	
	public String[] getLaborQuantityhours() {
		return laborQuantityhours;
	}
	public void setLaborQuantityhours(String[] laborQuantityhours) {
		this.laborQuantityhours = laborQuantityhours;
	}
	public String getLaborQuantityhours( int i ) {
		return laborQuantityhours[i];
	}
	
	public String[] getLaborSellablequantity() {
		return laborSellablequantity;
	}
	public void setLaborSellablequantity(String[] laborSellablequantity) {
		this.laborSellablequantity = laborSellablequantity;
	}
	public String getLaborSellablequantity( int i ) {
		return laborSellablequantity[i];
	}
	
	public String[] getLaborStatus() {
		return laborStatus;
	}
	public void setLaborStatus(String[] laborStatus) {
		this.laborStatus = laborStatus;
	}
	public String getLaborStatus( int i ) {
		return laborStatus[i];
	}
	
	public String[] getLaborSubcategory() {
		return laborSubcategory;
	}
	public void setLaborSubcategory(String[] laborSubcategory) {
		this.laborSubcategory = laborSubcategory;
	}
	public String getLaborSubcategory( int i ) {
		return laborSubcategory[i];
	}
	
	public String[] getLabortype() {
		return labortype;
	}
	public void setLabortype(String[] labortype) {
		this.labortype = labortype;
	}
	public String getLabortype( int i ) {
		return labortype[i];
	}
	public String[] getClr_detail_id() {
		return clr_detail_id;
	}
	public void setClr_detail_id(String[] clr_detail_id) {
		this.clr_detail_id = clr_detail_id;
	}
	public String getClr_detail_id( int i ) {
		return clr_detail_id[i];
	}
	
	public String[] getLaborChecktransit() {
		return laborChecktransit;
	}
	public void setLaborChecktransit(String[] laborChecktransit) {
		this.laborChecktransit = laborChecktransit;
	}
	public String getLaborChecktransit( int i ) {
		return laborChecktransit[i];
	}
	
	public String[] getLaborTransit() {
		return laborTransit;
	}
	public void setLaborTransit(String[] laborTransit) {
		this.laborTransit = laborTransit;
	}
	public String getLaborTransit( int i ) {
		return laborTransit[i];
	}
	public String[] getTravelCheck() {
		return travelCheck;
	}
	public void setTravelCheck(String[] travelCheck) {
		this.travelCheck = travelCheck;
	}
	public String getTravelCheck( int i ) {
		return travelCheck[i];
	}
	
	public String[] getTravelCnspartnumber() {
		return travelCnspartnumber;
	}
	public void setTravelCnspartnumber(String[] travelCnspartnumber) {
		this.travelCnspartnumber = travelCnspartnumber;
	}
	public String getTravelCnspartnumber( int i ) {
		return travelCnspartnumber[i];
	}
	
	public String[] getTravelcostlibid() {
		return travelcostlibid;
	}
	public void setTravelcostlibid(String[] travelcostlibid) {
		this.travelcostlibid = travelcostlibid;
	}
	public String getTravelcostlibid( int i ) {
		return travelcostlibid[i];
	}
	
	public String[] getTravelEstimatedtotalcost() {
		return travelEstimatedtotalcost;
	}
	public void setTravelEstimatedtotalcost(String[] travelEstimatedtotalcost) {
		this.travelEstimatedtotalcost = travelEstimatedtotalcost;
	}
	public String getTravelEstimatedtotalcost( int i ) {
		return travelEstimatedtotalcost[i];
	}
	
	public String[] getTravelEstimatedunitcost() {
		return travelEstimatedunitcost;
	}
	public void setTravelEstimatedunitcost(String[] travelEstimatedunitcost) {
		this.travelEstimatedunitcost = travelEstimatedunitcost;
	}
	public String getTravelEstimatedunitcost( int i ) {
		return travelEstimatedunitcost[i];
	}
	
	public String[] getTravelid() {
		return travelid;
	}
	public void setTravelid(String[] travelid) {
		this.travelid = travelid;
	}
	public String getTravelid( int i ) {
		return travelid[i];
	}
	
	public String[] getTravelMinimumquantity() {
		return travelMinimumquantity;
	}
	public void setTravelMinimumquantity(String[] travelMinimumquantity) {
		this.travelMinimumquantity = travelMinimumquantity;
	}
	public String getTravelMinimumquantity( int i ) {
		return travelMinimumquantity[i];
	}
	
	public String[] getTravelname() {
		return travelname;
	}
	public void setTravelname(String[] travelname) {
		this.travelname = travelname;
	}
	public String getTravelname( int i ) {
		return travelname[i];
	}
	
	public String[] getTravelPrevquantity() {
		return travelPrevquantity;
	}
	public void setTravelPrevquantity(String[] travelPrevquantity) {
		this.travelPrevquantity = travelPrevquantity;
	}
	public String getTravelPrevquantity( int i ) {
		return travelPrevquantity[i];
	}
	
	public String[] getTravelPriceextended() {
		return travelPriceextended;
	}
	public void setTravelPriceextended(String[] travelPriceextended) {
		this.travelPriceextended = travelPriceextended;
	}
	public String getTravelPriceextended( int i ) {
		return travelPriceextended[i];
	}
	
	public String[] getTravelPriceunit() {
		return travelPriceunit;
	}
	public void setTravelPriceunit(String[] travelPriceunit) {
		this.travelPriceunit = travelPriceunit;
	}
	public String getTravelPriceunit( int i ) {
		return travelPriceunit[i];
	}
	
	public String[] getTravelProformamargin() {
		return travelProformamargin;
	}
	public void setTravelProformamargin(String[] travelProformamargin) {
		this.travelProformamargin = travelProformamargin;
	}
	public String getTravelProformamargin( int i ) {
		return travelProformamargin[i];
	}
	
	public String[] getTravelQuantity() {
		return travelQuantity;
	}
	public void setTravelQuantity(String[] travelQuantity) {
		this.travelQuantity = travelQuantity;
	}
	public String getTravelQuantity( int i ) {
		return travelQuantity[i];
	}
	
	public String[] getTravelSellablequantity() {
		return travelSellablequantity;
	}
	public void setTravelSellablequantity(String[] travelSellablequantity) {
		this.travelSellablequantity = travelSellablequantity;
	}
	public String getTravelSellablequantity( int i ) {
		return travelSellablequantity[i];
	}
	
	public String[] getTravelStatus() {
		return travelStatus;
	}
	public void setTravelStatus(String[] travelStatus) {
		this.travelStatus = travelStatus;
	}
	public String getTravelStatus( int i ) {
		return travelStatus[i];
	}
	
	public String[] getTraveltype() {
		return traveltype;
	}
	public void setTraveltype(String[] traveltype) {
		this.traveltype = traveltype;
	}
	public String getTraveltype( int i ) {
		return traveltype[i];
	}
	public String[] getTravelFlag() {
		return travelFlag;
	}
	public void setTravelFlag(String[] travelFlag) {
		this.travelFlag = travelFlag;
	}
	public String getTravelFlag( int i ) {
		return travelFlag[i];
	}
	
	public String[] getFlag_freightid() {
		return flag_freightid;
	}
	public void setFlag_freightid(String[] flag_freightid) {
		this.flag_freightid = flag_freightid;
	}
	public String getFlag_freightid( int i ) {
		return flag_freightid[i];
	}
	
	public String[] getFreightCheck() {
		return freightCheck;
	}
	public void setFreightCheck(String[] freightCheck) {
		this.freightCheck = freightCheck;
	}
	public String getFreightCheck( int i ) {
		return freightCheck[i];
	}
	
	public String[] getFreightCnspartnumber() {
		return freightCnspartnumber;
	}
	public void setFreightCnspartnumber(String[] freightCnspartnumber) {
		this.freightCnspartnumber = freightCnspartnumber;
	}
	public String getFreightCnspartnumber( int i ) {
		return freightCnspartnumber[i];
	}
	
	public String[] getFreightcostlibid() {
		return freightcostlibid;
	}
	public void setFreightcostlibid(String[] freightcostlibid) {
		this.freightcostlibid = freightcostlibid;
	}
	public String getFreightcostlibid( int i ) {
		return freightcostlibid[i];
	}
	
	public String[] getFreightEstimatedtotalcost() {
		return freightEstimatedtotalcost;
	}
	public void setFreightEstimatedtotalcost(String[] freightEstimatedtotalcost) {
		this.freightEstimatedtotalcost = freightEstimatedtotalcost;
	}
	public String getFreightEstimatedtotalcost( int i ) {
		return freightEstimatedtotalcost[i];
	}
	
	public String[] getFreightEstimatedunitcost() {
		return freightEstimatedunitcost;
	}
	public void setFreightEstimatedunitcost(String[] freightEstimatedunitcost) {
		this.freightEstimatedunitcost = freightEstimatedunitcost;
	}
	public String getFreightEstimatedunitcost( int i ) {
		return freightEstimatedunitcost[i];
	}
	
	public String[] getFreightFlag() {
		return freightFlag;
	}
	public void setFreightFlag(String[] freightFlag) {
		this.freightFlag = freightFlag;
	}
	public String getFreightFlag( int i ) {
		return freightFlag[i];
	}
	
	public String[] getFreightid() {
		return freightid;
	}
	public void setFreightid(String[] freightid) {
		this.freightid = freightid;
	}
	public String getFreightid( int i ) {
		return freightid[i];
	}
	
	public String[] getFreightMinimumquantity() {
		return freightMinimumquantity;
	}
	public void setFreightMinimumquantity(String[] freightMinimumquantity) {
		this.freightMinimumquantity = freightMinimumquantity;
	}
	public String getFreightMinimumquantity( int i ) {
		return freightMinimumquantity[i];
	}
	
	public String[] getFreightname() {
		return freightname;
	}
	public void setFreightname(String[] freightname) {
		this.freightname = freightname;
	}
	public String getFreightname( int i ) {
		return freightname[i];
	}
	
	public String[] getFreightPrevquantity() {
		return freightPrevquantity;
	}
	public void setFreightPrevquantity(String[] freightPrevquantity) {
		this.freightPrevquantity = freightPrevquantity;
	}
	public String getFreightPrevquantity( int i ) {
		return freightPrevquantity[i];
	}
	
	public String[] getFreightPriceextended() {
		return freightPriceextended;
	}
	public void setFreightPriceextended(String[] freightPriceextended) {
		this.freightPriceextended = freightPriceextended;
	}
	public String getFreightPriceextended( int i ) {
		return freightPriceextended[i];
	}
	
	public String[] getFreightPriceunit() {
		return freightPriceunit;
	}
	public void setFreightPriceunit(String[] freightPriceunit) {
		this.freightPriceunit = freightPriceunit;
	}
	public String getFreightPriceunit( int i ) {
		return freightPriceunit[i];
	}
	
	public String[] getFreightProformamargin() {
		return freightProformamargin;
	}
	public void setFreightProformamargin(String[] freightProformamargin) {
		this.freightProformamargin = freightProformamargin;
	}
	public String getFreightProformamargin( int i ) {
		return freightProformamargin[i];
	}
	
	public String[] getFreightQuantity() {
		return freightQuantity;
	}
	public void setFreightQuantity(String[] freightQuantity) {
		this.freightQuantity = freightQuantity;
	}
	public String getFreightQuantity( int i ) {
		return freightQuantity[i];
	}
	
	public String[] getFreightSellablequantity() {
		return freightSellablequantity;
	}
	public void setFreightSellablequantity(String[] freightSellablequantity) {
		this.freightSellablequantity = freightSellablequantity;
	}
	public String getFreightSellablequantity( int i ) {
		return freightSellablequantity[i];
	}
	
	public String[] getFreightStatus() {
		return freightStatus;
	}
	public void setFreightStatus(String[] freightStatus) {
		this.freightStatus = freightStatus;
	}
	public String getFreightStatus( int i ) {
		return freightStatus[i];
	}
	
	public String[] getFreighttype() {
		return freighttype;
	}
	public void setFreighttype(String[] freighttype) {
		this.freighttype = freighttype;
	}
	public String getFreighttype( int i ) {
		return freighttype[i];
	}
	public String getResourceListType() {
		return resourceListType;
	}
	public void setResourceListType(String resourceListType) {
		this.resourceListType = resourceListType;
	}
	
	public void materialreset( ActionMapping mapping , HttpServletRequest request ) 
	{
		 materialid = null;
		 materialCostlibid = null;
		 materialCheck= null;
	}
	
	public void laborreset( ActionMapping mapping , HttpServletRequest request ) 
	{
		 laborid = null;
		 laborcostlibid = null;
		 laborCheck= null;
	}
	
	public void travelreset( ActionMapping mapping , HttpServletRequest request ) 
	{
		 travelid = null;
		 travelcostlibid = null;
		 travelCheck= null;
	}
	
	public void freightreset( ActionMapping mapping , HttpServletRequest request ) 
	{
		 freightid = null;
		 freightcostlibid = null;
		 freightCheck= null;
	}
	
	
	
	
	public String getChecknetmedx() {
		return checknetmedx;
	}
	public void setChecknetmedx(String checknetmedx) {
		this.checknetmedx = checknetmedx;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getEditableResource() {
		return editableResource;
	}
	public void setEditableResource(String editableResource) {
		this.editableResource = editableResource;
	}
	public String getTicketMinLaborHour() {
		return ticketMinLaborHour;
	}
	public void setTicketMinLaborHour(String ticketMinLaborHour) {
		this.ticketMinLaborHour = ticketMinLaborHour;
	}
	public String getTicketMinTravelHour() {
		return ticketMinTravelHour;
	}
	public void setTicketMinTravelHour(String ticketMinTravelHour) {
		this.ticketMinTravelHour = ticketMinTravelHour;
	}
	public String getTicketRequestType() {
		return ticketRequestType;
	}
	public void setTicketRequestType(String ticketRequestType) {
		this.ticketRequestType = ticketRequestType;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	
}
