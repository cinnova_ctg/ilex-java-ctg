/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for activity library tabular page
*
*/


package com.mind.formbean.AM;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ManageActivityForm extends ActionForm
{
	private String msa_Id = null;
	private String category_Id = null;
	private String category_name = null;
	private String msaname = null;
	
	private String check[] = null;
	private String activity_Id[] = null;
	private String name[] = null;
	private String cat_Id[] = null;
	private String activitytype[] = null;
	private String activitytypecombo[] = null;
	
	private String quantity[] = null;
	private String duration[] = null;
	
	private String estimatedmaterialcost[] = null;
	private String estimatedcnslabourcost[] = null;
	private String estimatedcontractlabourcost[] = null;
	private String estimatedfreightcost[] = null;
	private String estimatedtravelcost[] = null;
	private String estimatedtotalcost[] = null;
	private String extendedprice[] = null;
	private String proformamargin[] = null;
	private String status[] = null;
	
	private String newactivity_Id = null;
	private String newname = null;
	private String newactivitytype = null;
	private String newactivitytypecombo = null;
	
	private String newcategorytype = null;
	private String newcategorytypecombo = null;
	
	
	private String newquantity = null;
	private String newduration = null;
	
	private String newestimatedmaterialcost = null;
	private String newestimatedcnslabourcost = null;
	private String newestimatedcontractlabourcost = null;
	private String newestimatedfreightcost = null;
	private String newestimatedtravelcost = null;
	private String newestimatedtotalcost = null;
	private String newextendedprice = null;
	private String newproformamargin = null;
	private String newstatus = null;
	private String authenticate = "";
	
	private String update = null;
	
	private String delete = null;
	private String approve = null;
	
	private ArrayList activity = null;
	private ArrayList category = null;
	
	private String category_type[] = null;
	private String category_typecombo[] = null;
	
	
	
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id( String msa_Id ) {
		this.msa_Id = msa_Id;
	}
	

	public String getCategory_Id() {
		return category_Id;
	}
	public void setCategory_Id( String category_Id ) {
		this.category_Id = category_Id;
	}
	
	
	public String[] getCheck() {
		return check;
	}
	public void setCheck( String[] check ) {
		this.check = check;
	}
	public String getCheck( int i ) {
		return check[i];
	}
	
	
	public String[] getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String[] activity_Id ) {
		this.activity_Id = activity_Id;
	}
	public String getActivity_Id( int i ) {
		return activity_Id[i];
	}
	
	
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name( String category_name ) {
		this.category_name = category_name;
	}
	
	public String[] getCat_Id() {
		return cat_Id;
	}
	public void setCat_Id( String[] cat_Id ) {
		this.cat_Id = cat_Id;
	}
	public String getCat_Id( int i ) {
		return cat_Id[i];
	}
	
	
	
	public String[] getName() {
		return name;
	}
	public void setName( String[] name ) {
		this.name = name;
	}
	public String getName( int i ) {
		return name[i];
	}
	
	
	
	public String[] getActivitytype() {
		return activitytype;
	}
	public void setActivitytype( String[] activitytype ) {
		this.activitytype = activitytype;
	}
	public String getActivitytype( int i ) {
		return activitytype[i];
	}
	
	
	
	public String[] getActivitytypecombo() {
		return activitytypecombo;
	}
	public void setActivitytypecombo( String[] activitytypecombo ) {
		this.activitytypecombo = activitytypecombo;
	}
	public String getActivitytypecombo( int i ) {
		return activitytypecombo[i];
	}
	
	
	public String[] getDuration() {
		return duration;
	}
	public void setDuration(String[] duration) {
		this.duration = duration;
	}
	public String getDuration( int i ) {
		return duration[i];
	}
	
	
	public String[] getQuantity() {
		return quantity;
	}
	public void setQuantity( String[] quantity ) {
		this.quantity = quantity;
	}
	public String getQuantity( int i ) {
		return quantity[i];
	}
	
	
	public String[] getEstimatedmaterialcost() {
		return estimatedmaterialcost;
	}
	public void setEstimatedmaterialcost( String[] estimatedmaterialcost ) {
		this.estimatedmaterialcost = estimatedmaterialcost;
	}
	public String getEstimatedmaterialcost( int i ) {
		return estimatedmaterialcost[i];
	}
	
	
	public String[] getEstimatedcnslabourcost() {
		return estimatedcnslabourcost;
	}
	public void setEstimatedcnslabourcost( String[] estimatedcnslabourcost ) {
		this.estimatedcnslabourcost = estimatedcnslabourcost;
	}
	public String getEstimatedcnslabourcost( int i ) {
		return estimatedcnslabourcost[i];
	}
	
	
	public String[] getEstimatedcontractlabourcost() {
		return estimatedcontractlabourcost;
	}
	public void setEstimatedcontractlabourcost( String[] estimatedcontractlabourcost ) {
		this.estimatedcontractlabourcost = estimatedcontractlabourcost;
	}
	public String getEstimatedcontractlabourcost( int i ) {
		return estimatedcontractlabourcost[i];
	}
	
	
	public String[] getEstimatedfreightcost() {
		return estimatedfreightcost;
	}
	public void setEstimatedfreightcost( String[] estimatedfreightcost ) {
		this.estimatedfreightcost = estimatedfreightcost;
	}
	public String getEstimatedfreightcost( int i ) {
		return estimatedfreightcost[i];
	}
	
	
	public String[] getEstimatedtravelcost() {
		return estimatedtravelcost;
	}
	public void setEstimatedtravelcost( String[] estimatedtravelcost ) {
		this.estimatedtravelcost = estimatedtravelcost;
	}
	public String getEstimatedtravelcost( int i ) {
		return estimatedtravelcost[i];
	}
	
	
	public String[] getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost( String[] estimatedtotalcost ) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	public String getEstimatedtotalcost( int i ) {
		return estimatedtotalcost[i];
	}
	
	
	public String[] getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice( String[] extendedprice ) {
		this.extendedprice = extendedprice;
	}
	public String getExtendedprice( int i ) {
		return extendedprice[i];
	}
	
	
	public String[] getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String[] proformamargin ) {
		this.proformamargin = proformamargin;
	}
	public String getProformamargin( int i ) {
		return proformamargin[i];
	}
	
	
	public String[] getStatus() {
		return status;
	}
	public void setStatus(String[] status) {
		this.status = status;
	}
	public String getStatus( int i )
	{
		return status[i];
	}
	
	
	public String getNewactivity_Id() {
		return newactivity_Id;
	}
	public void setNewactivity_Id( String newactivity_Id ) {
		this.newactivity_Id = newactivity_Id;
	}
	
	
	public String getNewestimatedcnslabourcost() {
		return newestimatedcnslabourcost;
	}
	public void setNewestimatedcnslabourcost( String newestimatedcnslabourcost ) {
		this.newestimatedcnslabourcost = newestimatedcnslabourcost;
	}
	
	
	public String getNewestimatedcontractlabourcost() {
		return newestimatedcontractlabourcost;
	}
	public void setNewestimatedcontractlabourcost( String newestimatedcontractlabourcost ) {
		this.newestimatedcontractlabourcost = newestimatedcontractlabourcost;
	}
	
	
	public String getNewestimatedfreightcost() {
		return newestimatedfreightcost;
	}
	public void setNewestimatedfreightcost( String newestimatedfreightcost ) {
		this.newestimatedfreightcost = newestimatedfreightcost;
	}
	
	
	public String getNewestimatedmaterialcost() {
		return newestimatedmaterialcost;
	}
	public void setNewestimatedmaterialcost( String newestimatedmaterialcost ) {
		this.newestimatedmaterialcost = newestimatedmaterialcost;
	}
	
	
	public String getNewestimatedtotalcost() {
		return newestimatedtotalcost;
	}
	public void setNewestimatedtotalcost( String newestimatedtotalcost ) {
		this.newestimatedtotalcost = newestimatedtotalcost;
	}
	
	
	public String getNewestimatedtravelcost() {
		return newestimatedtravelcost;
	}
	public void setNewestimatedtravelcost( String newestimatedtravelcost ) {
		this.newestimatedtravelcost = newestimatedtravelcost;
	}
	
	
	public String getNewextendedprice() {
		return newextendedprice;
	}
	public void setNewextendedprice( String newextendedprice ) {
		this.newextendedprice = newextendedprice;
	}
	
	
	public String getNewname() {
		return newname;
	}
	public void setNewname( String newname ) {
		this.newname = newname;
	}
	
	
	public String getNewproformamargin() {
		return newproformamargin;
	}
	public void setNewproformamargin( String newproformamargin ) {
		this.newproformamargin = newproformamargin;
	}
	
	
	public String getNewquantity() {
		return newquantity;
	}
	public void setNewquantity( String newquantity ) {
		this.newquantity = newquantity;
	}
	
	
	public String getNewduration() {
		return newduration;
	}
	public void setNewduration( String newduration ) {
		this.newduration = newduration;
	}
	public String getNewactivitytype() {
		return newactivitytype;
	}
	public void setNewactivitytype( String newactivitytype ) {
		this.newactivitytype = newactivitytype;
	}
	

	public String getNewactivitytypecombo() {
		return newactivitytypecombo;
	}
	public void setNewactivitytypecombo( String newactivitytypecombo ) {
		this.newactivitytypecombo = newactivitytypecombo;
	}
	
	
	public String getNewcategorytype() {
		return newcategorytype;
	}
	public void setNewcategorytype( String newcategorytype ) {
		this.newcategorytype = newcategorytype;
	}
	
	
	public String getNewcategorytypecombo() {
		return newcategorytypecombo;
	}
	public void setNewcategorytypecombo( String newcategorytypecombo ) {
		this.newcategorytypecombo = newcategorytypecombo;
	}
	
	
	public String getNewstatus() {
		return newstatus;
	}
	public void setNewstatus( String newstatus ) {
		this.newstatus = newstatus;
	}
	
	
	public ArrayList getActivity() {
		return activity;
	}
	public void setActivity( ArrayList activity ) {
		this.activity = activity;
	}
	
	
	public ArrayList getCategory() {
		return category;
	}
	public void setCategory( ArrayList category ) {
		this.category = category;
	}
	
	
	public String getUpdate() {
		return update;
	}
	public void setUpdate( String update ) {
		this.update = update;
	}
	
	
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate( String authenticate ) {
		this.authenticate = authenticate;
	}
	
	
	public String[] getCategory_type() {
		return category_type;
	}
	public void setCategory_type(String[] category_type) {
		this.category_type = category_type;
	}
	public String[] getCategory_typecombo() {
		return category_typecombo;
	}
	public void setCategory_typecombo(String[] category_typecombo) {
		this.category_typecombo = category_typecombo;
	}
	public String getCategory_typecombo( int i ) {
		return category_typecombo[i];
	}
	
	
	
	
/** This method reset all the fields
* @param mapping						object of ActionMapping
* @param request   			       		object of HttpServletRequest
* @return void              			This method returns object of ActionForward.
*/
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		check = null;
		name = null;
		category_name = null;
		activitytype = null;
		activitytypecombo = null;
		quantity = null;
		duration = null;
		estimatedmaterialcost = null;
		estimatedcnslabourcost = null;
		estimatedcontractlabourcost = null;
		estimatedfreightcost = null;
		estimatedtravelcost = null;
		estimatedtotalcost = null;
		extendedprice = null;
		proformamargin = null;
		status = null;
		
		newactivity_Id = null;
		newname = null;
		
		newactivitytype = null;
		newactivitytypecombo = null;
		
		newcategorytype = null;
		newcategorytypecombo = null;
		
		newquantity = null;
		newduration = null;
		newestimatedmaterialcost = null;
		newestimatedcnslabourcost = null;
		newestimatedcontractlabourcost = null;
		newestimatedfreightcost = null;
		newestimatedtravelcost = null;
		newestimatedtotalcost = null;
		newextendedprice = null;
		newproformamargin = null;
		newstatus = null;
		
		activity = null;
		category = null;
		
		category_type = null;
		category_typecombo = null;
	}
public String getDelete() {
	return delete;
}
public void setDelete(String delete) {
	this.delete = delete;
}
public String getApprove() {
	return approve;
}
public void setApprove(String approve) {
	this.approve = approve;
}
public String getMsaname() {
	return msaname;
}
public void setMsaname(String msaname) {
	this.msaname = msaname;
}
	
	
	
	
	
	
	
	
	
	
	
	
}
