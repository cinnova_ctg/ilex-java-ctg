/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A formbean containing getter/setter methods for material resource detail page
*
*/

package com.mind.formbean.AM;

import org.apache.struts.action.ActionForm;

public class MaterialResourceDetailForm extends ActionForm
{
	private String activity_Id = null;
	private String materialid = null;
	private String materialname = null;
	private String msa_id = null;
	
	private String job_id = null;
	
	private String materialtype = null;
	
	private String manufacturername = null;
	private String manufacturerpartnumber = null;
	private String cnspartnumber = null;
	private String quantity = null;
	private String estimatedunitcost = null;
	private String estimatedtotalcost = null;
	private String proformamargin = null;
	private String priceunit = null;
	private String priceextended = null;
	
	private String sellablequantity = null;
	private String minimumquantity = null;
	
	private String status= null;

	private String chkaddendum = null;
	
	private String addendum_id = null;
	
	private String chkdetailactivity = null;
	
	private String activityname = null;
	
	private String msaname = null;
	private String appendix_id = null;
	private String appendixname = null;
	private String jobname = null;
	
	public String getActivity_Id() {
		return activity_Id;
	}

	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}

	
	public String getCnspartnumber() {
		return cnspartnumber;
	}
	public void setCnspartnumber( String cnspartnumber ) {
		this.cnspartnumber = cnspartnumber;
	}

	
	public String getEstimatedtotalcost() {
		return estimatedtotalcost;
	}

	public void setEstimatedtotalcost( String estimatedtotalcost ) {
		this.estimatedtotalcost = estimatedtotalcost;
	}

	
	public String getEstimatedunitcost() {
		return estimatedunitcost;
	}

	public void setEstimatedunitcost( String estimatedunitcost ) {
		this.estimatedunitcost = estimatedunitcost;
	}

	
	
	public String getManufacturername() {
		return manufacturername;
	}

	public void setManufacturername( String manufacturername ) {
		this.manufacturername = manufacturername;
	}

	
	public String getManufacturerpartnumber() {
		return manufacturerpartnumber;
	}

	public void setManufacturerpartnumber( String manufacturerpartnumber ) {
		this.manufacturerpartnumber = manufacturerpartnumber;
	}

	
	public String getMaterialid() {
		return materialid;
	}

	public void setMaterialid( String materialid ) {
		this.materialid = materialid;
	}

	
	
	public String getMaterialname() {
		return materialname;
	}

	public void setMaterialname( String materialname ) {
		this.materialname = materialname;
	}


	public String getMaterialtype() {
		return materialtype;
	}

	public void setMaterialtype( String materialtype ) {
		this.materialtype = materialtype;
	}

	
	
	public String getMinimumquantity() {
		return minimumquantity;
	}

	public void setMinimumquantity( String minimumquantity ) {
		this.minimumquantity = minimumquantity;
	}

	
	
	public String getPriceextended() {
		return priceextended;
	}

	public void setPriceextended( String priceextended ) {
		this.priceextended = priceextended;
	}

	
	
	public String getPriceunit() {
		return priceunit;
	}

	public void setPriceunit( String priceunit ) {
		this.priceunit = priceunit;
	}

	
	
	public String getProformamargin() {
		return proformamargin;
	}

	public void setProformamargin( String proformamargin ) {
		this.proformamargin = proformamargin;
	}

	
	
	public String getQuantity() {
		return quantity;
	}

	public void setQuantity( String quantity ) {
		this.quantity = quantity;
	}

	
	
	public String getSellablequantity() {
		return sellablequantity;
	}

	public void setSellablequantity( String sellablequantity ) {
		this.sellablequantity = sellablequantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus( String status ) {
		this.status = status;
	}

	public String getChkaddendum() {
		return chkaddendum;
	}

	public void setChkaddendum( String chkaddendum ) {
		this.chkaddendum = chkaddendum;
	}

	public String getAddendum_id() {
		return addendum_id;
	}

	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}

	public String getChkdetailactivity() {
		return chkdetailactivity;
	}

	public void setChkdetailactivity( String chkdetailactivity ) {
		this.chkdetailactivity = chkdetailactivity;
	}

	public String getMsa_id() {
		return msa_id;
	}

	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	public String getActivityname() {
		return activityname;
	}

	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}

	public String getAppendix_id() {
		return appendix_id;
	}

	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname;
	}	
}
