package com.mind.common;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.mind.docm.TransferAllDoc;
import com.mind.docm.dao.TransferSupplements;
import com.mind.xml.DocUtil;

public class SingletonDocumentTransfer {
	private static SingletonDocumentTransfer singletonDocumentTransfer = null;
	private static Thread myThread = null;

	private SingletonDocumentTransfer() {
		TransferSupplements transferSupplemnts = new TransferSupplements();
		this.myThread = new Thread(transferSupplemnts);
		myThread.start();
	}

	private SingletonDocumentTransfer(String type, DataSource ds,
			HttpServletRequest request) {
		if (type != null && !type.trim().equalsIgnoreCase("all")) {
			DocUtil docUtil = new DocUtil(type, ds);

			DocUtil.setVariables(type, docUtil);
			this.myThread = new Thread(docUtil);
			myThread.start();
		} else {
			TransferAllDoc transferAllDoc = new TransferAllDoc(type, ds,
					request);
			this.myThread = new Thread(transferAllDoc);
			myThread.start();
		}
	}

	public static SingletonDocumentTransfer getInstance() {
		if (!hasThread() || singletonDocumentTransfer == null) {
			singletonDocumentTransfer = new SingletonDocumentTransfer();
			return singletonDocumentTransfer;
		} else {
			return null;
		}
	}

	public static boolean hasThread() {
		return !(myThread == null || !myThread.isAlive());
	}

	public static boolean hasInstance() {
		if (!hasThread()) {
			myThread = null;
			singletonDocumentTransfer = null;
			return false;
		} else {
			return true;
		}
	}

	public static SingletonDocumentTransfer getInstance(String type,
			DataSource ds, HttpServletRequest request) {
		if (!hasThread() || singletonDocumentTransfer == null) {
			singletonDocumentTransfer = new SingletonDocumentTransfer(type, ds,
					request);
			return singletonDocumentTransfer;
		} else {
			return null;
		}
	}

}
