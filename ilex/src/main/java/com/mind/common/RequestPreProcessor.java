package com.mind.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.RequestProcessor;

/**
 * The Class AskAmadeusRequestProcessor.
 */
public class RequestPreProcessor extends RequestProcessor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.action.RequestProcessor#processPreprocess(javax.servlet
	 * .http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public boolean processPreprocess(HttpServletRequest request,
			HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		return true;
	}
}
