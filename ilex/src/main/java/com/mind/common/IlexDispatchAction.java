package com.mind.common;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.actions.DispatchAction;

import com.mind.fw.core.dao.util.DBUtil;

public class IlexDispatchAction extends DispatchAction{
	/** The logger. */
	private static Logger logger = Logger.getLogger(IlexDispatchAction.class);

	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#getDataSource(javax.servlet.http.HttpServletRequest, java.lang.String)
	 */
	public DataSource getDataSource(HttpServletRequest argServReq, String argDSName)
	{
		if(argDSName.equals("ilexnewDB")) {
			argDSName = IlexConstants.ILEX_DS_NAME;
		}
		try {
			return DBUtil.getDataSource(argDSName);
		} catch (Exception e) {
			
			logger.error( "Error getting DataSource.", e);
		}
		return null;
	}
}
