package com.mind.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;



public class Graphdao 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Graphdao.class);

	public  static float[] getJobsStatus(String appendixid, DataSource ds)
	{
		
		
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int retval=0;
		float[] jobsstates = new float[3];
		
		try {
			conn=ds.getConnection();
			
			cstmt=conn.prepareCall("{?=call lp_job_status_graph (?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,appendixid);
			
			rs=cstmt.executeQuery();
			while(rs.next())
			{
				jobsstates[0]=rs.getInt("Late_job");
				jobsstates[1]=rs.getInt("open_job");
				jobsstates[2]=rs.getInt("complete_job");
			}
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("getJobsStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobsStatus(String, DataSource) - Error occured during getting job graph:::" + e);
			}
		}
		
		
	

		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getJobsStatus(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( SQLException e )
			{
				logger.warn("getJobsStatus(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobsStatus(String, DataSource) - exception ignored", e);
}
			
		}
		return jobsstates;		
	}
	
	
	public  static float[] getActivitiesStatus(String jobid,DataSource ds)
	{
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int retval=0;
		float[] activitystates=new float[3];
		try{
			conn=ds.getConnection();
			
			cstmt=conn.prepareCall("{?=call lp_activity_status_graph (?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,jobid);
			
			rs=cstmt.executeQuery();
			if(rs.next())
			{
				activitystates[0]=rs.getInt("Late_act");
				activitystates[1]=rs.getInt("open_act");
				activitystates[2]=rs.getInt("complete_act");
			}
			retval = cstmt.getInt(1);
		}
		catch(Exception e){
			logger.error("getActivitiesStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitiesStatus(String, DataSource) - Error occured during getting activity graph" + e);
			}
		}
		
		
	

		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getActivitiesStatus(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getActivitiesStatus(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getActivitiesStatus(String, DataSource) - exception ignored", e);
}
			
		}
		return activitystates;		
			
			
	}
	
	
	
	
}
