package com.mind.common;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class MultipartRequestWrapper extends
		org.apache.struts.upload.MultipartRequestWrapper {

	public MultipartRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public DispatcherType getDispatcherType() {
		DispatcherType dt = (DispatcherType) request
				.getAttribute("org.apache.catalina.core.DISPATCHER_TYPE");

		return dt;
	}

	@Override
	public boolean isAsyncSupported() {
		return false;
	}

	@Override
	public ServletContext getServletContext() {
		return request.getSession().getServletContext();
	}
}
