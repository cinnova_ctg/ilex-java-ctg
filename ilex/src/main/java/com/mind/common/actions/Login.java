package com.mind.common.actions;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.mind.bean.LoginForm;
import com.mind.common.EnvironmentSelector;
import com.mind.common.IlexConstants;
import com.mind.common.dao.Logindao;
import com.mind.formbean.CNSWEB.CnsLoginForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.masterSchedulerTask.MasterSchedulerTaskUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

public class Login extends com.mind.common.IlexAction {

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String logout = request.getParameter("logout");
        String sessionParam = request.getParameter("session");
        if (logout != null && logout.compareTo("true") == 0) {
            HttpSession session = request.getSession();

            if (session != null) {
                session.invalidate();
            }

            return mapping.findForward("logout"); // - return to index.jsp
        } else if (sessionParam != null && sessionParam.compareTo("expired") == 0) {
            HttpSession session = request.getSession();

            if (session != null) {
                session.invalidate();
            }

            CnsLoginForm loginForm = (CnsLoginForm) form;
            loginForm.setFailmessage("Session Expired.");
            return mapping.findForward("LoginFail"); // - return to index.jsp
        } else {
            CnsLoginForm loginForm = (CnsLoginForm) form;

            String jsonString = "";
            if (loginForm.getToken() != "" && loginForm.getUserid() != "") {
                jsonString = sendAPIRequestForLoginCheck(loginForm.getToken(), loginForm.getUserid());
            } else {
                jsonString = sendAPIRequestForLogin(loginForm.getUsername(), loginForm.getPassword());
            }

            if (jsonString.compareTo("401") == 0) {
                loginForm.setFailmessage("Session Expired.");
                return mapping.findForward("LoginFail");
            } else if (jsonString.compareTo("") == 0) {
                loginForm.setFailmessage("Empty Response");
                return mapping.findForward("LoginFail");
            }

            JSONObject obj = JSONObject.fromObject(jsonString);

            String status = obj.getString("status");
            if (status.compareTo("error") == 0) {
                String msg = obj.getString("msg");
                loginForm.setFailmessage(msg);
                return mapping.findForward("LoginFail");
            }

            String token = "";
            if (loginForm.getToken() != "" && loginForm.getUserid() != "") {
                token = loginForm.getToken();
            } else {
                token = obj.getString("token");
            }

            JSONObject dataObj = obj.getJSONObject("data");

            String userId = dataObj.getString("id");
            String first_name = dataObj.getString("first_name");
            String last_name = dataObj.getString("last_name");
            String userName = first_name + " " + last_name;
            String userEmail = dataObj.getString("email");

            JSONObject permissions = dataObj.getJSONObject("permissions");

            String userprin = loginForm.getPassword(); // request.getUserPrincipal().toString();
            String username = loginForm.getUsername(); // request.getRemoteUser();
            String id = "";
            LoginForm formdetail = null;
            ArrayList managerList = new ArrayList();
            HttpSession session = request.getSession(true);

            if (loginForm.getToken() == "" || loginForm.getUserid() == "") {
                JSONArray groups = dataObj.getJSONArray("groups");

                session.setAttribute("groups", groups);
            }

            IlexConstants.SERVER_NAME = request.getServerName();
            IlexConstants.IMG_REAL_PATH = this.getServlet().getServletContext().getRealPath("/images");
            IlexConstants.ILEX_REQ_URL = request.getRequestURL().toString();
            String dbName = DatabaseUtilityDao.getKeyValue("DbName");
            if (dbName.trim().equalsIgnoreCase("ilex")) {
                formdetail = Logindao.getUserInfo(username, getDataSource(request, "ilexnewDB"));
            } else {
                formdetail = Logindao.getUserInfoFromDocM(username);
            }

            if ("S".equals(request.getParameter("mode"))) {
                response.setHeader("X-UA-Compatible", "IE=edge;chrome=1");
            } else {
                response.setHeader("X-UA-Compatible", "IE=8");
            }
            request.setAttribute("mode", request.getParameter("mode"));

            session.setAttribute("login_raw", jsonString);
            session.setAttribute("api_url", EnvironmentSelector.getBundleString("api.secure.url"));

            session.setAttribute("userid", userId);
            session.setAttribute("useremail", userEmail);
            session.setAttribute("username", userName);

            session.setAttribute("medius_url", EnvironmentSelector.getBundleString("medius.url"));

            /* Filter value for Project Manager */
            session.setAttribute("ownerId", formdetail.getLogin_user_id());
            session.setAttribute("viewjobtype", "ION");
            // Latest PM UI Changes to make the selection in MSA List when user logs
            // in.
            session.setAttribute("selectedStatus", "('D','R','P','A','S')");
            session.setAttribute("MSASelectedStatus", "('D','R','P','A','S')");
            session.setAttribute("tempStatus", "D,R,P,A,S");

            session.setAttribute("selectedOwners", "(" + session.getAttribute("userid").toString() + ")");
            session.setAttribute("MSASelectedOwners", "(" + session.getAttribute("userid").toString() + ")");
            session.setAttribute("tempOwner", session.getAttribute("userid").toString());

            session.setAttribute("jobSelectedStatus", "('II','IO','ION','IOF')");
            session.setAttribute("jobSelectedOwners", "(" + session.getAttribute("userid").toString() + ")");
            session.setAttribute("jobTempStatus", "II,IO,ION,IOF");
            session.setAttribute("jobTempOwner", session.getAttribute("userid").toString());
            session.setAttribute("timeFrame", "clear");

            session.setAttribute("RDM", Logindao.checkRDMRole(formdetail.getLogin_user_id(), getDataSource(request, "ilexnewDB"))); // - Set Attribute for
            // RDM Role Used in PVS
            // Manager
            session.setAttribute("RdmRds", Logindao.checkRdmRdsRole(formdetail.getLogin_user_id(), getDataSource(request, "ilexnewDB"))); // - Set Attribute for
            // RDM or RDS Role Used
            // in PVS Manager

            session.removeAttribute("managerList");
            managerList = Logindao.getManagerList(formdetail.getLogin_user_id(), getDataSource(request, "ilexnewDB")); // - Set Attribute for RDM
            // Role Used in PVS
            // Manager
            session.setAttribute("managerList", managerList);
            session.setAttribute("size", managerList.size());
            // Logging.setDebugLevel();

            session.setAttribute("isUserRoleSeniorPM", Logindao.isRoleSeniorProjectManager(formdetail.getLogin_user_id(), getDataSource(request, "ilexnewDB")));

            // new values to be added to session 6/25/2015
            session.setAttribute("token", token);
            session.setAttribute("permissions", permissions);

            if (request.getParameter("module") != null) { // - call from url-link of
                // email of Appendix
                // Creation/Project
                // Manager Updation.
                String moduleName = request.getParameter("module");
                String LinkURL = request.getRequestURL().toString().replaceAll(request.getServletPath(), "/") + request.getQueryString();
                session.setAttribute("moduleName", moduleName);
                session.setAttribute("LinkURL", LinkURL);
                return mapping.findForward("linkurl"); // - return to index2.jsp
            } // - end of if

            if (request.getParameter("rpt") != null) { // - for RPT Module
                return mapping.findForward("RPT"); // - return to /RPT/RPTindex.jsp
            }

            if (request.getParameter("msp") != null) { // - for MSP Module
                return mapping.findForward("MSP"); // - return to /MSP/MspIndex.jsp
            }

            return mapping.findForward("success"); // - return to index.jsp
        }
    }

    public final static String CONTENT_TYPE_KEY = "Content-type";
    public final static String CONTENT_TYPE_VALUE = "application/json";
    public final static String POST_REQUEST = "POST";
    public final static String PROTOCOL = "TLSv1";
    public final static String PATH = "ms/project/template_task/auto_complete/";
    static Logger logger = Logger.getLogger(Login.class);

    private static String getBasicAuthenticationEncoding() {
        String userPassword = EnvironmentSelector.getBundleString("api.username") + ":" + EnvironmentSelector.getBundleString("api.password");
        return new String(Base64.encodeBase64(userPassword.getBytes()));
    }

    public static String sendAPIRequestForLogin(String userName, String password) {
        String jsonString = "";
        HttpsURLConnection httpcon = null;
        try {
            URL url = new URL(EnvironmentSelector.getBundleString("api.secure.url") + "ilex/login");
            URLConnection uc = url.openConnection();

            uc.setRequestProperty("Authorization", "Basic " + getBasicAuthenticationEncoding());

            SSLContext ssl = SSLContext.getInstance(PROTOCOL);
            ssl.init(null, new TrustManager[]{new SimpleX509TrustManager()}, null);
            SSLSocketFactory factory = ssl.getSocketFactory();

            httpcon = (HttpsURLConnection) (uc);

            httpcon.setRequestMethod(POST_REQUEST);
            httpcon.setDoInput(true);
            httpcon.setDoOutput(true);
            httpcon.setSSLSocketFactory(factory);

            httpcon.setHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            // setting up the parameters for POST request.
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("username", userName);
            params.put("password", password);

            // params.put("task_type_id", bean.getTaskTypeId());
            // params.put("task_type_key", bean.getTaskTypeKey());
            params.put("user_id", "4");
            params.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
            // to send the param in POST.
            OutputStream os = httpcon.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostParamString(params));
            writer.flush();
            writer.close();
            os.close();

            httpcon.connect();

            // to get response.
            InputStream is = httpcon.getInputStream();
            Properties properties = new Properties();
            properties.load(is);

            jsonString = properties.toString();
            jsonString = jsonString.substring(1, jsonString.length() - 1);
            jsonString = jsonString.replace("=", ":");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        try {
            if (httpcon != null) {
                if (httpcon.getResponseCode() == 401) {
                    jsonString = "401";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return jsonString;
    }

    public static String sendAPIRequestForLoginCheck(String token, String user_id) {
        String jsonString = "";
        HttpsURLConnection httpcon = null;
        try {
            if (token == null || user_id == null) {
                return "401";
            }

            URL url = new URL(EnvironmentSelector.getBundleString("api.secure.url") + "ilex/user");
            URLConnection uc = url.openConnection();

            SSLContext ssl = SSLContext.getInstance(PROTOCOL);
            ssl.init(null, new TrustManager[]{new SimpleX509TrustManager()}, null);
            SSLSocketFactory factory = ssl.getSocketFactory();

            httpcon = (HttpsURLConnection) (uc);

            httpcon.setRequestMethod(POST_REQUEST);
            httpcon.setDoInput(true);
            httpcon.setDoOutput(true);
            httpcon.setSSLSocketFactory(factory);

            httpcon.setRequestProperty("x-access-token", token);
            httpcon.setRequestProperty("x-access-user_id", user_id);

            httpcon.setHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            // setting up the parameters for POST request.
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("user_id", user_id);

            params.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
            // to send the param in POST.

            OutputStream os = httpcon.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostParamString(params));
            writer.flush();
            writer.close();
            os.close();

            httpcon.connect();

            // to get response.
            InputStream is = httpcon.getInputStream();
            Properties properties = new Properties();
            properties.load(is);

            jsonString = properties.toString();
            jsonString = jsonString.substring(1, jsonString.length() - 1);
            jsonString = jsonString.replace("=", ":");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        try {
            if (httpcon != null) {
                if (httpcon.getResponseCode() == 401) {
                    jsonString = "401";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return jsonString;
    }

    private static String getPostParamString(Hashtable<String, String> params) {
        if (params.size() == 0) {
            return "";
        }

        StringBuffer buf = new StringBuffer();
        Enumeration<String> keys = params.keys();
        while (keys.hasMoreElements()) {
            buf.append(buf.length() == 0 ? "" : "&");
            String key = keys.nextElement();
            buf.append(key).append("=").append(params.get(key));
        }
        return buf.toString();
    }

}

class SimpleX509TrustManager implements X509TrustManager {

    public void checkClientTrusted(X509Certificate[] cert, String s)
            throws CertificateException {
    }

    public void checkServerTrusted(X509Certificate[] cert, String s)
            throws CertificateException {
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        // TODO Auto-generated method stub
        return null;
    }

}
