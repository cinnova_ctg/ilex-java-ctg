package com.mind.common.actions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PVS.Pvsdao;
import com.mind.fw.core.dao.util.DBUtil;

public class SendEmailAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SendEmailAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String rdmEmailID = "";
		String partnerName = "";
		String partnerLocation = "";
		String isEmailNotProvided = "N";
		Statement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		DataSource ds = getDataSource(request, "ilexnewDB");
		try {
			if (request.getParameter("pid") != null) {
				rdmEmailID = Pvsdao.getResourceDevManagerEmail(ds);
				partnerName = Pvsdao.getPartnername(
						request.getParameter("pid"), ds);
				con = ds.getConnection();
				stmt = con.createStatement();
				rs = stmt
						.executeQuery("select lo_ad_city,lo_ad_state from [dbo].[func_cp_partner_info_general_tab]("
								+ request.getParameter("pid") + ")");
				if (rs.next()) {
					partnerLocation = rs.getString("lo_ad_city") + ", "
							+ rs.getString("lo_ad_state");
				}
				response.setContentType("text/html");
				response.getWriter()
						.write(rdmEmailID + ";" + partnerName + ", "
								+ partnerLocation);
			} else if (request.getParameter("checkNotProvidedPid") != null) {
				con = ds.getConnection();
				stmt = con.createStatement();
				rs = stmt
						.executeQuery("select lo_pc_email1 from cp_partner inner join  cp_partner_details on cp_partner_id = cp_pd_partner_id inner join lo_poc on cp_pd_pri_pc_id = lo_pc_id where cp_partner_id ="
								+ request.getParameter("checkNotProvidedPid"));
				if (rs.next()) {
					if (rs.getString(1).equals("Not Provided")) {
						isEmailNotProvided = "Y";
					}
				}
				response.setContentType("text/html");
				response.getWriter().write(isEmailNotProvided);
			}
		} catch (Exception e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(rs);
			DBUtil.close(con);
		}
		return null;
	}

}
