package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.Graphdao;
import com.mind.common.formbean.GraphForm;

public class ViewGraph extends com.mind.common.IlexAction
{

    public ViewGraph()
    {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
        throws Exception
    {	
        GraphForm graph = new GraphForm();
		
		if(request.getParameter("graphtype").equals("jobgraph"))
		{
			float[] jobstatus = new float[4];
			jobstatus=(float[])Graphdao.getJobsStatus(request.getParameter("appendixid"),getDataSource( request , "ilexnewDB" ));
			
			graph.setTITLECHART("");
	        graph.setLEGEND("TRUE");
	        graph.setSERIE_1("Pie");
	        graph.setSERIE_TYPE_1("PIE");
			
	        graph.setPIE_STYLE_1("#F7C046");
	        graph.setPIE_STYLE_2("#86A9D8");
	        graph.setPIE_STYLE_3("#BCE14B");
	        
	        graph.setPIE_NAME_1("Late");
	        graph.setPIE_NAME_2("Open");
	        graph.setPIE_NAME_3("Complete");
	        
	        graph.setPIE_LABEL_FORMAT("#PERCENTAGE#");
	      
			graph.setSERIE_DATA_1(jobstatus[0]+"|"+jobstatus[1]+"|"+jobstatus[2]);
			
			graph.setSERIE_FONT_1("Arial|6");
			
	        graph.setSERIE_LABELS_1("Late|Open|Complete");
	        graph.setSERIE_TOGETHER_1("true|true");
	        graph.setCHART_BORDER("0.2|WHITE|LINE");
	        graph.setCHART_FILL("#FFFFFF");
	        graph.setLEGEND_BORDER("0.2|WHITE|LINE");
	        graph.setLEGEND_VERTICAL("true");
	        graph.setLEGEND_POSITION("RIGHT");
	        graph.setLEGEND_FILL("WHITE");
	        graph.setPIECHART_3D("false");
	        graph.setWIDTH("290");
	        graph.setHEIGHT("200");
	        graph.setLENGTH("22");
	        graph.setLEGEND_TITLE("");
		}
		
		
		if(request.getParameter("graphtype").equals("activitygraph"))
		{
			float[] activitiesstatus = new float[4];
			
			activitiesstatus=(float[])Graphdao.getActivitiesStatus(request.getParameter("jobid"),getDataSource( request , "ilexnewDB" ));
			
			graph.setTITLECHART("");
	        graph.setLEGEND("TRUE");
	        graph.setSERIE_1("Pie");
	        graph.setSERIE_TYPE_1("PIE");
			
			 graph.setPIE_STYLE_1("#F7C046");
		     graph.setPIE_STYLE_2("#86A9D8");
		     graph.setPIE_STYLE_3("#BCE14B");
				
	        graph.setPIE_NAME_1("Late");
	        graph.setPIE_NAME_2("Open");
	        graph.setPIE_NAME_3("Complete");
	        
	        graph.setPIE_LABEL_FORMAT("#PERCENTAGE#");
            graph.setSERIE_DATA_1(activitiesstatus[0]+"|"+activitiesstatus[1]+"|"+activitiesstatus[2]);
			
			graph.setSERIE_FONT_1("Arial|6");
			
	        graph.setSERIE_LABELS_1("Late|Open|Complete");
	        graph.setSERIE_TOGETHER_1("true|true|true");
	        graph.setCHART_BORDER("0.2|WHITE|LINE");
	        graph.setCHART_FILL("#ECECEC");
	        graph.setLEGEND_BORDER("0.2|WHITE|LINE");
	        graph.setLEGEND_VERTICAL("true");
	        graph.setLEGEND_POSITION("RIGHT");
	        graph.setLEGEND_FILL("WHITE");
	        graph.setPIECHART_3D("false");
	        graph.setWIDTH("290");
	        graph.setHEIGHT("200");
	        graph.setLENGTH("22");
	        graph.setLEGEND_TITLE("Activity");
		}
        
		/*if(request.getParameter("graphtype").equals("activitygraph"))
		{
			float[] resourcecost = new float[4];
			resourcecost=Graphdao.getDifferentResourceTotalCost(request.getParameter("jobid"),request.getParameter("activityid"),getDataSource( request , "ilexnewDB" ));
			
			graph.setTITLECHART("");
	        graph.setLEGEND("TRUE");
	        graph.setSERIE_1("Pie");
	        graph.setSERIE_TYPE_1("PIE");
	        graph.setPIE_STYLE_1("RED");
	        graph.setPIE_STYLE_2("BLUE");
	        graph.setPIE_STYLE_3("GREEN");
	        graph.setPIE_STYLE_4("BLACK");
	        graph.setPIE_NAME_1("Open");
	        graph.setPIE_NAME_2("Closed");
	        graph.setPIE_NAME_3("Complete");
	        graph.setPIE_NAME_4("Hold");
	        graph.setPIE_LABEL_FORMAT("#PERCENTAGE#");
	        graph.setSERIE_DATA_1(resourcecost[0]+"|"+resourcecost[1]+"|"+resourcecost[2]+"|"+resourcecost[3]);
	        graph.setSERIE_FONT_1("Arial|6");
			
	        graph.setSERIE_LABELS_1("Material|Labor|Freight|Travel");
	        graph.setSERIE_TOGETHER_1("true|true|true");
	        graph.setCHART_BORDER("0.2|WHITE|LINE");
	        graph.setCHART_FILL("#ECECEC");
	        graph.setLEGEND_BORDER("0.2|WHITE|LINE");
	        graph.setLEGEND_VERTICAL("true");
	        graph.setLEGEND_POSITION("RIGHT");
	        graph.setLEGEND_FILL("WHITE");
	        graph.setPIECHART_3D("false");
	        graph.setWIDTH("290");
	        graph.setHEIGHT("200");
	        graph.setLENGTH("24");
	        graph.setLEGEND_TITLE("Job");
		}*/
        
        
        request.setAttribute("GraphForm", graph);
        return mapping.findForward("success");
    }
}
