package com.mind.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.PartnerSISCBean;
import com.mind.common.dao.PartnerSOWAssumptiondao;
import com.mind.common.formbean.PartnerSISCForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.MSAdao;
import com.mind.util.WebUtil;

public class PartnerSISCAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		PartnerSISCForm partnerSISCForm = (PartnerSISCForm) form;

		HttpSession session = request.getSession(true);
		String loginUserId = (String) session.getAttribute("userid");

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("appendixId") != null)
			partnerSISCForm.setAppendixId(request.getParameter("appendixId"));

		if (request.getParameter("fromType") != null)
			partnerSISCForm.setFromType(request.getParameter("fromType"));

		if (request.getParameter("viewjobtype") != null)
			partnerSISCForm.setViewJobType(request.getParameter("viewjobtype"));

		if (request.getParameter("ownerId") != null)
			partnerSISCForm.setOwnerId(request.getParameter("ownerId"));

		if (request.getParameter("nettype") != null)
			partnerSISCForm.setNetType(request.getParameter("nettype"));

		if (request.getParameter("viewType") != null) {
			partnerSISCForm.setViewType(request.getParameter("viewType"));

			if (partnerSISCForm.getViewType().equals("SI"))
				partnerSISCForm.setDataType("Special Instruction");
			if (partnerSISCForm.getViewType().equals("SC"))
				partnerSISCForm.setDataType("Special Condition");
		}

		partnerSISCForm.setAppendixName(Appendixdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				partnerSISCForm.getAppendixId()));

		if (partnerSISCForm.getSave() != null) {
			int retvalue = 0;
			retvalue = PartnerSOWAssumptiondao.managePartnerSISC(
					partnerSISCForm.getAppendixId(),
					partnerSISCForm.getFromType(),
					partnerSISCForm.getSIValue(), partnerSISCForm.getSCValue(),
					loginUserId, getDataSource(request, "ilexnewDB"));
			request.setAttribute("addFlag", retvalue + "");
			request.setAttribute("appendixId", partnerSISCForm.getAppendixId());
			request.setAttribute("fromType", partnerSISCForm.getFromType());
			request.setAttribute("viewjobtype",
					partnerSISCForm.getViewJobType());
			request.setAttribute("ownerId", partnerSISCForm.getOwnerId());
			request.setAttribute("nettype", partnerSISCForm.getNetType());

			return mapping.findForward("tempappendixpage");
		}

		if (partnerSISCForm.getDelete() != null) {
			int retvalue = -1;
			if (request.getParameter("SCSIId").equals("0")) {
				if (partnerSISCForm.getViewType().equals("SI"))
					partnerSISCForm.setSIValue("");
				if (partnerSISCForm.getViewType().equals("SC"))
					partnerSISCForm.setSCValue("");
				retvalue = 0;

			} else {
				retvalue = PartnerSOWAssumptiondao.deletePartnerSISC(
						partnerSISCForm.getAppendixId(),
						partnerSISCForm.getViewType(),
						partnerSISCForm.getFromType(),
						getDataSource(request, "ilexnewDB"));
			}
			request.setAttribute("deleteFlag", retvalue + "");
		}

		/* Get data value */
		PartnerSISCBean partnerSISCBean = new PartnerSISCBean();
		BeanUtils.copyProperties(partnerSISCBean, partnerSISCForm);
		PartnerSOWAssumptiondao.getPartnerSISCValue(partnerSISCBean,
				partnerSISCForm.getAppendixId(), partnerSISCForm.getFromType(),
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(partnerSISCForm, partnerSISCBean);

		/* Set header variable */
		if (partnerSISCForm.getFromType().equals("prm_app")) {
			partnerSISCForm.setContextType("Appendix");
			// partnerSISCForm.setContext(Appendixdao.getAppendixname(getDataSource(
			// request , "ilexnewDB" ), partnerSISCForm.getFromId()));
			partnerSISCForm
					.setContext("<a href=AppendixHeader.do?function=view&viewjobtype="
							+ partnerSISCForm.getViewJobType()
							+ "&ownerId="
							+ partnerSISCForm.getOwnerId()
							+ "&appendixid="
							+ partnerSISCForm.getAppendixId()
							+ ">"
							+ Appendixdao.getAppendixname(
									getDataSource(request, "ilexnewDB"),
									partnerSISCForm.getAppendixId()) + "</a>");
		}

		/* Start : Appendix Dashboard View Selector Code */
		if (request.getParameter("fromPage") != null)
			partnerSISCForm.setFromPage(request.getParameter("fromPage"));

		if (partnerSISCForm.getJobOwnerOtherCheck() != null
				&& !partnerSISCForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					partnerSISCForm.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", partnerSISCForm.getAppendixId());
			request.setAttribute("msaId", partnerSISCForm.getMsaId());

			return mapping.findForward("temppage");

		}

		if (partnerSISCForm.getJobSelectedStatus() != null
				&& partnerSISCForm.getJobSelectedStatus().length != 0) {
			for (int i = 0; i < partnerSISCForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ partnerSISCForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ partnerSISCForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (partnerSISCForm.getJobSelectedOwners() != null
				&& partnerSISCForm.getJobSelectedOwners().length != 0) {
			for (int j = 0; j < partnerSISCForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ partnerSISCForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ partnerSISCForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					partnerSISCForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					partnerSISCForm.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", partnerSISCForm.getAppendixId());
			request.setAttribute("msaId", partnerSISCForm.getMsaId());

			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				partnerSISCForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				partnerSISCForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				partnerSISCForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				partnerSISCForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(loginUserId,
				partnerSISCForm.getJobOwnerOtherCheck(), "prj_job_new",
				partnerSISCForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		partnerSISCForm.setMsaId(IMdao.getMSAId(
				partnerSISCForm.getAppendixId(),
				getDataSource(request, "ilexnewDB")));
		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(partnerSISCForm.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				partnerSISCForm.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("appendixid", partnerSISCForm.getAppendixId());
		request.setAttribute("msaId", partnerSISCForm.getMsaId());
		request.setAttribute("viewjobtype", partnerSISCForm.getViewJobType());
		request.setAttribute("ownerId", partnerSISCForm.getOwnerId());

		return mapping.findForward("success");
	}
}
