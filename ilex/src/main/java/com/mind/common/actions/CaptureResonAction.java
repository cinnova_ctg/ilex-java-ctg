package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.CaptureReason;
import com.mind.common.dao.Menu;
import com.mind.common.formbean.CaptureResonForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.JobDashboarddao;

public class CaptureResonAction extends com.mind.common.IlexDispatchAction {
	public ActionForward showReson(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String Type = "";
		String Job_Id = "";
		String status = "";
		String MSA_Id = "";
		String Appendix_Id = "";
		CaptureResonForm captureresonform = (CaptureResonForm) form;

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (captureresonform.getType() == null
				|| captureresonform.getType().equals("")) {
			Type = (request.getAttribute("Type") != null) ? request
					.getAttribute("Type").toString() : "";
			Job_Id = (request.getAttribute("Job_Id") != null) ? request
					.getAttribute("Job_Id").toString() : "";
			status = (request.getAttribute("status") != null) ? request
					.getAttribute("status").toString() : "";
			MSA_Id = (request.getAttribute("MSA_Id") != null) ? request
					.getAttribute("MSA_Id").toString() : "";
			Appendix_Id = (request.getAttribute("Appendix_Id") != null) ? request
					.getAttribute("Appendix_Id").toString() : "";

			captureresonform.setType(Type);
			captureresonform.setJobId(Job_Id);
			captureresonform.setMsaId(MSA_Id);
			captureresonform.setAppendixId(Appendix_Id);

			if (Type.trim().equalsIgnoreCase("prj_job")) {
				captureresonform.setType("Addendum");
			}
		}

		// System.out.println("Type in the formbean is :" +
		// captureresonform.getType());
		// System.out.println("Type is" + Type);
		// System.out.println("Job_Id" + Job_Id);
		// System.out.println("MSA_Id" + MSA_Id);
		// System.out.println("Appendix_Id" + Appendix_Id);

		captureresonform.setStatusAction(returnStatusAction(Type, Job_Id,
				status, MSA_Id, Appendix_Id));
		request.setAttribute("statusAction", captureresonform.getStatusAction());
		// System.out.println("Status Action is ::: " +
		// captureresonform.getStatusAction());

		if (captureresonform.getType().equalsIgnoreCase("MSA")) {
			captureresonform.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					captureresonform.getMsaId()));
			request.setAttribute("MSA_Id", captureresonform.getMsaId());
			String msaStatus = MSAdao.getMSAStatus(captureresonform.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String msaUploadCheck = MSAdao.getMSAUploadCheck(
					captureresonform.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
					captureresonform.getMsaId(), "", "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);
			request.setAttribute("status", msaStatus);
			request.setAttribute("uploadstatus", msaUploadCheck);
		}

		if (captureresonform.getType().equals("Appendix")) {
			captureresonform.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					captureresonform.getMsaId()));
			String appendixStatus = Appendixdao.getAppendixStatus(
					captureresonform.getAppendixId(),
					captureresonform.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String addendumlist = Appendixdao.getAddendumsIdName(
					captureresonform.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(captureresonform.getAppendixId(),
							getDataSource(request, "ilexnewDB"));
			String appendixType = Appendixdao.getAppendixPrType(
					captureresonform.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			captureresonform.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					captureresonform.getAppendixId()));
			String list = Menu.getStatus("pm_appendix",
					appendixStatus.charAt(0), captureresonform.getMsaId(),
					captureresonform.getAppendixId(), "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixStatusList", list);
			request.setAttribute("Appendix_Id",
					captureresonform.getAppendixId());
			request.setAttribute("MSA_Id", captureresonform.getMsaId());
			request.setAttribute("appendixStatus", appendixStatus);
			request.setAttribute("latestaddendumid", addendumid);
			request.setAttribute("addendumlist", addendumlist);
			request.setAttribute("appendixType", appendixType);
		}

		if (captureresonform.getType().equals("Addendum")) {
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					captureresonform.getJobId(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			captureresonform.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"),
					captureresonform.getJobId()));
			String jobViewtype = JobDashboarddao.getJobViewType(
					captureresonform.getJobId(),
					getDataSource(request, "ilexnewDB"));
			captureresonform.setAppendixId(IMdao.getAppendixId(
					captureresonform.getJobId(),
					getDataSource(request, "ilexnewDB")));
			captureresonform.setMsaId(IMdao.getMSAId(
					captureresonform.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			captureresonform.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					captureresonform.getAppendixId()));
			captureresonform.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					captureresonform.getMsaId()));
			String list = Menu.getStatus("Addendum", jobStatus.charAt(0), "",
					captureresonform.getAppendixId(),
					captureresonform.getJobId(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("addendumStatusList", list);
			String list_new = Menu.getStatus("Addendum_New", jobStatus.charAt(0), "",
					captureresonform.getAppendixId(),
					captureresonform.getJobId(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("addendumStatusListNew", list_new);
			request.setAttribute("Appendix_Id",
					captureresonform.getAppendixId());
			request.setAttribute("Job_Id", captureresonform.getJobId());
			request.setAttribute("job_Status", jobStatus);
			request.setAttribute("jobViewType", jobViewtype);
		}

		return mapping.findForward("success");
	}

	public ActionForward updateReson(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String Id = "";
		String Type = "";
		String reson = "";
		if (request.getParameter("MSA_Id") != null) {
			Id = request.getParameter("MSA_Id");
		}
		if (request.getParameter("Appendix_Id") != null) {
			Id = request.getParameter("Appendix_Id");
		}
		if (request.getParameter("Job_Id") != null) {
			Id = request.getParameter("Job_Id");
		}

		// System.out.println("XXXXX " + request.getParameter("reson"));
		reson = (request.getParameter("reson") != null) ? request
				.getParameter("reson") : "";
		Type = (request.getParameter("Type") != null) ? request
				.getParameter("Type") : "";
		// System.out.println("Id is :::" + Id);
		// System.out.println("Type is :::" + Type);
		int ans = CaptureReason.updateReasonForChange(Id, Type, reson,
				getDataSource(request, "ilexnewDB"));
		// System.out.println(ans);
		response.setContentType("text/html");
		response.getWriter().write(ans + "");
		return null;
	}

	public static String returnStatusAction(String Type, String Job_Id,
			String status, String MSA_Id, String Appendix_Id) {
		StringBuffer statusAction = new StringBuffer();
		statusAction.append("MenuFunctionChangeStatusAction.do?Type=");
		statusAction.append(Type);

		// System.out.println("In making of the String" + Appendix_Id);

		if (!Job_Id.trim().equalsIgnoreCase("")) {
			statusAction.append("&Job_Id=" + Job_Id);
		}
		if (!MSA_Id.trim().equalsIgnoreCase("")) {
			statusAction.append("&MSA_Id=" + MSA_Id);
		}
		if (!Appendix_Id.trim().equalsIgnoreCase("")) {
			statusAction.append("&Appendix_Id=" + Appendix_Id);
		}
		statusAction.append("&Status=" + status);
		statusAction.append("&resonCaptured=true");

		return statusAction.toString();
	}
}
