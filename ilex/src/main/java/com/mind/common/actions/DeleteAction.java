package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.EnvironmentSelector;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.POWOdao;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotMarkDeleteException;
import com.mind.docm.exceptions.CouldNotPhysicallyDeleteException;
import com.mind.docm.exceptions.DocumentIdNotFoundException;
import com.mind.docm.exceptions.DocumentNotExistsException;
public class DeleteAction extends com.mind.common.IlexAction
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DeleteAction.class);

	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		/* Page Security Start*/
		
		HttpSession session = request.getSession( true );
		
		String userid = ( String )session.getAttribute("userid");
		String loginusername = ( String ) session.getAttribute( "username" );
		String temp = "";
		boolean flag = true;
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( request.getParameter( "Type" ).equals ( "am" ) )
			temp = "Activity";
		
		else 
			temp = request.getParameter( "Type" );
		
		if( request.getParameter( "Type" ).equals ( "POWO" ) ) flag = false;
		
		if(flag) {
			if( !Authenticationdao.getPageSecurity( userid , temp , "Delete" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
		}
		/* Page Security End*/
		
		
		
		if( request.getParameter( "Type" ).equals( "MSA" ) )
		{
//			try{
//				EntityManager.deleteEntity(request.getParameter( "MSA_Id" ),"MSA",false,Util.changeStringToDoubleQuotes(loginusername));
//				}catch(CouldNotCheckDocumentException e) {			
//				}catch(DocumentNotExistsException e) {			
//				}catch(CouldNotMarkDeleteException e) {
//				}catch(DocumentIdNotFoundException e) {			
//				}catch(CouldNotPhysicallyDeleteException e) {
//				}catch(Exception e) {			
//			}	
			int deleteflag = MSAdao.deletemsa( request.getParameter( "MSA_Id" ) , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "deleteflag" , ""+deleteflag );
			return mapping.findForward( "msatabularpage" );
		}
		
		if( request.getParameter( "Type" ).equals( "POWO" ) )
		{
			try{
				EntityManager.deleteEntity(request.getParameter( "powoid" ),"PO",false,Util.changeStringToDoubleQuotes(loginusername));
				EntityManager.deleteEntity(request.getParameter( "powoid" ),"WO",false,Util.changeStringToDoubleQuotes(loginusername));
			}catch(CouldNotCheckDocumentException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);			
			}catch(DocumentNotExistsException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);			
			}catch(CouldNotMarkDeleteException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);
			}catch(DocumentIdNotFoundException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);			
			}catch(CouldNotPhysicallyDeleteException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);
			}catch(Exception e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);		
			}	
			int deleteflag = POWOdao.deletePOWO( request.getParameter( "powoid" ) , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "deleteflag" , ""+deleteflag );
			request.setAttribute( "jobid" , ""+request.getParameter( "jobid" ) );
			
			return mapping.findForward( "powodashboard" );
		}
		
		if( request.getParameter( "Type" ).equals( "Appendix" ) )
		{
			try{
				EntityManager.deleteEntity(request.getParameter( "Appendix_Id" ),"Appendix",false,Util.changeStringToDoubleQuotes(loginusername));
			}catch(CouldNotCheckDocumentException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);			
			}catch(DocumentNotExistsException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);			
			}catch(CouldNotMarkDeleteException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);
			}catch(DocumentIdNotFoundException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);			
			}catch(CouldNotPhysicallyDeleteException e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);
			}catch(Exception e) {
				logger.warn("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored", e);			
			}	
			int deleteflag = Appendixdao.deleteappendix( request.getParameter( "Appendix_Id" ) , getDataSource( request , "ilexnewDB" ) );
			
			request.setAttribute( "deleteflag" , ""+deleteflag );
			request.setAttribute( "MSA_Id" , request.getParameter( "MSA_Id" ) );
			
			return mapping.findForward( "appendixtabularpage" );
		}
		
		if( request.getParameter( "Type" ).equals( "Job" ) )
		{
			//query for deleting job
			int deleteflag = Jobdao.deletejob( request.getParameter( "Job_Id" ) , getDataSource( request , "ilexnewDB" ) );
			request.setAttribute( "deleteflag" , ""+deleteflag );
			request.setAttribute( "Appendix_Id" , request.getParameter( "Appendix_Id" ) );
			request.setAttribute( "ref" , request.getParameter( "ref" ) );
			request.setAttribute( "Job_Id" , request.getParameter( "Job_Id" ) );
			return mapping.findForward( "jobtabularpage" );
		}
		
		if( request.getParameter( "Type" ).equals( "Activity" ) )
		{
			int val;   //query for deleting activity
			//mySqlUrl=this.getResources(request).getMessage("appendix.detail.changestatus.mysql.url");
			String mySqlUrl = EnvironmentSelector.getBundleString("appendix.detail.changestatus.mysql.url");
			int deleteflag = Activitydao.deleteactivity(request.getParameter( "Activity_Id" ) , getDataSource( request , "ilexnewDB" ), mySqlUrl );
			
			request.setAttribute( "deleteflag" , ""+deleteflag );
			request.setAttribute( "ref" , request.getParameter( "ref" ) );
			request.setAttribute( "Job_Id" , request.getParameter( "Job_Id" ) );
			
			return mapping.findForward( "activitytabularpage" );
		}
		
		if( request.getParameter( "Type" ).equals( "am" ) )
		{
			 int deleteflag = ActivityLibrarydao.deleteactivity( request.getParameter( "Activitylibrary_Id" ) , getDataSource( request , "ilexnewDB" ) );
			
			request.setAttribute( "deleteflag" , ""+deleteflag );
			request.setAttribute( "MSA_Id" , request.getParameter( "MSA_Id" ) );
			return mapping.findForward( "activitymanagertabularpage" );
		}
		
		else
			return mapping.findForward( "errorpage" );
	}
}
