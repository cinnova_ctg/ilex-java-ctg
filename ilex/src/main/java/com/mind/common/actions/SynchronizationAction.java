package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.SynchronizationDao;

public class SynchronizationAction extends com.mind.common.IlexAction
{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		
		SynchronizationDao.syncOrganizationType();
		SynchronizationDao.syncOrganizationDicipline();
		SynchronizationDao.syncLoRole();
		SynchronizationDao.syncFunctionListFromDocMToIlex();
		SynchronizationDao.synCcFunction();
		SynchronizationDao.synRolePriviledge();
		
		return null;
	}

}
