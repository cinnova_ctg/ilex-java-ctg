package com.mind.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.common.dao.PartnerSOWAssumptiondao;
import com.mind.common.dao.ViewList;
import com.mind.common.formbean.PartnerManageSOWAssumptionForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.util.WebUtil;

public class PartnerSOWAssumptionAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		PartnerManageSOWAssumptionForm SowAssumptionForm = (PartnerManageSOWAssumptionForm) form;

		HttpSession session = request.getSession(true);
		String loginUserId = (String) session.getAttribute("userid");

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobAppStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (request.getParameter("fromId") != null)
			SowAssumptionForm.setFromId(request.getParameter("fromId"));

		if (request.getParameter("fromType") != null)
			SowAssumptionForm.setFromType(request.getParameter("fromType"));

		if (request.getParameter("actId") != null)
			SowAssumptionForm.setActId(request.getParameter("actId"));

		if (request.getParameter("viewType") != null)
			SowAssumptionForm.setViewType(request.getParameter("viewType"));

		if (request.getParameter("viewjobtype") != null)
			SowAssumptionForm.setViewJobType(request
					.getParameter("viewjobtype"));

		if (request.getParameter("ownerId") != null)
			SowAssumptionForm.setOwnerId(request.getParameter("ownerId"));

		if (request.getParameter("nettype") != null)
			SowAssumptionForm.setNetType(request.getParameter("nettype"));

		SowAssumptionForm.setActivityName(PartnerSOWAssumptiondao
				.getPartnerActivityName(SowAssumptionForm.getActId(),
						SowAssumptionForm.getFromType(),
						getDataSource(request, "ilexnewDB")));

		if (SowAssumptionForm.getSave() != null) {
			int retvalue = -1;
			retvalue = PartnerSOWAssumptiondao.managePartnerSOWAssumption(
					SowAssumptionForm.getActId(),
					SowAssumptionForm.getFromType(),
					SowAssumptionForm.getPartnerInformation(),
					SowAssumptionForm.getViewType(), loginUserId,
					getDataSource(request, "ilexnewDB"));

			// System.out.println("addFlag in Action:::::::::"+retvalue);
			request.setAttribute("addFlag", retvalue + "");
			request.setAttribute("fromId", SowAssumptionForm.getFromId());
			request.setAttribute("fromType", SowAssumptionForm.getFromType());
			request.setAttribute("viewType", SowAssumptionForm.getViewType());
			request.setAttribute("viewjobtype",
					SowAssumptionForm.getViewJobType());
			request.setAttribute("ownerId", SowAssumptionForm.getOwnerId());
			request.setAttribute("nettype", SowAssumptionForm.getNetType());

			return mapping.findForward("templistpage");
		}

		/* Get data value */
		SowAssumptionForm.setPartnerInformation(PartnerSOWAssumptiondao
				.getPartnerDataValue(SowAssumptionForm.getActId(),
						SowAssumptionForm.getFromType(),
						SowAssumptionForm.getViewType(),
						getDataSource(request, "ilexnewDB")));

		/* Set header variable */
		if (SowAssumptionForm.getViewType().equals("sow"))
			SowAssumptionForm.setDataType("SOW");

		if (SowAssumptionForm.getViewType().equals("assumption"))
			SowAssumptionForm.setDataType("Assumptions");

		if (SowAssumptionForm.getFromType().equals("am_act")) {
			SowAssumptionForm
					.setContext("<a href=ActivitySummaryAction.do?ref=View&MSA_Id="
							+ SowAssumptionForm.getFromId()
							+ ">"
							+ Appendixdao.getMsaname(
									getDataSource(request, "ilexnewDB"),
									SowAssumptionForm.getFromId()) + "</a>");

			if (SowAssumptionForm.getContext().equals("Master Library"))
				SowAssumptionForm.setContextType("Master Library");
			else
				SowAssumptionForm.setContextType("MSA");
		}

		if (SowAssumptionForm.getFromType().equals("pm_act")) {
			SowAssumptionForm.setContextType("Appendix");
			// SowAssumptionForm.setContext(Appendixdao.getAppendixname(getDataSource(
			// request , "ilexnewDB" ),
			// IMdao.getAppendixId(SowAssumptionForm.getFromId(),
			// getDataSource(request , "ilexnewDB" ))));
			SowAssumptionForm
					.setContext("<a href=JobDetailAction.do?addendum_id=0&ref=detailjob&Job_Id="
							+ SowAssumptionForm.getFromId()
							+ ">"
							+ Appendixdao.getAppendixname(
									getDataSource(request, "ilexnewDB"), IMdao
											.getAppendixId(
													SowAssumptionForm
															.getFromId(),
													getDataSource(request,
															"ilexnewDB")))
							+ "</a>");
			SowAssumptionForm.setAppendixId(IMdao.getAppendixId(
					SowAssumptionForm.getFromId(),
					getDataSource(request, "ilexnewDB")));
			SowAssumptionForm.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					SowAssumptionForm.getAppendixId()));
			SowAssumptionForm.setMsaId(IMdao.getMSAId(
					SowAssumptionForm.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			SowAssumptionForm.setMsaName(com.mind.dao.PM.Appendixdao
					.getMsaname(getDataSource(request, "ilexnewDB"),
							SowAssumptionForm.getMsaId()));

			SowAssumptionForm.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"),
					SowAssumptionForm.getFromId()));
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					SowAssumptionForm.getFromId(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String jobStatusList = "";
			String appendixType = "";

			if (jobType1.equals("Default"))
				jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
						+ SowAssumptionForm.getFromId() + "&Status='D'\"" + ","
						+ "0";
			if (jobType1.equals("Newjob"))
				jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
						"", "", SowAssumptionForm.getFromId(), "", loginUserId,
						getDataSource(request, "ilexnewDB"));
			if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
				jobStatusList = "\"Approved\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ SowAssumptionForm.getFromId() + "&Status=A\"" + ","
						+ "0";
			if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
				jobStatusList = "\"Draft\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ SowAssumptionForm.getFromId() + "&Status=D\"" + ","
						+ "0";
			if (jobType1.equals("Addendum"))
				jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
						"", "", SowAssumptionForm.getFromId(), "", loginUserId,
						getDataSource(request, "ilexnewDB"));

			appendixType = ViewList.getAppendixtypedesc(
					SowAssumptionForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("jobStatus", jobStatus);
			request.setAttribute("job_type", jobType1);
			request.setAttribute("appendixtype", appendixType);
			request.setAttribute("Job_Id", SowAssumptionForm.getFromId());
			request.setAttribute("Appendix_Id",
					SowAssumptionForm.getAppendixId());
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("addendum_id", "0");

			if (jobType1.equals("Newjob") || jobType1.equals("Default")) {
				request.setAttribute("chkadd", "detailjob");
				request.setAttribute("chkaddendum", "detailjob");
				request.setAttribute("chkaddendumactivity", "View");
			}
			if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
				request.setAttribute("chkadd", "inscopejob");
				request.setAttribute("chkaddendum", "inscopejob");
				request.setAttribute("chkaddendumactivity", "inscopeactivity");
			}

		}

		if (SowAssumptionForm.getFromType().equals("prm_act")) {
			SowAssumptionForm.setContextType("Appendix");
			SowAssumptionForm
					.setContext("<a href=AppendixHeader.do?function=view&viewjobtype="
							+ SowAssumptionForm.getViewJobType()
							+ "&ownerId="
							+ SowAssumptionForm.getOwnerId()
							+ "&appendixid="
							+ SowAssumptionForm.getFromId()
							+ ">"
							+ Appendixdao.getAppendixname(
									getDataSource(request, "ilexnewDB"),
									SowAssumptionForm.getFromId()) + "</a>");

			/* Start : Appendix Dashboard View Selector Code */
			if (request.getParameter("fromPage") != null)
				SowAssumptionForm.setFromPage(request.getParameter("fromPage"));

			if (SowAssumptionForm.getJobOwnerOtherCheck() != null
					&& !SowAssumptionForm.getJobOwnerOtherCheck().equals("")) {
				session.setAttribute("jobOwnerOtherCheck",
						SowAssumptionForm.getJobOwnerOtherCheck());
			}

			if (request.getParameter("opendiv") != null) {
				request.setAttribute("opendiv", request.getParameter("opendiv"));
			}
			if (request.getParameter("resetList") != null
					&& request.getParameter("resetList").equals("something")) {
				WebUtil.setDefaultAppendixDashboardAttribute(session);
				request.setAttribute("type", "AppendixViewSelector");
				request.setAttribute("appendixId",
						SowAssumptionForm.getFromId());
				request.setAttribute("msaId", SowAssumptionForm.getMsaId());

				return mapping.findForward("temppage");

			}

			if (SowAssumptionForm.getJobSelectedStatus() != null) {
				for (int i = 0; i < SowAssumptionForm.getJobSelectedStatus().length; i++) {
					jobSelectedStatus = jobSelectedStatus + "," + "'"
							+ SowAssumptionForm.getJobSelectedStatus(i) + "'";
					jobTempStatus = jobTempStatus + ","
							+ SowAssumptionForm.getJobSelectedStatus(i);
				}

				jobSelectedStatus = jobSelectedStatus.substring(1,
						jobSelectedStatus.length());
				jobSelectedStatus = "(" + jobSelectedStatus + ")";
			}

			if (SowAssumptionForm.getJobSelectedOwners() != null) {
				for (int j = 0; j < SowAssumptionForm.getJobSelectedOwners().length; j++) {
					jobSelectedOwner = jobSelectedOwner + ","
							+ SowAssumptionForm.getJobSelectedOwners(j);
					jobTempOwner = jobTempOwner + ","
							+ SowAssumptionForm.getJobSelectedOwners(j);
				}
				jobSelectedOwner = jobSelectedOwner.substring(1,
						jobSelectedOwner.length());
				if (jobSelectedOwner.equals("0")) {
					jobSelectedOwner = "%";
				}
				jobSelectedOwner = "(" + jobSelectedOwner + ")";
			}

			if (request.getParameter("showList") != null
					&& request.getParameter("showList").equals("something")) {

				session.setAttribute("jobSelectedOwners", jobSelectedOwner);
				session.setAttribute("jobTempOwner", jobTempOwner);
				session.setAttribute("jobSelectedStatus", jobSelectedStatus);
				session.setAttribute("jobTempStatus", jobTempStatus);
				session.setAttribute("timeFrame",
						SowAssumptionForm.getJobMonthWeekCheck());
				session.setAttribute("jobOwnerOtherCheck",
						SowAssumptionForm.getJobOwnerOtherCheck());

				request.setAttribute("type", "AppendixViewSelector");
				request.setAttribute("appendixId",
						SowAssumptionForm.getFromId());
				request.setAttribute("msaId", SowAssumptionForm.getMsaId());

				return mapping.findForward("temppage");

			} else {
				if (session.getAttribute("jobSelectedOwners") != null) {
					jobSelectedOwner = session
							.getAttribute("jobSelectedOwners").toString();
					SowAssumptionForm
							.setJobSelectedOwners(session
									.getAttribute("jobTempOwner").toString()
									.split(","));
				}
				if (session.getAttribute("jobSelectedStatus") != null) {
					jobSelectedStatus = session.getAttribute(
							"jobSelectedStatus").toString();
					SowAssumptionForm.setJobSelectedStatus(session
							.getAttribute("jobTempStatus").toString()
							.split(","));
				}
				if (session.getAttribute("jobOwnerOtherCheck") != null) {
					SowAssumptionForm.setJobOwnerOtherCheck(session
							.getAttribute("jobOwnerOtherCheck").toString());
				}
				if (session.getAttribute("timeFrame") != null) {
					SowAssumptionForm.setJobMonthWeekCheck(session
							.getAttribute("timeFrame").toString());
				}
			}

			jobOwnerList = MSAdao.getOwnerList(loginUserId,
					SowAssumptionForm.getJobOwnerOtherCheck(), "prj_job_new",
					SowAssumptionForm.getFromId(),
					getDataSource(request, "ilexnewDB"));
			jobAppStatusList = MSAdao.getStatusList("prj_job_new",
					getDataSource(request, "ilexnewDB"));

			if (jobOwnerList.size() > 0)
				jobOwnerListSize = jobOwnerList.size();

			request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
			request.setAttribute("jobStatusList", jobAppStatusList);
			request.setAttribute("jobOwnerList", jobOwnerList);
			/* End : Appendix Dashboard View Selector Code */

			String contractDocoumentList = com.mind.dao.PM.Appendixdao
					.getcontractDocoumentMenu(SowAssumptionForm.getFromId(),
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("contractDocMenu", contractDocoumentList);
			String appendixcurrentstatus = Appendixdao.getCurrentstatus(
					SowAssumptionForm.getFromId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
			SowAssumptionForm.setMsaId(IMdao.getMSAId(
					SowAssumptionForm.getFromId(),
					getDataSource(request, "ilexnewDB")));
			request.setAttribute("appendixid", SowAssumptionForm.getFromId());
			request.setAttribute("msaId", SowAssumptionForm.getMsaId());
			request.setAttribute("viewjobtype",
					SowAssumptionForm.getViewJobType());
			request.setAttribute("ownerId", SowAssumptionForm.getOwnerId());
		}
		return mapping.findForward("success");
	}
}
