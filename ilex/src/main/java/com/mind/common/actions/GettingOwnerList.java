package com.mind.common.actions;

import java.util.ArrayList;
import java.util.Iterator;

import com.mind.bean.Owner;

public class GettingOwnerList {

	public static String convertArrayListToHTML( ArrayList OwnerList ,boolean meChecked,boolean allChecked){
		StringBuffer xml =new StringBuffer();
		String me=meChecked?" checked ":"";
		String all =allChecked?" checked ":"";
		int i=0;
		Iterator ownerIterator = OwnerList.iterator();
		while(ownerIterator.hasNext()){
			Owner owner = (Owner) ownerIterator.next();
			xml.append("<input type='checkbox'");
			if(owner.getOwnerName().equals("Me"))
				xml.append(me);
			if(owner.getOwnerName().equals("All"))
				xml.append(all);
			xml.append(" name='jobSelectedOwners' value='"+owner.getOwnerId()+"' onclick=\"checkingOwner('"+owner.getOwnerId()+"');\"/>" + owner.getOwnerName());
			xml.append("<br>");
			if(i==1){
				xml.append("<br>");
				xml.append("<span style='overflow-Y: auto; scrollbar-base-color: none; height: 150'>");
				xml.append("Other<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			}
			if(i>1){
				xml.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			}
			i++;
		}
		xml.append("</span>");
		return xml.toString(); 
	}

}
