package com.mind.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PM.MSAdao;

public class OwnerListAction extends com.mind.common.IlexAction{

	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String loginusername = ( String ) session.getAttribute( "username" );
		String check = "";
		String ownerType="";
		String Id="";
		ArrayList ownerList= new ArrayList();
		String Owners="";
		boolean meChecked=false;
		boolean allChecked=false;
		
		if(request.getParameter("project")!=null){
			check = request.getParameter("project");
			session.setAttribute("jobOwnerOtherCheck","something");
		}
		if(request.getParameter("ownerType")!=null){
			ownerType = request.getParameter("ownerType");
		}
		if(request.getParameter("Id")!=null){
			Id = request.getParameter("Id");
		}
		
		if(request.getParameter("ME")!=null && request.getParameter("ME").equals("true")){
			meChecked=true;
		}
		
		if(request.getParameter("All")!=null && request.getParameter("All").equals("true")){
			allChecked=true;
		}
		
		Owners  = GettingOwnerList.convertArrayListToHTML(MSAdao.getOwnerList(loginuserid, check, ownerType,Id,getDataSource(request, "ilexnewDB")),meChecked,allChecked);
		response.setContentType("text/html");
		
		response.getWriter().write(Owners);
		
		
		return null;
	}
	
}
