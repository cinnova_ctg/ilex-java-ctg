
/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* 
*/package com.mind.common.actions;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.EnvironmentSelector;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.util.MySqlConnection;
import com.mind.dao.PMT.DeleteJobdao;

/**
*  Used for deleting jobs from Project Manager 
* 
* @see 
* 
**/

public class MasterDeleteAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MasterDeleteAction.class);

	public static boolean connCheck = true;
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		Connection sqlConn  = null;
		Connection mySqlConn = null;
		DataSource ds  = null;
		
		HttpSession session = request.getSession( true );
		String loginuserid = ( String ) session.getAttribute( "userid" );
		String loginusername = ( String ) session.getAttribute( "username" );
		
		String type = request.getParameter("type") == null?"":request.getParameter("type");
		String Id   = request.getParameter("jobId") == null?"":request.getParameter("jobId");
		String destinationForward="";
		
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		if( !Authenticationdao.getPageSecurity( loginuserid , "Manage Proposal " , "Delete Job" , getDataSource( request , "ilexnewDB" ) ) ){
			return( mapping.findForward( "UnAuthenticate" ) );	
		}
		
		try{
			ds = getDataSource( request , "ilexnewDB" );
			sqlConn = ds.getConnection();
			sqlConn.setAutoCommit(false);
			
			//String url = this.getResources( request ).getMessage( "appendix.detail.changestatus.mysql.url" );
			String url = EnvironmentSelector.getBundleString("appendix.detail.changestatus.mysql.url");
			mySqlConn = MySqlConnection.getMySqlConnection(url);
			
		}catch(Exception e){
			logger.error("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Exception caught in establishing connection " + e);
			}
		}

		try{
			if(!type.equals("") && type.equals("PRMJob")){
				boolean status = deleteJobFromDatabases(sqlConn,mySqlConn,Id);
				int deleteflag = (status == true) ?0:1;
				request.setAttribute("deleteSqlFlag",""+deleteflag);
			}
			
		}catch(Exception e){
			logger.error("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)", e);

			sqlConn.commit();
			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Exception occurred while deleting job record " + e.getMessage());
			}
			request.setAttribute("deleteMySqlFlag",""+1);
		}finally{
			try{
				if( sqlConn != null ){
					sqlConn.close();
				}
				if(mySqlConn != null){
					mySqlConn.close();
				}
				
			}catch( SQLException sql ){
				logger.error("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Exception caught in closing resultset" + sql);
				}
			}
		}
		return mapping.getInputForward();
		
	}
		/**
	    * Method             Used to delete jobs from SqlServer and MySql 
	    * @param             Connection 		SqlServer conneciton object
	    * @param 			 Connection 		MySql conneciton object
	    * @param 			 String 			Job Id to be deleted
	    * @return 			 Boolean 			status whether job is successfully deleted or not
	    * @see   
	    **/
	
	private boolean deleteJobFromDatabases(Connection sqlConn,Connection mySqlConn,String jobId) throws Exception{
		int retval = -1;
		int sqlDeleteFlag = -1 ;
		
		try{       
			retval = DeleteJobdao.insertDeletedJob(sqlConn,jobId);
			sqlDeleteFlag = DeleteJobdao.sqlDeleteJob(sqlConn,jobId);
		}catch(Exception E){
			logger.error("deleteJobFromDatabases(Connection, Connection, String)", E);

			sqlConn.rollback();
			return false;
		}finally{
			if(sqlDeleteFlag == 0 && retval == 0){
				sqlConn.commit();
			}
		}
		DeleteJobdao.mySqlDeleteJob(mySqlConn,jobId);
		return true;
	}
}
