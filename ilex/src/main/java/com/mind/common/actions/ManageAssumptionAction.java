package com.mind.common.actions;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.bean.Assumption;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.formbean.AssumptionForm;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ManageAssumptionAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManageAssumptionAction.class);

	public ActionForward View(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AssumptionForm assumptionform = (AssumptionForm) form;
		String type = "";
		String Id = "";
		int assumption_size = 0;
		String temp = "";
		String name = "";
		NameId idname = null;
		NameId appendixidname = null;
		String activityname = "";
		String from = "";
		String jobid = "";
		char resourceValue;
		String resource = "";

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (request.getParameter("Type") != null) {
			assumptionform.setType(request.getParameter("Type"));

		}
		if (request.getParameter("resourceListType") != null) {
			assumptionform.setResourceListType(request
					.getParameter("resourceListType"));

		}
		if (request.getParameter("Job_Id") != null)
			assumptionform.setJobId(request.getParameter("Job_Id"));

		if (request.getParameter("Id") != null)
			assumptionform.setId(request.getParameter("Id"));

		if (request.getParameter("Activity_Id") != null)
			assumptionform.setActivity_Id(request.getParameter("Activity_Id"));

		if (request.getParameter("addendum_id") != null)
			assumptionform.setAddendum_id(request.getParameter("addendum_id"));

		if (request.getParameter("ref") != null)
			assumptionform.setRef(request.getParameter("ref"));

		if (request.getParameter("resourcelistpage") != null)
			assumptionform.setResourceListPage(request
					.getParameter("resourcelistpage"));

		if (assumptionform.getType().equals("am")
				|| assumptionform.getType().equals("Activity"))
			temp = "Activity";

		if (assumptionform.getType().equals("M"))
			temp = "Material";

		if (assumptionform.getType().equals("L"))
			temp = "Labor";

		if (assumptionform.getType().equals("F"))
			temp = "Freight";

		if (assumptionform.getType().equals("T"))
			temp = "Travel";

		if (assumptionform.getType().equals("pm_resM")
				|| assumptionform.getType().equals("pm_resL")
				|| assumptionform.getType().equals("pm_resF")
				|| assumptionform.getType().equals("pm_resT")
				|| assumptionform.getType().equals("pm_resA"))
			temp = "Resource";

		// From Project Manager
		if (assumptionform.getType().equals("PRM"))
			temp = "Manage PRM Activity";

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, temp,
				"Manage Assumption", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (assumptionform.getType().equals("pm_resM")
				|| assumptionform.getType().equals("pm_resL")
				|| assumptionform.getType().equals("pm_resF")
				|| assumptionform.getType().equals("pm_resT")
				|| assumptionform.getType().equals("pm_resA")) {
			resourceValue = assumptionform.getType().charAt(6);
			assumptionform.setResourceType(resourceValue + "");
		}
		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			jobid = request.getParameter("jobid");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));

			assumptionform.setDashboardid(request.getParameter("jobid"));
			assumptionform.setFromflag(request.getParameter("from"));
		}

		if (assumptionform.getType().equals("Activity")) {

			assumptionform
					.setActivityName(Activitydao.getActivityname(
							assumptionform.getId(),
							getDataSource(request, "ilexnewDB")));
			assumptionform.setAppendixName(Jobdao.getoplevelname(
					assumptionform.getJobId(), "Activity",
					getDataSource(request, "ilexnewDB")));
			assumptionform.setAppendixId(Activitydao.getAppendixid(
					getDataSource(request, "ilexnewDB"),
					assumptionform.getJobId()));
			idname = Activitydao.getIdName(assumptionform.getJobId(),
					"Activity", "MSA", getDataSource(request, "ilexnewDB"));
			assumptionform.setJobName(Jobdao.getJobname(
					assumptionform.getJobId(),
					getDataSource(request, "ilexnewDB")));

		}

		if (assumptionform.getType().equals("pm_resM")
				|| assumptionform.getType().equals("pm_resL")
				|| assumptionform.getType().equals("pm_resF")
				|| assumptionform.getType().equals("pm_resT")) {

			appendixidname = Activitydao.getIdName(
					assumptionform.getActivity_Id(), "Resource", "Appendix",
					getDataSource(request, "ilexnewDB"));
			assumptionform.setAppendixId(appendixidname.getId());
			assumptionform.setAppendixName(appendixidname.getName());

			idname = Activitydao.getIdName(assumptionform.getActivity_Id(),
					"Resource", "MSA", getDataSource(request, "ilexnewDB"));
			assumptionform.setMsaId(idname.getId());
			assumptionform.setMsaName(idname.getName());

			assumptionform.setJobName(Jobdao.getoplevelname(
					assumptionform.getActivity_Id(), "Resource",
					getDataSource(request, "ilexnewDB")));
			assumptionform.setJobId(Activitydao.getJobid(
					getDataSource(request, "ilexnewDB"),
					assumptionform.getActivity_Id()));
			activityname = Activitydao.getActivityname(
					assumptionform.getActivity_Id(),
					getDataSource(request, "ilexnewDB"));
			assumptionform.setActivityName(activityname);

		}

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"), assumptionform.getId(),
				assumptionform.getType());

		assumptionform.setDefaultAssumptionList(ActivityLibrarydao
				.getDefaultAssumptionList(getDataSource(request, "ilexnewDB")));

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		if (!(assumptionform.getType().equals("am") || assumptionform.getType()
				.equals("Activity"))) {
			name = ActivityLibrarydao.Getnameforsowassumption(
					assumptionform.getId(), assumptionform.getType(),
					getDataSource(request, "ilexnewDB"));
		}
		assumptionform.setName(name);

		// request.setAttribute("defaultAssumptionList",
		// assumptionform.getDefaultAssumptionList());
		request.setAttribute("assumptionlist", assumptionlist);
		request.setAttribute("Size", "" + assumption_size);
		request.setAttribute("menuid", assumptionform.getId());

		// Undefined change which could not be traced to SPR.

		// if(assumptionform.getType().equals("M") ||
		// assumptionform.getType().equals("L") |
		// assumptionform.getType().equals("T") ||
		// assumptionform.getType().equals("F")) {
		// ActivityLibrary activitylibrary = ActivityLibrarydao.getActivity(
		// assumptionform.getActivity_Id() , getDataSource( request ,
		// "ilexnewDB" ) );
		//
		// assumptionform.setMsaId(activitylibrary.getMsa_Id());
		// assumptionform.setMsaName(activitylibrary.getMsaname());
		// assumptionform.setActivityName(activitylibrary.getName());
		// request.setAttribute("ActivityManager", "AM");
		// }

		if (assumptionform.getType().equals("Activity")) {

			String jobtype = "";
			String activityStatus = "";
			String activityType = "";
			String activityStatusList = "";
			String MSA_Id = "";
			String MSA_Name = "";
			int checkNetmedXappendix = 0;
			String[] activityStatusAndType = Activitydao
					.getActivityTypeStatus(assumptionform.getId(),
							getDataSource(request, "ilexnewDB"));
			activityStatus = activityStatusAndType[0];
			activityType = activityStatusAndType[1];

			if (request.getParameter("from") != null) {
				from = request.getParameter("from");
				jobid = request.getParameter("jobid");
				request.setAttribute("from", request.getParameter("from"));
				request.setAttribute("jobid", request.getParameter("jobid"));
			}

			jobtype = Jobdao.getJobtype(assumptionform.getJobId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("jobtype", jobtype);

			if (assumptionform.getRef().equalsIgnoreCase("view")
					|| assumptionform.getRef().equalsIgnoreCase(
							"detailactivity")) {
				request.setAttribute("chkaddendum", "detailactivity");
				request.setAttribute("chkaddendumresource", "View");

				if (request.getParameter("from") != null) {
					if (jobtype.equals("Addendum") || jobtype.equals("Default")) {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), from, jobid,
								assumptionform.getId(), "detailactivity",
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					} else {
						activityStatus = JobDashboarddao
								.getActivityprojectstatus(
										assumptionform.getId(),
										getDataSource(request, "ilexnewDB"));
						activityStatusList = Menu.getStatus("prj_act",
								activityStatus.charAt(0), from, jobid,
								assumptionform.getId(), "detailactivity",
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					}
				} else {
					activityStatusList = Menu.getStatus("pm_act",
							activityStatus.charAt(0), "", "",
							assumptionform.getId(), "detailactivity",
							loginuserid, getDataSource(request, "ilexnewDB"));

				}

			}

			if (assumptionform.getRef().equalsIgnoreCase("inscopeactivity")
					|| assumptionform.getRef().equalsIgnoreCase(
							"inscopedetailactivity")) {
				request.setAttribute("chkaddendum", "inscopedetailactivity");
				request.setAttribute("chkaddendumresource", "Viewinscope");

				if (request.getParameter("from") != null) {

					if (jobtype.equals("Addendum") || jobtype.equals("Default")) {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), from, jobid,
								assumptionform.getId(),
								"inscopedetailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					} else {
						activityStatus = JobDashboarddao
								.getActivityprojectstatus(
										assumptionform.getId(),
										getDataSource(request, "ilexnewDB"));
						activityStatusList = Menu.getStatus("prj_act",
								activityStatus.charAt(0), from, jobid,
								assumptionform.getId(),
								"inscopedetailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					}
				} else {
					activityStatusList = Menu.getStatus("pm_act",
							activityStatus.charAt(0), "", "",
							assumptionform.getId(), "inscopedetailactivity",
							loginuserid, getDataSource(request, "ilexnewDB"));

				}

			}

			MSA_Id = Activitydao.getmsaid(getDataSource(request, "ilexnewDB"),
					assumptionform.getJobId());
			MSA_Name = Activitydao.getMsaname(
					getDataSource(request, "ilexnewDB"), MSA_Id);
			assumptionform.setMsaName(MSA_Name);
			assumptionform.setMsaId(MSA_Id);
			request.setAttribute("MSA_Name", MSA_Name);
			request.setAttribute("MSA_Id", MSA_Id);

			checkNetmedXappendix = Activitydao.checkAppendixname(
					assumptionform.getAppendixId(), assumptionform.getJobId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("checknetmedx", checkNetmedXappendix + "");
			request.setAttribute("activityStatusList", activityStatusList);
			request.setAttribute("Activity_Id", assumptionform.getId());
			request.setAttribute("Job_Id", assumptionform.getJobId());
			request.setAttribute("jobtype", jobtype);
			request.setAttribute("from", from);
			request.setAttribute("jobid", jobid);
			request.setAttribute("activityStatus", activityStatus);
			request.setAttribute("activityType", activityType);
			request.setAttribute("activityTypeForJob", assumptionform.getRef());

		}
		if (assumptionform.getJobId() != null
				&& !assumptionform.getJobId().trim().equalsIgnoreCase("")) {
			if (logger.isDebugEnabled()) {
				logger.debug("View(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in menu");
			}
			Map<String, Object> map;

			map = DatabaseUtilityDao.setMenu(assumptionform.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		request.setAttribute("checkingRef", assumptionform.getRef());
		return mapping.findForward("assumptiontabularpage");

	}

	public ActionForward Submit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		AssumptionForm assumptionform = (AssumptionForm) form;
		String type = "";
		String Id = "";
		int assumption_size = 0;
		Assumption assumption = null;
		String addendum_id = "";
		String tempref = "";
		String name = "";
		Id = assumptionform.getId();
		type = assumptionform.getType();
		addendum_id = assumptionform.getAddendum_id();
		tempref = assumptionform.getRef();
		String activity_id = assumptionform.getActivity_Id();

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		int checklength = 0;

		if (assumptionform.getCheck() != null) {
			checklength = assumptionform.getCheck().length;
		}

		int[] index = new int[checklength];
		int k = 0;

		if (checklength > 0) {
			int length = assumptionform.getAssumption_Id().length;
			for (int i = 0; i < checklength; i++) {
				for (int j = 0; j < length; j++) {
					if (assumptionform.getCheck(i).equals(
							assumptionform.getAssumption_Id(j))) {
						index[k] = j;
						k++;
					}
				}
			}
		}

		if (checklength > 0) {
			for (int i = 0; i < index.length; i++) {
				assumption = new Assumption();

				assumption.setAssumption_Id(assumptionform
						.getAssumption_Id(index[i]));
				assumption
						.setAssumption(assumptionform.getAssumption(index[i]));
				assumption.setType(type);

				int updateflag = ActivityLibrarydao.updateAssumption(
						assumption, getDataSource(request, "ilexnewDB"));
				request.setAttribute("updateflag", updateflag + "");
				request.setAttribute("assumptionupdateflag", updateflag + "");

			}
		}

		if (assumptionform.getNewassumption_Id() != null) {
			try {
				assumption = new Assumption();

				assumption.setId(assumptionform.getId());
				assumption.setAssumption(assumptionform.getNewassumption());
				assumption.setType(assumptionform.getType());

				int addflag = ActivityLibrarydao.addassumption(assumption,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("addflag", addflag + "");
				request.setAttribute("assumptionaddflag", addflag + "");
			} catch (Exception e) {
				logger.error(
						"Submit(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("Submit(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Error Occured"
							+ e);
				}
			}

		}

		// if( assumptionform.getFromflag().equals( "jobdashboard" ) )
		// {
		// request.setAttribute( "jobid" , assumptionform.getDashboardid() );
		// return mapping.findForward( "jobdashboard" );
		// }

		assumptionform.reset(mapping, request);

		assumptionform.setId(Id);
		assumptionform.setType(type);
		assumptionform.setAddendum_id(addendum_id);
		assumptionform.setRef(tempref);

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"), assumptionform.getId(),
				assumptionform.getType());

		assumptionform.setDefaultAssumptionList(ActivityLibrarydao
				.getDefaultAssumptionList(getDataSource(request, "ilexnewDB")));

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		if (!(assumptionform.getType().equals("am") || assumptionform.getType()
				.equals("Activity"))) {
			name = ActivityLibrarydao.Getnameforsowassumption(
					assumptionform.getId(), assumptionform.getType(),
					getDataSource(request, "ilexnewDB"));
		}
		assumptionform.setName(name);

		request.setAttribute("assumptionlist", assumptionlist);
		request.setAttribute("Size", "" + assumption_size);
		request.setAttribute("menuid", assumptionform.getId());

		if (assumptionform.getJobId() != null
				&& !assumptionform.getJobId().trim().equalsIgnoreCase("")) {
			if (logger.isDebugEnabled()) {
				logger.debug("View(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in menu");
			}
			Map<String, Object> map;

			map = DatabaseUtilityDao.setMenu(assumptionform.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}

		// Undefined change which could not be traced to SPR.

		// if (assumptionform.getType().equals("M")
		// || assumptionform.getType().equals("L")
		// | assumptionform.getType().equals("T")
		// || assumptionform.getType().equals("F")) {
		// ActivityLibrary activitylibrary = ActivityLibrarydao.getActivity(
		// assumptionform.getActivity_Id(),
		// getDataSource(request, "ilexnewDB"));
		//
		// assumptionform.setMsaId(activitylibrary.getMsa_Id());
		// assumptionform.setMsaName(activitylibrary.getMsaname());
		// assumptionform.setActivityName(activitylibrary.getName());
		// request.setAttribute("ActivityManager", "AM");
		// }
		if (assumptionform.getResourceListPage() != null
				&& assumptionform.getResourceListPage().equals(
						"resourcelistpage")) {

			request.setAttribute("from", assumptionform.getFromflag());
			request.setAttribute("type", "sowAssumptiontoResource");
			request.setAttribute("jobid", assumptionform.getDashboardid());
			request.setAttribute("Activity_Id", assumptionform.getActivity_Id());
			request.setAttribute("resourceType",
					assumptionform.getResourceType());
			request.setAttribute("resourceListType",
					assumptionform.getResourceListType());
			request.setAttribute("ref", assumptionform.getRef());
			request.setAttribute("addendum_id", "0");

			return mapping.findForward("assumptiontabularpage");
			// return mapping.findForward("tempuploadpage");

		}

		return mapping.findForward("assumptiontabularpage");
	}

	public ActionForward Delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AssumptionForm assumptionform = (AssumptionForm) form;
		String type = "";
		String Id = "";
		int assumption_size = 0;
		Assumption assumption = null;
		String addendum_id = "";
		String tempref = "";
		String name = "";
		Id = assumptionform.getId();
		type = assumptionform.getType();
		addendum_id = assumptionform.getAddendum_id();
		tempref = assumptionform.getRef();

		int checklength = 0;

		if (assumptionform.getCheck() != null) {
			checklength = assumptionform.getCheck().length;
		}

		int[] index = new int[checklength];
		int k = 0;

		if (checklength > 0) {
			int length = assumptionform.getAssumption_Id().length;
			for (int i = 0; i < checklength; i++) {
				for (int j = 0; j < length; j++) {
					if (assumptionform.getCheck(i).equals(
							assumptionform.getAssumption_Id(j))) {
						index[k] = j;
						k++;
					}
				}
			}
		}

		if (checklength > 0) {
			for (int i = 0; i < index.length; i++) {
				assumption = new Assumption();

				assumption.setAssumption_Id(assumptionform
						.getAssumption_Id(index[i]));
				assumption
						.setAssumption(assumptionform.getAssumption(index[i]));
				assumption.setType(type);

				int deleteflag = ActivityLibrarydao.deleteAssumption(
						assumption, getDataSource(request, "ilexnewDB"));
				request.setAttribute("deleteflag", deleteflag + "");
			}
		}

		if (assumptionform.getFromflag().equals("jobdashboard")) {
			request.setAttribute("jobid", assumptionform.getDashboardid());
			return mapping.findForward("jobdashboard");
		}

		assumptionform.reset(mapping, request);

		assumptionform.setId(Id);
		assumptionform.setType(type);
		assumptionform.setAddendum_id(addendum_id);
		assumptionform.setRef(tempref);

		ArrayList assumptionlist = ActivityLibrarydao.getAssumptionlist(
				getDataSource(request, "ilexnewDB"), assumptionform.getId(),
				assumptionform.getType());

		assumptionform.setDefaultAssumptionList(ActivityLibrarydao
				.getDefaultAssumptionList(getDataSource(request, "ilexnewDB")));

		if (assumptionlist.size() > 0) {
			assumption_size = assumptionlist.size();
		}

		if (!(assumptionform.getType().equals("am") || assumptionform.getType()
				.equals("Activity"))) {
			name = ActivityLibrarydao.Getnameforsowassumption(
					assumptionform.getId(), assumptionform.getType(),
					getDataSource(request, "ilexnewDB"));
		}
		assumptionform.setName(name);

		// Undefined change which could not be traced to SPR.

		// if (assumptionform.getType().equals("M")
		// || assumptionform.getType().equals("L")
		// | assumptionform.getType().equals("T")
		// || assumptionform.getType().equals("F")) {
		// ActivityLibrary activitylibrary = ActivityLibrarydao.getActivity(
		// assumptionform.getActivity_Id(),
		// getDataSource(request, "ilexnewDB"));
		//
		// assumptionform.setMsaId(activitylibrary.getMsa_Id());
		// assumptionform.setMsaName(activitylibrary.getMsaname());
		// assumptionform.setActivityName(activitylibrary.getName());
		// request.setAttribute("ActivityManager", "AM");
		// }

		request.setAttribute("assumptionlist", assumptionlist);
		request.setAttribute("Size", "" + assumption_size);
		request.setAttribute("menuid", assumptionform.getId());

		if (assumptionform.getType().equals("pm_resM")
				|| assumptionform.getType().equals("pm_resL")
				|| assumptionform.getType().equals("pm_resF")
				|| assumptionform.getType().equals("pm_resT")) {
			request.setAttribute("from", assumptionform.getFromflag());
			request.setAttribute("jobid", assumptionform.getDashboardid());
			request.setAttribute("Activity_Id", assumptionform.getActivity_Id());
			request.setAttribute("Material_Id", assumptionform.getId());
			request.setAttribute("Labor_Id", assumptionform.getId());
			request.setAttribute("Freight_Id", assumptionform.getId());
			request.setAttribute("Travel_Id", assumptionform.getId());

			request.setAttribute("commonResourceStatus",
					assumptionform.getResourceStatus());
			request.setAttribute("materialStatusList",
					assumptionform.getResourceStatusList());
			request.setAttribute("laborStatusList",
					assumptionform.getResourceStatusList());
			request.setAttribute("freightStatusList",
					assumptionform.getResourceStatusList());
			request.setAttribute("travelStatusList",
					assumptionform.getResourceStatusList());
			request.setAttribute("checkingRef", assumptionform.getRef());
			request.setAttribute("addendum_id", "0");
			request.setAttribute("chkaddendum", assumptionform.getChkaddendum());
			request.setAttribute("chkdetailactivity",
					assumptionform.getChkdetailactivity());
		}
		return mapping.findForward("assumptiontabularpage");

	}

}
