/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class would be used for loading sorting parameters and forwarding the sortquery string 
 * containing the order by clause to the page from where the sort button is being clicked
 * 
 */

package com.mind.common.actions;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.LabelValue;
import com.mind.common.bean.Sortcodelist;
import com.mind.common.dao.Customersortparamdao;
import com.mind.common.formbean.SortForm;

public class SortAction extends com.mind.common.IlexAction {

	/**
	 * This method is used to load sorting parameters, preparing the sort query
	 * string and forwarding the request to page from where it is called
	 * 
	 * @param mapping
	 *            object of ActionMapping
	 * @param form
	 *            object of ActionForm
	 * @param request
	 *            object of HttpServletRequest
	 * @param response
	 *            object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SortForm sortform = (SortForm) form;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String rdofilter = null;

		if (request.getParameter("partnersearchType") != null) {
			sortform.setPartnersearchType(request
					.getParameter("partnersearchType"));
		} else
			sortform.setPartnersearchType("");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		if (request.getParameter("rdofilter") != null) {
			sortform.setRdofilter(request.getParameter("rdofilter"));
		}
		if (request.getParameter("from") != null) {
			sortform.setFrom(request.getParameter("from"));
		}
		if (request.getParameter("ref1") != null) {
			sortform.setRef1(request.getParameter("ref1"));
		}
		if (request.getParameter("page") != null) {
			sortform.setPage(request.getParameter("page"));
		}
		if (request.getParameter("viewjobtype") != null) {
			sortform.setViewjobtype(request.getParameter("viewjobtype"));
		}
		if (request.getParameter("ownerId") != null) {
			sortform.setOwnerId(request.getParameter("ownerId"));
		}
		// Start :Added For Edit Site Master

		if (request.getParameter("msaid") != null) {
			sortform.setId(request.getParameter("msaid"));
		}

		if (request.getParameter("stateid") != null) {
			sortform.setId1(request.getParameter("stateid"));
			sortform.setStateid(request.getParameter("stateid"));
		}
		if (request.getParameter("sitename") != null) {
			sortform.setSitename(request.getParameter("sitename"));
		}
		if (request.getParameter("sitenumber") != null) {
			sortform.setSitenumber(request.getParameter("sitenumber"));
		}

		if (request.getParameter("sitecity") != null) {
			sortform.setSitecity(request.getParameter("sitecity"));
		}
		if (request.getParameter("pageType") != null)
			sortform.setPageType(request.getParameter("pageType"));

		if (request.getParameter("site_msaid") != null)
			sortform.setSite_msaid(request.getParameter("site_msaid"));

		if (request.getParameter("siteSearchName") != null)
			sortform.setSiteSearchName(request.getParameter("siteSearchName"));

		if (request.getParameter("siteSearchNumber") != null)
			sortform.setSiteSearchNumber(request
					.getParameter("siteSearchNumber"));

		if (request.getParameter("siteSearchCity") != null)
			sortform.setSiteSearchCity(request.getParameter("siteSearchCity"));

		if (request.getParameter("siteSearchState") != null)
			sortform.setSiteSearchState(request.getParameter("siteSearchState"));

		if (request.getParameter("siteSearchZip") != null)
			sortform.setSiteSearchZip(request.getParameter("siteSearchZip"));

		if (request.getParameter("siteSearchCountry") != null)
			sortform.setSiteSearchCountry(request
					.getParameter("siteSearchCountry"));

		if (request.getParameter("siteSearchGroup") != null)
			sortform.setSiteSearchGroup(request.getParameter("siteSearchGroup"));

		if (request.getParameter("siteSearchEndCustomer") != null)
			sortform.setSiteSearchEndCustomer(request
					.getParameter("siteSearchEndCustomer"));

		if (request.getParameter("siteSearchStatus") != null)
			sortform.setSiteSearchStatus(request
					.getParameter("siteSearchStatus"));

		if (request.getParameter("lines_per_page") != null)
			sortform.setLines_per_page(request.getParameter("lines_per_page"));

		if (request.getParameter("org_lines_per_page") != null)
			sortform.setOrg_lines_per_page(request
					.getParameter("org_lines_per_page"));

		if (request.getParameter("siteNameForColumn") != null)
			sortform.setSiteNameForColumn(request
					.getParameter("siteNameForColumn"));

		if (request.getParameter("siteNumberForColumn") != null)
			sortform.setSiteNumberForColumn(request
					.getParameter("siteNumberForColumn"));

		if (request.getParameter("siteAddressForColumn") != null)
			sortform.setSiteAddressForColumn(request
					.getParameter("siteAddressForColumn"));

		if (request.getParameter("siteCityForColumn") != null)
			sortform.setSiteCityForColumn(request
					.getParameter("siteCityForColumn"));

		if (request.getParameter("siteStateForColumn") != null)
			sortform.setSiteStateForColumn(request
					.getParameter("siteStateForColumn"));

		if (request.getParameter("siteZipForColumn") != null)
			sortform.setSiteZipForColumn(request
					.getParameter("siteZipForColumn"));

		if (request.getParameter("siteCountryForColumn") != null)
			sortform.setSiteCountryForColumn(request
					.getParameter("siteCountryForColumn"));
		if (request.getParameter("flag") != null) {
			sortform.setFlag(request.getParameter("flag"));
		}
		if (request.getParameter("invoice_Flag") != null) {
			sortform.setInvoice_Flag(request.getParameter("invoice_Flag"));
		}

		if (request.getParameter("partner_name") != null) {
			sortform.setPartner_name(request.getParameter("partner_name"));
		} else
			sortform.setPartner_name("");

		if (request.getParameter("powo_number") != null) {
			sortform.setPowo_number(request.getParameter("powo_number"));
		} else
			sortform.setPowo_number("");

		if (request.getParameter("from_date") != null) {
			sortform.setFrom_date(request.getParameter("from_date"));
		} else
			sortform.setFrom_date("");

		if (request.getParameter("to_date") != null) {
			sortform.setTo_date(request.getParameter("to_date"));
		} else
			sortform.setTo_date("");

		if (request.getParameter("invoiceno") != null) {
			sortform.setInvoiceno(request.getParameter("invoiceno"));
		} else
			sortform.setInvoiceno("");
		// End : For Site Master

		// Parameter for partnerSeach start
		if (request.getParameter("name") != null) {
			sortform.setName(request.getParameter("name"));
		} else
			sortform.setName("");

		if (request.getParameter("keyword") != null) {
			sortform.setKeyword(request.getParameter("keyword"));
		} else
			sortform.setKeyword("");

		if (request.getParameter("mvsaVers") != null) {
			sortform.setMvsaVers(request.getParameter("mvsaVers"));
		} else
			sortform.setMvsaVers("");

		if (request.getParameter("zip") != null) {
			sortform.setZip(request.getParameter("zip"));
		} else
			sortform.setZip("");

		if (request.getParameter("radius") != null) {
			sortform.setRadius(request.getParameter("radius"));
		} else
			sortform.setRadius("");

		if (request.getParameter("mainoffice") != null) {
			sortform.setMainoffice(request.getParameter("mainoffice"));
		} else
			sortform.setMainoffice("");

		if (request.getParameter("fieldOffice") != null) {
			sortform.setFieldOffice(request.getParameter("fieldOffice"));
		} else
			sortform.setFieldOffice("");

		if (request.getParameter("techhi") != null) {
			sortform.setTechhi(request.getParameter("techhi"));
		} else
			sortform.setTechhi("");

		if (request.getParameter("rating") != null) {
			sortform.setRating(request.getParameter("rating"));
		} else
			sortform.setRating("");

		if (request.getParameter("technician") != null) {
			sortform.setTechinician(request.getParameter("technician"));
		} else
			sortform.setTechinician("");

		if (request.getParameter("certification") != null) {
			sortform.setCertification(request.getParameter("certification"));
		} else
			sortform.setCertification("");

		if (request.getParameter("corecomp") != null) {
			sortform.setCorecomp(request.getParameter("corecomp"));
		} else
			sortform.setCorecomp("");

		if (request.getParameter("chekBox1") != null) {
			sortform.setChekBox1(request.getParameter("chekBox1"));
		} else
			sortform.setChekBox1("");

		if (request.getParameter("chekBox2") != null) {
			sortform.setChekBox2(request.getParameter("chekBox2"));
		} else
			sortform.setChekBox2("");
		if (request.getParameter("jobId") != null) {
			sortform.setJobId(request.getParameter("jobId"));
		} else
			sortform.setJobId("");

		// partner search end.

		// Start: For PVS Search
		if (request.getParameter("Type") != null
				&& (request.getParameter("Type").equalsIgnoreCase("PVSSearch") || request
						.getParameter("Type").equalsIgnoreCase("PVSSearchDate"))) {
			sortform.setOrgTopName(request.getParameter("orgTopName") + "");
			sortform.setAction(request.getParameter("action") + "");
			sortform.setStartdate(request.getParameter("startdate") + "");
			sortform.setEnddate(request.getParameter("enddate") + "");
			sortform.setDivision(request.getParameter("division") + "");
			sortform.setPartnerType(request.getParameter("partnerType"));
		}
		// End: For PVS Search
		if (sortform.getSort() != null)// After clicking on sort:Start
		{
			int i = 0;
			String querystring = "";

			if (sortform.getType().equals("partnerSearch")) {
				querystring = querystring + ",";
			} else {
				querystring = querystring + "Order by ";
			}
			for (i = 0; i < sortform.getSelectedparam().length; i++) {
				querystring = querystring + sortform.getSelectedparam(i);
				querystring = querystring + ",";
			}
			querystring = querystring.substring(0, querystring.length() - 1);
			querystring = querystring + " " + sortform.getSortcriteria();

			request.setAttribute("querystring", querystring);

			if (sortform.getType().equals("MSA")) {
				if (session.getAttribute("msa_sort") != null)
					session.setAttribute("msa_sort", querystring);
				else
					session.setAttribute("msa_sort", querystring);

				return (mapping.findForward("msatabularpage"));
			} else if (sortform.getType().equals("partnerSearch")) {
				session.setAttribute("partnerQuery", querystring);
				request.setAttribute("searchNmae", sortform.getName());
				request.setAttribute("keyword", sortform.getKeyword());
				request.setAttribute("partnerType",
						sortform.getPartnersearchType());

				request.setAttribute("mcsaVers", sortform.getMvsaVers());
				request.setAttribute("zipcode", sortform.getZip());
				request.setAttribute("radius", sortform.getRadius());
				request.setAttribute("mainOffice", sortform.getMainoffice());
				request.setAttribute("fieldOffice", sortform.getFieldOffice());
				request.setAttribute("Techii", sortform.getTechhi());
				request.setAttribute("rating", sortform.getRating());
				request.setAttribute("techinicians", sortform.getTechinician());
				request.setAttribute("certification",
						sortform.getCertification());
				request.setAttribute("corecomp", sortform.getCorecomp());
				request.setAttribute("checkbox1", sortform.getChekBox1());
				request.setAttribute("checkbox2", sortform.getChekBox2());
				request.setAttribute("ref", request.getParameter("ref"));
				request.setAttribute("jobId", sortform.getJobId());
				request.setAttribute("type", sortform.getType());
				return (mapping.findForward("tempsortpage"));

			}

			else if (sortform.getType().equals("Appendix")) {
				if (session.getAttribute("appendix_sort") != null)
					session.setAttribute("appendix_sort", querystring);
				else
					session.setAttribute("appendix_sort", querystring);

				request.setAttribute("MSA_Id", sortform.getId());
				return (mapping.findForward("appendixtabularpage"));
			}

			else if (sortform.getType().equals("Job")) {
				if (session.getAttribute("job_sort") != null)
					session.setAttribute("job_sort", querystring);
				else
					session.setAttribute("job_sort", querystring);

				request.setAttribute("ref", sortform.getRef());

				request.setAttribute("id", sortform.getId());
				request.setAttribute("type", sortform.getType());
				request.setAttribute("addendum_id", sortform.getAddendum_id());
				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("Activity")) {
				if (session.getAttribute("activity_sort") != null)
					session.setAttribute("activity_sort", querystring);
				else
					session.setAttribute("activity_sort", querystring);

				request.setAttribute("ref", sortform.getRef());
				request.setAttribute("type", sortform.getType());
				request.setAttribute("id", sortform.getId());
				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().contains("pm_res")) {
				if (sortform.getType().equals("pm_resM")) {
					session.setAttribute("pmresM_sort", querystring);
				}
				if (sortform.getType().equals("pm_resL")) {
					session.setAttribute("pmresL_sort", querystring);
				}
				if (sortform.getType().equals("pm_resF")) {
					session.setAttribute("pmresF_sort", querystring);
				}
				if (sortform.getType().equals("pm_resT")) {
					session.setAttribute("pmresT_sort", querystring);
				}

				request.setAttribute("ref", sortform.getRef());
				request.setAttribute("type", sortform.getType());
				request.setAttribute("id", sortform.getId());
				request.setAttribute("addendum_id", sortform.getAddendum_id());

				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("am")) {
				session.setAttribute("am_sort", querystring);

				request.setAttribute("MSA_Id", sortform.getId());
				return (mapping.findForward("manageactivitypage"));
			}

			else if (sortform.getType().equals("amc")) {
				session.setAttribute("am_sort", querystring);

				request.setAttribute("MSA_Id", sortform.getId());
				request.setAttribute("category_Id", sortform.getId1());
				return (mapping.findForward("manageactivitypage"));
			}

			else if (sortform.getType().equals("M")
					|| sortform.getType().equals("L")
					|| sortform.getType().equals("F")
					|| sortform.getType().equals("T")) {
				if (sortform.getType().equals("M")) {
					session.setAttribute("amresM_sort", querystring);
				}
				if (sortform.getType().equals("L")) {
					session.setAttribute("amresL_sort", querystring);
				}
				if (sortform.getType().equals("F")) {
					session.setAttribute("amresF_sort", querystring);
				}
				if (sortform.getType().equals("T")) {
					session.setAttribute("amresT_sort", querystring);
				}

				request.setAttribute("Type", sortform.getType());
				request.setAttribute("Activitylibrary_Id", sortform.getId());
				return (mapping.findForward("resourcetemppage"));
			}

			else if (sortform.getType().equals("prj_job")) {
				// for filter sorting

				sortform.setCurrentstatus(sortform.getCurrentstatus());
				request.setAttribute("currentstatus",
						sortform.getCurrentstatus());
				request.setAttribute("type", sortform.getType());
				request.setAttribute("appendixid", sortform.getId());
				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("Inv_MSA")
					|| sortform.getType().equals("All")
					|| sortform.getType().equals("Inv_Appendix")) {

				session.setAttribute("inv_sort", querystring);

				// for filter sorting
				if (sortform.getType().equals("Inv_MSA"))
					request.setAttribute("type", "MSA");

				if (sortform.getType().equals("Inv_Appendix"))
					request.setAttribute("type", "Appendix");

				if (sortform.getType().equals("All"))
					request.setAttribute("type", "All");

				request.setAttribute("appendixid", sortform.getId());
				request.setAttribute("rdofilter", sortform.getRdofilter());

				request.setAttribute("invoice_Flag", sortform.getInvoice_Flag());
				request.setAttribute("partner_name", sortform.getPartner_name());
				request.setAttribute("powo_number", sortform.getPowo_number());
				request.setAttribute("from_date", sortform.getFrom_date());
				request.setAttribute("to_date", sortform.getTo_date());
				request.setAttribute("invoiceno", sortform.getInvoiceno());

				return (mapping.findForward("tempsortpage"));
			} else if (sortform.getType().equals("siteManage")) {
				if (sortform.getType().equals("siteManage"))
					request.setAttribute("pageType", sortform.getPageType());
				request.setAttribute("siteSearchName",
						sortform.getSiteSearchName());
				request.setAttribute("siteSearchNumber",
						sortform.getSiteSearchNumber());
				request.setAttribute("siteSearchCity",
						sortform.getSiteSearchCity());
				request.setAttribute("siteSearchState",
						sortform.getSiteSearchState());
				request.setAttribute("siteSearchZip",
						sortform.getSiteSearchZip());
				request.setAttribute("siteSearchCountry",
						sortform.getSiteSearchCountry());
				request.setAttribute("siteSearchGroup",
						sortform.getSiteSearchGroup());
				request.setAttribute("siteSearchEndCustomer",
						sortform.getSiteSearchEndCustomer());
				request.setAttribute("siteSearchStatus",
						sortform.getSiteSearchStatus());
				request.setAttribute("lines_per_page",
						sortform.getLines_per_page());
				request.setAttribute("org_lines_per_page",
						sortform.getOrg_lines_per_page());
				request.setAttribute("siteNameForColumn",
						sortform.getSiteNameForColumn());
				request.setAttribute("siteNumberForColumn",
						sortform.getSiteNumberForColumn());
				request.setAttribute("siteAddressForColumn",
						sortform.getSiteAddressForColumn());
				request.setAttribute("siteCityForColumn",
						sortform.getSiteCityForColumn());
				request.setAttribute("siteStateForColumn",
						sortform.getSiteStateForColumn());
				request.setAttribute("siteZipForColumn",
						sortform.getSiteZipForColumn());
				request.setAttribute("siteCountryForColumn",
						sortform.getSiteCountryForColumn());

				request.setAttribute("site_msaid", sortform.getSite_msaid());
				request.setAttribute("type", sortform.getType());
				session.setAttribute("site_sort", querystring);
				request.setAttribute("appendixid", sortform.getId());

				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("prj_netmedx")) {
				// for filter sorting
				sortform.setCurrentstatus(sortform.getCurrentstatus());
				sortform.setNettype(sortform.getNettype());
				sortform.setOwnerId(sortform.getOwnerId());

				request.setAttribute("currentstatus",
						sortform.getCurrentstatus());
				request.setAttribute("nettype", sortform.getNettype());
				request.setAttribute("ownerId", sortform.getOwnerId());
				request.setAttribute("type", sortform.getType());
				request.setAttribute("appendixid", sortform.getId());
				request.setAttribute("viewjobtype", sortform.getViewjobtype());
				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("prj_jobnew")) {
				request.setAttribute("type", sortform.getType());
				request.setAttribute("appendixid", sortform.getId());
				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("prj_activity")) {
				request.setAttribute("type", sortform.getType());
				request.setAttribute("jobid", sortform.getId());
				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("prj_materialresource")
					|| sortform.getType().equals("prj_laborresource")
					|| sortform.getType().equals("prj_freightresource")
					|| sortform.getType().equals("prj_travelresource")) {
				request.setAttribute("type", sortform.getType());
				request.setAttribute("activityid", sortform.getId());
				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("invoice_job")) {
				request.setAttribute("type", sortform.getType());
				request.setAttribute("appendixid", sortform.getId());
				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("prj_powo")) {
				session.setAttribute("powo_sort", querystring);
				request.setAttribute("type", sortform.getType());
				request.setAttribute("id", sortform.getId());

				return (mapping.findForward("tempsortpage"));
			}

			else if (sortform.getType().equals("jobdashboard")) {
				request.setAttribute("jobid", sortform.getId());

				// Start :Added By Amit to Hold Sort For JobDashBoard

				if (session != null) {
					if (session.getAttribute("HoldingSortJobDB") != null) {
						if (!session.getAttribute("HoldingSortJobDB").equals(
								querystring)) {
							session.setAttribute("HoldingSortJobDB",
									querystring);
						}
					} else {
						session.setAttribute("HoldingSortJobDB", querystring);
					}
				}
				// End : Added by Amit

				return (mapping.findForward("jobdashboard"));
			}

			else if (sortform.getType().equals("All")
					|| sortform.getType().equals("MSA")
					|| sortform.getType().equals("Appendix")) {

				request.setAttribute("type", sortform.getType());
				request.setAttribute("id", sortform.getId());

				request.setAttribute("rdofilter", sortform.getRdofilter());

				return (mapping.findForward("tempsortpage"));
			}

			// Start : For Masters
			else if (sortform.getType().equals("Masters")) {
				session.setAttribute("sitemaster_sort", querystring);

				request.setAttribute("type", sortform.getType());

				request.setAttribute("id", sortform.getId());// MSA Id
				request.setAttribute("stateid", sortform.getId1());
				request.setAttribute("sitename", sortform.getSitename());
				request.setAttribute("sitenumber", sortform.getSitenumber());
				request.setAttribute("sitecity", sortform.getSitecity());
				request.setAttribute("flag", sortform.getFlag());

				return (mapping.findForward("tempsortpage"));
			}
			// End :

			else if (sortform.getType().equals("siteHistory")) {
				session.setAttribute("sitehist_sort", querystring);

				request.setAttribute("type", sortform.getType());
				request.setAttribute("jobid", sortform.getId1());
				request.setAttribute("from", sortform.getFrom());
				request.setAttribute("appendixid", sortform.getId());

				request.setAttribute("page", sortform.getPage());
				request.setAttribute("ref1", sortform.getRef1());
				request.setAttribute("viewjobtype", sortform.getViewjobtype());

				request.setAttribute("ownerId", sortform.getOwnerId());

				return (mapping.findForward("tempsortpage"));
			}

			// Start : For Job/site Info for Partner
			else if (sortform.getType().equals("jobInformationPerPartner")) {
				session.setAttribute("jobinfopartner_sort", querystring);

				request.setAttribute("type", sortform.getType());
				request.setAttribute("pid", sortform.getId());
				request.setAttribute("from", sortform.getFrom());
				request.setAttribute("sortwindow", "1");

				return (mapping.findForward("toJobInformationPagePerPartner"));
			}

			else if (sortform.getType().equals("bulkJobCreation")) {
				request.setAttribute("ownerId", sortform.getOwnerId());
				request.setAttribute("viewjobtype", sortform.getViewjobtype());
				request.setAttribute("type", sortform.getType());
				request.setAttribute("appendixid", sortform.getId());
				request.setAttribute("stateid", sortform.getStateid());
				request.setAttribute("sitenumber", sortform.getSitenumber());
				request.setAttribute("sitecity", sortform.getSitecity());
				return (mapping.findForward("tempsortpage"));
			}
			if (sortform.getType().equals("PVSSearch")
					|| sortform.getType().equals("PVSSearchDate")) {
				if (sortform.getType().equals("PVSSearch")) {
					if (session.getAttribute("pvssearch_sort") != null)
						session.setAttribute("pvssearch_sort", querystring);
					else
						session.setAttribute("pvssearch_sort", querystring);
				}
				if (sortform.getType().equals("PVSSearchDate")) {
					if (session.getAttribute("pvssearch_date_sort") != null)
						session.setAttribute("pvssearch_date_sort", querystring);
					else
						session.setAttribute("pvssearch_date_sort", querystring);
				}
				request.setAttribute("orgTopName", sortform.getOrgTopName());
				request.setAttribute("action", sortform.getAction());
				request.setAttribute("startdate", sortform.getStartdate());
				request.setAttribute("enddate", sortform.getEnddate());
				request.setAttribute("division", sortform.getDivision());
				request.setAttribute("partnerType", sortform.getPartnerType());
				request.setAttribute("fromsort", "fromsort");

				// System.out.println("orgTopName="+request.getAttribute("orgTopName"));
				// System.out.println("action="+request.getAttribute("action"));
				// System.out.println("startdate="+request.getAttribute("startdate"));
				// System.out.println("enddate="+request.getAttribute("enddate"));
				// System.out.println("division="+request.getAttribute("division"));
				return (mapping.findForward("PVSSearch"));
			}

			// End
			return (mapping.findForward("errorpage"));
		} // End ;clicking on sort

		else {

			if (request.getParameter("Type") != null) {
				sortform.setType(request.getParameter("Type"));
			}

			if (request.getParameter("MSA_Id") != null) {
				sortform.setId(request.getParameter("MSA_Id"));
			}

			if (request.getParameter("Appendix_Id") != null) {
				sortform.setRef(request.getParameter("ref"));
				sortform.setId(request.getParameter("Appendix_Id"));
				sortform.setAddendum_id(request.getParameter("addendum_id"));
			}

			if (request.getParameter("Job_Id") != null) {
				sortform.setRef(request.getParameter("ref"));
				sortform.setId(request.getParameter("Job_Id"));
			}

			// Start
			if (request.getParameter("job_id") != null) {
				sortform.setId1(request.getParameter("job_id"));
			}
			if (request.getParameter("appendix_id") != null) {
				sortform.setId(request.getParameter("appendix_id"));
			}
			// End
			// Start for Job/site Information for a partner
			if (request.getParameter("partnerid") != null) {
				sortform.setId(request.getParameter("partnerid"));
			}
			// End
			if (request.getParameter("Activitylibrary_Id") != null) {
				if (request.getParameter("ref") != null) {
					sortform.setRef(request.getParameter("ref"));
				}

				if (request.getParameter("addendum_id") != null) {
					sortform.setAddendum_id(request.getParameter("addendum_id"));
				}

				sortform.setId(request.getParameter("Activitylibrary_Id"));
			}

			if (request.getParameter("category_Id") != null) {
				sortform.setId1(request.getParameter("category_Id"));
			}

			if (request.getParameter("appendixid") != null) {
				if (request.getParameter("appendixid").equals(""))
					sortform.setId("%");
				else
					sortform.setId(request.getParameter("appendixid"));
			}

			if (request.getParameter("jobid") != null) {
				sortform.setId(request.getParameter("jobid"));
			}

			if (request.getParameter("siteId") != null) {
				sortform.setSiteid(request.getParameter("siteId"));
			}
			if (request.getParameter("activityid") != null) {
				sortform.setId(request.getParameter("activityid"));
			}

			if (request.getParameter("powo_Jobid") != null) {
				sortform.setId(request.getParameter("powo_Jobid"));
			}

			Sortcodelist codes = new Sortcodelist();
			Collection parameter = null;
			parameter = new java.util.ArrayList();

			if (sortform.getType().equals("MSA")) {
				parameter.add(new LabelValue("MSA", "lo_ot_name"));
				parameter.add(new LabelValue("BDM", "SalesPOCname"));
				parameter
						.add(new LabelValue("Planned Delivery", "lp_need_date"));
				parameter.add(new LabelValue("Planned Effective",
						"lp_effective_date"));
				parameter.add(new LabelValue("Status", "lp_md_status"));
			}

			else if (sortform.getType().equals("Appendix")) {
				parameter.add(new LabelValue("Appendix", "appendixname"));
				parameter.add(new LabelValue("BDM", "CNSPOCname"));
				parameter.add(new LabelValue("Customer Division",
						"lo_om_division"));
				parameter
						.add(new LabelValue("Planned Delivery", "lx_need_date"));
				parameter.add(new LabelValue("Planned Effective",
						"lx_effective_date"));
				parameter.add(new LabelValue("Status", "lx_pr_status"));
			}

			else if (sortform.getType().equals("Job")) {
				parameter.add(new LabelValue("Job", "jobname"));
				parameter.add(new LabelValue("Sitename", "lm_si_name"));
				parameter.add(new LabelValue("Estimated Schedule Start Date",
						"lm_planned_start_date"));
				parameter.add(new LabelValue(
						"Estimated Schedule Complete Date",
						"lm_planned_end_date"));
				parameter.add(new LabelValue("Status", "lm_js_status"));
			}

			else if (sortform.getType().equals("Activity")) {
				parameter.add(new LabelValue("Activity Name", "lm_at_title"));
				parameter.add(new LabelValue("Material Cost",
						"lx_ce_material_cost"));
				parameter.add(new LabelValue("CNS Labor Cost",
						"lx_ce_cns_field_labor_cost"));
				parameter.add(new LabelValue("Contract Labor Cost",
						"lx_ce_ctr_field_labor_cost"));
				parameter.add(new LabelValue("Freight Cost",
						"lx_ce_frieght_cost"));
				parameter
						.add(new LabelValue("Travel Cost", "lx_ce_travel_cost"));
				parameter.add(new LabelValue("Total Cost", "lx_ce_total_cost"));
				parameter.add(new LabelValue("Extended Price",
						"lx_ce_total_price"));
				parameter
						.add(new LabelValue("ProForma Margin", "lx_ce_margin"));
				parameter.add(new LabelValue("Status", "lm_at_status"));
			}

			else if (sortform.getType().contains("pm_res")) {
				parameter.add(new LabelValue("Name", "iv_sc_name"));
				parameter.add(new LabelValue("Quantity", "lm_rs_quantity"));
				parameter.add(new LabelValue("Estimated Total Cost",
						"lx_ce_total_cost"));
				parameter
						.add(new LabelValue("ProForma Margin", "lx_ce_margin"));
				parameter.add(new LabelValue("Unit Price", "lx_ce_unit_price"));
				parameter.add(new LabelValue("Extended Price",
						"lx_ce_total_price"));
				parameter.add(new LabelValue("Status", "lm_rs_status"));
			}

			else if (sortform.getType().equals("am")
					|| sortform.getType().equals("amc")) {
				parameter.add(new LabelValue("Activity Name", "lm_at_title"));
				parameter.add(new LabelValue("Material Cost",
						"lx_cle_material_cost"));
				parameter.add(new LabelValue("CNS Labor Cost",
						"lx_cle_cns_field_labor_cost"));
				parameter.add(new LabelValue("Contract Labor Cost",
						"lx_cle_ctr_field_labor_cost"));
				parameter.add(new LabelValue("Freight Cost",
						"lx_cle_frieght_cost"));
				parameter.add(new LabelValue("Travel Cost",
						"lx_cle_travel_cost"));
				parameter
						.add(new LabelValue("Total Cost", "lx_cle_total_cost"));
				parameter.add(new LabelValue("Extended Price",
						"lx_cle_total_price"));
				parameter
						.add(new LabelValue("ProForma Margin", "lx_cle_margin"));
				parameter.add(new LabelValue("Status", "lm_at_status"));
			}

			else if (sortform.getType().equals("M")) {
				parameter.add(new LabelValue("Material Name", "iv_sc_name"));
				parameter.add(new LabelValue("Quantity", "lm_rs_quantity"));
				parameter.add(new LabelValue("Estimated Total Cost",
						"lx_cle_total_cost"));
				parameter
						.add(new LabelValue("ProForma Margin", "lx_cle_margin"));
				parameter
						.add(new LabelValue("Unit Price", "lx_cle_unit_price"));
				parameter.add(new LabelValue("Extended Price",
						"lx_cle_total_price"));
				parameter.add(new LabelValue("Status", "lm_rs_status"));
			}

			else if (sortform.getType().equals("L")) {
				parameter.add(new LabelValue("Labor Name", "iv_sc_name"));
				parameter.add(new LabelValue("Quantity", "lm_rs_quantity"));
				parameter.add(new LabelValue("Estimated Total Cost",
						"lx_cle_total_cost"));
				parameter
						.add(new LabelValue("ProForma Margin", "lx_cle_margin"));
				parameter
						.add(new LabelValue("Unit Price", "lx_cle_unit_price"));
				parameter.add(new LabelValue("Extended Price",
						"lx_cle_total_price"));
				parameter.add(new LabelValue("Status", "lm_rs_status"));
			}

			else if (sortform.getType().equals("F")) {
				parameter.add(new LabelValue("Freight Name", "iv_sc_name"));
				parameter.add(new LabelValue("Quantity", "lm_rs_quantity"));
				parameter.add(new LabelValue("Estimated Total Cost",
						"lx_cle_total_cost"));
				parameter
						.add(new LabelValue("ProForma Margin", "lx_cle_margin"));
				parameter
						.add(new LabelValue("Unit Price", "lx_cle_unit_price"));
				parameter.add(new LabelValue("Extended Price",
						"lx_cle_total_price"));
				parameter.add(new LabelValue("Status", "lm_rs_status"));
			}

			else if (sortform.getType().equals("T")) {
				parameter.add(new LabelValue("Travel Name", "iv_sc_name"));
				parameter.add(new LabelValue("Quantity", "lm_rs_quantity"));
				parameter.add(new LabelValue("Estimated Total Cost",
						"lx_cle_total_cost"));
				parameter
						.add(new LabelValue("ProForma Margin", "lx_cle_margin"));
				parameter
						.add(new LabelValue("Unit Price", "lx_cle_unit_price"));
				parameter.add(new LabelValue("Extended Price",
						"lx_cle_total_price"));
				parameter.add(new LabelValue("Status", "lm_rs_status"));
			}

			else if (sortform.getType().equals("prj_job")) {
				int k = 0;
				parameter.add(new LabelValue("Job Name", "jobname"));
				parameter.add(new LabelValue("Scope", "scope"));
				parameter.add(new LabelValue("Site#", "lm_si_number"));
				parameter.add(new LabelValue("State", "lm_si_state"));
				parameter.add(new LabelValue("Zipcode", "lm_si_zip_code"));
				parameter.add(new LabelValue("Schedule Start",
						"lm_planned_start_date"));
				parameter.add(new LabelValue("Schedule End",
						"lm_planned_end_date"));
				parameter.add(new LabelValue("Status", "lm_js_status"));
				parameter.add(new LabelValue("Owner", "job_created_by"));
				parameter.add(new LabelValue("Updated By", "job_changed_by"));

				// for filter sorting

				String currentstatus = request.getParameter("status");
				sortform.setCurrentstatus(currentstatus);

				request.setAttribute("currentstatus", currentstatus);
				// for filter sorting

			}

			else if (sortform.getType().equals("prj_netmedx")) {
				int k = 0;
				parameter.add(new LabelValue("Ticket #", "lm_tc_number"));
				parameter.add(new LabelValue("Standby", "lm_tc_standby"));
				parameter.add(new LabelValue("MSP", "lm_tc_msp"));
				parameter.add(new LabelValue("Help Desk", "lm_tc_help_desk"));
				parameter.add(new LabelValue("Site #", "lm_si_number"));
				parameter.add(new LabelValue("State", "cp_state_name"));
				parameter.add(new LabelValue("Zipcode", "lm_si_zip_code"));
				parameter.add(new LabelValue("Request Date",
						"lm_requested_date"));
				parameter.add(new LabelValue("Schedule Start",
						"lm_preferred_arrival"));
				parameter.add(new LabelValue("Criticality",
						"lo_call_criticality_des"));
				parameter.add(new LabelValue("Status", "lm_js_status"));
				parameter.add(new LabelValue("Owner", "job_created_by"));
				parameter.add(new LabelValue("Updated By", "job_changed_by"));

				String sortcustparam[] = Customersortparamdao
						.Customersortparam(sortform.getId(),
								getDataSource(request, "ilexnewDB"));

				if (sortcustparam != null) {
					for (int h = 0; h < sortcustparam.length; h++) {
						k = h + 1;

						parameter.add(new LabelValue(sortcustparam[h], "val"
								+ k));
					}
				}

				String currentstatus = request.getParameter("status");
				sortform.setCurrentstatus(currentstatus);
				request.setAttribute("currentstatus", currentstatus);

				String nettype = request.getParameter("nettype");
				sortform.setNettype(nettype);
				request.setAttribute("nettype", nettype);

				String ownerId = request.getParameter("ownerId");
				sortform.setOwnerId(ownerId);
				request.setAttribute("ownerId", sortform.getOwnerId());
			}

			else if (sortform.getType().equals("prj_activity")) {
				parameter.add(new LabelValue("Activity", "activityname"));
				parameter.add(new LabelValue("Estimated Schedule Start",
						"lx_se_start"));
				parameter.add(new LabelValue("Estimated Schedule End",
						"lx_se_end"));
				parameter.add(new LabelValue("Status", "lm_at_status"));
			}

			else if (sortform.getType().equals("prj_materialresource")) {
				parameter.add(new LabelValue("Material Name", "iv_rp_name"));
				parameter.add(new LabelValue("Material Type", "iv_ct_name"));
				parameter
						.add(new LabelValue("Manufacturer Name", "iv_mf_name"));

				parameter.add(new LabelValue("Status", "lm_rs_status"));
			}

			else if (sortform.getType().equals("prj_laborresource")) {
				parameter.add(new LabelValue("Labor Name", "iv_rp_name"));
				parameter.add(new LabelValue("Labor Type", "iv_ct_name"));
				parameter.add(new LabelValue("Status", "lm_rs_status"));
			} else if (sortform.getType().equals("prj_freightresource")) {
				parameter.add(new LabelValue("Freight Name", "iv_rp_name"));
				parameter.add(new LabelValue("freight Type", "iv_ct_name"));
				parameter.add(new LabelValue("Status", "lm_rs_status"));
			} else if (sortform.getType().equals("prj_travelresource")) {
				parameter.add(new LabelValue("Travel Name", "iv_rp_name"));
				parameter.add(new LabelValue("Travel Type", "iv_ct_name"));
				parameter.add(new LabelValue("Status", "lm_rs_status"));
			} else if (sortform.getType().equals("invoice_job")) {
				parameter.add(new LabelValue("Job Name", "jobname"));
				parameter.add(new LabelValue("Site Name", "lm_si_name"));
			}

			else if (sortform.getType().equals("prj_powo")) {

				parameter.add(new LabelValue("Source Company Name",
						"lm_po_partner_name"));
				parameter.add(new LabelValue("PO Number", "lm_po_number"));
				parameter.add(new LabelValue("Type", "lm_po_type"));
				parameter.add(new LabelValue("Issue Date", "lm_po_issue_date"));
				parameter.add(new LabelValue("Deliver By Date",
						"lm_po_deliver_by_date"));
				parameter.add(new LabelValue("Terms", "lm_po_terms"));
				parameter.add(new LabelValue("Master PO Item",
						"lm_pwo_type_desc"));
			}

			else if (sortform.getType().equals("jobdashboard")) {
				parameter.add(new LabelValue("Activity Name", "lm_at_title"));
				parameter.add(new LabelValue("Scope", "scope"));
				parameter.add(new LabelValue("Status", "lm_at_prj_status"));
			}

			else if (sortform.getType().equals("Inv_Appendix")
					|| sortform.getType().equals("Inv_MSA")
					|| sortform.getType().equals("All")) {
				parameter.add(new LabelValue("Customer Name", "lo_ot_name"));
				parameter.add(new LabelValue("Project Name", "lx_pr_title"));
				parameter.add(new LabelValue("Job Name", "jobname"));
				parameter.add(new LabelValue("Purchase Order", "lm_po_number"));
				parameter.add(new LabelValue("PO Authorized Total",
						"pwo_authorised_total_cost"));
				parameter.add(new LabelValue("Extended Price",
						"lx_ce_total_price"));
				parameter.add(new LabelValue("Status", "lm_js_status"));
				parameter.add(new LabelValue("Actual Start Date", "lx_start"));
				parameter.add(new LabelValue("Actual Complete Date", "lx_end"));
				parameter.add(new LabelValue("Submitted Date",
						"lm_submitted_date"));
				parameter.add(new LabelValue("Invoice Number",
						"lm_js_invoice_no"));
				parameter.add(new LabelValue("Invoice Date",
						"lm_associated_date"));
				parameter.add(new LabelValue("Owner", "job_created_by"));
				parameter.add(new LabelValue("Received Date",
						"lm_requested_date"));
				parameter.add(new LabelValue("Site Number", "lm_si_number"));
				parameter.add(new LabelValue("PVS Name", "lm_po_partner_name"));
				parameter.add(new LabelValue("Closed Date", "lm_closed_date"));
				parameter.add(new LabelValue("Customer Reference",
						"lm_tc_cust_reference"));
			}

			// Start : For Master Module
			else if (sortform.getType().equals("Masters")) {

				parameter.add(new LabelValue("MSA", "lo_ot_name"));
				parameter.add(new LabelValue("Site Name", "lp_si_name"));
				parameter.add(new LabelValue("Number", "lp_si_number"));
				parameter.add(new LabelValue("Locality Factor",
						"lp_si_locality_factor"));
				parameter.add(new LabelValue("Union Site", "lp_si_union_site"));
				parameter.add(new LabelValue("Designator", "lp_si_designator"));
				parameter.add(new LabelValue("Work Location",
						"lp_si_work_location"));
				parameter.add(new LabelValue("Address", "lp_si_address"));
				parameter.add(new LabelValue("City", "lp_si_city"));
				parameter.add(new LabelValue("State", "lp_si_state"));
				parameter.add(new LabelValue("Zip Code", "lp_si_zip_code"));
				parameter.add(new LabelValue("Country", "lp_si_country"));
				parameter.add(new LabelValue("Directions", "lp_si_directions"));
				parameter.add(new LabelValue("Primary Contact Person",
						"lp_si_pri_poc"));
				parameter.add(new LabelValue("Primary Phone Number",
						"lp_si_phone_no"));
				parameter.add(new LabelValue("Primary Email Id",
						"lp_si_pri_email_id"));
				parameter.add(new LabelValue("Secondary Contact Person",
						"lp_si_sec_poc"));
				parameter.add(new LabelValue("Secondary Email Id",
						"lp_si_sec_email_id"));
				parameter
						.add(new LabelValue("Special Condition", "lp_si_notes"));
			}
			// End :

			else if (sortform.getType().equals("siteHistory")) {
				if (sortform.getFrom().equals("netmedx")) {
					parameter
							.add(new LabelValue("Customer Name", "lo_ot_name"));
					parameter
							.add(new LabelValue("Appendix Name", "lx_pr_title"));
					parameter.add(new LabelValue("Job Name", "jobname"));

					parameter.add(new LabelValue("Request Received Date",
							"lm_tc_requested_date"));
					parameter.add(new LabelValue("Request Received Time",
							"lm_tc_requested_time"));
					parameter.add(new LabelValue("Schedule Arrival Date",
							"lm_tc_preferred_arrival"));
					parameter.add(new LabelValue("Scheduled Arrival Time",
							"lm_tc_preferred_arrival_time"));

					parameter.add(new LabelValue("Criticality",
							"lo_call_criticality_des"));
					parameter.add(new LabelValue("Resource", "resource_level"));

					parameter.add(new LabelValue("MSP", "lm_tc_msp"));
					parameter.add(new LabelValue("Standby", "lm_tc_standby"));
					parameter.add(new LabelValue("Hourly Cost", "hourly_cost"));
					parameter
							.add(new LabelValue("Hourly Price", "hourly_price"));
					parameter.add(new LabelValue("Hourly VGP", "hourly_vgp"));

					parameter.add(new LabelValue("Owner", "job_created_by"));
					parameter.add(new LabelValue("Status", "lm_js_status"));
				} else {
					parameter
							.add(new LabelValue("Customer Name", "lo_ot_name"));
					parameter
							.add(new LabelValue("Appendix Name", "lx_pr_title"));
					parameter.add(new LabelValue("Job Name", "jobname"));

					parameter.add(new LabelValue("Schedule Start",
							"lm_js_planned_start_date"));
					parameter.add(new LabelValue("Scheduled Time",
							"lx_act_start_time"));
					parameter.add(new LabelValue("Schedule Complete",
							"lm_js_planned_end_date"));

					parameter.add(new LabelValue("Actual Start Date",
							"lx_se_start"));
					parameter.add(new LabelValue("Actual Start Time",
							"lx_se_start_time"));
					parameter.add(new LabelValue("Actual End", "lx_se_end"));

					parameter.add(new LabelValue("Estimated Cost",
							"lx_ce_total_cost"));
					parameter.add(new LabelValue("Extended  Price",
							"lx_ce_total_price"));
					parameter.add(new LabelValue("Pro Forma VGP",
							"lx_ce_margin"));
					parameter.add(new LabelValue("Actual Cost",
							"lx_ce_actual_cost"));
					parameter.add(new LabelValue("Actual VGP",
							"lx_ce_actual_vgp"));

					parameter.add(new LabelValue("Owner", "job_created_by"));
					parameter.add(new LabelValue("Status", "lm_js_status"));
				}
			}
			// End
			// Start
			else if (sortform.getType().equals("jobInformationPerPartner")) {
				parameter.add(new LabelValue("Customer Name", "lo_ot_name"));
				parameter.add(new LabelValue("Appendix Name", "lx_pr_title"));
				parameter.add(new LabelValue("Job Name", "lm_js_title"));
				parameter.add(new LabelValue("Purchase Order", "lm_po_number"));

				parameter.add(new LabelValue("Planned Start",
						"lm_planned_start_date"));
				parameter.add(new LabelValue("Actual Start", "lx_start"));
				parameter.add(new LabelValue("Actual End", "lx_end"));

				parameter.add(new LabelValue("Owner", "job_created_by"));
				parameter.add(new LabelValue("Site Number", "lm_si_number"));
				parameter.add(new LabelValue("Site Name", "lm_si_name"));
				parameter.add(new LabelValue("Status", "lm_js_status"));

				parameter.add(new LabelValue("Estimated Cost",
						"lx_ce_total_cost"));
				parameter.add(new LabelValue("Extended Price",
						"lx_ce_total_price"));
				parameter.add(new LabelValue("ProFormaVGP", "lx_ce_margin"));
				parameter
						.add(new LabelValue("Actual Cost", "lx_ce_actual_cost"));
				parameter.add(new LabelValue("Actual VGP", "lx_ce_actual_vgp"));
			}

			else if (sortform.getType().equals("siteManage")) {
				if (sortform.getSiteNumberForColumn().equals("on"))
					parameter.add(new LabelValue("Number", "lp_si_number"));

				if (sortform.getSiteNameForColumn().equals("on"))
					parameter.add(new LabelValue("Name", "lp_si_name"));

				if (sortform.getSiteAddressForColumn().equals("on"))
					parameter.add(new LabelValue("Address", "lp_si_address"));

				if (sortform.getSiteCityForColumn().equals("on"))
					parameter.add(new LabelValue("City ", "lp_si_city"));

				if (sortform.getSiteStateForColumn().equals("on"))
					parameter.add(new LabelValue("State", "lp_si_state"));

				if (sortform.getSiteZipForColumn().equals("on"))
					parameter.add(new LabelValue("ZipCode", "lp_si_zip_code"));

				if (sortform.getSiteCountryForColumn().equals("on"))
					parameter.add(new LabelValue("Country", "lp_si_country"));

				parameter.add(new LabelValue("Brand", "lp_si_group"));

				parameter.add(new LabelValue("End Customer",
						"lp_si_end_customer"));

			}

			else if (sortform.getType().equals("bulkJobCreation")) {
				parameter.add(new LabelValue("Site Name", "lp_si_name"));
				parameter.add(new LabelValue("Brand", "lp_si_group"));
				parameter.add(new LabelValue("Site City", "lp_si_city"));
				parameter.add(new LabelValue("Site State", "lp_si_state"));
			} else if (sortform.getType().equals("PVSSearch")) {
				parameter.add(new LabelValue("Partner Name", "lo_om_division"));
				parameter
						.add(new LabelValue("Address Line 1", "lo_ad_address1"));
				parameter
						.add(new LabelValue("Address Line 2", "lo_ad_address2"));
				parameter.add(new LabelValue("City", "lo_ad_city"));
				parameter.add(new LabelValue("State", "lo_ad_state"));
				parameter.add(new LabelValue("Country", "lo_ad_country"));
				parameter.add(new LabelValue("Zip Code", "lo_ad_zip_code"));
			} else if (sortform.getType().equals("PVSSearchDate")) {
				if (sortform.getAction().equals("annualreport"))
					parameter.add(new LabelValue("Review Date",
							"cp_pd_reg_renewal_date"));
				else
					parameter.add(new LabelValue("Date", "cp_pt_date"));

				parameter.add(new LabelValue("Partner Name", "lo_om_division"));
				parameter
						.add(new LabelValue("Address Line 1", "lo_ad_address1"));
				parameter
						.add(new LabelValue("Address Line 2", "lo_ad_address2"));
				parameter.add(new LabelValue("City", "lo_ad_city"));
				parameter.add(new LabelValue("State", "lo_ad_state"));
				parameter.add(new LabelValue("Country", "lo_ad_country"));
				parameter.add(new LabelValue("Zip Code", "lo_ad_zip_code"));
			}

			// for PVs search Page
			else if (sortform.getType().equals("partnerSearch")) {
				parameter.add(new LabelValue("Partner Name", "lo_om_division"));
				parameter.add(new LabelValue("City", "lo_ad_city"));
				parameter.add(new LabelValue("State", "lo_ad_state"));
				parameter.add(new LabelValue("ADPO", "cp_pd_adpo"));
				parameter.add(new LabelValue("Rating", "qc_ps_average_rating"));
				parameter.add(new LabelValue("Distance", "distance"));
				parameter.add(new LabelValue("Last Used", "last_used"));
				parameter.add(new LabelValue("Diversity Indicator",
						"cp_pd_diversity_indicator"));
				if (!sortform.getZip().equals("")
						|| !sortform.getJobId().equals("")) {
					parameter.add(new LabelValue("Bid", "bid"));
					parameter.add(new LabelValue("Vendex Matches",
							"vendex_matches"));
				}
			}
			// End
			codes.setSortparameter(parameter);
			request.setAttribute("codelist", codes);
			return (mapping.findForward("sortwindow"));
		}
	}
}
