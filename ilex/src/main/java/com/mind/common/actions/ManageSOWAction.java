package com.mind.common.actions;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.am.ActivityLibrary;
import com.mind.common.bean.Sow;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.formbean.SOWForm;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ManageSOWAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManageSOWAction.class);

	public ActionForward View(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SOWForm sowform = (SOWForm) form;
		String type = "";
		String Id = "";
		int sow_size = 0;
		String temp = "";
		String name = "";
		NameId idname = null;
		NameId appendixidname = null;
		String activityname = "";
		String from = "";
		String jobid = "";
		char resourceValue;
		String resource = "";

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (request.getParameter("Type") != null) {
			sowform.setType(request.getParameter("Type"));

		}
		if (request.getParameter("resourceListType") != null) {
			sowform.setResourceListType(request
					.getParameter("resourceListType"));

		}
		if (request.getParameter("Id") != null)
			sowform.setId(request.getParameter("Id"));

		if (request.getParameter("Job_Id") != null)
			sowform.setJobId(request.getParameter("Job_Id"));

		if (request.getParameter("Activity_Id") != null)
			sowform.setActivity_Id(request.getParameter("Activity_Id"));

		if (request.getParameter("addendum_id") != null)
			sowform.setAddendum_id(request.getParameter("addendum_id"));

		if (request.getParameter("ref") != null)
			sowform.setRef(request.getParameter("ref"));

		if (request.getParameter("resourcelistpage") != null)
			sowform.setResourceListPage(request
					.getParameter("resourcelistpage"));

		if (sowform.getType().equals("am")
				|| sowform.getType().equals("Activity"))
			temp = "Activity";

		if (sowform.getType().equals("M"))
			temp = "Material";

		if (sowform.getType().equals("L"))
			temp = "Labor";

		if (sowform.getType().equals("F"))
			temp = "Freight";

		if (sowform.getType().equals("T"))
			temp = "Travel";

		if (sowform.getType().equals("pm_resM")
				|| sowform.getType().equals("pm_resL")
				|| sowform.getType().equals("pm_resF")
				|| sowform.getType().equals("pm_resT")
				|| sowform.getType().equals("pm_resA"))
			temp = "Resource";
		// From Project Manager
		if (sowform.getType().equals("PRM"))
			temp = "Manage PRM Activity";

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(loginuserid, temp, "Manage SOW",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (sowform.getType().equals("pm_resM")
				|| sowform.getType().equals("pm_resL")
				|| sowform.getType().equals("pm_resF")
				|| sowform.getType().equals("pm_resT")) {
			resourceValue = sowform.getType().charAt(6);
			sowform.setResourceType(resourceValue + "");
		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			jobid = request.getParameter("jobid");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
			sowform.setDashboardid(request.getParameter("jobid"));
			sowform.setFromflag(request.getParameter("from"));
		}

		sowform.setAppendixName(Jobdao.getoplevelname(
				request.getParameter("Job_Id"), "Activity",
				getDataSource(request, "ilexnewDB")));
		sowform.setAppendixId(Activitydao.getAppendixid(
				getDataSource(request, "ilexnewDB"),
				request.getParameter("Job_Id")));

		if (sowform.getType().equals("Activity")) {
			idname = Activitydao.getIdName(request.getParameter("Job_Id"),
					"Activity", "MSA", getDataSource(request, "ilexnewDB"));
			sowform.setMsaName(idname.getName());
			sowform.setMsaId(idname.getId());
			sowform.setActivityName(Activitydao.getActivityname(
					sowform.getId(), getDataSource(request, "ilexnewDB")));
			sowform.setJobName(Jobdao.getJobname(
					request.getParameter("Job_Id"),
					getDataSource(request, "ilexnewDB")));
		}

		if (sowform.getType().equals("pm_resM")
				|| sowform.getType().equals("pm_resL")
				|| sowform.getType().equals("pm_resF")
				|| sowform.getType().equals("pm_resT")) {

			appendixidname = Activitydao
					.getIdName(sowform.getActivity_Id(), "Resource",
							"Appendix", getDataSource(request, "ilexnewDB"));
			sowform.setAppendixId(appendixidname.getId());
			sowform.setAppendixName(appendixidname.getName());

			idname = Activitydao.getIdName(sowform.getActivity_Id(),
					"Resource", "MSA", getDataSource(request, "ilexnewDB"));
			sowform.setMsaId(idname.getId());
			sowform.setMsaName(idname.getName());

			sowform.setJobName(Jobdao.getoplevelname(sowform.getActivity_Id(),
					"Resource", getDataSource(request, "ilexnewDB")));
			sowform.setJobId(Activitydao.getJobid(
					getDataSource(request, "ilexnewDB"),
					sowform.getActivity_Id()));
			activityname = Activitydao.getActivityname(
					sowform.getActivity_Id(),
					getDataSource(request, "ilexnewDB"));
			sowform.setActivityName(activityname);

		}

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"), sowform.getId(),
				sowform.getType());

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		if (!(sowform.getType().equals("am") || sowform.getType().equals(
				"Activity"))) {
			name = ActivityLibrarydao.Getnameforsowassumption(sowform.getId(),
					sowform.getType(), getDataSource(request, "ilexnewDB"));
		}

		sowform.setName(name);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("Size", "" + sow_size);
		request.setAttribute("menuid", sowform.getId());

		if (sowform.getType().equals("M") || sowform.getType().equals("L")
				|| sowform.getType().equals("T")
				|| sowform.getType().equals("F")) {
			ActivityLibrary activitylibrary = ActivityLibrarydao.getActivity(
					sowform.getActivity_Id(),
					getDataSource(request, "ilexnewDB"));

			sowform.setMsaId(activitylibrary.getMsa_Id());
			sowform.setMsaName(activitylibrary.getMsaname());
			sowform.setActivityName(activitylibrary.getName());
			request.setAttribute("ActivityManager", "AM");
		}

		if (sowform.getType().equals("Activity")) {

			String jobtype = "";
			String activityStatus = "";
			String activityType = "";
			String activityStatusList = "";
			String MSA_Id = "";
			String MSA_Name = "";
			int checkNetmedXappendix = 0;
			String[] activityStatusAndType = Activitydao.getActivityTypeStatus(
					sowform.getId(), getDataSource(request, "ilexnewDB"));
			activityStatus = activityStatusAndType[0];
			activityType = activityStatusAndType[1];

			if (request.getParameter("from") != null) {
				from = request.getParameter("from");
				jobid = request.getParameter("jobid");
				request.setAttribute("from", request.getParameter("from"));
				request.setAttribute("jobid", request.getParameter("jobid"));
			}

			jobtype = Jobdao.getJobtype(sowform.getJobId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("jobtype", jobtype);

			if (sowform.getRef().equalsIgnoreCase("view")
					|| sowform.getRef().equalsIgnoreCase("detailactivity")) {
				request.setAttribute("chkaddendum", "detailactivity");
				request.setAttribute("chkaddendumresource", "View");

				if (request.getParameter("from") != null) {
					if (jobtype.equals("Addendum") || jobtype.equals("Default")) {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), from, jobid,
								sowform.getId(), "detailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					} else {
						activityStatus = JobDashboarddao
								.getActivityprojectstatus(sowform.getId(),
										getDataSource(request, "ilexnewDB"));
						activityStatusList = Menu.getStatus("prj_act",
								activityStatus.charAt(0), from, jobid,
								sowform.getId(), "detailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					}
				} else {
					activityStatusList = Menu.getStatus("pm_act",
							activityStatus.charAt(0), "", "", sowform.getId(),
							"detailactivity", loginuserid,
							getDataSource(request, "ilexnewDB"));

				}

			}

			if (sowform.getRef().equalsIgnoreCase("inscopeactivity")
					|| sowform.getRef().equalsIgnoreCase(
							"inscopedetailactivity")) {
				request.setAttribute("chkaddendum", "inscopedetailactivity");
				request.setAttribute("chkaddendumresource", "Viewinscope");

				if (request.getParameter("from") != null) {

					if (jobtype.equals("Addendum") || jobtype.equals("Default")) {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), from, jobid,
								sowform.getId(), "inscopedetailactivity",
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					} else {
						activityStatus = JobDashboarddao
								.getActivityprojectstatus(sowform.getId(),
										getDataSource(request, "ilexnewDB"));
						activityStatusList = Menu.getStatus("prj_act",
								activityStatus.charAt(0), from, jobid,
								sowform.getId(), "inscopedetailactivity",
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					}
				} else {
					activityStatusList = Menu.getStatus("pm_act",
							activityStatus.charAt(0), "", "", sowform.getId(),
							"inscopedetailactivity", loginuserid,
							getDataSource(request, "ilexnewDB"));

				}

			}

			MSA_Id = Activitydao.getmsaid(getDataSource(request, "ilexnewDB"),
					sowform.getJobId());
			MSA_Name = Activitydao.getMsaname(
					getDataSource(request, "ilexnewDB"), MSA_Id);
			request.setAttribute("MSA_Name", MSA_Name);
			request.setAttribute("MSA_Id", MSA_Id);

			checkNetmedXappendix = Activitydao.checkAppendixname(
					sowform.getAppendixId(), sowform.getJobId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("checknetmedx", checkNetmedXappendix + "");
			request.setAttribute("activityStatusList", activityStatusList);
			request.setAttribute("Activity_Id", sowform.getId());
			request.setAttribute("Job_Id", sowform.getJobId());
			request.setAttribute("jobtype", jobtype);
			request.setAttribute("from", from);
			request.setAttribute("jobid", jobid);
			request.setAttribute("activityStatus", activityStatus);
			request.setAttribute("activityType", activityType);
			request.setAttribute("activityTypeForJob", sowform.getRef());

		}
		if (sowform.getJobId() != null
				&& !sowform.getJobId().trim().equalsIgnoreCase("")) {
			Map<String, Object> map;

			map = DatabaseUtilityDao.setMenu(sowform.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		request.setAttribute("checkingRef", sowform.getRef());
		return mapping.findForward("sowtabularpage");

	}

	public ActionForward Submit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SOWForm sowform = (SOWForm) form;
		String type = "";
		String Id = "";
		String addendum_id = "";
		String tempref = "";
		int sow_size = 0;
		Sow sow = null;
		String name = "";

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		Id = sowform.getId();
		type = sowform.getType();
		addendum_id = sowform.getAddendum_id();
		tempref = sowform.getRef();

		int checklength = 0;

		if (sowform.getCheck() != null) {
			checklength = sowform.getCheck().length;
		}

		int[] index = new int[checklength];
		int k = 0;

		if (checklength > 0) {
			int length = sowform.getSow_Id().length;
			for (int i = 0; i < checklength; i++) {
				for (int j = 0; j < length; j++) {
					if (sowform.getCheck(i).equals(sowform.getSow_Id(j))) {
						index[k] = j;
						k++;
					}
				}
			}
		}

		if (checklength > 0) {
			for (int i = 0; i < index.length; i++) {
				sow = new Sow();

				sow.setSow_Id(sowform.getSow_Id(index[i]));
				sow.setSow(sowform.getSow(index[i]));
				sow.setType(type);

				int updateflag = ActivityLibrarydao.Updatesow(sow,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("updateflag", updateflag + "");

				request.setAttribute("sowupdateflag", updateflag + "");
			}
		}

		if (sowform.getNewsow_Id() != null) {
			try {
				sow = new Sow();

				sow.setId(sowform.getId());
				sow.setSow(sowform.getNewsow());

				sow.setType(sowform.getType());

				int addflag = ActivityLibrarydao.addsow(sow,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("addflag", addflag + "");
				request.setAttribute("sowaddflag", addflag + "");
			} catch (Exception e) {
				logger.error(
						"Submit(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("Submit(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Error Occured"
							+ e);
				}
			}

		}

		// if( sowform.getFromflag().equals( "jobdashboard" ))
		// {
		// request.setAttribute( "jobid" , sowform.getDashboardid() );
		// return mapping.findForward( "jobdashboard" );
		// }

		sowform.reset(mapping, request);

		sowform.setId(Id);
		sowform.setType(type);
		sowform.setAddendum_id(addendum_id);
		sowform.setRef(tempref);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"), sowform.getId(),
				sowform.getType());

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		if (!(sowform.getType().equals("am") || sowform.getType().equals(
				"Activity"))) {
			name = ActivityLibrarydao.Getnameforsowassumption(sowform.getId(),
					sowform.getType(), getDataSource(request, "ilexnewDB"));
		}
		sowform.setName(name);

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("Size", "" + sow_size);
		request.setAttribute("menuid", sowform.getId());

		/*
		 * System.out.println("Activity_Id ::: " + sowform.getActivity_Id());
		 * System.out.println("checkingRef ::: " + sowform.getRef());
		 * System.out.println("Material_Id ::: " + sowform.getId());
		 * System.out.println("Resource Status ::: " +
		 * sowform.getResourceStatus());
		 * System.out.println("Resource Status list ::: " +
		 * sowform.getResourceStatusList());
		 * System.out.println("Addendum Id ::: " + "0");
		 * System.out.println("From  ::::" + sowform.getFromflag());
		 * System.out.println("job id ::::" + sowform.getDashboardid() );
		 */

		if (sowform.getType().equals("M") || sowform.getType().equals("L")
				|| sowform.getType().equals("T")
				|| sowform.getType().equals("F")) {
			ActivityLibrary activitylibrary = ActivityLibrarydao.getActivity(
					sowform.getActivity_Id(),
					getDataSource(request, "ilexnewDB"));

			sowform.setMsaId(activitylibrary.getMsa_Id());
			sowform.setMsaName(activitylibrary.getMsaname());
			sowform.setActivityName(activitylibrary.getName());
			request.setAttribute("ActivityManager", "AM");
		}

		if (sowform.getJobId() != null
				&& !sowform.getJobId().trim().equalsIgnoreCase("")) {
			Map<String, Object> map;

			map = DatabaseUtilityDao.setMenu(sowform.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		if (sowform.getResourceListPage() != null
				&& sowform.getResourceListPage().equals("resourcelistpage")) {

			request.setAttribute("from", sowform.getFromflag());
			request.setAttribute("type", "sowAssumptiontoResource");
			request.setAttribute("jobid", sowform.getDashboardid());
			request.setAttribute("Activity_Id", sowform.getActivity_Id());
			request.setAttribute("resourceType", sowform.getResourceType());
			request.setAttribute("resourceListType",
					sowform.getResourceListType());
			request.setAttribute("ref", sowform.getRef());
			request.setAttribute("addendum_id", "0");

			return mapping.findForward("sowtabularpage");

			// return mapping.findForward("tempuploadpage");

		}

		return mapping.findForward("sowtabularpage");
	}

	public ActionForward Delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SOWForm sowform = (SOWForm) form;
		String type = "";
		String Id = "";
		int sow_size = 0;
		Sow sow = null;
		String addendum_id = "";
		String tempref = "";
		String name = "";
		Id = sowform.getId();
		type = sowform.getType();
		addendum_id = sowform.getAddendum_id();
		tempref = sowform.getRef();

		int checklength = 0;

		if (sowform.getCheck() != null) {
			checklength = sowform.getCheck().length;
		}

		int[] index = new int[checklength];
		int k = 0;

		if (checklength > 0) {
			int length = sowform.getSow_Id().length;
			for (int i = 0; i < checklength; i++) {
				for (int j = 0; j < length; j++) {
					if (sowform.getCheck(i).equals(sowform.getSow_Id(j))) {
						index[k] = j;
						k++;
					}
				}
			}
		}

		if (checklength > 0) {
			for (int i = 0; i < index.length; i++) {
				sow = new Sow();

				sow.setSow_Id(sowform.getSow_Id(index[i]));
				sow.setSow(sowform.getSow(index[i]));
				sow.setType(type);

				int deleteflag = ActivityLibrarydao.Deletesow(sow,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("deleteflag", deleteflag + "");
			}
		}

		if (sowform.getFromflag().equals("jobdashboard")) {
			request.setAttribute("jobid", sowform.getDashboardid());
			return mapping.findForward("jobdashboard");
		}

		sowform.reset(mapping, request);

		sowform.setId(Id);
		sowform.setType(type);
		sowform.setAddendum_id(addendum_id);
		sowform.setRef(tempref);

		ArrayList sowlist = ActivityLibrarydao.getSowlist(
				getDataSource(request, "ilexnewDB"), sowform.getId(),
				sowform.getType());

		if (sowlist.size() > 0) {
			sow_size = sowlist.size();
		}

		if (!(sowform.getType().equals("am") || sowform.getType().equals(
				"Activity"))) {
			name = ActivityLibrarydao.Getnameforsowassumption(sowform.getId(),
					sowform.getType(), getDataSource(request, "ilexnewDB"));
		}
		sowform.setName(name);

		if (sowform.getType().equals("M") || sowform.getType().equals("L")
				|| sowform.getType().equals("T")
				|| sowform.getType().equals("F")) {
			ActivityLibrary activitylibrary = ActivityLibrarydao.getActivity(
					sowform.getActivity_Id(),
					getDataSource(request, "ilexnewDB"));

			sowform.setMsaId(activitylibrary.getMsa_Id());
			sowform.setMsaName(activitylibrary.getMsaname());
			sowform.setActivityName(activitylibrary.getName());
			request.setAttribute("ActivityManager", "AM");
		}

		request.setAttribute("sowlist", sowlist);
		request.setAttribute("Size", "" + sow_size);
		request.setAttribute("menuid", sowform.getId());

		if (sowform.getType().equals("pm_resM")
				|| sowform.getType().equals("pm_resL")
				|| sowform.getType().equals("pm_resF")
				|| sowform.getType().equals("pm_resT")) {
			request.setAttribute("from", sowform.getFromflag());
			request.setAttribute("jobid", sowform.getDashboardid());
			request.setAttribute("Activity_Id", sowform.getActivity_Id());
			request.setAttribute("Material_Id", sowform.getId());
			request.setAttribute("Labor_Id", sowform.getId());
			request.setAttribute("Freight_Id", sowform.getId());
			request.setAttribute("Travel_Id", sowform.getId());

			request.setAttribute("commonResourceStatus",
					sowform.getResourceStatus());
			request.setAttribute("materialStatusList",
					sowform.getResourceStatusList());
			request.setAttribute("laborStatusList",
					sowform.getResourceStatusList());
			request.setAttribute("freightStatusList",
					sowform.getResourceStatusList());
			request.setAttribute("travelStatusList",
					sowform.getResourceStatusList());
			request.setAttribute("checkingRef", sowform.getRef());
			request.setAttribute("addendum_id", "0");
			request.setAttribute("chkaddendum", sowform.getChkaddendum());
			request.setAttribute("chkdetailactivity",
					sowform.getChkdetailactivity());
		}

		return mapping.findForward("sowtabularpage");

	}
}
