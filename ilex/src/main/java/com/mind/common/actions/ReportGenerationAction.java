package com.mind.common.actions;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.DynamicComboDocM;
import com.mind.common.dao.DocMIlexDao;
import com.mind.common.formbean.ReportGenerationForm;
import com.mind.docm.dao.MSA;
import com.mind.docm.dao.MSAList;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.ilex.reports.utils.ReRunUtility;

public class ReportGenerationAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ReportGenerationAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ReportGenerationForm reportgenerateform = (ReportGenerationForm) form;
		DynamicComboDocM dcdocm = new DynamicComboDocM();
		ReRunUtility reRunUtility = new ReRunUtility();
		MSAList msalist = new MSAList();
		String status = null;

		dcdocm.setMsalist(DocMIlexDao.getMsalist()); // for filling the msa
														// combo list
		request.setAttribute("msalist", dcdocm);
		dcdocm.setMonthnamelist(DocMIlexDao.getMonthName()); // for filling the
																// month combo
																// list
		request.setAttribute("monthname", dcdocm);
		dcdocm.setReportlist(DocMIlexDao.getReportName()); // for filling the
															// report combo list
		request.setAttribute("reportname", dcdocm);

		if (request.getParameter("firsttime") != null) {
			reportgenerateform.setSelectedReport("1");
			Calendar cal = Calendar.getInstance();
			int month = cal.get(cal.MONTH);
			int year = cal.get(cal.YEAR);

			if (month == 0) {
				month = 12;
				year = year - 1;
			}

			Calendar calendar = Calendar.getInstance();
			reportgenerateform.setSelectedYear("" + year);
			reportgenerateform.setSelectedMonth("" + month);

		}
		try {
			if (reportgenerateform.getGenerateReport() != null) {
				if (reportgenerateform.getSelectedMsa() != null) {
					MSA[] msaArray = new MSA[reportgenerateform
							.getSelectedMsa().length];
					for (int j = 0; j < reportgenerateform.getSelectedMsa().length; j++) {
						MSA msa = new MSA();
						String id[] = reportgenerateform.getSelectedMsa(j)
								.split("~");
						msa.setEntityId(id[0]);
						msa.getAppendix().setEntityId(id[1]);
						msa.setEntityName(id[2]);
						msaArray[j] = msa;
						if (logger.isDebugEnabled()) {
							logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - "
									+ reportgenerateform.getSelectedMsa(j));
						}
					}
					msalist.setMsaArray(msaArray);
				}
				List list = reRunUtility
						.reRunReportScheduler(
								DatabaseUtilityDao
										.getKeyValue("ilex.docm.entity.netmedx.report"),
								msalist,
								new Long(reportgenerateform.getSelectedMonth())
										.longValue(), new Long(
										reportgenerateform.getSelectedYear())
										.longValue(), request,
								getDataSource(request, "ilexnewDB"));
				request.setAttribute("MSAmap", list);
			}
		} catch (Exception e) {
			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
			request.setAttribute("Error", "Error Occurred");
			status = "error";
		}
		if (request.getParameter("firsttime") == null) {
			if (status == null)
				request.setAttribute("Success", "Reports Re-Generated");
		}

		return mapping.findForward("success");
	}
}
