/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is an dispatch action class used for viewing uploaded documents      
 *
 */

package com.mind.common.actions;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.bean.Fileinfo;
import com.mind.common.dao.Menu;
import com.mind.common.dao.Upload;
import com.mind.common.dao.ViewList;
import com.mind.common.formbean.ViewDocumentForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class ViewuploadedocumentAction extends
		com.mind.common.IlexDispatchAction {
	/**
	 * This method is used to retrieve uploaded documents against Job
	 * 
	 * @param mapping
	 *            Object of ActionMapping
	 * @param form
	 *            Object of ActionForm
	 * @param request
	 *            Object of HttpServletRequest
	 * @param response
	 *            Object of HttpServletResponse
	 * @return ActionForward This method returns object of ActionForward.
	 */
	public ActionForward Viewjobdocument(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ViewDocumentForm formdetail = (ViewDocumentForm) form;
		String Job_Id = "";
		ArrayList uploadeddocs = new ArrayList();
		String name = "";
		String filelistlength = "";
		String MSA_Id = "";
		String msaname = "";
		String Appendix_Id = "";
		String appendixName = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (request.getParameter("Id") != null) {
			formdetail.setJobId(request.getParameter("Id"));
			formdetail.setId(request.getParameter("Id"));
			formdetail
					.setAppendix_Id(com.mind.dao.PRM.Jobdao.getAppendixid(
							formdetail.getJobId(),
							getDataSource(request, "ilexnewDB")));
			formdetail.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					formdetail.getAppendix_Id()));
			formdetail.setMSA_Id(IMdao.getMSAId(formdetail.getAppendix_Id(),
					getDataSource(request, "ilexnewDB")));
			formdetail
					.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
							getDataSource(request, "ilexnewDB"),
							formdetail.getMSA_Id()));
		}

		if (request.getParameter("ref") != null) {
			formdetail.setRef(request.getParameter("ref"));
		}

		// if( request.getParameter( "ref1" ) != null ) {
		// formdetail.setRef(request.getParameter( "ref1" ) );
		// }

		if (request.getParameter("addendum_id") != null) {
			formdetail.setAddendum_id(request.getParameter("addendum_id"));
		}

		if (formdetail.getDelete() != null) {
			Job_Id = formdetail.getId();
			int retval = -1;
			String indexvalue = "";

			if (formdetail.getCheck() != null) {
				if (formdetail.getCheck().length > 0) {
					String index[] = new String[formdetail.getCheck().length];
					index = formdetail.getCheck();

					for (int i = 0; i < formdetail.getCheck().length; i++) {
						indexvalue = indexvalue + index[i] + ",";
					}
					indexvalue = indexvalue.substring(0,
							indexvalue.length() - 1);
				}
			}
			retval = Upload.Deleteuploadedfile(indexvalue,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("retval", "" + retval);
		}

		uploadeddocs = Upload.getuploadedfiles("%", formdetail.getJobId(),
				"Job", getDataSource(request, "ilexnewDB"));
		filelistlength = "" + uploadeddocs.size();
		formdetail.setName(Jobdao.getJobname(formdetail.getJobId(),
				getDataSource(request, "ilexnewDB")));
		request.setAttribute("uploadeddocs", uploadeddocs);
		request.setAttribute("filelistlength", filelistlength);

		formdetail.setType("Job");

		String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
				formdetail.getJobId(), "%", "%",
				getDataSource(request, "ilexnewDB"));
		String jobStatus = jobStatusAndType[0];
		String jobType1 = jobStatusAndType[1];
		String jobStatusList = "";
		String appendixType = "";

		if (jobType1.equals("Default"))
			jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
					+ formdetail.getJobId() + "&Status='D'\"" + "," + "0";
		if (jobType1.equals("Newjob"))
			jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0), "",
					"", formdetail.getJobId(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));
		if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
			jobStatusList = "\"Approved\""
					+ ","
					+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
					+ formdetail.getJobId() + "&Status=A\"" + "," + "0";
		if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
			jobStatusList = "\"Draft\""
					+ ","
					+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
					+ formdetail.getJobId() + "&Status=D\"" + "," + "0";
		if (jobType1.equals("Addendum"))
			jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0), "",
					"", formdetail.getJobId(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));

		appendixType = ViewList.getAppendixtypedesc(
				formdetail.getAppendix_Id(),
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("jobStatus", jobStatus);
		request.setAttribute("job_type", jobType1);
		request.setAttribute("appendixtype", appendixType);
		request.setAttribute("Job_Id", formdetail.getJobId());
		request.setAttribute("Appendix_Id", formdetail.getAppendix_Id());
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("addendum_id", "0");

		if (jobType1.equals("Newjob")) {
			request.setAttribute("chkadd", "detailjob");
			request.setAttribute("chkaddendum", "detailjob");
			request.setAttribute("chkaddendumactivity", "View");
		}
		if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
			request.setAttribute("chkadd", "inscopejob");
			request.setAttribute("chkaddendum", "inscopejob");
			request.setAttribute("chkaddendumactivity", "inscopeactivity");
		}

		return mapping.findForward("viewdocumentpage");

	}

	public ActionForward Viewappendixdocument(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ViewDocumentForm formdetail = (ViewDocumentForm) form;
		String Appendix_Id = "";
		ArrayList uploadeddocs = new ArrayList();
		String name = "";
		String filelistlength = "";
		String MSA_Id = "";
		String msaname = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		// Variables Added to add the common functionality for the View
		// Selector.

		boolean chkOtherOwner = false;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		int ownerListSize = 0;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "appendix";

		if (request.getParameter("Id") != null) {
			formdetail.setAppendix_Id(request.getParameter("Id"));
			formdetail.setId(request.getParameter("Id"));
			formdetail.setMSA_Id(IMdao.getMSAId(formdetail.getId(),
					getDataSource(request, "ilexnewDB")));
			formdetail
					.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
							getDataSource(request, "ilexnewDB"),
							formdetail.getMSA_Id()));
		}

		if (request.getParameter("ref") != null) {
			formdetail.setRef(request.getParameter("ref"));
		}

		if (formdetail.getDelete() != null) {
			Appendix_Id = formdetail.getId();
			int retval = -1;
			String indexvalue = "";

			if (formdetail.getCheck() != null) {
				if (formdetail.getCheck().length > 0) {
					String index[] = new String[formdetail.getCheck().length];
					index = formdetail.getCheck();

					for (int i = 0; i < formdetail.getCheck().length; i++) {
						indexvalue = indexvalue + index[i] + ",";
					}
					indexvalue = indexvalue.substring(0,
							indexvalue.length() - 1);
				}
			}
			retval = Upload.Deleteuploadedfile(indexvalue,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("retval", "" + retval);
		}

		uploadeddocs = Upload.getuploadedfiles("%",
				formdetail.getAppendix_Id(), "Appendix",
				getDataSource(request, "ilexnewDB"));
		filelistlength = "" + uploadeddocs.size();
		formdetail.setName(Appendixdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				formdetail.getAppendix_Id()));
		request.setAttribute("uploadeddocs", uploadeddocs);
		request.setAttribute("filelistlength", filelistlength);
		formdetail.setType("Appendix");

		if (request.getParameter("home") != null) // This is called when reset
													// button is pressed from
													// the view selector.
		{
			formdetail.setAppendixSelectedStatus(null);
			formdetail.setAppendixSelectedOwners(null);
			formdetail.setPrevSelectedStatus("");
			formdetail.setPrevSelectedOwner("");
			formdetail.setAppendixOtherCheck(null);
			formdetail.setMonthWeekCheck(null);
			return (mapping.findForward("appendixlistpage"));
		}

		if (request.getParameter("opendiv") != null) // This is used to make the
														// div remain open when
														// other check box is
														// checked.
			request.setAttribute("opendiv", request.getParameter("opendiv")
					.toString());

		// These two parameter month/week are checked are kept in session. (
		// View Images)
		if (request.getParameter("month") != null) {
			session.setAttribute("monthAppendix", request.getParameter("month")
					.toString());

			session.removeAttribute("weekAppendix");
			formdetail.setMonthWeekCheck("month");
		}

		if (request.getParameter("week") != null) {
			session.setAttribute("weekAppendix", request.getParameter("week")
					.toString());

			session.removeAttribute("monthAppendix");
			formdetail.setMonthWeekCheck("week");
		}

		if (formdetail.getAppendixOtherCheck() != null) // OtherCheck box status
														// is kept in session.
			session.setAttribute("appendixOtherCheck", formdetail
					.getAppendixOtherCheck().toString());

		// When Status Check Boxes are Clicked :::::::::::::::::
		if (formdetail.getAppendixSelectedStatus() != null) {
			for (int i = 0; i < formdetail.getAppendixSelectedStatus().length; i++) {
				selectedStatus = selectedStatus + "," + "'"
						+ formdetail.getAppendixSelectedStatus(i) + "'";
				tempStatus = tempStatus + ","
						+ formdetail.getAppendixSelectedStatus(i);
			}

			selectedStatus = selectedStatus.substring(1,
					selectedStatus.length());
			selectedStatus = "(" + selectedStatus + ")";
		}

		// When Owner Check Boxes are Clicked :::::::::::::::::
		if (formdetail.getAppendixSelectedOwners() != null) {
			for (int j = 0; j < formdetail.getAppendixSelectedOwners().length; j++) {
				selectedOwner = selectedOwner + ","
						+ formdetail.getAppendixSelectedOwners(j);
				tempOwner = tempOwner + ","
						+ formdetail.getAppendixSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if (selectedOwner.equals("0")) {
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" + selectedOwner + ")";
		}

		if (formdetail.getGo() != null && !formdetail.getGo().equals("")) {
			session.setAttribute("appendixSelectedOwners", selectedOwner);
			session.setAttribute("appSelectedOwners", selectedOwner);
			session.setAttribute("appendixTempOwner", tempOwner);
			session.setAttribute("appendixSelectedStatus", selectedStatus);
			session.setAttribute("appSelectedStatus", selectedStatus);
			session.setAttribute("appendixTempStatus", tempStatus);

			if (formdetail.getMonthWeekCheck() != null) {
				if (formdetail.getMonthWeekCheck().equals("week")) {
					session.removeAttribute("monthAppendix");
					session.setAttribute("weekAppendix", "0");

				}
				if (formdetail.getMonthWeekCheck().equals("month")) {
					session.removeAttribute("weekAppendix");
					session.setAttribute("monthAppendix", "0");

				}
				if (formdetail.getMonthWeekCheck().equals("clear")) {
					session.removeAttribute("weekAppendix");
					session.removeAttribute("monthAppendix");

				}

			}
			return (mapping.findForward("appendixlistpage"));
		} else {
			if (session.getAttribute("appendixSelectedOwners") != null) {
				selectedOwner = session.getAttribute("appendixSelectedOwners")
						.toString();
				formdetail.setAppendixSelectedOwners(session
						.getAttribute("appendixTempOwner").toString()
						.split(","));
			}
			if (session.getAttribute("appendixSelectedStatus") != null) {
				selectedStatus = session.getAttribute("appendixSelectedStatus")
						.toString();
				formdetail.setAppendixSelectedStatus(session
						.getAttribute("appendixTempStatus").toString()
						.split(","));
			}
			if (session.getAttribute("appendixOtherCheck") != null)
				formdetail.setAppendixOtherCheck(session.getAttribute(
						"appendixOtherCheck").toString());

			if (session.getAttribute("monthAppendix") != null) {
				session.removeAttribute("weekAppendix");
				formdetail.setMonthWeekCheck("month");
			}
			if (session.getAttribute("weekAppendix") != null) {
				session.removeAttribute("monthAppendix");
				formdetail.setMonthWeekCheck("week");
			}

		}

		if (!chkOtherOwner) {
			formdetail.setPrevSelectedStatus(selectedStatus);
			formdetail.setPrevSelectedOwner(selectedOwner);
		}

		ownerList = MSAdao.getOwnerList(loginuserid,
				formdetail.getAppendixOtherCheck(), ownerType,
				formdetail.getMSA_Id(), getDataSource(request, "ilexnewDB"));
		statusList = MSAdao.getStatusList("pm_appendix",
				getDataSource(request, "ilexnewDB"));

		if (ownerList.size() > 0)
			ownerListSize = ownerList.size();

		request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", statusList);
		request.setAttribute("ownerlist", ownerList);
		request.setAttribute("ownerListSize", ownerListSize + "");

		String appendixStatus = Appendixdao.getAppendixStatus(
				formdetail.getId(), formdetail.getMSA_Id(),
				getDataSource(request, "ilexnewDB"));
		String addendumlist = Appendixdao.getAddendumsIdName(
				formdetail.getId(), getDataSource(request, "ilexnewDB"));
		String addendumid = com.mind.dao.PM.Addendumdao.getLatestAddendumId(
				formdetail.getId(), getDataSource(request, "ilexnewDB"));
		String list = Menu.getStatus("pm_appendix", appendixStatus.charAt(0),
				formdetail.getMSA_Id(), formdetail.getId(), "", "",
				loginuserid, getDataSource(request, "ilexnewDB"));
		String appendix_type = Appendixdao.getAppendixPrType(
				formdetail.getMSA_Id(), formdetail.getAppendix_Id(),
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("appendixType", appendix_type);
		request.setAttribute("appendixStatusList", list);
		request.setAttribute("Appendix_Id", formdetail.getId());
		request.setAttribute("MSA_Id", formdetail.getMSA_Id());
		request.setAttribute("appendixStatus", appendixStatus);
		request.setAttribute("latestaddendumid", addendumid);
		request.setAttribute("addendumlist", addendumlist);

		return mapping.findForward("viewdocumentpage");
	}

	public ActionForward Viewmsadocument(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ViewDocumentForm formdetail = (ViewDocumentForm) form;
		String MSA_Id = "";
		ArrayList uploadeddocs = new ArrayList();
		String name = "";
		String filelistlength = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		// Variables added for the View Selector Functioning.
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "msa";
		String msaid = "0";
		int ownerListSize = 0;

		if (request.getParameter("Id") != null) {
			formdetail.setId(request.getParameter("Id"));
			formdetail.setMSA_Id(request.getParameter("Id"));
			formdetail.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), formdetail.getId()));
		}

		if (request.getParameter("ref") != null) {
			formdetail.setRef(request.getParameter("ref"));
		}

		if (formdetail.getDelete() != null) {
			MSA_Id = formdetail.getId();
			int retval = -1;
			String indexvalue = "";

			if (formdetail.getCheck() != null) {
				if (formdetail.getCheck().length > 0) {
					String index[] = new String[formdetail.getCheck().length];
					index = formdetail.getCheck();

					for (int i = 0; i < formdetail.getCheck().length; i++) {
						indexvalue = indexvalue + index[i] + ",";
					}
					indexvalue = indexvalue.substring(0,
							indexvalue.length() - 1);
				}
			}
			retval = Upload.Deleteuploadedfile(indexvalue,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("retval", "" + retval);
		}

		uploadeddocs = Upload.getuploadedfiles("%", formdetail.getMSA_Id(),
				"MSA", getDataSource(request, "ilexnewDB"));
		filelistlength = "" + uploadeddocs.size();
		formdetail.setName(Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"), formdetail.getMSA_Id()));
		request.setAttribute("uploadeddocs", uploadeddocs);
		request.setAttribute("filelistlength", filelistlength);
		formdetail.setType("MSA");

		if (session.getAttribute("appendixSelectedOwners") != null
				|| session.getAttribute("appendixSelectedStatus") != null)
			request.setAttribute("specialCase", "specialCase");

		if (session.getAttribute("monthAppendix") != null
				|| session.getAttribute("weekAppendix") != null) {
			request.setAttribute("specialCase", "specialCase");
		}

		// Added to refersh the previous Appendix List state::::
		WebUtil.removeAppendixSession(session);
		// This is required for the div to remain open after pressing show.
		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		// The Menu Images are to be set in session respective of one other.
		if (request.getParameter("month") != null) {
			session.setAttribute("monthMSA", request.getParameter("month")
					.toString());
			monthMSA = request.getParameter("month").toString();
			session.removeAttribute("weekMSA");
			formdetail.setMonthWeekCheck("month");
		}

		if (request.getParameter("week") != null) {
			session.setAttribute("weekMSA", request.getParameter("week")
					.toString());
			weekMSA = request.getParameter("week").toString();
			session.removeAttribute("monthMSA");
			formdetail.setMonthWeekCheck("week");
		}

		// When the reset button is pressed remove every thing from the session.
		if (request.getParameter("home") != null) {
			formdetail.setSelectedStatus(null);
			formdetail.setSelectedOwners(null);
			formdetail.setPrevSelectedStatus("");
			formdetail.setPrevSelectedOwner("");
			formdetail.setOtherCheck(null);
			formdetail.setMonthWeekCheck(null);
			// MSAdao.setValuesWhenReset(session);
			return (mapping.findForward("msalistpage"));
		}

		// when the other check box is checked in the view selector.
		if (formdetail.getOtherCheck() != null)
			session.setAttribute("otherCheck", formdetail.getOtherCheck()
					.toString());

		if (formdetail.getSelectedStatus() != null) {
			for (int i = 0; i < formdetail.getSelectedStatus().length; i++) {
				selectedStatus = selectedStatus + "," + "'"
						+ formdetail.getSelectedStatus(i) + "'";
				tempStatus = tempStatus + "," + formdetail.getSelectedStatus(i);
			}

			selectedStatus = selectedStatus.substring(1,
					selectedStatus.length());
			selectedStatus = "(" + selectedStatus + ")";
		}

		if (formdetail.getSelectedOwners() != null) {
			for (int j = 0; j < formdetail.getSelectedOwners().length; j++) {
				selectedOwner = selectedOwner + ","
						+ formdetail.getSelectedOwners(j);
				tempOwner = tempOwner + "," + formdetail.getSelectedOwners(j);
			}
			selectedOwner = selectedOwner.substring(1, selectedOwner.length());
			if (selectedOwner.equals("0")) {
				chkOtherOwner = true;
				selectedOwner = "%";
			}
			selectedOwner = "(" + selectedOwner + ")";
		}

		// To set the values in the session when the Show button is Pressed.
		if (formdetail.getGo() != null && !formdetail.getGo().equals("")) {
			session.setAttribute("selectedOwners", selectedOwner);
			session.setAttribute("MSASelectedOwners", selectedOwner);
			session.setAttribute("tempOwner", tempOwner);
			session.setAttribute("selectedStatus", selectedStatus);
			session.setAttribute("MSASelectedStatus", selectedStatus);
			session.setAttribute("tempStatus", tempStatus);

			if (formdetail.getMonthWeekCheck() != null) {
				if (formdetail.getMonthWeekCheck().equals("week")) {
					session.removeAttribute("monthMSA");
					session.setAttribute("weekMSA", "0");
				}
				if (formdetail.getMonthWeekCheck().equals("month")) {
					session.removeAttribute("weekMSA");
					session.setAttribute("monthMSA", "0");
				}
				if (formdetail.getMonthWeekCheck().equals("clear")) {
					session.removeAttribute("monthMSA");
					session.removeAttribute("weekMSA");
				}

			}

			if (session.getAttribute("monthMSA") != null) {
				session.removeAttribute("weekMSA");
				monthMSA = session.getAttribute("monthMSA").toString();
			}
			if (session.getAttribute("weekMSA") != null) {
				session.removeAttribute("monthMSA");
				weekMSA = session.getAttribute("weekMSA").toString();
			}
			return (mapping.findForward("msalistpage"));
		} else // If not pressed see the values exists in the session or not.
		{
			if (session.getAttribute("selectedOwners") != null) {
				selectedOwner = session.getAttribute("selectedOwners")
						.toString();
				formdetail.setSelectedOwners(session.getAttribute("tempOwner")
						.toString().split(","));
			}
			if (session.getAttribute("selectedStatus") != null) {
				selectedStatus = session.getAttribute("selectedStatus")
						.toString();
				formdetail.setSelectedStatus(session.getAttribute("tempStatus")
						.toString().split(","));
			}
			if (session.getAttribute("otherCheck") != null)
				formdetail.setOtherCheck(session.getAttribute("otherCheck")
						.toString());
		}

		if (session.getAttribute("monthMSA") != null) {
			session.removeAttribute("weekMSA");
			monthMSA = session.getAttribute("monthMSA").toString();
		}
		if (session.getAttribute("weekMSA") != null) {
			session.removeAttribute("monthMSA");
			weekMSA = session.getAttribute("weekMSA").toString();
		}

		if (!chkOtherOwner) {
			formdetail.setPrevSelectedStatus(selectedStatus);
			formdetail.setPrevSelectedOwner(selectedOwner);
		}

		ownerList = MSAdao.getOwnerList(loginuserid,
				formdetail.getOtherCheck(), ownerType, msaid,
				getDataSource(request, "ilexnewDB"));
		statusList = MSAdao.getStatusList("pm_msa",
				getDataSource(request, "ilexnewDB"));

		if (ownerList.size() > 0)
			ownerListSize = ownerList.size();

		request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", statusList);
		request.setAttribute("ownerlist", ownerList);
		request.setAttribute("ownerListSize", ownerListSize + "");

		request.setAttribute("MSA_Id", formdetail.getId());
		String msaStatus = MSAdao.getMSAStatus(formdetail.getId(),
				getDataSource(request, "ilexnewDB"));
		String msaUploadCheck = MSAdao.getMSAUploadCheck(formdetail.getId(),
				getDataSource(request, "ilexnewDB"));
		String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
				formdetail.getId(), "", "", "", loginuserid,
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("list", list);
		request.setAttribute("status", msaStatus);
		request.setAttribute("uploadstatus", msaUploadCheck);

		return mapping.findForward("viewdocumentpage");
	}

	public ActionForward Viewappendixdashboarddocument(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ViewDocumentForm formdetail = (ViewDocumentForm) form;
		String Appendix_Id = "";
		ArrayList uploadeddocs = new ArrayList();
		String name = "";
		String filelistlength = "";

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (request.getParameter("Id") != null) {
			formdetail.setId(request.getParameter("Id"));
			formdetail.setAppendix_Id(request.getParameter("Id"));
		}

		if (request.getParameter("ref") != null) {
			formdetail.setRef(request.getParameter("ref"));
		}

		// for dispatch view of netmedx manager bak button
		if (request.getParameter("nettype") != null) {
			formdetail.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getParameter("viewjobtype") != null) {
			formdetail.setViewjobtype("" + request.getParameter("viewjobtype"));
		}

		if (request.getParameter("ownerId") != null) {
			formdetail.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			formdetail.setOwnerId("%");
		}

		// by hamid
		if (request.getParameter("path") != null) {
			formdetail.setPath(request.getParameter("path"));
			formdetail.setAppendix_Id(request.getParameter("appendix_Id"));
		}

		if (formdetail.getDelete() != null) {
			Appendix_Id = formdetail.getId();
			int retval = -1;
			String indexvalue = "";

			request.setAttribute("appendix_Id", formdetail.getAppendix_Id());
			request.setAttribute("path", formdetail.getPath());

			if (formdetail.getCheck() != null) {
				if (formdetail.getCheck().length > 0) {
					String index[] = new String[formdetail.getCheck().length];
					index = formdetail.getCheck();

					for (int i = 0; i < formdetail.getCheck().length; i++) {
						indexvalue = indexvalue + index[i] + ",";
					}
					indexvalue = indexvalue.substring(0,
							indexvalue.length() - 1);
				}
			}
			retval = Upload.Deleteuploadedfile(indexvalue,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("retval", "" + retval);
		}

		/* Start : Appendix Dashboard View Selector Code */
		if (request.getParameter("fromPage") != null)
			formdetail.setFromPage(request.getParameter("fromPage"));

		if (formdetail.getJobOwnerOtherCheck() != null
				&& !formdetail.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					formdetail.getJobOwnerOtherCheck());
		}

		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", formdetail.getId());
			request.setAttribute("msaId", formdetail.getMSA_Id());
			return mapping.findForward("temppage");
		}

		if (formdetail.getJobSelectedStatus() != null) {
			for (int i = 0; i < formdetail.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ formdetail.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ formdetail.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}

		if (formdetail.getJobSelectedOwners() != null) {
			for (int j = 0; j < formdetail.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ formdetail.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ formdetail.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}

		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame", formdetail.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					formdetail.getJobOwnerOtherCheck());

			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", formdetail.getId());
			request.setAttribute("msaId", formdetail.getMSA_Id());
			return mapping.findForward("temppage");

		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				formdetail.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				formdetail.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				formdetail.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				formdetail.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}

		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				formdetail.getJobOwnerOtherCheck(), "prj_job_new",
				formdetail.getId(), getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));

		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		uploadeddocs = Upload.getuploadedfiles("%",
				formdetail.getAppendix_Id(), "Appendix",
				getDataSource(request, "ilexnewDB"));
		filelistlength = "" + uploadeddocs.size();
		formdetail.setName(Appendixdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				formdetail.getAppendix_Id()));
		request.setAttribute("uploadeddocs", uploadeddocs);
		request.setAttribute("filelistlength", filelistlength);
		formdetail.setType("Appendixdashboard");

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(formdetail.getId(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				formdetail.getId(), getDataSource(request, "ilexnewDB"));
		formdetail.setMSA_Id(IMdao.getMSAId(formdetail.getId(),
				getDataSource(request, "ilexnewDB")));
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("msaId", formdetail.getMSA_Id());
		request.setAttribute("viewjobtype", formdetail.getViewjobtype());
		request.setAttribute("ownerId", formdetail.getOwnerId());
		request.setAttribute("appendixid", formdetail.getId());

		return mapping.findForward("viewdocumentpage");
	}

	public ActionForward Viewjobdashboarddocument(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ViewDocumentForm formdetail = (ViewDocumentForm) form;
		String Job_Id = "";
		ArrayList uploadeddocs = new ArrayList();
		String name = "";
		String filelistlength = "";

		if (request.getParameter("Id") != null) {
			formdetail.setJobId(request.getParameter("Id"));
			formdetail.setId(request.getParameter("Id"));
		}

		if (request.getParameter("ref") != null) {
			formdetail.setRef(request.getParameter("ref"));
		}

		// by hamid
		if (request.getParameter("path") != null) {
			formdetail.setPath(request.getParameter("path"));
			formdetail.setAppendix_Id(request.getParameter("appendix_Id"));

		}

		// for dispatch view of netmedx manager bak button
		if (request.getParameter("nettype") != null) {
			formdetail.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getParameter("viewjobtype") != null) {
			formdetail.setViewjobtype("" + request.getParameter("viewjobtype"));
		}

		if (request.getParameter("ownerId") != null) {
			formdetail.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			formdetail.setOwnerId("%");
		}

		if (formdetail.getDelete() != null) {
			Job_Id = formdetail.getId();
			int retval = -1;
			String indexvalue = "";

			request.setAttribute("appendix_Id", formdetail.getAppendix_Id());
			request.setAttribute("path", formdetail.getPath());

			if (formdetail.getCheck() != null) {
				if (formdetail.getCheck().length > 0) {
					String index[] = new String[formdetail.getCheck().length];
					index = formdetail.getCheck();

					for (int i = 0; i < formdetail.getCheck().length; i++) {
						indexvalue = indexvalue + index[i] + ",";
					}
					indexvalue = indexvalue.substring(0,
							indexvalue.length() - 1);
				}
			}
			retval = Upload.Deleteuploadedfile(indexvalue,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("retval", "" + retval);
		}

		uploadeddocs = Upload.getuploadedfiles("%", formdetail.getJobId(),
				"Job", getDataSource(request, "ilexnewDB"));
		filelistlength = "" + uploadeddocs.size();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		formdetail.setName(Jobdao.getJobname(formdetail.getJobId(),
				getDataSource(request, "ilexnewDB")));
		Map<String, Object> map;

		if (formdetail.getJobId() != null
				&& !formdetail.getJobId().trim().equalsIgnoreCase("")) {

			map = DatabaseUtilityDao.setMenu(formdetail.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		request.setAttribute("uploadeddocs", uploadeddocs);
		request.setAttribute("filelistlength", filelistlength);
		formdetail.setType("Jobdashboard");
		return mapping.findForward("viewdocumentpage");
	}

	public ActionForward downloaddocument(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String file_id = request.getParameter("file_id");

		Fileinfo fileinfo = Upload.getuploadedfiledata(file_id, "", "",
				getDataSource(request, "ilexnewDB"));

		if (fileinfo.getFile_data() != null) {
			response.setContentType("application/x-msdownload");// msword
			response.setHeader("Content-Disposition",
					"attachment;filename=" + fileinfo.getFile_name() + ";size="
							+ fileinfo.getFile_data().length + "");

			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(fileinfo.getFile_data());
			outStream.close();


		}
		return mapping.findForward("viewdocumentpage");
	}
	public ActionForward downloadCertificate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String tech_id = request.getParameter("tech_id");
		String partner_id = request.getParameter("partner_id");
		String fileType = request.getParameter("fileType");
		String file_id = Pvsdao.getFileId(partner_id, tech_id, fileType,
				getDataSource(request, "ilexnewDB"));
		if (file_id != null) {
			Fileinfo fileinfo = Upload.getuploadedfiledata(file_id, "", "",
					getDataSource(request, "ilexnewDB"));
			if (fileinfo.getFile_data() != null) {
				response.setContentType("application/x-msdownload");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + fileinfo.getFile_name()
								+ ";size=" + fileinfo.getFile_data().length
								+ "");
				ServletOutputStream outStream = response.getOutputStream();
				outStream.write(fileinfo.getFile_data());
				outStream.close();
			}
		}
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("No File Uploaded");
		return null;
	}
	public ActionForward downloadPartnerFile(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String partner_id = request.getParameter("partner_id");
		String fileType = request.getParameter("fileType");
		String file_id = Pvsdao.getFileId(partner_id, fileType,
				getDataSource(request, "ilexnewDB"));
		if (file_id != null) {
			Fileinfo fileinfo = Upload.getuploadedfiledata(file_id, "", "",
					getDataSource(request, "ilexnewDB"));
			if (fileinfo.getFile_data() != null) {
				response.setContentType("application/x-msdownload");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + fileinfo.getFile_name()
								+ ";size=" + fileinfo.getFile_data().length
								+ "");
				ServletOutputStream outStream = response.getOutputStream();
				outStream.write(fileinfo.getFile_data());
				outStream.close();
			}
		}
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("No File Uploaded");
		return null;
	}
}