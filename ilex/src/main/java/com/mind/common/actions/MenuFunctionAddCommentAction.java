package com.mind.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.AddComment;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.common.formbean.MenuFunctionAddCommentForm;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.util.WebUtil;

public class MenuFunctionAddCommentAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MenuFunctionAddCommentForm menufunctionaddcommentform = (MenuFunctionAddCommentForm) form;

		String MSA_Id = "";
		String msaname = "";
		String Appendix_Id = "";
		String appendixName = "";
		String Job_Id = "";
		String Activity_Id = "";
		String Resource_Id = "";
		String Type = "";
		String ref = "";
		String temp = "";
		HttpSession session = request.getSession(true);
		String forwardid = "";
		String loginuserid = (String) session.getAttribute("userid");
		NameId idname = null;
		NameId appendixidname = null;
		String activityname = "";
		String from = "";
		String jobid = "";

		// Start: Variables added for the View Selector Functioning.
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "";
		String msaid = "";
		int ownerListSize = 0;
		// End

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobAppStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		if (request.getParameter("Type") != null) {
			Type = request.getParameter("Type");
			temp = Type;
		}

		/* Page Security Start */

		String userid = (String) session.getAttribute("userid");

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (menufunctionaddcommentform.getAuthenticate().equals("")) {

			if (Type.contains("pm_res"))
				temp = "Resource";

			if (Type.equals("prm_appendix"))
				temp = "Appendix";

			if (Type.equals("Jobdashboard"))
				temp = "Job";

			if (!Authenticationdao.getPageSecurity(userid, temp, "Add Comment",
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}

		}

		/* Page Security End */

		if (request.getParameter("MSA_Id") != null) {
			MSA_Id = request.getParameter("MSA_Id");
			msaname = com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), MSA_Id);
			menufunctionaddcommentform.setForwardId(MSA_Id);
		}

		if (request.getParameter("Appendix_Id") != null) {
			Appendix_Id = request.getParameter("Appendix_Id");
			menufunctionaddcommentform.setForwardId(Appendix_Id);
			menufunctionaddcommentform.setMSA_Id(IMdao.getMSAId(
					menufunctionaddcommentform.getForwardId(),
					getDataSource(request, "ilexnewDB")));
		}

		if (request.getParameter("Job_Id") != null) {
			Job_Id = request.getParameter("Job_Id");
			menufunctionaddcommentform.setForwardId(Job_Id);
			menufunctionaddcommentform.setJobName(Jobdao.getJobname(Job_Id,
					getDataSource(request, "ilexnewDB")));
			Appendix_Id = com.mind.dao.PRM.Jobdao.getAppendixid(Job_Id,
					getDataSource(request, "ilexnewDB"));
			appendixName = Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), Appendix_Id);
			MSA_Id = IMdao.getMSAId(Appendix_Id,
					getDataSource(request, "ilexnewDB"));
			msaname = com.mind.dao.PM.Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"), MSA_Id);

		}

		if (request.getParameter("Activity_Id") != null) {
			Activity_Id = request.getParameter("Activity_Id");
			menufunctionaddcommentform.setForwardId(Activity_Id);
		}

		if (request.getParameter("Resource_Id") != null) {
			Resource_Id = request.getParameter("Resource_Id");
			menufunctionaddcommentform.setForwardId(Resource_Id);

		}

		if (request.getParameter("viewjobtype") != null) {
			menufunctionaddcommentform.setViewjobtype(request
					.getParameter("viewjobtype"));
		}

		if (request.getParameter("ownerId") != null) {
			menufunctionaddcommentform.setOwnerId(request
					.getParameter("ownerId"));
		}

		if (request.getParameter("ref") != null) {
			ref = request.getParameter("ref");

		}

		if (request.getParameter("from") != null) {
			from = request.getParameter("from");
			jobid = request.getParameter("jobid");
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("jobid", request.getParameter("jobid"));
			menufunctionaddcommentform
					.setFromflag(request.getParameter("from"));
			menufunctionaddcommentform.setDashboardid(request
					.getParameter("jobid"));
		}

		// Common Code for both MSA And Appendix View Selector.
		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		if (menufunctionaddcommentform.getAdd() != null) {

			int addcommentflag = AddComment.addcomment(
					menufunctionaddcommentform.getMSA_Id(),
					menufunctionaddcommentform.getAppendix_Id(),
					menufunctionaddcommentform.getJob_Id(),
					menufunctionaddcommentform.getActivity_Id(),
					menufunctionaddcommentform.getResource_Id(),
					menufunctionaddcommentform.getType(),
					menufunctionaddcommentform.getComment(), loginuserid,
					getDataSource(request, "ilexnewDB"));

			if (menufunctionaddcommentform.getType().equals("MSA")) {
				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("MSA_Id",
						menufunctionaddcommentform.getMSA_Id());
				return (mapping.findForward("msadetailpage"));
			}

			if (menufunctionaddcommentform.getType().equals("Appendix")) {
				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("MSA_Id",
						menufunctionaddcommentform.getMSA_Id());
				request.setAttribute("Appendix_Id",
						menufunctionaddcommentform.getAppendix_Id());
				return (mapping.findForward("appendixdetailpage"));
			}

			if (menufunctionaddcommentform.getFromflag().equals("jobdashboard")) {
				if (menufunctionaddcommentform.getType().equals("Activity")) {
					request.setAttribute("flag", "" + addcommentflag);
					request.setAttribute("id1",
							menufunctionaddcommentform.getActivity_Id());
					request.setAttribute("id3",
							menufunctionaddcommentform.getAddendum_id());
					request.setAttribute("id2",
							menufunctionaddcommentform.getDashboardid());
					request.setAttribute("ref",
							menufunctionaddcommentform.getRef());
					request.setAttribute("from",
							menufunctionaddcommentform.getFromflag());
					request.setAttribute("type",
							menufunctionaddcommentform.getType());
					return mapping.findForward("activitydetailfromprmpage");
				}

				if (menufunctionaddcommentform.getType().equals("Job")) {
					request.setAttribute("id1",
							menufunctionaddcommentform.getAppendix_Id());
					request.setAttribute("id2",
							menufunctionaddcommentform.getJob_Id());
					request.setAttribute("ref", ref);
					request.setAttribute("id3",
							menufunctionaddcommentform.getAddendum_id());
					request.setAttribute("from",
							menufunctionaddcommentform.getFromflag());
					request.setAttribute("type",
							menufunctionaddcommentform.getType());
					return (mapping.findForward("forwardjobdahboard"));
				}

			}

			if (menufunctionaddcommentform.getType().equals("Job")) {
				request.setAttribute("MSA_Id",
						menufunctionaddcommentform.getMSA_Id());
				request.setAttribute("msaname",
						menufunctionaddcommentform.getMsaname());
				request.setAttribute("Appendix_Id",
						menufunctionaddcommentform.getAppendix_Id());
				request.setAttribute("appendixName",
						menufunctionaddcommentform.getAppendixName());
				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("Appendix_Id",
						menufunctionaddcommentform.getAppendix_Id());
				request.setAttribute("Job_Id",
						menufunctionaddcommentform.getJob_Id());
				request.setAttribute("ref", ref);
				request.setAttribute("addendum_id",
						menufunctionaddcommentform.getAddendum_id());
				return (mapping.findForward("forwardpmcommentpages"));
			}

			if (menufunctionaddcommentform.getType().equals("Jobdashboard")) {
				request.setAttribute("jobid",
						menufunctionaddcommentform.getJob_Id());
				return (mapping.findForward("jobdashboard"));
			}

			if (menufunctionaddcommentform.getType().equals("Activity")) {

				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("Activity_Id",
						menufunctionaddcommentform.getActivity_Id());
				request.setAttribute("addendum_id",
						menufunctionaddcommentform.getAddendum_id());
				request.setAttribute("ref", ref);

				return (mapping.findForward("activitydetailpage"));
			}

			if (menufunctionaddcommentform.getType().equals("pm_resM")) {
				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("Material_Id",
						menufunctionaddcommentform.getResource_Id());
				request.setAttribute("ref", ref);
				request.setAttribute("addendum_id",
						menufunctionaddcommentform.getAddendum_id());
				return (mapping.findForward("materialdetailpage"));
			}

			if (menufunctionaddcommentform.getType().equals("pm_resL")) {
				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("Labor_Id",
						menufunctionaddcommentform.getResource_Id());
				request.setAttribute("ref", ref);
				request.setAttribute("addendum_id",
						menufunctionaddcommentform.getAddendum_id());
				return (mapping.findForward("labordetailpage"));
			}

			if (menufunctionaddcommentform.getType().equals("pm_resF")) {
				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("Freight_Id",
						menufunctionaddcommentform.getResource_Id());
				request.setAttribute("ref", ref);
				request.setAttribute("addendum_id",
						menufunctionaddcommentform.getAddendum_id());
				return (mapping.findForward("freightdetailpage"));
			}

			if (menufunctionaddcommentform.getType().equals("pm_resT")) {
				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("Travel_Id",
						menufunctionaddcommentform.getResource_Id());
				request.setAttribute("ref", ref);
				request.setAttribute("addendum_id",
						menufunctionaddcommentform.getAddendum_id());
				return (mapping.findForward("traveldetailpage"));
			}

			if (menufunctionaddcommentform.getType().equals("prm_appendix")) {
				request.setAttribute("addcommentflag", "" + addcommentflag);
				request.setAttribute("id1",
						menufunctionaddcommentform.getAppendix_Id());
				request.setAttribute("type",
						menufunctionaddcommentform.getType());
				return (mapping.findForward("appendixdahboard"));
			}

			else {
				return (mapping.findForward("errorpage"));
			}
		} else {

			menufunctionaddcommentform.setMSA_Id(MSA_Id);
			menufunctionaddcommentform.setMsaname(msaname);
			menufunctionaddcommentform.setAppendix_Id(Appendix_Id);
			menufunctionaddcommentform.setAppendixName(appendixName);
			menufunctionaddcommentform.setJob_Id(Job_Id);
			menufunctionaddcommentform.setActivity_Id(Activity_Id);
			menufunctionaddcommentform.setResource_Id(Resource_Id);
			menufunctionaddcommentform.setRef(ref);
			menufunctionaddcommentform.setType(Type);

			if (request.getParameter("addendum_id") != null)
				menufunctionaddcommentform.setAddendum_id(request
						.getParameter("addendum_id"));

			if (menufunctionaddcommentform.getType().equals("MSA")) {
				menufunctionaddcommentform.setName(Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						menufunctionaddcommentform.getMSA_Id()));

				ownerType = "msa";
				msaid = "0";

				if (session.getAttribute("appendixSelectedOwners") != null
						|| session.getAttribute("appendixSelectedStatus") != null)
					request.setAttribute("specialCase", "specialCase");

				if (session.getAttribute("monthAppendix") != null
						|| session.getAttribute("weekAppendix") != null) {
					request.setAttribute("specialCase", "specialCase");
				}

				WebUtil.removeAppendixSession(session);
				// when the other check box is checked in the view selector.
				if (menufunctionaddcommentform.getOtherCheck() != null)
					session.setAttribute("otherCheck",
							menufunctionaddcommentform.getOtherCheck()
									.toString());

				if (menufunctionaddcommentform.getSelectedStatus() != null) {
					for (int i = 0; i < menufunctionaddcommentform
							.getSelectedStatus().length; i++) {
						selectedStatus = selectedStatus
								+ ","
								+ "'"
								+ menufunctionaddcommentform
										.getSelectedStatus(i) + "'";
						tempStatus = tempStatus
								+ ","
								+ menufunctionaddcommentform
										.getSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				if (menufunctionaddcommentform.getSelectedOwners() != null) {
					for (int j = 0; j < menufunctionaddcommentform
							.getSelectedOwners().length; j++) {
						selectedOwner = selectedOwner
								+ ","
								+ menufunctionaddcommentform
										.getSelectedOwners(j);
						tempOwner = tempOwner
								+ ","
								+ menufunctionaddcommentform
										.getSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}
				// End of the code.

				// The Menu Images are to be set in session respective of one
				// other.
				if (request.getParameter("month") != null) {
					session.setAttribute("monthMSA",
							request.getParameter("month").toString());
					monthMSA = request.getParameter("month").toString();
					session.removeAttribute("weekMSA");
					menufunctionaddcommentform.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekMSA", request
							.getParameter("week").toString());
					weekMSA = request.getParameter("week").toString();
					session.removeAttribute("monthMSA");
					menufunctionaddcommentform.setMonthWeekCheck("week");
				}

				// When the reset button is pressed remove every thing from the
				// session.
				if (request.getParameter("home") != null) {
					menufunctionaddcommentform.setSelectedStatus(null);
					menufunctionaddcommentform.setSelectedOwners(null);
					menufunctionaddcommentform.setPrevSelectedStatus("");
					menufunctionaddcommentform.setPrevSelectedOwner("");
					menufunctionaddcommentform.setOtherCheck(null);
					menufunctionaddcommentform.setMonthWeekCheck(null);
					// MSAdao.setValuesWhenReset(session); add the function of
					// reset button.
					return (mapping.findForward("msalistpage"));

				}

				// To set the values in the session when the Show button is
				// Pressed.
				if (menufunctionaddcommentform.getGo() != null
						&& !menufunctionaddcommentform.getGo().equals("")) {
					session.setAttribute("selectedOwners", selectedOwner);
					session.setAttribute("MSASelectedOwners", selectedOwner);
					session.setAttribute("tempOwner", tempOwner);
					session.setAttribute("selectedStatus", selectedStatus);
					session.setAttribute("MSASelectedStatus", selectedStatus);
					session.setAttribute("tempStatus", tempStatus);

					if (menufunctionaddcommentform.getMonthWeekCheck() != null) {
						if (menufunctionaddcommentform.getMonthWeekCheck()
								.equals("week")) {
							session.removeAttribute("monthMSA");
							session.setAttribute("weekMSA", "0");
						}
						if (menufunctionaddcommentform.getMonthWeekCheck()
								.equals("month")) {
							session.removeAttribute("weekMSA");
							session.setAttribute("monthMSA", "0");
						}
						if (menufunctionaddcommentform.getMonthWeekCheck()
								.equals("clear")) {
							session.removeAttribute("monthMSA");
							session.removeAttribute("weekMSA");
						}

					}

					if (session.getAttribute("monthMSA") != null) {
						session.removeAttribute("weekMSA");
						monthMSA = session.getAttribute("monthMSA").toString();
					}
					if (session.getAttribute("weekMSA") != null) {
						session.removeAttribute("monthMSA");
						weekMSA = session.getAttribute("weekMSA").toString();
					}
					return (mapping.findForward("msalistpage"));
				} else // If not pressed see the values exists in the session or
						// not.
				{
					if (session.getAttribute("selectedOwners") != null) {
						selectedOwner = session.getAttribute("selectedOwners")
								.toString();
						menufunctionaddcommentform.setSelectedOwners(session
								.getAttribute("tempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("selectedStatus") != null) {
						selectedStatus = session.getAttribute("selectedStatus")
								.toString();
						menufunctionaddcommentform.setSelectedStatus(session
								.getAttribute("tempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("otherCheck") != null)
						menufunctionaddcommentform.setOtherCheck(session
								.getAttribute("otherCheck").toString());
				}

				if (session.getAttribute("monthMSA") != null) {
					session.removeAttribute("weekMSA");
					monthMSA = session.getAttribute("monthMSA").toString();
				}
				if (session.getAttribute("weekMSA") != null) {
					session.removeAttribute("monthMSA");
					weekMSA = session.getAttribute("weekMSA").toString();
				}

				if (!chkOtherOwner) {
					menufunctionaddcommentform
							.setPrevSelectedStatus(selectedStatus);
					menufunctionaddcommentform
							.setPrevSelectedOwner(selectedOwner);
				}

				ownerList = MSAdao.getOwnerList(loginuserid,
						menufunctionaddcommentform.getOtherCheck(), ownerType,
						msaid, getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_msa",
						getDataSource(request, "ilexnewDB"));

				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

				// End of the code for the View Selector Part

			}

			if (menufunctionaddcommentform.getType().equals("Appendix")
					|| menufunctionaddcommentform.getType().equals(
							"prm_appendix")) {
				menufunctionaddcommentform.setName(Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"),
						menufunctionaddcommentform.getAppendix_Id()));
				menufunctionaddcommentform.setMSA_Id(IMdao.getMSAId(
						Appendix_Id, getDataSource(request, "ilexnewDB")));
				ownerType = "appendix";

				if (menufunctionaddcommentform.getType().equals("Appendix")) {

					if (request.getParameter("home") != null) // This is called
																// when reset
																// button is
																// pressed from
																// the view
																// selector.
					{
						menufunctionaddcommentform
								.setAppendixSelectedStatus(null);
						menufunctionaddcommentform
								.setAppendixSelectedOwners(null);
						menufunctionaddcommentform.setPrevSelectedStatus("");
						menufunctionaddcommentform.setPrevSelectedOwner("");
						menufunctionaddcommentform.setAppendixOtherCheck(null);
						menufunctionaddcommentform.setMonthWeekCheck(null);
						return (mapping.findForward("appendixlistpage"));
					}

					// These two parameter month/week are checked are kept in
					// session. ( View Images)
					if (request.getParameter("month") != null) {
						session.setAttribute("monthAppendix", request
								.getParameter("month").toString());

						session.removeAttribute("weekAppendix");
						menufunctionaddcommentform.setMonthWeekCheck("month");
					}

					if (request.getParameter("week") != null) {
						session.setAttribute("weekAppendix", request
								.getParameter("week").toString());

						session.removeAttribute("monthAppendix");
						menufunctionaddcommentform.setMonthWeekCheck("week");
					}

					if (menufunctionaddcommentform.getAppendixOtherCheck() != null) // OtherCheck
																					// box
																					// status
																					// is
																					// kept
																					// in
																					// session.
						session.setAttribute("appendixOtherCheck",
								menufunctionaddcommentform
										.getAppendixOtherCheck().toString());

					// When Status Check Boxes are Clicked :::::::::::::::::
					if (menufunctionaddcommentform.getAppendixSelectedStatus() != null) {
						for (int i = 0; i < menufunctionaddcommentform
								.getAppendixSelectedStatus().length; i++) {
							selectedStatus = selectedStatus
									+ ","
									+ "'"
									+ menufunctionaddcommentform
											.getAppendixSelectedStatus(i) + "'";
							tempStatus = tempStatus
									+ ","
									+ menufunctionaddcommentform
											.getAppendixSelectedStatus(i);
						}

						selectedStatus = selectedStatus.substring(1,
								selectedStatus.length());
						selectedStatus = "(" + selectedStatus + ")";
					}

					// When Owner Check Boxes are Clicked :::::::::::::::::
					if (menufunctionaddcommentform.getAppendixSelectedOwners() != null) {
						for (int j = 0; j < menufunctionaddcommentform
								.getAppendixSelectedOwners().length; j++) {
							selectedOwner = selectedOwner
									+ ","
									+ menufunctionaddcommentform
											.getAppendixSelectedOwners(j);
							tempOwner = tempOwner
									+ ","
									+ menufunctionaddcommentform
											.getAppendixSelectedOwners(j);
						}
						selectedOwner = selectedOwner.substring(1,
								selectedOwner.length());
						if (selectedOwner.equals("0")) {
							chkOtherOwner = true;
							selectedOwner = "%";
						}
						selectedOwner = "(" + selectedOwner + ")";
					}

					if (menufunctionaddcommentform.getGo() != null
							&& !menufunctionaddcommentform.getGo().equals("")) {
						session.setAttribute("appendixSelectedOwners",
								selectedOwner);
						session.setAttribute("appSelectedOwners", selectedOwner);
						session.setAttribute("sessionMSAId",
								menufunctionaddcommentform.getMSA_Id());
						session.setAttribute("appendixTempOwner", tempOwner);
						session.setAttribute("appendixSelectedStatus",
								selectedStatus);
						session.setAttribute("appSelectedStatus",
								selectedStatus);
						session.setAttribute("appendixTempStatus", tempStatus);

						if (menufunctionaddcommentform.getMonthWeekCheck() != null) {
							if (menufunctionaddcommentform.getMonthWeekCheck()
									.equals("week")) {
								session.removeAttribute("monthAppendix");
								session.setAttribute("weekAppendix", "0");

							}
							if (menufunctionaddcommentform.getMonthWeekCheck()
									.equals("month")) {
								session.removeAttribute("weekAppendix");
								session.setAttribute("monthAppendix", "0");

							}
							if (menufunctionaddcommentform.getMonthWeekCheck()
									.equals("clear")) {
								session.removeAttribute("weekAppendix");
								session.removeAttribute("monthAppendix");

							}

						}
						return (mapping.findForward("appendixlistpage"));
					} else {
						if (session.getAttribute("appendixSelectedOwners") != null) {
							selectedOwner = session.getAttribute(
									"appendixSelectedOwners").toString();
							menufunctionaddcommentform
									.setAppendixSelectedOwners(session
											.getAttribute("appendixTempOwner")
											.toString().split(","));
						}
						if (session.getAttribute("appendixSelectedStatus") != null) {
							selectedStatus = session.getAttribute(
									"appendixSelectedStatus").toString();
							menufunctionaddcommentform
									.setAppendixSelectedStatus(session
											.getAttribute("appendixTempStatus")
											.toString().split(","));
						}
						if (session.getAttribute("appendixOtherCheck") != null)
							menufunctionaddcommentform
									.setAppendixOtherCheck(session
											.getAttribute("appendixOtherCheck")
											.toString());

						if (session.getAttribute("monthAppendix") != null) {
							session.removeAttribute("weekAppendix");
							menufunctionaddcommentform
									.setMonthWeekCheck("month");
						}
						if (session.getAttribute("weekAppendix") != null) {
							session.removeAttribute("monthAppendix");
							menufunctionaddcommentform
									.setMonthWeekCheck("week");
						}

					}
					if (!chkOtherOwner) {
						menufunctionaddcommentform
								.setPrevSelectedStatus(selectedStatus);
						menufunctionaddcommentform
								.setPrevSelectedOwner(selectedOwner);
					}
					ownerList = MSAdao.getOwnerList(loginuserid,
							menufunctionaddcommentform.getAppendixOtherCheck(),
							ownerType, menufunctionaddcommentform.getMSA_Id(),
							getDataSource(request, "ilexnewDB"));
					statusList = MSAdao.getStatusList("pm_appendix",
							getDataSource(request, "ilexnewDB"));

					if (ownerList.size() > 0)
						ownerListSize = ownerList.size();

					request.setAttribute("clickShow",
							request.getParameter("clickShow"));
					request.setAttribute("statuslist", statusList);
					request.setAttribute("ownerlist", ownerList);
					request.setAttribute("ownerListSize", ownerListSize + "");
				} else {

					/* Start : Appendix Dashboard View Selector Code */

					if (menufunctionaddcommentform.getJobOwnerOtherCheck() != null
							&& !menufunctionaddcommentform
									.getJobOwnerOtherCheck().equals("")) {
						session.setAttribute("jobOwnerOtherCheck",
								menufunctionaddcommentform
										.getJobOwnerOtherCheck());
					}

					if (request.getParameter("opendiv") != null) {
						request.setAttribute("opendiv",
								request.getParameter("opendiv"));
					}
					if (request.getParameter("resetList") != null
							&& request.getParameter("resetList").equals(
									"something")) {
						WebUtil.setDefaultAppendixDashboardAttribute(session);
						request.setAttribute("type", "AppendixViewSelector");
						request.setAttribute("appendixId",
								menufunctionaddcommentform.getAppendix_Id());
						request.setAttribute("msaId",
								menufunctionaddcommentform.getMSA_Id());

						return mapping.findForward("temppage");

					}

					if (menufunctionaddcommentform.getJobSelectedStatus() != null) {
						for (int i = 0; i < menufunctionaddcommentform
								.getJobSelectedStatus().length; i++) {
							jobSelectedStatus = jobSelectedStatus
									+ ","
									+ "'"
									+ menufunctionaddcommentform
											.getJobSelectedStatus(i) + "'";
							jobTempStatus = jobTempStatus
									+ ","
									+ menufunctionaddcommentform
											.getJobSelectedStatus(i);
						}

						jobSelectedStatus = jobSelectedStatus.substring(1,
								jobSelectedStatus.length());
						jobSelectedStatus = "(" + jobSelectedStatus + ")";
					}

					if (menufunctionaddcommentform.getJobSelectedOwners() != null) {
						for (int j = 0; j < menufunctionaddcommentform
								.getJobSelectedOwners().length; j++) {
							jobSelectedOwner = jobSelectedOwner
									+ ","
									+ menufunctionaddcommentform
											.getJobSelectedOwners(j);
							jobTempOwner = jobTempOwner
									+ ","
									+ menufunctionaddcommentform
											.getJobSelectedOwners(j);
						}
						jobSelectedOwner = jobSelectedOwner.substring(1,
								jobSelectedOwner.length());
						if (jobSelectedOwner.equals("0")) {
							jobSelectedOwner = "%";
						}
						jobSelectedOwner = "(" + jobSelectedOwner + ")";
					}

					if (request.getParameter("showList") != null
							&& request.getParameter("showList").equals(
									"something")) {

						session.setAttribute("jobSelectedOwners",
								jobSelectedOwner);
						session.setAttribute("jobTempOwner", jobTempOwner);
						session.setAttribute("jobSelectedStatus",
								jobSelectedStatus);
						session.setAttribute("jobTempStatus", jobTempStatus);
						session.setAttribute("timeFrame",
								menufunctionaddcommentform
										.getJobMonthWeekCheck());
						session.setAttribute("jobOwnerOtherCheck",
								menufunctionaddcommentform
										.getJobOwnerOtherCheck());

						request.setAttribute("type", "AppendixViewSelector");
						request.setAttribute("appendixId",
								menufunctionaddcommentform.getAppendix_Id());
						request.setAttribute("msaId",
								menufunctionaddcommentform.getMSA_Id());

						return mapping.findForward("temppage");

					} else {
						if (session.getAttribute("jobSelectedOwners") != null) {
							jobSelectedOwner = session.getAttribute(
									"jobSelectedOwners").toString();
							menufunctionaddcommentform
									.setJobSelectedOwners(session
											.getAttribute("jobTempOwner")
											.toString().split(","));
						}
						if (session.getAttribute("jobSelectedStatus") != null) {
							jobSelectedStatus = session.getAttribute(
									"jobSelectedStatus").toString();
							menufunctionaddcommentform
									.setJobSelectedStatus(session
											.getAttribute("jobTempStatus")
											.toString().split(","));
						}
						if (session.getAttribute("jobOwnerOtherCheck") != null) {
							menufunctionaddcommentform
									.setJobOwnerOtherCheck(session
											.getAttribute("jobOwnerOtherCheck")
											.toString());
						}
						if (session.getAttribute("timeFrame") != null) {
							menufunctionaddcommentform
									.setJobMonthWeekCheck(session.getAttribute(
											"timeFrame").toString());
						}
					}

					jobOwnerList = MSAdao.getOwnerList(userid,
							menufunctionaddcommentform.getJobOwnerOtherCheck(),
							"prj_job_new",
							menufunctionaddcommentform.getAppendix_Id(),
							getDataSource(request, "ilexnewDB"));
					jobAppStatusList = MSAdao.getStatusList("prj_job_new",
							getDataSource(request, "ilexnewDB"));

					if (jobOwnerList.size() > 0)
						jobOwnerListSize = jobOwnerList.size();

					request.setAttribute("jobOwnerListSize", jobOwnerListSize
							+ "");
					request.setAttribute("jobAppStatusList", jobAppStatusList);
					request.setAttribute("jobOwnerList", jobOwnerList);
					/* End : Appendix Dashboard View Selector Code */

					String contractDocoumentList = com.mind.dao.PM.Appendixdao
							.getcontractDocoumentMenu(
									menufunctionaddcommentform.getAppendix_Id(),
									getDataSource(request, "ilexnewDB"));
					request.setAttribute("contractDocMenu",
							contractDocoumentList);
					String appendixcurrentstatus = Appendixdao
							.getCurrentstatus(
									menufunctionaddcommentform.getAppendix_Id(),
									getDataSource(request, "ilexnewDB"));
					request.setAttribute("appendixcurrentstatus",
							appendixcurrentstatus);
					request.setAttribute("ownerId",
							menufunctionaddcommentform.getOwnerId());
					request.setAttribute("viewjobtype",
							menufunctionaddcommentform.getViewjobtype());
					request.setAttribute("msaId",
							menufunctionaddcommentform.getMSA_Id());
					request.setAttribute("appendixid",
							menufunctionaddcommentform.getAppendix_Id());
				}

			}

			if (menufunctionaddcommentform.getType().equals("Job")
					|| menufunctionaddcommentform.getType().equals(
							"Jobdashboard")) {
				menufunctionaddcommentform.setName(Activitydao.getJobname(
						getDataSource(request, "ilexnewDB"),
						menufunctionaddcommentform.getJob_Id()));
			}

			if (menufunctionaddcommentform.getType().equals("Activity")) {
				menufunctionaddcommentform.setName(Activitydao.getActivityname(
						menufunctionaddcommentform.getActivity_Id(),
						getDataSource(request, "ilexnewDB")));
			}

			if (menufunctionaddcommentform.getType().equals("pm_resM")) {
				menufunctionaddcommentform
						.setName(ActivityLibrarydao.Getnameforsowassumption(
								menufunctionaddcommentform.getResource_Id(),
								"pm_resM", getDataSource(request, "ilexnewDB")));
				appendixidname = Activitydao.getIdName(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", "Appendix",
						getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setAppendix_Id(appendixidname
						.getId());
				menufunctionaddcommentform.setAppendixName(appendixidname
						.getName());

				idname = Activitydao.getIdName(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", "MSA", getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setMSA_Id(idname.getId());
				menufunctionaddcommentform.setMsaname(idname.getName());

				menufunctionaddcommentform.setJobName(Jobdao.getoplevelname(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", getDataSource(request, "ilexnewDB")));
				menufunctionaddcommentform.setJob_Id(Activitydao.getJobid(
						getDataSource(request, "ilexnewDB"),
						menufunctionaddcommentform.getActivity_Id()));
				activityname = Activitydao.getActivityname(
						menufunctionaddcommentform.getActivity_Id(),
						getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setActivityName(activityname);

			}

			if (menufunctionaddcommentform.getType().equals("pm_resL")) {
				menufunctionaddcommentform
						.setName(ActivityLibrarydao.Getnameforsowassumption(
								menufunctionaddcommentform.getResource_Id(),
								"pm_resL", getDataSource(request, "ilexnewDB")));
				appendixidname = Activitydao.getIdName(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", "Appendix",
						getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setAppendix_Id(appendixidname
						.getId());
				menufunctionaddcommentform.setAppendixName(appendixidname
						.getName());

				idname = Activitydao.getIdName(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", "MSA", getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setMSA_Id(idname.getId());
				menufunctionaddcommentform.setMsaname(idname.getName());

				menufunctionaddcommentform.setJobName(Jobdao.getoplevelname(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", getDataSource(request, "ilexnewDB")));
				menufunctionaddcommentform.setJob_Id(Activitydao.getJobid(
						getDataSource(request, "ilexnewDB"),
						menufunctionaddcommentform.getActivity_Id()));
				activityname = Activitydao.getActivityname(
						menufunctionaddcommentform.getActivity_Id(),
						getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setActivityName(activityname);
			}

			if (menufunctionaddcommentform.getType().equals("pm_resF")) {
				menufunctionaddcommentform
						.setName(ActivityLibrarydao.Getnameforsowassumption(
								menufunctionaddcommentform.getResource_Id(),
								"pm_resF", getDataSource(request, "ilexnewDB")));
				appendixidname = Activitydao.getIdName(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", "Appendix",
						getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setAppendix_Id(appendixidname
						.getId());
				menufunctionaddcommentform.setAppendixName(appendixidname
						.getName());

				idname = Activitydao.getIdName(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", "MSA", getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setMSA_Id(idname.getId());
				menufunctionaddcommentform.setMsaname(idname.getName());

				menufunctionaddcommentform.setJobName(Jobdao.getoplevelname(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", getDataSource(request, "ilexnewDB")));
				menufunctionaddcommentform.setJob_Id(Activitydao.getJobid(
						getDataSource(request, "ilexnewDB"),
						menufunctionaddcommentform.getActivity_Id()));
				activityname = Activitydao.getActivityname(
						menufunctionaddcommentform.getActivity_Id(),
						getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setActivityName(activityname);
			}

			if (menufunctionaddcommentform.getType().equals("pm_resT")) {
				menufunctionaddcommentform
						.setName(ActivityLibrarydao.Getnameforsowassumption(
								menufunctionaddcommentform.getResource_Id(),
								"pm_resT", getDataSource(request, "ilexnewDB")));
				appendixidname = Activitydao.getIdName(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", "Appendix",
						getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setAppendix_Id(appendixidname
						.getId());
				menufunctionaddcommentform.setAppendixName(appendixidname
						.getName());

				idname = Activitydao.getIdName(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", "MSA", getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setMSA_Id(idname.getId());
				menufunctionaddcommentform.setMsaname(idname.getName());

				menufunctionaddcommentform.setJobName(Jobdao.getoplevelname(
						menufunctionaddcommentform.getActivity_Id(),
						"Resource", getDataSource(request, "ilexnewDB")));
				menufunctionaddcommentform.setJob_Id(Activitydao.getJobid(
						getDataSource(request, "ilexnewDB"),
						menufunctionaddcommentform.getActivity_Id()));
				activityname = Activitydao.getActivityname(
						menufunctionaddcommentform.getActivity_Id(),
						getDataSource(request, "ilexnewDB"));
				menufunctionaddcommentform.setActivityName(activityname);
			}

			if (menufunctionaddcommentform.getType().equals("Jobdashboard")) {
				request.setAttribute("appendixname", Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"), IMdao
								.getAppendixId(
										menufunctionaddcommentform.getJob_Id(),
										getDataSource(request, "ilexnewDB"))));
			}

			if (menufunctionaddcommentform.getType().equals("MSA")) {
				request.setAttribute("MSA_Id",
						menufunctionaddcommentform.getForwardId());
				String msaStatus = MSAdao.getMSAStatus(
						menufunctionaddcommentform.getForwardId(),
						getDataSource(request, "ilexnewDB"));
				String msaUploadCheck = MSAdao.getMSAUploadCheck(
						menufunctionaddcommentform.getForwardId(),
						getDataSource(request, "ilexnewDB"));
				String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
						menufunctionaddcommentform.getForwardId(), "", "", "",
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("list", list);
				request.setAttribute("status", msaStatus);
				request.setAttribute("uploadstatus", msaUploadCheck);
			}

			if (menufunctionaddcommentform.getType().equals("Appendix")) {
				String appendixStatus = Appendixdao.getAppendixStatus(
						menufunctionaddcommentform.getForwardId(),
						menufunctionaddcommentform.getMSA_Id(),
						getDataSource(request, "ilexnewDB"));
				String addendumlist = Appendixdao.getAddendumsIdName(
						menufunctionaddcommentform.getForwardId(),
						getDataSource(request, "ilexnewDB"));
				String addendumid = com.mind.dao.PM.Addendumdao
						.getLatestAddendumId(
								menufunctionaddcommentform.getForwardId(),
								getDataSource(request, "ilexnewDB"));
				String appendixType = Appendixdao.getAppendixPrType(
						menufunctionaddcommentform.getMSA_Id(),
						menufunctionaddcommentform.getAppendix_Id(),
						getDataSource(request, "ilexnewDB"));
				String list = Menu.getStatus("pm_appendix",
						appendixStatus.charAt(0),
						menufunctionaddcommentform.getMSA_Id(),
						menufunctionaddcommentform.getForwardId(), "", "",
						loginuserid, getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixStatusList", list);
				request.setAttribute("Appendix_Id",
						menufunctionaddcommentform.getForwardId());
				request.setAttribute("MSA_Id",
						menufunctionaddcommentform.getMSA_Id());
				request.setAttribute("appendixStatus", appendixStatus);
				request.setAttribute("latestaddendumid", addendumid);
				request.setAttribute("addendumlist", addendumlist);
				request.setAttribute("appendixType", appendixType);
			}

			if (menufunctionaddcommentform.getType().equals("Job")) {
				String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
						menufunctionaddcommentform.getForwardId(), "%", "%",
						getDataSource(request, "ilexnewDB"));
				String jobStatus = jobStatusAndType[0];
				String jobType1 = jobStatusAndType[1];
				String jobStatusList = "";
				String appendixType = "";

				if (jobType1.equals("Default"))
					jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
							+ menufunctionaddcommentform.getForwardId()
							+ "&Status='D'\"" + "," + "0";
				if (jobType1.equals("Newjob"))
					jobStatusList = Menu.getStatus("pm_job",
							jobStatus.charAt(0), "", "",
							menufunctionaddcommentform.getForwardId(), "",
							loginuserid, getDataSource(request, "ilexnewDB"));
				if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
					jobStatusList = "\"Approved\""
							+ ","
							+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
							+ menufunctionaddcommentform.getForwardId()
							+ "&Status=A\"" + "," + "0";
				if (jobType1.equals("inscopejob")
						&& jobStatus.equals("Approved"))
					jobStatusList = "\"Draft\""
							+ ","
							+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
							+ menufunctionaddcommentform.getForwardId()
							+ "&Status=D\"" + "," + "0";
				if (jobType1.equals("Addendum"))
					jobStatusList = Menu.getStatus("Addendum",
							jobStatus.charAt(0), "", "",
							menufunctionaddcommentform.getForwardId(), "",
							loginuserid, getDataSource(request, "ilexnewDB"));

				appendixType = ViewList.getAppendixtypedesc(
						menufunctionaddcommentform.getAppendix_Id(),
						getDataSource(request, "ilexnewDB"));

				request.setAttribute("jobStatus", jobStatus);
				request.setAttribute("job_type", jobType1);
				request.setAttribute("appendixtype", appendixType);
				request.setAttribute("Job_Id",
						menufunctionaddcommentform.getForwardId());
				request.setAttribute("Appendix_Id",
						menufunctionaddcommentform.getAppendix_Id());
				request.setAttribute("jobStatusList", jobStatusList);
				request.setAttribute("addendum_id", "0");

				if (jobType1.equals("Newjob")) {
					request.setAttribute("chkadd", "detailjob");
					request.setAttribute("chkaddendum", "detailjob");
					request.setAttribute("chkaddendumactivity", "View");
				}
				if (jobType1.equals("inscopejob")
						|| jobType1.equals("addendum")) {
					request.setAttribute("chkadd", "inscopejob");
					request.setAttribute("chkaddendum", "inscopejob");
					request.setAttribute("chkaddendumactivity",
							"inscopeactivity");
				}

				if (jobType1.equals("Default")
						&& menufunctionaddcommentform.getType()
								.equalsIgnoreCase("Jobdashboard")) {
					request.setAttribute("chkadd", "inscopejob");
					request.setAttribute("chkaddendum", "inscopejob");
					request.setAttribute("chkaddendumactivity",
							"inscopeactivity");
				}

				if (jobType1.equals("Default")
						&& !menufunctionaddcommentform.getType()
								.equalsIgnoreCase("Jobdashboard")) {
					request.setAttribute("chkadd", "detailjob");
					request.setAttribute("chkaddendum", "detailjob");
					request.setAttribute("chkaddendumactivity", "View");
				}

			}

			if (menufunctionaddcommentform.getType().equals("Activity")) {

				String jobtype = "";
				String activityStatus = "";
				String activityType = "";
				String activityStatusList = "";
				String MSA_Name = "";
				int checkNetmedXappendix = 0;
				String[] activityStatusAndType = Activitydao
						.getActivityTypeStatus(
								menufunctionaddcommentform.getActivity_Id(),
								getDataSource(request, "ilexnewDB"));
				activityStatus = activityStatusAndType[0];
				activityType = activityStatusAndType[1];

				if (request.getParameter("from") != null) {
					from = request.getParameter("from");
					jobid = request.getParameter("jobid");
					request.setAttribute("from", request.getParameter("from"));
					request.setAttribute("jobid", request.getParameter("jobid"));
				}

				jobtype = Jobdao.getJobtype(
						menufunctionaddcommentform.getJob_Id(),
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("jobtype", jobtype);

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("view")
						|| menufunctionaddcommentform.getRef()
								.equalsIgnoreCase("detailactivity")) {
					request.setAttribute("chkaddendum", "detailactivity");
					request.setAttribute("chkaddendumresource", "View");

					if (request.getParameter("from") != null) {
						if (jobtype.equals("Addendum")
								|| jobtype.equals("Default")) {
							activityStatusList = Menu
									.getStatus("pm_act", activityStatus
											.charAt(0), from, jobid,
											menufunctionaddcommentform
													.getActivity_Id(),
											"detailactivity", loginuserid,
											getDataSource(request, "ilexnewDB"));
						} else {
							activityStatus = JobDashboarddao
									.getActivityprojectstatus(
											menufunctionaddcommentform
													.getActivity_Id(),
											getDataSource(request, "ilexnewDB"));
							activityStatusList = Menu
									.getStatus("prj_act", activityStatus
											.charAt(0), from, jobid,
											menufunctionaddcommentform
													.getActivity_Id(),
											"detailactivity", loginuserid,
											getDataSource(request, "ilexnewDB"));
						}
					} else {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), "", "",
								menufunctionaddcommentform.getActivity_Id(),
								"detailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));

					}

				}

				if (menufunctionaddcommentform.getRef().equalsIgnoreCase(
						"inscopeactivity")
						|| menufunctionaddcommentform.getRef()
								.equalsIgnoreCase("inscopedetailactivity")) {
					request.setAttribute("chkaddendum", "inscopedetailactivity");
					request.setAttribute("chkaddendumresource", "Viewinscope");

					if (request.getParameter("from") != null) {

						if (jobtype.equals("Addendum")
								|| jobtype.equals("Default")) {
							activityStatusList = Menu
									.getStatus("pm_act", activityStatus
											.charAt(0), from, jobid,
											menufunctionaddcommentform
													.getActivity_Id(),
											"inscopedetailactivity",
											loginuserid,
											getDataSource(request, "ilexnewDB"));
						} else {
							activityStatus = JobDashboarddao
									.getActivityprojectstatus(
											menufunctionaddcommentform
													.getActivity_Id(),
											getDataSource(request, "ilexnewDB"));
							activityStatusList = Menu
									.getStatus("prj_act", activityStatus
											.charAt(0), from, jobid,
											menufunctionaddcommentform
													.getActivity_Id(),
											"inscopedetailactivity",
											loginuserid,
											getDataSource(request, "ilexnewDB"));
						}
					} else {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), "", "",
								menufunctionaddcommentform.getActivity_Id(),
								"inscopedetailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));

					}

				}

				MSA_Id = Activitydao.getmsaid(
						getDataSource(request, "ilexnewDB"),
						menufunctionaddcommentform.getJob_Id());
				MSA_Name = Activitydao.getMsaname(
						getDataSource(request, "ilexnewDB"), MSA_Id);
				request.setAttribute("MSA_Name", MSA_Name);
				request.setAttribute("MSA_Id", MSA_Id);

				checkNetmedXappendix = Activitydao.checkAppendixname(
						menufunctionaddcommentform.getAppendix_Id(),
						menufunctionaddcommentform.getJob_Id(),
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("checknetmedx", checkNetmedXappendix + "");
				request.setAttribute("activityStatusList", activityStatusList);
				request.setAttribute("Activity_Id",
						menufunctionaddcommentform.getActivity_Id());
				request.setAttribute("Job_Id",
						menufunctionaddcommentform.getJob_Id());
				request.setAttribute("jobtype", jobtype);
				request.setAttribute("from", from);
				request.setAttribute("jobid", jobid);
				request.setAttribute("activityStatus", activityStatus);
				request.setAttribute("activityType", activityType);
				request.setAttribute("activityTypeForJob",
						menufunctionaddcommentform.getRef());

			}

			if (menufunctionaddcommentform.getType().equals("pm_resM")) {
				// For the Material Menu
				String materialType = "";
				String materialStatus = "";
				String materialStatusList = "";

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("View"))
					materialType = "M";
				else
					materialType = "IM";

				materialStatus = Resourcedao.getResourceStatus(
						menufunctionaddcommentform.getActivity_Id(),
						menufunctionaddcommentform.getResource_Id(),
						materialType, request.getSession().getId(),
						getDataSource(request, "ilexnewDB"));

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("View")) {
					request.setAttribute("chkaddendum", "View");
					request.setAttribute("chkdetailactivity", "detailactivity");

					if (request.getParameter("from") != null)
						materialStatusList = Menu.getStatus(
								"jobdashboard_resM", materialStatus.charAt(0),
								from, request.getParameter("jobid"), "View",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					else
						materialStatusList = Menu.getStatus("pm_resM",
								materialStatus.charAt(0), "", "", "",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));

				}

				if (menufunctionaddcommentform.getRef().equalsIgnoreCase(
						"Viewinscope")) {
					request.setAttribute("chkaddendum", "Viewinscope");
					request.setAttribute("chkdetailactivity",
							"inscopedetailactivity");

					if (request.getParameter("from") != null)
						materialStatusList = Menu.getStatus(
								"jobdashboard_resM", materialStatus.charAt(0),
								from, request.getParameter("jobid"),
								"Viewinscope",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					else
						materialStatusList = Menu.getStatus(
								"jobdashboard_resM", materialStatus.charAt(0),
								"", "", "Viewinscope",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
				}

				request.setAttribute("Activity_Id",
						menufunctionaddcommentform.getActivity_Id());
				request.setAttribute("Material_Id",
						menufunctionaddcommentform.getResource_Id());
				request.setAttribute("commonResourceStatus", materialStatus);
				request.setAttribute("materialStatusList", materialStatusList);
				request.setAttribute("checkingRef",
						menufunctionaddcommentform.getRef());
				request.setAttribute("addendum_id", "0");

				/*
				 * System.out.println("Activity_Id ::: " +
				 * menufunctionaddcommentform.getActivity_Id());
				 * System.out.println("Material_Id ::: " +
				 * menufunctionaddcommentform.getResource_Id());
				 * System.out.println("Material Status ::: " + materialStatus);
				 * System.out.println("Material Status list ::: " +
				 * materialStatusList); System.out.println("Addendum Id ::: " +
				 * "0"); System.out.println("From  ::::" + from );
				 * System.out.println("job id ::::" + jobid );
				 */
				// End of the Menu Code for Resource Material
			}

			if (menufunctionaddcommentform.getType().equals("pm_resL")) {

				// For the Material Menu
				String laborType = "";
				String laborStatus = "";
				String laborStatusList = "";

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("View"))
					laborType = "L";
				else
					laborType = "IL";

				laborStatus = Resourcedao.getResourceStatus(
						menufunctionaddcommentform.getActivity_Id(),
						menufunctionaddcommentform.getResource_Id(), laborType,
						request.getSession().getId(),
						getDataSource(request, "ilexnewDB"));

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("View")) {
					request.setAttribute("chkaddendum", "View");
					request.setAttribute("chkdetailactivity", "detailactivity");

					if (request.getParameter("from") != null)
						laborStatusList = Menu.getStatus("jobdashboard_resL",
								laborStatus.charAt(0), from,
								request.getParameter("jobid"), "View",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					else
						laborStatusList = Menu.getStatus("pm_resL",
								laborStatus.charAt(0), "", "", "",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));

				}

				if (menufunctionaddcommentform.getRef().equalsIgnoreCase(
						"Viewinscope")) {
					request.setAttribute("chkaddendum", "Viewinscope");
					request.setAttribute("chkdetailactivity",
							"inscopedetailactivity");

					if (request.getParameter("from") != null)
						laborStatusList = Menu.getStatus("jobdashboard_resL",
								laborStatus.charAt(0), from,
								request.getParameter("jobid"), "Viewinscope",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					else
						laborStatusList = Menu.getStatus("jobdashboard_resL",
								laborStatus.charAt(0), "", "", "Viewinscope",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
				}

				request.setAttribute("Activity_Id",
						menufunctionaddcommentform.getActivity_Id());
				request.setAttribute("Labor_Id",
						menufunctionaddcommentform.getResource_Id());
				request.setAttribute("commonResourceStatus", laborStatus);
				request.setAttribute("laborStatusList", laborStatusList);
				request.setAttribute("checkingRef",
						menufunctionaddcommentform.getRef());
				request.setAttribute("addendum_id", "0");

				/*
				 * System.out.println("Activity_Id ::: " +
				 * menufunctionaddcommentform.getActivity_Id());
				 * System.out.println("Labor_Id ::: " +
				 * menufunctionaddcommentform.getResource_Id());
				 * System.out.println("labor Status ::: " + laborStatus);
				 * System.out.println("labor Status list ::: " +
				 * laborStatusList); System.out.println("Addendum Id ::: " +
				 * "0"); System.out.println("From  ::::" + from );
				 * System.out.println("job id ::::" + jobid );
				 */
				// End of the Menu Code for Resource Labor

			}

			if (menufunctionaddcommentform.getType().equals("pm_resF")) {

				// For the Freight Menu
				String freightType = "";
				String freightStatus = "";
				String freightStatusList = "";

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("View"))
					freightType = "F";
				else
					freightType = "IF";

				freightStatus = Resourcedao.getResourceStatus(
						menufunctionaddcommentform.getActivity_Id(),
						menufunctionaddcommentform.getResource_Id(),
						freightType, request.getSession().getId(),
						getDataSource(request, "ilexnewDB"));

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("View")) {
					request.setAttribute("chkaddendum", "View");
					request.setAttribute("chkdetailactivity", "detailactivity");

					if (request.getParameter("from") != null)
						freightStatusList = Menu.getStatus("jobdashboard_resF",
								freightStatus.charAt(0), from,
								request.getParameter("jobid"), "View",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					else
						freightStatusList = Menu.getStatus("pm_resF",
								freightStatus.charAt(0), "", "", "",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));

				}

				if (menufunctionaddcommentform.getRef().equalsIgnoreCase(
						"Viewinscope")) {
					request.setAttribute("chkaddendum", "Viewinscope");
					request.setAttribute("chkdetailactivity",
							"inscopedetailactivity");

					if (request.getParameter("from") != null)
						freightStatusList = Menu.getStatus("jobdashboard_resF",
								freightStatus.charAt(0), from,
								request.getParameter("jobid"), "Viewinscope",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					else
						freightStatusList = Menu.getStatus("jobdashboard_resF",
								freightStatus.charAt(0), "", "", "Viewinscope",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
				}

				request.setAttribute("Activity_Id",
						menufunctionaddcommentform.getActivity_Id());
				request.setAttribute("Freight_Id",
						menufunctionaddcommentform.getResource_Id());
				request.setAttribute("commonResourceStatus", freightStatus);
				request.setAttribute("freightStatusList", freightStatusList);
				request.setAttribute("checkingRef",
						menufunctionaddcommentform.getRef());
				request.setAttribute("addendum_id", "0");

				/*
				 * System.out.println("Activity_Id ::: " +
				 * menufunctionaddcommentform.getActivity_Id());
				 * System.out.println("Freight_Id ::: " +
				 * menufunctionaddcommentform.getResource_Id());
				 * System.out.println("Freight Status ::: " + freightStatus);
				 * System.out.println("Freight Status list ::: " +
				 * freightStatusList); System.out.println("Addendum Id ::: " +
				 * "0"); System.out.println("From  ::::" + from );
				 * System.out.println("job id ::::" + jobid );
				 */
				// End of the Menu Code for Resource Freight

			}

			if (menufunctionaddcommentform.getType().equals("pm_resT")) {

				// For the Travel Menu
				String travelType = "";
				String travelStatus = "";
				String travelStatusList = "";

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("View"))
					travelType = "T";
				else
					travelType = "IT";

				travelStatus = Resourcedao.getResourceStatus(
						menufunctionaddcommentform.getActivity_Id(),
						menufunctionaddcommentform.getResource_Id(),
						travelType, request.getSession().getId(),
						getDataSource(request, "ilexnewDB"));

				if (menufunctionaddcommentform.getRef()
						.equalsIgnoreCase("View")) {
					request.setAttribute("chkaddendum", "View");
					request.setAttribute("chkdetailactivity", "detailactivity");

					if (request.getParameter("from") != null)
						travelStatusList = Menu.getStatus("jobdashboard_resT",
								travelStatus.charAt(0), from,
								request.getParameter("jobid"), "View",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					else
						travelStatusList = Menu.getStatus("pm_resT",
								travelStatus.charAt(0), "", "", "",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));

				}

				if (menufunctionaddcommentform.getRef().equalsIgnoreCase(
						"Viewinscope")) {
					request.setAttribute("chkaddendum", "Viewinscope");
					request.setAttribute("chkdetailactivity",
							"inscopedetailactivity");

					if (request.getParameter("from") != null)
						travelStatusList = Menu.getStatus("jobdashboard_resT",
								travelStatus.charAt(0), from,
								request.getParameter("jobid"), "Viewinscope",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
					else
						travelStatusList = Menu.getStatus("jobdashboard_resT",
								travelStatus.charAt(0), "", "", "Viewinscope",
								menufunctionaddcommentform.getResource_Id(),
								loginuserid,
								getDataSource(request, "ilexnewDB"));
				}

				request.setAttribute("Activity_Id",
						menufunctionaddcommentform.getActivity_Id());
				request.setAttribute("Travel_Id",
						menufunctionaddcommentform.getResource_Id());
				request.setAttribute("commonResourceStatus", travelStatus);
				request.setAttribute("travelStatusList", travelStatusList);
				request.setAttribute("checkingRef",
						menufunctionaddcommentform.getRef());
				request.setAttribute("addendum_id", "0");

				/*
				 * System.out.println("Activity_Id ::: " +
				 * menufunctionaddcommentform.getActivity_Id());
				 * System.out.println("Travel_Id ::: " +
				 * menufunctionaddcommentform.getResource_Id());
				 * System.out.println("Travel Status ::: " + travelStatus);
				 * System.out.println("Travel Status list ::: " +
				 * travelStatusList); System.out.println("Addendum Id ::: " +
				 * "0"); System.out.println("From  ::::" + from );
				 * System.out.println("job id ::::" + jobid );
				 */
				// End of the Menu Code for Resource Travel

			}

			request.setAttribute("checkingRef",
					menufunctionaddcommentform.getRef());
			return (mapping.findForward("commentpage"));
		}
	}
}