package com.mind.common.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.UploadBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.Upload;
import com.mind.common.dao.ViewList;
import com.mind.common.formbean.UploadForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class UploadAction extends com.mind.common.IlexDispatchAction {
	public ActionForward FromJob(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		UploadForm uploadform = (UploadForm) form;
		UploadBean uploadBean = new UploadBean();
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		int uploadflag = -1;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "MSA", "Upload MSA",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (uploadform.getUpload() != null) {

			uploadBean.setIn(uploadform.getFilename().getInputStream());
			uploadBean.setFileSize(uploadform.getFilename().getFileSize());
			uploadBean.setFileName(uploadform.getFilename().getFileName());
			uploadflag = Upload.fileupload(uploadBean, "Job", userid,
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("type", uploadform.getType());
			request.setAttribute("uploadflag", uploadflag + "");
			request.setAttribute("Id1", uploadform.getId1());
			request.setAttribute("ref", uploadform.getRef1());
			request.setAttribute("addendum_id", uploadform.getAddendum_id());
			return (mapping.findForward("tempuploadpage"));
		}

		else {
			uploadform.setType("Job");
			uploadform.setRef1(request.getParameter("ref1"));
			uploadform.setAddendum_id(request.getParameter("addendum_id"));

			if (request.getParameter("Id") != null) {
				uploadform.setId1(request.getParameter("Id"));
				uploadform.setJobName(Activitydao.getJobname(
						getDataSource(request, "ilexnewDB"),
						uploadform.getId1()));
				uploadform.setAppendix_Id(com.mind.dao.PRM.Jobdao
						.getAppendixid(uploadform.getId1(),
								getDataSource(request, "ilexnewDB")));
				uploadform.setAppendixName(Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"),
						uploadform.getAppendix_Id()));
				uploadform.setMSA_Id(IMdao.getMSAId(
						uploadform.getAppendix_Id(),
						getDataSource(request, "ilexnewDB")));
				uploadform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						uploadform.getMSA_Id()));

			}

			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					uploadform.getId1(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String jobStatusList = "";
			String appendixType = "";

			if (jobType1.equals("Default"))
				jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
						+ uploadform.getId1() + "&Status='D'\"" + "," + "0";
			if (jobType1.equals("Newjob"))
				jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
						"", "", uploadform.getId1(), "", userid,
						getDataSource(request, "ilexnewDB"));
			if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
				jobStatusList = "\"Approved\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ uploadform.getId1() + "&Status=A\"" + "," + "0";
			if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
				jobStatusList = "\"Draft\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ uploadform.getId1() + "&Status=D\"" + "," + "0";
			if (jobType1.equals("Addendum"))
				jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
						"", "", uploadform.getId1(), "", userid,
						getDataSource(request, "ilexnewDB"));

			appendixType = ViewList.getAppendixtypedesc(
					uploadform.getAppendix_Id(),
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("jobStatus", jobStatus);
			request.setAttribute("job_type", jobType1);
			request.setAttribute("appendixtype", appendixType);
			request.setAttribute("Job_Id", uploadform.getId1());
			request.setAttribute("Appendix_Id", uploadform.getAppendix_Id());
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("addendum_id", "0");

			if (jobType1.equals("Newjob")) {
				request.setAttribute("chkadd", "detailjob");
				request.setAttribute("chkaddendum", "detailjob");
				request.setAttribute("chkaddendumactivity", "View");
			}
			if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
				request.setAttribute("chkadd", "inscopejob");
				request.setAttribute("chkaddendum", "inscopejob");
				request.setAttribute("chkaddendumactivity", "inscopeactivity");
			}

			return (mapping.findForward("fileuploadwindow"));
		}
	}

	public ActionForward FromMSA(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		UploadForm uploadform = (UploadForm) form;
		UploadBean uploadBean = new UploadBean();
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		int uploadflag = -1;
		char status = 'A';
		String orgStatus = "";
		String loginuserid = (String) session.getAttribute("userid");

		// Variables added for the View Selector Functioning.
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "msa";
		String msaid = "0";
		int ownerListSize = 0;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "MSA", "Upload MSA",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (uploadform.getUpload() != null) {
			uploadBean.setIn(uploadform.getFilename().getInputStream());
			uploadBean.setFileSize(uploadform.getFilename().getFileSize());
			uploadBean.setFileName(uploadform.getFilename().getFileName());
			uploadflag = Upload.fileupload(uploadBean, "MSA", userid,
					getDataSource(request, "ilexnewDB"));

			// request.setAttribute( "type" , uploadform.getType() );
			request.setAttribute("uploadflag", uploadflag + "");
			request.setAttribute("MSA_Id", uploadform.getId1());
			return (mapping.findForward("msadetailpage"));
		}

		else {
			uploadform.setType("MSA");
			if (request.getParameter("Status") != null) {
				status = request.getParameter("Status").charAt(0);
				orgStatus = orgStatus + status;
				uploadform.setStatus(orgStatus);
			}
			uploadform.setStatuslist(Upload.getNextstatuslist("pm_msa",
					uploadform.getStatus().charAt(0), userid,
					getDataSource(request, "ilexnewDB")));

			if (request.getParameter("Id") != null) {
				uploadform.setId1(request.getParameter("Id"));
				uploadform.setMsaname(Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						uploadform.getId1()));
			}

			if (session.getAttribute("appendixSelectedOwners") != null
					|| session.getAttribute("appendixSelectedStatus") != null)
				request.setAttribute("specialCase", "specialCase");

			if (session.getAttribute("monthAppendix") != null
					|| session.getAttribute("weekAppendix") != null) {
				request.setAttribute("specialCase", "specialCase");
			}
			// Added to refersh the previous Appendix List state::::
			WebUtil.removeAppendixSession(session);
			// This is required for the div to remain open after pressing show.
			if (request.getParameter("opendiv") != null) {
				opendiv = request.getParameter("opendiv");
				request.setAttribute("opendiv", opendiv);
			}

			// The Menu Images are to be set in session respective of one other.
			if (request.getParameter("month") != null) {
				session.setAttribute("monthMSA", request.getParameter("month")
						.toString());
				monthMSA = request.getParameter("month").toString();
				session.removeAttribute("weekMSA");
				uploadform.setMonthWeekCheck("month");
			}

			if (request.getParameter("week") != null) {
				session.setAttribute("weekMSA", request.getParameter("week")
						.toString());
				weekMSA = request.getParameter("week").toString();
				session.removeAttribute("monthMSA");
				uploadform.setMonthWeekCheck("week");
			}

			// When the reset button is pressed remove every thing from the
			// session.
			if (request.getParameter("home") != null) {
				uploadform.setSelectedStatus(null);
				uploadform.setSelectedOwners(null);
				uploadform.setPrevSelectedStatus("");
				uploadform.setPrevSelectedOwner("");
				uploadform.setOtherCheck(null);
				uploadform.setMonthWeekCheck(null);
				// MSAdao.setValuesWhenReset(session);
				return (mapping.findForward("msalistpage"));
			}

			// when the other check box is checked in the view selector.
			if (uploadform.getOtherCheck() != null)
				session.setAttribute("otherCheck", uploadform.getOtherCheck()
						.toString());

			if (uploadform.getSelectedStatus() != null) {
				for (int i = 0; i < uploadform.getSelectedStatus().length; i++) {
					selectedStatus = selectedStatus + "," + "'"
							+ uploadform.getSelectedStatus(i) + "'";
					tempStatus = tempStatus + ","
							+ uploadform.getSelectedStatus(i);
				}

				selectedStatus = selectedStatus.substring(1,
						selectedStatus.length());
				selectedStatus = "(" + selectedStatus + ")";
			}

			if (uploadform.getSelectedOwners() != null) {
				for (int j = 0; j < uploadform.getSelectedOwners().length; j++) {
					selectedOwner = selectedOwner + ","
							+ uploadform.getSelectedOwners(j);
					tempOwner = tempOwner + ","
							+ uploadform.getSelectedOwners(j);
				}
				selectedOwner = selectedOwner.substring(1,
						selectedOwner.length());
				if (selectedOwner.equals("0")) {
					chkOtherOwner = true;
					selectedOwner = "%";
				}
				selectedOwner = "(" + selectedOwner + ")";
			}

			// To set the values in the session when the Show button is Pressed.
			if (uploadform.getGo() != null && !uploadform.getGo().equals("")) {
				session.setAttribute("selectedOwners", selectedOwner);
				session.setAttribute("MSASelectedOwners", selectedOwner);
				session.setAttribute("tempOwner", tempOwner);
				session.setAttribute("selectedStatus", selectedStatus);
				session.setAttribute("MSASelectedStatus", selectedStatus);
				session.setAttribute("tempStatus", tempStatus);

				if (uploadform.getMonthWeekCheck() != null) {
					if (uploadform.getMonthWeekCheck().equals("week")) {
						session.removeAttribute("monthMSA");
						session.setAttribute("weekMSA", "0");
					}
					if (uploadform.getMonthWeekCheck().equals("month")) {
						session.removeAttribute("weekMSA");
						session.setAttribute("monthMSA", "0");
					}
					if (uploadform.getMonthWeekCheck().equals("clear")) {
						session.removeAttribute("monthMSA");
						session.removeAttribute("weekMSA");
					}

				}

				if (session.getAttribute("monthMSA") != null) {
					session.removeAttribute("weekMSA");
					monthMSA = session.getAttribute("monthMSA").toString();
				}
				if (session.getAttribute("weekMSA") != null) {
					session.removeAttribute("monthMSA");
					weekMSA = session.getAttribute("weekMSA").toString();
				}
				return (mapping.findForward("msalistpage"));
			} else // If not pressed see the values exists in the session or
					// not.
			{
				if (session.getAttribute("selectedOwners") != null) {
					selectedOwner = session.getAttribute("selectedOwners")
							.toString();
					uploadform.setSelectedOwners(session
							.getAttribute("tempOwner").toString().split(","));
				}
				if (session.getAttribute("selectedStatus") != null) {
					selectedStatus = session.getAttribute("selectedStatus")
							.toString();
					uploadform.setSelectedStatus(session
							.getAttribute("tempStatus").toString().split(","));
				}
				if (session.getAttribute("otherCheck") != null)
					uploadform.setOtherCheck(session.getAttribute("otherCheck")
							.toString());
			}

			if (session.getAttribute("monthMSA") != null) {
				session.removeAttribute("weekMSA");
				monthMSA = session.getAttribute("monthMSA").toString();
			}
			if (session.getAttribute("weekMSA") != null) {
				session.removeAttribute("monthMSA");
				weekMSA = session.getAttribute("weekMSA").toString();
			}

			if (!chkOtherOwner) {
				uploadform.setPrevSelectedStatus(selectedStatus);
				uploadform.setPrevSelectedOwner(selectedOwner);
			}

			ownerList = MSAdao.getOwnerList(userid, uploadform.getOtherCheck(),
					ownerType, msaid, getDataSource(request, "ilexnewDB"));
			statusList = MSAdao.getStatusList("pm_msa",
					getDataSource(request, "ilexnewDB"));

			if (ownerList.size() > 0)
				ownerListSize = ownerList.size();

			request.setAttribute("clickShow", request.getParameter("clickShow"));
			request.setAttribute("statuslist", statusList);
			request.setAttribute("ownerlist", ownerList);
			request.setAttribute("ownerListSize", ownerListSize + "");

			request.setAttribute("MSA_Id", uploadform.getId1());
			String msaStatus = MSAdao.getMSAStatus(uploadform.getId1(),
					getDataSource(request, "ilexnewDB"));
			String msaUploadCheck = MSAdao.getMSAUploadCheck(
					uploadform.getId1(), getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
					uploadform.getId1(), "", "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);
			request.setAttribute("status", msaStatus);
			request.setAttribute("uploadstatus", msaUploadCheck);

			return (mapping.findForward("fileuploadwindow"));
		}
	}

	public ActionForward FromAppendix(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		UploadForm uploadform = (UploadForm) form;
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		int uploadflag = -1;

		// Start of View Selector code//
		// Variables Added to add the common functionality for the View
		// Selector.

		boolean chkOtherOwner = false;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		UploadBean uploadBean = new UploadBean();
		String selectedStatus = "";
		String selectedOwner = "";
		int ownerListSize = 0;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "appendix";

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		if (!Authenticationdao.getPageSecurity(userid, "MSA", "Upload MSA",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		if (uploadform.getUpload() != null) {
			uploadBean.setIn(uploadform.getFilename().getInputStream());
			uploadBean.setFileSize(uploadform.getFilename().getFileSize());
			uploadBean.setFileName(uploadform.getFilename().getFileName());
			uploadflag = Upload.fileupload(uploadBean, "Appendix", userid,
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("type", uploadform.getType());
			request.setAttribute("uploadflag", uploadflag + "");
			request.setAttribute("Appendix_Id", uploadform.getId1());
			request.setAttribute("MSA_Id", uploadform.getMSA_Id());
			return (mapping.findForward("appendixdetailpage"));
		}

		else {
			uploadform.setType("Appendix");

			if (request.getParameter("Id") != null) {
				uploadform.setId1(request.getParameter("Id"));
				uploadform.setAppendixName(Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"),
						uploadform.getId1()));
				uploadform.setMSA_Id(IMdao.getMSAId(uploadform.getId1(),
						getDataSource(request, "ilexnewDB")));
				uploadform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						uploadform.getMSA_Id()));
			}

			if (request.getParameter("home") != null) // This is called when
														// reset button is
														// pressed from the view
														// selector.
			{
				uploadform.setAppendixSelectedStatus(null);
				uploadform.setAppendixSelectedOwners(null);
				uploadform.setPrevSelectedStatus("");
				uploadform.setPrevSelectedOwner("");
				uploadform.setAppendixOtherCheck(null);
				uploadform.setMonthWeekCheck(null);
				request.setAttribute("MSA_Id", uploadform.getMSA_Id());
				return (mapping.findForward("appendixlistpage"));

			}

			if (request.getParameter("opendiv") != null) // This is used to make
															// the div remain
															// open when other
															// check box is
															// checked.
				request.setAttribute("opendiv", request.getParameter("opendiv")
						.toString());

			// These two parameter month/week are checked are kept in session. (
			// View Images)
			if (request.getParameter("month") != null) {
				session.setAttribute("monthAppendix",
						request.getParameter("month").toString());

				session.removeAttribute("weekAppendix");
				uploadform.setMonthWeekCheck("month");
			}

			if (request.getParameter("week") != null) {
				session.setAttribute("weekAppendix",
						request.getParameter("week").toString());

				session.removeAttribute("monthAppendix");
				uploadform.setMonthWeekCheck("week");
			}

			if (uploadform.getAppendixOtherCheck() != null) // OtherCheck box
															// status is kept in
															// session.
				session.setAttribute("appendixOtherCheck", uploadform
						.getAppendixOtherCheck().toString());

			// When Status Check Boxes are Clicked :::::::::::::::::
			if (uploadform.getAppendixSelectedStatus() != null) {
				for (int i = 0; i < uploadform.getAppendixSelectedStatus().length; i++) {
					selectedStatus = selectedStatus + "," + "'"
							+ uploadform.getAppendixSelectedStatus(i) + "'";
					tempStatus = tempStatus + ","
							+ uploadform.getAppendixSelectedStatus(i);
				}

				selectedStatus = selectedStatus.substring(1,
						selectedStatus.length());
				selectedStatus = "(" + selectedStatus + ")";
			}

			// When Owner Check Boxes are Clicked :::::::::::::::::
			if (uploadform.getAppendixSelectedOwners() != null) {
				for (int j = 0; j < uploadform.getAppendixSelectedOwners().length; j++) {
					selectedOwner = selectedOwner + ","
							+ uploadform.getAppendixSelectedOwners(j);
					tempOwner = tempOwner + ","
							+ uploadform.getAppendixSelectedOwners(j);
				}
				selectedOwner = selectedOwner.substring(1,
						selectedOwner.length());
				if (selectedOwner.equals("0")) {
					chkOtherOwner = true;
					selectedOwner = "%";
				}
				selectedOwner = "(" + selectedOwner + ")";
			}

			if (uploadform.getGo() != null && !uploadform.getGo().equals("")) {
				session.setAttribute("appendixSelectedOwners", selectedOwner);
				session.setAttribute("appSelectedOwners", selectedOwner);
				session.setAttribute("appendixTempOwner", tempOwner);
				session.setAttribute("appendixSelectedStatus", selectedStatus);
				session.setAttribute("appSelectedStatus", selectedStatus);
				session.setAttribute("appendixTempStatus", tempStatus);

				if (uploadform.getMonthWeekCheck() != null) {
					if (uploadform.getMonthWeekCheck().equals("week")) {
						session.removeAttribute("monthAppendix");
						session.setAttribute("weekAppendix", "0");

					}
					if (uploadform.getMonthWeekCheck().equals("month")) {
						session.removeAttribute("weekAppendix");
						session.setAttribute("monthAppendix", "0");

					}
					if (uploadform.getMonthWeekCheck().equals("clear")) {
						session.removeAttribute("weekAppendix");
						session.removeAttribute("monthAppendix");

					}

				}
				request.setAttribute("MSA_Id", uploadform.getMSA_Id());
				return (mapping.findForward("appendixlistpage"));
			} else {
				if (session.getAttribute("appendixSelectedOwners") != null) {
					selectedOwner = session.getAttribute(
							"appendixSelectedOwners").toString();
					uploadform.setAppendixSelectedOwners(session
							.getAttribute("appendixTempOwner").toString()
							.split(","));
				}
				if (session.getAttribute("appendixSelectedStatus") != null) {
					selectedStatus = session.getAttribute(
							"appendixSelectedStatus").toString();
					uploadform.setAppendixSelectedStatus(session
							.getAttribute("appendixTempStatus").toString()
							.split(","));
				}
				if (session.getAttribute("appendixOtherCheck") != null)
					uploadform.setAppendixOtherCheck(session.getAttribute(
							"appendixOtherCheck").toString());

				if (session.getAttribute("monthAppendix") != null) {
					session.removeAttribute("weekAppendix");
					uploadform.setMonthWeekCheck("month");
				}
				if (session.getAttribute("weekAppendix") != null) {
					session.removeAttribute("monthAppendix");
					uploadform.setMonthWeekCheck("week");
				}

			}

			if (!chkOtherOwner) {
				uploadform.setPrevSelectedStatus(selectedStatus);
				uploadform.setPrevSelectedOwner(selectedOwner);
			}
			ownerList = MSAdao
					.getOwnerList(userid, uploadform.getAppendixOtherCheck(),
							ownerType, uploadform.getMSA_Id(),
							getDataSource(request, "ilexnewDB"));
			statusList = MSAdao.getStatusList("pm_appendix",
					getDataSource(request, "ilexnewDB"));
			if (ownerList.size() > 0)
				ownerListSize = ownerList.size();

			request.setAttribute("clickShow", request.getParameter("clickShow"));
			request.setAttribute("statuslist", statusList);
			request.setAttribute("ownerlist", ownerList);
			request.setAttribute("ownerListSize", ownerListSize + "");

			String appendixStatus = Appendixdao.getAppendixStatus(
					uploadform.getId1(), uploadform.getMSA_Id(),
					getDataSource(request, "ilexnewDB"));
			String addendumlist = Appendixdao.getAddendumsIdName(
					uploadform.getId1(), getDataSource(request, "ilexnewDB"));
			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(uploadform.getId1(),
							getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_appendix",
					appendixStatus.charAt(0), uploadform.getMSA_Id(),
					uploadform.getId1(), "", "", userid,
					getDataSource(request, "ilexnewDB"));
			String appendix_type = Appendixdao.getAppendixPrType(
					uploadform.getMSA_Id(), uploadform.getId1(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixType", appendix_type);
			request.setAttribute("appendixStatusList", list);
			request.setAttribute("Appendix_Id", uploadform.getId1());
			request.setAttribute("MSA_Id", uploadform.getMSA_Id());
			request.setAttribute("appendixStatus", appendixStatus);
			request.setAttribute("latestaddendumid", addendumid);
			request.setAttribute("addendumlist", addendumlist);

			return (mapping.findForward("fileuploadwindow"));
		}
	}

	public ActionForward FromAppendixDashboard(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		UploadForm uploadform = (UploadForm) form;
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		int uploadflag = -1;
		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		UploadBean uploadBean = new UploadBean();

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		// if( !Authenticationdao.getPageSecurity( userid , "MSA" ,
		// "Upload MSA", getDataSource( request , "ilexnewDB" ) ) )
		// {
		// return( mapping.findForward( "UnAuthenticate" ) );
		// }

		if (request.getParameter("viewjobtype") != null) {
			uploadform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			uploadform.setViewjobtype("A");
		}
		request.setAttribute("viewjobtype", uploadform.getViewjobtype());

		if (request.getParameter("ownerId") != null) {
			uploadform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			uploadform.setOwnerId("%");
		}

		if (uploadform.getUpload() != null) {
			uploadBean.setIn(uploadform.getFilename().getInputStream());
			uploadBean.setFileSize(uploadform.getFilename().getFileSize());
			uploadBean.setFileName(uploadform.getFilename().getFileName());
			uploadflag = Upload.fileupload(uploadBean, "Appendix", userid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("type", uploadform.getType());
			request.setAttribute("uploadflag", uploadflag + "");
			request.setAttribute("Id1", uploadform.getId1());
			return (mapping.findForward("tempuploadpage"));
		}

		else {
			uploadform.setType("Appendixdashboard");
			if (request.getParameter("Id") != null) {
				uploadform.setId1(request.getParameter("Id"));
				request.setAttribute(
						"appendixname",
						Jobdao.getAppendixname(
								getDataSource(request, "ilexnewDB"),
								uploadform.getId1()));
				uploadform.setAppendixName(Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"),
						uploadform.getId1()));

			}

			String contractDocoumentList = com.mind.dao.PM.Appendixdao
					.getcontractDocoumentMenu(uploadform.getId1(),
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("contractDocMenu", contractDocoumentList);
			String appendixcurrentstatus = Appendixdao.getCurrentstatus(
					uploadform.getId1(), getDataSource(request, "ilexnewDB"));
			uploadform.setMSA_Id(IMdao.getMSAId(uploadform.getId1(),
					getDataSource(request, "ilexnewDB")));
			request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
			request.setAttribute("msaId", uploadform.getMSA_Id());
			request.setAttribute("viewjobtype", uploadform.getViewjobtype());
			request.setAttribute("ownerId", uploadform.getOwnerId());
			request.setAttribute("appendixid", uploadform.getId1());

			if (request.getParameter("fromPage") != null)
				uploadform.setFromPage(request.getParameter("fromPage"));

			/* Start : Appendix Dashboard View Selector Code */

			if (uploadform.getJobOwnerOtherCheck() != null
					&& !uploadform.getJobOwnerOtherCheck().equals("")) {
				session.setAttribute("jobOwnerOtherCheck",
						uploadform.getJobOwnerOtherCheck());
			}

			if (request.getParameter("opendiv") != null) {
				request.setAttribute("opendiv", request.getParameter("opendiv"));
			}
			if (request.getParameter("resetList") != null
					&& request.getParameter("resetList").equals("something")) {
				WebUtil.setDefaultAppendixDashboardAttribute(session);
				request.setAttribute("type", "AppendixViewSelector");
				request.setAttribute("appendixId", uploadform.getId1());
				request.setAttribute("msaId", uploadform.getMSA_Id());

				return mapping.findForward("tempuploadpage");

			}

			if (uploadform.getJobSelectedStatus() != null) {
				for (int i = 0; i < uploadform.getJobSelectedStatus().length; i++) {
					jobSelectedStatus = jobSelectedStatus + "," + "'"
							+ uploadform.getJobSelectedStatus(i) + "'";
					jobTempStatus = jobTempStatus + ","
							+ uploadform.getJobSelectedStatus(i);
				}

				jobSelectedStatus = jobSelectedStatus.substring(1,
						jobSelectedStatus.length());
				jobSelectedStatus = "(" + jobSelectedStatus + ")";
			}

			if (uploadform.getJobSelectedOwners() != null) {
				for (int j = 0; j < uploadform.getJobSelectedOwners().length; j++) {
					jobSelectedOwner = jobSelectedOwner + ","
							+ uploadform.getJobSelectedOwners(j);
					jobTempOwner = jobTempOwner + ","
							+ uploadform.getJobSelectedOwners(j);
				}
				jobSelectedOwner = jobSelectedOwner.substring(1,
						jobSelectedOwner.length());
				if (jobSelectedOwner.equals("0")) {
					jobSelectedOwner = "%";
				}
				jobSelectedOwner = "(" + jobSelectedOwner + ")";
			}

			if (request.getParameter("showList") != null
					&& request.getParameter("showList").equals("something")) {

				session.setAttribute("jobSelectedOwners", jobSelectedOwner);
				session.setAttribute("jobTempOwner", jobTempOwner);
				session.setAttribute("jobSelectedStatus", jobSelectedStatus);
				session.setAttribute("jobTempStatus", jobTempStatus);
				session.setAttribute("timeFrame",
						uploadform.getJobMonthWeekCheck());
				session.setAttribute("jobOwnerOtherCheck",
						uploadform.getJobOwnerOtherCheck());

				request.setAttribute("type", "AppendixViewSelector");
				request.setAttribute("appendixId", uploadform.getId1());
				request.setAttribute("msaId", uploadform.getMSA_Id());

				return mapping.findForward("tempuploadpage");

			} else {
				if (session.getAttribute("jobSelectedOwners") != null) {
					jobSelectedOwner = session
							.getAttribute("jobSelectedOwners").toString();
					uploadform
							.setJobSelectedOwners(session
									.getAttribute("jobTempOwner").toString()
									.split(","));
				}
				if (session.getAttribute("jobSelectedStatus") != null) {
					jobSelectedStatus = session.getAttribute(
							"jobSelectedStatus").toString();
					uploadform.setJobSelectedStatus(session
							.getAttribute("jobTempStatus").toString()
							.split(","));
				}
				if (session.getAttribute("jobOwnerOtherCheck") != null) {
					uploadform.setJobOwnerOtherCheck(session.getAttribute(
							"jobOwnerOtherCheck").toString());
				}
				if (session.getAttribute("timeFrame") != null) {
					uploadform.setJobMonthWeekCheck(session.getAttribute(
							"timeFrame").toString());
				}
			}

			jobOwnerList = MSAdao.getOwnerList(userid,
					uploadform.getJobOwnerOtherCheck(), "prj_job_new",
					uploadform.getId1(), getDataSource(request, "ilexnewDB"));
			jobStatusList = MSAdao.getStatusList("prj_job_new",
					getDataSource(request, "ilexnewDB"));

			if (jobOwnerList.size() > 0)
				jobOwnerListSize = jobOwnerList.size();

			request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("jobOwnerList", jobOwnerList);
			/* End : Appendix Dashboard View Selector Code */

			return (mapping.findForward("fileuploadwindow"));
		}
	}

	public ActionForward FromJobDashboard(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		UploadForm uploadform = (UploadForm) form;
		HttpSession session = request.getSession(true);
		UploadBean uploadBean = new UploadBean();
		String userid = (String) session.getAttribute("userid");
		int uploadflag = -1;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired

		// if( !Authenticationdao.getPageSecurity( userid , "MSA" ,
		// "Upload MSA", getDataSource( request , "ilexnewDB" ) ) )
		// {
		// return( mapping.findForward( "UnAuthenticate" ) );
		// }

		if (request.getParameter("path") != null) {
			uploadform.setPath(request.getParameter("path"));
			uploadform.setAppendix_Id(request.getParameter("appendix_Id"));
		}

		// for dispatch view of netmedx manager bak button
		if (request.getParameter("nettype") != null) {
			uploadform.setNettype("" + request.getParameter("nettype"));
		}

		if (request.getParameter("viewjobtype") != null) {
			uploadform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			uploadform.setViewjobtype("A");
		}
		request.setAttribute("viewjobtype", uploadform.getViewjobtype());

		if (uploadform.getUpload() != null) {
			uploadBean.setIn(uploadform.getFilename().getInputStream());
			uploadBean.setFileSize(uploadform.getFilename().getFileSize());
			uploadBean.setFileName(uploadform.getFilename().getFileName());
			uploadflag = Upload.fileupload(uploadBean, "Job", userid,
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("type", uploadform.getType());
			request.setAttribute("uploadflag", uploadflag + "");
			request.setAttribute("Id1", uploadform.getId1());
			request.setAttribute("appendix_Id", uploadform.getAppendix_Id());
			request.setAttribute("path", uploadform.getPath());

			if (uploadform.getPath().equals("jobdashboard")) {
				request.setAttribute("jobid", uploadform.getId1());
				return mapping.findForward("jobdashboard");
			} else {
				return (mapping.findForward("tempuploadpage"));
			}

		}

		else {
			uploadform.setType("Jobdashboard");

			if (request.getParameter("Id") != null) {
				uploadform.setId1(request.getParameter("Id"));
				uploadform.setJobName(Jobdao.getJobname(uploadform.getId1(),
						getDataSource(request, "ilexnewDB")));
				Map<String, Object> map;

				map = DatabaseUtilityDao.setMenu(uploadform.getId1(), userid,
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);

			}

			return (mapping.findForward("fileuploadwindow"));
		}
	}

}