package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;




public class LogOutAction extends com.mind.common.IlexAction
{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception
	{
		String contextipaddress = "";
		
		contextipaddress = request.getLocalAddr();
		request.setAttribute( "contextipaddress" , contextipaddress );
		
		
		return mapping.findForward( "success" );
	}
}
