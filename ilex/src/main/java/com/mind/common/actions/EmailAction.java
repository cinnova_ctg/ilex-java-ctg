package com.mind.common.actions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.Document;
import com.mind.common.EnvironmentSelector;
import com.mind.common.IlexConstants;
import com.mind.common.POStatusTypes;
import com.mind.common.Util;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.common.formbean.EmailForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PMT.EditMasterlDao;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.dao.PRM.POWOdao;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.DocumentIdNotFoundException;
import com.mind.docm.exceptions.DocumentNotExistsException;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.AttachDocument;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.newjobdb.util.InvalidPOActionException;
import com.mind.util.WebUtil;
import com.mind.xml.Domxmltaglist;
import com.mind.xml.EditXmldao;
import com.mind.xml.IRPdf;
import com.mind.xml.POPdf;
import com.mind.xml.ViewAction;
import com.mind.xml.WOPdf;

public class EmailAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EmailAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		EmailForm emailform = (EmailForm) form;
		EmailBean emailBean = new EmailBean();

		String name = "";
		String type = "";
		String changestatus = "";
		String sessionid = request.getRequestedSessionId();

		String formaldocdate = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		// Start: Variables added for the View Selector Functioning.
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "";
		String msaid = "";
		int ownerListSize = 0;
		// End
		emailform.setChangeStatus("false");
		String currentStatus = null;
		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		String currentJobStatus = "";

		/* Get message body from database start */
		emailform.setMessageContent(null);
		emailform.setMessageContent(EditMasterlDao.getEmailMessage("emailbody",
				getDataSource(request, "ilexnewDB")));
		/* Get message body from database end */

		/* for security */
		// String temp_fu_type = "";
		emailform.setTemp_fu_type("");
		emailform.setTemp_fu_name("Send");
		// System.out.println("GO ::::" + emailform.getGo());
		if (emailform.getAttachPOWO() != null) {
			for (int i = 0; i < emailform.getAttachPOWO().length; i++) {
			}
		}

		if (request.getParameter("onlyEmail") != null) {
			emailform.setOnlyEmail(request.getParameter("onlyEmail"));
		}

		if (request.getParameter("Type") != null) {
			if (request.getParameter("Type").equals("PRMAppendix")) {
				emailform.setTemp_fu_type("Manage Project");
				emailform.setTemp_fu_name("Send");
			}
			if (request.getParameter("Type").equals("Appendix")) {
				emailform.setTemp_fu_type("Appendix");
				emailform.setTemp_fu_name("Send");
			}
			if (request.getParameter("Type").equals("MSA")) {
				emailform.setTemp_fu_type("MSA");
				emailform.setTemp_fu_name("Send");
			}
		}

		if (request.getAttribute("Type") != null
				&& request.getAttribute("Type").equals("Appendix")) {
			emailform.setTemp_fu_type("Appendix");
			emailform.setTemp_fu_name("Send");
		}
		if (request.getAttribute("docMException") != null) {
			request.setAttribute("docMException", "true");
		}

		if (request.getParameter("Type1") != null) {
			if (request.getParameter("Type").equals("Send POWO")) { // security
																	// para for
																	// Send PO -
																	// WO
				emailform.setTemp_fu_type("Manage Partner");
				emailform.setTemp_fu_name("Send PO/WO");
			}

			if (request.getParameter("Type1").equals("prm_appendix")) {
				emailform.setTemp_fu_type("Manage Partner");
				emailform.setTemp_fu_name("Send PO/WO");
			}
			if (request.getParameter("Type1").equals("Manage Status Report")) {
				emailform.setTemp_fu_type("Manage Partner");
				emailform.setTemp_fu_name("Send PO/WO");
			}
			if (request.getParameter("Type1").equals("prm_job")) {
				emailform.setTemp_fu_type("Manage Partner");
				emailform.setTemp_fu_name("Send PO/WO");

			}

			if (request.getParameter("Type1").equals("prm_activity")) {
				emailform.setTemp_fu_type("Manage Partner");
				emailform.setTemp_fu_name("Send PO/WO");

			}
			if (request.getParameter("Type").trim().equalsIgnoreCase("prm_job")) {
				if (logger.isDebugEnabled()) {
					logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - job id-----------"
							+ emailform.getId());
				}
				Map<String, Object> map;

				if (emailform.getId() != null
						&& !emailform.getId().trim().equalsIgnoreCase("")) {
					map = DatabaseUtilityDao.setMenu(emailform.getId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
					WebUtil.copyMapToRequest(request, map);
				}
			}

		}

		if (request.getAttribute("Type") != null) { // for security
			if (request.getAttribute("Type").equals("Send POWO")) { // security
																	// para for
																	// Send PO -
																	// WO
				emailform.setTemp_fu_type("Manage Partner");
				emailform.setTemp_fu_name("Send PO/WO");

			}

			if (request.getAttribute("Type").equals("Invoice Request")) {
				emailform.setTemp_fu_type("Manage Invoice");
				emailform.setTemp_fu_name("Send Invoice Request");

			}

		}

		if (request.getParameter("Type1") != null) { // security para for Send
														// IR
			if (request.getParameter("Type1").equals("Invoice Request")) {
				emailform.setTemp_fu_type("Manage Invoice");
				emailform.setTemp_fu_name("Send Invoice Request");
			}
		}

		if (request.getParameter("addendumflag") != null) {
			emailform.setAddendumFlag(request.getParameter("addendumflag"));
		}

		String userId = (String) session.getAttribute("userid");
		String loginusername = (String) session.getAttribute("username");
		/* Page Security Start */

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		if (emailform.getAuthenticate().equals("")) {
			if (!Authenticationdao.getPageSecurity(userId,
					emailform.getTemp_fu_type(), emailform.getTemp_fu_name(),
					getDataSource(request, "ilexnewDB"))) {
				return (mapping.findForward("UnAuthenticate"));
			}
		}
		/* Page Security End */

		if (request.getParameter("viewjobtype") != null) {
			emailform.setViewjobtype("" + request.getParameter("viewjobtype"));
		} else {
			emailform.setViewjobtype("A");
		}

		if (request.getParameter("ownerId") != null) {
			emailform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			emailform.setOwnerId("%");
		}
		if (request.getParameter("fromPage") != null) {
			emailform.setFromPage(request.getParameter("fromPage"));
		}

		String poId = emailform.getId1();
		String authString = PurchaseOrderDAO.getAuthString(String
				.valueOf(PurchaseOrderDAO.getParnterId(poId,
						getDataSource(request, "ilexnewDB"))),
				getDataSource(request, "ilexnewDB"));
		String mobileContent = "";
		mobileContent = Domxmltaglist.addParameter(EditMasterlDao
				.getEmailMessage("emailbodymobile",
						getDataSource(request, "ilexnewDB")), poId, authString);

		emailform.setContentFromDB(mobileContent);

		if (emailform.getSend() != null) {
			int i = 0;
			String address = "";
			String emailContent = emailform.getContent();
			String signContent = emailform.getContentSign();
			// if (emailform.isMobileUploadAllowed()) {

			// int index = emailContent.indexOf("\n");
			// if (index > 0) {
			// String firstHalf = emailContent.substring(0, index + 1);
			// String lastHalf = emailContent.substring(index);
			emailContent = "<p>" + emailContent.replaceAll("\\r\\n", "<br/>")
					+ "</p>" + mobileContent + "<p>"
					+ signContent.replaceAll("\\r\\n", "<br/>") + "</p>";
			// } else {
			// emailContent = emailContent + "\n\n" + mobileContent;
			// }
			// }

			// emailContent = emailContent.replaceAll("\\r\\n", "<br/>");
			// emailContent = "<HTML><Body>" + emailContent;
			emailContent = "<HTML><Body>"
					+ "<div style=\"font-family:Verdana,Arial, Helvetica, sans-serif;font-size:14px;\">"
					+ emailContent + "   </div>" + "</HTML></Body>";
			emailform.setContent(emailContent);
			emailBean.setContentType("text/html");

			emailform.setUsername(this.getResources(request).getMessage(
					"common.Email.username"));
			emailform.setUserpassword(this.getResources(request).getMessage(
					"common.Email.userpassword"));
			emailform.setSmtpservername(EnvironmentSelector
					.getBundleString("common.Email.smtpservername"));
			emailform.setSmtpserverport(this.getResources(request).getMessage(
					"common.Email.smtpserverport"));

			if (emailform.getMailto() != null) {
				for (i = 0; i < emailform.getMailto().length; i++) {
					if (!(emailform.getMailto(i).equals("-") || emailform
							.getMailto(i).equals(""))) {
						if (Email.isValidEmailAddress(emailform.getMailto(i))) {
							address = address + emailform.getMailto(i);
							address = address + ",";
						}
					}
				}
			}

			// Add additional to recipients start
			String textmailstring = "";
			if (emailform.getTextmailaddto() != null) {
				textmailstring = emailform.getTextmailaddto();
				StringTokenizer str = new StringTokenizer(textmailstring, ";");

				while (str.hasMoreTokens()) {
					String tempAddress = str.nextToken();
					if (Email.isValidEmailAddress(tempAddress)) {
						address = address + tempAddress + ",";
					}
				}
			}
			// Add additional to recipients end

			if (address != "")
				address = address.substring(0, address.length() - 1);
			emailform.setTo(address);

			address = "";

			if (emailform.getMailcc() != null) {
				for (i = 0; i < emailform.getMailcc().length; i++) {
					if (!(emailform.getMailcc(i).equals("-") || emailform
							.getMailcc(i).equals("-"))) {
						if (Email.isValidEmailAddress(emailform.getMailcc(i))) {
							address = address + emailform.getMailcc(i);
							address = address + ",";
						}
					}
				}
			}

			// for cc of user
			address = address + emailform.getFrom() + ",";

			/* by hamid for cc textbox start */

			if (emailform.getTextmailcc() != null) {
				if (emailform.getTextmailcc().length() > 0) {
					textmailstring = emailform.getTextmailcc();
					StringTokenizer str = new StringTokenizer(textmailstring,
							";");
					while (str.hasMoreTokens()) {
						String tempAddress = str.nextToken();
						if (Email.isValidEmailAddress(tempAddress)) {
							address = address + tempAddress + ",";
						}
					}
				}
			}
			/* by hamid for cc textbox end */

			if (address != "")
				address = address.substring(0, address.length() - 1);

			emailform.setCc(address);

			address = "";

			if (emailform.getMailbcc() != null) {
				for (i = 0; i < emailform.getMailbcc().length; i++) {
					if (!(emailform.getMailbcc(i).equals("-") || emailform
							.getMailbcc(i).equals("-"))) {
						if (Email.isValidEmailAddress(emailform.getMailbcc(i))) {
							address = address + emailform.getMailbcc(i);
							address = address + ",";
						}
					}
				}
			}

			if (address != "")
				address = address.substring(0, address.length() - 1);

			emailform.setBcc(address);

			String fileformat = emailform.getFileformat();

			/* by hamid for sending attached file start */
			// System.out.println("empty file "+emailform.getAttachfilename());
			if (emailform.getAttachfilename() != null) {
				if (emailform.getAttachfilename().getFileSize() > 0) {
					byte[] buffer2 = emailform.getAttachfilename()
							.getFileData();
					// String sitedata = new
					// String(emailform.getAttachfilename().getFileData());
					FileOutputStream fw2 = new FileOutputStream(Util.getTemp()
							+ "/" + emailform.getAttachfilename());
					fw2.write(buffer2);
					fw2.close();
					File f1 = new File(Util.getTemp() + "/"
							+ emailform.getAttachfilename());
					emailform.setFile2(f1);
					emailform.setFilename2("" + emailform.getAttachfilename());
					f1.deleteOnExit();
				}
			}

			/* by hamid for sending attached file start */

			// Start :Added by Amit For Multiple Attachment

			if (emailform.getAttachfilename1() != null) {

				if (emailform.getAttachfilename1().getFileSize() > 0) {

					byte[] buffer3 = emailform.getAttachfilename1()
							.getFileData();
					// String sitedata = new
					// String(emailform.getAttachfilename().getFileData());
					FileOutputStream fw3 = new FileOutputStream(Util.getTemp()
							+ "/" + emailform.getAttachfilename1());
					fw3.write(buffer3);
					fw3.close();
					File f2 = new File(
							(Util.getTemp() + "/" + emailform
									.getAttachfilename1()));
					emailform.setFileA(f2);
					emailform.setFilenameA("" + emailform.getAttachfilename1());
					f2.deleteOnExit();
				}
			}

			if (emailform.getAttachfilename2() != null) {

				if (emailform.getAttachfilename2().getFileSize() > 0) {

					byte[] buffer4 = emailform.getAttachfilename2()
							.getFileData();
					// String sitedata = new
					// String(emailform.getAttachfilename().getFileData());
					FileOutputStream fw4 = new FileOutputStream(Util.getTemp()
							+ "/" + emailform.getAttachfilename2());
					fw4.write(buffer4);
					fw4.close();
					File f3 = new File(
							(Util.getTemp() + "/" + emailform
									.getAttachfilename2()));
					emailform.setFileB(f3);
					emailform.setFilenameB("" + emailform.getAttachfilename2());
					f3.deleteOnExit();
				}
			}

			// End

			/* Email for Invoice Request Start */
			if (emailform.getType1().equals("Invoice Request")) {

				String imagepath = WebUtil.getRealPath(request, "images")
						+ "/clogo.jpg";

				// String indexvalue = "";
				IRPdf ir = new IRPdf();
				byte[] buffer1 = ir.viewIR(emailform.getAct_id(), imagepath,
						getDataSource(request, "ilexnewDB"));

				/* Temporary folder for temp pdf - Aditya */
				Calendar calendar = Calendar.getInstance();
				String tempFolderName = String.valueOf(calendar
						.getTimeInMillis()) + "-" + loginuserid;
				File tempFolder = new File(Util.getTemp() + "/ilex/"
						+ tempFolderName);
				tempFolder.mkdirs();
				FileOutputStream fw = new FileOutputStream(Util.getTemp()
						+ "/ilex/" + tempFolderName + "/" + "IR_"
						+ emailform.getAct_id() + ".pdf");
				fw.write(buffer1);
				fw.close();
				File f4 = new File(Util.getTemp() + "/ilex/" + tempFolderName
						+ "/" + "IR_" + emailform.getAct_id() + ".pdf");
				emailform.setFile(f4);
				emailform.setFilenameX("IR_" + emailform.getAct_id() + ".pdf");
				f4.deleteOnExit();
				f4.getParentFile().deleteOnExit();
			}
			/* Email for Invoice Request End */

			/* Email for POWO Start */
			if (emailform.getType().equals("Send POWO")) {
				// Here some cases are not handled like Document exists in DocM,
				// its status being changed to superded but fail to approve that
				// document.
				byte[] poBuffer = null;
				byte[] woBuffer = null;
				boolean sendtoDocM = false;
				boolean error = false;

				/* In case of Email from Buy Tab no request goes to DocM */
				if (emailform.getOnlyEmail() != null
						&& emailform.getOnlyEmail().equals("1")) {
					sendtoDocM = true;
				}

				poBuffer = getBuffer(emailform.getId1(), "PO", userId, request,
						getDataSource(request, "ilexnewDB"));
				woBuffer = getBuffer(emailform.getId1(), "WO", userId, request,
						getDataSource(request, "ilexnewDB"));

				if (!sendtoDocM) {
					if (docmInteractionException(emailform.getId1(), "PO",
							poBuffer)) {
						request.setAttribute("docMError", "true");
						error = true;
					}

					if (docmInteractionException(emailform.getId1(), "WO",
							woBuffer)) {
						request.setAttribute("docMError", "true");
						error = true;
					}
				}

				try {
					if (emailform.getAttachPOWO() != null) {
						setAttachDocumentForPOWO(request, emailform);
						for (int j = 0; j < emailform.getAttachPOWO().length; j++) {
							String AttachPOWO = emailform.getAttachPOWO(j);
							if (AttachPOWO != null && AttachPOWO.equals("PO")) { // If
																					// we
																					// have
																					// to
																					// send
																					// the
																					// PO
								Document document = EntityManager.getDocument(
										emailform.getId1(), "PO");
								String fileName = "PO";

								File f5 = getAttachment(emailform, sessionid,
										document, fileName, loginuserid);
								emailform.setFile(f5);
								emailform.setFilenameX(f5.getName());
								File tempFolder = f5.getParentFile();
								f5.deleteOnExit();
								tempFolder.deleteOnExit();
								// f5.deleteOnExit();
							} else if (AttachPOWO != null
									&& AttachPOWO.equals("WO")) { // If we have
																	// to send
																	// the PO
								Document document = EntityManager.getDocument(
										emailform.getId1(), "WO");
								String fileName = "WO";
								File f6 = getAttachment(emailform, sessionid,
										document, fileName, loginuserid);
								emailform.setFile1(f6);
								emailform.setFilename1(f6.getName());
								File tempFolder = f6.getParentFile();
								f6.deleteOnExit();
								tempFolder.deleteOnExit();
								// f6.deleteOnExit();
							}
						}
					}

					// To check whether pdW9W9 value.
					String pdW9W9 = Email.checkIsPdW9W9(emailform.getId1(),
							getDataSource(request, "ilexnewDB"));
					if (!IlexConstants.CHARACTER_Y.equalsIgnoreCase(pdW9W9)) {
						File filePdW9W9 = new File(
								request.getSession()
										.getServletContext()
										.getRealPath(
												IlexConstants.W9W9_PDF_BLANK_FILE_LOCATION));

						emailform.setFilePdW9W9(filePdW9W9);
						emailform
								.setFileNamePdW9W9(IlexConstants.W9W9_PDF_BLANK_FILENAME);

						String content = emailform.getContent();
						String[] tempW9W9 = content
								.split(IlexConstants.W9W9_SPLIT_EXP);
						String w9W9body = "";
						String sign = "";
						w9W9body += tempW9W9[0];
						w9W9body += IlexConstants.W9W9_PDF_BLANK_TEXT_BODY;

						sign = IlexConstants.W9W9_APPEND_SINCERELY;
						if (tempW9W9.length > 1 && !tempW9W9[1].equals("")) {
							sign += tempW9W9[1];
						}
						emailform.setContent(w9W9body);
						emailform.setContentSign(sign);

					}

				} catch (Exception e) {
					logger.error(
							"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
							e);
					e.printStackTrace();
					request.setAttribute("docMError", "true");
					error = true;
				}

				if (error) {
					request.setAttribute("job_id", emailform.getId2());
					request.setAttribute("jobid", emailform.getId2());
					request.setAttribute("from_type",
							emailform.getPowo_fromtype());
					return (mapping.findForward("emailpowo"));
				}
			}
			/* Email for POWO End */

			if (emailform.getType().equals("MSA")) {
				Document document = null;
				byte[] buffer1 = null;
				boolean error = false;
				try {
					document = EntityManager.getDocument(emailform.getId1(),
							"MSA");
					buffer1 = document.getFileBytes();
				} catch (Exception e) {
					logger.error(
							"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
							e);
					request.setAttribute("docMException", "true");
					error = true;
				}

				if (error) {
					request.setAttribute("MSA_Id", emailform.getId1());
					return mapping.findForward("msadetailpage");
				} else {
					String fileName = "MSA";

					File f7 = getAttachment(emailform, sessionid, document,
							fileName, loginuserid);
					emailform.setFile(f7);
					emailform.setFilenameX(f7.getName());
					File tempFolder = f7.getParentFile();
					f7.deleteOnExit();
					tempFolder.deleteOnExit();
					currentStatus = com.mind.dao.PRM.Appendixdao
							.getCurrentMSAstatus(emailform.getId1(),
									getDataSource(request, "ilexnewDB"));
				}

			}

			if (emailform.getType().equals("Appendix")
					|| emailform.getType().equals("PRMAppendix")) {
				byte[] buffer1 = null;
				Document document = null;
				byte[] buffer2 = null;
				Document document1 = null;
				boolean error = false;
				try {
					if (emailform.getAddendumFlag() == null
							|| emailform.getAddendumFlag().trim().equals("")) {
						document = EntityManager.getDocument(
								emailform.getId2(), "Appendix");
					} else {
						document = EntityManager.getDocument(
								emailform.getId3(), "Addendum");
					}

					buffer1 = document.getFileBytes();
				} catch (Exception e) {
					logger.error(
							"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
							e);
					request.setAttribute("docMException", "true");
					error = true;
				}

				if (error) {
					if (emailform.getType().equals("Appendix")) {
						request.setAttribute("Appendix_Id", emailform.getId2());
						request.setAttribute("MSA_Id", emailform.getMSA_Id());
						return mapping.findForward("appendixdetailpage");
					} else if (emailform.getType().equals("PRMAppendix")) {
						request.setAttribute("appendixid", emailform.getId2());
						request.setAttribute("type", emailform.getType());
						request.setAttribute("typename", type);
						request.setAttribute("viewjobtype",
								emailform.getViewjobtype());
						request.setAttribute("OwnerId", emailform.getOwnerId());
						return mapping.findForward("appendixdashboard");
					}
				} else {
					/* Temporary folder for temp pdf - Aditya */
					Calendar calendar = Calendar.getInstance();
					String tempFolderName = String.valueOf(calendar
							.getTimeInMillis()) + "-" + loginuserid;
					File tempFolder = new File(Util.getTemp() + "/ilex/"
							+ tempFolderName);
					tempFolder.mkdirs();
					FileOutputStream fw = new FileOutputStream(Util.getTemp()
							+ "/ilex/" + tempFolderName + "/" + "Appendix"
							+ "_" + emailform.getId2() + ".pdf");
					fw.write(buffer1);
					fw.close();
					File f8 = new File(Util.getTemp() + "/ilex/"
							+ tempFolderName + "/" + "Appendix" + "_"
							+ emailform.getId2() + ".pdf");
					emailform.setFile(f8);
					emailform.setFilenameX("Appendix" + "_"
							+ emailform.getId2() + ".pdf");

					tempFolder = f8.getParentFile();
					f8.deleteOnExit();
					tempFolder.deleteOnExit();
					// f8.deleteOnExit();
				}

				// Attach MSA for Appendix Dashboard: Start

				if (emailform.getIncludeMsa() != null) {
					boolean error1 = false;
					try {
						document = EntityManager.getDocument(
								emailform.getMSA_Id(), "MSA");
						buffer2 = document.getFileBytes();
					} catch (DocumentIdNotFoundException documentIdNotFoundException) {
						logger.error(
								"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
								documentIdNotFoundException);
						request.setAttribute("Type", "Appendix");
						request.setAttribute("Appendix_Id", emailform.getId2());
						request.setAttribute("MSA_Id", emailform.getMSA_Id());
						request.setAttribute("jobId", emailform.getId3());

						request.setAttribute("View", "pdf");
						request.setAttribute("from_type", "PM");
						buffer2 = ViewAction.getDocumentbytes(request,
								emailform.getMSA_Id(), "MSA", userId,
								getDataSource(request, "ilexnewDB"));
						error = false;

					} catch (DocumentNotExistsException documentNotExistsException) {
						logger.error(
								"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
								documentNotExistsException);
						request.setAttribute("View", "pdf");
						request.setAttribute("from_type", "PM");
						buffer2 = ViewAction.getDocumentbytes(request,
								emailform.getMSA_Id(), "MSA", userId,
								getDataSource(request, "ilexnewDB"));
						error = false;
					} catch (Exception e) {
						logger.error(
								"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
								e);
						request.setAttribute("docMException", "true");
						error = true;
					}
					if (error) {
						request.setAttribute("appendixid", emailform.getId2());
						request.setAttribute("type", emailform.getType());
						request.setAttribute("typename", type);
						request.setAttribute("viewjobtype",
								emailform.getViewjobtype());
						request.setAttribute("OwnerId", emailform.getOwnerId());

						return mapping.findForward("appendixdashboard");
					} else {
						Calendar calendar = Calendar.getInstance();
						String tempFolderName = String.valueOf(calendar
								.getTimeInMillis()) + "-" + loginuserid;
						File tempFolder = new File(Util.getTemp() + "/ilex/"
								+ tempFolderName);
						tempFolder.mkdirs();
						FileOutputStream fw = new FileOutputStream(
								Util.getTemp() + "/ilex/" + tempFolderName
										+ "/" + "MSA" + "_"
										+ emailform.getMSA_Id() + ".pdf");
						fw.write(buffer2);
						fw.close();
						File f9 = new File(Util.getTemp() + "/ilex/"
								+ tempFolderName + "/" + "MSA" + "_"
								+ emailform.getMSA_Id() + ".pdf");
						emailform.setFileForMsa(f9);
						emailform.setFilenameForMsa("MSA" + "_"
								+ emailform.getMSA_Id() + ".pdf");

						tempFolder = f9.getParentFile();
						f9.deleteOnExit();
						tempFolder.deleteOnExit();
						// Attach MSA for Appendix Dashboard: Start
					}
				}
				currentStatus = com.mind.dao.PRM.Appendixdao
						.getCurrentstatus(emailform.getId2(),
								getDataSource(request, "ilexnewDB"));
			}

			if (currentStatus != null) {
				if (emailform.getType().equalsIgnoreCase("MSA")
						&& currentStatus.trim().equalsIgnoreCase("A")) {
					emailform.setChangeStatus("true");
				} else if ((emailform.getType().equals("Appendix") || emailform
						.getType().equals("PRMAppendix"))
						&& currentStatus.trim().equalsIgnoreCase("A")) {
					emailform.setChangeStatus("true");
				}

				if ((emailform.getType().equals("Appendix") || emailform
						.getType().equals("PRMAppendix"))
						&& emailform.getAddendumFlag() != null
						&& emailform.getAddendumFlag().trim().equals("true")) {
					if (emailform.getId3() != null
							&& !emailform.getId3().trim().equals("")) {
						String[] jobStatusAndType = Jobdao.getJobStatusAndType(
								"%", emailform.getId3(), "%", "%",
								getDataSource(request, "ilexnewDB"));
						if (logger.isDebugEnabled()) {
							logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Status :"
									+ jobStatusAndType[0]);
						}
						String jobType = jobStatusAndType[1];
						if (jobStatusAndType[0].trim().equalsIgnoreCase(
								"Approved")
								&& jobStatusAndType[1].trim().equalsIgnoreCase(
										"Addendum"))
							emailform.setChangeStatus("true");
					}
				}
			}

                        
                        //Check to make sure that the email recepient isn't an agent for PO/WOs.
                        
                        logger.info("Agent Status");
                        
                        int agentStatus = -1;
                        
                        if (emailform.getType().equals("Send POWO")) {
                            String poIda = emailform.getId1();
                            
                            logger.info(poIda); 
                            
                            
                            agentStatus = PurchaseOrderDAO.checkAgentStatus(poIda, getDataSource(request, "ilexnewDB"));
                            
                        }
  
                        
			// System.out.println("Before Sending the email ::: " +
			// emailform.getChangeStatus());
			BeanUtils.copyProperties(emailBean, emailform);
                      
			// ---------------------------------------------------Email Send
                        //Don't send the email if the e-mail contains a work order and is being sent to an ESA.
                        if(agentStatus == -1){
			changestatus = Email.Send(emailBean,
					getDataSource(request, "ilexnewDB"));
                        }

			if (emailform.getType1().equals("Invoice Request")) {
				request.setAttribute("id", emailform.getId2());
				request.setAttribute("type", emailform.getInvtype1());
				request.setAttribute("rdofilter", emailform.getInvtype2());
				return (mapping.findForward("invoicemain"));
			}

			if (emailform.getType1().equals("Send POWO")) {

				request.setAttribute("job_id", emailform.getId2());
				request.setAttribute("jobid", emailform.getId2());
				request.setAttribute("from_type", emailform.getPowo_fromtype());
				request.setAttribute("order_type", emailform.getOrder_type());

				// Start :Added By Amit
				String temp[] = changestatus.split(",");

				if (!temp[0].equalsIgnoreCase("0")) {
					String order = "";
					if (logger.isDebugEnabled()) {
						logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - type--------------------"
								+ emailform.getType());
					}
					emailform.setPowo_type(""
							+ request.getAttribute("powo_type"));
					emailform.setPowo_fromtype(""
							+ request.getAttribute("from_type"));

					// Amit :following have to be checked
					emailform.setCustPOC(ViewList.getCustPOCEmail(
							emailform.getId1(), emailform.getId2(),
							emailform.getType(),
							getDataSource(request, "ilexnewDB")));
					// emailform.setType1("Send POWO");

					emailform.setCheck_for_fileupload(MSAdao
							.getfile_upload_flag(emailform.getId1(),
									getDataSource(request, "ilexnewDB")));
					name = Appendixdao.getMsaname(
							getDataSource(request, "ilexnewDB"),
							emailform.getId1());
					type = EditXmldao.getTypeName(emailform.getId1(), "MSA",
							getDataSource(request, "ilexnewDB"));
					formaldocdate = MSAdao.getformaldocdatename(
							getDataSource(request, "ilexnewDB"),
							emailform.getId1());

					emailform.setSubject(order);
					emailform
							.setContent("For your review and records, the "
									+ order
									+ " is "
									+ "attached.\n\nPlease sign and fax your concurrence back to me at the number listed below. You may direct any comments or questions to me as well.");
					emailform
							.setContentSign("\n\nSincerely,\n\n"
									+ loginusername
									+ "\nContingent Network Services\n4400 Port Union Road\nWest Chester, OH 45011\n(800) 506-9609\n(513)860-2105(facsimile)\nmedius.contingent.com\n\nHow are we doing?Please let us know! medius.contingent.com/satisfaction");
					request.setAttribute("name", name);
					request.setAttribute("typename", type);
					request.setAttribute("type", emailform.getType());
					request.setAttribute("temp_type1", emailform.getType1());
					request.setAttribute("temp_ordertype",
							emailform.getOrder_type());
					request.setAttribute("emailflag", temp[0]);
					request.setAttribute("jobid", emailform.getId2());
					return (mapping.findForward("emailwindow"));
				}
				request.setAttribute("emailflag", temp[0]);
				// End :Added By Amit
				try {
					// ---------------------
					if (emailform.getOnlyEmail() != null
							&& emailform.getOnlyEmail().equals("0")) {
						try {
							PurchaseOrder.setNewStatus(emailform.getId1(),
									loginuserid,
									POStatusTypes.STATUS_PARTNER_REVIEW,
									getDataSource(request, "ilexnewDB"));
						} catch (InvalidPOActionException e) {
							logger.error(
									"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
									e);
						}
					}
					// ---------------------
					ActionForward fwd = new ActionForward();
					fwd.setPath("/JobDashboardAction.do?tabId="
							+ emailform.getTabId() + "&jobid="
							+ emailform.getId2() + "&isClicked=");
					return fwd;
				} catch (Exception e) {
					logger.warn(
							"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored",
							e);

				}

			}

			if (emailform.getType().equals("MSA")) {

				request.setAttribute("MSA_Id", emailform.getId1());
				String temp[] = changestatus.split(",");

				request.setAttribute("emailflag", temp[0]);

				return (mapping.findForward("msadetailpage"));
			}

			if (emailform.getType().equals("Appendix")) {
				// request.setAttribute( "MSA_Id" , emailform.getId1() );
				request.setAttribute("Appendix_Id", emailform.getId2());
				request.setAttribute("MSA_Id", emailform.getMSA_Id());
				String temp[] = changestatus.split(",");

				request.setAttribute("emailflag", temp[0]);
				request.setAttribute("emailchangestatusflag", temp[1]);

				return (mapping.findForward("appendixdetailpage"));
			}

			if (emailform.getType().equals("PRMAppendix")) {
				// request.setAttribute( "MSA_Id" , emailform.getId1() );
				request.setAttribute("appendixid", emailform.getId2());
				request.setAttribute("type", emailform.getType());
				request.setAttribute("typename", type);
				request.setAttribute("viewjobtype", emailform.getViewjobtype());
				request.setAttribute("OwnerId", emailform.getOwnerId());

				// Start :Added By Amit
				String temp[] = changestatus.split(",");
				if (!temp[0].equalsIgnoreCase("0")) {
					name = Jobdao.getAppendixname(
							getDataSource(request, "ilexnewDB"),
							emailform.getId2());
					type = EditXmldao.getTypeName(emailform.getId1(), "MSA",
							getDataSource(request, "ilexnewDB"));
					emailform.setCustPOC(ViewList.getCustPOCEmail(
							emailform.getId1(), emailform.getId2(),
							emailform.getType(),
							getDataSource(request, "ilexnewDB")));
					request.setAttribute("emailflag", temp[0]);
					request.setAttribute("type", emailform.getType());
					request.setAttribute("name", name);
					request.setAttribute("typename", type);
					request.setAttribute("type", emailform.getType());

					// System.out.println("emailflag-------"+temp[0]);
					// System.out.println("type-------"+emailform.getType());
					// System.out.println("name-------"+name);
					// System.out.println("typename-------"+type);

					return (mapping.findForward("emailwindow"));
				}

				request.setAttribute("emailflag", temp[0]);
				// End :Added By Amit

				return (mapping.findForward("appendixdashboard"));
			}

			if (emailform.getType().equals("prm_appendix")) {
				request.setAttribute("id", emailform.getId1());

				// Added by Amit: For Email alert
				request.setAttribute("appendixid", emailform.getId2());
				request.setAttribute("type", emailform.getType());
				request.setAttribute("temp_type1", "Manage Status Report");
				request.setAttribute("typename", type);
				String temp[] = changestatus.split(",");
				request.setAttribute("emailflag", temp[0]);

				if (!temp[0].equalsIgnoreCase("0")) {

					name = Jobdao.getAppendixname(
							getDataSource(request, "ilexnewDB"),
							emailform.getId2());
					type = EditXmldao.getTypeName(emailform.getId1(), "MSA",
							getDataSource(request, "ilexnewDB"));
					emailform.setCustPOC(ViewList.getCustPOCEmail(
							emailform.getId1(), emailform.getId2(),
							emailform.getType(),
							getDataSource(request, "ilexnewDB")));

					request.setAttribute("name", name);
					request.setAttribute("typename", type);
					request.setAttribute("emailflag", temp[0]);

					request.setAttribute("type", emailform.getType());
					return (mapping.findForward("emailwindow"));

				}
				// End : Added By Amit

				return (mapping.findForward("dashboardpage"));
			}

			if (emailform.getType().equals("prm_job")) {
				request.setAttribute("jobid", emailform.getId1());
				request.setAttribute("type", emailform.getType());

				// Added by Amit: For Email alert

				request.setAttribute("temp_type1", "Manage Status Report");
				request.setAttribute("typename", type);
				String temp[] = changestatus.split(",");
				request.setAttribute("emailflag", temp[0]);

				if (!temp[0].equalsIgnoreCase("0")) {

					name = Jobdao.getAppendixname(
							getDataSource(request, "ilexnewDB"),
							emailform.getId2());
					type = EditXmldao.getTypeName(emailform.getId1(), "MSA",
							getDataSource(request, "ilexnewDB"));
					emailform.setCustPOC(ViewList.getCustPOCEmail(
							emailform.getId1(), emailform.getId2(),
							emailform.getType(),
							getDataSource(request, "ilexnewDB")));

					request.setAttribute("name", name);
					request.setAttribute("typename", type);
					request.setAttribute("emailflag", temp[0]);

					request.setAttribute("type", emailform.getType());
					return (mapping.findForward("emailwindow"));

				}
				// End : Added By Amit
				ActionForward fwd = new ActionForward();
				fwd.setPath("/JobDashboardAction.do?tabId="
						+ emailform.getTabId() + "&jobid=" + emailform.getId1()
						+ "&isClicked=");
				return fwd;
				// return( mapping.findForward( "jobdashboard" ) );
			}

			if (emailform.getType().equals("prm_activity")) {
				request.setAttribute("id", emailform.getId1());
				request.setAttribute("type", emailform.getType());
				return (mapping.findForward("dashboardpage"));
			}

			else {
				return (mapping.findForward("errorpage"));
			}
		} // End of getSend

		else {

			if (request.getParameter("MSA_Id") != null) {
				emailform.setId1(request.getParameter("MSA_Id"));
			}
			if (request.getAttribute("MSA_Id") != null) {
				emailform.setId1(request.getAttribute("MSA_Id").toString());
			}
			if (request.getParameter("Appendix_Id") != null) {
				emailform.setId2(request.getParameter("Appendix_Id"));
				emailform.setMSA_Id(IMdao.getMSAId(emailform.getId2(),
						getDataSource(request, "ilexnewDB")));
				emailform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						emailform.getMSA_Id()));
			}
			if (request.getAttribute("Appendix_Id") != null) {
				emailform
						.setId2(request.getAttribute("Appendix_Id").toString());
				emailform.setMSA_Id(IMdao.getMSAId(emailform.getId2(),
						getDataSource(request, "ilexnewDB")));
				emailform.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						emailform.getMSA_Id()));
			}
			if (request.getParameter("jobId") != null) {
				emailform.setId3(request.getParameter("jobId"));
			}

			if (request.getAttribute("jobId") != null) {
				emailform.setId3(request.getAttribute("jobId").toString());
			}

			if (request.getParameter("id") != null) // /used for sending status
													// report from dashboard
			{
				emailform.setId1(request.getParameter("id"));
			}

			if (request.getParameter("Type") != null) {
				emailform.setType(request.getParameter("Type"));
			}

			if (request.getAttribute("Type") != null) {
				emailform.setType("" + request.getAttribute("Type"));
			}

			if (emailform.getType().equals("Send POWO")) {
				String siteNumber = "";
				String siteCity = "";
				emailform.setJobId("" + request.getAttribute("Job_Id"));
				emailform.setPowoId("" + request.getAttribute("powoid"));
				String appendixid = IMdao.getAppendixId(emailform.getJobId(),
						getDataSource(request, "ilexnewDB"));
				emailform.setMSA_Id(IMdao.getMSAId(appendixid,
						getDataSource(request, "ilexnewDB")));
				siteNumber = IMdao.getJobSiteNumber(emailform.getJobId(),
						getDataSource(request, "ilexnewDB"));
				// add for subject change - -start
				siteCity = IMdao.getJobSiteCity(emailform.getJobId(),
						getDataSource(request, "ilexnewDB"));
				// End
				emailform.setOrder("");
				String emailPoWoContent = "PO/WO: " + emailform.getPowoId();

				if (request.getAttribute("order_type").equals("po"))
					emailform.setOrder("PO: " + emailform.getPowoId()
							+ siteNumber);
				if (request.getAttribute("order_type").equals("wo"))
					emailform.setOrder("WO: " + emailform.getPowoId()
							+ siteNumber);
				if (request.getAttribute("order_type").equals("powo")) {
					// add for subject change -start
					StringBuilder sbOrder = new StringBuilder();
					sbOrder.append("CNS PO/WO: ");
					sbOrder.append(emailform.getPowoId());
					sbOrder.append(" ");
					sbOrder.append("- Scheduled ");
					Timestamp poDeliverByDate = Jobdao.getPODeliverByDate(
							emailform.getPowoId(),
							getDataSource(request, "ilexnewDB"));

					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yy hh:mm a");
					try {
						sbOrder.append(sdf.format(poDeliverByDate));
					} catch (NullPointerException e) {
						sbOrder.append("");
					}

					sbOrder.append(" ");
					sbOrder.append(Appendixdao.getMsaname(
							getDataSource(request, "ilexnewDB"),
							emailform.getMSA_Id()));
					sbOrder.append(" ");
					sbOrder.append(siteCity);
					sbOrder.append(" ");
					sbOrder.append(siteNumber);
					// add for subject change - -End
					emailform.setOrder(sbOrder.toString());
				}
				emailform.setId1(emailform.getPowoId());
				emailform.setId2(emailform.getJobId()); // set job id at id2
				emailform
						.setOrder_type("" + request.getAttribute("order_type"));
				emailform.setAct_id("" + request.getAttribute("powoid")); // set
																			// POWO
																			// Id
				emailform.setPowo_type("" + request.getAttribute("powo_type"));
				emailform.setPowo_fromtype(""
						+ request.getAttribute("from_type"));
				boolean poDeliverableExist = PurchaseOrderDAO
						.isPoDeliverableExistForMobile(emailform.getId1(),
								getDataSource(request, "ilexnewDB"));

				if (poDeliverableExist)
					request.setAttribute("poDeliverableExist",
							poDeliverableExist);

				emailform.setType1("Send POWO");

				// emailform.setPo_wo_number( request.getAttribute(
				// "po_wo_number" ).toString() );

				emailform
						.setCheck_for_fileupload(MSAdao.getfile_upload_flag(
								emailform.getId1(),
								getDataSource(request, "ilexnewDB")));
				emailform
						.setName(Appendixdao.getMsaname(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1()));
				if (emailform.getType().equals(null))
					emailform.setType(EditXmldao.getTypeName(
							emailform.getId1(), "MSA",
							getDataSource(request, "ilexnewDB")));
				emailform
						.setFormaldocdate(MSAdao.getformaldocdatename(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1()));

				emailform.setSubject(emailform.getOrder());

				// if(messageContent.indexOf("~") >= 0) messageContent =
				// Domxmltaglist.addParameter(messageContent, order,
				// "Email Purchasr/Work Order", userId, "0",
				// getDataSource(request, "ilexnewDB"));
				if (emailform.getMessageContent().indexOf("~") >= 0) {

					String get42CharString = getStringOfGivenLength(42);

					emailform.setMessageContent(Domxmltaglist.addParameter(
							emailform.getMessageContent(), emailPoWoContent,
							"Email Purchasr/Work Order", userId,
							emailform.getId2(), emailform.getPowoId(),
							get42CharString,
							getDataSource(request, "ilexnewDB")));

					POWOdao.insertAndDeleteRecords(emailform.getPowoId(),
							get42CharString,
							getDataSource(request, "ilexnewDB"));
				}

				// emailform.setContent(emailform.getMessageContent());
				if (emailform.getMessageContent().contains("Sincerely,")) {
					String[] splitContent = emailform.getMessageContent()
							.split("Sincerely,");
					emailform.setContent(splitContent[0]);
					emailform.setContentSign("Sincerely," + splitContent[1]);
				} else {
					emailform.setContent(emailform.getMessageContent());
				}

				// emailform.setContent(
				// "For your review and records, the "+order+" is " +
				// "attached.\n\nPlease sign and fax your concurrence back to me at the number listed below. You may direct any comments or questions to me as well.\n\nSincerely,\n\n"+loginusername+"\nContingent Network Services\n4400 Port Union Road\nWest Chester, OH 45011\n(800) 506-9609\n(513)860-2105(facsimile)\nwww.contingent.net\n\nHow are we doing?Please let us know! www.contingent.net/satisfaction");
				request.setAttribute("name", name);
				request.setAttribute("typename", type);
				request.setAttribute("type", emailform.getType());
				request.setAttribute(
						"documentExists",
						AttachDocument.checkAttachedDocuments(
								emailform.getId1(),
								getDataSource(request, "ilexnewDB")));
			}

			if (emailform.getType().equals("MSA")) {

				emailform
						.setCheck_for_fileupload(MSAdao.getfile_upload_flag(
								emailform.getId1(),
								getDataSource(request, "ilexnewDB")));
				name = Appendixdao
						.getMsaname(getDataSource(request, "ilexnewDB"),
								emailform.getId1());
				// type = EditXmldao.getTypeName( emailform.getId1() , "MSA" ,
				// getDataSource( request , "ilexnewDB" ) );
				formaldocdate = MSAdao
						.getformaldocdatename(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1());

				emailform.setSubject(name + " MSA, " + formaldocdate);

				if (emailform.getMessageContent().indexOf("~") >= 0)
					emailform.setMessageContent(Domxmltaglist.addParameter(
							emailform.getMessageContent(), emailform.getId1(),
							"Email MSA", userId, "0",
							getDataSource(request, "ilexnewDB")));

				// emailform.setContent(emailform.getMessageContent());
				if (emailform.getMessageContent().contains("Sincerely,")) {
					String[] splitContent = emailform.getMessageContent()
							.split("Sincerely,");
					emailform.setContent(splitContent[0]);
					emailform.setContentSign("Sincerely," + splitContent[1]);
				} else {
					emailform.setContent(emailform.getMessageContent());
				}

				// emailform.setContent(
				// "For your review and records, the "+name+" "+"MSA, "+formaldocdate+" is "
				// +
				// "attached.\n\nPlease sign and fax your concurrence back to me at the number listed below. You may direct any comments or questions to me as well.\n\nSincerely,\n\n"+loginusername+"\nContingent Network Services\n4400 Port Union Road\nWest Chester, OH 45011\n(800) 506-9609\n(513)860-2105(facsimile)\nwww.contingent.net\n\nHow are we doing?Please let us know! www.contingent.net/satisfaction");

				emailform.setTypeName(emailform.getType());
			}

			if (emailform.getType().equals("Invoice Request")) {
				emailform.setId("" + request.getAttribute("id")); // set page id
				emailform.setJobIds("" + request.getAttribute("jobids"));
				emailform.setMSA_Id("");
				if (emailform.getJobIds().indexOf(",") > 0)
					emailform.setMSA_Id(IMdao.getMSAId(IMdao.getAppendixId(
							emailform.getJobIds().substring(0,
									emailform.getJobIds().indexOf(",")),
							getDataSource(request, "ilexnewDB")),
							getDataSource(request, "ilexnewDB")));
				else
					emailform.setMSA_Id(IMdao.getMSAId(IMdao.getAppendixId(
							emailform.getJobIds(),
							getDataSource(request, "ilexnewDB")),
							getDataSource(request, "ilexnewDB")));
				emailform.setId1(emailform.getMSA_Id());
				emailform.setId2(emailform.getId());
				emailform.setType("Invoice Request");
				emailform.setType1("Invoice Request");
				emailform.setAct_id(emailform.getJobIds()); // set job ids
				emailform.setInvtype1("" + request.getAttribute("invtype1"));
				emailform.setInvtype2("" + request.getAttribute("invtype2"));
				emailform
						.setCheck_for_fileupload(MSAdao.getfile_upload_flag(
								emailform.getId1(),
								getDataSource(request, "ilexnewDB")));
				emailform
						.setName(Appendixdao.getMsaname(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1()));
				if (emailform.getType() != null)
					emailform.setType(EditXmldao.getTypeName(
							emailform.getId1(), "MSA",
							getDataSource(request, "ilexnewDB")));
				emailform
						.setFormaldocdate(MSAdao.getformaldocdatename(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1()));

				emailform.setSubject("Invoice Request");
				emailform
						.setContent("For your review and records, the Invoice Request, is "
								+ "attached.\n\nPlease sign and fax your concurrence back to me at the number listed below. You may direct any comments or questions to me as well.");

				emailform
						.setContentSign("\n\nSincerely,\n\n"
								+ loginusername
								+ "\nContingent Network Services\n4400 Port Union Road\nWest Chester, OH 45011\n(800) 506-9609\n(513)860-2105(facsimile)\nmedius.contingent.com\n\nHow are we doing?Please let us know! medius.contingent.com/satisfaction");
				emailform.setTypeName(emailform.getType());

			}

			if (emailform.getType().equals("Appendix")
					|| emailform.getType().equals("PRMAppendix")) {
				emailform.setCheck_for_fileupload("");
				emailform
						.setName(Jobdao.getAppendixname(
								getDataSource(request, "ilexnewDB"),
								emailform.getId2()));
				emailform.setSubject(emailform.getName() + " Appendix");

				if (emailform.getMessageContent().indexOf("~") >= 0)
					emailform.setMessageContent(Domxmltaglist.addParameter(
							emailform.getMessageContent(), emailform.getId2(),
							"Email Appendix", userId, emailform.getId3(),
							getDataSource(request, "ilexnewDB")));
				if (emailform.getMessageContent().contains("Sincerely,")) {
					String[] splitContent = emailform.getMessageContent()
							.split("Sincerely,");
					emailform.setContent(splitContent[0]);
					emailform.setContentSign("Sincerely," + splitContent[1]);
				} else {
					emailform.setContent(emailform.getMessageContent());
				}

				// check for addendam job:-start
				emailform.setJobtype(com.mind.dao.PM.Jobdao.getJobtype(
						emailform.getJobId(),
						getDataSource(request, "ilexnewDB")));
				// check for addendam job:-start

			}

			emailform.setTemp_type1("");
			emailform.setTemp_ordertype("");
			request.setAttribute("temp_ordertype", "");

			if (emailform.getType().equals("Send POWO")) {
				emailform.setTemp_type1(emailform.getType1());
				emailform.setTemp_ordertype(emailform.getOrder_type());
			}

			if (emailform.getType1() != null) {
				if (emailform.getType1().equals("Invoice Request"))
					emailform.setTemp_type1(emailform.getType1());
			}

			if (emailform.getType().equals("prm_job")) {

				emailform.setSubject("Manage Status Report");
				emailform.setId2(IMdao.getAppendixId(emailform.getId1(),
						getDataSource(request, "ilexnewDB")));
				emailform
						.setName(com.mind.dao.PM.Jobdao.getJobname(
								emailform.getId1(),
								getDataSource(request, "ilexnewDB")));

				emailform.setContent(com.mind.dao.PRM.Appendixdao
						.getStatusreportforemail(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1(), emailform.getType()));
				emailform.setCheck_for_fileupload("U");

				emailform.setTypeName("");
				emailform.setType1("Manage Status Report");
				emailform.setTemp_type1("Manage Status Report");
			}

			if (emailform.getType().equals("prm_appendix")) {

				emailform.setSubject("Manage Status Report");
				emailform.setId2(emailform.getId1());
				emailform.setContent(com.mind.dao.PRM.Appendixdao
						.getStatusreportforemail(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1(), emailform.getType()));
				emailform.setCheck_for_fileupload("U");
				emailform
						.setName(Jobdao.getAppendixname(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1()));

				request.setAttribute("name", name);
				request.setAttribute("typename", "");
				request.setAttribute("type", "");
				emailform.setType1("Manage Status Report");
				emailform.setTemp_type1("Manage Status Report");
			}

			if (emailform.getType().equals("prm_activity")) {

				emailform.setSubject("Manage Status Report");
				emailform
						.setId2(IMdao.getAppendixIdforactivity(
								emailform.getId1(),
								getDataSource(request, "ilexnewDB")));
				emailform.setContent(com.mind.dao.PRM.Appendixdao
						.getStatusreportforemail(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1(), emailform.getType()));
				emailform.setCheck_for_fileupload("U");
				emailform
						.setName(com.mind.dao.PM.Activitydao.getActivityname(
								emailform.getId1(),
								getDataSource(request, "ilexnewDB")));

				emailform.setTypeName("");
				emailform.setType1("Manage Status Report");
				emailform.setTemp_type1("Manage Status Report");

			}

			emailform.setFrom((String) session.getAttribute("useremail"));
			emailform.setFromName((String) session.getAttribute("username"));

			emailform.setCustPOC(ViewList.getCustPOCEmail(emailform.getId1(),
					emailform.getId2(), emailform.getType(),
					getDataSource(request, "ilexnewDB")));

			if (emailform.getType().equals("MSA")) {
				if (request.getAttribute("MSA_Id") != null) {
					emailform.setId1("" + request.getAttribute("MSA_Id"));
				}
				emailform
						.setName(Appendixdao.getMsaname(
								getDataSource(request, "ilexnewDB"),
								emailform.getId1()));
				ownerType = "msa";
				msaid = "0";

				if (session.getAttribute("appendixSelectedOwners") != null
						|| session.getAttribute("appendixSelectedStatus") != null)
					request.setAttribute("specialCase", "specialCase");

				if (session.getAttribute("monthAppendix") != null
						|| session.getAttribute("weekAppendix") != null) {
					request.setAttribute("specialCase", "specialCase");
				}
				WebUtil.removeAppendixSession(session);

				// When the reset button is pressed remove every thing from the
				// session.
				if (request.getParameter("home") != null) {

					emailform.setSelectedStatus(null);
					emailform.setSelectedOwners(null);
					emailform.setPrevSelectedStatus("");
					emailform.setPrevSelectedOwner("");
					emailform.setOtherCheck(null);
					emailform.setMonthWeekCheck(null);
					// MSAdao.setValuesWhenReset(session);
					return (mapping.findForward("msalistpage"));

					// if(session.getAttribute("selectedOwners")!=null) {
					// selectedOwner =
					// session.getAttribute("selectedOwners").toString();
					// emailform.setSelectedOwners(session.getAttribute("tempOwner").toString().split(","));
					// }
					// if(session.getAttribute("selectedStatus")!=null) {
					// selectedStatus =
					// session.getAttribute("selectedStatus").toString();
					// emailform.setSelectedStatus(session.getAttribute("tempStatus").toString().split(","));
					// }

				}

				// System.out.println("111111111111111" +
				// session.getAttribute("MSASelectedStatus"));
				// System.out.println("222222222222222" +
				// session.getAttribute("selectedStatus"));

				// when the other check box is checked in the view selector.
				if (emailform.getOtherCheck() != null)
					session.setAttribute("otherCheck", emailform
							.getOtherCheck().toString());

				if (emailform.getSelectedStatus() != null) {
					for (int i = 0; i < emailform.getSelectedStatus().length; i++) {
						selectedStatus = selectedStatus + "," + "'"
								+ emailform.getSelectedStatus(i) + "'";
						tempStatus = tempStatus + ","
								+ emailform.getSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				if (emailform.getSelectedOwners() != null) {
					for (int j = 0; j < emailform.getSelectedOwners().length; j++) {
						selectedOwner = selectedOwner + ","
								+ emailform.getSelectedOwners(j);
						tempOwner = tempOwner + ","
								+ emailform.getSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}
				// End of the code.

				// The Menu Images are to be set in session respective of one
				// other.
				if (request.getParameter("month") != null) {
					session.setAttribute("monthMSA",
							request.getParameter("month").toString());
					monthMSA = request.getParameter("month").toString();
					session.removeAttribute("weekMSA");
					emailform.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekMSA", request
							.getParameter("week").toString());
					weekMSA = request.getParameter("week").toString();
					session.removeAttribute("monthMSA");
					emailform.setMonthWeekCheck("week");
				}

				// To set the values in the session when the Show button is
				// Pressed.
				// System.out.println("xxxxxxxx"+emailform.getGo()+"yyyyyyyyyy");
				// if(emailform.getGo()!= null && !emailform.getGo().equals(""))
				if (emailform.getGo() != null && !emailform.getGo().equals("")) {
					session.setAttribute("selectedOwners", selectedOwner);
					session.setAttribute("MSASelectedOwners", selectedOwner);
					session.setAttribute("tempOwner", tempOwner);
					session.setAttribute("selectedStatus", selectedStatus);
					session.setAttribute("MSASelectedStatus", selectedStatus);
					session.setAttribute("tempStatus", tempStatus);

					if (emailform.getMonthWeekCheck() != null) {
						if (emailform.getMonthWeekCheck().equals("week")) {
							session.removeAttribute("monthMSA");
							session.setAttribute("weekMSA", "0");
						}
						if (emailform.getMonthWeekCheck().equals("month")) {
							session.removeAttribute("weekMSA");
							session.setAttribute("monthMSA", "0");
						}

						if (emailform.getMonthWeekCheck().equals("clear")) {
							session.removeAttribute("monthMSA");
							session.removeAttribute("weekMSA");
						}

					}

					if (session.getAttribute("monthMSA") != null) {
						session.removeAttribute("weekMSA");
						monthMSA = session.getAttribute("monthMSA").toString();
					}
					if (session.getAttribute("weekMSA") != null) {
						session.removeAttribute("monthMSA");
						weekMSA = session.getAttribute("weekMSA").toString();
					}
					return (mapping.findForward("msalistpage"));
				} else // If not pressed see the values exists in the session or
						// not.
				{
					if (session.getAttribute("selectedOwners") != null) {
						selectedOwner = session.getAttribute("selectedOwners")
								.toString();
						emailform.setSelectedOwners(session
								.getAttribute("tempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("selectedStatus") != null) {
						selectedStatus = session.getAttribute("selectedStatus")
								.toString();
						emailform.setSelectedStatus(session
								.getAttribute("tempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("otherCheck") != null)
						emailform.setOtherCheck(session.getAttribute(
								"otherCheck").toString());
				}

				if (session.getAttribute("monthMSA") != null) {
					session.removeAttribute("weekMSA");
					monthMSA = session.getAttribute("monthMSA").toString();
				}
				if (session.getAttribute("weekMSA") != null) {
					session.removeAttribute("monthMSA");
					weekMSA = session.getAttribute("weekMSA").toString();
				}

				if (!chkOtherOwner) {
					emailform.setPrevSelectedStatus(selectedStatus);
					emailform.setPrevSelectedOwner(selectedOwner);
				}

				ownerList = MSAdao.getOwnerList(loginuserid,
						emailform.getOtherCheck(), ownerType, msaid,
						getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_msa",
						getDataSource(request, "ilexnewDB"));

				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

				// End of the code for the View Selector Part

			}

			if (emailform.getType().equals("Appendix")) {
				emailform
						.setName(Jobdao.getAppendixname(
								getDataSource(request, "ilexnewDB"),
								emailform.getId2()));
				emailform.setMSA_Id(IMdao.getMSAId(emailform.getId2(),
						getDataSource(request, "ilexnewDB")));
				ownerType = "appendix";

				if (request.getParameter("home") != null) // This is called when
															// reset button is
															// pressed from the
															// view selector.
				{
					emailform.setAppendixSelectedStatus(null);
					emailform.setAppendixSelectedOwners(null);
					emailform.setPrevSelectedStatus("");
					emailform.setPrevSelectedOwner("");
					emailform.setAppendixOtherCheck(null);
					emailform.setMonthWeekCheck(null);
					return (mapping.findForward("appendixlistpage"));
				}

				// These two parameter month/week are checked are kept in
				// session. ( View Images)
				if (request.getParameter("month") != null) {
					session.setAttribute("monthAppendix",
							request.getParameter("month").toString());

					session.removeAttribute("weekAppendix");
					emailform.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekAppendix",
							request.getParameter("week").toString());

					session.removeAttribute("monthAppendix");
					emailform.setMonthWeekCheck("week");
				}

				if (emailform.getAppendixOtherCheck() != null) // OtherCheck box
																// status is
																// kept in
																// session.
					session.setAttribute("appendixOtherCheck", emailform
							.getAppendixOtherCheck().toString());

				// When Status Check Boxes are Clicked :::::::::::::::::
				if (emailform.getAppendixSelectedStatus() != null) {
					for (int i = 0; i < emailform.getAppendixSelectedStatus().length; i++) {
						selectedStatus = selectedStatus + "," + "'"
								+ emailform.getAppendixSelectedStatus(i) + "'";
						tempStatus = tempStatus + ","
								+ emailform.getAppendixSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				// When Owner Check Boxes are Clicked :::::::::::::::::
				if (emailform.getAppendixSelectedOwners() != null) {
					for (int j = 0; j < emailform.getAppendixSelectedOwners().length; j++) {
						selectedOwner = selectedOwner + ","
								+ emailform.getAppendixSelectedOwners(j);
						tempOwner = tempOwner + ","
								+ emailform.getAppendixSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}

				if (emailform.getGo() != null && !emailform.getGo().equals("")) {
					session.setAttribute("appendixSelectedOwners",
							selectedOwner);
					session.setAttribute("appSelectedOwners", selectedOwner);
					session.setAttribute("appendixTempOwner", tempOwner);
					session.setAttribute("appendixSelectedStatus",
							selectedStatus);
					session.setAttribute("appSelectedStatus", selectedStatus);
					session.setAttribute("appendixTempStatus", tempStatus);

					if (emailform.getMonthWeekCheck() != null) {
						if (emailform.getMonthWeekCheck().equals("week")) {
							session.removeAttribute("monthAppendix");
							session.setAttribute("weekAppendix", "0");

						}
						if (emailform.getMonthWeekCheck().equals("month")) {
							session.removeAttribute("weekAppendix");
							session.setAttribute("monthAppendix", "0");

						}
						if (emailform.getMonthWeekCheck().equals("clear")) {
							session.removeAttribute("monthAppendix");
							session.removeAttribute("weekAppendix");
						}

					}
					return (mapping.findForward("appendixlistpage"));
				} else {
					if (session.getAttribute("appendixSelectedOwners") != null) {
						selectedOwner = session.getAttribute(
								"appendixSelectedOwners").toString();
						emailform.setAppendixSelectedOwners(session
								.getAttribute("appendixTempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("appendixSelectedStatus") != null) {
						selectedStatus = session.getAttribute(
								"appendixSelectedStatus").toString();
						emailform.setAppendixSelectedStatus(session
								.getAttribute("appendixTempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("appendixOtherCheck") != null)
						emailform.setAppendixOtherCheck(session.getAttribute(
								"appendixOtherCheck").toString());

					if (session.getAttribute("monthAppendix") != null) {
						session.removeAttribute("weekAppendix");
						emailform.setMonthWeekCheck("month");
					}
					if (session.getAttribute("weekAppendix") != null) {
						session.removeAttribute("monthAppendix");
						emailform.setMonthWeekCheck("week");
					}

				}
				if (!chkOtherOwner) {
					emailform.setPrevSelectedStatus(selectedStatus);
					emailform.setPrevSelectedOwner(selectedOwner);
				}
				ownerList = MSAdao.getOwnerList(loginuserid,
						emailform.getAppendixOtherCheck(), ownerType,
						emailform.getMSA_Id(),
						getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_appendix",
						getDataSource(request, "ilexnewDB"));

				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");
			}

			if (emailform.getType().equals("MSA")) {
				request.setAttribute("MSA_Id", emailform.getId1());
				String msaStatus = MSAdao.getMSAStatus(emailform.getId1(),
						getDataSource(request, "ilexnewDB"));
				String msaUploadCheck = MSAdao
						.getMSAUploadCheck(emailform.getId1(),
								getDataSource(request, "ilexnewDB"));
				String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
						emailform.getId1(), "", "", "", loginuserid,
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("list", list);
				request.setAttribute("status", msaStatus);
				request.setAttribute("uploadstatus", msaUploadCheck);
			}

			if (emailform.getType().equals("Appendix")) {
				String appendixStatus = Appendixdao.getAppendixStatus(
						emailform.getId2(), emailform.getMSA_Id(),
						getDataSource(request, "ilexnewDB"));
				String addendumlist = Appendixdao
						.getAddendumsIdName(emailform.getId2(),
								getDataSource(request, "ilexnewDB"));
				String addendumid = com.mind.dao.PM.Addendumdao
						.getLatestAddendumId(emailform.getId2(),
								getDataSource(request, "ilexnewDB"));
				String list = Menu.getStatus("pm_appendix",
						appendixStatus.charAt(0), emailform.getMSA_Id(),
						emailform.getId2(), "", "", loginuserid,
						getDataSource(request, "ilexnewDB"));
				String appendixType = Appendixdao.getAppendixPrType(
						emailform.getMSA_Id(), emailform.getId2(),
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixType", appendixType);

				request.setAttribute("appendixStatusList", list);
				request.setAttribute("Appendix_Id", emailform.getId2());
				request.setAttribute("MSA_Id", emailform.getMSA_Id());
				request.setAttribute("appendixStatus", appendixStatus);
				request.setAttribute("latestaddendumid", addendumid);
				request.setAttribute("addendumlist", addendumlist);
			}
			if (request.getParameter("opendiv") != null) {
				opendiv = request.getParameter("opendiv");
				request.setAttribute("opendiv", opendiv);
			}
			if (emailform.getType() != null
					&& (emailform.getType().equalsIgnoreCase("prm_appendix"))
					|| emailform.getType().equals("PRMAppendix")) {

				/* Start : Appendix Dashboard View Selector Code */

				if (emailform.getJobOwnerOtherCheck() != null
						&& !emailform.getJobOwnerOtherCheck().equals("")) {
					session.setAttribute("jobOwnerOtherCheck",
							emailform.getJobOwnerOtherCheck());
				}

				if (request.getParameter("opendiv") != null) {
					request.setAttribute("opendiv",
							request.getParameter("opendiv"));
				}
				if (request.getParameter("resetList") != null
						&& request.getParameter("resetList")
								.equals("something")) {
					WebUtil.setDefaultAppendixDashboardAttribute(session);
					request.setAttribute("type", "AppendixViewSelector");
					request.setAttribute("appendixId", emailform.getId2());
					request.setAttribute("msaId", emailform.getMSA_Id());

					return mapping.findForward("temppage");

				}

				if (emailform.getJobSelectedStatus() != null) {
					for (int i = 0; i < emailform.getJobSelectedStatus().length; i++) {
						jobSelectedStatus = jobSelectedStatus + "," + "'"
								+ emailform.getJobSelectedStatus(i) + "'";
						jobTempStatus = jobTempStatus + ","
								+ emailform.getJobSelectedStatus(i);
					}

					jobSelectedStatus = jobSelectedStatus.substring(1,
							jobSelectedStatus.length());
					jobSelectedStatus = "(" + jobSelectedStatus + ")";
				}

				if (emailform.getJobSelectedOwners() != null) {
					for (int j = 0; j < emailform.getJobSelectedOwners().length; j++) {
						jobSelectedOwner = jobSelectedOwner + ","
								+ emailform.getJobSelectedOwners(j);
						jobTempOwner = jobTempOwner + ","
								+ emailform.getJobSelectedOwners(j);
					}
					jobSelectedOwner = jobSelectedOwner.substring(1,
							jobSelectedOwner.length());
					if (jobSelectedOwner.equals("0")) {
						jobSelectedOwner = "%";
					}
					jobSelectedOwner = "(" + jobSelectedOwner + ")";
				}

				if (request.getParameter("showList") != null
						&& request.getParameter("showList").equals("something")) {

					session.setAttribute("jobSelectedOwners", jobSelectedOwner);
					session.setAttribute("jobTempOwner", jobTempOwner);
					session.setAttribute("jobSelectedStatus", jobSelectedStatus);
					session.setAttribute("jobTempStatus", jobTempStatus);
					session.setAttribute("timeFrame",
							emailform.getJobMonthWeekCheck());
					session.setAttribute("jobOwnerOtherCheck",
							emailform.getJobOwnerOtherCheck());
					request.setAttribute("type", "AppendixViewSelector");
					request.setAttribute("appendixId", emailform.getId2());
					request.setAttribute("msaId", emailform.getMSA_Id());

					return mapping.findForward("temppage");

				} else {
					if (session.getAttribute("jobSelectedOwners") != null) {
						jobSelectedOwner = session.getAttribute(
								"jobSelectedOwners").toString();
						emailform.setJobSelectedOwners(session
								.getAttribute("jobTempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("jobSelectedStatus") != null) {
						jobSelectedStatus = session.getAttribute(
								"jobSelectedStatus").toString();
						emailform.setJobSelectedStatus(session
								.getAttribute("jobTempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("jobOwnerOtherCheck") != null) {
						emailform.setJobOwnerOtherCheck(session.getAttribute(
								"jobOwnerOtherCheck").toString());
					}
					if (session.getAttribute("timeFrame") != null) {
						emailform.setJobMonthWeekCheck(session.getAttribute(
								"timeFrame").toString());
					}
				}

				jobOwnerList = MSAdao
						.getOwnerList(loginuserid,
								emailform.getJobOwnerOtherCheck(),
								"prj_job_new", emailform.getId2(),
								getDataSource(request, "ilexnewDB"));
				jobStatusList = MSAdao.getStatusList("prj_job_new",
						getDataSource(request, "ilexnewDB"));

				if (jobOwnerList.size() > 0)
					jobOwnerListSize = jobOwnerList.size();

				request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
				request.setAttribute("jobStatusList", jobStatusList);
				request.setAttribute("jobOwnerList", jobOwnerList);
				/* End : Appendix Dashboard View Selector Code */

				String contractDocoumentList = com.mind.dao.PM.Appendixdao
						.getcontractDocoumentMenu(emailform.getId2(),
								getDataSource(request, "ilexnewDB"));
				request.setAttribute("contractDocMenu", contractDocoumentList);
				String appendixcurrentstatus = Appendixdao
						.getCurrentstatus(emailform.getId2(),
								getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixcurrentstatus",
						appendixcurrentstatus);
				request.setAttribute(
						"msaId",
						IMdao.getMSAId(emailform.getId2(),
								getDataSource(request, "ilexnewDB")));
				request.setAttribute("appendixid", emailform.getId2());
				request.setAttribute("viewjobtype", emailform.getViewjobtype());
				request.setAttribute("ownerId", emailform.getOwnerId());
			}

			request.setAttribute("type", emailform.getType());
			return (mapping.findForward("emailwindow"));
		}
	}

	private File getAttachment(EmailForm emailform, String sessionid,
			Document document, String fileName, String loginuserid)
			throws FileNotFoundException, IOException {
		Calendar calendar = Calendar.getInstance();
		String tempFolderName = String.valueOf(calendar.getTimeInMillis())
				+ "-" + loginuserid;
		File tempFolder = new File(Util.getTemp() + "/ilex/" + tempFolderName);
		tempFolder.mkdirs();
		FileOutputStream fw = null;
		fw = new FileOutputStream(Util.getTemp() + "/ilex/" + tempFolderName
				+ "/" + fileName + "_" + emailform.getId1() + ".pdf");

		fw.write(document.getFileBytes());
		fw.close();
		File f6 = null;
		f6 = new File(Util.getTemp() + "/ilex/" + tempFolderName + "/"
				+ fileName + "_" + emailform.getId1() + ".pdf");
		return f6;
	}

	/**
	 * Set bean for Document attachment.
	 * 
	 * @param request
	 * @param emailform
	 * @param i
	 * @throws Exception
	 */
	private void setAttachDocumentForPOWO(HttpServletRequest request,
			EmailForm emailform) throws Exception {

		if (emailform.getAttachPOWO().length > 0) {
			AttachDocument attachDocument = new AttachDocument();
			Document[] documents = attachDocument.getDocument(
					emailform.getAttachPOWO(), emailform.getId1(),
					getDataSource(request, "ilexnewDB"));
			String path = Util.getTemp();
			File[] files = new File[documents.length];
			String[] fileNames = new String[documents.length];

			for (int k = 0; k < documents.length; k++) {
				if (documents[k].getFileName() != null
						&& !documents[k].getFileName().equals("")) {
					String fileName = getFileNameExt(
							documents[k].getFileName(),
							documents[k].getMimeType());
					String filePath = path + "/" + fileName;
					Util.writeFile(documents[k].getFileBytes(), filePath);
					files[k] = new File(filePath);
					fileNames[k] = fileName;
				}
			}
			emailform.setDocuments(files);
			emailform.setDocumentNames(fileNames);

		}
	}

	private String getFileNameExt(String fileName, String ext) {
		if (!fileName.contains("." + ext)) {
			fileName = fileName + "." + ext;
		}
		return fileName;
	}

	/**
	 * Discription: This mehtod is used to generate bytes for PO/WO pdf.
	 * 
	 * @param type
	 * @param request
	 */
	public static byte[] getBuffer(String powoId, String type, String userId,
			HttpServletRequest request, DataSource ds) {

		String imagePath = WebUtil.getRealPath(request, "images")
				+ "/clogo.jpg";
		String imagePath1 = WebUtil.getRealPath(request, "images")
				+ "/checkbox.gif";
		String fileName = "PoFinal";
		String fontPath = WebUtil.getRealPath(request, "/images")
				+ "/ARIALN.TTF";
		String imageBulletPath = WebUtil.getRealPath(request, "/images")
				+ "/Bullet.gif";
		String WONwConfigImg = WebUtil.getRealPath(request, "/images")
				+ "/witechfooter.jpg";
		String WONwConfigImgheader = WebUtil.getRealPath(request, "/images")
				+ "/witechheader.jpg";
		String icChrysler = WebUtil.getRealPath(request, "/images")
				+ "/chrysler.jpg";
		String icSnapOn = WebUtil.getRealPath(request, "/images") + "/snap.jpg";
		String icCompuCom = WebUtil.getRealPath(request, "/images")
				+ "/compu.jpg";
		byte[] buffer = null;

		if (type.equalsIgnoreCase("PO")) { // If we have to send the PO
			POPdf po = new POPdf();
			buffer = po.viewPO(powoId, fileName, imagePath, fontPath, userId,
					ds, imageBulletPath);
			POWODetaildao.updateEmailPOWO(powoId, "PO", userId, ds);
		}

		if (type.equalsIgnoreCase("WO")) {// If we have to send the WO
			WOPdf wo = new WOPdf();
			buffer = wo.viewwo(powoId, imagePath, imagePath1, fontPath, userId,
					ds, WONwConfigImg, WONwConfigImgheader, icSnapOn,
					icChrysler, icCompuCom);
			POWODetaildao.updateEmailPOWO(powoId, "WO", userId, ds);
		}

		return buffer;
	}

	/**
	 * Method is used to send the Inventory PO to DocM.
	 * 
	 * @param id
	 * @param type
	 * @param buffer
	 * @return
	 */
	public static boolean sendDoc(String id, String type, byte[] buffer)
			throws Exception {
		boolean sendToDocM = false;
		String docId = EntityManager.approveEntity(id, type, buffer); // Send
																		// the
																		// Inventory
																		// PO to
																		// DocM
																		// Interface
		return sendToDocM;
	}

	/**
	 * Method is used to retreive the byte[] buffer for the inventory PO
	 * document from DocM.
	 * 
	 * @param id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public static byte[] retrieveDoc(String id, String type) throws Exception {
		byte[] buffer = null;
		Document document = null;
		if (type.equalsIgnoreCase(type)) {
			document = EntityManager.getDocument(id, type);
			buffer = document.getFileBytes();
		}
		return buffer;
	}

	/**
	 * Method used to get the document Id of the inventory PO
	 * 
	 * @param Id
	 * @param type
	 * @return
	 */
	public static Long getDocumentId(String Id, String type) {
		Long docId = null;
		try {
			docId = Long.valueOf(EntityManager.getDocumentId(Id, type));
		} catch (DocumentIdNotFoundException e) {
			logger.debug("Documnet Id not found for po " + Id);
		} catch (Exception e) {
			logger.warn("getDocumentId(String, String) - exception ignored", e);

			// e.printStackTrace();
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getDocumentId(String, String) - Document Id is :"
					+ docId);
		}
		return docId;
	}

	/**
	 * Method is used to interact with DocM while sending Inventory PO.
	 * 
	 * @param Id
	 * @param type
	 * @param buffer
	 * @return
	 */
	public static boolean docmInteractionException(String Id, String type,
			byte[] buffer) {
		boolean interaction = false;
		try {
			checkExistence(Id, type);
			sendDoc(Id, type, buffer);
			retrieveDoc(Id, type);
		} catch (Exception e) {
			logger.error("docmInteractionException(String, String, byte[])", e);
			interaction = true;
		}
		return interaction;
	}

	/**
	 * Discription this method check the Existence of PO in DocM
	 * 
	 * @param id
	 * @param type
	 * @param request
	 * @return
	 */
	public static boolean checkExistence(String id, String type)
			throws Exception {
		boolean setToDraft = false;
		if (getDocumentId(id, type) != null) {
			EntityManager.setEntityToDraft(id, type);
		}
		return setToDraft;
	}

	private static final String dCase = "abcdefghijklmnopqrstuvwxyz";
	private static final String uCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	// private static final String sChar = "!@#$%^&*";
	private static final String intChar = "0123456789";
	private static Random r = new Random();

	public static String getStringOfGivenLength(int lenght) {
		String pass = "";
		// System.out.println("Generating pass...");
		while (pass.length() != lenght) {
			int rPick = r.nextInt(3);
			if (rPick == 0) {
				int spot = r.nextInt(25);
				pass += dCase.charAt(spot);
			} else if (rPick == 1) {
				int spot = r.nextInt(25);
				pass += uCase.charAt(spot);
			} else if (rPick == 2) {
				int spot = r.nextInt(9);
				pass += intChar.charAt(spot);
			}
		}
		return pass;
	}
}
