package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.dao.PVS.Pvsdao;

public class StateCountryAction extends com.mind.common.IlexAction{
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		HttpSession session = request.getSession( true );
		
		String countryId = "0";
		
		countryId = Pvsdao.getStateCountryMatch(request.getParameter("stateid"),getDataSource(request, "ilexnewDB"));
		response.setContentType("text/html");
		
		response.getWriter().write(countryId);
		return null;
	}
}
