//
//
//
//@ Project : Ilex
//@ File Name : EntityHistoryAction.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.common.actions;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.Document;
import com.mind.bean.docm.DocumentMasterDTO;
import com.mind.bean.docm.Field;
import com.mind.bean.docm.MetaData;
import com.mind.docm.dao.DocumentMasterDao;
import com.mind.docm.dao.EntityManager;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class EntityHistoryAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(EntityHistoryAction.class);

	// Use Case
	/**
	 * @name viewHistory
	 * @purpose This method is used when the user request to see the History of
	 *          documents for Ilex Entity in DocM
	 * @steps 1. User request to see if there is any history available for ilex
	 *        Entity
	 * 
	 * @param ActionMapping
	 *            mapping
	 * @param ActionForm
	 *            form
	 * @param HttpServletRequest
	 *            request
	 * @param HttpServletResponse
	 *            response
	 * @exception Exception
	 * @return
	 * @returnDescription control flow to view the JSP
	 */
	public ActionForward viewHistory(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String docId = null;
		String entityId = (request.getParameter("entityId") != null) ? request
				.getParameter("entityId") : "";
		String entityType = (request.getParameter("entityType") != null) ? request
				.getParameter("entityType") : "";
		String MSA_ID = (request.getParameter("MSA_Id") != null) ? request
				.getParameter("MSA_Id") : "";
		String appendixid = (request.getParameter("appendixid") != null) ? request
				.getParameter("appendixid") : "";
		String fromPage = (request.getParameter("fromPage") != null) ? request
				.getParameter("fromPage") : "";

		request.setAttribute("fromPage", fromPage);
		request.setAttribute("entityType", entityType);
		request.setAttribute("entityId", entityId);

		if (logger.isDebugEnabled()) {
			logger.debug("viewHistory(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - entity ID is :: "
					+ entityId);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("viewHistory(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - entity ID is :: "
					+ entityType);
		}

		// request.setAttribute("MSA_ID",MSA_ID);
		// request.setAttribute("appendixId",appendixid);
		request.setAttribute("SourceType", "Ilex");

		try {
			docId = EntityManager.getDocumentId(entityId, entityType);
			request.setAttribute("docId", docId);
			request.setAttribute("msastatus", request.getParameter("msastatus"));
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("viewHistory(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Document Id not found");
			}
		}
		return mapping.findForward("DocMHistory");
	}

	// Use Case
	/**
	 * @name viewFile
	 * @purpose This method is used to view the Document from DocM in Ilex
	 * @steps 1. User request to see the Document
	 * 
	 * @param ActionMapping
	 *            mapping
	 * @param ActionForm
	 *            form
	 * @param HttpServletRequest
	 *            request
	 * @param HttpServletResponse
	 *            response
	 * @exception Exception
	 * @return
	 * @returnDescription control flow to see the document
	 */
	public ActionForward viewFile(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String entityId = null;
		String entityType = null;
		Document document = null;
		String fileId = null;
		ServletOutputStream outStream = null;
		DocumentMasterDao docMasterDao = new DocumentMasterDao();
		DocumentMasterDTO bean = new DocumentMasterDTO();
		entityId = (request.getParameter("entityId") != null) ? request
				.getParameter("entityId") : "";
		entityType = (request.getParameter("entityType") != null) ? request
				.getParameter("entityType") : "";
		try {
			document = EntityManager.getDocument(entityId, entityType);
			MetaData metaData = (MetaData) document.getMetaData();
			Field[] field = metaData.getFields();
			for (int i = 0; i < field.length; i++) {
				if (field[i].getName().equalsIgnoreCase("dm_doc_file_id")) {
					fileId = (field[i].getValue());
				}
			}
			docMasterDao.getFileData(fileId, bean);
			byte[] byteFile = docMasterDao.openFile(fileId);
			response.setContentType("application/" + bean.getMIME());
			String filename = bean.getFilename();
			if (filename.indexOf(".") > 0)
				filename = filename.substring(0, filename.indexOf("."));
			response.setHeader("Cache-Control",
					"must-revalidate, post-check=0, pre-check=0");
			response.setHeader(
					"Content-disposition",
					"attachment;inline;filename=" + filename + "."
							+ bean.getMIME() + ";size=" + bean.getSize());
			outStream = response.getOutputStream();
			outStream.write(byteFile);
			return null;

		} catch (Exception e) {
			logger.error(
					"viewFile(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"viewFile(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		return null;
	}

	// Use Case
	/**
	 * @name modifyDocument
	 * @purpose This method is used to modify the document in DocM from Ilex
	 * @steps 1. User request to see the Document
	 * 
	 * @param ActionMapping
	 *            mapping
	 * @param ActionForm
	 *            form
	 * @param HttpServletRequest
	 *            request
	 * @param HttpServletResponse
	 *            response
	 * @exception Exception
	 * @return
	 * @returnDescription control flow to modify the document
	 */
	public ActionForward modifyDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String docId = "";
		String libId = "";
		String status = "";

		String entityId = (request.getParameter("entityId") != null) ? request
				.getParameter("entityId") : "";
		String entityType = (request.getParameter("entityType") != null) ? request
				.getParameter("entityType") : "";
		String appendixid = (request.getParameter("appendixid") != null) ? request
				.getParameter("appendixid") : "";
		String MSA_ID = (request.getParameter("MSA_Id") != null) ? request
				.getParameter("MSA_Id") : "";
		request.setAttribute("entityType", entityType);
		request.setAttribute("entityId", entityId);
		request.setAttribute("appendixId", appendixid);
		request.setAttribute("MSA_ID", MSA_ID);
		try {
			docId = EntityManager.getDocumentId(entityId, entityType);
			libId = EntityManager.getLibraryId(entityId, entityType);
			status = EntityManager.getFieldValue(docId, "status");
			request.setAttribute("docId", docId);
			request.setAttribute("libId", libId);
			request.setAttribute("SourceType", "Ilex");
		} catch (Exception e) {
			logger.error(
					"modifyDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			request.setAttribute("docId", docId);
			request.setAttribute("libId", libId);
			request.setAttribute("SourceType", "Ilex");
			logger.error(
					"modifyDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		// dispatch=((status.equalsIgnoreCase("Approved") ||
		// status.equalsIgnoreCase("Draft"))?"modifyDocument":"viewDocument");
		return mapping.findForward("modifyDocument");
	}

	// Use Case
	/**
	 * @name addSupplement
	 * @purpose This method is used to add the supplement document in DocM from
	 *          the Ilex against any Entity
	 * @steps 1. User request to add the Supplement Document
	 * 
	 * @param ActionMapping
	 *            mapping
	 * @param ActionForm
	 *            form
	 * @param HttpServletRequest
	 *            request
	 * @param HttpServletResponse
	 *            response
	 * @exception Exception
	 * @return
	 * @returnDescription control flow to add the document
	 */

	public ActionForward addSupplement(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String libId = "";
		String entityId = null;
		String entityType = null;
		String msaId = null;
		String appendixId = null;
		String jobId = null;
		String poId = null;
		String fromPage = null;
		String linkLibName = null;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		entityId = (request.getParameter("entityId") != null) ? request
				.getParameter("entityId") : "";
		entityType = (request.getParameter("entityType") != null) ? request
				.getParameter("entityType") : "";
		jobId = (request.getParameter("jobId") != null) ? request
				.getParameter("jobId") : "";
		poId = (request.getParameter("poId") != null) ? request
				.getParameter("poId") : "";
		appendixId = (request.getParameter("appendixId") != null) ? request
				.getParameter("appendixId") : "";
		msaId = (request.getParameter("msaId") != null) ? request
				.getParameter("msaId") : "";
		fromPage = (request.getParameter("fromPage") != null) ? request
				.getParameter("fromPage") : "";
		linkLibName = (request.getParameter("linkLibName") != null) ? request
				.getParameter("linkLibName") : "";
		Map<String, Object> map = null;

		if (jobId != null && !jobId.trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(jobId, loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		try {
			libId = EntityManager.getLibraryId(entityId, entityType);
			request.setAttribute("libId", libId);
			request.setAttribute("msaId", msaId);
			request.setAttribute("appendixId", appendixId);
			request.setAttribute("jobId", jobId);
			request.setAttribute("poId", poId);
			request.setAttribute("SourceType", "Ilex");
			request.setAttribute("linkLibName", linkLibName);
			request.setAttribute("fromPage", fromPage);

		} catch (Exception e) {
			logger.error(
					"addSupplement(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"addSupplement(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		return mapping.findForward("addsupplement");
	}

	// Use Case
	/**
	 * @name supplementHistory
	 * @purpose This method is used to see the supplement document hostory in
	 *          DocM from the Ilex against any Entity
	 * @steps 1. User request to see the Supplement Document History
	 * 
	 * @param ActionMapping
	 *            mapping
	 * @param ActionForm
	 *            form
	 * @param HttpServletRequest
	 *            request
	 * @param HttpServletResponse
	 *            response
	 * @exception Exception
	 * @return
	 * @returnDescription control flow to see the document history
	 */
	public ActionForward supplementHistory(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String entityType = null;
		String msaId = null;
		String appendixId = null;
		String jobId = null;
		String linkLibName = null;
		String libId = null;
		String linkTo = null;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String fromPage = (request.getParameter("fromPage") != null) ? request
				.getParameter("fromPage") : "";

		entityType = (request.getParameter("entityType") != null) ? request
				.getParameter("entityType") : "";
		linkLibName = (request.getParameter("linkLibName") != null) ? request
				.getParameter("linkLibName") : "";
		jobId = (request.getParameter("jobId") != null) ? request
				.getParameter("jobId") : "";
		appendixId = (request.getParameter("appendixId") != null) ? request
				.getParameter("appendixId") : "";
		msaId = (request.getParameter("msaId") != null) ? request
				.getParameter("msaId") : "";
		linkTo = (request.getParameter("linkTo") != null) ? request
				.getParameter("linkTo") : "";
		Map<String, Object> map;

		if (jobId != null && !jobId.trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(jobId, loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		try {
			libId = EntityManager.getLibraryId("", entityType);
			request.setAttribute("libId", libId);
			request.setAttribute("msaId", msaId);
			request.setAttribute("appendixId", appendixId);
			request.setAttribute("jobId", jobId);
			request.setAttribute("entityType", entityType);
			request.setAttribute("SourceType", "Ilex");
			request.setAttribute("linkLibName", linkLibName);
			request.setAttribute("searchSupplements", "yes");
			request.setAttribute("linkTo", linkTo);
			request.setAttribute("fromPage", fromPage);

			// System.out.println("Libraray Id is ::: " + libId);
			// System.out.println("msaId Id is ::: " + msaId);
			// System.out.println("appendixId Id is ::: " + appendixId);
			// System.out.println("jobId Id is ::: " + jobId);
			// System.out.println("entityType is :: " + entityType);

		} catch (Exception e) {
			logger.error(
					"supplementHistory(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"supplementHistory(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		return mapping.findForward("supplementHistory");
	}
}
