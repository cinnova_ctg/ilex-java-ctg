package com.mind.common.actions;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.OpenSearchWindowTempBean;
import com.mind.common.dao.OpenSearchWindowTempdao;
import com.mind.common.formbean.OpenSearchWindowTempForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class OpenSearchWindowTempAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OpenSearchWindowTempForm opensearchwindowtempform = (OpenSearchWindowTempForm) form;
		ArrayList list = new ArrayList();
		ArrayList templist = new ArrayList();
		String loginuserid = (String) request.getSession().getAttribute(
				"userid");
		int size = 0;
		String clr = "no";

		// System.out.println("XXXX1 type action "+request.getParameter( "type"
		// ));

		if (request.getParameter("checknetmedx") != null) {
			opensearchwindowtempform.setChecknetmedx(request
					.getParameter("checknetmedx"));
			request.setAttribute("checknetmedx",
					request.getParameter("checknetmedx"));

		}

		if (request.getParameter("type") != null) {
			opensearchwindowtempform.setType(request.getParameter("type"));
			request.setAttribute("restype", request.getParameter("type"));
		}

		// System.out.println("XXXX2 from action "+request.getParameter( "from"
		// ));

		if (request.getParameter("from") != null) {
			opensearchwindowtempform.setFrom(request.getParameter("from"));
		}

		if (request.getParameter("fromflag") != null) {
			opensearchwindowtempform.setFromflag(request
					.getParameter("fromflag"));
		}

		// System.out.println("XXXX3 activity_Id action "+request.getParameter(
		// "activity_Id" ));

		if (request.getParameter("activity_Id") != null) {
			opensearchwindowtempform.setActivity_Id(request
					.getParameter("activity_Id"));
		}

		if (request.getParameter("addendum_id") != null) {
			opensearchwindowtempform.setAddendum_id(request
					.getParameter("addendum_id"));
		}

		if (request.getParameter("resourceListType") != null) {
			opensearchwindowtempform.setResourceListType(request
					.getParameter("resourceListType"));
		}

		// System.out.println("XXXX4  action ");
		/* by hamid for selecting multiple resources start */
		if (request.getParameter("MSA_Id") != null) {
			opensearchwindowtempform.setMSA_Id(request.getParameter("MSA_Id"));

		}

		if (request.getParameter("ref") != null) {
			opensearchwindowtempform.setRef(request.getParameter("ref"));

		}

		if (request.getParameter("ok") != null) {

			int checklength = 0;
			String index = "";
			// String index_clr = "";
			String index_detail_id = "";
			int k = 0;
			if (opensearchwindowtempform.getCheck() != null) {
				checklength = opensearchwindowtempform.getCheck().length;
			}

			if (checklength > 0) {
				for (int i = 0; i < checklength; i++) {
					index = index + opensearchwindowtempform.getCheck(i) + ",";

					if (opensearchwindowtempform.getChecknetmedx()
							.equalsIgnoreCase("Y")
							&& opensearchwindowtempform.getType()
									.equalsIgnoreCase("Labor")) {
						for (int j = 0; j < opensearchwindowtempform
								.getDetail_id().length; j++) {
							if (opensearchwindowtempform.getCheck(i).equals(
									opensearchwindowtempform.getDetail_id(j))) {
								// index_clr
								// =index_clr+opensearchwindowtempform.getClr(j)+",";
								index_detail_id = index_detail_id
										+ opensearchwindowtempform
												.getDetail_id(j) + ",";
							}
						}
					}

				}
				if (opensearchwindowtempform.getChecknetmedx()
						.equalsIgnoreCase("Y")
						&& opensearchwindowtempform.getType().equalsIgnoreCase(
								"Labor")) {
					// index_clr=index_clr.substring(0,index_clr.length()-1);
					index_detail_id = index_detail_id.substring(0,
							index_detail_id.length() - 1);
				}

				index = index.substring(0, index.length() - 1);
			}

			OpenSearchWindowTempdao.addResourcesInTempTable(index, request
					.getSession().getId() + "",
					opensearchwindowtempform.getType(),
					opensearchwindowtempform.getActivity_Id(), index_detail_id,
					opensearchwindowtempform.getFrom(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("type", opensearchwindowtempform.getType());
			request.setAttribute("Activity_Id",
					opensearchwindowtempform.getActivity_Id());
			request.setAttribute("MSA_Id", opensearchwindowtempform.getMSA_Id());
			if (opensearchwindowtempform.getFromflag() != null
					&& opensearchwindowtempform.getFromflag() != "") {
				request.setAttribute("from",
						opensearchwindowtempform.getFromflag());
			} else {
				request.setAttribute("from", opensearchwindowtempform.getFrom());
			}
			request.setAttribute("ref", opensearchwindowtempform.getRef());
			request.setAttribute("addendum_id",
					opensearchwindowtempform.getAddendum_id());
			request.setAttribute("resourceListType",
					opensearchwindowtempform.getResourceListType());
			request.setAttribute("tempdeleteflag", "Y");
			request.setAttribute("edit", "editable");
			if (opensearchwindowtempform.getJobId() != null
					&& !opensearchwindowtempform.getJobId().trim()
							.equalsIgnoreCase("")) {
				Map<String, Object> map;

				map = DatabaseUtilityDao.setMenu(
						opensearchwindowtempform.getJobId(), loginuserid,
						getDataSource(request, "ilexnewDB"));
				WebUtil.copyMapToRequest(request, map);
			}
			return mapping.findForward("temppage");

		}

		if (request.getParameter("searchmore") != null) {
			int checklength = 0;
			String index = "";
			// String index_clr = "";
			String index_detail_id = "";
			int k = 0;
			if (opensearchwindowtempform.getCheck() != null) {
				checklength = opensearchwindowtempform.getCheck().length;
			}

			if (checklength > 0) {
				for (int i = 0; i < checklength; i++) {
					index = index + opensearchwindowtempform.getCheck(i) + ",";

					if (opensearchwindowtempform.getChecknetmedx()
							.equalsIgnoreCase("Y")
							&& opensearchwindowtempform.getType()
									.equalsIgnoreCase("Labor")) {
						for (int j = 0; j < opensearchwindowtempform
								.getDetail_id().length; j++) {
							if (opensearchwindowtempform.getCheck(i).equals(
									opensearchwindowtempform.getDetail_id(j))) {

								// index_clr
								// =index_clr+opensearchwindowtempform.getClr(j)+",";
								index_detail_id = index_detail_id
										+ opensearchwindowtempform
												.getDetail_id(j) + ",";
							}
						}
					}

				}

				if (opensearchwindowtempform.getChecknetmedx()
						.equalsIgnoreCase("Y")
						&& opensearchwindowtempform.getType().equalsIgnoreCase(
								"Labor")) {
					// index_clr=index_clr.substring(0,index_clr.length()-1);
					index_detail_id = index_detail_id.substring(0,
							index_detail_id.length() - 1);
				}

				index = index.substring(0, index.length() - 1);

			}

			OpenSearchWindowTempdao.addResourcesInTempTable(index, request
					.getSession().getId() + "",
					opensearchwindowtempform.getType(),
					opensearchwindowtempform.getActivity_Id(), index_detail_id,
					opensearchwindowtempform.getFrom(),
					getDataSource(request, "ilexnewDB"));
			OpenSearchWindowTempBean openSearchWindowTempBean = new OpenSearchWindowTempBean();
			BeanUtils.copyProperties(openSearchWindowTempBean,
					opensearchwindowtempform);
			templist = OpenSearchWindowTempdao.getTempResourcelist(
					openSearchWindowTempBean, request.getSession().getId(),
					opensearchwindowtempform.getChecknetmedx(),
					getDataSource(request, "ilexnewDB"));
			if (templist.size() > 0) {
				request.setAttribute("templist", templist);
			}

		}
		/* by hamid for selecting multiple resources end */

		/*
		 * if( request.getParameter( "clr" ) != null ) {
		 * clr=request.getParameter( "clr" ); if(request.getParameter( "offset1"
		 * ) != null ) {
		 * 
		 * request.setAttribute("offset1",request.getParameter( "offset1" ));
		 * opensearchwindowtempform.setOffset1(request.getParameter( "offset1"
		 * )); } if(request.getParameter( "offset2" ) != null ) {
		 * request.setAttribute("offset2",request.getParameter( "offset2" ));
		 * opensearchwindowtempform.setOffset2(request.getParameter( "offset2"
		 * )); }
		 * 
		 * }
		 * 
		 * opensearchwindowtempform.setClr( clr );
		 */
		OpenSearchWindowTempBean openSearchWindowTempBean = new OpenSearchWindowTempBean();
		BeanUtils.copyProperties(openSearchWindowTempBean,
				opensearchwindowtempform);

		if (opensearchwindowtempform.getSearch() != null) {
			list = OpenSearchWindowTempdao.getResourcelist(
					openSearchWindowTempBean,
					getDataSource(request, "ilexnewDB"));

			opensearchwindowtempform.setResourcelist(OpenSearchWindowTempdao
					.getResourcelist(openSearchWindowTempBean,
							getDataSource(request, "ilexnewDB")));
			size = list.size();
			/* for selected resource show */
			templist = OpenSearchWindowTempdao.getTempResourcelist(
					openSearchWindowTempBean, request.getSession().getId(),
					opensearchwindowtempform.getChecknetmedx(),
					getDataSource(request, "ilexnewDB"));
			if (templist.size() > 0) {
				request.setAttribute("templist", templist);
			}
			/* for selected resource show */

			opensearchwindowtempform.setSearchclick("true");
		} else {
			opensearchwindowtempform.setSearchclick("false");
		}

		opensearchwindowtempform.setCategorycombo(OpenSearchWindowTempdao
				.getCategory(opensearchwindowtempform.getType(), clr,
						getDataSource(request, "ilexnewDB")));
		opensearchwindowtempform.setSubcategorycombo(OpenSearchWindowTempdao
				.getSubcategory(opensearchwindowtempform.getCategory(), clr,
						getDataSource(request, "ilexnewDB")));
		opensearchwindowtempform.setSubsubcategorycombo(OpenSearchWindowTempdao
				.getSubsubcategory(opensearchwindowtempform.getSubcategory(),
						getDataSource(request, "ilexnewDB")));
		request.setAttribute("size", size + "");
		return mapping.findForward("success");
	}
}
