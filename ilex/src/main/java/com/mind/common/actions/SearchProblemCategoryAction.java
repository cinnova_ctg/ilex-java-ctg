package com.mind.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.OpenSearchWindowdao;
import com.mind.common.formbean.SearchProblemCategoryForm;

public class SearchProblemCategoryAction extends com.mind.common.IlexAction {
	
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		SearchProblemCategoryForm problemcategoryForm = ( SearchProblemCategoryForm ) form;
		ArrayList list = new ArrayList();
		int size=0;
		
		if(request.getParameter("search_type") != null)
			problemcategoryForm.setSearch_type(request.getParameter("search_type"));
		
		if(request.getParameter("lo_ot_id") != null)
			problemcategoryForm.setLo_ot_id(request.getParameter("lo_ot_id"));
		else
			problemcategoryForm.setLo_ot_id("0");
		
		if(problemcategoryForm.getSearch() != null) {
				if(problemcategoryForm.getSearch_type().equals("company_search"))
					problemcategoryForm.setProblemlist(OpenSearchWindowdao.getPOClist(problemcategoryForm.getProblemcategory(), problemcategoryForm.getLo_ot_id(), "C", problemcategoryForm.getKeyword(), problemcategoryForm.getSearch_type(), getDataSource( request , "ilexnewDB" ) ) );
				else
					problemcategoryForm.setProblemlist(OpenSearchWindowdao.getProblemCategorylist(problemcategoryForm.getProblemcategory(), problemcategoryForm.getKeyword(), getDataSource( request , "ilexnewDB" )));
				size = problemcategoryForm.getProblemlist().size();
				problemcategoryForm.setSearchclick( "true" );
		}	
		else
		{
			problemcategoryForm.setSearchclick( "false" );
		}
		
		
		if(problemcategoryForm.getSearch_type().equals("company_search"))
			problemcategoryForm.setProblemcategorycombo(OpenSearchWindowdao.getMSANameList(getDataSource( request,"ilexnewDB" )));
		
		if(problemcategoryForm.getSearch_type().equals("problem_search"))		
			problemcategoryForm.setProblemcategorycombo(DynamicComboDao.getProblemCategory(getDataSource(request,"ilexnewDB")));
		
		request.setAttribute("size",size+"");
		return mapping.findForward( "success" );
	}
}
