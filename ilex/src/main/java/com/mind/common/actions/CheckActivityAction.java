package com.mind.common.actions;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class CheckActivityAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CheckActivityAction.class);

	public static int checkActivitieAppendix(HttpServletRequest request)throws IOException{
		int count = 0;
		DataSource ds = (DataSource) request.getSession(false).getAttribute("datasource");
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String sql="";
		int activityCount = 0;
		try
		{
			int appendixid = Integer.parseInt(request.getAttribute("appendixid").toString());
			String MSA_id = request.getAttribute("MSAid").toString();
			String version_id = (request.getAttribute("version").toString());
			String startDate = (request.getAttribute("startdate").toString());
			String endDate = (request.getAttribute("enddate").toString());
			String dateOnSite = (request.getAttribute("dateOnSite").toString());
			String dateOffSite = (request.getAttribute("dateOffSite").toString());
			String dateComplete = (request.getAttribute("dateComplete").toString());
			String dateClosed = (request.getAttribute("dateClosed").toString());
			String msp = (request.getAttribute("msp").toString());
			
			
		/*	System.out.println("appendix_id " + appendixid);	
			System.out.println("version_id " + version_id);	
			System.out.println("startDate " + startDate);	
			System.out.println("endDate " + endDate);	
			System.out.println("dateOnSite " + dateOnSite);
			System.out.println("dateOffSite " + dateOffSite);
			System.out.println("dateComplete " + dateComplete);
			System.out.println("dateClosed " + dateClosed);
			System.out.println("msp " + msp);  */	
			
			
			conn = ds.getConnection();	
			cstmt = conn.prepareCall("{?=call getActivityCount(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(11, java.sql.Types.INTEGER);
			cstmt.setInt(2, appendixid);
			cstmt.setString(3, version_id);
			cstmt.setString(4, startDate);
			cstmt.setString(5, endDate);
			cstmt.setString(6, dateOnSite);
			cstmt.setString(7, dateOffSite);
			cstmt.setString(8, dateComplete);
			cstmt.setString(9, dateClosed);
			cstmt.setString(10, msp);
			cstmt.execute();
			activityCount = cstmt.getInt(11);
						
		}
		catch(Exception e)
		{
			logger.error("checkActivitieAppendix(HttpServletRequest)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkActivitieAppendix(HttpServletRequest) - Error Getting Activity Count.  com.mind.common.actions.CheckActivityAction  --" + e);
			}
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				
				if (cstmt!= null)
					cstmt.close();

				if (conn!= null)
					conn.close();
				
			} catch (Exception ex) {
				logger.error("checkActivitieAppendix(HttpServletRequest)", ex);

				if (logger.isDebugEnabled()) {
					logger.debug("checkActivitieAppendix(HttpServletRequest) - exception in closing --" + ex);
				}
			}

		}
	//	System.out.println("Total No of Activities are ::: "  + activityCount);
		return activityCount;
	}

	}