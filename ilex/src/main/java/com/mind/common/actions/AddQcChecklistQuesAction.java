package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Checklistdao;
import com.mind.common.formbean.AddQcChecklistQuesForm;

public class AddQcChecklistQuesAction extends com.mind.common.IlexAction
{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		AddQcChecklistQuesForm addqcchecklistquesform = ( AddQcChecklistQuesForm ) form;
		String Appendix_Id = "";
				
		int addflag = -1;
		String fromtype = "";
		int offset = 0;
		String optionlist = "";
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		if( session.getAttribute( "userid" ) == null ) return( mapping.findForward( "SessionExpire" ) );   //Check for session expired
		
		String userid = ( String )session.getAttribute( "userid" );
		
		/* Page Security End*/
			
		if( addqcchecklistquesform.getSave() != null )
		{
			if( addqcchecklistquesform.getCheck() != null )
			{
				for( int i = 0; i < addqcchecklistquesform.getCheck().length; i++ )
				{
					offset = Integer.parseInt( addqcchecklistquesform.getCheck( i ) );
					if( i == ( addqcchecklistquesform.getCheck().length - 1 ) )
					{
						optionlist = optionlist + addqcchecklistquesform.getOption( offset ) ;	
					}
					else
					{
						optionlist = optionlist + addqcchecklistquesform.getOption( offset ) + "~";	
					}
				}
				
				addflag = Checklistdao.addAppendixChecklist( addqcchecklistquesform.getGroup() , addqcchecklistquesform.getItem() , optionlist , addqcchecklistquesform.getOptiontype() , getDataSource( request , "ilexnewDB" ) );
				
				request.setAttribute( "addflag" , addflag+"" );
				return mapping.findForward( "checklistmaster" );
			}
		}
		return mapping.findForward( "success" );
	}
}
