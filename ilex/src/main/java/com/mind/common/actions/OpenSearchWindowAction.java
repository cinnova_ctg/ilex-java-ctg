package com.mind.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.OpenSearchWindowBean;
import com.mind.common.dao.OpenSearchWindowdao;
import com.mind.common.formbean.OpenSearchWindowForm;

public class OpenSearchWindowAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OpenSearchWindowForm opensearchwindowform = (OpenSearchWindowForm) form;
		OpenSearchWindowBean openSearchWindowBean = new OpenSearchWindowBean();
		ArrayList list = new ArrayList();
		int size = 0;
		String clr = "no";

		// System.out.println("XXXX1 type action "+request.getParameter( "type"
		// ));

		if (request.getParameter("type") != null) {
			opensearchwindowform.setType(request.getParameter("type"));
			request.setAttribute("restype", request.getParameter("type"));
		}

		// System.out.println("XXXX2 from action "+request.getParameter( "from"
		// ));

		if (request.getParameter("from") != null) {
			opensearchwindowform.setFrom(request.getParameter("from"));
		}

		// System.out.println("XXXX3 activity_Id action "+request.getParameter(
		// "activity_Id" ));

		if (request.getParameter("activity_Id") != null) {
			opensearchwindowform.setActivity_Id(request
					.getParameter("activity_Id"));
		}

		// System.out.println("XXXX4  action ");

		if (request.getParameter("clr") != null) {
			clr = request.getParameter("clr");
			if (request.getParameter("offset1") != null) {

				request.setAttribute("offset1", request.getParameter("offset1"));
				opensearchwindowform
						.setOffset1(request.getParameter("offset1"));
			}
			if (request.getParameter("offset2") != null) {
				request.setAttribute("offset2", request.getParameter("offset2"));
				opensearchwindowform
						.setOffset2(request.getParameter("offset2"));
			}

		}

		opensearchwindowform.setClr(clr);

		if (opensearchwindowform.getSearch() != null) {
			BeanUtils
					.copyProperties(openSearchWindowBean, opensearchwindowform);
			list = OpenSearchWindowdao.getResourcelist(openSearchWindowBean,
					getDataSource(request, "ilexnewDB"));
			opensearchwindowform.setResourcelist(OpenSearchWindowdao
					.getResourcelist(openSearchWindowBean,
							getDataSource(request, "ilexnewDB")));
			size = list.size();
			BeanUtils
					.copyProperties(opensearchwindowform, openSearchWindowBean);
			opensearchwindowform.setSearchclick("true");
		} else {
			opensearchwindowform.setSearchclick("false");
		}

		opensearchwindowform.setCategorycombo(OpenSearchWindowdao.getCategory(
				opensearchwindowform.getType(), clr,
				getDataSource(request, "ilexnewDB")));
		opensearchwindowform.setSubcategorycombo(OpenSearchWindowdao
				.getSubcategory(opensearchwindowform.getCategory(), clr,
						getDataSource(request, "ilexnewDB")));
		opensearchwindowform.setSubsubcategorycombo(OpenSearchWindowdao
				.getSubsubcategory(opensearchwindowform.getSubcategory(),
						getDataSource(request, "ilexnewDB")));

		// System.out.println("XXXX5  action ");

		request.setAttribute("size", size + "");
		return mapping.findForward("success");
	}
}
