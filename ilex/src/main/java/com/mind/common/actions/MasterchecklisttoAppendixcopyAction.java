package com.mind.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Checklistdao;
import com.mind.common.formbean.ChecklistForm;


public class MasterchecklisttoAppendixcopyAction extends com.mind.common.IlexAction
{
	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		ChecklistForm sitechecklistform = ( ChecklistForm ) form;
		String Appendix_Id = "";
		int master_checklist_size = 0;
		String appendix_checklist_id_type= "";
		String appendix_checklist_type= "";
		String itemlist= "";
		int updateflag = -1;
		String fromtype="";
		int index=0;
		
		/* Page Security Start*/
		HttpSession session = request.getSession( true );
		String userid = (String)session.getAttribute( "userid" );
		if( sitechecklistform.getAuthenticate().equals( "" ) )
		{
									
			if( !Authenticationdao.getPageSecurity( userid , "Manage Appendix" , "QC Checklist" , getDataSource( request , "ilexnewDB" ) ) ) 
			{
				return( mapping.findForward( "UnAuthenticate" ) );	
			}
			
			
		}
		/* Page Security End*/
		
		if( sitechecklistform.getSubmit() != null )
		{
			if( sitechecklistform.getCheck() != null )
			{
				for( int i = 0; i < sitechecklistform.getCheck().length; i++ )
				{
					if( i == ( sitechecklistform.getCheck().length - 1 ) )
					{
						appendix_checklist_id_type = appendix_checklist_id_type + sitechecklistform.getCheck( i ) ;
						appendix_checklist_type = appendix_checklist_type + sitechecklistform.getCheck( i ).charAt( 0 );
					}
					else
					{
						appendix_checklist_id_type = appendix_checklist_id_type + sitechecklistform.getCheck( i ) + "~";
						appendix_checklist_type = appendix_checklist_type + sitechecklistform.getCheck( i ).charAt( 0 ) + "~";
					}				
					for(int j = 0; j < sitechecklistform.getChecklistidtype().length; j++ )
					{
						if( sitechecklistform.getCheck( i ).equals( sitechecklistform.getChecklistidtype( j ) ) )
						{
							itemlist = itemlist+sitechecklistform.getItemname( j )+"~";
						}
					}	
				}
				if( itemlist.length() > 0 )
				{
					itemlist = itemlist.substring( 0 , itemlist.length() - 1 );
				}
				
				updateflag = Checklistdao.addappendixChecklistitem( appendix_checklist_id_type  , appendix_checklist_type , getDataSource( request , "ilexnewDB" ) , itemlist );
				
				request.setAttribute( "updateflag" , updateflag+"" );
			}
		}
		sitechecklistform.setCheck( null );
		
		ArrayList mastersitechecklist;
		
		String[] checkedappendixlist;

		mastersitechecklist = Checklistdao.getQCchecklist( getDataSource( request , "ilexnewDB" ) );
			
		if( mastersitechecklist.size() > 0 )
		{
			master_checklist_size = mastersitechecklist.size();
			
		}
		
		if( request.getAttribute( "addflag" ) != null )
		{
			request.setAttribute( "addflag" , request.getAttribute( "addflag" )  ) ;
		}
		
		
		request.setAttribute( "Size" , new Integer ( master_checklist_size ) );
		sitechecklistform.setIntialchecklistsize( master_checklist_size+"" );
		
		request.setAttribute( "master_checklist" , mastersitechecklist );
		

		return mapping.findForward( "success" );
	}
}
