package com.mind.common.actions;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.ViewAllComment;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.common.formbean.MenuFunctionViewCommentForm;
import com.mind.dao.AM.ActivityLibrarydao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PM.NameId;
import com.mind.dao.PM.Resourcedao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class MenuFunctionViewCommentAction extends com.mind.common.IlexAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* Page Security */
		MenuFunctionViewCommentForm menuFunctionViewCommentForm = (MenuFunctionViewCommentForm) form;
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		String temp = request.getParameter("Type");
		String Type = request.getParameter("Type");
		String loginuserid = (String) session.getAttribute("userid");

		// Start: Variables added for the View Selector Functioning.
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "";
		String msaid = "";
		int ownerListSize = 0;
		NameId idname = null;
		NameId appendixidname = null;
		String activityname = "";
		String from = "";
		String jobid = "";
		// End

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobAppStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		if (temp.contains("pm_res")) {
			temp = "Resource";
		}

		if (temp.equals("prm_appendix")) {
			temp = "Appendix";
		}

		if (Type.equals("prm_appendix"))
			menuFunctionViewCommentForm.setType("Appendix");

		if (temp.equals("Jobdashboard")) {
			temp = "Job";
		}
		if (!Authenticationdao.getPageSecurity(userid, temp,
				"View All Comment", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		ArrayList all_comment = new ArrayList();

		if (request.getParameter("Id") != null)
			menuFunctionViewCommentForm
					.setForwardId(request.getParameter("Id"));

		if (request.getParameter("viewjobtype") != null) {
			menuFunctionViewCommentForm.setViewjobtype(request
					.getParameter("viewjobtype"));
		}

		if (request.getParameter("ownerId") != null) {
			menuFunctionViewCommentForm.setOwnerId(request
					.getParameter("ownerId"));
		}

		if (request.getParameter("Activity_Id") != null)
			menuFunctionViewCommentForm.setActivity_Id(request
					.getParameter("Activity_Id"));

		all_comment = ViewAllComment.Getallcomment(
				menuFunctionViewCommentForm.getForwardId(), Type,
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("all_comment", all_comment);
		menuFunctionViewCommentForm.setType(request.getParameter("Type"));

		if (menuFunctionViewCommentForm.getType().equals("Appendix"))
			menuFunctionViewCommentForm.setMsaId(IMdao.getMSAId(
					menuFunctionViewCommentForm.getForwardId(),
					getDataSource(request, "ilexnewDB")));

		if (menuFunctionViewCommentForm.getType() != null) {
			if (menuFunctionViewCommentForm.getType().equals("MSA")) {
				menuFunctionViewCommentForm.setName(Appendixdao.getMsaname(
						getDataSource(request, "ilexnewDB"),
						menuFunctionViewCommentForm.getForwardId()));
				ownerType = "msa";
				msaid = "0";

				if (session.getAttribute("appendixSelectedOwners") != null
						|| session.getAttribute("appendixSelectedStatus") != null)
					request.setAttribute("specialCase", "specialCase");

				if (session.getAttribute("monthAppendix") != null
						|| session.getAttribute("weekAppendix") != null) {
					request.setAttribute("specialCase", "specialCase");
				}

				WebUtil.removeAppendixSession(session);
				// when the other check box is checked in the view selector.
				if (menuFunctionViewCommentForm.getOtherCheck() != null)
					session.setAttribute("otherCheck",
							menuFunctionViewCommentForm.getOtherCheck()
									.toString());

				if (menuFunctionViewCommentForm.getSelectedStatus() != null) {
					for (int i = 0; i < menuFunctionViewCommentForm
							.getSelectedStatus().length; i++) {
						selectedStatus = selectedStatus
								+ ","
								+ "'"
								+ menuFunctionViewCommentForm
										.getSelectedStatus(i) + "'";
						tempStatus = tempStatus
								+ ","
								+ menuFunctionViewCommentForm
										.getSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				if (menuFunctionViewCommentForm.getSelectedOwners() != null) {
					for (int j = 0; j < menuFunctionViewCommentForm
							.getSelectedOwners().length; j++) {
						selectedOwner = selectedOwner
								+ ","
								+ menuFunctionViewCommentForm
										.getSelectedOwners(j);
						tempOwner = tempOwner
								+ ","
								+ menuFunctionViewCommentForm
										.getSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}
				// End of the code.

				// The Menu Images are to be set in session respective of one
				// other.
				if (request.getParameter("month") != null) {
					session.setAttribute("monthMSA",
							request.getParameter("month").toString());
					monthMSA = request.getParameter("month").toString();
					session.removeAttribute("weekMSA");
					menuFunctionViewCommentForm.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekMSA", request
							.getParameter("week").toString());
					weekMSA = request.getParameter("week").toString();
					session.removeAttribute("monthMSA");
					menuFunctionViewCommentForm.setMonthWeekCheck("week");
				}

				// When the reset button is pressed remove every thing from the
				// session.
				if (request.getParameter("home") != null) {
					menuFunctionViewCommentForm.setSelectedStatus(null);
					menuFunctionViewCommentForm.setSelectedOwners(null);
					menuFunctionViewCommentForm.setPrevSelectedStatus("");
					menuFunctionViewCommentForm.setPrevSelectedOwner("");
					menuFunctionViewCommentForm.setOtherCheck(null);
					menuFunctionViewCommentForm.setMonthWeekCheck(null);
					// MSAdao.setValuesWhenReset(session);
					return (mapping.findForward("msalistpage"));
				}
				// To set the values in the session when the Show button is
				// Pressed.
				if (menuFunctionViewCommentForm.getGo() != null
						&& menuFunctionViewCommentForm.getGo() != "") {
					session.setAttribute("selectedOwners", selectedOwner);
					session.setAttribute("MSASelectedOwners", selectedOwner);
					session.setAttribute("tempOwner", tempOwner);
					session.setAttribute("selectedStatus", selectedStatus);
					session.setAttribute("MSASelectedStatus", selectedStatus);
					session.setAttribute("tempStatus", tempStatus);

					if (menuFunctionViewCommentForm.getMonthWeekCheck() != null) {
						if (menuFunctionViewCommentForm.getMonthWeekCheck()
								.equals("week")) {
							session.removeAttribute("monthMSA");
							session.setAttribute("weekMSA", "0");
						}
						if (menuFunctionViewCommentForm.getMonthWeekCheck()
								.equals("month")) {
							session.removeAttribute("weekMSA");
							session.setAttribute("monthMSA", "0");
						}
						if (menuFunctionViewCommentForm.getMonthWeekCheck()
								.equals("clear")) {
							session.removeAttribute("monthMSA");
							session.removeAttribute("weekMSA");
						}

					}

					if (session.getAttribute("monthMSA") != null) {
						session.removeAttribute("weekMSA");
						monthMSA = session.getAttribute("monthMSA").toString();
					}
					if (session.getAttribute("weekMSA") != null) {
						session.removeAttribute("monthMSA");
						weekMSA = session.getAttribute("weekMSA").toString();
					}
					return (mapping.findForward("msalistpage"));
				} else // If not pressed see the values exists in the session or
						// not.
				{

					if (session.getAttribute("selectedOwners") != null) {
						selectedOwner = session.getAttribute("selectedOwners")
								.toString();
						menuFunctionViewCommentForm.setSelectedOwners(session
								.getAttribute("tempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("selectedStatus") != null) {
						selectedStatus = session.getAttribute("selectedStatus")
								.toString();
						menuFunctionViewCommentForm.setSelectedStatus(session
								.getAttribute("tempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("otherCheck") != null)
						menuFunctionViewCommentForm.setOtherCheck(session
								.getAttribute("otherCheck").toString());
				}

				if (session.getAttribute("monthMSA") != null) {
					session.removeAttribute("weekMSA");
					monthMSA = session.getAttribute("monthMSA").toString();
				}
				if (session.getAttribute("weekMSA") != null) {
					session.removeAttribute("monthMSA");
					weekMSA = session.getAttribute("weekMSA").toString();
				}

				if (!chkOtherOwner) {
					menuFunctionViewCommentForm
							.setPrevSelectedStatus(selectedStatus);
					menuFunctionViewCommentForm
							.setPrevSelectedOwner(selectedOwner);
				}

				ownerList = MSAdao.getOwnerList(userid,
						menuFunctionViewCommentForm.getOtherCheck(), ownerType,
						msaid, getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_msa",
						getDataSource(request, "ilexnewDB"));

				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

				// End of the code for the View Selector Part
			}
			if (request.getParameter("Type").equals("Appendix")) {
				menuFunctionViewCommentForm.setName(Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"),
						menuFunctionViewCommentForm.getForwardId()));
				menuFunctionViewCommentForm.setMsaId(IMdao.getMSAId(
						menuFunctionViewCommentForm.getForwardId(),
						getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm
						.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
								getDataSource(request, "ilexnewDB"),
								menuFunctionViewCommentForm.getMsaId()));
				menuFunctionViewCommentForm
						.setAppendixId(com.mind.dao.PRM.Jobdao.getAppendixid(
								menuFunctionViewCommentForm.getForwardId(),
								getDataSource(request, "ilexnewDB")));

				ownerType = "appendix";

				if (request.getParameter("home") != null) // This is called when
															// reset button is
															// pressed from the
															// view selector.
				{
					menuFunctionViewCommentForm.setAppendixSelectedStatus(null);
					menuFunctionViewCommentForm.setAppendixSelectedOwners(null);
					menuFunctionViewCommentForm.setPrevSelectedStatus("");
					menuFunctionViewCommentForm.setPrevSelectedOwner("");
					menuFunctionViewCommentForm.setAppendixOtherCheck(null);
					menuFunctionViewCommentForm.setMonthWeekCheck(null);
					return (mapping.findForward("appendixlistpage"));
				}

				// These two parameter month/week are checked are kept in
				// session. ( View Images)
				if (request.getParameter("month") != null) {
					session.setAttribute("monthAppendix",
							request.getParameter("month").toString());

					session.removeAttribute("weekAppendix");
					menuFunctionViewCommentForm.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekAppendix",
							request.getParameter("week").toString());

					session.removeAttribute("monthAppendix");
					menuFunctionViewCommentForm.setMonthWeekCheck("week");
				}

				if (menuFunctionViewCommentForm.getAppendixOtherCheck() != null) // OtherCheck
																					// box
																					// status
																					// is
																					// kept
																					// in
																					// session.
					session.setAttribute("appendixOtherCheck",
							menuFunctionViewCommentForm.getAppendixOtherCheck()
									.toString());

				// When Status Check Boxes are Clicked :::::::::::::::::
				if (menuFunctionViewCommentForm.getAppendixSelectedStatus() != null) {
					for (int i = 0; i < menuFunctionViewCommentForm
							.getAppendixSelectedStatus().length; i++) {
						selectedStatus = selectedStatus
								+ ","
								+ "'"
								+ menuFunctionViewCommentForm
										.getAppendixSelectedStatus(i) + "'";
						tempStatus = tempStatus
								+ ","
								+ menuFunctionViewCommentForm
										.getAppendixSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				// When Owner Check Boxes are Clicked :::::::::::::::::
				if (menuFunctionViewCommentForm.getAppendixSelectedOwners() != null) {
					for (int j = 0; j < menuFunctionViewCommentForm
							.getAppendixSelectedOwners().length; j++) {
						selectedOwner = selectedOwner
								+ ","
								+ menuFunctionViewCommentForm
										.getAppendixSelectedOwners(j);
						tempOwner = tempOwner
								+ ","
								+ menuFunctionViewCommentForm
										.getAppendixSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}

				if (menuFunctionViewCommentForm.getGo() != null
						&& menuFunctionViewCommentForm.getGo() != "") {
					session.setAttribute("appendixSelectedOwners",
							selectedOwner);
					session.setAttribute("appSelectedOwners", selectedOwner);
					session.setAttribute("appendixTempOwner", tempOwner);
					session.setAttribute("appendixSelectedStatus",
							selectedStatus);
					session.setAttribute("appSelectedStatus", selectedStatus);
					session.setAttribute("appendixTempStatus", tempStatus);

					if (menuFunctionViewCommentForm.getMonthWeekCheck() != null) {
						if (menuFunctionViewCommentForm.getMonthWeekCheck()
								.equals("week")) {
							session.removeAttribute("monthAppendix");
							session.setAttribute("weekAppendix", "0");

						}
						if (menuFunctionViewCommentForm.getMonthWeekCheck()
								.equals("month")) {
							session.removeAttribute("weekAppendix");
							session.setAttribute("monthAppendix", "0");

						}
						if (menuFunctionViewCommentForm.getMonthWeekCheck()
								.equals("clear")) {
							session.removeAttribute("weekAppendix");
							session.removeAttribute("monthAppendix");

						}

					}
					return (mapping.findForward("appendixlistpage"));
				} else {
					if (session.getAttribute("appendixSelectedOwners") != null) {
						selectedOwner = session.getAttribute(
								"appendixSelectedOwners").toString();
						menuFunctionViewCommentForm
								.setAppendixSelectedOwners(session
										.getAttribute("appendixTempOwner")
										.toString().split(","));
					}
					if (session.getAttribute("appendixSelectedStatus") != null) {
						selectedStatus = session.getAttribute(
								"appendixSelectedStatus").toString();
						menuFunctionViewCommentForm
								.setAppendixSelectedStatus(session
										.getAttribute("appendixTempStatus")
										.toString().split(","));
					}
					if (session.getAttribute("appendixOtherCheck") != null)
						menuFunctionViewCommentForm
								.setAppendixOtherCheck(session.getAttribute(
										"appendixOtherCheck").toString());

					if (session.getAttribute("monthAppendix") != null) {
						session.removeAttribute("weekAppendix");
						menuFunctionViewCommentForm.setMonthWeekCheck("month");
					}
					if (session.getAttribute("weekAppendix") != null) {
						session.removeAttribute("monthAppendix");
						menuFunctionViewCommentForm.setMonthWeekCheck("week");
					}

				}
				if (!chkOtherOwner) {
					menuFunctionViewCommentForm
							.setPrevSelectedStatus(selectedStatus);
					menuFunctionViewCommentForm
							.setPrevSelectedOwner(selectedOwner);
				}
				ownerList = MSAdao.getOwnerList(userid,
						menuFunctionViewCommentForm.getAppendixOtherCheck(),
						ownerType, menuFunctionViewCommentForm.getMsaId(),
						getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_appendix",
						getDataSource(request, "ilexnewDB"));

				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

			}

			if (request.getParameter("Type").equals("Job")
					|| request.getParameter("Type").equals("Jobdashboard")) {
				menuFunctionViewCommentForm.setName(Activitydao.getJobname(
						getDataSource(request, "ilexnewDB"),
						menuFunctionViewCommentForm.getForwardId()));
				menuFunctionViewCommentForm
						.setAppendixId(com.mind.dao.PRM.Jobdao.getAppendixid(
								menuFunctionViewCommentForm.getForwardId(),
								getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm.setAppendixName(Jobdao
						.getAppendixname(getDataSource(request, "ilexnewDB"),
								menuFunctionViewCommentForm.getAppendixId()));
				menuFunctionViewCommentForm.setMsaId(IMdao.getMSAId(
						menuFunctionViewCommentForm.getAppendixId(),
						getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm
						.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
								getDataSource(request, "ilexnewDB"),
								menuFunctionViewCommentForm.getMsaId()));

				// added by pankaj
				if (menuFunctionViewCommentForm.getForwardId() != null
						&& !menuFunctionViewCommentForm.getForwardId().trim()
								.equalsIgnoreCase("")) {
					Map<String, Object> map;

					map = DatabaseUtilityDao.setMenu(
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
					WebUtil.copyMapToRequest(request, map);
				}
				// added by pankaj
			}

			if (menuFunctionViewCommentForm.getType().equals("Activity")) {

				menuFunctionViewCommentForm.setJobId(request
						.getParameter("Job_Id"));
				menuFunctionViewCommentForm.setRef(request.getParameter("ref"));
				menuFunctionViewCommentForm.setJobName(Jobdao.getJobname(
						menuFunctionViewCommentForm.getJobId(),
						getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm
						.setAppendixName(Jobdao.getoplevelname(
								request.getParameter("Job_Id"), "Activity",
								getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm.setAppendixId(Activitydao
						.getAppendixid(getDataSource(request, "ilexnewDB"),
								request.getParameter("Job_Id")));
				idname = Activitydao.getIdName(request.getParameter("Job_Id"),
						"Activity", "MSA", getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setMsaId(idname.getId());
				menuFunctionViewCommentForm.setMsaName(idname.getName());
				menuFunctionViewCommentForm.setName(Activitydao
						.getActivityname(
								menuFunctionViewCommentForm.getForwardId(),
								getDataSource(request, "ilexnewDB")));
			}

			if (menuFunctionViewCommentForm.getType().equals("pm_resM")) {
				menuFunctionViewCommentForm
						.setName(ActivityLibrarydao.Getnameforsowassumption(
								menuFunctionViewCommentForm.getForwardId(),
								"pm_resM", getDataSource(request, "ilexnewDB")));
				appendixidname = Activitydao.getIdName(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", "Appendix",
						getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setAppendixId(appendixidname
						.getId());
				menuFunctionViewCommentForm.setAppendixName(appendixidname
						.getName());

				idname = Activitydao.getIdName(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", "MSA", getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setMsaId(idname.getId());
				menuFunctionViewCommentForm.setMsaName(idname.getName());

				menuFunctionViewCommentForm.setJobName(Jobdao.getoplevelname(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm.setJobId(Activitydao.getJobid(
						getDataSource(request, "ilexnewDB"),
						menuFunctionViewCommentForm.getActivity_Id()));
				activityname = Activitydao.getActivityname(
						menuFunctionViewCommentForm.getActivity_Id(),
						getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setActivityName(activityname);
			}

			if (menuFunctionViewCommentForm.getType().equals("pm_resL")) {
				menuFunctionViewCommentForm
						.setName(ActivityLibrarydao.Getnameforsowassumption(
								menuFunctionViewCommentForm.getForwardId(),
								"pm_resL", getDataSource(request, "ilexnewDB")));
				appendixidname = Activitydao.getIdName(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", "Appendix",
						getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setAppendixId(appendixidname
						.getId());
				menuFunctionViewCommentForm.setAppendixName(appendixidname
						.getName());

				idname = Activitydao.getIdName(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", "MSA", getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setMsaId(idname.getId());
				menuFunctionViewCommentForm.setMsaName(idname.getName());

				menuFunctionViewCommentForm.setJobName(Jobdao.getoplevelname(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm.setJobId(Activitydao.getJobid(
						getDataSource(request, "ilexnewDB"),
						menuFunctionViewCommentForm.getActivity_Id()));
				activityname = Activitydao.getActivityname(
						menuFunctionViewCommentForm.getActivity_Id(),
						getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setActivityName(activityname);
			}
			if (menuFunctionViewCommentForm.getType().equals("pm_resF")) {
				menuFunctionViewCommentForm
						.setName(ActivityLibrarydao.Getnameforsowassumption(
								menuFunctionViewCommentForm.getForwardId(),
								"pm_resF", getDataSource(request, "ilexnewDB")));
				appendixidname = Activitydao.getIdName(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", "Appendix",
						getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setAppendixId(appendixidname
						.getId());
				menuFunctionViewCommentForm.setAppendixName(appendixidname
						.getName());

				idname = Activitydao.getIdName(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", "MSA", getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setMsaId(idname.getId());
				menuFunctionViewCommentForm.setMsaName(idname.getName());

				menuFunctionViewCommentForm.setJobName(Jobdao.getoplevelname(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm.setJobId(Activitydao.getJobid(
						getDataSource(request, "ilexnewDB"),
						menuFunctionViewCommentForm.getActivity_Id()));
				activityname = Activitydao.getActivityname(
						menuFunctionViewCommentForm.getActivity_Id(),
						getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setActivityName(activityname);
			}
			if (menuFunctionViewCommentForm.getType().equals("pm_resT")) {
				menuFunctionViewCommentForm
						.setName(ActivityLibrarydao.Getnameforsowassumption(
								menuFunctionViewCommentForm.getForwardId(),
								"pm_resT", getDataSource(request, "ilexnewDB")));
				appendixidname = Activitydao.getIdName(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", "Appendix",
						getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setAppendixId(appendixidname
						.getId());
				menuFunctionViewCommentForm.setAppendixName(appendixidname
						.getName());

				idname = Activitydao.getIdName(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", "MSA", getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setMsaId(idname.getId());
				menuFunctionViewCommentForm.setMsaName(idname.getName());

				menuFunctionViewCommentForm.setJobName(Jobdao.getoplevelname(
						menuFunctionViewCommentForm.getActivity_Id(),
						"Resource", getDataSource(request, "ilexnewDB")));
				menuFunctionViewCommentForm.setJobId(Activitydao.getJobid(
						getDataSource(request, "ilexnewDB"),
						menuFunctionViewCommentForm.getActivity_Id()));
				activityname = Activitydao.getActivityname(
						menuFunctionViewCommentForm.getActivity_Id(),
						getDataSource(request, "ilexnewDB"));
				menuFunctionViewCommentForm.setActivityName(activityname);
			}
			if (menuFunctionViewCommentForm.getType().equals("prm_appendix")) {

				/* Start : Appendix Dashboard View Selector Code */

				if (menuFunctionViewCommentForm.getJobOwnerOtherCheck() != null
						&& !menuFunctionViewCommentForm.getJobOwnerOtherCheck()
								.equals("")) {
					session.setAttribute("jobOwnerOtherCheck",
							menuFunctionViewCommentForm.getJobOwnerOtherCheck());
				}

				if (request.getParameter("opendiv") != null) {
					request.setAttribute("opendiv",
							request.getParameter("opendiv"));
				}
				if (request.getParameter("resetList") != null
						&& request.getParameter("resetList")
								.equals("something")) {
					WebUtil.setDefaultAppendixDashboardAttribute(session);
					request.setAttribute("type", "AppendixViewSelector");
					request.setAttribute("appendixId",
							menuFunctionViewCommentForm.getForwardId());
					request.setAttribute("msaId",
							menuFunctionViewCommentForm.getMsaId());

					return mapping.findForward("temppage");

				}

				if (menuFunctionViewCommentForm.getJobSelectedStatus() != null) {
					for (int i = 0; i < menuFunctionViewCommentForm
							.getJobSelectedStatus().length; i++) {
						jobSelectedStatus = jobSelectedStatus
								+ ","
								+ "'"
								+ menuFunctionViewCommentForm
										.getJobSelectedStatus(i) + "'";
						jobTempStatus = jobTempStatus
								+ ","
								+ menuFunctionViewCommentForm
										.getJobSelectedStatus(i);
					}

					jobSelectedStatus = jobSelectedStatus.substring(1,
							jobSelectedStatus.length());
					jobSelectedStatus = "(" + jobSelectedStatus + ")";
				}

				if (menuFunctionViewCommentForm.getJobSelectedOwners() != null) {
					for (int j = 0; j < menuFunctionViewCommentForm
							.getJobSelectedOwners().length; j++) {
						jobSelectedOwner = jobSelectedOwner
								+ ","
								+ menuFunctionViewCommentForm
										.getJobSelectedOwners(j);
						jobTempOwner = jobTempOwner
								+ ","
								+ menuFunctionViewCommentForm
										.getJobSelectedOwners(j);
					}
					jobSelectedOwner = jobSelectedOwner.substring(1,
							jobSelectedOwner.length());
					if (jobSelectedOwner.equals("0")) {
						jobSelectedOwner = "%";
					}
					jobSelectedOwner = "(" + jobSelectedOwner + ")";
				}

				if (request.getParameter("showList") != null
						&& request.getParameter("showList").equals("something")) {

					session.setAttribute("jobSelectedOwners", jobSelectedOwner);
					session.setAttribute("jobTempOwner", jobTempOwner);
					session.setAttribute("jobSelectedStatus", jobSelectedStatus);
					session.setAttribute("jobTempStatus", jobTempStatus);
					session.setAttribute("timeFrame",
							menuFunctionViewCommentForm.getJobMonthWeekCheck());
					session.setAttribute("jobOwnerOtherCheck",
							menuFunctionViewCommentForm.getJobOwnerOtherCheck());

					request.setAttribute("type", "AppendixViewSelector");
					request.setAttribute("appendixId",
							menuFunctionViewCommentForm.getForwardId());
					request.setAttribute("msaId",
							menuFunctionViewCommentForm.getMsaId());

					return mapping.findForward("temppage");

				} else {
					if (session.getAttribute("jobSelectedOwners") != null) {
						jobSelectedOwner = session.getAttribute(
								"jobSelectedOwners").toString();
						menuFunctionViewCommentForm
								.setJobSelectedOwners(session
										.getAttribute("jobTempOwner")
										.toString().split(","));
					}
					if (session.getAttribute("jobSelectedStatus") != null) {
						jobSelectedStatus = session.getAttribute(
								"jobSelectedStatus").toString();
						menuFunctionViewCommentForm
								.setJobSelectedStatus(session
										.getAttribute("jobTempStatus")
										.toString().split(","));
					}
					if (session.getAttribute("jobOwnerOtherCheck") != null) {
						menuFunctionViewCommentForm
								.setJobOwnerOtherCheck(session.getAttribute(
										"jobOwnerOtherCheck").toString());
					}
					if (session.getAttribute("timeFrame") != null) {
						menuFunctionViewCommentForm
								.setJobMonthWeekCheck(session.getAttribute(
										"timeFrame").toString());
					}
				}

				jobOwnerList = MSAdao.getOwnerList(userid,
						menuFunctionViewCommentForm.getJobOwnerOtherCheck(),
						"prj_job_new",
						menuFunctionViewCommentForm.getForwardId(),
						getDataSource(request, "ilexnewDB"));
				jobAppStatusList = MSAdao.getStatusList("prj_job_new",
						getDataSource(request, "ilexnewDB"));

				if (jobOwnerList.size() > 0)
					jobOwnerListSize = jobOwnerList.size();

				request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
				request.setAttribute("jobAppStatusList", jobAppStatusList);
				request.setAttribute("jobOwnerList", jobOwnerList);
				/* End : Appendix Dashboard View Selector Code */

				menuFunctionViewCommentForm.setName(Jobdao.getAppendixname(
						getDataSource(request, "ilexnewDB"),
						menuFunctionViewCommentForm.getForwardId()));
				String contractDocoumentList = com.mind.dao.PM.Appendixdao
						.getcontractDocoumentMenu(
								menuFunctionViewCommentForm.getForwardId(),
								getDataSource(request, "ilexnewDB"));
				request.setAttribute("contractDocMenu", contractDocoumentList);
				String appendixcurrentstatus = Appendixdao.getCurrentstatus(
						menuFunctionViewCommentForm.getForwardId(),
						getDataSource(request, "ilexnewDB"));
				request.setAttribute("appendixcurrentstatus",
						appendixcurrentstatus);
				menuFunctionViewCommentForm.setMsaId(IMdao.getMSAId(
						menuFunctionViewCommentForm.getForwardId(),
						getDataSource(request, "ilexnewDB")));
				request.setAttribute("ownerId",
						menuFunctionViewCommentForm.getOwnerId());
				request.setAttribute("viewjobtype",
						menuFunctionViewCommentForm.getViewjobtype());
				request.setAttribute("ownerId",
						menuFunctionViewCommentForm.getOwnerId());
				request.setAttribute("msaId",
						menuFunctionViewCommentForm.getMsaId());
				request.setAttribute("appendixid",
						menuFunctionViewCommentForm.getForwardId());
			}

			if (menuFunctionViewCommentForm.getType().equals("Jobdashboard"))
				menuFunctionViewCommentForm.setAppendixName(Jobdao
						.getAppendixname(getDataSource(request, "ilexnewDB"),
								IMdao.getAppendixId(menuFunctionViewCommentForm
										.getForwardId(),
										getDataSource(request, "ilexnewDB"))));

		}
		// Common Code for both MSA And Appendix View Selector.
		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		if (menuFunctionViewCommentForm.getType().equals("MSA")) {
			request.setAttribute("MSA_Id",
					menuFunctionViewCommentForm.getForwardId());
			String msaStatus = MSAdao.getMSAStatus(
					menuFunctionViewCommentForm.getForwardId(),
					getDataSource(request, "ilexnewDB"));
			String msaUploadCheck = MSAdao.getMSAUploadCheck(
					menuFunctionViewCommentForm.getForwardId(),
					getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
					menuFunctionViewCommentForm.getForwardId(), "", "", "",
					loginuserid, getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);
			request.setAttribute("status", msaStatus);
			request.setAttribute("uploadstatus", msaUploadCheck);
		}

		if (menuFunctionViewCommentForm.getType().equals("Appendix")) {
			String appendixStatus = Appendixdao.getAppendixStatus(
					menuFunctionViewCommentForm.getForwardId(),
					menuFunctionViewCommentForm.getMsaId(),
					getDataSource(request, "ilexnewDB"));
			String addendumlist = Appendixdao.getAddendumsIdName(
					menuFunctionViewCommentForm.getForwardId(),
					getDataSource(request, "ilexnewDB"));
			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(
							menuFunctionViewCommentForm.getForwardId(),
							getDataSource(request, "ilexnewDB"));
			String appendixType = Appendixdao.getAppendixPrType(
					menuFunctionViewCommentForm.getMsaId(),
					menuFunctionViewCommentForm.getForwardId(),
					getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_appendix",
					appendixStatus.charAt(0),
					menuFunctionViewCommentForm.getMsaId(),
					menuFunctionViewCommentForm.getForwardId(), "", "",
					loginuserid, getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixStatusList", list);
			request.setAttribute("Appendix_Id",
					menuFunctionViewCommentForm.getForwardId());
			request.setAttribute("MSA_Id",
					menuFunctionViewCommentForm.getMsaId());
			request.setAttribute("appendixStatus", appendixStatus);
			request.setAttribute("latestaddendumid", addendumid);
			request.setAttribute("addendumlist", addendumlist);
			request.setAttribute("appendixType", appendixType);

		}

		if (menuFunctionViewCommentForm.getType().equals("Job")) {

			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					menuFunctionViewCommentForm.getForwardId(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String jobStatusList = "";
			String appendixType = "";

			if (jobType1.equals("Default"))
				jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
						+ menuFunctionViewCommentForm.getForwardId()
						+ "&Status='D'\"" + "," + "0";
			if (jobType1.equals("Newjob"))
				jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
						"", "", menuFunctionViewCommentForm.getForwardId(), "",
						loginuserid, getDataSource(request, "ilexnewDB"));
			if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
				jobStatusList = "\"Approved\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ menuFunctionViewCommentForm.getForwardId()
						+ "&Status=A\"" + "," + "0";
			if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
				jobStatusList = "\"Draft\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ menuFunctionViewCommentForm.getForwardId()
						+ "&Status=D\"" + "," + "0";
			if (jobType1.equals("Addendum"))
				jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
						"", "", menuFunctionViewCommentForm.getForwardId(), "",
						loginuserid, getDataSource(request, "ilexnewDB"));

			appendixType = ViewList.getAppendixtypedesc(
					menuFunctionViewCommentForm.getAppendixId(),
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("jobStatus", jobStatus);
			request.setAttribute("job_type", jobType1);
			request.setAttribute("appendixtype", appendixType);
			request.setAttribute("Job_Id",
					menuFunctionViewCommentForm.getForwardId());
			request.setAttribute("Appendix_Id",
					menuFunctionViewCommentForm.getAppendixId());
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("addendum_id", "0");

			if (jobType1.equals("Newjob")) {
				request.setAttribute("chkadd", "detailjob");
				request.setAttribute("chkaddendum", "detailjob");
				request.setAttribute("chkaddendumactivity", "View");
			}
			if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
				request.setAttribute("chkadd", "inscopejob");
				request.setAttribute("chkaddendum", "inscopejob");
				request.setAttribute("chkaddendumactivity", "inscopeactivity");
			}

			if (jobType1.equalsIgnoreCase("Default")
					&& menuFunctionViewCommentForm.getType().equalsIgnoreCase(
							"jobdashboard")) {
				request.setAttribute("chkadd", "inscopejob");
				request.setAttribute("chkaddendum", "inscopejob");
				request.setAttribute("chkaddendumactivity", "inscopeactivity");
			}

			if (jobType1.equalsIgnoreCase("Default")
					&& !menuFunctionViewCommentForm.getType().equalsIgnoreCase(
							"jobdashboard")) {
				request.setAttribute("chkadd", "detailjob");
				request.setAttribute("chkaddendum", "detailjob");
				request.setAttribute("chkaddendumactivity", "View");
			}

		}

		if (menuFunctionViewCommentForm.getType().equals("Activity")) {
			String jobtype = "";
			String activityStatus = "";
			String activityType = "";
			String activityStatusList = "";
			String MSA_Id = "";
			String MSA_Name = "";
			int checkNetmedXappendix = 0;
			String[] activityStatusAndType = Activitydao.getActivityTypeStatus(
					menuFunctionViewCommentForm.getForwardId(),
					getDataSource(request, "ilexnewDB"));
			activityStatus = activityStatusAndType[0];
			activityType = activityStatusAndType[1];

			if (request.getParameter("from") != null) {
				from = request.getParameter("from");
				jobid = request.getParameter("jobid");
				request.setAttribute("from", request.getParameter("from"));
				request.setAttribute("jobid", request.getParameter("jobid"));
			}

			jobtype = Jobdao.getJobtype(menuFunctionViewCommentForm.getJobId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("jobtype", jobtype);

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("view")
					|| menuFunctionViewCommentForm.getRef().equalsIgnoreCase(
							"detailactivity")) {
				request.setAttribute("chkaddendum", "detailactivity");
				request.setAttribute("chkaddendumresource", "View");

				if (request.getParameter("from") != null) {
					if (jobtype.equals("Addendum") || jobtype.equals("Default")) {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), from, jobid,
								menuFunctionViewCommentForm.getForwardId(),
								"detailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					} else {
						activityStatus = JobDashboarddao
								.getActivityprojectstatus(
										menuFunctionViewCommentForm
												.getForwardId(),
										getDataSource(request, "ilexnewDB"));
						activityStatusList = Menu.getStatus("prj_act",
								activityStatus.charAt(0), from, jobid,
								menuFunctionViewCommentForm.getForwardId(),
								"detailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					}
				} else {
					activityStatusList = Menu.getStatus("pm_act",
							activityStatus.charAt(0), "", "",
							menuFunctionViewCommentForm.getForwardId(),
							"detailactivity", loginuserid,
							getDataSource(request, "ilexnewDB"));

				}

			}

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase(
					"inscopeactivity")
					|| menuFunctionViewCommentForm.getRef().equalsIgnoreCase(
							"inscopedetailactivity")) {
				request.setAttribute("chkaddendum", "inscopedetailactivity");
				request.setAttribute("chkaddendumresource", "Viewinscope");

				if (request.getParameter("from") != null) {

					if (jobtype.equals("Addendum") || jobtype.equals("Default")) {
						activityStatusList = Menu.getStatus("pm_act",
								activityStatus.charAt(0), from, jobid,
								menuFunctionViewCommentForm.getForwardId(),
								"inscopedetailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					} else {
						activityStatus = JobDashboarddao
								.getActivityprojectstatus(
										menuFunctionViewCommentForm
												.getForwardId(),
										getDataSource(request, "ilexnewDB"));
						activityStatusList = Menu.getStatus("prj_act",
								activityStatus.charAt(0), from, jobid,
								menuFunctionViewCommentForm.getForwardId(),
								"inscopedetailactivity", loginuserid,
								getDataSource(request, "ilexnewDB"));
					}
				} else {
					activityStatusList = Menu.getStatus("pm_act",
							activityStatus.charAt(0), "", "",
							menuFunctionViewCommentForm.getForwardId(),
							"inscopedetailactivity", loginuserid,
							getDataSource(request, "ilexnewDB"));

				}

			}

			MSA_Id = Activitydao.getmsaid(getDataSource(request, "ilexnewDB"),
					menuFunctionViewCommentForm.getJobId());
			MSA_Name = Activitydao.getMsaname(
					getDataSource(request, "ilexnewDB"), MSA_Id);
			request.setAttribute("MSA_Name", MSA_Name);
			request.setAttribute("MSA_Id", MSA_Id);

			checkNetmedXappendix = Activitydao.checkAppendixname(
					menuFunctionViewCommentForm.getAppendixId(),
					menuFunctionViewCommentForm.getJobId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("checknetmedx", checkNetmedXappendix + "");
			request.setAttribute("activityStatusList", activityStatusList);
			request.setAttribute("Activity_Id",
					menuFunctionViewCommentForm.getForwardId());
			request.setAttribute("Job_Id",
					menuFunctionViewCommentForm.getJobId());
			request.setAttribute("jobtype", jobtype);
			request.setAttribute("from", from);
			request.setAttribute("jobid", jobid);
			request.setAttribute("activityStatus", activityStatus);
			request.setAttribute("activityType", activityType);
			request.setAttribute("activityTypeForJob",
					menuFunctionViewCommentForm.getRef());

		}

		if (menuFunctionViewCommentForm.getType().equals("pm_resM")) {
			// For the Material Menu
			String materialType = "";
			String materialStatus = "";
			String materialStatusList = "";

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("View"))
				materialType = "M";
			else
				materialType = "IM";

			materialStatus = Resourcedao.getResourceStatus(
					menuFunctionViewCommentForm.getActivity_Id(),
					menuFunctionViewCommentForm.getForwardId(), materialType,
					request.getSession().getId(),
					getDataSource(request, "ilexnewDB"));

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("View")) {
				request.setAttribute("chkaddendum", "View");
				request.setAttribute("chkdetailactivity", "detailactivity");

				if (request.getParameter("from") != null)
					materialStatusList = Menu.getStatus("jobdashboard_resM",
							materialStatus.charAt(0), from,
							request.getParameter("jobid"), "View",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
				else
					materialStatusList = Menu.getStatus("pm_resM",
							materialStatus.charAt(0), "", "", "",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));

			}

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase(
					"Viewinscope")) {
				request.setAttribute("chkaddendum", "Viewinscope");
				request.setAttribute("chkdetailactivity",
						"inscopedetailactivity");

				if (request.getParameter("from") != null)
					materialStatusList = Menu.getStatus("jobdashboard_resM",
							materialStatus.charAt(0), from,
							request.getParameter("jobid"), "Viewinscope",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
				else
					materialStatusList = Menu.getStatus("jobdashboard_resM",
							materialStatus.charAt(0), "", "", "Viewinscope",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
			}

			request.setAttribute("Activity_Id",
					menuFunctionViewCommentForm.getActivity_Id());
			request.setAttribute("Material_Id",
					menuFunctionViewCommentForm.getForwardId());
			request.setAttribute("commonResourceStatus", materialStatus);
			request.setAttribute("materialStatusList", materialStatusList);
			request.setAttribute("checkingRef",
					menuFunctionViewCommentForm.getRef());
			request.setAttribute("addendum_id", "0");

			/*
			 * System.out.println("Activity_Id ::: " +
			 * menuFunctionViewCommentForm.getActivity_Id());
			 * System.out.println("Material_Id ::: " +
			 * menuFunctionViewCommentForm.getForwardId());
			 * System.out.println("Material Status ::: " + materialStatus);
			 * System.out.println("Material Status list ::: " +
			 * materialStatusList); System.out.println("Addendum Id ::: " +
			 * "0"); System.out.println("From  ::::" + from );
			 * System.out.println("job id ::::" + jobid );
			 */
			// End of the Menu Code for Resource Material
		}
		if (menuFunctionViewCommentForm.getType().equals("pm_resL")) {
			// For the Material Menu
			String laborType = "";
			String laborStatus = "";
			String laborStatusList = "";

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("View"))
				laborType = "L";
			else
				laborType = "IL";

			laborStatus = Resourcedao.getResourceStatus(
					menuFunctionViewCommentForm.getActivity_Id(),
					menuFunctionViewCommentForm.getForwardId(), laborType,
					request.getSession().getId(),
					getDataSource(request, "ilexnewDB"));

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("View")) {
				request.setAttribute("chkaddendum", "View");
				request.setAttribute("chkdetailactivity", "detailactivity");

				if (request.getParameter("from") != null)
					laborStatusList = Menu.getStatus("jobdashboard_resL",
							laborStatus.charAt(0), from,
							request.getParameter("jobid"), "View",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
				else
					laborStatusList = Menu.getStatus("pm_resL",
							laborStatus.charAt(0), "", "", "",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));

			}

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase(
					"Viewinscope")) {
				request.setAttribute("chkaddendum", "Viewinscope");
				request.setAttribute("chkdetailactivity",
						"inscopedetailactivity");

				if (request.getParameter("from") != null)
					laborStatusList = Menu.getStatus("jobdashboard_resL",
							laborStatus.charAt(0), from,
							request.getParameter("jobid"), "Viewinscope",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
				else
					laborStatusList = Menu.getStatus("jobdashboard_resL",
							laborStatus.charAt(0), "", "", "Viewinscope",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
			}

			request.setAttribute("Activity_Id",
					menuFunctionViewCommentForm.getActivity_Id());
			request.setAttribute("Labor_Id",
					menuFunctionViewCommentForm.getForwardId());
			request.setAttribute("commonResourceStatus", laborStatus);
			request.setAttribute("laborStatusList", laborStatusList);
			request.setAttribute("checkingRef",
					menuFunctionViewCommentForm.getRef());
			request.setAttribute("addendum_id", "0");

			/*
			 * System.out.println("Activity_Id ::: " +
			 * menuFunctionViewCommentForm.getActivity_Id());
			 * System.out.println("labor_Id ::: " +
			 * menuFunctionViewCommentForm.getForwardId());
			 * System.out.println("labor Status ::: " + laborStatus);
			 * System.out.println("labor Status list ::: " + laborStatusList);
			 * System.out.println("Addendum Id ::: " + "0");
			 * System.out.println("From  ::::" + from );
			 * System.out.println("job id ::::" + jobid );
			 */
			// End of the Menu Code for Resource labor
		}

		if (menuFunctionViewCommentForm.getType().equals("pm_resF")) {
			// For the Freight Menu
			String freightType = "";
			String freightStatus = "";
			String freightStatusList = "";

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("View"))
				freightType = "F";
			else
				freightType = "IF";

			freightStatus = Resourcedao.getResourceStatus(
					menuFunctionViewCommentForm.getActivity_Id(),
					menuFunctionViewCommentForm.getForwardId(), freightType,
					request.getSession().getId(),
					getDataSource(request, "ilexnewDB"));

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("View")) {
				request.setAttribute("chkaddendum", "View");
				request.setAttribute("chkdetailactivity", "detailactivity");

				if (request.getParameter("from") != null)
					freightStatusList = Menu.getStatus("jobdashboard_resF",
							freightStatus.charAt(0), from,
							request.getParameter("jobid"), "View",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
				else
					freightStatusList = Menu.getStatus("pm_resF",
							freightStatus.charAt(0), "", "", "",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));

			}

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase(
					"Viewinscope")) {
				request.setAttribute("chkaddendum", "Viewinscope");
				request.setAttribute("chkdetailactivity",
						"inscopedetailactivity");

				if (request.getParameter("from") != null)
					freightStatusList = Menu.getStatus("jobdashboard_resF",
							freightStatus.charAt(0), from,
							request.getParameter("jobid"), "Viewinscope",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
				else
					freightStatusList = Menu.getStatus("jobdashboard_resF",
							freightStatus.charAt(0), "", "", "Viewinscope",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
			}

			request.setAttribute("Activity_Id",
					menuFunctionViewCommentForm.getActivity_Id());
			request.setAttribute("Freight_Id",
					menuFunctionViewCommentForm.getForwardId());
			request.setAttribute("commonResourceStatus", freightStatus);
			request.setAttribute("freightStatusList", freightStatusList);
			request.setAttribute("checkingRef",
					menuFunctionViewCommentForm.getRef());
			request.setAttribute("addendum_id", "0");

			/*
			 * System.out.println("Activity_Id ::: " +
			 * menuFunctionViewCommentForm.getActivity_Id());
			 * System.out.println("freight_Id ::: " +
			 * menuFunctionViewCommentForm.getForwardId());
			 * System.out.println("freight Status ::: " + freightStatus);
			 * System.out.println("freight Status list ::: " +
			 * freightStatusList); System.out.println("Addendum Id ::: " + "0");
			 * System.out.println("From  ::::" + from );
			 * System.out.println("job id ::::" + jobid );
			 */
			// End of the Menu Code for Resource Freight
		}

		if (menuFunctionViewCommentForm.getType().equals("pm_resT")) {
			// For the Travel Menu
			String travelType = "";
			String travelStatus = "";
			String travelStatusList = "";

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("View"))
				travelType = "T";
			else
				travelType = "IT";

			travelStatus = Resourcedao.getResourceStatus(
					menuFunctionViewCommentForm.getActivity_Id(),
					menuFunctionViewCommentForm.getForwardId(), travelType,
					request.getSession().getId(),
					getDataSource(request, "ilexnewDB"));

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase("View")) {
				request.setAttribute("chkaddendum", "View");
				request.setAttribute("chkdetailactivity", "detailactivity");

				if (request.getParameter("from") != null)
					travelStatusList = Menu.getStatus("jobdashboard_resT",
							travelStatus.charAt(0), from,
							request.getParameter("jobid"), "View",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
				else
					travelStatusList = Menu.getStatus("pm_resT",
							travelStatus.charAt(0), "", "", "",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));

			}

			if (menuFunctionViewCommentForm.getRef().equalsIgnoreCase(
					"Viewinscope")) {
				request.setAttribute("chkaddendum", "Viewinscope");
				request.setAttribute("chkdetailactivity",
						"inscopedetailactivity");

				if (request.getParameter("from") != null)
					travelStatusList = Menu.getStatus("jobdashboard_resT",
							travelStatus.charAt(0), from,
							request.getParameter("jobid"), "Viewinscope",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
				else
					travelStatusList = Menu.getStatus("jobdashboard_resT",
							travelStatus.charAt(0), "", "", "Viewinscope",
							menuFunctionViewCommentForm.getForwardId(),
							loginuserid, getDataSource(request, "ilexnewDB"));
			}

			request.setAttribute("Activity_Id",
					menuFunctionViewCommentForm.getActivity_Id());
			request.setAttribute("Travel_Id",
					menuFunctionViewCommentForm.getForwardId());
			request.setAttribute("commonResourceStatus", travelStatus);
			request.setAttribute("travelStatusList", travelStatusList);
			request.setAttribute("checkingRef",
					menuFunctionViewCommentForm.getRef());
			request.setAttribute("addendum_id", "0");

			/*
			 * System.out.println("Activity_Id ::: " +
			 * menuFunctionViewCommentForm.getActivity_Id());
			 * System.out.println("travel_Id ::: " +
			 * menuFunctionViewCommentForm.getForwardId());
			 * System.out.println("travel Status ::: " + travelStatus);
			 * System.out.println("freight Status list ::: " +
			 * travelStatusList); System.out.println("Addendum Id ::: " + "0");
			 * System.out.println("From  ::::" + from );
			 * System.out.println("job id ::::" + jobid );
			 */
			// End of the Menu Code for Resource Travel
		}

		request.setAttribute("checkingRef",
				menuFunctionViewCommentForm.getRef());
		return (mapping.findForward("viewcommentpage"));
	}
}