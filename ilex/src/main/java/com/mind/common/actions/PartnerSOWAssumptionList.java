package com.mind.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Menu;
import com.mind.common.dao.PartnerSOWAssumptiondao;
import com.mind.common.dao.ViewList;
import com.mind.common.formbean.PartnerSOWAssumptionForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.util.WebUtil;

public class PartnerSOWAssumptionList extends com.mind.common.IlexAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		PartnerSOWAssumptionForm partnerSOWassumptionform = (PartnerSOWAssumptionForm) form;

		HttpSession session = request.getSession(true);
		String loginUserId = (String) session.getAttribute("userid");

		/* Start : Appendix Dashboard View Selector Code */
		ArrayList jobOwnerList = new ArrayList();
		ArrayList jobAppStatusList = new ArrayList();
		String jobSelectedStatus = "";
		String jobSelectedOwner = "";
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;

		if (request.getParameter("fromId") != null) {
			partnerSOWassumptionform.setFromId(request.getParameter("fromId"));
		}

		if (request.getParameter("fromType") != null)
			partnerSOWassumptionform.setFromType(request
					.getParameter("fromType"));

		if (request.getParameter("viewType") != null)
			partnerSOWassumptionform.setViewType(request
					.getParameter("viewType"));

		if (request.getParameter("viewjobtype") != null)
			partnerSOWassumptionform.setViewJobType(request
					.getParameter("viewjobtype"));

		if (request.getParameter("ownerId") != null)
			partnerSOWassumptionform
					.setOwnerId(request.getParameter("ownerId"));

		if (request.getParameter("nettype") != null)
			partnerSOWassumptionform
					.setNetType(request.getParameter("nettype"));

		if (request.getParameter("addFlag") != null)
			partnerSOWassumptionform
					.setAddFlag(request.getParameter("addFlag"));

		/* Delete a sow */
		if (request.getParameter("action") != null) {
			if (request.getParameter("action").equals("delete")) {
				if (request.getParameter("SOWAssumptionId") != null)
					partnerSOWassumptionform.setDeleteSOWAssumptionId(request
							.getParameter("SOWAssumptionId"));

				int retvalue = -1;
				retvalue = PartnerSOWAssumptiondao.deletePartnerSOWAssumption(
						partnerSOWassumptionform.getDeleteSOWAssumptionId(),
						partnerSOWassumptionform.getViewType(),
						getDataSource(request, "ilexnewDB"));

				request.setAttribute("deleteFlag", retvalue + "");
			}

			if (request.getParameter("action").equals("getdefault")) {
				// partnerSOWassumptionform.setDeleteSOWAssumptionId(request.getParameter("SOWAssumptionId"));
				partnerSOWassumptionform
						.setPartnerInformation(PartnerSOWAssumptiondao
								.getDefaultPartnerSOWAssumption(
										partnerSOWassumptionform.getActId(),
										partnerSOWassumptionform.getFromType(),
										partnerSOWassumptionform.getViewType(),
										"customer",
										getDataSource(request, "ilexnewDB")));
				int retvalue = -1;
				if (!partnerSOWassumptionform.getPartnerInformation()
						.equals(""))
					retvalue = PartnerSOWAssumptiondao
							.managePartnerSOWAssumption(
									partnerSOWassumptionform.getActId(),
									partnerSOWassumptionform.getFromType(),
									partnerSOWassumptionform
											.getPartnerInformation(),
									partnerSOWassumptionform.getViewType(),
									loginUserId,
									getDataSource(request, "ilexnewDB"));
				// else // This will delete the existing Partner SOW/Assumption
				// for Activity if Customer is blank.
				// retvalue =
				// PartnerSOWAssumptiondao.deletePartnerSOWAssumption(partnerSOWassumptionform.getDeleteSOWAssumptionId(),
				// partnerSOWassumptionform.getViewType(), getDataSource(
				// request , "ilexnewDB" ));
			}
		}

		/* Get list of activities */
		ArrayList partnerSOWassumptionList = PartnerSOWAssumptiondao
				.getPartnerSOWAssumptionList(
						partnerSOWassumptionform.getFromId(),
						partnerSOWassumptionform.getFromType(),
						partnerSOWassumptionform.getViewType(),
						getDataSource(request, "ilexnewDB"));
		request.setAttribute("partnerSOWassumptionList",
				partnerSOWassumptionList);

		/* Set header variable */
		if (partnerSOWassumptionform.getViewType().equals("sow"))
			partnerSOWassumptionform.setDataType("SOW");

		if (partnerSOWassumptionform.getViewType().equals("assumption"))
			partnerSOWassumptionform.setDataType("Assumptions");

		if (partnerSOWassumptionform.getFromType().equals("am_act")) {
			String msaName = Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					partnerSOWassumptionform.getFromId());
			partnerSOWassumptionform
					.setContext("<a href=ActivitySummaryAction.do?ref=View&MSA_Id="
							+ partnerSOWassumptionform.getFromId()
							+ ">"
							+ msaName + "</a>");

			if (msaName.equals("Master Library"))
				partnerSOWassumptionform.setContextType("Master Library");
			else
				partnerSOWassumptionform.setContextType("MSA");
		}

		if (partnerSOWassumptionform.getFromType().equals("pm_act")) {
			partnerSOWassumptionform.setContextType("Appendix");
			partnerSOWassumptionform
					.setContext("<a href=JobDetailAction.do?addendum_id=0&ref=detailjob&Job_Id="
							+ partnerSOWassumptionform.getFromId()
							+ ">"
							+ Appendixdao.getAppendixname(
									getDataSource(request, "ilexnewDB"), IMdao
											.getAppendixId(
													partnerSOWassumptionform
															.getFromId(),
													getDataSource(request,
															"ilexnewDB")))
							+ "</a>");
			partnerSOWassumptionform.setAppendixId(IMdao.getAppendixId(
					partnerSOWassumptionform.getFromId(),
					getDataSource(request, "ilexnewDB")));
			partnerSOWassumptionform.setAppendixName(Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"),
					partnerSOWassumptionform.getAppendixId()));
			partnerSOWassumptionform.setMsaId(IMdao.getMSAId(
					partnerSOWassumptionform.getAppendixId(),
					getDataSource(request, "ilexnewDB")));
			partnerSOWassumptionform.setMsaName(com.mind.dao.PM.Appendixdao
					.getMsaname(getDataSource(request, "ilexnewDB"),
							partnerSOWassumptionform.getMsaId()));

			partnerSOWassumptionform.setJobName(Activitydao.getJobname(
					getDataSource(request, "ilexnewDB"),
					partnerSOWassumptionform.getFromId()));
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					partnerSOWassumptionform.getFromId(), "%", "%",
					getDataSource(request, "ilexnewDB"));
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String jobStatusList = "";
			String appendixType = "";

			if (jobType1.equals("Default"))
				jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
						+ partnerSOWassumptionform.getFromId()
						+ "&Status='D'\"" + "," + "0";
			if (jobType1.equals("Newjob"))
				jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
						"", "", partnerSOWassumptionform.getFromId(), "",
						loginUserId, getDataSource(request, "ilexnewDB"));
			if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
				jobStatusList = "\"Approved\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ partnerSOWassumptionform.getFromId() + "&Status=A\""
						+ "," + "0";
			if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
				jobStatusList = "\"Draft\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ partnerSOWassumptionform.getFromId() + "&Status=D\""
						+ "," + "0";
			if (jobType1.equals("Addendum"))
				jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
						"", "", partnerSOWassumptionform.getFromId(), "",
						loginUserId, getDataSource(request, "ilexnewDB"));

			appendixType = ViewList.getAppendixtypedesc(
					partnerSOWassumptionform.getAppendixId(),
					getDataSource(request, "ilexnewDB"));

			request.setAttribute("jobStatus", jobStatus);
			request.setAttribute("job_type", jobType1);
			request.setAttribute("appendixtype", appendixType);
			request.setAttribute("Job_Id", partnerSOWassumptionform.getFromId());
			request.setAttribute("Appendix_Id",
					partnerSOWassumptionform.getAppendixId());
			request.setAttribute("jobStatusList", jobStatusList);
			request.setAttribute("addendum_id", "0");

			if (jobType1.equals("Newjob") || jobType1.equals("Default")) {
				request.setAttribute("chkadd", "detailjob");
				request.setAttribute("chkaddendum", "detailjob");
				request.setAttribute("chkaddendumactivity", "View");
			}
			if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
				request.setAttribute("chkadd", "inscopejob");
				request.setAttribute("chkaddendum", "inscopejob");
				request.setAttribute("chkaddendumactivity", "inscopeactivity");
			}

		}

		if (partnerSOWassumptionform.getFromType().equals("prm_act")) {
			partnerSOWassumptionform.setContextType("Appendix");
			partnerSOWassumptionform
					.setContext("<a href=AppendixHeader.do?function=view&viewjobtype="
							+ partnerSOWassumptionform.getViewJobType()
							+ "&ownerId="
							+ partnerSOWassumptionform.getOwnerId()
							+ "&appendixid="
							+ partnerSOWassumptionform.getFromId()
							+ ">"
							+ Appendixdao.getAppendixname(
									getDataSource(request, "ilexnewDB"),
									partnerSOWassumptionform.getFromId())
							+ "</a>");
			partnerSOWassumptionform.setMsaId(IMdao.getMSAId(
					partnerSOWassumptionform.getFromId(),
					getDataSource(request, "ilexnewDB")));
			String contractDocoumentList = com.mind.dao.PM.Appendixdao
					.getcontractDocoumentMenu(
							partnerSOWassumptionform.getFromId(),
							getDataSource(request, "ilexnewDB"));
			request.setAttribute("contractDocMenu", contractDocoumentList);
			String appendixcurrentstatus = Appendixdao.getCurrentstatus(
					partnerSOWassumptionform.getFromId(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
			request.setAttribute("appendixid",
					partnerSOWassumptionform.getFromId());
			request.setAttribute("msaId", partnerSOWassumptionform.getMsaId());
			request.setAttribute("viewjobtype",
					partnerSOWassumptionform.getViewJobType());
			request.setAttribute("ownerId",
					partnerSOWassumptionform.getOwnerId());

			/* Start : Appendix Dashboard View Selector Code */
			if (request.getParameter("fromPage") != null)
				partnerSOWassumptionform.setFromPage(request
						.getParameter("fromPage"));

			if (partnerSOWassumptionform.getJobOwnerOtherCheck() != null
					&& !partnerSOWassumptionform.getJobOwnerOtherCheck()
							.equals("")) {
				session.setAttribute("jobOwnerOtherCheck",
						partnerSOWassumptionform.getJobOwnerOtherCheck());
			}

			if (request.getParameter("opendiv") != null) {
				request.setAttribute("opendiv", request.getParameter("opendiv"));
			}
			if (request.getParameter("resetList") != null
					&& request.getParameter("resetList").equals("something")) {
				WebUtil.setDefaultAppendixDashboardAttribute(session);
				request.setAttribute("type", "AppendixViewSelector");
				request.setAttribute("appendixId",
						partnerSOWassumptionform.getFromId());
				request.setAttribute("msaId",
						partnerSOWassumptionform.getMsaId());

				return mapping.findForward("temppage");

			}

			if (partnerSOWassumptionform.getJobSelectedStatus() != null) {
				for (int i = 0; i < partnerSOWassumptionform
						.getJobSelectedStatus().length; i++) {
					jobSelectedStatus = jobSelectedStatus + "," + "'"
							+ partnerSOWassumptionform.getJobSelectedStatus(i)
							+ "'";
					jobTempStatus = jobTempStatus + ","
							+ partnerSOWassumptionform.getJobSelectedStatus(i);
				}

				jobSelectedStatus = jobSelectedStatus.substring(1,
						jobSelectedStatus.length());
				jobSelectedStatus = "(" + jobSelectedStatus + ")";
			}

			if (partnerSOWassumptionform.getJobSelectedOwners() != null) {
				for (int j = 0; j < partnerSOWassumptionform
						.getJobSelectedOwners().length; j++) {
					jobSelectedOwner = jobSelectedOwner + ","
							+ partnerSOWassumptionform.getJobSelectedOwners(j);
					jobTempOwner = jobTempOwner + ","
							+ partnerSOWassumptionform.getJobSelectedOwners(j);
				}
				jobSelectedOwner = jobSelectedOwner.substring(1,
						jobSelectedOwner.length());
				if (jobSelectedOwner.equals("0")) {
					jobSelectedOwner = "%";
				}
				jobSelectedOwner = "(" + jobSelectedOwner + ")";
			}

			if (request.getParameter("showList") != null
					&& request.getParameter("showList").equals("something")) {

				session.setAttribute("jobSelectedOwners", jobSelectedOwner);
				session.setAttribute("jobTempOwner", jobTempOwner);
				session.setAttribute("jobSelectedStatus", jobSelectedStatus);
				session.setAttribute("jobTempStatus", jobTempStatus);
				session.setAttribute("timeFrame",
						partnerSOWassumptionform.getJobMonthWeekCheck());
				session.setAttribute("jobOwnerOtherCheck",
						partnerSOWassumptionform.getJobOwnerOtherCheck());

				request.setAttribute("type", "AppendixViewSelector");
				request.setAttribute("appendixId",
						partnerSOWassumptionform.getFromId());
				request.setAttribute("msaId",
						partnerSOWassumptionform.getMsaId());

				return mapping.findForward("temppage");

			} else {
				if (session.getAttribute("jobSelectedOwners") != null) {
					jobSelectedOwner = session
							.getAttribute("jobSelectedOwners").toString();
					partnerSOWassumptionform
							.setJobSelectedOwners(session
									.getAttribute("jobTempOwner").toString()
									.split(","));
				}
				if (session.getAttribute("jobSelectedStatus") != null) {
					jobSelectedStatus = session.getAttribute(
							"jobSelectedStatus").toString();
					partnerSOWassumptionform.setJobSelectedStatus(session
							.getAttribute("jobTempStatus").toString()
							.split(","));
				}
				if (session.getAttribute("jobOwnerOtherCheck") != null) {
					partnerSOWassumptionform.setJobOwnerOtherCheck(session
							.getAttribute("jobOwnerOtherCheck").toString());
				}
				if (session.getAttribute("timeFrame") != null) {
					partnerSOWassumptionform.setJobMonthWeekCheck(session
							.getAttribute("timeFrame").toString());
				}
			}

			jobOwnerList = MSAdao.getOwnerList(loginUserId,
					partnerSOWassumptionform.getJobOwnerOtherCheck(),
					"prj_job_new", partnerSOWassumptionform.getFromId(),
					getDataSource(request, "ilexnewDB"));
			jobAppStatusList = MSAdao.getStatusList("prj_job_new",
					getDataSource(request, "ilexnewDB"));

			if (jobOwnerList.size() > 0)
				jobOwnerListSize = jobOwnerList.size();

			request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
			request.setAttribute("jobAppStatusList", jobAppStatusList);
			request.setAttribute("jobOwnerList", jobOwnerList);
			/* End : Appendix Dashboard View Selector Code */

		}

		if (partnerSOWassumptionList != null)
			request.setAttribute("partnerSOWassumptionListSize",
					partnerSOWassumptionList.size() + "");

		return mapping.findForward("success");
	}

}
