package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.formbean.PdfNotGeneratedForm;

public class PdfNotGeneratedAction extends com.mind.common.IlexAction
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PdfNotGeneratedAction.class);

	public ActionForward execute( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		PdfNotGeneratedForm pdfNotGeneratedForm = (PdfNotGeneratedForm)form;
		if (logger.isDebugEnabled()) {
			logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In Pdf Not Generated Action ::");
		}
		

		if (logger.isDebugEnabled()) {
			logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - " + pdfNotGeneratedForm.getGeneratePDF());
		}
		
		return mapping.findForward("success");
	}
}
