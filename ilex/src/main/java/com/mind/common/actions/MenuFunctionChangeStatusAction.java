//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : MenuFuncitonChangeStatusAction Servlet
//  @ Date : 12-June-2008
//  @ Author : Vishal Kapoor (Modified cettain part for DocM Changes) 
//
//
package com.mind.common.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MPO.dao.MPODao;
import com.mind.bean.mpo.MPODTO;
import com.mind.common.EnvironmentSelector;
import com.mind.common.InteractionToDocM;
import com.mind.common.dao.ChangeStatus;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.docm.dao.EntityManager;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class MenuFunctionChangeStatusAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MenuFunctionChangeStatusAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String Type = "";
		String MSA_Id = "";
		String Appendix_Id = "";
		String status = "";
		String Job_Id = "";
		String firstjobsuccess = "";
		String Activitylibrary_Id = "";
		String resource_Id = "";
		String appendixprevstatus = "";
		HttpSession session = request.getSession(true);
		String userid = (String) session.getAttribute("userid");
		String jobType = "";
		String userId = "";
		String docId = null;
		int changestatusflag = -1;
		String entityId = null;
		boolean pdfInsert = false;
		boolean pdfDraft = false;
		boolean pdfCancel = false;
		String entityIdType[] = null;
		boolean exceptionOccurred = false;
		boolean addendumJob = false;

		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		userId = session.getAttribute("userid").toString();
		String loginusername = (String) session.getAttribute("username");

		if (request.getParameter("Type") != null) {
			Type = request.getParameter("Type");
			request.setAttribute("Type", Type);
		}

		// Start :Added By Amit For First Job success

		if (request.getAttribute("jobid") != null) {
			Job_Id = (String) request.getAttribute("jobid");
			request.setAttribute("Job_Id", Job_Id);
		}

		if (request.getAttribute("status") != null) {
			status = (String) request.getAttribute("status");
			request.setAttribute("status", status);
		}

		if (request.getParameter("status") != null) {
			status = request.getParameter("status");
			request.setAttribute("status", status);
		}

		if (request.getAttribute("firstjobsuccess") != null) {
			firstjobsuccess = (String) request.getAttribute("firstjobsuccess");
		} else {
			firstjobsuccess = "%";
		}

		if (request.getAttribute("Type") != null) {
			Type = (String) request.getAttribute("Type");
			request.setAttribute("Type", Type);
		}

		// End : Added By Amit For First Job success

		if (request.getParameter("MSA_Id") != null) {
			MSA_Id = request.getParameter("MSA_Id");
			entityId = request.getParameter("MSA_Id");
			request.setAttribute("MSA_Id", MSA_Id);
		}

		if (request.getParameter("Appendix_Id") != null) {
			Appendix_Id = request.getParameter("Appendix_Id");
			entityId = request.getParameter("Appendix_Id");
			request.setAttribute("Appendix_Id", Appendix_Id);
		}

		if (request.getParameter("Job_Id") != null) {
			Job_Id = request.getParameter("Job_Id");
			request.setAttribute("Job_Id", Job_Id);
		}

		// am
		if (request.getParameter("Activitylibrary_Id") != null)
			Activitylibrary_Id = request.getParameter("Activitylibrary_Id");

		// am
		if (request.getParameter("resource_Id") != null)
			resource_Id = request.getParameter("resource_Id");

		if (request.getParameter("Status") != null) {
			status = request.getParameter("Status");
			request.setAttribute("status", status);
		}
		// pm
		if (request.getParameter("Activity_Id") != null)
			Activitylibrary_Id = request.getParameter("Activity_Id");

		// prm
		if (request.getParameter("appendixid") != null) {
			Appendix_Id = request.getParameter("appendixid");
			entityId = request.getParameter("appendixid");
			request.setAttribute("Appendix_Id", Appendix_Id);
		}

		// prm
		if (request.getParameter("jobid") != null) {
			Job_Id = request.getParameter("jobid");
			request.setAttribute("Job_Id", Job_Id);
		}

		// prm
		if (request.getParameter("activityid") != null) {
			Activitylibrary_Id = request.getParameter("activityid");
		}

		if (Type.equals("Appendix")) {
			appendixprevstatus = Appendixdao.getAppendixcurrentstate(
					getDataSource(request, "ilexnewDB"), Appendix_Id);
			if (appendixprevstatus.equals("T") && status.equals("D")) {
				request.setAttribute("type", "Appendix");
				request.setAttribute("Appendix_Id", Appendix_Id);

				return (mapping.findForward("dispatchactionpages"));
			}

		}

		if (Type.trim().equalsIgnoreCase("prj_job")) {
			String[] checkJobType = null;
			checkJobType = Jobdao.getJobStatusAndType("%", Job_Id, "%", "%",
					getDataSource(request, "ilexnewDB"));
			jobType = checkJobType[1];
			if (jobType != null) {
				if ("Addendum".equalsIgnoreCase(jobType.trim())) {
					addendumJob = true;
				}
			}

		}

		if (status.trim().equalsIgnoreCase("D")) {
			if ((Type.trim().equalsIgnoreCase("MSA")
					|| Type.trim().equalsIgnoreCase("Appendix") || (Type.trim()
					.equalsIgnoreCase("prj_job") && addendumJob == true))
					&& request.getParameter("resonCaptured") == null) {
				request.setAttribute("changeStatus", status);
				return mapping.findForward("captureReson");
			}
		}

		String url = EnvironmentSelector
				.getBundleString("appendix.detail.changestatus.mysql.url");
		if (logger.isDebugEnabled()) {
			logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - url---"
					+ url);
		}

		/*
		 * The following code is added to pass the PDF Doc to DocM when the
		 * Entity gets Approved
		 */
		// check if there is interaction with DocM i.e we have to transfer the
		// PDF to DocM
		if (Type.trim().equalsIgnoreCase("MSA")
				|| Type.trim().equalsIgnoreCase("Appendix")
				|| (Type.trim().equalsIgnoreCase("prj_job") && addendumJob == true)) {
			logger.trace("Status Changed to " + status);

			// code to check if the Ilex Entity is Previously Approved so avoid
			// calling the pdfInsert (Browser back button error)
			if (!InteractionToDocM.checkPreviousStatus(entityId, Type, Job_Id,
					getDataSource(request, "ilexnewDB")))
				pdfInsert = InteractionToDocM.checkPDFIntesert(entityId, Type,
						status, Job_Id, getDataSource(request, "ilexnewDB"));

			pdfDraft = InteractionToDocM.checkPDFDraft(entityId, Type, status,
					Job_Id, getDataSource(request, "ilexnewDB"));
			pdfCancel = InteractionToDocM.checkPDFCancel(entityId, Type,
					status, Job_Id, getDataSource(request, "ilexnewDB"));
		}

		if (pdfDraft || pdfCancel) {
			try {
				if (pdfDraft) {
					entityIdType = InteractionToDocM.getEntityIdType(entityId,
							Type, Job_Id);
					EntityManager.setEntityToDraft(entityIdType[0],
							entityIdType[1]);
				} else if (pdfCancel) {
					entityIdType = InteractionToDocM.getEntityIdType(entityId,
							Type, Job_Id);
					EntityManager.setEntityToDraft(entityIdType[0],
							entityIdType[1]);
				}
			} catch (Exception e) {
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				exceptionOccurred = true;
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);
			} finally {
				try {
					if (!exceptionOccurred) {
						changestatusflag = ChangeStatus.Status(MSA_Id,
								Appendix_Id, Job_Id, Activitylibrary_Id,
								resource_Id, Type, status, firstjobsuccess,
								getDataSource(request, "ilexnewDB"), url,
								userId);
					}
					if (!exceptionOccurred && changestatusflag != 0) {
						if (logger.isDebugEnabled()) {
							logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - If status not got changed then what to do");
						}
						entityIdType = InteractionToDocM.getEntityIdType(
								entityId, Type, Job_Id);
						EntityManager.setEntityToApproved(entityIdType[0],
								entityIdType[1]);
					}
				} catch (Exception e) {
					logger.error(
							"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
							e);
					logger.error(
							"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
							e);
				}
			}
		}

		if (pdfInsert) {
			docId = InteractionToDocM.callToDocMSystemForPdfInsertion(request,
					Type, InteractionToDocM.getIdBasedOnType(request, Type,
							getDataSource(request, "ilexnewDB")), status,
					userid, getDataSource(request, "ilexnewDB"));
		} else {
			if (!pdfDraft && !pdfCancel) {
				// If there is no interaction with DocM then simply change the
				// status
				changestatusflag = ChangeStatus.Status(MSA_Id, Appendix_Id,
						Job_Id, Activitylibrary_Id, resource_Id, Type, status,
						firstjobsuccess, getDataSource(request, "ilexnewDB"),
						url, userId);
				logger.trace("Status should get Changed with no DocM Interaction");
			}
		}

		// if there was the interaction with the DocM and docId is retrieved
		// successfully then allow the status change
		if (pdfInsert && docId != null && !docId.equals("")) {
			logger.trace("PDF Transferred : Status Change Allowed");
			changestatusflag = ChangeStatus.Status(MSA_Id, Appendix_Id, Job_Id,
					Activitylibrary_Id, resource_Id, Type, status,
					firstjobsuccess, getDataSource(request, "ilexnewDB"), url,
					userId);
		} else if (pdfInsert && docId == null) {
			logger.trace("PDF Not Transferred : Status should not get changed");
		}

		try {
			if (pdfInsert && changestatusflag != 0) {
				InteractionToDocM.delDocument(entityId, Type, Job_Id,
						loginusername);
			}
		} catch (Exception e) {
			logger.warn(
					"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored",
					e);
		}

		if ((pdfDraft || pdfCancel) && changestatusflag == 0) {
			InteractionToDocM.updateReasonInDocM(entityId, Type, Job_Id,
					getDataSource(request, "ilexnewDB"), loginusername);
		}

		/* Create Default MPO when Appendix Status changed to inwork. */
		if (changestatusflag == 0 && status.equalsIgnoreCase("I")
				&& Type.equals("Appendix")) {
			if (DatabaseUtilityDao.getProjectType(Appendix_Id,
					getDataSource(request, "ilexnewDB")).equalsIgnoreCase(
					"NetMedX"))
				createDefaultMPO(Appendix_Id, request,
						getDataSource(request, "ilexnewDB"));
		}

		request.setAttribute("status", status);
		request.setAttribute("changestatusflag", "" + changestatusflag);
		request.setAttribute("MSA_Id", MSA_Id);
		request.setAttribute("Appendix_Id", Appendix_Id);

		request.setAttribute("Activitylibrary_Id", Activitylibrary_Id);

		if (Type.equals("MSA")) {
			return (mapping.findForward("msadetailpage"));
		}

		if (Type.equals("Appendix")) {
			return (mapping.findForward("appendixdetailpage"));
		}

		if (Type.equals("Job")) {
			if (request.getParameter("ref") != null)
				request.setAttribute("ref", request.getParameter("ref"));
			else
				request.setAttribute("ref", "detailjob");
			request.setAttribute("type", Type);
			request.setAttribute("Job_Id", Job_Id);
			return (mapping.findForward("jobdetailpage"));
		}

		if (Type.equals("Activity")) {
			if (request.getParameter("ref") != null)
				request.setAttribute("ref", request.getParameter("ref"));
			else
				request.setAttribute("ref", "detailactivity");
			request.setAttribute("type", Type);
			request.setAttribute("Activity_Id", Activitylibrary_Id);
			return (mapping.findForward("activitydetailpage"));
		}

		if (Type.equals("prj_act")) {
			request.setAttribute("ref", request.getParameter("ref"));
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("id1", request.getParameter("activityid"));
			request.setAttribute("id2", request.getParameter("jobid"));
			request.setAttribute("type", Type);
			return (mapping.findForward("activitydetailfromprmpage"));
		}

		if (Type.contains("pm_res")) {
			request.setAttribute("type", Type);
			request.setAttribute("resource_Id", resource_Id);

			return (mapping.findForward("dispatchactionpages"));
		}

		if (Type.equals("am")) {
			return (mapping.findForward("activitylibrarydetailpage"));
		}

		/* customer labor rates */

		if (Type.equals("AM")) {
			request.setAttribute("type", Type);
			request.setAttribute("MSA_Id", MSA_Id);
			request.setAttribute("Activitylibrary_Id", Activitylibrary_Id);
			return (mapping.findForward("customerlaborratepage"));
		}

		if (Type.equals("AM_EDIT")) {
			request.setAttribute("type", Type);
			request.setAttribute("MSA_Id", MSA_Id);
			request.setAttribute("Activitylibrary_Id", Activitylibrary_Id);
			return (mapping.findForward("customerlaborratepage"));
		}

		if (Type.equals("PM")) {
			request.setAttribute("type", Type);
			request.setAttribute("MSA_Id", MSA_Id);
			request.setAttribute("Activitylibrary_Id", Activitylibrary_Id);
			return (mapping.findForward("customerlaborratepage"));
		}
		/* customer labor rates */

		if (Type.equals("M")) {
			request.setAttribute("Material_Id", resource_Id);
			return (mapping.findForward("materialresourcedetailpage"));
		}

		if (Type.equals("L")) {
			request.setAttribute("Labor_Id", resource_Id);
			return (mapping.findForward("laborresourcedetailpage"));
		}

		if (Type.equals("F")) {
			request.setAttribute("Freight_Id", resource_Id);
			return (mapping.findForward("freightresourcedetailpage"));
		}

		if (Type.equals("T")) {
			request.setAttribute("Travel_Id", resource_Id);
			return (mapping.findForward("travelresourcedetailpage"));
		}

		if (Type.equals("prj_appendix")) {
			request.setAttribute("type", Type);
			request.setAttribute("appendixid", Appendix_Id);
			return (mapping.findForward("dispatchactionpages"));
		}
		if (Type.equals("prj_job")) {
			request.setAttribute("type", Type);
			request.setAttribute("jobid", Job_Id);
			request.setAttribute("viewjobtype", "A");
			return (mapping.findForward("jobdashboard"));
		}

		if (Type.equals("jobdashboard_resM")) {
			request.setAttribute("ref", request.getParameter("ref"));
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("id1", request.getParameter("resource_Id"));
			request.setAttribute("id2", request.getParameter("jobid"));
			request.setAttribute("type", Type);

			return (mapping.findForward("activitydetailfromprmpage"));
		}

		if (Type.equals("jobdashboard_resL")) {
			request.setAttribute("ref", request.getParameter("ref"));
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("id1", request.getParameter("resource_Id"));
			request.setAttribute("id2", request.getParameter("jobid"));
			request.setAttribute("type", Type);
			return (mapping.findForward("activitydetailfromprmpage"));
		}

		if (Type.equals("jobdashboard_resF")) {
			request.setAttribute("ref", request.getParameter("ref"));
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("id1", request.getParameter("resource_Id"));
			request.setAttribute("id2", request.getParameter("jobid"));
			request.setAttribute("type", Type);
			return (mapping.findForward("activitydetailfromprmpage"));
		}

		if (Type.equals("jobdashboard_resT")) {
			request.setAttribute("ref", request.getParameter("ref"));
			request.setAttribute("from", request.getParameter("from"));
			request.setAttribute("id1", request.getParameter("resource_Id"));
			request.setAttribute("id2", request.getParameter("jobid"));
			request.setAttribute("type", Type);
			return (mapping.findForward("activitydetailfromprmpage"));
		}

		else
			return (mapping.findForward("errorpage"));
	}

	/**
	 * Create Default MPO "Troubleshoot and Repair" PO.
	 * 
	 * @param appendixId
	 * @param request
	 * @param ds
	 */
	private static void createDefaultMPO(String appendixId,
			HttpServletRequest request, DataSource ds) {
		MPODTO mpoBean = new MPODTO();
		mpoBean.setMpoName("Troubleshoot and Repair");
		mpoBean.setMpoItemType("ht");
		mpoBean.setMp_appendix_id(appendixId);
		MPODao mpDao = new MPODao();
		Map<String, Object> sessionMap;
		sessionMap = WebUtil.copySessionToAttributeMap(request.getSession());
		Map<String, Object> map = null;

		mpDao.MPOCreation(sessionMap, map, mpoBean);
		WebUtil.copyMapToRequest(request, map);

	}
}