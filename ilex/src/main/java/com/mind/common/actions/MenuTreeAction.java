package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.MenuController;
import com.mind.common.MenuElement;
import com.mind.common.dao.Logindao;
import com.mind.common.dao.MenuTree;
import com.mind.dao.PM.MSAdao;
import com.mind.util.MenuTreeCategory;

public class MenuTreeAction extends com.mind.common.IlexDispatchAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		MenuTree menutree = initMenuTreeBase();
		HttpSession session = request.getSession(true);
		// Change for 27-07-06
		// String userid = (String)session.getAttribute("userid");
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
		// expired

		String loginuserid = (String) session.getAttribute("userid");
		request.setAttribute(
				"isClicked",
				request.getParameter("isClicked") == null ? 0 : request
						.getParameter("isClicked"));

		request.setAttribute(
				"isRefreshed",
				request.getParameter("isRefreshed") == null ? "N" : request
						.getParameter("isRefreshed"));

		String categoryName = request.getParameter("menucriteria");

		MenuTreeCategory menuCategory = MenuTreeCategory.UNDEFINED;

		if (categoryName != null) {
			menuCategory = MenuTreeCategory.valueOf(categoryName);
		}

		switch (menuCategory) {
		case PM:
			initMenuTreePM(request, menutree, session);
			break;
		case RM:
			initMenuTreeRM(request, menutree, session);
			break;
		case UM:
			initMenuTreeUM(request, menutree);
			break;
		case CM:
			initMenuTreeCM(request, menutree, session);
			break;
		case AM:
			initMenuTreeAM(request, menutree, loginuserid);
			break;
		case PRM:
			initMenuTreePRM(request, menutree, session);
			break;
		case NM:
			initMenuTreeNM(request, menutree, session);
			break;
		case PVS:
			initMenuTreePVS(request, menutree, session);
			break;
		case PMT:
			initMenuTreePMT(request, menutree);
			break;
		case PRCM:
			initAccountingMenuLeft(mapping, form, request, response);
			break;
		default:
			break;

		}

		if (menuCategory.getViewName().equals(
				MenuTreeCategory.UNDEFINED.getViewName())) {
			return null;
		} else {
			return (mapping.findForward(menuCategory.getViewName()));
		}

	}

	private MenuTree initMenuTreeBase() {
		MenuTree menutree = new MenuTree();
		menutree.setImage_width(15);
		menutree.setImage_height(15);
		menutree.setImage_border(0);
		menutree.setTarget("ilexmain");
		return menutree;
	}

	private void initMenuTreePRCM(HttpServletRequest request, MenuTree menutree) {

		String[] image;
		String[] strarr_url;
		int[] strarr_level;
		int[] strarr_label;
		image = new String[3];

		image[0] = "images/t_msa.gif";
		image[1] = "images/t_appendix.gif";
		image[2] = "images/t_job.gif";

		menutree.setStrarr_img(image);

		menutree.setStr_headerImg("images/purchmngr_nor_small.gif");
		menutree.setStr_headerLabel("Accounting Manager");
		menutree.setStr_headerurl("InvoiceJobDetail.do?type=All&id=0&rdofilter=F");

		strarr_level = new int[3];
		strarr_level[0] = 1;
		strarr_level[1] = 4;
		strarr_level[2] = 7;

		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[3];
		strarr_label[0] = 2;
		strarr_label[1] = 5;
		strarr_label[2] = 8;
		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[3];
		strarr_url[0] = "InvoiceJobDetail.do";
		strarr_url[1] = "InvoiceJobDetail.do";
		strarr_url[2] = "";
		menutree.setStrarr_url(strarr_url);

		String strarr_urlparam[][] = {
				{ "1", "4", "7" },
				{ "function=view&type=MSA&rdofilter=F&id|1",
						"function=view&type=Appendix&rdofilter=F&id|4",
						"function=view&jobid|7~appendixid|4" } };

		menutree.setStrarr_urlparam(strarr_urlparam);
		menutree.setStr_query("select * from lp_invoice_hierarchy_01");

		menutree.generateMenu(getDataSource(request, "ilexnewDB"), "IM");

		request.setAttribute("element", menutree);

		request.setAttribute("controller", menutree.getMenuController());
	}

	private void initMenuTreePMT(HttpServletRequest request, MenuTree menutree) {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		String[] strarr_url;
		int[] strarr_level;
		int[] strarr_label;
		image = new String[2];
		image[0] = "images/t_msa.gif";
		image[1] = "images/t_appendix.gif";
		menutree.setStrarr_img(image);

		menutree.setStr_headerImg("images/t_msa.gif");
		menutree.setStr_headerLabel("Proposal Master");
		menutree.setStr_headerurl("");

		strarr_level = new int[2];
		strarr_level[0] = 1;
		strarr_level[1] = 3;

		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[2];
		strarr_label[0] = 2;
		strarr_label[1] = 5;

		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[2];
		strarr_url[0] = "";
		strarr_url[1] = "";

		menutree.setStrarr_url(strarr_url);

		String strarr_urlparam[][] = { { "1", "4" },
				{ "function=add&MSA_Id|1", "function=add&Appendix_Id|4" } };

		menutree.setStrarr_urlparam(strarr_urlparam);
		menutree.setStr_query("select * from lp_proposal_hierarchy_01");

		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				"PMT");

		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("menustring", menustring);
		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);
	}

	private void initMenuTreePVS(HttpServletRequest request, MenuTree menutree,
			HttpSession session) {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		String[] strarr_url;
		int[] strarr_level;
		int[] strarr_label;
		image = new String[1];

		String firsttime = "";
		String searchcriteria = "";

		if (request.getParameter("firsttime") != null)
			firsttime = request.getParameter("firsttime");

		if (request.getParameter("searchtext") != null)
			searchcriteria = request.getParameter("searchtext");

		if (searchcriteria.indexOf('\'') >= 0)
			searchcriteria = searchcriteria.replaceAll("\'", "\'\'");

		// image[0] = "images/t_appendix.gif";
		image[0] = "images/twored.gif";
		menutree.setStrarr_img(image);

		menutree.setStr_headerImg("images/red_folder.gif");
		menutree.setStr_headerLabel("Partners");
		menutree.setStr_headerurl("");

		strarr_level = new int[1];
		strarr_level[0] = 1;

		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[1];
		strarr_label[0] = 4;

		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[1];
		strarr_url[0] = "Partner_Edit.do";

		menutree.setStrarr_url(strarr_url);

		String strarr_urlparam[][] = { { "1" },
				{ "function=Update&formPVSSearch=fromPVSSearch&pid|1" } };

		menutree.setStrarr_urlparam(strarr_urlparam);

		menutree.setStr_query("select * from cp_partner_search_list_01 where lo_om_division like '%"
				+ searchcriteria + "%'");
		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				"PV");
		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("menustring", menustring);
		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);

		request.setAttribute("element", menutree);
		request.setAttribute("firsttime", "false");
		request.setAttribute("searchcriteria", searchcriteria);

		session.setAttribute("cookievalue", searchcriteria);
		request.setAttribute("controller", menutree.getMenuController());
	}

	private void initMenuTreeNM(HttpServletRequest request, MenuTree menutree,
			HttpSession session) {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		String[] strarr_url;
		int[] strarr_level;
		int[] strarr_label;
		String viewjobtype = null;
		String ownerId = null;

		if (session.getAttribute("ownerId") != null)
			ownerId = (String) session.getAttribute("ownerId");
		if (session.getAttribute("viewjobtype") != null)
			viewjobtype = (String) session.getAttribute("viewjobtype");

		image = new String[3];
		image[0] = "images/t_msa.gif";
		image[1] = "images/t_appendix.gif";
		image[2] = "images/t_job.gif";
		menutree.setStrarr_img(image);

		menutree.setStr_headerImg("images/projects_small.gif");
		menutree.setStr_headerLabel("NetMedX");
		menutree.setStr_headerurl("");

		strarr_level = new int[3];
		strarr_level[0] = 1;
		strarr_level[1] = 3;
		strarr_level[2] = 5;
		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[3];
		strarr_label[0] = 2;
		strarr_label[1] = 4;
		strarr_label[2] = 6;
		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[3];
		strarr_url[0] = "SiteMenu.do";
		strarr_url[1] = "AppendixHeader.do";
		strarr_url[2] = "JobDashboardAction.do";
		menutree.setStrarr_url(strarr_url);

		String strarr_urlparam[][] = {
				{ "1", "3", "5" },
				{
						"function=view&initialCheck=true&pageType=mastersites&viewjobtype="
								+ viewjobtype + "&ownerId=" + ownerId
								+ "&app=null&msaid|1",
						"function=view&fromPage=appendix&viewjobtype="
								+ viewjobtype + "&ownerId=" + ownerId
								+ "&msaId|1~appendixid|3",
						"function=view&jobid|5~appendixid|3" } };

		menutree.setStrarr_urlparam(strarr_urlparam);
		String whereClause = getTabCondition(request.getParameter("filter"));

		String orderClause = " order by lo_ot_name, isnull(lx_pr_title,''), job_order";

		menutree.setStr_query("select * from func_lp_netmedx_view_01() where 1=1 "
				+ whereClause + orderClause);
		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				"NM");

		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("menustring", menustring);
		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);
		request.setAttribute("element", menutree);
		request.setAttribute("controller", menutree.getMenuController());
	}

	private void initMenuTreePRM(HttpServletRequest request, MenuTree menutree,
			HttpSession session) {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		String[] strarr_url;
		int[] strarr_level;
		int[] strarr_label;
		String viewjobtype = null;
		String ownerId = null;

		if (session.getAttribute("ownerId") != null)
			ownerId = (String) session.getAttribute("ownerId");
		if (session.getAttribute("viewjobtype") != null)
			viewjobtype = (String) session.getAttribute("viewjobtype");

		image = new String[3];
		image[0] = "images/t_msa.gif";
		image[1] = "images/t_appendix.gif";
		image[2] = "images/t_job.gif";
		menutree.setStrarr_img(image);

		menutree.setStr_headerImg("images/projects_small.gif");
		menutree.setStr_headerLabel("Project");
		menutree.setStr_headerurl("");

		strarr_level = new int[3];
		strarr_level[0] = 1;
		strarr_level[1] = 3;
		strarr_level[2] = 5;
		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[3];
		strarr_label[0] = 2;
		strarr_label[1] = 4;
		strarr_label[2] = 6;
		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[3];
		strarr_url[0] = "SiteMenu.do";
		strarr_url[1] = "AppendixHeader.do";
		strarr_url[2] = "JobDashboardAction.do";
		menutree.setStrarr_url(strarr_url);

		String strarr_urlparam[][] = {
				{ "1", "3", "5" },
				{
						"function=view&initialCheck=true&pageType=mastersites&viewjobtype="
								+ viewjobtype + "&ownerId=" + ownerId
								+ "&app=null&msaid|1",
						"function=view&fromPage=appendix&viewjobtype="
								+ viewjobtype + "&ownerId=" + ownerId
								+ "&msaId|1~appendixid|3",
						"function=view&jobid|5~appendixid|3" } };

		menutree.setStrarr_urlparam(strarr_urlparam);
		String whereClause = getTabCondition(request.getParameter("filter"));
		menutree.setStr_query("select * from func_lp_project_view_01() where 1=1  "
				+ whereClause);

		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				"PR");
		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("menustring", menustring);
		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);
		request.setAttribute("element", menutree);
		request.setAttribute("controller", menutree.getMenuController());
	}

	private void initMenuTreeAM(HttpServletRequest request, MenuTree menutree,
			String loginuserid) {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		String[] strarr_url;
		int[] strarr_level;
		int[] strarr_label;
		image = new String[3];
		image[0] = "images/t_msa.gif";
		image[1] = "images/rm_mfg.gif";
		image[2] = "images/t_activity.gif";

		menutree.setStrarr_img(image);

		menutree.setStr_headerImg("images/activitymanager_small.gif");
		menutree.setStr_headerLabel("Activity Manager");
		menutree.setStr_headerurl("ManageCategoryAction.do?ref=View");

		strarr_level = new int[3];
		strarr_level[0] = 1;
		strarr_level[1] = 3;
		strarr_level[2] = 5;

		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[3];
		strarr_label[0] = 2;
		strarr_label[1] = 4;
		strarr_label[2] = 6;

		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[3];
		strarr_url[0] = "ActivitySummaryAction.do";
		strarr_url[1] = "ManageActivityAction.do";
		strarr_url[2] = "ActivityLibraryDetailAction.do";

		menutree.setStrarr_url(strarr_url);

		String strarr_urlparam[][] = {
				{ "1", "3", "5" },
				{ "ref=View&MSA_Id|1", "ref=View&MSA_Id|1~category_Id|3",
						"ref=View&type=AM&MSA_Id|1~Activitylibrary_Id|5" } };

		menutree.setStrarr_urlparam(strarr_urlparam);
		String whereCondition = " where ";
		String andStatement = " and ";
		String whereClause = " lo_ot_name in ('Master Library','NetMedX')";
		if (request.getParameter("filter") != null) {
			if (request.getParameter("filter").equals("Master")) {
				whereClause = " lo_ot_name in ('Master Library','NetMedX')";
			} else {
				String[] filteredCondition;
				if (!request.getParameter("filter").equals("T-Z")) {
					filteredCondition = request.getParameter("filter")
							.toString().split("-");
					whereClause = " upper(substring(lo_ot_name ,1,1)) between '"
							+ filteredCondition[0]
							+ "' and '"
							+ filteredCondition[1]
							+ "'"
							+ " and lo_ot_name not in ('Master Library','NetMedX')";
				} else {
					filteredCondition = request.getParameter("filter")
							.toString().split("-");
					whereClause = " upper(substring(lo_ot_name ,1,1)) NOT between 'A' and 'S'";
				}
			}
		}

		String checkRole = Logindao.checkRoleForUser(loginuserid, "ESA",
				getDataSource(request, "ilexnewDB"));
		if (checkRole.equals("Y")) {
			menutree.setStr_query("select * from lp_activity_hierarchy_01 where lp_md_cns_sales_pc_id = '"
					+ loginuserid
					+ "' or lp_md_cns_sales_pc_id  is null "
					+ andStatement + whereClause);
		} else {
			menutree.setStr_query("select * from lp_activity_hierarchy_01 "
					+ whereCondition + whereClause);
		}

		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				"AM");
		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("menustring", menustring);
		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);

		request.setAttribute("element", menutree);

		request.setAttribute("controller", menutree.getMenuController());
	}

	private void initMenuTreeCM(HttpServletRequest request, MenuTree menutree,
			HttpSession session) {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		int[][] samelevel_image;
		String[] strarr_url;
		int[][] samelevel_url;
		int[] strarr_level;
		int[] strarr_label;
		String searchcheck1 = "";
		String searchcheck2 = "";
		String searchtext = "";

		if (request.getParameter("searchcheck1") != null)
			searchcheck1 = request.getParameter("searchcheck1");

		if (request.getParameter("searchcheck2") != null)
			searchcheck2 = request.getParameter("searchcheck2");

		if (request.getParameter("searchtext") != null)
			searchtext = request.getParameter("searchtext");
		// BIGEN: By Vijay on 28-03-07
		if (searchtext.indexOf('\'') >= 0)
			searchtext = searchtext.replaceAll("\'", "\'\'");
		// END: By Vijay on 28-03-07
		image = new String[4];
		image[0] = "images/t_msa.gif";
		image[1] = "images/t_appendix.gif";
		image[2] = "images/t_job.gif";
		image[3] = "images/t_job.gif";
		menutree.setStrarr_img(image);

		menutree.setStr_headerImg("images/allthree.gif");
		menutree.setStr_headerLabel("Organization");
		menutree.setStr_headerurl("");

		samelevel_image = new int[2][4];
		samelevel_image[0][0] = 0;
		samelevel_image[1][0] = 10;
		samelevel_image[0][1] = 1;
		samelevel_image[1][1] = 11;
		samelevel_image[0][2] = 2;
		samelevel_image[1][2] = 12;
		samelevel_image[0][3] = 3;
		samelevel_image[1][3] = 13;

		menutree.setStrarr_diffImgSameLevelDb(samelevel_image);

		strarr_level = new int[4];
		strarr_level[0] = 1;
		strarr_level[1] = 3;
		strarr_level[2] = 5;
		strarr_level[3] = 7;

		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[4];
		strarr_label[0] = 2;
		strarr_label[1] = 4;
		strarr_label[2] = 6;
		strarr_label[3] = 8;

		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[4];
		strarr_url[0] = "AddTopLevel.do";
		strarr_url[1] = "AddDivison.do";
		// strarr_url[2] = "AddPoc.do";
		strarr_url[2] = "";
		strarr_url[3] = "AddPoc.do";

		menutree.setStrarr_url(strarr_url);
		// menutree.setStrarr_url(strarr_url);

		samelevel_url = new int[2][1];
		samelevel_url[0][0] = 2;
		samelevel_url[1][0] = 14;

		menutree.setStrarr_diffUrlSameLevelDb(samelevel_url);

		String strarr_urlparam[][] = {
				{ "1", "3", "5", "7" },
				{
						"function=add&org_type|1",
						// "function=add&org_discipline_id|3~org_type|1",
						// //20070901
						"function=add&org_discipline_id|3~org_division_id|5~org_type|1",
						"function=add&org_discipline_id|3~division_id|5~org_type|1",
						"function=update&org_discipline_id|3~division_id|5~lo_pc_id|7~org_type|1" } };

		menutree.setStrarr_urlparam(strarr_urlparam);

		// menutree.setStr_query("select  * from lo_organization_hierarchy_01 where lo_organization_type IN ( 'N'"+searchcheck+") and lo_ot_name like '"+searchtext+"%'");

		/*
		 * menutree.setStr_query(
		 * "select * from lo_organization_hierarchy_01 where lo_organization_type in ('"
		 * +searchcheck1+"') and " +"lo_ot_name like '"+searchtext+
		 * "%' union select * from lo_organization_hierarchy_01 where lo_organization_type in "
		 * +
		 * "('N') union select * from lo_organization_hierarchy_01 where lo_organization_type in ('"
		 * +searchcheck2+"') and lo_om_division " + "like '"+searchtext+"%'" );
		 */

		String module = "";

		if (searchcheck1 == "" && searchcheck2 == "" && searchtext == "") {
			menutree.setStr_query("select * from dbo.func_first_contact_hierarchy()");
			module = "CM1";
		} else {
			menutree.setStr_query("select * from dbo.func_contact_hierarchy('"
					+ searchcheck1 + "' , '" + searchcheck2 + "' , '"
					+ searchtext + "')");
			module = "CM";
		}

		// menustring = menutree.generateMenu(getDataSource(request,
		// "ilexnewDB"), "CM");
		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				module);

		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("element", menutree);

		session.setAttribute("searchcheck1", searchcheck1);
		session.setAttribute("searchcheck2", searchcheck2);
		session.setAttribute("searchtext", searchtext);

		request.setAttribute("controller", menutree.getMenuController());
		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);
	}

	private void initMenuTreeUM(HttpServletRequest request, MenuTree menutree) {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		String[] strarr_url;
		int[] strarr_level;
		int[] strarr_label;
		image = new String[3];
		image[0] = "images/t_msa.gif";
		image[1] = "images/t_appendix.gif";
		image[2] = "images/t_job.gif";
		menutree.setStrarr_img(image);

		menutree.setStr_headerImg("images/t_msa.gif");
		menutree.setStr_headerLabel("Organization");
		menutree.setStr_headerurl("");

		strarr_level = new int[3];
		strarr_level[0] = 1;
		strarr_level[1] = 3;
		strarr_level[2] = 5;

		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[3];
		strarr_label[0] = 2;
		strarr_label[1] = 4;
		strarr_label[2] = 6;

		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[3];
		strarr_url[0] = "";
		strarr_url[1] = "RolePrivilegeAction.do";
		strarr_url[2] = "";

		menutree.setStrarr_url(strarr_url);

		String strarr_urlparam[][] = {
				{ "1", "3", "5" },
				{ "org_type|1", "org_discipline_id|3~org_type|1",
						"org_discipline_id|3~role_id|5" } };

		menutree.setStrarr_urlparam(strarr_urlparam);
		menutree.setStr_query("select * from lp_user_hierarchy_01");

		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				"UM");
		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("menustring", menustring);
		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);

		request.setAttribute("element", menutree);

		request.setAttribute("controller", menutree.getMenuController());
	}

	private void initMenuTreeRM(HttpServletRequest request, MenuTree menutree,
			HttpSession session) {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		int[][] samelevel_image;
		String[] strarr_url;
		int[] strarr_level;
		int[] strarr_label;
		image = new String[4];

		/* by hamid for tree search */
		String firsttime = "";
		String searchcriteria = "";

		if (request.getParameter("firsttime") != null)
			firsttime = request.getParameter("firsttime");

		if (request.getParameter("searchtext") != null)
			searchcriteria = request.getParameter("searchtext");
		if (searchcriteria.indexOf('\'') >= 0)
			searchcriteria = searchcriteria.replaceAll("\'", "\'\'");

		/* by hamid for tree search */

		image[0] = "";
		image[1] = "images/rm_mfg.gif";
		image[2] = "images/rm_sub.gif";
		image[3] = "images/rm_res_view.gif";
		menutree.setStrarr_img(image);
		image = new String[4];

		samelevel_image = new int[2][1];
		samelevel_image[0][0] = 0;
		samelevel_image[1][0] = 13;

		menutree.setStrarr_diffImgSameLevelDb(samelevel_image);

		menutree.setStr_headerImg("images/resource_manager_small.gif");
		menutree.setStr_headerLabel("Resource Manager");
		menutree.setStr_headerurl("");

		strarr_level = new int[4];
		strarr_level[0] = 1;
		strarr_level[1] = 3;
		strarr_level[2] = 5;
		strarr_level[3] = 7;

		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[4];
		strarr_label[0] = 2;
		strarr_label[1] = 4;
		strarr_label[2] = 6;
		strarr_label[3] = 10;

		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[4];
		strarr_url[0] = "ManAdd.do";
		strarr_url[1] = "PPSAdd.do";
		strarr_url[2] = "ResourceAdd.do";
		strarr_url[3] = "ResourceAdd.do";

		menutree.setStrarr_url(strarr_url);

		String strarr_urlparam[][] = {
				{ "1", "3", "5", "7" },
				{ "function=add&id|1~f|8", "function=add&id|1~id1|3~f|8",
						"function=add&id|1~id1|3~id2|5~f|8",
						"function=view&id|1~id1|3~id2|5~id3|7~f|8" } };

		menutree.setStrarr_urlparam(strarr_urlparam);
		menutree.setStr_query("select * from lx_resource_hierarchy_01 where iv_ct_name like '%"
				+ searchcriteria
				+ "%' or iv_mf_name like '%"
				+ searchcriteria
				+ "%' or iv_sc_name like '%"
				+ searchcriteria
				+ "%' or iv_rp_name like '%" + searchcriteria + "%'");
		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				"RM");
		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("menustring", menustring);
		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);
		request.setAttribute("element", menutree);
		request.setAttribute("controller", menutree.getMenuController());

		/* by hamid for tree search */
		request.setAttribute("firsttime", "false");
		request.setAttribute("searchcriteria", searchcriteria);
		session.setAttribute("cookievalue", searchcriteria);
		/* by hamid for tree search */
	}

	private void initMenuTreePM(HttpServletRequest request, MenuTree menutree,
			HttpSession session) throws Exception {
		String menustring;
		String currState;
		String expansionState;
		String[] image;
		int[][] samelevel_image;
		String[] strarr_url;
		int[][] samelevel_url;
		int[] strarr_level;
		int[] strarr_label;
		image = new String[4];
		image[0] = "images/t_msa.gif";
		image[1] = "";
		image[2] = "images/t_job.gif";
		image[3] = "images/t_activity.gif";
		menutree.setStrarr_img(image);

		samelevel_image = new int[2][1];
		samelevel_image[0][0] = 1;
		samelevel_image[1][0] = 15;

		menutree.setStrarr_diffImgSameLevelDb(samelevel_image);

		menutree.setStr_headerImg("images/proposal_manager_small.gif");
		menutree.setStr_headerLabel("MSA");
		// menutree.setStr_headerurl("MSAUpdateAction.do");
		menutree.setStr_headerurl("MSAUpdateAction.do?firstCall=true");

		strarr_level = new int[4];
		strarr_level[0] = 1;
		strarr_level[1] = 4;
		strarr_level[2] = 7;
		strarr_level[3] = 10;

		menutree.setStrarr_level(strarr_level);

		strarr_label = new int[4];
		strarr_label[0] = 2;
		strarr_label[1] = 5;
		strarr_label[2] = 8;
		strarr_label[3] = 11;

		menutree.setStrarr_label(strarr_label);

		strarr_url = new String[4];
		strarr_url[0] = "AppendixDetailAction.do";
		// strarr_url[1] = "JobUpdateAction.do";
		strarr_url[1] = "";
		strarr_url[2] = "JobDetailAction.do";
		// strarr_url[3] = "MaterialUpdateAction.do";
		strarr_url[3] = "ResourceListAction.do";

		/* Start: Added By Atul 08/01/2007 */
		menutree.setStrarr_url(strarr_url);

		samelevel_url = new int[2][1];
		samelevel_url[0][0] = 1;
		samelevel_url[1][0] = 16;

		String msaStatus = "";
		String msaOwner = "";
		String appendixStatus = "";
		String appendixOwner = "";
		String monthMSA = "";
		String weekMSA = "";
		String monthAppendix = "";
		String weekAppendix = "";
		String type = "";
		boolean checkAppenidx = false;
		menutree.setStrarr_diffUrlSameLevelDb(samelevel_url);

		// if(session.getAttribute("MSASelectedOwners") != null &&
		// !session.getAttribute("MSASelectedOwners").equals("") &&
		// !session.getAttribute("MSASelectedOwners").toString().contains("%"))
		// {
		// msaOwner +=
		// "and lp_md_cns_sales_pc_id in "+session.getAttribute("MSASelectedOwners");
		// }

		if (session.getAttribute("MSASelectedOwners") != null
				&& !session.getAttribute("MSASelectedOwners").equals("")) {
			if (!session.getAttribute("MSASelectedOwners").toString()
					.contains("%")) {
				msaOwner += "and lp_md_cns_sales_pc_id in "
						+ session.getAttribute("MSASelectedOwners");
			} else {
				msaOwner = "and lp_md_cns_sales_pc_id  = lp_md_cns_sales_pc_id";
			}

		}

		if (session.getAttribute("MSASelectedStatus") != null
				&& !session.getAttribute("MSASelectedStatus").equals("")) {
			msaStatus = msaStatus + " and lp_md_status in "
					+ session.getAttribute("MSASelectedStatus");
		}

		if (session.getAttribute("appSelectedOwners") != null
				&& !session.getAttribute("appSelectedOwners").equals("")) {
			checkAppenidx = true;
			if (!session.getAttribute("appSelectedOwners").toString()
					.contains("%")) {
				appendixOwner = appendixOwner + " and lx_pr_cns_poc in "
						+ session.getAttribute("appSelectedOwners");
			} else {
				appendixOwner = "and lx_pr_cns_poc = lx_pr_cns_poc";
			}
		}

		if (session.getAttribute("appSelectedStatus") != null
				&& !session.getAttribute("appSelectedStatus").equals("")) {
			checkAppenidx = true;
			appendixStatus = appendixStatus + " and lx_pr_status in "
					+ session.getAttribute("appSelectedStatus");
		}

		if (session.getAttribute("monthMSA") != null
				&& session.getAttribute("monthMSA") != "") {
			monthMSA = monthMSA
					+ "	and	( "
					+ " ( datepart(mm, lp_md_need_date) =  datepart(mm, getdate())and datepart(yy, lp_md_need_date) =  datepart(yy, getdate()) )  "
					+ " or "
					+ " ( datepart(mm, lp_md_effective_date) =  datepart(mm, getdate())and datepart(yy, lp_md_effective_date) =  datepart(yy, getdate()) ) "
					+ " ) ";

		}
		if (session.getAttribute("weekMSA") != null
				&& session.getAttribute("weekMSA") != "") {
			weekMSA = weekMSA
					+ "	and	( "
					+ " ( datepart(wk, lp_md_need_date) =  datepart(wk, getdate())and datepart(yy, lp_md_need_date) =  datepart(yy, getdate()) )  "
					+ " or "
					+ " ( datepart(wk, lp_md_effective_date) =  datepart(wk, getdate())and datepart(yy, lp_md_effective_date) =  datepart(yy, getdate()) ) "
					+ " ) ";

		}

		if (session.getAttribute("monthAppendix") != null
				&& session.getAttribute("monthAppendix") != "") {
			checkAppenidx = true;
			monthAppendix = monthAppendix
					+ "	and	( "
					+ " ( datepart(mm, lx_pd_need_date) =  datepart(mm, getdate())and datepart(yy, lx_pd_need_date) =  datepart(yy, getdate()) )  "
					+ " or "
					+ " ( datepart(mm, lx_pd_effective_date) =  datepart(mm, getdate())and datepart(yy, lx_pd_effective_date) =  datepart(yy, getdate()) ) "
					+ " ) ";
		}

		if (session.getAttribute("weekAppendix") != null
				&& session.getAttribute("weekAppendix") != "") {
			checkAppenidx = true;
			weekAppendix = weekAppendix
					+ "	and	( "
					+ " ( datepart(wk, lx_pd_need_date) =  datepart(wk, getdate())and datepart(yy, lx_pd_need_date) =  datepart(yy, getdate()) )  "
					+ " or "
					+ " ( datepart(wk, lx_pd_effective_date) =  datepart(wk, getdate())and datepart(yy, lx_pd_effective_date) =  datepart(yy, getdate()) ) "
					+ " ) ";
		}

		String strarr_urlparam[][] = {
				{ "1", "4", "7", "10" },
				{
						"firstCall=true&MSA_Id|1~Name|2",
						"ref=View&Appendix_Id|4~MSA_Id|5",
						"ref=detailjob&Job_Id|7~Name|8",
						"ref=View&type=PM&resourceListType=A&Activitylibrary_Id|10~Activity_Id|10~Name|11" } };

		if (checkAppenidx)
			type = "Appendix";
		else
			type = "MSA";
		String whereClause = getTabCondition(request.getParameter("filter"));

		String sql = MSAdao.getProposalTreeSql(msaOwner, msaStatus,
				appendixOwner, appendixStatus, monthMSA, weekMSA,
				monthAppendix, weekAppendix, type, whereClause,
				getDataSource(request, "ilexnewDB"));

		menutree.setStrarr_urlparam(strarr_urlparam);
		menutree.setStr_query(sql);
		menustring = menutree.generateMenu(getDataSource(request, "ilexnewDB"),
				"PM");
		currState = menutree.getCurrState();
		expansionState = menutree.getExpansionState();
		request.setAttribute("menustring", menustring);

		request.setAttribute("element", menutree);

		request.setAttribute("controller", menutree.getMenuController());

		request.setAttribute("currState", currState);
		request.setAttribute("expansionState", expansionState);
	}

	private String getTabCondition(String filter) {
		String whereClause = "";
		if (filter != null) {
			String[] filteredCondition;
			if (!filter.equals("T-Z")) {
				filteredCondition = filter.toString().split("-");
				whereClause = " and upper(substring(lo_ot_name ,1,1)) between '"
						+ filteredCondition[0]
						+ "' and '"
						+ filteredCondition[1] + "'";
			} else {
				filteredCondition = filter.toString().split("-");
				whereClause = " and upper(substring(lo_ot_name ,1,1)) NOT between 'A' and 'S'";
			}
		} else {
			whereClause = " and upper(substring(lo_ot_name ,1,1)) between 'A' and 'D'";
		}
		return whereClause;
	}

	public void initAccountingMenuLeft(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MenuTree menutree = initMenuTreeBase();
		initMenuTreePRCM(request, menutree);
		MenuElement me = (menutree).getRootElement();
		MenuController mc = menutree.getMenuController();

		me.drawDirect(0, mc, response.getWriter());

	}

}
