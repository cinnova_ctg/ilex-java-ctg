package com.mind.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.bean.Schedule;
import com.mind.common.dao.ScheduleViewUpdate;
import com.mind.common.formbean.MenuFunctionScheduleForm;

public class MenuFunctionScheduleAction extends com.mind.common.IlexDispatchAction
{
	public ActionForward view( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		MenuFunctionScheduleForm menufunctionscheduleform = ( MenuFunctionScheduleForm ) form;
		Schedule schedule = new Schedule();
		
		if( request.getParameter( "Appendix_Id" ) != null )
		{
			menufunctionscheduleform.setAppendix_Id( request.getParameter( "Appendix_Id" ) );
		}
		
		if( request.getParameter( "Job_Id" ) != null )
		{
			menufunctionscheduleform.setJob_Id( request.getParameter( "Job_Id" ) );
		}
		
		if( request.getParameter( "Type" ) != null )
		{
			menufunctionscheduleform.setType( request.getParameter( "Type" ) );
		}
		
		
		schedule = ScheduleViewUpdate.viewschedule ( menufunctionscheduleform.getAppendix_Id() , menufunctionscheduleform.getJob_Id() , menufunctionscheduleform.getType() , getDataSource( request,"ilexnewDB" ) );
		menufunctionscheduleform.setPlanned_schedule_required( schedule.getScheduled_need_date() );
		menufunctionscheduleform.setPlanned_schedule_effective( schedule.getScheduled_effective_date() );
		menufunctionscheduleform.setPlanned_schedule_days( schedule.getScheduled_planned_days() );
		
		return( mapping.findForward( "schedulediaplaypage" ) );
	}
	
	public ActionForward update( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response ) throws Exception 
	{
		Schedule schedule = new Schedule();
		//schedule = ScheduleViewUpdate.viewschedule ( request.getParameter( "Id" ) , request.getParameter( "Type" ) , getDataSource( request,"ilexnewDB" ) );
		
		request.setAttribute( "schedule" , schedule );

		return( mapping.findForward( "schedulediaplaypage" ) );
	}
	
	
}
