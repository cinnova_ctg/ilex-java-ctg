package com.mind.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.bean.Comment;

public class ViewAllComment 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ViewAllComment.class);

	public static ArrayList Getallcomment( String Id , String Type , DataSource ds )
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String lp_md_id = null;
		String lx_pd_id = null;
		
		ArrayList all_comment = new ArrayList();
		Comment comment = null;
		
		if( Type.equals( "Jobdashboard" ) )
			Type = "Job";
		
		if( Type.equals( "prm_appendix" ) )
			Type = "Appendix";
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "";
			
			sql = "select com_desc , com_by , comment_date from dbo.func_view_comments('"+Id+"' ,'"+Type+"')";
			
			rs = stmt.executeQuery( sql );
			
			while ( rs.next() )
			{
				comment = new Comment();
				comment.setCommentby( rs.getString( "com_by" ) );
				comment.setComment( Util.replaceToBr(rs.getString( "com_desc" )) );
				comment.setCommentdate( rs.getString( "comment_date" ));
				all_comment.add( comment );
			}
			
		}
		
		catch( Exception e )
		{
			logger.error("Getallcomment(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Getallcomment(String, String, DataSource) - Exception in retrieving Comment" + e);
			}
		}
		
		finally
		{
			try
			{
				
				if( stmt != null)
				{
					stmt.close();
				}	
			}
			catch( SQLException sql )
			{
				logger.error("Getallcomment(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Getallcomment(String, String, DataSource) - Exception Caught" + sql);
				}
			}
			
			
			try
			{
				if(conn!=null)
				{	
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.error("Getallcomment(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Getallcomment(String, String, DataSource) - Exception Caught" + sql);
				}
			}
			
		}
			
		
		return ( all_comment );
	}

}
