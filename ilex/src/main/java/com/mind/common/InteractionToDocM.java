//
//
//
//@ Project : Ilex
//@ File Name : InteractionToDocM.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.docm.Document;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.DocumentIdNotUpdatedException;
import com.mind.docm.exceptions.EntityDetailsNotFoundException;
import com.mind.docm.exceptions.NotAbleToInsertDocument;
import com.mind.docm.utils.DocumentUtility;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.xml.ViewAction;

public class InteractionToDocM {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(InteractionToDocM.class);

	public static final String MSAStatus = "A"; // ( i.e Approved/Pending
	// Customer
	// Review/Signed/Cancelled )
	public static final String AppendixStatus = "A"; // ( i.e Approved/Pending
	// Customer
	// Review/Signed/Inwork/Cancelled
	// )
	public static final String prjJob = "A"; // ( i.e Approved/Pending Customer

	// Review/Signed/Inwork/Cancelled
	// )

	// Use Case
	/**
	 * @name checkPDFIntesert
	 * @purpose This method is used to check whether there is interaction with
	 *          DocM System for PDF Insertion
	 * @steps 1. User request to change the status of the Entity
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            type
	 * @param String
	 *            status
	 * @param String
	 *            jobId
	 * @param DataSource
	 *            ds
	 * @return boolean
	 * @returnDescription Return true if there is interaction otherwise false
	 */

	public static boolean checkPDFIntesert(String entityId, String type,
			String status, String jobId, DataSource ds) {
		boolean result = false;
		result = verifyInsert(type, status, jobId, ds);
		return result;
	}

	// Use Case
	/**
	 * @name checkPDFDraft
	 * @purpose This method is used to check whether there is interaction with
	 *          DocM System when status is changed to draft
	 * @steps 1. User request to change the status of the Entity
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            type
	 * @param String
	 *            status
	 * @param String
	 *            jobId
	 * @param DataSource
	 *            ds
	 * @return boolean
	 * @returnDescription Return true if there is interaction otherwise false
	 */
	public static boolean checkPDFDraft(String entityId, String type,
			String status, String jobId, DataSource ds) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(getSqlForDraftCancel(type));
			setPreparedParameterForDraftCancel(entityId, type, jobId, pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = verifyDraft(entityId, jobId, type, rs.getString(1),
						status, ds);
			}
		} catch (Exception e) {
			logger.error(
					"checkPDFDraft(String, String, String, String, DataSource)",
					e);

			logger.error(
					"checkPDFDraft(String, String, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return result;
	}

	// Use Case
	/**
	 * @name checkPDFCancel
	 * @purpose This method is used to check whether there is interaction with
	 *          DocM System when status is changed to cancel
	 * @steps 1. User request to change the status of the Entity
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            type
	 * @param String
	 *            status
	 * @param String
	 *            jobId
	 * @param DataSource
	 *            ds
	 * @return boolean
	 * @returnDescription Return true if there is interaction otherwise false
	 */
	public static boolean checkPDFCancel(String entityId, String type,
			String status, String jobId, DataSource ds) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(getSqlForDraftCancel(type));
			setPreparedParameterForDraftCancel(entityId, type, jobId, pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				// System.out.println("XXXXXX" + entityId);
				// System.out.println("YYYYYY" + jobId);
				// System.out.println("ZZZZZZ" + type);
				// System.out.println("AAAAAA" + rs.getString(1));
				result = verifyCancel(entityId, jobId, type, rs.getString(1),
						status, ds);
			}
		} catch (Exception e) {
			logger.error(
					"checkPDFCancel(String, String, String, String, DataSource)",
					e);

			logger.error(
					"checkPDFCancel(String, String, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return result;
	}

	// Use Case
	/**
	 * @name setPreparedParameterForDraftCancel
	 * @purpose This method is used to set the prepareStatement parameters used
	 *          to verify the Interaction
	 * @steps 1. User request to change the status of the Entity
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            type
	 * @param PreparedStatement
	 *            pstmt
	 * @return
	 * @returnDescription
	 */
	public static void setPreparedParameterForDraftCancel(String entityId,
			String type, String jobId, PreparedStatement pstmt) {
		try {
			if (type.trim().equalsIgnoreCase("prj_job"))
				pstmt.setString(1, jobId);
			else
				pstmt.setString(1, entityId);
		} catch (Exception e) {
			logger.warn(
					"setPreparedParameterForDraftCancel(String, String, String, PreparedStatement) - exception ignored",
					e);
		}
	}

	// Use Case
	/**
	 * @name getSqlForDraftCancel
	 * @purpose This method is used to get the sql query for different entities
	 *          when there is any interaction.
	 * @steps 1. User request to change the status of the Entity
	 * 
	 * @param String
	 *            type
	 * @return String
	 * @returnDescription Sql query to be used.
	 */
	public static String getSqlForDraftCancel(String type) {
		String sql = null;
		if (type.trim().equalsIgnoreCase("MSA"))
			sql = "select lp_md_status from lp_msa_detail where lp_md_mm_id = ?";
		else if (type.trim().equalsIgnoreCase("Appendix"))
			sql = "select lx_pr_status from lx_appendix_main where lx_pr_id = ?";
		else if (type.trim().equalsIgnoreCase("prj_job"))
			sql = "select lm_js_status from lm_job where lm_js_id = ?";
		return sql;
	}

	// Use Case
	/**
	 * @name verifyInsert
	 * @purpose This method is used to see whether there is interaction for PDf
	 *          Insertion in DocM.
	 * @steps 1. User request to change the status of the Entity
	 * 
	 * @param String
	 *            type
	 * @param String
	 *            status
	 * @param String
	 *            jobId
	 * @param DataSource
	 *            ds
	 * @return boolean
	 * @returnDescription return true for interaction otherwise false.
	 */
	public static boolean verifyInsert(String type, String status,
			String jobId, DataSource ds) {
		boolean result = false;
		if (type.trim().equalsIgnoreCase("MSA")) {
			if (MSAStatus.indexOf(status) != -1)
				result = true;
		} else if (type.trim().equalsIgnoreCase("Appendix")) { // For Appendix
			// and NetMedX
			// both
			if (AppendixStatus.indexOf(status) != -1)
				result = true;
		} else if (type.trim().equalsIgnoreCase("prj_job")) { // Here prj_job
			// can be both
			// the normal
			// job and
			// addendums
			String[] jobStatusAndType = null;
			jobStatusAndType = Jobdao.getJobStatusAndType("%", jobId, "%", "%",
					ds);
			String jobStatus = jobStatusAndType[0];
			String jobType = jobStatusAndType[1];
			if (jobType.equals("Addendum")) {
				if (prjJob.indexOf(status) != -1)
					result = true;
			}
		}
		return result;
	}

	// Use Case
	/**
	 * @name verifyDraft
	 * @purpose This method is used to see whether there is interaction when the
	 *          status gets Changed.
	 * @steps 1. User request to change the status of the Entity
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            jobId
	 * @param String
	 *            type
	 * @param String
	 *            currStatus
	 * @param String
	 *            newStatus
	 * @param DataSource
	 *            ds
	 * @return boolean
	 * @returnDescription return true for interaction otherwise false.
	 */
	public static boolean verifyDraft(String entityId, String jobId,
			String type, String currStatus, String newStatus, DataSource ds) {
		boolean result = false;
		if (type.trim().equalsIgnoreCase("prj_job")) {
			String[] jobStatusAndType = null;
			jobStatusAndType = Jobdao.getJobStatusAndType("%", jobId, "%", "%",
					ds);
			String jobStatus = jobStatusAndType[0];
			String jobType = jobStatusAndType[1];
			if (jobType.equals("Addendum")) {
				if ((currStatus.trim().equalsIgnoreCase("A") || currStatus
						.trim().equalsIgnoreCase("P"))
						&& newStatus.trim().equalsIgnoreCase("D"))
					result = true;
			}
		} else if ((currStatus.trim().equalsIgnoreCase("A") || currStatus
				.trim().equalsIgnoreCase("P"))
				&& newStatus.trim().equalsIgnoreCase("D"))
			result = true;

		return result;
	}

	// Use Case
	/**
	 * @name verifyCancel
	 * @purpose This method is used to see whether there is interaction when the
	 *          status gets Changed.
	 * @steps 1. User request to change the status of the Entity
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            jobId
	 * @param String
	 *            type
	 * @param String
	 *            currStatus
	 * @param String
	 *            newStatus
	 * @param DataSource
	 *            ds
	 * @return boolean
	 * @returnDescription return true for interaction otherwise false.
	 */
	public static boolean verifyCancel(String entityId, String jobId,
			String type, String currStatus, String newStatus, DataSource ds) {
		boolean result = false;
		if (type.trim().equalsIgnoreCase("prj_job")) {
			String[] jobStatusAndType = null;
			jobStatusAndType = Jobdao.getJobStatusAndType("%", jobId, "%", "%",
					ds);
			String jobStatus = jobStatusAndType[0];
			String jobType = jobStatusAndType[1];
			if (jobType.equals("Addendum")) {
				if ((currStatus.trim().equalsIgnoreCase("A") || currStatus
						.trim().equalsIgnoreCase("P"))
						&& newStatus.trim().equalsIgnoreCase("C"))
					result = true;
			}
		} else if ((currStatus.trim().equalsIgnoreCase("A") || currStatus
				.trim().equalsIgnoreCase("P"))
				&& newStatus.trim().equalsIgnoreCase("C"))
			result = true;
		return result;
	}

	// Use Case
	/**
	 * @name callToDocMSystemForPdfInsertion
	 * @purpose This method is used get insert the docment byte[] into DocM
	 *          System.
	 * @steps 1. User requests to set the document correspponding to any entity.
	 *        2. Special case is there for "prj_job" as document always gets
	 *        generted for Appendix Id not for Jobid so we have pass the
	 *        AppendixId to getDocumentbytes() method of ViewAction and jobid as
	 *        request objects because we cannot pass it as a paramter (which is
	 *        the normal flow of the Ilex System)
	 * 
	 * @param HttpServletRequest
	 * @param String
	 *            Type
	 * @param String
	 *            Id
	 * @param String
	 *            Status
	 * @param String
	 *            userId
	 * @param DataSource
	 *            ds
	 * @return String
	 * @returnDescription Return the documentId which we get from the DocM
	 *                    System.
	 */
	public static String callToDocMSystemForPdfInsertion(
			HttpServletRequest request, String Type, String Id, String status,
			String userId, DataSource ds) {
		byte[] buffer = null;
		String docId = null;
		if (Type.equals("MSA")) {
			request.setAttribute("View", "pdf");
			request.setAttribute("from_type", "PM");
			buffer = ViewAction.getDocumentbytes(request, Id, Type, userId, ds);
		} else if (Type.equals("Appendix")) {
			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(Id, ds);
			request.setAttribute("jobId", addendumid);
			request.setAttribute("View", "pdf");
			request.setAttribute("from_type", "PM");
			buffer = ViewAction.getDocumentbytes(request, Id, Type, userId, ds);
		} else if (Type.equals("prj_job")) {
			request.setAttribute(
					"jobId",
					request.getParameter("jobid") != null ? request
							.getParameter("jobid") : ""); // get the job Id and
			// set it the
			// request
			// attribute.
			request.setAttribute("View", "pdf");
			request.setAttribute("from_type", "PRM");
			Type = "Appendix";
			buffer = ViewAction.getDocumentbytes(request, Id, Type, userId, ds);
			Id = request.getParameter("jobid") != null ? request
					.getParameter("jobid") : "";
			Type = "Addendum";
		}

		try {
			docId = EntityManager.approveEntity(Id, Type, buffer);
		} catch (NotAbleToInsertDocument e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("documentInsertionInDocM", "failed");
		} catch (EntityDetailsNotFoundException e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("EntityDetailsNotFoundException", "failed");
		} catch (ControlledTypeException e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("ControlledTypeException", "failed");
		} catch (DocMFormatException e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("DocMException", "failed");
		} catch (CouldNotAddToRepositoryException e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("CouldNotAddToRepositoryException", "failed");
		} catch (CouldNotReplaceException e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("CouldNotReplaceException", "failed");
		} catch (CouldNotCheckDocumentException e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("CouldNotCheckDocumentException", "failed");
		} catch (DocumentIdNotUpdatedException e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("DocumentIdNotUpdatedException", "failed");
		} catch (Exception e) {
			logger.error(
					"callToDocMSystemForPdfInsertion(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(e.getMessage());
			request.setAttribute("Exception", "failed");
		}
		return docId;
	}

	// Use Case
	/**
	 * @name getIdBasedOnType
	 * @purpose This method is used get the Id for which we have to generate the
	 *          document.
	 * @steps 1. User requests to get the Document when Status of Entity Changes
	 *        to Approved. 2.Special case is there for "prj_job" as document
	 *        always gets generted for Appendix Id not for Jobid.
	 * 
	 * @param HttpServletRequest
	 * @param String
	 *            Type
	 * @param DataSource
	 *            ds
	 * @return String Id
	 * @returnDescription Id of the Entity to which we have to generate the
	 *                    document.
	 */

	public static String getIdBasedOnType(HttpServletRequest request,
			String Type, DataSource ds) {
		String Id = "";
		if (Type.equals("MSA")) {
			if (request.getParameter("MSA_Id") != null)
				Id = request.getParameter("MSA_Id");
		} else if (Type.equals("Appendix")) {
			if (request.getParameter("Appendix_Id") != null)
				Id = request.getParameter("Appendix_Id");
		} else if (Type.equals("prj_job")) {
			Id = IMdao.getAppendixId(request.getParameter("jobid"), ds);
		}
		return Id;
	}

	// Use Case
	/**
	 * @name delDocument
	 * @purpose This method is used to delete the document from DocM.
	 * @steps 1. User requests to get the Document when Status of Entity Changes
	 *        to Approved. 2. if Document gets inserted but status doesn't gets
	 *        changed then this method is there to delete the document
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            Type
	 * @param String
	 *            jobId
	 * @param String
	 *            loginusername
	 * @return
	 * @returnDescription
	 */
	public static void delDocument(String entityId, String type, String jobId,
			String loginusername) {
		try {
			if (type.trim().equalsIgnoreCase("MSA"))
				EntityManager.deleteEntity(entityId, "MSA", true,
						Util.changeStringToDoubleQuotes(loginusername));
			else if (type.trim().equalsIgnoreCase("Appendix"))
				EntityManager.deleteEntity(entityId, "Appendix", true,
						Util.changeStringToDoubleQuotes(loginusername));
			else if (type.trim().equalsIgnoreCase("prj_job"))
				EntityManager.deleteEntity(jobId, "Addendum", true,
						Util.changeStringToDoubleQuotes(loginusername));
		} catch (Exception e) {
			logger.error("delDocument(String, String, String, String)", e);
			logger.error("delDocument(String, String, String, String)", e);
		}

	}

	// Use Case
	/**
	 * @name getEntityIdType
	 * @purpose This method is used to get the entityId and entityType.
	 * @steps 1. User requests to change the Status.
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            Type
	 * @param String
	 *            jobId
	 * @return String[]
	 * @returnDescription consiting of entityId and entityType
	 */
	public static String[] getEntityIdType(String entityId, String type,
			String jobId) {
		String[] entityIdType = new String[2];
		if (type.trim().equalsIgnoreCase("MSA")) {
			entityIdType[0] = entityId;
			entityIdType[1] = "MSA";
		} else if (type.trim().equalsIgnoreCase("Appendix")) {
			entityIdType[0] = entityId;
			entityIdType[1] = "Appendix";
		} else if (type.trim().equalsIgnoreCase("prj_job")) {
			entityIdType[0] = jobId;
			entityIdType[1] = "Addendum";
		}
		return entityIdType;
	}

	// Use Case
	/**
	 * @name updateReasonInDocM
	 * @purpose This method is used to update certain fields in the DocM when
	 *          status gets changed and there is the Interaction.
	 * @steps 1. User requests to change the Status.
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            Type
	 * @param String
	 *            jobId
	 * @param DataSource
	 *            ds
	 * @param String
	 *            loginusername
	 * @return
	 * @returnDescription
	 */
	public static void updateReasonInDocM(String entityId, String type,
			String jobId, DataSource ds, String userName) {
		String ilexSqlQuery = null;
		String docmSqlQuery = null;
		String domSqlUserUpdate = null;
		String docId = null;
		String reasonForChange = null;
		Connection conn = null;
		Connection docMConn = null;
		Statement docMStmt = null;
		Statement ilexStmt = null;
		Statement docMStmt1 = null;
		ResultSet ilexRs = null;
		int updateResult;
		int updateResult1;
		Document doc = null;

		if (type.trim().equalsIgnoreCase("MSA")) {
			ilexSqlQuery = "select lp_md_reason_for_change from lp_msa_detail where lp_md_mm_id = "
					+ entityId;
		} else if (type.trim().equalsIgnoreCase("Appendix")) {
			ilexSqlQuery = "select lx_pd_reason_for_change from lx_appendix_detail where lx_pd_pr_id = "
					+ entityId;
		} else if (type.trim().equalsIgnoreCase("prj_job")) {
			ilexSqlQuery = "select lm_js_reason_for_change from lm_job where lm_js_id ="
					+ jobId;
		}

		try {
			if (type.trim().equalsIgnoreCase("prj_job")) {
				docId = EntityManager.getDocumentId(jobId, "Addendum");
			} else {
				docId = EntityManager.getDocumentId(entityId, type);
			}

			conn = ds.getConnection();
			docMConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			ilexStmt = conn.createStatement();
			docMStmt = docMConn.createStatement();
			docMStmt1 = docMConn.createStatement();
			ilexRs = ilexStmt.executeQuery(ilexSqlQuery);

			if (ilexRs.next()) {
				reasonForChange = ilexRs.getString(1);
				if (reasonForChange == null)
					reasonForChange = "";
			}

			// System.out.println("Document Id Found is : " + docId);
			// System.out.println("Reason For Change is : " + reasonForChange);

			String documentVersion = DocumentUtility.getMaxDocumentVersion(
					Integer.parseInt(docId), doc, docMConn, false);
			if (logger.isDebugEnabled()) {
				logger.debug("updateReasonInDocM(String, String, String, DataSource, String) - Max version of the document is :: "
						+ documentVersion);
			}

			docmSqlQuery = "update dm_document_version set comment = '"
					+ reasonForChange + "' where dm_doc_id = " + docId
					+ " and DocumentVersion = " + documentVersion;

			domSqlUserUpdate = "update dm_document_version set UpdateDocumentDate = getdate(),"
					+ "UpdateDocumentUser = '"
					+ Util.changeStringToDoubleQuotes(userName)
					+ "', UpdateMethod = 'Implicit' where dm_doc_id  = "
					+ docId + " " + "and DocumentVersion = " + documentVersion;
			// System.out.println(domSqlUserUpdate);
			updateResult = docMStmt.executeUpdate(docmSqlQuery);
			updateResult1 = docMStmt1.executeUpdate(domSqlUserUpdate);
		} catch (Exception e) {
			logger.error(
					"updateReasonInDocM(String, String, String, DataSource, String)",
					e);

			logger.error(
					"updateReasonInDocM(String, String, String, DataSource, String)",
					e);
		} finally {
			DBUtil.close(conn);
			DBUtil.close(docMConn);

			DBUtil.close(ilexStmt);
			DBUtil.close(docMStmt);
			DBUtil.close(docMStmt1);
			DBUtil.close(ilexRs);
		}
	}

	// Use Case
	/**
	 * @name checkPreviousStatus
	 * @purpose This method is used to check if the Ilex Entity is already in
	 *          the Approved state then we don't have to transfer the PDF.
	 *          Method is there as the browser back button retains the previous
	 *          action in its cache and when preesed the back button it again
	 *          calls the same action.
	 * @steps 1. User requests to change the Status.
	 * 
	 * @param String
	 *            entityId
	 * @param String
	 *            Type
	 * @param String
	 *            jobId
	 * @param DataSource
	 *            ds
	 * @return boolean
	 * @returnDescription True if Entity is already in the Approved State
	 *                    otherwise false.
	 */
	public static boolean checkPreviousStatus(String entityId, String type,
			String jobId, DataSource ds) {
		boolean check = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String ilexSqlQuery = null;
		String prevStatus = null;

		if (type.trim().equalsIgnoreCase("MSA")) {
			ilexSqlQuery = "select lp_md_status from lp_msa_detail where lp_md_mm_id = "
					+ entityId;
		} else if (type.trim().equalsIgnoreCase("Appendix")) {
			ilexSqlQuery = "select lx_pr_status from lx_appendix_main where lx_pr_id = "
					+ entityId;
		} else if (type.trim().equalsIgnoreCase("prj_job")) {
			ilexSqlQuery = "select lm_js_status from lm_job where lm_js_id = "
					+ jobId;
		}
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(ilexSqlQuery);
			if (rs.next()) {
				prevStatus = rs.getString(1);
				if (prevStatus.trim().equals("A"))
					check = true;
			}
		} catch (Exception e) {
			logger.error(
					"checkPreviousStatus(String, String, String, DataSource)",
					e);

			logger.error(
					"checkPreviousStatus(String, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(conn);
			DBUtil.close(stmt);
			DBUtil.close(rs);
		}
		return check;
	}
}
