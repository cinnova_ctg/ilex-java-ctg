package com.mind.common;
import java.awt.Font;

import net.sf.jasperreports.engine.JRAbstractChartCustomizer;
import net.sf.jasperreports.engine.JRChart;


public class ChangeFontOfChartLegend extends JRAbstractChartCustomizer {
	public void customize(org.jfree.chart.JFreeChart chart, JRChart jasperChart) {
		chart.getLegend().setBackgroundPaint(null);
        chart.getLegend().setBorder(0,0,0,0);
        chart.getLegend().setItemFont(new Font("Helvetica",0,5));
        chart.setBackgroundImageAlpha(0);
        chart.getPlot().setBackgroundAlpha(0);
	}

}
