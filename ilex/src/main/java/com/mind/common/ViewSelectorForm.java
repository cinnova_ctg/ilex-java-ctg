package com.mind.common;

import org.apache.struts.action.ActionForm;

public class ViewSelectorForm extends ActionForm {
	
	/* Following Variables are used for View Selector
	 */
	private String appendixId =null;
	private String msaId =null;
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	private String monthWeekCheck = null;
	private String travelAuthorized = null;
	private String checkAppendix = null;
	private String msaName = null;
	private String appendixName = null;
	private String type =null;
	private String url = null;
	
	/*
	 * End Variable declaration for View Selector
	 */
	
	/*
	 *  Start Variable for PRM View Selector
	 */
		private String jobOwnerOtherCheck = null;
		private String[] jobSelectedStatus = null;
		private String[] jobSelectedOwners = null;
		private String jobMonthWeekCheck = null;
		
		private String ownerId = null;
		private String viewjobtype = null;
		private String fromPage = null;
	/*
	 *  End Variable for PRM View Selector
	 */
		public String getAppendixId() {
			return appendixId;
		}
		public void setAppendixId(String appendixId) {
			this.appendixId = appendixId;
		}
		public String getAppendixName() {
			return appendixName;
		}
		public void setAppendixName(String appendixName) {
			this.appendixName = appendixName;
		}
		public String getAppendixOtherCheck() {
			return appendixOtherCheck;
		}
		public void setAppendixOtherCheck(String appendixOtherCheck) {
			this.appendixOtherCheck = appendixOtherCheck;
		}
		public String[] getAppendixSelectedOwners() {
			return appendixSelectedOwners;
		}
		public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
			this.appendixSelectedOwners = appendixSelectedOwners;
		}
		public String[] getAppendixSelectedStatus() {
			return appendixSelectedStatus;
		}
		public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
			this.appendixSelectedStatus = appendixSelectedStatus;
		}
		public String getCheckAppendix() {
			return checkAppendix;
		}
		public void setCheckAppendix(String checkAppendix) {
			this.checkAppendix = checkAppendix;
		}
		public String getFromPage() {
			return fromPage;
		}
		public void setFromPage(String fromPage) {
			this.fromPage = fromPage;
		}
		public String getGo() {
			return go;
		}
		public void setGo(String go) {
			this.go = go;
		}
		public String getHome() {
			return home;
		}
		public void setHome(String home) {
			this.home = home;
		}
		public String getJobMonthWeekCheck() {
			return jobMonthWeekCheck;
		}
		public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
			this.jobMonthWeekCheck = jobMonthWeekCheck;
		}
		public String getJobOwnerOtherCheck() {
			return jobOwnerOtherCheck;
		}
		public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
			this.jobOwnerOtherCheck = jobOwnerOtherCheck;
		}
		public String[] getJobSelectedOwners() {
			return jobSelectedOwners;
		}
		public void setJobSelectedOwners(String[] jobSelectedOwners) {
			this.jobSelectedOwners = jobSelectedOwners;
		}
		public String[] getJobSelectedStatus() {
			return jobSelectedStatus;
		}
		public void setJobSelectedStatus(String[] jobSelectedStatus) {
			this.jobSelectedStatus = jobSelectedStatus;
		}
		public String getMonthWeekCheck() {
			return monthWeekCheck;
		}
		public void setMonthWeekCheck(String monthWeekCheck) {
			this.monthWeekCheck = monthWeekCheck;
		}
		public String getMsaId() {
			return msaId;
		}
		public void setMsaId(String msaId) {
			this.msaId = msaId;
		}
		public String getMsaName() {
			return msaName;
		}
		public void setMsaName(String msaName) {
			this.msaName = msaName;
		}
		public String getOwnerId() {
			return ownerId;
		}
		public void setOwnerId(String ownerId) {
			this.ownerId = ownerId;
		}
		public String getPrevSelectedOwner() {
			return prevSelectedOwner;
		}
		public void setPrevSelectedOwner(String prevSelectedOwner) {
			this.prevSelectedOwner = prevSelectedOwner;
		}
		public String getPrevSelectedStatus() {
			return prevSelectedStatus;
		}
		public void setPrevSelectedStatus(String prevSelectedStatus) {
			this.prevSelectedStatus = prevSelectedStatus;
		}
		public String getTravelAuthorized() {
			return travelAuthorized;
		}
		public void setTravelAuthorized(String travelAuthorized) {
			this.travelAuthorized = travelAuthorized;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getViewjobtype() {
			return viewjobtype;
		}
		public void setViewjobtype(String viewjobtype) {
			this.viewjobtype = viewjobtype;
		}
}
