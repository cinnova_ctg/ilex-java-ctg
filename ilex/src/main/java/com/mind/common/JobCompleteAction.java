package com.mind.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.ManageOutageOrganizer;
import com.mind.bean.JobCompleteBean;
import com.mind.common.actions.EmailAction;
import com.mind.common.dao.ChangeStatus;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.common.formbean.JobCompleteForm;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.ESADao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.Jobdao;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.DocumentIdNotUpdatedException;
import com.mind.docm.exceptions.EntityDetailsNotFoundException;
import com.mind.docm.exceptions.NotAbleToInsertDocument;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class JobCompleteAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(JobCompleteAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		JobCompleteForm jobcompeleteform = (JobCompleteForm) form;
		JobCompleteBean jobCompleteBean = new JobCompleteBean();

		String list = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		int changestatusflag = -1;

		if (request.getParameter("ownerId") != null) {
			jobcompeleteform.setOwnerId("" + request.getParameter("ownerId"));
		} else {
			jobcompeleteform.setOwnerId("%");
		}
		if (request.getParameter("viewjobtype") != null) {
			jobcompeleteform.setViewjobtype(""
					+ request.getParameter("viewjobtype"));
		} else {
			jobcompeleteform.setViewjobtype("A");
		}

		if (request.getParameter("Type") != null) {
			jobcompeleteform.setType(request.getParameter("Type"));
		}
		if (request.getParameter("jobid") != null) {
			jobcompeleteform.setJobId(request.getParameter("jobid"));
		}
		if (request.getParameter("Status") != null) {
			jobcompeleteform.setStatus(request.getParameter("Status"));
		}

		jobcompeleteform.setAppendixId(IMdao.getAppendixId(
				jobcompeleteform.getJobId(),
				getDataSource(request, "ilexnewDB")));
		jobcompeleteform.setAppendixName(Appendixdao.getAppendixname(
				jobcompeleteform.getAppendixId(),
				getDataSource(request, "ilexnewDB")));
		jobcompeleteform.setMsaId(IMdao.getMSAId(
				jobcompeleteform.getAppendixId(),
				getDataSource(request, "ilexnewDB")));
		jobcompeleteform.setMsaName(Appendixdao.getMsanameFromMsaID(
				jobcompeleteform.getMsaId(),
				getDataSource(request, "ilexnewDB")));
		BeanUtils.copyProperties(jobCompleteBean, jobcompeleteform);
		Jobdao.checkJobCompleteStatus(jobCompleteBean,
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(jobcompeleteform, jobCompleteBean);
		jobcompeleteform.setJobName(Jobdao.getJobname(
				jobcompeleteform.getJobId(),
				getDataSource(request, "ilexnewDB")));
		jobcompeleteform.setJobType(Jobdao.getJobtypename(
				jobcompeleteform.getJobId(),
				getDataSource(request, "ilexnewDB")));
		BeanUtils.copyProperties(jobCompleteBean, jobcompeleteform);
		Jobdao.getJobStatusName(jobCompleteBean,
				getDataSource(request, "ilexnewDB"));

		int customerFieldValuesCount = Jobdao.getCustomFieldCount(
				jobcompeleteform.getJobId(), jobcompeleteform.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		jobcompeleteform.setCustomerFieldValuesCount(customerFieldValuesCount);

		String financialFlag = Jobdao.getFinancialFlagCheckforJobCompletion(
				jobcompeleteform.getJobId(),
				getDataSource(request, "ilexnewDB"));
		BeanUtils.copyProperties(jobcompeleteform, jobCompleteBean);

		if (jobcompeleteform.getJobType().equals("Addendum")) {
			list = Menu.getStatus("Addendum", jobcompeleteform.getJobStatus()
					.charAt(0), "", jobcompeleteform.getAppendixId(),
					jobcompeleteform.getJobId(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));
		} else {
			list = Menu.getStatus("prj_job", jobcompeleteform.getJobStatus()
					.charAt(0), "", jobcompeleteform.getAppendixId(),
					jobcompeleteform.getJobId(), "", loginuserid,
					getDataSource(request, "ilexnewDB"));
		}
		request.setAttribute("list", list);

		if (ViewList.CheckAppendixType(jobcompeleteform.getAppendixId(),
				getDataSource(request, "ilexnewDB")).equals("1")) {
			request.setAttribute("NetMedX", "NetMedX");
		}

		request.setAttribute("jobtype", jobcompeleteform.getJobType());
		request.setAttribute("list", request.getAttribute("list"));
		request.setAttribute("viewType", jobcompeleteform.getViewType());
		request.setAttribute("financialFlag", financialFlag);
		Map<String, Object> map;

		if (jobcompeleteform.getJobId() != null
				&& !jobcompeleteform.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(jobcompeleteform.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);

		}

		if (jobcompeleteform.getNext() != null) {
			request.setAttribute("verified", "verified");
			if (jobcompeleteform.getOverAllStatus().equals("Passed")) {
				changestatusflag = ChangeStatus.Status(
						jobcompeleteform.getMsaId(),
						jobcompeleteform.getAppendixId(),
						jobcompeleteform.getJobId(), "", "",
						jobcompeleteform.getType(),
						jobcompeleteform.getStatus(), "",
						getDataSource(request, "ilexnewDB"), "", loginuserid);
				request.setAttribute("changestatusflag", "" + changestatusflag);
				Jobdao.updateJobOffSiteDate(jobcompeleteform.getJobId(),
						getDataSource(request, "ilexnewDB"));
				if (changestatusflag == 0) {
					int[] outageIds = ManageOutageOrganizer
							.getEscalatedTicketOutageID(
									jobcompeleteform.getJobId(),
									getDataSource(request, "ilexnewDB"));
					if (outageIds[0] > 0) {
						changestatusflag = ManageOutageOrganizer
								.setStatusForDashBoardEscalatedTicket(
										jobcompeleteform.getJobId(),
										outageIds[0], outageIds[1],
										loginuserid,
										getDataSource(request, "ilexnewDB"));
					}
				}

				if (changestatusflag == 0) {
					if (!jobcompeleteform.getJobStatus().equals("Complete")
							&& jobcompeleteform.getStatus().equals("F")) {
						ArrayList commissionPOs = ESADao.getCommissionPOList(
								jobcompeleteform.getJobId(),
								getDataSource(request, "ilexnewDB"));
						String POId = "0";

						Iterator PoList = commissionPOs.iterator();
						while (PoList.hasNext()) {
							POId = (String) PoList.next();
							byte[] poBuffer = null;
							byte[] woBuffer = null;
							String POdocId = null;
							String WOdocId = null;

							poBuffer = EmailAction.getBuffer(POId, "PO",
									loginuserid, request,
									getDataSource(request, "ilexnewDB"));
							woBuffer = EmailAction.getBuffer(POId, "WO",
									loginuserid, request,
									getDataSource(request, "ilexnewDB"));

							try {
								POdocId = EntityManager.approveEntity(POId,
										"PO", poBuffer);
								WOdocId = EntityManager.approveEntity(POId,
										"WO", woBuffer);
							} catch (NotAbleToInsertDocument e) {
								e.printStackTrace();
								logger.error(
										"JobCompletion ESA PO Creation Process - approveEntity(String, String, byte[])",
										e);
							} catch (ControlledTypeException e) {
								e.printStackTrace();
								logger.error(
										"JobCompletion ESA PO Creation Process - approveEntity(String, String, byte[])",
										e);
							} catch (DocMFormatException e) {
								e.printStackTrace();
								logger.error(
										"JobCompletion ESA PO Creation Process - approveEntity(String, String, byte[])",
										e);
							} catch (CouldNotAddToRepositoryException e) {
								e.printStackTrace();
								logger.error(
										"JobCompletion ESA PO Creation Process - approveEntity(String, String, byte[])",
										e);
							} catch (CouldNotReplaceException e) {
								e.printStackTrace();
								logger.error(
										"JobCompletion ESA PO Creation Process - approveEntity(String, String, byte[])",
										e);
							} catch (CouldNotCheckDocumentException e) {
								e.printStackTrace();
								logger.error(
										"JobCompletion ESA PO Creation Process - approveEntity(String, String, byte[])",
										e);
							} catch (EntityDetailsNotFoundException e) {
								e.printStackTrace();
								logger.error(
										"JobCompletion ESA PO Creation Process - approveEntity(String, String, byte[])",
										e);
							} catch (DocumentIdNotUpdatedException e) {
								e.printStackTrace();
								logger.error(
										"JobCompletion ESA PO Creation Process - approveEntity(String, String, byte[])",
										e);
							}
						}
					}
				}

				request.setAttribute("type", "jobCompleted");
				request.setAttribute("ownerId", jobcompeleteform.getOwnerId());
				request.setAttribute("job_Id", jobcompeleteform.getJobId());
				request.setAttribute("viewjobtype",
						jobcompeleteform.getViewjobtype());
				request.setAttribute("pagedashboard", "jobdashboard");
				return mapping.findForward("temppage");
			}
		}
		/*
		 * if(request.getParameter("throughJobDashboard")!=null){ String url =
		 * "/JobDashboardAction.do?tabId="
		 * +JobDashboardTabType.COMPLETE_TAB+"&appendixId="
		 * +jobcompeleteform.getAppendixId
		 * ()+"&jobId="+jobcompeleteform.getJobId(); ActionForward fwd = new
		 * ActionForward(); fwd.setPath(url); return fwd; }
		 */
		if (logger.isDebugEnabled()) {
			logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - ---------------------------"
					+ request.getParameter("throughJobDashboard"));
		}
		if (jobcompeleteform.getJobId() != null
				&& !jobcompeleteform.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(jobcompeleteform.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		if (request.getParameter("throughJobDashboard") != null) {
			request.setAttribute("throughJobDashboard", "throughJobDashboard");
			RequestDispatcher ds = request
					.getRequestDispatcher("/common/JobComplete.jsp");
			try {
				if (request
						.getClass()
						.getName()
						.equals("org.apache.struts.upload.MultipartRequestWrapper")) {
					logger.debug("Casting MultiparRequestWrapper to custom class");
					request = new MultipartRequestWrapper(request);
				}
				ds.include(request, response);
			} catch (Exception e) {
				logger.warn(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - exception ignored",
						e);

			}
		} else
			return mapping.findForward("success");

		return null;
	}

}
