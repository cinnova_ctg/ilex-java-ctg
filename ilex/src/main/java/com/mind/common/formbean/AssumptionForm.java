package com.mind.common.formbean;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AssumptionForm extends ActionForm {
	private String check[] = null;
	private String assumption[] = null;
	private String assumption_Id[] = null;

	private String newassumption_Id = null;
	private String newassumption = null;

	private String type = null;
	private String id = null;

	private String ref = null;

	private String submit = null;

	private String addendum_id = null;
	private String name = null;

	private String fromflag = null;
	private String dashboardid = null;

	private String jobId = null;
	private String jobName = null;
	private String msaName = null;
	private String msaId = null;
	private String appendixName = null;
	private String appendixId = null;
	private String activityName = null;
	private String activity_Id = null;

	private String from = null;
	private String jobid = null;
	private String resourceStatus = null;
	private String resourceStatusList = null;
	private String chkaddendum = null;
	private String chkdetailactivity = null;
	private String resourceListPage = null;
	private String resourceType = null;
	private String resourceListType = null;

	private ArrayList defaultAssumptionList = new ArrayList();
	private String defaultAssumption = null;

	public String getDefaultAssumption() {
		return defaultAssumption;
	}

	public void setDefaultAssumption(String defaultAssumption) {
		this.defaultAssumption = defaultAssumption;
	}

	public ArrayList getDefaultAssumptionList() {
		return defaultAssumptionList;
	}

	public void setDefaultAssumptionList(ArrayList defaultAssumptionList) {
		this.defaultAssumptionList = defaultAssumptionList;
	}

	public String getResourceListType() {
		return resourceListType;
	}

	public void setResourceListType(String resourceListType) {
		this.resourceListType = resourceListType;
	}

	public String getResourceListPage() {
		return resourceListPage;
	}

	public void setResourceListPage(String resourceListPage) {
		this.resourceListPage = resourceListPage;
	}

	public String getChkaddendum() {
		return chkaddendum;
	}

	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}

	public String getChkdetailactivity() {
		return chkdetailactivity;
	}

	public void setChkdetailactivity(String chkdetailactivity) {
		this.chkdetailactivity = chkdetailactivity;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getResourceStatus() {
		return resourceStatus;
	}

	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	public String getResourceStatusList() {
		return resourceStatusList;
	}

	public void setResourceStatusList(String resourceStatusList) {
		this.resourceStatusList = resourceStatusList;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String[] getCheck() {
		return check;
	}

	public void setCheck(String[] check) {
		this.check = check;
	}

	public String getCheck(int i) {
		return check[i];
	}

	public String[] getAssumption() {
		return assumption;
	}

	public void setAssumption(String[] assumption) {
		this.assumption = assumption;
	}

	public String getAssumption(int i) {
		return assumption[i];
	}

	public String[] getAssumption_Id() {
		return assumption_Id;
	}

	public void setAssumption_Id(String[] assumption_Id) {
		this.assumption_Id = assumption_Id;
	}

	public String getAssumption_Id(int i) {
		return assumption_Id[i];
	}

	public String getNewassumption() {
		return newassumption;
	}

	public void setNewassumption(String newassumption) {
		this.newassumption = newassumption;
	}

	public String getNewassumption_Id() {
		return newassumption_Id;
	}

	public void setNewassumption_Id(String newassumption_Id) {
		this.newassumption_Id = newassumption_Id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getAddendum_id() {
		return addendum_id;
	}

	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDashboardid() {
		return dashboardid;
	}

	public void setDashboardid(String dashboardid) {
		this.dashboardid = dashboardid;
	}

	public String getFromflag() {
		return fromflag;
	}

	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		check = null;
		assumption = null;
		assumption_Id = null;

		newassumption_Id = null;
		newassumption = null;

		type = null;
		id = null;

		submit = null;
		ref = null;
		addendum_id = null;
		name = null;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getActivity_Id() {
		return activity_Id;
	}

	public void setActivity_Id(String activity_Id) {
		this.activity_Id = activity_Id;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
}
