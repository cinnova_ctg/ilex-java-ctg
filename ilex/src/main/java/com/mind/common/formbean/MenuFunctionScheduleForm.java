package com.mind.common.formbean;

import org.apache.struts.action.ActionForm;

public class MenuFunctionScheduleForm extends ActionForm 
{
	private String appendix_Id = null;
	private String job_Id = null;
	private String type = null;
	
	private String planned_schedule_required = null;
	private String planned_schedule_effective = null;
	private String planned_schedule_days = null;
	
	
	public String getAppendix_Id() {
		return appendix_Id;
	}
	public void setAppendix_Id(String appendix_Id) {
		this.appendix_Id = appendix_Id;
	}
	
	
	public String getJob_Id() {
		return job_Id;
	}
	public void setJob_Id(String job_Id) {
		this.job_Id = job_Id;
	}
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	public String getPlanned_schedule_required() {
		return planned_schedule_required;
	}
	public void setPlanned_schedule_required(String planned_schedule_required) {
		this.planned_schedule_required = planned_schedule_required;
	}
	
	
	public String getPlanned_schedule_effective() {
		return planned_schedule_effective;
	}
	public void setPlanned_schedule_effective(String planned_schedule_effective) {
		this.planned_schedule_effective = planned_schedule_effective;
	}
	
	
	public String getPlanned_schedule_days() {
		return planned_schedule_days;
	}
	public void setPlanned_schedule_days(String planned_schedule_days) {
		this.planned_schedule_days = planned_schedule_days;
	}	
}
