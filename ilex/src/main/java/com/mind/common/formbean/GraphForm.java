package com.mind.common.formbean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class GraphForm extends ActionForm
{

    private static final long serialVersionUID = 0x2d36313934353436L;
    private String TITLECHART;
    private String LEGEND;
    private String SERIE_1;
    private String SERIE_TYPE_1;
    private String PIE_STYLE_1;
    private String PIE_STYLE_2;
    private String PIE_STYLE_3;
    private String PIE_STYLE_4;
    private String PIE_NAME_1;
    private String PIE_NAME_2;
    private String PIE_NAME_3;
    private String PIE_NAME_4;
    private String PIE_LABEL_FORMAT;
    private String SERIE_DATA_1;
    private String SERIE_FONT_1;
    private String SERIE_LABELS_1;
    private String SERIE_TOGETHER_1;
    private String CHART_BORDER;
    private String CHART_FILL;
    private String LEGEND_BORDER;
    private String LEGEND_VERTICAL;
    private String LEGEND_POSITION;
    private String LEGEND_FILL;
    private String PIECHART_3D;
    private String WIDTH;
    private String HEIGHT;
    private String LENGTH;
    private String LEGEND_TITLE;
//	private String TITLE_FONT;
	
    public GraphForm()
    {
        TITLECHART = null;
        LEGEND = null;
        SERIE_1 = null;
        SERIE_TYPE_1 = null;
        PIE_STYLE_1 = null;
        PIE_STYLE_2 = null;
        PIE_STYLE_3 = null;
        PIE_STYLE_4 = null;
        PIE_NAME_1 = null;
        PIE_NAME_2 = null;
        PIE_NAME_3 = null;
        PIE_NAME_4 = null;
        PIE_LABEL_FORMAT = null;
        SERIE_DATA_1 = null;
        SERIE_FONT_1 = null;
        SERIE_LABELS_1 = null;
        SERIE_TOGETHER_1 = null;
        CHART_BORDER = null;
        CHART_FILL = null;
        LEGEND_BORDER = null;
        LEGEND_VERTICAL = null;
        LEGEND_POSITION = null;
        LEGEND_FILL = null;
        PIECHART_3D = null;
        WIDTH = null;
        HEIGHT = null;
        LENGTH = null;
        LEGEND_TITLE = null;
//		TITLE_FONT = null;
    }

 /*   public String getTITLE_FONT() {
		return TITLE_FONT;
	}

	public void setTITLE_FONT(String title_font) {
		TITLE_FONT = title_font;
	}
*/
	public void setTITLECHART(String s)
    {
        TITLECHART = s;
    }

    public String getTITLECHART()
    {
        return TITLECHART;
    }

    public void setLEGEND(String s)
    {
        LEGEND = s;
    }

    public String getLEGEND()
    {
        return LEGEND;
    }

    public void setSERIE_1(String s)
    {
        SERIE_1 = s;
    }

    public String getSERIE_1()
    {
        return SERIE_1;
    }

    public void setSERIE_TYPE_1(String s)
    {
        SERIE_TYPE_1 = s;
    }

    public String getSERIE_TYPE_1()
    {
        return SERIE_TYPE_1;
    }

    public void setPIE_STYLE_1(String s)
    {
        PIE_STYLE_1 = s;
    }

    public String getPIE_STYLE_1()
    {
        return PIE_STYLE_1;
    }

    public void setPIE_STYLE_2(String s)
    {
        PIE_STYLE_2 = s;
    }

    public String getPIE_STYLE_2()
    {
        return PIE_STYLE_2;
    }

    public void setPIE_STYLE_3(String s)
    {
        PIE_STYLE_3 = s;
    }

    public String getPIE_STYLE_3()
    {
        return PIE_STYLE_3;
    }

    public void setPIE_STYLE_4(String s)
    {
        PIE_STYLE_4 = s;
    }

    public String getPIE_STYLE_4()
    {
        return PIE_STYLE_4;
    }

    public void setPIE_NAME_1(String s)
    {
        PIE_NAME_1 = s;
    }

    public String getPIE_NAME_1()
    {
        return PIE_NAME_1;
    }

    public void setPIE_NAME_2(String s)
    {
        PIE_NAME_2 = s;
    }

    public String getPIE_NAME_2()
    {
        return PIE_NAME_2;
    }

    public void setPIE_NAME_3(String s)
    {
        PIE_NAME_3 = s;
    }

    public String getPIE_NAME_3()
    {
        return PIE_NAME_3;
    }

    public void setPIE_NAME_4(String s)
    {
        PIE_NAME_4 = s;
    }

    public String getPIE_NAME_4()
    {
        return PIE_NAME_4;
    }

    public void setPIE_LABEL_FORMAT(String s)
    {
        PIE_LABEL_FORMAT = s;
    }

    public String getPIE_LABEL_FORMAT()
    {
        return PIE_LABEL_FORMAT;
    }

    public void setSERIE_DATA_1(String s)
    {
        SERIE_DATA_1 = s;
    }

    public String getSERIE_DATA_1()
    {
        return SERIE_DATA_1;
    }

    public void setSERIE_FONT_1(String s)
    {
        SERIE_FONT_1 = s;
    }

    public String getSERIE_FONT_1()
    {
        return SERIE_FONT_1;
    }

    public void setSERIE_LABELS_1(String s)
    {
        SERIE_LABELS_1 = s;
    }

    public String getSERIE_LABELS_1()
    {
        return SERIE_LABELS_1;
    }

    public void setSERIE_TOGETHER_1(String s)
    {
        SERIE_TOGETHER_1 = s;
    }

    public String getSERIE_TOGETHER_1()
    {
        return SERIE_TOGETHER_1;
    }

    public void setCHART_BORDER(String s)
    {
        CHART_BORDER = s;
    }

    public String getCHART_BORDER()
    {
        return CHART_BORDER;
    }

    public void setCHART_FILL(String s)
    {
        CHART_FILL = s;
    }

    public String getCHART_FILL()
    {
        return CHART_FILL;
    }

    public void setLEGEND_BORDER(String s)
    {
        LEGEND_BORDER = s;
    }

    public String getLEGEND_BORDER()
    {
        return LEGEND_BORDER;
    }

    public void setLEGEND_VERTICAL(String s)
    {
        LEGEND_VERTICAL = s;
    }

    public String getLEGEND_VERTICAL()
    {
        return LEGEND_VERTICAL;
    }

    public void setLEGEND_POSITION(String s)
    {
        LEGEND_POSITION = s;
    }

    public String getLEGEND_POSITION()
    {
        return LEGEND_POSITION;
    }

    public void setLEGEND_FILL(String s)
    {
        LEGEND_FILL = s;
    }

    public String getLEGEND_FILL()
    {
        return LEGEND_FILL;
    }

    public void setPIECHART_3D(String s)
    {
        PIECHART_3D = s;
    }

    public String getPIECHART_3D()
    {
        return PIECHART_3D;
    }

    public void setWIDTH(String s)
    {
        WIDTH = s;
    }

    public String getWIDTH()
    {
        return WIDTH;
    }

    public void setHEIGHT(String s)
    {
        HEIGHT = s;
    }

    public String getHEIGHT()
    {
        return HEIGHT;
    }

    public void setLENGTH(String s)
    {
        LENGTH = s;
    }

    public String getLENGTH()
    {
        return LENGTH;
    }

    public void setLEGEND_TITLE(String s)
    {
        LEGEND_TITLE = s;
    }

    public String getLEGEND_TITLE()
    {
        return LEGEND_TITLE;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request)
    {
        TITLECHART = null;
        LEGEND = null;
        SERIE_1 = null;
        SERIE_TYPE_1 = null;
        PIE_STYLE_1 = null;
        PIE_STYLE_2 = null;
        PIE_STYLE_3 = null;
        PIE_STYLE_4 = null;
        PIE_NAME_1 = null;
        PIE_NAME_2 = null;
        PIE_NAME_3 = null;
        PIE_NAME_4 = null;
        PIE_LABEL_FORMAT = null;
        SERIE_DATA_1 = null;
        SERIE_FONT_1 = null;
        SERIE_LABELS_1 = null;
        SERIE_TOGETHER_1 = null;
        CHART_BORDER = null;
        CHART_FILL = null;
        LEGEND_BORDER = null;
        LEGEND_VERTICAL = null;
        LEGEND_POSITION = null;
        LEGEND_FILL = null;
        PIECHART_3D = null;
        WIDTH = null;
        HEIGHT = null;
        LENGTH = null;
        LEGEND_TITLE = null;
    }
}
