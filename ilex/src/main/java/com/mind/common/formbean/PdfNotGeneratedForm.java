package com.mind.common.formbean;

import org.apache.struts.action.ActionForm;

public class PdfNotGeneratedForm extends ActionForm{
	
	private String generatePDF = null;

	public String getGeneratePDF() {
		return generatePDF;
	}

	public void setGeneratePDF(String generatePDF) {
		this.generatePDF = generatePDF;
	}

}
