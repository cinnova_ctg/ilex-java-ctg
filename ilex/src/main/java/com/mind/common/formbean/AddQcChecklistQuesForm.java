package com.mind.common.formbean;

import org.apache.struts.action.ActionForm;

public class AddQcChecklistQuesForm extends ActionForm{

	
	private String appendix_Id = null;
	private String appendixname = null;
	private String save = null;
	private String back = null;
	//private String fromtype = null;
	private String authenticate = "";
	private String newrowsadded = "";
	private String viewjobtype = null;
	private String ownerId = null;
	private String addmore = "";
	private String addoption = "";
	private String[] check = null;
	private String group = "General";
	private String item = null;
	private String[] option = null;
	private String optiontype = "S";
	
	public String getAddmore() {
		return addmore;
	}
	public void setAddmore(String addmore) {
		this.addmore = addmore;
	}
	public String getAddoption() {
		return addoption;
	}
	public void setAddoption(String addoption) {
		this.addoption = addoption;
	}
	public String getAppendix_Id() {
		return appendix_Id;
	}
	public void setAppendix_Id(String appendix_Id) {
		this.appendix_Id = appendix_Id;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String[] getCheck() {
		return check;
	}
	public String getCheck( int i ) {
		return check[i];
	}
	
	public void setCheck(String[] check) {
		this.check = check;
	}
	
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getNewrowsadded() {
		return newrowsadded;
	}
	public void setNewrowsadded(String newrowsadded) {
		this.newrowsadded = newrowsadded;
	}
	public String[] getOption() {
		return option;
	}
	public String getOption(int i) {
		return option[i];
	}
	public void setOption(String[] option) {
		this.option = option;
	}
	public String getOptiontype() {
		return optiontype;
	}
	public void setOptiontype(String optiontype) {
		this.optiontype = optiontype;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	
	

}
