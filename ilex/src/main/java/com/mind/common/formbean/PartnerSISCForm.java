package com.mind.common.formbean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class PartnerSISCForm extends ActionForm {
	
	private String appendixId = null;
	private String appendixName = null;
	private String msaId = null;
	private String fromType = null;
	private String viewType = null;
	private String dataType = null;
	
	private String SCSIId= null;
	private String SCValue = null;
	private String SIValue = null;
	
	private String Context = null;
	private String contextType = null;
	
	private String delete=null;
	private String save=null;
	private String action=null;
	
	private String viewJobType = null;
	private String ownerId = null;
	private String netType = null;
	private String fromPage = null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getContext() {
		return Context;
	}
	public void setContext(String context) {
		Context = context;
	}
	public String getContextType() {
		return contextType;
	}
	public void setContextType(String contextType) {
		this.contextType = contextType;
	}
	public String getFromType() {
		return fromType;
	}
	public void setFromType(String fromType) {
		this.fromType = fromType;
	}
	public String getNetType() {
		return netType;
	}
	public void setNetType(String netType) {
		this.netType = netType;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getSCSIId() {
		return SCSIId;
	}
	public void setSCSIId(String id) {
		SCSIId = id;
	}
	public String getSCValue() {
		return SCValue;
	}
	public void setSCValue(String value) {
		SCValue = value;
	}
	public String getSIValue() {
		return SIValue;
	}
	public void setSIValue(String value) {
		SIValue = value;
	}
	public String getViewJobType() {
		return viewJobType;
	}
	public void setViewJobType(String viewJobType) {
		this.viewJobType = viewJobType;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public String getDelete() {
		return delete;
	}
	public void setDelete(String delete) {
		this.delete = delete;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}
}
