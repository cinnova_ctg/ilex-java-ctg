package com.mind.common.formbean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class PartnerSOWForm extends ActionForm
{
	private String pfCheck[] = null;
	private String pfSow[] = null;
	private String[] pfSow_Id = null;
	
	private String pfNewsow_Id = null;
	private String pfNewsow= null;
	
	private String pfType = null;
	private String pfId = null;
	
	private String pfRef = null;
	
	private String pfSubmit = null;
	
	private String pfAddendum_id = null;
	
	private String pfName = null;
	private String pfBack = null;
	private String pfAuthenticate = "";
	
	private String pfFromflag = null;
	private String pfDashboardid = null;
	
	private String pfAppindixName = null;
	private String pfJobName = null;
	private String pfViewjobtype = null;
	private String pfOwnerId = null;
	
	public String getPfAddendum_id() {
		return pfAddendum_id;
	}
	public void setPfAddendum_id(String pfAddendum_id) {
		this.pfAddendum_id = pfAddendum_id;
	}
	public String getPfAppindixName() {
		return pfAppindixName;
	}
	public void setPfAppindixName(String pfAppindixName) {
		this.pfAppindixName = pfAppindixName;
	}
	public String getPfAuthenticate() {
		return pfAuthenticate;
	}
	public void setPfAuthenticate(String pfAuthenticate) {
		this.pfAuthenticate = pfAuthenticate;
	}
	public String getPfBack() {
		return pfBack;
	}
	public void setPfBack(String pfBack) {
		this.pfBack = pfBack;
	}
	public String[] getPfCheck() {
		return pfCheck;
	}
	public void setPfCheck(String[] pfCheck) {
		this.pfCheck = pfCheck;
	}
	public String getPfDashboardid() {
		return pfDashboardid;
	}
	public void setPfDashboardid(String pfDashboardid) {
		this.pfDashboardid = pfDashboardid;
	}
	public String getPfFromflag() {
		return pfFromflag;
	}
	public void setPfFromflag(String pfFromflag) {
		this.pfFromflag = pfFromflag;
	}
	public String getPfId() {
		return pfId;
	}
	public void setPfId(String pfId) {
		this.pfId = pfId;
	}
	public String getPfJobName() {
		return pfJobName;
	}
	public void setPfJobName(String pfJobName) {
		this.pfJobName = pfJobName;
	}
	public String getPfName() {
		return pfName;
	}
	public void setPfName(String pfName) {
		this.pfName = pfName;
	}
	public String getPfNewsow() {
		return pfNewsow;
	}
	public void setPfNewsow(String pfNewsow) {
		this.pfNewsow = pfNewsow;
	}
	public String getPfNewsow_Id() {
		return pfNewsow_Id;
	}
	public void setPfNewsow_Id(String pfNewsow_Id) {
		this.pfNewsow_Id = pfNewsow_Id;
	}
	public String getPfOwnerId() {
		return pfOwnerId;
	}
	public void setPfOwnerId(String pfOwnerId) {
		this.pfOwnerId = pfOwnerId;
	}
	public String getPfRef() {
		return pfRef;
	}
	public void setPfRef(String pfRef) {
		this.pfRef = pfRef;
	}
	public String[] getPfSow() {
		return pfSow;
	}
	public void setPfSow(String[] pfSow) {
		this.pfSow = pfSow;
	}
	public String[] getPfSow_Id() {
		return pfSow_Id;
	}
	public void setPfSow_Id(String[] pfSow_Id) {
		this.pfSow_Id = pfSow_Id;
	}
	public String getPfSubmit() {
		return pfSubmit;
	}
	public void setPfSubmit(String pfSubmit) {
		this.pfSubmit = pfSubmit;
	}
	public String getPfType() {
		return pfType;
	}
	public void setPfType(String pfType) {
		this.pfType = pfType;
	}
	public String getPfViewjobtype() {
		return pfViewjobtype;
	}
	public void setPfViewjobtype(String pfViewjobtype) {
		this.pfViewjobtype = pfViewjobtype;
	}
	
	
		
	public String getPfCheck( int i ) {
		return pfCheck[i];
	}
	
		
	public String getPfSow( int i ) {
		return pfSow[i];
	}
		
	public String getPfSow_Id( int i ) {
		return pfSow_Id[i];
	}
	
	
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		pfCheck = null;
		pfSow = null;
		
		pfNewsow_Id = null;
		pfNewsow = null;
		
		pfType = null;
		pfId = null;
		pfRef = null;
		pfSubmit = null;
		pfAddendum_id = null;
		pfName = null;
	}
	
	
	
}

