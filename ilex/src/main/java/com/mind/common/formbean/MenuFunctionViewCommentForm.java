package com.mind.common.formbean;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
public class MenuFunctionViewCommentForm extends ActionForm{

	private String forwardId = null;
	private String type = null;
	private String commentSize = null;
	private String msaId = null;
	private String msaName = null;
	private String appendixId =null;
	private String appendixName = null;
	private String name = null;
	private String jobId = null;
	private String jobName = null;
	private String activity_Id = null;
	private String activityName = null;
	
//	New UI Changes Related to MSA ViewSelector and ViewImages.
	
	private String monthWeekCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	
//	New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String   appendixOtherCheck = null;
	private String   viewjobtype = null;
	private String   ownerId = null;
	
// For Activity Detail Menu
    private String ref = null;	
	
	//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}
	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}
	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}
	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}
	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}
	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}
	public String getAppendixSelectedOwners( int i ) {
		return appendixSelectedOwners[i];
	}
	
	public String getAppendixSelectedStatus( int i ) {
		return appendixSelectedStatus[i];
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}
	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}
	public String getOtherCheck() {
		return otherCheck;
	}
	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}
	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}
	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}
	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}
	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}
	public String[] getSelectedOwners() {
		return selectedOwners;
	}
	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}
	public String[] getSelectedStatus() {
		return selectedStatus;
	}
	public String getSelectedStatus( int i ) {
		return selectedStatus[i];
	}
	public String getSelectedOwners( int i ) {
		return selectedOwners[i];
	}
	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appenedixId) {
		this.appendixId = appenedixId;
	}
	public String getCommentSize() {
		return commentSize;
	}
	public void setCommentSize(String commentSize) {
		this.commentSize = commentSize;
	}
	public String getForwardId() {
		return forwardId;
	}
	public void setForwardId(String forwardId) {
		this.forwardId = forwardId;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id(String activity_Id) {
		this.activity_Id = activity_Id;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	
	
}
