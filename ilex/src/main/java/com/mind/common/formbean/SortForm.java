/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: A formbean containing getter/setter methods for sorting window page
 *
 */

package com.mind.common.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class SortForm extends ActionForm {
	private ArrayList sortparam = null;
	private String[] selectedparam = null;
	private String sortcriteria = "ASC";
	private String type = "";
	private String ref = "";
	private String id = "";
	private String id1 = "";
	private String addendum_id = null;
	private String rdofilter = null;
	private String from = null;

	private String siteid = null;

	private String sort = null;

	private String ref1 = null;
	private String page = null;
	private String viewjobtype = null;
	// for filter sorting
	private String currentstatus = null;
	private String nettype = null;
	private String ownerId = null;
	// for filter sorting

	// Start :Added For Edit Site Master

	private String sitename = null;
	private String sitenumber = null;
	private String sitecity = null;
	private String flag = null;
	// End : For Site Master
	private String invoice_Flag = null;
	private String partner_name = null;
	private String powo_number = null;
	private String from_date = null;
	private String to_date = null;
	private String invoiceno = null;

	private String siteSearchName = null;
	private String siteSearchNumber = null;
	private String siteSearchCity = null;
	private String siteSearchState = null;
	private String siteSearchZip = null;
	private String siteSearchCountry = null;
	private String siteSearchGroup = null;
	private String siteSearchEndCustomer = null;
	private String siteSearchStatus = null;

	private String lines_per_page = null;
	private String current_page_no = null;
	private String total_page_size = null;
	private String org_page_no = null;
	private String org_lines_per_page = null;

	private String siteNameForColumn = null;
	private String siteNumberForColumn = null;
	private String siteAddressForColumn = null;
	private String siteCityForColumn = null;
	private String siteStateForColumn = null;
	private String siteZipForColumn = null;
	private String siteCountryForColumn = null;
	private String stateid = null;
	private String pageType = null;
	private String site_msaid = null;
	// Start : For PVS Search
	private String orgTopName = null;
	private String action = null;
	private String startdate = null;
	private String enddate = null;
	private String division = null;
	private String partnerType = null;
	// partnerSearch
	private String name = "";
	private String keyword = "";
	private String partnersearchType = "";
	private String mvsaVers = "";
	private String zip = "";
	private String radius = "";
	private String mainoffice = "";
	private String fieldOffice = "";
	private String techhi = "";
	private String rating = "";
	private String techinician = "";
	private String certification = "";
	private String corecomp = "";
	private String chekBox1 = "";
	private String chekBox2 = "";
	private String jobId = "";
	private String selectedPartnersList;
	private String coreCompetencyList;
	private String techniciansList;
	private String securityCertiftn;
	private String selectedCountryName;
	private String selectedCityName;

	public String getSelectedPartnersList() {
		return selectedPartnersList;
	}

	public void setSelectedPartnersList(String selectedPartnersList) {
		this.selectedPartnersList = selectedPartnersList;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getCertification() {
		return certification;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}

	public String getChekBox1() {
		return chekBox1;
	}

	public void setChekBox1(String chekBox1) {
		this.chekBox1 = chekBox1;
	}

	public String getChekBox2() {
		return chekBox2;
	}

	public void setChekBox2(String chekBox2) {
		this.chekBox2 = chekBox2;
	}

	public String getCorecomp() {
		return corecomp;
	}

	public void setCorecomp(String corecomp) {
		this.corecomp = corecomp;
	}

	public String getFieldOffice() {
		return fieldOffice;
	}

	public void setFieldOffice(String fieldOffice) {
		this.fieldOffice = fieldOffice;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getMainoffice() {
		return mainoffice;
	}

	public void setMainoffice(String mainoffice) {
		this.mainoffice = mainoffice;
	}

	public String getMvsaVers() {
		return mvsaVers;
	}

	public void setMvsaVers(String mvsaVers) {
		this.mvsaVers = mvsaVers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPartnersearchType() {
		return partnersearchType;
	}

	public void setPartnersearchType(String partnersearchType) {
		this.partnersearchType = partnersearchType;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getTechhi() {
		return techhi;
	}

	public void setTechhi(String techhi) {
		this.techhi = techhi;
	}

	public String getTechinician() {
		return techinician;
	}

	public void setTechinician(String techinician) {
		this.techinician = techinician;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	// End : For PVS Search
	public String getSite_msaid() {
		return site_msaid;
	}

	public void setSite_msaid(String site_msaid) {
		this.site_msaid = site_msaid;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getFrom_date() {
		return from_date;
	}

	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getPowo_number() {
		return powo_number;
	}

	public void setPowo_number(String powo_number) {
		this.powo_number = powo_number;
	}

	public String getTo_date() {
		return to_date;
	}

	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}

	public String[] getSelectedparam() {
		return selectedparam;
	}

	public String getSelectedparam(int i) {
		return selectedparam[i];
	}

	public void setSelectedparam(String[] selectedparam) {
		this.selectedparam = selectedparam;
	}

	public ArrayList getSortparam() {
		return sortparam;
	}

	public void setSortparam(ArrayList sortparam) {
		this.sortparam = sortparam;
	}

	public String getSortcriteria() {
		return sortcriteria;
	}

	public void setSortcriteria(String sortcriteria) {
		this.sortcriteria = sortcriteria;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(String id1) {
		this.id1 = id1;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getAddendum_id() {
		return addendum_id;
	}

	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}

	public String getCurrentstatus() {
		return currentstatus;
	}

	public void setCurrentstatus(String currentstatus) {
		this.currentstatus = currentstatus;
	}

	public String getNettype() {
		return nettype;
	}

	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	public String getRdofilter() {
		return rdofilter;
	}

	public void setRdofilter(String rdofilter) {
		this.rdofilter = rdofilter;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getSiteid() {
		return siteid;
	}

	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getRef1() {
		return ref1;
	}

	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getSitecity() {
		return sitecity;
	}

	public void setSitecity(String sitecity) {
		this.sitecity = sitecity;
	}

	public String getSitenumber() {
		return sitenumber;
	}

	public void setSitenumber(String sitenumber) {
		this.sitenumber = sitenumber;
	}

	public String getInvoice_Flag() {
		return invoice_Flag;
	}

	public void setInvoice_Flag(String invoice_Flag) {
		this.invoice_Flag = invoice_Flag;
	}

	public String getSiteSearchName() {
		return siteSearchName;
	}

	public void setSiteSearchName(String siteSearchName) {
		this.siteSearchName = siteSearchName;
	}

	public String getSiteSearchNumber() {
		return siteSearchNumber;
	}

	public void setSiteSearchNumber(String siteSearchNumber) {
		this.siteSearchNumber = siteSearchNumber;
	}

	public String getCurrent_page_no() {
		return current_page_no;
	}

	public void setCurrent_page_no(String current_page_no) {
		this.current_page_no = current_page_no;
	}

	public String getLines_per_page() {
		return lines_per_page;
	}

	public void setLines_per_page(String lines_per_page) {
		this.lines_per_page = lines_per_page;
	}

	public String getOrg_lines_per_page() {
		return org_lines_per_page;
	}

	public void setOrg_lines_per_page(String org_lines_per_page) {
		this.org_lines_per_page = org_lines_per_page;
	}

	public String getOrg_page_no() {
		return org_page_no;
	}

	public void setOrg_page_no(String org_page_no) {
		this.org_page_no = org_page_no;
	}

	public String getTotal_page_size() {
		return total_page_size;
	}

	public void setTotal_page_size(String total_page_size) {
		this.total_page_size = total_page_size;
	}

	public String getSiteAddressForColumn() {
		return siteAddressForColumn;
	}

	public void setSiteAddressForColumn(String siteAddressForColumn) {
		this.siteAddressForColumn = siteAddressForColumn;
	}

	public String getSiteCityForColumn() {
		return siteCityForColumn;
	}

	public void setSiteCityForColumn(String siteCityForColumn) {
		this.siteCityForColumn = siteCityForColumn;
	}

	public String getSiteCountryForColumn() {
		return siteCountryForColumn;
	}

	public void setSiteCountryForColumn(String siteCountryForColumn) {
		this.siteCountryForColumn = siteCountryForColumn;
	}

	public String getSiteNameForColumn() {
		return siteNameForColumn;
	}

	public void setSiteNameForColumn(String siteNameForColumn) {
		this.siteNameForColumn = siteNameForColumn;
	}

	public String getSiteNumberForColumn() {
		return siteNumberForColumn;
	}

	public void setSiteNumberForColumn(String siteNumberForColumn) {
		this.siteNumberForColumn = siteNumberForColumn;
	}

	public String getSiteStateForColumn() {
		return siteStateForColumn;
	}

	public void setSiteStateForColumn(String siteStateForColumn) {
		this.siteStateForColumn = siteStateForColumn;
	}

	public String getSiteZipForColumn() {
		return siteZipForColumn;
	}

	public void setSiteZipForColumn(String siteZipForColumn) {
		this.siteZipForColumn = siteZipForColumn;
	}

	public String getSiteSearchCity() {
		return siteSearchCity;
	}

	public void setSiteSearchCity(String siteSearchCity) {
		this.siteSearchCity = siteSearchCity;
	}

	public String getSiteSearchCountry() {
		return siteSearchCountry;
	}

	public void setSiteSearchCountry(String siteSearchCountry) {
		this.siteSearchCountry = siteSearchCountry;
	}

	public String getSiteSearchState() {
		return siteSearchState;
	}

	public void setSiteSearchState(String siteSearchState) {
		this.siteSearchState = siteSearchState;
	}

	public String getSiteSearchZip() {
		return siteSearchZip;
	}

	public void setSiteSearchZip(String siteSearchZip) {
		this.siteSearchZip = siteSearchZip;
	}

	public String getSiteSearchEndCustomer() {
		return siteSearchEndCustomer;
	}

	public void setSiteSearchEndCustomer(String siteSearchEndCustomer) {
		this.siteSearchEndCustomer = siteSearchEndCustomer;
	}

	public String getSiteSearchGroup() {
		return siteSearchGroup;
	}

	public void setSiteSearchGroup(String siteSearchGroup) {
		this.siteSearchGroup = siteSearchGroup;
	}

	public String getStateid() {
		return stateid;
	}

	public void setStateid(String stateid) {
		this.stateid = stateid;
	}

	public String getSiteSearchStatus() {
		return siteSearchStatus;
	}

	public void setSiteSearchStatus(String siteSearchStatus) {
		this.siteSearchStatus = siteSearchStatus;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getOrgTopName() {
		return orgTopName;
	}

	public void setOrgTopName(String orgTopName) {
		this.orgTopName = orgTopName;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCoreCompetencyList() {
		return coreCompetencyList;
	}

	public void setCoreCompetencyList(String coreCompetencyList) {
		this.coreCompetencyList = coreCompetencyList;
	}

	public String getTechniciansList() {
		return techniciansList;
	}

	public void setTechniciansList(String techniciansList) {
		this.techniciansList = techniciansList;
	}

	public String getSecurityCertiftn() {
		return securityCertiftn;
	}

	public void setSecurityCertiftn(String securityCertiftn) {
		this.securityCertiftn = securityCertiftn;
	}

	public String getSelectedCountryName() {
		return selectedCountryName;
	}

	public void setSelectedCountryName(String selectedCountryName) {
		this.selectedCountryName = selectedCountryName;
	}

	public String getSelectedCityName() {
		return selectedCityName;
	}

	public void setSelectedCityName(String selectedCityName) {
		this.selectedCityName = selectedCityName;
	}

}