package com.mind.common.formbean;

import java.io.File;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class EmailForm extends ActionForm {
	private String id1 = null;
	private String id2 = null; // appendix id
	private String id3 = null; // job id
	private String type = null;
	private String jobtype = null;

	private String type1 = null; // for IR, PO and WO
	private String order_type = null; // for PO and WO
	private String act_id = null; // for PO and WO
	private String powo_type = null; // for PO and WO
	private String po_wo_number = null; // used for po_wo_number for sending po
										// and wo through email
	private String powo_fromtype = null; // for PO and WO

	private String invtype1 = null; // for IR
	private String invtype2 = null; // for IR

	private ArrayList custPOC = null;

	private String mailto[] = null;
	private String to = null;

	private String fileformat = "PDF";

	private String from = null;
	private String fromPage = null;

	private String mailcc[] = null;
	private String cc = null;

	private String mailbcc[] = null;
	private String bcc = null;

	private String replyto = null;
	private String subject = null;
	private String content = null;
	private String contentSign = null;
	private String contentFromDB = null;
	
	private String attachment = null;

	private String smtpservername = null;
	private String smtpserverport = null;

	private String send = null;
	private String authenticate = "";

	private File file = null;
	private File file1 = null;
	private String filenameX = null;
	private String filename1 = null;

	private String check_for_fileupload = null;

	private String fromName = null;
	private String viewjobtype = null;
	private String ownerId = null;

	private String textmailcc = null;
	private String textmailaddto = null;
	private FormFile attachfilename = null;
	// for sending attachment
	private File file2 = null;
	private String filename2 = null;
	// for msa attachment with appendix
	private String filenameForMsa = null;
	private File fileForMsa = null;

	private String username = null;
	private String userpassword = null;

	// Start :Added By Amit

	private File fileA = null;
	private File fileB = null;

	private String filenameA = null;
	private String filenameB = null;

	private FormFile attachfilename1 = null;
	private FormFile attachfilename2 = null;

	private String temp_fu_name = null;
	private String temp_fu_type = null;
	private String messageContent = null;
	private String MSA_Id = null;
	private String msaname = null;
	private String jobId = null;
	private String powoId = null;
	private String name = null;
	private String formaldocdate = null;
	private String order = null;
	private String typeName = null;
	private String id = null;
	private String jobIds = null;
	private String temp_type1 = null;
	private String temp_ordertype = null;

	// New UI Changes Related to MSA ViewSelector and ViewImages.

	private String monthWeekCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;

	// New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;
	private FormFile filename = null;

	// For View Selector
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;

	// for mas inclue in appendix send email
	private String includeMsa = null;

	private String changeStatus = null;
	private String addendumFlag = null;
	private String tabId = null;
	private String isClicked = null;
	private String onlyEmail = null;

	private File[] documents = null;
	private String[] documentNames = null;

	private String[] attachPOWO = new String[2];
	private boolean mobileUploadAllowed;
	private boolean poDeliverableExist;

	// The below fields added for W9W9 blank file atatchment

	private File filePdW9W9 = null;
	private String fileNamePdW9W9 = null;

	public boolean isPoDeliverableExist() {
		return poDeliverableExist;
	}

	public void setPoDeliverableExist(boolean poDeliverableExist) {
		this.poDeliverableExist = poDeliverableExist;
	}

	public boolean isMobileUploadAllowed() {
		return mobileUploadAllowed;
	}

	public void setMobileUploadAllowed(boolean mobileUploadAllowed) {
		this.mobileUploadAllowed = mobileUploadAllowed;
	}

	public String[] getAttachPOWO() {
		return attachPOWO;
	}

	public void setAttachPOWO(String[] attachPOWO) {
		this.attachPOWO = attachPOWO;
	}

	public String getAttachPOWO(int i) {
		return attachPOWO[i];
	}

	public String getOnlyEmail() {
		return onlyEmail;
	}

	public void setOnlyEmail(String onlyEmail) {
		this.onlyEmail = onlyEmail;
	}

	public String getAddendumFlag() {
		return addendumFlag;
	}

	public void setAddendumFlag(String addendumFlag) {
		this.addendumFlag = addendumFlag;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}

	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}

	public String[] getDocumentNames() {
		return documentNames;
	}

	public String getDocumentNames(int i) {
		return documentNames[i];
	}

	public void setDocumentNames(String[] documentNames) {
		this.documentNames = documentNames;
	}

	public File[] getDocuments() {
		return documents;
	}

	public void setDocuments(File[] documents) {
		this.documents = documents;
	}

	public File getDocuments(int i) {
		return documents[i];
	}

	public String getJobSelectedStatus(int i) {
		return jobSelectedStatus[i];
	}

	public String getJobSelectedOwners(int i) {
		return jobSelectedOwners[i];
	}

	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	// End : Added By Amit

	public String getTemp_ordertype() {
		return temp_ordertype;
	}

	public void setTemp_ordertype(String temp_ordertype) {
		this.temp_ordertype = temp_ordertype;
	}

	public String getTemp_type1() {
		return temp_type1;
	}

	public void setTemp_type1(String temp_type1) {
		this.temp_type1 = temp_type1;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJobIds() {
		return jobIds;
	}

	public void setJobIds(String jobIds) {
		this.jobIds = jobIds;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getFormaldocdate() {
		return formaldocdate;
	}

	public void setFormaldocdate(String formaldocdate) {
		this.formaldocdate = formaldocdate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMSA_Id() {
		return MSA_Id;
	}

	public void setMSA_Id(String id) {
		MSA_Id = id;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getTemp_fu_type() {
		return temp_fu_type;
	}

	public void setTemp_fu_type(String temp_fu_type) {
		this.temp_fu_type = temp_fu_type;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(String id1) {
		this.id1 = id1;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(String id2) {
		this.id2 = id2;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList getCustPOC() {
		return custPOC;
	}

	public void setCustPOC(ArrayList custPOC) {
		this.custPOC = custPOC;
	}

	public String getFileformat() {
		return fileformat;
	}

	public void setFileformat(String fileformat) {
		this.fileformat = fileformat;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String[] getMailcc() {
		return mailcc;
	}

	public void setMailcc(String[] mailcc) {
		this.mailcc = mailcc;
	}

	public String getMailcc(int i) {
		return mailcc[i];
	}

	public String[] getMailbcc() {
		return mailbcc;
	}

	public void setMailbcc(String[] mailbcc) {
		this.mailbcc = mailbcc;
	}

	public String getMailbcc(int i) {
		return mailbcc[i];
	}

	public String getReplyto() {
		return replyto;
	}

	public void setReplyto(String replyto) {
		this.replyto = replyto;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getSmtpservername() {
		return smtpservername;
	}

	public void setSmtpservername(String smtpservername) {
		this.smtpservername = smtpservername;
	}

	public String[] getMailto() {
		return mailto;
	}

	public void setMailto(String[] mailto) {
		this.mailto = mailto;
	}

	public String getMailto(int i) {
		return mailto[i];
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSend() {
		return send;
	}

	public void setSend(String send) {
		this.send = send;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getCheck_for_fileupload() {
		return check_for_fileupload;
	}

	public void setCheck_for_fileupload(String check_for_fileupload) {
		this.check_for_fileupload = check_for_fileupload;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public FormFile getFilename() {
		return filename;
	}

	public void setFilename(FormFile filename) {
		this.filename = filename;
	}

	public String getFilenameX() {
		return filenameX;
	}

	public void setFilenameX(String filenameX) {
		this.filenameX = filenameX;
	}

	public String getType1() {
		return type1;
	}

	public void setType1(String type1) {
		this.type1 = type1;
	}

	public String getAct_id() {
		return act_id;
	}

	public void setAct_id(String act_id) {
		this.act_id = act_id;
	}

	public String getOrder_type() {
		return order_type;
	}

	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}

	public String getPowo_type() {
		return powo_type;
	}

	public void setPowo_type(String powo_type) {
		this.powo_type = powo_type;
	}

	public String getPowo_fromtype() {
		return powo_fromtype;
	}

	public void setPowo_fromtype(String powo_fromtype) {
		this.powo_fromtype = powo_fromtype;
	}

	public String getPo_wo_number() {
		return po_wo_number;
	}

	public void setPo_wo_number(String po_wo_number) {
		this.po_wo_number = po_wo_number;
	}

	public String getSmtpserverport() {
		return smtpserverport;
	}

	public void setSmtpserverport(String smtpserverport) {
		this.smtpserverport = smtpserverport;
	}

	public String getInvtype1() {
		return invtype1;
	}

	public void setInvtype1(String invtype1) {
		this.invtype1 = invtype1;
	}

	public String getInvtype2() {
		return invtype2;
	}

	public void setInvtype2(String invtype2) {
		this.invtype2 = invtype2;
	}

	public String getFilename1() {
		return filename1;
	}

	public void setFilename1(String filename1) {
		this.filename1 = filename1;
	}

	public File getFile1() {
		return file1;
	}

	public void setFile1(File file1) {
		this.file1 = file1;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getTextmailcc() {
		return textmailcc;
	}

	public void setTextmailcc(String textmailcc) {
		this.textmailcc = textmailcc;
	}

	public FormFile getAttachfilename() {
		return attachfilename;
	}

	public void setAttachfilename(FormFile attachfilename) {
		this.attachfilename = attachfilename;
	}

	public File getFile2() {
		return file2;
	}

	public void setFile2(File file2) {
		this.file2 = file2;
	}

	public String getFilename2() {
		return filename2;
	}

	public void setFilename2(String filename2) {
		this.filename2 = filename2;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserpassword() {
		return userpassword;
	}

	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public FormFile getAttachfilename1() {
		return attachfilename1;
	}

	public void setAttachfilename1(FormFile attachfilename1) {
		this.attachfilename1 = attachfilename1;
	}

	public FormFile getAttachfilename2() {
		return attachfilename2;
	}

	public void setAttachfilename2(FormFile attachfilename2) {
		this.attachfilename2 = attachfilename2;
	}

	public File getFileA() {
		return fileA;
	}

	public void setFileA(File fileA) {
		this.fileA = fileA;
	}

	public File getFileB() {
		return fileB;
	}

	public void setFileB(File fileB) {
		this.fileB = fileB;
	}

	public String getFilenameA() {
		return filenameA;
	}

	public void setFilenameA(String filenameA) {
		this.filenameA = filenameA;
	}

	public String getFilenameB() {
		return filenameB;
	}

	public void setFilenameB(String filenameB) {
		this.filenameB = filenameB;
	}

	public String getTextmailaddto() {
		return textmailaddto;
	}

	public void setTextmailaddto(String textmailaddto) {
		this.textmailaddto = textmailaddto;
	}

	public String getId3() {
		return id3;
	}

	public void setId3(String id3) {
		this.id3 = id3;
	}

	public String getTemp_fu_name() {
		return temp_fu_name;
	}

	public void setTemp_fu_name(String temp_fu_name) {
		this.temp_fu_name = temp_fu_name;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getPowoId() {
		return powoId;
	}

	public void setPowoId(String powoId) {
		this.powoId = powoId;
	}

	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}

	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}

	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}

	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}

	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}

	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getOtherCheck() {
		return otherCheck;
	}

	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String[] getSelectedOwners() {
		return selectedOwners;
	}

	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}

	public String[] getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public String getSelectedOwners(int i) {
		return selectedOwners[i];
	}

	public String getSelectedStatus(int i) {
		return selectedStatus[i];
	}

	public String getAppendixSelectedOwners(int i) {
		return appendixSelectedOwners[i];
	}

	public String getAppendixSelectedStatus(int i) {
		return appendixSelectedStatus[i];
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public File getFileForMsa() {
		return fileForMsa;
	}

	public void setFileForMsa(File fileForMsa) {
		this.fileForMsa = fileForMsa;
	}

	public String getFilenameForMsa() {
		return filenameForMsa;
	}

	public void setFilenameForMsa(String filenameForMsa) {
		this.filenameForMsa = filenameForMsa;
	}

	public String getIncludeMsa() {
		return includeMsa;
	}

	public void setIncludeMsa(String includeMsa) {
		this.includeMsa = includeMsa;
	}

	public String getJobtype() {
		return jobtype;
	}

	public void setJobtype(String jobtype) {
		this.jobtype = jobtype;
	}

	public String getChangeStatus() {
		return changeStatus;
	}

	public void setChangeStatus(String changeStatus) {
		this.changeStatus = changeStatus;
	}

	public String getIsClicked() {
		return isClicked;
	}

	public void setIsClicked(String isClicked) {
		this.isClicked = isClicked;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

public File getFilePdW9W9() {
		return filePdW9W9;
	}

	public void setFilePdW9W9(File filePdW9W9) {
		this.filePdW9W9 = filePdW9W9;
	}

	public String getFileNamePdW9W9() {
		return fileNamePdW9W9;
	}

	public void setFileNamePdW9W9(String fileNamePdW9W9) {
		this.fileNamePdW9W9 = fileNamePdW9W9;
	}

	public String getContentSign() {
		return contentSign;
	}

	public void setContentSign(String contentSign) {
		this.contentSign = contentSign;
	}
	
	public String getContentFromDB() {
		return contentFromDB;
	}

	public void setContentFromDB(String contentFromDB) {
		this.contentFromDB = contentFromDB;
	}

}