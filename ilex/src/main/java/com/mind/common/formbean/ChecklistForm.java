package com.mind.common.formbean;

import org.apache.struts.action.ActionForm;



public class ChecklistForm extends ActionForm
{
	private String[] check = null;
	private String appendix_Id = null;
	private String name = null;
	private String submit = null;
	private String back = null;
	//private String fromtype = null;
	private String authenticate = "";
	private String addmore = "";
	private String newrowsadded = "";
	private String intialchecklistsize = "";
	
	private String[] checklistid = null;
	//private String[] checklistname = null;
	private String[] itemname = null;
	//private String[] itemdesc = null;
	private String[] checklisttype = null;
	private String[] checklistidtype = null;
	private String viewjobtype = null;
	private String ownerId = null;
	
	private String[] groupname = null;
	private String[] option = null;
	
	
	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String[] getCheck() {
		return check;
	}

	public void setCheck( String[] check ) {
		this.check = check;
	}



	public String getCheck( int i ) {
		return check[i];
	}

	public String getAppendix_Id() {
		return appendix_Id;
	}

	public void setAppendix_Id( String appendix_Id ) {
		this.appendix_Id = appendix_Id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit( String submit) {
		this.submit = submit;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}
	

	public String getAddmore() {
		return addmore;
	}

	public void setAddmore(String addmore) {
		this.addmore = addmore;
	}

	public String getNewrowsadded() {
		return newrowsadded;
	}

	public void setNewrowsadded(String newrowsadded) {
		this.newrowsadded = newrowsadded;
	}

	public String[] getChecklistid() {
		return checklistid;
	}
	
	public String getChecklistid(int i) {
		return checklistid[i];
	}

	public void setChecklistid(String[] checklistid) {
		this.checklistid = checklistid;
	}

	public String[] getChecklistidtype() {
		return checklistidtype;
	}

	public String getChecklistidtype(int i) {
		return checklistidtype[i];
	}
		
	public void setChecklistidtype(String[] checklistidtype) {
		this.checklistidtype = checklistidtype;
	}

	/*public String[] getChecklistname() {
		return checklistname;
	}
	
	public String getChecklistname(int i) {
		return checklistname[i];
	}

	public void setChecklistname(String[] checklistname) {
		this.checklistname = checklistname;
	}*/

	public String[] getChecklisttype() {
		return checklisttype;
	}
	
	public String getChecklisttype(int i) {
		return checklisttype[i];
	}

	public void setChecklisttype(String[] checklisttype) {
		this.checklisttype = checklisttype;
	}

	/*public String[] getItemdesc() {
		return itemdesc;
	}
	
	public String getItemdesc(int i) {
		return itemdesc[i];
	}

	public void setItemdesc(String[] itemdesc) {
		this.itemdesc = itemdesc;
	}*/

	public String[] getItemname() {
		return itemname;
	}
	
	public String getItemname(int i) {
		return itemname[i];
	}

	public void setItemname(String[] itemname) {
		this.itemname = itemname;
	}

	public String getIntialchecklistsize() {
		return intialchecklistsize;
	}

	public void setIntialchecklistsize(String intialchecklistsize) {
		this.intialchecklistsize = intialchecklistsize;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String[] getGroupname() {
		return groupname;
	}
	
	public String getGroupname(int i) {
		return groupname[i];
	}
	
	public void setGroupname(String[] groupname) {
		this.groupname = groupname;
	}

	public String[] getOption() {
		return option;
	}
	
	public String getOption(int i) {
		return option[i];
	}
	public void setOption(String[] option) {
		this.option = option;
	}
	

}
