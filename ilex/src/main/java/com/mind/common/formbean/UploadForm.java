package com.mind.common.formbean;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class UploadForm extends ActionForm
{
	private FormFile filename = null;
	private String id1 = null;
	private String type = null;
	private String ref = null;
    private String ref1 = null;
	private String upload = null;
	private String addmore = null;
	private String remarks = null;
	private String formaldocdate = null;
	private String status = null;
	
	private String addendum_id = null;
	private ArrayList statuslist = null;
	
	private String appendix_Id = null;
	private String path = null;
	
	private String viewjobtype = null;
	private String ownerId = null;
	private String nettype = null;
	
	private String MSA_Id = null;
	private String msaname = null;
	private String appendixName = null;
	private String jobName = null;
	private String fromPage = null;
	
//	For the div which should appear in Every Form.
	
	private String monthWeekCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	
	
	
//	New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	
	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}

	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}

	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}

	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}

	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}

	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}
	
	public String getAppendixSelectedOwners( int i ) {
		return appendixSelectedOwners[i];
	}
	
	public String getAppendixSelectedStatus( int i ) {
		return appendixSelectedStatus[i];
	}
	

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getOtherCheck() {
		return otherCheck;
	}

	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String[] getSelectedOwners() {
		return selectedOwners;
	}

	public String getSelectedOwners( int i ) {
		return selectedOwners[i];
	}
	
	public String getSelectedStatus( int i ) {
		return selectedStatus[i];
	}
	
	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}

	public String[] getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getMSA_Id() {
		return MSA_Id;
	}

	public void setMSA_Id(String id) {
		MSA_Id = id;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getNettype() {
		return nettype;
	}

	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public void setFilename( FormFile file ) {
        filename = file;
		
    }
    
    public FormFile getFilename() 
	{
        return filename;
    }
	
	/*public FormFile getFilename( int i ) 
	{
        return filename[i];
    }*/
	
	

	
	public String getId1() {
		return id1;
	}

	public void setId1( String id1 ) {
		this.id1 = id1;
	}
	

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}
	
	
	public String getRef() {
		return ref;
	}

	public void setRef( String ref ) {
		this.ref = ref;
	}

	public String getUpload() {
		return upload;
	}

	public void setUpload( String upload ) {
		this.upload = upload;
	}

	public String getAddmore() {
		return addmore;
	}

	public void setAddmore( String addmore ) {
		this.addmore = addmore;
	}

	
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks( String remarks ) {
		this.remarks = remarks;
	}

	
	
	public String getFormaldocdate() {
		return formaldocdate;
	}

	public void setFormaldocdate( String formaldocdate ) {
		this.formaldocdate = formaldocdate;
	}

	
	
	public String getStatus() {
		return status;
	}

	public void setStatus( String status ) {
		this.status = status;
	}

	
	
	public ArrayList getStatuslist() {
		return statuslist;
	}

	public void setStatuslist( ArrayList statuslist ) {
		this.statuslist = statuslist;
	}

	public String getRef1() {
		return ref1;
	}

	public void setRef1( String ref1 ) {
		this.ref1 = ref1;
	}

	
	public String getAddendum_id() {
		return addendum_id;
	}

	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}

	public String getAppendix_Id() {
		return appendix_Id;
	}

	public void setAppendix_Id(String appendix_Id) {
		this.appendix_Id = appendix_Id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	
	

	
	
	
}

