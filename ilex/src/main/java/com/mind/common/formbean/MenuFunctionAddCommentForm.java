package com.mind.common.formbean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class MenuFunctionAddCommentForm extends ActionForm
{
	private String type = null;
	private String MSA_Id = null;
	private String appendix_Id = null;
	private String appendixName = null;
	private String job_Id = null;
	private String jobName = null;
	private String activity_Id = null;
	private String resource_Id = null;
	private String ref = null;
	private String comment = null;
	private String add = null;
	private String authenticate = "";
	private String addendum_id = null;
	private String name = null;
	private String activityName = null;
	
	private String fromflag = null;
	private String dashboardid = null;
	private String msaname = null;
	private String forwardId = null;
	
	//New UI Changes Related to MSA ViewSelector and ViewImages.
	
	private String monthWeekCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;
	
	private String from = null;
	private String jobid = null;
	private String resourceStatus = null;
	private String resourceStatusList = null;
	private String chkaddendum =  null;
	private String chkdetailactivity = null;
	
//	New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String   appendixOtherCheck = null;
	private String   viewjobtype = null;
	private String   ownerId = null;
	
	//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	
	
	
	
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getGo() {
		return go;
	}
	public void setGo(String go) {
		this.go = go;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}
	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}
	public String getOtherCheck() {
		return otherCheck;
	}
	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}
	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}
	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}
	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}
	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}
	public String[] getSelectedOwners() {
		return selectedOwners;
	}
	
	public String getSelectedOwners( int i ) {
		return selectedOwners[i];
	}
	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}
	public String[] getSelectedStatus() {
		return selectedStatus;
	}
	public String getSelectedStatus( int i ) {
		return selectedStatus[i];
	}
	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}
	public String getForwardId() {
		return forwardId;
	}
	public void setForwardId(String forwardId) {
		this.forwardId = forwardId;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getComment() {
		return comment;
	}
	public void setComment( String comment ) {
		this.comment = comment;
	}
	
	
	public String getAppendix_Id() {
		return appendix_Id;
	}
	
	public void setAppendix_Id( String appendix_Id ) {
		this.appendix_Id = appendix_Id;
	}
	
	
	public String getMSA_Id() {
		return MSA_Id;
	}
	public void setMSA_Id( String msa_Id ) {
		MSA_Id = msa_Id;
	}
	
	
	public String getJob_Id() {
		return job_Id;
	}
	public void setJob_Id( String job_Id ) {
		this.job_Id = job_Id;
	}
	
	
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	
	public String getType() {
		return type;
	}
	public void setType( String type ) {
		this.type = type;
	}
	
	public String getAdd() {
		return add;
	}
	public void setAdd( String add ) {
		this.add = add;
	}
	
	
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate( String authenticate ) {
		this.authenticate = authenticate;
	}
	
	
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	
	
	public String getResource_Id() {
		return resource_Id;
	}
	public void setResource_Id( String resource_Id ) {
		this.resource_Id = resource_Id;
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	public String getDashboardid() {
		return dashboardid;
	}
	public void setDashboardid(String dashboardid) {
		this.dashboardid = dashboardid;
	}
	public String getFromflag() {
		return fromflag;
	}
	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}
	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}
	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}
	public String getAppendixSelectedOwners( int i ) {
		return appendixSelectedOwners[i];
	}
	
	public String getAppendixSelectedStatus( int i ) {
		return appendixSelectedStatus[i];
	}
	
	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}
	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}
	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}
	public String getChkdetailactivity() {
		return chkdetailactivity;
	}
	public void setChkdetailactivity(String chkdetailactivity) {
		this.chkdetailactivity = chkdetailactivity;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getResourceStatus() {
		return resourceStatus;
	}
	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}
	public String getResourceStatusList() {
		return resourceStatusList;
	}
	public void setResourceStatusList(String resourceStatusList) {
		this.resourceStatusList = resourceStatusList;
	}
	
	
	
	
}
