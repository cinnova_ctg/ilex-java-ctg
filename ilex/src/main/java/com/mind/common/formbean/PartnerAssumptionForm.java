package com.mind.common.formbean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class PartnerAssumptionForm extends ActionForm
{
	private String pfCheck[] = null;
	private String pfAssumption[] = null;
	private String pfAssumption_Id[] = null;
	
	private String pfNewassumption_Id = null;
	private String pfNewassumption = null;
	
	private String pfType = null;
	private String pfId = null;
	
	private String pfRef = null;
	
	private String pfSubmit = null;
	
	private String pfAddendum_id = null;
	private String pfName = null;
	
	
	private String pfFromflag = null;
	private String pfDashboardid = null;
	
	public String getPfAddendum_id() {
		return pfAddendum_id;
	}
	public void setPfAddendum_id(String pfAddendum_id) {
		this.pfAddendum_id = pfAddendum_id;
	}
	public String[] getPfAssumption() {
		return pfAssumption;
	}
	public void setPfAssumption(String[] pfAssumption) {
		this.pfAssumption = pfAssumption;
	}
	public String[] getPfAssumption_Id() {
		return pfAssumption_Id;
	}
	public void setPfAssumption_Id(String[] pfAssumption_Id) {
		this.pfAssumption_Id = pfAssumption_Id;
	}
	public String[] getPfCheck() {
		return pfCheck;
	}
	public void setPfCheck(String[] pfCheck) {
		this.pfCheck = pfCheck;
	}
	public String getPfDashboardid() {
		return pfDashboardid;
	}
	public void setPfDashboardid(String pfDashboardid) {
		this.pfDashboardid = pfDashboardid;
	}
	public String getPfFromflag() {
		return pfFromflag;
	}
	public void setPfFromflag(String pfFromflag) {
		this.pfFromflag = pfFromflag;
	}
	public String getPfId() {
		return pfId;
	}
	public void setPfId(String pfId) {
		this.pfId = pfId;
	}
	public String getPfName() {
		return pfName;
	}
	public void setPfName(String pfName) {
		this.pfName = pfName;
	}
	public String getPfNewassumption() {
		return pfNewassumption;
	}
	public void setPfNewassumption(String pfNewassumption) {
		this.pfNewassumption = pfNewassumption;
	}
	public String getPfNewassumption_Id() {
		return pfNewassumption_Id;
	}
	public void setPfNewassumption_Id(String pfNewassumption_Id) {
		this.pfNewassumption_Id = pfNewassumption_Id;
	}
	public String getPfRef() {
		return pfRef;
	}
	public void setPfRef(String pfRef) {
		this.pfRef = pfRef;
	}
	public String getPfSubmit() {
		return pfSubmit;
	}
	public void setPfSubmit(String pfSubmit) {
		this.pfSubmit = pfSubmit;
	}
	public String getPfType() {
		return pfType;
	}
	public void setPfType(String pfType) {
		this.pfType = pfType;
	}
	
		
	public String getPfCheck( int i ) {
		return pfCheck[i];
	}
	
	
	public String getPfAssumption( int i ) {
		return pfAssumption[i];
	}
	
	public String getPfAssumption_Id( int i ) {
		return pfAssumption_Id[i];
	}
	
		
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		pfCheck = null;
		pfAssumption = null;
		pfAssumption_Id = null;
		
		pfNewassumption_Id = null;
		pfNewassumption = null;
		
		pfType = null;
		pfId = null;
		
		pfSubmit = null;
		pfRef = null;
		pfAddendum_id = null;
		pfName = null;
	}

}

