package com.mind.common.formbean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class SOWForm extends ActionForm
{
	private String check[] = null;
	private String sow[] = null;
	private String[] sow_Id = null;
	
	private String newsow_Id = null;
	private String newsow= null;
	
	private String type = null;
	private String id = null;
	
	private String ref = null;
	
	private String submitFrm = null;
	
	private String addendum_id = null;
	
	private String name = null;
	private String back = null;
	private String authenticate = "";
	
	private String fromflag = null;
	private String dashboardid = null;
	
	private String appindixName = null;
	private String jobName = null;
	private String jobId = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String activityName = null;	
	private String appendixName = null;
	private String AppendixId = null;
	private String msaName = null;
	private String msaId = null;
	private String activity_Id = null;
	private String appendixId = null;
	private String from = null;
	private String jobid = null;
	private String resourceStatus = null;
	private String resourceStatusList = null;
	private String chkaddendum =  null;
	private String chkdetailactivity = null;
	private String resourceListPage = null;
	private String resourceType = null;
	private String resourceListType = null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	
	
	
	
	
	

	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id(String activity_Id) {
		this.activity_Id = activity_Id;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getAppindixName() {
		return appindixName;
	}
	public void setAppindixName(String appindixName) {
		this.appindixName = appindixName;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String[] getCheck() {
		return check;
	}
	public void setCheck( String[] check ) {
		this.check = check;
	}
	public String getCheck( int i ) {
		return check[i];
	}
	
	
	public String[] getSow_Id() {
		return sow_Id;
	}
	public void setSow_Id(String[] sow_Id) {
		this.sow_Id = sow_Id;
	}
	public String getSow_Id( int i ) {
		return sow_Id[i];
	}
	
	
	public String[] getSow() {
		return sow;
	}
	public void setSow( String[] sow ) {
		this.sow = sow;
	}
	public String getSow( int i ) {
		return sow[i];
	}
	
	
	public String getNewsow_Id() {
		return newsow_Id;
	}
	public void setNewsow_Id( String newsow_Id ) {
		this.newsow_Id = newsow_Id;
	}
	

	public String getNewsow() {
		return newsow;
	}
	public void setNewsow( String newsow ) {
		this.newsow = newsow;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId( String id ) {
		this.id = id;
	}
	
	
	public String getType() {
		return type;
	}
	public void setType( String type ) {
		this.type = type;
	}
	
	
	public String getSubmitFrm() {
		return submitFrm;
	}
	public void setSubmitFrm( String submit ) {
		this.submitFrm = submit;
	}
	
	
	public String getRef() {
		return ref;
	}
	public void setRef( String ref ) {
		this.ref = ref;
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	
	
	
	public String getDashboardid() {
		return dashboardid;
	}
	public void setDashboardid(String dashboardid) {
		this.dashboardid = dashboardid;
	}
	public String getFromflag() {
		return fromflag;
	}
	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}
	public void reset( ActionMapping mapping , HttpServletRequest request ) 
	{
		check = null;
		sow = null;
		
		newsow_Id = null;
		newsow = null;
		
		type = null;
		id = null;
		ref = null;
		submitFrm = null;
		addendum_id = null;
		name = null;
		jobOwnerOtherCheck = null;
		jobSelectedStatus = null;
		jobSelectedOwners = null;
		jobMonthWeekCheck = null;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getAppendixId() {
		return AppendixId;
	}
	public void setAppendixId(String appendixId) {
		AppendixId = appendixId;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getResourceStatus() {
		return resourceStatus;
	}
	public void setResourceStatus(String materialStatus) {
		this.resourceStatus = materialStatus;
	}
	public String getResourceStatusList() {
		return resourceStatusList;
	}
	public void setResourceStatusList(String materialStatusList) {
		this.resourceStatusList = materialStatusList;
	}
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}
	public String getChkdetailactivity() {
		return chkdetailactivity;
	}
	public void setChkdetailactivity(String chkaddendumresource) {
		this.chkdetailactivity = chkaddendumresource;
	}
	public String getResourceListPage() {
		return resourceListPage;
	}
	public void setResourceListPage(String resourceListPage) {
		this.resourceListPage = resourceListPage;
	}
	public String getResourceListType() {
		return resourceListType;
	}
	public void setResourceListType(String resourceListType) {
		this.resourceListType = resourceListType;
	}
}

