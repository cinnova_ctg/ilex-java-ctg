package com.mind.common.formbean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ReportGenerationForm extends ActionForm{
	
	private String[] selectedMsa = null;
	private String selectedMonth = null;
	private String selectedYear  = null;
	private String generateReport = null;
	private String selectedReport = null;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		selectedMsa = null;
		selectedMonth = null;
		selectedYear  = null;
		generateReport = null;
		selectedReport = null;
	}

	public String getGenerateReport() {
		return generateReport;
	}

	public void setGenerateReport(String generateReport) {
		this.generateReport = generateReport;
	}

	public String getSelectedMonth() {
		return selectedMonth;
	}

	public void setSelectedMonth(String selectedMonth) {
		this.selectedMonth = selectedMonth;
	}

	public String getSelectedYear() {
		return selectedYear;
	}

	public void setSelectedYear(String selectedYear) {
		this.selectedYear = selectedYear;
	}

	public String getSelectedReport() {
		return selectedReport;
	}

	public void setSelectedReport(String selectedReport) {
		this.selectedReport = selectedReport;
	}

	public String[] getSelectedMsa() {
		return selectedMsa;
	}

	public String getSelectedMsa(int i) {
		return selectedMsa[i];
	}
		
	public void setSelectedMsa(String[] selectedMsa) {
		this.selectedMsa = selectedMsa;
	}
	
}
