package com.mind.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;
 
public class CharsetEncoding implements Filter
{
		/** The logger. */
		private static Logger logger = Logger.getLogger(CharsetEncoding.class);
 
        public void init(FilterConfig config) throws ServletException
        {
        	logger.debug(this.getClass().getName() + ": Starting filter.");
        }
 
        public void destroy()
        {
        	logger.debug(this.getClass().getName() + ": Destroying filter.");
        }
 
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                throws IOException, ServletException
        {
 
                request.setCharacterEncoding("UTF-8");
                response.setCharacterEncoding("UTF-8");
                response.setContentType("text/html; charset=UTF-8");
                chain.doFilter(request,response);
        }
}
