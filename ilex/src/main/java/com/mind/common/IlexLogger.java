package com.mind.common;

import org.apache.log4j.Logger;

public class IlexLogger {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IlexLogger.class);

	/*
	 * Logic for printing logs for Ilex/Ilex TestRelease/Ilex Dev.
	 */

	static boolean isPrint = false; // - True for Development(Ilex Local).
	static {
		try {
			String ilexLog = EnvironmentSelector.getBundleString("ilex.log");
			if (ilexLog != null && ilexLog.equalsIgnoreCase("true")) {
				isPrint = true;
			} // - End of If
		} catch (Exception e) {
			isPrint = false;
		} // - End of Catch block
	} // - End of Static

	/**
	 * Print logs on the basis of condition. Print all logs for development(Ilex
	 * Local). Print only Exception or Error log for Ilex TestRelease and Ilex
	 * Production.
	 * 
	 * @param obj
	 *            - Sring value of Print Statment.
	 */
	public static void printLog(Object obj) {
		try {
			/* Printing logs */
			if (isPrint) { // - For Ilex Local
				if (logger.isDebugEnabled()) {
					logger.debug("printLog(Object) - " + obj);
				}
			} else { // - For TestRelease/Ilex
				if (obj == null) {
					//
				} else if (obj instanceof Exception) {
					if (logger.isDebugEnabled()) {
						logger.debug("printLog(Object) - " + obj);
					}
				} else if (obj.toString().indexOf("Exception") > -1) {
					if (logger.isDebugEnabled()) {
						logger.debug("printLog(Object) - " + obj);
					}
				} else if (obj.toString().indexOf("exception") > -1) {
					if (logger.isDebugEnabled()) {
						logger.debug("printLog(Object) - " + obj);
					}
				} else if (obj.toString().indexOf("Error") > -1) {
					if (logger.isDebugEnabled()) {
						logger.debug("printLog(Object) - " + obj);
					}
				} else if (obj.toString().indexOf("error") > -1) {
					if (logger.isDebugEnabled()) {
						logger.debug("printLog(Object) - " + obj);
					}
				} else {

				}
			} // - End of Else
		} catch (Exception e) {
			logger.warn("printLog(Object) - exception ignored", e);
		}
	}

	/**
	 * For Testing only.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Exception e = new Exception();
		IlexLogger.printLog("exception " + e);
		IlexLogger.printLog(e);
		IlexLogger.printLog("error");
	}

}
