package com.mind.MPO.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MPO.dao.MPODao;
import com.mind.MPO.formbean.MPOBean;
import com.mind.bean.mpo.MPODTO;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.common.dao.PartnerSOWAssumptiondao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.docm.DynamicComboDocM;
import com.mind.docm.client.ClientOperator;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class MPOAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MPOAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Appendix",
				"Manage MPO", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */

		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		MPOBean mpoBean = (MPOBean) form;

		MPODao mpDao = new MPODao();
		mpoBean.setMp_appendix_id(((request.getParameter("appendixid") == null) ? mpoBean
				.getMp_appendix_id() : request.getParameter("appendixid")));

		mpoBean.setMsaId(IMdao.getMSAId(mpoBean.getMp_appendix_id(),
				getDataSource(request, "ilexnewDB")));
		mpoBean.setAppendixName(Jobdao.getAppendixname(
				getDataSource(request, "ilexnewDB"),
				mpoBean.getMp_appendix_id()));
		mpoBean.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"), mpoBean.getMsaId()));

		boolean isPm = mpDao.isPM(loginuserid, mpoBean.getMp_appendix_id());
		if (isPm == true) {
			request.setAttribute("PM", "Y");
			mpoBean.setIsPM("Y");
		}
		Map<String, Object> map = new HashMap<String, Object>();
		String tabId = request.getParameter("type");
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, mpoBean);
		} catch (IllegalAccessException e2) {
			logger.error(e2);
			e2.printStackTrace();
		} catch (InvocationTargetException e2) {
			logger.error(e2);
			e2.printStackTrace();
		}
		Map<String, String> paramMap;
		Map<String, Object> attrmap;
		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		mpDao.fetchDataForMPO(paramMap, map, attrmap, dto, tabId);
		try {
			BeanUtils.copyProperties(mpoBean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		setEntityNames(request.getParameter("appendixid"), request);
		if (!mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX")) {
			mpDao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
			try {
				BeanUtils.copyProperties(mpoBean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
		} else if (mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(
					mpoBean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		logger.trace("End :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		return (mapping.findForward("success"));
	}

	public ActionForward continuePressed(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		MPOBean mpoBean = (MPOBean) form;
		MPODao mpDao = new MPODao();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		Map<String, Object> sessionMap;
		Map<String, Object> map = new HashMap<String, Object>();
		sessionMap = WebUtil.copySessionToAttributeMap(request.getSession());
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, mpoBean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		boolean isCreated = mpDao.MPOCreation(sessionMap, map, dto);
		WebUtil.copyMapToRequest(request, map);
		if (isCreated) {
			String mpoId = mpDao.getMaxMPOId(Integer.parseInt(mpoBean
					.getMp_appendix_id()));
			mpoBean.setMpoId(mpoId);
			mpDao.getActivityList(dto, map);
			try {
				BeanUtils.copyProperties(mpoBean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			WebUtil.copyMapToRequest(request, map);

			request.setAttribute("isContinue", "continue");
			setEntityNames(request.getParameter("appendixid"), request);
		}
		request.setAttribute("tabId", "1");
		if (!mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX")) {
			mpDao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));

		} else if (mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(
					mpoBean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		logger.trace("End :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		return (mapping.findForward("success"));
	}

	public ActionForward Save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		MPOBean mpoBean = (MPOBean) form;
		MPODao mpDao = new MPODao();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String tabId = request.getParameter("type");
		Map<String, Object> sessionMap;
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		sessionMap = WebUtil.copySessionToAttributeMap(request.getSession());
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, mpoBean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		if (tabId.equalsIgnoreCase("1")) {
			request.setAttribute("tabId", "1");
			mpDao.MPODetailSave(sessionMap, map, attrmap, paramMap, dto,
					"dbo.lp_mpo_activity_and_resource", tabId);
			request.setAttribute("ActivityCreated", "Activity Saved.");
		} else if (tabId.equalsIgnoreCase("2")) {
			request.setAttribute("tabId", "2");
			mpDao.MPODetailSave(sessionMap, map, attrmap, paramMap, dto,
					"dbo.mp_purchase_order_detail", tabId);
			request.setAttribute("ActivityCreated", "Purchase order created");
		} else if (tabId.equalsIgnoreCase("3")) {
			request.setAttribute("tabId", "3");
			mpDao.MPODetailSave(sessionMap, map, attrmap, paramMap, dto,
					"dbo.mp_work_order_detail", tabId);
			request.setAttribute("ActivityCreated", "WorkOrder Saved.");
		} else if (tabId.equalsIgnoreCase("4")) {
			request.setAttribute("tabId", "4");
			mpDao.MPODetailSave(sessionMap, map, attrmap, paramMap, dto,
					"dbo.mp_document", tabId);

			ArrayList al = mpDao.featchDocListForMPO(new Integer(mpoBean
					.getMpoId()).intValue());
			request.setAttribute("docDetailPageList", al);
			request.setAttribute("Document", "Document List saved.");
		} else if (tabId.equalsIgnoreCase("5")) {
			request.setAttribute("tabId", "5");
			// mpoBean.setDevId(request.getParameter("devId"));
			mpDao.MPODetailSave(sessionMap, map, attrmap, paramMap, dto,
					"dbo.mp_purchase_order_deliverables", tabId);

			mpoBean.setDevMode("");
			request.setAttribute("ActivityCreated", "Deliverable Saved.");
		}
		WebUtil.copyMapToRequest(request, map);
		request.setAttribute("save", "save");
		request.setAttribute("isContinue", "continue");
		setEntityNames(request.getParameter("appendixid"), request);

		if (!mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			mpDao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));

		else if (mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(
					mpoBean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(mpoBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		logger.trace("End :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		return (mapping.findForward("success"));
	}

	public ActionForward fetchMPOInsertedData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		// String appendixId = request.getParameter("appendixId");
		MPOBean mpoBean = (MPOBean) form;
		MPODao mpDao = new MPODao();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String actionTaken = request.getParameter("actionTaken");
		String mpoId = request.getParameter("mpoId") != null ? request
				.getParameter("mpoId") : mpoBean.getMpoId();
		mpoBean.setMpoId(mpoId);
		// mpoBean.setMp_appendix_id(appendixId);
		String tabId = request.getParameter("type");
		// request.setAttribute("tabId", tabId);
		String clicked = request.getParameter("clicked");
		Map<String, Object> map = new HashMap<String, Object>();
		if (tabId.equalsIgnoreCase("5"))
			request.setAttribute("clicked", "clicked");
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, mpoBean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		Map<String, Object> attrmap;
		Map<String, String> paramMap;

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);

		WebUtil.copyMapToRequest(request, map);
		mpDao.fetchDataForMPO(paramMap, map, attrmap, dto, tabId);

		setEntityNames(request.getParameter("appendixid"), request);
		if (!mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			mpDao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(
					mpoBean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(mpoBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		return (mapping.findForward("success"));
	}

	public ActionForward fetchDataForMPO(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		// String appendixId = request.getParameter("appendixId");
		MPOBean mpoBean = (MPOBean) form;
		MPODao mpDao = new MPODao();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String mpoId = request.getParameter("mpoId") != null ? request
				.getParameter("mpoId") : mpoBean.getMpoId();
		mpoBean.setMpoId(mpoId);
		// mpoBean.setMp_appendix_id(appendixId);
		String type = request.getParameter("type");
		request.setAttribute("tabId", type);
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, mpoBean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		Map<String, Object> map = new HashMap<String, Object>();

		setEntityNames(request.getParameter("appendixid"), request);
		if (!mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			mpDao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));

		else if (mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(
					mpoBean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		try {
			BeanUtils.copyProperties(mpoBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return (mapping.findForward("success"));
	}

	public ActionForward AddNewMPO(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPODao mpDao = new MPODao();
		MPOBean mpoBean = (MPOBean) form;
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String type = request.getParameter("type");
		request.setAttribute("tabId", type);
		// setLibraryListCombo(request);
		setEntityNames(request.getParameter("appendixid"), request);
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, mpoBean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		if (!mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			mpDao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(
					mpoBean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		try {
			BeanUtils.copyProperties(mpoBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return (mapping.findForward("success"));
	}

	public void setLibraryListCombo(HttpServletRequest request) {
		DynamicComboDocM dynamicCombo = new DynamicComboDocM();
		// ArrayList libName = DocumentAddDao.getAllLibraryName();
		// dynamicCombo.setLibrarylist(libName);
		request.setAttribute("libNameCombo", dynamicCombo);
	}

	public ActionForward updateDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPODao mpDao = new MPODao();
		MPOBean mpoBean = (MPOBean) form;
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		mpoBean.setDoc_id(request.getParameter("docId"));
		if (logger.isDebugEnabled()) {
			logger.debug("updateDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - +++ bean.getMpoId()).intValue() ++ "
					+ mpoBean.getMpoId());
		}
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, mpoBean);
		} catch (IllegalAccessException e2) {
			logger.error(e2);
			e2.printStackTrace();
		} catch (InvocationTargetException e2) {
			logger.error(e2);
			e2.printStackTrace();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> sessionMap;
		sessionMap = WebUtil.copySessionToAttributeMap(request.getSession());
		mpDao.updateDocFormMPO(dto, sessionMap);
		Map<String, Object> attrmap;
		Map<String, String> paramMap;

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);

		mpDao.fetchDataForMPO(paramMap, map, attrmap, dto,
				request.getParameter("type"));
		try {
			BeanUtils.copyProperties(mpoBean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		ArrayList al = mpDao
				.featchDocListForMPO(new Integer(mpoBean.getMpoId()).intValue());
		request.setAttribute("docDetailPageList", al);
		request.setAttribute("tabId", request.getParameter("type"));
		setEntityNames(request.getParameter("appendixid"), request);
		if (!mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			mpDao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(
					mpoBean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		try {
			BeanUtils.copyProperties(mpoBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return (mapping.findForward("success"));
	}

	public ActionForward deleteDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPODao dao = new MPODao();
		MPOBean bean = (MPOBean) form;
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		bean.setDoc_id(request.getParameter("docId"));
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e2) {
			logger.error(e2);
			e2.printStackTrace();
		} catch (InvocationTargetException e2) {
			logger.error(e2);
			e2.printStackTrace();
		}
		dao.deleteDocFormMPO(dto);
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		Map<String, Object> map = new HashMap<String, Object>();

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);

		dao.fetchDataForMPO(paramMap, map, attrmap, dto,
				request.getParameter("type"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		ArrayList al = dao.featchDocListForMPO(new Integer(bean.getMpoId())
				.intValue());
		request.setAttribute("docDetailPageList", al);
		request.setAttribute("tabId", request.getParameter("type"));
		if (!bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			dao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(bean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		setEntityNames(request.getParameter("appendixid"), request);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);

		return (mapping.findForward("success"));
	}

	public ActionForward deleteDev(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPODao dao = new MPODao();
		MPOBean bean = (MPOBean) form;
		Map<String, Object> map = new HashMap<String, Object>();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		Map<String, Object> attrmap;
		Map<String, String> paramMap;

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e2) {
			logger.error(e2);
			e2.printStackTrace();
		} catch (InvocationTargetException e2) {
			logger.error(e2);
			e2.printStackTrace();
		}
		dao.deleteDev(paramMap, map, attrmap, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		bean.setDevId(request.getParameter("devId"));
		dto.setDevId(request.getParameter("devId"));
		setEntityNames(request.getParameter("appendixid"), request);
		if (!bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			dao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(bean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		return (mapping.findForward("success"));
	}

	public ActionForward approveMPO(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPODao dao = new MPODao();
		MPOBean bean = (MPOBean) form;
		EmailBean emailform = new EmailBean();
		StringBuffer accountContent = new StringBuffer();
		String loginUserName = (String) request.getSession().getAttribute(
				"username");
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		bean.setMpoId(request.getParameter("mpoId"));
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e2) {
			logger.error(e2);
			e2.printStackTrace();
		} catch (InvocationTargetException e2) {
			logger.error(e2);
			e2.printStackTrace();
		}
		String mpoName = dao.getMPOName(dto);
		String msaName = dao.getMsaName(bean.getMp_appendix_id());
		String appendixName = dao.getAppendixName(bean.getMp_appendix_id());
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> sessionMap;
		sessionMap = WebUtil.copySessionToAttributeMap(request.getSession());

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		String emailAddressTo = dao.approveMPO(sessionMap, paramMap, map,
				attrmap, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		String emailAddressFrom = dao.getFromAdd((String) request.getSession(
				true).getAttribute("userid"));

		// emailAddressTo = "pankaj.chaudhary@mind-infotech.com";
		if (emailAddressTo != null
				&& !emailAddressTo.trim().equalsIgnoreCase("")
				&& !bean.getIsPM().equals("Y")) {
			emailform.setFrom(emailAddressFrom);
			emailform.setTo(emailAddressTo);
			emailform.setUsername(this.getResources(request).getMessage(
					"common.Email.username"));
			emailform.setUserpassword(this.getResources(request).getMessage(
					"common.Email.userpassword"));
			// emailform.setSmtpservername( this.getResources( request
			// ).getMessage( "common.Email.smtpservername" ) );

			emailform.setSmtpservername(EnvironmentSelector
					.getBundleString("common.Email.smtpservername"));
			emailform.setSmtpserverport(this.getResources(request).getMessage(
					"common.Email.smtpserverport"));
			emailform.setSubject("Request for Approval : MPO, " + mpoName
					+ " under " + appendixName + " for " + msaName + " ");
			emailform.setCc("");
			emailform.setBcc("");
			emailform.setType("prm_appendix");
			accountContent
					.append("The purpose of this mail is to inform you that a new MPO "
							+ "has been created with title '"
							+ mpoName
							+ "' under appendix '"
							+ appendixName
							+ "' for '"
							+ msaName
							+ "'."
							+ " \n \nThis MPO needs your review and approval."
							+ "\nThanks," + "\n" + loginUserName + "");
			emailform.setContent(accountContent.toString());
			Email.Send(emailform, getDataSource(request, "ilexnewDB"));
		}
		setEntityNames(request.getParameter("appendixid"), request);
		if (!bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			dao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			map = DatabaseUtilityDao.setNetMedXMenu(bean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		return (mapping.findForward("success"));
	}

	public ActionForward mpoSearchResult(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		MPOBean bean = (MPOBean) form;
		MPODao dao = new MPODao();
		String type = request.getParameter("type");
		Map<String, Object> map = new HashMap<String, Object>();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		request.setAttribute("tabId", type);
		ArrayList al = null;
		if (logger.isDebugEnabled()) {
			logger.debug("mpoSearchResult(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - docIdList :: "
					+ request.getParameter("docIdList"));
		}
		if (logger.isDebugEnabled()) {
			logger.debug("mpoSearchResult(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - mpoId"
					+ new Integer(bean.getMpoId()).intValue());
		}
		ArrayList detailList = dao.featchDocListForMPO(new Integer(bean
				.getMpoId()).intValue());
		if (request.getParameter("through") != null) {
			bean.setDocIdList(request.getParameter("docIdList"));
			al = ClientOperator.getApprovedDocForMPO(bean.getDocIdList());
		} else {
			String docIdList = bean.getDocIdList();
			if (logger.isDebugEnabled()) {
				logger.debug("mpoSearchResult(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - docIdList :: "
						+ docIdList);
			}
			al = ClientOperator.getApprovedDocForMPO(docIdList.substring(1,
					docIdList.length()));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("mpoSearchResult(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - al :: "
					+ al);
		}
		request.setAttribute("docMList", al);
		request.setAttribute("docDetailPageList", detailList);
		setEntityNames(request.getParameter("appendixid"), request);
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		if (!bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			dao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (bean.getFromPage().trim().equalsIgnoreCase("NetMedX")) {
			map = DatabaseUtilityDao.setNetMedXMenu(bean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));

		}
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		return (mapping.findForward("success"));
	}

	public ActionForward deleteMPO(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPODao dao = new MPODao();
		MPOBean bean = (MPOBean) form;
		Map<String, Object> map = new HashMap<String, Object>();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		Map<String, Object> attrmap;
		Map<String, String> paramMap;

		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		bean.setMpoId(request.getParameter("mpoId"));

		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e2) {
			logger.error(e2);
			e2.printStackTrace();
		} catch (InvocationTargetException e2) {
			logger.error(e2);
			e2.printStackTrace();
		}
		dao.deleteMPO(paramMap, map, attrmap, dto, request.getParameter("type"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		bean.setDevId(request.getParameter("devId"));
		dto.setDevId(request.getParameter("devId"));
		setEntityNames(request.getParameter("appendixid"), request);
		if (!bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			dao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (bean.getFromPage().trim().equalsIgnoreCase("NetMedX")) {
			map = DatabaseUtilityDao.setNetMedXMenu(bean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));

		}
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		return (mapping.findForward("success"));
	}

	public ActionForward docManagement(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPODao dao = new MPODao();
		MPOBean mpoBean = (MPOBean) form;
		Map<String, Object> map = new HashMap<String, Object>();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String mpoId = request.getParameter("mpoId") != null ? request
				.getParameter("mpoId") : mpoBean.getMpoId();
		mpoBean.setMpoId(mpoId);
		ArrayList al = dao.featchDocListForMPO(new Integer(mpoBean.getMpoId())
				.intValue());
		request.setAttribute("docDetailPageList", al);
		String type = request.getParameter("type");
		request.setAttribute("tabId", type);
		setEntityNames(request.getParameter("appendixid"), request);
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, mpoBean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		if (!mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			dao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (mpoBean.getFromPage().trim().equalsIgnoreCase("NetMedX")) {
			map = DatabaseUtilityDao.setNetMedXMenu(
					mpoBean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));

		}
		WebUtil.copyMapToRequest(request, map);
		try {
			BeanUtils.copyProperties(mpoBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return (mapping.findForward("success"));
	}

	public ActionForward sowAssumption(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPOBean mpoBean = (MPOBean) form;
		MPODao mpDao = new MPODao();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String sow = "";
		String assumption = "";
		String resourcelist = "";
		String result = "";
		try {
			resourcelist = mpDao.getResouceIdByMPO(
					request.getParameter("mpoId"),
					getDataSource(request, "ilexnewDB"));
			String appendixId = request.getParameter("appendixId");
			// mpDao.fetchDataForMPO(request,mpoBean,request.getParameter("type"));
			// request.setAttribute("tabId",request.getParameter("type"));

			if (request.getParameter("check") != null
					&& request.getParameter("check").equals("partner")) {
				if (!resourcelist.equals("")) {
					sow = mpDao.getdefaultSOWAssumption(resourcelist,
							"prm_act", "sow", "partner",
							getDataSource(request, "ilexnewDB"));
					assumption = mpDao.getdefaultSOWAssumption(resourcelist,
							"prm_act", "assumption", "partner",
							getDataSource(request, "ilexnewDB"));
				}
				// mpoBean.setWoNOA("Statement of Work \n" + sow +
				// "\nAssumptions:\n" + assumption);
				result = "Statement of Work:\n\n" + sow
						+ "\n\nAssumptions:\n\n" + assumption;
			} else if (request.getParameter("check") != null
					&& request.getParameter("check").equals("specialIns")) {
				// mpoBean.setWoSI(PartnerSOWAssumptiondao.getDefaultPartnerSOWAssumption(appendixId,
				// "prm_app", "si", "partner", getDataSource( request ,
				// "ilexnewDB" )));
				result = PartnerSOWAssumptiondao
						.getDefaultPartnerSOWAssumption(appendixId, "prm_app",
								"si", "partner",
								getDataSource(request, "ilexnewDB"));
			} else if (request.getParameter("check") != null
					&& request.getParameter("check").equals("specialCondt")) {
				// mpoBean.setSpecial_condition(PartnerSOWAssumptiondao.getDefaultPartnerSOWAssumption(appendixId,
				// "prm_app", "sc", "partner", getDataSource( request ,
				// "ilexnewDB" )));
				result = PartnerSOWAssumptiondao
						.getDefaultPartnerSOWAssumption(appendixId, "prm_app",
								"sc", "partner",
								getDataSource(request, "ilexnewDB"));
				// request.setAttribute("tabId","2");
			}

			// System.out.println("Sow is : " + sow);
			// System.out.println("assumption is : " + assumption);
			// System.out.println("MPO Id :" + mpoBean.getMpoId());
			// System.out.println("AppendixId :" + mpoBean.getMp_appendix_id());

			response.setContentType("text/html");
			response.getWriter().write(result);
			return null;
		} catch (Exception e) {
			logger.error(
					"sowAssumption(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"sowAssumption(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		return null;
		// return mapping.findForward("success");
	}

	public void setEntityNames(String appId, HttpServletRequest request) {
		String msaName = "MSA";
		String appendixName = "Appendix";
		try {
			msaName = Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					IMdao.getMSAId(appId, getDataSource(request, "ilexnewDB")));
			appendixName = Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), appId);
		} catch (Exception e) {
			logger.warn(
					"setEntityNames(String, HttpServletRequest) - exception ignored",
					e);
		}
		if (msaName == null || msaName.equals(""))
			msaName = "MSA";
		if (appendixName == null || appendixName.equals(""))
			appendixName = "Appendix";
		request.setAttribute("msaName", msaName);
		request.setAttribute("appendixName", appendixName);

	}

	public ActionForward resetToDraft(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		MPODao dao = new MPODao();
		MPOBean bean = (MPOBean) form;
		Map<String, Object> map = new HashMap<String, Object>();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		bean.setMpoId(request.getParameter("mpoId"));
		bean.setMp_appendix_id(request.getParameter("appendixId"));
		String type = request.getParameter("type");
		request.setAttribute("tabId", type);
		Map<String, Object> attrmap;
		Map<String, String> paramMap;
		Map<String, Object> sessionMap = null;
		sessionMap = WebUtil.copySessionToAttributeMap(request.getSession());
		attrmap = WebUtil.copyRequestToAttributeMap(request);
		paramMap = WebUtil.copyRequestToParamMap(request);
		MPODTO dto = new MPODTO();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e3) {
			logger.error(e3);
			e3.printStackTrace();
		} catch (InvocationTargetException e3) {
			logger.error(e3);
			e3.printStackTrace();
		}
		dao.resetToDraft(paramMap, sessionMap, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e2) {
			logger.error(e2);
			e2.printStackTrace();
		} catch (InvocationTargetException e2) {
			logger.error(e2);
			e2.printStackTrace();
		}
		setEntityNames(request.getParameter("appendixId"), request);
		dao.fetchDataForMPO(paramMap, map, attrmap, dto,
				request.getParameter("type"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		if (!bean.getFromPage().trim().equalsIgnoreCase("NetMedX"))
			dao.setMenuForAppendix(map, dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
		else if (bean.getFromPage().trim().equalsIgnoreCase("NetMedX")) {
			map = DatabaseUtilityDao.setNetMedXMenu(bean.getMp_appendix_id(),
					getDataSource(request, "ilexnewDB"));

		}
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		return (mapping.findForward("success"));
	}
}