package com.mind.MPO.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class MPOBean extends ActionForm {

	String hmode = "";
	String mpoId = "";
	// Activity Resource items start.
	String mp_appendix_id = null;
	String jobId = null;
	String mpoName = "";
	String mpoItemType = "";
	String[] mpoActivityResource = null;
	String[] mpoActivityIsWOShow = null;
	String[] mpoActivityPlanQuantity = null;
	String[] mpoActivityPlanUnitCost = null;
	String[] mpoActResourceSubNamePre = null;
	String[] mpoActResourceSubName = null;
	String[] mpoActResourceSubId = null;
	String[] mpoTypeName = null;
	String[] mpoType = null;
	String[] mpoActId = null;
	String[] mpoJobId = null;
	String[] mpoAct = null;
	String[] mpoRes = null;
	String[] mpoUnit = null;
	String[] isShowHidden = null;
	String[] resourceHidden = null;
	String[] actResId = null;
	String estimatedCost = "";
	// Activity Resource end.

	// WorkOrder items start.
	String woNOA = "";
	String woSI = "";
	String[] woRE = null;
	ArrayList woToolsList = new ArrayList();
	String[] workOrderCheck = null;
	// WorkOrder items end.

	// Devlivrables items start.

	String devTitle = "";
	String devType = "";
	String devFormat = "";
	String devDescription = "";
	String devId = "";
	String devMode = "";
	// Devlivrables items end.

	// purchase order tab start

	String special_condition = "";
	// purchase order tab end

	String docmKeyword = null;

	String docIdList = null;
	String[] listOfDoc_Id = null;
	String[] include_with_po = null;
	String[] include_with_wo = null;
	String[] document_id = null;

	String mpo_doc_id = "";
	String doc_id = "";
	String in_with_po = "";
	String in_with_wo = "";
	String mpoStatus;
	// BreadCrum
	private String msaId = null;
	private String msaName = null;
	private String appendixName = null;
	private String appendixType = null;
	private String fromPage = null;
	// Sow/Assumptions
	private String defaultPartnerSowAssumption = null;
	private String defaultCustomerSowAssumption = null;

	private String isPM = "";

	private String mobileAllowed = "";

	public String getDefaultCustomerSowAssumption() {
		return defaultCustomerSowAssumption;
	}

	public void setDefaultCustomerSowAssumption(
			String defaultCustomerSowAssumption) {
		this.defaultCustomerSowAssumption = defaultCustomerSowAssumption;
	}

	public String getDefaultPartnerSowAssumption() {
		return defaultPartnerSowAssumption;
	}

	public void setDefaultPartnerSowAssumption(
			String defaultPartnerSowAssumption) {
		this.defaultPartnerSowAssumption = defaultPartnerSowAssumption;
	}

	public String[] getDocument_id() {
		return document_id;
	}

	public void setDocument_id(String[] document_id) {
		this.document_id = document_id;
	}

	public String[] getInclude_with_po() {
		return include_with_po;
	}

	public void setInclude_with_po(String[] include_with_po) {
		this.include_with_po = include_with_po;
	}

	public String[] getInclude_with_wo() {
		return include_with_wo;
	}

	public void setInclude_with_wo(String[] include_with_wo) {
		this.include_with_wo = include_with_wo;
	}

	public String[] getListOfDoc_Id() {
		return listOfDoc_Id;
	}

	public void setListOfDoc_Id(String[] listOfDoc_Id) {
		this.listOfDoc_Id = listOfDoc_Id;
	}

	public String getDocIdList() {
		return docIdList;
	}

	public void setDocIdList(String docIdList) {
		this.docIdList = docIdList;
	}

	public String getHmode() {
		return hmode;
	}

	public void setHmode(String hmode) {
		this.hmode = hmode;
	}

	public String getDevDescription() {
		return devDescription;
	}

	public void setDevDescription(String devDescription) {
		this.devDescription = devDescription;
	}

	public String getDevFormat() {
		return devFormat;
	}

	public void setDevFormat(String devFormat) {
		this.devFormat = devFormat;
	}

	public String getDevTitle() {
		return devTitle;
	}

	public void setDevTitle(String devTitle) {
		this.devTitle = devTitle;
	}

	public String getDevType() {
		return devType;
	}

	public void setDevType(String devType) {
		this.devType = devType;
	}

	public String getWoNOA() {
		return woNOA;
	}

	public void setWoNOA(String woNOA) {
		this.woNOA = woNOA;
	}

	public String getWoSI() {
		return woSI;
	}

	public void setWoSI(String woSI) {
		this.woSI = woSI;
	}

	public String[] getMpoActivityIsWOShow() {
		return mpoActivityIsWOShow;
	}

	public void setMpoActivityIsWOShow(String[] mpoActivityIsWOShow) {
		this.mpoActivityIsWOShow = mpoActivityIsWOShow;
	}

	public String[] getMpoActivityPlanQuantity() {
		return mpoActivityPlanQuantity;
	}

	public void setMpoActivityPlanQuantity(String[] mpoActivityPlanQuantity) {
		this.mpoActivityPlanQuantity = mpoActivityPlanQuantity;
	}

	public String[] getMpoActivityPlanUnitCost() {
		return mpoActivityPlanUnitCost;
	}

	public void setMpoActivityPlanUnitCost(String[] mpoActivityPlanUnitCost) {
		this.mpoActivityPlanUnitCost = mpoActivityPlanUnitCost;
	}

	public String[] getMpoActivityResource() {
		return mpoActivityResource;
	}

	public void setMpoActivityResource(String[] mpoActivityResource) {
		this.mpoActivityResource = mpoActivityResource;
	}

	public String getMpoItemType() {
		return mpoItemType;
	}

	public void setMpoItemType(String mpoItemType) {
		this.mpoItemType = mpoItemType;
	}

	public String getMpoName() {
		return mpoName;
	}

	public void setMpoName(String mpoName) {
		this.mpoName = mpoName;
	}

	public String[] getWoRE() {
		return woRE;
	}

	public void setWoRE(String[] woRE) {
		this.woRE = woRE;
	}

	public String getMp_appendix_id() {
		return mp_appendix_id;
	}

	public void setMp_appendix_id(String mp_appendix_id) {
		this.mp_appendix_id = mp_appendix_id;
	}

	public String getMpoId() {
		return mpoId;
	}

	public void setMpoId(String mpoId) {
		this.mpoId = mpoId;
	}

	public String getDevId() {
		return devId;
	}

	public void setDevId(String devId) {
		this.devId = devId;
	}

	public ArrayList getWoToolsList() {
		return woToolsList;
	}

	public void setWoToolsList(ArrayList woToolsList) {
		this.woToolsList = woToolsList;
	}

	public String getDevMode() {
		return devMode;
	}

	public void setDevMode(String devMode) {
		this.devMode = devMode;
	}

	public String getSpecial_condition() {
		return special_condition;
	}

	public void setSpecial_condition(String special_condition) {
		this.special_condition = special_condition;
	}

	public String getDocmKeyword() {
		return docmKeyword;
	}

	public void setDocmKeyword(String docmKeyword) {
		this.docmKeyword = docmKeyword;
	}

	public String[] getMpoActResourceSubId() {
		return mpoActResourceSubId;
	}

	public void setMpoActResourceSubId(String[] mpoActResourceSubId) {
		this.mpoActResourceSubId = mpoActResourceSubId;
	}

	public String[] getMpoActResourceSubName() {
		return mpoActResourceSubName;
	}

	public void setMpoActResourceSubName(String[] mpoActResourceSubName) {
		this.mpoActResourceSubName = mpoActResourceSubName;
	}

	public String[] getMpoTypeName() {
		return mpoTypeName;
	}

	public void setMpoTypeName(String[] mpoTypeName) {
		this.mpoTypeName = mpoTypeName;
	}

	public String[] getMpoActId() {
		return mpoActId;
	}

	public void setMpoActId(String[] mpoActId) {
		this.mpoActId = mpoActId;
	}

	public String[] getMpoJobId() {
		return mpoJobId;
	}

	public void setMpoJobId(String[] mpoJobId) {
		this.mpoJobId = mpoJobId;
	}

	public String[] getMpoType() {
		return mpoType;
	}

	public void setMpoType(String[] mpoType) {
		this.mpoType = mpoType;
	}

	public String[] getMpoAct() {
		return mpoAct;
	}

	public void setMpoAct(String[] mpoAct) {
		this.mpoAct = mpoAct;
	}

	public String[] getMpoRes() {
		return mpoRes;
	}

	public void setMpoRes(String[] mpoRes) {
		this.mpoRes = mpoRes;
	}

	public String[] getMpoUnit() {
		return mpoUnit;
	}

	public void setMpoUnit(String[] mpoUnit) {
		this.mpoUnit = mpoUnit;
	}

	public String[] getIsShowHidden() {
		return isShowHidden;
	}

	public void setIsShowHidden(String[] isShowHidden) {
		this.isShowHidden = isShowHidden;
	}

	public String[] getResourceHidden() {
		return resourceHidden;
	}

	public void setResourceHidden(String[] resourceHidden) {
		this.resourceHidden = resourceHidden;
	}

	public String[] getActResId() {
		return actResId;
	}

	public void setActResId(String[] actResId) {
		this.actResId = actResId;
	}

	public String[] getWorkOrderCheck() {
		return workOrderCheck;
	}

	public void setWorkOrderCheck(String[] workOrderCheck) {
		this.workOrderCheck = workOrderCheck;
	}

	public String getDoc_id() {
		return doc_id;
	}

	public void setDoc_id(String doc_id) {
		this.doc_id = doc_id;
	}

	public String getIn_with_po() {
		return in_with_po;
	}

	public void setIn_with_po(String in_with_po) {
		this.in_with_po = in_with_po;
	}

	public String getIn_with_wo() {
		return in_with_wo;
	}

	public void setIn_with_wo(String in_with_wo) {
		this.in_with_wo = in_with_wo;
	}

	public String getMpo_doc_id() {
		return mpo_doc_id;
	}

	public void setMpo_doc_id(String mpo_doc_id) {
		this.mpo_doc_id = mpo_doc_id;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(String estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public String getIsPM() {
		return isPM;
	}

	public void setIsPM(String isPM) {
		this.isPM = isPM;
	}

	public String getAppendixType() {
		return appendixType;
	}

	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getMpoStatus() {
		return mpoStatus;
	}

	public void setMpoStatus(String mpoStatus) {
		this.mpoStatus = mpoStatus;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String[] getMpoActResourceSubNamePre() {
		return mpoActResourceSubNamePre;
	}

	public void setMpoActResourceSubNamePre(String[] mpoActResourceSubNamePre) {
		this.mpoActResourceSubNamePre = mpoActResourceSubNamePre;
	}

	public String getMobileAllowed() {
		return mobileAllowed;
	}

	public void setMobileAllowed(String mobileAllowed) {
		this.mobileAllowed = mobileAllowed;
	}
}
