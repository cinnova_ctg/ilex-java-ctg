package com.mind.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.docm.DocumentType;
import com.mind.common.IlexConstants;
import com.mind.common.bean.Fileinfo;
import com.mind.common.dao.Upload;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.dao.TransferDocDao;

public class DocUtil implements Runnable {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TransferDocDao.class);

	/** The type. */
	private String type = null;

	// For MSA/Appendix/Addendum

	/** The image path. */
	private String imagePath = null;

	/** The font path. */
	private String fontPath = null;

	/** The html image path. */
	private String htmlImagePath = null;

	/** The orginial type. */
	private String orginialType = null;

	// For POWO
	/** The image path1. */
	private String imagePath1 = null;

	/** The WO nw config img. */
	private String WONwConfigImg = null;

	/** The WO nw config imgheader. */
	private String WONwConfigImgheader = null;

	/** The ic chrysler. */
	private String icChrysler = null;

	/** The ic snap on. */
	private String icSnapOn = null;

	/** The ic compu com. */
	private String icCompuCom = null;

	/** The image bullet path. */
	private String imageBulletPath = null;

	// Exception Cases
	/** The count. */
	int count = 0;

	/** The Doc m. */
	int DocM = 0;

	/** The buffer exception. */
	int bufferException = 0;

	/** The count of document transfered. */
	int countOfDocumentTransfered = 0;

	/** The file not found exception. */
	int fileNotFoundException = 0;

	/** The document uploaded. */
	int documentUploaded = 0;

	/** The xml modified. */
	int xmlModified = 0;

	/** The document superseded. */
	int documentSuperseded = 0;

	// DataSource
	/** The ds. */
	private DataSource ds = null;

	/**
	 * Instantiates a new transfer doc dao.
	 * 
	 * @param type
	 *            the type
	 * @param ds
	 *            the ds
	 */
	public DocUtil(String type, DataSource ds) {
		this.type = type;
		this.ds = ds;
	}

	/***
	 * Method :method used to call the DocM interface
	 * 
	 * @param transferDocDao
	 * 
	 * @param DocumentType
	 */

	public synchronized void callToDocM(DocumentType documenttype,
			boolean flag, TransferDocDao transferDocDao) {
		String Id = null;
		try {
			if (orginialType.equalsIgnoreCase("Addendum")) {
				Id = EntityManager.approveEntity(documenttype.getJobId(),
						orginialType, documenttype.getBuffer(),
						documenttype.getFileName());
			} else {
				Id = EntityManager.approveEntity(documenttype.getId(),
						documenttype.getType(), documenttype.getBuffer(),
						documenttype.getFileName());
			}
			if (orginialType.equalsIgnoreCase("Addendum")) {
				transferDocDao.logSuccessMessage(documenttype.getJobId(),
						orginialType, Id);
			} else {
				transferDocDao.logSuccessMessage(documenttype.getId(),
						orginialType, Id);
			}

			if (flag)
				countOfDocumentTransfered++;
			else
				documentSuperseded++;

		} catch (Exception e) {
			DocM++;
			logger.error("callToDocM(DocumentType, boolean)", e);
		}
	}

	/***
	 * Method :method used to get the byte buffer for particular entity.
	 * 
	 * @param Id
	 * @param jobId
	 * @param Type
	 * @param fromType
	 * @param DataSource
	 * @return byte[]
	 */
	public synchronized byte[] getbuffer(String Id, String jobId, String Type,
			String fromType, DataSource ds) {
		byte[] buffer = null;
		buffer = getByteByType(Id, jobId, Type, fromType, ds);
		return buffer;
	}

	/***
	 * Method :method used to set the variables used for generating pdf.
	 * 
	 * @param Type
	 * @param HttpServletRequest
	 */

	public static synchronized void setVariables(String type,
			DocUtil transferDocuments) {
		transferDocuments.orginialType = type;
		transferDocuments.imagePath = IlexConstants.IMG_REAL_PATH
				+ "/clogo.jpg";
		transferDocuments.fontPath = IlexConstants.IMG_REAL_PATH
				+ "/ARIALN.TTF";
		String msaContPath = IlexConstants.ILEX_REQ_URL;
		msaContPath = msaContPath.substring(0, msaContPath.lastIndexOf("/"));
		transferDocuments.htmlImagePath = msaContPath + "/images/clogo.jpg";

		// For POWO
		transferDocuments.imagePath1 = IlexConstants.IMG_REAL_PATH
				+ "/checkbox.gif";
		transferDocuments.WONwConfigImg = IlexConstants.IMG_REAL_PATH
				+ "/witechfooter.jpg";
		transferDocuments.WONwConfigImgheader = IlexConstants.IMG_REAL_PATH
				+ "/witechheader.jpg";
		transferDocuments.icChrysler = IlexConstants.IMG_REAL_PATH
				+ "/chrysler.jpg";
		transferDocuments.icSnapOn = IlexConstants.IMG_REAL_PATH + "/snap.jpg";
		transferDocuments.icCompuCom = IlexConstants.IMG_REAL_PATH
				+ "/compu.jpg";
		transferDocuments.imageBulletPath = IlexConstants.IMG_REAL_PATH
				+ "/Bullet.gif";

		// For Fresh Cycle of Document Transfer
		transferDocuments.count = 0;
		transferDocuments.DocM = 0;
		transferDocuments.bufferException = 0;
		transferDocuments.countOfDocumentTransfered = 0;
		transferDocuments.fileNotFoundException = 0;
		transferDocuments.documentUploaded = 0;
		transferDocuments.xmlModified = 0;
		transferDocuments.documentSuperseded = 0;
	}

	/***
	 * Method :method used to check whether the modified xml file for particular
	 * entity exists or not .
	 * 
	 * @param Id
	 * @param Type
	 * @param DataSource
	 */

	public synchronized boolean fileExistsOrNot(String Id, String Type,
			DataSource ds) {
		try {
			String chkfiletype = EditXmldao.getChkXmlFile(Id, Type, ds);
			String newFilePath = ViewAction.getFilePath(Type, Id, chkfiletype);
			File file = new File(newFilePath);
			boolean exists = file.exists();
			if (!exists) {
				fileNotFoundException++;
				return false;
			} else {
				return true;
			}

		} catch (Exception e) {
			logger.error("fileExistsOrNot(String, String, DataSource)", e);
		}
		return true;
	}

	/***
	 * Method :method used to get the byte buffer according to the type
	 * 
	 * @param Id
	 * @param jobId
	 * @param Type
	 * @param fromType
	 * @param DataSource
	 */

	public synchronized byte[] getByteByType(String Id, String jobId,
			String Type, String fromType, DataSource ds) {
		byte[] buffer = null;
		POPdf po = new POPdf();
		WOPdf wo = new WOPdf();
		try {
			if (Type.equalsIgnoreCase("PO") || Type.equalsIgnoreCase("WO")) {
				if (Type.equalsIgnoreCase("PO"))
					buffer = po.viewPO(Id, "PoFinal", imagePath, fontPath, "1",
							ds, imageBulletPath);
				else if (Type.equalsIgnoreCase("WO"))
					buffer = wo.viewwo(Id, imagePath, imagePath1, fontPath,
							"1", ds, WONwConfigImg, WONwConfigImgheader,
							icSnapOn, icChrysler, icCompuCom);
			} else {
				buffer = ViewAction.getInnerDocumentBytesForTransfer(Id, jobId,
						Type, "pdf", "1", fromType, imagePath, fontPath,
						htmlImagePath, ds);
			}
			if (buffer == null)
				bufferException++;

			return buffer;

		} catch (Exception e) {
			bufferException++;
			logger.error(
					"getByteByType(String, String, String, String, DataSource)",
					e);
		}
		return buffer;
	}

	/***
	 * Method :method used to print the result
	 */

	public synchronized void documentResult() {
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - *******************************************");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - Document Details :");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - Document Type :" + orginialType);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - Total         :" + count);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - Uploaded      :"
					+ documentUploaded);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - xmlModified   :" + xmlModified);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - Transfered    :"
					+ countOfDocumentTransfered);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - Superseded    :"
					+ documentSuperseded);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - BufferError   :" + bufferException);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - DocMError     :" + DocM);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - FileNotFound  :"
					+ fileNotFoundException);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("documentResult() - *******************************************");
		}
	}

	/***
	 * Method :this method iterates over the list and call is made to DocM
	 * interface to insert documents.
	 * 
	 * @param transferDocDao
	 * 
	 * @param ArrayList
	 * @param DataSource
	 *            ds
	 */

	public synchronized void IterateAndCallDocM(ArrayList list, DataSource ds,
			TransferDocDao transferDocDao) {
		Iterator it = list.iterator();
		while (it.hasNext()) {
			DocumentType documents = (DocumentType) it.next();
			documents.setBuffer(getbuffer(documents.getId(),
					documents.getJobId(), documents.getType(),
					documents.getFromType(), ds));

			if (entityCheckForPOWO(documents.getType()))
				POWODetaildao.updateEmailPOWO(documents.getId(),
						documents.getType(), "1", ds);

			callToDocM(documents, true, transferDocDao);

			if (!entityCheckForPOWO(documents.getType()))
				makeEntitySuperseded(documents, ds, transferDocDao);
		}
		documentResult(); /* This would print the result of one entity Iteration */
		transferDocDao.setDocumentSummary();
	}

	/**
	 * @name checkEmptyBuffer
	 * @purpose method to check whether buffer is empty or not
	 * @param buffer
	 * @return boolean
	 */
	public synchronized boolean checkEmptyBuffer(byte[] buffer) {
		if (buffer != null)
			return true;
		else
			return false;
	}

	/**
	 * @name entityCheckForPOWO
	 * @purpose method used to check if the entity is of type po/wo
	 * @param type
	 * @return boolean
	 */
	public synchronized boolean entityCheckForPOWO(String type) {
		if (type != null
				&& (type.trim().equalsIgnoreCase("PO") || type.trim()
						.equalsIgnoreCase("WO")))
			return true;
		else
			return false;
	}

	/***
	 * Method : used to iterate over the cancelled entities and call the method
	 * updating for updatig the status to 'Superseded' which have status
	 * cancelled in Ilex
	 * 
	 * @param transferDocDao
	 * 
	 * @param ArrayList
	 */

	public synchronized void iterateAndUpdate(ArrayList list,
			TransferDocDao transferDocDao) {
		Iterator it = list.iterator();

		while (it.hasNext()) {
			DocumentType document = (DocumentType) it.next();
			transferDocDao.updating(document.getId(), document.getDocId(),
					document.getType());
		}
	}

	/**
	 * @name makeEntitySuperseded
	 * @purpose method is used to make the entity superseded if uploaded
	 *          manually or xml is modified and file does exists
	 * @param documents
	 * @param ds
	 * @param transferDocDao
	 */
	public synchronized void makeEntitySuperseded(DocumentType documents,
			DataSource ds, TransferDocDao transferDocDao) {
		byte[] buffer = null;
		if (documents.getIsXmlModified() != null
				&& documents.getIsXmlModified().trim().equalsIgnoreCase("U")) {
			documentUploaded++;
			Fileinfo fileinfo = Upload.getuploadedfiledata(documents.getId(),
					documents.getType(), "U", ds);
			buffer = fileinfo.getFile_data();
			documents.setBuffer(buffer);
			// manual upload simulation
			documents.setFileName(fileinfo.getFile_name());
			if (checkEmptyBuffer(buffer)) {
				try {
					EntityManager.setEntityToDraft(documents.getId(),
							orginialType);
					callToDocM(documents, false, transferDocDao);
				} catch (Exception e) {
					logger.error(
							"makeEntitySuperseded(DocumentType, DataSource)", e);
					logger.error(
							"makeEntitySuperseded(DocumentType, DataSource)", e);
				}
			}
		} else if (documents.getIsXmlModified() != null
				&& documents.getIsXmlModified().trim().equalsIgnoreCase("X")) {
			xmlModified++;
			if (fileExistsOrNot(documents.getId(), documents.getType(), ds)) {
				// automatic upload simulation
				buffer = ViewAction.getInnerDocumentBytes(documents.getId(),
						documents.getJobId(), documents.getType(), "pdf", "1",
						documents.getFromType(), imagePath, fontPath,
						htmlImagePath, ds, null);
				documents.setBuffer(buffer);
				if (checkEmptyBuffer(buffer)) {
					try {
						EntityManager.setEntityToDraft(documents.getId(),
								orginialType);
						callToDocM(documents, false, transferDocDao);
					} catch (Exception e) {
						logger.error(
								"makeEntitySuperseded(DocumentType, DataSource)",
								e);
					}
				}
			}
		}
	}

	/***
	 * Method : method is called from Ilex interface to run the transfer of
	 * documents to DocM in background
	 */

	public void run() {
		ArrayList list = new ArrayList();
		ArrayList cacelledList = new ArrayList();
		TransferDocDao transferDocDao = new TransferDocDao();
		list = transferDocDao.moveDocumentsList(this.type);
		IterateAndCallDocM(list, this.ds, transferDocDao);
		if (!entityCheckForPOWO(this.type)) {
			cacelledList = transferDocDao.getCancelledEntites(this.type);
			iterateAndUpdate(cacelledList, transferDocDao);
		}
	}
}
