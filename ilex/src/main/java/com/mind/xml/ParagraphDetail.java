package com.mind.xml;

public class ParagraphDetail {
	
	private String paragraph_id = null;
	private String paragraph_number = null;
	private String paragraph_title = null;
	private String paragraph_content = null;
	
	public String getParagraph_id() 
	{
		return paragraph_id;
	}
	public void setParagraph_id(String paragraph_id) 
	{
		this.paragraph_id = paragraph_id;
	}

	public String getParagraph_content() 
	{
		return paragraph_content;
	}
	public void setParagraph_content(String paragraph_content) 
	{
		this.paragraph_content = paragraph_content;
	}
	
	public String getParagraph_number()
	{
		return paragraph_number;
	}
	public void setParagraph_number(String paragraph_number) 
	{
		this.paragraph_number = paragraph_number;
	}
	
	public String getParagraph_title() 
	{
		return paragraph_title;
	}
	public void setParagraph_title(String paragraph_title) 
	{
		this.paragraph_title = paragraph_title;
	}


}
