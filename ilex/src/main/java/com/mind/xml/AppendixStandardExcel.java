package com.mind.xml;

import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import com.mind.dao.IM.IMdao;
import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;

public class AppendixStandardExcel {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AppendixStandardExcel.class);

	public HSSFWorkbook viewAppendixExcel(String jobId, String type,
			String fromType, String fileName, String imagePath,
			String fontPath, String loginUserId, DataSource ds) {

		HSSFWorkbook wb = null;

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			String appendixId = null;
			appendixId = IMdao.getAppendixId(jobId, ds);
			String[] HeaderData = { "Item #", "Activity Name", "Scope of Work",
					"Assumptions", "Qty/ Site", "Unit Price", "Extended Price" };

			wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet("Appendix");

			sheet.setDefaultColumnWidth((short) 18);
			sheet.setGridsPrinted(false);

			HSSFCellStyle contentHeader = wb.createCellStyle();
			contentHeader.setAlignment(HSSFCellStyle.ALIGN_RIGHT);

			HSSFCellStyle headerStyle = wb.createCellStyle();
			headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

			HSSFFont titleFont = wb.createFont();
			titleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			titleFont.setFontHeightInPoints((short) 9);
			titleFont.setFontName(HSSFFont.FONT_ARIAL);

			HSSFCellStyle titleStyle = wb.createCellStyle();
			titleStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			titleStyle.setFont(titleFont);
			titleStyle.setWrapText(true);

			HSSFFont contentFont = wb.createFont();
			contentFont.setFontHeightInPoints((short) 9);
			contentFont.setFontName(HSSFFont.FONT_ARIAL);

			HSSFCellStyle contentStyle = wb.createCellStyle();
			contentStyle.setAlignment(HSSFCellStyle.ALIGN_JUSTIFY);
			contentStyle.setWrapText(true);
			contentStyle.setFont(contentFont);

			HSSFCellStyle tabletitleStyle = wb.createCellStyle();
			tabletitleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			tabletitleStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			tabletitleStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
			tabletitleStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			tabletitleStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			tabletitleStyle.setWrapText(true);
			tabletitleStyle.setFont(titleFont);
			tabletitleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			tabletitleStyle
					.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);

			HSSFCellStyle tableColumnStyle = wb.createCellStyle();
			tableColumnStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			tableColumnStyle.setFont(titleFont);
			// tableColumnStyle.setWrapText(true);
			tableColumnStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tableColumnStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tableColumnStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tableColumnStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

			HSSFCellStyle tableContentStyle = wb.createCellStyle();
			tableContentStyle.setAlignment(HSSFCellStyle.ALIGN_JUSTIFY);
			tableContentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tableContentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tableContentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tableContentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			tableContentStyle.setFont(contentFont);

			int i = 0;
			int k = 0;
			String titleText = "";
			String numberText = "";
			String contentText = "";
			String numberType = "";
			String titleType = "";
			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(fileName, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator it = list1.iterator();

			short rnum = 7;
			short cnum = 0;
			HSSFRow r = null;
			HSSFCell c = null;
			Title title = new Title();
			Number number = new Number();
			Content content = new Content();

			while (it.hasNext()) {
				titleText = "";
				numberText = "";
				contentText = "";
				section = (Section) it.next();

				numberType = section.getNumber().getType();
				titleType = section.getTitle().getType();

				title = section.getTitle();
				titleText = title.getContent();
				if (titleText.indexOf("~") >= 0)
					titleText = Domxmltaglist.addParameter(titleText,
							appendixId, type, loginUserId, jobId, ds);
				number = section.getNumber();
				numberText = number.getContent();
				if (numberText.indexOf("~") >= 0)
					numberText = Domxmltaglist.addParameter(numberText,
							appendixId, type, loginUserId, jobId, ds);
				// contentText = xt.getContent();
				content = section.getContent();
				contentText = content.getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = Domxmltaglist.addParameter(contentText,
							appendixId, type, loginUserId, jobId, ds);

				if (i == 0) {
					sheet.addMergedRegion(new Region(3, (short) 0, 3, (short) 6));
					sheet.addMergedRegion(new Region(4, (short) 0, 4, (short) 6));
					sheet.addMergedRegion(new Region(5, (short) 0, 5, (short) 6));
					HSSFRow headerNumber = sheet.createRow((short) 3);
					HSSFRow headerTitle = sheet.createRow((short) 4);
					HSSFRow headerContent = sheet.createRow((short) 5);
					HSSFCell numberCell = headerNumber.createCell((short) 0);
					HSSFCell titleCell = headerTitle.createCell((short) 0);
					HSSFCell contentCell = headerContent.createCell((short) 0);

					numberCell.setCellValue(numberText);
					numberCell.setCellStyle(headerStyle);
					titleCell.setCellValue(titleText);
					titleCell.setCellStyle(headerStyle);
					contentCell.setCellValue(contentText);
					contentCell.setCellStyle(contentHeader);

					section = (Section) it.next();
					title = section.getTitle();
					titleText = title.getContent();
					if (titleText.indexOf("~") >= 0)
						titleText = Domxmltaglist.addParameter(titleText,
								appendixId, type, loginUserId, jobId, ds);
					number = section.getNumber();
					numberText = number.getContent();
					if (numberText.indexOf("~") >= 0)
						numberText = Domxmltaglist.addParameter(numberText,
								appendixId, type, loginUserId, jobId, ds);
					content = section.getContent();
					contentText = content.getContent();
					if (contentText.indexOf("~") >= 0)
						contentText = Domxmltaglist.addParameter(contentText,
								appendixId, type, loginUserId, jobId, ds);
					numberType = section.getNumber().getType();

				}
				i++;

				if (i != 0) {
					if (!titleType.equals("table")) {
						float contentlength = 0;
						float lengthPerLine = 160;

						String numTitle = "";
						if (!numberText.equals(""))
							numTitle = numberText;
						if (!titleText.equals(""))
							numTitle = numTitle + titleText;

						if (!numberText.trim().equals("")
								|| !titleText.trim().equals("")) {
							r = sheet.createRow(rnum);
							int b = sheet.addMergedRegion(new Region(rnum,
									(short) 0, rnum, (short) 6));
							c = r.createCell(cnum);
							c.setCellStyle(titleStyle);
							c.setCellValue(numTitle);
							rnum++;
						}
						if (!contentText.equals("")) {
							r = sheet.createRow(rnum);
							sheet.addMergedRegion(new Region(rnum, (short) 0,
									rnum, (short) 6));
							c = r.createCell(cnum);
							contentlength = (float) contentText.length()
									/ lengthPerLine;

							if ((contentlength % 10) > 0)
								contentlength = contentlength + 1;

							r.setHeightInPoints((float) contentlength * 12);
							c.setCellStyle(contentStyle);
							c.setCellValue(contentText);
							rnum++;
						}

						r = sheet.createRow(rnum);
						sheet.addMergedRegion(new Region(rnum, (short) 0, rnum,
								(short) 6));
						c = r.createCell(cnum);
						c.setCellValue("");
						rnum++;

					} else {
						String activityType = null;
						String tempActivityType = null;
						String prevActivityType = null;
						boolean flagActivityType = true;

						conn = ds.getConnection();
						cstmt = conn
								.prepareCall("{?=call lx_appendix_document_data_01(?,?)}");
						cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
						cstmt.setString(2, jobId);
						cstmt.setString(3, fromType);
						rs = cstmt.executeQuery();
						short m = 0;
						short l = 0;

						while (rs.next()) {
							activityType = rs.getString(1);

							if (activityType.equals(prevActivityType)) {
								flagActivityType = false;
							}

							if (flagActivityType) {
								l = 0;
								m++;
								r = sheet.createRow(rnum);

								int b = sheet.addMergedRegion(new Region(rnum,
										(short) 0, rnum, (short) 6));
								c = r.createCell(cnum);
								c.setCellValue(activityType);
								c.setCellStyle(tabletitleStyle);

								for (short g = 1; g < 7; g++) {
									c = r.createCell(g);
									c.setCellStyle(tabletitleStyle);
								}
								rnum++;
								r = sheet.createRow(rnum);
								rnum++;
								for (short j = 0; j < HeaderData.length; j++) {
									c = r.createCell(j);
									c.setCellValue(HeaderData[j]);
									c.setCellStyle(tableColumnStyle);
								}
							}

							r = sheet.createRow(rnum);
							rnum++;

							for (short j = 0; j < HeaderData.length; j++) {
								c = r.createCell(j);
								c.setCellStyle(tableContentStyle);
								switch (j) {
								case 0:
									c.setCellValue(m + "." + l);
									break;
								case 1:
									c.setCellValue(rs.getString(3));
									break;
								case 2:
									c.setCellValue(rs.getString(4));
									break;
								case 3:
									c.setCellValue(rs.getString(5));
									break;
								case 4:
									c.setCellValue(rs.getString(6));
									break;
								case 5:
									c.setCellValue(rs.getString(7));
									break;
								case 6:
									c.setCellValue(rs.getString(8));
									break;
								}
							}
							flagActivityType = true;
							prevActivityType = activityType;
							l++;
						}

					}
				}
			}

		} catch (Exception e) {
			logger.error(
					"viewAppendixExcel(String, String, String, String, String, String, String, String, DataSource)",
					e);

			logger.error(
					"viewAppendixExcel(String, String, String, String, String, String, String, String, DataSource)",
					e);
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"viewAppendixExcel(String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"viewAppendixExcel(String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"viewAppendixExcel(String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}

		return wb;
	}

}
