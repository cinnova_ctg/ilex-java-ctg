/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class used for generating Standard MSA in rtf format.      
 *
 */

package com.mind.xml;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.VerticalAlign;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTable.XWPFBorderType;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.table.RtfCell;
import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;
import com.mind.util.WebUtil;

/**
 * Methods : viewmsartf
 */

public class MSAStandardRtf extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MSAStandardRtf.class);

	/**
	 * This method return Byte Array for Standard MSA in rtf.
	 * 
	 * @param id
	 *            String. msa Id
	 * @param type
	 *            String. type msa/appendix
	 * @param fileName
	 *            String. path of xml file
	 * @param imagePath
	 *            String. path of logo
	 * @param ds
	 *            Object of DataSource
	 * @return Byte Array This method returns byte array, for standard msa in
	 *         rtf.
	 */

	public byte[] viewMsaRtf(String id, String type, String fileName,
			String imagePath, String fontPath, String loginUserId, DataSource ds) {

		Document maindocument = new Document(PageSize.A4, 1, 1, 20, 20);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();

		try {
			String footertext = "   Confidential & Proprietary \n  \u00a9 2009 Contingent Network Services, LLC \n\t\t\t\t\t\t\t   Page ";

			RtfWriter2 writer = RtfWriter2.getInstance(maindocument, outbuff);

			/* Set font array */
			BaseFont bfArialN = BaseFont.createFont(fontPath,
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

			Font[] fonts = new Font[13];
			fonts[0] = new Font(bfArialN, 8.9f, Font.NORMAL); // for paragraph
																// content
			fonts[1] = new Font(bfArialN, 10.5f, Font.BOLD); // for page heading
			fonts[2] = new Font(bfArialN, 8, Font.BOLD); // not used
			fonts[3] = new Font(bfArialN, 8.5f, Font.NORMAL); // for header row
			fonts[4] = new Font(Font.TIMES_ROMAN, 7, Font.NORMAL); // for footer
																	// row
			fonts[5] = new Font(bfArialN, 7, Font.NORMAL); // use as variable
			fonts[6] = new Font(bfArialN, 10, Font.BOLD); // for main heading
			fonts[7] = new Font(bfArialN, 8, Font.BOLD); // for heading
			fonts[8] = new Font(bfArialN, 8.5f, Font.UNDERLINE | Font.BOLD); // for
																				// underline
																				// heading
			fonts[11] = new Font(bfArialN, 11.5f, Font.BOLD);
			fonts[12] = new Font(bfArialN, 8, Font.BOLD);

			Table headertable = new Table(1);
			headertable.setWidth(100);
			headertable.setBorderWidth(0);
			headertable.setBorder(Rectangle.NO_BORDER);
			headertable.setBorderColor(new Color(255, 255, 255));

			Table maintable = new Table(2);
			maintable.setWidth(100);

			Paragraph para1 = new Paragraph();
			Paragraph para2 = new Paragraph();
			RtfCell cell1 = new RtfCell();
			RtfCell cell2 = new RtfCell();

			Image logo = Image.getInstance(imagePath);

			float aspectRatio = 381.0f / 148.0f;
			System.out.println(aspectRatio);
			float y = 112.39f;
			System.out.println("width = " + aspectRatio * y);
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			logo.setAlignment(Image.MIDDLE);

			Chunk chunk = new Chunk(logo, 0, -20);
			HeaderFooter header = new HeaderFooter(new Phrase(chunk), false);
			HeaderFooter footer = new HeaderFooter(new Phrase(footertext,
					fonts[4]), true);

			header.setAlignment(Element.ALIGN_CENTER);
			footer.setAlignment(Element.ALIGN_LEFT);
			maindocument.setHeader(header);
			maindocument.setFooter(footer);

			maindocument.open();
			maindocument.setMargins(10, 20, 5, 5); // set document margin

			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(fileName, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator it = list1.iterator();
			int listSize = list1.size();

			String jobId = "0";

			int i = 0;
			int chksize1 = 0;
			int chksize2 = 0;
			int pageno = 0;
			float chkchunk = 0;
			boolean addtable = false;
			Title title = new Title();
			Number number = new Number();
			Content content = new Content();
			String titleText = "";
			String numberText = "";
			String contentText = "";
			String numberType = "";
			String titleType = "";
			String contentType = "";
			String Onbullet = "";
			String Title = "";
			String Content = "";
			boolean bt1 = false;
			boolean bt2 = false;
			while (it.hasNext()) {
				if (bt1 && bt2) {
					pageno++;
					para1.setAlignment(Element.ALIGN_JUSTIFIED);
					para1.setIndentationRight(10); // for middle space
					para1.setLeading(.2f);
					para2.setAlignment(Element.ALIGN_JUSTIFIED);
					para2.setLeading(.2f);

					cell1 = new RtfCell(para1);
					cell2 = new RtfCell(para2);

					maintable.addCell(cell1);
					maintable.addCell(cell2);

					maindocument.add(maintable);
					if (pageno <= 2)
						maindocument.newPage(); // for first three pages

					maintable = new Table(2);
					maintable.setWidth(100);

					para1 = new Paragraph();
					para2 = new Paragraph();

					bt1 = false;
					bt2 = false;

					chksize1 = 7;
					chksize2 = 12;
					if (pageno >= 2) // for last page
					{
						chksize1 = 0;
						chksize2 = 0;
					}
				}

				titleText = "";
				numberText = "";
				contentText = "";

				section = (Section) it.next();

				numberType = section.getNumber().getType();
				titleType = section.getTitle().getType();
				contentType = section.getContent().getType();

				title = section.getTitle();
				titleText = title.getContent();
				if (titleText.indexOf("~") >= 0)
					titleText = addParameter(titleText, id, type, loginUserId,
							jobId, ds);

				number = section.getNumber();
				numberText = number.getContent();

				if (numberText.indexOf("~") >= 0)
					numberText = addParameter(numberText, id, type,
							loginUserId, jobId, ds);

				content = section.getContent();
				contentText = content.getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = addParameter(contentText, id, type,
							loginUserId, jobId, ds);

				/* Header rows start */
				if (i == 0) {
					Paragraph para = new Paragraph(
							fonts[5].getCalculatedLeading(1), titleText,
							fonts[11]);
					cell1 = new RtfCell(para);
					cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell1.setBottom(10f);

					para = new Paragraph(fonts[5].getCalculatedLeading(1.5f),
							contentText, fonts[12]);
					cell2 = new RtfCell(para);
					cell2.setHorizontalAlignment(Element.ALIGN_CENTER);

					headertable.addCell(cell1);
					headertable.addCell(cell2);
					maindocument.add(headertable);

					section = (Section) it.next();
					title = section.getTitle();
					titleText = title.getContent();
					if (titleText.indexOf("~") >= 0)
						titleText = addParameter(titleText, id, type,
								loginUserId, jobId, ds);
					number = section.getNumber();
					numberText = number.getContent();
					if (numberText.indexOf("~") >= 0)
						numberText = addParameter(numberText, id, type,
								loginUserId, jobId, ds);
					content = section.getContent();
					contentText = content.getContent();
					if (contentText.indexOf("~") >= 0)
						contentText = addParameter(contentText, id, type,
								loginUserId, jobId, ds);
					numberType = section.getNumber().getType();
				}
				/* Header rows end */
				i++;

				if (i != 0) {
					if (numberText.equals(""))
						Title = titleText;
					else
						Title = numberText + titleText;
					Paragraph paranumber = new Paragraph(
							fonts[12].getCalculatedLeading(1), "\n" + Title
									+ "\n", fonts[12]);
					Onbullet = "";

					if (section.getNumber().getType().equals("notshow"))

						titleText = ""; // for first paragraph "General"
					if (section.getContent().getType().equals("bullet"))
						Onbullet = "\u2022 ";

					Content = Onbullet + "" + contentText;
					Paragraph paracontent = new Paragraph(
							fonts[0].getCalculatedLeading(.2f), Content,
							fonts[0]);

					chksize1 = chksize1 + 1;
					chksize2 = chksize2 + 1;

					if (chksize1 <= 15) {
						if (!titleText.equals(""))
							para1.add(paranumber);
						if (!contentText.equals(""))
							para1.add(paracontent);
					}
					if (chksize1 > 15 && chksize2 <= 29) {
						if (!titleText.equals(""))
							para2.add(paranumber);
						if (!contentText.equals(""))
							para2.add(paracontent);
					}

					if (chksize1 > 15)
						bt1 = true;
					if (chksize2 >= 29)
						bt2 = true;

					// if (xt.sectionAttribute.equals("yes")) { // for last page
					if (section.getPagebreakAfter().equals("yes"))
						bt1 = true;
					bt2 = true;
				}
			}

		} catch (Exception de) {
			logger.error(
					"viewMsaRtf(String, String, String, String, String, String, DataSource)",
					de);

			logger.error(
					"viewMsaRtf(String, String, String, String, String, String, DataSource)",
					de);
		}

		maindocument.close();
		return outbuff.toByteArray();
	}
	public byte[] viewMsaRtf(String id, String type, String fileName,
			String imagePath, String fontPath, String loginUserId,
			DataSource ds, HttpServletRequest request) {
		try {
			String docFilePath1 = WebUtil.getRealPath(request, "resources")
					+ "/MSADocTemplate2.docx";
			XWPFDocument templatedoc1 = new XWPFDocument(new FileInputStream(
					docFilePath1));			
			List<XWPFHeader> headerList = templatedoc1.getHeaderList();
			XWPFParagraph singleColumn1 = templatedoc1.getParagraphArray(0);
			XWPFParagraph multiColumn1 = templatedoc1.getParagraphArray(2);
			XWPFParagraph singleColumnNewPageHeading1 = templatedoc1.getParagraphArray(3);
			XWPFParagraph singleColumnNewPagePara1 = templatedoc1.getParagraphArray(4);
			XWPFParagraph singleColumnTable1 = templatedoc1.getParagraphArray(5);
			XWPFParagraph singleColumnNewPageHeading2 = templatedoc1.getParagraphArray(6);
			XWPFParagraph singleColumnNewPagePara2 = templatedoc1.getParagraphArray(7);
			XWPFParagraph singleColumnTable2 = templatedoc1.getParagraphArray(8);
			XWPFParagraph multiColumn2 = templatedoc1.getParagraphArray(9);
			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(fileName, ds);
			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);
			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator iterator = list1.iterator();
			int listSize = list1.size();
			String jobId = "0";
			String titleText = "";
			String numberText = "";
			String contentText = "";
			int rowNum = 0;
			int newPageCount = 0;
			int tableCount = 0;
			int tableDataHeadColumnCount = 0;
			XWPFParagraph para = multiColumn1;
			XWPFParagraph tablepara = singleColumnTable1;
			XWPFParagraph pageHeading = singleColumnNewPageHeading1;
			List<XWPFTable> allTables = templatedoc1.getTables();
			XWPFTable table = null;
			while (iterator.hasNext()) {
				section = (Section) iterator.next();			
				Title title = section.getTitle();
				titleText = title.getContent();
				System.out.println("Title---" + titleText);
				Number number = section.getNumber();
				numberText = number.getContent();
				System.out.println("Number---" + numberText);
				Content content = section.getContent();
				contentText = content.getContent();
				System.out.println(contentText);
				if ("page_title_main".equals(section.getType())) {
					singleColumn1.setAlignment(ParagraphAlignment.CENTER);
					XWPFRun run = singleColumn1.createRun();
					run.setFontSize(8);
					run.setFontFamily("Arial Narrow");
					run.setBold(true);
					run.setText(titleText);
					run.addCarriageReturn();
					XWPFRun run1 = singleColumn1.createRun();					
					run1.setFontSize(8);
					run1.setFontFamily("Arial Narrow");
					run1.setText(contentText);
					run1.addCarriageReturn();					
				} else if ("table_header".equals(section.getType()) && tableCount==0) {
					title = section.getTitle();
					String[] tableHeader = title.getContent().split("~\\|~");
					table = allTables.get(0);
					int numberOfColumns = tableHeader.length;
					XWPFTableRow headerRow = table.getRow(0);					
					XWPFTableCell cell0 = headerRow.getCell(0);						
					List<XWPFParagraph> paralst = cell0.getParagraphs();
					XWPFParagraph parat = paralst.get(0);			
					XWPFRun cellRun0 = parat.createRun();
					cellRun0.setFontSize(7);
					cellRun0.setBold(true);	
					cellRun0.setFontFamily("Arial Narrow");
					cellRun0.setText(tableHeader[0]);				
					for (int i = 1; i < numberOfColumns; i++) {
						XWPFTableCell hcell = headerRow.getCell(i);						
						paralst = hcell.getParagraphs();
						parat = paralst.get(0);						
						XWPFRun hrun = parat.createRun();
						hrun.setFontSize(7);
						hrun.setBold(true);
						hrun.setFontFamily("Arial Narrow");
						hrun.setText(tableHeader[i]);
					}
					tableCount++;
				}else if("table_header".equals(section.getType()) && tableCount==1){
					title = section.getTitle();
					String titleContent = title.getContent();					
					table = allTables.get(1);
					XWPFTableRow row = table.getRow(0);					
					XWPFTableCell cell = row.getCell(0);
					List<XWPFParagraph> paralst = cell.getParagraphs();
					XWPFParagraph parat = paralst.get(0);					
					XWPFRun cellRun0 = parat.createRun();
					cellRun0.setFontSize(8);
					cellRun0.setFontFamily("Arial Narrow");
					cellRun0.setText(titleContent);
					tableCount++;
				}
				else if ("table_data".equals(section.getType())) {
					title = section.getTitle();
					content = section.getContent();
					String[] tableRowsData = content.getContent().trim()
							.split("~\\|\\|~");
					XWPFTableRow tableRow = table.getRow(1);					
					String[] firstRowData = tableRowsData[0].trim().split("~\\|~");
					XWPFTableCell cell10 = tableRow.getCell(0);
				if(!"".equals(firstRowData[0])){	
					List<XWPFParagraph> paralst = cell10.getParagraphs();
					XWPFParagraph parat = paralst.get(0);					
					XWPFRun cellRun0 = parat.createRun();
					cellRun0.setFontSize(7);
					cellRun0.setFontFamily("Arial Narrow");
					cellRun0.setText(firstRowData[0]);
					int length = firstRowData.length;
					if(tableDataHeadColumnCount	>0){
						length = tableDataHeadColumnCount;
					}					
					for(int i=1;i<length;i++){
						try{
						XWPFTableCell cell = tableRow.getCell(i);
						if(cell!=null || firstRowData[i]==null){
						paralst = cell.getParagraphs();	
						parat = paralst.get(0);
						parat.setAlignment(ParagraphAlignment.LEFT);
						XWPFRun cellRun = parat.createRun();
						cellRun.setFontSize(7);
						cellRun.setFontFamily("Arial Narrow");
						cellRun.setText(firstRowData[i].trim());
						}else{
							tableRow.createCell().setText("");
						}
						}catch(ArrayIndexOutOfBoundsException ee){
							tableRow.createCell().setText("");
						}
					}
					String[] allRowsData = null;
					for (int k = 1; k < tableRowsData.length; k++) {
						allRowsData = tableRowsData[k].trim().split("~\\|~");
						length = allRowsData.length;
						if(tableDataHeadColumnCount	>0){
							length = tableDataHeadColumnCount;
						}
						XWPFTableRow row = table.createRow();
						for (int j = 0; j < length; j++) {
							XWPFTableCell cell = row.getCell(j);
							if (cell != null) {								
								List<XWPFParagraph> paralist = cell.getParagraphs();
								XWPFParagraph parag = paralist.get(0);
								parag.setAlignment(ParagraphAlignment.LEFT);								
								XWPFRun cellRun = parag.createRun();								
								cellRun.setFontSize(7);	
								try{
								cellRun.setFontFamily("Arial Narrow");	
								cellRun.setText(allRowsData[j]);
								}catch(ArrayIndexOutOfBoundsException ee){
									row.createCell().setText("");
								}
							} else {
								row.createCell().setText("");
							}
						}
					}
				}
				}else if("table_data_head".equals(section.getType())){
					title = section.getTitle();
					String[] tableHeader = title.getContent().split("~\\|~");
					int numberOfColumns = tableHeader.length;
					tableDataHeadColumnCount = numberOfColumns;
					XWPFTableRow headerRow = table.getRow(1);
					XWPFTableCell cell0 = headerRow.getCell(0);						
					List<XWPFParagraph> paralst = cell0.getParagraphs();
					XWPFParagraph parat = paralst.get(0);							
					XWPFRun cellRun0 = parat.createRun();					
					cellRun0.setFontSize(8);
					cellRun0.setBold(true);	
					cellRun0.setFontFamily("Arial Narrow");
					cellRun0.setText(tableHeader[0]);
					for (int i = 1; i < numberOfColumns; i++) {
						XWPFTableCell hcell = headerRow.getCell(i);
						paralst = hcell.getParagraphs();
						parat = paralst.get(0);						
						XWPFRun hrun = parat.createRun();	
						hrun.setFontSize(8);
						hrun.setBold(true);
						hrun.setFontFamily("Arial Narrow");
						hrun.setText(tableHeader[i]);
					}
				}else if ("new_page".equals(section.getType())) {
					if(newPageCount>0){
						pageHeading = singleColumnNewPageHeading2;
						para=singleColumnNewPagePara2;
					}
						XWPFRun run = pageHeading.createRun();
						run.setFontSize(8);
						run.setBold(true);
						run.setFontFamily("Arial Narrow");
						run.setText(titleText);
						run.addCarriageReturn();
						XWPFRun run1 = pageHeading.createRun();
						run1.setFontSize(8);
						run1.setFontFamily("Arial Narrow");
						run1.setText(contentText);
						run1.addCarriageReturn();	
						if(pageHeading==singleColumnNewPageHeading1){
							run1.addCarriageReturn();	
						}
						newPageCount++;
				} else if ("page_title".equals(section.getType())) {
					XWPFRun run = singleColumnNewPagePara1.createRun();
					run.setFontSize(7);
					run.setFontFamily("Arial Narrow");
					String[] contnt = contentText.split("\\r?\\n");
					for(String text:contnt){
						run.setText(text);
						run.addCarriageReturn();						
					}
					run.addCarriageReturn();
				}
				else {
					if(tableCount==2){
						para = multiColumn2;
					}
					if( (section.getTitle().getStyle()).contains("underline")  || "mainheading".equals(section.getTitle().getType()) || "subHeading".equals(section.getTitle().getType())){	
						XWPFRun run2 = para.createRun();
						run2.setFontSize(8);
						if( !(section.getTitle().getStyle()).contains("normal") ){
							run2.setBold(true);	
						}						
						if(!"".equals(numberText)){
							run2.setText(numberText);
							run2.setText(" ");
							run2.addTab();							
						}else{
								if(para==multiColumn1 || pageHeading==singleColumnNewPageHeading1){
									if(!"subHeading".equals(title.getType())  )
										run2.setUnderline(UnderlinePatterns.SINGLE);
								}
						}
						if("WARRANTY:".equals(titleText)){
							for(int i =0; i<27;i++){
								run2.addCarriageReturn();
							}							
						}
						run2.setFontFamily("Arial Narrow");
						run2.setText(titleText+" ");
						if("mainheading".equals(section.getTitle().getType())){
						run2.addCarriageReturn();						
						}
						XWPFRun run3 = para.createRun();
						run3.setFontSize(8);
						run3.setFontFamily("Arial Narrow");
						if(!"".equals(contentText)){
							if("mainheading".equals(section.getTitle().getType()) && title.getStyle().contains("bold")){
								run3.addCarriageReturn();								
								}
						run3.setText(contentText);
						run3.addCarriageReturn();
						}
						if( !(section.getTitle().getStyle()).contains("noParaSpace") ){
							run3.addCarriageReturn();
						}
						}
					else if (section.getType().equals("heading")) {
						XWPFRun run2 = para.createRun();
						run2.setFontSize(8);	
						run2.setBold(true);
						run2.setFontFamily("Arial Narrow");
						run2.setText(titleText + " ");
						XWPFRun run3 = para.createRun();
						run3.setFontSize(8);
						run3.setFontFamily("Arial Narrow");
						run3.setText(contentText);
						run3.addCarriageReturn();
						run3.addCarriageReturn();
					} else if (section.getType().equals("signature_block")) {
						XWPFRun run2 = para.createRun();
						run2.setFontSize(8);
						run2.setBold(true);
						run2.setFontFamily("Arial Narrow");
						run2.setText(contentText);
						if(contentText.contains("Signature")){
							run2.addCarriageReturn();
						}else if(contentText.contains("Print")){
							run2.addCarriageReturn();
						}else if(contentText.contains("CONTINGENT NETWORK")){
							run2.addCarriageReturn();
						}
						run2.addCarriageReturn();	
					} else {
						XWPFRun run2 = para.createRun();
						run2.setFontSize(8);
						run2.setBold(true);
						run2.setFontFamily("Arial Narrow");
						if(!"".equals(numberText)){
							run2.setText(numberText);
							run2.setText(" ");
							run2.addTab();
						}
						if(!"".equals(titleText)){
							run2.setText(titleText+" ");
						}
						if("mainheading".equals(section.getTitle().getType())){
							run2.addCarriageReturn();
							run2.addCarriageReturn();
						}
						XWPFRun run3 = para.createRun();
						run3.setFontSize(8);
						run3.setFontFamily("Arial Narrow");
						String[] contnt = contentText.split("\\r?\\n");
						for(String text:contnt){
							run3.setText(text.trim());
							run3.addCarriageReturn();
						}
						if(!"".equals(contentText)){
							run3.addCarriageReturn();
						}
					}
				}
			}
			File file = new File("tempMSADoc.docx");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream(file);
			templatedoc1.write(fos);
			byte[] bFile = new byte[(int) file.length()];
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			file.delete();
			fileInputStream.close();
			fos.close();
			return bFile;
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		return null;
	}
}