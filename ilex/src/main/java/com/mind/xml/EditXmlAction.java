package com.mind.xml;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.MSAdao;
import com.mind.ilex.xml.doc.Document;
import com.mind.util.WebUtil;

public class EditXmlAction extends com.mind.common.IlexAction {
	private static final Logger logger = Logger.getLogger(EditXmlAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		EditXmlForm formdetail = (EditXmlForm) form;
		String addid = "";
		String Type = "";
		String EditType = "";
		int list_size = 0;
		int retval = 0;
		String filename = null;
		String file_name = "";
		String Id = "";
		String name = "";
		String nameNetmedx = "";
		String type_name = "";
		String temp_type = "";
		// System.out.println("filePath------"+filepath);
		String netMedxXml = null;

		// Start: Variables added for the View Selector Functioning.
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String monthMSA = null;
		String weekMSA = null;
		String opendiv = null;
		ArrayList statusList = new ArrayList();
		ArrayList ownerList = new ArrayList();
		String selectedStatus = "";
		String selectedOwner = "";
		boolean chkOtherOwner = false;
		String tempStatus = "";
		String tempOwner = "";
		String ownerType = "";
		String msaid = "";
		int ownerListSize = 0;
		// End

		if (request.getParameter("Id") != null) {
			formdetail.setId(request.getParameter("Id"));
			formdetail.setTypeid(formdetail.getId());
		} else {
			formdetail.setId(formdetail.getTypeid());
			formdetail.setTypeid(formdetail.getId());
		}
		if (request.getParameter("Type") != null) {
			formdetail.setType(request.getParameter("Type"));
		}

		if (request.getParameter("EditType") != null) {
			formdetail.setEdittype(request.getParameter("EditType"));
		}

		if (request.getParameter("Id") != null) {
			formdetail.setAppendixId(request.getParameter("Id"));
		}

		String temp_fu_type = "Edit " + formdetail.getType().toString()
				+ " Master";
		String temp_fu_name = formdetail.getEdittype();

		if (formdetail.getEdittype().equals("PMT"))
			temp_fu_name = "Manage Proposal"; // for proposal master

		if (formdetail.getEdittype().equals("PM")) {
			/* set type of MSA */
			if (formdetail.getType().equals("MSA")) {
				if (!formdetail.getId().equals(""))
					formdetail.setType_name(EditXmldao.getTypeName(
							formdetail.getId(), formdetail.getType(),
							getDataSource(request, "ilexnewDB")));
			}
			if (formdetail.getType().equalsIgnoreCase("NetMedX")
					|| formdetail.getType().equalsIgnoreCase("Hardware"))
				formdetail.setType("Appendix");

			temp_fu_type = "Edit " + formdetail.getType() + " Text/Format";
			temp_fu_name = formdetail.getType();
			if (formdetail.getType().equals("NetMedX"))
				temp_fu_name = "MSA";
		}

		/* Page Security Start */
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userid = (String) session.getAttribute("userid");

		if (!Authenticationdao.getPageSecurity(userid, temp_fu_name,
				temp_fu_type, getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		/* Page Security End */
		/* Get and set the name of MSA/Appendix */
		if (formdetail.getId() != null && !formdetail.getId().equals(""))
			formdetail.setName(EditXmldao.getName(formdetail.getId(),
					formdetail.getType(), getDataSource(request, "ilexnewDB")));

		/* Check the type for NetMedX or Hardware and set the type NetMedX */
		if (formdetail.getId() != null && !formdetail.getId().equals("")
				&& !formdetail.getType().equals("MSA")
				&& !formdetail.getEdittype().equals("PMT"))
			formdetail.setTempType(ViewList.getAppendixtypedesc(
					formdetail.getId(), getDataSource(request, "ilexnewDB")));

		if (formdetail.getTempType() != null) {
			if (formdetail.getTempType().equals("NetMedX")
					|| formdetail.getTempType().equals("Hardware"))
				formdetail.setType(formdetail.getTempType());
		}

		/* Get file name */
		if (formdetail.getType().equals("MSA")) {
			formdetail.setFilename1(this.getResources(request).getMessage(
					"msa.view.filename"));
		}

		// System.out.println("Id-----"+Id);

		if (formdetail.getType().equals("NetMedX")
				|| formdetail.getType().equals("NewNetMedX")) {
			if (formdetail.getEdittype().equals("PMT")) {
				if (formdetail.getType().equals("NetMedX")) {
					formdetail.setFilename1(this.getResources(request)
							.getMessage("netmedx.view.filename"));
				} else if (formdetail.getType().equals("NewNetMedX")) {
					formdetail.setFilename1(this.getResources(request)
							.getMessage("netmedx.view.filename.fromproposal"));
				}
			} else {
				netMedxXml = EditXmldao.getNetMedxXmlType(Id,
						getDataSource(request, "ilexnewDB"));
				formdetail.setFilename1(this.getResources(request).getMessage(
						"netmedx.view.filename"));

				if (!(netMedxXml != null && !netMedxXml.equals("") && netMedxXml
						.equals("Old"))) {
					formdetail.setFilename1(this.getResources(request)
							.getMessage("netmedx.view.filename.fromproposal"));
				}
			}
		}

		// System.out.println("filename1-----"+formdetail.getFilename1());

		if (formdetail.getType().equals("Appendix")) {
			formdetail.setFilename1(this.getResources(request).getMessage(
					"appendix.view.filename"));
		}

		if (formdetail.getType().equals("Hardware")) {
			formdetail.setFilename1(this.getResources(request).getMessage(
					"hardware.view.filename"));
		}

		/*
		 * Check MSA/Appendix previously edited or not and set file name
		 * accordingly
		 */
		boolean chkfile = false;
		String chkfiletype = EditXmldao.getChkXmlFile(formdetail.getId(),
				formdetail.getType(), getDataSource(request, "ilexnewDB")); // chk
																			// xml
																			// file
																			// for
																			// this
																			// msa
																			// is
																			// exist
																			// or
																			// not
		if (chkfiletype == null)
			chkfiletype = "";
		if (chkfiletype.equals("X"))
			chkfile = true;
		if (chkfiletype.equals(""))
			chkfile = false;

		if (chkfile) {
			if (formdetail.getId().equals("0"))
				addid = "";
			else
				addid = formdetail.getId();
			file_name = formdetail.getFilename1() + "" + addid;
		} else {
			file_name = formdetail.getFilename1();
		}

		/*
		 * if(formdetail.getType().equals("NetMedX")) {
		 * if(formdetail.getId().equals("0")) formdetail.setId(""); }
		 */

		// System.out.println("In SAVE_________________chkfiletype---------------"+chkfiletype+"---file_name----"+file_name);
		formdetail.setMSA_Id(IMdao.getMSAId(formdetail.getTypeid(),
				getDataSource(request, "ilexnewDB")));
		formdetail.setMsaname(com.mind.dao.PM.Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"), formdetail.getMSA_Id()));

		request.setAttribute("file_name", file_name);

		String filepath_write = "";
		if (formdetail.getId() != null) {
			filepath_write =

			formdetail.getFilename1() + "" + formdetail.getId();
		}

		if (formdetail.getSave() != null) {
			StringWriter sw = null;

			Document docFile = formdetail.getDocument();
			sw = new StringWriter();
			Marshaller marshaller = XMLUtil.getDocumentMarshaller();
			marshaller.marshal(docFile, sw);
			if (logger.isDebugEnabled()) {
				logger.debug("Marshalling  Xml File " + sw.toString());
			}
			boolean recordsExist = EditXmldao.isXmlRecordExist(filepath_write,
					getDataSource(request, "ilexnewDB"));
			if (!recordsExist) {

				EditXmldao.insertXmlFileTODB(filepath_write, sw.toString(),
						formdetail.getType(), formdetail.getId(), loginuserid,
						getDataSource(request, "ilexnewDB"));

			} else {
				EditXmldao.updateXmlFile(filepath_write, "" + sw.toString(),
						loginuserid, getDataSource(request, "ilexnewDB"));
			}

			// if(!formdetail.getType().equals("NetMedX"))
			EditXmldao.setChkXmlFile(formdetail.getId(), formdetail.getType(),
					getDataSource(request, "ilexnewDB"));

			/* Refresh data of page after save */
			/* get new file name */
			/*
			 * Check MSA/Appendix previously edited or not and set file name
			 * accordingly
			 */

			file_name = "";
			chkfile = false;
			chkfiletype = EditXmldao.getChkXmlFile(formdetail.getId(),
					formdetail.getType(), getDataSource(request, "ilexnewDB")); // chk
																				// xml
																				// file
																				// for
																				// this
																				// msa
																				// is
																				// exist
																				// or
																				// not
			if (chkfiletype == null)
				chkfiletype = "";
			if (chkfiletype.equals("X"))
				chkfile = true;
			if (chkfiletype.equals(""))
				chkfile = false;

			if (chkfile) {
				if (formdetail.getId().equals("0"))
					addid = "";
				else
					addid = formdetail.getId();
				file_name = formdetail.getFilename1().toString() + "" + addid;
			} else
				file_name = formdetail.getFilename1().toString();

			formdetail.setRefresh(true);

			/* Get and set the name of MSA/Appendix */
			if (!formdetail.getId().equals(""))
				formdetail.setName(EditXmldao.getName(formdetail.getId(),
						formdetail.getType(),
						getDataSource(request, "ilexnewDB")));
			if (formdetail.getName() == null)
				formdetail.setName("");
			if (!formdetail.getName().equals(""))
				formdetail.setName(formdetail.getName());

			/* Check the name for NetMedX and set the type NetMedX */
			if (!formdetail.getId().equals(""))
				nameNetmedx = EditXmldao.getName(formdetail.getId(),
						formdetail.getType(),
						getDataSource(request, "ilexnewDB"));
			if (nameNetmedx.equalsIgnoreCase("NetMedX"))
				formdetail.setType("NetMedX"); // for netmedx

			if (formdetail.getType().equals("MSA"))
				retval = 1;
			if (formdetail.getType().equals("NetMedX")
					|| formdetail.getType().equals("NewNetMedX"))
				retval = 2;
			if (formdetail.getType().equals("Appendix"))
				retval = 3;
			if (formdetail.getType().equals("Hardware"))
				retval = 4;
			request.setAttribute("retval", "" + retval);

			// System.out.println("****End update xml**********");
		}

		String xmlFile = EditXmldao.getXmlFile(file_name,
				getDataSource(request, "ilexnewDB"));

		if (xmlFile == null || "".equals(xmlFile.trim())) {
			throw new RuntimeException("xmlcontent not found for : "
					+ file_name);
		}

		final StringReader xmlReader = new StringReader(xmlFile);
		final StreamSource xmlSource = new StreamSource(xmlReader);

		Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();

		com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
				.unmarshal(xmlSource);
		if (logger.isDebugEnabled()) {
			logger.debug("xmlFile from database " + xmlFile);
		}
		session.setAttribute("xmlDocument", doc);
		formdetail.setDocument(doc);

		if (formdetail.getId() == null)
			formdetail.setId("0");

		// Common Code for both MSA And Appendix View Selector.
		if (request.getParameter("opendiv") != null) {
			opendiv = request.getParameter("opendiv");
			request.setAttribute("opendiv", opendiv);
		}

		if (formdetail.getType() != null) {
			if (!formdetail.getEdittype().equals("PMT")
					&& (formdetail.getType().equals("MSA"))) {
				ownerType = "msa";
				msaid = "0";

				if (session.getAttribute("appendixSelectedOwners") != null
						|| session.getAttribute("appendixSelectedStatus") != null)
					request.setAttribute("specialCase", "specialCase");

				if (session.getAttribute("monthAppendix") != null
						|| session.getAttribute("weekAppendix") != null) {
					request.setAttribute("specialCase", "specialCase");
				}

				WebUtil.removeAppendixSession(session);
				// when the other check box is checked in the view selector.
				if (formdetail.getOtherCheck() != null)
					session.setAttribute("otherCheck", formdetail
							.getOtherCheck().toString());

				if (formdetail.getSelectedStatus() != null) {
					for (int i = 0; i < formdetail.getSelectedStatus().length; i++) {
						selectedStatus = selectedStatus + "," + "'"
								+ formdetail.getSelectedStatus(i) + "'";
						tempStatus = tempStatus + ","
								+ formdetail.getSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				if (formdetail.getSelectedOwners() != null) {
					for (int j = 0; j < formdetail.getSelectedOwners().length; j++) {
						selectedOwner = selectedOwner + ","
								+ formdetail.getSelectedOwners(j);
						tempOwner = tempOwner + ","
								+ formdetail.getSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}
				// End of the code.

				// The Menu Images are to be set in session respective of one
				// other.
				if (request.getParameter("month") != null) {
					session.setAttribute("monthMSA",
							request.getParameter("month").toString());
					monthMSA = request.getParameter("month").toString();
					session.removeAttribute("weekMSA");
					formdetail.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekMSA", request
							.getParameter("week").toString());
					weekMSA = request.getParameter("week").toString();
					session.removeAttribute("monthMSA");
					formdetail.setMonthWeekCheck("week");
				}

				// When the reset button is pressed remove every thing from the
				// session.
				if (request.getParameter("home") != null) {
					formdetail.setSelectedStatus(null);
					formdetail.setSelectedOwners(null);
					formdetail.setPrevSelectedStatus("");
					formdetail.setPrevSelectedOwner("");
					formdetail.setOtherCheck(null);
					formdetail.setMonthWeekCheck(null);
					// MSAdao.setValuesWhenReset(session);
					return (mapping.findForward("msalistpage"));
				}

				// To set the values in the session when the Show button is
				// Pressed.
				if (formdetail.getGo() != null
						&& !formdetail.getGo().equals("")) {
					session.setAttribute("selectedOwners", selectedOwner);
					session.setAttribute("MSASelectedOwners", selectedOwner);
					session.setAttribute("tempOwner", tempOwner);
					session.setAttribute("selectedStatus", selectedStatus);
					session.setAttribute("MSASelectedStatus", selectedStatus);
					session.setAttribute("tempStatus", tempStatus);

					if (formdetail.getMonthWeekCheck() != null) {
						if (formdetail.getMonthWeekCheck().equals("week")) {
							session.removeAttribute("monthMSA");
							session.setAttribute("weekMSA", "0");
						}
						if (formdetail.getMonthWeekCheck().equals("month")) {
							session.removeAttribute("weekMSA");
							session.setAttribute("monthMSA", "0");
						}
						if (formdetail.getMonthWeekCheck().equals("clear")) {
							session.removeAttribute("monthMSA");
							session.removeAttribute("weekMSA");
						}

					}

					if (session.getAttribute("monthMSA") != null) {
						session.removeAttribute("weekMSA");
						monthMSA = session.getAttribute("monthMSA").toString();
					}
					if (session.getAttribute("weekMSA") != null) {
						session.removeAttribute("monthMSA");
						weekMSA = session.getAttribute("weekMSA").toString();
					}
					return (mapping.findForward("msalistpage"));
				} else // If not pressed see the values exists in the session or
						// not.
				{
					if (session.getAttribute("selectedOwners") != null) {
						selectedOwner = session.getAttribute("selectedOwners")
								.toString();
						formdetail.setSelectedOwners(session
								.getAttribute("tempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("selectedStatus") != null) {
						selectedStatus = session.getAttribute("selectedStatus")
								.toString();
						formdetail.setSelectedStatus(session
								.getAttribute("tempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("otherCheck") != null)
						formdetail.setOtherCheck(session.getAttribute(
								"otherCheck").toString());
				}

				if (session.getAttribute("monthMSA") != null) {
					session.removeAttribute("weekMSA");
					monthMSA = session.getAttribute("monthMSA").toString();
				}
				if (session.getAttribute("weekMSA") != null) {
					session.removeAttribute("monthMSA");
					weekMSA = session.getAttribute("weekMSA").toString();
				}

				if (!chkOtherOwner) {
					formdetail.setPrevSelectedStatus(selectedStatus);
					formdetail.setPrevSelectedOwner(selectedOwner);
				}

				ownerList = MSAdao.getOwnerList(loginuserid,
						formdetail.getOtherCheck(), ownerType, msaid,
						getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_msa",
						getDataSource(request, "ilexnewDB"));
				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

				// End of the code for the View Selector Part
			}
			if (!formdetail.getEdittype().equals("PMT")
					&& (formdetail.getType().equalsIgnoreCase("Appendix")
							|| formdetail.getType()
									.equalsIgnoreCase("Hardware") || formdetail
							.getType().equalsIgnoreCase("NetMedX"))) {
				ownerType = "appendix";
				if (request.getParameter("home") != null) // This is called when
															// reset button is
															// pressed from the
															// view selector.
				{
					formdetail.setAppendixSelectedStatus(null);
					formdetail.setAppendixSelectedOwners(null);
					formdetail.setPrevSelectedStatus("");
					formdetail.setPrevSelectedOwner("");
					formdetail.setAppendixOtherCheck(null);
					formdetail.setMonthWeekCheck(null);
					return (mapping.findForward("appendixlistpage"));
				}

				// These two parameter month/week are checked are kept in
				// session. ( View Images)
				if (request.getParameter("month") != null) {
					session.setAttribute("monthAppendix",
							request.getParameter("month").toString());

					session.removeAttribute("weekAppendix");
					formdetail.setMonthWeekCheck("month");
				}

				if (request.getParameter("week") != null) {
					session.setAttribute("weekAppendix",
							request.getParameter("week").toString());

					session.removeAttribute("monthAppendix");
					formdetail.setMonthWeekCheck("week");
				}

				if (formdetail.getAppendixOtherCheck() != null) // OtherCheck
																// box status is
																// kept in
																// session.
					session.setAttribute("appendixOtherCheck", formdetail
							.getAppendixOtherCheck().toString());

				// When Status Check Boxes are Clicked :::::::::::::::::
				if (formdetail.getAppendixSelectedStatus() != null) {
					for (int i = 0; i < formdetail.getAppendixSelectedStatus().length; i++) {
						selectedStatus = selectedStatus + "," + "'"
								+ formdetail.getAppendixSelectedStatus(i) + "'";
						tempStatus = tempStatus + ","
								+ formdetail.getAppendixSelectedStatus(i);
					}

					selectedStatus = selectedStatus.substring(1,
							selectedStatus.length());
					selectedStatus = "(" + selectedStatus + ")";
				}

				// When Owner Check Boxes are Clicked :::::::::::::::::
				if (formdetail.getAppendixSelectedOwners() != null) {
					for (int j = 0; j < formdetail.getAppendixSelectedOwners().length; j++) {
						selectedOwner = selectedOwner + ","
								+ formdetail.getAppendixSelectedOwners(j);
						tempOwner = tempOwner + ","
								+ formdetail.getAppendixSelectedOwners(j);
					}
					selectedOwner = selectedOwner.substring(1,
							selectedOwner.length());
					if (selectedOwner.equals("0")) {
						chkOtherOwner = true;
						selectedOwner = "%";
					}
					selectedOwner = "(" + selectedOwner + ")";
				}
				if (formdetail.getGo() != null
						&& !formdetail.getGo().equals("")) {
					session.setAttribute("appendixSelectedOwners",
							selectedOwner);
					session.setAttribute("appSelectedOwners", selectedOwner);
					session.setAttribute("sessionMSAId", formdetail.getMSA_Id());
					session.setAttribute("appendixTempOwner", tempOwner);
					session.setAttribute("appendixSelectedStatus",
							selectedStatus);
					session.setAttribute("appSelectedStatus", selectedStatus);
					session.setAttribute("appendixTempStatus", tempStatus);

					if (formdetail.getMonthWeekCheck() != null) {
						if (formdetail.getMonthWeekCheck().equals("week")) {
							session.removeAttribute("monthAppendix");
							session.setAttribute("weekAppendix", "0");

						}
						if (formdetail.getMonthWeekCheck().equals("month")) {
							session.removeAttribute("weekAppendix");
							session.setAttribute("monthAppendix", "0");

						}
						if (formdetail.getMonthWeekCheck().equals("clear")) {
							session.removeAttribute("weekAppendix");
							session.removeAttribute("monthAppendix");
						}
						if (formdetail.getMonthWeekCheck().equals("clear")) {
							session.removeAttribute("weekAppendix");
							session.removeAttribute("monthAppendix");

						}

					}
					return (mapping.findForward("appendixlistpage"));
				} else {
					if (session.getAttribute("appendixSelectedOwners") != null) {
						selectedOwner = session.getAttribute(
								"appendixSelectedOwners").toString();
						formdetail.setAppendixSelectedOwners(session
								.getAttribute("appendixTempOwner").toString()
								.split(","));
					}
					if (session.getAttribute("appendixSelectedStatus") != null) {
						selectedStatus = session.getAttribute(
								"appendixSelectedStatus").toString();
						formdetail.setAppendixSelectedStatus(session
								.getAttribute("appendixTempStatus").toString()
								.split(","));
					}
					if (session.getAttribute("appendixOtherCheck") != null)
						formdetail.setAppendixOtherCheck(session.getAttribute(
								"appendixOtherCheck").toString());

					if (session.getAttribute("monthAppendix") != null) {
						session.removeAttribute("weekAppendix");
						formdetail.setMonthWeekCheck("month");
					}
					if (session.getAttribute("weekAppendix") != null) {
						session.removeAttribute("monthAppendix");
						formdetail.setMonthWeekCheck("week");
					}

				}
				if (!chkOtherOwner) {
					formdetail.setPrevSelectedStatus(selectedStatus);
					formdetail.setPrevSelectedOwner(selectedOwner);
				}
				ownerList = MSAdao.getOwnerList(loginuserid,
						formdetail.getAppendixOtherCheck(), ownerType,
						formdetail.getMSA_Id(),
						getDataSource(request, "ilexnewDB"));
				statusList = MSAdao.getStatusList("pm_appendix",
						getDataSource(request, "ilexnewDB"));
				if (ownerList.size() > 0)
					ownerListSize = ownerList.size();

				request.setAttribute("clickShow",
						request.getParameter("clickShow"));
				request.setAttribute("statuslist", statusList);
				request.setAttribute("ownerlist", ownerList);
				request.setAttribute("ownerListSize", ownerListSize + "");

			}
		}
		request.setAttribute("Appendix_Id", formdetail.getAppendixId());
		request.setAttribute("MSA_Id", formdetail.getMSA_Id());
		request.setAttribute("Type", formdetail.getType());
		request.setAttribute("EditType", formdetail.getEdittype());
		request.setAttribute("Size", new Integer(list_size));

		if (formdetail.getType().equals("MSA")
				&& !formdetail.getEdittype().equals("PMT")) {
			request.setAttribute("MSA_Id", formdetail.getId());
			String msaStatus = MSAdao.getMSAStatus(formdetail.getId(),
					getDataSource(request, "ilexnewDB"));
			String msaUploadCheck = MSAdao.getMSAUploadCheck(
					formdetail.getId(), getDataSource(request, "ilexnewDB"));
			String list = "";
			if (formdetail.getId() != null && !formdetail.getId().equals(""))
				list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
						formdetail.getId(), "", "", "", loginuserid,
						getDataSource(request, "ilexnewDB"));
			request.setAttribute("list", list);
			request.setAttribute("status", msaStatus);
			request.setAttribute("uploadstatus", msaUploadCheck);
		}

		if ((formdetail.getType().equals("Appendix")
				|| formdetail.getType().equalsIgnoreCase("Hardware") || formdetail
				.getType().equalsIgnoreCase("NetMedx"))
				&& !formdetail.getEdittype().equals("PMT")) {
			String appendixStatus = Appendixdao.getAppendixStatus(
					formdetail.getAppendixId(), formdetail.getMSA_Id(),
					getDataSource(request, "ilexnewDB"));
			String addendumlist = Appendixdao.getAddendumsIdName(
					formdetail.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			String addendumid = com.mind.dao.PM.Addendumdao
					.getLatestAddendumId(formdetail.getAppendixId(),
							getDataSource(request, "ilexnewDB"));
			String list = Menu.getStatus("pm_appendix",
					appendixStatus.charAt(0), formdetail.getMSA_Id(),
					formdetail.getAppendixId(), "", "", loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("appendixType", formdetail.getType());
			request.setAttribute("appendixStatusList", list);
			request.setAttribute("Appendix_Id", formdetail.getAppendixId());
			request.setAttribute("MSA_Id", formdetail.getMSA_Id());
			request.setAttribute("appendixStatus", appendixStatus);
			request.setAttribute("latestaddendumid", addendumid);
			request.setAttribute("addendumlist", addendumlist);
		}

		// System.out.println("type in end----------"+formdetail.getType());
		return (mapping.findForward("success"));

	}
}
