package com.mind.xml;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.html.HtmlWriter;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.mind.common.Decimalroundup;
import com.mind.dao.IM.IMdao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.xml.doc.Section;

public class MSANetMedXHtml extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MSANetMedXHtml.class);

	public byte[] viewNetmedxHtml(String id, String type, String filename,
			String imagepath, String login_userid, DataSource ds) {
		Document document = new Document(PageSize.A4, 1, 1, 20, 20);
		Document maindocument = new Document(PageSize.A4, 1, 1, 20, 20);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();
		Font[] fonts = new Font[9];
		fonts[0] = new Font(Font.HELVETICA, 7, Font.NORMAL);
		fonts[1] = new Font(Font.HELVETICA, 11, Font.BOLD);
		fonts[3] = new Font(Font.HELVETICA, 8, Font.NORMAL);
		fonts[4] = new Font(Font.TIMES_ROMAN, 7, Font.NORMAL);
		fonts[5] = new Font(Font.HELVETICA, 7, Font.BOLD);
		fonts[6] = new Font(Font.HELVETICA, 6, Font.BOLD);
		fonts[7] = new Font(Font.HELVETICA, 6, Font.NORMAL);
		fonts[8] = new Font(Font.HELVETICA, 5, Font.BOLD);

		Connection conn = null;
		Statement stmt = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {

			HtmlWriter writer = HtmlWriter.getInstance(document,
					new FileOutputStream(System.getProperty("ilex.temp.path")
							+ "/tempMSAnetmedXpdf.html"));
			HtmlWriter writermain = HtmlWriter.getInstance(maindocument,
					outbuff);

			String footertext = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Confidential & Proprietary \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t \u00a9 2009 Contingent Network Services, LLC \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Page ";

			// System.out.println("id::::::::::::::"+id);
			// System.out.println("type::::::::::::::"+type);
			// System.out.println("filename::::::::::::::"+filename);
			// System.out.println("imagepath::::::::::::::"+imagepath);
			// System.out.println("temp_filepath::::::::::::::"+temp_filepath);

			PdfPTable table = new PdfPTable(2);
			PdfPTable table3 = new PdfPTable(2);
			PdfPTable maintable3 = new PdfPTable(2);
			PdfPTable headertable = new PdfPTable(2);
			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			PdfPCell cell3 = new PdfPCell();

			Table headertable1 = new Table(2);
			headertable1.setBorderWidth(0);
			headertable1.setPadding(45);

			// headertable1.setSpaceBetweenCells(5);

			Image logo = Image.getInstance(imagepath);
			logo.setAlignment(Image.MIDDLE);

			Chunk chunk = new Chunk(logo, 0, -60);

			HeaderFooter header = new HeaderFooter(new Phrase(chunk), false);
			HeaderFooter footer = new HeaderFooter(new Phrase(footertext,
					fonts[4]), true);

			header.setAlignment(Element.ALIGN_CENTER);
			header.setBorder(Rectangle.NO_BORDER);
			footer.setAlignment(Element.ALIGN_LEFT);
			footer.setBorder(Rectangle.NO_BORDER);
			maindocument.setHeader(header);
			maindocument.setFooter(footer);
			document.open();

			maindocument.open();
			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(filename, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator it = list1.iterator();

			PdfPCell cella;
			PdfPCell cellnode, cellnumber;
			PdfPTable table1 = new PdfPTable(1);
			PdfPTable maintable1 = new PdfPTable(1);
			PdfPTable maintable2 = new PdfPTable(1);
			table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			PdfPTable table2 = new PdfPTable(1);
			table2.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			int i = 0;
			String titleText = "";
			String numberText = "";
			String contentText = "";
			String titleType = "";
			String numberType = "";
			String contentType = "";
			int colspan = 0;
			int rowspan = 0;
			int tableSize = 0;
			int t = 0;
			int k = 0;
			boolean addtable = false;
			boolean addfulltable = false;
			boolean adddata = false;
			boolean chkpadding = false;
			boolean addNewPara = false;
			boolean bt1 = false;
			boolean bt2 = false;
			double diff = 0.0;
			double count = 0.0;
			double pretable3Height = 0.0;
			double table3Height = 0.0;
			double setTableHeight = 610;
			double TableHeightLower = 610;
			double TableHeightUpper = 0.0;
			String Onbullet = "";

			String jobId = "0";

			String sql = "";
			int num = 0;
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String[][] resourcelabel = null;

			String msaid = IMdao.getMSAId(id, ds);

			/* Get the labor rates width start */

			rs = stmt.executeQuery("select num from func_clr_total_resources("
					+ msaid + ")");
			if (rs.next()) {
				num = rs.getInt(1);
			}

			resourcelabel = new String[num][2];
			rs = stmt.executeQuery("select * from func_clr_reslevel(" + msaid
					+ ")");
			i = 0;
			while (rs.next()) {
				resourcelabel[i][0] = rs.getString("clr_class_id");
				resourcelabel[i][1] = rs.getString("clr_reslevel_name");
				i++;
			}
			i = 0;

			if (conn != null) {
				conn.close();
			}

			if (stmt != null) {
				stmt.close();
			}
			/* Get the labor rates width end */

			while (it.hasNext()) {
				if (bt1 && bt2) {
					cella = new PdfPCell(maintable1);
					cella.setBorder(Rectangle.NO_BORDER);
					if (!chkpadding)
						cella.setPaddingTop(5);
					else
						cella.setPaddingTop(50);
					maintable3.addCell(cella);

					cella = new PdfPCell(maintable2);
					cella.setBorder(Rectangle.NO_BORDER);
					if (!chkpadding)
						cella.setPaddingTop(5);
					else
						cella.setPaddingTop(50);
					maintable3.addCell(cella);

					maindocument.add(maintable3);

					maintable3.deleteBodyRows();
					maintable1 = new PdfPTable(1);
					maintable2 = new PdfPTable(1);
					bt1 = false;
					bt2 = false;
					table3Height = 0.0;
					TableHeightLower = 610;
					chkpadding = true;
				}

				if (addfulltable && !bt1 && !bt2) {
					maindocument.newPage();
					cell1 = new PdfPCell(table);
					cell1.setPaddingTop(50);
					table1 = new PdfPTable(1);
					table1.addCell(cell1);
					table1.setWidthPercentage(90);
					maindocument.add(table1);
					addfulltable = false;
				}

				TableHeightUpper = 2 * TableHeightLower;
				titleText = "";
				numberText = "";
				contentText = "";
				titleType = "";
				numberType = "";
				section = (Section) it.next();
				titleType = section.getTitle().getType();
				titleText = section.getTitle().getContent();
				if (titleText.indexOf("~") >= 0)
					titleText = addParameter(titleText, id, type, login_userid,
							jobId, ds);
				numberText = section.getNumber().getContent();
				if (numberText.indexOf("~") >= 0)
					numberText = addParameter(numberText, id, type,
							login_userid, jobId, ds);
				contentText = section.getContent().getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = addParameter(contentText, id, type,
							login_userid, jobId, ds);
				if (i == 0) {
					Paragraph para1 = new Paragraph(
							fonts[0].getCalculatedLeading(4), titleText,
							fonts[1]);
					para1.setSpacingAfter(15f);
					para1.setSpacingBefore(15f);
					cell2 = new PdfPCell(para1);
					cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell2.setBorder(Rectangle.NO_BORDER);
					cell2.setPaddingTop(42);
					cell2.setColspan(2);

					para1 = new Paragraph(fonts[0].getCalculatedLeading(4),
							contentText, fonts[3]);
					para1.setSpacingAfter(10f);
					para1.setSpacingBefore(10f);
					cell3 = new PdfPCell(para1);
					cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell3.setBorder(Rectangle.NO_BORDER);
					cell3.setPaddingTop(2);
					cell3.setColspan(2);

					headertable.addCell(cell2);
					headertable.addCell(cell3);
					maindocument.add(headertable);

					section = (Section) it.next();
					titleType = section.getTitle().getType();
					titleText = section.getTitle().getContent();
					if (titleText.indexOf("~") >= 0)
						titleText = addParameter(titleText, id, type,
								login_userid, jobId, ds);
					numberText = section.getNumber().getContent();
					if (numberText.indexOf("~") >= 0)
						numberText = addParameter(numberText, id, type,
								login_userid, jobId, ds);
					contentText = section.getContent().getContent();
					if (contentText.indexOf("~") >= 0)
						contentText = addParameter(contentText, id, type,
								login_userid, jobId, ds);
				}
				i++;

				if (i != 0) {
					if (titleType.equals("table")) {
						contentType = section.getContent().getType();
						numberType = section.getNumber().getType();
						colspan = section.getTitle().getColspan().intValue();
						rowspan = section.getTitle().getRowspan().intValue();
						tableSize = section.getTitle().getSize().intValue();

						if (t == 0) {
							if (contentType.equals("iv_cns_field_services")) {
								tableSize = tableSize + num + 2;
								colspan = colspan + num + 2;
							}
							table = new PdfPTable(tableSize);
						}
						Paragraph para = new Paragraph(titleText, fonts[5]);
						cell1 = new PdfPCell(para);
						cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell1.setPaddingLeft(5f);
						cell1.setPaddingRight(5f);
						cell1.setColspan(colspan);
						table.addCell(cell1);
						adddata = true;
						t++;
					} else {
						if (adddata) {
							sql = "";
							conn = ds.getConnection();
							stmt = conn.createStatement();

							if (contentType.equals("iv_criticality"))
								sql = "select iv_crit_name, iv_crit_condition from iv_criticality";
							if (contentType.equals("iv_distance_restime"))
								sql = "select iv_distance_value, iv_response_time from iv_distance_restime";
							if (contentType.equals("iv_cns_call_res_price"))
								sql = "select iv_cns_res_name, iv_cns_price_per_min_pps, iv_cns_price_per_min_nonpps from iv_cns_call_res_price";
							if (contentType.equals("iv_cns_nw_price"))
								sql = "select iv_cns_nw_item_name, iv_cns_nw_item_price from iv_cns_nw_price";
							if (contentType.equals("iv_cns_logistics_services")) {
								// System.out.p
								sql = "select iv_ls_activity, iv_ls_desc, iv_ls_rates, iv_ls_addtional_info from iv_cns_logistics_services";
								addfulltable = true;
							}
							if (contentType.equals("iv_view_restime_goal")) {
								cstmt = conn
										.prepareCall("{?=call iv_view_restime_goal()}");
								cstmt.registerOutParameter(1,
										java.sql.Types.INTEGER);
								rs = cstmt.executeQuery();
							} else if (contentType
									.equals("iv_cns_field_services")) {
								cstmt = conn
										.prepareCall("{?=call iv_clr_tab_02(?, ?)}");
								cstmt.registerOutParameter(1,
										java.sql.Types.INTEGER);
								cstmt.setString(2, msaid);
								cstmt.setString(3, "0");
								rs = cstmt.executeQuery();
								addfulltable = true;

								cell1 = new PdfPCell(new Paragraph("Activity",
										fonts[6]));
								cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
								cell1.setColspan(2);
								table.addCell(cell1);

								cell1 = new PdfPCell(new Paragraph(
										"Description", fonts[6]));
								cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
								cell1.setColspan(2);
								table.addCell(cell1);

								for (int z = 0; z < num; z++) {
									cell1 = new PdfPCell(new Paragraph(
											resourcelabel[z][1], fonts[8]));
									cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
									table.addCell(cell1);
								}
							}

							else
								rs = stmt.executeQuery(sql);
							while (rs.next()) {

								if (contentType.equals("iv_cns_field_services")) {
									cell1 = new PdfPCell(
											new Paragraph(rs.getString(1)
													+ "\n" + rs.getString(2),
													fonts[0]));
									cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
									cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
									cell1.setColspan(2);
									table.addCell(cell1);

									cell1 = new PdfPCell(
											new Paragraph(rs.getString(4)
													+ "\n" + rs.getString(6),
													fonts[0]));
									cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
									cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
									cell1.setColspan(2);
									table.addCell(cell1);

									for (int z = 8; z <= 8 + (num - 1) * 4; z += 4) {
										if (rs.getString(z) != null)
											cell1 = new PdfPCell(
													new Paragraph(
															Decimalroundup
																	.twodecimalplaces(
																			rs.getFloat(z),
																			2),
															fonts[7]));
										cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
										cell1.setVerticalAlignment(Element.ALIGN_CENTER);
										cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
										// cell1.
										table.addCell(cell1);
									}
								}

								else {
									for (int z = 1; z <= tableSize; z++) {
										if (rs.getString(z) != null)
											cell1 = new PdfPCell(new Paragraph(
													rs.getString(z), fonts[0]));
										cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
										cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
										table.addCell(cell1);
									}
								}
							}
							addtable = true;
							adddata = false;
						}
						if (addtable && !addfulltable) {
							if (table3Height < TableHeightLower) {
								table1.addCell(table);
								maintable1.addCell(table);
							}

							else if (table3Height > TableHeightLower
									&& table3Height < TableHeightUpper) {
								table2.addCell(table);
								maintable2.addCell(table);

								if (pretable3Height < TableHeightLower) {
									diff = table3Height - pretable3Height;
									count = TableHeightLower - pretable3Height;
								}
							}

							cella = new PdfPCell(table1);
							cella.setBorder(Rectangle.NO_BORDER);
							// cella.setPaddingLeft(20);
							table3.addCell(cella);

							cella = new PdfPCell(table2);
							cella.setBorder(Rectangle.NO_BORDER);
							table3.addCell(cella);

							maintable3.setWidthPercentage(90);
							table3.setWidthPercentage(90);
							document.add(table3);

							pretable3Height = table3Height;
							table3Height += table3.getTotalHeight();
							if ((pretable3Height < table3Height && table3Height < TableHeightUpper)
									&& (section.getPagebreakAfter()
											.equals("yes"))) {
								table3Height = TableHeightUpper;
								TableHeightLower = 610;
							}
							if (table3Height > TableHeightLower)
								bt1 = true;
							if (table3Height >= TableHeightUpper)
								bt2 = true;

							table1.deleteBodyRows();
							table2.deleteBodyRows();
							table3.deleteBodyRows();
						}
						addtable = false;
						t = 0;

						Onbullet = "";
						if (section.getNumber().getType().equals("notshow"))
							titleText = ""; // for first paragraph "General"
						if (section.getNumber().getType().equals("bullet"))
							Onbullet = "\u2022 ";
						// PdfAction pa = new PdfAction();
						Paragraph para = new Paragraph(
								fonts[0].getCalculatedLeading(1), Onbullet + ""
										+ contentText, fonts[0]);
						para.setAlignment(Element.ALIGN_JUSTIFIED);

						fonts[2] = new Font(Font.HELVETICA, 7, Font.BOLD);
						Paragraph paranumber = new Paragraph(
								fonts[2].getCalculatedLeading(2), numberText
										+ "  " + titleText, fonts[2]);
						paranumber.setAlignment(Element.ALIGN_LEFT);
						cellnode = new PdfPCell(para);
						cellnode.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cellnode.setBorder(Rectangle.NO_BORDER);
						cellnode.setPaddingLeft(5f);
						cellnode.setPaddingRight(5f);

						cellnumber = new PdfPCell(paranumber);
						cellnumber
								.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cellnumber.setBorder(Rectangle.NO_BORDER);
						cellnumber.setPaddingLeft(5f);
						cellnumber.setPaddingRight(5f);

						if (table3Height < TableHeightLower) {
							if (!titleText.equals("")) {
								table1.addCell(cellnumber);
								maintable1.addCell(cellnumber);
							}
							table1.addCell(cellnode);
							maintable1.addCell(cellnode);
						}

						else if (table3Height > TableHeightLower
								&& table3Height < TableHeightUpper) {
							if (!titleText.equals("")) {
								table2.addCell(cellnumber);
								maintable2.addCell(cellnumber);
							}

							if (pretable3Height < TableHeightLower) {
								diff = table3Height - pretable3Height;
								count = TableHeightLower - pretable3Height;
							}

							table2.addCell(cellnode);
							maintable2.addCell(cellnode);
						}

						cella = new PdfPCell(table1);
						cella.setBorder(Rectangle.NO_BORDER);
						table3.addCell(cella);

						cella = new PdfPCell(table2);
						cella.setBorder(Rectangle.NO_BORDER);
						table3.addCell(cella);

						maintable3.setWidthPercentage(90);
						table3.setWidthPercentage(90);
						document.add(table3);

						pretable3Height = table3Height;
						table3Height += table3.getTotalHeight();
						if ((pretable3Height < table3Height && table3Height < TableHeightUpper)
								&& (section.getPagebreakAfter().equals("yes"))) {
							table3Height = TableHeightUpper;
							TableHeightLower = 610;
						}
						if (table3Height > TableHeightLower)
							bt1 = true;
						if (table3Height >= TableHeightUpper)
							bt2 = true;

						table1.deleteBodyRows();
						table2.deleteBodyRows();
						table3.deleteBodyRows();
					}
				}
			}
		} catch (Exception de) {
			logger.error(
					"viewNetmedxHtml(String, String, String, String, String, String, DataSource)",
					de);
		}

		finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(stmt);
			DBUtil.close(conn);
		}

		document.close();
		maindocument.close();
		return outbuff.toByteArray();
	}
}