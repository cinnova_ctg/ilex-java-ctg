/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class used for generating Standard MSA in rtf format.      
 *
 */

package com.mind.xml;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.table.RtfBorder;
import com.lowagie.text.rtf.table.RtfBorderGroup;
import com.lowagie.text.rtf.table.RtfCell;
import com.mind.common.Decimalroundup;
import com.mind.dao.IM.IMdao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.xml.doc.Section;

/**
 * Methods : viewmsartf
 */

public class MSANetMedXRtf extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MSANetMedXRtf.class);

	/**
	 * This method return Byte Array for Standard MSA in rtf.
	 * 
	 * @param id
	 *            String. msa Id
	 * @param type
	 *            String. type msa/appendix
	 * @param fileName
	 *            String. path of xml file
	 * @param imagePath
	 *            String. path of logo
	 * @param ds
	 *            Object of DataSource
	 * @return Byte Array This method returns byte array, for standard msa in
	 *         rtf.
	 */

	public byte[] viewNetmedxRtf(String id, String type, String fileName,
			String imagePath, String fontPath, String loginUserId, DataSource ds) {

		Document maindocument = new Document(PageSize.A4, 1, 1, 20, 20);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();

		Connection conn = null;
		Statement stmt = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String footertext = "   Confidential & Proprietary \n   \u00a9 2009 Contingent Network Services, LLC \n\t\t\t\t\t\t\t   Page ";

			RtfWriter2 writer = RtfWriter2.getInstance(maindocument, outbuff);

			/* Set font array */
			BaseFont bfArialN = BaseFont.createFont(fontPath,
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

			Font[] fonts = new Font[14];
			fonts[0] = new Font(bfArialN, 8.9f, Font.NORMAL); // for paragraph
																// content
			fonts[1] = new Font(bfArialN, 7, Font.BOLD); // for page heading
			fonts[2] = new Font(bfArialN, 8, Font.BOLD); // not used
			fonts[3] = new Font(bfArialN, 8.5f, Font.NORMAL); // for header row
			fonts[4] = new Font(Font.TIMES_ROMAN, 7, Font.NORMAL); // for footer
																	// row
			fonts[5] = new Font(bfArialN, 7, Font.NORMAL); // use as variable
			fonts[6] = new Font(bfArialN, 10, Font.BOLD); // for main heading
			fonts[7] = new Font(bfArialN, 8, Font.BOLD); // for heading
			fonts[8] = new Font(bfArialN, 8.5f, Font.UNDERLINE | Font.BOLD); // for
																				// underline
																				// heading
			fonts[11] = new Font(bfArialN, 11.5f, Font.BOLD);
			fonts[12] = new Font(bfArialN, 8, Font.NORMAL);
			fonts[13] = null;

			Table headertable = new Table(1);
			headertable.setWidth(95);
			headertable.setBorderWidth(0);
			headertable.setBorder(Rectangle.NO_BORDER);
			headertable.setBorderColor(new Color(255, 255, 255));

			Table maintable = new Table(1);
			maintable.setWidth(95);

			Table table = new Table(1);
			table.setWidth(95);
			table.setBorder(1);

			RtfCell cell1 = new RtfCell();
			RtfCell cell2 = new RtfCell();
			RtfCell cell0 = new RtfCell();
			RtfCell cellt = new RtfCell();

			Image logo = Image.getInstance(imagePath);

			float aspectRatio = 381.0f / 148.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewNetmedxRtf(String, String, String, String, String, String, DataSource) - "
						+ aspectRatio);
			}
			float y = 112.39f;
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			logo.setAlignment(Image.MIDDLE);

			Chunk chunk = new Chunk(logo, 0, -20);
			HeaderFooter header = new HeaderFooter(new Phrase(chunk), false);
			HeaderFooter footer = new HeaderFooter(new Phrase(footertext,
					fonts[4]), true);

			header.setAlignment(Element.ALIGN_CENTER);
			footer.setAlignment(Element.ALIGN_LEFT);
			maindocument.setHeader(header);
			maindocument.setFooter(footer);

			maindocument.open();
			maindocument.setMargins(20, 20, 5, 5); // set document margin

			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(fileName, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator it = list1.iterator();

			String jobId = "0";

			int i = 0;
			boolean addtable = false;
			String titleText = "";
			String numberText = "";
			String contentText = "";
			String numberType = "";
			String titleType = "";
			String contentType = "";
			String Onbullet = "";
			String Title = "";
			String Content = "";
			int colspan = 0;
			int rowspan = 0;
			int tableSize = 0;
			int t = 0;
			boolean adddata = false;
			String Type = "";
			int k = 0;
			int count = 0;
			boolean logistics = false;
			Table tableLogistic = null;
			boolean addLogistics = false;
			String sql = "";
			int num = 0;
			String[][] resourcelabel = null;
			int listIndex = 0;
			String contentLabel = "";

			String msaid = IMdao.getMSAId(id, ds);
			if (msaid.equals(""))
				msaid = null;

			while (it.hasNext()) {
				titleText = "";
				numberText = "";
				contentText = "";
				section = (Section) it.next();

				// numberType = xt.getNumberTypeAttribute();
				// titleType = xt.getTitleTypeAttribute();
				// contentType = xt.getContentTypeAttribute();
				numberType = section.getNumber().getType();
				titleType = section.getTitle().getType();
				contentType = section.getContent().getType();

				titleText = section.getTitle().getContent();
				if (titleText.indexOf("~") >= 0)
					titleText = addParameter(titleText, id, type, loginUserId,
							jobId, ds);

				numberText = section.getNumber().getContent();
				if (numberText.indexOf("~") >= 0)
					numberText = addParameter(numberText, id, type,
							loginUserId, jobId, ds);

				contentText = section.getContent().getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = addParameter(contentText, id, type,
							loginUserId, jobId, ds);
				/* Header rows start */

				// Code for check that Discount is 0 or not start.
				if (contentText.equals("NetMedX Standard Extended Discount")) {
					try {
						stmt = conn.createStatement();
						// String
						// str="select clr_discount from clr_master where lp_mm_id = "+msaid;
						String str = "select dbo.func_get_netmedx_discount_rate("
								+ msaid + ", " + id + ")";
						rs = stmt.executeQuery(str);
						if (rs.next()) {
							if (rs.getInt(1) != 0) {
								contentText = "NetMedX Standard Extended Discount  "
										+ rs.getInt(1) + "%";
							} else {
								contentText = "";
							}
						} else {
							contentText = "";
						}
					} catch (Exception e) {
						logger.error(
								"viewNetmedxRtf(String, String, String, String, String, String, DataSource)",
								e);

						if (logger.isDebugEnabled()) {
							logger.debug("viewNetmedxRtf(String, String, String, String, String, String, DataSource) - Exception Occured getting Netmedix Discount"
									+ e);
						}
					} finally {
						DBUtil.close(rs, stmt);
					}
				}
				// Code for check that Discount is 0 or not end

				if (i == 0) {
					Paragraph para = new Paragraph(
							fonts[5].getCalculatedLeading(1), titleText,
							fonts[11]);
					cell1 = new RtfCell(para);
					cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell1.setBottom(10f);

					para = new Paragraph(fonts[5].getCalculatedLeading(1.5f),
							contentText, fonts[7]);
					cell2 = new RtfCell(para);
					cell2.setHorizontalAlignment(Element.ALIGN_CENTER);

					para = new Paragraph(fonts[0].getCalculatedLeading(1.5f),
							numberText, fonts[0]);
					cell0 = new RtfCell(para);
					cell0.setHorizontalAlignment(Element.ALIGN_RIGHT);

					headertable.addCell(cell1);
					headertable.addCell(cell2);
					headertable.addCell(cell0);
					maindocument.add(headertable);

					section = (Section) it.next();
					titleText = section.getTitle().getContent();
					if (titleText.indexOf("~") >= 0)
						titleText = addParameter(titleText, id, type,
								loginUserId, jobId, ds);
					numberText = section.getNumber().getContent();
					if (numberText.indexOf("~") >= 0)
						numberText = addParameter(numberText, id, type,
								loginUserId, jobId, ds);
					contentText = section.getContent().getContent();
					if (contentText.indexOf("~") >= 0)
						contentText = addParameter(contentText, id, type,
								loginUserId, jobId, ds);
					numberType = section.getNumber().getType();
				}
				/* Header rows end */
				i++;

				if (i != 0) {
					if (titleType.equals("table")) {
						Type = "";
						contentType = section.getContent().getType();
						numberType = section.getNumber().getType();

						if ((contentType.equals("iv_criticality"))
								|| (contentType.equals("iv_view_restime_goal"))
								|| (contentType.equals("iv_distance_restime"))
								|| (contentType.equals("iv_cns_call_res_price"))
								|| (contentType.equals("iv_cns_nw_price"))
								|| (contentType.equals("iv_cns_field_services"))
								|| (contentType
										.equals("iv_cns_logistics_services")
										|| (contentType
												.equals("iv_cns_field_services_silver")) || contentType
											.equals("iv_cns_field_services_copper"))) {
							Type = contentType;
							colspan = section.getTitle().getColspan()
									.intValue();
							rowspan = section.getTitle().getRowspan()
									.intValue();
							tableSize = section.getTitle().getSize().intValue();

							if (t == 0) {
								if (contentType.equals("iv_cns_field_services")
										|| contentType
												.equals("iv_cns_field_services_silver")
										|| contentType
												.equals("iv_cns_field_services_copper")) {
									stmt = conn.createStatement();

									if (contentType
											.equals("iv_cns_field_services_copper")) {
										contentLabel = "iv_cns_field_services";
									} else {
										contentLabel = contentType;
									}

									sql = "select count(*) from (select distinct clr_class_id, clr_reslevel_id from func_iv_clr_show_resourcelevel("
											+ id
											+ ",'"
											+ contentLabel
											+ "') where clr_reslevel_show_on_doc = 'Y') as num";
									rs = stmt.executeQuery(sql);

									if (rs.next()) {
										num = rs.getInt(1);
									}

									resourcelabel = new String[num][2];
									sql = "select distinct clr_reslevel_name, clr_reslevel_id, clr_class_id  from func_iv_clr_show_resourcelevel("
											+ id
											+ ",'"
											+ contentLabel
											+ "') where clr_reslevel_show_on_doc = 'Y'"
											+ "order by clr_class_id, clr_reslevel_id";

									rs = stmt.executeQuery(sql);
									listIndex = 0;
									while (rs.next()) {
										resourcelabel[listIndex][0] = rs
												.getString("clr_class_id");
										resourcelabel[listIndex][1] = rs
												.getString("clr_reslevel_name");
										listIndex++;
									}

									if (contentType
											.equals("iv_cns_field_services")
											|| contentType
													.equals("iv_cns_field_services_silver")
											|| contentType
													.equals("iv_cns_field_services_copper")) {
										tableSize = tableSize + num + 2;
										colspan = colspan + num + 2;
									}
								}
								/* END */

								table = new Table(tableSize);
								if (contentType
										.equals("iv_cns_logistics_services")) {
									tableLogistic = new Table(tableSize);
									tableLogistic.setBorder(1);
									table.setWidths(new float[] { 25.0f, 25.0f,
											15.0f, 35.0f });
								}
							}

							Paragraph para = new Paragraph(titleText, fonts[6]);
							cell1 = new RtfCell(para);
							cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell1.setColspan(colspan);
							cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
									RtfBorder.BORDER_SINGLE, 1, new Color(0, 0,
											0)));
							table.addCell(cell1);
							adddata = true;
							t++;
						}
					} else {
						if (adddata) {
							if (Type.equals("iv_criticality")
									|| Type.equals("iv_view_restime_goal")
									|| Type.equals("iv_distance_restime")
									|| Type.equals("iv_cns_call_res_price")
									|| Type.equals("iv_cns_nw_price")
									|| Type.equals("iv_cns_field_services")
									|| Type.equals("iv_cns_logistics_services")
									|| Type.equals("iv_cns_field_services_silver")
									|| Type.equals("iv_cns_field_services_copper")) {
								sql = "";
								stmt = conn.createStatement();
								if (Type.equals("iv_criticality")) {
									sql = "select iv_crit_name, iv_crit_condition from iv_criticality";
									rs = stmt.executeQuery(sql);
								} else if (Type.equals("iv_cns_call_res_price")) {
									sql = "select iv_cns_res_name, iv_cns_price_per_min_pps, iv_cns_price_per_min_nonpps from iv_cns_call_res_price";
									rs = stmt.executeQuery(sql);
								} else if (Type.equals("iv_distance_restime")) {
									sql = "select iv_distance_value, iv_response_time from iv_distance_restime";
									rs = stmt.executeQuery(sql);
								} else if (Type.equals("iv_cns_nw_price")) {
									sql = "select iv_cns_nw_item_name, iv_cns_nw_item_price from iv_cns_nw_price";
									rs = stmt.executeQuery(sql);
								}

								else if (Type
										.equals("iv_cns_logistics_services")) {
									sql = "select iv_ls_activity, iv_ls_desc, iv_ls_rates, iv_ls_addtional_info from iv_cns_logistics_services";
									rs = stmt.executeQuery(sql);
									logistics = true;
								}

								else if (Type.equals("iv_view_restime_goal")) {
									cstmt = conn
											.prepareCall("{?=call iv_view_restime_goal()}");
									cstmt.registerOutParameter(1,
											java.sql.Types.INTEGER);
									rs = cstmt.executeQuery();
								} else if (Type.equals("iv_cns_field_services")
										|| Type.equals("iv_cns_field_services_silver")
										|| Type.equals("iv_cns_field_services_copper")) {
									// for showing discounted rates
									cstmt = conn
											.prepareCall("{?=call iv_clr_tab_03_show_on_doc(?, ?, ?)}");
									cstmt.registerOutParameter(1,
											java.sql.Types.INTEGER);
									cstmt.setString(2, id);
									cstmt.setString(3, "0");
									cstmt.setString(4, contentLabel);
									rs = cstmt.executeQuery();

									// Row 1
									table.addCell(getNewRtfCell("", 2, fonts[6]));
									if (Type.equals("iv_cns_field_services_silver")) {
										table.addCell(getNewRtfCell(
												"PLEASE NOTE: THESE ARE PREPAID DISPATCH RATES (PER VISIT)",
												3, fonts[6]));
										table.addCell(getNewRtfCell(
												"Per Dispatch Visit Discounted Rates"
														+ "\u00B9", num,
												fonts[6]));
									} else if (Type
											.equals("iv_cns_field_services_copper")) {
										table.addCell(getNewRtfCell(
												"PLEASE NOTE: THESE ARE DISPATCH RATES (PER VISIT)",
												3, fonts[6]));
										table.addCell(getNewRtfCell(
												"Per Dispatch Visit Rates (1 Hr Max Onsite)",
												num, fonts[6]));
									} else {
										table.addCell(getNewRtfCell("", 2,
												fonts[6]));
										table.addCell(getNewRtfCell(
												"Hourly Labor Rates" + "\u00B9",
												num, fonts[6]));
									}

									// Row 2
									table.addCell(getNewRtfCell("", 2, fonts[6]));
									if (Type.equals("iv_cns_field_services_silver")) {
										table.addCell(getNewRtfCell(
												"THESE ARE NOT PREPAID HOURLY RATES",
												3, fonts[6]));
									} else if (Type
											.equals("iv_cns_field_services_copper")) {
										table.addCell(getNewRtfCell("", 3,
												fonts[6]));
									} else {
										table.addCell(getNewRtfCell("", 2,
												fonts[6]));
									}
									table.addCell(getNewRtfCell("Technician",
											2, fonts[6]));
									table.addCell(getNewRtfCell("Engineer", 2,
											fonts[6]));

									// Row 3
									table.addCell(getNewRtfCell("Activity", 2,
											fonts[6]));
									table.addCell(getNewRtfCell("Description",
											2, fonts[6]));

									if (Type.equals("iv_cns_field_services_silver")) {
										table.addCell(getNewRtfCell(
												"Penalty For Late Arrival", 1,
												fonts[6]));
									} else if (Type
											.equals("iv_cns_field_services_copper")) {
										table.addCell(getNewRtfCell(
												"Hourly Rate Above 1 hr Onsite"
														+ "\u00B9", 1, fonts[6]));
									}

									for (int z = 0; z < num; z++) {
										if (resourcelabel[z][1]
												.contains("Level 2"))
											resourcelabel[z][1] = "Level 2";
										if (resourcelabel[z][1]
												.contains("Level 3"))
											resourcelabel[z][1] = "Level 3";
										table.addCell(getNewRtfCell(
												resourcelabel[z][1], 1,
												fonts[6]));
									}
								}

								while (rs.next()) {
									if (Type.equals("iv_cns_field_services")
											|| Type.equals("iv_cns_field_services_silver")
											|| Type.equals("iv_cns_field_services_copper")) {
										if (contentType
												.equals("iv_cns_field_services_copper")
												&& rs.getString(1).contains(
														"Scheduled")) {
											cell1 = new RtfCell(
													new Paragraph(
															rs.getString(1)
																	.substring(
																			0,
																			rs.getString(
																					1)
																					.length() - 4)
																	+ "\n"
																	+ rs.getString(2),
															fonts[2]));
										} else {
											cell1 = new RtfCell(new Paragraph(
													rs.getString(1) + "\n"
															+ rs.getString(2),
													fonts[2]));
										}
										cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
										cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
										cell1.setColspan(2);
										cell1.setLeading(0.3f);
										cell1.setBorders(new RtfBorderGroup(
												Rectangle.BOX,
												RtfBorder.BORDER_SINGLE, 1,
												new Color(0, 0, 0)));
										table.addCell(cell1);

										if (Type.equals("iv_cns_field_services")) {
											cell1 = new RtfCell(new Paragraph(
													rs.getString(4) + "\n"
															+ rs.getString(6),
													fonts[7]));
										} else if (Type
												.equals("iv_cns_field_services_silver")) {
											if (rs.getString(4).equals(
													"Next Business Day")
													|| rs.getString(4).equals(
															"2nd Business Day")) {
												cell1 = new RtfCell(
														new Paragraph(
																rs.getString(4)
																		+ " \nFlat (2) hour onsite maximum plus (1) hour travel\n"
																		+ rs.getString(6),
																fonts[7]));
											} else {
												cell1 = new RtfCell(
														new Paragraph(
																rs.getString(4)
																		+ " Flat (2) hour onsite maximum plus (1) hour travel\n"
																		+ rs.getString(6),
																fonts[7]));
											}
										} else if (Type
												.equals("iv_cns_field_services_copper")) {
											if (rs.getString(4).equals(
													"Next Business Day")
													|| rs.getString(4).equals(
															"2nd Business Day")) {
												cell1 = new RtfCell(
														new Paragraph(
																rs.getString(4)
																		+ " \nFlat (1) hour onsite maximum\n"
																		+ rs.getString(6),
																fonts[7]));
											} else {
												cell1 = new RtfCell(
														new Paragraph(
																rs.getString(4)
																		+ " Flat (1) hour onsite maximum\n"
																		+ rs.getString(6),
																fonts[7]));
											}
										}

										cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
										cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
										cell1.setColspan(2);
										cell1.setLeading(0.3f);
										cell1.setBorders(new RtfBorderGroup(
												Rectangle.BOX,
												RtfBorder.BORDER_SINGLE, 1,
												new Color(0, 0, 0)));
										table.addCell(cell1);

										if (Type.equals("iv_cns_field_services_silver")) {
											cell1 = new RtfCell(new Paragraph(
													"15%", fonts[7]));
											cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
											cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
											cell1.setBorders(new RtfBorderGroup(
													Rectangle.BOX,
													RtfBorder.BORDER_SINGLE, 1,
													new Color(0, 0, 0)));
											table.addCell(cell1);
										} else if (Type
												.equals("iv_cns_field_services_copper")) {
											cell1 = new RtfCell(
													new Paragraph(
															"Billable at NetMedX Standard Hourly Rates",
															fonts[7]));
											cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
											cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
											cell1.setBorders(new RtfBorderGroup(
													Rectangle.BOX,
													RtfBorder.BORDER_SINGLE, 1,
													new Color(0, 0, 0)));
											table.addCell(cell1);

										}

										for (int z = 8; z <= 8 + (num - 1) * 4; z += 4) {
											if (rs.getString(z) != null) {
												if (contentType
														.equals("iv_cns_field_services_copper")) {
													cell1 = new RtfCell(
															new Paragraph(
																	"$"
																			+ Decimalroundup
																					.twodecimalplaces(
																							(2 * rs.getFloat(z)),
																							2),
																	fonts[7]));
												} else {
													cell1 = new RtfCell(
															new Paragraph(
																	"$"
																			+ Decimalroundup
																					.twodecimalplaces(
																							rs.getFloat(z),
																							2),
																	fonts[7]));
												}
												cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
												cell1.setVerticalAlignment(Element.ALIGN_CENTER);
												cell1.setBorders(new RtfBorderGroup(
														Rectangle.BOX,
														RtfBorder.BORDER_SINGLE,
														1, new Color(0, 0, 0)));
												table.addCell(cell1);
											}
										}
									} else {
										for (int z = 1; z <= tableSize; z++) {
											if (logistics == true
													&& (z == 1 || z == 2)) {
												fonts[13] = new Font(bfArialN,
														8, Font.BOLD);
											} else {
												fonts[13] = new Font(bfArialN,
														8, Font.NORMAL);
											}

											if (rs.getString(z) != null) {
												if (logistics == true && z == 4) {
													cellt = new RtfCell(
															new Paragraph(
																	(rs.getString(z)
																			.replaceAll(
																					"~",
																					"\u2022 "))
																			.trim(),
																	fonts[13]));
												} else {
													cellt = new RtfCell(
															new Paragraph(
																	rs.getString(z),
																	fonts[13]));
												}
											}
											cellt.setHorizontalAlignment(Element.ALIGN_CENTER);
											cellt.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
											cellt.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
											cellt.setLeading(0.5f);
											cellt.setBorders(new RtfBorderGroup(
													Rectangle.BOX,
													RtfBorder.BORDER_SINGLE, 1,
													new Color(0, 0, 0)));
											table.addCell(cellt);
										}
									}
								}

								if (rs != null) {
									rs.close();
								}
								if (stmt != null) {
									stmt.close();
								}
								if (cstmt != null) {
									cstmt.close();
								}
								adddata = false;
								addtable = true;
							}
						}

						if (addtable) {
							// Commented one
							if (!Type.equals("iv_cns_logistics_services")) {
								maindocument.add(maintable);
							}

							if (Type.equals("iv_cns_field_services")
									|| Type.equals("iv_cns_field_services_silver")
									|| Type.equals("iv_cns_logistics_services")
									|| Type.equals("iv_cns_field_services_copper")) {
								// if(Type.equals("iv_cns_logistics_services")
								// ||
								// Type.equals("iv_cns_field_services_silver"))
								// {
								if (Type.equals("iv_cns_field_services")) {
									maindocument.newPage();
								}
								table.setWidth(95);
							} else {
								table.setWidth(60);
							}

							if (Type.equals("iv_view_restime_goal")) {
								maindocument.newPage();
							}

							maindocument.add(table);

							table = new Table(1);
							table.setBorder(1);

							addtable = false;
							maintable = new Table(1);
							maintable.setWidth(95);
							t = 0;
						}

						if (numberText.equals(""))
							Title = titleText;
						else
							Title = numberText + titleText;
						Paragraph paranumber = new Paragraph(
								fonts[7].getCalculatedLeading(1), "\n" + Title,
								fonts[7]);
						paranumber.setIndentationLeft(4);
						paranumber.setAlignment(Element.ALIGN_JUSTIFIED);
						paranumber.setIndentationRight(6);
						paranumber.setLeading(.5f);

						Onbullet = "";

						if (section.getNumber().getType().equals("notshow"))
							titleText = ""; // for first paragraph "General"
						if (section.getNumber().getType().equals("bullet"))
							Onbullet = "\u2022";

						Content = Onbullet + " " + contentText;
						Paragraph paracontent = new Paragraph(
								fonts[0].getCalculatedLeading(.2f), "\n"
										+ Content, fonts[0]);

						paracontent.setIndentationLeft(4);
						paracontent.setAlignment(Element.ALIGN_JUSTIFIED);
						paracontent.setIndentationRight(4);
						paracontent.setLeading(.5f);

						if (section.getContent().getAlign().equals("right")) {
							paracontent.setAlignment(Element.ALIGN_RIGHT);
						}

						count++;

						if (!titleText.equals("")) {
							RtfCell cell3 = new RtfCell();
							cell3.add(paranumber);
							cell3.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cell3.setBorderWidth(0);
							cell3.setBorder(Rectangle.NO_BORDER);
							maintable.addCell(cell3);
						}

						if (!contentText.equals("")) {
							RtfCell cell4 = new RtfCell();
							cell4.add(paracontent);
							cell4.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cell4.setBorderWidth(0);
							cell4.setBorder(Rectangle.NO_BORDER);

							if (section.getContent().getAlign().equals("right")) {
								cell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
							}
							maintable.addCell(cell4);
						}
					}
				}
			}
			maindocument.add(maintable);

		} catch (Exception de) {
			logger.error(
					"viewNetmedxRtf(String, String, String, String, String, String, DataSource)",
					de);

			logger.error(
					"viewNetmedxRtf(String, String, String, String, String, String, DataSource)",
					de);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(stmt);
			DBUtil.close(conn);
		}

		maindocument.close();
		return outbuff.toByteArray();
	}

	public static RtfCell getNewRtfCell(String label, int colspan, Font font) {
		RtfCell cell1 = null;
		try {
			cell1 = new RtfCell(new Paragraph(label, font));
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setColspan(colspan);
			cell1.setLeading(0.3f);
			cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
					RtfBorder.BORDER_SINGLE, 1, new Color(0, 0, 0)));
		} catch (Exception e) {
			logger.error("getNewRtfCell(String, int, Font)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNewRtfCell(String, int, Font) - Error occurred while creating Rtf Cell::"
						+ e);
			}
		}
		return cell1;
	}
}
