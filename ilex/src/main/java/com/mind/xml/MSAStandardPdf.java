/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class used for generating Standard MSA in pdf format.      
 *
 */

package com.mind.xml;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.MultiColumnText;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;

/**
 * Methods : viewmsa
 */

public class MSAStandardPdf extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MSAStandardPdf.class);

	/**
	 * This method return Byte Array for Standard MSA in pdf.
	 * 
	 * @param id
	 *            String. msa Id
	 * @param type
	 *            String. type msa/appendix
	 * @param filename
	 *            String. path of xml file
	 * @param imagepath
	 *            String. path of logo
	 * @param temp_filepath
	 *            String. path for string temp pdf
	 * @param ds
	 *            Object of DataSource
	 * @return Byte Array This method returns byte array, for standard msa in
	 *         pdf.
	 */
	public byte[] viewMsaPdf(String id, String type, String filename,
			String imagepath, String fontPath, String login_userid,
			DataSource ds) {

		Document document = new Document(PageSize.LETTER, 1, 1, 20, 20);
		Document maindocument = new Document(PageSize.LETTER, 1, 1, 20, 20);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();

		try {
			BaseFont bfArialN = BaseFont.createFont(fontPath,
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			Font[] fonts = new Font[9];
			fonts[0] = new Font(bfArialN, 8f, Font.NORMAL);
			fonts[2] = new Font(bfArialN, 8f, Font.BOLD);
			fonts[5] = new Font(bfArialN, 8f, Font.ITALIC);
			fonts[6] = new Font(bfArialN, 7f, Font.NORMAL, Color.LIGHT_GRAY);
			fonts[7] = new Font(bfArialN, 7f, Font.NORMAL);
			fonts[8] = new Font(bfArialN, 7f, Font.BOLD);

			PdfWriter.getInstance(document,
					new FileOutputStream(System.getProperty("ilex.temp.path")
							+ "/tempMSAstandradpdf.pdf"));
			PdfWriter writermain = PdfWriter.getInstance(maindocument, outbuff);

			maindocument.setMargins(10, 10, 5, 20); // set document margin

			Image logo = Image.getInstance(imagepath);

			float aspectRatio = 305.0f / 110.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewMsaPdf(String, String, String, String, String, String, String, DataSource) - "
						+ aspectRatio);
			}
			float y = 55f;
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			logo.setAlignment(Image.MIDDLE);

			Chunk chunk = new Chunk(logo, 0, -71);

			HeaderFooter header = new HeaderFooter(new Phrase(61F, chunk),
					false);
			/*
			 * added spaces to maintain the footer.
			 */
			String footerText1 = String
					.format("             Copyright "
							+ Calendar.getInstance().get(Calendar.YEAR)
							+ " \u00a9 Comcast Enterprise Services, LLC                                                                                    ",
							new String());
			String footerText2 = String.format(" | Page",
					document.getPageNumber())
					+ "                                                                                                                          Confidential & Proprietary";

			HeaderFooter footer = new HeaderFooter(new Phrase(footerText1,
					fonts[6]), (new Phrase(footerText2, fonts[6])));
			header.setAlignment(Element.ALIGN_CENTER);
			header.setBorder(Rectangle.NO_BORDER);
			footer.setAlignment(Element.ALIGN_LEFT);
			footer.setBorder(Rectangle.NO_BORDER);
			footer.setLeft(5F);

			maindocument.setHeader(header);
			maindocument.setFooter(footer);

			document.open();
			maindocument.open();

			PdfDestination pd = new PdfDestination(PdfDestination.XYZ, -1, -1,
					1);
			PdfAction action = PdfAction.gotoLocalPage(1, pd, writermain);
			writermain.setOpenAction(action);

			int setHeaderHeight = 71;

			MultiColumnText mct = new MultiColumnText(document.top(), 655);
			mct.addElement(new Phrase(chunk));
			mct.addRegularColumns(document.left() + 30, document.right() - 30,
					18f, 2);
			/*
			 * 3rd parameter of addRegularColumns maintaining space between two
			 * columns, we have to update this value in all addRegularColumns
			 * functions to display symmetrical.
			 */

			MultiColumnText mctmain = null;

			int i = 0;
			String jobId = "0";
			String Onbullet = "";
			Title title = new Title();
			Number number = new Number();
			Content content = new Content();
			String titleText = "";
			String numberText = "";
			String contentText = "";
			PdfPTable headertable = new PdfPTable(2);
			PdfPTable headertable2 = null;
			PdfPTable singleParaHeadertable = null;
			PdfPCell singleParaCell = new PdfPCell();
			PdfPCell cellForMSAText = new PdfPCell();
			PdfPCell cellProjectIdentifier = new PdfPCell();

			PdfPCell cellForHeading = new PdfPCell();
			PdfPCell cellForContentText = new PdfPCell();

			int numberOfColumns = 0;

			PdfPTable dataTable = null;

			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(filename, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			List<Section> list1 = doc.getSections().getSection();
			Iterator<Section> it = list1.iterator();

			boolean isTableHeaderAdded = false;

			while (it.hasNext()) {

				section = it.next();

				// Add embedded parameter start

				if ("new_page".equals(section.getType())) {
					document.add(mct);
					maindocument.add(mctmain);

					while (mct.isOverflow()) {
						mct.nextColumn();
						document.newPage();
						document.add(mct);
					}

					while (mctmain.isOverflow()) {
						mctmain.nextColumn();
						maindocument.newPage();
						maindocument.add(mctmain);
					}

					document.newPage();
					mct.nextColumn();
					maindocument.newPage();
					mctmain.nextColumn();

				}

				title = section.getTitle();

				titleText = title.getContent();

				if (titleText.indexOf("~") >= 0)
					titleText = addParameter(titleText, id, type, login_userid,
							jobId, ds);
				number = section.getNumber();
				numberText = number.getContent().trim();
				if (numberText.indexOf("~") >= 0)
					numberText = addParameter(numberText, id, type,
							login_userid, jobId, ds);
				content = section.getContent();
				contentText = content.getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = addParameter(contentText, id, type,
							login_userid, jobId, ds);

				String headingText = titleText.trim() + " ";
				if (!"".equals(numberText.trim())) {
					/* adding extra space with numbering bullets. */
					headingText = String.format(numberText.trim() + "         "
							+ titleText.trim() + " ", new String());
					if (numberText.length() > 4) {
						headingText = String.format(numberText.trim()
								+ "        " + titleText.trim() + " ",
								new String());
					}
				}

				if (i == 0) {
					Paragraph paraForTopHeading = new Paragraph(
							fonts[0].getCalculatedLeading(4), headingText,
							fonts[2]);

					cellForMSAText = new PdfPCell(paraForTopHeading);
					cellForMSAText.setHorizontalAlignment(Element.ALIGN_CENTER);
					cellForMSAText.setBorder(Rectangle.NO_BORDER);
					cellForMSAText.setColspan(2);

					paraForTopHeading = new Paragraph(
							fonts[0].getCalculatedLeading(4), contentText,
							fonts[0]);
					cellProjectIdentifier = new PdfPCell(paraForTopHeading);
					cellProjectIdentifier
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					cellProjectIdentifier.setBorder(Rectangle.NO_BORDER);
					cellProjectIdentifier.setColspan(2);

					headertable.addCell(cellForMSAText);
					headertable.addCell(cellProjectIdentifier);
					maindocument.add(headertable);

					section = (Section) it.next();
					title = section.getTitle();
					titleText = title.getContent();
					if (titleText.indexOf("~") >= 0)
						titleText = addParameter(titleText, id, type,
								login_userid, jobId, ds);
					number = section.getNumber();
					numberText = number.getContent().trim();
					if (numberText.indexOf("~") >= 0)
						numberText = addParameter(numberText, id, type,
								login_userid, jobId, ds);

					content = section.getContent();
					contentText = content.getContent();
					if (contentText.indexOf("~") >= 0)
						contentText = addParameter(contentText, id, type,
								login_userid, jobId, ds);

					mctmain = new MultiColumnText(document.top()
							- setHeaderHeight, 655);
					mctmain.addRegularColumns(document.left() + 30,
							document.right() - 30, 18f, 2);
				}
				i++;

				if ("new_page".equals(section.getType())) {
					Paragraph paraForHeading = new Paragraph(
							fonts[2].getCalculatedLeading(4), headingText,
							fonts[2]);

					paraForHeading.setSpacingBefore(15f);
					cellForHeading = new PdfPCell(paraForHeading);
					cellForHeading.setHorizontalAlignment(Element.ALIGN_CENTER);
					cellForHeading.setBorder(Rectangle.NO_BORDER);
					cellForHeading.setColspan(2);

					fonts[0] = new Font(bfArialN, 8f, Font.NORMAL);
					Paragraph paraForContentText = new Paragraph(
							fonts[0].getCalculatedLeading(4), contentText,
							fonts[0]);

					cellForContentText = new PdfPCell(paraForContentText);
					cellForContentText
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					cellForContentText.setBorder(Rectangle.NO_BORDER);

					cellForContentText.setColspan(2);
					headertable2 = new PdfPTable(2);
					headertable2.addCell(cellForHeading);
					headertable2.addCell(cellForContentText);
					maindocument.add(headertable2);

				} else if ("singlePara".equals(section.getContent().getType())) {

					Paragraph heading = null;
					heading = new Paragraph(
							fonts[2].getCalculatedLeading(1.13F), headingText,
							fonts[2]);

					Paragraph para = null;

					if ("smallFont".equals(content.getStyle())) {
						para = new Paragraph(
								fonts[7].getCalculatedLeading(1.13F),
								contentText, fonts[7]);
					} else {
						para = new Paragraph(
								fonts[0].getCalculatedLeading(1.13F),
								contentText, fonts[0]);
					}

					singleParaCell = new PdfPCell();
					singleParaCell.addElement(heading);
					singleParaCell.addElement(para);
					singleParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					singleParaCell.setBorder(Rectangle.NO_BORDER);
					singleParaCell.setColspan(2);
					singleParaCell.setTop(2f);
					singleParaCell.setPaddingBottom(10F);

					singleParaHeadertable = new PdfPTable(2);
					singleParaHeadertable.addCell(singleParaCell);
					singleParaHeadertable.setWidthPercentage(94);
					maindocument.add(singleParaHeadertable);
				} else if ("table_header".equals(section.getType())) {

					title = section.getTitle();
					/* Setting up with of 1st table. */
					float[] columnWidths = new float[] { 12f, 25f, 15f, 8f, 5f,
							40f };

					String[] tableHeader = title.getContent().split("~\\|~");
					numberOfColumns = tableHeader.length;

					if ("ColSpan".equals(title.getType())) {
						numberOfColumns = 7;
						/* Setting up with of 2nd table. */
						columnWidths = new float[] { 8f, 10f, 35f, 20f, 5f, 8f,
								8f };

					}

					dataTable = new PdfPTable(numberOfColumns);
					dataTable.setWidths(columnWidths);
					dataTable.setWidthPercentage(93);
					dataTable.setSpacingAfter(10F);
					if ("color_gray".equals(title.getStyle())) {
						dataTable.getDefaultCell().setBackgroundColor(
								Color.LIGHT_GRAY);
					}

					if ("ColSpan".equals(title.getType())) {
						PdfPCell cell = new PdfPCell(new Phrase(
								title.getContent(), fonts[7]));

						cell.setBackgroundColor(Color.LIGHT_GRAY);
						cell.setColspan(numberOfColumns);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						dataTable.addCell(cell);
					} else {
						for (int j = 0; j < numberOfColumns; j++) {

							dataTable.addCell(new Phrase(tableHeader[j],
									fonts[8]));
						}
					}

					isTableHeaderAdded = true;// Making it true to add the code.
					dataTable.getDefaultCell().setBackgroundColor(null);

				} else if ("table_data_head".equals(section.getType())) {
					/*
					 * To add the head as 2nd row in the table.
					 */

					String[] tableRowsData = section.getTitle().getContent()
							.trim().split("~\\|~");

					PdfPCell cell = null;

					for (int k = 0; k < tableRowsData.length; k++) {
						cell = new PdfPCell(new Phrase(
								tableRowsData[k] == null ? ""
										: tableRowsData[k], fonts[8]));
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						dataTable.addCell(cell);

					}
				} else if ("table_data".equals(section.getType())
						&& isTableHeaderAdded) {
					/*
					 * isTableHeaderAdded checking that if table header added
					 * then add the body of the table else no need to add other
					 * rows.
					 */

					content = section.getContent();

					String[] tableRowsData = content.getContent().trim()
							.split("~\\|\\|~");
					String[] tableData = null;
					for (int k = 0; k < tableRowsData.length; k++) {

						tableData = tableRowsData[k].trim().split("~\\|~");
						/*
						 * added condition of length with J to avoid array index
						 * out of bound exception. to maintain then number of
						 * cells of table.
						 */
						for (int j = 0; j < numberOfColumns; j++) {
							if (tableData.length > j) {
								dataTable.addCell(new Phrase(
										tableData[j] == null ? ""
												: tableData[j], fonts[7]));
							} else {
								dataTable.addCell(new Phrase("", fonts[7]));
							}

						}
					}
					maindocument.add(dataTable);
					/* making it false for next tables if any */
					isTableHeaderAdded = false;

					if ("lastTable".equals(content.getStyle())) {
						/*
						 * Value of old constructor: document.top() - 240, 495
						 */
						mctmain = new MultiColumnText(495);
						mctmain.addRegularColumns(document.left() + 30,
								document.right() - 30, 18f, 2);
					} else {
						mctmain = new MultiColumnText(document.top()
								- setHeaderHeight, 655);
						mctmain.addRegularColumns(document.left() + 30,
								document.right() - 30, 18f, 2);
					}

				} else if (i != 0) {

					Onbullet = "";
					if (section.getType().toString().equals("notshow"))
						titleText = ""; // for first paragraph "General"
					if (section.getType().toString().equals("bullet"))
						Onbullet = "\u2022 ";

					fonts[2] = new Font(bfArialN, 8f, Font.BOLD);
					if (title.getStyle().contains("bold")
							&& title.getStyle().contains("underline")) {
						fonts[2] = new Font(bfArialN, 8, Font.BOLD
								| Font.UNDERLINE);

					} else if (title.getStyle().contains("underline")
							&& title.getStyle().contains("normal")) {
						fonts[2] = new Font(bfArialN, 8, Font.NORMAL
								| Font.UNDERLINE);
					} else if (title.getStyle().contains("normal")) {
						fonts[2] = new Font(bfArialN, 8, Font.NORMAL);

					}

					if ("signature_block".equals(section.getType())) {
						fonts[0] = new Font(bfArialN, 8f, Font.BOLD);
					} else {
						fonts[0] = new Font(bfArialN, 8f, Font.NORMAL);
					}

					if ("subHeading".equals(title.getType())) {

						Chunk heading = new Chunk(headingText, fonts[2]);

						Chunk paraText = new Chunk(
								Onbullet + " " + contentText, fonts[0]);

						Paragraph pp = new Paragraph(
								fonts[0].getCalculatedLeading(1.13F));
						pp.add(heading);
						pp.add(paraText);
						pp.setAlignment(Element.ALIGN_JUSTIFIED);
						if (!title.getStyle().contains("noParaSpace")) {
							pp.setSpacingAfter(5f);
						}

						mct.addElement(pp);
						mctmain.addElement(pp);

					} else {
						Paragraph para = new Paragraph(
								fonts[0].getCalculatedLeading(1.13F), Onbullet
										+ contentText, fonts[0]);
						para.setAlignment(Element.ALIGN_JUSTIFIED);
						para.setSpacingAfter(5f);

						Paragraph paranumber = new Paragraph(
								fonts[2].getCalculatedLeading(1.13F),
								headingText, fonts[2]);
						paranumber.setAlignment(Element.ALIGN_LEFT);
						paranumber.setSpacingAfter(3f);

						if (!titleText.equals("")
								&& !"subHeading".equals(title.getType())) {
							mct.addElement(paranumber);
							mctmain.addElement(paranumber);
						}
						mct.addElement(para);
						mctmain.addElement(para);
					}

				}
			}

			document.add(mct);
			maindocument.add(mctmain);

			while (mct.isOverflow()) {
				mct.nextColumn();
				document.newPage();
				document.add(mct);
			}

			while (mctmain.isOverflow()) {
				mctmain.nextColumn();
				maindocument.newPage();
				maindocument.add(mctmain);
			}

		} catch (Exception e) {

			logger.error(
					"viewMsaPdf(String, String, String, String, String, String, String, DataSource) - exception ignored",
					e);
		}
		document.close();
		maindocument.close();

		return outbuff.toByteArray();
	}
}
