/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class used for generating Standard Appendix in pdf format.      
 *
 */

package com.mind.xml;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.MultiColumnText;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.mind.common.Decimalroundup;
import com.mind.dao.IM.IMdao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;

/**
 * Methods : viewappendix
 */

public class AppendixStandardPdf extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AppendixStandardPdf.class);

	/**
	 * This method return Byte Array for Standard Appendix in pdf.
	 * 
	 * @param appendixId
	 *            String. appendix Id
	 * @param type
	 *            String. type msa/appendix
	 * @param fromType
	 *            String. from where it has been genarated. PM/PMT/PRM
	 * @param fileName
	 *            String. path of xml file
	 * @param imagePath
	 *            String. path of logo
	 * @param ds
	 *            Object of DataSource
	 * @return Byte Array This method returns byte array, for standrad appendix
	 *         in pdf.
	 */

	public byte[] viewAppendixPdf(String jobId, String type, String fromType,
			String fileName, String imagePath, String fontPath,
			String loginUserId, DataSource ds) {

		Document mainDocument = new Document(PageSize.LETTER.rotate(), 20, 20,
				30, 20);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();

		String[] HeaderData = { "Item #", "Activity Name", "Scope of Work",
				"Assumptions", "Qty/ Site", "Unit Price", "Extended Price" };
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String appendixId = null;

		try {
			conn = ds.getConnection();
			PdfWriter writer = PdfWriter.getInstance(mainDocument, outbuff);

			// Set font array
			BaseFont bfArialN = BaseFont.createFont(fontPath,
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			Font[] fonts = new Font[9];
			fonts[0] = new Font(bfArialN, 8, Font.NORMAL);
			fonts[1] = new Font(bfArialN, 11, Font.BOLD);
			fonts[2] = new Font(bfArialN, 7, Font.BOLD);
			fonts[3] = new Font(bfArialN, 8, Font.NORMAL);
			fonts[4] = new Font(bfArialN, 7, Font.NORMAL);
			fonts[5] = new Font();
			fonts[6] = new Font(bfArialN, 10, Font.BOLD);
			fonts[7] = new Font(bfArialN, 8, Font.BOLD);
			fonts[8] = new Font(bfArialN, 8, Font.UNDERLINE | Font.BOLD);

			// get appendix id from job id
			appendixId = IMdao.getAppendixId(jobId, ds);
			String temp_data = null;
			float setPaddingL = 50;
			float setPaddingT = 0;

			PdfPTable headerTable = new PdfPTable(1);
			headerTable.setWidthPercentage(95);

			int headerwidths[] = { 4, 8, 36, 36, 4, 6, 6 }; // percentage
			PdfPTable table = new PdfPTable(HeaderData.length);
			table.setWidths(headerwidths);
			table.setWidthPercentage(100);
			table.setSplitRows(true);
			table.setSplitLate(false);
			table.setKeepTogether(false);

			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			PdfPCell cell3 = new PdfPCell();
			PdfPCell cell4 = new PdfPCell();

			PdfPCell cellContent, cellNumber;
			PdfPCell cellContentSig = new PdfPCell();
			cellContentSig.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cellContentSig.setBorder(Rectangle.NO_BORDER);

			Paragraph paraContent = new Paragraph();

			PdfPTable signTable = new PdfPTable(1);
			signTable.setWidthPercentage(95);

			PdfPTable tempTable = new PdfPTable(1);
			tempTable.setWidthPercentage(100);

			int i = 0;
			int k = 0;
			boolean addTable = false;
			String titleText = "";
			String numberText = "";
			String contentText = "";
			String numberType = "";
			String titleType = "";
			String contentType = "";
			String Onbullet = "";
			String title1 = "";

			// Header Footer Start
			String footerText = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Confidential & Proprietary \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t \u00a9 2009 Contingent Network Services, LLC \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Page ";
			Image logo = Image.getInstance(imagePath);

			float aspectRatio = 381.0f / 148.0f;
			float y = 122.39f;
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);
			logo.setAlignment(Image.MIDDLE);

			Chunk chunk = new Chunk(logo, 0, -95);
			Phrase phrase = new Phrase(chunk);
			phrase.add(new Paragraph("\n\n\n\n"));

			HeaderFooter header = new HeaderFooter(phrase, false);
			HeaderFooter footer = new HeaderFooter(new Phrase(footerText,
					fonts[4]), true);

			header.setAlignment(Element.ALIGN_CENTER);
			header.setBorder(Rectangle.NO_BORDER);
			header.setBottom(100);
			footer.setAlignment(Element.ALIGN_LEFT);
			footer.setBorder(Rectangle.NO_BORDER);

			mainDocument.setHeader(header);
			mainDocument.setFooter(footer);
			// Header Footer End
			mainDocument.open();

			PdfDestination pd = new PdfDestination(PdfDestination.XYZ, -1, -1,
					1);
			PdfAction action = PdfAction.gotoLocalPage(1, pd, writer);
			writer.setOpenAction(action);

			MultiColumnText mctMain = new MultiColumnText(470);
			mctMain.addRegularColumns(mainDocument.left() + 12,
					mainDocument.right() - 12, 0, 1);

			// Read xml tag start
			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(fileName, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator it = list1.iterator();
			Title title = new Title();
			Number number = new Number();
			Content content = new Content();
			// Read xml tag end

			while (it.hasNext()) {
				titleText = "";
				numberText = "";
				contentText = "";
				section = (Section) it.next();

				numberType = section.getNumber().getType();
				titleType = section.getTitle().getType();
				contentType = section.getContent().getType();

				title = section.getTitle();
				titleText = title.getContent();
				if (titleText.indexOf("~") >= 0)
					titleText = addParameter(titleText, appendixId, type,
							loginUserId, jobId, ds);
				number = section.getNumber();
				numberText = number.getContent();
				if (numberText.indexOf("~") >= 0)
					numberText = addParameter(numberText, appendixId, type,
							loginUserId, jobId, ds);
				content = section.getContent();
				contentText = content.getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = addParameter(contentText, appendixId, type,
							loginUserId, jobId, ds);

				if (i == 0) {
					Paragraph para1 = new Paragraph(
							fonts[0].getCalculatedLeading(4), numberText,
							fonts[1]);
					para1.setSpacingAfter(15f);
					para1.setSpacingBefore(15f);
					cell2 = new PdfPCell(para1);
					cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell2.setBorder(Rectangle.NO_BORDER);
					cell2.setPaddingTop(.5f);

					para1 = new Paragraph(fonts[0].getCalculatedLeading(4),
							titleText, fonts[3]);
					para1.setSpacingAfter(10f);
					para1.setSpacingBefore(10f);
					cell3 = new PdfPCell(para1);
					cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell3.setBorder(Rectangle.NO_BORDER);
					cell3.setPaddingTop(.5f);

					para1 = new Paragraph(fonts[0].getCalculatedLeading(4),
							contentText, fonts[3]);
					para1.setSpacingAfter(5f);
					para1.setSpacingBefore(10f);
					cell4 = new PdfPCell(para1);
					cell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell4.setBorder(Rectangle.NO_BORDER);
					cell4.setPaddingTop(.5f);

					headerTable.addCell(cell2);
					headerTable.addCell(cell3);
					headerTable.addCell(cell4);
					mctMain.addElement(headerTable);

					section = (Section) it.next();
					// titleText = xt.getTitle();
					title = section.getTitle();
					titleText = title.getContent();
					if (titleText.indexOf("~") >= 0)
						titleText = addParameter(titleText, appendixId, type,
								loginUserId, jobId, ds);
					number = section.getNumber();
					numberText = number.getContent();
					if (numberText.indexOf("~") >= 0)
						numberText = addParameter(numberText, appendixId, type,
								loginUserId, jobId, ds);
					content = section.getContent();
					contentText = content.getContent();
					if (contentText.indexOf("~") >= 0)
						contentText = addParameter(contentText, appendixId,
								type, loginUserId, jobId, ds);
					numberType = section.getNumber().getType();
				}
				// Header rows end
				i++;

				// set font according to paragraph
				if (numberType.equals("mainheading")) {
					fonts[5] = fonts[6];
					setPaddingL = 15;
					setPaddingT = 8;
				}
				if (numberType.equals("heading")) {
					fonts[5] = fonts[7];
					setPaddingL = 30;
					setPaddingT = 3;
				}
				if (numberType.equals("subheading")) {
					fonts[5] = fonts[8];
					setPaddingL = 35;
					setPaddingT = 5;
				}
				// Content start

				if (i != 0) {
					// Add table start
					if (titleType.equals("table")) {
						mctMain.addElement(tempTable);
						tempTable = new PdfPTable(1);
						tempTable.setWidthPercentage(100);

						String activityType = null;
						String tempActivityType = null;
						String prevActivityType = null;
						boolean flagActivityType = true;

						cstmt = conn
								.prepareCall("{?=call lx_appendix_document_data_01(?,?)}");
						cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
						cstmt.setString(2, jobId);
						cstmt.setString(3, fromType);
						rs = cstmt.executeQuery();

						k = 0;
						int j = 0;
						while (rs.next()) {
							// Add blank space before table
							if (j == 0) {
								cell1 = new PdfPCell(new Paragraph("\n"));
								cell1.setBorder(0);
								tempTable.addCell(cell1);
								mctMain.addElement(tempTable);
								tempTable = new PdfPTable(1);
								tempTable.setWidthPercentage(100);
							}

							activityType = rs.getString(1);
							if (activityType.equals(prevActivityType)) {
								flagActivityType = false;
							}

							if (flagActivityType) {
								k = 0;
								paraContent = new Paragraph(
										fonts[5].getCalculatedLeading(1),
										activityType, fonts[5]);
								cell1 = new PdfPCell(paraContent);
								cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
								cell1.setBackgroundColor(new Color(0xC0, 0xC0,
										0xC0));
								cell1.setColspan(7);
								cell1.setGrayFill(1);
								table.addCell(cell1);

								for (int x = 0; x < HeaderData.length; x++) {
									table.getDefaultCell()
											.setHorizontalAlignment(
													Element.ALIGN_CENTER);
									table.addCell(new Paragraph(HeaderData[x],
											fonts[2]));
								}
								j++;
							}
							paraContent = new Paragraph(j + "." + k, fonts[3]);
							cell1 = new PdfPCell(paraContent);
							table.addCell(cell1);

							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(.1f),
									rs.getString(3), fonts[3]);
							cell1 = new PdfPCell(paraContent);
							table.addCell(cell1);

							if (!rs.getString(4).equals(""))
								temp_data = "\u2022 "
										+ rs.getString(4).replace("~",
												"\n\u2022 ");
							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(2),
									temp_data, fonts[3]);
							cell1 = new PdfPCell(paraContent);
							cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							table.addCell(cell1);

							temp_data = null;
							if (!rs.getString(5).equals(""))
								temp_data = "\u2022 "
										+ rs.getString(5).replace("~",
												"\n\u2022 ");
							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(2),
									temp_data, fonts[3]);
							cell1 = new PdfPCell(paraContent);
							cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							table.addCell(cell1);

							paraContent = new Paragraph(rs.getString(6),
									fonts[3]);
							cell1 = new PdfPCell(paraContent);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							table.addCell(cell1);

							if (rs.getString(7).equals("0"))
								paraContent = new Paragraph("", fonts[3]);
							else
								paraContent = new Paragraph("$ "
										+ Decimalroundup.twodecimalplaces(
												rs.getFloat(7), 2), fonts[3]);
							cell1 = new PdfPCell(paraContent);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							table.addCell(cell1);

							if (rs.getString(8).equals("0"))
								paraContent = new Paragraph(
										fonts[3].getCalculatedLeading(2), "",
										fonts[3]);
							else
								paraContent = new Paragraph("$ "
										+ Decimalroundup.twodecimalplaces(
												rs.getFloat(8), 2), fonts[3]);
							cell1 = new PdfPCell(paraContent);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							table.addCell(cell1);

							flagActivityType = true;
							prevActivityType = activityType;
							k++;

							table.setWidthPercentage(100);
							mctMain.addElement(table);

							table = new PdfPTable(HeaderData.length);
							table.setWidths(headerwidths);
							table.setWidthPercentage(100);
							table.setSplitRows(true);
							table.setSplitLate(false);
							table.setKeepTogether(false);
						}
						addTable = true;
					}
					// Add table end
					else {
						if (addTable) {
						}
						addTable = false;

						if (numberText.equals(""))
							title1 = titleText;
						else
							title1 = numberText + titleText;

						Paragraph paraNumber = new Paragraph(
								fonts[5].getCalculatedLeading(2), title1,
								fonts[5]);
						paraNumber.setAlignment(Element.ALIGN_LEFT);
						paraNumber.setKeepTogether(false);
						cellNumber = new PdfPCell();
						cellNumber.addElement(paraNumber);
						cellNumber
								.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cellNumber.setBorder(Rectangle.NO_BORDER);
						cellNumber.setPaddingTop(setPaddingT);
						cellNumber.setPaddingLeft(setPaddingL);
						cellNumber.setVerticalAlignment(Element.ALIGN_TOP);
						Onbullet = "";

						// if (xt.getContentTypeAttribute().equals("bullet"))
						if (section.getContent().getType().equals("bullet"))
							Onbullet = "\u2022 ";
						paraContent = new Paragraph(
								fonts[0].getCalculatedLeading(2), Onbullet + ""
										+ contentText, fonts[0]);
						paraContent.setAlignment(Element.ALIGN_JUSTIFIED);
						paraContent.setKeepTogether(false);
						cellContent = new PdfPCell(paraContent);
						cellContent
								.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cellContent.setPaddingLeft(setPaddingL);
						cellContent.setBorder(Rectangle.NO_BORDER);

						tempTable.setSplitRows(true);
						tempTable.setSplitLate(false);
						tempTable.setKeepTogether(false);

						if (!titleText.trim().equals("")) {
							// "signature_block")) {
							if (section.getNumber().getType()
									.equals("signature_block")) {

								cellContentSig.addElement(paraNumber);
								cellContentSig.setPaddingLeft(20);
							} else {
								tempTable.addCell(cellNumber);
							}
						}
						if (section.getNumber().getType()
								.equals("signature_block")) {
							cellContentSig.addElement(paraContent);
							cellContentSig.setPaddingLeft(15);
						} else {
							tempTable.addCell(cellContent);
						}
					}
				}

			}
			signTable.addCell(cellContentSig);
			signTable.setSplitRows(false);
			signTable.setSplitLate(true);
			signTable.setKeepTogether(true);

			mctMain.addElement(tempTable);
			mctMain.addElement(signTable);
			mainDocument.add(mctMain);

			while (mctMain.isOverflow()) {
				mctMain.nextColumn();
				mainDocument.newPage();
				mainDocument.add(mctMain);
			}
		}

		catch (Exception de) {
			logger.error(
					"viewAppendixPdf(String, String, String, String, String, String, String, String, DataSource)",
					de);

			de.printStackTrace();
		}

		finally {
			DBUtil.closeCallableStatement(rs, cstmt);
			DBUtil.closeDbConnection(conn);
		}

		mainDocument.close();
		return outbuff.toByteArray();
	}

}