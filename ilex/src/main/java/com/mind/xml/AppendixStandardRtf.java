/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class used for generating Standard Appendix in rtf format.      
 *
 */

package com.mind.xml;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.table.RtfBorder;
import com.lowagie.text.rtf.table.RtfBorderGroup;
import com.lowagie.text.rtf.table.RtfCell;
import com.mind.common.Decimalroundup;
import com.mind.dao.IM.IMdao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;

/**
 * Methods : viewappendixrtf
 */

public class AppendixStandardRtf extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AppendixStandardRtf.class);

	/**
	 * This method return Byte Array for Standard Appendix in rtf.
	 * 
	 * @param appendixid
	 *            String. appendix Id
	 * @param type
	 *            String. type msa/appendix
	 * @param from_type
	 *            String. from where it has been genarated. PM/PMT/PRM
	 * @param filename
	 *            String. path of xml file
	 * @param imagepath
	 *            String. path of logo
	 * @param ds
	 *            Object of DataSource
	 * @return Byte Array This method returns byte array, for standrad appendix
	 *         in rtf.
	 */

	public byte[] viewAppendixRtf(String jobId, String type, String from_type,
			String filename, String imagepath, String fontPath,
			String login_userid, DataSource ds) {

		Rectangle doc_pagesize = PageSize.A4;
		doc_pagesize = PageSize.LETTER.rotate();
		Document maindocument = new Document(doc_pagesize);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();

		/* Set header array */
		String[] HeaderData = { "Item #", "Activity Name", "Scope of Work",
				"Assumptions", "Qty/ Site", "Unit Price", "Extended Price" };
		int HeaderColumns = 7;

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String footertext = "\tConfidential & Proprietary\n \u00a9 2009 Contingent Network Services, LLC \n\t\t\t\t\t\t\t\t\t   Page ";
			RtfWriter2 writer = RtfWriter2.getInstance(maindocument, outbuff);

			/* Set font array */
			BaseFont bfArialN = BaseFont.createFont(fontPath,
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

			/* Set font array */
			Font[] fonts = new Font[9];
			fonts[0] = new Font(bfArialN, 8.5f, Font.NORMAL); // for paragraph
																// content
			fonts[1] = new Font(bfArialN, 10.5f, Font.BOLD); // for page heading
			fonts[2] = new Font(bfArialN, 8, Font.BOLD); // not used
			fonts[3] = new Font(bfArialN, 8.5f, Font.NORMAL); // for header row
			fonts[4] = new Font(Font.TIMES_ROMAN, 7, Font.NORMAL); // for footer
																	// row
			fonts[5] = new Font(bfArialN, 7, Font.NORMAL); // use as variable
			fonts[6] = new Font(bfArialN, 10, Font.BOLD); // for main heading
			fonts[7] = new Font(bfArialN, 8, Font.BOLD); // for heading
			fonts[8] = new Font(bfArialN, 8.5f, Font.UNDERLINE | Font.BOLD); // for
																				// underline
																				// heading

			/* get appendix id from job id */
			String appendixId = null;
			appendixId = IMdao.getAppendixId(jobId, ds);

			Table headertable = new Table(1);
			headertable.setWidth(95);
			headertable.setBorderWidth(0);
			headertable.setBorder(Rectangle.NO_BORDER);

			Table maintable1 = new Table(1);
			maintable1.setWidth(95);
			maintable1.setBorderWidth(0);
			maintable1.setBorder(Rectangle.NO_BORDER);

			Table table = new Table(HeaderColumns);
			table.setWidth(94);

			int headerwidths[] = { 4, 9, 36, 36, 3, 6, 6 }; // percentage
			table.setWidths(headerwidths);

			RtfCell cellContent, cellNumber;
			String temp_data = null;
			Paragraph paraContent = new Paragraph();
			RtfCell cell1 = new RtfCell();
			RtfCell cell2 = new RtfCell();
			RtfCell cell3 = new RtfCell();
			RtfCell cell4 = new RtfCell();
			float setpaddingL = 20;
			float Leading = 0;
			float SpacingAfter = 0;
			float SpacingBefore = 0;
			int j = 0;
			int k = 0;
			Image logo = Image.getInstance(imagepath);
			float aspectRatio = 381.0f / 148.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewAppendixRtf(String, String, String, String, String, String, String, DataSource) - "
						+ aspectRatio);
			}
			float y = 112.39f;
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			logo.setAlignment(Image.MIDDLE);

			Chunk chunk = new Chunk(logo, 0, -20);
			HeaderFooter header = new HeaderFooter(new Phrase(chunk), false);
			HeaderFooter footer = new HeaderFooter(new Phrase(footertext,
					fonts[4]), true);

			header.setAlignment(Element.ALIGN_CENTER);
			footer.setAlignment(Element.ALIGN_LEFT);
			footer.setBorder(Rectangle.NO_BORDER);
			maindocument.setHeader(header);
			maindocument.setFooter(footer);

			maindocument.open();
			maindocument.setMargins(30f, 30f, .5f, .5f);

			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(filename, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator it = list1.iterator();
			Title title = new Title();
			Number number = new Number();
			Content content = new Content();

			int i = 0;
			boolean addtable = false;
			String titleText = "";
			String numberText = "";
			String contentText = "";
			String numberType = "";
			String titleType = "";
			String contentType = "";
			String Onbullet = "";
			String Title = "";
			while (it.hasNext()) {
				titleText = "";
				numberText = "";
				contentText = "";

				section = (Section) it.next();

				numberType = section.getNumber().getType();
				titleType = section.getTitle().getType();
				contentType = section.getContent().getType();

				title = section.getTitle();
				titleText = title.getContent();
				if (titleText.indexOf("~") >= 0)
					titleText = addParameter(titleText, appendixId, type,
							login_userid, jobId, ds);

				number = section.getNumber();
				numberText = number.getContent();
				if (numberText.indexOf("~") >= 0)
					numberText = addParameter(numberText, appendixId, type,
							login_userid, jobId, ds);

				content = section.getContent();
				contentText = content.getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = addParameter(contentText, appendixId, type,
							login_userid, jobId, ds);

				if (i == 0) {
					Paragraph para1 = new Paragraph(
							fonts[1].getCalculatedLeading(1.5f), numberText,
							fonts[1]);
					cell2 = new RtfCell(para1);
					cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell2.setBorderWidth(0);
					cell2.setBorder(Rectangle.NO_BORDER);

					para1 = new Paragraph(fonts[3].getCalculatedLeading(1.5f),
							titleText, fonts[3]);
					cell3 = new RtfCell(para1);
					cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell3.setBorderWidth(0);
					cell3.setBorder(Rectangle.NO_BORDER);

					para1 = new Paragraph(fonts[3].getCalculatedLeading(1.5f),
							contentText, fonts[3]);
					cell4 = new RtfCell(para1);
					cell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell4.setBorderWidth(0);
					cell4.setBorder(Rectangle.NO_BORDER);

					headertable.addCell(cell2);
					headertable.addCell(cell3);
					headertable.addCell(cell4);
					maindocument.add(headertable);

					section = (Section) it.next();
					// titleText = xt.getTitle();
					title = section.getTitle();
					titleText = title.getContent();
					if (titleText.indexOf("~") >= 0)
						titleText = addParameter(titleText, appendixId, type,
								login_userid, jobId, ds);
					number = section.getNumber();
					numberText = number.getContent();
					if (numberText.indexOf("~") >= 0)
						numberText = addParameter(numberText, appendixId, type,
								login_userid, jobId, ds);
					content = section.getContent();
					contentText = content.getContent();
					if (contentText.indexOf("~") >= 0)
						contentText = addParameter(contentText, appendixId,
								type, login_userid, jobId, ds);
					// numberType = xt.getNumberTypeAttribute();
					numberType = section.getNumber().getType();
				}
				i++;

				if (numberType.equals("mainheading")) {
					fonts[5] = fonts[6];
					setpaddingL = 15;
					Leading = 2.5f;
					SpacingAfter = 4;
					SpacingBefore = 4;
				}
				if (numberType.equals("heading")) {
					fonts[5] = fonts[7];
					setpaddingL = 30;
					Leading = 2;
					SpacingAfter = 3;
					SpacingBefore = 3;
				}
				if (numberType.equals("subheading")) {
					fonts[5] = fonts[8];
					setpaddingL = 35;
					Leading = 2;
					SpacingAfter = 2.5f;
				}

				if (i != 0) {
					if (titleType.equals("table")) {
						String activity_type = null;
						String temp_activity_type = null;
						String prevactivity_type = null;
						boolean flagactivity_type = true;

						cstmt = conn
								.prepareCall("{?=call lx_appendix_document_data_01(?,?)}");
						cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
						cstmt.setString(2, jobId);
						cstmt.setString(3, from_type);
						rs = cstmt.executeQuery();
						k = 0;
						j = 0;
						while (rs.next()) {
							activity_type = rs.getString(1);
							if (activity_type.equals(prevactivity_type)) {
								flagactivity_type = false;
							}

							if (flagactivity_type) {
								k = 0;
								paraContent = new Paragraph(
										fonts[5].getCalculatedLeading(.1f),
										activity_type, fonts[5]);

								cell1 = new RtfCell(paraContent);
								cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell1.setBackgroundColor(new Color(0xC0, 0xC0,
										0xC0));
								cell1.setColspan(7);
								cell1.setBorders(new RtfBorderGroup(
										Rectangle.BOX, RtfBorder.BORDER_SINGLE,
										1, new Color(0, 0, 0)));
								table.addCell(cell1);

								for (int x = 0; x < HeaderColumns; x++) {
									cell1 = new RtfCell(new Paragraph(
											HeaderData[x], fonts[2]));
									cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
									cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
									cell1.setBorders(new RtfBorderGroup(
											Rectangle.BOX,
											RtfBorder.BORDER_SINGLE, 1,
											new Color(0, 0, 0)));
									table.addCell(cell1);
								}
								j++;
							}

							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(.1f), j + "."
											+ k, fonts[3]);
							paraContent.setIndentationLeft(4);
							cell1 = new RtfCell(paraContent);
							cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
									RtfBorder.BORDER_SINGLE, 1, new Color(0, 0,
											0)));
							table.addCell(cell1);

							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(.1f),
									rs.getString(3), fonts[3]);
							paraContent.setIndentationLeft(4);
							cell1 = new RtfCell(paraContent);
							cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
									RtfBorder.BORDER_SINGLE, 1, new Color(0, 0,
											0)));
							table.addCell(cell1);

							if (!rs.getString(4).equals(""))
								temp_data = "\u2022 "
										+ rs.getString(4).replace("~",
												"\n\u2022 ");
							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(.1f),
									temp_data, fonts[3]);
							paraContent.setIndentationLeft(4);
							paraContent.setAlignment(Element.ALIGN_JUSTIFIED);
							paraContent.setIndentationRight(4);
							cell1 = new RtfCell(paraContent);
							cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
									RtfBorder.BORDER_SINGLE, 1, new Color(0, 0,
											0)));
							table.addCell(cell1);

							temp_data = null;
							if (!rs.getString(5).equals(""))
								temp_data = "\u2022 "
										+ rs.getString(5).replace("~",
												"\n\u2022 ");
							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(.1f),
									temp_data, fonts[3]);
							paraContent.setIndentationLeft(4);
							paraContent.setAlignment(Element.ALIGN_JUSTIFIED);
							paraContent.setIndentationRight(4);
							cell1 = new RtfCell(paraContent);
							cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
									RtfBorder.BORDER_SINGLE, 1, new Color(0, 0,
											0)));
							table.addCell(cell1);

							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(2),
									rs.getString(6), fonts[3]);
							cell1 = new RtfCell(paraContent);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
									RtfBorder.BORDER_SINGLE, 1, new Color(0, 0,
											0)));
							table.addCell(cell1);

							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(2), "$ "
											+ Decimalroundup.twodecimalplaces(
													rs.getFloat(7), 2),
									fonts[3]);
							cell1 = new RtfCell(paraContent);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
									RtfBorder.BORDER_SINGLE, 1, new Color(0, 0,
											0)));
							table.addCell(cell1);

							paraContent = new Paragraph(
									fonts[3].getCalculatedLeading(2), "$ "
											+ Decimalroundup.twodecimalplaces(
													rs.getFloat(8), 2),
									fonts[3]);
							cell1 = new RtfCell(paraContent);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell1.setBorders(new RtfBorderGroup(Rectangle.BOX,
									RtfBorder.BORDER_SINGLE, 1, new Color(0, 0,
											0)));
							table.addCell(cell1);

							flagactivity_type = true;
							prevactivity_type = activity_type;
							k++;
						}
						addtable = true;
					}

					else {
						if (addtable) {
							maindocument.add(maintable1);
							table.setAlignment(Element.ALIGN_CENTER);
							if (j != 0) {
								maindocument.add(table);
							}
							maintable1 = new Table(1);
							maintable1.setWidth(95);
							maintable1.setBorderWidth(0);
						}
						addtable = false;

						if (numberText.equals(""))
							Title = titleText;
						else
							Title = "\n" + numberText + titleText;

						Paragraph paraNumber = new Paragraph(Leading, Title,
								fonts[5]);
						paraNumber.setSpacingAfter(SpacingAfter);
						paraNumber.setSpacingBefore(SpacingBefore);
						paraNumber.setIndentationLeft(setpaddingL);

						cellNumber = new RtfCell(paraNumber);
						cellNumber
								.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cellNumber.setBorderWidth(0);
						cellNumber.setBorder(Rectangle.NO_BORDER);

						Onbullet = "";
						// if (xt.getContentTypeAttribute().equals("bullet"))
						if (section.getContent().getType().equals("bullet"))
							Onbullet = "\u2022 ";

						paraContent = new Paragraph(
								fonts[0].getCalculatedLeading(1.20f), Onbullet
										+ "" + contentText, fonts[0]);
						paraContent.setAlignment(Element.ALIGN_JUSTIFIED);
						paraContent.setIndentationLeft(setpaddingL);
						paraContent.setSpacingAfter(SpacingAfter);

						cellContent = new RtfCell(paraContent);
						cellContent
								.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cellContent.setBorderWidth(0);
						cellContent.setBorder(Rectangle.NO_BORDER);

						if (!titleText.equals(""))
							maintable1.addCell(cellNumber);
						maintable1.addCell(cellContent);
					}
				}
			}
			maindocument.add(maintable1);
		}

		catch (Exception de) {
			logger.error(
					"viewAppendixRtf(String, String, String, String, String, String, String, DataSource)",
					de);
		}

		finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}

		maindocument.close();
		return outbuff.toByteArray();
	}
}