/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class used for generating Work Order in pdf format.      
 *
 */

package com.mind.xml;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.mind.common.StoreWOArrayContents;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * Methods : viewwo
 */

public class WOPdf {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WOPdf.class);

	/**
	 * This method return Byte Array for Work Order in pdf.
	 * 
	 * @param powoid
	 *            String. Work Order Number
	 * @param imagepath
	 *            String. Path to get image that shows Contingent logo.
	 * @param imagepath1
	 *            String. Path to get image that shows checkbox.
	 * @param fontPath
	 *            String. Path to get Font class.
	 * @param userid
	 *            String. User Id.
	 * @param ds
	 *            Object of DataSource.
	 * @param WONwConfigImg
	 *            String. Path to get footer image for Network Certification
	 *            Sheet.
	 * @param WONwConfigImgheader
	 *            String. Path to get WONwConfigImgheader image for Network
	 *            Certification Sheet.
	 * @param icSnapOn
	 *            String. Path to get icSnapOn image
	 * @param icChrysler
	 *            String. Path to get icChrysler image
	 * @param icCompuCom
	 *            String. Path to get icCompuCom image
	 * @return Byte Array This method returns byte array, for work order in pdf.
	 */

	String HeaderRowData[] = null;
	String BehavioralGLData = null;
	String DescWRData[] = null;
	String HeaderData1[] = null;
	String HeaderData[] = null;
	String AddressData[] = null;
	String AddressData1[] = null;
	String SiteAddressData[] = null;
	String SiteAddressDataBrand[] = null;
	String ScheInfoData[] = null;
	String SiteContactData[] = null;
	String SiteContactData1[] = null;
	String ExpectationVerbiageData[] = null;
	static Font[] fonts = new Font[23];

	public byte[] viewWOWithBadge(String powoid, String imagepath,
			String imagepath1, String badgeImagePath, String fontPath,
			String userid, DataSource ds, String WONwConfigImg,
			String WONwConfigImgheader, String icSnapOn, String icChrysler,
			String icCompuCom) {
		Document maindocument = new Document(PageSize.A4, 1, 1, 50, 50);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();
		Connection con = null;
		Connection conn = null;
		CallableStatement cstmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs1 = null;

		try {
			PdfWriter writer = PdfWriter.getInstance(maindocument, outbuff);

			/* Set font array */
			getBaseFont(fontPath);

			PdfPTable maintable = new PdfPTable(2);
			maintable.setWidthPercentage(95);
			PdfPTable table = new PdfPTable(3);
			PdfPTable table1 = new PdfPTable(1);
			table.setWidthPercentage(95);
			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			Paragraph para = new Paragraph();

			int x = 0;
			maindocument.open();

			PdfDestination pd = new PdfDestination(PdfDestination.XYZ, -1, -1,
					1);
			PdfAction action = PdfAction.gotoLocalPage(1, pd, writer);
			writer.setOpenAction(action);

			maindocument.setMargins(20, 20, 80f, 25f);

			/* Retrieve Array contents for generating WO Pdf file. */
			getPdfData();

			Color black = new Color(0, 0, 0);
			Color red = new Color(255, 0, 0);

			String strDate = "";
			Calendar c = Calendar.getInstance();
			strDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE)
					+ "/" + c.get(Calendar.YEAR);

			String lm_po_issue_date = null;
			String lm_wo_number = null;
			String lm_wo_deliver_by_date = null;
			String lm_wo_deliver_by_time = null;
			String lm_po_special_instructions = null;
			String lm_po_sow = null;
			String lm_po_assumption = null;
			String lx_pr_title = null;
			String[] pvs_address_data = new String[AddressData.length];
			String[] site_address_data = new String[SiteAddressData.length];
			String cns_pc_name = null;
			String login_name = null;
			String project_manager = null;
			String facsimile = null;
			String company_name = null;
			String company_address = null;
			String company_city = null;
			String company_state = null;
			String company_zipcode = null;
			String Cell = null;
			String lm_si_brand = "";
			boolean check = false;
			boolean checkForAssumption = false;
			boolean checkForNetMedx = false;
			String ticketNo = null;
			String appendixId = null;
			String msaName = null;
			String currentid = "";
			boolean checkForAppendix = false;
			boolean checkForCashSurveyReport = false;
			String siteName = "";
			boolean checkNwCert = false;
			String workmanship = "Unless Contingent specifically states otherwise in writting, all work will be completed to accepted BICSI, NEC, ANSI/TIA/EIA best practices, codes and standards.";

			/* Fetch data from database. */
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_po_wo_data(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, powoid);
			cstmt.setInt(3, Integer.parseInt(userid));
			String sql1 = "";
			String sql2 = "";
			Statement stmt1 = conn.createStatement();

			rs = cstmt.executeQuery();
			if (rs.next()) {
				lm_po_issue_date = rs.getString("lm_po_issue_date");
				lm_wo_number = rs.getString("lm_po_number");
				pvs_address_data[0] = rs.getString("pvs_compname"); // pvs_name
				pvs_address_data[1] = rs.getString("pvs_address"); // pvs_address
				pvs_address_data[2] = rs.getString("pvs_city_state_zip"); // pvs_city_state_zip
				pvs_address_data[3] = rs.getString("partner_poc_phone"); // pvs_city_state_zip
				pvs_address_data[4] = rs.getString("partner_poc_name"); // pvs_city_state_zip

				siteName = rs.getString("lm_si_name");

				if (rs.getString("lm_si_group") == null
						|| rs.getString("lm_si_group").equals("")) {
					site_address_data[1] = rs.getString("lm_si_name"); // site_name
					check = false;
				} else {
					site_address_data[1] = rs.getString("lm_si_group"); // site_name
					check = true;
				}

				site_address_data[3] = rs.getString("lm_si_number"); // site_number
				site_address_data[4] = rs.getString("lm_si_address"); // site_address
				site_address_data[5] = rs.getString("site_city_state_zip"); // site_city_state_zip
				site_address_data[2] = rs.getString("lm_si_work_location"); // work
				// location
				lm_po_special_instructions = rs
						.getString("lm_po_special_instructions"); // sp. inst.
				lx_pr_title = rs.getString("lx_pr_title");
				project_manager = rs.getString("cns_pc_name") + " "
						+ rs.getString("cns_pc_phone") + " " + "Cell: "
						+ rs.getString("cns_pc_cell");

				lm_wo_deliver_by_date = rs.getString("lm_wo_deliver_by_date");
				lm_wo_deliver_by_time = rs.getString("lm_wo_deliver_by_time");

				if (lm_wo_deliver_by_date.equals("")) {
					if (lx_pr_title.equalsIgnoreCase("NetMedX")) {
						lm_wo_deliver_by_date = rs
								.getString("lm_tc_preferred_arrival_date");
						lm_wo_deliver_by_time = rs
								.getString("lm_tc_preferred_arrival_time");
					} else {
						lm_wo_deliver_by_date = rs
								.getString("lm_js_planned_start_date");
						lm_wo_deliver_by_time = rs
								.getString("lm_js_planned_start_time");
					}
				}
				login_name = rs.getString("created_by");
				cns_pc_name = rs.getString("job_owner") + " "
						+ rs.getString("job_owner_phone") + " " + "Cell: "
						+ rs.getString("job_owner_cell");
				site_address_data[0] = rs.getString("lo_ot_name"); // client_name
				lm_si_brand = rs.getString("lm_si_group"); // Brand Name

				if (!lm_si_brand.equals(""))
					site_address_data[0] = site_address_data[0] + " - "
							+ lm_si_brand;

				company_name = rs.getString("company_name");
				company_address = rs.getString("company_address");
				company_city = rs.getString("company_city") + ',' + ' '
						+ rs.getString("company_state") + ' '
						+ rs.getString("company_zip");
				facsimile = rs.getString("company_fax");

				if (facsimile == null || facsimile.equals(""))
					facsimile = "513-860-2105";
				lm_po_sow = rs.getString("lm_po_sow");
				lm_po_assumption = rs.getString("lm_po_assumption");

				if (rs.getString("lm_po_assumption") == null
						|| rs.getString("lm_po_assumption").equals("")) {
					checkForAssumption = true;
				}

				if (rs.getString("lx_pr_type").equals("3")) {
					checkForNetMedx = true;
					ticketNo = rs.getString("lm_tc_number");
				}

				appendixId = rs.getString("lx_pr_id");
				msaName = rs.getString("lo_ot_name");
			}

			/*
			 * Check database tables to add Field Sheet/Cash Survey/Network -
			 * Certification Sheet pages in WO Pdf.
			 */
			con = ds.getConnection();
			stmt = con.createStatement();

			// Check for Field Sheet page.
			checkForAppendix = checkPage(stmt, appendixId,
					"select fs_pr_id from fieldsheet");

			// Check for Cash Survey Report page.
			checkForCashSurveyReport = checkPage(
					stmt,
					appendixId,
					"select csr_pr_id from CashSurveyReport_WO where csr_type_desc = 'CashSurveyReport'");

			// Check for Network Certification page.
			checkNwCert = checkPage(
					stmt,
					appendixId,
					"select csr_pr_id from CashSurveyReport_WO where csr_type_desc = 'Network Certification'");

			if (checkForAppendix || checkForCashSurveyReport) {
				facsimile = "888-556-0932";
			}

			// Create First Page.
			// Zeroth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[15], fonts[0]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);

			table = new PdfPTable(3);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			Image logo = Image.getInstance(imagepath);

			float aspectRatio = 177.0f / 111.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewwo(String, String, String, String, String, DataSource, String, String, String, String, String) - "
						+ aspectRatio);
			}
			float y = 72.39f;
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			cell1 = new PdfPCell(logo);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[0], fonts[5]));
			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[16] + " "
					+ strDate, fonts[3]));
			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);
			cell1.setBorder(0);
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell1.setBorder(1);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// First Row
			table = new PdfPTable(4);
			table.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[17] + " "
					+ lm_wo_number + "\n" + HeaderRowData[1] + " "
					+ lx_pr_title, fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);
			cell1.setNoWrap(true);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[2] + " "
					+ lm_wo_deliver_by_date + " " + lm_wo_deliver_by_time,
					fonts[10]));
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setColspan(2);
			cell1.setBorder(0);
			table.addCell(cell1);

			if (checkForNetMedx) {
				cell1 = new PdfPCell(new Paragraph(HeaderRowData[3] + " "
						+ lm_wo_number + "\n" + HeaderRowData[28] + " "
						+ ticketNo, fonts[7]));
			} else {
				cell1 = new PdfPCell(new Paragraph(HeaderRowData[3] + " "
						+ lm_wo_number, fonts[7]));
			}

			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell1.setBorder(1);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Second Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[4], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[5], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Third Row // Column One
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			if (!check) {
				for (x = 0; x < SiteAddressData.length; x++) {

					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_RIGHT);
					table.getDefaultCell().setBorder(0);
					table.addCell(new Paragraph(SiteAddressData[x], fonts[2]));

					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_LEFT);
					table.getDefaultCell().setBorder(0);
					table.addCell(new Paragraph(site_address_data[x], fonts[3]));
				}
			} else {
				for (x = 0; x < SiteAddressDataBrand.length; x++) {
					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_RIGHT);
					table.getDefaultCell().setBorder(0);
					table.addCell(new Paragraph(SiteAddressDataBrand[x],
							fonts[2]));

					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_LEFT);
					table.getDefaultCell().setBorder(0);
					table.addCell(new Paragraph(site_address_data[x], fonts[3]));
				}
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			maintable.addCell(cell2);

			// Third Row Column Two
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[24], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_name, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[25], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_address, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[26], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_city, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[27], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(cns_pc_name, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[22], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(project_manager, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[23], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(facsimile, fonts[3]));

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Fourth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[6], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[7], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Fifth Row Column One
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			for (x = 0; x < AddressData.length; x++) {
				table.getDefaultCell().setHorizontalAlignment(
						Element.ALIGN_RIGHT);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(AddressData[x], fonts[2]));

				table.getDefaultCell().setHorizontalAlignment(
						Element.ALIGN_LEFT);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(pvs_address_data[x], fonts[3]));
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			maintable.addCell(cell2);

			// Fifth Row Column Two
			table = new PdfPTable(1);
			table.setWidthPercentage(100);

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(fonts[13].getCalculatedLeading(0.1f),
					BehavioralGLData, fonts[2]));

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Sixth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[8], fonts[11]));
			cell1.setBackgroundColor(red);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Seventh Row
			table = new PdfPTable(2);
			table.setWidthPercentage(100);
			int headerwidths[] = { 15, 85 }; // percentage
			table.setWidths(headerwidths);
			table.getDefaultCell().setBorder(1);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[9], fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setPaddingBottom(50);
			table.addCell(cell1);

			table1 = new PdfPTable(HeaderData1.length);
			table1.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(DescWRData[2] + " - "
					+ lm_po_sow + "\n", fonts[2]));
			cell1.setBorder(0);
			cell1.setColspan(4);
			cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			table1.addCell(cell1);

			if (checkForAssumption)
				cell1 = new PdfPCell(new Paragraph("", fonts[2]));
			else
				cell1 = new PdfPCell(new Paragraph(DescWRData[3] + " - "
						+ lm_po_assumption, fonts[2]));

			cell1.setBorder(0);
			cell1.setColspan(4);
			cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			table1.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table1);
			table.addCell(cell2);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[10], fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setPaddingBottom(50);
			table.addCell(cell1);

			// add condition

			String imageabslpath = "https://medius.contingent.com/images/TigerTeam.png?1";
			// File f = new File(System.getProperty("catalina.base"));
			// String abslpath = "/webapps/Ilex/images/TigerTeam.png";

			// Image image = Image.getInstance(f.getPath()
			// + "/webapps/Ilex/images/TigerTeam.png");

			Image image = Image.getInstance(imageabslpath);

			if (appendixId.equals("2023") || appendixId.equals("3887")) {

				PdfPCell cell;

				float[] widths = { 2, 1 };
				PdfPTable nestedTable = new PdfPTable(widths);

				nestedTable.addCell(new Phrase(lm_po_special_instructions,
						fonts[6]));
				// aspectRatio = 177.0f / 111.0f;

				image.setAlignment(2);
				// y = 72.39f;
				// image.setDpi(96, 96);
				// image.scaleAbsolute(aspectRatio * y, y);
				// image.setBorder(0);
				nestedTable.addCell(setCellImageAttribute(image,
						Element.ALIGN_RIGHT, 0, 2, 2, 5, 10,
						Rectangle.NO_BORDER, 1));
				table.addCell(nestedTable);

			} else {
				cell1 = new PdfPCell(new Paragraph(lm_po_special_instructions,
						fonts[6]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);

				table.addCell(cell1);

			}

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[30], fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setMinimumHeight(0f);
			table.addCell(cell1);

			// corrected placement of workmanship in WO pdf
			cell1 = new PdfPCell(new Paragraph(workmanship, fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			table.addCell(cell1);

			// deliverable Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[31], fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setMinimumHeight(0f);
			cell1.setPaddingBottom(50);
			table.addCell(cell1);

			PdfPCell cell3 = new PdfPCell();
			cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell3.setMinimumHeight(0f);

			PdfPTable table2 = new PdfPTable(1);
			table2.setWidthPercentage(100);
			int innerTableWidth[] = { 100 }; // percentag
			table2.setWidths(innerTableWidth);
			// table2.getDefaultCell().setBorder(0);

			List<String> deliverableTitles = getDeliverableTitles(powoid, ds);
			int count = 0;
			for (String deliverableTitle : deliverableTitles) {
				cell1 = new PdfPCell(new Paragraph(++count + ". "
						+ deliverableTitle, fonts[2]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setMinimumHeight(0f);
				cell1.setBorder(0);
				table2.addCell(cell1);
			}

			cell3.addElement(table2);

			table.addCell(cell3);

			// deliverable Row ends

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Eighth Row
			table = new PdfPTable(1);
			table.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[11], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Ninth Row
			String temp_str = "";
			table = new PdfPTable(HeaderData.length);
			table.setWidthPercentage(100);
			table.getDefaultCell().setBorder(1);

			for (x = 0; x < HeaderData.length; x++) {
				if (x < 2)
					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_LEFT);
				if (x > 1)
					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_RIGHT);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(HeaderData[x], fonts[4]));
			}

			for (int j = 0; j < 2; j++) {
				for (x = 0; x < HeaderData.length; x++) {
					temp_str = "";
					if (x < 2)
						table.getDefaultCell().setHorizontalAlignment(
								Element.ALIGN_LEFT);
					if (x > 1)
						table.getDefaultCell().setHorizontalAlignment(
								Element.ALIGN_RIGHT);
					if (x == 0)
						temp_str = "00" + (x + 1 + j);
					table.addCell(new Paragraph(temp_str, fonts[2]));
				}
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Tenth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[12], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Row
			table = new PdfPTable(1);
			table.setWidthPercentage(100);

			for (x = 0; x < ScheInfoData.length; x++) {
				cell1 = new PdfPCell(new Paragraph(ScheInfoData[x], fonts[2]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setBorder(0);
				table.addCell(cell1);
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Tenth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[13], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Row
			table = new PdfPTable(2);
			table.setWidthPercentage(100);
			int headerwidth[] = { 30, 70 }; // percentage
			table.setWidths(headerwidth);

			table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(SiteContactData[0], fonts[9]));
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setBorder(0);
			table1.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(SiteContactData[1], fonts[0]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);
			table1.addCell(cell1);

			for (x = 2; x < SiteContactData.length - 1; x++) {
				cell1 = new PdfPCell(
						new Paragraph(SiteContactData[x] + " "
								+ SiteContactData[SiteContactData.length - 1],
								fonts[0]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setBorder(0);
				table1.addCell(cell1);
			}

			cell1 = new PdfPCell();
			cell1.addElement(table1);
			table.addCell(cell1);

			table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(SiteContactData1[0], fonts[8]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);
			table1.addCell(cell1);

			for (x = 1; x < SiteContactData1.length; x++) {
				cell1 = new PdfPCell(new Paragraph(SiteContactData1[x],
						fonts[2]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setBorder(0);
				table1.addCell(cell1);

				cell1 = new PdfPCell(new Paragraph("  ", fonts[2]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setBorder(0);
				table1.addCell(cell1);
			}

			cell1 = new PdfPCell();
			cell1.addElement(table1);
			table.addCell(cell1);
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Add verbiage change - start

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[33],
					fonts[3]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[0],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			StringBuilder verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[1]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[2]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[3]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[4],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[5]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[6]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[7]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[8]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[9]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[10]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[11]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[12]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[13]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[14],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[15]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[16]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[17],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[18]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[19]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[20]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[21]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[22]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[23]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[24]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[25]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[26],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[27]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[28]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[29]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[30]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[31]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[32]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("", fonts[0]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Add verbiage change - End

			// Row
			table = new PdfPTable(2);
			int tableheaderwidths[] = { 8, 92 }; // percentage
			table.setWidths(tableheaderwidths);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[19] + facsimile,
					fonts[11]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setColspan(2);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("", fonts[0]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setColspan(2);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(strDate, fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[20], fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			/*
			 * Adding image to footer. START
			 */

			// TO-DO get image from DB

			Image badge = null;

			badge = Image.getInstance(badgeImagePath);

			/*
			 * aspectRatio = 177.0f / 111.0f; if (logger.isDebugEnabled()) {
			 * logger.debug(
			 * "viewwo(String, String, String, String, String, DataSource, String, String, String, String, String) - "
			 * + aspectRatio); } y = 72.39f; badge.setDpi(96, 96);
			 * badge.scaleAbsolute(aspectRatio * y, y); badge.setBorder(0);
			 * 
			 * cell1 = new PdfPCell(badge);
			 * cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			 * cell1.setPaddingTop(5); cell1.setPaddingLeft(2);
			 * cell1.setPaddingBottom(2);
			 * 
			 * cell1.setBorder(0); table.addCell(cell1);
			 * 
			 * cell2 = new PdfPCell(); cell2.addElement(table);
			 * cell2.setColspan(1); maintable.addCell(cell2);
			 * maindocument.add(maintable);
			 */

			/*
			 * aspectRatio = 177.0f / 111.0f;
			 */
			/*
			 * y = 72.39f; badge.setDpi(96, 96); badge.scaleAbsolute(aspectRatio
			 * * y, y);
			 */

			badge.setBorder(0);

			cell1 = new PdfPCell(badge);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setPaddingTop(5);
			cell1.setPaddingLeft(2);
			cell1.setPaddingBottom(2);

			cell1.setBorder(0);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("", fonts[5]));
			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(1);
			maintable.addCell(cell2);
			// Adding image to footer. END

			maindocument.add(maintable);

			if (checkForAppendix) {
				addFieldSheetPage(maindocument, imagepath1, msaName,
						site_address_data[3]); // Add Field Sheet Page in WO Pdf
				// if checkForAppendix == true.
			}

			if (checkForCashSurveyReport) {
				addCashSurveyReportPage(writer, maindocument, facsimile,
						imagepath, site_address_data[3], siteName,
						site_address_data[4], site_address_data[5]); // Add Cash
			}
			maindocument.close();
		} catch (Exception de) {
			logger.error(
					"viewwo(String, String, String, String, String, DataSource, String, String, String, String, String)",
					de);

			logger.error(
					"viewwo(String, String, String, String, String, DataSource, String, String, String, String, String)",
					de);
		}

		finally {

			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
				DBUtil.close(rs1, cstmt);
				DBUtil.close(con);

			} catch (Exception e) {
				logger.error(
						"viewwo(String, String, String, String, String, DataSource, String, String, String, String, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewwo(String, String, String, String, String, DataSource, String, String, String, String, String) - Error while closing database connection: "
							+ e);
				}
			}
		}

		return outbuff.toByteArray();
	}

	public byte[] viewwo(String powoid, String imagepath, String imagepath1,
			String fontPath, String userid, DataSource ds,
			String WONwConfigImg, String WONwConfigImgheader, String icSnapOn,
			String icChrysler, String icCompuCom) {
		Document maindocument = new Document(PageSize.A4, 1, 1, 50, 50);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();
		Connection con = null;
		Connection conn = null;
		CallableStatement cstmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs1 = null;

		try {
			PdfWriter writer = PdfWriter.getInstance(maindocument, outbuff);

			/* Set font array */
			getBaseFont(fontPath);

			PdfPTable maintable = new PdfPTable(2);
			maintable.setWidthPercentage(95);
			PdfPTable table = new PdfPTable(3);
			PdfPTable table1 = new PdfPTable(1);
			table.setWidthPercentage(95);
			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			Paragraph para = new Paragraph();

			int x = 0;
			maindocument.open();

			PdfDestination pd = new PdfDestination(PdfDestination.XYZ, -1, -1,
					1);
			PdfAction action = PdfAction.gotoLocalPage(1, pd, writer);
			writer.setOpenAction(action);

			maindocument.setMargins(20, 20, 80f, 25f);

			/* Retrieve Array contents for generating WO Pdf file. */
			getPdfData();

			Color black = new Color(0, 0, 0);
			Color red = new Color(255, 0, 0);

			String strDate = "";
			Calendar c = Calendar.getInstance();
			strDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE)
					+ "/" + c.get(Calendar.YEAR);

			String lm_po_issue_date = null;
			String lm_wo_number = null;
			String lm_wo_deliver_by_date = null;
			String lm_wo_deliver_by_time = null;
			String lm_po_special_instructions = null;
			String lm_po_sow = null;
			String lm_po_assumption = null;
			String lx_pr_title = null;
			String[] pvs_address_data = new String[AddressData.length];
			String[] site_address_data = new String[SiteAddressData.length];
			String cns_pc_name = null;
			String login_name = null;
			String project_manager = null;
			String facsimile = null;
			String company_name = null;
			String company_address = null;
			String company_city = null;
			String company_state = null;
			String company_zipcode = null;
			String Cell = null;
			String lm_si_brand = "";
			boolean check = false;
			boolean checkForAssumption = false;
			boolean checkForNetMedx = false;
			String ticketNo = null;
			String appendixId = null;
			String msaName = null;
			String currentid = "";
			boolean checkForAppendix = false;
			boolean checkForCashSurveyReport = false;
			String siteName = "";
			boolean checkNwCert = false;
			String workmanship = "Unless Contingent specifically states otherwise in writting, all work will be completed to accepted BICSI, NEC, ANSI/TIA/EIA best practices, codes and standards.";

			/* Fetch data from database. */
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_po_wo_data(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, powoid);
			cstmt.setInt(3, Integer.parseInt(userid));
			String sql1 = "";
			String sql2 = "";
			Statement stmt1 = conn.createStatement();

			rs = cstmt.executeQuery();
			if (rs.next()) {
				lm_po_issue_date = rs.getString("lm_po_issue_date");
				lm_wo_number = rs.getString("lm_po_number");
				pvs_address_data[0] = rs.getString("pvs_compname"); // pvs_name
				pvs_address_data[1] = rs.getString("pvs_address"); // pvs_address
				pvs_address_data[2] = rs.getString("pvs_city_state_zip"); // pvs_city_state_zip
				pvs_address_data[3] = rs.getString("partner_poc_phone"); // pvs_city_state_zip
				pvs_address_data[4] = rs.getString("partner_poc_name"); // pvs_city_state_zip

				siteName = rs.getString("lm_si_name");

				if (rs.getString("lm_si_group") == null
						|| rs.getString("lm_si_group").equals("")) {
					site_address_data[1] = rs.getString("lm_si_name"); // site_name
					check = false;
				} else {
					site_address_data[1] = rs.getString("lm_si_group"); // site_name
					check = true;
				}

				site_address_data[3] = rs.getString("lm_si_number"); // site_number
				site_address_data[4] = rs.getString("lm_si_address"); // site_address
				site_address_data[5] = rs.getString("site_city_state_zip"); // site_city_state_zip
				site_address_data[2] = rs.getString("lm_si_work_location"); // work
				// location
				lm_po_special_instructions = rs
						.getString("lm_po_special_instructions"); // sp. inst.
				lx_pr_title = rs.getString("lx_pr_title");
				project_manager = rs.getString("cns_pc_name") + " "
						+ rs.getString("cns_pc_phone") + " " + "Cell: "
						+ rs.getString("cns_pc_cell");

				lm_wo_deliver_by_date = rs.getString("lm_wo_deliver_by_date");
				lm_wo_deliver_by_time = rs.getString("lm_wo_deliver_by_time");

				if (lm_wo_deliver_by_date.equals("")) {
					if (lx_pr_title.equalsIgnoreCase("NetMedX")) {
						lm_wo_deliver_by_date = rs
								.getString("lm_tc_preferred_arrival_date");
						lm_wo_deliver_by_time = rs
								.getString("lm_tc_preferred_arrival_time");
					} else {
						lm_wo_deliver_by_date = rs
								.getString("lm_js_planned_start_date");
						lm_wo_deliver_by_time = rs
								.getString("lm_js_planned_start_time");
					}
				}
				login_name = rs.getString("created_by");
				cns_pc_name = rs.getString("job_owner") + " "
						+ rs.getString("job_owner_phone") + " " + "Cell: "
						+ rs.getString("job_owner_cell");
				site_address_data[0] = rs.getString("lo_ot_name"); // client_name
				lm_si_brand = rs.getString("lm_si_group"); // Brand Name

				if (!lm_si_brand.equals(""))
					site_address_data[0] = site_address_data[0] + " - "
							+ lm_si_brand;

				company_name = rs.getString("company_name");
				company_address = rs.getString("company_address");
				company_city = rs.getString("company_city") + ',' + ' '
						+ rs.getString("company_state") + ' '
						+ rs.getString("company_zip");
				facsimile = rs.getString("company_fax");

				if (facsimile == null || facsimile.equals(""))
					facsimile = "513-860-2105";
				lm_po_sow = rs.getString("lm_po_sow");
				lm_po_assumption = rs.getString("lm_po_assumption");

				if (rs.getString("lm_po_assumption") == null
						|| rs.getString("lm_po_assumption").equals("")) {
					checkForAssumption = true;
				}

				if (rs.getString("lx_pr_type").equals("3")) {
					checkForNetMedx = true;
					ticketNo = rs.getString("lm_tc_number");
				}

				appendixId = rs.getString("lx_pr_id");
				msaName = rs.getString("lo_ot_name");
			}

			/*
			 * Check database tables to add Field Sheet/Cash Survey/Network -
			 * Certification Sheet pages in WO Pdf.
			 */
			con = ds.getConnection();
			stmt = con.createStatement();

			// Check for Field Sheet page.
			checkForAppendix = checkPage(stmt, appendixId,
					"select fs_pr_id from fieldsheet");

			// Check for Cash Survey Report page.
			checkForCashSurveyReport = checkPage(
					stmt,
					appendixId,
					"select csr_pr_id from CashSurveyReport_WO where csr_type_desc = 'CashSurveyReport'");

			// Check for Network Certification page.
			checkNwCert = checkPage(
					stmt,
					appendixId,
					"select csr_pr_id from CashSurveyReport_WO where csr_type_desc = 'Network Certification'");

			if (checkForAppendix || checkForCashSurveyReport) {
				facsimile = "888-556-0932";
			}

			// Create First Page.
			// Zeroth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[15], fonts[0]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);

			table = new PdfPTable(3);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			Image logo = Image.getInstance(imagepath);

			float aspectRatio = 381.0f / 148.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewwo(String, String, String, String, String, DataSource, String, String, String, String, String) - "
						+ aspectRatio);
			}
			float y = 72.39f;
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			cell1 = new PdfPCell(logo);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[0], fonts[5]));
			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[16] + " "
					+ strDate, fonts[3]));
			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);
			cell1.setBorder(0);
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell1.setBorder(1);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// First Row
			table = new PdfPTable(4);
			table.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[17] + " "
					+ lm_wo_number + "\n" + HeaderRowData[1] + " "
					+ lx_pr_title, fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);
			cell1.setNoWrap(true);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[2] + " "
					+ lm_wo_deliver_by_date + " " + lm_wo_deliver_by_time,
					fonts[10]));
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setColspan(2);
			cell1.setBorder(0);
			table.addCell(cell1);

			if (checkForNetMedx) {
				cell1 = new PdfPCell(new Paragraph(HeaderRowData[3] + " "
						+ lm_wo_number + "\n" + HeaderRowData[28] + " "
						+ ticketNo, fonts[7]));
			} else {
				cell1 = new PdfPCell(new Paragraph(HeaderRowData[3] + " "
						+ lm_wo_number, fonts[7]));
			}

			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell1.setBorder(1);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Second Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[4], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[5], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Third Row // Column One
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			if (!check) {
				for (x = 0; x < SiteAddressData.length; x++) {

					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_RIGHT);
					table.getDefaultCell().setBorder(0);
					table.addCell(new Paragraph(SiteAddressData[x], fonts[2]));

					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_LEFT);
					table.getDefaultCell().setBorder(0);
					table.addCell(new Paragraph(site_address_data[x], fonts[3]));
				}
			} else {
				for (x = 0; x < SiteAddressDataBrand.length; x++) {
					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_RIGHT);
					table.getDefaultCell().setBorder(0);
					table.addCell(new Paragraph(SiteAddressDataBrand[x],
							fonts[2]));

					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_LEFT);
					table.getDefaultCell().setBorder(0);
					table.addCell(new Paragraph(site_address_data[x], fonts[3]));
				}
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			maintable.addCell(cell2);

			// Third Row Column Two
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[24], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_name, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[25], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_address, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[26], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_city, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[27], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(cns_pc_name, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[22], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(project_manager, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(HeaderRowData[23], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(facsimile, fonts[3]));

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Fourth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[6], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[7], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Fifth Row Column One
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			for (x = 0; x < AddressData.length; x++) {
				table.getDefaultCell().setHorizontalAlignment(
						Element.ALIGN_RIGHT);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(AddressData[x], fonts[2]));

				table.getDefaultCell().setHorizontalAlignment(
						Element.ALIGN_LEFT);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(pvs_address_data[x], fonts[3]));
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			maintable.addCell(cell2);

			// Fifth Row Column Two
			table = new PdfPTable(1);
			table.setWidthPercentage(100);

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(fonts[13].getCalculatedLeading(0.1f),
					BehavioralGLData, fonts[2]));

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Sixth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[8], fonts[11]));
			cell1.setBackgroundColor(red);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Seventh Row
			table = new PdfPTable(2);
			table.setWidthPercentage(100);
			int headerwidths[] = { 15, 85 }; // percentage
			table.setWidths(headerwidths);
			table.getDefaultCell().setBorder(1);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[9], fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setPaddingBottom(50);
			table.addCell(cell1);

			table1 = new PdfPTable(HeaderData1.length);
			table1.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(DescWRData[2] + " - "
					+ lm_po_sow + "\n", fonts[2]));
			cell1.setBorder(0);
			cell1.setColspan(4);
			cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			table1.addCell(cell1);

			if (checkForAssumption)
				cell1 = new PdfPCell(new Paragraph("", fonts[2]));
			else
				cell1 = new PdfPCell(new Paragraph(DescWRData[3] + " - "
						+ lm_po_assumption, fonts[2]));

			cell1.setBorder(0);
			cell1.setColspan(4);
			cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			table1.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table1);
			cell2.setColspan(2); // add by hamza
			table.addCell(cell2);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[10], fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setPaddingBottom(50);
			table.addCell(cell1);

			String imageabslpath = "https://medius.contingent.com/images/TigerTeam.png?1";
			File f = new File(System.getProperty("catalina.base"));
			// String abslpath = "/webapps/Ilex/images/TigerTeam.png";

			// logger.error(imageabslpath
			// + " -------------------------------------- " + f);

			// Image image = Image.getInstance(f.getPath()
			// + "/webapps/Ilex/images/TigerTeam.png");

			Image image = Image.getInstance(imageabslpath);

			// File f = new File(System.getProperty("catalina.base"));
			// String abslpath = "/webapps/Ilex/images/TigerTeam.png";

			// Image image = Image.getInstance(f.getPath()
			// + "/webapps/Ilex/images/TigerTeam.png");
			// System.out.println(image);

			if (appendixId.equals("2023") || appendixId.equals("3887")) {

				// JUST ADD CODE FOR Tiger Name Image

				PdfPCell cell;
				float[] widths = { 2, 1 };
				PdfPTable nestedTable = new PdfPTable(widths);

				logger.error(" in side the if ------------------------------------------------------------");
				nestedTable.addCell(new Phrase(lm_po_special_instructions,
						fonts[6]));

				/* aspectRatio = 177.0f / 111.0f; */
				image.setAlignment(2);
				/* y = 52.39f; */
				/*
				 * image.setDpi(76, 76); image.scaleAbsolute(aspectRatio * y,
				 * y); image.setBorder(0);
				 */
				nestedTable.addCell(setCellImageAttribute(image,
						Element.ALIGN_RIGHT, 0, 2, 2, 5, 10,
						Rectangle.NO_BORDER, 1));
				table.addCell(nestedTable);
				logger.error(" done with image...........................................");
			} else {
				cell1 = new PdfPCell(new Paragraph(lm_po_special_instructions,
						fonts[6]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);

				table.addCell(cell1);

			}

			// add condition //

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[30], fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setMinimumHeight(0f);
			table.addCell(cell1);

			// corrected placement of workmanship in WO pdf
			cell1 = new PdfPCell(new Paragraph(workmanship, fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			table.addCell(cell1);

			// deliverable Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[31], fonts[7]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setMinimumHeight(0f);
			cell1.setPaddingBottom(50);
			table.addCell(cell1);

			PdfPCell cell3 = new PdfPCell();
			cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell3.setMinimumHeight(0f);

			PdfPTable table2 = new PdfPTable(1);
			table2.setWidthPercentage(100);
			int innerTableWidth[] = { 100 }; // percentag
			table2.setWidths(innerTableWidth);
			// table2.getDefaultCell().setBorder(0);

			List<String> deliverableTitles = getDeliverableTitles(powoid, ds);
			int count = 0;
			for (String deliverableTitle : deliverableTitles) {
				cell1 = new PdfPCell(new Paragraph(++count + ". "
						+ deliverableTitle, fonts[2]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setMinimumHeight(0f);
				cell1.setBorder(0);
				table2.addCell(cell1);
			}

			cell3.addElement(table2);

			table.addCell(cell3);

			// deliverable Row ends

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Eighth Row
			table = new PdfPTable(1);
			table.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[11], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Ninth Row
			String temp_str = "";
			table = new PdfPTable(HeaderData.length);
			table.setWidthPercentage(100);
			table.getDefaultCell().setBorder(1);

			for (x = 0; x < HeaderData.length; x++) {
				if (x < 2)
					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_LEFT);
				if (x > 1)
					table.getDefaultCell().setHorizontalAlignment(
							Element.ALIGN_RIGHT);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(HeaderData[x], fonts[4]));
			}

			for (int j = 0; j < 2; j++) {
				for (x = 0; x < HeaderData.length; x++) {
					temp_str = "";
					if (x < 2)
						table.getDefaultCell().setHorizontalAlignment(
								Element.ALIGN_LEFT);
					if (x > 1)
						table.getDefaultCell().setHorizontalAlignment(
								Element.ALIGN_RIGHT);
					if (x == 0)
						temp_str = "00" + (x + 1 + j);
					table.addCell(new Paragraph(temp_str, fonts[2]));
				}
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Tenth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[12], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Row
			table = new PdfPTable(1);
			table.setWidthPercentage(100);

			for (x = 0; x < ScheInfoData.length; x++) {
				cell1 = new PdfPCell(new Paragraph(ScheInfoData[x], fonts[2]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setBorder(0);
				table.addCell(cell1);
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Tenth Row
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[13], fonts[11]));
			cell1.setBackgroundColor(black);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Row
			table = new PdfPTable(2);
			table.setWidthPercentage(100);
			int headerwidth[] = { 30, 70 }; // percentage
			table.setWidths(headerwidth);

			table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(SiteContactData[0], fonts[9]));
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setBorder(0);
			table1.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(SiteContactData[1], fonts[0]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);
			table1.addCell(cell1);

			for (x = 2; x < SiteContactData.length - 1; x++) {
				cell1 = new PdfPCell(
						new Paragraph(SiteContactData[x] + " "
								+ SiteContactData[SiteContactData.length - 1],
								fonts[0]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setBorder(0);
				table1.addCell(cell1);
			}

			cell1 = new PdfPCell();
			cell1.addElement(table1);
			table.addCell(cell1);

			table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(SiteContactData1[0], fonts[8]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);
			table1.addCell(cell1);

			for (x = 1; x < SiteContactData1.length; x++) {
				cell1 = new PdfPCell(new Paragraph(SiteContactData1[x],
						fonts[2]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setBorder(0);
				table1.addCell(cell1);

				cell1 = new PdfPCell(new Paragraph("  ", fonts[2]));
				cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell1.setBorder(0);
				table1.addCell(cell1);
			}

			cell1 = new PdfPCell();
			cell1.addElement(table1);
			table.addCell(cell1);
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Add verbiage change - start

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[33],
					fonts[3]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[0],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			StringBuilder verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[1]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[2]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[3]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[4],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[5]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[6]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[7]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[8]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[9]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[10]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[11]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[12]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[13]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[14],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[15]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[16]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[17],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[18]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[19]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[20]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[21]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[22]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[23]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[24]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[25]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(ExpectationVerbiageData[26],
					fonts[22]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			verbiagePoints = new StringBuilder();
			verbiagePoints.append(ExpectationVerbiageData[27]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[28]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[29]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[30]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[31]);
			verbiagePoints.append("\n");
			verbiagePoints.append(ExpectationVerbiageData[32]);

			cell1 = new PdfPCell(new Paragraph(verbiagePoints.toString(),
					fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("", fonts[0]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Add verbiage change - End

			// Row
			table = new PdfPTable(2);
			int tableheaderwidths[] = { 8, 92 }; // percentage
			table.setWidths(tableheaderwidths);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(HeaderRowData[19] + facsimile,
					fonts[11]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setColspan(2);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("", fonts[0]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setColspan(2);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(strDate, fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(HeaderRowData[20], fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell1);

			String withoutbadge = "IF YOU DO NOT SEE A BADGE PHOTO INSERTED HERE, PLEASE VISIT YOUR PORTAL TO UPLOAD AND RECEIVE A BADGE FOR FUTURE SITE VISITS https://www.contingent.com/partner_login";

			cell1 = new PdfPCell(new Paragraph(withoutbadge, fonts[8]));
			cell1.setBorder(0);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setColspan(2);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);
			maindocument.add(maintable);

			/* Add Network Certification Installation Checklist Sheet. : Begin */
			if (checkNwCert) {
				showNetworkConfiguration(writer, maindocument, lm_wo_number,
						site_address_data[3], ds);
				showCertificationSheet(writer, maindocument, lm_wo_number,
						imagepath1, WONwConfigImg, WONwConfigImgheader,
						siteName, site_address_data[3]); // Call Method :
				// showCertificationSheet
				// start
				showInstallationChecklist(writer, maindocument, siteName,
						WONwConfigImgheader, site_address_data[3],
						WONwConfigImg, imagepath1);
			}
			/* Add Network Certification Installation Checklist Sheet. : End */

			if (checkForAppendix) {
				addFieldSheetPage(maindocument, imagepath1, msaName,
						site_address_data[3]); // Add Field Sheet Page in WO Pdf
				// if checkForAppendix == true.
			}

			if (checkForCashSurveyReport) {
				addCashSurveyReportPage(writer, maindocument, facsimile,
						imagepath, site_address_data[3], siteName,
						site_address_data[4], site_address_data[5]); // Add Cash
			}
			maindocument.close();
		} catch (Exception de) {
			logger.error(
					"viewwo(String, String, String, String, String, DataSource, String, String, String, String, String)",
					de);

			logger.error(
					"viewwo(String, String, String, String, String, DataSource, String, String, String, String, String)",
					de);
		} finally {

			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
				DBUtil.close(rs1, cstmt);
				DBUtil.close(con);

			} catch (Exception e) {
				logger.error(
						"viewwo(String, String, String, String, String, DataSource, String, String, String, String, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewwo(String, String, String, String, String, DataSource, String, String, String, String, String) - Error while closing database connection: "
							+ e);
				}
			}
		}

		return outbuff.toByteArray();
	}

	/**
	 * Method: checkPage - Check if Field Sheet/ Cash Survey Report/ Newtwork -
	 * Certification Sheet page is to be shown in this WO Pdf or not.
	 * 
	 * @param stmt
	 *            - Statement Object.
	 * @param appendixId
	 *            - Appendix Id.
	 * @param sql
	 *            - String variable that holds sql statement.
	 * @return boolean - This method returns boolean value(true/false).
	 */

	private boolean checkPage(Statement stmt, String appendixId, String sql)
			throws SQLException {
		boolean isCheck = false;
		ResultSet rs1;
		String currentid;
		rs1 = stmt.executeQuery(sql);
		while (rs1.next()) {
			currentid = rs1.getString(1);
			if (appendixId.equals(currentid)) {
				isCheck = true;
				break;
			}
		}
		return isCheck;
	}

	/**
	 * Method: getPdfData - Set value for array objects that are to be used to
	 * generate WO Pdf.
	 * 
	 * @return void - This method does not return anything.
	 */
	private void getPdfData() {
		/* Retrieve Array contents for generating WO Pdf file. */
		HeaderRowData = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.HEADER_ROW_DATA);
		BehavioralGLData = "1. Smile and be friendly, courteous and helpful\n2. Be properly groomed\n3. Be on time and only in authorized areas of the site\n4. Answer questions in a mature, direct and thoughtful manner\n5. Complete the assigned tasks in the most expeditious manner possible\n6. Report any new opportunities that may arise directly to Contingent\n7. Please do not discuss price or attempt to compete with Contingent";
		DescWRData = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.DESC_WR_DATA);
		HeaderData1 = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.HEADER_DATA_1);
		HeaderData = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.HEADER_DATA);
		AddressData = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.ADDRESS_DATA);
		AddressData1 = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.ADDRESS_DATA_1);
		SiteAddressData = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.SITE_ADDRESS_DATA);
		SiteAddressDataBrand = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.SITE_ADDRESS_DATA_BRAND);
		ScheInfoData = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.SCHE_INFO_DATA);
		SiteContactData = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.SITE_CONTACT_DATA);
		SiteContactData1 = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.SITE_CONTACT_DATA_1);
		ExpectationVerbiageData = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.EXPECTATION_VERBIAGE);
	}

	/**
	 * Method: getBaseFont - Set value for class array variable fonts.
	 * 
	 * @param fontPath
	 *            - Path to get the Font class.
	 * @return void - This method does not return anything.
	 */

	public void getBaseFont(String fontPath) throws DocumentException,
			IOException {
		BaseFont bfArialN = BaseFont.createFont(fontPath, BaseFont.IDENTITY_H,
				BaseFont.EMBEDDED);
		fonts[0] = new Font(bfArialN, 8, Font.BOLD);
		fonts[1] = new Font(bfArialN, 10, Font.BOLD | Font.ITALIC);
		fonts[2] = new Font(bfArialN, 7, Font.NORMAL);
		fonts[3] = new Font(bfArialN, 7, Font.BOLD);
		fonts[4] = new Font(bfArialN, 7, Font.NORMAL | Font.UNDERLINE);
		fonts[5] = new Font(bfArialN, 14, Font.BOLD);
		fonts[6] = new Font(bfArialN, 6.5f, Font.NORMAL);
		fonts[7] = new Font(bfArialN, 8, Font.NORMAL);
		fonts[8] = new Font(bfArialN, 6, Font.NORMAL);
		fonts[9] = new Font(Font.TIMES_ROMAN, 7, Font.ITALIC);
		fonts[10] = new Font(bfArialN, 9, Font.BOLD);
		fonts[11] = new Font(bfArialN, 8, Font.BOLD);
		fonts[12] = new Font(bfArialN, 11, Font.BOLD);
		fonts[13] = new Font(bfArialN, 1, Font.NORMAL);
		fonts[14] = new Font(bfArialN, 9, Font.BOLD | Font.UNDERLINE);
		fonts[15] = new Font(bfArialN, 10, Font.NORMAL);
		fonts[16] = new Font(bfArialN, 10, Font.BOLD | Font.UNDERLINE);
		fonts[17] = new Font(bfArialN, 10, Font.BOLD | Font.UNDERLINE
				| Font.ITALIC);
		fonts[18] = new Font(bfArialN, 12, Font.BOLD | Font.ITALIC);
		fonts[19] = new Font(bfArialN, 10, Font.ITALIC);
		fonts[20] = new Font(bfArialN, 12, Font.NORMAL);
		fonts[21] = new Font(bfArialN, 11, Font.BOLD | Font.HELVETICA);
		fonts[22] = new Font(bfArialN, 6, Font.BOLD);
		fonts[11].setColor(255, 255, 255);
		fonts[12].setColor(new Color(51, 51, 153));
		fonts[0].setColor(new Color(0, 0, 0));
	}

	/**
	 * Method: addFieldSheetPage - To add Field Sheet page to WO PDF.
	 * 
	 * @param document
	 *            - Maindocument.
	 * @param imagePath
	 *            - Path for checkbox image.
	 * @param msaName
	 *            - MSA Name.
	 * @param siteNumber
	 *            - Site Number.
	 * @return void - This method does not return anything.
	 */

	public static void addFieldSheetPage(Document document, String imagePath,
			String msaName, String siteNumber) {
		document.setMargins(10, 10, 60f, 25f);
		PdfPTable maintable = null;
		PdfPTable table = null;
		PdfPCell cell1 = null;
		PdfPCell cell2 = null;

		String DollarGeneral[] = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.DOLLAR_GENERAL);
		String Register[] = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.REGISTER);
		try {
			document.newPage();
			maintable = new PdfPTable(2);
			maintable.setWidthPercentage(99);

			// Add a blank row.
			table = new PdfPTable(2);
			float[] arr = { 3, 97 };
			table.setWidths(arr);
			table.setWidthPercentage(100);

			table.addCell(setCellAttribute(msaName + DollarGeneral[0]
					+ siteNumber, fonts[10], Element.ALIGN_CENTER, 0, 2, 2, 2,
					10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[1] + "      "
					+ DollarGeneral[2], fonts[7], Element.ALIGN_LEFT, 0, 2, 2,
					2, 10, Rectangle.NO_BORDER, 2));
			Image logo1 = Image.getInstance(imagePath);
			table.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT, 0,
					2, 2, 5, 10, Rectangle.NO_BORDER, 1));

			table.addCell(setCellAttribute(DollarGeneral[3], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT, 0,
					2, 2, 5, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(DollarGeneral[4], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute("", fonts[7], Element.ALIGN_CENTER,
					0, 2, 2, 2, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(Register[0], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 1));

			table.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT, 0,
					2, 2, 5, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(DollarGeneral[5], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute("", fonts[7], Element.ALIGN_CENTER,
					0, 2, 2, 2, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(Register[1], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 1));

			table.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT, 0,
					2, 2, 5, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(DollarGeneral[6], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute("", fonts[7], Element.ALIGN_CENTER,
					0, 2, 2, 2, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(Register[2], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 1));

			table.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT, 0,
					2, 2, 5, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(DollarGeneral[7], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute("", fonts[7], Element.ALIGN_CENTER,
					0, 2, 2, 2, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(Register[3], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 1));

			table.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT, 0,
					2, 2, 5, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(DollarGeneral[8], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));

			table.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT, 0,
					2, 2, 5, 10, Rectangle.NO_BORDER, 1));
			table.addCell(setCellAttribute(DollarGeneral[9], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[10], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[11], fonts[14],
					Element.ALIGN_CENTER, 0, 2, 2, 2, 10, Rectangle.NO_BORDER,
					2));
			table.addCell(setCellAttribute(DollarGeneral[12], fonts[10],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[13], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[14], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[15], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 2, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[16], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[17], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[18], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[19], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 2, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[20], fonts[7],
					Element.ALIGN_LEFT, 0, 2, 2, 2, 10, Rectangle.NO_BORDER, 2));
			table.addCell(setCellAttribute(DollarGeneral[21], fonts[3],
					Element.ALIGN_CENTER, 0, 2, 2, 2, 2, Rectangle.NO_BORDER, 2));

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);
			document.add(maintable);
		} catch (Exception e) {
			logger.error("addFieldSheetPage(Document, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addFieldSheetPage(Document, String, String, String) - Error occurred while adding Field Sheet page in WO Pdf: "
						+ e);
			}
		}
	}

	/**
	 * Method: addCashSurveyReportPage - To add Cah Survey Report page to WO
	 * PDF.
	 * 
	 * @param writer
	 *            - PdfWriter.
	 * @param document
	 *            - Maindocument.
	 * @param facsimile
	 *            - Fax Number.
	 * @param imagepath
	 *            - Path for image that shows Contingent logo.
	 * @param siteNumber
	 *            - Site Number.
	 * @param siteName
	 *            - Site Name.
	 * @param siteAddress1
	 *            - Site Address.
	 * @param siteAddress2
	 *            - Site City, State and Zip.
	 * @return void - This method does not return anything.
	 */

	public static void addCashSurveyReportPage(PdfWriter writer,
			Document document, String facsimile, String imagepath,
			String siteNumber, String siteName, String siteAddress1,
			String siteAddress2) {
		document.setMargins(20, 20, 120, 70);
		PdfPTable maintable = null;
		PdfPTable table = null;
		PdfPCell cell1 = null;
		PdfPCell cell2 = null;
		int n = 0;

		String CashSurveyReport[] = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.CASH_SURVEY_REPORT);

		try {
			for (int j = 0; j < 3; j++) {
				document.newPage();

				// Set PdfPTable as header - Start
				setCSRHeader(writer, document, facsimile, imagepath);
				// Set PdfPTable as header - End

				maintable = new PdfPTable(2);
				maintable.setWidthPercentage(95);

				table = new PdfPTable(2);
				table.setWidthPercentage(100);
				table.getDefaultCell();

				cell1 = new PdfPCell(new Paragraph(" ", fonts[11])); // Row with
				// Black
				// Fill.
				cell1.setBackgroundColor(new Color(0, 0, 0));
				cell1.setColspan(2);
				table.addCell(cell1);

				cell2 = new PdfPCell();
				cell2.addElement(table);

				cell1 = new PdfPCell(new Paragraph("Site Number:  "
						+ siteNumber, fonts[15])); // Row with Site
				// Number.
				table.addCell(cell1);

				table.getDefaultCell();
				cell1 = new PdfPCell(new Paragraph("Site Name:  " + siteName,
						fonts[15])); // Row with Site Name.
				table.addCell(cell1);

				cell2 = new PdfPCell();
				cell2.addElement(table);

				cell1 = new PdfPCell(new Paragraph("Site Address:  "
						+ siteAddress1 + " " + siteAddress2, fonts[15])); // Row
				// with
				// Site
				// Address.
				cell1.setColspan(2);
				table.addCell(cell1);

				cell2 = new PdfPCell();
				cell2.addElement(table);
				cell2.setColspan(2);
				cell2.setBorder(0);
				cell2.setPaddingBottom(25);
				maintable.addCell(cell2);

				table = new PdfPTable(4);
				table.setWidthPercentage(100);
				table.getDefaultCell().setBorder(1);

				/* Creation of Cash Wrap Block - Start */
				for (int i = 0; i < 3; i++) {

					table.addCell(setCellAttribute("Cash Wrap " + (n + 1),
							fonts[15], Element.ALIGN_CENTER, 0, 0, 0, 2, 2,
							Rectangle.BOX, 4));
					cell2 = new PdfPCell();
					cell2.addElement(table);

					table.addCell(setCellAttribute(" ", fonts[15],
							Element.ALIGN_LEFT, 0, 0, 0, 2, 2, Rectangle.BOX, 1));
					table.addCell(setCellAttribute(CashSurveyReport[0],
							fonts[15], Element.ALIGN_CENTER, 0, 0, 0, 2, 2,
							Rectangle.BOX, 1));
					table.addCell(setCellAttribute(CashSurveyReport[1],
							fonts[15], Element.ALIGN_CENTER, 0, 0, 0, 2, 2,
							Rectangle.BOX, 1));
					table.addCell(setCellAttribute(CashSurveyReport[2],
							fonts[15], Element.ALIGN_CENTER, 0, 0, 0, 2, 2,
							Rectangle.BOX, 1));
					cell2 = new PdfPCell();
					cell2.addElement(table);

					/* Fill data for Cash Wrap Block. */
					for (int k = 3; k < CashSurveyReport.length; k++) {
						Font f = new Font();
						if (k == 9) {
							f = fonts[7];
						} else {
							f = fonts[15];
						}
						table.addCell(setCellAttribute(CashSurveyReport[k], f,
								Element.ALIGN_LEFT, 0, 2, 2, 2, 2,
								Rectangle.BOX, 1));
						table.addCell(setCellAttribute("", fonts[15],
								Element.ALIGN_LEFT, 0, 2, 2, 2, 2,
								Rectangle.BOX, 1));
						table.addCell(setCellAttribute("", fonts[15],
								Element.ALIGN_LEFT, 0, 2, 2, 2, 2,
								Rectangle.BOX, 1));
						table.addCell(setCellAttribute("", fonts[15],
								Element.ALIGN_LEFT, 0, 2, 2, 2, 2,
								Rectangle.BOX, 1));
						cell2 = new PdfPCell();
						cell2.addElement(table);
					}

					if (i != 2) {
						cell1 = new PdfPCell(new Paragraph(" ", fonts[11])); // Row
						cell1.setBackgroundColor(new Color(0xC0, 0xC0, 0xC0));
						cell1.setColspan(4);
						table.addCell(cell1);

						cell2 = new PdfPCell();
						cell2.addElement(table);
					}

					cell2.setBorder(0);
					cell2.setColspan(2);
					if (i == 2) {
						cell2.setPaddingBottom(15);
					}
					n++;
				}
				/* Creation of Cash Wrap Block - End */
				maintable.addCell(cell2);
				document.add(maintable);

				setCSRFooter(writer, document, facsimile, j); // Set PdfPTable
				// as footer.
			}
		} catch (Exception e) {
			logger.error(
					"addCashSurveyReportPage(PdfWriter, Document, String, String, String, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addCashSurveyReportPage(PdfWriter, Document, String, String, String, String, String, String) - Error occurred while adding Cash Survey Report page to WO pdf: "
						+ e);
			}
		}

	}

	/**
	 * Method: setCSRHeader - To add a PdfPTable as header for Cash Survey
	 * Report Pages.
	 * 
	 * @param writer
	 *            - PdfWriter.
	 * @param document
	 *            - Maindocument.
	 * @param facsimile
	 *            - facsimile.
	 * @param imagepath
	 *            - Path for logo to be displayed in header.
	 * @return void - This method does not return anything.
	 */

	public static void setCSRHeader(PdfWriter writer, Document document,
			String facsimile, String imagepath) {
		try {
			Rectangle page = document.getPageSize();
			PdfPCell cell1 = null;
			PdfPTable CSRHeader = new PdfPTable(3);
			CSRHeader.setWidthPercentage(85);
			CSRHeader.getDefaultCell().setBorder(0);

			cell1 = new PdfPCell(new Phrase(
					"Fax Results back to: " + facsimile, fonts[16]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell1.setPaddingLeft(15);
			cell1.setBorder(0);
			CSRHeader.addCell(cell1);

			Image logoHeader = Image.getInstance(imagepath);
			logoHeader.scaleAbsolute(90, 55);
			cell1 = new PdfPCell(logoHeader);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell1.setBorder(0);
			CSRHeader.addCell(cell1);

			cell1 = new PdfPCell(new Phrase("Cash Wrap Survey", fonts[17]));
			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell1.setPaddingRight(15);
			cell1.setBorder(0);
			CSRHeader.addCell(cell1);

			cell1 = new PdfPCell();
			cell1.setColspan(3);
			cell1.setPaddingBottom(25);
			cell1.setBorder(0);
			CSRHeader.addCell(cell1);

			CSRHeader.setTotalWidth(page.getWidth() - document.leftMargin()
					- document.rightMargin());
			CSRHeader.writeSelectedRows(
					0,
					-1,
					document.leftMargin(),
					page.getHeight() - document.topMargin()
							+ CSRHeader.getTotalHeight(),
					writer.getDirectContent());

		} catch (Exception e) {
			logger.error("setCSRHeader(PdfWriter, Document, String, String)", e);

			throw new ExceptionConverter(e);
		}

	}

	/**
	 * Method: setCSRFooter - To add a PdfPTable as footer for Cash Survey
	 * Report Pages.
	 * 
	 * @param writer
	 *            - PdfWriter.
	 * @param document
	 *            - Maindocument.
	 * @param facsimile
	 *            - facsimile.
	 * @param j
	 *            - Used for displaying value as Page Number.
	 * @return void - This method does not return anything.
	 */

	public static void setCSRFooter(PdfWriter writer, Document document,
			String facsimile, int j) {
		try {
			Rectangle page = document.getPageSize();
			PdfPCell cell1 = null;

			PdfPTable CSRFooter = new PdfPTable(2);
			CSRFooter.setWidthPercentage(85);
			CSRFooter.getDefaultCell().setBorder(0);

			cell1 = new PdfPCell(new Phrase("Fax Results to: " + facsimile,
					fonts[15]));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell1.setPaddingLeft(15);
			cell1.setBorder(0);
			CSRFooter.addCell(cell1);

			cell1 = new PdfPCell(new Phrase("Page " + (j + 1), fonts[15]));
			cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell1.setPaddingRight(15);
			cell1.setBorder(0);
			CSRFooter.addCell(cell1);

			// Add a black line as a part of footer - Start.
			float pageWidth = (page.getWidth() - document.leftMargin() - document
					.rightMargin());
			PdfContentByte cb = writer.getDirectContent();
			cb.setLineWidth(2.0f); // Make a bit thicker than 1.0 default
			cb.setGrayStroke(0f);
			cb.moveTo((document.leftMargin() + 15), document.bottomMargin() + 5);
			cb.lineTo((page.getWidth() - document.rightMargin() - 15),
					document.bottomMargin() + 5);
			cb.stroke();
			// Add a black line as a part of footer - End.

			CSRFooter.setTotalWidth(page.getWidth() - document.leftMargin()
					- document.rightMargin());
			CSRFooter.writeSelectedRows(0, -1, document.leftMargin(),
					document.bottomMargin(), writer.getDirectContent());
		} catch (Exception e) {
			logger.error("setCSRFooter(PdfWriter, Document, String, int)", e);

			throw new ExceptionConverter(e);
		}
	}

	/**
	 * Method: showNetworkConfiguration - To add Network Configuration page to
	 * WO PDF.
	 * 
	 * @param writer
	 *            - PdfWriter.
	 * @param document
	 *            - Maindocument.
	 * @param workOrderNo
	 *            - Work Order Number.
	 * @param siteNumber
	 *            - Site Number.
	 * @param ds
	 *            - Data Source.
	 * @return void - This method does not return anything.
	 */

	public static void showNetworkConfiguration(PdfWriter writer,
			Document document, String workOrderNo, String siteNumber,
			DataSource ds) {

		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		Statement stmt = null;

		try {
			document.setMargins(15, 20, 80, 70);
			document.newPage();
			PdfPTable showNWConfig = null;
			PdfPCell cellNWC1 = null;
			PdfPCell cellNWC2 = null;

			String showNetworkConfig[] = StoreWOArrayContents
					.getArrayContents(StoreWOArrayContents.NETWORK_SHEET);

			showNWConfig = new PdfPTable(4);
			showNWConfig.setWidthPercentage(85);
			showNWConfig.getDefaultCell().setBorder(1);
			showNWConfig.setWidths(new float[] { 10.0f, 40.0f, 25.0f, 10.0f });
			// Row 1
			showNWConfig.addCell(setCellAttribute("Dealer ", fonts[10],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 0, 1,
					Rectangle.NO_BORDER, 1));
			showNWConfig.addCell(setCellAttribute(siteNumber, fonts[15],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2f, 0, 0, 1,
					Rectangle.NO_BORDER, 1));
			showNWConfig.addCell(setCellAttribute("Work Order # ", fonts[10],
					Element.ALIGN_RIGHT, Element.ALIGN_BOTTOM, 2, 0, 0, 1,
					Rectangle.NO_BORDER, 1));
			showNWConfig.addCell(setCellAttribute(workOrderNo, fonts[15],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 5, 0, 0, 1,
					Rectangle.NO_BORDER, 1));
			showNWConfig.addCell(setCellAttribute("", fonts[15],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 0, 10,
					Rectangle.NO_BORDER, 4));
			// Row 2
			showNWConfig.addCell(setCellAttribute("", fonts[15],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 0, 10,
					Rectangle.NO_BORDER, 1));
			showNWConfig.addCell(setCellAttribute("Network Configuration",
					fonts[10], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0,
					0, 1, Rectangle.NO_BORDER, 3));
			showNWConfig.addCell(setCellAttribute("", fonts[15],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 10, 10,
					Rectangle.NO_BORDER, 4));
			con = ds.getConnection();
			// siteNumber="5002";
			cstmt = con.prepareCall("{?=call lm_snapon_wo_info(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, siteNumber);
			rs = cstmt.executeQuery();
			int i = 0;
			int j = 3;
			boolean checksitedata = false;
			/*
			 * Show data from table if Dealer Code exists for WO else show blank
			 * fields for Network page.
			 */
			if (rs.next()) {
				checksitedata = true;
				for (; i < (showNetworkConfig.length) - 2; i++, j++) {
					String showNwValue = rs.getString(j);
					if (rs.getMetaData().getColumnName(j)
							.equals("lm_sip_low_high_range")) {
						showNwValue = showNwValue.replaceAll("~", "\n");
					}

					showNWConfig.addCell(setCellAttribute("", fonts[15],
							Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2, 7,
							2, Rectangle.NO_BORDER, 1));
					showNWConfig
							.addCell(setCellAttribute(showNetworkConfig[i],
									fonts[15], Element.ALIGN_LEFT,
									Element.ALIGN_BOTTOM, 3, 2, 7, 2,
									Rectangle.BOX, 1));
					showNWConfig
							.addCell(setCellAttribute(showNwValue, fonts[15],
									Element.ALIGN_LEFT, Element.ALIGN_BOTTOM,
									3, 2, 7, 2, Rectangle.BOX, 1));
					showNWConfig.addCell(setCellAttribute("", fonts[15],
							Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2, 7,
							2, Rectangle.NO_BORDER, 1));

				}
			} else {
				for (i = 0; i < (showNetworkConfig.length) - 2; i++) {

					showNWConfig.addCell(setCellAttribute("", fonts[15],
							Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2, 7,
							2, Rectangle.NO_BORDER, 1));
					showNWConfig
							.addCell(setCellAttribute(showNetworkConfig[i],
									fonts[15], Element.ALIGN_LEFT,
									Element.ALIGN_BOTTOM, 3, 2, 7, 2,
									Rectangle.BOX, 1));
					showNWConfig.addCell(setCellAttribute("", fonts[15],
							Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 3, 2, 7,
							2, Rectangle.BOX, 1));
					showNWConfig.addCell(setCellAttribute("", fonts[15],
							Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2, 7,
							2, Rectangle.NO_BORDER, 1));

				}
			}

			// Table for Site Contact

			if (checksitedata
					&& (!rs.getString(j).equals("") || !rs.getString(j + 1)
							.equals(""))) {
				PdfPTable showsiteData = new PdfPTable(2);
				showsiteData.setWidthPercentage(100);
				showsiteData.setWidths(new float[] { 30.0f, 70.0f });

				showNWConfig.addCell(setCellAttribute("", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 0, 10,
						Rectangle.NO_BORDER, 1));
				showNWConfig.addCell(setCellAttribute("Site Contact",
						fonts[10], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2,
						0, 20, 4, Rectangle.NO_BORDER, 3));
				showNWConfig.addCell(setCellAttribute("", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 5, 5,
						Rectangle.NO_BORDER, 4));

				for (; i < showNetworkConfig.length; i++, j++) {
					showsiteData
							.addCell(setCellAttribute(showNetworkConfig[i],
									fonts[15], Element.ALIGN_LEFT,
									Element.ALIGN_BOTTOM, 3, 2, 7, 2,
									Rectangle.BOX, 1));
					showsiteData
							.addCell(setCellAttribute(rs.getString(j),
									fonts[15], Element.ALIGN_LEFT,
									Element.ALIGN_BOTTOM, 3, 2, 7, 2,
									Rectangle.BOX, 1));
				}

				PdfPCell cellSiteData = new PdfPCell(showsiteData);
				cellSiteData.setColspan(2);
				cellSiteData.setBorder(Rectangle.BOX);

				showNWConfig.addCell(setCellAttribute("", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 0, 10,
						Rectangle.NO_BORDER, 1));
				showNWConfig.addCell(cellSiteData);
				showNWConfig.addCell(setCellAttribute("", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 10, 10,
						Rectangle.NO_BORDER, 4));

			}

			// Fetch Data from database
			String sql = " select lm_spc_id,lm_spc_sys_name,lm_spc_os from lm_snapon_wo_pc_config where lm_spc_so_id = (select lm_so_id from lm_snapon_wo_xml where lm_so_dlr_no = '"
					+ siteNumber + "')";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				// Row heading for second table PC Configuration
				showNWConfig.addCell(setCellAttribute("", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 10, 5,
						Rectangle.NO_BORDER, 1));
				showNWConfig.addCell(setCellAttribute("PC Configuration",
						fonts[10], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2,
						0, 20, 4, Rectangle.NO_BORDER, 3));
				showNWConfig.addCell(setCellAttribute("", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 5, 5,
						Rectangle.NO_BORDER, 4));

				PdfPTable showNWConfigPcConf = new PdfPTable(3);
				showNWConfigPcConf.setWidthPercentage(100);
				showNWConfigPcConf
						.setWidths(new float[] { 33.0f, 33.0f, 34.0f });

				// table PC configuration Header

				showNWConfigPcConf.addCell(setCellAttribute(" PC ID ",
						fonts[10], Element.ALIGN_MIDDLE, Element.ALIGN_MIDDLE,
						3, 2, 7, 2, Rectangle.BOX, 1));
				showNWConfigPcConf.addCell(setCellAttribute(" System Name ",
						fonts[10], Element.ALIGN_MIDDLE, Element.ALIGN_MIDDLE,
						3, 2, 7, 2, Rectangle.BOX, 1));
				showNWConfigPcConf.addCell(setCellAttribute(" OS ", fonts[10],
						Element.ALIGN_MIDDLE, Element.ALIGN_MIDDLE, 2, 2, 7, 2,
						Rectangle.BOX, 1));

				showNWConfigPcConf.addCell(setCellAttribute(
						rs.getString("lm_spc_id"), fonts[15],
						Element.ALIGN_MIDDLE, Element.ALIGN_BOTTOM, 3, 2, 7, 2,
						Rectangle.BOX, 1));
				showNWConfigPcConf.addCell(setCellAttribute(
						rs.getString("lm_spc_sys_name"), fonts[15],
						Element.ALIGN_MIDDLE, Element.ALIGN_BOTTOM, 3, 2, 7, 2,
						Rectangle.BOX, 1));
				showNWConfigPcConf.addCell(setCellAttribute(
						rs.getString("lm_spc_os"), fonts[15],
						Element.ALIGN_MIDDLE, Element.ALIGN_BOTTOM, 2, 2, 7, 2,
						Rectangle.BOX, 1));

				while (rs.next()) {
					showNWConfigPcConf.addCell(setCellAttribute(
							rs.getString("lm_spc_id"), fonts[15],
							Element.ALIGN_MIDDLE, Element.ALIGN_BOTTOM, 3, 2,
							7, 2, Rectangle.BOX, 1));
					showNWConfigPcConf.addCell(setCellAttribute(
							rs.getString("lm_spc_sys_name"), fonts[15],
							Element.ALIGN_MIDDLE, Element.ALIGN_BOTTOM, 3, 2,
							7, 2, Rectangle.BOX, 1));
					showNWConfigPcConf.addCell(setCellAttribute(
							rs.getString("lm_spc_os"), fonts[15],
							Element.ALIGN_MIDDLE, Element.ALIGN_BOTTOM, 2, 2,
							7, 2, Rectangle.BOX, 1));

				}

				cellNWC1 = new PdfPCell(showNWConfigPcConf);
				cellNWC1.setColspan(2);
				cellNWC1.setBorder(Rectangle.BOX);

				showNWConfig.addCell(setCellAttribute("", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 0, 10,
						Rectangle.NO_BORDER, 1));
				showNWConfig.addCell(cellNWC1);
				showNWConfig.addCell(setCellAttribute("", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 0, 10, 10,
						Rectangle.NO_BORDER, 4));

			}

			document.add(showNWConfig);
		} catch (Exception e) {
			logger.error(
					"showNetworkConfiguration(PdfWriter, Document, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("showNetworkConfiguration(PdfWriter, Document, String, String, DataSource) - Error occurred while adding page details for NetWork Configuration in WO pdf:: "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(cstmt);
			DBUtil.close(con);
		}
	}

	/**
	 * Method: showCertificationSheet - To add Certification Sheet page to WO
	 * PDF.
	 * 
	 * @param writer
	 *            - PdfWriter.
	 * @param document
	 *            - Maindocument.
	 * @param workOrderNo
	 *            - Work Order Number.
	 * @param imagepath1
	 *            - Path for checkbox image.
	 * @param WONwConfigImg
	 *            - Path for image placed as footer on this page.
	 * @param WONwConfigImgheader
	 *            - Path for image placed as header on this page.
	 * @param siteName
	 *            - Site Name.
	 * @param siteNumber
	 *            - Site Number.
	 * @return void - This method does not return anything.
	 */

	public static void showCertificationSheet(PdfWriter writer,
			Document document, String workOrderNo, String imagepath1,
			String WONwConfigImg, String WONwConfigImgheader, String siteName,
			String siteNumber) {
		String certificationSheet[] = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.CERTIFICATION_SHEET);
		document.setMargins(1, 10, 70, 70);
		document.newPage();
		PdfPTable certSheet = new PdfPTable(2);
		certSheet.setWidthPercentage(85);
		PdfPCell csCell1 = null;
		PdfPCell csCell2 = null;
		Float calHeight = 0.0f;
		Phrase p, p1, p2, p3;

		try {
			certSheet.setWidths(new float[] { 3, 82 });
			fonts[18].setColor(new Color(51, 51, 153));
			fonts[1].setColor(new Color(0, 0, 0));
			fonts[17].setColor(new Color(0, 0, 0));
			Image ncImgHeader = Image.getInstance(WONwConfigImgheader);
			ncImgHeader.scalePercent(65, 48);
			certSheet.addCell(setCellImageAttribute(ncImgHeader,
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, -5, 10, 0, 4,
					Rectangle.NO_BORDER, 2));
			certSheet.addCell(setCellAttribute(certificationSheet[0],
					fonts[18], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					0, 8, Rectangle.NO_BORDER, 2));
			document.add(certSheet);
			calHeight = +certSheet.getTotalHeight();

			/*
			 * Calculate page dimensions to add line strokes under section
			 * headings.
			 */
			Rectangle page = document.getPageSize();
			float pageWidth = (page.getWidth() - document.leftMargin() - document
					.rightMargin());
			float pageHeightWithBM = (page.getHeight() - document.topMargin());

			/*
			 * Set line stroke width, color and add it at the specified
			 * position.
			 */
			PdfContentByte cb = writer.getDirectContent();
			cb.setLineWidth(1.8f);
			cb.setGrayStroke(0.5f);
			cb.moveTo((document.leftMargin() + 45), pageHeightWithBM
					- calHeight);
			cb.lineTo((page.getWidth() - document.rightMargin() - 35),
					pageHeightWithBM - calHeight);
			cb.stroke();

			certSheet.deleteBodyRows();

			p = new Phrase();
			p1 = new Phrase(certificationSheet[1], fonts[19]);
			p2 = new Phrase(certificationSheet[34], fonts[1]);
			p3 = new Phrase(certificationSheet[35], fonts[19]);
			p.add(p1);
			p.add(p2);
			p.add(p3);

			csCell1 = new PdfPCell();
			csCell1.addElement(new Phrase(p));
			csCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			csCell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			csCell1.setPaddingLeft(2);
			csCell1.setPaddingRight(2);
			csCell1.setPaddingBottom(6);
			csCell1.setPaddingTop(6);
			csCell1.setBorder(Rectangle.NO_BORDER);
			csCell1.setColspan(2);
			certSheet.addCell(csCell1);

			/* Add Dealer Code and Dealer Name. */
			PdfPTable dealerData = new PdfPTable(3);
			dealerData.setWidthPercentage(100);
			dealerData.setWidths(new float[] { 45, 40, 25 });
			dealerData.addCell(setCellAttribute(certificationSheet[2] + " "
					+ siteName, fonts[15], Element.ALIGN_LEFT,
					Element.ALIGN_BOTTOM, 2, 2, 6, 6, Rectangle.NO_BORDER, 1));
			dealerData.addCell(setCellAttribute(certificationSheet[3] + " "
					+ siteNumber, fonts[15], Element.ALIGN_LEFT,
					Element.ALIGN_BOTTOM, 2, 2, 6, 6, Rectangle.NO_BORDER, 1));
			dealerData.addCell(setCellAttribute(certificationSheet[4],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					6, 6, Rectangle.NO_BORDER, 1));

			csCell1 = new PdfPCell();
			csCell1.addElement(dealerData);
			csCell1.setBorder(Rectangle.NO_BORDER);
			csCell1.setColspan(2);
			certSheet.addCell(csCell1);

			certSheet.addCell(setCellAttribute(certificationSheet[5],
					fonts[12], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					9, 6, Rectangle.NO_BORDER, 2));
			document.add(certSheet);
			calHeight = calHeight + certSheet.getTotalHeight();

			cb.setLineWidth(1.8f);
			cb.setGrayStroke(0.5f);
			cb.moveTo((document.leftMargin() + 45), pageHeightWithBM
					- calHeight);
			cb.lineTo((page.getWidth() - document.rightMargin() - 35),
					pageHeightWithBM - calHeight);
			cb.stroke();

			certSheet.deleteBodyRows();
			certSheet.addCell(setCellAttribute(certificationSheet[6] + " "
					+ certificationSheet[36], fonts[15], Element.ALIGN_LEFT,
					Element.ALIGN_BOTTOM, 2, 2, 9, 3, Rectangle.NO_BORDER, 2));
			certSheet.addCell(setCellAttribute(certificationSheet[8] + " "
					+ certificationSheet[9] + " " + certificationSheet[10]
					+ " " + certificationSheet[11], fonts[15],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2, 2, 3,
					Rectangle.NO_BORDER, 2));
			certSheet.addCell(setCellAttribute(certificationSheet[12],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					2, 4, Rectangle.NO_BORDER, 2));

			Image logo1 = Image.getInstance(imagepath1);
			certSheet.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 1, 5, 3, Rectangle.NO_BORDER, 1));
			certSheet.addCell(setCellAttribute(certificationSheet[13],
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2,
					2, 3, Rectangle.NO_BORDER, 1));

			PdfPTable textTable = new PdfPTable(2);
			textTable.setWidthPercentage(100);
			textTable.setWidths(new float[] { 45, 55 });

			PdfPTable textTableA = new PdfPTable(2);
			textTableA.setWidthPercentage(100);
			textTableA.setWidths(new float[] { 3, 97 });

			textTableA.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 2, 4, 3, 3, Rectangle.NO_BORDER, 1));
			textTableA.addCell(setCellAttribute(certificationSheet[14],
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 9, 2,
					0, 3, Rectangle.NO_BORDER, 1));
			textTableA.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 2, 4, 5, 3, Rectangle.NO_BORDER, 1));
			textTableA.addCell(setCellAttribute(certificationSheet[15],
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 9, 2,
					2, 3, Rectangle.NO_BORDER, 1));
			textTableA.addCell(setCellAttribute("", fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 4, 5, 3,
					Rectangle.NO_BORDER, 1));
			textTableA.addCell(setCellAttribute(certificationSheet[39],
					fonts[17], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 9, 2,
					6, 3, Rectangle.NO_BORDER, 1));

			PdfPTable textTableB = new PdfPTable(1);
			textTableB.setWidthPercentage(100);
			textTableB.addCell(setCellAttribute(certificationSheet[37],
					fonts[10], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2,
					1, 3, Rectangle.NO_BORDER, 1));
			textTableB.addCell(setCellAttribute(certificationSheet[38],
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2,
					2, 5, Rectangle.NO_BORDER, 1));

			csCell1 = new PdfPCell();
			csCell1.addElement(textTableA);
			csCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			csCell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			csCell1.setPaddingLeft(0);
			csCell1.setPaddingRight(3);
			csCell1.setPaddingBottom(0);
			csCell1.setPaddingTop(2);
			csCell1.setBorder(Rectangle.NO_BORDER);
			textTable.addCell(csCell1);

			csCell1 = new PdfPCell();
			csCell1.addElement(textTableB);
			csCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			csCell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			csCell1.setPaddingLeft(5);
			csCell1.setPaddingRight(3);
			csCell1.setPaddingBottom(3);
			csCell1.setPaddingTop(2);
			csCell1.setBorder(Rectangle.BOX);
			csCell1.setBorderWidth(1.2f);
			textTable.addCell(csCell1);

			csCell1 = new PdfPCell(textTable);
			csCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			csCell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			csCell1.setPaddingLeft(1);
			csCell1.setPaddingRight(2);
			csCell1.setPaddingBottom(4);
			csCell1.setPaddingTop(2);
			csCell1.setBorder(Rectangle.NO_BORDER);
			csCell1.setColspan(4);
			certSheet.addCell(csCell1);

			certSheet.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 1, 5, 3, Rectangle.NO_BORDER, 1));
			certSheet.addCell(setCellAttribute(certificationSheet[16],
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2,
					2, 3, Rectangle.NO_BORDER, 1));
			certSheet.addCell(setCellAttribute(certificationSheet[17],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					2, 4, Rectangle.NO_BORDER, 2));
			certSheet.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 1, 5, 0, Rectangle.NO_BORDER, 1));
			certSheet.addCell(setCellAttribute(certificationSheet[18],
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2,
					2, 0, Rectangle.NO_BORDER, 1));

			/* Code to add checkbox image in front of Yes/No labels: Begin. */
			PdfPTable imgYesNo = new PdfPTable(5);
			imgYesNo.setWidthPercentage(100);
			imgYesNo.setWidths(new float[] { 84, 3, 10, 3, 10 });
			imgYesNo.addCell(setCellAttribute(certificationSheet[19], fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 33, 2, 0, 3,
					Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 0, 5, 3, Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellAttribute("Yes", fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 0, 3,
					Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 0, 5, 3, Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellAttribute("No", fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 0, 3,
					Rectangle.NO_BORDER, 1));

			csCell1 = new PdfPCell();
			csCell1.addElement(imgYesNo);
			csCell1.setBorder(Rectangle.NO_BORDER);
			csCell1.setColspan(2);
			certSheet.addCell(csCell1);
			/* Code to add checkbox image in front of Yes/No labels: End. */

			certSheet.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 1, 5, 3, Rectangle.NO_BORDER, 1));
			certSheet.addCell(setCellAttribute(certificationSheet[20],
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2,
					2, 3, Rectangle.NO_BORDER, 1));
			certSheet.addCell(setCellAttribute("", fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2, 2, 3,
					Rectangle.NO_BORDER, 1));

			csCell1 = new PdfPCell();
			PdfPTable subTable = new PdfPTable(2);
			subTable.setWidthPercentage(100);
			subTable.setWidths(new float[] { 3, 97 });

			subTable.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 2, 1, 5, 3, Rectangle.NO_BORDER, 1));
			subTable.addCell(setCellAttribute(certificationSheet[21], fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2, 2, 3,
					Rectangle.NO_BORDER, 1));
			subTable.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 2, 1, 5, 3, Rectangle.NO_BORDER, 1));
			subTable.addCell(setCellAttribute(certificationSheet[22], fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2, 2, 3,
					Rectangle.NO_BORDER, 1));

			csCell1.addElement(subTable);
			csCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			csCell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			csCell1.setPaddingLeft(1);
			csCell1.setPaddingRight(2);
			csCell1.setPaddingBottom(3);
			csCell1.setPaddingTop(2);
			csCell1.setBorder(Rectangle.NO_BORDER);
			csCell1.setColspan(1);
			certSheet.addCell(csCell1);

			certSheet.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 1, 5, 3, Rectangle.NO_BORDER, 1));
			certSheet.addCell(setCellAttribute(certificationSheet[23],
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2,
					2, 3, Rectangle.NO_BORDER, 1));
			certSheet.addCell(setCellAttribute(certificationSheet[24],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					2, 3, Rectangle.NO_BORDER, 2));

			/* Code to add checkbox image in front of Yes/No labels: Begin. */
			imgYesNo = new PdfPTable(5);
			imgYesNo.setWidthPercentage(100);
			imgYesNo.setWidths(new float[] { 84, 3, 10, 3, 10 });
			imgYesNo.addCell(setCellAttribute(certificationSheet[25], fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 2, 3,
					Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 0, 5, 3, Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellAttribute("Yes", fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 0, 3,
					Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 0, 5, 3, Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellAttribute("No", fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 0, 3,
					Rectangle.NO_BORDER, 1));

			imgYesNo.addCell(setCellAttribute(certificationSheet[26], fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 2, 3,
					Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 0, 5, 3, Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellAttribute("Yes", fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 0, 3,
					Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellImageAttribute(logo1, Element.ALIGN_LEFT,
					Element.ALIGN_TOP, 3, 0, 5, 3, Rectangle.NO_BORDER, 1));
			imgYesNo.addCell(setCellAttribute("No", fonts[7],
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 0, 3,
					Rectangle.NO_BORDER, 1));

			csCell1 = new PdfPCell();
			csCell1.addElement(imgYesNo);
			csCell1.setBorder(Rectangle.NO_BORDER);
			csCell1.setColspan(2);
			certSheet.addCell(csCell1);
			/* Code to add checkbox image in front of Yes/No labels: End. */

			certSheet.addCell(setCellAttribute(certificationSheet[27],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					2, 3, Rectangle.NO_BORDER, 2));
			certSheet.addCell(setCellAttribute(certificationSheet[28],
					fonts[12], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					12, 6, Rectangle.NO_BORDER, 2));
			document.add(certSheet);
			calHeight = calHeight + certSheet.getTotalHeight();

			cb.setLineWidth(1.8f);
			cb.setGrayStroke(0.5f);
			cb.moveTo((document.leftMargin() + 45), pageHeightWithBM
					- calHeight);
			cb.lineTo((page.getWidth() - document.rightMargin() - 35),
					pageHeightWithBM - calHeight);
			cb.stroke();

			certSheet.deleteBodyRows();
			certSheet.addCell(setCellAttribute(certificationSheet[29],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					2, 4, Rectangle.NO_BORDER, 2));
			certSheet.addCell(setCellAttribute(certificationSheet[30],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					3, 6, Rectangle.NO_BORDER, 2));
			certSheet.addCell(setCellAttribute(certificationSheet[31],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					15, 4, Rectangle.NO_BORDER, 2));
			certSheet.addCell(setCellAttribute(certificationSheet[32],
					fonts[12], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					12, 6, Rectangle.NO_BORDER, 2));
			document.add(certSheet);
			calHeight = calHeight + certSheet.getTotalHeight();

			cb.setLineWidth(1.8f);
			cb.setGrayStroke(0.5f);
			cb.moveTo((document.leftMargin() + 45), pageHeightWithBM
					- calHeight);
			cb.lineTo((page.getWidth() - document.rightMargin() - 35),
					pageHeightWithBM - calHeight);
			cb.stroke();

			certSheet.deleteBodyRows();
			/* Code used to insert different fonts in the same PdfPCell. */
			csCell1 = new PdfPCell();
			p = new Phrase();
			p1 = new Phrase(certificationSheet[33], fonts[0]);
			p2 = new Phrase(
					" _________________________________________________________________________",
					fonts[15]);
			p.add(p1);
			p.add(p2);

			csCell1.addElement(new Phrase(p));
			csCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			csCell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
			csCell1.setPaddingLeft(2);
			csCell1.setPaddingRight(2);
			csCell1.setPaddingBottom(12);
			csCell1.setPaddingTop(6);
			csCell1.setBorder(Rectangle.NO_BORDER);
			csCell1.setColspan(2);
			certSheet.addCell(csCell1);
			certSheet.addCell(setCellAttribute(certificationSheet[31],
					fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					10, 6, Rectangle.NO_BORDER, 2));
			document.add(certSheet);
			calHeight = calHeight + certSheet.getTotalHeight();

			/* Add image and contents as footer: Begin. */
			PdfPTable pageFooter = new PdfPTable(3);
			pageFooter.setWidthPercentage(85);
			pageFooter.setWidths(new float[] { 22, 34, 29 });

			Image ncImg = Image.getInstance(WONwConfigImg);
			ncImg.scalePercent(65, 48);
			pageFooter.addCell(setCellImageAttribute(ncImg, Element.ALIGN_LEFT,
					Element.ALIGN_BOTTOM, 2, 2, 12, 7, Rectangle.NO_BORDER, 3));
			pageFooter.addCell(setCellAttribute("WT016 IRD 10-25-2008",
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 12, 2,
					5, 8, Rectangle.NO_BORDER, 1));
			pageFooter.addCell(setCellAttribute("Work Order # " + workOrderNo,
					fonts[7], Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 2,
					5, 8, Rectangle.NO_BORDER, 1));
			pageFooter.addCell(setCellAttribute("Page 1 of 2", fonts[7],
					Element.ALIGN_RIGHT, Element.ALIGN_BOTTOM, 2, 40, 5, 8,
					Rectangle.NO_BORDER, 1));

			pageFooter.setTotalWidth(page.getWidth() - document.leftMargin()
					- 45 - document.rightMargin() - 15);
			pageFooter.writeSelectedRows(0, -1, document.leftMargin() + 35,
					document.bottomMargin() + 25, writer.getDirectContent());
			/* Add image and contents as footer: End. */

		} catch (Exception e) {
			logger.error(
					"showCertificationSheet(PdfWriter, Document, String, String, String, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("showCertificationSheet(PdfWriter, Document, String, String, String, String, String, String) - Error occurred while adding page details for Certification Sheet in WO pdf:: "
						+ e);
			}
		} finally {

		}
	}

	/**
	 * Method: showInstallationChecklist - To add Installation Checklist Sheet
	 * page to WO PDF.
	 * 
	 * @param writer
	 *            - PdfWriter.
	 * @param document
	 *            - Maindocument.
	 * @param siteName
	 *            - Site Name.
	 * @param siteNumber
	 *            - Site Number.
	 * @param icSnapOn
	 *            - Path for Snap-on image.
	 * @param icChrysler
	 *            - Path for Chrysler image.
	 * @param icCompuCom
	 *            - Path for CompuCom image.
	 * @return void - This method does not return anything.
	 */

	public static void showInstallationChecklist(PdfWriter writer,
			Document document, String siteName, String WONwConfigImgheader,
			String siteNumber, String WONwConfigImg, String imagepath1) {
		String installationChecklist[] = StoreWOArrayContents
				.getArrayContents(StoreWOArrayContents.INSTALLATION_CHECKLIST);
		document.setMargins(20, 20, 100, 70);
		document.newPage();
		PdfPTable addChecklist = new PdfPTable(8);
		addChecklist.setWidthPercentage(85);
		PdfPCell icCell1 = null;

		try {
			document.setMargins(1, 10, 70, 70);
			document.newPage();
			PdfPTable certSheet = new PdfPTable(2);
			certSheet.setWidthPercentage(85);
			PdfPCell csCell1 = null;
			PdfPCell csCell2 = null;
			Float calHeight = 0.0f;

			certSheet.setWidths(new float[] { 3, 82 });
			fonts[21].setColor(new Color(51, 51, 153));
			fonts[15].setColor(new Color(0, 0, 0));
			fonts[17].setColor(new Color(0, 0, 0));

			Image ncImgHeader = Image.getInstance(WONwConfigImgheader);
			ncImgHeader.scalePercent(65, 48);
			certSheet.addCell(setCellImageAttribute(ncImgHeader,
					Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, -5, 10, 0, 4,
					Rectangle.NO_BORDER, 2));

			certSheet.addCell(setCellAttribute(installationChecklist[0],
					fonts[21], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 2, 2,
					0, 8, Rectangle.NO_BORDER, 2));
			document.add(certSheet);
			calHeight = +certSheet.getTotalHeight();

			/*
			 * Calculate page dimensions to add line strokes under section
			 * headings.
			 */
			Rectangle page = document.getPageSize();
			float pageWidth = (page.getWidth() - document.leftMargin() - document
					.rightMargin());
			float pageHeightWithBM = (page.getHeight() - document.topMargin());

			/*
			 * Set line stroke width, color and add it at the specified
			 * position.
			 */
			PdfContentByte cb = writer.getDirectContent();
			cb.setLineWidth(1.8f);
			cb.setGrayStroke(0.5f);
			cb.moveTo((document.leftMargin() + 45), pageHeightWithBM
					- calHeight);
			cb.lineTo((page.getWidth() - document.rightMargin() - 35),
					pageHeightWithBM - calHeight);
			cb.stroke();

			certSheet.deleteBodyRows();
			calHeight = calHeight + certSheet.getTotalHeight();
			int i = 1;
			int x = 10;
			for (i = 1; i <= 6; i++) {
				if (i == 1) {
					x = 2;
				} else {
					x = 15;
				}
				Image logo1 = Image.getInstance(imagepath1);
				certSheet.addCell(setCellAttribute(installationChecklist[i],
						fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1,
						2, x, 3, Rectangle.NO_BORDER, 2));
				certSheet.addCell(setCellImageAttribute(logo1,
						Element.ALIGN_LEFT, Element.ALIGN_TOP, 3, 0, 5, 3,
						Rectangle.NO_BORDER, 1));
				certSheet.addCell(setCellAttribute("Yes", fonts[15],
						Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0, 2, 0, 3,
						Rectangle.NO_BORDER, 1));
				certSheet.addCell(setCellImageAttribute(logo1,
						Element.ALIGN_LEFT, Element.ALIGN_TOP, 3, 0, 5, 3,
						Rectangle.NO_BORDER, 1));
				certSheet.addCell(setCellAttribute("No (Please explain)",
						fonts[15], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 0,
						2, 0, 18, Rectangle.NO_BORDER, 1));
				document.add(certSheet);
				calHeight = calHeight + certSheet.getTotalHeight();
				if (logger.isDebugEnabled()) {
					logger.debug("showInstallationChecklist(PdfWriter, Document, String, String, String, String, String) - Page height"
							+ calHeight);
				}

				/*
				 * Set line stroke width, color and add it at the specified
				 * position.
				 */
				cb.setLineWidth(1.0f);
				cb.setGrayStroke(0.5f);
				cb.moveTo((document.leftMargin() + 45), pageHeightWithBM
						- calHeight);
				cb.lineTo((page.getWidth() - document.rightMargin() - 35),
						pageHeightWithBM - calHeight);
				cb.stroke();
				certSheet.deleteBodyRows();
			}

			certSheet.addCell(setCellAttribute("Service Manager's Signature",
					fonts[21], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 2,
					15, 25, Rectangle.NO_BORDER, 2));
			certSheet
					.addCell(setCellAttribute(
							"____________________________________________________________________________",
							fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM,
							1, 2, 2, 3, Rectangle.NO_BORDER, 2));
			document.add(certSheet);
			/* Add image and contents as footer: Begin. */
			PdfPTable pageFooter = new PdfPTable(3);
			pageFooter.setWidthPercentage(85);
			pageFooter.setWidths(new float[] { 22, 34, 29 });

			Image ncImg = Image.getInstance(WONwConfigImg);
			ncImg.scalePercent(65, 48);
			pageFooter.addCell(setCellImageAttribute(ncImg, Element.ALIGN_LEFT,
					Element.ALIGN_BOTTOM, 2, 2, 12, 7, Rectangle.NO_BORDER, 3));
			pageFooter.addCell(setCellAttribute("WT016 IRD 10-25-2008",
					fonts[7], Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 12, 2,
					5, 8, Rectangle.NO_BORDER, 2));
			// pageFooter.addCell(setCellAttribute("Work Order # "+workOrderNo,
			// fonts[7], Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 2, 5, 8,
			// Rectangle.NO_BORDER, 1));
			pageFooter.addCell(setCellAttribute("Page 2 of 2", fonts[7],
					Element.ALIGN_RIGHT, Element.ALIGN_BOTTOM, 2, 40, 5, 8,
					Rectangle.NO_BORDER, 1));

			pageFooter.setTotalWidth(page.getWidth() - document.leftMargin()
					- 45 - document.rightMargin() - 15);
			pageFooter.writeSelectedRows(0, -1, document.leftMargin() + 35,
					document.bottomMargin() + 25, writer.getDirectContent());
			/* Add image and contents as footer: End. */

		} catch (Exception e) {
			logger.error(
					"showInstallationChecklist(PdfWriter, Document, String, String, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("showInstallationChecklist(PdfWriter, Document, String, String, String, String, String) - Error occurred while adding page details for Installation Checklist Sheet in WO pdf:: "
						+ e);
			}
		} finally {

		}
	}

	/**
	 * Method: setCellAttribute - create a PdfPCell and set its attribute
	 * values.
	 * 
	 * @param label
	 *            - String contents that are to be displayed in the cell.
	 * @param font
	 *            - Font.
	 * @param alignAtt1
	 *            - Alignment 1 (can be left, right, top or bottom).
	 * @param allignAtt2
	 *            - Alignment 1 (can be left, right, top or bottom).
	 * @param paddingLeft
	 *            - Left Padding.
	 * @param paddingRight
	 *            - Right Padding.
	 * @param paddingTop
	 *            - Top Padding.
	 * @param paddingBottom
	 *            - Bottom Padding.
	 * @param border
	 *            - Cell border.
	 * @param colspan
	 *            - Cell colspan.
	 * @return PdfPCell - This method returns PdfPCell.
	 */

	public static PdfPCell setCellAttribute(String label, Font font,
			int alignAtt1, int allignAtt2, float paddingLeft,
			float paddingRight, float paddingTop, float paddingBottom,
			int border, int colspan) {

		PdfPCell cell1 = new PdfPCell(new Phrase(label, font));
		cell1.setHorizontalAlignment(alignAtt1);
		cell1.setVerticalAlignment(allignAtt2);
		cell1.setPaddingLeft(paddingLeft);
		cell1.setPaddingRight(paddingRight);
		cell1.setPaddingBottom(paddingBottom);
		cell1.setPaddingTop(paddingTop);
		cell1.setBorder(border);
		cell1.setColspan(colspan);
		return cell1;
	}

	/**
	 * Method: setCellImageAttribute - create a PdfPCell and set its attribute
	 * values.
	 * 
	 * @param image
	 *            - Image that is to be displayed in the cell.
	 * @param alignAtt1
	 *            - Alignment 1 (can be left, right, top or bottom).
	 * @param allignAtt2
	 *            - Alignment 1 (can be left, right, top or bottom).
	 * @param paddingLeft
	 *            - Left Padding.
	 * @param paddingRight
	 *            - Right Padding.
	 * @param paddingTop
	 *            - Top Padding.
	 * @param paddingBottom
	 *            - Bottom Padding.
	 * @param border
	 *            - Cell border.
	 * @param colspan
	 *            - Cell colspan.
	 * @return PdfPCell - This method returns PdfPCell.
	 */
	public static PdfPCell setCellImageAttribute(Image image, int alignAtt1,
			int allignAtt2, float paddingLeft, float paddingRight,
			float paddingTop, float paddingBottom, int border, int colspan) {

		PdfPCell cell1 = new PdfPCell(image);
		cell1.setHorizontalAlignment(alignAtt1);
		cell1.setVerticalAlignment(allignAtt2);
		cell1.setPaddingLeft(paddingLeft);
		cell1.setPaddingRight(paddingRight);
		cell1.setPaddingBottom(paddingBottom);
		cell1.setPaddingTop(paddingTop);
		cell1.setBorder(border);
		cell1.setColspan(colspan);
		return cell1;
	}

	public List<String> getDeliverableTitles(String powoid, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<String> deliverableTitles = null;
		try {
			conn = ds.getConnection();

			String sql = "Select podb_mn_dl_title from podb_purchase_order_deliverables where lm_po_id ="
					+ powoid;
			stmt = conn.createStatement();

			rs = stmt.executeQuery(sql);
			deliverableTitles = new ArrayList<String>();
			while (rs.next()) {
				String deliverableTitle = rs.getString("podb_mn_dl_title");
				deliverableTitles.add(deliverableTitle);
			}

		} catch (SQLException e) {
			logger.error("getDeliverableTitles(String powoId,DataSource ds)"
					+ e);
		} finally {

			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return deliverableTitles;

	}

}
