/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class used for generating Purchase Order in pdf format.      
 *
 */

package com.mind.xml;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.mind.common.Decimalroundup;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;

/**
 * Methods : viewpo
 * 
 * Revisions:
 * 
 * 09/10/2011 Update - see description below. Also include non-functional impact
 * to /PMT/EditMAster.jsp to include notes to user for PDF.
 * 
 */

public class POPdf {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(POPdf.class);

	/**
	 * This method return Byte Array for Purchase Order in pdf.
	 * 
	 * @param po_no
	 *            String. Purchase Order Number
	 * @param id
	 *            String. Job Id
	 * @param actid
	 *            String. Activity Id
	 * @param type
	 *            String. For whom PO is generated Job/ Activity
	 * @param fromtype
	 *            String. from where PO is generated Job/ Activity
	 * @param imagePath
	 *            String. path of logo
	 * @param ds
	 *            Object of DataSource
	 * @return Byte Array This method returns byte array, for purchase order in
	 *         pdf.
	 */

	public byte[] viewPO(String powoId, String fileName, String imagePath,
			String fontPath, String userId, DataSource ds,
			String imageBulletPath) {

		Document maindocument = new Document(PageSize.A4, 5, 5, 40, 40);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();
		/* Change for PO type..... */

		String checkpotype = "";
		Connection conn = null;
		try {
			conn = ds.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		checkpotype = checkType(powoId, conn);
		// String
		// imagepath1="C:"+"\\Documents and Settings\\ompandey\\workspace\\Ilex1.36\\.deployables\\Ilex1.36\\images"+"/Bullet.gif";
		// end

		if (logger.isDebugEnabled()) {
			logger.debug("viewPO(String, String, String, String, String, Connection, String) - Image Path is : "
					+ imagePath);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("viewPO(String, String, String, String, String, Connection, String) - Image Path is : "
					+ imagePath);
		}

		ResultSet rs = null;
		ResultSet rs1 = null;
		CallableStatement cstmt = null;
		Statement stmt = null;

		try {
			PdfWriter writer = PdfWriter.getInstance(maindocument, outbuff);

			/* Set font array */
			BaseFont bfArialN = BaseFont.createFont(fontPath,
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

			Font[] fonts = new Font[21];
			fonts[0] = new Font(bfArialN, 8, Font.BOLD);
			fonts[1] = new Font(bfArialN, 8, Font.NORMAL);
			fonts[2] = new Font(bfArialN, 7, Font.NORMAL);
			fonts[3] = new Font(bfArialN, 7, Font.BOLD);
			fonts[4] = new Font(bfArialN, 7, Font.NORMAL | Font.UNDERLINE);
			fonts[5] = new Font(bfArialN, 14, Font.BOLD);
			fonts[6] = new Font(bfArialN, 8.5f, Font.NORMAL);
			fonts[7] = new Font(bfArialN, 10, Font.BOLD);
			fonts[8] = new Font(bfArialN, 5.5f, Font.NORMAL);
			fonts[9] = new Font(bfArialN, 8, Font.BOLD);
			fonts[10] = new Font(bfArialN, 8, Font.NORMAL);
			fonts[11] = new Font(bfArialN, 36, Font.HELVETICA | Font.BOLD);
			fonts[12] = new Font(bfArialN, 12, Font.BOLD);
			fonts[13] = new Font(bfArialN, 8, Font.ITALIC);
			fonts[14] = new Font(bfArialN, 12, Font.BOLD | Font.UNDERLINE);
			fonts[15] = new Font(bfArialN, 8, Font.ITALIC | Font.UNDERLINE);
			fonts[9].setColor(255, 255, 255); // set font color white
			fonts[10].setColor(255, 255, 255); // set font color white
			fonts[11].setColor(255, 0, 0);
			fonts[17] = new Font(bfArialN, 7, Font.BOLD);
			fonts[18] = new Font(bfArialN, 5.0f, Font.NORMAL);
			fonts[19] = new Font(bfArialN, 6.0F, 1); // Added for 09/10/2011
														// update
			fonts[20] = new Font(bfArialN, 6.0F, 0);

			PdfPTable maintable = new PdfPTable(2);
			maintable.setWidthPercentage(95);
			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(95);
			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			Paragraph para = new Paragraph();
			int x = 0;
			maindocument.open();

			PdfDestination pd = new PdfDestination(PdfDestination.XYZ, -1, -1,
					1);
			PdfAction action = PdfAction.gotoLocalPage(1, pd, writer);
			writer.setOpenAction(action);

			PdfContentByte cb = writer.getDirectContent();
			String[] GeneralData = {
					"PURCHASE ORDER #", // 0
					"REQUIRED DATE", // 1
					"FOR DETAILED SOW REFERENCE WO#", // 2
					"SHIP TO:", // 3
					"BILL TO:", // 4
					"JOB SITE INFORMATION", // 5
					"CONTRACTOR/VENDOR/SUPPLIER INFORMATION",// 6
					"Please refer to Reference WO# for detailed Scope of Work",// 7
					"SPECIAL CONDITIONS", // 8
					"DESCRIPTION OF ITEMS ORDERED", // 9
					"All items listed below are subject to the terms and conditions of purchase found on page 2 of this Purchase Order", // 10
					"Description", // 11
					"FORWARD ALL QUESTIONS AND \nESCALATIONS TO CONTINGENT \n(800) 506-9609 and press 6 or via \nemail: dispatch@contingent.net", // 12
					"PURCHASE \n ORDER", // 13
					"PURCHASE ORDER ISSUE DATE", // 14
					"Sub Total", // 15
					"Shipping Price", // 16
					"Sales Tax", // 17
					"Price Adjustment", // 18
					"Total", // 19
					"When \"Labor Hours\" are specified under ", // 20
					"Time On-Site is derived from the time you are checked on site and off site by a Contingent representative. A site contact signature confirms your presence on site only.", // 21
					"Sales Terms:", // 22
					"TERMS AND CONDITIONS OF PURCHASE - PLEASE READ CAREFULLY", // 23
					"'Unit Cost' denotes hourly rate", // 24
					"Created By", // 25
					"By accepting this PO you are agreeing to the terms found on PAGES 1 & 2", // 26
					"Item Name", // 27
					", the ", // 28
					"Unit Cost", // 29
					" is the authorized hourly rate and additional quantities above those shown may be authorized by Contingent representative.", // 30
					"\"", // 31
					"Any deviations to the prescribed scope of work here or on the accompanying Work Order or other documentation that affects price MUST be approved in writing by Contingent personnel.", // 32
					"All costs are expressed in USD.", // 33
					"Project Manager", // 34
					"Facsimile", // 35
					"Company Name", // 36
					"Address", // 37
					"City, State, Zip", // 38
					"Point of Contact"// 39
			};

			String[] HeaderData = { "Line Item", "Item Name",
					"Item Description", "Part#", "Qty", "Unit Cost", "Ext Cost" };
			String[] AddressData = { "Company Name", "Address",
					"City, State, Zip", "Telephone, Facsimile",
					"Point of Contact" };
			String[] AddressData1 = { "Contingent Network Services, LLC",
					"4400 Port Union Road", "West Chester, OH 45011",
					"(800) 506-9609 (513) 860-2105", "CJ Herron" };
			String[] SiteAddressData = { "Client Name", "Site Name",
					"Site Number", "Address", "City, State, Zip" };
			int L, R, C;
			L = Element.ALIGN_LEFT;
			R = Element.ALIGN_RIGHT;
			C = Element.ALIGN_CENTER;
			Color gray = new Color(0xC0, 0xC0, 0xC0);
			Color red = new Color(255, 0, 0);

			String strDate = "";
			Calendar c = Calendar.getInstance();
			strDate = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE)
					+ "/" + c.get(Calendar.YEAR);
			String sql = "";
			String lm_po_data = "";
			/* fetching data from database start */
			String issue_date = null;
			String lm_po_number = null;
			String lm_po_special_conditions = null;
			String lm_po_issue_date = null;
			String lm_po_deliver_by_date = null;
			String lm_po_deliver_by_time = null;
			String lm_po_ship_to_address = null;
			String cns_pc_name = null;
			String appendix_name = "";
			String[] pvs_address_data = new String[AddressData.length];
			String[] site_address_data = new String[SiteAddressData.length];
			String login_name = null;
			String pwo_terms = null;
			String project_manager = null;
			String facsimile = null;
			String company_name = null;
			String company_address = null;
			String company_city = null;
			String company_state = null;
			String company_zipcode = null;
			String appendixId = null;
			String sow = null;
			int commission_flag = 0;
			String currentid = "";
			boolean checkForAppendix = false;

			cstmt = conn.prepareCall("{?=call lm_po_wo_data(?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, powoId);
			cstmt.setInt(3, Integer.parseInt(userId));

			rs = cstmt.executeQuery();
			if (rs.next()) {

				lm_po_issue_date = rs.getString("lm_po_issue_date");
				lm_po_number = rs.getString("lm_po_number");
				lm_po_deliver_by_date = rs.getString("lm_po_deliver_by_date");
				pvs_address_data[0] = rs.getString("pvs_compname"); // pvs_name
				pvs_address_data[1] = rs.getString("pvs_address"); // pvs_address
				pvs_address_data[2] = rs.getString("pvs_city_state_zip"); // pvs_city_state_zip
				pvs_address_data[3] = rs.getString("partner_poc_phone"); // pvs_city_state_zip
				pvs_address_data[4] = rs.getString("partner_poc_name"); // pvs_city_state_zip

				site_address_data[1] = rs.getString("lm_si_name"); // site_name
				site_address_data[2] = rs.getString("lm_si_number"); // site_number
				site_address_data[3] = rs.getString("lm_si_address"); // site_address
				site_address_data[4] = rs.getString("site_city_state_zip"); // site_city_state_zip

				lm_po_ship_to_address = rs.getString("lm_po_ship_to_address");
				lm_po_special_conditions = rs
						.getString("lm_po_special_conditions"); // sp. cond.
				appendix_name = rs.getString("lx_pr_title");
				project_manager = rs.getString("cns_pc_name") + " "
						+ rs.getString("cns_pc_phone");

				lm_po_deliver_by_time = rs.getString("lm_wo_deliver_by_time");
				login_name = rs.getString("created_by");
				cns_pc_name = rs.getString("job_owner") + " "
						+ rs.getString("job_owner_phone");

				site_address_data[0] = rs.getString("lo_ot_name"); // client_name
				pwo_terms = rs.getString("lm_pwo_terms_master_data");

				company_name = rs.getString("company_name");
				company_address = rs.getString("company_address");
				company_city = rs.getString("company_city") + ',' + ' '
						+ rs.getString("company_state") + ' '
						+ rs.getString("company_zip");
				facsimile = rs.getString("company_fax");
				if (facsimile == null || facsimile.equals(""))
					facsimile = "513-860-2105";

				appendixId = rs.getString("lx_pr_id");
				commission_flag = rs.getInt("lm_po_commission_flag");
				sow = rs.getString("lm_po_sow");
			}

			stmt = conn.createStatement();
			String sql1 = "select fs_pr_id from fieldsheet";
			rs1 = stmt.executeQuery(sql1);

			while (rs1.next()) {
				currentid = rs1.getString("fs_pr_id");
				if (appendixId.equals(currentid)) {
					checkForAppendix = true;
					break;
				}
			}
			if (checkForAppendix) {
				facsimile = "888-556-0932";
			}

			/* fetching data from database end */

			// For PO Page
			if (checkpotype == null) {
				checkpotype = "";
			}
			if (checkpotype.equals("P") || checkpotype.equals("M")
					|| checkpotype.equals("S")) {

				/* Set font array */

				PdfPCell cell1po = new PdfPCell();
				PdfPCell cell2po = new PdfPCell();
				PdfPCell cell3po = new PdfPCell();
				PdfPCell cell4po = new PdfPCell();
				Paragraph paraContent = new Paragraph();
				String temp_data = null;
				float setPaddingL = 50;
				float setPaddingT = 0;
				PdfPTable headerTable = new PdfPTable(1);
				headerTable.setWidthPercentage(95);
				PdfPTable mainTable1 = new PdfPTable(1);
				PdfPTable mainTablerow = new PdfPTable(1);
				mainTablerow.setWidthPercentage(100);
				mainTable1.setWidthPercentage(100);
				PdfPCell cellContent, cellNumber, cellImage, cellrow;
				PdfPTable poMainTable = new PdfPTable(1);

				int i = 0;
				int k = 0;
				int l = 0;
				String titleText = "";
				String numberText = "";
				String contentText = "";
				String numberType = "";
				String titleType = "";
				String contentType = "";
				String title1 = "";

				/* Read xml tag start */
				Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
				String xmlFile = EditXmldao.getXmlFile(fileName, ds);

				final StringReader xmlReader = new StringReader(xmlFile);
				final StreamSource xmlSource = new StreamSource(xmlReader);
				com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
						.unmarshal(xmlSource);

				Section section;
				java.util.List list1 = doc.getSections().getSection();
				Iterator it = list1.iterator();
				Title title = new Title();
				Number number = new Number();
				Content content = new Content();
				/* Read xml tag end */

				String col = "";
				int embdlength = 0;
				int embdlengthcontent = 0;

				while (it.hasNext()) {
					titleText = "";
					numberText = "";
					contentText = "";
					section = (Section) it.next();

					numberType = section.getNumber().getType();
					titleType = section.getTitle().getType();
					contentType = section.getContent().getType();

					title = section.getTitle();
					titleText = title.getContent();
					number = section.getNumber();
					numberText = number.getContent();
					content = section.getContent();
					contentText = content.getContent();
					embdlength = Domxmltaglist.totalOccurance(titleText);
					embdlengthcontent = Domxmltaglist
							.totalOccurance(contentText);

					String[] titleTexthyper = new String[embdlength];
					String[] contentTexthyper = new String[embdlength];
					PdfPTable mainTable12 = new PdfPTable(1);
					/* Add embedded parameter end */
					/* Header rows start */

					if (i == 0) {
						Paragraph para1 = new Paragraph(
								fonts[0].getCalculatedLeading(4), numberText,
								fonts[1]);
						para1.setSpacingAfter(15f);
						para1.setSpacingBefore(15f);
						cell2po = new PdfPCell(para1);
						cell2po.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell2po.setBorder(Rectangle.NO_BORDER);
						cell2po.setPaddingTop(.5f);

						para1 = new Paragraph(
								fonts[11].getCalculatedLeading(4), titleText,
								fonts[11]);
						para1.setSpacingAfter(10f);
						para1.setSpacingBefore(10f);
						cell3po = new PdfPCell(para1);
						cell3po.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell3po.setBorder(Rectangle.NO_BORDER);
						cell3po.setPaddingTop(.5f);

						para1 = new Paragraph();
						cell4po = new PdfPCell(para1);
						cell4po.setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell4po.setBorder(Rectangle.NO_BORDER);

						headerTable.addCell(cell2po);
						headerTable.addCell(cell3po);
						// headerTable.addCell(cell4po);
						maindocument.add(headerTable);

						section = (Section) it.next();
						title = section.getTitle();
						titleText = title.getContent();
						number = section.getNumber();
						numberText = number.getContent();
						// contentText = xt.getContent();
						content = section.getContent();
						contentText = content.getContent();
					}
					/* Header rows end */
					i++;

					/* Content start */
					// Blank paragraph
					PdfPTable blankTable = new PdfPTable(1);
					blankTable.setWidthPercentage(100);
					Paragraph paraBlank0 = new Paragraph();
					PdfPCell cellBlank0 = new PdfPCell(paraBlank0);
					cellBlank0.setHorizontalAlignment(Element.ALIGN_CENTER);
					cellBlank0.setBorder(Rectangle.NO_BORDER);
					blankTable.addCell(cellBlank0);
					maindocument.add(blankTable);
					// end blank
					if (i != 0) {
						PdfPTable poTable = new PdfPTable(2);
						float[] arr = { 3, 97 };
						poTable.setWidths(arr);
						poTable.setWidthPercentage(100);

						titleTexthyper = Domxmltaglist
								.getValueofEmbaded(titleText);
						contentTexthyper = Domxmltaglist
								.getValueofEmbaded(contentText);

						// Image logo = Image.getInstance(imagepath1);
						Image logo = Image.getInstance(imageBulletPath);
						// logo.
						PdfPCell cellLogo = new PdfPCell(logo);
						cellLogo.setPaddingTop(5.0f);
						cellLogo.setPaddingLeft(10.0f);
						cellLogo.setHorizontalAlignment(Element.ALIGN_LEFT);
						cellLogo.setBorder(Rectangle.NO_BORDER);
						poTable.addCell(cellLogo);

						if (numberText.equals(""))
							title1 = titleText;
						else
							title1 = numberText + titleText;
						if (embdlength > 0) {
							PdfPCell cellhyperTitle = new PdfPCell();
							Phrase p = new Phrase();
							for (l = 0; l < embdlength; l++) {
								if (titleTexthyper[l].contains("www")) {
									Chunk ck = new Chunk(titleTexthyper[l],
											fonts[14]);
									// ck.setAnchor("www.google.net");
									p.add(l, ck);

								} else {
									Chunk ck = new Chunk(titleTexthyper[l],
											fonts[12]);
									p.add(l, ck);
								}
							}

							PdfPCell cellTitle = new PdfPCell(p);
							cellTitle.setPaddingTop(5.0f);
							cellTitle.setPaddingLeft(10.0f);
							cellTitle
									.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cellTitle.setBorder(Rectangle.NO_BORDER);
							mainTable12.addCell(cellTitle);
						} else {
							Paragraph paraTitle = new Paragraph(title1,
									fonts[12]);
							PdfPCell cellTitle = new PdfPCell(paraTitle);
							cellTitle.setPaddingTop(5.0f);
							cellTitle.setPaddingLeft(10.0f);
							cellTitle
									.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cellTitle.setBorder(Rectangle.NO_BORDER);
							mainTable12.addCell(cellTitle);

						}
						if (embdlengthcontent > 0) {
							PdfPCell cellhyperTitle = new PdfPCell();
							Phrase p = new Phrase("");
							for (l = 0; l < embdlengthcontent; l++) {
								if (contentTexthyper[l].contains("www")) {
									Chunk ck = new Chunk(contentTexthyper[l],
											fonts[15]);
									// ck.setAnchor(contentTexthyper[l]);
									p.add(l, ck);

								} else {
									Chunk ck = new Chunk(contentTexthyper[l],
											fonts[13]);
									p.add(l, ck);
								}
							}

							// Paragraph paraTitle = new Paragraph(title,
							// fonts[12]);
							PdfPCell cellTitle = new PdfPCell(p);
							cellTitle.setPaddingTop(5.0f);
							cellTitle.setPaddingLeft(10.0f);
							cellTitle
									.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cellTitle.setBorder(Rectangle.NO_BORDER);
							mainTable12.addCell(cellTitle);

						} else {
							paraContent = new Paragraph(contentText, fonts[13]);
							PdfPCell cellText = new PdfPCell(paraContent);
							cellText.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							// cellText.setPaddingTop(5.0f);
							cellText.setPaddingLeft(10.0f);
							cellText.setBorder(Rectangle.NO_BORDER);
							mainTable12.addCell(cellText);
						}
						Paragraph paraBlank = new Paragraph();
						PdfPCell cellBlank = new PdfPCell(paraBlank);
						cellBlank.setHorizontalAlignment(Element.ALIGN_LEFT);
						cellBlank.setBorder(Rectangle.NO_BORDER);
						mainTable12.addCell(cellBlank);

						PdfPCell poCell = new PdfPCell(mainTable12);
						poCell.setBorder(Rectangle.NO_BORDER);
						poTable.addCell(poCell);
						PdfPCell poTempCell = new PdfPCell(poTable);
						poTempCell.setBorder(Rectangle.NO_BORDER);
						poMainTable.addCell(poTempCell);
					}

				}

				// cell2po = new PdfPCell();
				// cell2po.setBorder(Rectangle.NO_BORDER);
				// mainTable1.addCell(cell2po);
				// maindocument.add(mainTable1);
				maindocument.add(poMainTable);
			}
			// end PO type check
			maindocument.newPage();
			// Zeroth Row
			cell1 = new PdfPCell(new Paragraph(GeneralData[12], fonts[0]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			table = new PdfPTable(3);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			Image logo = Image.getInstance(imagePath);

			float aspectRatio = 381.0f / 148.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewPO(String, String, String, String, String, Connection, String) - "
						+ aspectRatio);
			}
			float y = 72.39f;
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			cell1 = new PdfPCell(logo);
			cell1.setHorizontalAlignment(C);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[13], fonts[5]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			cell2.setBorder(0);
			maintable.addCell(cell2);

			// Row
			table = new PdfPTable(1);
			table.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(GeneralData[14] + " "
					+ lm_po_issue_date, fonts[3]));
			cell1.setHorizontalAlignment(R);
			table.addCell(cell1);
			cell1.setBorder(0);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// First Row
			table = new PdfPTable(3);
			table.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(GeneralData[0] + " "
					+ lm_po_number, fonts[10]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[1] + " "
					+ lm_po_deliver_by_date + " " + lm_po_deliver_by_time,
					fonts[9]));
			cell1.setHorizontalAlignment(C);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[2] + " "
					+ lm_po_number, fonts[10]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setBackgroundColor(red);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Second Row
			cell1 = new PdfPCell(new Paragraph(GeneralData[3], fonts[0]));
			cell1.setBackgroundColor(gray);
			cell1.setHorizontalAlignment(C);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			cell1 = new PdfPCell(new Paragraph(GeneralData[4], fonts[0]));
			cell1.setHorizontalAlignment(C);
			cell1.setBackgroundColor(gray);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Third Row // Column One
			cell1 = new PdfPCell(new Paragraph(lm_po_ship_to_address, fonts[0]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			cell1.setMinimumHeight(69f);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// // Third Row Column Two
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			table.getDefaultCell().setHorizontalAlignment(R);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(GeneralData[36], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(L);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_name, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(R);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(GeneralData[37], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(L);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_address, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(R);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(GeneralData[38], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(L);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(company_city, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(R);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(GeneralData[39], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(L);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(cns_pc_name, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(R);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(GeneralData[34], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(L);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(project_manager, fonts[3]));

			table.getDefaultCell().setHorizontalAlignment(R);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(GeneralData[35], fonts[2]));

			table.getDefaultCell().setHorizontalAlignment(L);
			table.getDefaultCell().setBorder(0);
			table.addCell(new Paragraph(facsimile, fonts[3]));

			/*
			 * Comment for Release 1.251
			 * table.getDefaultCell().setHorizontalAlignment(R);
			 * table.getDefaultCell().setBorder(0); table.addCell(new
			 * Paragraph(GeneralData[25], fonts[2]));
			 * 
			 * table.getDefaultCell().setHorizontalAlignment(L);
			 * table.getDefaultCell().setBorder(0); table.addCell(new
			 * Paragraph(login_name, fonts[3]));
			 */

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Fourth Row
			cell1 = new PdfPCell(new Paragraph(GeneralData[5], fonts[0]));
			cell1.setBackgroundColor(gray);
			cell1.setHorizontalAlignment(C);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			cell1 = new PdfPCell(new Paragraph(GeneralData[6], fonts[0]));
			cell1.setHorizontalAlignment(C);
			cell1.setBackgroundColor(gray);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// // Fifth Row Column One
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			for (x = 0; x < SiteAddressData.length; x++) {

				table.getDefaultCell().setHorizontalAlignment(R);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(SiteAddressData[x], fonts[2]));

				table.getDefaultCell().setHorizontalAlignment(L);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(site_address_data[x], fonts[3]));
			}

			cell1 = new PdfPCell(new Paragraph(GeneralData[7], fonts[3]));
			cell1.setHorizontalAlignment(C);
			cell1.setBorder(0);
			cell1.setColspan(2);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			maintable.addCell(cell2);

			// // Fifth Row Column Two
			table = new PdfPTable(2);
			table.setWidthPercentage(100);

			for (x = 0; x < AddressData.length; x++) {

				table.getDefaultCell().setHorizontalAlignment(R);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(AddressData[x], fonts[2]));

				table.getDefaultCell().setHorizontalAlignment(L);
				table.getDefaultCell().setBorder(0);
				table.addCell(new Paragraph(pvs_address_data[x], fonts[3]));
			}

			cell2 = new PdfPCell();
			cell2.addElement(table);
			maintable.addCell(cell2);

			// Sixth Row
			cell1 = new PdfPCell(new Paragraph(GeneralData[8], fonts[0]));
			cell1.setBackgroundColor(gray);
			cell1.setHorizontalAlignment(C);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Seventh Row
			cell1 = new PdfPCell(new Paragraph(lm_po_special_conditions,
					fonts[2]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			cell1.setPaddingBottom(80);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Eighth Row
			cell1 = new PdfPCell(new Paragraph(GeneralData[9], fonts[0]));
			cell1.setBackgroundColor(gray);
			cell1.setHorizontalAlignment(C);

			table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.addCell(cell1);

			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Ninth Row
			int headerwidths[] = { 6, 20, 34, 10, 10, 10, 10 }; // percentage
			PdfPTable datatable = new PdfPTable(7);
			datatable.setWidths(headerwidths);
			datatable.setWidthPercentage(100);

			cell1 = new PdfPCell(new Paragraph(GeneralData[10], fonts[2]));
			cell1.setHorizontalAlignment(C);
			cell1.setBorder(0);
			cell1.setColspan(7);
			datatable.addCell(cell1);

			for (x = 0; x < HeaderData.length; x++) {
				if (x < 3)
					datatable.getDefaultCell().setHorizontalAlignment(L);
				if (x == 3)
					datatable.getDefaultCell().setHorizontalAlignment(C);
				if (x > 3)
					datatable.getDefaultCell().setHorizontalAlignment(R);
				datatable.getDefaultCell().setBorder(0);
				datatable.addCell(new Paragraph(HeaderData[x], fonts[4]));
			}

			/*
			 * cell1 = new PdfPCell(new Paragraph(GeneralData[11], fonts[4]));
			 * cell1.setHorizontalAlignment(L); cell1.setBorder(0);
			 * cell1.setColspan(7); datatable.addCell(cell1);
			 */

			/* fetching data from database start */
			int i = 0;
			float total_cost = 0;
			float sub_total = 0;
			String masterPOItem = "";
			cstmt = null;
			rs = null;
			// System.out.println("id::::::::::"+powoId);

			cstmt = conn.prepareCall("{?=call lm_purchase_order_manage_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, powoId);

			rs = cstmt.executeQuery();
			while (rs.next()) {
				masterPOItem = rs.getString(1);
				if (rs.getString(1).equals("Fixed Price Services")) {
					cell1 = new PdfPCell(new Paragraph(
							rs.getString("lm_pwo_type_desc")
									+ " and/or Materials for Job, "
									+ rs.getString("lm_js_title"), fonts[3]));
					cell1.setHorizontalAlignment(C);
					cell1.setBorder(0);
					cell1.setColspan(6);
					datatable.addCell(cell1);
					if (rs.getString("lm_po_authorised_total") != null)
						sub_total = Float.parseFloat(rs
								.getString("lm_po_authorised_total"));
					break;
				}

				if (rs.getString(1).equals("")) {
					if (rs.getString(6) != null)
						sub_total = Float.parseFloat(rs
								.getString("lm_po_authorised_total"));
				} else if (!rs.getString(1).equals("")) {
					i++;
					cell1 = new PdfPCell(new Paragraph("" + i, fonts[2]));
					cell1.setBorder(0);
					cell1.setHorizontalAlignment(L);
					datatable.addCell(cell1);

					cell1 = new PdfPCell(new Paragraph(
							rs.getString("iv_sc_name"), fonts[2]));
					cell1.setBorder(0);
					cell1.setHorizontalAlignment(L);
					datatable.addCell(cell1);

					cell1 = new PdfPCell(new Paragraph(
							rs.getString("iv_rp_name"), fonts[2]));
					cell1.setBorder(0);
					cell1.setHorizontalAlignment(L);
					datatable.addCell(cell1);

					cell1 = new PdfPCell(new Paragraph(
							rs.getString("lm_rs_iv_cns_part_number"), fonts[2]));
					cell1.setBorder(0);
					cell1.setHorizontalAlignment(C);
					datatable.addCell(cell1);

					cell1 = new PdfPCell(
							new Paragraph(Decimalroundup.twodecimalplaces(
									rs.getFloat("lx_ce_quantity"), 2), fonts[2]));
					cell1.setBorder(0);
					cell1.setHorizontalAlignment(R);
					datatable.addCell(cell1);

					cell1 = new PdfPCell(new Paragraph("$ "
							+ Decimalroundup.twodecimalplaces(
									rs.getFloat("lm_pwo_authorised_unit_cost"),
									2), fonts[2]));
					cell1.setBorder(0);
					cell1.setHorizontalAlignment(R);
					datatable.addCell(cell1);

					total_cost = (rs.getFloat("lx_ce_quantity"))
							* (rs.getFloat("lm_pwo_authorised_unit_cost"));

					cell1 = new PdfPCell(new Paragraph("$ "
							+ Decimalroundup.twodecimalplaces(total_cost, 2),
							fonts[2]));
					cell1.setBorder(0);
					cell1.setHorizontalAlignment(R);
					datatable.addCell(cell1);

					sub_total = sub_total + total_cost;
				}
			}

			cell1 = new PdfPCell(new Paragraph("", fonts[4]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			cell1.setColspan(7);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[15], fonts[3]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			cell1.setColspan(6);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("$ "
					+ Decimalroundup.twodecimalplaces(sub_total, 2), fonts[1]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[16], fonts[3]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			cell1.setColspan(6);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("0.00", fonts[1]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			datatable.addCell(cell1);

			// if(masterPOItem.equalsIgnoreCase("Hourly Services"))
			// cell1 = new PdfPCell(new Paragraph(GeneralData[24], fonts[3]));
			// else
			cell1 = new PdfPCell(new Paragraph("", fonts[2]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			cell1.setColspan(5);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[17] + " %0.00",
					fonts[3]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("0.00", fonts[1]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[22] + " "
					+ pwo_terms, fonts[6]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			cell1.setColspan(5);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[18], fonts[3]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("0.00", fonts[1]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			datatable.addCell(cell1);

			// new row

			if (masterPOItem.equalsIgnoreCase("Hourly Services")
					|| masterPOItem.equalsIgnoreCase("Show Items")) {
				// new row
				Chunk ck = new Chunk(GeneralData[20], fonts[2]);
				Phrase p = new Phrase(ck);

				ck = new Chunk(GeneralData[31], fonts[2]);
				p.add(ck);

				ck = new Chunk(GeneralData[27], fonts[4]);
				p.add(ck);

				ck = new Chunk(GeneralData[31], fonts[2]);
				p.add(ck);

				ck = new Chunk(GeneralData[28], fonts[2]);
				p.add(ck);

				ck = new Chunk(GeneralData[31], fonts[2]);
				p.add(ck);

				ck = new Chunk(GeneralData[29], fonts[4]);
				p.add(ck);

				ck = new Chunk(GeneralData[31], fonts[2]);
				p.add(ck);

				ck = new Chunk(GeneralData[30], fonts[2]);
				p.add(ck);

				cell1 = new PdfPCell(p);
				// cell1 = new PdfPCell(new Paragraph(GeneralData[20],
				// fonts[2]));
			} else {
				cell1 = new PdfPCell(new Paragraph(""));
			}
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			cell1.setColspan(5);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[19], fonts[7]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph("$ "
					+ Decimalroundup.twodecimalplaces(sub_total, 2), fonts[1]));
			cell1.setHorizontalAlignment(R);
			cell1.setBorder(0);
			datatable.addCell(cell1);

			if (commission_flag == 1) {
				cell1 = new PdfPCell(new Paragraph(sow, fonts[2]));
				cell1.setHorizontalAlignment(L);
				cell1.setBorder(0);
				cell1.setColspan(7);
				datatable.addCell(cell1);
			}

			if (masterPOItem.equalsIgnoreCase("Material")
					|| masterPOItem.equalsIgnoreCase("Fixed Price Services"))
				cell1 = new PdfPCell(new Paragraph("", fonts[2]));
			else
				cell1 = new PdfPCell(new Paragraph(GeneralData[21], fonts[2]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			cell1.setColspan(7);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[32], fonts[2]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			cell1.setColspan(7);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[33], fonts[2]));
			cell1.setHorizontalAlignment(L);
			cell1.setBorder(0);
			cell1.setColspan(7);
			datatable.addCell(cell1);

			cell1 = new PdfPCell(new Paragraph(GeneralData[26], fonts[3]));
			cell1.setHorizontalAlignment(C);
			cell1.setBorder(0);
			cell1.setColspan(7);
			datatable.addCell(cell1);

			/* fetching data from database end */

			cell2 = new PdfPCell();
			cell2.addElement(datatable);
			cell2.setColspan(2);
			maintable.addCell(cell2);

			// Ninth Row
			maindocument.add(maintable);

			// PO T&C

			maindocument.setMargins(5, 5, 10, 10);
			maindocument.newPage();

			/*
			 * 09/10/2011 Update
			 * 
			 * Updated generation of T&C to
			 * 
			 * 1) eliminate the default behavior to limit the T&C to a
			 * non-breaking page.
			 * 
			 * 2) take advantage of new format to add dynnamic formating for
			 * line beginning with numeric value and a dash (both in first
			 * column)
			 * 
			 * 3) correct the dynamic variable substitution.
			 * 
			 * 4) minimal top and bottom margin changes.
			 */

			maintable = new PdfPTable(1);
			maintable.setWidthPercentage(99.0F);

			// Build gray header

			cell1 = new PdfPCell(new Paragraph(GeneralData[23], fonts[17]));
			cell1.setBackgroundColor(gray);
			cell1.setHorizontalAlignment(C);
			maintable.addCell(cell1);

			// Get and build T&C

			String consolidated_address = "";

			Font SpecialCharacters = new Font();
			SpecialCharacters.setFamily("ZAPFDINGBATS");
			SpecialCharacters.setSize(4F);
			Chunk bullet = new Chunk((char) 0156, SpecialCharacters)
					.setTextRise(1);

			// The T&C is saved based on the latest copy available when a PO is
			// created

			stmt = conn.createStatement();
			sql = "select lm_po_tc_data from lm_po_term_condition inner join lm_purchase_order on lm_purchase_order.lm_po_tc_id = lm_po_term_condition.lm_po_tc_id where lm_po_id ="
					+ powoId;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				lm_po_data = new String(rs.getBytes(1));
				/*
				 * Perform dynamic variable substitution for
				 * 
				 * {~ILEX GENERATED DATE~} - lm_po_issue_date - if null use
				 * today's date {~ILEX GENERATED CONTRACTOR NAME~} -
				 * pvs_address_data[0] - force upper case {~ILEX GENERATED
				 * CONTRACTOR ADDRESS~} - pvs_address_data[1] +
				 * pvs_address_data[2] - force upper case
				 */

				lm_po_data = lm_po_data;

				// Test and build the required dynamic data to be substituted
				// into T&C

				// Verify there is a date and if not just default to current
				// date

				if (lm_po_issue_date == null || lm_po_issue_date.equals("")) {
					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					Date date = new Date();
					lm_po_issue_date = dateFormat.format(date);
				}

				consolidated_address = (pvs_address_data[1] + ", " + pvs_address_data[2])
						.toUpperCase();
				pvs_address_data[0] = pvs_address_data[0].toUpperCase();

				// Substitute dynamic variables

				lm_po_data = lm_po_data.replaceAll(
						"\\{~ILEX GENERATED DATE~\\}", lm_po_issue_date);
				lm_po_data = lm_po_data.replaceAll(
						"\\{~ILEX GENERATED CONTRACTOR NAME~\\}",
						pvs_address_data[0]);
				lm_po_data = lm_po_data.replaceAll(
						"\\{~ILEX GENERATED CONTRACTOR ADDRESS~\\}",
						consolidated_address);

				/*
				 * Take advantage of new document formating to dynamically add
				 * formatting This will include treating paragraphs individually
				 * allowing some minimal dydnamic formatting explained below.
				 */

				// Split into individual paragraphs

				String[] breakout = lm_po_data.split("\\r\\n");

				if (breakout.length > 1) {

					table = new PdfPTable(1);
					table.setWidthPercentage(100.0F);

					for (int j = 0; j < breakout.length; j++) {

						if (breakout[j].matches("^\\d.*")) { // Starts with a
																// number

							para = new Paragraph(
									fonts[3].getCalculatedLeading(2.0F),
									breakout[j], fonts[19]);

						} else if (breakout[j].matches("^\\-.*")) { // Starts
																	// with a
																	// bullet

							breakout[j] = breakout[j].replace("-", "");
							para = new Paragraph(
									fonts[2].getCalculatedLeading(2.0F),
									"           ", fonts[20]);
							para.add(bullet);
							para.add(" ");
							para.add(breakout[j]);

						} else { // Default

							para = new Paragraph(
									fonts[2].getCalculatedLeading(2.0F),
									breakout[j], fonts[20]);
						}

						para.setAlignment(3);
						cell1 = new PdfPCell(para);
						cell1.setHorizontalAlignment(3);
						cell1.setPaddingLeft(8.0F);
						cell1.setPaddingRight(8.0F);
						cell1.setPaddingBottom(1.1F);

						// Bottom and top margin variations

						if (j == breakout.length - 1) {
							cell1.setPaddingBottom(6.0F);
						} else {
							cell1.setPaddingBottom(.5F);
						}
						if (j == 0) {
							cell1.setPaddingTop(5.0F);
						} else {
							cell1.setPaddingTop(1.25F);
						}

						cell1.setBorder(0);
						table.addCell(cell1);
					}

					maintable.setSplitLate(false);
					maintable.addCell(table);

				} else { // Default action left if content is not presented as
							// expected from a word document.

					para = new Paragraph(fonts[8].getCalculatedLeading(2.0F),
							lm_po_data, fonts[8]);
					para.setAlignment(3);
					cell1 = new PdfPCell(para);
					cell1.setHorizontalAlignment(3);
					cell1.setPaddingLeft(5.0F);
					cell1.setPaddingRight(5.0F);
					maintable.setSplitLate(false);
					maintable.addCell(cell1);
				}

			}

			/* 09/10/2011 Update */

			maindocument.add(maintable);

		}

		catch (Exception de) {
			logger.error(
					"viewPO(String, String, String, String, String, Connection, String)",
					de);

			logger.error(
					"viewPO(String, String, String, String, String, Connection, String)",
					de);
		}

		finally {

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error(
						"viewPO(String, String, String, String, String, Connection, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewPO(String, String, String, String, String, Connection, String) - Error occured during closing Connection conn"
							+ e);
				}
			}

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.error(
						"viewPO(String, String, String, String, String, Connection, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewPO(String, String, String, String, String, Connection, String) - Error occured during closing Result set "
							+ e);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.error(
						"viewPO(String, String, String, String, String, Connection, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewPO(String, String, String, String, String, Connection, String) - Error occured during closing Statement "
							+ e);
				}
			}

		}

		maindocument.close();
		return outbuff.toByteArray();
	}

	public String checkType(String powoId, DataSource ds) {
		String checktype = "";
		Connection conn = null;
		try {
			conn = ds.getConnection();
			checktype = checkType(powoId, conn);
		} catch (SQLException se) {
			logger.error("checkType(String, DataSource)", se);
		} finally {
			DBUtil.close(conn);
		}
		return checktype;
	}

	public String checkType(String powoId, Connection conn) {
		String checktype = "";
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			stmt = conn.createStatement();
			sql = "select distinct(lm_po_type) from lm_purchase_order where lm_po_id="
					+ powoId;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				checktype = rs.getString("lm_po_type");
			}
		} catch (Exception e) {
			logger.error("checkType(String, Connection)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkType(String, Connection) - Exception gettting type"
						+ e);
			}
		} finally {

			DBUtil.close(rs, stmt);

		}

		return checktype;
	}
}
