package com.mind.xml;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.mind.common.Decimalroundup;
import com.mind.dao.IM.IMdao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;

public class NewNetMedXPdf extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(NewNetMedXPdf.class);

	public byte[] viewNetmedxPdf(String id, String type, String fileName,
			String imagePath, String fontPath, String loginUserId, DataSource ds) {

		Document document = new Document(PageSize.A4, 1, 1, 20, 20);
		Document maindocument = new Document(PageSize.A4, 1, 1, 20, 20);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();

		Connection conn = null;
		Statement stmt = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			PdfWriter.getInstance(document,
					new FileOutputStream(System.getProperty("ilex.temp.path")
							+ "/tempNewNetmedXPdf.pdf"));
			PdfWriter writermain = PdfWriter.getInstance(maindocument, outbuff);

			String footertext = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tConfidential & Proprietary \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t \u00a9 2009 Contingent Network Services, LLC \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Page ";

			/* Set font array */
			BaseFont bfArialN = BaseFont.createFont(fontPath,
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

			Font[] fonts = new Font[10];
			fonts[0] = new Font(bfArialN, 7, Font.NORMAL);
			fonts[1] = new Font(bfArialN, 11, Font.BOLD);
			fonts[2] = new Font(bfArialN, 7, Font.BOLD);
			fonts[3] = new Font(bfArialN, 8, Font.NORMAL);
			fonts[4] = new Font(Font.TIMES_ROMAN, 7, Font.NORMAL);
			fonts[5] = new Font(bfArialN, 7, Font.BOLD);
			fonts[6] = new Font(bfArialN, 6, Font.BOLD);
			fonts[7] = new Font(bfArialN, 6, Font.NORMAL);
			fonts[8] = new Font(bfArialN, 5, Font.BOLD);
			fonts[9] = null;

			PdfPTable table1 = new PdfPTable(1);
			PdfPTable table2 = new PdfPTable(1);
			PdfPTable table3 = new PdfPTable(2);

			PdfPTable maintable1 = new PdfPTable(1);
			PdfPTable maintable2 = new PdfPTable(1);
			PdfPTable maintable3 = new PdfPTable(2);

			PdfPTable table = new PdfPTable(2);
			PdfPTable headertable = new PdfPTable(2);

			headertable.setWidthPercentage(90);
			table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			table2.getDefaultCell().setBorder(Rectangle.NO_BORDER);

			PdfPCell cella, cellContent, cellNumber;
			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			PdfPCell cell3 = new PdfPCell();
			PdfPCell cell4 = new PdfPCell();

			Table headertable1 = new Table(2);
			headertable1.setBorderWidth(0);
			headertable1.setPadding(45);

			Image logo = Image.getInstance(imagePath);

			float aspectRatio = 381.0f / 148.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewNetmedxPdf(String, String, String, String, String, String, String, DataSource) - "
						+ aspectRatio);
			}
			float y = 122.39f;
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			logo.setAlignment(Image.MIDDLE);
			Chunk chunk = new Chunk(logo, 0, -85);

			HeaderFooter header = new HeaderFooter(new Phrase(chunk), false);
			HeaderFooter footer = new HeaderFooter(new Phrase(footertext,
					fonts[4]), true);

			header.setAlignment(Element.ALIGN_CENTER);
			header.setBorder(Rectangle.NO_BORDER);
			footer.setAlignment(Element.ALIGN_LEFT);
			footer.setBorder(Rectangle.NO_BORDER);
			maindocument.setHeader(header);
			maindocument.setFooter(footer);
			document.open();

			maindocument.open();

			PdfDestination pd = new PdfDestination(PdfDestination.XYZ, -1, -1,
					1);
			PdfAction action = PdfAction.gotoLocalPage(1, pd, writermain);
			writermain.setOpenAction(action);

			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(fileName, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator it = list1.iterator();
			Title title = new Title();
			Number number = new Number();
			Content content = new Content();

			int i = 0;
			String titleText = "";
			String numberText = "";
			String contentText = "";
			String titleType = "";
			String numberType = "";
			String contentType = "";
			int colspan = 0;
			int rowspan = 0;
			int tableSize = 0;
			int t = 0;
			int k = 0;
			boolean addtable = false;
			boolean addfulltable = false;
			boolean adddata = false;
			boolean chkpadding = false;
			boolean addNewPara = false;
			boolean bt1 = false;
			boolean bt2 = false;
			double diff = 0.0;
			double count = 0.0;
			double pretable3Height = 0.0;
			double table3Height = 0.0;
			double setTableHeight = 620;
			double TableHeightLower = 620;
			double TableHeightUpper = 0.0;
			boolean logistics = false;
			PdfPTable tableLogistic = null;
			String Onbullet = "";
			String jobId = "0";
			String sql = "";
			int num = 0;
			String[][] resourcelabel = null;
			String singleColumn = "";
			int listIndex = 0;
			String contentLabel = "";

			String msaid = IMdao.getMSAId(id, ds);
			if (msaid.equals(""))
				msaid = null;

			stmt = conn.createStatement();

			while (it.hasNext()) {
				if (bt1 && bt2) {
					cella = new PdfPCell(maintable1);
					cella.setBorder(Rectangle.NO_BORDER);
					if (!chkpadding)
						cella.setPaddingTop(5);
					else
						cella.setPaddingTop(50);
					maintable3.addCell(cella);

					cella = new PdfPCell(maintable2);
					cella.setBorder(Rectangle.NO_BORDER);
					if (!chkpadding)
						cella.setPaddingTop(5);
					else
						cella.setPaddingTop(50);
					maintable3.addCell(cella);

					maindocument.add(maintable3);

					if (singleColumn.equalsIgnoreCase("singleColumnStart")) {
						table3 = new PdfPTable(1);
						maintable3 = new PdfPTable(1);
					} else if (singleColumn.equalsIgnoreCase("")
							|| singleColumn.equalsIgnoreCase("singleColumnEnd")) {
						table3 = new PdfPTable(2);
						maintable3 = new PdfPTable(2);
					}

					maintable3.deleteBodyRows();
					maintable1 = new PdfPTable(1);
					maintable2 = new PdfPTable(1);
					bt1 = false;
					bt2 = false;
					table3Height = 0.0;
					TableHeightLower = 620;
					chkpadding = true;
				}

				TableHeightUpper = 2 * TableHeightLower;
				titleText = "";
				numberText = "";
				contentText = "";
				titleType = "";
				numberType = "";
				section = (Section) it.next();

				numberType = section.getNumber().getType();
				titleType = section.getTitle().getType();

				title = section.getTitle();
				titleText = title.getContent();
				if (titleText.indexOf("~") >= 0)
					titleText = addParameter(titleText, id, type, loginUserId,
							jobId, ds);
				number = section.getNumber();
				numberText = number.getContent();
				if (numberText.indexOf("~") >= 0)
					numberText = addParameter(numberText, id, type,
							loginUserId, jobId, ds);
				content = section.getContent();
				contentText = content.getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = addParameter(contentText, id, type,
							loginUserId, jobId, ds);

				if (numberType.equalsIgnoreCase("singleColumnStart")
						|| numberType.equalsIgnoreCase("singleColumnEnd")) {
					singleColumn = numberType;
				}

				// Code for check that Discount is 0 or not start.
				if (contentText.equals("NetMedX Standard Extended Discount")) {
					contentText = getContentTextFromDB(contentText, id, msaid,
							ds);
				}
				// Code for check that Discount is 0 or not end

				if (i == 0) {
					Paragraph para1 = new Paragraph(
							fonts[0].getCalculatedLeading(4), titleText,
							fonts[1]);
					para1.setSpacingAfter(15f);
					para1.setSpacingBefore(15f);
					cell2 = new PdfPCell(para1);
					cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell2.setBorder(Rectangle.NO_BORDER);
					cell2.setPaddingTop(42);
					cell2.setColspan(2);

					para1 = new Paragraph(fonts[0].getCalculatedLeading(4),
							contentText, fonts[3]);
					para1.setSpacingAfter(10f);
					para1.setSpacingBefore(10f);
					cell3 = new PdfPCell(para1);
					cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell3.setBorder(Rectangle.NO_BORDER);
					cell3.setPaddingTop(2);
					cell3.setColspan(2);

					para1 = new Paragraph(fonts[0].getCalculatedLeading(4),
							numberText, fonts[3]);
					para1.setSpacingAfter(5f);
					para1.setSpacingBefore(5f);
					cell4 = new PdfPCell(para1);
					cell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell4.setBorder(Rectangle.NO_BORDER);
					cell4.setPaddingTop(2);
					cell4.setColspan(2);

					headertable.addCell(cell2);
					headertable.addCell(cell3);
					headertable.addCell(cell4);
					maindocument.add(headertable);
					section = (Section) it.next();
					title = section.getTitle();
					titleText = title.getContent();
					titleType = section.getTitle().getType();
					if (titleText.indexOf("~") >= 0)
						titleText = addParameter(titleText, id, type,
								loginUserId, jobId, ds);
					// numberText = xt.getNumber() + " ";
					number = section.getNumber();
					numberText = number.getContent();
					if (numberText.indexOf("~") >= 0)
						numberText = addParameter(numberText, id, type,
								loginUserId, jobId, ds);
					content = section.getContent();
					contentText = content.getContent();
					if (contentText.indexOf("~") >= 0)
						contentText = addParameter(contentText, id, type,
								loginUserId, jobId, ds);
				}
				i++;

				if (i != 0) {
					if (titleType.equals("table")) {
						contentType = section.getContent().getType();
						numberType = section.getNumber().getType();

						colspan = section.getTitle().getColspan().intValue();
						rowspan = section.getTitle().getRowspan().intValue();
						tableSize = section.getTitle().getSize().intValue();

						if (t == 0) {
							/* BEGIN */
							if (contentType.equals("iv_cns_field_services")
									|| contentType
											.equals("iv_cns_field_services_silver")
									|| contentType
											.equals("iv_cns_field_services_copper")) {

								if (contentType
										.equals("iv_cns_field_services_copper")) {
									contentLabel = "iv_cns_field_services";
								} else {
									contentLabel = contentType;
								}

								// sql =
								// "select count(*) from (select distinct clr_class_id, clr_reslevel_id from func_iv_clr_show_resourcelevel("+msaid+",'"+contentType+"') where clr_reslevel_show_on_doc = 'Y') as num";
								sql = "select count(*) from (select distinct clr_class_id, clr_reslevel_id from func_iv_clr_show_resourcelevel("
										+ id
										+ ",'"
										+ contentLabel
										+ "') where clr_reslevel_show_on_doc = 'Y') as num";
								rs = stmt.executeQuery(sql);

								if (rs.next()) {
									num = rs.getInt(1);
								}

								resourcelabel = new String[num][2];

								sql = "select distinct clr_reslevel_name, clr_reslevel_id, clr_class_id  from func_iv_clr_show_resourcelevel("
										+ id
										+ ",'"
										+ contentLabel
										+ "') where clr_reslevel_show_on_doc = 'Y'"
										+ "order by clr_class_id, clr_reslevel_id";

								rs = stmt.executeQuery(sql);
								listIndex = 0;
								while (rs.next()) {
									resourcelabel[listIndex][0] = rs
											.getString("clr_class_id");
									resourcelabel[listIndex][1] = rs
											.getString("clr_reslevel_name");
									listIndex++;
								}
								if (contentType.equals("iv_cns_field_services")
										|| contentType
												.equals("iv_cns_field_services_silver")
										|| contentType
												.equals("iv_cns_field_services_copper")) {
									tableSize = tableSize + num + 2;
									colspan = colspan + num + 2;
								}
							}
							/* END */

							table = new PdfPTable(tableSize);
							if (contentType.equals("iv_cns_logistics_services")) {
								tableLogistic = new PdfPTable(tableSize);
							}
						}
						Paragraph para = new Paragraph(titleText, fonts[6]);
						cell1 = new PdfPCell(para);
						cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell1.setPaddingLeft(5f);
						cell1.setPaddingRight(5f);
						cell1.setColspan(colspan);
						table.addCell(cell1);
						adddata = true;
						t++;
					} else {
						if (adddata) {
							sql = "";
							if (contentType.equals("iv_criticality"))
								sql = "select iv_crit_name, iv_crit_condition from iv_criticality ";
							if (contentType.equals("iv_distance_restime"))
								sql = "select iv_distance_value, iv_response_time from iv_distance_restime";
							if (contentType.equals("iv_cns_call_res_price"))
								sql = "select iv_cns_res_name, iv_cns_price_per_min_pps, iv_cns_price_per_min_nonpps from iv_cns_call_res_price";
							if (contentType.equals("iv_cns_nw_price"))
								sql = "select iv_cns_nw_item_name, iv_cns_nw_item_price from iv_cns_nw_price";
							if (contentType.equals("iv_cns_logistics_services")) {
								sql = "select iv_ls_activity, iv_ls_desc, iv_ls_rates, iv_ls_addtional_info from iv_cns_logistics_services";
								logistics = true;
							}
							if (contentType.equals("iv_view_restime_goal")) {
								cstmt = conn
										.prepareCall("{?=call iv_view_restime_goal()}");
								cstmt.registerOutParameter(1,
										java.sql.Types.INTEGER);
								rs = cstmt.executeQuery();
							} else if (contentType
									.equals("iv_cns_field_services")
									|| contentType
											.equals("iv_cns_field_services_silver")
									|| contentType
											.equals("iv_cns_field_services_copper")) {
								// for showing discounted rates
								cstmt = conn
										.prepareCall("{?=call iv_clr_tab_03_show_on_doc(?, ?, ?)}");
								cstmt.registerOutParameter(1,
										java.sql.Types.INTEGER);
								cstmt.setString(2, id);
								cstmt.setString(3, "0");
								cstmt.setString(4, contentLabel);
								rs = cstmt.executeQuery();

								// Row 1
								table.addCell(getNewPdfPCell("", 2, fonts[6]));
								if (contentType
										.equals("iv_cns_field_services_silver")) {
									table.addCell(getNewPdfPCell(
											"PLEASE NOTE: THESE ARE PREPAID DISPATCH RATES (PER VISIT)",
											3, fonts[6]));
									table.addCell(getNewPdfPCell(
											"Per Dispatch Visit Discounted Rates"
													+ "\u00B9", num, fonts[6]));
								} else if (contentType
										.equals("iv_cns_field_services_copper")) {
									table.addCell(getNewPdfPCell(
											"PLEASE NOTE: THESE ARE DISPATCH RATES (PER VISIT)",
											3, fonts[6]));
									table.addCell(getNewPdfPCell(
											"Per Dispatch Visit Rates (1 Hr Max Onsite)",
											num, fonts[6]));
								} else {
									table.addCell(getNewPdfPCell("", 2,
											fonts[6]));
									table.addCell(getNewPdfPCell(
											"Hourly Labor Rates" + "\u00B9",
											num, fonts[6]));
								}

								// Row 2
								table.addCell(getNewPdfPCell("", 2, fonts[6]));
								if (contentType
										.equals("iv_cns_field_services_silver")) {
									table.addCell(getNewPdfPCell(
											"THESE ARE NOT PREPAID HOURLY RATES",
											3, fonts[6]));
								} else if (contentType
										.equals("iv_cns_field_services_copper")) {
									table.addCell(getNewPdfPCell("", 3,
											fonts[6]));
								} else {
									table.addCell(getNewPdfPCell("", 2,
											fonts[6]));
								}
								table.addCell(getNewPdfPCell("Technician", 2,
										fonts[6]));
								table.addCell(getNewPdfPCell("Engineer", 2,
										fonts[6]));

								// Row 3
								table.addCell(getNewPdfPCell("Activity", 2,
										fonts[6]));
								table.addCell(getNewPdfPCell("Description", 2,
										fonts[6]));

								if (contentType
										.equals("iv_cns_field_services_silver")) {
									table.addCell(getNewPdfPCell(
											"Penalty For Late Arrival", 1,
											fonts[6]));
								} else if (contentType
										.equals("iv_cns_field_services_copper")) {
									table.addCell(getNewPdfPCell(
											"Hourly Rate Above 1 hr Onsite"
													+ "\u00B9", 1, fonts[6]));
								}

								for (int z = 0; z < num; z++) {
									if (resourcelabel[z][1].contains("Level 2"))
										resourcelabel[z][1] = "Level 2";
									if (resourcelabel[z][1].contains("Level 3"))
										resourcelabel[z][1] = "Level 3";
									table.addCell(getNewPdfPCell(
											resourcelabel[z][1], 1, fonts[6]));
								}
							} else {
								rs = stmt.executeQuery(sql);
							}
							while (rs.next()) {
								if (contentType.equals("iv_cns_field_services")
										|| contentType
												.equals("iv_cns_field_services_silver")
										|| contentType
												.equals("iv_cns_field_services_copper")) {
									if (contentType
											.equals("iv_cns_field_services_copper")
											&& rs.getString(1).contains(
													"Scheduled")) {
										cell1 = new PdfPCell(new Paragraph(rs
												.getString(1).substring(
														0,
														rs.getString(1)
																.length() - 4)
												+ "\n" + rs.getString(2),
												fonts[8]));
									} else {
										cell1 = new PdfPCell(new Paragraph(
												rs.getString(1) + "\n"
														+ rs.getString(2),
												fonts[8]));
									}

									cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
									cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
									cell1.setColspan(2);
									table.addCell(cell1);

									if (contentType
											.equals("iv_cns_field_services")) {
										cell1 = new PdfPCell(new Paragraph(
												rs.getString(4) + "\n"
														+ rs.getString(6),
												fonts[8]));
									} else if (contentType
											.equals("iv_cns_field_services_silver")) {
										if (rs.getString(4).equals(
												"Next Business Day")
												|| rs.getString(4).equals(
														"2nd Business Day")) {
											cell1 = new PdfPCell(
													new Paragraph(
															rs.getString(4)
																	+ " \nFlat (2) hour onsite maximum plus (1) hour travel\n"
																	+ rs.getString(6),
															fonts[8]));
										} else {
											cell1 = new PdfPCell(
													new Paragraph(
															rs.getString(4)
																	+ " Flat (2) hour onsite maximum plus (1) hour travel\n"
																	+ rs.getString(6),
															fonts[8]));
										}
									} else if (contentType
											.equals("iv_cns_field_services_copper")) {
										if (rs.getString(4).equals(
												"Next Business Day")
												|| rs.getString(4).equals(
														"2nd Business Day")) {
											cell1 = new PdfPCell(
													new Paragraph(
															rs.getString(4)
																	+ " \nFlat (1) hour onsite maximum\n"
																	+ rs.getString(6),
															fonts[8]));
										} else {
											cell1 = new PdfPCell(
													new Paragraph(
															rs.getString(4)
																	+ " Flat (1) hour onsite maximum\n"
																	+ rs.getString(6),
															fonts[8]));
										}
									}

									cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
									cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
									cell1.setColspan(2);
									table.addCell(cell1);

									if (contentType
											.equals("iv_cns_field_services_silver")) {
										cell1 = new PdfPCell(new Paragraph(
												"15%", fonts[8]));
										cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
										cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
										table.addCell(cell1);
									} else if (contentType
											.equals("iv_cns_field_services_copper")) {
										cell1 = new PdfPCell(
												new Paragraph(
														"Billable at NetMedX Standard Hourly Rates",
														fonts[8]));
										cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
										cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
										table.addCell(cell1);
									}

									for (int z = 8; z <= 8 + (num - 1) * 4; z += 4) {
										if (rs.getString(z) != null) {
											if (contentType
													.equals("iv_cns_field_services_copper")) {
												cell1 = new PdfPCell(
														new Paragraph(
																"$"
																		+ Decimalroundup
																				.twodecimalplaces(
																						(2 * rs.getFloat(z)),
																						2),
																fonts[7]));
											} else {
												cell1 = new PdfPCell(
														new Paragraph(
																"$"
																		+ Decimalroundup
																				.twodecimalplaces(
																						rs.getFloat(z),
																						2),
																fonts[7]));
											}

											cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
											cell1.setVerticalAlignment(Element.ALIGN_CENTER);
											table.addCell(cell1);
										}

									}
								} else {
									for (int z = 1; z <= tableSize; z++) {
										if (logistics == true
												&& (z == 1 || z == 2)) {
											fonts[9] = new Font(bfArialN, 6,
													Font.BOLD);
										} else {
											fonts[9] = new Font(bfArialN, 7,
													Font.NORMAL);
										}

										if (rs.getString(z) != null) {
											if (logistics == true && z == 4) {
												cell1 = new PdfPCell(
														new Paragraph(
																rs.getString(z)
																		.replaceAll(
																				"~",
																				"\u2022 "),
																fonts[9]));
											} else {
												cell1 = new PdfPCell(
														new Paragraph(rs
																.getString(z),
																fonts[9]));
											}
										}
										cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
										cell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
										table.addCell(cell1);
									}
								}
							}
							addtable = true;
							adddata = false;
						}

						if (addtable && !addfulltable) {
							if (table3Height < TableHeightLower) {
								if (contentType.equals("iv_cns_field_services")
										|| contentType
												.equals("iv_cns_logistics_services")
										|| contentType
												.equals("iv_cns_field_services_silver")
										|| contentType
												.equals("iv_cns_field_services_copper")) {
									PdfPCell cellz = new PdfPCell();
									table.setWidthPercentage(100);
									cellz.addElement(table);
									cellz.setBorder(Rectangle.NO_BORDER);
									table1.addCell(cellz);
									maintable1.addCell(cellz);
								} else {
									table1.addCell(table);
									maintable1.addCell(table);
								}
							}

							else if (table3Height > TableHeightLower
									&& table3Height < TableHeightUpper) {
								table2.addCell(table);
								maintable2.addCell(table);

								if (pretable3Height < TableHeightLower) {
									diff = table3Height - pretable3Height;
									count = TableHeightLower - pretable3Height;
								}
							}

							cella = new PdfPCell(table1);
							cella.setBorder(Rectangle.NO_BORDER);
							table3.addCell(cella);

							cella = new PdfPCell(table2);
							cella.setBorder(Rectangle.NO_BORDER);
							table3.addCell(cella);

							maintable3.setWidthPercentage(90);
							table3.setWidthPercentage(90);
							document.add(table3);

							pretable3Height = table3Height;
							table3Height += table3.getTotalHeight();
							if ((pretable3Height < table3Height && table3Height < TableHeightUpper)
									&& (section.getPagebreakAfter()
											.equals("yes"))) {
								table3Height = TableHeightUpper;
								TableHeightLower = 620;
							}
							if (table3Height > TableHeightLower)
								bt1 = true;
							if (table3Height >= TableHeightUpper)
								bt2 = true;

							table1.deleteBodyRows();
							table2.deleteBodyRows();
							table3.deleteBodyRows();
						}
						addtable = false;
						t = 0;

						Onbullet = "";
						// if(xt.getNumberTypeAttribute().equals("notshow"))
						if (section.getNumber().getType().equals("notshow"))
							titleText = ""; // for first paragraph "General"
						// if(xt.getContentTypeAttribute().equals("bullet"))
						if (section.getContent().getType().equals("bullet"))
							Onbullet = "\u2022 ";

						Paragraph paraContent;
						paraContent = new Paragraph(
								fonts[3].getCalculatedLeading(1), Onbullet + ""
										+ contentText, fonts[3]);
						paraContent.setAlignment(Element.ALIGN_JUSTIFIED);

						Paragraph paraNumber = new Paragraph(
								fonts[2].getCalculatedLeading(2), numberText
										+ "  " + titleText, fonts[2]);
						paraNumber.setAlignment(Element.ALIGN_LEFT);

						cellContent = new PdfPCell(paraContent);
						cellContent
								.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cellContent.setBorder(Rectangle.NO_BORDER);
						cellContent.setPaddingLeft(5f);
						cellContent.setPaddingRight(5f);

						if (section.getContent().getAlign().equals("right")) {
							cellContent
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
						}

						cellNumber = new PdfPCell(paraNumber);
						cellNumber
								.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cellNumber.setBorder(Rectangle.NO_BORDER);
						cellNumber.setPaddingLeft(5f);
						cellNumber.setPaddingRight(5f);

						if (table3Height < TableHeightLower) {
							if (!titleText.equals("")) {
								table1.addCell(cellNumber);
								maintable1.addCell(cellNumber);
							}
							table1.addCell(cellContent);
							maintable1.addCell(cellContent);
						}

						else if (table3Height > TableHeightLower
								&& table3Height < TableHeightUpper) {
							if (!titleText.equals("")) {
								table2.addCell(cellNumber);
								maintable2.addCell(cellNumber);
							}

							if (pretable3Height < TableHeightLower) {
								diff = table3Height - pretable3Height;
								count = TableHeightLower - pretable3Height;
							}

							table2.addCell(cellContent);
							maintable2.addCell(cellContent);
						}

						cella = new PdfPCell(table1);
						cella.setBorder(Rectangle.NO_BORDER);
						table3.addCell(cella);

						cella = new PdfPCell(table2);
						cella.setBorder(Rectangle.NO_BORDER);
						table3.addCell(cella);

						maintable3.setWidthPercentage(90);
						table3.setWidthPercentage(90);
						document.add(table3);

						pretable3Height = table3Height;
						table3Height += table3.getTotalHeight();
						if ((pretable3Height < table3Height && table3Height < TableHeightUpper)
								&& (section.getPagebreakAfter().equals("yes"))) {
							table3Height = TableHeightUpper;
							TableHeightLower = 620;
						}
						if (table3Height > TableHeightLower)
							bt1 = true;
						if (table3Height >= TableHeightUpper)
							bt2 = true;

						table1.deleteBodyRows();
						table2.deleteBodyRows();
						table3.deleteBodyRows();
					}
				}
			}
		} catch (Exception de) {
			logger.error(
					"viewNetmedxPdf(String, String, String, String, String, String, String, DataSource)",
					de);
		}

		finally {
			DBUtil.closeCallableStatement(rs, cstmt);
			DBUtil.closeStatement(stmt);
			DBUtil.closeDbConnection(conn);
		}

		document.close();
		maindocument.close();
		return outbuff.toByteArray();
	}

	public static PdfPCell getNewPdfPCell(String label, int colspan, Font font) {
		PdfPCell cell1 = new PdfPCell(new Paragraph(label, font));
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setColspan(colspan);
		return cell1;
	}

	public static String getContentTextFromDB(String contentText,
			String netmedxId, String msaid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String retContentText = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// String
			// str="select clr_discount from clr_master where lp_mm_id = "+msaid;
			String str = "select dbo.func_get_netmedx_discount_rate(" + msaid
					+ ", " + netmedxId + ")";
			rs = stmt.executeQuery(str);

			if (rs.next()) {
				if (rs.getInt(1) != 0) {
					retContentText = "NetMedX Standard Extended Discount  "
							+ rs.getInt(1) + "%";
				} else {
					retContentText = "";
				}
			} else {
				retContentText = "";
			}
		} catch (Exception e) {
			logger.error(
					"getContentTextFromDB(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getContentTextFromDB(String, String, String, DataSource) - Exception Occured getting Netmedix Discount"
						+ e);
			}
		} finally {
			DBUtil.closeStatement(rs, stmt);
			DBUtil.closeDbConnection(conn);
		}
		return retContentText;
	}
}