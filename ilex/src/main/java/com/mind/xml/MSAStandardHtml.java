package com.mind.xml;

import java.io.StringReader;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;

public class MSAStandardHtml extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MSAStandardHtml.class);

	public byte[] viewMsaNewHtml(String id, String type, String fileName,
			String imagePath, String fontPath, String loginUserId, DataSource ds) {

		StringBuilder htmlBuilder = null;
		try {

			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(fileName, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			List<Section> list1 = doc.getSections().getSection();
			Iterator<Section> it = list1.iterator();
			Title title = new Title();
			Number number = new Number();
			Content content = new Content();

			int i = 0;
			String titleText = "";
			String numberText = "";
			String contentText = "";

			int numberOfColumns = 0;
			boolean isTableHeaderAdded = false;

			String boldText = "font-weight:bold;";
			String underLineText = "";
			String alignCenter = "text-align:center;";

			htmlBuilder = new StringBuilder();
			htmlBuilder.append("<html>");
			htmlBuilder.append("<head>");
			htmlBuilder.append("<title>MSA Details</title>");
			htmlBuilder.append("</head>");
			htmlBuilder
					.append("<body style=\"font-size:8pt; text-align:left; font-weight:normal; font-family: ArialNarrow; width: 98%; height: 100%; margin: 0 15px; padding: 0;\">");

			htmlBuilder
					.append("<p style=\"text-align: center;\"><img src='"
							+ imagePath
							+ "' name='graphics1' align='BOTTOM' border='0' width='238' height='80' /> </p>");

			while (it.hasNext()) {
				section = it.next();
				/* Getting inner tags of XML */
				title = section.getTitle();
				number = section.getNumber();
				content = section.getContent();

				/* Getting main contents from all tags */
				titleText = title.getContent();
				numberText = number.getContent();
				contentText = content.getContent().trim()
						.replaceAll("\\s", " ");

				String headingText = titleText.trim() + " ";
				if (!"".equals(numberText.trim())) {
					/* adding extra space with numbering bullets. */
					headingText = numberText.trim()
							+ "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"
							+ titleText.trim() + " ";
				}

				underLineText = "";
				if (title.getStyle().contains("underline")) {
					underLineText = "text-decoration: underline;";
				}

				if (i == 0 || "new_page".equals(section.getType())) {
					if (i > 0) {
						htmlBuilder.append("<p>&nbsp;</p>");
					}
					htmlBuilder.append("<p style=\"" + boldText + alignCenter
							+ underLineText + "\">" + headingText + "</p>");
					htmlBuilder.append("<p style=\"" + alignCenter + "\">"
							+ contentText + "</p>");
					i++;
				} else if ("subHeading".equals(title.getType())) {
					htmlBuilder.append("<p><span style=\"" + boldText
							+ underLineText + "\">" + headingText + "</span>");
					htmlBuilder.append(contentText + "</p>");
				} else if ("table_header".equals(section.getType())) {
					float[] columnWidths = new float[] { 12f, 25f, 15f, 8f, 5f,
							40f };

					String[] tableHeader = title.getContent().split("~\\|~");
					numberOfColumns = tableHeader.length;

					if ("ColSpan".equals(title.getType())) {
						numberOfColumns = 7;
						/* Setting up with of 2nd table. */
						columnWidths = new float[] { 8f, 10f, 35f, 20f, 5f, 8f,
								8f };
						htmlBuilder
								.append("<table width='100%' cellpadding='0' cellspacing='0' border='1'  style=\"font-size: 8pt; \">");
						htmlBuilder
								.append("<tr style=\"background-color: lightgray;\">");

						htmlBuilder.append("<td align='center' colspan='7'>"
								+ titleText + "</td>");

						htmlBuilder.append("</tr>");
					}

					if (!"ColSpan".equals(title.getType())) {
						htmlBuilder
								.append("<table width='100%'  cellpadding='0' cellspacing='0' border='1' style=\"font-size: 7pt; \">");
						htmlBuilder
								.append("<tr style=\"font-weight: bold; background-color: lightgray; \">");

						for (int j = 0; j < tableHeader.length; j++) {
							htmlBuilder
									.append("<td align='center' style=\"width: "
											+ columnWidths[j]
											+ "\">"
											+ tableHeader[j] + "</td>");
						}
						htmlBuilder.append("</tr>");
					}

					isTableHeaderAdded = true;// Making it true to add the code.
					/* end of table header section */
				} else if ("table_data_head".equals(section.getType())) {
					/*
					 * To add the head as 2nd row in the table.
					 */

					String[] tableRowsData = section.getTitle().getContent()
							.trim().split("~\\|~");

					/* Setting up with of 2nd table. */
					float[] columnWidths = new float[] { 8f, 10f, 35f, 20f, 5f,
							8f, 8f };
					htmlBuilder.append("<tr style=\"font-weight: bold; \">");
					for (int k = 0; k < tableRowsData.length; k++) {
						htmlBuilder.append("<td align='center' style=\"width: "
								+ columnWidths[k] + "\">" + tableRowsData[k]
								+ "</td>");
					}
					htmlBuilder.append("</tr>");

				} else if ("table_data".equals(section.getType())
						&& isTableHeaderAdded) {
					/*
					 * isTableHeaderAdded checking that if table header added
					 * then add the body of the table else no need to add other
					 * rows.
					 */

					String[] tableRowsData = content.getContent().trim()
							.split("~\\|\\|~");
					String[] tableData = null;

					for (int k = 0; k < tableRowsData.length; k++) {
						htmlBuilder.append("</tr>");
						tableData = tableRowsData[k].trim().split("~\\|~");
						for (int j = 0; j < numberOfColumns; j++) {
							/*
							 * added condition of length with J to avoid array
							 * index out of bound exception. to maintain then
							 * number of cells of table.
							 */
							if (tableData.length > j) {
								htmlBuilder.append("<td align='center'>"
										+ tableData[j] + "</td>");
							} else {
								htmlBuilder
										.append("<td align='center'>&nbsp;</td>");
							}

						}
						htmlBuilder.append("</tr>");
					}
					/* making it false for next tables if any */
					isTableHeaderAdded = false;
					htmlBuilder.append("</table>");
				} else {
					htmlBuilder.append("<p style=\"" + boldText + underLineText
							+ "\">" + headingText + "</p>");
					htmlBuilder.append("<p>" + contentText + "</p>");
				}

			}

			htmlBuilder.append("<p>&nbsp;</p>");
			htmlBuilder.append("<p>&nbsp;</p>");
			htmlBuilder
					.append("<p style=\"font-size: 7pt; color: lightgray; text-align: center;\"><span style=\"float:left;\">Copyright "
							+ Calendar.getInstance().get(Calendar.YEAR)
							+ " \u00a9 Comcast Enterprise Services, LLC</span><span>1 | Page</span><span style=\"float:right;\">Confidential & Proprietary</span></p>");
			htmlBuilder.append("<p>&nbsp;</p>");
			htmlBuilder.append("<p>&nbsp;</p>");

			htmlBuilder.append("</body>");
			htmlBuilder.append("</html>");

		}

		catch (Exception de) {
			System.out.println(de);
			logger.error(
					"viewMsaNewHtml(String, String, String, String, String, String, DataSource)",
					de);

			logger.error(
					"viewMsaNewHtml(String, String, String, String, String, String, DataSource)",
					de);
		}

		return htmlBuilder.toString().getBytes();
	}
}
