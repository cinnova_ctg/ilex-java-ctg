package com.mind.xml;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Domxmltaglist.class);

	public static String addParameter(String paragraph, String id, String type,
			String login_userid, String job_id, DataSource ds) {
		int total = 0;
		int i = 0;
		String pReplace = paragraph;
		String p = paragraph;
		total = totalOccurance(paragraph);
		int strlen = (total / 2);
		String[] para = new String[strlen];
		String[] em_para = new String[strlen];
		try {
			for (i = 0; i < strlen; i++) {
				if (totalOccurance(p) > 1) {
					p = p.substring(p.indexOf("~") + 1);
					if (i != 0)
						p = p.substring(p.indexOf("~") + 1);
					para[i] = p.substring(0, p.indexOf("~"));
				}
			}
			String str = "";

			for (i = 0; i < strlen; i++) {
				em_para[i] = getEmbeddedPara(id, type, para[i], login_userid,
						job_id, ds);
				str = "~" + para[i] + "~";
				pReplace = pReplace.replace(str, em_para[i]);
			}
		} catch (Exception e) {
			logger.error(
					"addParameter(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addParameter(String, String, String, String, String, DataSource) - *********************************"
						+ e);
			}
		}
		return pReplace;
	}

	public static String addParameter(String paragraph, String typeAndName,
			String type, String login_userid, String job_id, String poWoId,
			String stringOf42Char, DataSource ds) {
		int total = 0;
		int i = 0;
		String pReplace = paragraph;
		String p = paragraph;
		total = totalOccurance(paragraph);
		int strlen = (total / 2);
		String[] para = new String[strlen];
		String[] em_para = new String[strlen];
		try {
			for (i = 0; i < strlen; i++) {
				if (totalOccurance(p) > 1) {
					p = p.substring(p.indexOf("~") + 1);
					if (i != 0)
						p = p.substring(p.indexOf("~") + 1);
					para[i] = p.substring(0, p.indexOf("~"));
				}
			}
			String str = "";

			for (i = 0; i < strlen; i++) {
				str = "~" + para[i] + "~";
				if (para[i].equals("powoNumber")) {
					pReplace = pReplace.replace(str, poWoId);
				} else if (para[i].equals("addCodeHere")) {
					pReplace = pReplace.replace(str, stringOf42Char);
				} else {
					em_para[i] = getEmbeddedPara(typeAndName, type, para[i],
							login_userid, job_id, ds);

					pReplace = pReplace.replace(str, em_para[i]);
				}

			}
		} catch (Exception e) {
			logger.error(
					"addParameter(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addParameter(String, String, String, String, String, DataSource) - *********************************"
						+ e);
			}
		}
		return pReplace;
	}

	public static String addParameter(String paragraph, String poId,
			String authString) {
		int total = 0;
		int i = 0;
		String pReplace = paragraph;
		String p = paragraph;
		total = totalOccurance(paragraph);
		int strlen = (total / 2);
		String[] para = new String[strlen];
		String[] em_para = new String[strlen];
		try {
			for (i = 0; i < strlen; i++) {
				if (totalOccurance(p) > 1) {
					p = p.substring(p.indexOf("~") + 1);
					if (i != 0)
						p = p.substring(p.indexOf("~") + 1);
					para[i] = p.substring(0, p.indexOf("~"));
				}
			}
			String str = "";

			for (i = 0; i < strlen; i++) {
				if (para[i].equals("poid")) {
					em_para[i] = poId;
				}
				if (para[i].equals("authentication")) {
					em_para[i] = authString;
				}
				str = "~" + para[i] + "~";
				pReplace = pReplace.replace(str, em_para[i]);
			}
		} catch (Exception e) {
			logger.error(
					"addParameter(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addParameter(String, String, String, String, String, DataSource) - *********************************"
						+ e);
			}
		}
		return pReplace;
	}

	public static int totalOccurance(String paragraph) {
		int total = 0;
		String p = paragraph;
		if (p.indexOf("~") >= 0) {
			for (int i = 0; p.length() > 0; i++) {
				p = p.substring(p.indexOf("~") + 1);
				total++;
				if (p.indexOf("~") < 0)
					break;
			}
		}
		return total;
	}

	public static String getEmbeddedPara(String id, String type,
			String para_name, String login_userid, String job_id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String embedded_para = "";
		if (id == null || id.equals("")) {
			id = "0";
		}
		if (job_id == null || job_id.equals("")) {
			job_id = "0";
		}
		if (type.equals("NewNetMedX")) {
			type = "NetMedX";
		}

		try { /*
			 * System.out.println("embedded start");
			 * System.out.println("id:::::::"+id);
			 * System.out.println("type:::::::"+type);
			 * System.out.println("para_name:::::::"+para_name);
			 * System.out.println("login_userid:::::::"+login_userid);
			 * System.out.println("job_id:::::::"+job_id);
			 */

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call cc_embedded_para(?, ?, ?, ?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, id);
			cstmt.setString(3, type);
			cstmt.setString(4, para_name);
			cstmt.setString(5, login_userid);
			cstmt.setString(6, job_id);

			rs = cstmt.executeQuery();
			if (rs.next()) {
				embedded_para = rs.getString(1);
			} else
				embedded_para = para_name;
			if (embedded_para == null)
				embedded_para = para_name;
		} catch (Exception e) {
			logger.error(
					"getEmbeddedPara(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEmbeddedPara(String, String, String, String, String, DataSource) - Exception caught in rerieving Embedded Parameter"
						+ e);
			}
		}

		finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getEmbeddedPara(String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getEmbeddedPara(String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getEmbeddedPara(String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

		}

		return embedded_para;
	}

	public static String[] getValueofEmbaded(String paragraph) {

		int total = 0;
		int i = 0;
		String p = paragraph;
		total = totalOccurance(paragraph);
		int strlen = (total / 2);
		String[] para = new String[total];
		para = paragraph.split("~");
		return para;

	}

}
