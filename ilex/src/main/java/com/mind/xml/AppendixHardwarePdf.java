/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This class used for generating Hardware Appendix in pdf format.      
 *
 */

package com.mind.xml;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.mind.common.Decimalroundup;
import com.mind.dao.IM.IMdao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.xml.doc.Content;
import com.mind.ilex.xml.doc.Number;
import com.mind.ilex.xml.doc.Section;
import com.mind.ilex.xml.doc.Title;

/**
 * Methods : viewhardwareappendix
 */

public class AppendixHardwarePdf extends Domxmltaglist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AppendixHardwarePdf.class);

	/**
	 * This method return Byte Array for Hardware Appendix in pdf.
	 * 
	 * @param appendixId
	 *            String. appendix Id
	 * @param type
	 *            String. type msa/appendix
	 * @param fileName
	 *            String. xml file path
	 * @param imagePath
	 *            String. path of logo
	 * @param ds
	 *            Object of DataSource
	 * @return Byte Array This method returns byte array, for hardware appendix.
	 */

	public byte[] viewHardwareAppendix(String jobId, String type,
			String fileName, String imagePath, String fontPath,
			String loginUserId, DataSource ds) {

		Document maindocument = new Document(PageSize.A4, 1, 1, 40, 20);
		ByteArrayOutputStream outbuff = new ByteArrayOutputStream();

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String footertext = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Confidential & Proprietary \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t \u00a9 2009 Contingent Network Services, LLC \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Page ";
			PdfWriter writer = PdfWriter.getInstance(maindocument, outbuff);

			/* Set font array */
			BaseFont bfArialN = BaseFont.createFont(fontPath,
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

			Font[] fonts = new Font[7];
			fonts[0] = new Font(bfArialN, 8, Font.NORMAL);
			fonts[1] = new Font(bfArialN, 10.5f, Font.BOLD);
			fonts[2] = new Font(bfArialN, 7.5f, Font.NORMAL);
			fonts[3] = new Font(bfArialN, 8, Font.NORMAL);
			fonts[4] = new Font(Font.TIMES_ROMAN, 7, Font.NORMAL);
			fonts[5] = new Font(bfArialN, 8, Font.BOLD);
			fonts[6] = new Font(bfArialN, 8, Font.BOLD);

			/* get appendix id from job id */
			String appendixId = null;

			appendixId = IMdao.getAppendixId(jobId, ds);

			PdfPTable headertable = new PdfPTable(1);
			PdfPTable table = new PdfPTable(2);
			PdfPTable table1 = new PdfPTable(1);
			PdfPTable table2 = new PdfPTable(3);
			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			PdfPCell cell3 = new PdfPCell();
			PdfPCell cell4 = new PdfPCell();

			Image logo = Image.getInstance(imagePath);

			float aspectRatio = 381.0f / 148.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewHardwareAppendix(String, String, String, String, String, String, DataSource) - "
						+ aspectRatio);
			}
			float y = 122.39f;
			logo.setDpi(96, 96);
			logo.scaleAbsolute(aspectRatio * y, y);
			logo.setBorder(0);

			HeaderFooter footer = new HeaderFooter(new Phrase(footertext,
					fonts[4]), true);

			footer.setAlignment(Element.ALIGN_LEFT);
			footer.setBorder(Rectangle.NO_BORDER);
			maindocument.setFooter(footer);
			maindocument.open();

			PdfDestination pd = new PdfDestination(PdfDestination.XYZ, -1, -1,
					1);
			PdfAction action = PdfAction.gotoLocalPage(1, pd, writer);
			writer.setOpenAction(action);

			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();

			String xmlFile = EditXmldao.getXmlFile(fileName, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			Iterator it = list1.iterator();

			String str = "";
			String morespace = "";
			PdfPCell cellcontent, cellnumber;
			PdfPTable maintable1 = new PdfPTable(3);
			int i = 0;
			int t = 0;
			int tableSize = 0;
			boolean addtable = false;
			boolean addaddress = false;
			Title title = new Title();
			Number number = new Number();
			Content content = new Content();
			String titleText = "";
			String numberText = "";
			String contentText = "";
			String numberType = "";
			String titleType = "";
			String contentType = "";
			String Onbullet = "";
			String Title = "";
			float sub_total, est_ship, total;
			sub_total = 0;
			est_ship = 0;
			total = 0;
			while (it.hasNext()) {
				titleText = "";
				numberText = "";
				contentText = "";
				section = (Section) it.next();

				numberType = section.getNumber().getType();
				titleType = section.getTitle().getType();
				contentType = section.getContent().getType();

				title = section.getTitle();
				titleText = title.getContent();
				if (titleText.indexOf("~") >= 0)
					titleText = addParameter(titleText, appendixId, type,
							loginUserId, jobId, ds);
				number = section.getNumber();
				numberText = number.getContent();
				if (numberText.indexOf("~") >= 0)
					numberText = addParameter(numberText, appendixId, type,
							loginUserId, jobId, ds);
				// contentText = xt.getContent();
				content = section.getContent();
				contentText = content.getContent();
				if (contentText.indexOf("~") >= 0)
					contentText = addParameter(contentText, appendixId, type,
							loginUserId, jobId, ds);

				if (numberType.equals("mainheading")) {
					fonts[5] = fonts[6];
				}
				if (numberType.equals("heading")) {
					fonts[5] = fonts[2];
				}
				if (titleType.equals("address")) {
					cell1 = new PdfPCell(new Paragraph(
							fonts[5].getCalculatedLeading(2), numberText + " "
									+ titleText, fonts[5]));
					cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					numberText = "";
					titleText = "";
					cell1.setBorder(Rectangle.NO_BORDER);
					table1.addCell(cell1);

					cell3 = new PdfPCell(table1);
					cell3.setBorder(Rectangle.NO_BORDER);
					addaddress = true;
				} else if (addaddress) {
					logo.setAlignment(Image.MIDDLE);
					cell1 = new PdfPCell(logo);
					cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell1.setBorder(Rectangle.NO_BORDER);
					cell2 = new PdfPCell();
					cell2.setBorder(Rectangle.NO_BORDER);
					maintable1.addCell(cell3);
					maintable1.addCell(cell1);
					maintable1.addCell(cell2);
					addaddress = false;
				}
				if (titleType.equals("table")) {
					contentType = section.getContent().getType();
					numberType = section.getNumber().getType();
					tableSize = section.getTitle().getSize().intValue();
					if (t == 0) {
						table = new PdfPTable(tableSize);
					}

					int headerwidths[] = { 15, 15, 40, 15, 15 }; // percentage
					table.setWidths(headerwidths);

					Paragraph para = new Paragraph(titleText, fonts[5]);
					cell1 = new PdfPCell(para);
					cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell1.setPaddingLeft(5f);
					cell1.setPaddingRight(5f);
					table.addCell(cell1);

					addtable = true;
					t++;
				}

				else {
					if (addtable) {

						cstmt = conn
								.prepareCall("{?=call lx_hardware_appendix_document_data_01(?)}");
						cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
						cstmt.setString(2, jobId);
						rs = cstmt.executeQuery();
						while (rs.next()) {
							Paragraph para = new Paragraph(
									fonts[2].getCalculatedLeading(2),
									Decimalroundup.twodecimalplaces(
											rs.getFloat(2), 2), fonts[3]);
							cell1 = new PdfPCell(para);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							table.addCell(cell1);

							para = new Paragraph(
									fonts[2].getCalculatedLeading(2),
									rs.getString(3), fonts[3]);
							cell1 = new PdfPCell(para);
							cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
							table.addCell(cell1);

							para = new Paragraph(
									fonts[2].getCalculatedLeading(2),
									rs.getString(4), fonts[3]);
							cell1 = new PdfPCell(para);
							cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
							table.addCell(cell1);

							para = new Paragraph(
									fonts[2].getCalculatedLeading(2), "$"
											+ Decimalroundup.twodecimalplaces(
													rs.getFloat(5), 1),
									fonts[3]);
							cell1 = new PdfPCell(para);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							table.addCell(cell1);

							para = new Paragraph(
									fonts[2].getCalculatedLeading(2), "$"
											+ Decimalroundup.twodecimalplaces(
													rs.getFloat(6), 1),
									fonts[3]);
							cell1 = new PdfPCell(para);
							cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
							table.addCell(cell1);

							sub_total = sub_total + rs.getFloat(6);
						}

						cell2 = new PdfPCell(table);
						cell2.setColspan(3);
						maintable1.addCell(cell2);
						addtable = false;

						table = new PdfPTable(5);
						int headerwidths[] = { 15, 15, 40, 15, 15 }; // percentage
						table.setWidths(headerwidths);

						Paragraph para = new Paragraph("Sub Total", fonts[5]);
						cell1 = new PdfPCell(para);
						cell1.setColspan(4);
						cell1.setBorder(Rectangle.NO_BORDER);
						cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table.addCell(cell1);

						para = new Paragraph(fonts[2].getCalculatedLeading(2),
								"$"
										+ Decimalroundup.twodecimalplaces(
												sub_total, 1), fonts[3]);
						cell1 = new PdfPCell(para);
						cell1.setBorder(Rectangle.NO_BORDER);
						cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table.addCell(cell1);

						para = new Paragraph("Est. Ship", fonts[5]);
						cell1 = new PdfPCell(para);
						cell1.setColspan(4);
						cell1.setBorder(Rectangle.NO_BORDER);
						cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table.addCell(cell1);

						// para = new Paragraph(fonts[2].leading(2),
						// Decimalroundup.twodecimalplaces(est_ship, 2),
						// fonts[3]);
						para = new Paragraph(fonts[2].getCalculatedLeading(2),
								"see above", fonts[3]);
						cell1 = new PdfPCell(para);
						cell1.setBorder(Rectangle.NO_BORDER);
						cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table.addCell(cell1);

						para = new Paragraph("Total", fonts[5]);
						cell1 = new PdfPCell(para);
						cell1.setColspan(4);
						cell1.setBorder(Rectangle.NO_BORDER);
						cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table.addCell(cell1);

						para = new Paragraph(fonts[2].getCalculatedLeading(2),
								"$"
										+ Decimalroundup.twodecimalplaces(
												sub_total, 1), fonts[3]);
						cell1 = new PdfPCell(para);
						cell1.setBorder(Rectangle.NO_BORDER);
						cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table.addCell(cell1);

						cell2 = new PdfPCell(table);
						cell2.setColspan(3);
						cell2.setBorder(Rectangle.NO_BORDER);
						maintable1.addCell(cell2);
						addtable = false;
					}
					morespace = "";
					if (section.getNumber().getType().equals("notshow")) {
						titleText = "";
						numberText = "";
					} // for first paragraph "General"
					if (section.getTitle().getType().equals("morespace"))
						morespace = "\n"; // for more space
					if (numberText.equals(""))
						Title = titleText + morespace;
					else
						Title = numberText + titleText;

					Paragraph paranumber = new Paragraph(Title, fonts[5]);
					cellnumber = new PdfPCell(paranumber);
					if (titleType.equals("right"))
						cellnumber.setHorizontalAlignment(Element.ALIGN_RIGHT);
					else
						cellnumber
								.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
					cellnumber.setBorder(Rectangle.NO_BORDER);
					if (titleType.equals("mainheadingblank")) {
						Title = titleText + "\n";
						cellnumber.setFixedHeight(3f);
					} else {
						cellnumber.setFixedHeight(12f);
					}
					cellnumber.setColspan(3);

					Onbullet = "";
					if (section.getContent().getType().equals("bullet"))
						Onbullet = "\u2022 ";
					Paragraph paracontent = new Paragraph(Onbullet + ""
							+ contentText + "\n\n", fonts[2]);
					cellcontent = new PdfPCell(paracontent);
					cellcontent.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
					cellcontent.setBorder(Rectangle.NO_BORDER);
					cellcontent.setColspan(3);

					if (!Title.equals("")) {
						maintable1.addCell(cellnumber);
					}
					if (!contentText.equals("")) {
						maintable1.addCell(cellcontent);
					}
					maintable1.setWidthPercentage(90);
				}

			}

			maindocument.add(maintable1);
		}

		catch (Exception de) {
			logger.error(
					"viewHardwareAppendix(String, String, String, String, String, String, DataSource)",
					de);

			logger.error(
					"viewHardwareAppendix(String, String, String, String, String, String, DataSource)",
					de);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);

		}
		maindocument.close();
		return outbuff.toByteArray();
	}
}