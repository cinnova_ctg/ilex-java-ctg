//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : ViewActionServlet
//  @ Date : 12-June-2008
//  @ Author : Vishal Kapoor
//
//

package com.mind.xml;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.docm.Document;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DocMIlexDao;
import com.mind.common.dao.ViewList;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.DocumentIdNotFoundException;
import com.mind.docm.exceptions.DocumentNotExistsException;
import com.mind.util.WebUtil;

public class ViewAction extends com.mind.common.IlexAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ViewAction.class);

	// Use Case
	/**
	 * @name execute
	 * @purpose This method is used to get the PDF/RTF/Excel output from the
	 *          Ilex Corresponding to any Enitity.
	 * @steps 1. User requests to get the document in any format.
	 * @param ActionMapping
	 * @param ActionForm
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return ActionForward
	 * @exception Exception
	 * @returnDescription The jsp which will be visible to the user
	 */

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String temp_type = "";
		byte[] buffer = null;
		String Type = request.getParameter("Type") != null ? request
				.getParameter("Type") : "";
		String View = request.getParameter("View") != null ? request
				.getParameter("View") : "";
		String Id = request.getParameter("Id") != null ? request
				.getParameter("Id") : "0";
		String from_type = request.getParameter("from_type") != null ? request
				.getParameter("from_type") : "";
		String jobId = request.getParameter("jobId") != null ? request
				.getParameter("jobId") : "";
		String chkfiletype = EditXmldao.getChkXmlFile(Id, Type,
				getDataSource(request, "ilexnewDB"));
		boolean exceptionFlag = false;
		ServletOutputStream outStream = null;
		Document document = new Document();

		if (Type.equals("NetMedX") || Type.equals("Hardware"))
			temp_type = "Appendix";
		else
			temp_type = Type;

		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String userId = (String) session.getAttribute("userid");

		if (!Authenticationdao.getPageSecurity(userId, temp_type, "View "
				+ temp_type, getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}

		/* Page Security End */

		// Here check for the status

		if (Id.equals("")) {
			Id = "0";
		}

		if (DocMIlexDao.isEntityApproved(temp_type, Id, View, jobId,
				getDataSource(request, "ilexnewDB"))
				&& request.getParameter("temp") == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - From DocM");
			}

			try {
				if (temp_type.equals("Appendix") && jobId != "") {
					String jobType = DocMIlexDao.getjobType(jobId,
							getDataSource(request, "ilexnewDB"));
					if (logger.isDebugEnabled()) {
						logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - "
								+ jobType);
					}
					if (jobType.equalsIgnoreCase("Addendum")) {
						// buffer =
						// EntityManager.downloadEntityFile(jobId,"Addendum");
						document = EntityManager.getDocument(jobId, "Addendum");
					} else {
						// buffer =
						// EntityManager.downloadEntityFile(Id,"Appendix");
						document = EntityManager.getDocument(Id, "Appendix");
					}
				} else if (temp_type.equals("Appendix")
						&& (jobId == null || jobId.equals(""))) {
					document = EntityManager.getDocument(Id, "Appendix");
				} else if (temp_type.equals("MSA")) {
					// buffer = EntityManager.downloadEntityFile(Id,"MSA");
					document = EntityManager.getDocument(Id, "MSA");
				}
			} catch (DocumentIdNotFoundException e) {
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				exceptionFlag = true;
				request.setAttribute("DocumentIdNotFoundException",
						"Document Id Not Exists");
			} catch (CouldNotCheckDocumentException e) {
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				exceptionFlag = true;
				request.setAttribute("CouldNotCheckDocumentException",
						"CouldNotCheckDocumentException");
			} catch (DocumentNotExistsException e) {
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				exceptionFlag = true;
				request.setAttribute("DocumentNotExistsException",
						"DocumentNotExistsException");
			} catch (Exception e) {
				logger.error(
						"execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				exceptionFlag = true;
				request.setAttribute("Exception", "failed");
			}
			if (exceptionFlag) {
				request.setAttribute("Type", Type);
				request.setAttribute("View", View);
				request.setAttribute("Id", Id);
				request.setAttribute("from_type", from_type);
				request.setAttribute("jobId", jobId);

				return mapping.findForward("PdfNotGenerated");
			} else {
				outStream = response.getOutputStream();
				setResponseAttributesFromDocM(document, response);
				outStream.write(document.getFileBytes());
				outStream.close();
			}
		} else {
			// Object of the Servlet output stream used
			if (logger.isDebugEnabled()) {
				logger.debug("execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - From Ilex");
			}
			outStream = response.getOutputStream();
			if (Type.equals("Appendix") && View.equals("excel")) { // Special
																	// case for
																	// Appendix
																	// Excel
				HSSFWorkbook wb = null;
				String realPath = WebUtil.getRealPath(request, "/images");
				wb = getAppendixWorkBook(request, realPath, Type, Id, userId,
						from_type, getDataSource(request, "ilexnewDB"));
				response.setHeader(
						"Content-Disposition",
						"attachment;filename=AppendixStandard.xls;size="
								+ wb.getBytes().length + "");
				wb.write(outStream);
				outStream.close();
				return (mapping.findForward("success"));

				// }else if (Type.equals("MSA") && chkfiletype.equals("U")){
				// //Special case for MSA Uploaded file
				// Fileinfo fileinfo = Upload.getuploadedfiledata( Id , Type,
				// chkfiletype, getDataSource( request , "ilexnewDB" ) );
				// if( fileinfo.getFile_data() != null)
				// {
				// response.setContentType("application/x-msdownload");//msword
				// response.setHeader("Content-Disposition",
				// "attachment;filename=" +fileinfo.getFile_name() + ";size=" +
				// fileinfo.getFile_data().length + "");
				// outStream = response.getOutputStream();
				// outStream.write(fileinfo.getFile_data());
				// outStream.close();
				// }
			} else {
				buffer = getDocumentbytes(request, Id, Type, userId,
						getDataSource(request, "ilexnewDB")); // The method used
																// to get the
																// byte[]
				setResponseAttributes(Id, View, Type, buffer, response,
						getDataSource(request, "ilexnewDB"));
				outStream.write(buffer);
				outStream.close();
			}
		}

		// if(DocMIlexDao.isEntityApproved(temp_type,Id,View,jobId,getDataSource(request,"ilexnewDB"))){
		//
		// }
		return (null);
	}

	// Use Case
	/**
	 * @name getDocumentbytes
	 * @purpose This method is used to get the byte[] from the Ilex generated
	 *          PDF/RTF/HTML corresponding to any Entity. This whole class is
	 *          modified to be used from Ilex as well as DocM System. Some of
	 *          the Code is redundant for Special Condition for MSA (uploaded
	 *          documents) and Appendix Excel.
	 * @steps 1. User requests to see the PDF/RTF/Excel for any entity 2. This
	 *        method is initiated from IlexMenu Option ( View - >
	 *        PDF/RTF/Excel). 3. This method is also called from the
	 *        MenuFuncitonChangeStatusAction while interacting with DocM System.
	 * @param HttpServletRequest
	 * @param String
	 *            Id
	 * @param String
	 *            Type
	 * @param String
	 *            userId
	 * @param DataSource
	 * @return byte[]
	 * @returnDescription The byte[] correspponding to any entity within the
	 *                    Ilex.
	 */
	public static byte[] getDocumentbytes(HttpServletRequest request,
			String Id, String Type, String userId, DataSource ds) {

		String imagePath = WebUtil.getRealPath(request, "images")
				+ "/clogo.jpg";
		String fontPath = WebUtil.getRealPath(request, "images")
				+ "/ARIALN.TTF";

		String msaContPath = request.getRequestURL().toString();
		msaContPath = msaContPath.substring(0, msaContPath.lastIndexOf("/"));
		String htmlImagePath = msaContPath + "/images/clogo.jpg";
		// String contextPath = request.getContextPath();

		byte[] buffer = null;
		String jobId = "";
		/*
		 * Here values if not retrieved from the parameter objects then we go
		 * for a Attributes. Parameters will passes when method is used from
		 * Ilex but when used in context with DocM so we have use Attributes
		 */
		if (request.getParameter("jobId") != null)
			jobId = request.getParameter("jobId");
		if (jobId.equals(""))
			jobId = request.getAttribute("jobId") != null ? request
					.getAttribute("jobId").toString() : "0";

		String View = request.getParameter("View") != null ? request
				.getParameter("View") : "";
		if (View.equals(""))
			View = request.getAttribute("View") != null ? request.getAttribute(
					"View").toString() : "";

		String from_type = request.getParameter("from_type") != null ? request
				.getParameter("from_type") : "";
		if (from_type.equals(""))
			from_type = request.getAttribute("from_type") != null ? request
					.getAttribute("from_type").toString() : "";

		buffer = getInnerDocumentBytes(Id, jobId, Type, View, userId,
				from_type, imagePath, fontPath, htmlImagePath, ds, request);

		return buffer;
	}

	// Use Case
	/**
	 * @name getInnerDocumentBytes
	 * @purpose This method is used a used to get the byte[] from the Ilex
	 *          generated PDF/RTF/HTML corresponding to any Entity. This whole
	 *          class is modified to be used from Ilex as well as DocM System.
	 *          Some of the Code is redundant for Special Condition for MSA
	 *          (uploaded documents) and Appendix Excel.
	 * @steps 1. User requests to see the PDF/RTF/Excel for any entity 2. This
	 *        method is initiated from IlexMenu Option ( View - >
	 *        PDF/RTF/Excel). 3. This method is also called from the
	 *        MenuFuncitonChangeStatusAction while interacting with DocM System.
	 *        4. This method is also called for directly accessing the PDF for
	 *        any Entity (DocM Requirement).
	 * @param String
	 *            Id
	 * @param String
	 *            jobId
	 * @param String
	 *            Type
	 * @param String
	 *            View
	 * @param String
	 *            View
	 * @param String
	 *            userId
	 * @param String
	 *            from_type
	 * @param String
	 *            filePath
	 * @param String
	 *            imagePath
	 * @param String
	 *            temp_filepath
	 * @param String
	 *            fontPath
	 * @param String
	 *            htmlImagePath
	 * @param DataSource
	 * @return byte[]
	 * @returnDescription The byte[] correspponding to any entity within the
	 *                    Ilex.
	 */
	public static byte[] getInnerDocumentBytes(String Id, String jobId,
			String Type, String View, String userId, String from_type,
			String imagePath, String fontPath, String htmlImagePath,
			DataSource ds, HttpServletRequest request) {
		byte[] buffer = null;
		String chkfiletype = "";
		String nameNetmedx = null;

		try {
			chkfiletype = EditXmldao.getChkXmlFile(Id, Type, ds);
			if (Id != null && !Id.equals("0")) { // This block internally
													// manages Appendix as
													// "NetMedX/HardWare".
				if (Type.equals("Appendix")) {
					if (ViewList.getAppendixtypedesc(Id, ds).equals("NetMedX")) {
						Type = "NetMedX";
					} else if (ViewList.getAppendixtypedesc(Id, ds).equals(
							"Hardware")) {
						Type = "Hardware";
					}
				} else {
					nameNetmedx = EditXmldao.getName(Id, Type, ds);
					if (nameNetmedx != null)
						if (nameNetmedx.equalsIgnoreCase("NetMedX"))
							Type = "NetMedX"; // for netmedx
				}
			}
			String fileName;

			fileName = getFilePath(Type, Id, chkfiletype); // full
															// path
															// of
															// the
															// file
															// to be
															// used.

			/*
			 * This if else block check for the file to be viewed and make the
			 * objects of the corresponding class to be used
			 */

			if (View.equals("pdf")) { // if the file to be viewed is of type pdf
				if (Type.equals("NetMedX")) {
					if (ViewList.getIsNetMedXNew(Id)) {
						NewNetMedXPdf newNetMedX = new NewNetMedXPdf();
						buffer = newNetMedX.viewNetmedxPdf(Id, Type, fileName,
								imagePath, fontPath, userId, ds);
					} else {
						MSANetMedXPdf mn = new MSANetMedXPdf();
						buffer = mn.viewNetmedxPdf(Id, Type, fileName,
								imagePath, fontPath, userId, ds);
					}
				} else if (Type.equals("Hardware")) {
					if (logger.isDebugEnabled()) {
						logger.debug("getInnerDocumentBytes(String, String, String, String, String, String, String, String, String, String, String, DataSource) - In Generating HardWare PDF");
					}
					AppendixHardwarePdf ah = new AppendixHardwarePdf();
					buffer = ah.viewHardwareAppendix(jobId, Type, fileName,
							imagePath, fontPath, userId, ds);
				} else if (Type.equals("MSA")) {
					MSAStandardPdf ms = new MSAStandardPdf();
					buffer = ms.viewMsaPdf(Id, Type, fileName, imagePath,
							fontPath, userId, ds);
				} else if (Type.equals("Appendix")) {
					AppendixStandardPdf ap = new AppendixStandardPdf();
					buffer = ap.viewAppendixPdf(jobId, Type, from_type,
							fileName, imagePath, fontPath, userId, ds);
				}
			} else if (View.equals("rtf")) { // if the file to be viewed is of
												// type rtf
				if (Type.equals("NetMedX")) {
					MSANetMedXRtf mn = new MSANetMedXRtf();
					buffer = mn.viewNetmedxRtf(Id, Type, fileName, imagePath,
							fontPath, userId, ds);
				} else if (Type.equals("Hardware")) {
					AppendixHardwareRtf ah = new AppendixHardwareRtf();
					buffer = ah.viewHardwareAppendix(jobId, Type, fileName,
							imagePath, fontPath, userId, ds);
				} else if (Type.equals("MSA")) {
					MSAStandardRtf ms = new MSAStandardRtf();
					if (request == null) {
					buffer = ms.viewMsaRtf(Id, Type, fileName, imagePath,
								fontPath, userId, ds);
					} else {
						buffer = ms.viewMsaRtf(Id, Type, fileName, imagePath,
							fontPath, userId, ds, request);
					}
				} else if (Type.equals("Appendix")) {
					AppendixStandardRtf ar = new AppendixStandardRtf();
					buffer = ar.viewAppendixRtf(jobId, Type, from_type,
							fileName, imagePath, fontPath, userId, ds);
				}
			} else if (View.equals("html")) { // if the file to be viewed is of
												// type html,html code is there
												// but it never worked as before
												// changing
				if (Type.equals("NetMedX")) {
					if (ViewList.getIsNetMedXNew(Id)) {
						NewNetMedXPdf newNetMedX = new NewNetMedXPdf();
						buffer = newNetMedX.viewNetmedxPdf(Id, Type, fileName,
								imagePath, fontPath, userId, ds);
					} else {
						MSANetMedXPdf mn = new MSANetMedXPdf();
						buffer = mn.viewNetmedxPdf(Id, Type, fileName,
								imagePath, fontPath, userId, ds);
					}
				} else if (Type.equals("Hardware")) {
					AppendixHardwareHtml ah = new AppendixHardwareHtml();
					buffer = ah.viewHardwareAppendix(jobId, Type, fileName,
							htmlImagePath, fontPath, userId, ds);
				} else if (Type.equals("MSA")) {
					MSAStandardHtml ms = new MSAStandardHtml();
					buffer = ms.viewMsaNewHtml(Id, Type, fileName,
							htmlImagePath, fontPath, userId, ds);
				} else if (Type.equals("Appendix")) {
					AppendixStandardHtml ah = new AppendixStandardHtml();
					buffer = ah.viewAppendixHtml(jobId, Type, from_type,
							fileName, htmlImagePath, fontPath, userId, ds);
				}
			}
		} catch (Exception e) {
			logger.error(
					"getInnerDocumentBytes(String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);

			logger.error(
					"getInnerDocumentBytes(String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);
		}
		return buffer;
	}

	/**
	 * @name getInnerDocumentBytesForTransfer
	 * @purpose method is used to get the buffer of Pdf/rtf/html of different
	 *          entites. method is used for one time transfer of documents to
	 *          DocM
	 * @param Id
	 * @param jobId
	 * @param Type
	 * @param View
	 * @param userId
	 * @param from_type
	 * @param filePath
	 * @param imagePath
	 * @param temp_filepath
	 * @param fontPath
	 * @param htmlImagePath
	 * @param ds
	 * @return
	 */
	public static byte[] getInnerDocumentBytesForTransfer(String Id,
			String jobId, String Type, String View, String userId,
			String from_type, String imagePath, String fontPath,
			String htmlImagePath, DataSource ds) {
		byte[] buffer = null;
		// String chkfiletype = "";
		String nameNetmedx = null;

		try {
			// chkfiletype = EditXmldao.getChkXmlFile(Id, Type, ds);
			if (Id != null && !Id.equals("0")) { // This block internally
													// manages Appendix as
													// "NetMedX/HardWare".
				if (Type.equals("Appendix")) {
					if (ViewList.getAppendixtypedesc(Id, ds).equals("NetMedX")) {
						Type = "NetMedX";
					} else if (ViewList.getAppendixtypedesc(Id, ds).equals(
							"Hardware")) {
						Type = "Hardware";
					}
				} else {
					nameNetmedx = EditXmldao.getName(Id, Type, ds);
					if (nameNetmedx != null)
						if (nameNetmedx.equalsIgnoreCase("NetMedX"))
							Type = "NetMedX"; // for netmedx
				}
			}

			String fileName = getFilePath(Type, Id); // full path of the file
														// to be used.

			/*
			 * This if else block check for the file to be viewed and make the
			 * objects of the corresponding class to be used
			 */

			if (View.equals("pdf")) { // if the file to be viewed is of type pdf
				if (Type.equals("NetMedX")) {
					if (ViewList.getIsNetMedXNew(Id)) {
						NewNetMedXPdf newNetMedX = new NewNetMedXPdf();
						buffer = newNetMedX.viewNetmedxPdf(Id, Type, fileName,
								imagePath, fontPath, userId, ds);
					} else {
						MSANetMedXPdf mn = new MSANetMedXPdf();
						buffer = mn.viewNetmedxPdf(Id, Type, fileName,
								imagePath, fontPath, userId, ds);
					}
				} else if (Type.equals("Hardware")) {
					if (logger.isDebugEnabled()) {
						logger.debug("getInnerDocumentBytesForTransfer(String, String, String, String, String, String, String, String, String, String, String, DataSource) - In Generating HardWare PDF");
					}
					AppendixHardwarePdf ah = new AppendixHardwarePdf();
					buffer = ah.viewHardwareAppendix(jobId, Type, fileName,
							imagePath, fontPath, userId, ds);
				} else if (Type.equals("MSA")) {
					MSAStandardPdf ms = new MSAStandardPdf();
					buffer = ms.viewMsaPdf(Id, Type, fileName, imagePath,
							fontPath, userId, ds);
				} else if (Type.equals("Appendix")) {
					AppendixStandardPdf ap = new AppendixStandardPdf();
					buffer = ap.viewAppendixPdf(jobId, Type, from_type,
							fileName, imagePath, fontPath, userId, ds);
				}
			} else if (View.equals("rtf")) { // if the file to be viewed is of
												// type rtf
				if (Type.equals("NetMedX")) {
					MSANetMedXRtf mn = new MSANetMedXRtf();
					buffer = mn.viewNetmedxRtf(Id, Type, fileName, imagePath,
							fontPath, userId, ds);
				} else if (Type.equals("Hardware")) {
					AppendixHardwareRtf ah = new AppendixHardwareRtf();
					buffer = ah.viewHardwareAppendix(jobId, Type, fileName,
							imagePath, fontPath, userId, ds);
				} else if (Type.equals("MSA")) {
					MSAStandardRtf ms = new MSAStandardRtf();
					buffer = ms.viewMsaRtf(Id, Type, fileName, imagePath,
							fontPath, userId, ds);
				} else if (Type.equals("Appendix")) {
					AppendixStandardRtf ar = new AppendixStandardRtf();
					buffer = ar.viewAppendixRtf(jobId, Type, from_type,
							fileName, imagePath, fontPath, userId, ds);
				}
			} else if (View.equals("html")) { // if the file to be viewed is of
												// type html,html code is there
												// but it never worked as before
												// changing
				if (Type.equals("NetMedX")) {
					if (ViewList.getIsNetMedXNew(Id)) {
						NewNetMedXPdf newNetMedX = new NewNetMedXPdf();
						buffer = newNetMedX.viewNetmedxPdf(Id, Type, fileName,
								imagePath, fontPath, userId, ds);
					} else {
						MSANetMedXPdf mn = new MSANetMedXPdf();
						buffer = mn.viewNetmedxPdf(Id, Type, fileName,
								imagePath, fontPath, userId, ds);
					}
				} else if (Type.equals("Hardware")) {
					AppendixHardwareHtml ah = new AppendixHardwareHtml();
					buffer = ah.viewHardwareAppendix(jobId, Type, fileName,
							htmlImagePath, fontPath, userId, ds);
				} else if (Type.equals("MSA")) {
					MSAStandardHtml ms = new MSAStandardHtml();
					buffer = ms.viewMsaNewHtml(Id, Type, fileName,
							htmlImagePath, fontPath, userId, ds);
				} else if (Type.equals("Appendix")) {
					AppendixStandardHtml ah = new AppendixStandardHtml();
					buffer = ah.viewAppendixHtml(jobId, Type, from_type,
							fileName, htmlImagePath, fontPath, userId, ds);
				}
			}
		} catch (Exception e) {
			logger.error(
					"getInnerDocumentBytesForTransfer(String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);

			logger.error(
					"getInnerDocumentBytesForTransfer(String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);
		}
		return buffer;
	}

	// Use Case
	/**
	 * @name getFilePath
	 * @purpose This method is used to get the file path to generate the
	 *          document. FilePath corresponds to the IlexEntity XML file i.e (
	 *          MSAFinal.xml ) or MSA<ID>.xml
	 * @steps 1. This method is initaiated by getDocumentbytes method of
	 *        ViewAction Servlet.
	 * 
	 * @param String
	 *            Type
	 * @param String
	 *            Id
	 * @param String
	 *            filePath
	 * @param String
	 *            chkfiletype
	 * @return String
	 * @returnDescription The path of the XML file to be used for generating the
	 *                    document.
	 */

	public static String getFilePath(String Type, String Id, String chkfiletype) {
		String file_name = "";
		String filename = "";
		String addid = "";
		boolean chkfile = false;
		if (Type != null && !Type.equals("")) {

			if (Type.equals("NetMedX")) {
				if (ViewList.getIsNetMedXNew(Id))
					filename = Util
							.getKeyValue("netmedx.view.filename.fromproposal"); // get
																				// the
																				// filePath
																				// from
																				// the
																				// property
																				// file
				else
					filename = Util.getKeyValue("netmedx.view.filename"); // get
																			// the
																			// filePath
																			// from
																			// the
																			// property
																			// file
			} else if (Type.equals("Hardware"))
				filename = Util.getKeyValue("hardware.view.filename");
			else if (Type.equals("MSA"))
				filename = Util.getKeyValue("msa.view.filename");
			else if (Type.equals("Appendix"))
				filename = Util.getKeyValue("appendix.view.filename");

			if (chkfiletype != null && chkfiletype.equals("X"))
				chkfile = true; /*
								 * This block of code check for the modified xml
								 * file option for the entity
								 */
			if (chkfile) {
				if (Id.equals("0"))
					addid = "";
				else
					addid = Id;
				file_name = filename + "" + addid;
			} else
				file_name = filename;

		}
		return file_name;
	}

	/**
	 * @name getFilePath
	 * @purpose This method is used to get the xml file name for the different
	 *          entities. The condition to check for modified xml is removed
	 *          from this as of DocM requirement to do the one time transfer.
	 * @param Type
	 * @param Id
	 * @param filePath
	 * @return
	 */
	public static String getFilePath(String Type, String Id) {
		String file_name = "";
		String filename = "";
		if (Type != null && !Type.equals("")) {

			if (Type.equals("NetMedX")) {
				if (ViewList.getIsNetMedXNew(Id))
					filename = Util
							.getKeyValue("netmedx.view.filename.fromproposal"); // get
																				// the
																				// filePath
																				// from
																				// the
																				// property
																				// file
				else
					filename = Util.getKeyValue("netmedx.view.filename"); // get
																			// the
																			// filePath
																			// from
																			// the
																			// property
																			// file
			} else if (Type.equals("Hardware"))
				filename = Util.getKeyValue("hardware.view.filename");
			else if (Type.equals("MSA"))
				filename = Util.getKeyValue("msa.view.filename");
			else if (Type.equals("Appendix"))
				filename = Util.getKeyValue("appendix.view.filename");

			file_name = filename;

		}
		return file_name;
	}

	// Use Case
	/**
	 * @name getGeneratedFileName
	 * @purpose This method is used to get the file name to be displayed to the
	 *          user while downloading the document.
	 * @steps 1. This method is initaiated by getDocumentbytes method of
	 *        ViewAction Servlet.
	 * 
	 * @param String
	 *            Type
	 * @param String
	 *            View
	 * @return String
	 * @returnDescription The filename to be displayed to the user i.e
	 *                    (MSAStandard.pdf).
	 */

	public static String getGeneratedFileName(String Id, String Type,
			String View, DataSource ds) {
		StringBuffer filename = new StringBuffer();
		if (View.equals("pdf")) {
			filename = filename.append(getFileName(Id, Type, ds))
					.append(".pdf"); // append the extension to the filenames
		} else if (View.equals("rtf")) {
			filename = filename.append(getFileName(Id, Type, ds))
					.append(".rtf"); // append the extension to the filenames
		} else if (View.equals("html")) {
			filename = filename.append(getFileName(Id, Type, ds)).append(
					".html"); // append the extension to the filenames
		} else if (View.equals("excel")) {
			filename = filename.append(getFileName(Id, Type, ds))
					.append(".xls"); // append the extension to the filenames
		}
		return filename.toString();
	}

	// Use Case
	/**
	 * @name getFileName
	 * @purpose This method is used to get the file name to be displayed to the
	 *          user while downloading the document.
	 * @steps 1. This method is initaiated by getDocumentbytes method of
	 *        ViewAction Servlet.
	 * 
	 * @param String
	 *            Type
	 * @return String
	 * @returnDescription The filename to be displayed to the user i.e
	 *                    (MSAStandard.pdf).
	 */
	public static String getFileName(String Id, String Type, DataSource ds) {
		String filename = "";
		if (Type.equals("Appendix")) {
			if (ViewList.getAppendixtypedesc(Id, ds).equals("NetMedX")) {
				Type = "NetMedX";
			} else if (ViewList.getAppendixtypedesc(Id, ds).equals("Hardware")) {
				Type = "Hardware";
			}
		}
		if (Type.equals("Hardware")) {
			filename = Util.getKeyValue("hardware.generated.filename"); // get
																		// the
																		// filename
																		// fromm
																		// the
																		// property
																		// file.
		} else if (Type.equals("MSA")) {
			filename = Util.getKeyValue("msa.generated.filename");
		} else if (Type.equals("Appendix")) {
			filename = Util.getKeyValue("appendix.generated.filename");
		} else if (Type.equals("NetMedX")) {
			filename = Util.getKeyValue("netmedx.generated.filename"); // get
																		// the
																		// filename
																		// fromm
																		// the
																		// property
																		// file.
		}
		return filename;
	}

	// Use Case
	/**
	 * @name getAppendixWorkBook
	 * @purpose This method is used to get the workbook object for the Appendix
	 *          generated excel file.
	 * @steps 1. This is the redundant method as wrokbook object directly writes
	 *        to the output stream instead of writing the byte[].
	 * 
	 * @param HttpServletRequest
	 * @param String
	 *            Type
	 * @param String
	 *            Id
	 * @param String
	 *            userId
	 * @param String
	 *            from_type
	 * @param DataSource
	 *            ds
	 * 
	 * @return HSSFWorkbook
	 * @returnDescription The workbook object generated for the
	 *                    AppendixStandard.xls file.
	 */
	public static HSSFWorkbook getAppendixWorkBook(HttpServletRequest request,
			String realPath, String Type, String Id, String userId,
			String from_type, DataSource ds) {

		String imagePath = realPath + "/clogo.jpg";
		String fontPath = realPath + "/ARIALN.TTF";
		String jobId = request.getParameter("jobId") != null ? request
				.getParameter("jobId") : "0";
		String nameNetmedx = null;
		HSSFWorkbook wb = null;
		try {
			String chkfiletype = EditXmldao.getChkXmlFile(Id, Type, ds);
			if (Id != null && !Id.equals("0")) {
				if (Type.equals("Appendix")) {
					if (ViewList.getAppendixtypedesc(Id, ds).equals("NetMedX")) {
						Type = "NetMedX";
					} else if (ViewList.getAppendixtypedesc(Id, ds).equals(
							"Hardware")) {
						Type = "Hardware";
					}
				} else {
					nameNetmedx = EditXmldao.getName(Id, Type, ds);
					if (nameNetmedx != null)
						if (nameNetmedx.equalsIgnoreCase("NetMedX"))
							Type = "NetMedX"; // for netmedx
				}
			}

			AppendixStandardExcel aE = new AppendixStandardExcel();
			wb = aE.viewAppendixExcel(jobId, Type, from_type,
					getFilePath(Type, Id, chkfiletype), imagePath, fontPath,
					userId, ds);
		} catch (Exception e) {
			logger.error(
					"getAppendixWorkBook(HttpServletRequest, String, String, String, String, DataSource)",
					e);

			logger.error(
					"getAppendixWorkBook(HttpServletRequest, String, String, String, String, DataSource)",
					e);
		}
		return wb;
	}

	public static void setResponseAttributes(String Id, String View,
			String Type, byte[] buffer, HttpServletResponse response,
			DataSource ds) {
		if (View.equals("pdf")) {
			response.setContentType("application/pdf"); // set the content type
		} else if (View.equals("rtf")) {
			response.setContentType("application/rtf");
		} else if (View.equals("html")) {
			response.setContentType("application/pdf");
		} else if (View.equals("excel")) {
			response.setContentType("application/Excel");
		}
		if (buffer == null) {
			response.setHeader("Content-Disposition",
					"attachment;filename= XML NOT Found FOR this File"
							+ ";size= 5" + "");
		} else {
			response.setHeader("Content-Disposition", "attachment;filename="
					+ getGeneratedFileName(Id, Type, View, ds) + ";size="
					+ buffer.length + "");
		}
	}

	public static void setResponseAttributesFromDocM(Document document,
			HttpServletResponse response) {
		response.setContentType("application/pdf"); // set the content type
		response.setHeader("Content-Disposition", "attachment;filename="
				+ document.getFileName() + ";size="
				+ document.getFileBytes().length + "");
	}

}
