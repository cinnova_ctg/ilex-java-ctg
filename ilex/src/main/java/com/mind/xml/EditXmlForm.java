package com.mind.xml;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.mind.ilex.xml.doc.Document;

public class EditXmlForm extends ActionForm {

	private String paragraph_identifier = null;
	private String[] paragraph_number = null;
	private String[] paragraph_title = null;
	private String[] paragraph_content = null;
	private String save = null;
	private String cancel = null;
	private String view = null;
	private boolean refresh = false;
	private String filename1 = null;
	private String xmlfile = null;
	private String typeid = null;
	private String type = null;
	private String edittype = null;
	private String name = null;
	private String type_name = null;
	private int paragraph_length = 0;
	private String MSA_Id = null;
	private String msaname = null;
	private String appendixId = null;
	private String id = null;
	private String tempType = null;

	// New UI Changes Related to MSA ViewSelector and ViewImages.

	private String monthWeekCheck = null;
	private String[] selectedStatus = null;
	private String[] selectedOwners = null;
	private String otherCheck = null;
	private String prevSelectedStatus = null;
	private String prevSelectedOwner = null;
	private String go = null;
	private String home = null;

	// New UI Changes Related to Appendix ViewSelector and ViewImages.
	private String[] appendixSelectedStatus = null;
	private String[] appendixSelectedOwners = null;
	private String appendixOtherCheck = null;

	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}

	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}

	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}

	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}

	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}

	public String getAppendixSelectedOwners(int i) {
		return appendixSelectedOwners[i];
	}

	public String getAppendixSelectedStatus(int i) {
		return appendixSelectedStatus[i];
	}

	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	public String getOtherCheck() {
		return otherCheck;
	}

	public void setOtherCheck(String otherCheck) {
		this.otherCheck = otherCheck;
	}

	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	public String[] getSelectedOwners() {
		return selectedOwners;
	}

	public void setSelectedOwners(String[] selectedOwners) {
		this.selectedOwners = selectedOwners;
	}

	public String getSelectedStatus(int i) {
		return selectedStatus[i];
	}

	public String getSelectedOwners(int i) {
		return selectedOwners[i];
	}

	public String[] getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public String getTempType() {
		return tempType;
	}

	public void setTempType(String tempType) {
		this.tempType = tempType;
	}

	public String getMSA_Id() {
		return MSA_Id;
	}

	public void setMSA_Id(String id) {
		MSA_Id = id;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getParagraph_identifier() {
		return paragraph_identifier;
	}

	public void setParagraph_identifier(String paragraph_identifier) {
		this.paragraph_identifier = paragraph_identifier;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String[] getParagraph_content() {
		return paragraph_content;
	}

	public void setParagraph_content(String[] paragraph_content) {
		this.paragraph_content = paragraph_content;
	}

	public String[] getParagraph_number() {
		return paragraph_number;
	}

	public void setParagraph_number(String[] paragraph_number) {
		this.paragraph_number = paragraph_number;
	}

	public String[] getParagraph_title() {
		return paragraph_title;
	}

	public void setParagraph_title(String[] paragraph_title) {
		this.paragraph_title = paragraph_title;
	}

	public boolean isRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public String getXmlfile() {
		return xmlfile;
	}

	public void setXmlfile(String xmlfile) {
		this.xmlfile = xmlfile;
	}

	public int getParagraph_length() {
		return paragraph_length;
	}

	public void setParagraph_length(int paragraph_length) {
		this.paragraph_length = paragraph_length;
	}

	public String getTypeid() {
		return typeid;
	}

	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public String getEdittype() {
		return edittype;
	}

	public void setEdittype(String edittype) {
		this.edittype = edittype;
	}

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getFilename1() {
		return filename1;
	}

	public void setFilename1(String filename1) {
		this.filename1 = filename1;
	}

	private Document document = new Document();

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			this.document = (Document) session.getAttribute("xmlDocument");
		} else {
			this.document = new Document();
		}

	}
}
