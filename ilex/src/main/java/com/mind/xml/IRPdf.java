/**
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This class used for generating Invoice Request in pdf format.      
*
*/

package com.mind.xml;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.mind.common.Decimalroundup;

/**
* Methods : viewIR
*/

public class IRPdf 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IRPdf.class);

/** This method return Byte Array for Invoice Request in pdf. 
* @param jobid   			   	   String. Job Ids
* @param imagepath   		   	   String. path of logo
* @param ds   			       	   Object of DataSource
* @return Byte Array               This method returns byte array, for standard msa in pdf.
*/
	
	public byte[] viewIR (String jobid, String imagepath, DataSource ds) {
		 Rectangle doc_pagesize = PageSize.A4;
		 doc_pagesize = PageSize.LETTER.rotate();
		 Document maindocument = new Document(doc_pagesize, 10, 10, 10, 10);
		 ByteArrayOutputStream outbuff = new ByteArrayOutputStream();
		 
		 Font[] fonts = new Font[9];
		 	fonts[0] = new Font(Font.HELVETICA, 8, Font.BOLD);
			fonts[1] = new Font(Font.HELVETICA, 8, Font.NORMAL);
			fonts[2] = new Font(Font.HELVETICA, 7, Font.NORMAL);
			fonts[3] = new Font(Font.HELVETICA, 7, Font.BOLD);
			fonts[4] = new Font(Font.HELVETICA, 7, Font.BOLD);
			fonts[5] = new Font(Font.HELVETICA, 13, Font.BOLD);
			
			Connection conn = null;
			Connection con = null;
			ResultSet rs = null;
			CallableStatement cstmt = null;
			
			try {
			PdfWriter writer = PdfWriter.getInstance(maindocument, outbuff);
			
			PdfPTable maintable = new PdfPTable(7);
			maintable.setWidthPercentage(100);
			PdfPTable table = new PdfPTable(3);
			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			Paragraph para = new Paragraph();
			int L, R, C;
			L = Element.ALIGN_LEFT;
			R = Element.ALIGN_RIGHT;
			C = Element.ALIGN_CENTER;
			Color gray = new Color(0xC0, 0xC0, 0xC0);	
			int x = 0;
			maindocument.open();
			
			PdfDestination pd = new PdfDestination(PdfDestination.XYZ, -1, -1, 1);
			PdfAction action = PdfAction.gotoLocalPage(1, pd, writer);
			writer.setOpenAction(action);
			
			maindocument.setMargins(10, 10, .5f, .5f);
			String[] GeneralInfo =	{ "General Information - Must Be Completed", "CHECK ONE INVOICE IMMEDIATELY", "FOR ACCRUAL", "TARGET INVOICE DATE", "INVOICE REQUEST FORM" };										
			int headerGeneralInfowidths[] = { 22, 23, 13, 19, 23 };
			String[] CustInfo =	{"Your Name", "Customer Name", "Date", "4-Digit Project ID*", "Customer PO#" };	
			String[] ShipInfo =	{"Shipping - Complete Only As Needed", "Ship Attn To:", "Ship To: Address1", "Ship To: Address2", "Ship To: City, State Zip"}; 						
			String[] CardInfo =	{"Credit Card - Complete for Customer Credit Card Purchases Only", "Name on Card", "Credit Card Type", 	"Security ID", 	"Card Number", "Expiration", "Approval Number", "Date Run"};						
			String[] InvoiceInfo =	{"CNS Invoice (Sell-Side) - Must Be Completed", "Item Number (from Item List)", "Description (From Item List)", "Explanation as Needed (use additional sheets as necessary)", "Qty", "Unit Price", "Date of Request", "Date Completed", "Time Completed", "Customer Requestor", "Site Name/#", "Site City/State", "Freight Out Charge", "Taxable", "Drop Ship", "Electronic", "Cabling", "Warranty", "Sub Total", "Cur. Month Accrual Amt" }; 
			int headerInvoiceInfowidths[] = { 5, 9, 15, 4, 5, 5, 5, 5, 5, 5, 5, 5, 3, 3, 3, 3, 3, 7, 5 }; // percentage
			String[] PurchaseInfo =	{"CNS Purchase (Buy-Side) - Must Be Completed", "Item Number (from Item List)", "Vendor Name",	"Qty", "Unit Cost",	"CNS PO#", "Date of CNS PO", "Freight In Cost",	"Freight Out Cost",	"Ties to # Above", "Credit Card Purchase?",	"Sub Total" };
			int headerPurchaseInfowidths[] = { 12, 25, 7, 7, 7, 7, 7, 7, 7, 7, 7 }; // percentage		
			String[] BillingInfo =	{"Billing Contact: Complete for New Accounts or Changes Only", "Invoice Attn To:", "Bill To: Address1", "Bill To: City", "Bill To: State", "Bill To: Zip Code" }; 									
			String[] SalesInfo =	{"Sales: Complete for New Accounts or Changes Only", "Salesperson ID", "Sales Terms" };									

			int i = 0;
			/* fetching data from database start*/
			String[] CustInfo_data = new String[CustInfo.length]; 
			
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call li_ir_data(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobid);
			cstmt.setString(3, "general");
			rs  = cstmt.executeQuery();
			if(rs.next()){
				CustInfo_data[0] = rs.getString(1); 	// your_name
				CustInfo_data[1] = rs.getString(2); 	// cust_name
				CustInfo_data[2] = rs.getString(3);		// date
				CustInfo_data[3] = rs.getString(4);		// project id
				CustInfo_data[4] = rs.getString(5);		// cust PO	
			}
		
			if ( conn != null)
			{ 
				conn.close();
			}	
			/* fetching data from database end*/
			
			// Zeroth Row 
			table = new PdfPTable(GeneralInfo.length);
			table.setWidths(headerGeneralInfowidths);
			table.setWidthPercentage(100);
			for (x = 0; x < GeneralInfo.length-1; x++) {
				cell1 = new PdfPCell(new Paragraph(GeneralInfo[x], fonts[0]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
			}
			
			cell1 = new PdfPCell(new Paragraph(GeneralInfo[4], fonts[5]));
			cell1.setHorizontalAlignment(R);
			table.addCell(cell1);
			
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(7);
			cell2.setBorder(0);
			maintable.addCell(cell2);
			
			//first row
			table = new PdfPTable(2);
			table.setWidthPercentage(100);
			for (x = 0; x < CustInfo.length; x++) {
				cell1 = new PdfPCell(new Paragraph(CustInfo[x], fonts[2]));
				cell1.setHorizontalAlignment(R);
				cell1.setBackgroundColor(gray);
				table.addCell(cell1);

				cell1 = new PdfPCell(new Paragraph(CustInfo_data[x], fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
			}
			
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			cell2.setHorizontalAlignment(L);
			cell2.setBorder(0);
			maintable.addCell(cell2);
			
			
			table = new PdfPTable(2);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(ShipInfo[0], fonts[0]));
			cell1.setHorizontalAlignment(L);
			cell1.setColspan(2);
			table.addCell(cell1);
			
			for (x = 1; x < ShipInfo.length; x++) {
				cell1 = new PdfPCell(new Paragraph(ShipInfo[x], fonts[2]));
				cell1.setHorizontalAlignment(L);
				cell1.setBackgroundColor(gray);
				table.addCell(cell1);

				cell1 = new PdfPCell(new Paragraph("", fonts[2]));
				cell1.setHorizontalAlignment(R);
				table.addCell(cell1);
			}
			
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			cell2.setHorizontalAlignment(C);
			cell2.setBorder(0);
			maintable.addCell(cell2);
			
			
			table = new PdfPTable(4);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(CardInfo[0], fonts[0]));
			cell1.setHorizontalAlignment(L);
			cell1.setColspan(4);
			table.addCell(cell1);
			
			cell1 = new PdfPCell(new Paragraph(CardInfo[1], fonts[2]));
			cell1.setHorizontalAlignment(L);
			cell1.setBackgroundColor(gray);
			table.addCell(cell1);
			
			cell1 = new PdfPCell(new Paragraph("", fonts[2]));
			cell1.setHorizontalAlignment(L);
			cell1.setColspan(3);
			table.addCell(cell1);
			
			for (x = 2; x < CardInfo.length; x+=2) {
				cell1 = new PdfPCell(new Paragraph(CardInfo[x], fonts[2]));
				cell1.setHorizontalAlignment(L);	
				cell1.setBackgroundColor(gray);
				table.addCell(cell1);
				
				table.getDefaultCell().setHorizontalAlignment(L);	
				table.addCell(new Paragraph("", fonts[3]));
				
				cell1 = new PdfPCell(new Paragraph(CardInfo[x+1], fonts[2]));
				cell1.setHorizontalAlignment(L);	
				cell1.setBackgroundColor(gray);
				table.addCell(cell1);
				
				table.getDefaultCell().setHorizontalAlignment(L);	
				table.addCell(new Paragraph("", fonts[3]));
			}
			
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(3);
			cell2.setHorizontalAlignment(R);
			cell2.setBorder(0);
			maintable.addCell(cell2);
			
			
			// Third row
			table = new PdfPTable(InvoiceInfo.length-1);
			table.setWidths(headerInvoiceInfowidths);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(InvoiceInfo[0], fonts[0]));
			cell1.setHorizontalAlignment(L);
			cell1.setColspan(InvoiceInfo.length);
			table.addCell(cell1);
			
			
			for (x = 1; x < InvoiceInfo.length; x++) {
				cell1 = new PdfPCell(new Paragraph(InvoiceInfo[x], fonts[2]));
				cell1.setHorizontalAlignment(L);
				cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
				cell1.setBackgroundColor(gray);
				table.addCell(cell1);
			}
			
		/*	for (int i = 0; i <= 4 ; i++) {
				for (x = 0; x <= InvoiceInfo.length; x++) {
					cell1 = new PdfPCell(new Paragraph("a", fonts[2]));
					cell1.setHorizontalAlignment(L);
					cell1.setRunDirection(2);
					table.addCell(cell1);
				}
			}
		*/	
			i = 0;
			float sub_total = 0;
			float curr_mth_total = 0;
			con = ds.getConnection();
			cstmt = con.prepareCall("{?=call li_ir_data(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobid);
			cstmt.setString(3, "sale");
			rs  = cstmt.executeQuery();
			while(rs.next()){
				//if(i == InvoiceInfo.length-1) i = 0;
				i++;
				cell1 = new PdfPCell(new Paragraph(rs.getString(1), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(2), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(3), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(4), fonts[2]));
				cell1.setHorizontalAlignment(R);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(Decimalroundup.twodecimalplaces(rs.getFloat(5), 2), fonts[2]));
				cell1.setHorizontalAlignment(R);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(6), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(7), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(8), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(9), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(10), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(11), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(12), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(13), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(14), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(15), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(16), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(17), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph("$"+Decimalroundup.twodecimalplaces(rs.getFloat(18), 2), fonts[2]));
				cell1.setHorizontalAlignment(R);
				table.addCell(cell1);
				
				cell1 = new PdfPCell(new Paragraph(rs.getString(19), fonts[2]));
				cell1.setHorizontalAlignment(L);
				table.addCell(cell1);
				
				if(rs.getString(18) != null) sub_total = sub_total + Float.parseFloat(rs.getString(18));
				//if(rs.getString(19) != null) curr_mth_total = curr_mth_total + Float.parseFloat(rs.getString(19));
			}
		
			if ( con != null)
			{ 
				con.close();
			}	

			cell1 = new PdfPCell(new Paragraph("Page Total", fonts[0]));
			cell1.setHorizontalAlignment(R);
			cell1.setColspan(17);
			table.addCell(cell1);
			
			cell1 = new PdfPCell(new Paragraph("$"+Decimalroundup.twodecimalplaces(sub_total, 2), fonts[0]));
			cell1.setHorizontalAlignment(R);
			table.addCell(cell1);
			
			cell1 = new PdfPCell(new Paragraph("", fonts[0]));
			cell1.setHorizontalAlignment(R);
			table.addCell(cell1);
			
			
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setHorizontalAlignment(R);
			cell2.setColspan(7);
			cell2.setBorder(0);
			maintable.addCell(cell2);
			
			
//			Fourth row
			table = new PdfPTable(PurchaseInfo.length-1);
			table.setWidths(headerPurchaseInfowidths);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(PurchaseInfo[0], fonts[0]));
			cell1.setHorizontalAlignment(L);
			cell1.setColspan(PurchaseInfo.length);
			table.addCell(cell1);
			
			
			for (x = 1; x < PurchaseInfo.length; x++) {
				cell1 = new PdfPCell(new Paragraph(PurchaseInfo[x], fonts[2]));
				cell1.setHorizontalAlignment(L);
				cell1.setBackgroundColor(gray);
				table.addCell(cell1);
			}
			
			for (i = 0; i <= 4 ; i++) {
				for (x = 0; x <= PurchaseInfo.length; x++) {
					cell1 = new PdfPCell(new Paragraph("    ", fonts[2]));
					cell1.setHorizontalAlignment(L);
					cell1.setRunDirection(2);
					table.addCell(cell1);
				}
			}
			
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setHorizontalAlignment(L);
			cell2.setColspan(5);
			cell2.setBorder(0);
			maintable.addCell(cell2);
			
			table = new PdfPTable(2);
			table.setWidthPercentage(100);
			cell1 = new PdfPCell(new Paragraph(BillingInfo[0], fonts[3]));
			cell1.setHorizontalAlignment(L);
			cell1.setColspan(2);
			table.addCell(cell1);
			
			for (x = 1; x < BillingInfo.length; x++) {
				cell1 = new PdfPCell(new Paragraph(BillingInfo[x], fonts[2]));
				cell1.setHorizontalAlignment(L);
				cell1.setBackgroundColor(gray);
				table.addCell(cell1);

				cell1 = new PdfPCell(new Paragraph("", fonts[2]));
				cell1.setHorizontalAlignment(R);
				table.addCell(cell1);
			}
			
			cell1 = new PdfPCell(new Paragraph(SalesInfo[0], fonts[3]));
			cell1.setHorizontalAlignment(L);
			cell1.setColspan(2);
			table.addCell(cell1);
			
			for (x = 1; x < SalesInfo.length; x++) {
				cell1 = new PdfPCell(new Paragraph(SalesInfo[x], fonts[2]));
				cell1.setHorizontalAlignment(L);
				cell1.setBackgroundColor(gray);
				table.addCell(cell1);

				cell1 = new PdfPCell(new Paragraph("", fonts[2]));
				cell1.setHorizontalAlignment(R);
				table.addCell(cell1);
			}
			
			Image logo = Image.getInstance(imagepath);
			
			float aspectRatio = 177.0f/111.0f;
			if (logger.isDebugEnabled()) {
				logger.debug("viewIR(String, String, DataSource) - " + aspectRatio);
			}
			float y = 72.39f;
			logo.setDpi(96,96);
			logo.scaleAbsolute(aspectRatio*y, y);
			logo.setBorder(0);
			
			cell1 = new PdfPCell(logo);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setBorder(0);
			cell1.setColspan(2);
			table.addCell(cell1);
			
			cell2 = new PdfPCell();
			cell2.addElement(table);
			cell2.setColspan(2);
			cell2.setHorizontalAlignment(L);
			cell2.setBorder(0);
			maintable.addCell(cell2);
			
			
			
			maindocument.add(maintable);
	}
	 
		  	catch (Exception de) {
			logger.error("viewIR(String, String, DataSource)", de);

			logger.error("viewIR(String, String, DataSource)", de);
			}
			
			
			finally
			{
				try
				{
					if ( rs != null )
					{
						rs.close();
					}	
				}
				catch( Exception e )
				{
				logger.error("viewIR(String, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewIR(String, String, DataSource) - Error occured during closing Result set " + e);
				}
				}
				
				
				try
				{
					if ( cstmt != null )
					{
						cstmt.close();
					}	
				}
				catch( Exception e )
				{
				logger.error("viewIR(String, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewIR(String, String, DataSource) - Error occured during closing Statement " + e);
				}
				}
				
				
				try
				{
					if ( conn != null)
					{ 
						conn.close();
					}	
				}
				catch( Exception e )
				{
				logger.error("viewIR(String, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewIR(String, String, DataSource) - Error occured during closing Connection conn " + e);
				}
				}
				
				try
				{
					if ( con != null)
					{ 
						con.close();
					}	
				}
				catch( Exception e )
				{
				logger.error("viewIR(String, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("viewIR(String, String, DataSource) - Error occured during closing Connection con" + e);
				}
				}
				
			}
				maindocument.close();
				return outbuff.toByteArray();
		}
}
