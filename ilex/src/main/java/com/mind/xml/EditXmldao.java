package com.mind.xml;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import javax.sql.DataSource;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.xml.doc.Section;

public class EditXmldao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EditXmldao.class);

	public static String removeParameter(String paragraph) {
		int total = 0;
		int i = 0;
		String pReplace = paragraph;
		String p = paragraph;
		total = Domxmltaglist.totalOccurance(paragraph);
		int strlen = (total / 2);
		String[] para = new String[strlen];
		try {
			for (i = 0; i < strlen; i++) {
				if (Domxmltaglist.totalOccurance(p) > 1) {
					p = p.substring(p.indexOf("~") + 1);
					if (i != 0)
						p = p.substring(p.indexOf("~") + 1);
					para[i] = p.substring(0, p.indexOf("~"));
				}
			}
			String str = "";

			for (i = 0; i < strlen; i++) {
				str = "~" + para[i] + "~";
				pReplace = pReplace.replace(str, "");
			}
		} catch (Exception e) {
			logger.error("removeParameter(String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("removeParameter(String) - *********************************"
						+ e);
			}
		}
		return pReplace;
	}

	public static java.util.ArrayList getParagraphDetail(String filename,
			String Id, DataSource ds) {
		ArrayList EXList = new ArrayList();
		ParagraphDetail pdfn = null;

		try {
			Unmarshaller unmarshaller = XMLUtil.getDocumentUnmarshaller();
			String xmlFile = EditXmldao.getXmlFile(filename, ds);

			final StringReader xmlReader = new StringReader(xmlFile);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			com.mind.ilex.xml.doc.Document doc = (com.mind.ilex.xml.doc.Document) unmarshaller
					.unmarshal(xmlSource);

			Section section;
			java.util.List list1 = doc.getSections().getSection();
			java.util.List list2 = new ArrayList();
			Iterator it = list1.iterator();
			while (it.hasNext()) {
				section = (Section) it.next();
				if (section.getParentid().toString().equals(Id)) {
					list2.add(section);
				}

			}

			Iterator it2 = list2.iterator();
			while (it2.hasNext()) {
				section = (Section) it2.next();
				pdfn = new ParagraphDetail();

				if (section.getNumber() != null)
					pdfn.setParagraph_id(section.getId().toString());
				else
					pdfn.setParagraph_id("");

				if (section.getNumber() != null)
					pdfn.setParagraph_number(section.getNumber().getContent());
				else
					pdfn.setParagraph_number("");

				if (section.getTitle() != null)
					pdfn.setParagraph_title(section.getTitle().getContent());
				else
					pdfn.setParagraph_title("");

				if (section.getContent() != null)
					pdfn.setParagraph_content(section.getContent().getContent());
				else
					pdfn.setParagraph_content("");

				EXList.add(pdfn);
			}
		} catch (Exception e) {
			logger.error("getParagraphDetail(String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getParagraphDetail(String, String) - Exception caught in rerieving Paragraph Detail"
						+ e);
			}
		}

		return EXList;
	}

	public static String getChkXmlFile(String id, String type, DataSource ds)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String lp_md_uploaded_flag = "Z";
		if (id == null || id.equals(""))
			id = "0";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = null;

			// if type is hardware or NetMedX set to Appendix
			if (type.equals("Hardware") || type.equals("NetMedX")
					|| type.equals("NewNetMedX"))
				type = "Appendix";

			if (type.equals("MSA")) {
				sql = "select isnull(lp_md_uploaded_flag,'') from lp_msa_detail where lp_md_mm_id = "
						+ id;
			}
			if (type.equals("Appendix")) {
				sql = "select isnull(lx_pd_uploaded_flag,'') from lx_appendix_detail where lx_pd_pr_id = "
						+ id;
			}
			// if(!type.equals("NetMedX")){
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				lp_md_uploaded_flag = rs.getString(1);
			}
			// }
		}

		catch (Exception e) {
			logger.error("getChkXmlFile(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getChkXmlFile(String, String, DataSource) - Error occured during get Check Xml File "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return lp_md_uploaded_flag;
	}

	public static void setChkXmlFile(String id, String type, DataSource ds)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		String sql = "";
		if (id.equals(""))
			id = "0";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (type.equals("Hardware") || type.equals("NetMedX")
					|| type.equals("NewNetMedX"))
				type = "Appendix";

			if (type.equals("MSA"))
				sql = "update lp_msa_detail set lp_md_uploaded_flag = 'X' where lp_md_mm_id = "
						+ id;
			if (type.equals("Appendix"))
				sql = "update lx_appendix_detail set lx_pd_uploaded_flag = 'X' where lx_pd_pr_id = "
						+ id;

			// if(!type.equals("NetMedX"))
			stmt.executeUpdate(sql);

		}

		catch (Exception e) {
			logger.error("setChkXmlFile(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setChkXmlFile(String, String, DataSource) - Error occured during set check xml "
						+ e);
			}
		}

		finally {
			DBUtil.close(stmt);
			DBUtil.close(conn);

		}

	}

	public static String getName(String id, String type, DataSource ds)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		String name = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (type.equals("NetMedX") || type.equals("Hardware"))
				type = "Appendix";

			sql = "select dbo.func_cc_name('" + id + "','" + type + "')";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				name = rs.getString(1);
			}
		}

		catch (Exception e) {
			logger.error("getName(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getName(String, String, DataSource) - Error occured during get name "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return name;

	}

	public static String getTypeName(String id, String type, DataSource ds)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		String type_name = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select dbo.func_cc_type_name('" + id + "','" + type + "')";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				type_name = rs.getString(1);
			}
		}

		catch (Exception e) {
			logger.error("getTypeName(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTypeName(String, String, DataSource) - Error occured during get name "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return type_name;

	}

	public static String getNetMedxXmlType(String id, DataSource ds)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String netMedxXml = null;
		if (id == null || id.equals(""))
			id = "0";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = null;

			sql = "select lx_pd_xml from lx_appendix_detail where lx_pd_pr_id = "
					+ id;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				netMedxXml = rs.getString(1);
			}
		}

		catch (Exception e) {
			logger.error("getNetMedxXmlType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetMedxXmlType(String, DataSource) - Error occured while retrieving Xml type for NetMedx "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}

		return netMedxXml;
	}

	public static String getXmlFile(String FileName, DataSource ds) {

		Connection conn = null;

		ResultSet rs = null;
		String xmlFile = null;
		PreparedStatement ps2 = null;

		try {
			conn = ds.getConnection();

			ps2 = conn
					.prepareStatement("select fileContent from xml_fileContent where fileName =?");
			ps2.setString(1, FileName);

			rs = ps2.executeQuery();
			if (rs.next()) {
				xmlFile = rs.getString(1);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("xmlFile Retrived from database is " + xmlFile);
			}
		}

		catch (Exception e) {
			logger.error("getXmlFIle(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getXmlFIle(String, DataSource) - Error occured while retrieving Xml File "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, ps2);
			DBUtil.close(conn);
		}

		return xmlFile;

	}

	public static boolean isXmlRecordExist(String FileName, DataSource ds) {

		Connection conn = null;

		ResultSet rs = null;
		String xmlFile = null;
		PreparedStatement ps2 = null;
		boolean recordsExist = false;

		try {
			conn = ds.getConnection();

			ps2 = conn
					.prepareStatement("select * from xml_fileContent where fileName =?");
			ps2.setString(1, FileName);

			rs = ps2.executeQuery();
			if (rs.next())
				recordsExist = true;

			if (logger.isDebugEnabled()) {
				logger.debug("xmlFile Retrived from database is " + xmlFile);
			}
		}

		catch (Exception e) {
			logger.error("getXmlFIle(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getXmlFIle(String, DataSource) - Error occured while retrieving Xml File "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, ps2);
			DBUtil.close(conn);
		}

		return recordsExist;

	}

	public static void insertXmlFileTODB(String FileName, String FileContent,
			String Type, String Id, String UserID, DataSource ds)
			throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = ds.getConnection();

			ps = conn
					.prepareStatement("insert into xml_fileContent values(?,?,?,?,?,getdate(),?,getdate())");
			ps.setString(1, FileName);
			ps.setString(2, FileContent);
			ps.setString(3, Type);
			ps.setInt(4, Integer.parseInt(Id));
			ps.setString(5, UserID);
			ps.setString(6, UserID);
			ps.executeUpdate();

		}

		catch (Exception e) {
			logger.error("insertXmlFileTODB(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("insertXmlFileTODB(String, String, DataSource) - Error occured during set check xml "
						+ e);
			}
		}

		finally {
			DBUtil.close(ps);
			DBUtil.close(conn);
		}

	}

	public static void updateXmlFile(String FileName, String FileContent,
			String UserID, DataSource ds) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = ds.getConnection();

			ps = conn
					.prepareStatement("UPDATE xml_fileContent SET fileContent =?, modifiedBy=?,modifiedDate=getdate()  WHERE fileName =?");
			ps.setString(1, FileContent);
			ps.setString(2, UserID);
			ps.setString(3, FileName);

			ps.executeUpdate();

		}

		catch (Exception e) {
			logger.error("updateXmlFile(String, String, DataSource)", e);
			throw new RuntimeException(
					"Failure saving XML to database. Please contact system administrator.");

		}

		finally {
			DBUtil.close(ps);
			DBUtil.close(conn);
		}

	}

}
