package com.mind.util;

public enum MenuTreeCategory {

	PM("msatabularpage"), RM("resourcemanagerpage"), UM("usermainpage"), CM(
			"customermainpage"), AM("activitymanagerpage"), PRM(
			"projectmainpage"), NM("netmedxmainpage"), PVS("pvsmainpage"), PMT(
			"proposalmasterpage"), PRCM("undefined"), UNDEFINED("undefined");

	private String viewName = "undefined";

	MenuTreeCategory(String viewName) {
		this.viewName = viewName;
	}

	public String getViewName() {
		return this.viewName;
	}
}
