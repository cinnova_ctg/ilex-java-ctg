package com.mind.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.mind.common.MultipartRequestWrapper;

public class WebUtil {

	private static final Logger logger = Logger.getLogger(WebUtil.class);

	public static String getRealPath(HttpServletRequest request,
			String relativePath) {
		if (request.getClass().getName()
				.equals("org.apache.struts.upload.MultipartRequestWrapper")) {
			logger.debug("Casting MultiparRequestWrapper to custom class");
			request = new MultipartRequestWrapper(request);
		}

		return request.getServletContext().getRealPath(relativePath);
	}

	public static void copyMapToRequest(HttpServletRequest request,
			Map<String, Object> map) {
		Iterator<String> iterator = map.keySet().iterator();

		while (iterator.hasNext()) {
			String key = iterator.next();
			Object value = map.get(key);
			request.setAttribute(key, value);
		}

	}

	public static Map<String, String> copyRequestToParamMap(
			HttpServletRequest request) {
		Map<String, String> map = new HashMap<String, String>();

		Enumeration<String> paramNameEnum = request.getParameterNames();

		while (paramNameEnum.hasMoreElements()) {
			String paramName = paramNameEnum.nextElement();
			map.put(paramName, request.getParameter(paramName));
		}

		return map;
	}

	public static Map<String, Object> copyRequestToAttributeMap(
			HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();

		Enumeration<String> attributeNameEnum = request.getAttributeNames();

		while (attributeNameEnum.hasMoreElements()) {
			String attributeName = attributeNameEnum.nextElement();
			map.put(attributeName, request.getAttribute(attributeName));
		}

		return map;
	}

	public static Map<String, Object> copySessionToAttributeMap(
			HttpSession session) {
		Map<String, Object> map = new HashMap<String, Object>();

		Enumeration<String> attributeNameEnum = session.getAttributeNames();

		while (attributeNameEnum.hasMoreElements()) {
			String attributeName = attributeNameEnum.nextElement();
			map.put(attributeName, session.getAttribute(attributeName));
		}

		return map;
	}

	public static void setValuesWhenReset(HttpSession session) {
		session.removeAttribute("selectedOwners");
		session.removeAttribute("selectedStatus");
		session.removeAttribute("tempOwner");
		session.removeAttribute("tempStatus");
		session.removeAttribute("otherCheck");
		session.removeAttribute("monthMSA");
		session.removeAttribute("weekMSA");
		session.removeAttribute("MSASelectedOwners");
		session.removeAttribute("MSASelectedStatus");
		session.setAttribute("selectedStatus", "('D','R','P','T','S')");
		session.setAttribute("MSASelectedStatus", "('D','R','P','T','S')");
		session.setAttribute("tempStatus", "D,R,P,T,S");
		session.setAttribute("selectedOwners",
				"(" + session.getAttribute("userid").toString() + ")");
		session.setAttribute("MSASelectedOwners",
				"(" + session.getAttribute("userid").toString() + ")");
		session.setAttribute("tempOwner", session.getAttribute("userid")
				.toString());

	}

	public static void removeAppendixSession(HttpSession session) {
		session.removeAttribute("appendixSelectedOwners");
		session.removeAttribute("appendixSelectedStatus");
		session.removeAttribute("appendixTempOwner");
		session.removeAttribute("appendixTempStatus");
		session.removeAttribute("appendixOtherCheck");
		session.removeAttribute("monthAppendix");
		session.removeAttribute("weekAppendix");
		session.removeAttribute("appSelectedOwners");
		session.removeAttribute("appSelectedStatus");
	}

	public static void setDefaultAppendixDashboardAttribute(HttpSession session) {

		session.removeAttribute("jobSelectedOwners");
		session.removeAttribute("jobTempOwner");
		session.removeAttribute("jobSelectedStatus");
		session.removeAttribute("jobTempStatus");
		session.removeAttribute("jobMonthWeekCheck");
		session.removeAttribute("timeFrame");
		session.removeAttribute("jobOwnerOtherCheck");

		session.setAttribute("jobSelectedStatus", "('II','IO','ION','IOF')");
		session.setAttribute("jobSelectedOwners",
				"(" + session.getAttribute("userid").toString() + ")");
		session.setAttribute("jobTempStatus", "II,IO,ION,IOF");
		session.setAttribute("jobTempOwner", session.getAttribute("userid")
				.toString());
		session.setAttribute("timeFrame", "clear");

	}

	public static String getDataFromWeb(String url, int timeout,
			String lineBreak) {
		try {
			URL u = new URL(url);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(
						c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + lineBreak);
				}
				br.close();
				return sb.toString();
			}
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
