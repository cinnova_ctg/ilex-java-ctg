package com.mind.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.AbstractMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class RestWSUtils {

	static Logger logger = Logger.getLogger(RestWSUtils.class);

	public final static String CONTENT_TYPE_KEY = "Content-type";
	public final static String CONTENT_TYPE_VALUE = "application/octet-stream";
	public final static String SYMBOL_QUESTION = "?";
	public final static String SYMBOL_EQUALTO = "=";
	public final static String SYMBOL_AND = "&";
	public final static String SYMBOL_AT = "@";
	public final static String RANDOM = "random";
	public final static String POST_REQUEST = "POST";

	public static void uploadFile(String baseURL,
			List<AbstractMap.SimpleEntry<String, String>> requestParams,
			byte[] file) {
		try {
			WebResource webResource = getWebResource(baseURL);

			StringBuffer buffer = new StringBuffer();
			buffer.append(webResource.getURI());
			buffer.append(SYMBOL_QUESTION);
			int count = 1;
			for (AbstractMap.SimpleEntry<String, String> requestParam : requestParams) {
				buffer.append(requestParam.getKey());
				buffer.append(SYMBOL_EQUALTO);
				buffer.append(requestParam.getValue());
				if (count != 5) {
					buffer.append(SYMBOL_AND);
				}

				if (count == 4) {
					buffer.append(RANDOM);
					buffer.append(SYMBOL_EQUALTO);
					buffer.append(System.currentTimeMillis());
					buffer.append(SYMBOL_AND);
				}
				count++;
			}

			InputStream inputStream = new ByteArrayInputStream(file);

			logger.info("---upload url---- :" + buffer.toString());
			URL url = null;
			try {
				url = new URL(buffer.toString());
			} catch (MalformedURLException e) {
				logger.error(e);
			}

			URLConnection urlConnection = null;
			try {
				urlConnection = url.openConnection();
			} catch (IOException e) {
				logger.error(e);
			}
			urlConnection.setDoOutput(true);

			if (urlConnection instanceof HttpURLConnection) {
				try {
					((HttpURLConnection) urlConnection)
							.setRequestMethod(POST_REQUEST);
					((HttpURLConnection) urlConnection).setRequestProperty(
							CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
					((HttpURLConnection) urlConnection).connect();

				} catch (ProtocolException e) {
					logger.error(e);
				}
			}

			OutputStream outputStream = urlConnection.getOutputStream();

			int totalbyte = inputStream.available();

			int targetbytes = totalbyte / 100;
			int bytesRead = 0;
			byte[] bufferF = null;
			if (totalbyte > 100) {
				bufferF = new byte[targetbytes];
			} else {
				bufferF = new byte[1];
			}
			if (totalbyte > 0) {
				while ((bytesRead = inputStream
						.read(bufferF, 0, bufferF.length)) != -1) {
					outputStream.write(bufferF, 0, bytesRead);

				}
				outputStream.close();
				inputStream.close();
			} else {
				outputStream.close();
				inputStream.close();

			}
			String responseMsg = ((HttpURLConnection) urlConnection)
					.getResponseMessage();
			logger.info("--- *** R E S PO N S E MSG *** ---" + responseMsg);
			if (!"OK".equals(responseMsg)) {
				String errMsg = "Not able to upload deliverable to Customer Care Center";
				throw new RuntimeException(errMsg, null);
			}

		} catch (UniformInterfaceException uie) {
			Status responseStatus = uie.getResponse().getClientResponseStatus();
			String errMsg = "(Error code: " + responseStatus.getStatusCode()
					+ ") " + responseStatus.getReasonPhrase();
			throw new RuntimeException(errMsg, uie);
		} catch (Exception che) {
			String errMsg = "Not able to upload deliverable to Customer Care Center";
			throw new RuntimeException(errMsg, che);
		}
	}

	private static WebResource getWebResource(String url) {
		ClientConfig config = new DefaultClientConfig();
		config.getProperties().put(ClientConfig.PROPERTY_CONNECT_TIMEOUT,
				new Integer(60000));
		config.getProperties().put(ClientConfig.PROPERTY_READ_TIMEOUT,
				new Integer(60000));
		config.getProperties().put("http.socket.timeout", new Integer(60000));
		config.getProperties().put("http.keepAlive", true);
		Client client = Client.create(config);
		client.setConnectTimeout(120000);
		client.setReadTimeout(120000);
		if (logger.isDebugEnabled()) {
			client.addFilter(new LoggingFilter());
		}

		WebResource webResource = client.resource(url);
		logger.info(config.getProperties().get(
				ClientConfig.PROPERTY_CONNECT_TIMEOUT));
		logger.info(config.getProperties().get(
				ClientConfig.PROPERTY_READ_TIMEOUT));
		return webResource;
	}

}
