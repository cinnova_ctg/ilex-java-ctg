package com.mind.util;

import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

public class VelocityUtils {

	private static final Logger logger = Logger.getLogger(VelocityUtils.class);

	public static String getMailContent(String templateName,
			Map<String, String> model) {
		String emailBody = null;
		Properties property = System.getProperties();
		property.setProperty(VelocityEngine.RESOURCE_LOADER, "classpath");

		property.setProperty("classpath." + VelocityEngine.RESOURCE_LOADER
				+ ".class",

		ClasspathResourceLoader.class.getName());

		StringWriter writer = new StringWriter();
		try {
			Velocity.init(property);
			VelocityContext context = new VelocityContext(model);
			Template template = Velocity.getTemplate(templateName);
			template.merge(context, writer);
			emailBody = writer.toString();
		} catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
		} catch (ParseErrorException p) {
			logger.error(p.getMessage(), p);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return emailBody;

	}

}
