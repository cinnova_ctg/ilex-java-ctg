package com.mind.util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.AbstractMap;
import java.util.List;

import org.apache.http.HttpVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.log4j.Logger;

public class HttpClientUtils {
	static Logger logger = Logger.getLogger(HttpClientUtils.class);

	public static void uploadFile(String url,
			List<AbstractMap.SimpleEntry<String, String>> requestParams,
			byte[] byteFile, String fileParamName, String fileName)
			throws IOException {
		HttpClient client = new DefaultHttpClient();
		client.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
				HttpVersion.HTTP_1_1);

		HttpPost post = new HttpPost(url);
		MultipartEntity entity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);

		// For File parameters
		entity.addPart(fileParamName, new ByteArrayBody(byteFile, fileName));

		// For usual String parameters
		for (AbstractMap.SimpleEntry<String, String> requestParam : requestParams) {
			entity.addPart(requestParam.getKey(),
					new StringBody(requestParam.getValue(), "text/plain",
							Charset.forName("UTF-8")));
		}

		post.setEntity(entity);

		StatusLine response = client.execute(post).getStatusLine();
		logger.info("--- *** R E S PO N S E MSG *** ---" + response);

		if (!response.toString().contains("OK")) {
			throw new RuntimeException(
					"Not Able to upload Document to Customer Care Center.");
		}

		client.getConnectionManager().shutdown();

	}
}
