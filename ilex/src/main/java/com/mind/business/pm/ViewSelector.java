package com.mind.business.pm;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.mind.bean.pm.ViewSelectorBean;

/**
 * The Class ViewSelector.
 */
public class ViewSelector {

	/** The form beanobj. */
	// static Object formBeanobj;

	/** The class obj. */
	// static Class classObj;

	/**
	 * Sets the object.
	 * 
	 * @param beanobj
	 *            the beanobj
	 * @param session
	 *            the session
	 * @param request
	 *            the request
	 * @param ds
	 *            the ds
	 * 
	 * @return the view selector bean
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws NoSuchMethodException
	 */
	public static void setObject(Object beanobj, HttpSession session,
			HttpServletRequest request, DataSource ds) {
		ViewSelectorBean viewBean = new ViewSelectorBean();
		// formBeanobj = beanobj;
		try {
			Class classObj = getClass(beanobj);
			viewBean.setMsaId(getValue(classObj, beanobj, "getMsaId", null));
			viewBean
					.setMsaName(getValue(classObj, beanobj, "getMsaName", null));
			viewBean.setAppendixId(getValue(classObj, beanobj, "getAppendixId",
					null));
			viewBean = ViewSelectorController.callViewSelector(viewBean
					.getAppendixId(), session, request, ds);

			Object argList[] = new Object[1];
			Class clsString[] = new Class[1];
			clsString[0] = String.class;

			Class clsArray[] = new Class[1];
			clsArray[0] = String[].class;

			argList[0] = viewBean.getAppendixSelectedStatus();
			setValue(classObj, beanobj, "setAppendixSelectedStatus", argList,
					clsArray);

			argList[0] = viewBean.getAppendixSelectedOwners();
			setValue(classObj, beanobj, "setAppendixSelectedOwners", argList,
					clsArray);

			argList[0] = viewBean.getAppendixOtherCheck();
			setValue(classObj, beanobj, "setAppendixOtherCheck", argList,
					clsString);

			argList[0] = viewBean.getPrevSelectedStatus();
			setValue(classObj, beanobj, "setPrevSelectedStatus", argList,
					clsString);

			argList[0] = viewBean.getPrevSelectedOwner();
			setValue(classObj, beanobj, "setPrevSelectedOwner", argList,
					clsString);

			argList[0] = viewBean.getMonthWeekCheck();
			setValue(classObj, beanobj, "setMonthWeekCheck", argList, clsString);

			argList[0] = viewBean.getCheckAppendix();
			setValue(classObj, beanobj, "setCheckAppendix", argList, clsString);

			argList[0] = viewBean.getType();
			setValue(classObj, beanobj, "setType", argList, clsString);

			argList[0] = viewBean.getUrl();
			setValue(classObj, beanobj, "setUrl", argList, clsString);

			/*
			 * form.setAppendixSelectedStatus(bean.getAppendixSelectedStatus());
			 * form.setAppendixSelectedOwners(bean.getAppendixSelectedOwners());
			 * form.setAppendixOtherCheck(bean.getAppendixOtherCheck());
			 * form.setPrevSelectedStatus(bean.getPrevSelectedStatus());
			 * form.setPrevSelectedOwner(bean.getPrevSelectedOwner());
			 * form.setMonthWeekCheck(bean.getMonthWeekCheck());
			 * form.setCheckAppendix(bean.getCheckAppendix());
			 * form.setType(bean.getType()); form.setUrl(bean.getUrl());
			 */
		} catch (Throwable e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param formBeanobj
	 * @return
	 * @throws ClassNotFoundException
	 */
	private static Class getClass(Object formBeanobj)
			throws ClassNotFoundException {
		Class c = Class.forName(formBeanobj.getClass().getName());
		return c;
	}

	/**
	 * @param formBeanobj
	 * @param c
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private static String getValue(Class classObj, Object formBeanobj,
			String methodName, Class arglist[]) throws NoSuchMethodException,
			IllegalAccessException, InvocationTargetException {
		Method methodget = classObj.getMethod(methodName, arglist);
		Object retobj = methodget.invoke(formBeanobj, arglist);
		return (String) retobj;
	}

	/**
	 * @param formBeanobj
	 * @param c
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private static void setValue(Class classObj, Object formBeanobj,
			String methodName, Object arglist[], Class cls[])
			throws NoSuchMethodException, IllegalAccessException,
			InvocationTargetException {
		Method methodget = classObj.getMethod(methodName, cls);
		methodget.invoke(formBeanobj, arglist);
	}
}
