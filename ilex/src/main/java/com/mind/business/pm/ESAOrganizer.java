package com.mind.business.pm;

import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import com.mind.bean.pm.ESAActivityCommissionBean;
import com.mind.bean.pm.ESAEditBean;
import com.mind.bean.pm.ESAList;
import com.mind.dao.PM.ESADao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.formbean.PM.ESAActivityCommissionForm;
import com.mind.formbean.PM.ESAEditForm;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

/**
 * The Class ESAOrganizer.
 */
public class ESAOrganizer {

	/**
	 * Sets ESA bean.
	 * 
	 * @param esaBean
	 *            - ESAEditBean object.
	 * @param ds
	 *            - data source.
	 */
	public static void setEsaBean(ESAEditBean esaBean, DataSource ds) {
		esaBean.setEsaAgentList(ESADao.getESAAgent(ds));
		esaBean.setEsaPaymentList(ESADao.getESAPaymentTerms(ds));
		esaBean.setEsaList(GetESADataFromDatabase(esaBean, ds));
		setESADetailToBean(esaBean.getEsaList(), esaBean);
	}

	/**
	 * Gets ESA details specific to an appendix.
	 * 
	 * @param esaBean
	 *            - ESAEditBean object.
	 * @param ds
	 *            - data source.
	 * 
	 * @return ArrayList.
	 */
	public static ArrayList GetESADataFromDatabase(ESAEditBean esaBean,
			DataSource ds) {
		ArrayList esaDetailsFromDB = ESADao.getAppendixESADetails(
				esaBean.getAppendixId(), ds);
		return esaDetailsFromDB;
	}

	/**
	 * Populate ESAEditBean object from ArrayList that holds ESA details that
	 * are specific to an appendix.
	 * 
	 * @param esaDetails
	 *            - ArrayList that holds ESA details that are specific to an
	 *            appendix.
	 * @param esaBean
	 *            - ESAEditBean object.
	 */
	public static void setESADetailToBean(ArrayList<ESAList> esaDetails,
			ESAEditBean esaBean) {

		String esaId[] = new String[esaDetails.size()];
		String esaSeqNo[] = new String[esaDetails.size()];
		String agent[] = new String[esaDetails.size()];
		String commission[] = new String[esaDetails.size()];
		String payementTerms[] = new String[esaDetails.size()];
		String effectiveDate[] = new String[esaDetails.size()];
		String status[] = new String[esaDetails.size()];
		String commissionType[] = new String[esaDetails.size()];
		int sequence = 0;

		for (int i = 0; i < esaDetails.size(); i++) {
			ESAList esaList = esaDetails.get(i);
			sequence = Integer.parseInt(esaList.getEsaSeqNo()) - 1;

			esaId[sequence] = esaList.getEsaId();
			esaSeqNo[sequence] = esaList.getEsaSeqNo();
			agent[sequence] = esaList.getAgent();
			commission[sequence] = esaList.getCommission();
			payementTerms[sequence] = esaList.getPayementTerms();
			effectiveDate[sequence] = esaList.getEffectiveDate();
			status[sequence] = esaList.getStatus();
			commissionType[sequence] = esaList.getCommissionType();
		}

		esaBean.setEsaId(esaId);
		esaBean.setEsaSeqNo(esaSeqNo);
		esaBean.setAgent(agent);
		esaBean.setCommission(commission);
		esaBean.setPayementTerms(payementTerms);
		esaBean.setEffectiveDate(effectiveDate);
		esaBean.setStatus(status);
		esaBean.setCommissionType(commissionType);
	}

	/**
	 * Delegate method to call method that saves ESA details for an appendix in
	 * database.
	 * 
	 * @param esaBean
	 *            - ESAEditBean object.
	 * @param loginuserid
	 *            - login user id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return int - error/success code for saving ESA details.
	 */
	public static int saveEsaDetails(ESAEditBean esaBean, String loginuserid,
			DataSource ds) {
		int esaSaveStatus = ESADao.saveProjectESAinDB(esaBean, loginuserid, ds);
		return esaSaveStatus;
	}

	/**
	 * Convert ESAEditBean to ESAEditForm.
	 * 
	 * @param form
	 *            - ESAEditForm object.
	 * @param esaBean
	 *            - ESAEditBean object.
	 */
	public static void convertToForm(ESAEditForm form, ESAEditBean esaBean) {
		PMConvertor.setFormFromEsa(form, esaBean);
	}

	/**
	 * Convert ESAEditBean to ESAEditForm.
	 * 
	 * @param form
	 *            - ESAEditForm object.
	 * @param esaBean
	 *            - ESAEditBean object.
	 */
	public static void convertToBean(ESAEditForm form, ESAEditBean esaBean) {
		PMConvertor.setEsaFromForm(form, esaBean);
	}

	/**
	 * Populate ESAActivityCommissionBean object.
	 * 
	 * @param esaActCommBean
	 *            - ESAActivityCommissionBean object.
	 * @param ds
	 *            - data source.
	 */
	public static void setEsaActivityCommissionBean(
			ESAActivityCommissionBean esaActCommBean, DataSource ds) {
		esaActCommBean.setCommisionType(ESADao.getCommissionType());
		esaActCommBean.setAgents(ESADao.appendixAgents(
				esaActCommBean.getAppendixId(), ds));
		esaActCommBean.setEsaActivityList(ESADao.getESAActivityList(
				esaActCommBean.getJobid(), esaActCommBean.getAppendixId(), ds));
	}

	/**
	 * Check type for Job(Default/Addendum/Ticket/Job).
	 * 
	 * @param appendixId
	 *            - appendix id.
	 * @param jobId
	 *            - job id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return String - job type.
	 */
	public static String getJobType(String appendixId, String jobId,
			DataSource ds) {
		String returnString = "";
		String jobType = JobSetUpDao.getjobType(jobId, ds);
		String appendixType = DatabaseUtilityDao.getProjectType(appendixId, ds);
		if (jobType != null) {
			if (!(jobType.equals("Default") || jobType.equals("Addendum"))) {
				if (appendixType.equalsIgnoreCase("NetMedX")) {
					returnString = "Ticket";
				} else {
					returnString = "Job";
				}
			} else {
				returnString = jobType;
			}
		}
		return returnString;
	}

	/**
	 * Convert to commission form.
	 * 
	 * @param esaActCommissionForm
	 *            - esaActCommissionForm object.
	 * @param esaActCommBean
	 *            - ESAActivityCommissionBean object.
	 */
	public static void ConvertToCommissionForm(
			ESAActivityCommissionForm esaActCommissionForm,
			ESAActivityCommissionBean esaActCommBean) {
		PMConvertor.setCommissionBeanToForm(esaActCommissionForm,
				esaActCommBean);
	}

	/**
	 * Convert commission form to bean.
	 * 
	 * @param esaActCommissionForm
	 *            - esaActCommissionForm object.
	 * @param esaActCommBean
	 *            - ESAActivityCommissionBean object.
	 */
	public static void convertFormToBean(
			ESAActivityCommissionForm esaActCommissionForm,
			ESAActivityCommissionBean esaActCommBean) {
		PMConvertor.setCommissionBeanFromForm(esaActCommissionForm,
				esaActCommBean);

	}

	/**
	 * Save commission page details into database.
	 * 
	 * @param bean
	 *            - ESAActivityCommissionBean object.
	 * @param loginuserid
	 *            - login user id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return the int - error/success return code.
	 */
	public static int saveCommissionBean(ESAActivityCommissionBean bean,
			String loginuserid, DataSource ds) {
		int retval = ESADao.saveCommissionForActESA(bean, loginuserid, ds);
		return retval;
	}

	/**
	 * Prepare menu to be displayed on Commission page from
	 * Proposal/Project/NetMedx Managers.
	 * 
	 * @param jobId
	 *            - job id.
	 * @param request
	 *            - request object.
	 * @param userId
	 *            - user id.
	 * @param ds
	 *            - data source.
	 */
	public static Map<String, Object> setCommissionPageMenu(String jobId,
			String userId, DataSource ds) {
		Map<String, Object> map;
		map = DatabaseUtilityDao.setMenu(jobId, userId, ds);
		return map;
	}
}
