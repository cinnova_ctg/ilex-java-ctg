package com.mind.business.pm;

import com.mind.bean.pm.ESAActivityCommissionBean;
import com.mind.bean.pm.ESAEditBean;
import com.mind.formbean.PM.ESAActivityCommissionForm;
import com.mind.formbean.PM.ESAEditForm;

/**
 * The Class PMConvertor.
 */
public class PMConvertor {

	/**
	 * Sets the form from esa.
	 * 
	 * @param form
	 *            the form
	 * @param esaBean
	 *            the esa bean
	 */
	public static void setFormFromEsa(ESAEditForm form, ESAEditBean esaBean) {
		form.setEsaId(esaBean.getEsaId());
		form.setEsaSeqNo(esaBean.getEsaSeqNo());
		form.setAgent(esaBean.getAgent());
		form.setCommission(esaBean.getCommission());
		form.setPayementTerms(esaBean.getPayementTerms());
		form.setEffectiveDate(esaBean.getEffectiveDate());
		form.setStatus(esaBean.getStatus());
		form.setCommissionType(esaBean.getCommissionType());
		form.setEsaAgentList(esaBean.getEsaAgentList());
		form.setEsaPaymentList(esaBean.getEsaPaymentList());
		form.setEsaList(esaBean.getEsaList());
	}

	public static void setEsaFromForm(ESAEditForm form, ESAEditBean esaBean) {
		esaBean.setEsaId(form.getEsaId());
		esaBean.setEsaSeqNo(form.getEsaSeqNo());
		esaBean.setAgent(form.getAgent());
		esaBean.setCommission(form.getCommission());
		esaBean.setPayementTerms(form.getPayementTerms());
		esaBean.setEffectiveDate(form.getEffectiveDate());
		esaBean.setStatus(form.getStatus());
		esaBean.setCommissionType(form.getCommissionType());
		esaBean.setEsaAgentList(form.getEsaAgentList());
		esaBean.setEsaPaymentList(form.getEsaPaymentList());
	}

	public static void setCommissionBeanToForm(ESAActivityCommissionForm form,
			ESAActivityCommissionBean bean) {
		form.setCommisionType(bean.getCommisionType());
		form.setAgents(bean.getAgents());
		form.setEsaActivityList(bean.getEsaActivityList());
	}

	public static void setCommissionBeanFromForm(
			ESAActivityCommissionForm form, ESAActivityCommissionBean bean) {
		bean.setActivityId(form.getActivityId());
		bean.setActivityQty(form.getActivityQty());

		bean.setEsaId1(form.getEsaId1());
		bean.setActCommissionType1(form.getActCommissionType1());
		bean.setCommissionStatus1(form.getCommissionStatus1());
		bean.setCommissionRate1(form.getCommissionRate1());
		bean.setEsaActActiveStatus1(form.getEsaActActiveStatus1());

		bean.setEsaId2(form.getEsaId2());
		bean.setActCommissionType2(form.getActCommissionType2());
		bean.setCommissionStatus2(form.getCommissionStatus2());
		bean.setCommissionRate2(form.getCommissionRate2());
		bean.setEsaActActiveStatus2(form.getEsaActActiveStatus2());

		bean.setEsaId3(form.getEsaId3());
		bean.setActCommissionType3(form.getActCommissionType3());
		bean.setCommissionStatus3(form.getCommissionStatus3());
		bean.setCommissionRate3(form.getCommissionRate3());
		bean.setEsaActActiveStatus3(form.getEsaActActiveStatus3());
	}
}
