package com.mind.business.pm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.mind.bean.pm.ViewSelectorBean;
import com.mind.common.Util;
import com.mind.dao.PM.MSAdao;

/**
 * The Class ViewSelectorController.
 */
public class ViewSelectorController {

	/**
	 * Call view selector.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @param session
	 *            the session
	 * @param request
	 *            the request
	 * @param ds
	 *            the ds
	 * 
	 * @return the view selector bean
	 */
	public static ViewSelectorBean callViewSelector(String appendixId,
			HttpSession session, HttpServletRequest request, DataSource ds) {
		ViewSelectorBean viewBean = new ViewSelectorBean();
		viewBean.setAppendixId(appendixId);
		setRequestParameter(request, viewBean);
		setSessionParameter(session, request, viewBean);
		setSelectedOwner(viewBean);
		setAppendixSelectedStatus(viewBean);

		if (!Util.isNullOrBlank(request.getParameter("home"))) {
			home(request, viewBean);
		} else if (!Util.isNullOrBlank(request.getParameter("go"))) {
			go(session, request, viewBean);
		} else {
			viewBean.setUrl(null);
		}
		unspecified(session, request, viewBean, ds);

		// System.out.println(viewBean.getAppendixId());
		// System.out.println(viewBean.getAppendixOtherCheck());
		// System.out.println(viewBean.getAppendixSelectedOwners());
		// System.out.println(viewBean.getAppendixSelectedStatus());
		// System.out.println(viewBean.getCheckAppendix());
		// System.out.println(viewBean.getGo());
		// System.out.println(viewBean.getHome());
		// System.out.println(viewBean.getMonthWeekCheck());
		// System.out.println(viewBean.getPrevSelectedOwner());
		// System.out.println(viewBean.getPrevSelectedStatus());
		// System.out.println(viewBean.getSelectedOwner());
		// System.out.println(viewBean.getSelectedStatus());
		// System.out.println(viewBean.getTempOwner());
		// System.out.println(viewBean.getTempStatus());
		// System.out.println(viewBean.getType());
		// System.out.println(viewBean.getAppendixSelectedOwners());
		// System.out.println(viewBean.getAppendixSelectedStatus());
		// System.out.println(viewBean.getOwnerList().size());
		// System.out.println(viewBean.getStatusList().size());
		// System.out.println(request.getAttribute("clickShow"));
		// System.out.println(request.getAttribute("statuslist"));
		// System.out.println(request.getAttribute("ownerlist"));

		return viewBean;
	}

	/**
	 * Unspecified.
	 * 
	 * @param session
	 *            the session
	 * @param request
	 *            the request
	 * @param viewBean
	 *            the view bean
	 * @param ds
	 *            the ds
	 */
	private static void unspecified(HttpSession session,
			HttpServletRequest request, ViewSelectorBean viewBean, DataSource ds) {

		if (session.getAttribute("appendixSelectedOwners") != null) {
			viewBean.setSelectedOwner(session.getAttribute(
					"appendixSelectedOwners").toString());
			viewBean.setAppendixSelectedOwners(session.getAttribute(
					"appendixTempOwner").toString().split(","));
		}
		if (session.getAttribute("appendixSelectedStatus") != null) {
			viewBean.setSelectedStatus(session.getAttribute(
					"appendixSelectedStatus").toString());
			viewBean.setAppendixSelectedStatus(session.getAttribute(
					"appendixTempStatus").toString().split(","));
		}
		if (session.getAttribute("appendixOtherCheck") != null) {
			viewBean.setAppendixOtherCheck(session.getAttribute(
					"appendixOtherCheck").toString());
		}
		if (session.getAttribute("monthAppendix") != null) {
			session.removeAttribute("weekAppendix");
			viewBean.setMonthWeekCheck("month");
		}
		if (session.getAttribute("weekAppendix") != null) {
			session.removeAttribute("monthAppendix");
			viewBean.setMonthWeekCheck("week");
		}

		if (!viewBean.isChkOtherOwner()) {
			viewBean.setPrevSelectedStatus(viewBean.getSelectedStatus());
			viewBean.setPrevSelectedOwner(viewBean.getSelectedOwner());
		}

		String loginuserid = (String) session.getAttribute("userid");
		viewBean.setOwnerList(MSAdao.getOwnerList(loginuserid, viewBean
				.getAppendixOtherCheck(), "Appendix", viewBean.getMsaId(), ds));
		viewBean.setStatusList(MSAdao.getStatusList("pm_appendix", ds));

		if (viewBean.getOwnerList().size() > 0) {
			request.setAttribute("ownerListSize", viewBean.getOwnerList()
					.size()
					+ "");
		}
		request.setAttribute("clickShow", request.getParameter("clickShow"));
		request.setAttribute("statuslist", viewBean.getStatusList());
		request.setAttribute("ownerlist", viewBean.getOwnerList());
	}

	/**
	 * Home.
	 * 
	 * @param request
	 *            the request
	 * @param viewBean
	 *            the view bean
	 */
	private static void home(HttpServletRequest request,
			ViewSelectorBean viewBean) {
		viewBean.setAppendixSelectedStatus(null);
		viewBean.setAppendixSelectedOwners(null);
		viewBean.setPrevSelectedStatus("");
		viewBean.setPrevSelectedOwner("");
		viewBean.setAppendixOtherCheck(null);
		viewBean.setMonthWeekCheck(null);
		viewBean.setUrl("/AppendixUpdateAction.do");
	}

	/**
	 * Go.
	 * 
	 * @param session
	 *            the session
	 * @param request
	 *            the request
	 * @param viewBean
	 *            the view bean
	 */
	private static void go(HttpSession session, HttpServletRequest request,
			ViewSelectorBean viewBean) {
		session.setAttribute("appendixSelectedOwners", viewBean
				.getSelectedOwner());
		session.setAttribute("appSelectedOwners", viewBean.getSelectedOwner());
		session.setAttribute("appendixTempOwner", viewBean.getTempOwner());
		session.setAttribute("appendixSelectedStatus", viewBean
				.getSelectedStatus());
		session.setAttribute("appSelectedStatus", viewBean.getSelectedStatus());
		session.setAttribute("appendixTempStatus", viewBean.getTempStatus());

		if (viewBean.getMonthWeekCheck() != null) {
			if (viewBean.getMonthWeekCheck().equals("week")) {
				session.removeAttribute("monthAppendix");
				session.setAttribute("weekAppendix", "0");
			}

			if (viewBean.getMonthWeekCheck().equals("month")) {
				session.removeAttribute("weekAppendix");
				session.setAttribute("monthAppendix", "0");

			}

			if (viewBean.getMonthWeekCheck().equals("clear")) {
				session.removeAttribute("weekAppendix");
				session.removeAttribute("monthAppendix");
			}
		}
		viewBean.setUrl("/AppendixUpdateAction.do");
	}

	/**
	 * Sets the selected owner.
	 * 
	 * @param viewBean
	 *            the new selected owner
	 */
	private static void setSelectedOwner(ViewSelectorBean viewBean) {
		StringBuffer selectedOwner = new StringBuffer();
		StringBuffer tempOwner = new StringBuffer();

		if (viewBean.getAppendixSelectedOwners() != null) {
			for (int j = 0; j < viewBean.getAppendixSelectedOwners().length; j++) {
				selectedOwner.append(","
						+ viewBean.getAppendixSelectedOwners(j));
				tempOwner.append("," + viewBean.getAppendixSelectedOwners(j));
			}

			selectedOwner.append(selectedOwner.toString().substring(1,
					selectedOwner.length()));
			if (selectedOwner.equals("0")) {
				viewBean.setChkOtherOwner(true);
				selectedOwner.append("%");
			}
			selectedOwner.append("(" + selectedOwner + ")");

			viewBean.setSelectedOwner(selectedOwner.toString());
			viewBean.setTempOwner(tempOwner.toString());
		}
	}

	/**
	 * Sets the appendix selected status.
	 * 
	 * @param viewBean
	 *            the new appendix selected status
	 */
	private static void setAppendixSelectedStatus(ViewSelectorBean viewBean) {
		StringBuffer selectedStatus = new StringBuffer();
		StringBuffer tempStatus = new StringBuffer();

		if (viewBean.getAppendixSelectedStatus() != null) {
			for (int i = 0; i < viewBean.getAppendixSelectedStatus().length; i++) {
				selectedStatus.append("," + "'"
						+ viewBean.getAppendixSelectedStatus(i) + "'");
				tempStatus.append("," + viewBean.getAppendixSelectedStatus(i));
			}

			selectedStatus.append(selectedStatus.substring(1, selectedStatus
					.length()));
			selectedStatus.append("(" + selectedStatus + ")");

			viewBean.setSelectedStatus(selectedStatus.toString());
			viewBean.setTempStatus(tempStatus.toString());
		}

	}

	/**
	 * Sets the request parameter.
	 * 
	 * @param request
	 *            the request
	 * @param viewBean
	 *            the view bean
	 */
	private static void setRequestParameter(HttpServletRequest request,
			ViewSelectorBean viewBean) {

		if (request.getAttribute("msaId") != null) {
			viewBean.setMsaId((String) request.getAttribute("msaId"));
			request.setAttribute("msaId", viewBean.getMsaId());
		}

		if (request.getAttribute("appendixId") != null) {
			viewBean.setAppendixId((String) request.getAttribute("appendixId"));
			request.setAttribute("appendixId", viewBean.getAppendixId());
		}

		if (request.getParameter("opendiv") != null) { // This is used to make
														// the div remain open
														// when other check box
														// is checked.
			request.setAttribute("opendiv", request.getParameter("opendiv")
					.toString());
		}

		if (request.getParameter("appendixOtherCheck") != null) { // This is
																	// used to
																	// make the
																	// div
																	// remain
																	// open when
																	// other
																	// check box
																	// is
																	// checked.
			viewBean.setAppendixOtherCheck(request
					.getParameter("appendixOtherCheck"));
		}

	}

	/**
	 * Sets the session parameter.
	 * 
	 * @param session
	 *            the session
	 * @param request
	 *            the request
	 * @param viewBean
	 *            the view bean
	 */
	private static void setSessionParameter(HttpSession session,
			HttpServletRequest request, ViewSelectorBean viewBean) {

		if (request.getParameter("month") != null) {
			session.setAttribute("monthAppendix", request.getParameter("month")
					.toString());
			session.removeAttribute("weekAppendix");
			viewBean.setMonthWeekCheck("month");
		}
		if (request.getParameter("week") != null) {
			session.setAttribute("weekAppendix", request.getParameter("week")
					.toString());
			session.removeAttribute("monthAppendix");
			viewBean.setMonthWeekCheck("week");
		}
		if (viewBean.getAppendixOtherCheck() != null) { // OtherCheck box status
														// is kept in session.
			session.setAttribute("appendixOtherCheck", viewBean
					.getAppendixOtherCheck().toString());
		}
	}
}
