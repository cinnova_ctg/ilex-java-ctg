/*
 * 
 */
package com.mind.business.PRM;

import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.prm.InsuranceRequestAttention;
import com.mind.bean.prm.InsuranceRequestBean;
import com.mind.bean.prm.InsuranceRequestDetails;
import com.mind.bean.prm.InsuranceRequestHolder;
import com.mind.bean.prm.InsuranceRequestReturn;
import com.mind.common.Util;
import com.mind.formbean.PRM.InsuranceRequestForm;

/**
 * The Class InsuranceRequestConverter.
 */
public class InsuranceRequestConverter {

	/**
	 * Sets the form from insurance request.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * @param bean
	 *            the bean
	 */
	public static void setFormFromInsuranceRequest(
			InsuranceRequestForm insuranceForm, InsuranceRequestBean bean) {
		insuranceForm.setJobId(bean.getJobId());
		insuranceForm.setAppendixId(bean.getAppendixId());
		insuranceForm.setMsaId(bean.getMsaId());
		insuranceForm.setContactPerson(bean.getContactPerson());
		insuranceForm.setTodayDate(bean.getTodayDate());
		setFormFromAttention(bean.getInsuranceRequestAttention(), insuranceForm);
		setFormFromCertificateHolder(bean.getInsuranceRequestHolder(),
				insuranceForm);
		setFormFromReturnTo(bean.getInsuranceRequestReturn(), insuranceForm);
		setFormFromJobDetails(bean.getInsuranceRequestDetails(), insuranceForm);
	}

	/**
	 * Sets the insurance request from form.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * @param bean
	 *            the bean
	 */
	public static void setInsuranceRequestFromForm(
			InsuranceRequestForm insuranceForm, InsuranceRequestBean bean) {
		bean.setJobId(insuranceForm.getJobId());
		bean.setAppendixId(insuranceForm.getAppendixId());
		bean.setMsaId(insuranceForm.getMsaId());
		bean.setContactPerson(insuranceForm.getContactPerson());
		bean.setLoginUserEmail(insuranceForm.getLoginUserEmail());
		bean.setLoginUserId(insuranceForm.getLoginUserId());
		bean.setTodayDate(IlexTimestamp.getSystemDateTime().getDate());
		bean.setInsuranceRequestAttention(setAttentionFromForm(insuranceForm));
		bean.setInsuranceRequestHolder(setCertificateHolderFromForm(insuranceForm));
		bean.setInsuranceRequestReturn(setReturnToFromForm(insuranceForm));
		bean.setInsuranceRequestDetails(setJobDetailsFromForm(insuranceForm));

	}

	/**
	 * Sets the attention from form.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * 
	 * @return the insurance request attention
	 */
	private static InsuranceRequestAttention setAttentionFromForm(
			InsuranceRequestForm insuranceForm) {
		InsuranceRequestAttention insuranceRequestAttention = new InsuranceRequestAttention();
		insuranceRequestAttention.setAttention(Util
				.getKeyValue("attention.name"));
		insuranceRequestAttention.setAttentionEmailTo(insuranceForm
				.getAttentionEmailTo());
		insuranceRequestAttention.setAttentionDirect(insuranceForm
				.getAttentionDirect());
		insuranceRequestAttention.setAttentionFax(insuranceForm
				.getAttentionFax());
		insuranceRequestAttention.setAttentionAdditionalCC(insuranceForm
				.getAttentionAdditionalCC());
		return insuranceRequestAttention;
	}

	/**
	 * Sets the certificate holder from form.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * 
	 * @return the insurance request holder
	 */
	private static InsuranceRequestHolder setCertificateHolderFromForm(
			InsuranceRequestForm insuranceForm) {
		InsuranceRequestHolder insuranceRequestHolder = new InsuranceRequestHolder();
		insuranceRequestHolder.setHolderName(insuranceForm.getHolderName());
		insuranceRequestHolder.setHolderAddress(insuranceForm
				.getHolderAddress());
		insuranceRequestHolder.setHolderCity(insuranceForm.getHolderCity());
		insuranceRequestHolder.setHolderState(insuranceForm.getHolderState());
		insuranceRequestHolder.setHolderZipCode(insuranceForm
				.getHolderZipCode());
		return insuranceRequestHolder;
	}

	/**
	 * Sets the return to from form.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * 
	 * @return the insurance request return
	 */
	private static InsuranceRequestReturn setReturnToFromForm(
			InsuranceRequestForm insuranceForm) {
		InsuranceRequestReturn insuranceRequestReturn = new InsuranceRequestReturn();
		insuranceRequestReturn.setRetName(insuranceForm.getRetName());
		insuranceRequestReturn.setRetEmail(insuranceForm.getRetEmail());
		insuranceRequestReturn.setRetDirectNumber(insuranceForm
				.getRetDirectNumber());
		insuranceRequestReturn.setRetFax(insuranceForm.getRetFax());
		return insuranceRequestReturn;
	}

	/**
	 * Sets the job details from form.
	 * 
	 * @param insuranceForm
	 *            the insurance form
	 * 
	 * @return the insurance request details
	 */
	private static InsuranceRequestDetails setJobDetailsFromForm(
			InsuranceRequestForm insuranceForm) {
		InsuranceRequestDetails insuranceRequestDetails = new InsuranceRequestDetails();
		insuranceRequestDetails.setScheduleStart(insuranceForm
				.getScheduleStart());
		insuranceRequestDetails.setScheduleEnd(insuranceForm.getScheduleEnd());
		insuranceRequestDetails.setSiteNumber(insuranceForm.getSiteNumber());
		insuranceRequestDetails.setAddress(insuranceForm.getAddress());
		insuranceRequestDetails.setCity(insuranceForm.getCity());
		insuranceRequestDetails.setState(insuranceForm.getState());
		insuranceRequestDetails.setZipCode(insuranceForm.getZipCode());
		insuranceRequestDetails.setInsuranceRequirements(insuranceForm
				.getInsuranceRequirements());
		return insuranceRequestDetails;
	}

	/**
	 * Sets the form from attention.
	 * 
	 * @param insuranceRequestAttention
	 *            the insurance request attention
	 * @param insuranceForm
	 *            the insurance form
	 */
	private static void setFormFromAttention(
			InsuranceRequestAttention insuranceRequestAttention,
			InsuranceRequestForm insuranceForm) {
		insuranceForm.setAttention(insuranceRequestAttention.getAttention());
		insuranceForm.setAttentionEmailTo(insuranceRequestAttention
				.getAttentionEmailTo());
		insuranceForm.setAttentionDirect(insuranceRequestAttention
				.getAttentionDirect());
		insuranceForm.setAttentionFax(insuranceRequestAttention
				.getAttentionFax());
		insuranceForm.setAttentionAdditionalCC(insuranceRequestAttention
				.getAttentionAdditionalCC());

	}

	/**
	 * Sets the form from certificate holder.
	 * 
	 * @param insuranceRequestHolder
	 *            the insurance request holder
	 * @param insuranceForm
	 *            the insurance form
	 */
	private static void setFormFromCertificateHolder(
			InsuranceRequestHolder insuranceRequestHolder,
			InsuranceRequestForm insuranceForm) {
		insuranceForm.setHolderAddress(insuranceRequestHolder
				.getHolderAddress());
		insuranceForm.setHolderCity(insuranceRequestHolder.getHolderCity());
		insuranceForm.setHolderName(insuranceRequestHolder.getHolderName());
		insuranceForm.setHolderState(insuranceRequestHolder.getHolderState());
		insuranceForm.setHolderStateList(insuranceRequestHolder
				.getHolderStateList());
		insuranceForm.setHolderZipCode(insuranceRequestHolder
				.getHolderZipCode());
	}

	/**
	 * Sets the form from return to.
	 * 
	 * @param insuranceRequestReturn
	 *            the insurance request return
	 * @param insuranceForm
	 *            the insurance form
	 */
	private static void setFormFromReturnTo(
			InsuranceRequestReturn insuranceRequestReturn,
			InsuranceRequestForm insuranceForm) {
		insuranceForm.setRetDirectNumber(insuranceRequestReturn
				.getRetDirectNumber());
		insuranceForm.setRetEmail(insuranceRequestReturn.getRetEmail());
		insuranceForm.setRetFax(insuranceRequestReturn.getRetFax());
		insuranceForm.setRetName(insuranceRequestReturn.getRetName());
	}

	/**
	 * Sets the form from job details.
	 * 
	 * @param insuranceRequestDetails
	 *            the insurance request details
	 * @param insuranceForm
	 *            the insurance form
	 */
	private static void setFormFromJobDetails(
			InsuranceRequestDetails insuranceRequestDetails,
			InsuranceRequestForm insuranceForm) {
		insuranceForm.setScheduleEnd(insuranceRequestDetails.getScheduleEnd());
		insuranceForm.setScheduleStart(insuranceRequestDetails
				.getScheduleStart());
		insuranceForm.setSiteNumber(insuranceRequestDetails.getSiteNumber());
		insuranceForm.setAddress(insuranceRequestDetails.getAddress());
		insuranceForm.setCity(insuranceRequestDetails.getCity());
		insuranceForm.setState(insuranceRequestDetails.getState());
		insuranceForm.setStateList(insuranceRequestDetails.getStateList());
		insuranceForm.setZipCode(insuranceRequestDetails.getZipCode());
		insuranceForm.setInsuranceRequirements(insuranceRequestDetails
				.getInsuranceRequirements());
	}

}
