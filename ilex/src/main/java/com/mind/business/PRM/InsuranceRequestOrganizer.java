package com.mind.business.PRM;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.SiteInfo;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.ScheduleInfo;
import com.mind.bean.prm.InsuranceRequestAttention;
import com.mind.bean.prm.InsuranceRequestBean;
import com.mind.bean.prm.InsuranceRequestDetails;
import com.mind.bean.prm.InsuranceRequestHolder;
import com.mind.bean.prm.InsuranceRequestReturn;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.StateList;
import com.mind.common.Util;
import com.mind.common.dao.Email;

import com.mind.dao.PRM.InsuranceRequestDao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.newjobdb.dao.JobDAO;

/**
 * The Class InsuranceRequestOrganizer.
 */
public class InsuranceRequestOrganizer {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Pvsdao.class);

	/**
	 * Send email.
	 * 
	 * @param bean
	 *            the bean
	 * @param request
	 *            the request
	 * @param ds
	 *            the ds
	 */
	public static int sendEmail(InsuranceRequestBean bean,
			HttpServletRequest request, DataSource ds) {
		EmailBean emailform = new EmailBean();
		emailform.setUsername(Util.getKeyValue("common.Email.username"));
		emailform
				.setUserpassword(Util.getKeyValue("common.Email.userpassword"));
		emailform.setSmtpservername(EnvironmentSelector
				.getBundleString("common.Email.smtpservername"));
		emailform.setSmtpserverport(Util
				.getKeyValue("common.Email.smtpserverport"));
		emailform.setTo(bean.getInsuranceRequestAttention()
				.getAttentionEmailTo());
		String cc = Util.validateEmailAddresses(bean.getLoginUserEmail()
				+ ";"
				+ bean.getInsuranceRequestAttention()
						.getAttentionAdditionalCC());
		emailform.setCc(cc);
		emailform.setBcc("");
		emailform.setSubject("Email Certificate of Insurance Request ");
		emailform.setFrom(EnvironmentSelector
				.getBundleString("appendix.default.Emailid"));
		emailform.setContent(getEmailContent(bean, request
				.getRealPath("/PRM/Job/InsuranceRequestEmailContent.html")));
		emailform.setType("partner_create");
		emailform.setFile1(bean.getFile1());
		if (Email.isValidEmailAddress(emailform.getTo())) {
			String emailstatus = Email.Send(emailform, ds);
			String[] sentCheck = emailstatus.split(",");
			return Integer.parseInt(sentCheck[0]);
		}
		return 0;
	}

	/**
	 * Organizer.
	 * 
	 * @param bean
	 *            the bean
	 * @param ds
	 *            the ds
	 */
	public static void organizer(InsuranceRequestBean bean, DataSource ds) {
		bean.setTodayDate(IlexTimestamp.getSystemDateTime().getDate());
		String[] jobInfo = JobDAO.getJobOwnerName(bean.getJobId(), ds);

		bean.setContactPerson(jobInfo[0] + ", " + jobInfo[1]);
		bean.setInsuranceRequestAttention(getAttention(ds));
		bean.setInsuranceRequestHolder(getHolder(ds));
		bean.setInsuranceRequestReturn(getReturnTo(ds));
		bean.setInsuranceRequestDetails(getDetails(bean.getJobId(), ds));
	}

	/**
	 * Gets the attention.
	 * 
	 * @param ds
	 *            the ds
	 * 
	 * @return the attention
	 */
	private static InsuranceRequestAttention getAttention(DataSource ds) {
		InsuranceRequestAttention insuranceRequestAttention = new InsuranceRequestAttention();
		insuranceRequestAttention.setAttention(Util
				.getKeyValue("attention.name"));
		insuranceRequestAttention.setAttentionEmailTo(Util
				.getKeyValue("attention.email.to"));
		insuranceRequestAttention.setAttentionDirect(Util
				.getKeyValue("attention.direct"));
		insuranceRequestAttention.setAttentionFax(Util
				.getKeyValue("attention.fax"));
		return insuranceRequestAttention;

	}

	/**
	 * Gets the holder.
	 * 
	 * @param ds
	 *            the ds
	 * 
	 * @return the holder
	 */
	private static InsuranceRequestHolder getHolder(DataSource ds) {
		InsuranceRequestHolder insuranceRequestHolder = new InsuranceRequestHolder();
		insuranceRequestHolder.setHolderStateList(StateList.getStateList(ds));
		return insuranceRequestHolder;
	}

	/**
	 * Gets the return to.
	 * 
	 * @param ds
	 *            the ds
	 * 
	 * @return the return to
	 */
	private static InsuranceRequestReturn getReturnTo(DataSource ds) {
		InsuranceRequestReturn insuranceRequestReturn = new InsuranceRequestReturn();

		return insuranceRequestReturn;
	}

	/**
	 * Gets the details.
	 * 
	 * @param jobId
	 *            the job id
	 * @param ds
	 *            the ds
	 * 
	 * @return the details
	 */
	private static InsuranceRequestDetails getDetails(String jobId,
			DataSource ds) {
		InsuranceRequestDetails insuranceRequestDetails = new InsuranceRequestDetails();
		insuranceRequestDetails.setStateList(StateList.getStateList(ds));
		ScheduleInfo scheduleInfo = JobDAO.getScheduleInfo(Long
				.parseLong(jobId), ds);

		insuranceRequestDetails.setScheduleStart(IlexTimestamp
				.converToTimestamp(scheduleInfo.getStartDate()).getDate());
		insuranceRequestDetails.setScheduleEnd(IlexTimestamp.converToTimestamp(
				scheduleInfo.getEndDate()).getDate());

		SiteInfo siteInfo = JobDAO.getSiteInfo(jobId, ds);
		insuranceRequestDetails.setSiteNumber(siteInfo.getSite_number());
		insuranceRequestDetails.setAddress(siteInfo.getSite_address());
		insuranceRequestDetails.setCity(siteInfo.getSite_city());
		insuranceRequestDetails.setState(siteInfo.getSite_state() + ","
				+ siteInfo.getSite_country());
		insuranceRequestDetails.setZipCode(siteInfo.getSite_zip());
		return insuranceRequestDetails;
	}

	/**
	 * Gets the email content.
	 * 
	 * @param filePath
	 *            the file path
	 * @return the email content
	 */
	public static String getEmailContent(InsuranceRequestBean bean,
			String filePath) {
		StringBuffer mailContent = new StringBuffer();
		String content = "";
		BufferedReader reader = null;
		FileInputStream stream = null;
		try {
			stream = new FileInputStream(new File(filePath));
			reader = new BufferedReader(new InputStreamReader(stream));
			while ((content = reader.readLine()) != null) {
				if (content.trim().equals("")) {
				} else if (content.trim().equals(
						"&lt;&lt;Contact Person&lt;&lt;")) {
					mailContent.append(bean.getContactPerson());
				} else if (content.trim()
						.equals("&lt;&lt;Today's Date&lt;&lt;")) {
					mailContent.append(bean.getTodayDate());
				} else if (content.trim().equals(
						"&lt;&lt;Attention Name&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestAttention()
							.getAttention());
				} else if (content.trim().equals(
						"&lt;&lt;Attention Email&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestAttention()
							.getAttentionEmailTo());
				} else if (content.trim().equals(
						"&lt;&lt;Attention Direct&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestAttention()
							.getAttentionDirect());
				} else if (content.trim().equals(
						"&lt;&lt;Attention Fax&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestAttention()
							.getAttentionFax());
				} else if (content.trim().equals(
						"&lt;&lt;Attention Additional CC&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestAttention()
							.getAttentionAdditionalCC());
				} else if (content.trim().equals("&lt;&lt;Holder Name&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestHolder()
							.getHolderName());
				} else if (content.trim().equals(
						"&lt;&lt;Holder Address&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestHolder()
							.getHolderAddress());
				} else if (content.trim().equals("&lt;&lt;Holder City&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestHolder()
							.getHolderCity());
				} else if (content.trim()
						.equals("&lt;&lt;Holder State&lt;&lt;")) {
					mailContent.append(getState(bean
							.getInsuranceRequestHolder().getHolderState()));
				} else if (content.trim().equals(
						"&lt;&lt;Holder Zip Code&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestHolder()
							.getHolderZipCode());
				} else if (content.trim().equals("&lt;&lt;Ret Name&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestReturn()
							.getRetName());
				} else if (content.trim().equals("&lt;&lt;Ret Email&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestReturn()
							.getRetEmail());
				} else if (content.trim().equals(
						"&lt;&lt;Ret Direct Number&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestReturn()
							.getRetDirectNumber());
				} else if (content.trim().equals("&lt;&lt;Ret Fax&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestReturn()
							.getRetFax());
				} else if (content.trim().equals(
						"&lt;&lt;Schedule Start&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestDetails()
							.getScheduleStart());
				} else if (content.trim()
						.equals("&lt;&lt;Schedule End&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestDetails()
							.getScheduleEnd());
				} else if (content.trim().equals("&lt;&lt;Site Number&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestDetails()
							.getSiteNumber());
				} else if (content.trim().equals("&lt;&lt;Address&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestDetails()
							.getAddress());
				} else if (content.trim().equals("&lt;&lt;City&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestDetails()
							.getCity());
				} else if (content.trim().equals("&lt;&lt;State&lt;&lt;")) {
					mailContent.append(getState(bean
							.getInsuranceRequestDetails().getState()));
				} else if (content.trim().equals("&lt;&lt;Zip Code&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestDetails()
							.getZipCode());
				} else if (content.trim().equals(
						"&lt;&lt;Insurance Requirements&lt;&lt;")) {
					mailContent.append(bean.getInsuranceRequestDetails()
							.getInsuranceRequirements());
				} else {
					mailContent.append(content);
				}
				mailContent.append("\n");
			}
		} catch (Exception fe) {
			logger
					.error(
							"getEmailContent(String, String, String, String, String, String, String, DataSource, String)",
							fe);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (stream != null) {
					stream.close();
				}
			} catch (Exception e) {
				logger
						.warn(
								"getEmailContent(String, String, String, String, String, String, String, DataSource, String) - exception ignored",
								e);
			}
		}
		return mailContent.toString();
	}

	/**
	 * Gets the state.
	 * 
	 * @param str
	 *            the str
	 * @return the state
	 */
	private static String getState(String str) {
		if (str != null && str.indexOf(",") > -1) {
			String[] countyState = str.split(",");
			str = countyState[0];
		}
		return str;
	}

	public static void saveSendAction(InsuranceRequestBean bean, DataSource ds) {
		InsuranceRequestDao.saveSendAction(bean.getJobId(), bean
				.getLoginUserId(), ds);
	}
}
