package com.mind.dms;

public interface DocManager {

	public void generate(Object obj);
	public void update(Object obj);
}

