package com.java4less.rchart;

import java.util.Enumeration;
import java.util.Vector;

import com.java4less.rchart.gc.ChartColor;
import com.java4less.rchart.gc.ChartFont;
import com.java4less.rchart.gc.ChartGraphics;
import com.java4less.rchart.gc.ChartImage;
import com.java4less.rchart.gc.GraphicsProvider;

// Referenced classes of package com.java4less.rchart:
//            ChartComponent, Chart, ChartLabel, LineStyle, 
//            FillStyle

public class Legend extends ChartComponent {

	Vector items;
	Vector names;
	public FillStyle background;
	public LineStyle border;
	public int legendMargin;
	public String title;
	public ChartColor color;
	public ChartFont font;
	public boolean verticalLayout;
	public String legendLabel;

	public Legend() {
		items = new Vector(10, 10);
		names = new Vector(10, 10);
		legendMargin = 8;
		title = null;
		color = GraphicsProvider.getColor(ChartColor.BLACK);
		font = GraphicsProvider.getFont("Arial", ChartFont.PLAIN, 16);
		verticalLayout = true;
		legendLabel = "";
		if (Chart.d() != 1) {
			addItem("RChart demo", null);
		} else if (title != null) {
			addItem(title, null);
		}
	}

	public Legend(String val) {
		items = new Vector(10, 10);
		names = new Vector(10, 10);
		legendMargin = 8;
		title = null;
		color = GraphicsProvider.getColor(ChartColor.BLACK);
		font = GraphicsProvider.getFont("Arial", ChartFont.PLAIN, 10);
		verticalLayout = true;
		legendLabel = "";
		if (Chart.d() != 1) {
			addItem(val, null);
		} else if (title != null) {
			addItem(title, null);
		}
	}

	public void addItem(String name, Object icon) {
		items.addElement(icon);
		names.addElement(name);
	}

	public void draw(ChartGraphics g) {
		if (legendLabel != null && legendLabel.length() > 0) {
			ChartLabel cl = new ChartLabel(legendLabel, "", false, true);
			cl.initialize(g, super.chart);
			cl.paint(g, super.x, super.y, super.width, super.height);
			return;
		}
		if (verticalLayout) {
			drawVertical(g);
		} else {
			drawHorizontal(g);
		}
	}

	public void drawHorizontal(ChartGraphics g) {
		g.setFont(font);
		int textWidth = 0;
		int iconWidth = 0;
		int totalWidth = 0;
		int iconHeight = 0;
		int w = 0;
		int h = 0;
		int textHeight = g.getFontHeight();
		int iconSeparator = 3;
		int textSeparator = 5;
		for (Enumeration e = names.elements(); e.hasMoreElements();) {
			String s = (String) e.nextElement();
			w = g.getFontWidth(s);
			if (w > textWidth) {
				textWidth = w;
			}
		}

		totalWidth = (textWidth + textSeparator) * names.size();
		for (Enumeration e = items.elements(); e.hasMoreElements();) {
			Object o = e.nextElement();
			w = 0;
			h = 0;
			if (o instanceof LineStyle) {
				w = 10;
				h = 10;
			}
			if (o instanceof FillStyle) {
				w = 10;
				h = 10;
			}
			if (o instanceof ChartImage) {
				w = ((ChartImage) o).getWidth();
				h = ((ChartImage) o).getHeight();
			}
			if (w > iconWidth) {
				iconWidth = w;
			}
			if (h > iconHeight) {
				iconHeight = h;
			}
		}

		totalWidth += (iconWidth + iconSeparator) * names.size();
		int itemHeight = textHeight;
		if (iconHeight > itemHeight) {
			itemHeight = iconHeight;
		}
		int toCenterX = (super.width - totalWidth) / 2;
		int toCenterY = (super.height - itemHeight) / 2;
		if (toCenterY < 0) {
			toCenterY = 0;
		}
		if (toCenterX < 0) {
			toCenterX = 0;
		}
		int legendX1 = super.x + toCenterX;
		int legendY1 = super.y + toCenterY;
		int legendX2 = super.x + toCenterX + totalWidth;
		int legendY2 = super.y + toCenterY + itemHeight;
		if (background != null) {
			background.draw(g, legendX1 - legendMargin,
					legendY1 - legendMargin, legendX2 + legendMargin, legendY2
							+ legendMargin);
		}
		if (border != null) {
			border.drawRect(g, legendX1 - legendMargin,
					legendY1 - legendMargin, legendX2 + legendMargin, legendY2
							+ legendMargin);
		}
		int offset = 0;
		for (int i = 1; i <= names.size(); i++) {
			g.setColor(color);
			g.drawString((String) names.elementAt(i - 1), toCenterX + offset
					+ iconWidth + iconSeparator + super.x, toCenterY + super.y
					+ itemHeight);
			offset = offset + iconWidth + iconSeparator + textWidth
					+ textSeparator;
		}

		offset = 0;
		for (int i = 1; i <= names.size(); i++) {
			Object icon = items.elementAt(i - 1);
			if (icon instanceof ChartImage) {
				g.drawImage((ChartImage) icon, toCenterX + super.x + offset,
						toCenterY + super.y);
			}
			if (icon instanceof LineStyle) {
				LineStyle l = (LineStyle) icon;
				l.draw(g, toCenterX + super.x + offset, toCenterY + super.y
						+ iconHeight / 2,
						((toCenterX + super.x + iconWidth) - 2) + offset,
						toCenterY + super.y + iconHeight / 2);
			}
			if (icon instanceof FillStyle) {
				int sidelentgh = iconWidth / 2;
				FillStyle f = (FillStyle) icon;
				f.draw(g, toCenterX + super.x + offset, toCenterY + super.y,
						toCenterX + super.x + offset + sidelentgh, toCenterY
								+ super.y + sidelentgh);
			}
			offset = offset + iconWidth + iconSeparator + textWidth
					+ textSeparator;
		}

	}

	public void drawVertical(ChartGraphics g) {
		g.setFont(font);
		int textWidth = 0;
		int iconWidth = 0;
		int iconHeight = 0;
		int w = 0;
		int h = 0;
		int textHeight = g.getFontHeight();
		for (Enumeration e = names.elements(); e.hasMoreElements();) {
			String s = (String) e.nextElement();
			w = g.getFontWidth(s);
			if (w > textWidth) {
				textWidth = w;
			}
		}

		for (Enumeration e = items.elements(); e.hasMoreElements();) {
			Object o = e.nextElement();
			w = 0;
			h = 0;
			if (o instanceof LineStyle) {
				w = 10;
				h = 10;
			}
			if (o instanceof FillStyle) {
				w = 10;
				h = 10;
			}
			if (o instanceof ChartImage) {
				w = ((ChartImage) o).getWidth();
				h = ((ChartImage) o).getHeight();
			}
			if (w > iconWidth) {
				iconWidth = w;
			}
			if (h > iconHeight) {
				iconHeight = h;
			}
		}

		int itemHeight = textHeight;
		if (iconHeight > itemHeight) {
			itemHeight = iconHeight;
		}
		int toCenterX = (super.width - (iconWidth + textWidth)) / 2;
		int toCenterY = (super.height - names.size() * itemHeight) / 2;
		if (toCenterY < 0) {
			toCenterY = 0;
		}
		if (toCenterX < 0) {
			toCenterX = 0;
		}
		int legendX1 = super.x + toCenterX;
		int legendY1 = super.y + toCenterY;
		int legendX2 = super.x + toCenterX + iconWidth + textWidth;
		int legendY2 = super.y + toCenterY + names.size() * itemHeight;
		if (background != null) {
			background.draw(g, legendX1 - legendMargin,
					legendY1 - legendMargin, legendX2 + legendMargin, legendY2
							+ legendMargin);
		}
		if (border != null) {
			border.drawRect(g, legendX1 - legendMargin,
					legendY1 - legendMargin, legendX2 + legendMargin, legendY2
							+ legendMargin);
		}
		for (int i = 1; i <= names.size(); i++) {
			g.setColor(color);
			g.drawString((String) names.elementAt(i - 1), toCenterX + iconWidth
					+ super.x, toCenterY + super.y + i * itemHeight);
		}

		for (int i = 1; i <= names.size(); i++) {
			Object icon = items.elementAt(i - 1);
			if (icon instanceof ChartImage) {
				g.drawImage((ChartImage) icon, toCenterX + super.x, toCenterY
						+ super.y + (i - 1) * itemHeight);
			}
			if (icon instanceof LineStyle) {
				LineStyle l = (LineStyle) icon;
				l.draw(g, toCenterX + super.x, toCenterY + super.y + iconHeight
						/ 2 + (i - 1) * itemHeight,
						(toCenterX + super.x + iconWidth) - 2, toCenterY
								+ super.y + iconHeight / 2 + (i - 1)
								* itemHeight);
			}
			if (icon instanceof FillStyle) {
				int sidelentgh = iconWidth / 2;
				FillStyle f = (FillStyle) icon;
				f.draw(g, toCenterX + super.x, toCenterY + super.y + itemHeight
						/ 2 + (i - 1) * itemHeight, toCenterX + super.x
						+ sidelentgh, toCenterY + super.y + itemHeight / 2
						+ (i - 1) * itemHeight + sidelentgh);
			}
		}

	}
}
