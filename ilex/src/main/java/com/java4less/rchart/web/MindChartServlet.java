package com.java4less.rchart.web;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import sun.misc.BASE64Encoder;

import com.java4less.rchart.Chart;
import com.java4less.rchart.ChartLoader;

public class MindChartServlet extends HttpServlet
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MindChartServlet.class);

	private static final long serialVersionUID = 3545521694417368377L;
	private boolean debug;

    public MindChartServlet()
    {
        debug = false;
    }

    public void init()
        throws ServletException
    {
    }

    private Chart getChart(HttpServletRequest request)
    {
		if(debug)
        {
			if (logger.isDebugEnabled()) {
				logger.debug("getChart(HttpServletRequest) - Headless Mode is: " + System.getProperty("java.awt.headless"));
			}
        }
        if(debug)
        {
			if (logger.isDebugEnabled()) {
				logger.debug("getChart(HttpServletRequest) - Current directory is: " + (new File("dummy")).getAbsolutePath());
			}
        }
        if(debug)
        {
			if (logger.isDebugEnabled()) {
				logger.debug("getChart(HttpServletRequest) - User directory: " + request.getRequestURI() + " " + request.getPathTranslated() + " " + request.getPathInfo());
			}
        }
        ChartLoader loader = new ChartLoader();
        loader.paintDirect = true;
        String dataFile = null;
        String fileEncoding = "";
        String user = "";
        String psw = "";
        String delim = null;
        if(request != null)
        {
            boolean isUri = false;
            if(request.getParameter("DEBUG") != null && request.getParameter("DEBUG").toUpperCase().compareTo("ON") == 0)
            {
                debug = true;
            }
            for(Enumeration ps = request.getParameterNames(); ps.hasMoreElements();)
            {
                String name = (String)ps.nextElement();
                if(!isUri)
                {
                    loader.setParameter(name, request.getParameter(name));
                    if(name.compareTo("DATAFILE") == 0)
                    {
                        dataFile = request.getParameter(name);
                    }
                    if(name.compareTo("PSW") == 0)
                    {
                        psw = request.getParameter(name);
                    }
                    if(name.compareTo("USER") == 0)
                    {
                        user = request.getParameter(name);
                    }
                    if(name.compareTo("FILEENCODING") == 0)
                    {
                        fileEncoding = request.getParameter(name);
                    }
                    if(name.compareTo("ISURI") == 0)
                    {
                        dataFile = request.getParameter(name);
                        isUri = true;
                        delim = "?";
                    }
                    if(debug)
                    {
						if (logger.isDebugEnabled()) {
							logger.debug("getChart(HttpServletRequest) - PARAM: " + name + "=" + request.getParameter(name));
						}
                    }
                } else
                {
                    dataFile = dataFile + delim + name + "=" + request.getParameter(name);
                    delim = "&";
                }
            }

            if(debug)
            {
				if (logger.isDebugEnabled()) {
					logger.debug("getChart(HttpServletRequest) - DATAFILE: " + dataFile);
				}
            }
        }
        String encodedUserPsw = "";
        if(user.length() > 0)
        {
            String userPassword = user + ":" + psw;
            encodedUserPsw = (new BASE64Encoder()).encode(userPassword.getBytes());
            if(debug)
            {
				if (logger.isDebugEnabled()) {
					logger.debug("getChart(HttpServletRequest) - Aut. :" + encodedUserPsw);
				}
            }
        }
        File f = new File("a.txt");
        if(debug)
        {
			if (logger.isDebugEnabled()) {
				logger.debug("getChart(HttpServletRequest) - " + f.getAbsolutePath());
			}
        }
        if(dataFile != null)
        {
            if(debug)
            {
				if (logger.isDebugEnabled()) {
					logger.debug("getChart(HttpServletRequest) - file encoding " + fileEncoding);
				}
            }
            loader.fileEncoding = fileEncoding;
            if(debug)
            {
				if (logger.isDebugEnabled()) {
					logger.debug("getChart(HttpServletRequest) - loading file " + dataFile);
				}
            }
            loader.loadFromFile(dataFile, false, encodedUserPsw);
        }
        else
		{
						
			String []chartparamname=new String[new Integer(request.getParameter("LENGTH")).intValue()];
			String []chartparamvalue=new String[new Integer(request.getParameter("LENGTH")).intValue()];
			int i=0;
			
			for(Enumeration ps = request.getParameterNames(); ps.hasMoreElements();)
	        {
	            String name = (String)ps.nextElement();
				if((name.equals("WIDTH"))||(name.equals("HEIGHT"))||(name.equals("LENGTH"))||(name.equals("LEGEND_TITLE")))
				{}
				else
				{
					chartparamname[i]=name;
					chartparamvalue[i]=request.getParameter(name);
					i++;
					
				}
			}
			loader.setvalues(chartparamname,chartparamvalue,request.getParameter("LEGEND_TITLE"));
		}
		Chart chart = loader.build(false, false);
        if(debug)
        {
			if (logger.isDebugEnabled()) {
				logger.debug("getChart(HttpServletRequest) - Chart Built");
			}
        }
        return chart;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        
		String encode = "jpeg";
        if(request != null)
        {
            if(request.getParameter("FORMAT") != null)
            {
                encode = request.getParameter("FORMAT").toLowerCase();
            }
            if(encode.toLowerCase().compareTo("gif") != 0 && encode.toLowerCase().compareTo("png") != 0)
            {
                encode = "jpeg";
            }
        }
        response.setContentType("image/" + encode);
        javax.servlet.ServletOutputStream outb = response.getOutputStream();
        response.setDateHeader("Expires", 0L);
        try
        {
            int w = 300;
            int h = 300;
            if(request != null)
            {
                if(request.getParameter("WIDTH") != null)
                {
                    w = (new Integer(request.getParameter("WIDTH"))).intValue();
                }
                if(request.getParameter("HEIGHT") != null)
                {
                    h = (new Integer(request.getParameter("HEIGHT"))).intValue();
                }
            }
            Chart c = getChart(request);
            if(debug)
            {
				if (logger.isDebugEnabled()) {
					logger.debug("doGet(HttpServletRequest, HttpServletResponse) - Size: " + w + " " + h);
				}
            }
            c.setSize(w, h);
            c.doubleBuffering = false;
            if(debug)
            {
				if (logger.isDebugEnabled()) {
					logger.debug("doGet(HttpServletRequest, HttpServletResponse) - Double buf. " + c.doubleBuffering);
				}
            }
            c.saveToFile(outb, encode);
            outb.close();
        }
        catch(Exception e)
        {
			logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("doGet(HttpServletRequest, HttpServletResponse) - " + e.getMessage());
			}
			logger.error("doGet(HttpServletRequest, HttpServletResponse)", e);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException
    {
        try
        {
            doGet(request, response);
        }
        catch(Exception e)
        {
			logger.error("doPost(HttpServletRequest, HttpServletResponse)", e);

			logger.error("doPost(HttpServletRequest, HttpServletResponse)", e);
        }
    }
}
