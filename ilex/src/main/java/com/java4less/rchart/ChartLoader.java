package com.java4less.rchart;

import java.applet.Applet;
import java.awt.Component;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.java4less.rchart.gc.ChartColor;
import com.java4less.rchart.gc.ChartFont;
import com.java4less.rchart.gc.GraphicsProvider;

// Referenced classes of package com.java4less.rchart:
//            DataSerie, ParamInput, JDBCSource, LineStyle, 
//            FillStyle, Axis, Scale, LogScale, 
//            AxisTargetZone, Legend, LineDataSerie, Title, 
//            HAxisLabel, AxisLabel, VAxisLabel, LinePlotter, 
//            CurvePlotter, Plotter, MaxMinDataSerie, LinePlotter3D, 
//            RadarPlotter, BarDataSerie, BarPlotter3D, BarPlotter, 
//            PieDataSerie, PiePlotter, GaugeDataSerie, GaugePlotter, 
//            Chart, TargetZone

public class ChartLoader
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ChartLoader.class);

    public String fileEncoding;
    private Applet chartApplet;
    private JDBCSource jdbcsource;
    private Component parentComponent;
    public Chart gChart;
    public boolean paintDirect;
    String ptitle;
    String ptitleFont;
    String ptitleColor;
    String Y2label;
    String Y2labelFont;
    String Y2labelColor;
    String Xlabel;
    String Ylabel;
    String XlabelFont;
    String YlabelFont;
    String XlabelColor;
    String YlabelColor;
    double pchartLeftMargin;
    double pchartTopMargin;
    double pchartLegendMargin;
    double pchartBottomMargin;
    double pchartSecondYAxisMargin;
    String plegendPosition;
    String pSerie;
    public String pSeriesNames[];
    String pSeriesData[];
    String pSeriesMaxData[];
    String pSeriesMinData[];
    String pSeriesCloseData[];
    public DataSerie pSeries[];
    public int pnumSeries;
    boolean plegend;
    String loadedParameters[];
    String loadedValues[];
    int loadedParametersCount;
    String label;
	public boolean promptForParameters;
    private String dataFile;
    private String SQLparams[];
    private String SQLparamsV[];
    private int SQLparamCount;
	
    public ChartLoader(Applet ap)
    {
        fileEncoding = "";
        jdbcsource = null;
        parentComponent = null;
        gChart = null;
        paintDirect = true;
        pSeriesNames = new String[50];
        pSeriesData = new String[50];
        pSeriesMaxData = new String[50];
        pSeriesMinData = new String[50];
        pSeriesCloseData = new String[50];
        pSeries = new DataSerie[50];
        pnumSeries = 0;
        loadedParameters = new String[1500];
        loadedValues = new String[1500];
        loadedParametersCount = 0;
        promptForParameters = false;
        dataFile = "";
        SQLparams = new String[10];
        SQLparamsV = new String[10];
        SQLparamCount = 0;
        chartApplet = ap;
        loadedParametersCount = 0;
    }

    public ChartLoader()
    {
        fileEncoding = "";
        jdbcsource = null;
        parentComponent = null;
        gChart = null;
        paintDirect = true;
        pSeriesNames = new String[50];
        pSeriesData = new String[50];
        pSeriesMaxData = new String[50];
        pSeriesMinData = new String[50];
        pSeriesCloseData = new String[50];
        pSeries = new DataSerie[50];
        pnumSeries = 0;
        loadedParameters = new String[1500];
        loadedValues = new String[1500];
        loadedParametersCount = 0;
        promptForParameters = false;
        dataFile = "";
        SQLparams = new String[10];
        SQLparamsV = new String[10];
        SQLparamCount = 0;
        loadedParametersCount = 0;
    }

    private String getSQLParameterValue(String param)
    {
        for(int i = 0; i < SQLparamCount; i++)
        {
            if(SQLparams[i].compareTo(param) == 0)
            {
                return SQLparamsV[i];
            }
        }

        return null;
    }

    private String repSQLParameters(String s)
    {
        int p1 = s.indexOf("[%");
        if(p1 == -1)
        {
            return s;
        }
        for(int p2 = s.indexOf("]", p1); p1 >= 0 && p2 >= 0 && p2 > p1; p2 = s.indexOf("]", p1))
        {
            String param = s.substring(p1 + 2, p2);
            String value = getSQLParameterValue(param);
            if(value == null && promptForParameters)
            {
                ParamInput pi = new ParamInput((Frame)parentComponent, param);
                pi.show();
                if(!pi.cancelled)
                {
                    value = pi.result;
                }
                pi = null;
            }
            if(value != null)
            {
                s = s.substring(0, p1) + value + s.substring(p2 + 1, s.length());
            }
            p1 = s.indexOf("[%", p2 + 1);
            if(p1 == -1)
            {
                return s;
            }
        }

        return s;
    }

    public void setSQLParameter(String param, String value)
    {
        for(int i = 0; i < SQLparamCount; i++)
        {
            if(SQLparams[i].compareTo(param) == 0)
            {
                SQLparamsV[i] = value;
                return;
            }
        }

        SQLparams[SQLparamCount] = param;
        SQLparamsV[SQLparamCount] = value;
        SQLparamCount++;
    }

    protected static boolean convertBooleanParam(String s, boolean def)
    {
        try
        {
            if(s.compareTo("") == 0)
            {
                return def;
            }
            if(s.toUpperCase().compareTo("N") == 0)
            {
                return false;
            }
            if(s.toUpperCase().compareTo("NO") == 0)
            {
                return false;
            }
            if(s.toUpperCase().compareTo("0") == 0)
            {
                return false;
            }
            if(s.toUpperCase().compareTo("FALSE") == 0)
            {
                return false;
            }
            if(s.toUpperCase().compareTo("Y") == 0)
            {
                return true;
            }
            if(s.toUpperCase().compareTo("YES") == 0)
            {
                return true;
            }
            if(s.toUpperCase().compareTo("TRUE") == 0)
            {
                return true;
            }
            if(s.toUpperCase().compareTo("1") == 0)
            {
                return true;
            } else
            {
                return Boolean.valueOf(s).booleanValue();
            }
        }
        catch(Exception e)
        {
			logger.error("convertBooleanParam(String, boolean)", e);

            return def;
        }
    }

    public boolean getBooleanParam(String Param, boolean def)
    {
        String s = getParameter(Param, "");
        return convertBooleanParam(s, def);
    }

    private String replaceStr(String s, String sub1, String sub2)
    {
        for(int p = s.indexOf(sub1); p >= 0; p = s.indexOf(sub1))
        {
            s = s.substring(0, p) + sub2 + s.substring(p + sub1.length(), s.length());
        }

        return s;
    }

    protected Date getDateParam(String Param, String def)
    {
        try
        {
            String s = getParameter(Param, "");
            if(s.compareTo("") == 0)
            {
                s = def;
            }
            if(s.compareTo("") == 0)
            {
                return null;
            }
            s = replaceStr(s, "/", "-");
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            if(s.length() > 10)
            {
                df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            }
            return df.parse(s);
        }
        catch(Exception e)
        {
			logger.error("getDateParam(String, String)", e);

			logger.error("getDateParam(String, String)", e);
        }
        return null;
    }

    protected Integer getIntParam(String Param, Integer def)
    {
        try
        {
            String s = getParameter(Param, "");
            if(s.compareTo("") == 0)
            {
                return def;
            } else
            {
                return new Integer(s);
            }
        }
        catch(Exception e)
        {
			logger.error("getIntParam(String, Integer)", e);

            return def;
        }
    }

    public Double getDoubleParam(String Param, Double def)
    {
        try
        {
            String s = getParameter(Param, "");
            if(s.compareTo("") == 0)
            {
                return def;
            } else
            {
                return new Double(s);
            }
        }
        catch(Exception e)
        {
			logger.error("getDoubleParam(String, Double)", e);

            return def;
        }
    }

    private String getStringParam(String Param, String def)
    {
        return getParameter(Param, def);
    }

    private String getParsedValue(String s)
    {
        if(jdbcsource == null)
        {
            return s;
        }
        if(s.indexOf("JDBC:") == 0)
        {
            s = repSQLParameters(s);
            s = jdbcsource.executeQuery(s.substring(5, s.length()));
        }
        return s;
    }

    public String getParameter(String key, String def)
    {
        String v = "";
        for(int i = 0; i < loadedParametersCount; i++)
        {
            if(loadedParameters[i].compareTo(key) == 0)
            {
                v = loadedValues[i];
                if(v.length() == 0)
                {
                    return def;
                } else
                {
                    return getParsedValue(v);
                }
            }
        }

        if(chartApplet != null)
        {
            v = chartApplet.getParameter(key);
        }
        if(v == null)
        {
            return def;
        }
        if(v.length() == 0)
        {
            return def;
        } else
        {
            return v;
        }
    }

    public static ChartColor convertColor(String s)
    {
        return GraphicsProvider.getColor(s);
    }

    protected static ChartFont convertFont(String f)
    {
        String items[] = convertList(f, "|");
        if(items == null)
        {
            return null;
        }
        if(items.length < 3)
        {
            return null;
        }
        int s = ChartFont.PLAIN;
        if(items[1].compareTo("BOLD") == 0)
        {
            s = ChartFont.BOLD;
        }
        if(items[1].compareTo("ITALIC") == 0)
        {
            s = ChartFont.ITALIC;
        }
        try
        {
            return GraphicsProvider.getFont(items[0], s, (new Integer(items[2])).intValue());
        }
        catch(Exception e)
        {
			logger.error("convertFont(String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("convertFont(String) - Error converting font " + f + " " + e.getMessage());
			}
        }
        return null;
    }

    protected static String[] convertList(String items)
    {
        return convertList(items, "|");
    }

    protected static String[] convertList(String items, String separator)
    {
        int elements = 1;
        for(int p = items.indexOf(separator); p >= 0; p = items.indexOf(separator, p + 1))
        {
            elements++;
        }

        String itema[] = new String[elements];
        int itemCount = 0;
        for(int p = items.indexOf(separator); p >= 0; p = items.indexOf(separator))
        {
            itema[itemCount++] = items.substring(0, p);
            items = items.substring(p + separator.length(), items.length());
        }

        if(items.compareTo("") != 0)
        {
            itema[itemCount++] = items;
        }
        if(itemCount == 0)
        {
            return null;
        }
        String result[] = new String[itemCount];
        for(int i = 0; i < itemCount; i++)
        {
            result[i] = itema[i];
        }

        return result;
    }

    private String[] getItemsParameter(String key)
    {
        String items = getStringParam(key, "");
        return convertList(items);
    }

    private double[] convertDoubleList(String s)
    {
        String items[] = convertList(s);
        if(items == null)
        {
            return null;
        }
        double d[] = new double[items.length];
        for(int i = 0; i < items.length; i++)
        {
            try
            {
                d[i] = (new Double(items[i])).doubleValue();
            }
            catch(Exception e)
            {
				logger.error("convertDoubleList(String)", e);

                d[i] = 0.0D;
            }
        }

        return d;
    }

    private int[] convertIntList(String s)
    {
        String items[] = convertList(s);
        int d[] = new int[items.length];
        for(int i = 0; i < items.length; i++)
        {
            try
            {
                d[i] = (new Integer(items[i])).intValue();
            }
            catch(Exception e)
            {
				logger.error("convertIntList(String)", e);

                d[i] = 0;
            }
        }

        return d;
    }

    private Double[] convertDoubleListWithNulls(String s)
    {
        String items[] = convertList(s);
        if(items == null)
        {
            return new Double[0];
        }
        Double d[] = new Double[items.length];
        for(int i = 0; i < items.length; i++)
        {
            try
            {
                if(items[i].toUpperCase().compareTo("NULL") == 0)
                {
                    d[i] = null;
                } else
                {
                    d[i] = new Double(items[i]);
                }
            }
            catch(Exception e)
            {
				logger.error("convertDoubleListWithNulls(String)", e);

                d[i] = new Double(0.0D);
            }
        }

        return d;
    }

    private boolean[] convertBooleanList(String s)
    {
        String items[] = convertList(s);
        boolean d[] = new boolean[items.length];
        for(int i = 0; i < items.length; i++)
        {
            try
            {
                d[i] = items[i].compareTo("0") != 0 && items[i].toUpperCase().compareTo("FALSE") != 0;
            }
            catch(Exception e)
            {
				logger.error("convertBooleanList(String)", e);

                d[i] = false;
            }
        }

        return d;
    }

    protected static LineStyle convertLineStyle(String f)
    {
        return LineStyle.createFromString(f);
    }

    protected static FillStyle convertFillStyle(String f)
    {
        return FillStyle.createFromString(f);
    }

    private String convertFromDates(String v, String dateStep, Date initialDate)
    {
        String vs[] = convertList(v);
        String result = "";
        for(int i = 0; i < vs.length; i++)
        {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            if(vs[i].length() > 10)
            {
                df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            }
            Date d = initialDate;
            vs[i] = replaceStr(vs[i], "/", "-");
            try
            {
                d = df.parse(vs[i]);
            }
            catch(Exception e)
            {
				logger.error("convertFromDates(String, String, Date)", e);

				logger.error("convertFromDates(String, String, Date)", e);
            }
            double diff2 = Axis.convertFromDate(d, dateStep, initialDate);
            if(i > 0)
            {
                result = result + "|";
            }
            diff2 = Math.floor(diff2 * 100D) / 100D;
            result = result + diff2;
        }

        return result;
    }

    private Axis loadAxisAndScale(String name, int type)
    {
        Scale sc = new Scale();
        if(getBooleanParam(name + "SCALE_LOG", false))
        {
            sc = new LogScale();
        }
        int iTmp = getIntParam(name + "SCALE_LOG_BASE", new Integer(2)).intValue();
        if(iTmp != 2 && (sc instanceof LogScale))
        {
            ((LogScale)sc).base = iTmp;
        }
        Double scaleMin = getDoubleParam(name + "SCALE_MIN", null);
        Double scaleMax = getDoubleParam(name + "SCALE_MAX", null);
        if(scaleMin != null)
        {
            sc.min = scaleMin.doubleValue();
        }
        if(scaleMax != null)
        {
            sc.max = scaleMax.doubleValue();
        }
        boolean bigTicksGrid = getBooleanParam("GRID" + name, false);
        Double scaleTickInterval = getDoubleParam("TICK_INTERVAL" + name, new Double(1.0D));
        Integer bigTickInterval = getIntParam("BIG_TICK_INTERVAL" + name, new Integer(5));
        int bigTickIntervalList[] = (int[])null;
        if(getStringParam("BIG_TICK_INTERVAL_LIST" + name, "").length() > 0)
        {
            bigTickIntervalList = convertIntList(getStringParam("BIG_TICK_INTERVAL_LIST" + name, ""));
        }
        String axisLabels = getStringParam(name + "AXIS_LABELS", "");
        sc.exactMaxValue = getBooleanParam(name + "SCALE_EXACT_MAX", false);
        sc.exactMinValue = getBooleanParam(name + "SCALE_EXACT_MIN", false);
        if(getStringParam(name + "SCALE_PREF_MAXMIN", "").length() > 0)
        {
            sc.preferred_MaxMin_values = convertDoubleList(getStringParam(name + "SCALE_PREF_MAXMIN", ""));
        }
        Axis axis = new Axis(type, sc);
        axis.bigTickInterval = bigTickInterval.intValue();
        axis.bigTickIntervalList = bigTickIntervalList;
        axis.scaleTickInterval = scaleTickInterval.doubleValue();
        axis.bigTicksGrid = bigTicksGrid;
        if(axisLabels.compareTo("") != 0)
        {
            axis.tickLabels = convertList(axisLabels);
        }
        loadAxis(axis, name);
        for(int subAxis = 1; getBooleanParam(name + "_" + subAxis + "AXIS", false); subAxis++)
        {
            axis.addAdditionalAxis(loadAxisAndScale(name + "_" + subAxis, type));
        }

        return axis;
    }

    private void loadAxis(Axis axis, String name)
    {
        axis.autoNumberOfTicks = getIntParam(name + "AXIS_AUTO_TICKS", new Integer(0)).intValue();
        if(getStringParam(name + "AXIS_PREF_TICK_INTERVAL", "").length() > 0)
        {
            axis.ticks_preferred_Interval = convertDoubleList(getStringParam(name + "AXIS_PREF_TICK_INTERVAL", ""));
        }
        if(getBooleanParam(name + "AXIS_START_WITH_BIG_TICK", false))
        {
            axis.startWithBigTick = true;
        }
        axis.logarithmicIntervals = getBooleanParam("TICK_LOG_INTERVAL" + name, false);
        axis.axisFrame = getBooleanParam(name + "AXIS_CLOSED", false);
        axis.xscaleOnTop = getBooleanParam(name + "AXIS_ON_TOP", false);
        axis.stackAdditionalAxis = getBooleanParam(name + "AXIS_STACK_ADDITIONAL", false);
        axis.labelTemplate = getStringParam(name + "AXIS_TEMPLATE", "");
        axis.label = getStringParam(name + "AXIS_LABEL", "");
        axis.dateLabelFormat = getStringParam(name + "AXIS_DATE_FORMAT", "dd-MMM-yyyy");
        axis.dateStep = getStringParam(name + "AXIS_DATE_STEP", "d");
        axis.initialDate = getDateParam(name + "AXIS_INITIAL_DATE", "");
        Date d = getDateParam(name + "AXIS_FINAL_DATE", "");
        if(d != null)
        {
            axis.scale.max = Axis.convertFromDate(d, axis.dateStep, axis.initialDate);
            axis.dateStepPerUnit = true;
        }
        axis.tickLabelLength = getIntParam(name + "AXIS_TICK_TEXT_LINE", new Integer(1000)).intValue();
        axis.tickPixels = (int)getDoubleParam(name + "AXIS_TICKPIXELS", new Double(axis.tickPixels)).doubleValue();
        axis.bigTickPixels = (int)getDoubleParam(name + "AXIS_BIGTICKPIXELS", new Double(axis.bigTickPixels)).doubleValue();
        String ceroAxis = getStringParam("CERO_XAXIS", "");
        if(ceroAxis.compareTo("LINE") == 0)
        {
            axis.ceroAxis = 0;
        }
        if(ceroAxis.compareTo("NO") == 0)
        {
            axis.ceroAxis = 1;
        }
        if(ceroAxis.compareTo("SCALE") == 0)
        {
            axis.ceroAxis = 2;
        }
        LineStyle lstyle = convertLineStyle(getStringParam("CERO_XAXIS_STYLE", ""));
        if(lstyle != null)
        {
            axis.ceroAxisStyle = lstyle;
        }
        lstyle = convertLineStyle(getStringParam(name + "AXIS_STYLE", ""));
        if(lstyle != null)
        {
            axis.style = lstyle;
        }
        lstyle = convertLineStyle(getStringParam(name + "AXIS_GRID", ""));
        if(lstyle != null)
        {
            axis.gridStyle = lstyle;
        }
        FillStyle fstyle = convertFillStyle(getStringParam(name + "AXIS_FILL_GRID", ""));
        if(fstyle != null)
        {
            axis.gridFillStyle = fstyle;
        }
        ChartColor c = convertColor(getStringParam(name + "AXIS_FONT_COLOR", ""));
        axis.DescColor = c;
        ChartFont font = convertFont(getStringParam(name + "AXIS_FONT", ""));
        if(font != null)
        {
            axis.DescFont = font;
        }
        axis.IntegerScale = getBooleanParam(name + "AXIS_INTEGER", true);
        axis.scaleLabelFormat = getStringParam(name + "AXIS_LABEL_FORMAT", "");
        axis.tickAtBase = getBooleanParam(name + "AXIS_TICKATBASE", false);
        axis.rotateLabels = getIntParam(name + "AXIS_ROTATE_LABELS", new Integer(0)).intValue();
        if(getBooleanParam(name + "AXIS_VERTICAL_LABELS", false))
        {
            axis.rotateLabels = 90;
        }
        fstyle = convertFillStyle(getStringParam(name + "AXIS_FILLING", ""));
        if(fstyle != null)
        {
            axis.barFilling = fstyle;
        }
        String tmp = getStringParam(name + "AXIS_BAR_STYLE", "NONE");
        if(tmp.equals("BAR"))
        {
            axis.barStyle = 1;
        }
        if(tmp.equals("RAISED"))
        {
            axis.barStyle = 2;
        }
        axis.barWidth = getIntParam(name + "AXIS_BAR_WIDTH", new Integer(axis.barWidth)).intValue();
        for(int zoneNr = 1; getParameter(name + "AXIS_TARGET_ZONE_" + zoneNr, "").length() > 0; zoneNr++)
        {
            axis.addTargetZone(AxisTargetZone.createFromString(getParameter(name + "AXIS_TARGET_ZONE_" + zoneNr, "")));
        }

    }

    private void setAxisForSerie(int i, Axis cXAxis, Axis cYAxis, DataSerie s)
    {
        int secAxis = getIntParam("SERIE_SECONDARY_XAXIS_" + i, new Integer(0)).intValue();
        if(secAxis > 0 && cXAxis.getAdditionalAxisCount() >= secAxis)
        {
            s.secondaryXAxis = cXAxis.getAdditionalAxis(secAxis - 1);
        }
        secAxis = getIntParam("SERIE_SECONDARY_YAXIS_" + i, new Integer(0)).intValue();
        if(secAxis > 0 && cYAxis.getAdditionalAxisCount() >= secAxis)
        {
            s.secondaryYAxis = cYAxis.getAdditionalAxis(secAxis - 1);
        }
    }

    private Chart buildChart(Chart currentChart)
    {
        ChartFont font = null;
        Title cTitle = null;
        HAxisLabel cXLabel = null;
        VAxisLabel cYLabel = null;
        VAxisLabel cY2Label = null;
        GaugePlotter gaugePlot = null;
        LinePlotter linePlot = null;
        LinePlotter3D linePlot3D = null;
        BarPlotter barPlot = null;
        BarPlotter3D barPlot3D = null;
        PiePlotter piePlot = null;
        RadarPlotter radarPlot = null;
        String jdbcdb = "";
        String jdbcdriver = "";
        String jdbcuser = "";
        String jdbcpwd = "";
        Axis cXAxis = null;
        Axis cYAxis = null;
        Axis cY2Axis = null;
        Legend clegend = new Legend(label);
        jdbcdb = getStringParam("JDBC_DATABASE", "");
        jdbcdriver = getStringParam("JDBC_DRIVER", "");
        jdbcuser = getStringParam("JDBC_USER", "");
        jdbcpwd = getStringParam("JDBC_PASSWORD", "");
        if(jdbcdb.length() > 0)
        {
            jdbcsource = new JDBCSource();
            jdbcsource.open(jdbcdb, jdbcdriver, jdbcuser, jdbcpwd);
        }
        LineDataSerie.startingXValue = getIntParam("LINECHART_START_VALUE_X", new Integer(0)).intValue();
        ptitle = getStringParam("TITLECHART", "");
        if(ptitle.compareTo("") == 0)
        {
            ptitle = getStringParam("TITLE", "");
        }
        if(ptitle.compareTo("") != 0)
        {
            cTitle = new Title(ptitle);
            ptitleFont = getStringParam("TITLE_FONT", "");
			
            font = convertFont(ptitleFont);
            if(font != null)
            {
                cTitle.font = font;
            }
            ptitleColor = getStringParam("TITLE_COLOR", "");
            if(ptitleColor != null)
            {
                cTitle.color = convertColor(ptitleColor);
            }
        }
        Xlabel = getStringParam("XLABEL", "");
        ChartColor c;
        if(Xlabel.compareTo("") != 0)
        {
            XlabelFont = getStringParam("XLABEL_FONT", "");
            font = convertFont(XlabelFont);
            if(font == null)
            {
                font = GraphicsProvider.getFont("Arial", ChartFont.BOLD, 12);
            }
            XlabelColor = getStringParam("XLABEL_COLOR", "");
            if(XlabelColor != null)
            {
                c = convertColor(XlabelColor);
            } else
            {
                c = GraphicsProvider.getColor(ChartColor.BLACK);
            }
            cXLabel = new HAxisLabel(Xlabel, c, font);
            cXLabel.vertical = getBooleanParam("XLABEL_VERTICAL", false);
        }
        Ylabel = getStringParam("YLABEL", "");
        if(Ylabel.compareTo("") != 0)
        {
            YlabelFont = getStringParam("YLABEL_FONT", "");
            font = convertFont(YlabelFont);
            if(font == null)
            {
                font = GraphicsProvider.getFont("Arial", ChartFont.BOLD, 12);
            }
            YlabelColor = getStringParam("YLABEL_COLOR", "");
            if(YlabelColor != null)
            {
                c = convertColor(YlabelColor);
            } else
            {
                c = GraphicsProvider.getColor(ChartColor.BLACK);
            }
            cYLabel = new VAxisLabel(Ylabel, c, font);
            cYLabel.vertical = getBooleanParam("YLABEL_VERTICAL", false);
        }
        Y2label = getStringParam("Y2LABEL", "");
        if(Y2label.compareTo("") != 0)
        {
            Y2labelFont = getStringParam("Y2LABEL_FONT", "");
            font = convertFont(Y2labelFont);
            if(font == null)
            {
                font = GraphicsProvider.getFont("Arial", ChartFont.BOLD, 12);
            }
            Y2labelColor = getStringParam("Y2LABEL_COLOR", "");
            if(Y2labelColor != null)
            {
                c = convertColor(Y2labelColor);
            } else
            {
                c = GraphicsProvider.getColor(ChartColor.BLACK);
            }
            cY2Label = new VAxisLabel(Y2label, c, font);
            cY2Label.vertical = getBooleanParam("Y2LABEL_VERTICAL", false);
        }
        boolean axisX = getBooleanParam("XAXIS", true);
        boolean axisY = getBooleanParam("YAXIS", true);
        boolean axisY2 = getBooleanParam("Y2AXIS", false);
        if(axisX)
        {
            cXAxis = loadAxisAndScale("X", 0);
        }
        if(axisY)
        {
            cYAxis = loadAxisAndScale("Y", 1);
        }
        if(axisY2)
        {
            cY2Axis = loadAxisAndScale("Y2", 1);
        }
        FillStyle fstyle = convertFillStyle(getStringParam("LEGEND_FILL", ""));
        if(fstyle != null)
        {
            clegend.background = fstyle;
        }
        LineStyle lstyle = convertLineStyle(getStringParam("LEGEND_BORDER", ""));
        if(lstyle != null)
        {
            clegend.border = lstyle;
        }
        c = convertColor(getStringParam("LEGEND_COLOR", ""));
        clegend.color = c;
        font = convertFont(getStringParam("LEGEND_FONT", ""));
        if(font != null)
        {
            clegend.font = font;
        }
        clegend.verticalLayout = getBooleanParam("LEGEND_VERTICAL", true);
        clegend.legendLabel = getStringParam("LEGEND_LABEL", "");
        pnumSeries = 0;
        for(int i = 1; i < 50; i++)
        {
            pSerie = getStringParam("SERIE_" + i, "");
            if(pSerie.compareTo("") == 0)
            {
                break;
            }
            pSeriesNames[i] = pSerie;
            pSeriesData[i] = getStringParam("SERIE_DATA_" + i, "");
            if(pSeriesData[i].indexOf("/") > 0)
            {
                pSeriesData[i] = convertFromDates(pSeriesData[i], cYAxis.dateStep, cYAxis.initialDate);
            }
            String Typ = getStringParam("SERIE_TYPE_" + i, "LINE");
            pSeries[i] = null;
            if(Typ.compareTo("LINE") == 0 || Typ.compareTo("B-SPLINES") == 0 || Typ.compareTo("CURVE") == 0 || Typ.compareTo("LINE_LEAST_SQUARES") == 0)
            {
                if(linePlot == null)
                {
                    if(Typ.compareTo("LINE") == 0)
                    {
                        linePlot = new LinePlotter();
                    } else
                    {
                        linePlot = new CurvePlotter();
                    }
                    linePlot.XScale = cXAxis.scale;
                    linePlot.YScale = cYAxis.scale;
                    if(cY2Axis != null)
                    {
                        linePlot.Y2Scale = cY2Axis.scale;
                    }
                    linePlot.fixedLimits = getBooleanParam("LINECHART_FIXED_LIMITS", false);
                    linePlot.pointSize = getIntParam("LINECHART_POINT_SIZE", new Integer(6)).intValue();
                    FillStyle backstyle = convertFillStyle(getStringParam("LINECHART_BACK", ""));
                    if(backstyle != null)
                    {
                        linePlot.back = backstyle;
                    }
                }
                lstyle = convertLineStyle(getStringParam("SERIE_STYLE_" + i, ""));
                if(lstyle == null)
                {
                    lstyle = new LineStyle(0.2F, GraphicsProvider.getColor(ChartColor.BLACK), 1);
                }
                LineStyle vstyle = convertLineStyle(getStringParam("SERIE_V_STYLE_" + i, ""));
                if(!getBooleanParam("SERIE_DRAW_LINE_" + i, true))
                {
                    lstyle = null;
                }
                pSeriesMaxData[i] = getStringParam("SERIE_MAX_DATA_" + i, "");
                pSeriesMaxData[i] = getStringParam("SERIE_MAX_DATA_" + i, "");
                pSeriesMinData[i] = getStringParam("SERIE_MIN_DATA_" + i, "");
                pSeriesCloseData[i] = getStringParam("SERIE_CLOSE_DATA_" + i, "");
                pSeries[i] = null;
                if(pSeriesMaxData[i].length() == 0)
                {
                    pSeries[i] = new LineDataSerie(convertDoubleListWithNulls(pSeriesData[i]), lstyle);
                    ((LineDataSerie)pSeries[i]).vstyle = vstyle;
                }
                if(pSeriesMaxData[i].length() > 0 && pSeriesCloseData[i].length() == 0)
                {
                    pSeries[i] = new MaxMinDataSerie(convertDoubleListWithNulls(pSeriesData[i]), lstyle);
                    if(pSeriesMinData[i].length() > 0)
                    {
                        ((MaxMinDataSerie)pSeries[i]).setMaxMinValues(convertDoubleList(pSeriesMaxData[i]), convertDoubleList(pSeriesMinData[i]));
                    } else
                    {
                        ((MaxMinDataSerie)pSeries[i]).setMaxMinValues(convertDoubleList(pSeriesMaxData[i]), null);
                    }
                    ((MaxMinDataSerie)pSeries[i]).drawLineEnd = getStringParam("SERIE_LINE_END_" + i, "").toUpperCase().compareTo("TRUE") == 0;
                    linePlot.MaxMinType = 0;
                    LineStyle mmstyle = convertLineStyle(getStringParam("SERIE_MAXMIN_STYLE_" + i, ""));
                    if(mmstyle != null)
                    {
                        ((MaxMinDataSerie)pSeries[i]).maxminStyle = mmstyle;
                    }
                    if(getStringParam("SERIE_BUBBLE_" + i, "").toUpperCase().compareTo("TRUE") == 0)
                    {
                        ((MaxMinDataSerie)pSeries[i]).bubbleChart = true;
                        if(getStringParam("SERIE_FILL_BUBBLE_" + i, "").toUpperCase().compareTo("TRUE") == 0)
                        {
                            ((MaxMinDataSerie)pSeries[i]).fillBubble = true;
                        } else
                        {
                            ((MaxMinDataSerie)pSeries[i]).fillBubble = false;
                        }
                    }
                }
                if(pSeriesMaxData[i].length() > 0 && pSeriesCloseData[i].length() > 0)
                {
                    pSeries[i] = new MaxMinDataSerie(convertDoubleListWithNulls(pSeriesData[i]), convertDoubleList(pSeriesCloseData[i]), convertDoubleList(pSeriesMaxData[i]), convertDoubleList(pSeriesMinData[i]), lstyle);
                    linePlot.MaxMinType = 1;
                    ((MaxMinDataSerie)pSeries[i]).drawLineEnd = false;
                    if(getStringParam("SERIE_LINE_TYPE_" + i, "").compareTo("CANDLESTICK") == 0)
                    {
                        linePlot.MaxMinType = 2;
                    }
                    if(getStringParam("SERIE_CANDLE_POSITIVE_COLOR_" + i, "").length() > 0)
                    {
                        ((MaxMinDataSerie)pSeries[i]).positiveValueColor = convertColor(getStringParam("SERIE_CANDLE_POSITIVE_COLOR_" + i, ""));
                    }
                    if(getStringParam("SERIE_CANDLE_NEGATIVE_COLOR_" + i, "").length() > 0)
                    {
                        ((MaxMinDataSerie)pSeries[i]).negativeValueColor = convertColor(getStringParam("SERIE_CANDLE_NEGATIVE_COLOR_" + i, ""));
                    }
                    ((MaxMinDataSerie)pSeries[i]).openCloseWidth = getIntParam("SERIE_OPENCLOSE_WIDTH_" + i, new Integer(7)).intValue();
                    LineStyle mmstyle = convertLineStyle(getStringParam("SERIE_MAXMIN_STYLE_" + i, ""));
                    if(mmstyle != null)
                    {
                        ((MaxMinDataSerie)pSeries[i]).maxminStyle = mmstyle;
                    }
                }
                if(Typ.compareTo("LINE") == 0)
                {
                    ((LineDataSerie)pSeries[i]).lineType = 0;
                }
                if(Typ.compareTo("B-SPLINES") == 0)
                {
                    ((LineDataSerie)pSeries[i]).lineType = 2;
                }
                if(Typ.compareTo("CURVE") == 0)
                {
                    ((LineDataSerie)pSeries[i]).lineType = 1;
                }
                if(Typ.compareTo("LINE_LEAST_SQUARES") == 0)
                {
                    ((LineDataSerie)pSeries[i]).lineType = 3;
                }
                String Xs = getStringParam("SERIE_DATAX_" + i, "");
                if(Xs.length() > 0)
                {
                    if(Xs.indexOf("/") > 0)
                    {
                        Xs = convertFromDates(Xs, cXAxis.dateStep, cXAxis.initialDate);
                    }
                    pSeries[i].setDatax(convertDoubleList(Xs));
                }
                pSeries[i].secondYAxis = getBooleanParam("SERIE_SECONDYAXIS_" + i, false);
                setAxisForSerie(i, cXAxis, cYAxis, pSeries[i]);
                fstyle = convertFillStyle(getStringParam("SERIE_FILL_" + i, ""));
                if(fstyle != null)
                {
                    ((LineDataSerie)pSeries[i]).fillStyle = fstyle;
                }
                font = convertFont(getStringParam("SERIE_FONT_" + i, ""));
                if(font != null)
                {
                    ((LineDataSerie)pSeries[i]).valueFont = font;
                }
                c = convertColor(getStringParam("SERIE_COLOR_" + i, ""));
                ((LineDataSerie)pSeries[i]).valueColor = c;
                c = convertColor(getStringParam("SERIE_POINT_COLOR_" + i, ""));
                ((LineDataSerie)pSeries[i]).pointColor = c;
                ((LineDataSerie)pSeries[i]).drawPoint = getBooleanParam("SERIE_POINT_" + i, false);
                com.java4less.rchart.gc.ChartImage im2 = null;
                String pointImageStr = getStringParam("SERIE_POINT_IMAGE_" + i, "");
                if(pointImageStr.compareTo("") != 0)
                {
                    im2 = GraphicsProvider.getImage(pointImageStr);
                    ((LineDataSerie)pSeries[i]).icon = im2;
                }
                if(!pSeriesNames[i].startsWith("HIDDEN"))
                {
                    if(im2 != null)
                    {
                        clegend.addItem(pSeriesNames[i], im2);
                    } else
                    if(fstyle == null)
                    {
                        if(lstyle != null)
                        {
                            clegend.addItem(pSeriesNames[i], lstyle);
                        } else
                        {
                            clegend.addItem(pSeriesNames[i], new FillStyle(((LineDataSerie)pSeries[i]).valueColor));
                        }
                    } else
                    {
                        clegend.addItem(pSeriesNames[i], fstyle);
                    }
                }
                pSeries[i].name = pSeriesNames[i];
                linePlot.addSerie(pSeries[i]);
            }
            if(Typ.compareTo("LINE3D") == 0)
            {
                if(linePlot3D == null)
                {
                    linePlot3D = new LinePlotter3D();
                    linePlot3D.XScale = cXAxis.scale;
                    linePlot3D.YScale = cYAxis.scale;
                    if(cY2Axis != null)
                    {
                        linePlot3D.Y2Scale = cY2Axis.scale;
                    }
                    linePlot3D.fixedLimits = getBooleanParam("LINECHART_FIXED_LIMITS", false);
                    if(getStringParam("LINECHART3D_BORDER", "").length() > 0)
                    {
                        linePlot3D.border = convertLineStyle(getStringParam("LINECHART3D_BORDER", ""));
                    }
                    if(getStringParam("LINECHART3D_DEPTH", "").length() > 0)
                    {
                        linePlot3D.depth = (int)getDoubleParam("LINECHART3D_DEPTH", new Double(20D)).doubleValue();
                    }
                    if(getStringParam("LINECHART3D_SEPARATOR", "").length() > 0)
                    {
                        linePlot3D.interLineSpace = (int)getDoubleParam("LINECHART3D_SEPARATOR", new Double(0.0D)).doubleValue();
                    }
                    FillStyle backstyle = convertFillStyle(getStringParam("LINECHART3D_BACK", ""));
                    if(backstyle != null)
                    {
                        linePlot3D.back = backstyle;
                    }
                }
                lstyle = convertLineStyle(getStringParam("SERIE_STYLE_" + i, ""));
                if(lstyle == null)
                {
                    lstyle = new LineStyle(0.2F, GraphicsProvider.getColor(ChartColor.BLACK), 1);
                }
                if(getStringParam("SERIE_DRAW_LINE_" + i, "").compareTo("N") == 0)
                {
                    lstyle = null;
                }
                pSeries[i] = new LineDataSerie(convertDoubleListWithNulls(pSeriesData[i]), lstyle);
                String Xs = getStringParam("SERIE_DATAX_" + i, "");
                if(Xs.length() > 0)
                {
                    pSeries[i].setDatax(convertDoubleList(Xs));
                }
                pSeries[i].secondYAxis = getBooleanParam("SERIE_SECONDYAXIS_" + i, false);
                setAxisForSerie(i, cXAxis, cYAxis, pSeries[i]);
                fstyle = convertFillStyle(getStringParam("SERIE_FILL_" + i, ""));
                if(fstyle != null)
                {
                    ((LineDataSerie)pSeries[i]).fillStyle = fstyle;
                }
                font = convertFont(getStringParam("SERIE_FONT_" + i, ""));
                if(font != null)
                {
                    ((LineDataSerie)pSeries[i]).valueFont = font;
                }
                c = convertColor(getStringParam("SERIE_COLOR_" + i, ""));
                ((LineDataSerie)pSeries[i]).valueColor = c;
                com.java4less.rchart.gc.ChartImage im2 = null;
                String pointImageStr = getStringParam("SERIE_POINT_IMAGE_" + i, "");
                if(pointImageStr.compareTo("") != 0)
                {
                    im2 = GraphicsProvider.getImage(pointImageStr);
                    ((LineDataSerie)pSeries[i]).icon = im2;
                }
                if(im2 != null)
                {
                    clegend.addItem(pSeriesNames[i], im2);
                } else
                if(fstyle == null)
                {
                    if(lstyle != null)
                    {
                        clegend.addItem(pSeriesNames[i], lstyle);
                    } else
                    {
                        clegend.addItem(pSeriesNames[i], new FillStyle(((LineDataSerie)pSeries[i]).valueColor));
                    }
                } else
                {
                    clegend.addItem(pSeriesNames[i], fstyle);
                }
                pSeries[i].name = pSeriesNames[i];
                linePlot3D.addSerie(pSeries[i]);
            }
            if(Typ.compareTo("RADAR") == 0)
            {
                if(radarPlot == null)
                {
                    radarPlot = new RadarPlotter();
                }
                lstyle = convertLineStyle(getStringParam("SERIE_STYLE_" + i, ""));
                if(lstyle == null)
                {
                    lstyle = new LineStyle(0.2F, GraphicsProvider.getColor(ChartColor.BLACK), 1);
                }
                pSeries[i] = null;
                pSeries[i] = new LineDataSerie(convertDoubleList(pSeriesData[i]), lstyle);
                fstyle = convertFillStyle(getStringParam("SERIE_FILL_" + i, ""));
                if(fstyle != null)
                {
                    ((LineDataSerie)pSeries[i]).fillStyle = fstyle;
                }
                if(fstyle == null)
                {
                    clegend.addItem(pSeriesNames[i], lstyle);
                } else
                {
                    clegend.addItem(pSeriesNames[i], fstyle);
                }
                font = convertFont(getStringParam("SERIE_FONT_" + i, ""));
                if(font != null)
                {
                    ((LineDataSerie)pSeries[i]).valueFont = font;
                }
                c = convertColor(getStringParam("SERIE_COLOR_" + i, ""));
                ((LineDataSerie)pSeries[i]).valueColor = c;
                c = convertColor(getStringParam("SERIE_POINT_COLOR_" + i, ""));
                ((LineDataSerie)pSeries[i]).pointColor = c;
                ((LineDataSerie)pSeries[i]).drawPoint = getBooleanParam("SERIE_POINT_" + i, false);
                pSeries[i].name = pSeriesNames[i];
                radarPlot.addSerie(pSeries[i]);
            }
            if(Typ.compareTo("BAR3D") == 0)
            {
                
				fstyle = convertFillStyle(getStringParam("SERIE_STYLE_" + i, ""));
                if(fstyle == null)
                {
                    fstyle = convertFillStyle(getStringParam("SERIE_BAR_STYLE_" + i, ""));
                }
                if(fstyle == null)
                {
                    fstyle = new FillStyle(GraphicsProvider.getColor(ChartColor.BLUE));
                }
                pSeries[i] = new BarDataSerie(convertDoubleList(pSeriesData[i]), fstyle);
                if(getStringParam("SERIE_NEGATIVE_STYLE_" + i, "").length() > 0)
                {
                    ((BarDataSerie)pSeries[i]).negativeStyle = convertFillStyle(getStringParam("SERIE_NEGATIVE_STYLE_" + i, ""));
                }
                clegend.addItem(pSeriesNames[i], fstyle);
                if(getStringParam("SERIE_BARS_COLORS_" + i, "").length() > 0)
                {
                    String listTMP[] = convertList(getStringParam("SERIE_BARS_COLORS_" + i, ""));
                    ((BarDataSerie)pSeries[i]).barStyles = new FillStyle[listTMP.length];
                    for(int j = 0; j < listTMP.length; j++)
                    {
                        ((BarDataSerie)pSeries[i]).barStyles[j] = new FillStyle(convertColor(listTMP[j]));
                    }

                }
                font = convertFont(getStringParam("SERIE_FONT_" + i, ""));
                if(font != null)
                {
                    ((BarDataSerie)pSeries[i]).valueFont = font;
                }
                c = convertColor(getStringParam("SERIE_COLOR_" + i, ""));
                ((BarDataSerie)pSeries[i]).valueColor = c;
                pSeries[i].secondYAxis = getBooleanParam("SERIE_SECONDYAXIS_" + i, false);
                setAxisForSerie(i, cXAxis, cYAxis, pSeries[i]);
                String t = getStringParam("SERIE_BORDER_TYPE_" + i, "");
                if(t.compareTo("LOWERED") == 0)
                {
                    ((BarDataSerie)pSeries[i]).borderType = 1;
                }
                if(t.compareTo("RAISED") == 0)
                {
                    ((BarDataSerie)pSeries[i]).borderType = 2;
                }
                if(t.compareTo("NO") == 0)
                {
                    ((BarDataSerie)pSeries[i]).borderType = 3;
                }
                if(t.compareTo("NORMAL") == 0)
                {
                    ((BarDataSerie)pSeries[i]).borderType = 0;
                }
                lstyle = convertLineStyle(getStringParam("SERIE_BORDER_" + i, ""));
                if(lstyle != null)
                {
                    ((BarDataSerie)pSeries[i]).border = lstyle;
                }
                if(barPlot3D == null)
                {
                    barPlot3D = new BarPlotter3D();
                    barPlot3D.XScale = cXAxis.scale;
                    barPlot3D.YScale = cYAxis.scale;
                    if(cY2Axis != null)
                    {
                        barPlot3D.Y2Scale = cY2Axis.scale;
                    }
                    if(getStringParam("BARCHART3D_DEPTH", "").length() > 0)
                    {
                        barPlot3D.depth = (int)getDoubleParam("BARCHART3D_DEPTH", new Double(20D)).doubleValue();
                    }
                    barPlot3D.fullDepth = getStringParam("BARCHART3D_FULL_DEPTH", "").toUpperCase().compareTo("TRUE") == 0;
                    FillStyle backstyle = convertFillStyle(getStringParam("BARCHART3D_BACK", ""));
                    if(backstyle != null)
                    {
                        barPlot3D.back = backstyle;
                    }
                    barPlot3D.cumulative = getBooleanParam("BARCHART_CUMULATIVE", false);
                }
                barPlot3D.verticalBars = getBooleanParam("BARCHART_VERTICAL", true);
                pSeries[i].name = pSeriesNames[i];
                barPlot3D.addSerie(pSeries[i]);
            }
            if(Typ.compareTo("BAR") == 0)
            {
                fstyle = convertFillStyle(getStringParam("SERIE_STYLE_" + i, ""));
                if(fstyle == null)
                {
                    fstyle = convertFillStyle(getStringParam("SERIE_BAR_STYLE_" + i, ""));
                }
                if(fstyle == null)
                {
                    fstyle = new FillStyle(GraphicsProvider.getColor(ChartColor.BLUE));
                }
                pSeries[i] = new BarDataSerie(convertDoubleList(pSeriesData[i]), fstyle);
                ((BarDataSerie)pSeries[i]).addBarsBase(convertDoubleList(getStringParam("SERIE_BARS_START_" + i, "")));
                String Xs = getStringParam("SERIE_DATAX_" + i, "");
                if(Xs.length() > 0)
                {
                    pSeries[i].setDatax(convertDoubleList(Xs));
                }
                if(getStringParam("SERIE_NEGATIVE_STYLE_" + i, "").length() > 0)
                {
                    ((BarDataSerie)pSeries[i]).negativeStyle = convertFillStyle(getStringParam("SERIE_NEGATIVE_STYLE_" + i, ""));
                }
                clegend.addItem(pSeriesNames[i], fstyle);
                if(getStringParam("SERIE_BARS_COLORS_" + i, "").length() > 0)
                {
                    String listTMP[] = convertList(getStringParam("SERIE_BARS_COLORS_" + i, ""));
                    ((BarDataSerie)pSeries[i]).barStyles = new FillStyle[listTMP.length];
                    for(int j = 0; j < listTMP.length; j++)
                    {
                        ((BarDataSerie)pSeries[i]).barStyles[j] = new FillStyle(convertColor(listTMP[j]));
                    }

                }
                font = convertFont(getStringParam("SERIE_FONT_" + i, ""));
                if(font != null)
                {
                    ((BarDataSerie)pSeries[i]).valueFont = font;
                }
                c = convertColor(getStringParam("SERIE_COLOR_" + i, ""));
                ((BarDataSerie)pSeries[i]).valueColor = c;
                pSeries[i].secondYAxis = getBooleanParam("SERIE_SECONDYAXIS_" + i, false);
                setAxisForSerie(i, cXAxis, cYAxis, pSeries[i]);
                String t = getStringParam("SERIE_BORDER_TYPE_" + i, "");
                if(t.compareTo("LOWERED") == 0)
                {
                    ((BarDataSerie)pSeries[i]).borderType = 1;
                }
                if(t.compareTo("RAISED") == 0)
                {
                    ((BarDataSerie)pSeries[i]).borderType = 2;
                }
                if(t.compareTo("NO") == 0)
                {
                    ((BarDataSerie)pSeries[i]).borderType = 3;
                }
                if(t.compareTo("NORMAL") == 0)
                {
                    ((BarDataSerie)pSeries[i]).borderType = 0;
                }
                lstyle = convertLineStyle(getStringParam("SERIE_BORDER_" + i, ""));
                if(lstyle != null)
                {
                    ((BarDataSerie)pSeries[i]).border = lstyle;
                }
                if(barPlot == null)
                {
                    barPlot = new BarPlotter();
                    barPlot.XScale = cXAxis.scale;
                    barPlot.YScale = cYAxis.scale;
                    if(cY2Axis != null)
                    {
                        barPlot.Y2Scale = cY2Axis.scale;
                    }
                    barPlot.barAtAxis = getBooleanParam("BARCHART_BAR_AT_AXIS", false);
                    FillStyle backstyle = convertFillStyle(getStringParam("BARCHART_BACK", ""));
                    if(backstyle != null)
                    {
                        barPlot.back = backstyle;
                    }
                    barPlot.cumulative = getBooleanParam("BARCHART_CUMULATIVE", false);
                    if(getBooleanParam("BARCHART_CUMULATIVE_BACK_COMPATIBLE", false))
                    {
                        barPlot.cumulativeBackwardsCompatible = true;
                    }
                    barPlot.eventChart = getBooleanParam("BARCHART_EVENTCHART", false);
                }
                barPlot.verticalBars = getBooleanParam("BARCHART_VERTICAL", true);
                pSeries[i].name = pSeriesNames[i];
                barPlot.addSerie(pSeries[i]);
            }
            if(Typ.compareTo("PIE") == 0)
            {
                FillStyle styles[] = new FillStyle[50];
                for(int j = 1; j < 50; j++)
                {
                    fstyle = convertFillStyle(getStringParam("PIE_STYLE_" + j, ""));
                    if(fstyle == null)
                    {
                        break;
                    }
                    styles[j - 1] = fstyle;
                    clegend.addItem(getStringParam("PIE_NAME_" + j, ""), fstyle);
                }

                boolean pieKeepTogether[] = (boolean[])null;
                String pieLabels[] = (String[])null;
                if(getStringParam("SERIE_LABELS_" + i, "").length() > 0)
                {
                    pieLabels = getItemsParameter("SERIE_LABELS_" + i);
                }
                if(getStringParam("SERIE_TOGETHER_" + i, "").length() > 0)
                {
                    pieKeepTogether = convertBooleanList(getStringParam("SERIE_TOGETHER_" + i, ""));
                }
                pSeries[i] = new PieDataSerie(convertDoubleList(pSeriesData[i]), styles, pieKeepTogether, pieLabels);
                font = convertFont(getStringParam("SERIE_FONT_" + i, ""));
                if(font != null)
                {
                    ((PieDataSerie)pSeries[i]).valueFont = font;
                }
                if(getStringParam("SERIE_FONT_" + i, "").compareTo("NULL") == 0)
                {
                    ((PieDataSerie)pSeries[i]).valueFont = null;
                }
                c = convertColor(getStringParam("SERIE_COLOR_" + i, ""));
                ((PieDataSerie)pSeries[i]).valueColor = c;
                ((PieDataSerie)pSeries[i]).drawPercentages = getBooleanParam("SERIE_PERCENTAGES_" + i, true);
                Double d = getDoubleParam("SERIE_DISTCENTER_" + i, null);
                if(d != null)
                {
                    ((PieDataSerie)pSeries[i]).textDistanceToCenter = d.doubleValue();
                }
                if(piePlot == null)
                {
                    piePlot = new PiePlotter();
                    piePlot.labelFormat = getStringParam("PIE_LABEL_FORMAT", "");
                }
                pSeries[i].name = pSeriesNames[i];
                piePlot.addSerie(pSeries[i]);
            }
            if(Typ.compareTo("GAUGE") == 0)
            {
                if(pSeriesData[i].compareTo("") == 0)
                {
                    break;
                }
                FillStyle styles[] = new FillStyle[50];
                for(int j = 1; j < 50; j++)
                {
                    fstyle = convertFillStyle(getStringParam("GAUGE_NEEDLE_STYLE_" + j, ""));
                    if(fstyle == null)
                    {
                        break;
                    }
                    styles[j - 1] = fstyle;
                    clegend.addItem(getStringParam("GAUGE_NEEDLE_" + j, ""), fstyle);
                }

                pSeries[i] = new GaugeDataSerie(convertDoubleList(pSeriesData[i]), styles);
                font = convertFont(getStringParam("SERIE_FONT_" + i, ""));
                if(font != null)
                {
                    ((GaugeDataSerie)pSeries[i]).valueFont = font;
                }
                if(getStringParam("SERIE_FONT_" + i, "").compareTo("NULL") == 0)
                {
                    ((GaugeDataSerie)pSeries[i]).valueFont = null;
                }
                c = convertColor(getStringParam("SERIE_COLOR_" + i, ""));
                ((GaugeDataSerie)pSeries[i]).valueColor = c;
                lstyle = convertLineStyle(getStringParam("SERIE_NEEDLE_STYLE_" + i, ""));
                if(lstyle == null)
                {
                    lstyle = new LineStyle(2.0F, GraphicsProvider.getColor(ChartColor.BLACK), 1);
                }
                ((GaugeDataSerie)pSeries[i]).needleStyle = lstyle;
                ((GaugeDataSerie)pSeries[i]).needleLength = getIntParam("SERIE_NEEDLE_LENTGH_" + i, new Integer(80)).intValue();
                if(gaugePlot == null)
                {
                    gaugePlot = new GaugePlotter();
                }
                pSeries[i].name = pSeriesNames[i];
                gaugePlot.addSerie(pSeries[i]);
            }
            if(pSeries[i] != null)
            {
                pSeries[i].valueFormat = getStringParam("SERIE_FORMAT_" + i, "#####.##");
                pSeries[i].labelTemplate = getStringParam("SERIE_LABEL_TEMPLATE_" + i, "");
                pSeries[i].tips = convertList(getStringParam("SERIE_TIPS_" + i, ""));
                pSeries[i].htmlLinks = convertList(getStringParam("SERIE_LINKS_" + i, ""));
                if(getStringParam("SERIE_DATA_LABELS_" + i, "").length() > 0)
                {
                    pSeries[i].dataLabels = convertList(getStringParam("SERIE_DATA_LABELS_" + i, ""));
                }
            }
        }

        if(linePlot != null && (linePlot instanceof CurvePlotter))
        {
            ((CurvePlotter)linePlot).segments = getIntParam("CURVECHART_SEGMENTS", new Integer(24)).intValue();
        }
        if(barPlot != null)
        {
            barPlot.barsBase = getDoubleParam("BARCHART_BASE", new Double(0.0D)).doubleValue();
            barPlot.summedLabels = getBooleanParam("BARCHART_SUMMED_LABELS", true);
            Integer in = getIntParam("BARCHART_BARWIDTH", null);
            if(in != null)
            {
                barPlot.barWidth = in.intValue();
            }
            in = getIntParam("BARCHART_BARSPACE", null);
            if(in != null)
            {
                barPlot.interBarSpace = in.intValue();
            }
            Double dob = getDoubleParam("BARCHART_GROUPSPACE", null);
            if(dob != null)
            {
                barPlot.InterGroupSpace = dob.doubleValue();
            }
        }
        if(barPlot3D != null)
        {
            barPlot3D.barsBase = getDoubleParam("BARCHART_BASE", new Double(0.0D)).doubleValue();
            barPlot3D.summedLabels = getBooleanParam("BARCHART_SUMMED_LABELS", true);
            Integer in = getIntParam("BARCHART_BARWIDTH", null);
            if(in != null)
            {
                barPlot3D.barWidth = in.intValue();
            }
            in = getIntParam("BARCHART_BARSPACE", null);
            if(in != null)
            {
                barPlot3D.interBarSpace = in.intValue();
            }
            Double dob = getDoubleParam("BARCHART_GROUPSPACE", null);
            if(dob != null)
            {
                barPlot3D.InterGroupSpace = dob.doubleValue();
            }
        }
        if(radarPlot != null)
        {
            Double d = getDoubleParam("RADARCHART_RADIUS", null);
            if(d != null)
            {
                radarPlot.radiusModifier = d.doubleValue();
            }
            radarPlot.drawCircle = getBooleanParam("RADARCHART_CIRCLE", false);
            fstyle = convertFillStyle(getStringParam("RADARCHART_BACK", ""));
            if(fstyle != null)
            {
                radarPlot.backStyle = fstyle;
            }
            if(getStringParam("RADARCHART_POINT_COLORS_SCALE", "").length() > 0)
            {
                radarPlot.pointColorScale = convertDoubleList(getStringParam("RADARCHART_POINT_COLORS_SCALE", ""));
            }
            if(getStringParam("RADARCHART_POINT_COLORS", "").length() > 0)
            {
                String listTMP[] = convertList(getStringParam("RADARCHART_POINT_COLORS", ""));
                radarPlot.pointColors = new ChartColor[listTMP.length];
                for(int j = 0; j < listTMP.length; j++)
                {
                    radarPlot.pointColors[j] = convertColor(listTMP[j]);
                }

            }
            if(getStringParam("RADARCHART_FACTOR_COLORS", "").length() > 0)
            {
                String listTMP[] = convertList(getStringParam("RADARCHART_FACTOR_COLORS", ""));
                radarPlot.factorColors = new ChartColor[listTMP.length];
                for(int j = 0; j < listTMP.length; j++)
                {
                    radarPlot.factorColors[j] = convertColor(listTMP[j]);
                }

            }
            lstyle = convertLineStyle(getStringParam("RADARCHART_BORDER", ""));
            if(lstyle != null)
            {
                radarPlot.border = lstyle;
            }
            lstyle = convertLineStyle(getStringParam("RADARCHART_GRID", ""));
            if(lstyle != null)
            {
                radarPlot.gridStyle = lstyle;
            }
            font = convertFont(getStringParam("RADARCHART_GRID_FONT", ""));
            if(font != null)
            {
                radarPlot.gridFont = font;
            }
            radarPlot.gridFontColor = convertColor(getStringParam("RADARCHART_GRID_FONT_COLOR", ""));
            font = convertFont(getStringParam("RADARCHART_FACTOR_FONT", ""));
            if(font != null)
            {
                radarPlot.factorFont = font;
            }
            radarPlot.factorColor = convertColor(getStringParam("RADARCHART_FACTOR_COLOR", ""));
            String tmpList = getStringParam("RADARCHART_FACTOR_MAX", "");
            if(tmpList.length() > 0)
            {
                radarPlot.factorMaxs = convertDoubleList(tmpList);
            }
            tmpList = getStringParam("RADARCHART_FACTOR_MIN", "");
            if(tmpList.length() > 0)
            {
                radarPlot.factorMins = convertDoubleList(tmpList);
            }
            tmpList = getStringParam("RADARCHART_FACTOR_NAMES", "");
            if(tmpList.length() > 0)
            {
                radarPlot.factorNames = convertList(tmpList);
            }
            tmpList = getStringParam("RADARCHART_TICKS", "");
            if(tmpList.length() > 0)
            {
                radarPlot.ticks = (new Integer(tmpList)).intValue();
            }
            radarPlot.tickLabelFormat = getStringParam("RADARCHART_TICK_FORMAT", "");
        }
        if(piePlot != null)
        {
            Double d = getDoubleParam("PIECHART_RADIUS", null);
            if(d != null)
            {
                piePlot.radiusModifier = d.doubleValue();
            }
            piePlot.effect3D = getBooleanParam("PIECHART_3D", false);
            lstyle = convertLineStyle(getStringParam("PIE_BORDER", ""));
            if(lstyle != null)
            {
                piePlot.border = lstyle;
            }
            lstyle = convertLineStyle(getStringParam("PIE_LABEL_LINE", ""));
            if(lstyle != null)
            {
                piePlot.labelLine = lstyle;
            }
        }
        if(gaugePlot != null)
        {
            Double d = getDoubleParam("GAUGE_RADIUS", null);
            if(d != null)
            {
                gaugePlot.radiusModifier = d.doubleValue();
            }
            gaugePlot.subGaugeSize = getIntParam("GAUGE_SUBGAUGE_SIZE", new Integer(30)).intValue();
            gaugePlot.axis2Margin = getIntParam("GAUGE_AXIS2_MARGIN", new Integer(40)).intValue();
            gaugePlot.axis2LabelMargin = getIntParam("GAUGE_AXIS2_LABEL_MARGIN", new Integer(-20)).intValue();
            GaugePlotter gp = null;
            GaugePlotter gp1 = null;
            GaugePlotter gp2 = null;
            for(int sb = 0; sb < 3; sb++)
            {
                String prefix = "";
                if(sb > 0)
                {
                    prefix = "" + sb;
                }
                gp = null;
                if(sb == 0)
                {
                    gp = gaugePlot;
                }
                if(sb == 1 && getBooleanParam("GAUGE1_ENABLE", false))
                {
                    gp = new GaugePlotter();
                    gp1 = gp;
                }
                if(sb == 2 && getBooleanParam("GAUGE2_ENABLE", false))
                {
                    gp = new GaugePlotter();
                    gp2 = gp;
                }
                if(gp != null)
                {
                    lstyle = convertLineStyle(getStringParam("GAUGE" + prefix + "_BORDER", ""));
                    if(lstyle != null)
                    {
                        gp.border = lstyle;
                    }
                    gp.needleBase = getIntParam("GAUGE" + prefix + "_BASE", new Integer(8)).intValue();
                    gp.axis1Margin = getIntParam("GAUGE" + prefix + "_AXIS1_MARGIN", new Integer(5)).intValue();
                    gp.axis1LabelMargin = getIntParam("GAUGE" + prefix + "_AXIS1_LABEL_MARGIN", new Integer(10)).intValue();
                    gp.maxZone = getDoubleParam("GAUGE" + prefix + "_MAX_ZONE", new Double(-1D)).doubleValue();
                    if(getStringParam("GAUGE" + prefix + "_MAX_ZONE_COLOR", "").length() > 0)
                    {
                        gp.maxZoneColor = new FillStyle(convertColor(getStringParam("GAUGE_MAX_ZONE_COLOR", "")));
                    }
                    gp.minZone = getDoubleParam("GAUGE" + prefix + "_MIN_ZONE", new Double(-1D)).doubleValue();
                    if(getStringParam("GAUGE" + prefix + "_MIN_ZONE_COLOR", "").length() > 0)
                    {
                        gp.minZoneColor = new FillStyle(convertColor(getStringParam("GAUGE_MIN_ZONE_COLOR", "")));
                    }
                    gp.maxminZoneWidth = getIntParam("GAUGE" + prefix + "_MAXMIN_ZONE_WIDTH", new Integer(50)).intValue();
                    gp.maxminZoneMargin = getIntParam("GAUGE" + prefix + "_MAXMIN_ZONE_MARGIN", new Integer(5)).intValue();
                    gp.endAngle = getIntParam("GAUGE" + prefix + "_END_ANGLE", new Integer(200)).intValue();
                    gp.startAngle = getIntParam("GAUGE" + prefix + "_START_ANGLE", new Integer(-20)).intValue();
                    fstyle = convertFillStyle(getStringParam("GAUGE" + prefix + "_BACK", ""));
                    if(fstyle != null)
                    {
                        gp.gaugeback = fstyle;
                    }
                }
            }

            gaugePlot.subGauge1 = gp1;
            gaugePlot.subGauge2 = gp2;
            gaugePlot.axis1 = cXAxis;
            if(gp1 != null)
            {
                gp1.axis1 = cYAxis;
            }
            if(gp2 != null)
            {
                gp2.axis1 = cY2Axis;
            }
            if(gp2 == null)
            {
                gaugePlot.axis2 = cY2Axis;
            }
            if(gp1 == null)
            {
                gaugePlot.axis2 = cYAxis;
            }
            cXAxis = null;
            cYAxis = null;
            cY2Axis = null;
        }
        Plotter plot = null;
        if(plot == null)
        {
            plot = barPlot;
        }
        if(plot == null)
        {
            plot = barPlot3D;
        }
        if(plot == null)
        {
            plot = linePlot3D;
        }
        if(plot == null)
        {
            plot = linePlot;
        }
        if(plot == null)
        {
            plot = piePlot;
        }
        if(plot == null)
        {
            plot = radarPlot;
        }
        if(plot == null)
        {
            plot = gaugePlot;
        }
        Chart chart = currentChart;
        if(chart == null)
        {
            chart = new Chart();
        }
        chart.resetChart(cTitle, plot, cXAxis, cYAxis);
        if(plot == piePlot)
        {
            chart.resetChart(cTitle, plot, null, null);
        }
        if(plot == radarPlot)
        {
            chart.resetChart(cTitle, plot, null, null);
        }
        chart.showTips = getBooleanParam("CHART_SHOW_TIPS", false);
        chart.showPosition = getBooleanParam("CHART_SHOW_POSITION", false);
        chart.activateSelection = getBooleanParam("CHART_POINT_SELECTION", false);
        if(chart.showTips)
        {
            chart.activateSelection = true;
        }
        if(chart.showPosition)
        {
            chart.activateSelection = true;
        }
        for(int noteCount = 1; getStringParam("CHART_NOTE" + noteCount, "").length() > 0; noteCount++)
        {
            chart.addNote(getStringParam("CHART_NOTE" + noteCount, ""));
        }

        chart.tipColor = convertColor(getStringParam("CHART_TIPS_COLOR", "YELLOW"));
        chart.tipFontColor = convertColor(getStringParam("CHART_TIPS_FONT_COLOR", "BLACK"));
        chart.tipFont = convertFont(getStringParam("CHART_TIPS_FONT", "SERIF|PLAIN|10"));
        chart.htmlLinkTarget = getStringParam("CHART_LINKS_TARGET", "_new");
        chart.repaintAll = true;
        chart.XLabel = cXLabel;
        chart.YLabel = cYLabel;
        chart.Y2Label = cY2Label;
        if(cY2Axis != null)
        {
            chart.setY2Scale(cY2Axis);
        }
        if(piePlot != null && piePlot != plot)
        {
            chart.addPlotter(piePlot);
        }
        if(linePlot != null && linePlot != plot)
        {
            chart.addPlotter(linePlot);
        }
        if(linePlot3D != null && linePlot3D != plot)
        {
            chart.addPlotter(linePlot3D);
        }
        if(barPlot3D != null && barPlot3D != plot)
        {
            chart.addPlotter(barPlot3D);
        }
        if(barPlot != null && barPlot != plot)
        {
            chart.addPlotter(barPlot);
        }
        if(gaugePlot != null && gaugePlot != plot)
        {
            chart.addPlotter(gaugePlot);
        }
        if(getBooleanParam("LEGEND", true))
        {
            chart.legend = clegend;
        }
        plegendPosition = getStringParam("LEGEND_POSITION", "RIGHT");
        if(plegendPosition.compareTo("RIGHT") == 0)
        {
            chart.layout = 0;
        }
        if(plegendPosition.compareTo("BOTTOM") == 0)
        {
            chart.layout = 2;
        }
        if(plegendPosition.compareTo("TOP") == 0)
        {
            chart.layout = 1;
        }
        Double d = getDoubleParam("LEFT_MARGIN", null);
        if(d != null)
        {
            chart.leftMargin = d.doubleValue();
        }
        d = getDoubleParam("RIGHT_MARGIN", null);
        if(d != null)
        {
            chart.rightMargin = d.doubleValue();
        }
        d = getDoubleParam("TOP_MARGIN", null);
        if(d != null)
        {
            chart.topMargin = d.doubleValue();
        }
        d = getDoubleParam("LEGEND_MARGIN", null);
        if(d != null)
        {
            chart.legendMargin = d.doubleValue();
        }
        d = getDoubleParam("BOTTOM_MARGIN", null);
        if(d != null)
        {
            chart.bottomMargin = d.doubleValue();
        }
        lstyle = convertLineStyle(getStringParam("CHART_BORDER", ""));
        if(lstyle != null)
        {
            chart.border = lstyle;
        }
        fstyle = convertFillStyle(getStringParam("CHART_FILL", ""));
        if(fstyle != null)
        {
            chart.back = fstyle;
        }
        if(getBooleanParam("CHART_FULL_XAXIS", false))
        {
            chart.fullXAxis = true;
        }
        double tmpDouble = getDoubleParam("CHART_SECOND_AXIS_MARGIN", new Double(0.0D)).doubleValue();
        chart.secondYAxisMargin = tmpDouble;
        if(getStringParam("CHART_AXIS_MARGIN", "").length() > 0)
        {
            chart.axisMargin = getDoubleParam("CHART_AXIS_MARGIN", new Double(0.0D)).doubleValue();
        }
        String backImageStr = getStringParam("BACK_IMAGE", "");
        if(backImageStr.compareTo("") != 0)
        {
            com.java4less.rchart.gc.ChartImage im2 = null;
            im2 = GraphicsProvider.getImage(backImageStr);
            chart.backImage = im2;
        }
        chart.repaintAll = true;
        chart.repaintAlways = true;
        gChart = chart;
        if(getStringParam("DOUBLE_BUFFERING", "").toUpperCase().compareTo("NO") == 0)
        {
            chart.doubleBuffering = false;
        }
        if(chartApplet == null)
        {
            paintDirect = true;
        }
        if(paintDirect && chartApplet != null)
        {
            chart.setHeight(chartApplet.getSize().height);
            chart.setWidth(chartApplet.getSize().width);
            chartApplet.paint(chartApplet.getGraphics());
        }
        chart.virtualWidth = (int)getDoubleParam("CHART_WIDTH", new Double(0.0D)).doubleValue();
        chart.virtualHeight = (int)getDoubleParam("CHART_HEIGHT", new Double(0.0D)).doubleValue();
        for(int zoneNr = 1; getParameter("CHART_TARGET_ZONE_" + zoneNr, "").length() > 0; zoneNr++)
        {
            chart.addTargetZone(TargetZone.createFromString(getParameter("CHART_TARGET_ZONE_" + zoneNr, "")));
        }

        chart.msecs = getIntParam("REALTIME_MSECS", new Integer(2000)).intValue();
        chart.reloadFrom = getParameter("REALTIME_DATAFILE", "");
        if(chartApplet == null)
        {
            int w = 500;
            int h = 500;
            try
            {
                w = (new Integer(getStringParam("WIDTH", "500"))).intValue();
                h = (new Integer(getStringParam("HEIGHT", "500"))).intValue();
            }
            catch(Exception exception) {
				logger.warn("buildChart(Chart) - exception ignored", exception);
 }
            chart.setWidth(w);
            chart.setHeight(h);
        }
        if(jdbcsource != null)
        {
            jdbcsource = null;
        }
        chart.loader = this;
        return chart;
    }

    private void loadDataFromFile(String sFile, String userpsw)
    {
        String fileData = "";
        String param = "";
        String value = "";
        try
        {
            BufferedReader in = null;
            if(chartApplet != null && sFile.indexOf(":/") == -1)
            {
                sFile = chartApplet.getCodeBase() + sFile;
            }
            in = null;
            if(sFile.indexOf("file://") == 0)
            {
                if(fileEncoding.length() > 0)
                {
                    in = new BufferedReader(new InputStreamReader(new FileInputStream(sFile.substring(7, sFile.length())), fileEncoding));
                } else
                {
                    in = new BufferedReader(new InputStreamReader(new FileInputStream(sFile.substring(7, sFile.length()))));
                }
            } else
            {
                URL u = null;
                try
                {
                    u = getClass().getClassLoader().getResource(sFile);
                }
                catch(Exception exception) {
					logger.warn("loadDataFromFile(String, String) - exception ignored", exception);
 }
                try
                {
                    if(u == null)
                    {
                        u = ClassLoader.getSystemResource(sFile);
                    }
                }
                catch(Exception exception1) {
					logger.warn("loadDataFromFile(String, String) - exception ignored", exception1);
 }
                if(u == null)
                {
                    u = new URL(sFile);
                }
                URLConnection uc = u.openConnection();
                if(userpsw.length() > 0)
                {
                    uc.setRequestProperty("Authorization", "Basic " + userpsw);
                }
                java.io.InputStream content = uc.getInputStream();
                if(fileEncoding.length() > 0)
                {
                    in = new BufferedReader(new InputStreamReader(content, fileEncoding));
                } else
                {
                    in = new BufferedReader(new InputStreamReader(content));
                }
            }
            for(String inputLine = ""; (inputLine = in.readLine()) != null;)
            {
                fileData = fileData + inputLine + '\r' + '\n';
            }

            in.close();
        }
        catch(Exception e)
        {
			logger.error("loadDataFromFile(String, String)", e);

            String err = e.getMessage();
        }
        String line = "";
        while(fileData.length() > 0) 
        {
            int p = fileData.indexOf("\r\n");
            int p2 = fileData.indexOf("\n");
            if(p <= p2 && p >= 0)
            {
                line = fileData.substring(0, p);
                fileData = fileData.substring(p + 2, fileData.length());
            }
            if(p > p2 && p2 >= 0)
            {
                line = fileData.substring(0, p2);
                fileData = fileData.substring(p2 + 1, fileData.length());
            }
            if(p2 == -1 && p == -1)
            {
                line = fileData;
                fileData = "";
            }
            p = line.indexOf("=");
            if(p > -1)
            {
                param = line.substring(0, p);
                value = line.substring(p + 1, line.length());
                param = param.toUpperCase();
                boolean alreadyFound = false;
                for(int h = 0; h < loadedParametersCount; h++)
                {
                    if(loadedParameters[h].compareTo(param) == 0)
                    {
                        loadedValues[h] = value;
                        alreadyFound = true;
                    }
                }

                if(!alreadyFound)
                {
                    loadedParameters[loadedParametersCount] = param;
                    loadedValues[loadedParametersCount] = value;
                    loadedParametersCount++;
                }
            }
        }
    }

    public int getLoadedParametersCount()
    {
        return loadedParametersCount;
    }

    public String getLoadedParameter(int i)
    {
        return loadedParameters[i];
    }

    public String getLoadedValue(int i)
    {
        return loadedValues[i];
    }

    public void setDataFile(String df)
    {
        dataFile = df;
    }

    public Chart build(boolean clear, boolean reReadFile)
    {
        return build(null, clear, reReadFile);
    }

    public Chart build(Chart currentChart, boolean clear, boolean reReadFile)
    {
        if(clear)
        {
            loadedParametersCount = 0;
        }
        if(reReadFile)
        {
            String pFile = dataFile;
            if(chartApplet != null)
            {
                dataFile = getStringParam("DATAFILE", "");
                pFile = dataFile;
            }
            if(pFile.length() > 0)
            {
                loadFromFile(pFile, false);
            }
        }
        if(getStringParam("PAINT_DIRECT", "").compareTo("NO") == 0)
        {
            paintDirect = false;
        }
        return buildChart(currentChart);
    }

    public void clearParams()
    {
        loadedParametersCount = 0;
    }

    public void loadFromFile(String psFile, boolean Clear, String userpsw)
    {
        if(Clear)
        {
            loadedParametersCount = 0;
        }
        loadDataFromFile(psFile, userpsw);
    }

    public void loadFromFile(String psFile, boolean Clear)
    {
        if(Clear)
        {
            loadedParametersCount = 0;
        }
        loadDataFromFile(psFile, "");
    }

    public void setParameter(String param, String value)
    {
        boolean alreadyFound = false;
        for(int h = 0; h < loadedParametersCount; h++)
        {
            if(loadedParameters[h].compareTo(param) == 0)
            {
                loadedValues[h] = value;
                alreadyFound = true;
                return;
            }
        }

        if(!alreadyFound)
        {
            loadedParameters[loadedParametersCount] = param;
            loadedValues[loadedParametersCount] = value;
            loadedParametersCount++;
        }
		
	}

	public void setvalues(String []param,String []value,String label)
	{
		
		for(loadedParametersCount=0;loadedParametersCount<param.length;loadedParametersCount++)
		{	
			loadedParameters[loadedParametersCount] = param[loadedParametersCount];
			loadedValues[loadedParametersCount] = value[loadedParametersCount];
		}
		this.label=label;
	}
	
	/*public Chart buildPie(boolean temp)
	{
		return(buildPieChart(null,temp));
	}
	
	public Chart buildPieChart(Chart PieChart, boolean temp)
	{
		int i=0;
		ChartFont font = null;
        Title cTitle = null;
        HAxisLabel cXLabel = null;
        VAxisLabel cYLabel = null;
        VAxisLabel cY2Label = null;
        GaugePlotter gaugePlot = null;
        LinePlotter linePlot = null;
        LinePlotter3D linePlot3D = null;
        BarPlotter barPlot = null;
        BarPlotter3D barPlot3D = null;
        PiePlotter piePlot = null;
        RadarPlotter radarPlot = null;
		ChartColor c;
		Legend clegend = new Legend();
		
		FillStyle styles[] = new FillStyle[50];
        for(int j = 1; j < 22; j++)
        {
            fstyle = convertFillStyle(getStringParam("PIE_STYLE_" + j, ""));
            if(fstyle == null)
            {
                break;
            }
            styles[j - 1] = fstyle;
            clegend.addItem(getStringParam("PIE_NAME_" + j, ""), fstyle);
        }

        boolean pieKeepTogether[] = (boolean[])null;
        String pieLabels[] = (String[])null;
        if(getStringParam("SERIE_LABELS_" + i, "").length() > 0)
        {
            pieLabels = getItemsParameter("SERIE_LABELS_" + i);
        }
        if(getStringParam("SERIE_TOGETHER_" + i, "").length() > 0)
        {
            pieKeepTogether = convertBooleanList(getStringParam("SERIE_TOGETHER_" + i, ""));
        }
        pSeries[i] = new PieDataSerie(convertDoubleList(pSeriesData[i]), styles, pieKeepTogether, pieLabels);
        font = convertFont(getStringParam("SERIE_FONT_" + i, ""));
        if(font != null)
        {
            ((PieDataSerie)pSeries[i]).valueFont = font;
        }
        if(getStringParam("SERIE_FONT_" + i, "").compareTo("NULL") == 0)
        {
            ((PieDataSerie)pSeries[i]).valueFont = null;
        }
        c = convertColor(getStringParam("SERIE_COLOR_" + i, ""));
        ((PieDataSerie)pSeries[i]).valueColor = c;
        ((PieDataSerie)pSeries[i]).drawPercentages = getBooleanParam("SERIE_PERCENTAGES_" + i, true);
        Double d = getDoubleParam("SERIE_DISTCENTER_" + i, null);
        if(d != null)
        {
            ((PieDataSerie)pSeries[i]).textDistanceToCenter = d.doubleValue();
        }
        if(piePlot == null)
        {
            piePlot = new PiePlotter();
            piePlot.labelFormat = getStringParam("PIE_LABEL_FORMAT", "");
        }
        pSeries[i].name = pSeriesNames[i];
        piePlot.addSerie(pSeries[i]);
   	
		return PieChart;
	}*/
}
