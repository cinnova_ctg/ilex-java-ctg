
<SCRIPT language = JavaScript1.2>
var msaSend = '<bean:message bundle = "pm" key = "msa.detail.sendMSA.nopriviledgestatus"/>';
var msaCRS = '<bean:message bundle = "pm" key = "msa.detail.upload.state"/>';
<!--
if ( isMenu ) {
arMenu1 = new Array()
arMenu3 = new Array() 
arMenu2 = new Array(
130,
findPosX( 'pop2' ),findPosY( 'pop2' ),
"","",
"","",
"","",
	<%if( ( String )request.getAttribute( "list" ) != "" ){%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/>","#",1,
	<%}else{%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/>","#",0,
	<%}%>
		"<bean:message bundle = "pm" key = "msa.detail.Viewmsa"/>","#",1,
		
    <%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )){%>
   		 "<bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/>","javascript:msaSendEmail();",0,
		"Edit BDM","EditBdmAction.do?msaId=<%= MSA_Id %>&fromType=MSADetail",0,
   		 "<bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus"/>')",0,
    <%} else {%>
    	"<bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/>","javascript:alert(msaSend);",0,
    	<%if(msastatus.equals( "Cancelled" )){%>
	    	"Edit BDM","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.noprivilegetransstatus.bdm.cancelled"/>')",0,
	    	"<bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus.cancelled"/>')",0,
    	<%}else{%>
	    	"Edit BDM","EditBdmAction.do?msaId=<%= MSA_Id %>&fromType=MSADetail",0,
	    	"<bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/>","javascript:msaSetFormalDocDate();",0,
    	<%}%>
	<%}%>
	
	"<bean:message bundle = "pm" key = "msa.detail.Comments"/>","#",1,
	<% if( msastatus.equals( "Pending Customer Review" )){%>
	      "<bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/>","EntityHistory.do?function=modifyDocument&entityId=<%= MSA_Id %>&entityType=MSA",0,
	<%}else{%>
		 "<bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/>","javascript:alert(msaCRS)",0,	
	<%}%>
		
		<%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )){%>
			<%if(msastatus.equals( "Cancelled" )){%>
				"<bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')",0,
			<%}else{%>
				"<bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')",0,		
			<%}%>
		<%}else{%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/>","javascript:msaEditMsa();",0,
		<%}%>
		"Documents","#",1,	
		"<bean:message bundle = "pm" key = "msa.detail.menu.deletemsa"/>","javascript:msaDel();",0
)

<% if( ( String )request.getAttribute( "list" ) != "" ) {%>
arMenu2_1=new Array(
<%=( String ) request.getAttribute( "list" )%>
)
<%}%>

arMenu2_6=new Array(
"<bean:message bundle = "pm" key = "msa.detail.menu.viewall"/>","javascript:msaViewComments();",0,
<%if(msastatus.equals( "Cancelled" )){%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>');",0
	<%}else{%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/>","javascript:msaAddComment();",0
<%}%>
)

arMenu2_2=new Array(
<%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )){%>
	"Details","MSADetailAction.do?MSA_Id=<%= MSA_Id %>&Prjid=<%= MSA_Id %>",0,
	"<bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/>","javascript:msaView('pdf');",0	
<%}else{%>
	"Details","MSADetailAction.do?MSA_Id=<%= MSA_Id %>&Prjid=<%= MSA_Id %>",0,
	"<bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/>","javascript:msaView('pdf');",0,
	"<bean:message bundle = "pm" key = "msa.detail.menu.viewrtf"/>","javascript:msaView('rtf');",0,
	"<bean:message bundle = "pm" key = "msa.detail.menu.viewhtml"/>","javascript:msaView('html');",0
<%}%>

)
arMenu2_7 = new Array(
 "<bean:message bundle = "pm" key = "msa.detail.menu.upload"/>","javascript:msaUpload();",0
)

	arMenu2_9 = new Array(
	"MSA History","EntityHistory.do?entityId=<%= MSA_Id %>&entityType=MSA&function=viewHistory",0,
	"Support Documents","",1
	)

arMenu2_9_2 = new Array(
"Upload","EntityHistory.do?entityId=<%= MSA_Id %>&msaId=<%= MSA_Id %>&entityType=MSA Supporting Documents&function=addSupplement&linkLibName=MSA",0,
"History","EntityHistory.do?entityType=MSA Supporting Documents&msaId=<%= MSA_Id %>&linkLibName=MSA&function=supplementHistory",0
)

}
document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
//-->
</script>
