<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <c:choose>
            <c:when test="${empty requestScope.mode}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
            </c:when>
            <c:when test="${requestScope.mode ne 'S'}">
		<meta http-equiv="X-UA-Compatible" content="IE=8;">
            </c:when>
            <c:otherwise>
		<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
            </c:otherwise>
        </c:choose>
                
        <script type="text/javascript">
            var optValue = '${param.opt}';
        </script>

        <script src="/Ilex/javascript/jquery-1.11.0.min.js"></script>
        <script src="/Ilex/javascript/cookie_management.js"></script>
        <%
        if (session.getAttribute("login_raw") != null) {
            %>
            <script>
                var login_data = JSON.parse('<%=session.getAttribute("login_raw")%>');

                if (login_data.status === 'success') {
                    setCookie('cns-user_data', JSON.stringify(login_data.data), 1);
                    setCookie('cns-token', login_data.token, 1);

                    var api_url = '<%=session.getAttribute("api_url")%>';
                    $.ajax({
                        method: "POST",
                        url: api_url + "ilex/user/",
                        headers: {
                            "x-access-token": login_data.token,
                            "x-access-user-id": login_data.data.id
                        },
                        data: {
                            user_id: login_data.data.id
                        },
                        cache: true,
                        dataType: "json"         
                    }).done(function (res) {
                        if (res.status !== 'success') {
                            //window.location = "/Ilex/Login.do?logout=true";
                        }
                    }).error(function() {
                        //window.location = "/Ilex/Login.do?logout=true";
                    });
                } else {
                    deleteCookie('cns-user_data');
                    deleteCookie('cns-token');
                }
            </script>
            <%
        }
        %>

        <title>ILEX</title>
    </head>
    <form id="browserChangeForm" name="browserChangeForm" action="/Ilex-Dashport/changeBrowserode.html">
	<input type="hidden" value="" name="mode" id="mode" />
    </form>

    <%@include file="Top.jsp"%>
    <% 
    String url = request.getParameter("param");

    if(url!=null) {
	String token = request.getParameter("token");
	if(token != null){
            url = url+"&token="+token;
	}
        %>
        <iframe id="autoIframe" name="ilexbottom" scrolling="auto" frameborder="0" src="<%= url %>" style="width: 100%;" marginwidth="0" marginheight="0"></iframe>
        <noframes>
        <% 	
    } else {
	%>
	<iframe id="autoIframe" name="ilexbottom" scrolling="auto" frameborder="0" src="/content/landing_zone/" style="width: 100%;" marginwidth="0" marginheight="0"></iframe>
        <noframes>
	<%
    }
    %>
    <body>
        <p>This page uses frames, but your browser doesn't support them.</p>
    </body>
    </noframes>
    <script type="text/javascript">
 	$("#autoIframe").css("height", $(window).height() -97);
    </script>
</html>
