<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/summary.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='./dwr/interface/JobSetUpDao.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
function redirectToPO(obj)
{
	
	var url = "./CopyMpoToPoAction.do?hmode=createPO&tabId=0";
	document.forms[0].action = url;
	document.forms[0].submit();
		
}
</script>
	<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
</head>
	<body>
		<form name="CopyMpoToPoBean" method="post">		
		<table cellpadding="0" cellspacing="0" border="0">			
			<tr>
				<td width="100%">
				<input type="hidden" name ="jobId" value="<c:out value='${CopyMpoToPoBean.jobId}'/>"/>
				<input type="hidden" name="appendixId" value="<c:out value='${CopyMpoToPoBean.appendixId}'/>"/>
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
								<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
								<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
								
										<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
						</tr>
						<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
						         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
									 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
									 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
									  <a><span class="breadCrumb1">Copy MPO</a>
						    </td>
						</tr>
					</table>
				</td>
			</tr>
			<c:if test="${CopyMpoToPoBean.isValidToCopy eq 'False'}">
			<tr>
				<td class = "message">
					The selected MPO can not be used because of Unmatched resources.
				</td>
			</tr>
			</c:if>
			<c:if test="${CopyMpoToPoBean.isValidToCopy eq 'Exists'}">
			<tr>
				<td class = "message">
					The selected MPO can not be used because of overlapping resources.
				</td>
			</tr>
			</c:if>
			<tr>
				<td style="padding-left: 5px;"> 
					<table >
						
						<tr>
							<td colspan="2" class = "message">
								<h2 style="font-size: 14px;padding-left: 0px;">General Setup</h2>
							</td>
						</tr>
						<tr>
							<td class = "labeleboldhierrarchynowrap" width="10%">Deliver By Date:
							</td>
							<td><input type="text"  class="textbox" size="10" name="deliveryDate" readonly="true" value="<c:out value='${CopyMpoToPoBean.deliveryDate}'/>"/>
				 				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].deliveryDate, document.forms[0].deliveryDate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;" />
				 				<select name="deliveryHour" class="comboWhite">
				 					<c:forEach items="${CopyMpoToPoBean.hourList}" varStatus="index" var="itr">
										<option value="<c:out value='${itr.value}'/>" <c:if test="${itr.value eq CopyMpoToPoBean.deliveryHour}">selected="selected"</c:if>><c:out value='${itr.label}'/></option>
									</c:forEach>
								</select>
								<select name="deliveryMinute" class="comboWhite">
									<c:forEach items="${CopyMpoToPoBean.minuteList}" varStatus="index" var="itr">
										<option value="<c:out value='${itr.value}'/>"<c:if test="${itr.value eq CopyMpoToPoBean.deliveryMinute}">selected="selected"</c:if>><c:out value='${itr.label}'/></option>
									</c:forEach>
								</select>
								<select name="deliveryIsAm" class="comboWhite">
									<option value="AM"<c:if test="${CopyMpoToPoBean.deliveryIsAm eq 'AM'}">selected="selected"</c:if>>AM</option>
									<option value="PM"<c:if test="${CopyMpoToPoBean.deliveryIsAm eq 'PM'}">selected="selected"</c:if>>PM</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class = "labeleboldhierrarchynowrap" width="10%">Issue Date:
							</td>
							<td><input type="text"  class="textbox" size="10" name="issueDate" readonly="true" value="<c:out value='${CopyMpoToPoBean.issueDate}'/>"/>
				 				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].issueDate, document.forms[0].issueDate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;" />
				 				<select name="issueHour" class="comboWhite">
									<c:forEach items="${CopyMpoToPoBean.hourList}" varStatus="index" var="itr">
										<option value="<c:out value='${itr.value}'/>"<c:if test="${itr.value eq CopyMpoToPoBean.issueHour}">selected="selected"</c:if>><c:out value='${itr.label}'/></option>
									</c:forEach>
								</select>
								<select name="issueMinute" class="comboWhite">
									<c:forEach items="${CopyMpoToPoBean.minuteList}" varStatus="index" var="itr">
										<option value="<c:out value='${itr.value}'/>"<c:if test="${itr.value eq CopyMpoToPoBean.issueMinute}">selected="selected"</c:if>><c:out value='${itr.label}'/></option>
									</c:forEach>
								</select>
								<select name="issueIsAm" class="comboWhite">
									<option value="AM"<c:if test="${CopyMpoToPoBean.issueIsAm eq 'AM'}">selected="selected"</c:if>>AM</option>
									<option value="PM"<c:if test="${CopyMpoToPoBean.issueIsAm eq 'PM'}">selected="selected"</c:if>>PM</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2" class = "message">
								<h2 style="font-size: 14px;padding-left: 0px;">Master Purchase Orders</h2>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table cellpadding="0" cellspacing="1">
									<c:if test="${requestScope.listSize <= 0 }">
										<tr>
											<td class="message">
												No MPO found.
											</td>
										</tr>
									</c:if>
									<c:forEach items="${CopyMpoToPoBean.mpoId}" var="mpoItr" varStatus="index">
										<c:if test="${index.first}">
											<tr>
												<td class = "texthdNoBorder" colspan="2" width="40%"><b>Master Purchase Orders</b></td>
												<td class = "texthdNoBorder"><b>Master PO Item</td>
									           	<td class = "texthdNoBorder"><b>Estimated Total Cost</td>
									           	<td class = "texthdNoBorder" align ="center"><b>Status</td>
											</tr>
										</c:if>
										<tr>
											<c:set var="mpoItemName" value=""/>
											<c:if test="${CopyMpoToPoBean.masterPOItem[index.index] eq 'fp'}"><c:set var="mpoItemName" value="Fixed Price"/></c:if>
											<c:if test="${CopyMpoToPoBean.masterPOItem[index.index] eq 'ht'}"><c:set var="mpoItemName" value="Hourly Type"/></c:if>
											<c:if test="${CopyMpoToPoBean.masterPOItem[index.index] eq 'mt'}"><c:set var="mpoItemName" value="Materials"/></c:if>
											<c:if test="${CopyMpoToPoBean.masterPOItem[index.index] eq 'si'}"><c:set var="mpoItemName" value="Show Items"/></c:if>
											<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> ><input type="radio" name="mpoId" class="radio" <c:if test="${CopyMpoToPoBean.isSelectable[index.index] eq 'N' }"> disabled="disabled"</c:if> value="<c:out value="${CopyMpoToPoBean.mpoId[index.index]}"/>"onclick="redirectToPO(this);"></td>
											<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> ><c:out value="${CopyMpoToPoBean.mpoName[index.index]}"/></td>
											<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  align="center"><c:out value="${mpoItemName}"/></td>
								           	<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  align="center"> <fmt:formatNumber value='${CopyMpoToPoBean.estimatedTotalCost[index.index]}' type='currency' pattern='$ #####0.00'/></td>
								           	<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  align ="center"><c:out value="${CopyMpoToPoBean.status[index.index]}"/></td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
			<c:if test = "${requestScope.jobType eq 'Addendum'}">
				<%@ include  file="/AddendumMenuScript.inc" %>
			</c:if>
			<c:if test = "${requestScope.jobType eq 'inscopejob'}">
				<%@ include  file="/ProjectJobMenuScript.inc" %>
			</c:if>	
		</form>
	</body>
</html>