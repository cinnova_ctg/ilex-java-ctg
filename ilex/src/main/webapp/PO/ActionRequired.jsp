<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<script language = "JavaScript" src = "javascript/UtilityFunction.js"></script>
<script>
function setTotal(object){
	var value = '';
	for(var i=0; i<object.length; i++){
		if(object[i].checked){
			value = object[i].value;	
		}
	}
	if(value == 'payInFull'){
		document.forms[0].invoiceApprovedTotal.value = document.forms[0].lineTotal.value;
		document.forms[0].invoiceApprovedTotal.readOnly = true;
	}
	else{
		document.forms[0].invoiceApprovedTotal.readOnly = false;
	}
}
</script>
<script>
function saveApprovedTotal(){
	if(validate()){
		var appendixId = document.forms[0].appendixId.value;
		var jobId = document.forms[0].jobId.value;
		var poId = document.forms[0].poId.value;
		var tabId = document.forms[0].tabId.value;
		var url = "POAction.do?hmode=ApprovedEInvoice&jobId="+jobId+"&appendix_Id="+appendixId+"&poId="+poId+"&tabId="+tabId;
		document.forms[0].action = url;
		document.forms[0].submit();
	}
}

function validate(){

	//alert(getRedioSelectedIndex(document.forms[0].invoiceDisposition));
	if(getRedioSelectedIndex(document.forms[0].invoiceDisposition) < 0){
		alert('Please select eInvoice Disposition.\r')
		document.forms[0].explanationToPartner.focus();
		return false;
	}
	
	if(!document.forms[0].invoiceApprovedTotal.value == ''){
		if(isNaN(document.forms[0].invoiceApprovedTotal.value)){
			alert('Invalid integer entered.\r')
			document.forms[0].invoiceApprovedTotal.value = '';
			document.forms[0].invoiceApprovedTotal.focus();
			return false;
		} else if(eval(document.forms[0].invoiceApprovedTotal.value) == 0){
			alert('Please enter Approved Total.\r');
			document.forms[0].invoiceApprovedTotal.focus();
			return false;
		}
	}
	else{
		alert('Please enter Approved Total.\r')
		document.forms[0].invoiceApprovedTotal.focus();
		return false;
	}
	if(getRedioValue(document.forms[0].invoiceDisposition) == 'AdjustedPayment' && document.forms[0].explanationToPartner.value == ''){
		alert('Please enter Explanation to Partner.\r')
		document.forms[0].explanationToPartner.focus();
		return false;
	}
	return true;
}
</script>

<input type="hidden" name="lineTotal" value="<fmt:formatNumber value='${POBean.lineTotal}' type="currency" pattern="######0.00"/>">
<input type="hidden" name="invoiceId" value="<c:out value='${POBean.invoiceId}'/>">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr valign="top">
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td>
				<h3 style="font-size: 12px;padding-left: 0px;padding-top: 5px;">Action Required:</h3>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table style="padding-left: 5px;padding-right: 5px;"  class="Ntextoleftalignnowrappaleyellowborder2" cellpadding="0" cellspacing="0" border="0">
			<c:choose>
			<c:when test="${POBean.invoiceStatus eq 'Approved'}">
			<tr height="20">
				<td>&nbsp;&nbsp;</td>
				<td class="Ntextoleftalignnowrappaleyellowborder4" width="80%">
					<b>The eInvoice is approved. There is no further action required. Payment will be</br>
					handled by the Accounting department.</b>
				</td>
				<td>&nbsp;&nbsp;</td>
			</tr>
			</c:when>
			<c:when test="${POBean.invoiceStatus eq 'Paid'}">
			<tr height="40">
				<td>&nbsp;&nbsp;</td>
				<td class="Ntextoleftalignnowrappaleyellowborder4" valign="top" width="80%">
					<b>This eInvoice was paid. There is no further action required.</b>
				</td>
				<td>&nbsp;&nbsp;</td>
			</tr>
			</c:when>
			<c:when test="${POBean.invoiceStatus eq 'Pending Receipt' && POBean.poType eq 'PVS'}">
			<tr height="40">
				<td>&nbsp;&nbsp;</td>
				<td class="Ntextoleftalignnowrappaleyellowborder4" valign="top" width="80%">
					<b>This eInvoice is pending receipt.</b>
				</td>
				<td>&nbsp;&nbsp;</td>
			</tr>
			</c:when>
			<c:when test="${(POBean.invoiceStatus eq 'Pending Receipt') && (POBean.poType eq 'Minuteman' || POBean.poType eq 'Speedpay')}">
			<tr height="40">
				<td>&nbsp;&nbsp;</td>
				<td class="Ntextoleftalignnowrappaleyellowborder4" valign="top" width="80%">
					<b>This is a [<c:out value='${POBean.poType}' />] PO. No eInvoice is required. It will be automatically</br>
					approved and paid.</b>
				</td>
				<td>&nbsp;&nbsp;</td>
			</tr>
			</c:when>
			<c:when test="${POBean.invoiceStatus eq 'Received' && POBean.poType eq 'PVS'}">
				<tr>
					<td>&nbsp;&nbsp;</td>
					<td class="Ntextoleftalignnowrappaleyellowborder4"><b>eInvoice Disposition:</b></td>
					<td>&nbsp;</td>
					<td class="Ntextoleftalignnowrappaleyellowborder2">
						<html:radio name="POBean" property="invoiceDisposition" value="payInFull" styleClass="radio" onclick="setTotal(document.forms[0].invoiceDisposition);" style="margin-left: -4px;"></html:radio>
						<font class="Ntextoleftalignnowrappaleyellowborder2">Pay in full - Pay Invoice Total</font>
					</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="Ntextoleftalignnowrappaleyellowborder4">&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
					<td class="Ntextoleftalignnowrappaleyellowborder2">
						<html:radio name="POBean" property="invoiceDisposition" value="AdjustedPayment" styleClass="radio" onclick="setTotal(document.forms[0].invoiceDisposition);" style="margin-left: -4px;"></html:radio>
						<font class="Ntextoleftalignnowrappaleyellowborder2">Adjusted Payment. Enter amount and explanation below.</font>
					</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr><td colspan="5">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;&nbsp;</td>
					<td class="Ntextoleftalignnowrappaleyellowborder4"><b>Approved Total:</b></td>
					<td>&nbsp;&nbsp;</td>
					<td class="Ntextoleftalignnowrappaleyellowborder2">
						<html:text name="POBean" property="invoiceApprovedTotal" size="10" styleClass="text"></html:text>
						<font class="Ntextoleftalignnowrappaleyellowborder2">(Enter the valid dollar amount - do not include "$")</font>
					</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr valign="top">
					<td>&nbsp;&nbsp;</td>
					<td class="Ntextoleftalignnowrappaleyellowborder4"><b>Explanation to Partner:</b></td>
					<td>&nbsp;&nbsp;</td>
					<td class="Ntextoleftalignnowrappaleyellowborder2">
						<html:textarea name="POBean" property="explanationToPartner" rows="4" cols="65" styleClass="textarea"></html:textarea>
					</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5">&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" name="approved" value="Approve" class="button_c" onclick="saveApprovedTotal();"/>&nbsp;&nbsp;
						<input type="reset" name="reset" value="Cancel" class="button_c"/>
					</td>
				</tr>
			</c:when>
			</c:choose>
		</table>
	</td>
</tr>
</table>

