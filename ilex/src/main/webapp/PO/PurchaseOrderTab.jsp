<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<script>
function getSowAssumptionInstuction(checkType){
	   var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               }catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       }catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 if(checkType == 'sowAssumption'){
               		 	document.forms[0].woNOA.value =  response;
               		 }else if(checkType == 'specialIns'){
               		 	document.forms[0].woSI.value =  response;
               		} else if(checkType == 'spclCond'){
               		 	document.forms[0].special_condition.value =  response;
               }
        	}
        }
        var appendixId = document.forms[0].appendixId.value;
        var jobId = document.forms[0].jobId.value;
        var poId = document.forms[0].poId.value;
        
        ajaxRequest.open("POST", "POAction.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("hmode=sowAssumption&poId="+poId+"&check="+checkType+"&jobId="+jobId+"&appendixId="+appendixId); 
}
 
</script>
<script>
			
	function getAjaxRequestObject(){
	var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }	
	return ajaxRequest;        
}



	function getStandardContactLine() {			
		var ajaxRequest = getAjaxRequestObject();
		var initialSI = document.getElementById("sitextarea").value;
		//alert("initialSI: "+initialSI)	;
		    ajaxRequest.onreadystatechange = function(){
       if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){ 
           var xmlDoc=ajaxRequest.responseXML;        		
           		
           	var id;
			 id = xmlDoc.getElementsByTagName("standardConLine")[0];			    
			 var standardContactLine=id.getElementsByTagName("standardContactLine")[0].firstChild.nodeValue;					 
			 //alert("standardContactLine: "+standardContactLine);
			 document.getElementById("sitextarea").value = "";	
			 document.getElementById("sitextarea").value = initialSI+ standardContactLine;
		}
    }
   
    
		var url = "POAction.do?hmode=getStandarContactLine";		
        ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send(null); 
	
	}
</script>
     	<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" 
     	 	height="300" cellspacing="1" cellpadding="0" width="100%">
     		<tr valign="top" height="10">
     			<td  width="100%"  colspan="7" cellspacing="1" cellpadding="0">
     				<table width="100%" cellspacing="0" cellpadding="0" >
     					<tr>
     						<td width="100%">
			     				<jsp:include page="GeneralPOInformation.jsp"/>
						     </td>
					     </tr>
				    </table>
			     </td>
     		</tr>
     		<tr><td class="dbtopborderblue" >&nbsp;</td></tr>
     		<tr valign="top">
     			<td width="100%" colspan="2">
     				<table cellpadding="2" cellspacing="1" width="100%" border=0>
						<tr>
							<td>
								<h2 style="font-size: 12px;padding-left: 0px;">Purchase Order:</h2>
							</td>
						</tr>
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="1" cellpadding="2" border="0" class="Ntexteleftalignnowrapnoborder" border=0>
						     		<tr align="center" valign="top" height="25">
						     			<td  align="center" width="70%" class = "labelodarkborder">Special Conditions</td>
										<td width="40%">&nbsp;</td>
						     		</tr>
						     		<tr valign="center" >
						     			<td ><textarea  name="special_condition" cols="160" rows="8" class="textarea" <c:if test="${empty POBean.isEditablePO}">readonly="readonly"</c:if>><c:out value='${POBean.special_condition}'/></textarea></td>
						     			<td width="20%" valign="bottom">
						     				&nbsp;&nbsp;&nbsp;<input type="button" name="defaultPartnerSowAssumption"  class="button_c" value="Default" onclick="getSowAssumptionInstuction('spclCond');" <c:if test="${requestScope.copiedFromMpo eq 'y' || empty POBean.isEditablePO}">disabled="disabled"</c:if> />&nbsp;&nbsp;&nbsp;
						     			</td>
						     		</tr>
							 	</table>
					 		</td>
					 	</tr>
				 	</table>
				</td>
			</tr>
			<tr valign="top" height="10">
     			<td  colspan="2">
     				<table width="100%" cellspacing="1" cellpadding="2" border="0" >
     					<tr>
     						<td>
								<h2 style="font-size: 12px;padding-left: 0px;">Work Order:</h2>
							</td>
					     </tr>
				    </table>
			     </td>
     		</tr>
     		<tr valign="top">
     			<td colspan="2">
     			<table cellpadding="2" cellspacing="1" width="100%" border=0 class="Ntexteleftalignnowrapnoborder">
					<tr>
						<td>
							<table  cellspacing="1" cellpadding="2" border="0" width="100%">
					     		<tr valign="top" height="25" >
					     			<td align="center" colspan="2" class = "labelodarkborder">Description of Work Requested</td>
					     			<td>&nbsp;</td>
					     		</tr>
							     <tr>
									<td class='Nwrapwithbottomrightborder' width="20%" valign="top" nowrap>
										NATURE OF ACTIVITY <br>(SOW and Assumptions)
									</td>
									<td class = "colLight"  valign="middle"  >
										<table border="0"  height='100%' valign='middle' cellspacing="0" cellpadding="0" border=0>
											<tr>
												<TD >
													<textarea  name="woNOA" class="textarea" rows="6" cols="118" <c:if test="${empty POBean.isEditablePO}">readonly="readonly"</c:if>><c:out value='${POBean.woNOA}'/></textarea>
													
												</td>
												
											</tr>
										</table>
									 </td>
									<td valign="top"  width="20%" >&nbsp;
										<input type="button" name="defaultPartnerSowAssumption" class="button_c" value="Default" onclick="getSowAssumptionInstuction('sowAssumption');" <c:if test="${requestScope.copiedFromMpo eq 'y'  || empty POBean.isEditablePO}">disabled="disabled"</c:if> />
									</TD>
								</tr>
								<tr>
									<td class='Nwrapwithbottomrightborder' valign="top" nowrap>
										SPECIAL INSTRUCTIONS <br>(Additional details may be attached)
									</td>
									<td class = "colLight"   valign="middle" >
										<table  height='100%' valign='middle' cellspacing="0" cellpadding="0" border=0>
											<tr>
												<TD >
													<textarea id="sitextarea" name="woSI"  class="textarea" rows="6" cols="118" <c:if test="${empty POBean.isEditablePO}">readonly="readonly"</c:if>><c:out value='${POBean.woSI}'/></textarea>
												</td>
		
											</tr>
										</table>
									</td>
									<td class = "colLight"  valign="top">
										<input type="button" name="defaultPartnerSowAssumption" class="button_c" value="Default" onclick="getSowAssumptionInstuction('specialIns'); " <c:if test="${requestScope.copiedFromMpo eq 'y'  || empty POBean.isEditablePO}">disabled="disabled"</c:if> />
										</br><input type="button" name="add_contact" class="button_c" value="Add Contact" onclick="getStandardContactLine();" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>/>
									</TD>	
								</tr>
								<tr>
									<td class='Nwrapwithbottomrightborder'  valign="top" nowrap>
										REQUIRED EQUIPMENT <br>(Additional equipment may be required)
									</td>
									<td colspan ="2" width="100%">
								<table cellpadding="0" cellspacing="0" border="0" >
									<c:forEach var="list" items="${POBean.woToolsList}" varStatus="count">						         	
										<c:if test="${(count.index)%4 eq 0}">			         								
							         		<tr>
							         	</c:if>	
							         	<c:if test="${(count.index)%4 eq 0}">			         								
							         		<td  class='Ntexteleftalignnowrapfirstborder'>
							         	</c:if>	
							    		<c:if test="${(count.index)%4 ne 0 && (count.index+1)%4 ne 0 }">			         								
							         		<td  class='Ntexteleftalignnowraprightborder'>
							         	</c:if>
							           	<c:if test="${(count.index+1)%4 eq 0}">
							         		<td class='Ntexteleftalignnowrapwithoutborder' >
							         	</c:if>
												<input type="checkbox" name="woRE" class="chkbx" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> value="<c:out value='${list[0]}'/>" 
													<c:forEach var="innerList" items="${POBean.woRE}" varStatus="innerCount">	
														<c:if test="${list[0] eq POBean.woRE[innerCount.index]}">			         	
														checked="checked"
														</c:if>
													</c:forEach>	
												> 
												<c:if test="${list[5] eq 'N'}">
													<font color='red'>	
														<i><c:out value="${list[1]}"/> (Inactive!)<i>
													</font>
												</c:if>
												<c:if test="${list[5] ne 'N'}">
													<c:out value="${list[1]}"/>
												</c:if>
												<input type="hidden" name="workOrderCheck" value="<c:out value='${list[0]}'/>"></td>

										<c:if test="${(count.index+1)%4 eq 0 || count.last}">			         								
											<c:if test="${count.last && (count.index+1)%4 ne 0}">
												<c:if test="${(count.index+1)%4 eq 1}">
													<td  class='Ntexteleftalignnowraprightborder'>&nbsp;</td>
													<td  class='Ntexteleftalignnowraprightborder'>&nbsp;</td>
													<td  class='Ntexteleftalignnowrapwithoutborder'>&nbsp;</td>
												</c:if>
												<c:if test="${(count.index+1)%4 eq 2}">
													<td  class='Ntexteleftalignnowraprightborder'>&nbsp;</td>
													<td  class='Ntexteleftalignnowrapwithoutborder'>&nbsp;</td>
												</c:if>
												<c:if test="${(count.index+1)%4 eq 3}">
													<td  class='Ntexteleftalignnowrapwithoutborder'>&nbsp;</td>
												</c:if>
								         	</c:if>	
							         		</tr>
							         	</c:if>	

						         	</c:forEach>
						         	
								</table>
							</td>
						</tr>
								
						<tr>
							<td class='Nwrapwithbottomrightborder' width="20%" valign="top" nowrap>
								WORKMANSHIP AND STANDARDS
							</td>
							<td class = "colLight"  valign="middle"  >
								<table border="0"  height='100%' valign='middle' cellspacing="0" cellpadding="0" border=0>
									<tr>
										<TD>
											<textarea  name="workmanship" class="textarea" rows="4" cols="118" <c:if test="${empty POBean.isEditablePO}">readonly="readonly"</c:if>><bean:message bundle = "PRM" key="prm.powo.workmanship"/></textarea>
										</td>
									</tr>
								</table>
							 </td>
							<td valign="top"  width="20%" >&nbsp;</TD>
						</tr>
								
								
								
								
					   			<tr>
								   	<td  class = "colLight" align="left" colspan='2'>
								   		<input type="button" name="button1" class="button_c" onclick="savePO(1);" value="Save" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>/>
								   		<input type="reset" name="button2" class="button_c" value="Reset" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>/>
								   	</td>
								</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr height="60">
		<td align="right" valign="bottom" colspan="7">
			<jsp:include page="POFooter.jsp"/> 	
		</td>
	</tr>
</table>
     