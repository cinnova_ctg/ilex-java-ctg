<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
<% String timeinms = String.valueOf((new java.util.Date()).getTime()); 
 Boolean userRole = (Boolean)session.getAttribute("isUserRoleSeniorPM");
// boolean userRole = true;
%>
<script  language = "JavaScript" src = "javascript/POActivityResource/GeneralResource.js?<%=timeinms %>"></script>
<script  language = "JavaScript" src = "javascript/POActivityResource/OnChangeObject.js?<%=timeinms %>"></script>
<script  language = "JavaScript" src = "javascript/POActivityResource/POCost.js?<%=timeinms %>"></script>
<script  language = "JavaScript" src = "javascript/POActivityResource/PVSResource.js?<%=timeinms %>"></script>

	<script>
	function resetPage() 
	{
		<c:if test="${empty requestScope.save}">
			var url = "POAction.do?hmode=unspecified&isBuild=Y"
		</c:if>
		<c:if test="${ not empty requestScope.save}">
			var url = "POAction.do?hmode=unspecified"
		</c:if>
			document.forms[0].action = url;
			document.forms[0].submit();
	
		}
	</script>
	
	<script>
			var checkForFP = '<c:out value="${POBean.updateQuantity}"/>';
			var requestlistSize = '<c:out value="${requestScope.listSize}" />';
			<c:if test = '${not empty requestScope.poAuthorizedTotalCost}'>
				poAuthorized = '<c:out value="${requestScope.poAuthorizedTotalCost}" />';
			</c:if>
			<c:if test = '${not empty requestScope.poAuthorizedTotalCostDiff}'>
				poAuthorizedDiff = '<c:out value="${requestScope.poAuthorizedTotalCostDiff}" />';
			</c:if>
			var multiFactor  =0;
			<c:if test="${requestScope.isTicket eq 'Ticket'}">	
				multiFactor = 20;
			</c:if>
			<c:if test="${requestScope.isTicket ne 'Ticket'}">	
				multiFactor = 5;
			</c:if>
			var poStatus = '<c:out value="${POBean.poStatus}"/>';
			var updatable = '<c:out value="${POBean.updateQuantity}"/>';
			var isEditablePo = false;
			<c:if test="${not empty POBean.isEditablePO}">
				isEditablePo = true;
			</c:if>
		
		
	</script>
</head>
<input type="hidden" name="updateQuantity" value="<c:out value="${POBean.updateQuantity}" />">
<table cellpadding="0" cellspacing="0" border=0   width="100%">
	<tr>
		<td>
			<table   width="100%">
				<tr>
					<td >
					<c:set var="prevJobfield"  value=""/>
					<c:set var="currJobfield"  value=""/>
					<c:set var="prevActfield" value=""/>
					<c:set var="currActfield" value=""/>
						<table cellpadding="2" cellspacing="1" width="100%" class="Ntextoleftalignnowrappaleyellowborder2">
							<tr><td style="padding-left:10px;padding:3px" >
							<table cellpadding="1" cellspacing="1" width="100%"  id="actTable" border="0"> 
										<tr>						
											<td align="center" class="texthNoBorder" rowspan="2" colspan="1"><b>Activity</td>
											<td align="center" class="texthNoBorder" rowspan="2"><b>Resource</td>
											<td align="center" class="texthNoBorder" rowspan="2" width="2%"><b>Rev</td>
											<td align="center" class="texthNoBorder" rowspan="2"><b>Type</td>
											<td align="center" class="texthNoBorder" colspan="3"><b>Quantity</td>
											<td align="center" class="texthNoBorder" colspan="2"><b><b>Estimated Cost</td>
											<td align="center" class="texthNoBorder" colspan="3" id="planned"><b>As Planned</td>
											<td align="center" class="texthNoBorder" rowspan="2"><b>Show<br>on<br>WO</td>
										</tr>
										<tr>
											<td class="texthNoBorder"><b>Activity</td>
											<td class="texthNoBorder"><b>Resource</td>
											<td class="texthNoBorder"><b>Total</td>
											<td class="texthNoBorder"><b>Unit</td>
											<td class="texthNoBorder"><b>Subtotal</td>
											<td class="texthNoBorder" id="planned" ><b>Quantity</td>
											<td class="texthNoBorder" nowrap="nowrap" id="planned"><b>Unit Cost</td>
											<td class="texthNoBorder" id="planned" ><b>Subtotal</td>
										</tr>
										<c:if test="${requestScope.listSize <= 0}">
											<tr>
												<td class="dblabel" colspan="14">No activity Present.</td>
											</tr>
										</c:if>
									<c:forEach var="list" items="${requestScope.actvityList}" varStatus="count">
										<c:if test="${prevJobfield eq list.activityId}">
											<c:set var="currJobfield"  value=""/>
											<c:if test="${prevActfield eq list.resourceId}">
												<c:set var="currActfield"  value=""/>
											</c:if>
											<c:if test="${prevActfield ne list.resourceId}">
												<c:set var="currActfield"  value="${list.resourceId}"/>
												<c:set var="prevActfield"  value="${list.resourceId}"/>
											</c:if>
										</c:if>
										<c:if test="${prevJobfield ne list.activityId}">
											<c:set var="currJobfield"  value="${list.activityId}"/>
											<c:set var="currActfield"  value="${list.resourceId}"/>
											<c:set var="prevJobfield"  value="${list.activityId}"/>
											<c:set var="prevActfield"  value="${list.resourceId}"/>
										</c:if>	
										
											<tr>
												<input type="hidden" name="activityResourceId" value="<c:out value='${list.activityId}'/>~<c:out value='${list.resourceId}'/>">
												<input type="hidden" name="checkInTransit" value="<c:out value='${list.inTransit}'/>" > 
												
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> valign="middle"><c:if test="${not empty currJobfield}"><input type="checkbox" name="activityId" value="<c:out value='${list.activityId}'/>"  <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> onclick="calculateByActivity(this,'actTable');" <c:forEach var="innerList" items="${POBean.mpoActId}" varStatus="innerCount">
																																																																					<c:if test="${list.activityId eq POBean.mpoActId[innerCount.index]}">
																																																																						checked = "checked" 
																																																																					</c:if>
																																																																				</c:forEach>> </c:if><c:if test="${not empty currJobfield}"><c:out value='${list.activityName}'/></c:if></td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> valign="middle"><input type="checkbox" name="mpoActivityResource" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> <c:forEach var="innerList" items="${POBean.mpoActivityResource}" varStatus="innerCount">
																																																																					<c:if test="${list.resourceId eq POBean.mpoActivityResource[innerCount.index]}">
																																																																						checked = "checked" 
																																																																					</c:if>
																																																																				</c:forEach>class="chkbx" name="resourceId" value="<c:out value="${list.resourceId}" />" height="5" onclick="calculateSubTotal(this,'actTable');" 
																																																																				 >&nbsp;&nbsp;<c:out value="${list.resourceName}"/> <input type="hidden" name="resourceHidden" value="<c:out value="N" />"/></td>

												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center" width="2%"><c:out value="${list.revision}" /></td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center" title="<c:out value='${list.resourceSubName}'/>"><c:out value="${list.type}" /></td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center" ><c:out value="${list.activityQuantity}" /></td>
												<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"><fmt:formatNumber value="${list.resourceQuantity}" type="currency" pattern="#####0.0000"/></td>
												<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"></td>
												<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"><fmt:formatNumber value="${list.estimatedCostUnit}" type="currency" pattern="#####0.00"/></td>
												<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"></td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> id="planned" ><input type="text" name="mpoActivityPlanQuantity" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> <c:forEach var="innerList" items="${POBean.mpoActivityResource}" varStatus="innerCount">
																																																																					<c:if test="${list.resourceId eq POBean.mpoActivityResource[innerCount.index]}">
																																																																						value="<fmt:formatNumber value="${POBean.mpoActivityPlanQuantity[innerCount.index]}" type="currency" pattern="#####0.0000"/>" 
																																																																					</c:if>
																																																																					
																																																																				</c:forEach>class="text" style="text-align:right"
																																																																				 size="8" onChange="checkIntransitQty('<c:out value="${list.inTransit}"/>', this); calCostForPlan(this);exceedSubtotal('<c:out value="${count.index}"/>',this);return false;"/> 
																																																																				</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> id="planned"><input type="text" size="14" name="mpoActivityPlanUnitCost"  <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> <c:forEach var="innerList" items="${POBean.mpoActivityResource}" varStatus="innerCount">
																																																																					<c:if test="${list.resourceId eq POBean.mpoActivityResource[innerCount.index]}">
																																																																						value="<fmt:formatNumber value="${POBean.mpoActivityPlanUnitCost[innerCount.index]}" type="currency" pattern="#####0.00"/>" 
																																																																					</c:if>
																																																																					
																																																																				</c:forEach> class="text" style="text-align:right"
																																																																				size="8" onblur="" onChange="calCostForPlan(this);exceedSubtotal('<c:out value="${count.index}"/>',this);return false;"/></td>
												<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> id="planned" align="center"></td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"><input type="checkbox" name="mpoActivityIsWOShow" class="chkbx" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if><c:forEach var="innerList" items="${POBean.mpoActivityResource}" varStatus="innerCount">
																																																																								<c:if test="${list.resourceId eq POBean.mpoActivityResource[innerCount.index]}">
																																																																									<c:if test="${ POBean.mpoActivityIsWOShow[innerCount.index] eq 'Y'}">
																																																																										checked = "checked"
																																																																									</c:if>
																																																																								</c:if>
																																																																								</c:forEach> value="<c:out value="${list.resourceId}" />" ><input type="hidden" name="isShowHidden" value="Y"/></td>
												<input type="hidden" name="mpoActResourceSubNamePre" value="<c:out value="${list.resourceSubNamePre}" />"/>		
												<input type="hidden" name="overrideMinumtemanCostCheckedHidden" value="<c:out value="${POBean.overrideMinumtemanCostChecked}"/>"></input>																																																																			
												<input type="hidden" name="mpoActResourceSubName" value="<c:out value="${list.resourceSubName}" />"/>
												<input type="hidden" name="mpoActId" value="<c:out value="${list.activityId}" />"/>
												<%--<input type="hidden" name="mpoJobId" value="<c:out value="${list[1]}" />"/>--%>
												<input type="hidden" name="mpoActResourceSubId" value="<c:out value="${list.resourceSubId}" />"/>
												<input type="hidden" name="mpoTotal" value="">
												<input type="hidden" name="savedMpoUnit" value="<fmt:formatNumber value="${list.estimatedCostUnit}" type="currency" pattern="######0.00"/>">
												<input type="hidden" name="mpoUnit" value="<fmt:formatNumber value="${list.estimatedCostUnit}" type="currency" pattern="######0.00"/>">
												<input type="hidden" name="mpoSubTotal" value="">
												<input type="hidden" name="mpoActivityPlanSubTotal" value="">
												<input type="hidden" name="mpoRes" value="<fmt:formatNumber value="${list.resourceQuantity}" type="currency" pattern="######0.0000"/>">
												<input type="hidden" name="mpoAct" value="<c:out value="${list.activityQuantity}" />">
												<input type="hidden" name="mpoType" value="<c:out value="${list.typeId}" />">
												<input type="hidden" name="mpoTypeName" value="<c:out value="${list.type}" />">																																																											
															
											</tr>
									
										<c:if test="${count.last}">
											<tr id="est">
												<input type="hidden" name="estimatedCost" value="<c:out value="${POBean.estimatedCost}"/>">
												<input type="hidden" name="authorizedTotalCost" value="<c:out value="${POBean.authorizedTotalCost}"/>">
												<input type="hidden" name="poAuthorizedTotalCost" value="<c:out value="${POBean.authorizedTotalCost}"/>">
												<input type="hidden" name="deltaCost" value="<c:out value="${POBean.deltaCost}"/>">
												<input type="hidden" name="mpoTotalCost" value="<c:out value="${POBean.mpoTotalCost}"/>">
												<td   colspan=1 class='Ntextoleftalignnowrappaleyellowborder2'><input type="button" name="selectAll" value="Select All" class="button_c" onclick="checkSelectAll('actTable');" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
													<input type="button" name="deSelectAll" value="Deselect All" class="button_c" onclick="checkDeSelectAll('actTable');" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
												</td>
												
												<td class='Ntextorightalignnowrappaleyellowborder2' colspan="1">
													<c:if test="${POBean.poFooterSource ne 'New'}">
														MPO Total Cost: <fmt:formatNumber value="${POBean.mpoTotalCost}" type="currency" pattern="######0.00"/>
													</c:if>
												</td>
												
												<td id='imgMeetOrBeat' align="center" valign='middle'  colspan=4 class='Ntextorightalignnowrappaleyellowborder2' valign="top"><img src="images/MeetOrBeat_small.gif" ></td>
												<td colspan="2" align="right" valign='middle' class='Ntextorightalignnowrappaleyellowborder2'>
													&nbsp;Estimated Total Cost:</td>
													<td class='Ntextoleftalignnowrappaleyellowborder2' colspan="1"></td>
												<td class='Ntextoleftalignnowrappaleyellowborder2' colspan="1">&nbsp;</td>
												<td class='Ntextoleftalignnowrappaleyellowborder2' colspan="1" id="planned" width="20%">Authorized Cost:</td>
												<td class='Ntextocenteralignnowrappaleyellowborder' colspan="1" id="planned" width="20%"></td>
												<td class='Ntextocenteralignnowrappaleyellowborder' colspan="1" id="planned" width="20%"></td>
											</tr>
											
											<tr valign="top" id = "showAuthorizedCostInput" style="display:none;" bordercolor="red">
													<td class = "Ntextoleftalignnowrappaleyellowborder2" colspan="11">&nbsp;</td>
													
													<td class = "Ntextoleftalignnowrappaleyellowborder2" colspan="1" align="right">
														<div style="position: relative;margin-top: -12px;">
														<table>
														<tr>
														<td>
														<input type="text" size="6" name="authorizedCostInput" class="text" style="text-align:right" value="<fmt:formatNumber value="${POBean.authorizedCostInput}" type="currency" pattern="#####0.00" />" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> onblur="checkFloatAuthorizedinput(this);calEstimateCost();changeAuthorizedInput();return false;"/>
														</td>
														</tr>
														</table>
														</div>
													</td>
													<td class = "Ntextoleftalignnowrappaleyellowborder2" colspan="1">&nbsp;</td>
											</tr>
											<tr>
												<td align="right" class = "Ntextoleftalignnowrappaleyellowborder2" colspan="10"></td>
												<td class = "Ntextoleftalignnowrappaleyellowborder2" id="planned" colspan="1" width="20%">Delta Cost:</td>
												<td class = 'Ntextocenteralignnowrappaleyellowborder' id="planned" colspan="1" width="20%"></td>
												<td class = 'Ntextocenteralignnowrappaleyellowborder' id="planned" colspan="1" width="20%"></td>
											</tr>
											<tr>
												<td align="right" class = "Ntextoleftalignnowrappaleyellowborder2" colspan="1">
												<td colspan="1" align='left'></td>
												<td colspan="10" id="exceeds" align='right' wrap></td>
												<td colspan="1"></td>
											</tr>
											
											<tr valign="middle" id = "AuthorizedInputExceeds" style="display:none;">
													<td class = "Ntextorightalignnowrappaleyellowborder2bold" colspan="12">Authorized Total cannot exceed Estimated Total Cost by 5%.</td>
													<td class = "Ntextoleftalignnowrappaleyellowborder2" colspan="1">&nbsp;</td>
											</tr>
											<tr>
												<td align="right" class = "Ntextoleftalignnowrappaleyellowborder2" colspan="10" id="button">
													<c:if test="${POBean.updateQuantity ne 'Update Quantity'}">
													<c:if test="${not empty requestScope.tabId}">
														<input type="button" name="Reset" value="Reset"  class="button_c" onClick="resetPage();" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
														
													</c:if>
													</c:if>
													<c:if test="${POBean.updateQuantity eq 'Update Quantity'}">
													<input type="button" name="Reset" value="Reset"  class="button_c" onClick="resetPage();">
													</c:if>
													<c:if test="${empty requestScope.tabId}">
														&nbsp;
													</c:if>
												</td>
												<td  class = "Ntextorightalignnowrappaleyellowborder2" colspan="1" id="button">
												<c:if test="${not empty requestScope.tabId}">
													<c:choose>
													<c:when test="${POBean.updateQuantity eq 'Update Quantity'}">
													<input type="button" name="Save" value="Save" class="button_c" onClick='onFPSave("0","<%=userRole%>");'>
													</c:when>
													<c:when test="${POBean.updateQuantity ne 'Update Quantity'}">
													<input type="button" name="Save" value="Save" class="button_c" onClick='onSave("0","<%=userRole%>")' <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> >
													</c:when>
													</c:choose>
												</c:if>
												<c:if test="${empty requestScope.tabId}">
													&nbsp;
												</c:if>
												</td>
												<td class = "Ntextoleftalignnowrappaleyellowborder2" colspan="10">&nbsp;</td>
											</tr>
											
												<tr valign="top" id = "showTr1" style="display:none;">
													<td id = "showTd" class = "Ntextorightalignnowrappaleyellowborder2bold" colspan="12"><b>Note: The saved authorized cost of $<c:out value="${requestScope.poAuthorizedTotalCost}" /> is not the same as the displayed value. Verify before changing this PO.</b></td>
													<td class = "Ntextoleftalignnowrappaleyellowborder2" colspan="1">&nbsp;</td>
												</tr>
												<tr valign="top" id = "showTr2" style="display:none;">
													<td class = "Ntextoleftalignnowrappaleyellowborder2" colspan="2">&nbsp;</td>
												</tr>
										</c:if>
									</c:forEach>
							</table>
						</table>	
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table >
				<tr <c:if test="${requestScope.listSize <=0 || empty requestScope.listSize}"> style="display:none" </c:if> id="generalTable" >
					<td valign="top">
						<table cellpadding="0" cellspacing="0" >
							<tr>
								<td class="dbvaluesmallFont" valign="top" id="header"><b>Estimated Cost Variations by PO Type</b></td>								
							</tr>
							<tr>
								<td align="right">
									<table  class="labelowhitebackgroundblueborder" id="CVPT">
										<tr>
											<td class="labelowhitebackgroundblueborder" width="80" nowrap="nowrap">&nbsp;
											</td>
											<td class="labelowhitebackgroundblueborder" width="100" nowrap="nowrap"> Labor
											</td>
											<td class="labelowhitebackgroundblueborder" width="80" nowrap="nowrap"> Travel
											</td>
											<td class="labelowhitebackgroundblueborder" width="80" nowrap="nowrap">Total
											</td>
											<td class="labelowhitebackgroundblueborder" width="80" nowrap="nowrap"><b> Savings</b>
											</td>
										</tr>
										<tr>
											<td class="labeloleftwhitebackgrounddblueborder" align="left">Minuteman
											</td>
											<td class="labelorightwhitebackgrounddblueborder" >$1,000.00
											</td>
											<td class="labelorightwhitebackgrounddblueborder" >$1,000.00
											</td>
											<td class="labelorightwhitebackgrounddblueborder" >$2,000.00
											</td>
											<td class="labelorightwhitebackgrounddblueborder" ><b>$2,000.00
											</td>
										</tr>
										<tr>
											<td class="labeloleftwhitebackgrounddblueborder" align="left">Speedpay
											</td>
											<td class="labelorightwhitebackgrounddblueborder" >$1,500.00
											</td>
											<td class="labelorightwhitebackgrounddblueborder" >$1,500.00
											</td>
											<td class="labelorightwhitebackgrounddblueborder" >$3,000.00
											</td>
											<td class="labelorightwhitebackgrounddblueborder" ><b>$1,000.00
											</td>
										</tr>
										<tr>
											<td class="labeloleftwhitebackgrounddblueborder" align="left">PVS
											</td>
											<td class="labelorightwhitebackgrounddblueborder" >$2,500.00
											</td>
											<td class="labelorightwhitebackgrounddblueborder" >$2,500.00
											</td >
											<td class="labelorightwhitebackgrounddblueborder">$4,000.00
											</td>
											<td class="colDarkNoWidth" >&nbsp;
											</td>
										</tr>
									</table> 
								</td>
							</tr>
						</table>
					</td>
					<td valign="top" align="right" width="50%" style="padding-left: 10px;padding-right: 10px">
					<table cellpadding="0" cellspacing="0" >
						<tr>
							<td class="dbvaluesmallFont" valign="top" id="header"><b>Authorized Cost Breakdown</b></td>								
						</tr>
						<tr>
							<td>
								<table class="labelowhitebackgroundblueborder" id="costTable" >
									<tr>
										<td class="labelowhitebackgroundblueborder" align="center" width="80">&nbsp;</td>
										<td class="labelowhitebackgroundblueborder" align="center" width="80">Labor</td>
										<td class="labelowhitebackgroundblueborder" align="center" width="80">Travel</td>
										<td class="labelowhitebackgroundblueborder" align="center" width="80">Materials</td>
										<td class="labelowhitebackgroundblueborder" align="center" width="80">Freight</td>
										<td class="labelowhitebackgroundblueborder" align="center" width="80"><b>Totals</b></td>
									</tr>
									<tr>
										<td class="labeloleftwhitebackgrounddblueborder">Contract</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
									</tr>
									<tr>
										<td class="labeloleftwhitebackgrounddblueborder">Contingent</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" >$1,000.00</td>
									</tr>
									<tr>
										<td class="labeloleftwhitebackgrounddblueborder"><b>Totals</b></td>
										<td class="labelorightwhitebackgrounddblueborder" ><b>$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" ><b>$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" ><b>$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" ><b>$1,000.00</td>
										<td class="labelorightwhitebackgrounddblueborder" ><b>$1,000.00</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
<td>
	<table>
		<tr>
			<c:if test="${POBean.overrideMinumtemanCostChecked eq 1}">
			<td class="labeloleftwhitebackgrounddblueborder" style=" border: none; "><input type="checkbox" checked="checked" <%if(userRole.toString().equalsIgnoreCase("false")){ %>disabled <%} %>  name="overrideMinumtemanCostChecked"/> </td>
			</c:if>
			<c:if test="${POBean.overrideMinumtemanCostChecked eq 0}">
			<td class="labeloleftwhitebackgrounddblueborder" style=" border: none; "><input type="checkbox" <%if(userRole.toString().equalsIgnoreCase("false")){ %>disabled <%} %>  name="overrideMinumtemanCostChecked"/> </td>
			</c:if>
			<td class="labeloleftwhitebackgrounddblueborder" style=" border: none; "><bean:message key="override.minuteman.minimumun.cost"  bundle="pm"  />   </td>
		</tr>
	</table>
</td>
</tr>
</table>
<script>
onPageLoad();
</script>