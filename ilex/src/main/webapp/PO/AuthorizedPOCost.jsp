<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" 
     	  cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border=0 width="100%">
				<tr>
					<td valign="top">
						<jsp:include page="GeneralPOInformation.jsp"/>
					</td>
				</tr>
				<tr>
					<td valign="top" width="100%" >
						<jsp:include page="GeneralActivityResource.jsp"/>
					</td>
				</tr>
				<tr>
					<td align="right" valign="bottom">
						<jsp:include page="POFooter.jsp"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>