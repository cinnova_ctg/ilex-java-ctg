<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<table border="0" align="right" >
	<tr>
		<td class = "smallGray" valign="bottom">
			Source: <c:out value='${POBean.poFooterSource}'/>,&nbsp;
		</td>
		<td class = "smallGray" valign="bottom">
			Created By: <c:out value='${POBean.poFooterCreateBy}' />, Created: <c:out value='${POBean.poFooterCreateDate}'/><c:if test="${not empty POBean.poFooterChangeDate}">,</c:if>
		</td>
		<c:if test="${not empty POBean.poFooterChangeDate}">
			<td class = "smallGray" valign="bottom">
				&nbsp;Updated By: <c:out value='${POBean.poFooterChangeBy}'/>, Updated: <c:out value='${POBean.poFooterChangeDate}'/>
			</td>
		</c:if>
	</tr>
</table>