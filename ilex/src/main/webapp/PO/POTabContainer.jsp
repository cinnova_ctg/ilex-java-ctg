<!DOCTYPE HTML>

<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.mind.common.*,com.mind.newjobdb.business.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/summary.css" rel="stylesheet" type="text/css" />

<script type='text/javascript' src='./dwr/interface/JobSetUpDao.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>

<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>

<title>MPOList</title>
<% String nonPm=request.getParameter("NonPm");%>
<%@ include  file="/DashboardVariables.inc" %>
<script>
	function savePO(tabId)
	{	
		if(tabId == '3')
		{
			if(is_empty(document.all.devTitle,'Title')){
				return false;
			}
		}
		var url = "POAction.do?hmode=savePO&poId="+document.forms[0].poId.value+"&tabId="+tabId+"&mode=view&appendix_Id="+document.forms[0].appendixId.value+"&jobId="+document.forms[0].jobId.value;		
		document.forms[0].action = url;
		document.forms[0].submit();
	}
	function is_empty(chkObj, message)
	{
		if(chkObj.value == '')
			{
				alert(message + ' is required.');
				chkObj.focus();
				return true;
			}
		return false;

	}
	function is_radio(chkObj, message)
	{
		if (!chkObj.checked) {
			alert(message +' is required.');
			return true;
			}
		return false;
	}

	function getCheckedValue(radioObj) {
		if(!radioObj)
			return "";
		var radioLength = radioObj.length;
		if(radioLength == undefined)
			if(radioObj.checked)
				return radioObj;
			else
				return "";
		for(var i = 0; i < radioLength; i++) {
			if(radioObj[i].checked) {
				return radioObj[i];
			}
		}
		return "";
	}
</script>
<script>

function fun_HideShowBlock(index,isClicked)
{
	if(isClicked!='')
	{
		if(index==0){
			var url = "./POAction.do?poId="+document.forms[0].poId.value+"&tabId="+index+"&appendix_Id="+document.forms[0].appendixId.value+"&jobId="+document.forms[0].jobId.value;
			<c:if test="${empty requestScope.save}">
			var url = "./POAction.do?poId="+document.forms[0].poId.value+"&isBuild=Y&tabId="+index+"&appendix_Id="+document.forms[0].appendixId.value+"&jobId="+document.forms[0].jobId.value;
			</c:if>
		}
		else
			var url = "POAction.do?hmode=unspecified&poId="+document.forms[0].poId.value+"&tabId="+index+"&mode=view&appendix_Id="+document.forms[0].appendixId.value+"&jobId="+document.forms[0].jobId.value;
		document.forms[0].action = url;
		document.forms[0].submit();
	}
	else{
    	var len=document.all.tab.length;
        for(var id=0;id<len;id++){
        	if(document.all.tab[id].style.display=="block"){
        		document.all.tab[id].style.display="none";
        		}
        }
		document.all.tab[index].style.display="block"; 
		buttonPressed(index);
	}
	
}
function buttonPressed(idx)
		{
			var length = document.all.TOP_BUTTON.length/3;
				for(var i=0;i<document.all.TOP_BUTTON.length/3;i++) {
					document.all.TOP_BUTTON[3*i+1].style.fontSize='10';
					//document.all.TOP_BUTTON[3*i+1].width='150';
					document.all.TOP_BUTTON[3*i+1].width=document.all.TOP_BUTTON[3*i+1].innerHTML.length*8;
					document.all.TOP_BUTTON[3*i+1].style.fontFamily='Verdana|Arial|Helvetica|sans-serif';
					if(length==1)
						document.all.TOP_TAB.style.width='5';
					else
						document.all.TOP_TAB[i].style.width='5';
	
					if(i==idx)
					{
						/*if(idx==0){
							document.all.TOP_BUTTON[3*i].style.display='block';
							document.all.TOP_BUTTON[3*i+1].style.display='block';
							document.all.TOP_BUTTON[3*i+2].style.display='block';
						}*/
						document.all.TOP_BUTTON[3*i].innerHTML='<img src="images/left-cor-light.gif" width="5" height="18" border="0">';
						document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#D9DFEF';
						document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="images/right-cor-light.gif" width="5" height="18"  border="0">';
						document.all.TOP_BUTTON[3*i+1].style.borderBottom = '0px #D9DFEF solid';
						document.all.TOP_BUTTON[3*i+1].style.fontWeight='bold';
					}
					else//Restore Remaining
					{
						/*if(idx==0){
							document.all.TOP_BUTTON[3*i].style.display='none';
							document.all.TOP_BUTTON[3*i+1].style.display='none';
							document.all.TOP_BUTTON[3*i+2].style.display='none';
						 }else{*/
							document.all.TOP_BUTTON[3*i].innerHTML='<img src="images/left-cor.gif" width="5" height="18"  border="0">';
							document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#B5C8D9';
							document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="images/right-cor.gif" width="5" height="18"  border="0">';
						    document.all.TOP_BUTTON[3*i+1].style.borderBottom = '2px #B5C8D9 solid';
							document.all.TOP_BUTTON[3*i+1].style.fontWeight='normal';
						//}
					}
				}
			
		}

</script>

<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
</head>
<body onload="<c:if test="${not empty requestScope.tabId}"><c:if test="${requestScope.tabId eq 0}">changePOItem();</c:if>fun_HideShowBlock('<c:out value="${requestScope.tabId}"/>','');</c:if>">
<form action="POAction" method='post' name="POBean"  type="com.mind.PO.formbean.POBean">
	<input type="hidden" name="appendixId" value="<c:out value='${POBean.appendixId}'/>"/>
	<input type="hidden" name="jobId" value="<c:out value='${POBean.jobId}'/>"/>
	<input type="hidden" name="poId" value="<c:out value='${POBean.poId}'/>"/>
	<input type="hidden" name="mpoId" value="<c:out value='${POBean.mpoId}'/>"/>
	<input type="hidden" name="tabId" value="<c:out value='${POBean.tabId}'/>"/>
	<input type="hidden" name="poStatus" value="<c:out value='${POBean.poStatus}'/>"/>
	<input type="hidden" name="updateQuantity" value="<c:out value='${POBean.updateQuantity}'/>"/>
	<input type="hidden" name="appendixType" value="<c:out value='${POBean.appendixType}'/>" />
	
	<input type="hidden" name="isNewPO" value="<c:out value='${POBean.isNewPO}'/>" />

	<c:if test = "${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/Menu_Addendum.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob'}">
		<%@ include  file="/Menu_ProjectJob.inc" %>
	</c:if>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
						 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
						 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
						  <a><span class="breadCrumb1">Purchase Order</a>
			    </td>
			</tr>
			<tr><td height="5" >&nbsp;</td></tr>
		</table> 
	<TABLE cellspacing="1" cellpadding="1"  border="0" >
		<tr >
			<td>
				<TABLE cellspacing="0" cellpadding="0" style="padding-left: 12px;" >
					<tr>
						<td>
							<div class="tab_nav">
								<ul>
								 	<%
								 	String[] tabNames = {
							 			"Authorized Cost",
							 			"Purchase Order/Work Order",
							 			"Document Management",
							 			"Deliverables",
							 			"Directions"
						 			};
								 	
								 	if(request.getParameter("isBuild")!=null && request.getAttribute("save")==null) {
							 			String[] newTabNames  = {"Authorized Cost"};
							 			tabNames = newTabNames;
							 		}
								 	
								 	if(request.getAttribute("poStatus") != null && request.getAttribute("poStatus").equals("Completed")) {
							 			if(request.getAttribute("poType") != null && !request.getAttribute("poType").equals("Internal")) {
								 			String[] newTabNames = {
									 			"Authorized Cost",
									 			"Purchase Order/Work Order",
									 			"Document Management",
									 			"Deliverables",
									 			"Directions",
									 			"eInvoice"
								 			};
								 			tabNames = newTabNames;
							 			}
							 		}
								 
								 	for(int tabIndex=0;tabIndex<tabNames.length;tabIndex++) {
								 		%>
										<li id="tab_<%=tabIndex%>"><a onclick="fun_HideShowBlock('<%=tabIndex%>','<%=tabIndex%>');"><%=tabNames[tabIndex]%></a></li>
										<%
								 	}
									%>
									</ul>
								</div>
							</td>
						</tr>
					     
                                                    <%
                                                    String selectedTab = "";
                                                    %>
						   <!--- Include SubMPOList.jsp for tab0. -->
							  <tr valign="top" id="tab" style="display:none;">
					  			<td width="100%">
					  				<c:if test="${requestScope.tabId eq 0}">
										<jsp:include page="AuthorizedPOCost.jsp"/>
                                                                                <%
                                                                                selectedTab = "tab_0";
                                                                                %>
									</c:if>
					  			</td>
					  		</tr>
							   <!--- Include ActivityResource.jsp for tab1. -->
							  <tr valign="top" id="tab" style="display:none;">
					  			<td width="100%">
					  				<c:if test="${requestScope.tabId eq 1}">
					  					<jsp:include page="PurchaseOrderTab.jsp"/> 
                                                                                <%
                                                                                selectedTab = "tab_1";
                                                                                %>
					  				</c:if>
					  			</td>
					  		</tr>

						<!--- Include PurchaseOrderTab.jsp for tab1.-->
						<tr id="tab" style="display: none;">
				  			<td  width="100%" >
					  			<c:if test="${requestScope.tabId eq 2}">
						   			<jsp:include page="GeneralDocumentManagement.jsp"/>
                                                                        <%
                                                                                selectedTab = "tab_2";
                                                                                %>
						   		</c:if>
							</td>
						  </tr>
						   <!--- Include MPOWorkOrder.jsp for tab1. -->
						   <tr id="tab" style="display: none;">
				  			<td width="100%" >
				  				<c:if test="${requestScope.tabId eq 3}">
					  				<jsp:include page="PODeliverables.jsp"/>
                                                                        <%
                                                                                selectedTab = "tab_3";
                                                                                %>
					  			</c:if>
				  			</td>
						   </tr>
						   <!--- Include PODirections.jsp for tab1. -->
						   <tr id="tab" style="display: none;">
				  			<td width="100%" >
				  				<c:if test="${requestScope.tabId eq 4}">
					  				<jsp:include page="PODirections.jsp"/>
                                                                        <%
                                                                                selectedTab = "tab_4";
                                                                                %>
					  			</c:if>
				  			</td>
						   </tr>
						   
						   <!--- Include EInvoice.jsp for tab4. -->
						   <%if(request.getAttribute("poStatus") != null && request.getAttribute("poStatus").equals("Completed")){ 
							 if(request.getAttribute("poType") != null && !request.getAttribute("poType").equals("Internal")){%>
						   <tr id="tab" style="display: none;">
				  			<td width="100%" >
				  				<c:if test="${requestScope.tabId eq 5}">
					  				<jsp:include page="EInvoice.jsp"/>
                                                                        <%
                                                                                selectedTab = "tab_5";
                                                                                %>
					  			</c:if>
				  			</td>
						  </tr>
						  <%}} %>
					</table>
				</td>
			</tr>
		</TABLE>				
</form>
<script>
                    var selectedTab = '<%=selectedTab%>';
</script>
<script>
                    $(document).ready(function() {
            if (typeof selectedTab !== "undefined") {
            $('#' + selectedTab).addClass('active');
            }
            });
</script>
</body>
</html>