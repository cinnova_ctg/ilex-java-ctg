<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
<%
	String backgroundclass = null;
	boolean csschooser = true;
%>
</head>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr valign="top">
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td>
				<h3 style="font-size: 12px;padding-left: 0px;padding-top: 5px;">Line Items and Cost</h3>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
	<table  cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td>
			<table>
			<tr height="19">
				<td class="texthNoBorder" align="center"><b>Item</b></td>
				<td class="texthNoBorder" align="center"><b>Type</b></td>
				<td class="texthNoBorder" align="center" width="45%"><b>Description</b></td>
				<td class="texthNoBorder" align="center"><b>Qty</b></td>
				<td class="texthNoBorder" align="center"><b>Unit Cost</b></td>
				<td class="texthNoBorder" align="center"><b>Total Cost</b></td>
			</tr>
			
			<c:forEach items="${POBean.invoiceItems}" var="invoiceItems" varStatus="index">
			<%	
				if ( csschooser == true ) 
				{
					backgroundclass = "Ntextoleftalignnowrap";
					csschooser = false;
				}
		
				else
				{
					csschooser = true;	
					backgroundclass = "Ntexteleftalignnowrap";
				}
			%>
			<tr>
				<td class="<%=backgroundclass %>" ><c:out value='${invoiceItems.lineItem}'/></td>
				<td class="<%=backgroundclass %>" ><c:out value='${invoiceItems.lineType}'/></td>
				<td class="<%=backgroundclass %>" ><c:out value='${invoiceItems.lineDiscription}'/></td>
				<td class="<%=backgroundclass %>" ><fmt:formatNumber value='${invoiceItems.lineQuantity}' type="currency" pattern="######0.00"/></td>
				<td class="<%=backgroundclass %>" ><fmt:formatNumber value='${invoiceItems.lineUnitCost}' type="currency" pattern="$#,###,##0.00"/></td>
				<td class="<%=backgroundclass %>" ><fmt:formatNumber value='${invoiceItems.lineTotalCost}' type="currency" pattern="$#,###,##0.00"/></td>
			</tr>
			</c:forEach>
			<tr>
				<td class="dbvaluesmallFont" align="right" colspan="5">
					<b>Tax:</b>
				</td>
				<td class="dbvaluesmallFont" align="right" nowrap="nowrap">
					<b><fmt:formatNumber value='${POBean.lineTaxAmount}' type="currency" pattern="$#,###,##0.00"/></b>
				</td>
			</tr>
			<tr>
				<td class="dbvaluesmallFont" align="right" colspan="5">
					<b>Invoice Total:</b>
				</td>
				<td class="dbvaluesmallFont" align="right" nowrap="nowrap">
					<b><fmt:formatNumber value="${POBean.lineTotal}" type="currency" pattern="$#,###,##0.00"/></b>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>	
	</td>
</tr>
</table>
