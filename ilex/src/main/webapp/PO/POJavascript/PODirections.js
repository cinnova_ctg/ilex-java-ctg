/*Initializing  Global var*/
var defaultZoomLevel = 4;
var defaultCenterLat = 38.81362;
var defaultCenterLong = -94.554209;
var map;
var directions;
var markers = [];

/**
	Initialize GMap. Its should call on page load.
	Its insitialise map, add controls, set default center point and set default zoom level.
	@param	elementId - Div Id where map should display on page.
	@index	index - Index of map[].
*/
function initialiseMap(mapId,routeInfoId){
	map = new GMap2(document.getElementById(mapId));
	map.addControl(new GLargeMapControl());
	map.addControl(new GMapTypeControl());
    directions = new GDirections(map, document.getElementById(routeInfoId));
}//- End initialiseMap()

/*This function is used to show direction into map on the basis of 
  latitude and longitude.
*/
function setDirections(jobLatitude,jobLongitude,partnerLatitude,partnerLongitude){
	if(jobLatitude == null)jobLatitude = '';
	if(jobLongitude == null)jobLongitude = '';
	if(partnerLatitude == null)partnerLatitude = '';
	if(partnerLongitude == null)partnerLongitude = '';
	
	if(jobLatitude == '' || jobLongitude == '' || partnerLatitude=='' || partnerLongitude==''){
		return false;
	}
	
	var coordinates = [];
	coordinates.push(partnerLatitude+","+partnerLongitude);	
	coordinates.push(jobLatitude + "," + jobLongitude);
    directions.loadFromWaypoints(coordinates);
    removeMarker();
}//- End setDirections()

/**
	Add controls, set default center point and set default zoom level on given map.
	@index	index - Index of map[].
*/
function setMapAttribute(latitude, longitude){
	var burnsvilleMN;
	if(longitude != '' && longitude != ''){
		burnsvilleMN = new GLatLng(latitude,longitude);
	} else{
		burnsvilleMN = new GLatLng(defaultCenterLat,defaultCenterLong);
	}
	map.setCenter(burnsvilleMN);
   	map.setZoom(defaultZoomLevel);
	
}//- End setMapAttribute()

/**
	This function is used to set GMarker.
*/
function setMarker(latitude,longitude){	
	if(latitude != '' && longitude != ''){
	    var point = new GLatLng(latitude, longitude);
		var marker = new GMarker(point);
	    map.addOverlay(marker);
	    markers.push(marker);
    }
}//- End setMarker()

/*To remove marker from Map.*/
function removeMarker(){
	$(markers).each(function(i,marker){ map.removeOverlay(marker); })
	markers = [];
}//- removeMarker()

