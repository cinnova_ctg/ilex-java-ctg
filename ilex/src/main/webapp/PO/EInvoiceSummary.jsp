<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
</head>
<table cellspacing="0" cellpadding="0" border="0" width="100%" style="padding-left: 15px">
<tr>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" class="dbbottomborder1" width="100%">
		<tr>
			<td>
				<h3 style="font-size: 12px;padding-left: 0px;padding-top: 5px;">Summary</h3>
			</td>
		</tr>
		</table>
	</td>
</tr>
<!-- Recipt: Start -->
<tr>
	<td>
	<table cellspacing="0" cellpadding="0" border="0" class="dbbottomborder1" width="100%">
		<tr>
			<td class="dbvaluesmallFont">
				<b>Receipt</b>
			</td>
		</tr>
		<tr style="padding-left: 15px">
			<td >
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td class="dbvaluesmallFont">
						Invoice#:&nbsp;<c:out value='${POBean.invoiceNumber}'/>&nbsp;(<c:out value='${POBean.invoiceType}'/>)
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Invoice Status:&nbsp;<c:out value='${POBean.invoiceStatus}'/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Invoice Date:&nbsp;<c:out value='${POBean.invoiceDate}'/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Received Date:&nbsp;<c:out value='${POBean.invoiceReceivedDate}'/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Partner Comments:
					</td>
				</tr>
				<tr>
					<td style="padding-left: 10px">
						<table class="dbbottomborder2" width="100%">
						<tr>
							<td class="dbvaluesmallFont2">
								<c:out value='${POBean.invoicePartnerComment}'/>&nbsp;
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Received By:&nbsp;<c:out value='${POBean.invoiceRecivedBy}'/>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>	
	</td>
</tr>
<!-- Recipt: End -->

<!-- Approval: Start -->
<tr>
	<td>
	<table cellspacing="0" cellpadding="0" border="0" class="dbbottomborder1" width="100%">
		<tr>
			<td class="dbvaluesmallFont">
				<b>Approval</b>
			</td>
		</tr>
		<tr style="padding-left: 15px">
			<td>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td class="dbvaluesmallFont">
						Approved Date:&nbsp;<c:out value='${POBean.invoiceApprovedDate}'/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Approved By:&nbsp;<c:out value='${POBean.invoiceApprovedBy}'/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Explanation to Partner:
					</td>
				</tr>
				<tr>
					<td style="padding-left: 10px">
						<table class="dbbottomborder2" width="100%">
						<tr>
							<td class="dbvaluesmallFont2">
								<c:out value='${POBean.invoiceExplanation}'/>&nbsp;
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>	
	</td>
</tr>
<!-- Approval: Start -->

<!-- Payment: Start -->
<tr>
	<td>
	<table cellspacing="0" cellpadding="0" border="0" class="dbbottomborder1" width="100%">
		<tr>
			<td class="dbvaluesmallFont">
				<b>Payment</b>
			</td>
		</tr>
		<tr style="padding-left: 15px">
			<td>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td class="dbvaluesmallFont">
						Paid Date:&nbsp;<c:out value='${POBean.invoicePaidDate}'/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Check Number:&nbsp;<c:out value='${POBean.invoiceCheckNumber}'/>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>	
	</td>
</tr>
<!-- Payment: End -->

<!-- Cost: Start -->
<tr>
	<td>
	<table cellspacing="0" cellpadding="0" border="0" class="dbbottomborder1" width="100%">
		<tr>
			<td class="dbvaluesmallFont">
				<b>Cost</b>
			</td>
		</tr>
		<tr style="padding-left: 15px">
			<td>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td class="dbvaluesmallFont">
						Authorized Cost:&nbsp;<fmt:formatNumber value='${POBean.invoiceAuthorisedTotal}' type="currency" pattern="$#,###,##0.00"/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Invoice Total:&nbsp;<fmt:formatNumber value='${POBean.invoiceTotal}' type="currency" pattern="$#,###,##0.00"/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						Approved Total:&nbsp;<fmt:formatNumber value='${POBean.invoiceApprovedTotal}' type="currency" pattern="$#,###,##0.00"/>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>	
	</td>
</tr>
<!-- Cost: End -->
<tr>
	<td class="dbvaluesmallFont">
		Last Updated By:&nbsp;<c:out value='${POBean.invoiceLastUpdatedBy}'/>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont">
		Last Update:&nbsp;<c:out value='${POBean.invoiceLastUpdate}'/>
	</td>
</tr>
</table>

