<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<head>
	<script>

			 $(document).ready(function(){
				 checkFormSubmit();
				var selected = {};

				$('input:checked').each(function() {
					
					selected[$(this).attr('value')] = 1;

				});

			   $(".checkbox").click(function(){
			
					if(selected[$(this).attr('value')]){
						if($(this).attr('checked')){

							$(this).parent().removeClass("remove");

							$(this).parent().addClass("selected");

						} else {

							$(this).parent().removeClass("selected");

							$(this).parent().addClass("remove");

						}

					} else {

						if($(this).attr('checked')){

							$(this).parent().addClass("highlight");

						} else {

							$(this).parent().removeClass("highlight");

						}

					}

				});

			});

			 
			 
			 function getQuerystringNameValue(name)
				{
				    // For example... passing a name parameter of "name1" will return a value of "100", etc.
				    // page.htm?name1=100&name2=101&name3=102

				    var winURL = window.location.href;
				    var queryStringArray = winURL.split("?");
				    var queryStringParamArray = queryStringArray[1].split("&");
				    var nameValue = null;

				    for ( var i=0; i<queryStringParamArray.length; i++ )
				    {           
				        queryStringNameValueArray = queryStringParamArray[i].split("=");

				        if ( name == queryStringNameValueArray[0] )
				        {
				            nameValue = queryStringNameValueArray[1];
				        }                       
				    }

				    return nameValue;
				}
				function closeWindow(){
					//POAction.do?hmode=unspecified&poId=266601&tabId=3&mode=view&appendix_Id=4027&jobId=257869
					//window.opener.location.reload(false);
					window.opener.location = 'POAction.do?hmode=unspecified&poId='+document.forms[0].poId.value+'&tabId=3&mode=view&appendix_Id='+document.forms[0].appendixId.value+'&jobId='+document.forms[0].jobId.value;
					window.close();
				}
				
				
			function checkFormSubmit(){
					if (getQuerystringNameValue("submitted") == '1'){
						closeWindow();
					}
					
				}
			 
			 
		</script>

<style>

	#freqUsedDel {

		width: 700px;

	}

	#freqUsedDel   tr td{

		font-family: Arial, Tahoma, Verdana;

		font-size: 11px;

		width: 33%;

	}

	#freqUsedDel   tr td#footer{

		text-align: right;	

	}

	#freqUsedDel   tr td#header{

		font-weight: bold;	

	}

	#freqUsedDel   tr td.highlight{

		background-color: #F4F9FD;

	}

	#freqUsedDel   tr td.remove{

		background-color: #FFEAEA;

	}

	#freqUsedDel   tr td.selected{

		background-color: #C9D9E8;

	}

	</style>

	<script>
		function saveDeliverableData(formfile) {
				var url = "POAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
				document.forms[0].action = url; 
				document.forms[0].submit();
		}
	</script>

	<script>
		function test(){
			var url = "JobDashboardAction.do?hmode=POaction&tabId=3&ticketType=&ticket_id=&poActionName=Edit&poId="+document.forms[0].poId.value;
			document.forms[0].action=url;
			document.forms[0].submit();
		}
		function addEditDev(devId,obj,tabId)
			{	
				var url = "POAction.do?hmode=savePO&tabId="+tabId+"&devId="+devId+"&clicked="+obj;
				document.forms[0].action=url;
				document.forms[0].submit();
				
			}
			
	function openSearchPage(devId){
			var url="./DocMSearch.do?dev=yes&page=mpo&view=po&appendixId="+document.forms[0].appendixId.value+"&entityType=Engineering Support"+"&poId="+document.forms[0].poId.value+"&devId="+devId;
			child = open(url,'popupwindow','width=800,height=600,scrollbars=yes,status=yes');
		}
		
		
		function insertDoc(devId,poDocId)
		{
			var url = "POAction.do?hmode=savePODoc&tabId=3&devId="+devId+"&poDocId="+poDocId;
			document.forms[0].action=url;
			document.forms[0].submit();
		}
		function viewFile(docId)
		{
			var url = 'DocumentMaster.do?hmode=openFile&fileId=&docId='+docId;
			document.forms[0].target = "_self";
			document.forms[0].action = url;
			document.forms[0].submit();
			return true;
			
		}
		function viewFileForAccept(docId,devId,devFormat,type){
	//	alert("1");
	//	 if(devFormat =='Other'){	
			
	//		var url = 'DeliverablesAction.do?hmode=viewDeliverableBeforeAcceptExceptCase&devId='+devId+'&docId='+docId+"&poId="+document.forms[0].poId.value+"&jobId="+document.forms[0].jobId.value;
	//		child = open(url,'popupwindow','scrollbars=no,left=250,top=100,width=830,height=480,status=yes');
	//	}else
		{ 	
			var url = 'DeliverablesAction.do?hmode=viewDeliverableBeforeAccept&devId='+devId+'&docId='+docId+"&poId="+document.forms[0].poId.value+"&jobId="+document.forms[0].jobId.value+"&type="+type;
			child = open(url,'popupwindow','scrollbars=no,left=250,top=100,width=830,height=480,status=yes');
		
		}
		
	} 
		function viewFileInWindow(docId,devFormat) {
		//	alert(formfile);
			if(devFormat=='Other'){	
	//		alert("1");	
			var url = 'DeliverablesAction.do?hmode=viewDeliverableBeforeAcceptExceptCaseView&devId=&docId='+docId+"&poId=";
			child = open(url,'popupwindow','scrollbars=no,left=250,top=100,width=830,height=480,status=yes');
			} 
			else
			{	
	
	
			var imageDetailUrl='DocumentMaster.do?hmode=getFileDetails&fileId=&docId='+docId;
			var newheight;
		    var newwidth; 
		    var maxheight=400;
		    var maxwidth=800;
		    var mimetype;
			$.getJSON(imageDetailUrl, function(data) {
				
			$.each(data, function (key, value) {
				
				
				if(key=="height"){
					value=parseInt(value);
					if(value>maxheight){
						value=maxheight;
					}
					newheight=value+"px" ;
				}
				if(key=="width"){
					value=parseInt(value);
					if(value>maxwidth){
						value=maxwidth;
					}
					newwidth=value+"px";
				}
				if(key=="mimetype"){
					mimetype=value;
				//	alert(mimetype);
				}
								});
	//		alert("2");
			var url = 'DocumentMaster.do?hmode=viewFile&fileId=&docId='+docId;
			if(mimetype=="pdf")
			mywidow=window.open(url,'','titlebar=no,toolbar=no,scrollbars=auto,,resizable=yes,width='+newwidth+',height='+newheight);
			else{
				mywidow=window.open(url,'','titlebar=no,toolbar=no,scrollbars=auto,,resizable=yes,width='+newwidth+',height='+newheight);
		//		window.showModalDialog(url,'','dialogWidth:'+newwidth+'; dialogHeight:'+newheight);
				
			}
				
			});
			
			
			
			
		
		}

		}	
		function newUpload(devId) {
		
			var url = "DeliverablesAction.do?hmode=uploadPODeliverableData&tabId=3&devId="+devId+"&poId="+document.forms[0].poId.value+"&jobId="+document.forms[0].jobId.value+"&appendixId="+document.forms[0].appendixId.value;
			child = open(url,'popupwindow','left=200,top=180,width=650,height=400,scrollbars=no,status=yes');
			
		}
		
		function supportDeliverableUpload(destination) {
			var supportDeliverable = "uploadSupportDeliverable";
			var devType = "";
			var poStatus = "";
			var poStatusId = "";
			var title = "";
			var devId = "0";
			var devMobile = true;
			var docId = -1;
			var DevStatus = 1;
			if(destination!=""){
				var arr = destination.split(",");
				supportDeliverable = "editDeliverable";
				devType = arr[0];
				poStatus = arr[1];
				poStatusId = arr[2];
				title = arr[3];
				devId = arr[4];
				devMobile = arr[5];
				docId = arr[6];
				DevStatus = arr[7];
				var devStatusName = document.getElementById("status_"+arr[8]).innerHTML;
				if(devStatusName=='Rejected'){
					DevStatus = 3;
				}
				
			}
			
			var url = "DeliverablesAction.do?hmode=uploadPODeliverableData&tabId=3&devId="+devId+"&poId="+document.forms[0].poId.value+"&jobId="
					+document.forms[0].jobId.value+"&appendixId="+document.forms[0].appendixId.value+"&supportDeliverable="+
					supportDeliverable+"&devType="+devType+"&poStatus="+poStatus+"&poStatusId="+poStatusId+"&devTitle="+title+"&devMobile="+devMobile+"&poDocId="+docId+"&devStatus="+DevStatus ;
			
			child = open(url,'popupwindow','left=200,top=180,width=650,height=400,scrollbars=no,status=yes');
		}
		
		
	</script>
	<script>
	function doDeliverableAction(devId,obj,tabId)
			{				
				var url = "POAction.do?hmode=savePO&tabId="+tabId+"&devId="+devId+"&clicked="+obj;
				document.forms[0].action=url;
				document.forms[0].submit();
				
			}
			
	function getAjaxRequestObject(){
	var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }	
	return ajaxRequest;        
}



	function doDeliverableAction(poId, devId, actionType, tdId) {			
		var ajaxRequest = getAjaxRequestObject();
	
		    ajaxRequest.onreadystatechange = function(){
       if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
           var xmlDoc=ajaxRequest.responseXML;        		
           		
           	var id;
			 id = xmlDoc.getElementsByTagName("devInfo")[0];			    
			 var devTitle=id.getElementsByTagName("devTitle")[0].firstChild.nodeValue;					 
			 var devType=id.getElementsByTagName("devType")[0].firstChild.nodeValue;					 

			 var devFormat=id.getElementsByTagName("devFormat")[0].firstChild.nodeValue;					 
			 var devDesc=id.getElementsByTagName("devDesc")[0].firstChild.nodeValue;
			 
			 var devStatus=id.getElementsByTagName("devStatus")[0].firstChild.nodeValue;			 
			 var devDate=id.getElementsByTagName("devDate")[0].firstChild.nodeValue;
			 var docId=id.getElementsByTagName("docId")[0].firstChild.nodeValue;
			 poId=id.getElementsByTagName("poId")[0].firstChild.nodeValue;

			 document.getElementById("status_"+tdId).innerHTML = "";	
			 document.getElementById("status_"+tdId).innerHTML = devStatus;				 
			 var devActions = "<span class='dbvalue'>";	
			 if(devDate!=''){
				 document.getElementById("date_"+tdId).innerHTML = devDate.toString();	 
			 }
			// document.location.reload(true);

			 if(devStatus == 'Pending'){
			 	devActions =devActions+'<a href = "javascript:newUpload('+devId+")"+";"+'"'+">Upload"+'</a>';
			 }
			 if(devStatus == 'Rejected'){
			 	devActions =devActions+'<a href = "javascript:newUpload('+devId+")"+";"+'"'+">Upload"+'</a>'+"&nbsp;|&nbsp;";
			 	if(document.getElementById("isview_"+tdId).value != ''){
			 		devActions =devActions+'<a href = "javascript:viewFileInWindow('+docId+")"+";"+'"'+">View"+"</a>"+"&nbsp;|&nbsp;";
			 		devActions =devActions+'<a href = "javascript:viewFile('+docId+")"+";"+'"'+">Download"+"</a>";
			 	}else{
					devActions =devActions+"View";
			 	}
			 	
			 	if(document.getElementById("showType_"+tdId).value == 'Supporting'){
			 		devActions =devActions+"&nbsp;|&nbsp;";
			 		devActions =devActions+'<a href = "javascript:deleteDev('+devId+", 'delete','3')"+";"+'"'+">Delete"+"</a>";
			 	}
			 }
			 if(devStatus == 'Accepted'){
			 	document.getElementById("date_"+tdId).innerHTML = devDate;
			 	devActions =devActions+'<a href = "javascript:newUpload('+devId+")"+";"+'"'+">Upload"+'</a>'+"&nbsp;|&nbsp;"+'<a href = "javascript:doDeliverableAction('+poId+','+ devId+','+ "'"+"Reject"+"'"+","+ tdId+")"+";"+'"'+">  Reject</a>"+"&nbsp;|&nbsp;";
			 	if(document.getElementById("isview_"+tdId).value != ''){
			 		devActions =devActions+'<a href = "javascript:viewFileInWindow('+docId+")"+";"+'"'+">View"+"</a>"+"&nbsp;|&nbsp;";
			 		devActions =devActions+'<a href = "javascript:viewFile('+docId+")"+";"+'"'+">Download"+"</a>";
			 	}else{
					devActions =devActions+"View";
			 	}
			 }
			 if(devStatus == 'Uploaded'){
			 	devActions =devActions+'<a href = "javascript:doDeliverableAction('+poId+','+ devId+','+ "'"+"Accept"+"'"+","+ tdId+")"+";"+'"'+">  Accept</a>"+"&nbsp;|&nbsp;"+'<a href = "#">Upload</a>'+"&nbsp;|&nbsp;"+'<a href = "javascript:doDeliverableAction('+poId+','+ devId+','+ "'"+"Reject"+"'"+","+ tdId+")"+";"+'"'+">  Reject</a>"+"&nbsp;|&nbsp;";
			 	if(document.getElementById("isview_"+tdId).value != ''){
			 		devActions =devActions+'<a href = "javascript:viewFileInWindow('+devId+")"+";"+'"'+">View"+"</a>"+"&nbsp;|&nbsp;";
			 		devActions =devActions+'<a href = "javascript:viewFile('+docId+")"+";"+'"'+">Download"+"</a>";
			 	}else{
					devActions =devActions+"View";
			 	}
			 }
			 document.getElementById("actions_"+tdId).innerHTML = "";	
			 document.getElementById("actions_"+tdId).innerHTML = devActions;
		}
    }
   
    
		var url = "POAction.do?hmode=poActionAjax&devId="+devId+"&actionType="+actionType+"&poId="+poId;
		
        ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send(null); 
	
	
	//alert(ajaxRequestObject);
		//JobSetUpDao.updateWorkflowChecklist(
		//	{callback:function(str){ret=str;},async:false}
		//);
	}		
	</script>
	
	<script>
		function deleteDev(devId,obj,tabId)
		{
			var returnVal = confirm("Do you really want to delete the deliverable")	;
			if (returnVal>0){
				var url = "POAction.do?hmode=DeleteDev&type="+tabId+"&devId="+devId;
				document.forms[0].action=url;
				document.forms[0].submit();
			}else{
				//return false;
			}	
		}
	</script>
</head>
   
     	<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" 
     	  cellspacing="0" cellpadding="0" width="100%">
     	  	<tr>
     	 		<td>
     	 			<jsp:include page="GeneralPOInformation.jsp"/>
     	 		</td>
     	 	</tr>
     	 	<tr height="40">
				<td>
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="bottom">
								<input type="button" class="button_c" value="Add Deliverable" onclick="javascript:supportDeliverableUpload('');" />
							</td>
							<td align="right">
							</td>
						<tr>
					</table>
				</td>
		  </tr>
     		<tr valign="top">
     			<td>
					<table cellpadding="2" cellspacing="1" width="100%">
					<tr><td>
					<table  width="100%" cellspacing="1" cellpadding="0">
			     		<tr valign="top">
			     			<td>
						   	  <table width="100%" border="0" id="devListTable" cellpadding="1" cellspacing="1" <c:if test="${(requestScope.clicked eq 'addFreqDel') or (not empty requestScope.clicked) }">style="display:none;"</c:if>>
						   	  		<tr>
						         		 <td class = "texthNoBorder" width="150">
								    		<table width="100%" border="0" cellpadding="1" cellspacing="1">
								    			<tr>
								    				<td class = "texthNoBorder" align="center"  width="100%">
								    					<b>Title</b>
								    				</td>
								    				<!--  <td class = "texthNoBorder" align="left">
								    					<table width="100%"border="0">
								    						<tr>
								    							<td  align="right">
								    								<input type="button" name="Add" value="Add" onclick="addEditDev('0','add','3');" class="NbuttonBrown" <c:if test="${requestScope.POStatus eq 'Accepted' || empty POBean.isEditablePO}">disabled="disabled"</c:if>>
								    							</td>
								    						</tr>
								    					</table>
								    				</td>-->
								    			</tr>
								    		</table>
							    		</td>
					         			<td class = "texthNoBorder"  >Destination </td>
			           					<td class = "texthNoBorder"  >Format </td>
				           				<td class = "texthNoBorder" width="350">Description</td>
				           				<td class = "texthNoBorder" >Source</td>
				           				<td class = "texthNoBorder" >Created Date</td>
				           				<td class = "texthNoBorder" >Status</td>
				           				<td class = "texthNoBorder" >Updated Date</td>
				           				<td class = "texthNoBorder" >Mobile</td>
				           				<td class = "texthNoBorder" >Actions/Workflow</td>
					         		</tr>
						         	<c:forEach var="list" items="${requestScope.devList}" varStatus="count">						         	
							         		<tr>
								         		<td id='title_<c:out value="${count.count}"/>' class = "Nhyperodd" ><c:out value="${list[2]}"/></td>
								         		<td id='type_<c:out value="${count.count}"/>' class = "Nhyperodd" align="center"><c:out value="${list[3]}"/></td>
								         		<input id='showType_<c:out value="${count.count}"/>' type="hidden" name="showType" value="<c:out value="${list[3]}"/>" />
								         		<td id='format_<c:out value="${count.count}"/>' class = "Ntextoleftalignnowrap" align="center"><c:if test="${list[4] eq '-1'}"> </c:if><c:if test="${list[4] ne '-1'}"><c:out value="${list[4]}"/></c:if></td>
								         		<td id='desc_<c:out value="${count.count}"/>' class = "Ntexto" width="350" style="white-space: normal;"><c:out value="${list[5]}"/></td>
												<td id='source_<c:out value="${count.count}"/>' class = "Ntextoleftalignnowrap" align="center"><c:if test="${list[11] eq 'Internal'}">Ilex</c:if><c:if test="${list[11] ne 'Internal'}">Mobile Upload</c:if></td>
												<td id='c_date_<c:out value="${count.count}"/>' class = "Ntexto" width="10%"><c:out value="${list[13]}"/></td>
												<td id='status_<c:out value="${count.count}"/>'	class="Ntextoleftalignnowrap" align="center"><c:out value="${list[6]}" /></td>
												<td id='date_<c:out value="${count.count}"/>' class = "Ntexto" width="10%"><c:out value="${list[8]}"/></td>
								         		<td id='date_<c:out value="${count.count}"/>' class = "Ntexto" ><c:if test="${list[10] eq 'y'}">Yes </c:if><c:if test="${list[10] ne 'y'}">No </c:if></td>
								         		<input id='isview_<c:out value="${count.count}"/>' type="hidden" name="fileId" value="<c:out value="${list[9]}"/>" />
								         		<td id='actions_<c:out value="${count.count}"/>' class = "Ntexto" align="left">
									         		<span>
										         		[&nbsp;
										         		<a href = "#" onclick="javascript:supportDeliverableUpload('<c:out value="${list[3]}"/>,<c:out value="${POBean.poStatus}"/>,<c:out value="${POBean.poStatusId}"/>,<c:out value="${list[2]}"/>,<c:out value="${list[1]}"/>,<c:out value="${list[10]}"/>,<c:out value="${list[7]}"/>,<c:out value="${list[12]}"/>,<c:out value="${count.count}"/>');" >Edit</a>
										         		&nbsp;|&nbsp;
										         		<c:if test="${not empty list[9] && not empty list[7]}">
										         			<a href = "javascript:viewFileInWindow('<c:out value="${list[7]}"/>','<c:out value="${list[4]}"/>');" >View</a>
														</c:if>
														<c:if test="${empty list[9] || empty list[7]}">View</c:if>
														&nbsp;|&nbsp;
														<c:if test="${not empty list[9] && not empty list[7]}">
														<a href = "javascript:viewFileForAccept('<c:out value="${list[7]}"/>','<c:out value="${list[1]}"/>','<c:out value="${list[4]}"/>','<c:out value="${list[3]}"/>');">Accept</a>
														</c:if>
														<c:if test="${empty list[9] || empty list[7]}">Accept</c:if>
														&nbsp;|&nbsp;
														<c:if test="${not empty list[9] && not empty list[7]}">
														<a href = "javascript:doDeliverableAction('<c:out value="${param.poId}"/>','<c:out value="${list[1]}"/>', 'Reject',	'<c:out value="${count.count}"/>');">Reject</a>
														</c:if>
														<c:if test="${empty list[9] || empty list[7]}">Reject</c:if>
														&nbsp;|&nbsp;
														<c:if test="${not empty list[9] && not empty list[7]}">
															<a href = "javaScript:viewFile('<c:out value="${list[7]}"/>')" >Download</a>
														</c:if>
														<c:if test="${empty list[9] || empty list[7]}">Download</c:if>
<%-- 														<c:if test="${(list[2]  ne 'Before Work') && (list[2]  ne 'After Work') && (list[2]  ne 'Work Order') && (list[2]  ne 'Equipment Cabinet/Rack')}"> --%>
										         		&nbsp;|&nbsp;
										         		<a href = "#" onclick="deleteDev('<c:out value="${list[1]}"/>','delete','3');">Delete</a>
<%-- 										         		</c:if> --%>
										         		&nbsp;]
													</span>
								         		</td>		    
							         		</tr>
						         	</c:forEach>
						         		
						  		</table>
							</td>
						  </tr>
						  <tr valign="top">
						  	<td>
						  		<table id="devEditTable" width="100%" border="0" <c:if test="${ (requestScope.clicked =='addFreqDel') or (empty requestScope.clicked)}">style="display:none;"</c:if> cellpadding="1" cellspacing="1">
						  			<input type="hidden" name="devId" value="<c:out value="${POBean.devId}"/>">
						  			<input type="hidden" name="devMode" value="<c:out value="${POBean.devMode}"/>">
						  			<input type="hidden" name="devStatus" value="<c:out value="${POBean.devStatus}"/>">
						  			<input type="hidden" name="devDate" value="<c:out value="${POBean.devDate}"/>">
						  			
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Title <font class="redbold">*</font>
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<input type="text" name="devTitle" size="80" class="text" value="<c:out value='${POBean.devTitle}'/>">
						  				</td>
						  			</tr>
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Type <font class="redbold">*</font>
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<input type="radio" name="devType" value="Internal" <c:if test="${POBean.devType eq 'Internal'}">checked="checked"</c:if>>&nbsp;Internal
						  					<input type="radio" name="devType" value="Customer" <c:if test="${POBean.devType eq 'Customer'}">checked="checked"</c:if>>&nbsp;Customer
						  				</td>
						  			</tr>
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Mobile <font class="redbold">*</font>
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
<!-- 						  					mobileUpload Allowed char value compared of y and n   -->
						  					<input type="radio" name="mobileUploadAllowed" value="y" <c:if test="${POBean.mobileUploadAllowed eq 121}">checked="checked"</c:if>>&nbsp;Yes
						  					<input type="radio" name="mobileUploadAllowed" value="n" <c:if test="${POBean.mobileUploadAllowed eq 110}">checked="checked"</c:if>>&nbsp;No
						  				</td>
						  			</tr>
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Format <font class="redbold">*</font>
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<select name="devFormat" class="select">
						  						<option value="-1" <c:if test="${POBean.devFormat eq '-1'}">selected="selected"</c:if>>Select Format</option>
						  						<option value="PDF" <c:if test="${POBean.devFormat eq 'PDF'}">selected="selected"</c:if>>PDF</option>
						  						<option value="Image" <c:if test="${POBean.devFormat eq 'Image'}">selected="selected"</c:if>>Image</option>
						  						<option value="Other" <c:if test="${POBean.devFormat eq 'Other'}">selected="selected"</c:if>>Other</option>
						  					</select>
						  				</td>
						  			</tr>
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Description
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<textarea  name="devDescription"  class="textarea" rows="5" cols="80" ><c:out value='${POBean.devDescription}'/></textarea>
						  				</td>
						  			</tr>
						  			 <tr>
									   	<td  class = "colLight" align="left" colspan='2'>
									   		<input type="button" name="button1" class="button_c" value="Save" onclick="savePO(3)"/>
									   		<input type="reset" name="button2" class="button_c" value="Reset" />
									   	</td>
									</tr>
						  		</table>
						 	</td>
						</tr>	
					</table>	
					</td></tr></table>
				</td>
			</tr>
			<tr>
					<td>
							<logic:present name="freqUsedDeliverables" scope ="request">
									<table id="freqUsedDel" border="0" width="99%" <c:if test="${ (requestScope.clicked eq 'addFreqDel') or (not empty requestScope.clicked)}">style="display:none;"</c:if>>
									<tr>
										<td id="header" colspan="3">Frequently Used Deliverables
										</td>
									</tr>
										<%int count=0;%>
											<c:forEach var="freqUsedDeliverable" items="${freqUsedDeliverables}">
												<%if(count%3==0) { %>
													<tr>
												<%} %>
													<td width="33%" nowrap="nowrap"
													<c:forEach var="item" items="${selectedFreqUsedDeliverables}">
  														<c:if test="${item eq freqUsedDeliverable.deliverableTitle}">class="selected"</c:if>
													</c:forEach> />
													
													<input type="checkbox" class="checkbox" name="selectedFreqUsedDeliverables" value="${freqUsedDeliverable.deliverableTitle}" 
													<c:forEach var="item" items="${selectedFreqUsedDeliverables}">
  														<c:if test="${item eq freqUsedDeliverable.deliverableTitle}">checked="checked"</c:if>
													</c:forEach> />
															${freqUsedDeliverable.deliverableTitle}
													</td>	
												<% if(count%3!=0 && count%3!=1) {%>
													</tr>
												 <%} count++;%>
											</c:forEach>
												<td colspan="3" id="footer"><input type="button" name="update" value="Update" onclick="addEditDev('0','addFreqDel','3');" class="NbuttonBrown"/> 
												</td>
											</tr>
											
									</table>
								</logic:present>
					</td>			
			</tr>	
				
			<tr height="60">
				<td>
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="bottom">
							</td>
							<td align="right">
								<jsp:include page="POFooter.jsp"/>
							</td>
						<tr>
					</table>
				</td>
		  </tr>
		  
		</table>	
