<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
	<script>
		function openSearchPage(){
			var url="./DocMSearch.do?page=mpo&view=po&appendixId="+document.forms[0].appendixId.value+"&entityType=Engineering Support"+"&poId="+document.forms[0].poId.value;
			child = open(url,'popupwindow','width=800,height=600,scrollbars=yes,status=yes');
		}
		
		function callOpener(val){
			var pars="";
			
			for(var i=0;i<val.length;i++){
				if(val[i].checked){
					pars=pars+","+val[i].value;
				}
			}
			document.forms[0].docIdList.value=pars;
			url = "DocumentManagementAction.do?hmode=poSearchResult&type=2";
			document.forms[0].action=url;
			document.forms[0].submit();
		}
	function redirrectToOpener(docIdList){	
		url = "DocumentManagementAction.do?hmode=poSearchResult&type=2&docIdList="+docIdList+"&through=PO";
		document.forms[0].action=url;
		document.forms[0].submit();
	}
	function checkDocument(obj,text)
	{
		tableObj = document.getElementById('docMTable');
		if(text == 'PO')
		{
			if(obj.checked == true)
			{
				var len = document.all.in_wi_po.length;
				if(!len)
				{
					if(obj == document.all.in_wi_po)
					{
						document.all.chkdoc_Id.checked = true;
					}
				}
				else
				{
					for(var i=0;i<len;i++)
					{
						if(obj == document.all.in_wi_po[i])
						{
							document.all.chkdoc_Id[i].checked = true;
						}
					}
				}
			}
		}
		else
		{
			if(obj.checked == true)
			{
				var len = document.all.in_wi_wo.length;
				if(!len)
				{
					if(obj == document.all.in_wi_wo)
					{
						document.all.chkdoc_Id.checked = true;
					}
				}
				else
				{
					for(var i=0;i<len;i++)
					{
						if(obj == document.all.in_wi_wo[i])
						{
							document.all.chkdoc_Id[i].checked = true;
						}
					}
				}
			}
		}
	}
	
	function ViewFile(url)
	{
    document.forms[0].target = "_self";
	document.forms[0].action = url;
	document.forms[0].submit();
	return true;
	}
	function docMgmtSave(obj){
		var isCheck= false;
		var len=document.getElementsByName('document_id').length;
		
		var polength=document.getElementsByName('in_wi_po').length;
		
		var poObj=document.getElementsByName('in_wi_po');
		var woObj=document.getElementsByName('in_wi_wo');
		var docidObj=document.getElementsByName('chkdoc_Id');
		var checkLength = document.getElementsByName('chkdoc_Id').length;
		
		var includeWithPoObj=document.getElementsByName('include_with_po');
		var includeWithPoObjLength=document.getElementsByName('include_with_po').length;
		
		var includeWithWoObj=document.getElementsByName('include_with_wo');
		var listOfDoc_IdObj=document.getElementsByName('listOfDoc_Id');
		var title=document.getElementsByName('title');
		var forIdCheck = false;
		
		for(var i=0;i<checkLength;i++)
			{
				if(poObj[i].checked!=true){
					includeWithPoObj[i].value='0';
				}else if(poObj[i].checked==true){
					isCheck= true;
					includeWithPoObj[i].value='1';
					//docidObj[i].checked=true;

				}
				if(woObj[i].checked!=true){
					includeWithWoObj[i].value='0';
				}else if(woObj[i].checked==true){
					isCheck= true;
					includeWithWoObj[i].value='1';
					//docidObj[i].checked=true;

				}
				
				if(docidObj[i].checked!=true){
					listOfDoc_IdObj[i].value='N';
					isCheck = true;
					
				}else if(docidObj[i].checked==true){
					if(poObj[i].checked==true || woObj[i].checked==true){
						isCheck= true;
						listOfDoc_IdObj[i].value='Y';
					}else if(poObj[i].checked!=true && woObj[i].checked!=true){
						isCheck= false;
					}
					forIdCheck = true;
				}
				
				if(!isCheck){
					alert('Documents should be included with atleast either PO or WO.');
					return false;
				}
			}
			if(!forIdCheck){
					alert('Please select at least one document.');
					return false;
				}
			if(isCheck){
				document.forms[0].documentManagement.disabled = true;
				//return false;
				onSave("2");
			} 
	}
	
	function editDoc(docId,tabId)
	{
		var url = "DocumentManagementAction.do?hmode=updateDocument&docId="+docId+"&type="+tabId+"&poId="+document.forms[0].poId.value;
		var length = document.all.doc_id.length;
		if(!length)
		{
			if(docId==document.all.doc_id.value){
				if(document.all.in_wi_po_detail.checked==false && document.all.in_wi_wo_detail.checked == false){
					alert('Documents should be included with atleast either PO or WO.');
					return false;
				}
				if(document.all.in_wi_po_detail.checked==true){
					document.forms[0].in_with_po.value='1';
					
				}
				if(document.all.in_wi_wo_detail.checked==true){
					document.forms[0].in_with_wo.value='1';	
					
				}
			}
		}
		for(var i=0;i<document.all.doc_id.length;i++){
			
			if(docId==document.all.doc_id[i].value){
				if(document.all.in_wi_po_detail[i].checked==false && document.all.in_wi_wo_detail[i].checked == false){
					alert('Documents should be included with atleast either PO or WO.');
					return false;
				}
				if(document.all.in_wi_po_detail[i].checked==true){
					document.forms[0].in_with_po.value='1';
					
				}
				if(document.all.in_wi_wo_detail[i].checked==true){
					document.forms[0].in_with_wo.value='1';	
					
				}
			}
		}		
		
		document.forms[0].action=url;
		document.forms[0].submit();
	}
	function deleteDoc(docId,tabId)
	{
	var flag=confirm("Do you really want to delete the document? ");
 			if(flag==false)return false;
		var url = "DocumentManagementAction.do?hmode=deleteDocument&docId="+docId+"&type="+tabId+"&poId="+document.forms[0].poId.value;
		document.forms[0].action=url;
		document.forms[0].submit();
		return true;
	}
	function onSave(obj){
		var url = "DocumentManagementAction.do?hmode=saveDocument&type="+obj;
		document.forms[0].action=url;
		document.forms[0].submit();
	}
	</script>
</head>
	<html:hidden property="docIdList" name="POBean" />
     	<table  width="100%" height="300">
     		<tr valign="top" height="10">
     			<td  width="100%"  colspan="7" cellspacing="1" cellpadding="0">
     				<table width="100%" cellspacing="0" cellpadding="0" >
     					<tr>
     						<td>
			     				<jsp:include page="GeneralPOInformation.jsp"/>
							</td>
					     </tr>
				    </table>
			     </td>
     		</tr>

			     		
			<tr valign="top" >
    			<td >
    			<HR style="border-color: #D9DFEF;border-width: 2px;border-style: solid;">
    				<table cellspacing="1" cellpadding="0" width="100%" id="docMTable">
    		<c:if test="${empty requestScope.docDetailPageList && empty requestScope.docMList}">
    			<tr id="ListTab" valign="top">
		     		<td>
				   	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				       	<tr>
				       	
						    <td class = "texthNoBorder" >
							    <table width="100%" border="0">
							  	  	<tr>
							  	  		<TD class = "texthNoBorder"  id="check" width="1%"></TD>
								    	<td class = "texthNoBorder" align="center"  width="100%">Document</td>
								    	<td class = "texthNoBorder" align="left">
								    		<input type="button"  name="Add" value="Add" onclick="openSearchPage();" class="NbuttonBrown" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
								    	</td>
							    	</tr>
							    </table>
							</td>
						</tr>
					</table>
				    </td>
		           	<td class = "texthNoBorder"  >Status </td>
		           	<td class = "texthNoBorder"  >Date </td>
		           	<td class = "texthNoBorder" >Include with PO </td>
		           	<td class = "texthNoBorder" >Include with WO  </td>
					<td class = "texthNoBorder" ></td>
        			</tr>
    		</c:if>
        	<input type="hidden" name="in_with_po" value="0"/>
			<input type="hidden" name="in_with_wo" value="0"/>
		<c:forEach items="${requestScope.docDetailPageList}" var="doc" varStatus="index">
			<c:if test="${index.first}">
				<tr id="ListTab" valign="top">
		     		<td>
				   	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				       	<tr>
				       	
						    <td class = "texthNoBorder" >
							    <table width="100%" border="0">
							  	  	<tr>
								    	<td class = "texthNoBorder" align="center"  width="100%">Document</b></td>
								    	<td class = "texthNoBorder" align="left">
								    		<input type="button"  name="Add" value="Add" onclick="openSearchPage();" class="NbuttonBrown" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
								    	</td>
							    	</tr>
							    </table>
							</td>
						</tr>
					</table>
				    </td>
		           	<td class = "texthNoBorder"  >Status </td>
		           	<td class = "texthNoBorder"  >Date </td>
		           	<td class = "texthNoBorder" >Include with PO </td>
		           	<td class = "texthNoBorder" >Include with WO  </td>
		           	<td class = "texthNoBorder" id="action">Action</td>
         		</tr>
			</c:if>
        		<%--<input type="hidden" name="document_id" value="<c:out value="${doc[3]}"/>"/>--%>
			
			<input type="hidden" name="doc_id" value="<c:out value="${doc[5]}"/>"/>
			<tr >
				<td class = "Nhypereven" id="title"> <c:out value="${doc[0]}"/></td>
				<td align="center" class = "Nhypereven" ><c:out value="${doc[1]}"/></td>
				
				<td  class = "Nhypereven" align="center"> <c:out value="${doc[2]}"/></td>
				<td align="center" class = "Ntexteleftalignnowrap" >
				<input type="checkbox"  class="chkbx"  name="in_wi_po_detail" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> <c:if test="${doc[3] eq '1'}">checked</c:if> /></td>
				
				<td  align="center" class = "Ntexteleftalignnowrap" >
				<input type="checkbox" class="chkbx" name="in_wi_wo_detail" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> <c:if test="${doc[4] eq '1'}">checked</c:if> /></td>
				
				<c:if test="${doc[6] eq '0'}">
					<td id="action" class = "Nhypereven" align="center">[&nbsp;Download&nbsp;|&nbsp;<a href = "#" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> onclick="editDoc('<c:out value="${doc[5]}"/>','2');" >Update</a>&nbsp;|&nbsp;<a href = "#" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> onclick="deleteDoc('<c:out value="${doc[5]}"/>','2');" >Remove</a>&nbsp;]</td>
				</c:if>
				<c:if test="${doc[6] ne '0'}">
					<td id="action" class = "Nhypereven" align="center">[&nbsp;<a href = "#"  onclick="ViewFile('DocumentMaster.do?hmode=openFile&fileId=<c:out value="${doc[6]}"/>&libId=10');" >Download</a>&nbsp;|&nbsp;<a href = "#" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> onclick="editDoc('<c:out value="${doc[5]}"/>','2');" >Update</a>&nbsp;|&nbsp;<a href = "#" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if> onclick="deleteDoc('<c:out value="${doc[5]}"/>','2');" >Remove</a>&nbsp;]</td>
				</c:if>
		  	</tr>
	  	</c:forEach>
	  	<c:if test="${not empty requestScope.docDetailPageList && not empty requestScope.docMList}">
		  	<tr><td colspan=6></td></tr>
		  	<tr><td colspan=6></td></tr>
		  	<tr><td colspan=6></td></tr>
		  	<tr><td colspan=6></td></tr>
		  	<tr><td colspan=6></td></tr>
	  	</c:if>
	  	
	  	<c:forEach items="${requestScope.docMList}" var="doc" varStatus="index">
		  	<c:if test="${not empty requestScope.docDetailPageList && not empty requestScope.docMList}">
		  		<tr><td colspan=6 ><h4 id="header">Please click on Save button. </h2></td> </tr>
		  	</c:if>
        		<c:if test="${index.first}">
        			<tr id="ListTab" valign="top">
		     		<td>
				   	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				       	<tr>
				       	
						    <td class = "texthNoBorder" >
							    <table width="100%" border="0">
							  	  	<tr>
							  	  		<TD class = "texthNoBorder"  id="check" width="1%"></TD>
								    	<td class = "texthNoBorder" align="center"  width="100%">Document</b></td>
								    	
							    	</tr>
							    </table>
							</td>
						</tr>
					</table>
				    </td>
		           	<td class = "texthNoBorder"  >Status </td>
		           	<td class = "texthNoBorder"  >Date </td>
		           	<td class = "texthNoBorder" >Include with PO </td>
		           	<td class = "texthNoBorder" >Include with WO  </td>
					<td class = "texthNoBorder" ></td>
        			</tr>
        		</c:if>
        		<input type="hidden" name="document_id" value="<c:out value="${doc[3]}"/>"/>
        		<input type="hidden" name="listOfDoc_Id" value="N"/>
			<input type="hidden" name="include_with_po" value="0"/>
			<input type="hidden" name="include_with_wo" value="0"/>
			<input type="hidden" name="doc_id" value="<c:out value="${doc[5]}"/>"/>

			<tr >
				<td id="check" colspan=1 class = "Nhypereven"><input type="checkbox" class="chkbx" name="chkdoc_Id" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>><span class="Nhypereven" align="right"><c:out value="${doc[0]}"/></span></td>
				
			
				<td align="center" class = "Nhypereven" ><c:out value="${doc[2]}"/></td>
				
				<td  class = "Nhypereven" align="center"> <c:out value="${doc[1]}"/></td>
				<td align="center" class = "Ntexteleftalignnowrap" >
				<input type="checkbox"  class="chkbx" name="in_wi_po" onclick = "checkDocument(this,'PO');" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>></td>
				
				<td  align="center" class = "Ntexteleftalignnowrap" >
				<input type="checkbox" class="chkbx" name="in_wi_wo" onclick = "checkDocument(this,'WO');" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>></td>
				<td class = "Nhypereven" ></td>
		  	</tr>
	  	</c:forEach>
	  	<c:if test="${not empty requestScope.docMList}">
			<tr id="buttonRow" valign="top">
			   	<td  class = "colLight" align="left" colspan='6'>
			  		<input type="button" name="documentManagement" class="button_c" value="Save" onclick="docMgmtSave('2');" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>/>
					<input type="reset" name="resetDMtab" class="button_c" value="Reset" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>/>
				</td>
		   </tr>
		 </c:if>
		
   </table>

  