<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/summary.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>

<title>Upload PO Deliverables</title>



<script>
	function changeTitle(){
		
		var docId = document.getElementById("poDocId").value;
		document.getElementById("uploadDeliverable").value = "Save";
		if(docId == "" || docId == "-1"){
			document.getElementById("uploadDeliverable").value = "Add";
		}
		
		
}

	function saveDeliverable(){
		if(is_empty(document.forms[0].devTitle,'Deliverable Title')){
			return false;
		}
		
		if(document.forms[0].devType.value == ''){
			alert(' Deliverable Destination is required.');
			return false;
		}
		if(document.forms[0].devMobile.value == -1){
			alert('Mobile must be true/false.');
			return false;
		}
		if(document.forms[0].devFormat.value == -1){
			alert('Deliverable Format is required.');
			return false;
		}
		
		if(document.forms[0].devFormat.value == 'Image'){
			 var fileName = document.getElementById("formfile").value;
			//	alert(fileName);
			    
				if(fileName == "") 
			    {
					
					var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
					<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
					var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
					</c:if>
					document.forms[0].action = url; 
					document.forms[0].submit();	
			    	return true;	
					   
			      }  
				   if ((fileName.split(".")[1].toUpperCase() == "png") || (fileName.split(".")[1].toLowerCase() == "png")){
					 //   break;
					var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
					<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
					var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
					</c:if>

				//document.forms[0].target = "_self" ;//ilexmain 
					document.forms[0].action = url; 
					document.forms[0].submit();
				    	return true;
					}
				   	    else if ((fileName.split(".")[1].toUpperCase() == "jpeg")|| (fileName.split(".")[1].toLowerCase() == "jpeg")){
					    
				    	var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();	
				    	return true;	
					}
				    else if ((fileName.split(".")[1].toUpperCase() == "x-png")|| (fileName.split(".")[1].toLowerCase() == "x-png")){
					    
				    	
				    	var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();	
				    	return true;
					}
				    else if ((fileName.split(".")[1].toUpperCase() == "bmp")|| (fileName.split(".")[1].toLowerCase() == "bmp")){
					    
				    	var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();
				    	return true;
					}
				   else if ((fileName.split(".")[1].toUpperCase() == "gif") || (fileName.split(".")[1].toLowerCase() == "gif")){
					   
					   var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit(); 
					   
					   return true;
					}
				   else if ((fileName.split(".")[1].toUpperCase() == "jpg")||(fileName.split(".")[1].toLowerCase() == "jpg")){
					    
					   var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();  
					    
					   return true;
					}
				   else if ((fileName.split(".")[1].toUpperCase() == "psd")||(fileName.split(".")[1].toLowerCase() == "psd")){
					    
					   var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();
					    
					   
					   return true;
					} 
				   else if ((fileName.split(".")[1].toUpperCase() == "pspimage")||(fileName.split(".")[1].toLowerCase() == "pspimage")){
					    
					   var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();
					    
					   return true;
					}
				   else if ((fileName.split(".")[1].toUpperCase() == "thm")||(fileName.split(".")[1].toLowerCase() == "thm")){
					    
					   
					   
					   var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();
					   
		                 return true;
					}
				   
				   else if ((fileName.split(".")[1].toUpperCase() == "tif")||(fileName.split(".")[1].toLowerCase() == "tif")){
					    
					    var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();
					   return true;
					}
				   else if ((fileName.split(".")[1].toUpperCase() == "yuv") || (fileName.split(".")[1].toLowerCase() == "yuv")){
				    
					   var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>
		                 //document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();
					   
					   return true;
					} 
			
	        alert("Select Image  file only");	   
			return false;
			
		}
			
			

		
	  if(document.forms[0].devFormat.value == "PDF"){
				
				var fileName = document.getElementById("formfile").value;
				var str = fileName.slice( -3 );
			//	alert(str);	
				//alert(fileName.split(".")[1]);
						 if (fileName == "") {
							 	
							 	var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
								<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
								var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
								</c:if>
								document.forms[0].action = url; 
								document.forms[0].submit();	
						    	return true;
						} 
				//	   if ((fileName.split(".")[1].toUpperCase() == "pdf") || (fileName.split(".")[1].toLowerCase() == "pdf")){
						 //   break;
						if(str=="pdf" || str =="PDF") {
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();
					    	return true;
						}
					   
				alert("Select PDF file only");
				
				return false;
			}
	  
	  
	  else{
		  
	//	  alert("1");
		  var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
			<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
			var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
			</c:if>
			document.forms[0].action = url; 
			document.forms[0].submit();
		   
		   return true;
		  
		  
		  
	  }
				
	if(document.forms[0].devFormat.value == "Other"){
				 var fileName = document.getElementById("formfile").value;
					//alert(fileName.split(".")[1]);
						/* if (fileName == "") {
						    alert("Browse to upload a  File ");
						    return false;
						} */
					  
						 //   break;
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
						<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
						var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
						</c:if>

					//document.forms[0].target = "_self" ;//ilexmain 
						document.forms[0].action = url; 
						document.forms[0].submit();
					    	return true;
						
				return false;
			}
			
	else{
		
		var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
		<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
		var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
		</c:if>
		document.forms[0].action = url; 
		document.forms[0].submit();
	   
	   return true;
		
	}	
		
		
		
		
		/* var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&submitted=1&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value;
		<c:if test="${not empty requestScope.supportDeliverable && requestScope.supportDeliverable eq 'uploadSupportDeliverable'}">
			var url = "DeliverablesAction.do?hmode=saveDeliverableDocument&tabId=3&devId="+document.forms[0].devId.value+"&poId="+document.forms[0].poId.value+"&supportDeliverable=<c:out value="${requestScope.supportDeliverable}" />&submitted=1";
		</c:if>

		//document.forms[0].target = "_self" ;//ilexmain 
		document.forms[0].action = url; 
		document.forms[0].submit();
		//	self.close();
		return true; 
		 */
	}	
		
		
		     
		
			//  alert("File is invalid. Please  Upload a validfile");
			//  document.getElementById("ErrorMessage").style.display = "";
			 
		//	  return false;
		
   
	

	

	 function changeStatus(){
		if(document.forms[0].devType.value != ''){
			document.forms[0].devStatusName.value = 'Uploaded';
			document.forms[0].devStatusId.value = '1';
			document.getElementById("ErrorMessage").style.display = "none";
			
		}	

	}	
		

</script>
<script language="JavaScript">
	function is_empty(chkObj, message)
	{
		if(chkObj.value == '')
			{
				alert(message + ' is required.');
				chkObj.focus();
				return true;
			}
		return false;

	}

</script>
</head>
<body onload="changeTitle();">
<html:form action="DeliverablesAction" method="post" enctype="multipart/form-data">
	<input type="hidden" name="devId" value="<c:out value="${POBean.devId}"/>">
	<input type="hidden" name="poId" value="<c:out value="${POBean.poId}"/>">
	<input type="hidden" name="jobId" value="<c:out value="${POBean.jobId}"/>">
	<input type="hidden" name="appendixId" value="<c:out value="${POBean.appendixId}"/>">
	<input type="hidden" name="poDocId" id="poDocId" value="<c:out value="${POBean.poDocId}"/>">
	

	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr><td height="19" class = "Ntoprow1">&nbsp;</td></tr>
		<tr><td background="images/content_head_04.jpg" height="21">&nbsp;</td></tr>
		<tr><td height="5" >&nbsp;</td></tr>
	</table> 
	
	<table cellspacing="1" cellpadding="1"  border="0">
		<tr>
			<td>
				<table cellspacing="0" cellpadding="0" style="padding-left: 12px;">
					<tr>
						<td>
							<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" cellspacing="0" cellpadding="0" width="600">
								<tr valign="top">
									<td>
										<table cellpadding="2" cellspacing="1" width="100%">
											<tr>
												<td>
													<table cellpadding="2" cellspacing="1" width="600">
														<tr>
											  				<td class = "Ntextoleftalignnowrapbold" width="15%">Title:<font class="redbold"><b>*</b></font></td>
											  				<td class='Ntexteleftalignnowrap' width="85%">
											  					<input type="text" name="devTitle" id="devTitle" size="50" class="text" value="<c:out value='${POBean.devTitle}'/>">
											  				</td>
											  			</tr>
											  			<tr>
											  				<td class = "Ntextoleftalignnowrapbold" width="15%">Destination:<font class="redbold"><b>*</b></font></td>
											  				<td class='Ntexteleftalignnowrap' width="85%">
<%-- 											  					<c:if test="${POBean.devType ne 'Supporting'}"> --%>
												  					<select class="select" name="devType">
																		<c:forEach items="${requestScope.deliverableType}" var="deliverableType" varStatus="innerIndex">
																			<option value="<c:out value='${deliverableType.value}'/>" <c:if test="${POBean.devType eq deliverableType.value}">selected="selected"</c:if>>
																			<c:out value='${deliverableType.label}' /></option>
																		</c:forEach>
																	</select>
<%-- 																</c:if> --%>
<%-- 																<c:if test="${POBean.devType eq 'Supporting'}">Supporting  --%>
<%-- 																	<input type="hidden" name="devType" value="<c:out value="${POBean.devType}"/>"> --%>
<%-- 																</c:if> --%>
											   				</td>
											  			</tr>
											  			<tr>
											  				<td class = "Ntextoleftalignnowrapbold" width="15%">Mobile:<font class="redbold">*</font></td>
											  				<td class='Ntexteleftalignnowrap' width="85%">
											  					<select name="devMobile" class="select">
											  						<!-- <option value="-1" <c:if test="${POBean.devMobile eq '-1'}">selected="selected"</c:if>>Select Format</option> -->
											  						<option value="true" selected="selected">true</option>											  						
											  						<option value="false" <c:if test="${POBean.devMobile eq 'false'}">selected="selected"</c:if>>false</option>
											  					</select>
											  				</td>
											  			</tr>
											  			<tr>
											  				<td class = "Ntextoleftalignnowrapbold" width="15%">Format:<font class="redbold">*</font></td>
											  				<td class='Ntexteleftalignnowrap' width="85%">
											  					<select name="devFormat" class="select">
											  						<!-- <option value="-1" <c:if test="${POBean.devFormat eq '-1'}">selected="selected"</c:if>>Select Format</option> -->
											  						
											  						<option selected="selected" value="Image" <c:if test="${POBean.devFormat eq 'Image'}">selected="selected"</c:if>>Image</option>
											  						<option value="PDF" <c:if test="${POBean.devFormat eq 'PDF'}">selected="selected"</c:if>>PDF</option>
											  						<option value="Other" <c:if test="${POBean.devFormat eq 'Other'}">selected="selected"</c:if>>Other</option>
											  					</select>
											  				</td>
											  			</tr>
											  			<tr>
											  				<td class = "Ntextoleftalignnowrapbold" width="15%">Description:</td>
											  				<td class='Ntexteleftalignnowrap' width="85%">
											  					<textarea  name="devDescription"  class="textarea" rows="5" cols="80" ><c:out value='${POBean.devDescription}'/></textarea>
											  				</td>
											  			</tr>
											  			
											  			<tr>
											  				<td class = "Ntextoleftalignnowrapbold" width="15%">Status:</td>
											  				<td class='Ntexteleftalignnowrap' width="85%">
<%-- 											  					<c:if test="${POBean.devType ne 'Supporting'}"> --%>
												  					<input type="text" name="devStatusName" id="devStatusName" size="20" class="text" style="background: transparent; border: none;" value="<c:out value='${POBean.devStatusName}'/>" readonly="readonly">
												  					<input type="hidden" name="devStatus" id="devStatusId" value="<c:out value="${POBean.devStatus}"/>">
<!-- 												  					<select class="select" name="devStatus"> -->
<%-- 																		<c:forEach items="${requestScope.deliverableStatus}" var="deliverableStatus" varStatus="innerIndex"> --%>
<%-- 																			<option value="<c:out value='${deliverableStatus.value}'/>" <c:if test="${POBean.devStatus eq deliverableStatus.value}">selected="selected"</c:if>> --%>
<%-- 																			<c:out value='${deliverableStatus.label}' /></option> --%>
<%-- 																		</c:forEach> --%>
<!-- 																	</select> -->
<%-- 																</c:if> --%>
<%-- 																<c:if test="${POBean.devType eq 'Supporting'}">Accepted --%>
<%-- 																	<input type="hidden" name="devStatus" value="<c:out value="${POBean.devStatus}"/>"> --%>
<%-- 																</c:if> --%>
											  				</td>
											  			</tr>
											  			<tr>
											  				<td class = "Ntextoleftalignnowrapbold" width="15%">Date:</td>
											  				<td class='Ntexteleftalignnowrap' width="85%">
											  					<input type="text" name="devDate" size="10" class="textbox" value="<c:out value='${POBean.devDate}'/>" readonly="true" />
											  					<img src="images/calendar.gif" width=19 height=17 border=0 align="center" 
																	onclick="return popUpCalendar(document.forms[0].devDate, document.forms[0].devDate, 'mm/dd/yyyy')"
																	onmouseover="window.status = 'Date Picker';return true;"
																	onmouseout="window.status = '';return true;" />
											  				</td>
											  			</tr>
											  			<tr>
 											  				<td class = "Ntextoleftalignnowrapbold" width="15%">Upload:<!--<font class="redbold">*</font> --></td> 
											  				<td class='Ntexteleftalignnowrap' width="85%">
											  					<html:file name="POBean" styleId="formfile" onchange="changeStatus();" property = "myfile" styleClass = "button_c" size="50" title="Upload File"/>
<!-- 											  					<br/><span   style="display:none;color:red;font-size:10px;" id="ErrorMessage" >*Please Select jpg,jpeg,gif,bmp,png Format</span> -->
											  				</td>
<!-- 											  				<td style="display:none" id="ErrorMessage"><font class="redbold"><b>*Please Select jpg,jpeg,gif,bmp,png Format</b></font></td> -->
											  			</tr>
											  			
											  			<tr>
														  	<td class = "Ntexteleftalignnowrap" align="left" colspan='2'>
															  	<input type="button" name="uploadDeliverable" id="uploadDeliverable" class="button_c" value="Save" onclick="return saveDeliverable();"/>
															</td>
														</tr>
													</table>	<!-- Actual value table -->
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>	<!-- blue border table. -->
						</td>
					</tr>
				</table>	<!-- left padding between screen and blue table border. -->
			</td>
		</tr>
	</table>	<!-- Main table. -->
</html:form>
</body>
</html>
