<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<jsp:include page="/common/IncludeGoogleKey.jsp"/>
	<script language = "JavaScript" src = "PO/POJavascript/PODirections.js?timestamp="+(new Date()*1)></script>
	<style media="screen" type="text/css">
	
		 #map{ width:600px; height:530px; }
		 div#routeInfo {
			font-family:Arial,sans-serif;
			font-size: 11px;
			font-weight: bold;
			display:block;
			background: white;
			border: black 1px solid;
			padding: 3px;
		}
		div#routeInfo td {
			font-size: 11px;
		}
		
	</style>

<script type="text/javascript">
var jobLatitude = '<c:out value="${POBean.jobLatitude}" />';
var jobLongitude = '<c:out value="${POBean.jobLongitude}" />';
var partnerLatitude = '<c:out value="${POBean.partnerLatitude}" />';
var partnerLongitude = '<c:out value="${POBean.partnerLongitude}" />';

function callMap(){
	initialiseMap("map","routeInfo");
	setMapAttribute(partnerLatitude,partnerLongitude);
	setMarker(jobLatitude,jobLongitude);
	setMarker(partnerLatitude,partnerLongitude);
	setTimeout('callDirections()',2000);
}
function callDirections(){
	setDirections(jobLatitude,jobLongitude,partnerLatitude,partnerLongitude);	
}
$(document).ready(function(){
	setTimeout('callMap()',1500);
	
});
</script>

<script>
function onLoadUpdatePOInfo() {
	var distance = "0.0";
	var duration = "0.0";

	var partnerLat = <%=request.getAttribute("partnerLatitude")== null? "0.0": request.getAttribute("partnerLatitude")%>;
	var partnerLong = <%=request.getAttribute("partnerLongitude")==null ? "0.0": request.getAttribute("partnerLongitude")%>;
	var siteLat = <%=request.getAttribute("siteLatitude")== null ? "0.0": request.getAttribute("siteLatitude") %>;
	var siteLong = <%=request.getAttribute("siteLongitude")== null ? "0.0": request.getAttribute("siteLongitude")%>;
	if( (partnerLat != 0.0 && partnerLong != 0.0) && (siteLat != 0.0 && siteLong != 0.0) ){
		var coordinates = [];
		// Change the logic to assign partner in any case either Google API calculate the Distance or not.
		coordinates.push(siteLat+","+siteLong);	
		coordinates.push(partnerLat + "," + partnerLong);
		gDir = new google.maps.Directions();
		gDir.loadFromWaypoints(coordinates);
		google.maps.Event.addListener(gDir, "load", function(){
				distance = gDir.getDistance().meters;
				duration = gDir.getDuration().seconds;
				setTimeout("updateSiteInfo("+distance+","+duration+")",1000); 
			});
		
	} 
}
function updateSiteInfo(distance, duration) {
	updatePoInfo("Partner_Search.do?ref=updatePOByAjax&distance="+distance+"&duration="+duration+"&powoId="+<%=request.getAttribute("purchaseOrderId")%>);
}
</script>
<script>
updatePoInfo = function (poURL){
	/* This function call Ajax which is inmplemented into /javascript/prototype.js */	 
		var pars = '';	
		
		$.ajax({
		    type: "post",
		    dataType: "html",
		    url: poURL+"&timestamp="+(new Date()*1),
		    async: false,
		    timeout: 10000,
		    error: function() {},
		    success: function(data) {}
		});	
	}
	function disableStatus() {
	}
</script>
</head>

<body onload="<c:if test="${not empty requestScope.tabId}"><c:if test="${requestScope.tabId eq 0}">changePOItem();onLoadUpdatePOInfo();</c:if></c:if>" onunload="GUnload();">
<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" width="100%">
<tr>
	 <td width="100%">
		<div id="map"></div>
	 </td>
	 <td class="dbvaluesmallFont" valign="top">
	 	<div id="routeInfo" style="width: 250px; height: 530px; overflow: scroll;" ></div>
	 </td>
</tr>
</table>
</body>
</html>