<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<LINK href="styles/content.css" rel="stylesheet" type="text/css">
<LINK href="styles/base/ui.all.css" rel="stylesheet" type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script language="JavaScript" src="javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/jquery.ui.core.js"></script>
<script language="JavaScript" src="javascript/jquery.ui.datepicker.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]>  
    <script src="jquery-1.9.0.js"></script>  
<![endif]-->  
<!--[if (gte IE 9) | (!IE)]><!-->  
<!--     <script src="jquery-2.0.0.js"></script> -->
     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>   
<!--<![endif]-->


</script>
<style>
.calImageStyle{
background-color: transparent;
border: none;
padding: 0px;
margin: 0px;
vertical-align: middle;
}
.PCSTD0007
{
 font-size: 11px;
 font-weight: normal;
 font-family:Verdana, Arial, Helvetica, sans-serif;
 
</style>
<script>
function changeTerms(obj){
	if(obj.value == 'P'){
		document.forms[0].terms.value = '6'; // for Net 60
	}else if(obj.value == 'S' || obj.value == 'M'){
		document.forms[0].terms.value = '8'; // Next Pay Period
	}
	typeChanged();
	checkNonInernalPOs();
	//added for PVS Justification
	if(obj.value == 'P'){
		document.forms[0].pvsJustification.value = '0';
		document.forms[0].pvsJustification.disabled=false;
		
	}
	else{
		document.forms[0].pvsJustification.value = '1';
		document.forms[0].pvsJustification.disabled=true;
		document.forms[0].geographicConstraint.value = '0';
		document.forms[0].geographicConstraint.disabled=true;
		$('#coreCompetencyDiv').hide();
	}
}
function changeGeographicConstraint(obj){
	if(obj.value == '5'){
		document.forms[0].geographicConstraint.value = '0';
		document.forms[0].geographicConstraint.disabled=false;
		
	}
	else{
		document.forms[0].geographicConstraint.value = '0';
		document.forms[0].geographicConstraint.disabled=true;
		$('#coreCompetencyDiv').hide();
	}
}
function openCoreCompetencyDiv(obj)
{
	if(obj.value == '4')
		{
		$('#coreCompetencyDiv').show();
		}
	else{
		$('#coreCompetencyDiv').hide();
	}
}
$(document).ready(function(){
	
	
	$(  "#issueDate").datepicker({
		buttonImage: 'images/calendar.gif',
			showOn : "both",
				yearRange: "-10:+5" ,
				disabled: true,
				changeMonth: true,
				changeYear: true
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
	$(  "#deliveryDate").datepicker({
		buttonImage: 'images/calendar.gif',
			showOn : "both",
				yearRange: "-10:+5" ,
				
				changeMonth: true,
				changeYear: true
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
	<c:if test="${empty POBean.isEditablePO}">$("#issueDate").datepicker('disable');</c:if>
	<c:if test="${empty POBean.isEditablePO}">$("#deliveryDate").datepicker('disable');</c:if>
	$('#ui-datepicker-div').css('display','none');
	<c:if test="${param.tabId eq '0'}">
		if(document.forms[0].geographicConstraint.value ==4)
			{
			$('#coreCompetencyDiv').show();
			}
		var selectedCompetancyArray=new Array();
		<c:forEach items="${POBean.cmboxCoreCompetency}" var="cmboxCoreCompetencyVar" varStatus="status">
		selectedCompetancyArray.push(${cmboxCoreCompetencyVar});
		</c:forEach>
		$('.competencyCheck').each(function(){
			if ($.inArray(parseInt($(this).attr('value')),selectedCompetancyArray) > -1)
			{
								$(this).attr('checked',true);	
							}
					<c:if test="${empty POBean.isEditablePO}">
					$(this).attr('disabled',true);
					</c:if>
				});
	</c:if>
});

var ilex_url = document.URL;

function openPopupWindow(){
	window.open("/content/projects/noc_calendar/", '_blank', 'scrollbars=yes');
}

function AddRemoveNocCalender(poVal){
	
	var hyperlinkText = document.getElementById('myAnchor').innerHTML;
	//alert(hyperlinkText);
	var url="";
    if(hyperlinkText === 'Add to NOC Calendar'){
		
    	
    	 $.ajax({
    		 
    		dataType: "jsonp",
    		contentType: "text/json; charset=utf-8",
    		crossDomain: true,
    		type: "GET",	 
    		url : 'POAction.do?poValue='+poVal+'&refValue=Insert',       
        	success : function(data) {
           	//alert("ret the ans " +  data);
		
       }
         
    	});
    	 	
    	 document.getElementById('myAnchor').innerHTML="Remove From Noc Calendar";	
 }
 else{
		
		
	 $.ajax({
        
		dataType: "jsonp",
	    contentType: "text/json; charset=utf-8",
	    crossDomain: true,
	    type: "GET",
		url : 'POAction.do?poValue='+poVal+'&refValue=Delete',        
        success : function(data) {
          // alert("ret the ans " +  data);
           
        }
	
    }); 	
	document.getElementById('myAnchor').innerHTML="Add to NOC Calendar"	;
	
	
} 

} 


</script>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="dbvaluesmallFont" valign="middle" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>PO#</b>:&nbsp;
			</c:if>	
			<c:if test="${param.tabId ne '0'}">
				PO#:&nbsp;
			</c:if>	
			<c:out value='${POBean.poId}' /> <input type="hidden"
			name="poNumber" value="<c:out value='${POBean.poId}'/>" />
		</td>
		<td class="dbvaluesmallFont" valign="middle" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Status</b>:&nbsp;
			</c:if>
			<c:if test="${param.tabId ne '0'}">
				Status:&nbsp;
			</c:if>	
			<c:if
				test="${empty requestScope.mode}">
				<c:out value="${POBean.poStatus}"/>
			</c:if> <c:if test="${not empty requestScope.mode}">
				<c:out value="${POBean.poStatus}"/>
			</c:if>
		</td>
		<td class="dbvaluesmallFont" valign="top" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Issue Date</b>:&nbsp;
			</c:if>	
			<c:if test="${param.tabId ne '0'}">
				Issue Date:&nbsp;
			</c:if>
			
			<c:if
				test="${empty requestScope.mode}">
				<input type="text" class="textbox" size="10" name="issueDate" id="issueDate"
					value="<c:out value='${POBean.issueDate}' />" readonly="true" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>/>

				<select name="issueHour" class="comboWhite" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.hourList}" var="hourList" varStatus="innerIndex">
						<option value="<c:out value='${hourList.value}'/>" <c:if test="${POBean.issueHour eq hourList.value}">selected="selected"</c:if>> <c:out value='${hourList.label}' /></option>
					</c:forEach>
				</select>
				<select name="issueMinute" class="comboWhite" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.minuteList}" var="minuteList" varStatus="innerIndex">
						<option value="<c:out value='${minuteList.value}'/>" <c:if test="${POBean.issueMinute eq minuteList.value}">selected="selected"</c:if>> <c:out value='${minuteList.label}' /></option>
					</c:forEach>
				</select>
				<select name="issueIsAm" class="comboWhite" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<option value="AM" <c:if test="${POBean.issueIsAm eq 'AM'}">selected="selected"</c:if>>AM</option>
					<option value="PM" <c:if test="${POBean.issueIsAm eq 'PM'}">selected="selected"</c:if>>PM</option>
				</select>
			</c:if> <c:if test="${not empty requestScope.mode}">
				<c:out value='${POBean.issueDate}' /> <c:out value='${POBean.issueHour}'/>:<c:out value='${POBean.issueMinute}'/> <c:if test="${POBean.issueIsAm eq 'PM'}">PM </c:if> <c:if test="${POBean.issueIsAm eq 'AM'}">AM </c:if>
			</c:if>
		</td>
		
	</tr>
	<tr>
		
		<td class="dbvaluesmallFont" valign="top" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Master PO Item</b>:&nbsp;
			</c:if>	
			<c:if test="${param.tabId ne '0'}">
				Master PO Item:&nbsp;
			</c:if>	
			<c:if
				test="${empty requestScope.mode}">
				<SELECT class="select" name="masterPOItem" onchange="changePOItem();" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.masterpOItem}" var="masterpOItem"
						varStatus="innerIndex">
						<option value="<c:out value='${masterpOItem.value}'/>" <c:if test="${POBean.masterPOItem eq masterpOItem.value}">selected="selected"</c:if>>
						<c:out value='${masterpOItem.label}' /></option>
					</c:forEach>

				</SELECT>
			</c:if> <c:if test="${not empty requestScope.mode}">
				<c:out value='${POBean.masterPOItemDesc}' />
				<input type="hidden" name="masterPOItem"
					value="<c:out value='${POBean.masterPOItem}'/>" />
			</c:if>
		</td>
			
		<td class="dbvaluesmallFont" colspan="2">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Geographic constraint</b>:&nbsp;
				<SELECT class="select" name="geographicConstraint" onchange="openCoreCompetencyDiv(this)"
				<c:if test="${POBean.pvsJustification ne 5 or empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.geographicConstraintList}" var="geographicConstraintList" varStatus="innerIndex">
						<option value="<c:out value='${geographicConstraintList.value}'/>" <c:if test="${POBean.geographicConstraint eq geographicConstraintList.value}">selected="selected"</c:if>><c:out
							value='${geographicConstraintList.label}' /></option>
					</c:forEach>

				</SELECT>
			</c:if>
			<c:if test="${requestScope.tabId ne 0}">
				<c:if test="${param.tabId eq '0'}">
					<b>Authorized Payment Method</b>:&nbsp;
				</c:if>	
				<c:if test="${param.tabId ne '0'}">
					Authorized Payment Method:&nbsp;
				</c:if>
				<c:out
					value='${POBean.authorizedPaymentMethod}' />
			</c:if>
		</td>
            </tr>
            <tr>
		<td class="dbvaluesmallFont" valign="top" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Geographic constraint</b>:&nbsp;
				<SELECT class="select" name="geographicConstraint"  onchange="openCoreCompetencyDiv(this)"
				<c:if test="${POBean.pvsJustification ne 5}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.geographicConstraintList}" var="geographicConstraintList" varStatus="innerIndex">
						<option value="<c:out value='${geographicConstraintList.value}'/>" <c:if test="${POBean.geographicConstraint eq geographicConstraintList.value}">selected="selected"</c:if>><c:out
							value='${geographicConstraintList.label}' /></option>
					</c:forEach>
				</SELECT>
			</c:if>
		</td>
	
		<td class="dbvaluesmallFont" valign="top" align="left" width="25%" colspan="2">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Deliver By Date</b>:&nbsp;
			</c:if>	
			<c:if test="${param.tabId ne '0'}">
				Deliver By Date:&nbsp;
			</c:if>	
			<c:if
				test="${empty requestScope.mode}">
				<input type="text" class="textbox" size="10" id="deliveryDate"
					name="deliveryDate" value="<c:out value='${POBean.deliveryDate}'/>" readonly="true" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>/>
				<select name="deliveryHour" class="comboWhite" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.hourList}" var="hourList" varStatus="innerIndex">
						<option value="<c:out value='${hourList.value}'/>" <c:if test="${POBean.deliveryHour eq hourList.value}">selected="selected"</c:if>> <c:out value='${hourList.label}' /></option>
					</c:forEach>
				</select>
				<select name="deliveryMinute" class="comboWhite" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.minuteList}" var="minuteList" varStatus="innerIndex">
						<option value="<c:out value='${minuteList.value}'/>" <c:if test="${POBean.deliveryMinute eq minuteList.value}">selected="selected"</c:if>> <c:out value='${minuteList.label}' /></option>
					</c:forEach>
				</select>
				<select name="deliveryIsAm" class="comboWhite" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<option value="AM" <c:if test="${POBean.deliveryIsAm eq 'AM'}">selected="selected"</c:if>>AM</option>
					<option value="PM" <c:if test="${POBean.deliveryIsAm eq 'PM'}">selected="selected"</c:if>>PM</option>
				</select>&nbsp;
			</c:if> <c:if test="${not empty requestScope.mode}">
				<c:out value='${POBean.deliveryDate}' /> <c:out value='${POBean.deliveryHour}'/>:<c:out value='${POBean.deliveryMinute}'/> <c:if test="${POBean.deliveryIsAm eq 'PM'}">PM </c:if> <c:if test="${POBean.deliveryIsAm eq 'AM'}">AM </c:if>
				<input type="hidden" name="deliveryDate"
					value="<c:out value='${POBean.deliveryDate}'/>" />
				<input type="hidden" name="deliveryHour"
					value="<c:out value='${POBean.deliveryHour}'/>" />
				<input type="hidden" name="deliveryMinute"
					value="<c:out value='${POBean.deliveryMinute}'/>" />
					<input type="hidden" name="deliveryIsAm"
					value="<c:out value='${POBean.deliveryIsAm}'/>" />
				<input type="hidden" name="issueDate"
					value="<c:out value='${POBean.issueDate}'/>" />
				<input type="hidden" name="issueHour"
					value="<c:out value='${POBean.issueHour}'/>" />
				<input type="hidden" name="issueMinute"
					value="<c:out value='${POBean.issueMinute}'/>" />
				<input type="hidden" name="issueIsAm"
					value="<c:out value='${POBean.issueIsAm}'/>" />
			</c:if>
			<c:if test="${param.tabId eq '0'}">

				<%	
					
					String stVal = (String) request.getAttribute("refValue");
					
				if ("Delete".equals(stVal)) {
					
				%>

				<span><a id="myAnchor" href="#"
					onclick="AddRemoveNocCalender('${POBean.poId}');">Remove From Noc Calendar</a>&nbsp;|&nbsp;<a href="#" onclick="openPopupWindow();">View
						Calendar</a></span>

				<%
					} else {
				%>

				<span><a id="myAnchor" href="#"
					onclick="AddRemoveNocCalender('${POBean.poId}');">Add to NOC Calendar</a>&nbsp;|&nbsp;<a href="#" onclick="openPopupWindow();">View
						Calendar</a></span>


				<%
					}
				%>
			</c:if>
		</td>
	</tr>
	
	<tr>
		<td class="dbvaluesmallFont" valign="bottom" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Revision Level</b>:&nbsp;
			</c:if>	
			<c:if test="${param.tabId ne '0'}">
				Revision Level:&nbsp;
			</c:if>
			<c:out
				value='${POBean.revisionLevel}' /> <input type="hidden"
				name="revisionLevel"
				value="<c:out value='${POBean.revisionLevel}'/>" />
		</td>
		<td class="dbvaluesmallFont" valign="top" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Type</b>:&nbsp;
			</c:if>	
			<c:if test="${param.tabId ne '0'}">
				Type:&nbsp;
			</c:if>	
			<c:if
				test="${empty requestScope.mode}">
				<SELECT class="select" name="poType" onchange="changeTerms(this)" <c:if test="${POBean.partnerType eq 'M'}">disabled="disabled"</c:if> <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.po}" var="po" varStatus="innerIndex">
						<option value="<c:out value='${po.value}'/>" <c:if test="${POBean.poType eq po.label}">selected="selected"</c:if>><c:out
							value='${po.label}' /></option>
					</c:forEach>

				</SELECT>
			</c:if> <c:if test="${not empty requestScope.mode}">
				<c:out value='${POBean.poType}' />
				<input type="hidden" name="poType"
					value="<c:out value='${POBean.poType}'/>" />
			</c:if>
		</td>
		<td class="dbvaluesmallFont" valign="top" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
				<b>Terms</b>:&nbsp;
			</c:if>
			<c:if test="${param.tabId ne '0'}">
				Terms:&nbsp;
			</c:if>

			<c:if
				test="${empty requestScope.mode}">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<SELECT class="select" name="terms" <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.poTerms}" var="poTerms"
						varStatus="innerIndex">
						<option value="<c:out value='${poTerms.value}'/>" <c:if test="${POBean.terms eq poTerms.value}">selected="selected"</c:if>><c:out
							value='${poTerms.label}' /></option>
					</c:forEach>

				</SELECT>
			</c:if> <c:if test="${not empty requestScope.mode}">
				<c:out value='${POBean.termsDesc}' />
				<input type="hidden" name="terms"
					value="<c:out value='${POBean.terms}'/>" />
			</c:if>
		</td>
	</tr>
	<c:if test ="${empty POBean.partner || POBean.partner eq ''}">
		<tr>
			<td class="dbvaluesmallFont" valign="bottom" align="left" width="25%">&nbsp;
			<b>Partner</b>:&nbsp;
			</td>
			<td class="dbvaluesmallFont" valign="bottom" align="left" width="25%">&nbsp;
			<b>Contact</b>:&nbsp;
			</td>
			<td class="dbvaluesmallFont" valign="top" align="left" width="25%">&nbsp;
			<c:if test="${param.tabId eq '0'}">
			<b>PVS Justification</b>:&nbsp;
				<SELECT class="select" name="pvsJustification"  onchange="changeGeographicConstraint(this)" 
				<c:if test="${POBean.poType ne 'PVS'}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.pVSJustificationList}" var="pVSJustificationList" varStatus="innerIndex">
						<option value="<c:out value='${pVSJustificationList.value}'/>" <c:if test="${POBean.pvsJustification eq pVSJustificationList.value}">selected="selected"</c:if>><c:out
							value='${pVSJustificationList.label}' /></option>
					</c:forEach>

				</SELECT>
			</c:if>
			</td>
	</tr>
	</c:if>
	<c:if test ="${not empty POBean.partner || POBean.partner ne ''}">
		<tr>
			<td class="dbvaluesmallFont" valign="middle" align="left" width="25%">&nbsp;
				<c:if test="${param.tabId eq '0'}">
					<b>Partner</b>:&nbsp;
				</c:if>	
				<c:if test="${param.tabId ne '0'}">
					Partner:&nbsp;
				</c:if>	
				<c:out value='${POBean.partner}' />
				<input type="hidden" name="partner"
					value="<c:out value='${POBean.partner}'/>" />
			</td>
			<td class="dbvaluesmallFont" valign="top" align="left" width="25%">&nbsp;
				<c:if test="${param.tabId eq '0'}">
					<b>Contact</b>:&nbsp;
				</c:if>	
				<c:if test="${param.tabId ne '0'}">
					Contact:&nbsp;
				</c:if>	
				<c:if test="${empty requestScope.mode}">
					<SELECT
						class="select" name="contactPoc"  <c:if test="${empty POBean.isEditablePO}">disabled="disabled"</c:if>>
						<c:forEach items="${POBean.partnerPoc}" var="partnerPoc"
							varStatus="innerIndex">
							<option value="<c:out value='${partnerPoc.value}'/>" <c:if test="${POBean.contactPoc eq partnerPoc.value}">selected="selected"</c:if>>
							<c:out value='${partnerPoc.label}' /></option>
						</c:forEach>
	
					</SELECT>
				</c:if>
				<c:if test="${not empty requestScope.mode}">
					<c:out value='${POBean.contact}' />
				</c:if>
			</td>
			<td class="dbvaluesmallFont" valign="bottom" align="left" width="25%">&nbsp;
			<b>PVS Justification</b>:&nbsp;
				<SELECT class="select" name="pvsJustification"  onchange="changeGeographicConstraint(this)" 
				<c:if test="${POBean.poType ne 'PVS' or param.tabId ne '0' or empty POBean.isEditablePO}">disabled="disabled"</c:if>>
					<c:forEach items="${POBean.pVSJustificationList}" var="pVSJustificationList" varStatus="innerIndex">
						<option value="<c:out value='${pVSJustificationList.value}'/>" <c:if test="${POBean.pvsJustification eq pVSJustificationList.value}">selected="selected"</c:if>><c:out
							value='${pVSJustificationList.label}' /></option>
					</c:forEach>

				</SELECT>
			</td>
		</tr>
	</c:if>
</table>
<c:if test="${param.tabId eq '0'}">
<div id="coreCompetencyDiv" style="background:#ecf5ff; margin:10px;display:none;" >
	<c:set var="counter" value="0"></c:set>
		<c:forEach items="${POBean.dcCoreCompetancy}" var="dcCoreCompetancyVar" varStatus="status">
					<c:if test="${counter%3 eq 0}">
					<div class=" PCSTD0007" style="padding-top:2px;padding-bottom:2px;">
					</c:if>
					<span style="width:200px; display: inline-block;">
							<input type="checkbox" class="competencyCheck" value="${dcCoreCompetancyVar.value}" style="margin: 0px; padding: 0px;" name="cmboxCoreCompetency"/>&nbsp;${fn:trim(dcCoreCompetancyVar.label)}</span>
								<c:if test="${(counter%3 ne 0 and counter%3 ne 1)or status.last}">
								</div>
								</c:if>
						<c:set var="counter" value="${counter + 1}" />
			</c:forEach>
</div>
</c:if>
