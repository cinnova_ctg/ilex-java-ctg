<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
</head>
<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td>
			<jsp:include page="GeneralPOInformation.jsp"/>
		</td>
	</tr>
	<tr>
		<td class="dbbottomborder1"><font style="font-size: 1px;">&nbsp;</font></td>
	</tr>			
	
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0"> 
			<tr>
				<td align="left" width="39%" valign="top">
					<jsp:include page="EInvoiceSummary.jsp"/>
				</td>
				<td style="padding-left: 10px;">
					&nbsp;
				</td>
				<td width="59%" valign="top">
					<jsp:include page="LineItemsCost.jsp"/>
				</td>
				<td style="padding-left: 10px;">
					&nbsp;
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr height="60">
		<td align="right" valign="bottom" >
			<jsp:include page="POFooter.jsp"/> 	
		</td>
	</tr> 
</table>	


