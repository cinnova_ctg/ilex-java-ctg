<%

if(request.getAttribute("appendixid")!=null) 	appendixid = request.getAttribute("appendixid").toString(); 
if(request.getAttribute("viewjobtype")!=null) 	viewjobtype = request.getAttribute("viewjobtype").toString(); 
if(request.getAttribute("ownerId")!=null) 	ownerid =  request.getAttribute("ownerId").toString(); 
if(request.getAttribute("msaId")!=null) 	msaid =  request.getAttribute("msaId").toString(); 
if(request.getAttribute("contractDocMenu")!=null) 	contractDocMenu = request.getAttribute("contractDocMenu").toString(); 
if(request.getAttribute("appendixcurrentstatus")!=null) 	appendixcurrentstatus = request.getAttribute("appendixcurrentstatus").toString(); 
/*System.out.println("in appendixDashboardMenu appendixid = "+appendixid);
System.out.println("in appendixDashboardMenu viewjobtype = "+viewjobtype);
System.out.println("in appendixDashboardMenu ownerid = "+ownerid);
System.out.println("in appendixDashboardMenu msaid = "+msaid);
System.out.println("in appendixDashboardMenu contractDocMenu = "+contractDocMenu);
System.out.println("in appendixDashboardMenu appendixcurrentstatus = "+appendixcurrentstatus);
*/
%>

<script>
//Added For pop up window for all owner of the MSA

function bulkJobCreation() {
	document.forms[0].target = "_self";
	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix";
	document.forms[0].submit();
	return true;
}
function sendaddendumemail(jobId) {
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=PRMAppendix&jobId="+jobId+"&Appendix_Id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix&addendumflag=true";
	document.forms[0].submit();
	return true;	
}

function sendemail(jobId) {
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=PRMAppendix&jobId="+jobId+"&Appendix_Id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix";
	document.forms[0].submit();
	return true;	
}
function viewContractDocument(id, docType) {
	if(docType=='html')     document.forms[0].target = "_blank";
	else                    document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+id+"&View="+docType+"&Type=Appendix&from_type=PRM&Id=<%= appendixid %>&fromPage=Appendix";
	document.forms[0].submit();
}
function addaddendum(action, para) {
	if( para == 'view' )  document.forms[0].action = "JobUpdateAction.do?addendum=Addendum&ref=Inscopeview&Appendix_Id=<%= appendixid %>&fromPage=Appendix";
	else document.forms[0].action = "ManageAddendumAction.do?flag=A&from=AddAddendumActivity&Appendix_Id=<%= appendixid %>&fromPage=Appendix";
	document.forms[0].target = "ilexmain";
	document.forms[0].submit();
	return true;	
}
function viewdocuments() {
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdashboarddocument&ref1=view&Id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix";
	document.forms[0].submit();
	return true;
}
function upload() {
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendixDashboard&ref1=view&Id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix";
	document.forms[0].submit();
	return true;
}
function viewcomments() {
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=prm_appendix&Id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix";
	document.forms[0].submit();
	return true;	 
}
function addcomment() {
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=prm_appendix&Appendix_Id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix";
	document.forms[0].submit();
	return true;	  
}
function managestatus(v) {
	document.forms[0].target = "_self";
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_appendix&id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix";
	setTimeout(frmsub(),5000);
	return true;	
}
function frmsub(){
document.forms[0].submit();
}

function sendstatusreport() {
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=prm_appendix&Type1=prm_appendix&id=<%= appendixid %>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerid%>&fromPage=Appendix";
	document.forms[0].submit();
	return true;
}
function openreportschedulewindow(report_type) {
	
	str = "ReportScheduleWindow.do?&report_type="+report_type+"&typeid=<%= appendixid%>&fromPage=Appendix";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}
</script>