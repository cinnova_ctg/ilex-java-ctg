<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
<head>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Performance Dashboard</title>
	<link rel = "stylesheet" href = "RPT/rptstyle/rptstyle.css" type = "text/css">
</head>
<%
String tdclass="";
int colspan=0;
String bgcolor="";
boolean show = false;
int i=0;
%>
<body>
<table cellpadding="0" cellspacing="0" border="0" width = "100%">
	<tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
	<tr><td background="images/content_head_04.jpg" height="21" align="left">
			<div id="RbreadCrumb"><a href="RPTPerformanceDashboard.do" class="RbreadCrumb"><bean:message bundle="RPT" key="rpt.dashboard.breadcrumb"/></a></div>
		</td>
		<td background="images/content_head_04.jpg" height="21" class="RbreadCrumbText" align="right"><bean:message bundle="RPT" key="rpt.dashboard.servicetime"/><bean:write name="RPTPerformanceDashboardForm" property="serviceTime" />&nbsp;&nbsp;Months&nbsp;&nbsp;&nbsp;</td>
	</tr>
</table>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0">
  <tr>
  	<td style="padding-left: 20px;"></td>
    <td style="padding-top: 10px"><table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000">
      <tr>
        <td class="TopHd"><bean:write name="RPTPerformanceDashboardForm" property="loginName"/><bean:message bundle="RPT" key="rpt.dashboard.heading"/></td>
      </tr>
    </table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="50%" rowspan="2" valign="top">   <br />   
	      <table width="360" border="0" cellspacing="0" cellpadding="0">
	        <tr>
	          <td><table width="360" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td><img src="RPT/rptimages/left_cor_td.jpg" alt="" width="22" height="31" /></td>
	              <td width="100%" background="RPT/rptimages/bg.jpg"><div align="center"><span class="TabHd"><strong><bean:message bundle="RPT" key="rpt.dashboard.jobperformance.title"/></strong></span></div></td>
	              <td><img src="RPT/rptimages/right_cor_td.jpg" alt="" width="22" height="31" /></td>
	            </tr>
	          </table></td>
	        </tr>
	        <tr>
	          <td><table width="360" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#BBC4CB" style=" border-collapse:collapse;">
	            <tr>
	              <td width="42%" class="TabData" bgcolor="#D7E1ED"><bean:message bundle="RPT" key="rpt.accounting.job"/></td>
	              <td width="15%" class="TabData" bgcolor="#D7E1ED"><bean:message bundle="RPT" key="rpt.accounting.count"/></td>
	              <td width="25%" class="TabData" bgcolor="#D7E1ED"><bean:message bundle="RPT" key="rpt.accounting.revenue"/></td>
	              <td width="18%" class="TabData" bgcolor="#D7E1ED"><bean:message bundle="RPT" key="rpt.accounting.vgpm"/></td>
	            </tr>
	            
	            <logic:iterate id="jobperformancesummary" name="RPTPerformanceDashboardForm" property="jobPerformanaceList">
	            <%
				show = false;
				bgcolor= "";
				tdclass= "TabText1";
				colspan = 1;
				
            	if(i==0 || i==9){
					colspan = 4;
					show = true;
            	}
            	else if((i>0 && i<9) ||(i>9 && i<12)){
					tdclass= "TabText";
	            }
            	else if(i==13){
					bgcolor= "#D7E1ED";
	        	}     	
	        	else{}%>
	            <tr>
	            	<td class="<%=tdclass %>" bgcolor="<%=bgcolor %>" colspan="<%=colspan %>"><bean:write name="jobperformancesummary" property="colName" /></td>
	         	<%if(!show){ %>
	              	<td class="TabData" bgcolor="<%=bgcolor %>"><bean:write name="jobperformancesummary" property="count" /></td>
	              	<td class="TabData" bgcolor="<%=bgcolor %>"><bean:write name="jobperformancesummary" property="revenue" /></td>
	              	<td class="TabData" bgcolor="<%=bgcolor %>"><bean:write name="jobperformancesummary" property="aveVGPM" /></td>
		         <%}i++;%>
		         </tr>
	             </logic:iterate>
	          </table></td>
	        </tr>
	      </table>
	      <br />
	      <table width="360" border="0" cellspacing="0" cellpadding="0">
	        <tr>
	          <td><table width="360" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td><img src="RPT/rptimages/left_cor_td.jpg" alt="" width="22" height="31" /></td>
	                <td width="100%" background="RPT/rptimages/bg.jpg"><div align="center"><span class="TabHd"><strong><bean:message bundle="RPT" key="rpt.dashboard.einvoice.title"/></strong></span></div></td>
	                <td><img src="RPT/rptimages/right_cor_td.jpg" alt="" width="22" height="31" /></td>
	              </tr>
	            </table></td>
	          </tr>
	        <tr>
	          <td><table width="360" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#BBC4CB" style="border-collapse:collapse;">
	            
	            <tr>
	              <td width="42%" bgcolor="#D7E1ED" class="TabData"><strong><bean:message bundle="RPT" key="rpt.dashboard.einvoice.staus"/></strong></td>
		          <td width="15%" bgcolor="#D7E1ED" class="TabData"><strong><bean:message bundle="RPT" key="rpt.accounting.count"/></strong></td>
		          <td width="25%" bgcolor="#D7E1ED" class="TabData"><strong><bean:message bundle="RPT" key="rpt.dashboard.cost"/></strong></td>
		          <td width="18%" bgcolor="#D7E1ED" class="TabData"><strong><bean:message bundle="RPT" key="rpt.dashboard.avgdelay"/></strong> </td>
	        </tr>
	        <% i=0; %>
	        <logic:iterate id="invoicestatuslist" name="RPTPerformanceDashboardForm" property="invoiceStatusList">
	        <% 
	        	bgcolor= "";
	        	if(i==4){
					bgcolor= "#D7E1ED";
	        	}
	        %>
	        <tr>
	              <td class="TabText1" bgcolor="<%=bgcolor %>"><bean:write name="invoicestatuslist" property="colName" /></td>
	              <td class="TabData" bgcolor="<%=bgcolor %>"><bean:write name="invoicestatuslist" property="count" /></td>
	              <td class="TabData" bgcolor="<%=bgcolor %>"><bean:write name="invoicestatuslist" property="revenue" /></td>
	              <td class="TabData" bgcolor="<%=bgcolor %>"><bean:write name="invoicestatuslist" property="aveVGPM" /></td>
	        </tr>
	        <%i++; %>
	        </logic:iterate>
	        </table></td>
	      </tr>
	      </table><br /> 
	      <table width="360" border="0" align="center" cellpadding="0" cellspacing="0">
	        <tr>
	          <td width="50%" class="TabData"><strong><bean:message bundle="RPT" key="rpt.dashboard.adpo"/></strong></td>
	            <td class="TabData"><strong><bean:message bundle="RPT" key="rpt.dashboard.compadpo"/></strong></td>
	          </tr>
	      </table></td>
	    <td width="50%" align="center"><br />
	      <table width="333px" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	        <tr>
	              <td><img src="RPT/rptimages/left_cor_td.jpg" alt="" width="22" height="31" /></td>
	              <td width="100%" background="RPT/rptimages/bg.jpg"><div align="center"><span class="TabHd"><strong><bean:message bundle="RPT" key="rpt.dashboard.minutemanutilization"/></strong></span></div></td>
	              <td><img src="RPT/rptimages/right_cor_td.jpg" alt="" width="22" height="31" /></td>
	            </tr>
	      </table>
	      <table width="331" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#BBC4CB" style=" border-collapse:collapse;">
	        <tr>
	      	 <td colspan="4" align="left"><strong class="TabHd1"><bean:message bundle="RPT" key="rpt.dashboard.minutemanusage"/></strong><br />
			  <img src="RPT/rptimages/graph.jpg" alt="" width="331" height="181" vspace="5" />
			 </td>
	        </tr>
	        <tr>
	          <td class="TabData1"><bean:message bundle="RPT" key="rpt.dashboard.averages"/></td>
	          <td class="TabData"><strong><bean:message bundle="RPT" key="rpt.accounting.count"/></strong></td>
	          <td class="TabData"><strong><bean:message bundle="RPT" key="rpt.dashboard.cost"/></strong></td>
	        </tr>
	        <tr>
	          <td class="TabData"><div align="left" style="padding-left: 4px;"><bean:message bundle="RPT" key="rpt.dashboard.companywide"/></div></td>
	          <td class="TabData">xxx</td>
	          <td class="TabData">$xx,xxx</td>
	        </tr>
	        <tr>
	          <td class="TabData"><div align="left" style="padding-left: 4px;"><bean:message bundle="RPT" key="rpt.dashboard.myusage"/></div></td>
	          <td class="TabData">xxx</td>
	          <td class="TabData">$xx,xxx</td>
	          </tr>
	      </table>
	      </td>
	  </tr>
	  <tr>
	    <td width="50%" valign="top" align="center"><br />
	        <table width="333" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	          <tr>
	              <td><img src="RPT/rptimages/left_cor_td.jpg" alt="" width="22" height="31" /></td>
	              <td width="100%" background="RPT/rptimages/bg.jpg"><div align="center"><span class="TabHd"><strong><bean:message bundle="RPT" key="rpt.dashboard.speedpayutilization"/></strong></span></div></td>
	              <td><img src="RPT/rptimages/right_cor_td.jpg" alt="" width="22" height="31" /></td>
	            </tr>
	        </table><table width="82%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#BBC4CB" style=" border-collapse:collapse;">
	       
	        <tr>
	          <td colspan="4" align="left"><strong class="TabHd1"><bean:message bundle="RPT" key="rpt.dashboard.minutemanusage"/></strong><br />
	            <img src="RPT/rptimages/graph.jpg" alt="" width="331" height="181" vspace="5" /></td>
	        </tr>
	        <tr>
	          <td class="TabData1"><bean:message bundle="RPT" key="rpt.dashboard.averages"/></td>
	          <td class="TabData"><strong><bean:message bundle="RPT" key="rpt.accounting.count"/></strong></td>
	          <td class="TabData"><strong><bean:message bundle="RPT" key="rpt.dashboard.cost"/></strong></td>
	        </tr>
	        <tr>
	          <td class="TabData"><div align="left" style="padding-left: 4px;"><bean:message bundle="RPT" key="rpt.dashboard.companywide"/></div></td>
	          <td class="TabData">xxx</td>
	          <td class="TabData">$xx,xxx</td>
	        </tr>
	        <tr>
	          <td class="TabData"><div align="left" style="padding-left: 4px;"><bean:message bundle="RPT" key="rpt.dashboard.myusage"/></div></td>
	          <td class="TabData">xxx</td>
	          <td class="TabData">$xx,xxx</td>
	          </tr>
	      </table>
	      <br /></td>
	  </tr>
	  <tr>
	    <td valign="top">&nbsp;</td>
	    <td valign="top" align="center">&nbsp;</td>
	  </tr>
	</table>
</td>
</tr>
</table>
</body>

</html>