<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: 
-->

<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<head>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel = "stylesheet" href = "RPT/rptstyle/rptstyle.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<title>Snap On</title>
</head>
<html:html>
<body>
<!-- Form: Start -->
<html:form action="/SnapOn.do?" enctype = "multipart/form-data">

<table cellpadding="0" cellspacing="0" border="0" width = "100%">
	<!-- BreadCrumb: Start -->
    <tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
    <tr>
    	<td background="images/content_head_04.jpg" height="21" colspan="2" class="RbreadCrumb">&nbsp;<bean:message bundle="RPT" key="rpt.support.breadcrumb"/></td>
   	</tr>
   	<!-- BreadCrumb: End -->
    <tr><td colspan="2"><h2>Snap-on XML Upload</h2></td></tr>
    <tr>
    	<td>&nbsp;</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" style="vertical-align: top">
	<logic:present name="Exception">
		<logic:equal name="Exception" value="1000">
		<tr>
			<td>&nbsp;&nbsp;</td>
			<td class = "message" height = "30">File not found</td>
		</tr>
		</logic:equal>
		<logic:equal name="Exception" value="1001">
		<tr>
			<td>&nbsp;&nbsp;</td>
			<td class = "message" height = "30"><bean:write name="errMessage"/></td>
		</tr>
		</logic:equal>
	</logic:present>
</table>
<table cellpadding="0" cellspacing="0" border="0" width = "100%">
<tr>
	<td>&nbsp;</td>
	<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>		
				<td class="Rlabeleboldwhite">Upload&nbsp;</td>
				<td>
					<html:file name="SnapOnForm" property = "filename" styleClass = "Uploadbutton" size="50" />
				</td>
				<td align="right">&nbsp;
					<html:submit property = "upload" styleClass = "Rbutton" onclick="return checkExt();">Upload</html:submit>
				</td>
			</tr>
		</table>
		<logic:notEmpty name="SnapOnForm" property="info">
			<logic:iterate id="info" name="SnapOnForm" property="info" >
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class = "message">Upload Summary:</td>
					</tr>
					<tr><td height="5"></td></tr>
					<tr>
						<td class="RtextwhiteFont" nowrap="nowrap">
							Record Created:&nbsp;<bean:write name="info" property="created"/>
						</td>	
					</tr>
					<tr>
						<td class="RtextwhiteFont" nowrap="nowrap">
							Record Updated:&nbsp;<bean:write name="info" property="updated"/>
						</td>	
					</tr>
					<tr>
						<td class="RtextwhiteFont">
							Site Not Found #:&nbsp;<bean:write name="info" property="notFound"/>
						</td>	
					</tr>
					<tr>
						<td class="RtextwhiteFont" style="padding-left: 15px;">
							<bean:write name="info" property="notFoundsiteNo"/>
						</td>	
					</tr>
					<logic:notEqual name="info" property="foundsiteNo" value="">
					</br>
					<tr>
						<td class = "message">Sites having incorrect data:</td>
					</tr>
					<tr></tr>
					<tr>
						<td class="RtextwhiteFont" style="padding-left: 15px;">
							<bean:write name="info" property="foundsiteNo"/>
						</td>	
					</tr>
					</logic:notEqual>
				</table>
			</logic:iterate>
		</logic:notEmpty>
	</td>
</tr>
</table>
</html:form>
</body>
</html:html>
<script>
function checkExt(){
	var filename = document.forms[0].filename.value;
	if( filename == '' ){
		alert('Select file to upload.');
		return false;
	}
	if( filename.substring(filename.length-4, filename.length) != '.xml' ) {
		alert('Please select correct XML format for upload.');
		return false;
	}
	return true;
}
</script>