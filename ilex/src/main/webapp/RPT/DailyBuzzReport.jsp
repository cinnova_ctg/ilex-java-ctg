<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This SP show Daily Buzz Report in RPT module.
-->

<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<head>
<!-- Import Files -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></meta>

<title>Daily Buzz Report</title>
<!-- Include Files -->
	<link href="RPT/rptstyle/rptstyle.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "RPT/rptjscript/tabcontent.js"></script>
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>

</head>
<%
	String temptitle = "";
	String backgroundclass = "";
	boolean csschooser = true;
%>
<html:html>
<body onload="">

<!-- Form: Start -->
<html:form action="/DailyBuzzReport.do">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td colspan="2">
			<!-- BreadCrumb: Start -->
			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
				<tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
				<tr><td background="images/content_head_04.jpg" height="21" colspan="2" class="RbreadCrumb">&nbsp;<bean:message bundle="RPT" key="rpt.support.breadcrumb"/></td></tr>
				<tr><td colspan="2"><h2>Daily Buzz Report</h2></td></tr>
			</table>
			<!-- BreadCrumb: End -->
		</td>
	</tr>
	<tr>
	  	<td>&nbsp;</td>
	  	<td>
		  	<table border="0" cellspacing="0" cellpadding="0">
			    <tr>
			      <td align="left">
				      <table border="0" cellspacing="0" cellpadding="0" width="100%">
				        <tr>
					        <td colspan="2" align="right" class="textbold">Job Owner:&nbsp;
				        	<!-- Job Owner Combobox: Start -->
					          	<html:select name="DailyBuzzReportForm" property = "jobOwner" styleClass = "select">
									<html:optionsCollection name = "DailyBuzzReportForm" property = "jobOwnerList" value = "value" label = "label"/> 
								</html:select>	
							<!-- Job Owner Combobox: End -->							
					        </td>
				        </tr>
				        <tr>
					    	<td colspan="2" align="right" class="textbold" style="padding-top: 4px;padding-bottom: 4px">Project:&nbsp;
					        	<!-- Project Combobox: Start -->
					    		<html:select name="DailyBuzzReportForm" property = "appendixName" styleClass = "select">
					    			<logic:iterate id="msalist" name="DailyBuzzReportForm" property="msaList">
					    				<logic:notEqual name="msalist" property="msaName" value="notShow">
					    					<optgroup class=labelebold label="<bean:write name = "msalist" property = "msaName" />">
					    				</logic:notEqual>
										<html:optionsCollection name = "msalist" property = "appendixList" value = "value" label = "label"/> 
										<logic:notEqual name="msalist" property="msaName" value="notShow">
					    					</optgroup>
					    				</logic:notEqual>
									</logic:iterate>
								</html:select>
					        	<!-- Project Combobox: End -->	
					    	</td>
				        </tr>
				           <tr>
					        <td colspan="2" align="right" class="textbold" style="padding-bottom: 4px">Sr Project Manager :&nbsp;
				        	<!--  Sr Project Manager Combobox: Start -->
					          	<html:select name="DailyBuzzReportForm" property = "srPrjManager" styleClass = "select">
									<html:optionsCollection name = "DailyBuzzReportForm" property = "srPrjManagerList" value = "value" label = "label"/> 
								</html:select>	
							<!-- Sr Project Manager Combobox: End -->							
					        </td>
				        </tr>
				        <tr>
				       		<td align="right" class="textbold">Date View:&nbsp;
					       		<html:text  name  ="DailyBuzzReportForm" styleClass = "textbox" size = "10" property = "date" readonly = "true"/>
							 	<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].date , document.forms[0].date , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							 	<html:submit property="go" styleClass="Rbutton" onclick="return validateAll();">Go</html:submit>
						 	</td>
				       </tr>
				      </table>
			        </td>
			    </tr>
			    <tr>
				  <td>
				  	<table border="0" cellpadding="0" cellspacing="0">
				      <tr>
				        <td bgcolor="#FFFFFF">
				        	<table border="0" cellspacing="0" cellpadding="0">
					            <tr>
					              <td>
					              	<!-- Scheduled: Start -->
					              	<table border='0' cellspacing='1' cellpadding='0'>
					              		<tr>
					              			<td height='28' colspan="6" bgcolor="E7EEF8" class="headingh">Scheduled</td>
					              		</tr>
					              		<tr></tr>
										<tr>
											<td rowspan='2' align='center' class='texth'>Job Title </td>
										    <td class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
											<td rowspan='2' class='texth'>Actions</td>
										</tr>
										<tr>
											<td class='texth'>Planned Start Date/Time</td>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
										</tr>
										
										<logic:iterate id="schjoblist" name="DailyBuzzReportForm" property="schList">
											<%
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="schjoblist" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="schjoblist" property="appendix_title" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='6' class='Rheading'>Project:&nbsp;<bean:write name="schjoblist" property="msa_title"/>,&nbsp;<bean:write name="schjoblist" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
											<tr>
												<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="job_name"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="planned_start_date"/>&nbsp;<bean:write name="schjoblist" property="planned_start_time"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="site_number"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="site_state"/></td>
												<td class="<%=backgroundclass%>">
													<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="schjoblist" property="job_id"/>&appendixid=<bean:write name="schjoblist" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
													<a href="POWOAction.do?jobid=<bean:write name="schjoblist" property="job_id"/>">PO Dashboard</a></td>
											</tr>
				
										</logic:iterate>
										<logic:empty name="DailyBuzzReportForm" property="schList">
										<tr>
											<td class='message' height='10' colspan='6'>No records match report criteria.</td>
										</tr>
										</logic:empty>
					          		<!-- Scheduled: End -->
					          			<tr>
					              			<td height='10' colspan="6">&nbsp;</td>
					              		</tr>
					          		<!-- Confirmed: Start -->
					          			<tr>
					              			<td height='28' colspan="6" bgcolor="E7EEF8" class="headingh">Confirmed</td>
					              		</tr>
					              		<tr></tr>
										<tr>
											<td rowspan='2' align='center' class='texth'>Job Title </td>
										    <td class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
											<td rowspan='2' class='texth'>Actions</td>
										</tr>
										<tr>
											<td class='texth'>Planned Start Date/Time</td>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
										</tr>
										<%temptitle = ""; %>
										<logic:iterate id="cnfJobList" name="DailyBuzzReportForm" property="cnfList">
											<%
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="cnfJobList" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="cnfJobList" property="appendix_title" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='6' class='Rheading'>Project:&nbsp;<bean:write name="cnfJobList" property="msa_title"/>,&nbsp;<bean:write name="cnfJobList" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
											<tr>
												<td class="<%=backgroundclass%>"><bean:write name="cnfJobList" property="job_name"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="cnfJobList" property="planned_start_date"/>&nbsp;<bean:write name="cnfJobList" property="planned_start_time"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="cnfJobList" property="site_number"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="cnfJobList" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="cnfJobList" property="site_state"/></td>
												<td class="<%=backgroundclass%>">
													<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="cnfJobList" property="job_id"/>&appendixid=<bean:write name="cnfJobList" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
													<a href="POWOAction.do?jobid=<bean:write name="cnfJobList" property="job_id"/>">PO Dashboard</a></td>
											</tr>
				
										</logic:iterate>
										<logic:empty name="DailyBuzzReportForm" property="cnfList">
										<tr>
											<td height='28' class='message' height='10' colspan='6'>No records match report criteria.</td>
										</tr>
										</logic:empty>
					          		<!-- Confirmed: End -->
					          			<tr>
					              			<td colspan="6" height="10">&nbsp;</td>
					              		</tr>
					          		<!-- OnSite: Start -->
					          			<tr>
					              			<td height='28' colspan="6" bgcolor="E7EEF8" class="headingh">Onsite</td>
					              		</tr>
					              		<tr></tr>
										<tr>
											<td rowspan='2' align='center' class='texth'>Job Title</td>
											<td class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
											<td rowspan='2' class='texth'>Actions</td>
										</tr>
										<tr>
											<td class='texth'>Onsite Date/Time</td>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
										</tr>
										<%temptitle = ""; %>
										<logic:iterate id="onsiteJobList" name="DailyBuzzReportForm" property="onsiteList">
											<%
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="onsiteJobList" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="onsiteJobList" property="appendix_title" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='6' class='Rheading'>Project:&nbsp;<bean:write name="onsiteJobList" property="msa_title"/>,&nbsp;<bean:write name="onsiteJobList" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
											<tr>
												<td class="<%=backgroundclass%>"><bean:write name="onsiteJobList" property="job_name"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="onsiteJobList" property="actual_start_date"/>&nbsp;<bean:write name="onsiteJobList" property="actual_start_time"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="onsiteJobList" property="site_number"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="onsiteJobList" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="onsiteJobList" property="site_state"/></td>
												<td class="<%=backgroundclass%>">
													<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="onsiteJobList" property="job_id"/>&appendixid=<bean:write name="onsiteJobList" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
													<a href="POWOAction.do?jobid=<bean:write name="onsiteJobList" property="job_id"/>">PO Dashboard</a>
												</td>
											</tr>
										</logic:iterate>
										<logic:empty name="DailyBuzzReportForm" property="onsiteList">
										<tr>
											<td class='message' height='10' colspan='6'>No records match report criteria.</td>
										</tr>
										</logic:empty>
					          		<!-- OnSite: End -->
					          			<tr>
					              			<td height='10' colspan="6">&nbsp;</td>
					              		</tr>
					          		<!-- OffSite Tab: Start -->
					          			<tr>
					              			<td height='28' colspan="6" bgcolor="E7EEF8" class="headingh">Offsite</td>
					              		</tr>
					              		<tr></tr>
								        <tr>
											<td rowspan='2' align='center' class='texth'>Job Title</td>
											<td class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
											<td rowspan='2' class='texth'>Actions</td>
									   	</tr>
									   	<tr>
											<td class='texth'>Offsite Date/Time</td>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
									  	</tr>
									  	<%temptitle = ""; %>
									  	<logic:iterate id="offsiteJobList" name="DailyBuzzReportForm" property="offsiteList">
											<%
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="offsiteJobList" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="offsiteJobList" property="appendix_title" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='6' class='Rheading'>Project:&nbsp;<bean:write name="offsiteJobList" property="msa_title"/>,&nbsp;<bean:write name="offsiteJobList" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
									  		<tr>
										        <td class="<%=backgroundclass%>"><bean:write name="offsiteJobList" property="job_name"/></td>
										        <td class="<%=backgroundclass%>"><bean:write name="offsiteJobList" property="actual_end_date"/>&nbsp;<bean:write name="offsiteJobList" property="actual_end_time"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="offsiteJobList" property="site_number"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="offsiteJobList" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="offsiteJobList" property="site_state"/></td>
												<td class="<%=backgroundclass%>">
													<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="offsiteJobList" property="job_id"/>&appendixid=<bean:write name="offsiteJobList" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
													<a href="POWOAction.do?jobid=<bean:write name="offsiteJobList" property="job_id"/>">PO Dashboard</a>
												</td>
								        	</tr>
								        </logic:iterate>
								        <logic:empty name="DailyBuzzReportForm" property="offsiteList">
										<tr>
											<td class='message' height='10' colspan='6'>No records match report criteria.</td>
										</tr>
										</logic:empty>
									 </table>
					          		<!-- OffSite: End -->
				        		</td>
				      		</tr>
				  		</table>
				 	</td>
			    </tr>
		  </table>
	  </td>
  </tr>
</table>
</html:form>
<!-- Form: End -->
</body>
</html:html>
<script>
function validateAll(){
	if(document.forms[0].date.value == ''){
		alert('Select Date for Search');
		return false;
	}
}
</script>