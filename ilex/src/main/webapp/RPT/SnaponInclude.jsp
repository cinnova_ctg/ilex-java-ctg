<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%if(request.getParameter("status").equals("sch")){%>

<tr>
	<td height='28' colspan="6" bgcolor="E7EEF8" class="headingh">Scheduled</td>
</tr>
<tr></tr>
<tr>
	<td rowspan='2' align='center' class='texth'>Job Title </td>
	<td class='texth'>Schedule</td>
	<td colspan='3' class='texth'>Site Information</td>
	<td rowspan='2' class='texth'>Actions</td>
</tr>
<tr>
	<td class='texth'>Planned Start Date/Time</td>
	<td class='texth'>Site Number</td>
	<td class='texth'>City</td>
	<td class='texth'>State</td>
</tr>
		
<%}%>

<%if(request.getParameter("status").equals("cnf")){%>
<tr>
	<%if(request.getParameter("cnfStatus").equals("cnfStatus")) {%>
	<td height='28' colspan="9" bgcolor="E7EEF8" class="headingh">Confirmed - 5 Day Call Pending</td>
	<%} %>
	
	<%if(request.getParameter("cnfStatus").equals("hour24CallList")) {%>
	<td height='28' colspan="9" bgcolor="E7EEF8" class="headingh">Confirmed - 24 Hour Call Pending</td>
	<%} %>
	
	<%if(request.getParameter("cnfStatus").equals("readyToGo")) {%>
	<td height='28' colspan="9" bgcolor="E7EEF8" class="headingh">Confirmed - Pre-calls Completed</td>
	<%} %>
	
	<%if(request.getParameter("cnfStatus").equals("escalate")) {%>
	<td height='28' colspan="9" bgcolor="E7EEF8" class="headingh">Confirmed - Escalate</td>
	<%} %>
	
	<%if(request.getParameter("cnfStatus").equals("rescheduled")) {%>
	<td height='28' colspan="9" bgcolor="E7EEF8" class="headingh">Confirmed - Reschedule</td>
	<%} %>
</tr>
<tr></tr>
<tr>
	<td rowspan='2' align='center' class='texth'>Job</td>
	<td rowspan='2' class='texth'>Scheduled</td>
	<td colspan='3' class='texth'>Site Information</td>
	<td colspan='3' class='texth'>Pre Calls</td>
	<td rowspan='2' class='texth'>Actions</td>
</tr>
<tr>
	<td class='texth'>Site Number</td>
	<td class='texth'>City</td>
	<td class='texth'>State</td>
	<td class='texth'>5 Day (1)</td>
	<td class='texth'>4 Day (2)</td>
	<td class='texth'>24 Hour</td>
</tr>
<%}%>
