<script>
function gettingAjaxData(id){
		enableDates(); // - Enable Date range textBox
        var ajaxRequest;  // The variable that makes Ajax possible!
        document.forms[0].tabId.value = id;
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		
               		 var response = ajaxRequest.responseText;
               		 document.getElementById(id).innerHTML = response;
               }
        }
        
     	var ownerid = document.forms[0].jobOwner.value;
     	var appendixid= document.forms[0].appendixName.value;
     	var to= document.forms[0].toDate.value;
     	var from= document.forms[0].fromDate.value;
     	//alert(to+from);
        ajaxRequest.open("POST", "JobOwnerSummaryList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("tabname="+id+"&appendixid="+appendixid+"&ownerid="+ownerid+"&toDate="+to+"&fromDate="+from); 
}
 
</script>
