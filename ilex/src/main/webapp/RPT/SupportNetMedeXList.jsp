<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Display Support Ticket and its site info list according to Date Range.
-->


<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<head>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel = "stylesheet" href = "RPT/rptstyle/rptstyle.css" type = "text/css">
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<title>Support Appendix</title>
</head>
<%
	String backgroundclass = null; //- td style class
	boolean csschooser = true;		// - for alternet change color of rows
	int supportAppedixList = 0; // length of list
%>
<html:html>
<body>
<!-- Form: Start -->
<html:form action="/SupprortNetMedXList.do">
<table cellpadding="0" cellspacing="0" border="0" width="1700">
<tr>
<td>
	<table cellpadding="0" cellspacing="0" border="0" width = "100%">
		<!-- BreadCrumb: Start -->
	    <tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
	    <tr>
	    	<td background="images/content_head_04.jpg" height="21" colspan="2" class="RbreadCrumb">&nbsp;<bean:message bundle="RPT" key="rpt.support.breadcrumb"/></td>
	   	</tr>
	   	<!-- BreadCrumb: End -->
	    <tr><td colspan="2"><h2><bean:message bundle="RPT" key="rpt.support.netmedex.title"/></h2></td></tr>
	    <tr>
			<td colspan="2" style="padding-left: 8px">
				<table cellpadding="1" cellspacing="0" border="0" align="left">
					
					<tr>
						<!-- TextBox for Data range: Start -->
						<td class="Rlabeleboldwhite"><bean:message bundle="RPT" key="rpt.accounting.fromdate"/>
		 					<html:text  name  ="SupportNetMedXForm" styleClass = "Rtext" size = "10" property = "fromDate" readonly = "true"/>
		 					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].fromDate , document.forms[0].fromDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
		 					
						</td>
						<td class="Rlabeleboldwhite">&nbsp;&nbsp;</td>
						<td class="Rlabeleboldwhite" align="left"><bean:message bundle="RPT" key="rpt.accounting.todate"/>
							<html:text  name  ="SupportNetMedXForm" styleClass = "Rtext" size = "10" property = "toDate" readonly = "true"/>
							<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].toDate , document.forms[0].toDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
		 				</td>
						<!-- TextBox for Data range: Start -->
						<!-- Go Button: Start -->
						<td class="Rlabeleboldwhite">&nbsp;&nbsp;</td>
						<td class="Rlabeleboldwhite">
		 					<html:button property="go" styleClass="Rbutton_c" onclick="return blankcheck();"><bean:message bundle="RPT" key="rpt.accounting.go"/></html:button>
						</td>
						<!-- Go Button: End -->
					</tr>
					
				</table>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>&nbsp;&nbsp;</td>
		<td>
			<table cellpadding="0" cellspacing="1" border="0">
				<!-- Column header: Start -->
				<tr height="19"> 
				    <td class = "texth"><bean:message bundle="RPT" key="rpt.supprot.site.number"/></td>
				    <td class = "texth"><bean:message bundle="RPT" key="rpt.support.site.city"/></td>
				    <td class = "texth"><bean:message bundle="RPT" key="rpt.support.site.state"/></td>
				    <td class = "texth"><bean:message bundle="RPT" key="rpt.supprot.onsite.date.time"/></td>
				    <td class = "texth"><bean:message bundle="RPT" key="rpt.supprot.offsite.time"/></td>
				    <td class = "texth"><bean:message bundle="RPT" key="rpt.supprot.ticket.problem"/></td>
				    <td class = "texth"><bean:message bundle="RPT" key="rpt.supprot.closing.summary"/></td>
		   		</tr>
		   		<!-- Column header: End -->
		   		<!-- List of Support Appendix: Start -->
		   		<logic:iterate id="listsupport" name="SupportNetMedXForm" property="supportNetMedXlist">
		   		<%	
				if ( csschooser == true ) 
				{
					backgroundclass = "Rtexto";
					csschooser = false;
				}
		
				else
				{
					csschooser = true;	
					backgroundclass = "Rtexte";
				}
			%>
		   		<tr>
			   		<td class = "<%= backgroundclass %>" valign="top"><bean:write name="listsupport" property="siteNumber"/></td>
			   		<td class = "<%= backgroundclass %>" valign="top"><bean:write name="listsupport" property="siteCity"/></td>
			   		<td class = "<%= backgroundclass %>" valign="top"><bean:write name="listsupport" property="siteState"/></td>
			   		<td class = "<%= backgroundclass %>" valign="top"><bean:write name="listsupport" property="onSiteDate"/>&nbsp;<bean:write name="listsupport" property="onSiteTime"/></td>
			   		<td class = "<%= backgroundclass %>" valign="top"><bean:write name="listsupport" property="offSiteTime"/></td>
			   		<td class = "<%= backgroundclass %>" width="300" valign="top"><bean:write name="listsupport" property="ticketProDesc"/></td>
			   		<td class = "<%= backgroundclass %>" width="1000" valign="top"><bean:write name="listsupport" property="closingSummary"/></td>
		   		</tr>
		   		</logic:iterate>
		   		<!-- List of Support Appendix: End -->
		   		
				<logic:empty name="SupportNetMedXForm" property="supportNetMedXlist" >
					  <!-- When Support NetMedeX has no data  -->
					  <tr>
				  			<td class="Rmessage" height="10" colspan="7"><bean:message bundle="RPT" key="rpt.accounting.message"/></td>
					  </tr>
				</logic:empty>
			</table>
		</td>
	</tr>
	</table>
</td>
</tr>
</table>
</html:form>
</body>
</html:html>
<!-- Form: Start -->
<script>
function blankcheck(){
/*
	This method is use to fire validation on date range TextBox and Sumbmit form.
*/
	if(document.forms[0].fromDate.value  == ""){ // - When From Date is blank
	
		alert('Select From Date for Search');
		document.forms[0].fromDate.focus();
		return false;
	}
	if(document.forms[0].toDate.value  == ""){ // - When To Date is blank
	
		alert('Select To Date for Search');
		document.forms[0].toDate.focus();
		return false;
	}
	if (!compDate_mdy(document.forms[0].fromDate.value, document.forms[0].toDate.value)){ // compare To Date and From Date
	
		alert("To Date Should be greater than From Date");
		return false;
	} 
	//document.forms[0].action = "/SalesSummary.do?";
	document.forms[0].submit();
	return true;
}
</script>