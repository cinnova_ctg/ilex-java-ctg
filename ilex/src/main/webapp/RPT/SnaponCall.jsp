<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This page include two jsp on the basis of condition.
-->

<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<head>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel = "stylesheet" href = "RPT/rptstyle/rptstyle.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<title>Snap-on Pre-call Tracking - Record Call</title>
</head>

<html:html>
<body>
<!-- Form: Start -->
<html:form action="/SnaponCall.do">
<html:hidden name="SnaponCallForm" property="jobId"/>
<html:hidden name="SnaponCallForm" property="appendixId"/>
<html:hidden name="SnaponCallForm" property="date"/>
<html:hidden name="SnaponCallForm" property="occurance"/>
<html:hidden name="SnaponCallForm" property="from"/>

<table cellpadding="0" cellspacing="0" border="0" width = "100%">
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			<!-- BreadCrumb: Start -->
		    <tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
		    <tr>
		    	<td background="images/content_head_04.jpg" height="21" colspan="2" class="RbreadCrumb">&nbsp;<bean:message bundle="RPT" key="rpt.support.breadcrumb"/></td>
		   	</tr>
		   	<!-- BreadCrumb: End -->
		    <tr><td colspan="2"><h2>Snap-on Pre-call Tracking - Record Call</h2></td></tr>
		</table></br>
		
		<!-- When status is not Reschedule or Escalate -->
		<logic:notEqual name="SnaponCallForm" property="from" value="rs-es">
			<jsp:include page = '/RPT/SnaponCall-1.jsp'>
				<jsp:param name = 'occurance' value = '<%=(String)request.getAttribute("occurance")%>'/>
			</jsp:include>
		</logic:notEqual>
		
		<!-- When status is Reschedule or Escalate -->
		<logic:equal name="SnaponCallForm" property="from" value="rs-es">
			<jsp:include page = '/RPT/SnaponCall-2.jsp'>
				<jsp:param name = 'occurance' value = '<%=(String)request.getAttribute("occurance")%>'/>
			</jsp:include>
		</logic:equal>
		
	</td>
</tr>
</table>
</html:form>
</body>
</html:html>
<!-- Form: End -->
<script type="text/javascript">
<!--
function setEscalate(){
		for(var i=0; i<document.forms[0].reschedule.length; i++){
		document.forms[0].reschedule[i].checked = false;
	}
}

function setReschedule(){
		for(var i=0; i<document.forms[0].escalate.length; i++){
		document.forms[0].escalate[i].checked = false;
	}
}

//-->
</script>
