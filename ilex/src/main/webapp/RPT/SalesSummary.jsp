<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<head>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel = "stylesheet" href = "RPT/rptstyle/rptstyle.css" type = "text/css">
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<title>Sales Summary</title>
</head>
<%
	String backgroundclass = null;
	boolean csschooser = true;
	int summaySize = 0;
	if(request.getAttribute("summaySize")!= null){  summaySize=Integer.parseInt(request.getAttribute("summaySize")+"");}
%>
<html:html>
<body>
<html:form action="/SalesSummary.do">
	<table cellpadding="0" cellspacing="0" border="0" width = "100%">
	    <tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
	    <tr>
	    	<td background="images/content_head_04.jpg" height="21" colspan="2" class="RbreadCrumb">&nbsp;<bean:message bundle="RPT" key="rpt.accounting.breadcrumb"/></td>
	   	</tr>
	    <tr><td colspan="2"><h2><bean:message bundle="RPT" key="rpt.accounting.title"/></h2></td></tr>
	    <tr>
	    	<td>&nbsp;</td>
			<td>
				<table cellpadding="1" cellspacing="0" border="0">
					<tr>
						<td class="Rlabeleboldwhite"><bean:message bundle="RPT" key="rpt.accounting.todate"/>
							<html:text  name  ="SalesSummaryForm" styleClass = "Rtext" size = "10" property = "toDate" readonly = "true"/>
							<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].toDate , document.forms[0].toDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
		 				</td>
		 				<td class="Rlabeleboldwhite">&nbsp;&nbsp;</td>
		 				<td class="Rlabeleboldwhite"><bean:message bundle="RPT" key="rpt.accounting.fromdate"/>
		 					<html:text  name  ="SalesSummaryForm" styleClass = "Rtext" size = "10" property = "fromDate" readonly = "true"/>
		 					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].fromDate , document.forms[0].fromDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
		 					
						</td>
						<td class="Rlabeleboldwhite">&nbsp;&nbsp;</td>
						<td class="Rlabeleboldwhite">
		 					<html:button property="go" styleClass="Rbutton_c" onclick="return blankcheck();"><bean:message bundle="RPT" key="rpt.accounting.go"/></html:button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>&nbsp;&nbsp;</td>
		<td>
			<table cellpadding="0" cellspacing="1" border="0">
				<tr height="19"> 
				    <td class = "texth"><div align = "center"><bean:message bundle="RPT" key="rpt.accounting.sitename"/></div></td>
				    <td class = "texth"><div align = "center"><bean:message bundle="RPT" key="rpt.accounting.jobtitle"/></div></td>
				    <td class = "texth"><div align = "center"><bean:message bundle="RPT" key="rpt.accounting.sitenumber"/></div></td>
				    <td class = "texth"><div align = "center"><bean:message bundle="RPT" key="rpt.accounting.sitestate"/></div></td>
				    <td class = "texth"><div align = "center"><bean:message bundle="RPT" key="rpt.accounting.closedate"/></div></td>
				    <td class = "texth"><div align = "center"><bean:message bundle="RPT" key="rpt.accounting.invoicenumber"/></div></td>
				    <td class = "texth"><div align = "center"><bean:message bundle="RPT" key="rpt.accounting.invoiceprice"/></div></td>
		   		</tr>
		   		<%if(summaySize > 0) {%>
		   		<logic:iterate id="listsummary" name="SalesSummaryForm" property="salesSummarylist">
		   		<%	
				if ( csschooser == true ) 
				{
					backgroundclass = "Rtexto";
					csschooser = false;
				}
		
				else
				{
					csschooser = true;	
					backgroundclass = "Rtexte";
				}
			%>
		   		<tr>
		   		<td class = "<%= backgroundclass %>"><bean:write name="listsummary" property="lm_si_name"/></td>
		   		<td class = "<%= backgroundclass %>"><bean:write name="listsummary" property="lm_js_title"/></td>
		   		<td class = "<%= backgroundclass %>"><bean:write name="listsummary" property="lm_si_number"/></td>
		   		<td class = "<%= backgroundclass %>"><bean:write name="listsummary" property="lm_si_state"/></td>
		   		<td class = "<%= backgroundclass %>"><bean:write name="listsummary" property="lm_js_closed_date"/></td>
		   		<td class = "<%= backgroundclass %>"><bean:write name="listsummary" property="lm_js_invoice_no"/></td>
		   		<td class = "<%= backgroundclass %>"><bean:write name="listsummary" property="lx_ce_invoice_price"/></td>
		   		</tr>
		   		</logic:iterate>
		   		<tr> 
					<td colspan = "7" class = "Nbuttonrow">&nbsp;</td>
				</tr>
				<%}else{ %>
					  <tr>
				  			<td class="Rmessage" height="10" colspan="7"><bean:message bundle="RPT" key="rpt.accounting.message"/></td>
					  </tr>
				<%} %>
			</table>
		</td>
	</tr>
	</table>
</html:form>
</body>
</html:html>
<script>
function blankcheck(){
	if(document.forms[0].toDate.value  == ""){
		alert('Select To Date for Search');
		document.forms[0].toDate.focus();
		return false;
	}
	if(document.forms[0].fromDate.value  == ""){
		alert('Select From Date for Search');
		document.forms[0].fromDate.focus();
		return false;
	}
	if (!compDate_mdy(document.forms[0].toDate.value, document.forms[0].fromDate.value)){
	
		alert("From Date Should be greater than To Date");
		return false;
	} 
	//document.forms[0].action = "/SalesSummary.do?";
	document.forms[0].submit();
	return true;
}
</script>