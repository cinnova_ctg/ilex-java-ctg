<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Display Audit Trail Report.
-->
<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<head>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel = "stylesheet" href = "RPT/rptstyle/rptstyle.css" type = "text/css">	
	<title>Audit Trail Reports</title>		
	<script language="JavaScript">
		//cache 2 arrow images
		imageUp = new Image();
		imageUp.src = "RPT/rptimages/Expand.gif";
		imageDown = new Image();
		imageDown.src = "RPT/rptimages/Collapse.gif";		
		var cnt;										// for doing the loop
		var objSpanCollection;							// store a collecion of Menu
		var menuHeightCollection = new Array(); 		// store a collection of Menulists' height
		var objMenu;									// the menu is clicked on
		
		function InitializeMenu() 
		{
			// get a collection of menus
			objSpanCollection = document.body.getElementsByTagName("SPAN");
			for (var i = 0; i < objSpanCollection.length; i++)
			{
				var objSpan = objSpanCollection(i);
				// get a collection of Menus' height
				menuHeightCollection[i] = objSpan.childNodes.item(1).clientHeight;
				// assign the click event to every Menuheader
				objSpan.childNodes.item(0).onclick = ControlMenu;
			}
		}
		
		function ControlMenu() 
		{
			cnt = 1;
			objMenu = this.parentNode.childNodes.item(1);	// memorize the Menulist has been clicked
			
			// Get the arrow that belongs to the clicked menu
			// starting with <div> then <table> then <tbody> then <tr> then <td> and the last one is
			// what we need: <img>
			var objArrow = this.childNodes(0).childNodes(0).childNodes(0).childNodes(0).childNodes(0);
			  
			if (objMenu.style.display == "none")
			{
				objArrow.src = imageDown.src;  //change to the Down Arrow
				ShowMenu();
			}
			else
			{
				objArrow.src = imageUp.src;  //change to the Up Arrow
				HideMenu();
			}
		}
		
		function ShowMenu()
		{	
			var objList = objMenu.childNodes.item(0);	// get the Linkslist of the Menulist
			if (cnt < 10)
			{
				// display the Menulist
				objMenu.style.display = "block";
				// increase the tranparency of the Menulist
				objMenu.filters["alpha"].opacity = objMenu.filters["alpha"].opacity + 30;
				// loop through the Menu collection to find the position of the clicked Menu
				// to get the actual height of the menu list and then increase the height of the Menulist
				for (var i = 0; i < objSpanCollection.length; i++)
					if (objMenu.parentNode == objSpanCollection[i])
						objMenu.style.height = objMenu.clientHeight + (menuHeightCollection[i]/10);
				cnt++;
				// do this function again after 30 miliseconds until the actual Menulist's height is returned
				//setTimeout("ShowMenu()",00)
				ShowMenu();
			}
			// display the Menulist if the it's actual height is returned
			if (cnt >= 10)
				objList.style.display = "block";  
		}
		
		function HideMenu()
		{	
			var objList = objMenu.childNodes.item(0);	// get the Linkslist of the Menulist
			if (cnt < 10)
			{
				objMenu.filters["alpha"].opacity = objMenu.filters["alpha"].opacity - 10;
				for (var i = 0; i < objSpanCollection.length; i++)
					if (objMenu.parentNode == objSpanCollection[i])
						objMenu.style.height = objMenu.clientHeight - (menuHeightCollection[i]/10);
				objList.style.display = "none";
				cnt++;
				//setTimeout("HideMenu()",00)
				HideMenu();
			}
			if (cnt >= 10)
				objMenu.style.display = "none";
		}		
</script>	
<script>
function blankcheck(){	
	document.forms[0].action='./AuditTrail.do?&submitButton='+document.forms[0].submitButton.value;
	document.forms[0].submit();
	return true;	
}
function selectTable(){
	document.forms[0].action='./AuditTrail.do';	
	document.forms[0].submit();
	return true;	
}
</script>	
</head>
<% 
String backgroundclass = null; //- td style class
boolean csschooser = true;	
String backgroundclas = null; //- td style class
boolean csschoosers = true;// - for alternet change color of rows
int auditTraillist=0;
int auditList=0;
if(request.getAttribute("auditTraillist")!= null){  
	auditTraillist=Integer.parseInt(request.getAttribute("auditTraillist")+""); // get no. of rows in list	
}
if(request.getAttribute("auditList")!= null){  
	auditList=Integer.parseInt(request.getAttribute("auditList")+""); // get no. of rows in list	
}
%>
<html:html>
<body onLoad="InitializeMenu()">
<html:form action="/AuditTrail.do">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<!-- BreadCrumb: Start -->
    <tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
    <tr>
    	<td background="images/content_head_04.jpg" height="21" colspan="2" class="RbreadCrumb">&nbsp;Audit Trail</td>
   	</tr>
   	<!-- BreadCrumb: End -->
    <tr><td colspan="2"><h2>Audit Trail Report</h2></td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
				<tr>
					<TD> </TD>
				</tr>
				<tr>
					<TD class="Rlabeleboldwhite">SEARCH</TD>
					<TD class="data" align="right">Select Table:</TD>
					
					<TD align="lift">
						<html:select onchange="selectTable()" name="AuditTrailForm" property="tn" styleClass="select" >				       
				        <html:options  collection="SelectTable" property="tablename" labelProperty="tablename"/>
						</html:select>														 
			        </TD>				        
					<TD class="data"align="right">Field:</TD>
					
					<TD><html:select  name="AuditTrailForm" property="fn" styleClass="select" >				       
				        	<html:options collection="Field" property="fieldName" labelProperty="fieldName"/>
						</html:select>
					</TD>				        
				</tr>
				<TR><td height="5"></td></TR>
				<tr>
					<TD></TD>
					<TD class="data"align="right">Operation:</TD>
					<TD>
						<html:select  name="AuditTrailForm" property="oper" styleClass="select" >				       
				        	<html:options collection="Operation" property="operation" labelProperty="operation"/>
						</html:select>
						</TD>
					<TD class="data"align="right">User:</TD>
					<TD>					
					 <html:select  name="AuditTrailForm" property="users" styleClass="select" >				       
				        	<html:options collection="users" property="updatedBy" labelProperty="updatedBy"/>
						</html:select>
					&nbsp;&nbsp;&nbsp;<input type="Button" class="Rbutton" valign="bottom" name="submitButton" onClick="blankcheck()" value=Submit></TD>
				</tr>
				<TR><td height="5"></td></TR>
				<TR>
					<TD colspan=5>						
							<table cellpadding="0" cellspacing="1" border="0" width = "100%">
								<tr>
									<TD class = "texth" width="5%"></TD>
									<TD class = "texth" width="30%">Table Name</TD>
									<TD class = "texth" width="15%">User </TD>
									<TD class = "texth" width="15%">Operation </TD>
									<TD class = "texth" width="35%">Date Time</TD>
								</tr>
							</table>						
					</TD>								
				</TR>
				<TR>
				<%if(auditList > 0) {%>
				<logic:iterate id="listdata" name="AuditTrailForm" property="searchData">
				<%
					if ( csschooser == true ) 
				{
					backgroundclass = "RtextoAudit";
					csschooser = false;
				}
		
				else
				{
					csschooser = true;	
					backgroundclass = "RtexteAudit";
				}
			%>		
				<TD colspan="5">					
				<span>
						<div style="display:inherit">
							<table cellpadding="1" cellspacing="1" border="0" width = "100%">								
								<tr>
									<td wrap class = "<%= backgroundclass %>" width="5%"><img src="RPT/rptimages/Expand.gif" border="0"></td>
									<td wrap class = "<%= backgroundclass %>"  width="30%"><bean:write name="listdata" property="selectTable"/></td>
									<td wrap class = "<%= backgroundclass %>"  width="15%"><bean:write name="listdata" property="user"/></td>
									<td wrap class = "<%= backgroundclass %>" width="15%"><bean:write name="listdata" property="soperation"/></td>
									<td wrap class = "<%= backgroundclass %>" width="35%"><bean:write  name="listdata" property="dateTime"/></td>
								</tr>															
							</table>
						</div>
						<div class="RMenu_Items1" style="display:none;">
							<div>
							<Table cellpadding="0" cellspacing="1" border="0" width = "100%">
								<TR>
									<td width="5%">&nbsp;</td>										
									<td class="texth" colspan=2 width="15%">Field Name</td>									 										     
									<td class="texth" width="30%">Pre Value</td>
									<td class="texth" width="35%">Post Value </td>
								</TR>
								
								
								<c:forEach varStatus="status.count" items="${requestScope.Flist}"   var="sele">
								   <c:if test="${sele.soper eq listdata.soperation && sele.sdateTime eq listdata.dateTime && sele.auditTxnID eq listdata.sauditID}">
								<TR>
								<%
					if ( csschoosers == true ) 
				{
					backgroundclas = "RtextoAudit";
					csschoosers = false;
				}
		
				else
				{
					csschoosers = true;	
					backgroundclas = "RtexteAudit";
				}
			%>	
									<td wrap width="5%">&nbsp;</td>								
									<td wrap valign='top' class = "<%= backgroundclas %>" colspan="2" width="15%"><c:out value="${sele.field}"/></td>																			     
									<td wrap valign='top' class = "<%= backgroundclas %>"  width="30%"><c:out value="${sele.preVal}"/></td>
									<td wrap valign='top' class = "<%= backgroundclas %>"  width="35%"><c:out value="${sele.postVal}"/></td>
								</TR></c:if>
								</c:forEach>
							</Table>
							</div>
						</div>
					</span>	
					</TD>								
				
				</TR></logic:iterate>								
				<%}else{ %>
				 <tr>
					 <td class="Rmessage" height="10" colspan="3">No records match report criteria.</td>				  		
				  </tr>
				  <%} %>				
			</table>
		</td>
	</tr>
</table>
</html:form>
</body>
</html:html>