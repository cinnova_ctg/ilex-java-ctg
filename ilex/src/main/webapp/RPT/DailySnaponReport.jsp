<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This SP show Daily Buzz Report in RPT module.
-->

<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<head>
<!-- Import Files -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></meta>

<title>Daily Buzz Report</title>
<!-- Include Files -->
	<link href="RPT/rptstyle/rptstyle.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "RPT/rptjscript/tabcontent.js"></script>
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>

</head>
<%
	String backgroundclass = "";
	boolean csschooser = true;
%>
<html:html>
<body onload="">
<!-- Form: Start -->
<html:form action="/DailySnaponReport.do">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td colspan="2">
			<!-- BreadCrumb: Start -->
			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
				<tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
				<tr><td background="images/content_head_04.jpg" height="21" colspan="2" class="RbreadCrumb">&nbsp;<bean:message bundle="RPT" key="rpt.support.breadcrumb"/></td></tr>
				<tr><td colspan="2"><h2>Snap-on Pre-call Tracking - Daily Report</h2></td></tr>
			</table>
			<!-- BreadCrumb: End -->
		</td>
	</tr>
	<tr>
	  	<td>&nbsp;</td>
	  	<td>
		  	<table border="0" cellspacing="0" cellpadding="0">
			    <tr>
			      <td align="left">
				      <table border="0" cellspacing="0" cellpadding="0" width="100%">
				        <tr>
				       		<td align="right" class="textbold">Date View:&nbsp;
					       		<html:text  name  ="DailySnaponReportForm" styleClass = "textbox" size = "10" property = "date" readonly = "true"/>
							 	<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].date , document.forms[0].date , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							 	<html:submit property="go" styleClass="Rbutton" onclick="return validateAll();">Go</html:submit>
						 	</td>
				       </tr>
				       
				       	<logic:present name="POCMailId" scope="request" >
					       	<tr>
					       		<td class="message">&nbsp;Mail could not be send as Site's Primary POC is not available.</td>
					       		
					       	</tr>
					       	<tr>
					       		<td height="5"></td>
					       	</tr>
				       	</logic:present>
				       
				      </table>
			        </td>
			    </tr>
			    <tr>
				  <td>
				  	<table border="0" cellpadding="0" cellspacing="0">
				      <tr>
				        <td bgcolor="#FFFFFF">
				        	<table border="0" cellspacing="0" cellpadding="0">
					            <tr>
					              <td>
					              	<!-- Scheduled: Start -->
					              	<table border='0' cellspacing='1' cellpadding='0'>
					              		<!-- Include list header name -->
					              		<tr>
										<td colspan="9">
											<table cellpadding="0" cellspacing="1" border="0" width="100%">
							              		<jsp:include page = '/RPT/SnaponInclude.jsp'>
													<jsp:param name = 'status' value = 'sch'/>
												</jsp:include>
												
												<logic:iterate id="schjoblist" name="DailySnaponReportForm" property="snSchJobList">
													<%
													if ( csschooser == true ){
														backgroundclass = "Rtexto";
														csschooser = false;
													}else{
														csschooser = true;	
														backgroundclass = "Rtexte";
													}
													%>
													<tr>
														<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="job_name"/></td>
														<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="planned_start_date"/>&nbsp;<bean:write name="schjoblist" property="planned_start_time"/></td>
														<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="site_number"/></td>
														<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="site_city"/></td>
														<td class="<%=backgroundclass%>"><bean:write name="schjoblist" property="site_state"/></td>
														<td class="<%=backgroundclass%>">
															<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="schjoblist" property="job_id"/>&appendixid=<bean:write name="schjoblist" property="appendix_id"/>">Job Dashboard</a>
															<!--  &nbsp;|&nbsp;
															<a href="SnaponCall.do?jobid=<bean:write name="schjoblist" property="job_id"/>&appendixid=<bean:write name="schjoblist" property="appendix_id"/>&date=<bean:write name="DailySnaponReportForm" property="date"/>">Tracking</a>-->
														</td>
															
													</tr>
						
												</logic:iterate>
											</table>
										</td>
										</tr>
										<logic:empty name="DailySnaponReportForm" property="snSchJobList">
										<tr>
											<td class='message' height='10' colspan='9'>No records match report criteria.</td>
										</tr>
										</logic:empty>
					          		<!-- Scheduled: End -->
					          			<tr>
					              			<td height='10' colspan="9">&nbsp;</td>
					              		</tr>
					          		<!-- Confirmed - 5 Day Call Pending: Start -->
					          			
										<logic:iterate id="jobList" name="DailySnaponReportForm" property="snCnfJobList">
											<!-- Include list header name -->
											<jsp:include page = '/RPT/SnaponInclude.jsp'>
												<jsp:param name = 'status' value = 'cnf'/>
												<jsp:param name = 'cnfStatus' value = 'cnfStatus'/>
											</jsp:include>
											
											<logic:iterate id="day5CallList" name="jobList" property="day5CallList">
												<%
												if ( csschooser == true ){
													backgroundclass = "Rtexto";
													csschooser = false;
												}else{
													csschooser = true;	
													backgroundclass = "Rtexte";
												}
												%>
												<tr>
													<td class="<%=backgroundclass%>"><bean:write name="day5CallList" property="job_name"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="day5CallList" property="planned_start_date"/>&nbsp;<bean:write name="day5CallList" property="planned_start_time"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="day5CallList" property="site_number"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="day5CallList" property="site_city"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="day5CallList" property="site_state"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="day5CallList" property="day5_1"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="day5CallList" property="day5_2"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="day5CallList" property="hour24"/></td>
													<td class="<%=backgroundclass%>">
														<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="day5CallList" property="job_id"/>&appendixid=<bean:write name="day5CallList" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
														<a href="SiteAction.do?ref=Add&&Job_Id=<bean:write name="day5CallList" property="job_id"/>&ref1=fromDailyReportPage&Appendix_Id=<bean:write name="day5CallList" property="appendix_id"/>&datefrom=<bean:write name="DailySnaponReportForm" property="date"/>">Site</a>&nbsp;|&nbsp;
														<a href="SnaponCall.do?jobid=<bean:write name="day5CallList" property="job_id"/>&appendixid=<bean:write name="day5CallList" property="appendix_id"/>&date=<bean:write name="DailySnaponReportForm" property="date"/>&from=5daycall">Tracking</a>
													</td>
												</tr>
											</logic:iterate>
											<logic:empty name="jobList" property="day5CallList">
											<tr>
												<td height='28' class='message' height='10' colspan='9'>No records match report criteria.</td>
											</tr>
											</logic:empty>
										<!-- Confirmed - 5 Day Call Pending: End -->
											<tr>
						              			<td height='10' colspan="6">&nbsp;</td>
						              		</tr>
						              		
										<!-- Confirmed - 24 Hour Call Pending: Start -->
											<!-- Include list header name -->
											<jsp:include page = '/RPT/SnaponInclude.jsp'>
												<jsp:param name = 'status' value = 'cnf'/>
												<jsp:param name = 'cnfStatus' value = 'hour24CallList'/>
											</jsp:include>
											<logic:iterate id="hour24CallList" name="jobList" property="hour24CallList">
												<%
												if ( csschooser == true ){
													backgroundclass = "Rtexto";
													csschooser = false;
												}else{
													csschooser = true;	
													backgroundclass = "Rtexte";
												}
												%>
												<tr>
													<td class="<%=backgroundclass%>"><bean:write name="hour24CallList" property="job_name"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="hour24CallList" property="planned_start_date"/>&nbsp;<bean:write name="hour24CallList" property="planned_start_time"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="hour24CallList" property="site_number"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="hour24CallList" property="site_city"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="hour24CallList" property="site_state"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="hour24CallList" property="day5_1"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="hour24CallList" property="day5_2"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="hour24CallList" property="hour24"/></td>
													<td class="<%=backgroundclass%>">
														<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="hour24CallList" property="job_id"/>&appendixid=<bean:write name="hour24CallList" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
														<a href="SiteAction.do?ref=Add&&Job_Id=<bean:write name="hour24CallList" property="job_id"/>&ref1=fromDailyReportPage&Appendix_Id=<bean:write name="hour24CallList" property="appendix_id"/>&datefrom=<bean:write name="DailySnaponReportForm" property="date"/>">Site</a>&nbsp;|&nbsp;
														<a href="SnaponCall.do?jobid=<bean:write name="hour24CallList" property="job_id"/>&appendixid=<bean:write name="hour24CallList" property="appendix_id"/>&date=<bean:write name="DailySnaponReportForm" property="date"/>&from=cnf">Tracking</a>
													</td>
												</tr>
											</logic:iterate>
											<logic:empty name="jobList" property="hour24CallList">
											<tr>
												<td height='28' class='message' height='10' colspan='9'>No records match report criteria.</td>
											</tr>
											</logic:empty>
										<!-- Confirmed - 24 Hour Call Pending: End -->
											<tr>
						              			<td height='10' colspan="6">&nbsp;</td>
						              		</tr>
						              		
										<!-- Confirmed - Calls Completed Successfully: Start -->
											<!-- Include list header name -->
											<jsp:include page = '/RPT/SnaponInclude.jsp'>
												<jsp:param name = 'status' value = 'cnf'/>
												<jsp:param name = 'cnfStatus' value = 'readyToGo'/>
											</jsp:include>
											<logic:iterate id="readyToGo" name="jobList" property="readyToGo">
												<%
												if ( csschooser == true ){
													backgroundclass = "Rtexto";
													csschooser = false;
												}else{
													csschooser = true;	
													backgroundclass = "Rtexte";
												}
												%>
												<tr>
													<td class="<%=backgroundclass%>"><bean:write name="readyToGo" property="job_name"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="readyToGo" property="planned_start_date"/>&nbsp;<bean:write name="readyToGo" property="planned_start_time"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="readyToGo" property="site_number"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="readyToGo" property="site_city"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="readyToGo" property="site_state"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="readyToGo" property="day5_1"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="readyToGo" property="day5_2"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="readyToGo" property="hour24"/></td>
													<td class="<%=backgroundclass%>">
														<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="readyToGo" property="job_id"/>&appendixid=<bean:write name="readyToGo" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
														<a href="SiteAction.do?ref=Add&&Job_Id=<bean:write name="readyToGo" property="job_id"/>&ref1=fromDailyReportPage&Appendix_Id=<bean:write name="readyToGo" property="appendix_id"/>&datefrom=<bean:write name="DailySnaponReportForm" property="date"/>">Site</a>&nbsp;|&nbsp;
														<a href="SnaponCall.do?jobid=<bean:write name="readyToGo" property="job_id"/>&appendixid=<bean:write name="readyToGo" property="appendix_id"/>&date=<bean:write name="DailySnaponReportForm" property="date"/>">Tracking</a>
													</td>
												</tr>
											</logic:iterate>
											<logic:empty name="jobList" property="readyToGo">
											<tr>
												<td height='28' class='message' height='10' colspan='9'>No records match report criteria.</td>
											</tr>
											</logic:empty>
										<!-- Confirmed - Calls Completed Successfully: End -->
											<tr>
						              			<td height='10' colspan="6">&nbsp;</td>
						              		</tr>
						              		
										<!-- Escalate: Start -->
											<!-- Include list header name -->
											<jsp:include page = '/RPT/SnaponInclude.jsp'>
												<jsp:param name = 'status' value = 'cnf'/>
												<jsp:param name = 'cnfStatus' value = 'escalate'/>
											</jsp:include>
											<logic:iterate id="escalate" name="jobList" property="escalate">
												<%
												if ( csschooser == true ){
													backgroundclass = "Rtexto";
													csschooser = false;
												}else{
													csschooser = true;	
													backgroundclass = "Rtexte";
												}
												%>
												<tr>
													<td class="<%=backgroundclass%>"><bean:write name="escalate" property="job_name"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="escalate" property="planned_start_date"/>&nbsp;<bean:write name="escalate" property="planned_start_time"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="escalate" property="site_number"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="escalate" property="site_city"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="escalate" property="site_state"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="escalate" property="day5_1"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="escalate" property="day5_2"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="escalate" property="hour24"/></td>
													<td class="<%=backgroundclass%>">
														<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="escalate" property="job_id"/>&appendixid=<bean:write name="escalate" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
														<a href="SiteAction.do?ref=Add&&Job_Id=<bean:write name="escalate" property="job_id"/>&ref1=fromDailyReportPage&Appendix_Id=<bean:write name="escalate" property="appendix_id"/>&datefrom=<bean:write name="DailySnaponReportForm" property="date"/>">Site</a>&nbsp;|&nbsp;
														<a href="SnaponCall.do?jobid=<bean:write name="escalate" property="job_id"/>&appendixid=<bean:write name="escalate" property="appendix_id"/>&date=<bean:write name="DailySnaponReportForm" property="date"/>&from=rs-es">Tracking</a>
													</td>
												</tr>
											</logic:iterate>
											<logic:empty name="jobList" property="escalate">
											<tr>
												<td height='28' class='message' height='10' colspan='9'>No records match report criteria.</td>
											</tr>
											</logic:empty>
										<!-- Escalate: End -->
											<tr>
						              			<td height='10' colspan="6">&nbsp;</td>
						              		</tr>
										<!-- Rescheduled: Start -->
											<!-- Include list header name -->
											<jsp:include page = '/RPT/SnaponInclude.jsp'>
												<jsp:param name = 'status' value = 'cnf'/>
												<jsp:param name = 'cnfStatus' value = 'rescheduled'/>
											</jsp:include>
											<logic:iterate id="rescheduled" name="jobList" property="rescheduled">
												<%
												if ( csschooser == true ){
													backgroundclass = "Rtexto";
													csschooser = false;
												}else{
													csschooser = true;	
													backgroundclass = "Rtexte";
												}
												%>
												<tr>
													<td class="<%=backgroundclass%>"><bean:write name="rescheduled" property="job_name"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="rescheduled" property="planned_start_date"/>&nbsp;<bean:write name="rescheduled" property="planned_start_time"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="rescheduled" property="site_number"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="rescheduled" property="site_city"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="rescheduled" property="site_state"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="rescheduled" property="day5_1"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="rescheduled" property="day5_2"/></td>
													<td class="<%=backgroundclass%>"><bean:write name="rescheduled" property="hour24"/></td>
													<td class="<%=backgroundclass%>">
														<a href="JobDashboardAction.do?function=view&jobid=<bean:write name="rescheduled" property="job_id"/>&appendixid=<bean:write name="rescheduled" property="appendix_id"/>">Job Dashboard</a>&nbsp;|&nbsp;
														<a href="SiteAction.do?ref=Add&&Job_Id=<bean:write name="rescheduled" property="job_id"/>&ref1=fromDailyReportPage&Appendix_Id=<bean:write name="rescheduled" property="appendix_id"/>&datefrom=<bean:write name="DailySnaponReportForm" property="date"/>">Site</a>&nbsp;|&nbsp;
														<a href="SnaponCall.do?jobid=<bean:write name="rescheduled" property="job_id"/>&appendixid=<bean:write name="rescheduled" property="appendix_id"/>&date=<bean:write name="DailySnaponReportForm" property="date"/>&from=rs-es">Tracking</a>
													</td>
												</tr>
											</logic:iterate>
											<logic:empty name="jobList" property="rescheduled">
											<tr>
												<td height='28' class='message' height='10' colspan='9'>No records match report criteria.</td>
											</tr>
											</logic:empty>
										<!-- Rescheduled: End -->
										</logic:iterate>
					          		<!-- Confirmed: End -->
					          			
				        		</td>
				      		</tr>
				  		</table>
				 	</td>
			    </tr>
		  </table>
	  </td>
  </tr>
</table>
</html:form>
<!-- Form: End -->
</body>
</html:html>
<script>
function validateAll(){
	if(document.forms[0].date.value == ''){
		alert('Select Date for Search');
		return false;
	}
}
</script>