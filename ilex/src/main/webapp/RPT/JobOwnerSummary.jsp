<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying  Job Owner Summary List according to selected Tab. All list data is come
				  when tab is clicked. This jsp include java.util.* & com.mind.RPT.dao.CalendarData java package for calendar tab only.
-->

<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<% 
response.setHeader("pragma","no-cache");//HTTP 1.1 
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Cache-Control","no-store"); 
response.addDateHeader("Expires", -1); 
response.setDateHeader("max-age", 0); 
//response.setIntHeader ("Expires", -1); //prevents caching at the proxy server 
response.addHeader("cache-Control", "private"); 
%>
<head>
<!-- Import Files -->
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.*" %>
<%@ page import = "com.mind.RPT.dao.CalendarData" %>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></meta>

<title>Job Owner Summary</title>
<!-- Include Files -->
	<link href="RPT/rptstyle/rptstyle.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "RPT/rptjscript/tabcontent.js"></script>
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
</head>
<%
	String temptitle = "";
	String tabname="";			// - Tab Name
	String tabselected="";		// - Selected Tab Function name
	String backgroundclass = "";
	boolean csschooser = true;
	int count = 0;
	int tabSize = 0;

	/* Get Tab size */
	if(!tabname.equals("calendar")){
		if(request.getAttribute("tabInfoSize") != null ){
			tabSize = Integer.parseInt( request.getAttribute("tabInfoSize").toString() );
		}
	}
	
	//- Use to call JavaScript Function on body Onload
	if(request.getAttribute("tabname") != null){tabname = (String)request.getAttribute("tabname");}

	if(tabname.equals("scheduled")){tabselected = "MM_swapImgRestore();MM_swapImage('Image1','','RPT/rptimages/sd_bold.gif',1);";}
	else if(tabname.equals("confirmed")){tabselected = "MM_swapImage('Image2','','RPT/rptimages/confirmed_bold.jpg',1);";}
	else if(tabname.equals("onsite")){tabselected = "MM_swapImage('Image3','','RPT/rptimages/onsite_bold.gif',1);";}
	else if(tabname.equals("offsite")){tabselected = "MM_swapImage('Image4','','RPT/rptimages/offsite_bold.gif',1);";}
	else if(tabname.equals("complete")){tabselected = "MM_swapImage('Image5','','RPT/rptimages/com_bold.gif',1);";}
	else if(tabname.equals("notscheduled")){tabselected = "MM_swapImage('Image6','','RPT/rptimages/ns_bold.gif',1);";}
	else if(tabname.equals("ccc")){tabselected = "MM_swapImage('Image7','','RPT/rptimages/ccc_bold.gif',1);";}
	else if(tabname.equals("calendar")){tabselected = "MM_swapImage('Image8','','RPT/rptimages/cal_bold.gif',1);";}
%>
<html:html>
<body onload="<%=tabselected %>;">

<!-- Form: Start -->
<html:form action="/JobOwnerSummary.do">
<html:hidden name="JobOwnerSummaryForm" property="tabName" />
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
	  	<td>&nbsp;</td>
	  	<td>
		  	<table border="0" cellspacing="0" cellpadding="0">
			    <tr>
			      <td align="left"><table border="0" cellspacing="0" cellpadding="0" width="100%">
			      	<logic:equal name="JobOwnerSummaryForm" property="tabName" value="calendar">
			        <tr>
			        	<td colspan="6" align="right" class="textbold" style="padding-bottom: 4px;">Sr. Project Manager:&nbsp;
			        		<html:select name="JobOwnerSummaryForm" property="srProjectManagersId" styleClass="select">
						    	<html:optionsCollection name = "JobOwnerSummaryForm"  property = "srProjectManagers"  label = "label"  value = "value" />
						    </html:select>
			        	</td>
			        </tr>
			        </logic:equal>
			        <tr>
				        <td colspan="6" align="right" class="textbold">Job Owner:&nbsp;
			        	<!-- Job Owner Combobox: Start -->
				          	<html:select name="JobOwnerSummaryForm" property = "jobOwner" styleClass = "select">
								<html:optionsCollection name = "JobOwnerSummaryForm" property = "jobOwnerList" value = "value" label = "label"/> 
							</html:select>	
						<!-- Job Owner Combobox: End -->							
				        </td>
			        </tr>
			        <tr>
				    	<td colspan="6" align="right" class="textbold" style="padding-top: 4px;padding-bottom: 4px">Project:&nbsp;
				        	<!-- Project Combobox: Start -->
				    		<html:select name="JobOwnerSummaryForm" property = "appendixName" styleClass = "select">
				    			<logic:iterate id="msalist" name="JobOwnerSummaryForm" property="msaList" >
				    				<logic:notEqual name="msalist" property="msaName" value="notShow">
				    					<optgroup class=labelebold label="<bean:write name = "msalist" property = "msaName" />">
				    				</logic:notEqual>
									<html:optionsCollection name = "msalist" property = "appendixList" value = "value" label = "label"/> 
									<logic:notEqual name="msalist" property="msaName" value="notShow">
				    					</optgroup>
				    				</logic:notEqual>
								</logic:iterate>
							</html:select>
				        	<!-- Project Combobox: End -->	
				    	</td>
			        </tr>
			        <tr>
			          <td><table width="320" height="21" border="0" cellpadding="0" cellspacing="0">
			            <tr><!-- Tab Row: Start -->
				            <td valign="bottom"><a href="#" onclick="submitform('scheduled');"><img src="RPT/rptimages/sd.gif" name="Image1" width="60" height="21" border="0" id="Image1"/></a></td>
				            <td valign="bottom"><a href="#" onclick="submitform('confirmed');"><img src="RPT/rptimages/confirmed.jpg" name="Image2" width="65" height="21" border="0" id="Image2"/></a></td>
				            <td valign="bottom"><a href="#" onclick="submitform('onsite');"><img src="RPT/rptimages/onsite.gif" name="Image3" width="40" height="21" border="0" id="Image3" /></a></td>
				            <td valign="bottom"><a href="#" onclick="submitform('offsite');"><img src="RPT/rptimages/offsite.gif" name="Image4" width="40" height="21" border="0" id="Image4" /></a></td>
				            <td valign="bottom"><a href="#" onclick="submitform('complete');"><img src="RPT/rptimages/com.gif" name="Image5" width="55" height="21" border="0" id="Image5" /></a></td>
				            <td valign="bottom"><a href="#" onclick="submitform('notscheduled');"><img src="RPT/rptimages/ns.gif" name="Image6" width="75" height="21" border="0" id="Image6" /></a></td>
				            <td valign="bottom"><a href="#" onclick="submitform('ccc');"><img src="RPT/rptimages/ccc.gif" name="Image7" width="35" height="21" border="0" id="Image7" /></a></td>
				            <td valign="bottom"><a href="#" onclick="submitform('calendar');"><img src="RPT/rptimages/cal.gif" name="Image8" width="55" height="21" border="0" id="Image8" /></a></td>
			            </tr><!-- Tab Row: End -->
			          </table></td>
			          <td align="right">
				          <table border="0" cellpadding="0" cellspacing="1">
					          <tr><!-- Date TextBox: Start -->
					               <td class="textbold">From:
					               		<logic:notEqual name="JobOwnerSummaryForm" property="tabName" value="calendar">
					               		<logic:notEqual name="JobOwnerSummaryForm" property="tabName" value="ccc">
							       	    	<html:text  name  ="JobOwnerSummaryForm" styleClass = "textbox" size = "10" property = "fromDate" readonly = "true"/>
						 					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].fromDate , document.forms[0].fromDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
						 					<html:hidden name="JobOwnerSummaryForm" property="calFromDate" />
						 					<html:hidden name="JobOwnerSummaryForm" property="cccFromDate" />
						 				</logic:notEqual>
						 				</logic:notEqual>
						 				<logic:equal name="JobOwnerSummaryForm" property="tabName" value="calendar">
								        	<html:text  name  ="JobOwnerSummaryForm" styleClass = "textbox" size = "10" property = "calFromDate" readonly = "true"/>
							 				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].calFromDate , document.forms[0].calFromDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							 				<html:hidden name="JobOwnerSummaryForm" property="fromDate" />
							 				<html:hidden name="JobOwnerSummaryForm" property="cccFromDate" />
						 				</logic:equal>
						 				<logic:equal name="JobOwnerSummaryForm" property="tabName" value="ccc">
						 					<html:text  name  ="JobOwnerSummaryForm" styleClass = "textbox" size = "10" property = "cccFromDate" readonly = "true"/>
							 				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].cccFromDate , document.forms[0].cccFromDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							 				<html:hidden name="JobOwnerSummaryForm" property="fromDate" />
							 				<html:hidden name="JobOwnerSummaryForm" property="calFromDate" />
						 				</logic:equal>
					            	</td>
						            <td class="textbold">To:
							        	<logic:notEqual name="JobOwnerSummaryForm" property="tabName" value="calendar">
							        	<logic:notEqual name="JobOwnerSummaryForm" property="tabName" value="ccc">
								        	<html:text  name  ="JobOwnerSummaryForm" styleClass = "textbox" size = "10" property = "toDate" readonly = "true" />
							 				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].toDate , document.forms[0].toDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							 				<html:hidden name="JobOwnerSummaryForm" property="cccToDate" />
						 				</logic:notEqual>
						 				</logic:notEqual>
							 				<logic:equal name="JobOwnerSummaryForm" property="tabName" value="calendar">
							 				<html:text  name  ="JobOwnerSummaryForm" styleClass = "textbox" size = "10" property = "calToDate" readonly = "true" disabled="true"/>
						 					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
						 					<html:hidden name="JobOwnerSummaryForm" property="toDate" />
						 					<html:hidden name="JobOwnerSummaryForm" property="cccToDate" />
						 				</logic:equal>
						 				<logic:equal name="JobOwnerSummaryForm" property="tabName" value="ccc">
						 					<html:text  name  ="JobOwnerSummaryForm" styleClass = "textbox" size = "10" property = "cccToDate" readonly = "true"/>
						 					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].cccToDate , document.forms[0].cccToDate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
						 					<html:hidden name="JobOwnerSummaryForm" property="toDate" />
						 					<html:hidden name="JobOwnerSummaryForm" property="calToDate" />
						 				</logic:equal>
							        </td>
						            <td><img src="RPT/rptimages/spacer.gif" width="5" height="2" /></td>
						            <td><html:button property="go" styleClass="Rbutton" onclick="return validateAll(document.forms[0].tabName.value);">Go</html:button></td>
					            </tr><!-- Date TextBox: End -->
				          </table>
			          </td>
			        </tr>
			      </table></td>
			    </tr>
			    <tr>
				  <td>
				  	<table border="0" cellpadding="1" cellspacing="1" bgcolor="#E5E5E5">
				      <tr>
				        <td bgcolor="#FFFFFF">
				        	<table border="0" cellspacing="0" cellpadding="0">
					            <tr>
					              <td valign="top" class="textbold">
					              
					              	<!-- Scheduled Tab: Start -->
					              	<logic:equal name="JobOwnerSummaryForm" property="tabName" value="scheduled">
					              	<table border='0' cellspacing='1' cellpadding='0' width="960px">
										<tr>
											<td rowspan='2' align='center' class='texth'>Job Title </td>
										    <td class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
										</tr>
										<tr>
											<td class='texth'>Planned Start Date/Time</td>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
										</tr>
										
										<logic:iterate id="list" name="JobOwnerSummaryForm" property="tabInfo">
											<bean:define id="appendix_id" name="list" property="appendix_id" />
											<bean:define id="site_number" name="list" property="site_number" />
											<%
											++count;
											if(tabSize == 201){
												if(count > 200){break;}
											}
											
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="list" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="list" property="appendix_title" type = "java.lang.String" />
											<bean:define id="project_type" name="list" property="project_type" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
												<tr>
													<td height='28' colspan='20' class='Rheading'>Project:&nbsp;<bean:write name="list" property="msa_title"/>,&nbsp;<bean:write name="list" property="appendix_title"/></td>
												</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
											<tr>
												<bean:define id="scheduledJobName" name="list" property="job_name" type="java.lang.String" ></bean:define>
													<%
														if(scheduledJobName.length()>48){
														scheduledJobName=scheduledJobName.substring(0,48);
											}
												%>
												<td class="<%=backgroundclass%>"><a href="JobDashboardAction.do?function=view&jobid=<bean:write name="list" property="job_id"/>&appendixid=<bean:write name="list" property="appendix_id"/>"><%=scheduledJobName %></a></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="planned_start_date"/>&nbsp;<bean:write name="list" property="planned_start_time"/></td>
											
											<%if( ((appendix_id).equals("1239") || appendix_id.equals("1352")) && !site_number.equals("")){%>
											<bean:define id="scheduledSiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<bean:define id="scheduledSiteName" name="list" property="site_name" type="java.lang.String" ></bean:define>
												<% scheduledSiteNumber=scheduledSiteNumber+scheduledSiteName;
												if(scheduledSiteNumber.length()>48)
												{
													scheduledSiteNumber=scheduledSiteNumber.substring(0,48);
												}
												%>
												<td  class="<%=backgroundclass%>"><%=scheduledSiteNumber %></td>
											<%}else{%>
											<bean:define id="scheduleSiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<%
											if(scheduleSiteNumber.length()>48){
												scheduleSiteNumber=scheduleSiteNumber.substring(0,48);
											}
												%>	
											
												<td class="<%=backgroundclass%>"><%=scheduleSiteNumber %></td>
											<%}%>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_state"/></td>
											</tr>
				
										</logic:iterate>
										<%
										if(count == 201){ // - when resultset has more then 200 record.%>
										<tr>
											<td class='Rheading' colspan='20'>This report was limited to the top 200 jobs.  Use the filter criteria to view additional jobs.</td>
										</tr>
										<%}%>
										<logic:empty name="JobOwnerSummaryForm" property="tabInfo">
										<tr>
											<td class='Rmessage' height='10' colspan='20'>No records match report criteria.</td>
										</tr>
										</logic:empty>
									</table>
					              	</logic:equal>
					          		<!-- Scheduled Tab: End -->
					          		
					          		<!-- Confirmed Tab: Start -->
					              	<logic:equal name="JobOwnerSummaryForm" property="tabName" value="confirmed">
					              	<table border='0' cellspacing='1' cellpadding='0' width="960px">
										<tr>
											<td rowspan='2' align='center' class='texth'>Job Title </td>
										    <td class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
										</tr>
										<tr>
											<td class='texth'>Planned Start Date/Time</td>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
										</tr>
										
										<logic:iterate id="list" name="JobOwnerSummaryForm" property="tabInfo">
											<bean:define id="appendix_id" name="list" property="appendix_id" />
											<bean:define id="site_number" name="list" property="site_number" />
											<%
											++count;
											if(tabSize == 201){
												if(count > 200){break;}
											}
											
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="list" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="list" property="appendix_title" type = "java.lang.String" />
											<bean:define id="project_type" name="list" property="project_type" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='20' class='Rheading'>Project:&nbsp;<bean:write name="list" property="msa_title"/>,&nbsp;<bean:write name="list" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
											<tr>
											<bean:define id="confirmedJobName" name="list" property="job_name" type="java.lang.String" ></bean:define>
											<%
											if(confirmedJobName.length()>48){
												confirmedJobName=confirmedJobName.substring(0,48);
											}
												%>
												<td class="<%=backgroundclass%>"><a href="JobDashboardAction.do?function=view&jobid=<bean:write name="list" property="job_id"/>&appendixid=<bean:write name="list" property="appendix_id"/>"><%=confirmedJobName %></a></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="planned_start_date"/>&nbsp;<bean:write name="list" property="planned_start_time"/></td>
											<%if( ((appendix_id).equals("1239") || appendix_id.equals("1352")) && !site_number.equals("")){%>
											<bean:define id="confirmedSiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<bean:define id="confirmedSiteName" name="list" property="site_name" type="java.lang.String" ></bean:define>
												<% confirmedSiteNumber=confirmedSiteNumber+confirmedSiteName;
												if(confirmedSiteNumber.length()>48)
												{
													confirmedSiteNumber=confirmedSiteNumber.substring(0,48);
												}
												%>
												<td  class="<%=backgroundclass%>"><%=confirmedSiteNumber %></td>
											<%}else{%>
											<bean:define id="confirmSiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<%
											if(confirmSiteNumber.length()>48){
												confirmSiteNumber=confirmSiteNumber.substring(0,48);
											}
												%>	
											
												<td class="<%=backgroundclass%>"><%=confirmSiteNumber %></td>
											<%}%>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_state"/></td>
										</logic:iterate>
										<%
										if(count == 201){ // - when resultset has more then 200 record.%>
										<tr>
											<td class='Rheading' colspan='20'>This report was limited to the top 200 jobs.  Use the filter criteria to view additional jobs.</td>
										</tr>
										<%}%>
										<logic:empty name="JobOwnerSummaryForm" property="tabInfo">
										<tr>
											<td class='Rmessage' height='10' colspan='20'>No records match report criteria.</td>
										</tr>
										</logic:empty>
									</table>
					              	</logic:equal>
					          		<!-- Confirmed Tab: End -->
					          		
					          		<!-- OnSite Tab: Start -->
					          		<logic:equal name="JobOwnerSummaryForm" property="tabName" value="onsite">
					          		<table border='0' cellspacing='1' cellpadding='0' width="960px">
										<tr>
											<td rowspan='2' align='center' class='texth'>Job Title</td>
											<td colspan='2' class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
<!--											<td rowspan='2' class='texth'>Actions</td>-->
										</tr>
										<tr>
											<td class='texth'>Planned Start Date/Time</td>
											<td class='texth'>Actual Start Date/Time</td>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
										</tr>
										
										<logic:iterate id="list" name="JobOwnerSummaryForm" property="tabInfo">
											<bean:define id="appendix_id" name="list" property="appendix_id" />
											<bean:define id="site_number" name="list" property="site_number" />
											<%
											++count;
											if(tabSize == 201){
												if(count > 200){break;}
											}
											
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="list" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="list" property="appendix_title" type = "java.lang.String" />
											<bean:define id="project_type" name="list" property="project_type" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='20' class='Rheading'>Project:&nbsp;<bean:write name="list" property="msa_title"/>,&nbsp;<bean:write name="list" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
											<tr>
											<bean:define id="onsiteJobName" name="list" property="job_name" type="java.lang.String" ></bean:define>
											<%
											if(onsiteJobName.length()>48){
												onsiteJobName=onsiteJobName.substring(0,48);
											}
												%>
												<td class="<%=backgroundclass%>"><a href="JobDashboardAction.do?function=view&jobid=<bean:write name="list" property="job_id"/>&appendixid=<bean:write name="list" property="appendix_id"/>"><%=onsiteJobName %></a></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="planned_start_date"/>&nbsp;<bean:write name="list" property="planned_start_time"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="actual_start_date"/>&nbsp;<bean:write name="list" property="actual_start_time"/></td>
											<%if( ((appendix_id).equals("1239") || appendix_id.equals("1352")) && !site_number.equals("")){%>
											<bean:define id="onSiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<bean:define id="onsiteSiteName" name="list" property="site_name" type="java.lang.String" ></bean:define>
												<% onSiteNumber=onSiteNumber+onsiteSiteName;
												if(onSiteNumber.length()>48)
												{
													onSiteNumber=onSiteNumber.substring(0,48);
												}
												%>
												<td  class="<%=backgroundclass%>"><%=onSiteNumber %></td>
											<%}else{%>
											<bean:define id="onsiteSiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<%
											if(onsiteSiteNumber.length()>48){
												onsiteSiteNumber=onsiteSiteNumber.substring(0,48);
											}
												%>	
											
												<td class="<%=backgroundclass%>"><%=onsiteSiteNumber %></td>
											<%}%>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_state"/></td>
											</tr>
										</logic:iterate>
										<%
										if(count == 201){ // - when resultset has more then 200 record.%>
										<tr>
											<td class='Rheading' colspan='20'>This report was limited to the top 200 jobs.  Use the filter criteria to view additional jobs.</td>
										</tr>
										<%}%>
										<logic:empty name="JobOwnerSummaryForm" property="tabInfo">
										<tr>
											<td class='Rmessage' height='10' colspan='20'>No records match report criteria.</td>
										</tr>
										</logic:empty>
									</table>
					          		</logic:equal>
					          		<!-- OnSite Tab: End -->
					          		
					          		<!-- OffSite Tab: Start -->
					          		<logic:equal name="JobOwnerSummaryForm" property="tabName" value="offsite">
					          		<table border='0' cellspacing='1' cellpadding='0' width="960px">
								        <tr>
											<td rowspan='2' align='center' class='texth'>Job Title</td>
											<td colspan='3' class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
									   	</tr>
									   	<tr>
											<td class='texth'>Actual Start Date/Time</td>
											<td class='texth'>Actual End Date/Time</td>
											<td class='texth'>Time on Site</td>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
									  	</tr>
									  	
									  	<logic:iterate id="list" name="JobOwnerSummaryForm" property="tabInfo">
											<bean:define id="appendix_id" name="list" property="appendix_id" />
											<bean:define id="site_number" name="list" property="site_number" />
											<%
											++count;
											if(tabSize == 201){
												if(count > 200){break;}
											}
											
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="list" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="list" property="appendix_title" type = "java.lang.String" />
											<bean:define id="project_type" name="list" property="project_type" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='20' class='Rheading'>Project:&nbsp;<bean:write name="list" property="msa_title"/>,&nbsp;<bean:write name="list" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
									  		<tr>
									  		<bean:define id="offsiteJobName" name="list" property="job_name" type="java.lang.String" ></bean:define>
											<%
											if(offsiteJobName.length()>48){
												offsiteJobName=offsiteJobName.substring(0,48);
											}
												%>
										        <td class="<%=backgroundclass%>"><a href="JobDashboardAction.do?function=view&jobid=<bean:write name="list" property="job_id"/>&appendixid=<bean:write name="list" property="appendix_id"/>"><%=offsiteJobName %></a></td>
										        <td class="<%=backgroundclass%>"><bean:write name="list" property="actual_start_date"/>&nbsp;<bean:write name="list" property="actual_start_time"/></td>
										        <td class="<%=backgroundclass%>"><bean:write name="list" property="actual_end_date"/>&nbsp;<bean:write name="list" property="actual_end_time"/></td>
										        <td class="<%=backgroundclass%>"><bean:write name="list" property="time_on"/></td>
											
											<%if( ((appendix_id).equals("1239") || appendix_id.equals("1352")) && !site_number.equals("")){%>
											<bean:define id="offsiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<bean:define id="offsiteSiteName" name="list" property="site_name" type="java.lang.String" ></bean:define>
												<% offsiteNumber=offsiteNumber+offsiteSiteName;
												if(offsiteNumber.length()>48)
												{
													offsiteNumber=offsiteNumber.substring(0,48);
												}
												%>
												<td  class="<%=backgroundclass%>"><%=offsiteNumber %></td>
											<%}else{%>
											<bean:define id="offsiteSiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<%
											if(offsiteSiteNumber.length()>48){
												offsiteSiteNumber=offsiteSiteNumber.substring(0,48);
											}
												%>	
											
												<td class="<%=backgroundclass%>"><%=offsiteSiteNumber %></td>
											<%}%>
												<td  class="<%=backgroundclass%>"><bean:write name="list" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_state"/></td>
								        	</tr>
								        </logic:iterate>
								        <%
										if(count == 201){ // - when resultset has more then 200 record.%>
										<tr>
											<td class='Rheading' colspan='20'>This report was limited to the top 200 jobs.  Use the filter criteria to view additional jobs.</td>
										</tr>
										<%}%>
								        <logic:empty name="JobOwnerSummaryForm" property="tabInfo">
										<tr>
											<td class='Rmessage' height='10' colspan='20'>No records match report criteria.</td>
										</tr>
										</logic:empty>
									 </table>
					          		</logic:equal>
					          		<!-- OffSite Tab: End -->
					          		
					          		<!-- Complete Tab: Start -->
					          		<logic:equal name="JobOwnerSummaryForm" property="tabName" value="complete">
					          		<table border='0' cellspacing='1' cellpadding='0'width="960px">
										<tr>
											<td rowspan='2' align='center' class='texth'>Job Title</td>
											<td colspan='3' class='texth'>Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
											<td colspan='1' class='texth'>Financial Information</td>
										</tr>
										<tr>
											<td class='texth'>Actual Start Date/Time</td>
											<td class='texth'>Actual End Date/Time</td>
											<td class='texth'>Time on Site</td>
											<td class='texth'>Site Number </td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
											<td class='texth'>Invoice Price</td>
										</tr>
										
										<logic:iterate id="list" name="JobOwnerSummaryForm" property="tabInfo">
											<bean:define id="appendix_id" name="list" property="appendix_id" />
											<bean:define id="site_number" name="list" property="site_number" />
											<%
											++count;
											if(tabSize == 201){
												if(count > 200){break;}
											}
											
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="list" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="list" property="appendix_title" type = "java.lang.String" />
											<bean:define id="project_type" name="list" property="project_type" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='20' class='Rheading'>Project:&nbsp;<bean:write name="list" property="msa_title"/>,&nbsp;<bean:write name="list" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
											<tr>
											<bean:define id="completeJobName" name="list" property="job_name" type="java.lang.String" ></bean:define>
											<%
											if(completeJobName.length()>48){
												completeJobName=completeJobName.substring(0,48);
											}
												%>
												<td class="<%=backgroundclass%>"><a href="JobDashboardAction.do?function=view&jobid=<bean:write name="list" property="job_id"/>&appendixid=<bean:write name="list" property="appendix_id"/>"><%=completeJobName %></a></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="actual_start_date"/>&nbsp;<bean:write name="list" property="actual_start_time"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="actual_end_date"/>&nbsp;<bean:write name="list" property="actual_end_time"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="time_on"/></td>
											<%if( ((appendix_id).equals("1239") || appendix_id.equals("1352")) && !site_number.equals("")){%>
											<bean:define id="completedsiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<bean:define id="completedsiteSiteName" name="list" property="site_name" type="java.lang.String" ></bean:define>
												<% completedsiteNumber=completedsiteNumber+completedsiteSiteName;
												if(completedsiteNumber.length()>48)
												{
													completedsiteNumber=completedsiteNumber.substring(0,48);
												}
												%>
												<td  class="<%=backgroundclass%>"><%=completedsiteNumber %></td>
											<%}else{%>
											<bean:define id="completeSiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<%
											if(completeSiteNumber.length()>48){
												completeSiteNumber=completeSiteNumber.substring(0,48);
											}
												%>	
												<td class="<%=backgroundclass%>"><%=completeSiteNumber %></td>
											<%}%>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_state"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="invoiced_price"/></td>
											</tr>
										</logic:iterate>
										<%
										if(count == 201){ // - when resultset has more then 200 record.%>
										<tr>
											<td class='Rheading' colspan='20'>This report was limited to the top 200 jobs.  Use the filter criteria to view additional jobs.</td>
										</tr>
										<%}%>
										<logic:empty name="JobOwnerSummaryForm" property="tabInfo">
										<tr>
											<td class='Rmessage' height='10' colspan='20'>No records match report criteria.</td>
										</tr>
										</logic:empty>
										
									</table>
					          		</logic:equal>
					          		<!-- Complete Tab: End -->
					          		
					          		<!-- NotScheduled Tab: Start -->
					          		<logic:equal name="JobOwnerSummaryForm" property="tabName" value="notscheduled">
					          		<table border='0' cellspacing='1' cellpadding='0' width="960px">
										<tr>
											<td rowspan='2' align='center' class='texth'>Job Title </td>
											<td rowspan='2' class='texth'>Requested Schedule</td>
											<td colspan='3' class='texth'>Site Information</td>
										</tr>
										<tr>
											<td class='texth'>Site Number </td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
										</tr>
										
										<logic:iterate id="list" name="JobOwnerSummaryForm" property="tabInfo">
											<bean:define id="appendix_id" name="list" property="appendix_id" />
											<bean:define id="site_number" name="list" property="site_number" />
											<%
											++count;
											if(tabSize == 201){
												if(count > 200){break;}
											}
											
											
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="msa_title" name="list" property="msa_title" type = "java.lang.String" />
											<bean:define id="appendix_title" name="list" property="appendix_title" type = "java.lang.String" />
											<bean:define id="project_type" name="list" property="project_type" type = "java.lang.String" />
											<%if(!temptitle.equals(msa_title+appendix_title)) {%>
											<tr>
												<td height='28' colspan='20' class='Rheading'>Project:&nbsp;<bean:write name="list" property="msa_title"/>,&nbsp;<bean:write name="list" property="appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = msa_title+appendix_title;%>
											<tr>
											<bean:define id="notscheduledJobName" name="list" property="job_name" type="java.lang.String" ></bean:define>
											<%
											if(notscheduledJobName.length()>48){
												notscheduledJobName=notscheduledJobName.substring(0,48);
											}
												%>
												<td class="<%=backgroundclass%>"><a href="JobDashboardAction.do?function=view&jobid=<bean:write name="list" property="job_id"/>&appendixid=<bean:write name="list" property="appendix_id"/>"><%=notscheduledJobName %></a></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="requested_date"/></td>
												<%if( ((appendix_id).equals("1239") || appendix_id.equals("1352")) && !site_number.equals("")){%>
											<bean:define id="notscheduledsiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<bean:define id="notscheduledSiteName" name="list" property="site_name" type="java.lang.String" ></bean:define>
												<% notscheduledsiteNumber=notscheduledsiteNumber+notscheduledSiteName;
												if(notscheduledsiteNumber.length()>48)
												{
													notscheduledsiteNumber=notscheduledsiteNumber.substring(0,48);
												}
												%>
												<td  class="<%=backgroundclass%>"><%=notscheduledsiteNumber %></td>
											<%}else{%>
											<bean:define id="notschedulesiteNumber" name="list" property="site_number" type="java.lang.String" ></bean:define>
											<%
											if(notschedulesiteNumber.length()>48){
												notschedulesiteNumber=notschedulesiteNumber.substring(0,48);
											}
												%>	
												<td class="<%=backgroundclass%>"><%=notschedulesiteNumber %></td>
												<%} %>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="site_state"/></td>
											</tr>
										</logic:iterate>
										<%
										if(count == 201){ // - when resultset has more then 200 record.%>
										<tr>
											<td class='Rheading' colspan='20'>This report was limited to the top 200 jobs.  Use the filter criteria to view additional jobs.</td>
										</tr>
										<%}%>
										<logic:empty name="JobOwnerSummaryForm" property="tabInfo">
										<tr>
											<td class='Rmessage' height='10' colspan='20'>No records match report criteria.</td>
										</tr>
										</logic:empty>
									</table>
					          		</logic:equal>
					          		<!-- NotScheduled Tab: End -->
					          		
					          		<!-- CCC Tab: Start -->
					          		<logic:equal name="JobOwnerSummaryForm" property="tabName" value="ccc">
					          		<table border='0' cellspacing='1' cellpadding='0' width="960px">
										<tr>
											<td class='texth'>Site Number</td>
											<td class='texth'>City</td>
											<td class='texth'>State</td>
											<td class='texth'>Status</td>
											<td class='texth'>Scheduled</td>
											<td class='texth'>Onsite</td>
											<td class='texth'>Offsite</td>
											<td class='texth'>Complete</td>
											<td class='texth'>Invoiced</td>
											<td class='texth'>Invoice Number</td>
										</tr>
										
										<logic:iterate id="list" name="JobOwnerSummaryForm" property="tabInfo">
											<bean:define id="appendix_id" name="list" property="ms_appendix_id" />
											<bean:define id="site_number" name="list" property="ms_site_number" />
											<bean:define id="site_state" name="list" property="ms_site_state" />
											<%
											++count;
											if(count > 200){break;}
											
											if ( csschooser == true ) 
											{
												backgroundclass = "Rtexto";
												csschooser = false;
											}
											else
											{
												csschooser = true;	
												backgroundclass = "Rtexte";
											}
											%>
											<bean:define id="title" name="list" property="msa_appendix_title" type = "java.lang.String" />
											<%if(!temptitle.equals(title)) {%>
											<tr>
												<td height='28' colspan='20' class='Rheading'>Project:&nbsp;<bean:write name="list" property="msa_appendix_title"/></td>
											</tr>
											<%} %>
											<% temptitle = title;%>
											<tr>
												<%if((appendix_id.equals("1239") || appendix_id.equals("1352")) && !site_number.equals("")){%>
													<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_site_number"/></td>
												<%}else{%>
													<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_site_number"/></td>
												<%}%>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_site_city"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_site_state"/></td>
												<%if(site_state.equals("Closed")){%>
													<td class="<%=backgroundclass%>">Invoiced</td>
												<%}else{%>
													<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_job_status"/></td>
												<%}%>
												
												<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_scheduled_date"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_onsite_date"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_offsite_date"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_job_complete_date"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_job_invoiced_date"/></td>
												<td class="<%=backgroundclass%>"><bean:write name="list" property="ms_job_invoiced_no"/></td>
											</tr>
										</logic:iterate>
										<%
										if(count > 200){ // - when resultset has more then 200 record.%>
										<tr>
											<td class='Rheading' colspan='20'>This report was limited to the top 200 jobs.  Use the filter criteria to view additional jobs.</td>
										</tr>
										<%}%>
										<logic:empty name="JobOwnerSummaryForm" property="tabInfo">
										<tr>
											<td class='Rmessage' height='10' colspan='20'>No records match report criteria.</td>
										</tr>
										</logic:empty>
									</table>
					          		</logic:equal>
					          		<!-- CCC Tab: End -->
					          		
					          		<!-- calendar Tab: Start -->
					          		<logic:equal name="JobOwnerSummaryForm" property="tabName" value="calendar">
					          		<table border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
						                <!-- Name of Month: Start -->
						                <tr class="year">
						                	<td height="25" align="center" bgcolor="#DDE2CC" colspan="7"><bean:write name="JobOwnerSummaryForm" property="currMMYY" /></td>
						                </tr>
						                <!-- Name of Month: End -->
						                <!-- Days Header Row: Start -->
						                <tr>
						                     <td width="150" align="center" bgcolor="#EFF1E6" class="days">Monday</td>
						                     <td width="150" align="center" bgcolor="#EFF1E6" class="days">Tuesday</td>
						                     <td width="150" align="center" bgcolor="#EFF1E6" class="days">Wednesday</td>
						                     <td width="150" align="center" bgcolor="#EFF1E6" class="days">Thursday</td>
						                     <td width="150" align="center" bgcolor="#EFF1E6" class="days">Friday</td>
						                     <td width="150" align="center" bgcolor="#EFF1E6" class="days">Saturday</td>
						                     <td width="150" align="center" bgcolor="#EFF1E6" class="days">Sunday</td>
					                   	 </tr>
					                   	 <!-- Days Header Row: End -->
					                   	 
										 <%	TreeMap calRow = (TreeMap)request.getAttribute("calendardata"); // - calendar info. Arranged as 5 weeks and every week has 7 days
										 	String[] dates = (String[])request.getAttribute("dates");		// - calendar dates
											String[] daterange = (String[])request.getAttribute("daterange");// - calendar Dates range
											ArrayList list = (ArrayList)request.getAttribute("list");		// - Data For Fill calendar
											TreeMap rowColumn = new TreeMap();	// - To store day of weeks
											//- Variables
											Integer wk; int count1 = -1; String dd = ""; String temp1 =""; String temp2 =""; Integer count2;
									        Iterator itData; Iterator itCol; Iterator itRow; String bgcolor ="";
									        
											itRow = calRow.keySet().iterator();
											
									        while(itRow.hasNext()){ //- calendar Creation: Start
												wk = (Integer)itRow.next(); // - Store weeks one by one
												rowColumn = (TreeMap)calRow.get(wk); // - Days of Every Week
												itCol = rowColumn.keySet().iterator(); // - Keys of Every Week%> 
												<!-- Date Row: Start -->
												<tr>
												<%for(int i1 =0; i1<7; i1++){ // - every Date Row has 7 column
													temp1 = dates[++count1]; temp2 = temp1;
													if( count1!=0 ){ temp2 = temp1.substring(3);}
													if( temp1.substring(3).trim().equals("01") ){temp2 = temp1;}%>
													<!-- Column of Date Row: Start -->
					                   	 	 		<td bgcolor="#E6EDF7" class="dates"><%=temp2%></td>
					                   	 	 		<!-- Column of Date Row: End -->
						                   	 	<%}// - end of for loop%>
						                   	 	</tr>
						                   	 	<!-- Date Row: Start -->
						                   	 	<!-- calendar Data Row: Start -->
									            <tr>
									            <%while(itCol.hasNext()){ // - every calendar Data Row has 7 column
													 count2 = (Integer)itCol.next();
													 dd = rowColumn.get(count2).toString();
													 itData = list.iterator();
													 bgcolor ="#E8E8E6";
													 for(int i2=0;i2<30;i2++){ if(dd.equals(daterange[i2])){ bgcolor="#FEFEFE"; } }//- Fill Color of calendar Data Column%>
													 <!-- Column of calendar Data Row: Start  -->
											         <td height="55" bgcolor="<%=bgcolor%>" class="data" nowrap="nowrap" valign="top">
											         <%while(itData.hasNext()){ // - For fill Data Column
														 CalendarData calData = (CalendarData)itData.next();
														 /* Compare calendar column date with all calendar data */
														 if(dd.equals(calData.getDate_list())){ // - Fill calendar Data column%>
														 <%
														 if(calData.getSite_number().length()>14)
															 calData.setSite_number((calData.getSite_number().substring(0,14)));
														 %>
														 	<%if(calData.getProject_type().equalsIgnoreCase("NetMedX")) { // - for NetMedX type%>
														 	<a href = "JobDashboardAction.do?function=view&jobid=<%=calData.getJob_id()%>&appendixid=<%=calData.getAppendix_id()%>"><%=calData.getSite_number()%></a>&nbsp;<%=calData.getStatus()%>:&nbsp;<a href = "JobDashboardAction.do?type=8&isClicked=7&ticketType=existingTicket&ticket_id=<%=calData.getTicket_id() %>&Job_Id=<%=calData.getJob_id() %>&appendix_Id=<%=calData.getAppendix_id()%>&from=RPT"><%=calData.getPlanned_start_time()%></a><br>
														 	<%}else{ // - for Appendix type%>
														 	<a href="JobDashboardAction.do?function=view&jobid=<%=calData.getJob_id()%>&appendixid=<%=calData.getAppendix_id()%>"><%=calData.getSite_number()%></a>&nbsp;<%=calData.getStatus()%>:&nbsp;<a href = "JobDashboardAction.do?type=7&isClicked=6&jobid=<%=calData.getJob_id() %>&appendixid=<%=calData.getAppendix_id()%>&from=RPT"><%=calData.getPlanned_start_time()%></a><br>
														 	<%} %>
														<%}else{}// - End of if Block%>
										             <%}//- End of While loop%>
										            </td>
										            <!-- Column of calendar Data Row: End  -->
									            <%}%>
											    </tr>
											    <!-- calendar Data Row: End -->
										    <%} //- calendar Creation: End%>
								 		</table>
					          		
					          		</logic:equal>
					          		<!-- calendar Tab: End -->
					          		
				        		</td>
				      		</tr>
				  		</table>
				 	</td>
			    </tr>
		  </table>
	  </td>
  </tr>
</table>
</html:form>
<!-- Form: End -->
</body>
</html:html>
<script type="text/javascript">
<!--

function validateAll(x){ 
// - Fire on click of Go button
// - For validate and submit form
<%if(!tabname.equals("calendar")){%>
	if( document.forms[0].toDate.value  == "" && document.forms[0].fromDate.value  != "" ){ // - when To Date is blank but From Date is not blank
		alert('Select To Date for Search');
		document.forms[0].toDate.focus();
		return false;
	}
	if( document.forms[0].toDate.value  != "" && document.forms[0].fromDate.value  == "" ){ // - when To Date is not blank and From Date is blanak
		alert('Select From Date for Search');
		document.forms[0].fromDate.focus();
		return false;
	}
	
	if( document.forms[0].toDate.value  != "" && document.forms[0].fromDate.value  != "" ){ // - when both To Date and From Date is not blank
	
		if ( !compDate_mdy(document.forms[0].fromDate.value, document.forms[0].toDate.value) ){ // - Compare To Date and From Date
		
			alert("To Date Should be greater than From Date");
			return false;
		} 
	}
<%}%>

	submitform(x);
}
function submitform(x){

<%if(!tabname.equals("calendar")){%>
	if( document.forms[0].toDate.value  == ""){ // - when To Date is blank but From Date is not blank
		document.forms[0].fromDate.value = "";
	}
	if( document.forms[0].fromDate.value  == "" ){ // - when To Date is not blank and From Date is blanak
		document.forms[0].toDate.value = ""
	}
<%}%>
	document.forms[0].action = "JobOwnerSummary.do?tabname="+x; // set tab name into request patameter
	document.forms[0].tabName.value = x;	// - enable To Date textBox
	document.forms[0].method= 'get';
	document.forms[0].submit(); // - Form submit
	
}


//-->
</script>
