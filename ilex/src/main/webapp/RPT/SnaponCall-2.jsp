<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%
	String call1Status = "";
	String call12Status = "";
	boolean seShow = true;
	boolean thShow = true;
	String occurance = (String)request.getParameter("occurance");
	
	/* Set boolean vairiable for show and hide call sections. */
	if(occurance.equals("1T")){ seShow = false; thShow = false; }
	else if(occurance.equals("2T")){ thShow = false; }

	if(request.getAttribute("call1Status") != null){ call1Status = (String)request.getAttribute("call1Status"); }
	if(request.getAttribute("call12Status") != null){ call12Status = (String)request.getAttribute("call12Status"); }
	
	/* Set boolean vairiable for show and hide call sections. */
	if(call1Status.equals("Failed") && call12Status.equals("Failed")){ thShow = false; }
%>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<td>&nbsp;&nbsp;</td>
	<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="Rlabeleboldwhite">Dealer Name:&nbsp;&nbsp;</td>
				<td class="RtextwhiteFont" colspan="3"><bean:write name="SnaponCallForm" property="dealerName"/></td>
			</tr>
			<tr height="10">
				<td></td>
			</tr>
			<tr>
				<td class="Rlabeleboldwhite">Primary Contact:&nbsp;&nbsp;</td>
				<td class="RtextwhiteFont" colspan="3"><bean:write name="SnaponCallForm" property="priContact"/>&nbsp;<bean:write name="SnaponCallForm" property="pirPhone"/></td>
			</tr>
			<tr height="10">
				<td></td>
			</tr>
			<tr>
				<td class="Rlabeleboldwhite">5 Day (1):&nbsp;&nbsp;</td>
				<td class="Rlabeleboldwhite">
					<html:select name="SnaponCallForm" property = "call1Status" styleClass = "select" disabled="true">
						<html:optionsCollection name = "SnaponCallForm" property = "day5Call1" value = "value" label = "label"/> 
					</html:select>	
				</td>
				<td class="RtextwhiteFont">&nbsp;&nbsp;&nbsp;&nbsp;Contact:&nbsp;</td>
				<td><html:text  name  ="SnaponCallForm" styleClass = "textbox" size = "60" property = "call1Contact" readonly="true"/></td>
			</tr>
			
			<%if(seShow){ %>
			<tr height="10">
				<td></td>
			</tr>
			<tr>
				<td class="Rlabeleboldwhite">4 Day (2):&nbsp;&nbsp;</td>
				<td class="RtextwhiteFont">
					<html:select name="SnaponCallForm" property = "call12Status" styleClass = "select" disabled="true">
						<html:optionsCollection name = "SnaponCallForm" property = "day5Call2" value = "value" label = "label"/> 
					</html:select>	
				</td>
				<td class="RtextwhiteFont">&nbsp;&nbsp;&nbsp;&nbsp;Contact:&nbsp;</td>
				<td><html:text  name  ="SnaponCallForm" styleClass = "textbox" size = "60" property = "call12Contact" readonly="true"/></td>
			</tr>
			<%} %>
			
			<%if(thShow){ %>
			<tr height="10">
				<td></td>
			</tr>
			<tr>
				<td class="Rlabeleboldwhite">24 Hour Call:&nbsp;&nbsp;</td>
				<td class="Rlabeleboldwhite">
					<html:select name="SnaponCallForm" property = "call2Status" styleClass = "select" disabled="true">
						<html:optionsCollection name = "SnaponCallForm" property = "hour24Call" value = "value" label = "label"/> 
					</html:select>
				</td>
				<td class="RtextwhiteFont">&nbsp;&nbsp;&nbsp;&nbsp;Contact:&nbsp;</td>
				<td><html:text  name  ="SnaponCallForm" styleClass = "textbox" size = "60" property = "call2Contact" readonly="true"/></td>
			</tr>
			<%} %>
			<tr height="8">
				<td></td>
			</tr>
			<tr>
				<td class="Rlabeleboldwhite">Escalate:&nbsp;&nbsp;</td>
				<td class="RtextwhiteFont">
					<html:radio name ="SnaponCallForm" property ="escalate" value ="Y" onclick="setEscalate();">Yes</html:radio>
					<html:radio name ="SnaponCallForm" property ="escalate" value ="N" onclick="setEscalate();">No</html:radio>
				</td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr height="10">
				<td></td>
			</tr>
			<tr>
				<td class="Rlabeleboldwhite">Reschedule:&nbsp;&nbsp;</td>
				<td class="RtextwhiteFont">
					<html:radio name ="SnaponCallForm" property ="reschedule" value ="Y" onclick="setReschedule();">Yes</html:radio>
					<html:radio name ="SnaponCallForm" property ="reschedule" value ="N" onclick="setReschedule();">No</html:radio>
				</td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr height="10">
				<td></td>
			</tr>
			<tr>
				<td colspan="4" align="center">
					<html:submit property = "save" styleClass = "Rbutton" onclick="vailidate();">Save</html:submit>&nbsp;
					<html:reset property = "reset" styleClass = "Rbutton" onclick="">Reset</html:reset>&nbsp;
					<html:button property = "back" styleClass = "Rbutton" onclick="history.go(-1);">Back</html:button>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
<script>
function vailidate(){
	try{
		document.forms[0].call1Status.disabled = false;
		document.forms[0].call12Status.disabled = false;
		document.forms[0].call2Status.disabled = false;
	}
	catch(e){
	}
}
</script>
