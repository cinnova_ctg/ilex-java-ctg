<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Display Reschedule Report info.
-->
<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<head>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel = "stylesheet" href = "RPT/rptstyle/rptstyle.css" type = "text/css">
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<title>Reschedule Report</title>
</head>
<%
	String temptitle = "";
	String backgroundclass = null; //- td style class
	boolean csschooser = true;		// - for alternet change color of rows
%>
<html:html>
<body>
<!-- Form: Start -->
<html:form action="/RescheduleReport.do">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td>
	<table cellpadding="0" cellspacing="0" border="0" width = "100%">
		<!-- BreadCrumb: Start -->
	    <tr><td  id="pop1" width="100%" class="Rheaderrow" height="19" colspan="2">&nbsp;</td></tr>
	    <tr>
	    	<td background="images/content_head_04.jpg" height="21" colspan="2" class="RbreadCrumb">&nbsp;<bean:message bundle="RPT" key="rpt.support.breadcrumb"/></td>
	   	</tr>
	   	<!-- BreadCrumb: End -->
	    <tr><td colspan="2"><h2>Reschedule Report</h2></td></tr>
	    <tr>
			<td colspan="2" style="padding-left: 8px">
				<table cellpadding="1" cellspacing="0" border="0" align="left">
					<tr>
						<td class="Rlabeleboldwhite">Project:&nbsp;
						<!-- Project Combobox: Start -->
				    		<html:select name="RescheduleReportForm" property = "appendixId" styleClass = "select">
				    			<logic:iterate id="msalist" name="RescheduleReportForm" property="msaList">
				    				<logic:notEqual name="msalist" property="msaName" value="notShow">
				    					<optgroup class=labelebold label="<bean:write name = "msalist" property = "msaName" />">
				    				</logic:notEqual>
									<html:optionsCollection name = "msalist" property = "appendixList" value = "value" label = "label"/> 
									<logic:notEqual name="msalist" property="msaName" value="notShow">
				    					</optgroup>
				    				</logic:notEqual>
								</logic:iterate>
							</html:select>
				        	<!-- Project Combobox: End -->	
						</td>
						<!-- Go Button: Start -->
						<td class="Rlabeleboldwhite">&nbsp;&nbsp;</td>
						<td class="Rlabeleboldwhite">
		 					<html:submit property="go" styleClass="Rbutton_c" onclick="">Go</html:submit>
						</td>
						<!-- Go Button: End -->
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>&nbsp;&nbsp;</td>
		<td>
			<table cellpadding="0" cellspacing="1" border="0">
				<!-- Column header: Start -->
				<tr height="19"> 
				    <td class = "texth">Week</td>
				    <td class = "texth">Executed #</td>
				    <td class = "texth">Rescheduled #</td>
				    <td class = "texth">Total #</td>
				    <td class = "texth">Reschedule Rate (%)</td>
				    <!--<td class = "texth">Billable Reschedules</td>-->
		   		</tr>
		   		<!-- Column header: End -->
		   		<!-- List of Reschedule Report: Start -->
		   		<logic:iterate id="rescheduleList" name="RescheduleReportForm" property="rescheduleList">
		   		<%	
				if ( csschooser == true ) 
				{
					backgroundclass = "Rtexto";
					csschooser = false;
				}
		
				else
				{
					csschooser = true;	
					backgroundclass = "Rtexte";
				}
			%>
				<bean:define id="inwork" name="rescheduleList" property="inwork" type = "java.lang.String" />
				<%
				if(Integer.parseInt(inwork) > 0){
					backgroundclass = "RtextRed";
				}
				%>
				<bean:define id="msa_title" name="rescheduleList" property="msaTitle" type = "java.lang.String" />
				<bean:define id="appendix_title" name="rescheduleList" property="appendixTitle" type = "java.lang.String" />
				<%if(!temptitle.equals(msa_title+appendix_title)) {%>
				<tr>
					<td height='28' colspan='20' class='Rheading'>Project:&nbsp;<bean:write name="rescheduleList" property="msaTitle"/>,&nbsp;<bean:write name="rescheduleList" property="appendixTitle"/></td>
				</tr>
				<%} %>
				<% temptitle = msa_title+appendix_title;%>
		   		<tr>
			   		<td class = "<%= backgroundclass %>" valign="top" align="center"><bean:write name="rescheduleList" property="monDate"/></td>
			   		<td class = "<%= backgroundclass %>" valign="top" align="right"><bean:write name="rescheduleList" property="executed"/></td>
			   		<td class = "<%= backgroundclass %>" valign="top" align="right"><bean:write name="rescheduleList" property="rescheduled"/></td>
			   		<td class = "<%= backgroundclass %>" valign="top" align="right"><bean:write name="rescheduleList" property="total"/></td>
			   		<td class = "<%= backgroundclass %>" valign="top" align="right"><bean:write name="rescheduleList" property="rescheduleRate"/>%</td>
					<!--<td class = "<%= backgroundclass %>" valign="top"><bean:write name="rescheduleList" property="billableRschedules"/></td>-->
		   		</tr>
		   		</logic:iterate>
		   		<!-- List of Reschedule Report: End -->
		   		
				<logic:empty name="RescheduleReportForm" property="rescheduleList" >
					  <!-- When Support Reschedule has no data  -->
					  <tr>
				  			<td class="Rmessage" height="10" colspan="7"><bean:message bundle="RPT" key="rpt.accounting.message"/></td>
					  </tr>
				</logic:empty>
			</table>
		</td>
	</tr>
	</table>
</td>
</tr>
</table>
</html:form>
</body>
</html:html>
