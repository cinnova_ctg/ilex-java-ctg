<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: Generating left frame
*
-->

<!DOCTYPE HTML>


<html:html>

	<head>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href = "RPT/rptstyle/rptstyle.css" type = "text/css">
	<title>Hierarchy Navigation</title>
	<style type="text/css">
		body {font-family : "Arial"; font-size : 10px; cursor : auto; color : #000000; padding : 0px 0px 0px 10px; white-space : nowrap;}
		a {text-decoration : none;	color : #000000;}
		a:hover {text-decoration: underline;}
	</style>
	
	<script language="JavaScript">
		//cache 2 arrow images
		imageUp = new Image();
		imageUp.src = "RPT/rptimages/Expand.gif";
		imageDown = new Image();
		imageDown.src = "RPT/rptimages/Collapse.gif";
		
		var cnt;										// for doing the loop
		var objSpanCollection;							// store a collecion of Menu
		var menuHeightCollection = new Array(); 		// store a collection of Menulists' height
		var objMenu;									// the menu is clicked on
		
		function InitializeMenu() 
		{
			// get a collection of menus
			objSpanCollection = document.body.getElementsByTagName("SPAN");
			for (var i = 0; i < objSpanCollection.length; i++)
			{
				var objSpan = objSpanCollection(i);
				// get a collection of Menus' height
				menuHeightCollection[i] = objSpan.childNodes.item(1).clientHeight;
				// assign the click event to every Menuheader
				objSpan.childNodes.item(0).onclick = ControlMenu;
			}
		}
		
		function ControlMenu() 
		{
			cnt = 1;
			objMenu = this.parentNode.childNodes.item(1);	// memorize the Menulist has been clicked
			
			// Get the arrow that belongs to the clicked menu
			// starting with <div> then <table> then <tbody> then <tr> then <td> and the last one is
			// what we need: <img>
			var objArrow = this.childNodes(0).childNodes(0).childNodes(0).childNodes(2).childNodes(0);
			
			if (objMenu.style.display == "none")
			{
				objArrow.src = imageDown.src;  //change to the Down Arrow
				ShowMenu();
			}
			else
			{
				objArrow.src = imageUp.src;  //change to the Up Arrow
				HideMenu();
			}
		}
		
		function ShowMenu()
		{	
			var objList = objMenu.childNodes.item(0);	// get the Linkslist of the Menulist
			if (cnt < 10)
			{
				// display the Menulist
				objMenu.style.display = "block";
				// increase the tranparency of the Menulist
				objMenu.filters["alpha"].opacity = objMenu.filters["alpha"].opacity + 30;
				// loop through the Menu collection to find the position of the clicked Menu
				// to get the actual height of the menu list and then increase the height of the Menulist
				for (var i = 0; i < objSpanCollection.length; i++)
					if (objMenu.parentNode == objSpanCollection[i])
						objMenu.style.height = objMenu.clientHeight + (menuHeightCollection[i]/10);
				cnt++;
				// do this function again after 30 miliseconds until the actual Menulist's height is returned
				//setTimeout("ShowMenu()",00)
				ShowMenu();
			}
			// display the Menulist if the it's actual height is returned
			if (cnt >= 10)
				objList.style.display = "block";  
		}
		
		function HideMenu()
		{	
			var objList = objMenu.childNodes.item(0);	// get the Linkslist of the Menulist
			if (cnt < 10)
			{
				objMenu.filters["alpha"].opacity = objMenu.filters["alpha"].opacity - 10;
				for (var i = 0; i < objSpanCollection.length; i++)
					if (objMenu.parentNode == objSpanCollection[i])
						objMenu.style.height = objMenu.clientHeight - (menuHeightCollection[i]/10);
				objList.style.display = "none";
				cnt++;
				//setTimeout("HideMenu()",00)
				HideMenu();
			}
			if (cnt >= 10)
				objMenu.style.display = "none";
		}
	</script>
	
	</head>

	<body onLoad="InitializeMenu()" background="images/sidebg.jpg">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td>&nbsp;</td>
	</tr>
	<tr>
	<td>
		<span>
			<div style="width:180;display: inherit">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="RPT/rptimages/left-cor.gif" border="0"></td>
						<td width="90%" class="RMenu_Head">Revenue</td>
						<td class="m2"><img src="RPT/rptimages/Expand.gif" border="0"></td>
						<td><img src="RPT/rptimages/right-cor.gif" border="0"></td>
					</tr>
				</table>
			</div>
			<div class="RMenu_Items" style="display: none;" >
				<div>
					<a  href="#" target="">Monthly by Status</a><br>
					<a  href="#" target="">Monthly by Account</a><br>
					<a  href="#" target="">Annual Progression</a><br>
				</div>
			</div>
		</span>
		
		<br>
		<span>
			<div style="width:180">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="RPT/rptimages/left-cor.gif" border="0"></td>
						<td width="90%" class="RMenu_Head">&nbsp;Management</td>
						<td class="m2"><img src="RPT/rptimages/Expand.gif" border="0"></td>
						<td><img src="RPT/rptimages/right-cor.gif" border="0"></td>
					</tr>
				</table>
			</div>
			<div class="RMenu_Items" style="display: none">
				<div>
					<a  href="./JobOwnerSummary.do?" target="ilexmain">Job Owner Summary</a><br>
					<a  href="#" target="">Real-Time Snapshot</a><br>
					<a  href="#" target="">Minuteman</a><br>
					<a  href="#" target="">Speedpay</a><br>
				</div>
			</div>
		</span>
		
		<br>
		<span>
			<div style="width:180">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="RPT/rptimages/left-cor.gif" border="0"></td>
						<td width="90%" class="RMenu_Head">&nbsp;Productivity</td>
						<td class="m2"><img src="RPT/rptimages/Expand.gif" border="0"></td>
						<td><img src="RPT/rptimages/right-cor.gif" border="0"></td>
					</tr>
				</table>
			</div>
			<div class="RMenu_Items" style="display: none">
				<div>
					<a  href="http://dw.contingent.local/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fProductivity_Headcount" target="ilexmain">Employees Assigned</a><br>
					<a  href="http://dw.contingent.local/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fProductivity_Revenue" target="ilexmain">Revenue by Employees</a><br>
					<a  href="http://dw.contingent.local/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fProductivity_Visits" target="ilexmain">Site Visits by Employees</a><br>
				</div>
			</div>
		</span>
		
		<br>
		<span>
			<div style="width:180">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="RPT/rptimages/left-cor.gif" border="0"></td>
						<td width="90%" class="RMenu_Head">&nbsp;Dispatch</td>
						<td class="m2"><img src="RPT/rptimages/Expand.gif" border="0"></td>
						<td><img src="RPT/rptimages/right-cor.gif" border="0"></td>
					</tr>
				</table>
			</div>
			<div class="RMenu_Items" style="display: none">
				<div>
					<a  href="#" target="">Dispatches by CCA</a><br>
					<a  href="#" target="">VGPM by CCA</a><br>
					<a  href="#" target="">Pricing by CCA</a><br>
				</div>
			</div>
		</span>
		
		<br>
		<span>
			<div style="width:180">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="RPT/rptimages/left-cor.gif" border="0"></td>
						<td width="90%" class="RMenu_Head">&nbsp;Accounting</td>
						<td class="m2"><img src="RPT/rptimages/Expand.gif" border="0"></td>
						<td><img src="RPT/rptimages/right-cor.gif" border="0"></td>
					</tr>
				</table>
			</div>
			<div class="RMenu_Items" style="display: none">
				<div>
					<a  href="./SalesSummary.do?" target="ilexmain">Ohio Sales</a><br>
				</div>
			</div>
		</span>
		
		<br>
		<span>
			<div style="width:180">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="RPT/rptimages/left-cor.gif" border="0"></td>
						<td width="90%" class="RMenu_Head">&nbsp;Out of Bounds</td>
						<td class="m2"><img src="RPT/rptimages/Expand.gif" border="0"></td>
						<td><img src="RPT/rptimages/right-cor.gif" border="0"></td>
					</tr>
				</table>
			</div>
			<div class="RMenu_Items" style="display: none">
				<div>
					<a  href="http://192.168.20.21/rpt/JSA.xo?form_action=jsa_summary" target="ilexmain">Job Status</a><br>
				</div>
			</div>
		</span>
		
		<br>
		<span>
			<div style="width:180">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="RPT/rptimages/left-cor.gif" border="0"></td>
						<td width="90%" class="RMenu_Head">&nbsp;Support</td>
						<td class="m2"><img src="RPT/rptimages/Expand.gif" border="0"></td>
						<td><img src="RPT/rptimages/right-cor.gif" border="0"></td>
					</tr>
				</table>
			</div>
			<div class="RMenu_Items" style="display: none">
				<div>
					<a  href="./SupportAppendixList.do?" target="ilexmain">IBM Corporation -> Appendix: Appendix 7 - Qwest CPE Installation Project</a><br>
					<a  href="./SupprortNetMedXList.do?" target="ilexmain">Levi's and Docker's Summary</a><br>
					<a  href="./RescheduleReport.do?" target="ilexmain">Reschedule Report</a><br>
					<a  href="./DailyBuzzReport.do?" target="ilexmain">Daily Buzz Report</a><br>					
					<a  href="./SnapOn.do?" target="ilexmain">Snap On</a><br>					
					<a  href="./DailySnaponReport.do?" target="ilexmain">Snap-on Pre-call Tracking - Daily Report</a><br>					
				</div>
			</div>
		</span>
		<br>
		<span>
			<div style="width:180">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="RPT/rptimages/left-cor.gif" border="0"></td>
						<td width="90%" class="RMenu_Head">&nbsp;Audit Trail </td>
						<td class="m2"><img src="RPT/rptimages/Expand.gif" border="0"></td>
						<td><img src="RPT/rptimages/right-cor.gif" border="0"></td>
					</tr>
				</table>
			</div>
			<div class="RMenu_Items" style="display: none">
				<div>					
					<a  href="./AuditTrail.do?" target="ilexmain">Audit Trail Reports </a><br>
				</div>
			</div>
		</span>
	</td>
	</tr>
	</table>
	</body>

</html:html>