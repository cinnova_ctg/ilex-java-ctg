<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ILEX</title>
<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<script>
function ViewLibrary(url)
{
    document.forms[0].target = "_self";
	document.forms[0].action = url;
	document.forms[0].submit();
	return true;
}
function onSubmit(url)
{// blank
}

function redirrectToView(obj,viewName,mode)
{
	var url = "ViewManupulation.do?hmode=addView&function="+mode+"&libId=<c:out value="${param.libId}"/>&viewId="+obj+"&viewName="+viewName+"&libName=<c:out value='${param.libName}'/>";
	document.forms[0].action=url;
	document.forms[0].submit();
}
</script>
<script>


function alertNotAvailable() {
	alert("This feature will be available in future releases!");
}
</script>
</head>

<body>
<html:form action="ViewLibrary" name="ViewLibraryBean" type="com.mind.docm.formbean.ViewLibraryBean">
 
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td class="headerrow" height="19">&nbsp;</td></tr>
			<tr>
				<td background="images/content_head_04.jpg" height="21" width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Administration</a>
										 <a href="#">Library Manager</a>
										 		
				</td>
				
			</tr>
			<tr><td><h2>Library Detail: <c:out value="${ViewLibraryBean.libraryName}"/></h2></td></tr>
	</table>
	

  <table width='100%' border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
      	<td>&nbsp;</td>
      <td colspan="2" align="left">
	      <table width='100%' border="0" align="left" cellpadding="1" cellspacing="1" >
	      	<c:if test="${not empty requestScope.delete}">
				<tr>
					<td class="message">Deleted successfully.
					</td>
				</tr>
			</c:if>
			<c:if test="${not empty requestScope.exception}">
				<tr>
					<td class="message">Deleted successfully.
					</td>
				</tr>
			</c:if>
	          <tr nowrap>
	            <td height="20" width='40%' bgcolor="#EEEEEE" class = "Ntryb" align='left' >Library Information</td>
	            <td height="20" bgcolor="#EEEEEE" class = "Ntryb" align='left' >Value</td>
	          </tr>
		          <tr>
		            <td class='Ntextoleftalignnowrap'>Notify E-mail</td>
		            <td class='Ntexteleftalignnowrap'><c:out value="${ViewLibraryBean.notifyEmail}"/></td>
		          </tr>
		          <tr>
		            <td class='Ntextoleftalignnowrap'>Is Notifiable</td>
		            <td class='Ntexteleftalignnowrap'>&nbsp;</td>
		          </tr>
	          <tr nowrap>
	            <td colspan='2' align='center'>
	            	<input type="submit" name="Submit2" Class="button_c"  onclick="javascript:ViewLibrary('EntryFieldSetUp.do?hmode=unspecified&libId=<c:out value="${param.libId}"/>');" value="Entry Field Setup" />
	            	<input type="button" name="Submit1"  Class="button_c"   value="Ok"  onclick="alertNotAvailable();"/>
	            	<input type="button" name="Submit2"  Class="button_c"   value="Modify"  onclick="alertNotAvailable();"/>
	            	<input type="button" name="Submit3"  Class="button_c"   value="Delete"  onclick="alertNotAvailable();"/>
	            </td>
	          </tr>
	      </table>
      </td>

	<tr>
          	<td>&nbsp;</td>
      <td height="1" colspan="2" class="blacktextbold"><img src="docm/images/spacer.gif" width="100" height="10" /></td>
    </tr>
 
    <tr>
          	<td>&nbsp;</td>
      <td colspan="2" align="left">
      <table width='100%' border="0" align="left" cellpadding="1" cellspacing="1" >
          <tr nowrap>
            <td width="40%" height="20" bgcolor="#EEEEEE" class = "Ntryb" nowrap>View Names
            </td>
            <td bgcolor="#EEEEEE" class = "Ntryb" >Details </td>
            <td bgcolor="#EEEEEE" class = "Ntryb" >Modify</td>
            <td bgcolor="#EEEEEE" class = "Ntryb" >Delete</td>
          </tr>
          <c:set var="idx" value="0" />
          <c:forEach items="${requestScope.libraryView}" var="vwLibrary" varStatus="status" >
	          <tr>
	            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if>><a href="#" onclick="redirrectToView('<c:out value="${vwLibrary.libraryViewId}"/>','<c:out value="${vwLibrary.libraryViewName}"/>','view') " ><c:out value="${vwLibrary.libraryViewName}"/></a></td>
	            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if>><img  style="cursor:hand;"  src="docm/images/details.gif" width="18" height="18" onclick="redirrectToView('<c:out value="${vwLibrary.libraryViewId}"/>','<c:out value="${vwLibrary.libraryViewName}"/>','view')"/></td>
	            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if>><img  style="cursor:hand;"  src="docm/images/modify.gif" width="18" height="18" onclick="redirrectToView('<c:out value="${vwLibrary.libraryViewId}"/>','<c:out value="${vwLibrary.libraryViewName}"/>','update')"/></td>
	            <c:if test="${vwLibrary.libraryViewName eq 'Default View'}">
	            	<td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if>><input type="button"  name="Submit" Class="button_c"  value="Add View" onclick="redirrectToView('','','add')"/></td>
	          	</c:if>
	          	<c:if test="${vwLibrary.libraryViewName ne 'Default View'}">
	          		<td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if>><img  style="cursor:hand;"  src="docm/images/delete.gif" width="18" height="18" onclick="redirrectToView('<c:out value="${vwLibrary.libraryViewId}"/>','<c:out value="${vwLibrary.libraryViewName}"/>','delete')"/></td>
	          	</c:if>
	          </tr>
	        <c:set var="idx" value="${idx+1}"/>
	       </c:forEach>
      </table></td>
    </tr>


  </table>


</html:form>
</body>
</html>