<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />

<%Object obj =  request.getAttribute("filterCondtion"); %>
<script>
function ViewFile(url){
    document.forms[0].target = "_self";
	document.forms[0].action = url;
	document.forms[0].submit();
	return true;
}

function backToSearch(){
	document.forms[0].action = "DocMSearch.do";
	document.forms[0].submit();
	return true;
}

</script>


</head>
	<body>
	<html:form action="DocumentMaster">
	
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td class="headerrow" height="19">&nbsp;</td></tr>
			<tr>
				<td background="images/content_head_04.jpg" height="21" width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
										 <a href="#">Search</a>
										 		
				</td>
				
			</tr>
			<tr><td><h2>Document Search Result</h2></td></tr>
				<logic:present name = "Error" scope = "request">
					<tr><td class ="message">&nbsp;&nbsp;Search criteria missing or not correct!</td></tr>
				</logic:present>
	</table>
	
		
		<table width='100%' border="0" align="left" cellpadding="1" cellspacing="1">
          <c:set var="docid" value="0"/>
          <c:set var="fileId" value="0"/>
          <c:set var="MimeType" value=""/>
          <c:set var="fileName" value=""/>
          <c:set var="viewOnWeb" value=""/>
          <c:set var="controlType" value=""/>
        <tr valign = "top"><td valign = "top">
			  <table>
				<tr>
					<td valign = "top">
				 <c:forEach items="${requestScope.documentList}" var="row" varStatus="status" >
				           <c:if test="${status.index== '0'}">
					           <tr><td>&nbsp;</td>
									<c:forEach items="${row}" var="rowfield" varStatus="index">
										<c:if test="${index.index gt '4'}">
											<c:if test="${index.index eq '5'}">
												<td class="Ntryb" colspan="2"><c:out value="${rowfield}" /></td>
											</c:if>
											<c:if test="${index.index ne '5'}">
												<td class="Ntryb"><c:out value="${rowfield}" /></td>
											</c:if>
										</c:if>
										<c:if test="${index.last}">
									      <c:set var="colcount" value="${index.count}"/>
									    </c:if>
									</c:forEach>
								</tr>
						    </c:if>
						    <c:set var="idx" value="0"/>
						    <c:if test="${status.index != '0'}">
					           <tr>
					           	   <td>&nbsp;</td>
						           <c:forEach items="${row}" var="rowfield" varStatus="ind">
							            <c:if test="${ind.index == '0'}"><c:set var="docid" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '1'}"><c:set var="fileId" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '2'}"><c:set var="MimeType" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '3'}"><c:set var="fileName" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '4'}"><c:set var="libId" value="${rowfield}"/></c:if>
										<c:if test="${ind.index gt '4'}">
											<c:if test="${ind.index eq '5'}">
												<td 
													<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
													<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
													 >
													 <c:out value="${rowfield}"/>
												</td>
												<td align="center" 
									       			<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
									       			<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
									       			 >
													[
													<c:if test="${not empty fileId}">
														<a href = "#" onclick="ViewFile('DocumentMaster.do?hmode=openFile&fileId=<c:out value="${fileId}"/>&libId=<c:out value="${libId}"/>')">Download</a>
													</c:if>
													<c:if test="${empty fileId}">Download</c:if>
													|
													<a href = "DocumentAdd.do?hmode=viewDocument&libId=<c:out value="${libId}"/>&docId=<c:out value="${docid}"/>">Details</a>&nbsp;|
													<a href = 'DocumentHistory.do?hmode=unspecified&documentId=<c:out value="${docid}"/>'>Revisions</a>&nbsp;|
													<a  href = "DocumentAdd.do?hmode=modifyDocument&libId=<c:out value="${param.libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>">Modify</a>&nbsp;| 
													<a href = "DocumentAdd.do?hmode=viewDocument&libId=<c:out value="${param.libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&delete=DeleteNow">Delete</a>
													]
													
									            </td>
											</c:if>
											<c:if test="${ind.index gt '5'}">
												<td  
													<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
													<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
													 > 
													 <c:out value="${rowfield}"/>
												</td>
											</c:if>
										</c:if>
						       	</c:forEach>
					       </tr>
					   </c:if>
					   <c:if test="${status.last}">
					      <c:set var="rowcount" value="${status.count}"/>
					    </c:if>
			         </c:forEach>
			        </td>
			      </tr>
				<c:if test="${rowcount lt '2'}">
					<tr><td>&nbsp;</td>
						<td class='Ntexteleftalignnowrap' colspan='<c:out value="${colcount}"/>'>No document found!</td>
					</tr>
    			</c:if>
			    </table>
        	</td></tr>
        	<tr><td><input type = "button" name ="button" value ="Search Again" onclick = "jasvscript:backToSearch();"/></td></tr>
     </table>
   </html:form> 
	</body>
</html>