<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For assigning and updating, function privilege to a role.
*
-->


<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<bean:define id = "codes" name = "dynamiccomboUM" scope = "request"/>

<html:html>
<head>
	<title><bean:message bundle = "DocM" key = "um.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table>
<html:form action = "/DocMRolePrivilegeAction">
<html:hidden  property="refresh"/>
<html:hidden  property="org_type"/>
<html:hidden  property="org_discipline_id"/>

<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr><td background="images/content_head_04.jpg" height="21">&nbsp;</td></tr>
				<tr><td><h2><bean:message bundle = "DocM" key = "um.label"/></h2></td></tr>
</table>


<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			 <td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "90%"> 
  			 			
			    	<logic:present name = "retval" scope = "request">
					    	<tr>
			    				<td  colspan = "3" class = "message" height = "30">
						    	 <logic:equal name = "retval" value = "0">
						    		<bean:message bundle = "DocM" key = "um.add.success"/>
						    	</logic:equal>
			    	
						    	<logic:equal name = "retval" value = "-9001">
				    				<bean:message bundle = "DocM" key = "um.add.failure"/>
			    				</logic:equal>
			    				</td>
			    		      </tr>
			          </logic:present>
					
					<tr>
					    <td class = "colDark" height="20"><bean:message bundle = "DocM" key = "um.assignroleprivilegeto"/></td>
					    <td class = "colLight" height="20">
					    <bean:write name = "DocMRolePrivilegeForm" property = "lo_organization_type_name"/>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "colDark" height="20"><bean:message bundle = "DocM" key = "um.oraganizationdiscipline"/></td>
					    <td class = "colLight" height="20">
  				        <bean:write name = "DocMRolePrivilegeForm" property = "lo_od_name"/>
					    </td>
 					</tr>
				
					
					<tr>
						<td class = "colDark" height="20"><bean:message bundle = "DocM" key = "um.functiongroup"/></td>
						<td class = "colLight" height="20">
						<html:select styleClass ="select" property = "cc_fu_group_name" onchange = "return setFuctionType();">  
						<html:optionsCollection name = "codes" property = "functiongroup" value = "value" label = "label"/> 
						</html:select>
						</td>
				    </tr>
				    
				    <tr>
					    <td class = "colDark" height="20"><bean:message bundle = "DocM" key = "um.role"/></td>
					    <td class = "colLight" height="20">
					    <html:select styleClass ="select" property = "lo_ro_role_desc" onchange = "return setFuctionType();">  
						<html:optionsCollection name = "codes" property = "rolename" value = "value" label = "label"/> 
						</html:select>
						</td>
 					</tr>
 				</table>
			</td>
		</tr>
</table>
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>		
			<td  width="2" height="0"></td>
 			<td>
  				<table border = "0" cellspacing = "1" cellpadding = "1" width = "90%"> 
 					<% 
 					int i = 0; 
 					String label = "";
 					%>					
 					<logic:present name = "UMList" scope="request"> 
					<logic:iterate id = "um" name = "UMList">
 		
 					<% if (i==0) {%>
 					<tr>
 	 	    			<logic:notEqual name = "um" property = "c2" value="">	 					
 	 					<td colspan = "2" class = "labellobold">
 	 					<bean:write name = "um" property = "c2"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:equal name="um" property = "c2" value=""> 
 	 			        <td colspan = "2" class = "labellobold">
 	 			        &nbsp;
 	 			        </td>  
 	 			        </logic:equal>
 	 					
 	 					<logic:notEqual name = "um" property = "c3" value="">
 	 					<td colspan = "2" class = "labellobold">
 	 					<bean:write name = "um" property = "c3"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:equal name="um" property = "c3" value=""> 
 	 			        <td colspan = "2" class = "labellobold">
 	 			        &nbsp;
 	 			        </td>  
 	 			        </logic:equal>
 	 		
 	 					<logic:notEqual name = "um" property = "c4" value="">
 	 					<td colspan = "2" class = "labellobold">
 	 					<bean:write name = "um" property = "c4"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:equal name="um" property = "c4" value=""> 
 	 			        <td colspan = "2" class = "labellobold">
 	 			        &nbsp;
 	 			        </td>  
 	 			        </logic:equal>
 	 	 				
 	 	 				<logic:notEqual name = "um" property = "c5" value="">			   	
 	 					<td colspan = "2" class = "labellobold">
 	 					<bean:write name = "um" property = "c5"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:equal name="um" property = "c5" value=""> 
 	 			        <td colspan = "2" class = "labellobold">
 	 			        &nbsp;
 	 			        </td>  
 	 			        </logic:equal>
 	 					
 	 					<logic:notEqual name = "um" property = "c6" value="">
 	 					<td colspan = "2" class = "labellobold">
 	 					<bean:write name = "um" property = "c6"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:equal name="um" property = "c6" value=""> 
 	 			        <td colspan = "2" class = "labellobold">
 	 			        &nbsp;
 	 			        </td>  
 	 			        </logic:equal>
 	 			        
 	 			        <logic:notEqual name = "um" property = "c7" value="">
 	 					<td colspan = "2" class = "labellobold">
 	 					<bean:write name = "um" property = "c7"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:equal name="um" property = "c7" value=""> 
 	 			        <td colspan = "2" class = "labellobold">
 	 			        &nbsp;
 	 			        </td>  
 	 			        </logic:equal>
 	 			        
 	 			        <logic:notEqual name = "um" property = "c8" value="">
 	 					<td colspan = "2" class = "labellobold">
 	 					<bean:write name = "um" property = "c8"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:equal name="um" property = "c8" value=""> 
 	 			        <td colspan = "2" class = "labellobold">
 	 			        &nbsp;
 	 			        </td>  
 	 			        </logic:equal>
 	 			   </tr>
	 	 				<% }
	 	 				else {
		 				if(i%2==0) label  = "Ntexteleftalignnowrap";
						if(i%2==1) label  = "Ntextoleftalignnowrap";
		 	 			%>
 	 				
 	 				<tr>
 	 					<logic:notEqual name = "um" property = "c2" value="">
 	 					<td colspan = "1" class = "<%= label %>">
 	 					<bean:write name = "um" property = "c2"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:notEqual name = "um" property = "c2id" value="">
 	 					<td colspan = "1" class = "<%= label %>"> 
						<html:multibox property = "check">
						<bean:write name = "um" property = "c2id"/>
 						</html:multibox>
			    		</td>
			    		</logic:notEqual>
			    					    		
			    		<logic:equal name="um" property = "c2" value=""> 
 	 			        <td colspan = "2" class = "<%= label %>">
 	 			        &nbsp;
 	 			        </td>  
 	 			        </logic:equal>
 	 			        
		 	 			<logic:notEqual name = "um" property = "c3" value="">
 	 					<td colspan = "1" class = "<%= label %>">
 	 					<bean:write name = "um" property = "c3"/>
 	 					</td>
 	 					</logic:notEqual>
 	 	
				 	 	<logic:notEqual name = "um" property = "c3id" value="">				
 	 					<td colspan = "1" class = "<%= label %>"> 
						<html:multibox property = "check">
						<bean:write name = "um" property = "c3id"/>
						</html:multibox>
			    		</td>
			    		</logic:notEqual>
			    		
			    		<logic:equal name="um" property = "c3" value=""> 
 	 			        <td colspan = "2" class = "<%= label %>">
 	 			        &nbsp;
 	 			        </td>
 	 			        </logic:equal>
 	 		 	 		
 	 		            <logic:notEqual name = "um" property = "c4" value="">
 	 					<td colspan = "1" class = "<%= label %>">
 	 					<bean:write name = "um" property = "c4"/>
 	 					</td>
 	 					</logic:notEqual>
 	 					
 	 					<logic:notEqual name = "um" property = "c4id" value="">
 	 					<td colspan = "1" class = "<%= label %>"> 
						<html:multibox property = "check">
						<bean:write name = "um" property = "c4id"/>
						</html:multibox>
			    		</td>
			    		</logic:notEqual>
			    		
			    		<logic:equal name="um" property = "c4" value=""> 
 	 			        <td colspan = "2" class = "<%= label %>">
 	 			        &nbsp;
 	 			        </td>
 	 			        </logic:equal>
 	 					
 	 					<logic:notEqual name = "um" property = "c5" value=""> 	 							   	
 	 					<td colspan = "1" class = "<%= label %>">
 	 					<bean:write name = "um" property = "c5"/>
 	 					</td>
 	 					</logic:notEqual>
 	 						 					
 	 					<logic:notEqual name = "um" property = "c5id" value="">
 	 					<td colspan = "1" class = "<%= label %>"> 
  					    <html:multibox property = "check">
						<bean:write name = "um" property = "c5id"/>
						</html:multibox>
			    		</td>
			    		</logic:notEqual>
			    		
			    		<logic:equal name="um" property = "c5" value=""> 
 	 			        <td colspan = "2" class = "<%= label %>">
 	 			        &nbsp;
 	 			        </td>
 	 			        </logic:equal>
			    		
			    		<logic:notEqual name = "um" property = "c6" value="">
			    		<td colspan = "1" class = "<%= label %>">
 	 					<bean:write name = "um" property = "c6"/>
 	 					</td>
 	 					</logic:notEqual>
			    		
			    		<logic:notEqual name = "um" property = "c6id" value="">
			    		<td colspan = "1" class = "<%= label %>"> 
						<html:multibox property = "check">
						<bean:write name = "um" property = "c6id"/>
						</html:multibox>
			    		</td>
			    		</logic:notEqual>
			    		
			    		<logic:equal name="um" property = "c6" value=""> 
 	 			        <td colspan = "2" class = "<%= label %>">
 	 			        &nbsp;
 	 			        </td>
 	 			        </logic:equal>
 	 			        
 	 			        <logic:notEqual name = "um" property = "c7" value="">
			    		<td colspan = "1" class = "<%= label %>">
 	 					<bean:write name = "um" property = "c7"/>
 	 					</td>
 	 					</logic:notEqual>
			    		
			    		<logic:notEqual name = "um" property = "c7id" value="">
			    		<td colspan = "1" class = "<%= label %>"> 
						<html:multibox property = "check">
						<bean:write name = "um" property = "c7id"/>
						</html:multibox>
			    		</td>
			    		</logic:notEqual>
			    		
			    		<logic:equal name="um" property = "c7" value=""> 
 	 			        <td colspan = "2" class = "<%= label %>">
 	 			        &nbsp;
 	 			        </td>
 	 			        </logic:equal>
 	 			        
 	 			        <logic:notEqual name = "um" property = "c8" value="">
			    		<td colspan = "1" class = "<%= label %>">
 	 					<bean:write name = "um" property = "c8"/>
 	 					</td>
 	 					</logic:notEqual>
			    		
			    		<logic:notEqual name = "um" property = "c8id" value="">
			    		<td colspan = "1" class = "<%= label %>"> 
						<html:multibox property = "check">
						<bean:write name = "um" property = "c8id"/>
						</html:multibox>
			    		</td>
			    		</logic:notEqual>
			    		
			    		<logic:equal name="um" property = "c8" value=""> 
 	 			        <td colspan = "2" class = "<%= label %>">
 	 			        &nbsp;
 	 			        </td>
 	 			        </logic:equal>
 	 					 					
 	 				</tr>
	 	 				<%	} 
	 	 				i++; 
	 	 				%>
 	 				</logic:iterate>
 	 				</logic:present>
 					 
 					
 					<tr> 
    				<td><img src = "images/spacer.gif" width = "1" height = "1"></td>
				    </tr>
				    		
					  <tr> 
					     <td colspan="14" class="colLight" align ="center"> 
					     <html:submit property="save" styleClass="button_c" onclick = "return validate();">
					     <bean:message bundle="DocM" key="um.save"/></html:submit>
					     <html:reset property="reset" styleClass="button_c">
					     <bean:message bundle="DocM" key="um.cancel"/>
					     </html:reset>
					  </tr>
					  
					  <jsp:include page = '/Footer.jsp'>
							<jsp:param name = 'colspan' value = '28'/>
							<jsp:param name = 'helpid' value = 'umroleprivilege'/>
					  </jsp:include>
				  		
								
				</table>
			</td>
		</tr>
</table>
</html:form>
</table>
</body>
<script>
function setFuctionType()
{
    document.forms[0].refresh.value = "true";
	document.forms[0].submit();
	return true;
}

function validate()
{
	document.forms[0].refresh.value = "false";
	if( document.forms[0].cc_fu_group_name.value == "0" )
	{
	alert("<bean:message bundle = "DocM" key = "um.selectfuncgrp"/>");
	document.forms[0].cc_fu_group_name.focus();
	return false;
	}
	
	if( document.forms[0].lo_ro_role_desc.value == "-1" )
	{
	alert("<bean:message bundle = "DocM" key = "um.selectrolename"/>");
	document.forms[0].lo_ro_role_desc.focus();
	return false;
	}
}
</script>
</html:html>
