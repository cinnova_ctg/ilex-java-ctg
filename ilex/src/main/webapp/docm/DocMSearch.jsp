<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ page import="java.sql.*" %> <%@ page import="javax.sql.DataSource" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:define id="dcdocm" name="dcdocm"  scope="request"/>
<%
	Connection	conn =(Connection)request.getAttribute("SQL");
	Statement	stmt = conn.createStatement();
	int size = 0;
	if(request.getAttribute("size") != null)
	size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString()); 
	String tdClass = "";
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document Search </title>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="JavaScript1.2" src="docm/javascript/ViewManipulation.js"></script>
</head>
<body>
	<html:form action="/DocMSearch">
	
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td class="headerrow" height="19">&nbsp;</td></tr>
			<tr>
				<td background="images/content_head_04.jpg" height="21" width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
										 <a href="#">Search</a>
										 		
				</td>
				
			</tr>
			<tr><td><h2>Filter Criteria</h2></td></tr>
	</table>
	
		<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1">
		<tr><td width="2" height="0"></td>
			<td><table border="0" cellspacing="1" cellpadding="1" >
					<tr>
						<td  class = "colDark">Search in Library</td>
						<td  class = "colLight" colspan = "4"> 
							 <html:select  name = "DocMSearchForm" property="selectedMSA" styleClass="select">
									<html:optionsCollection name = "dcdocm"  property = "liblist"  label = "label"  value = "value" />
								</html:select>   
						</td>
					</tr>
					<tr>
						<td colspan = "5">
							<table id="dynatableSearch"  width="100%" border="0" cellspacing="1" cellpadding="1" >
							<% int i=0;%>
							<% if(i%2==0){
								tdClass = "ColDark";
							}else{
								tdClass = "ColLight";
							}%>
							<tr><td class = "colDark">&nbsp;</td>
								<td class = "colDark">Field Name</td>
								<td class = "colDark">&nbsp;</td>
								<td class = "colDark">&nbsp;</td>
								<td class = "colDark">Delete</td>
							</tr>
							<logic:present name="filterCondtion" scope="request">
								<logic:iterate id="FC" name="filterCondtion">
									<tr>
										<%if(i==0){%>
											<td class = "<%=tdClass%>">where</td>
										<%}else{%>
											<td class = "<%=tdClass%>">&nbsp;</td>
										<%}%>
											<td class = "<%=tdClass%>"><html:select styleId="<%="fieldName"+i%>" name ="FC" property="fieldName" size="1" styleClass = "select">
											<html:optionsCollection name = "dcdocm"  property = "fieldlist"  label = "label"  value = "value" />
											</html:select>
										</td>
											<td class = "<%=tdClass%>"><html:select styleId="<%="fieldOperator"+i%>" name ="FC" property="fieldOperator" size="1"  styleClass = "select">
											<html:optionsCollection name = "dcdocm"  property = "relOptCmb"  label = "label"  value = "value" />
											</html:select>
										</td>
											<td class = "<%=tdClass%>"><html:text name ="FC" property = "fieldText" styleId="<%="fieldText"+i%>" styleClass = "select" size="70"></html:text></td>
											<td class = "<%=tdClass%>"><img style='cursor:hand;' src='docm/images/delete.gif' onclick="deleteRow(document.getElementById('dynatableSearch'),this);"/>
									</tr>
									<%if(i != size-1){%>
									<tr>
											<td class = "<%=tdClass%>" colspan="5"><html:select styleId="<%="logicOperator"+i%>" name ="FC" property="logicOperator" size="1"  styleClass = "select">
											<html:optionsCollection name = "dcdocm"  property = "logicOptCmb"  label = "label"  value = "value" />
											</html:select>
										</td>
									</tr>
									<%}%>
								<%i++; %>	
								</logic:iterate>
							</logic:present>	
							</table>
						</td>	
					</tr>
					<tr><td class="colLight" colspan="5" align = "left"><html:button property = "addConditiion" styleClass="button_c" value = "Add Condition" onclick = "moreAttachCondition(document.getElementById('dynatableSearch'),'fieldName','fieldOperator','fieldText','logicOperator','deleteImage');"></html:button></td></tr>
					<tr><td class ="ColLight" colspan="5" align = "center"><html:submit property="searchNow" styleClass="button_c" onclick = "return validateSearch();">Search Now</html:submit></td></tr>	
				</table>
			</td>
		</tr>
		</table>
	</html:form>
</body>
<Script>
	function moreAttachCondition(){
		var arg=moreAttachCondition.arguments;
		var i = arg[0].rows.length ;
		select="select";
		tdClass = "ColDark"
				    			
		if(i > 1 ){
			if(! validateMoreAttachCondition())return false;	
		}	
				
		if(i>1){
		
			var oRow=arg[0].insertRow();
			var oCell=oRow.insertCell(0);
			oCell.colSpan = "5";
			oCell.className=tdClass;
			//setStyleForCells(i,oCell);
			oCell.innerHTML = "<select id='"+arg[4]+i+"' name='"+arg[4]+"' size='1' Class='"+select+"'> <option value='0'>---Select---</option><option value='and'>And</option><option value='or'>OR</option></select>";	
		}
		
		
		var oRow=arg[0].insertRow();
		var oCell=oRow.insertCell(0);
		oCell.className=tdClass;
		//setStyleForCells(i-1,oCell);
		if(i ==1 ){
		oCell.innerHTML = "Where";
		}else{
		oCell.innerHTML = "";	
		}
		
		var oCell=oRow.insertCell(1);
		//setStyleForCells(i-1,oCell);
		oCell.className=tdClass;
		oCell.innerHTML = "<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+select+"'><option value='0'>---Select---</option><%String  sql="select dm_md_fl_id,dm_md_fl_nm from dm_metadata_fields_list order by dm_md_fl_id"; int countfl=0;   ResultSet   rs1 = stmt.executeQuery(sql); while(rs1.next()) { countfl++;%> <option value='<%= rs1.getString("dm_md_fl_id") %>'><%= rs1.getString("dm_md_fl_nm") %></option>	<%}%> </select>";	
		
		var oCell=oRow.insertCell(2);
		//setStyleForCells(i-1,oCell);
		oCell.className=tdClass;
		oCell.innerHTML = "<select id='"+arg[2]+i+"' name='"+arg[2]+"' size='1' Class='"+select+"'> <option value='0'>---Select---</option><option value='='>Equals</option><option value='<>'>Not Equals</option><option value='like'>Contains</option><option value='not like'>Not Contains</option></select>";	
		
		var oCell=oRow.insertCell(3);
		//setStyleForCells(i-1,oCell);
		oCell.className=tdClass;
		oCell.innerHTML="<input type='text' id='"+arg[3]+i+"' name='"+arg[3]+"' size='70' value='' class='text' />";
		
		var oCell=oRow.insertCell(4);
		//setStyleForCells(i-1,oCell);
		oCell.className=tdClass;
		oCell.innerHTML="<img  style='cursor:hand;'  src='docm/images/delete.gif' id='"+arg[5]+i+"'  name='"+arg[5]+"' onclick=\"deleteRow(document.getElementById('dynatableSearch'),this);\"/>";
	}
	
	function deleteRow(object,el){
		var row = el.parentNode.parentNode; 
		var rownum = row.rowIndex; 
		var tableRows = object.rows.length ;
		
		if(tableRows == 2 && rownum == 1){
			object.deleteRow(rownum);
		}else if(tableRows > 2 && rownum ==1 ){
			alert('Not Allowed');
		}else{
			object.deleteRow(rownum);
			object.deleteRow(rownum-1);
		}
	}
	
	function validateMoreAttachCondition(){
		var fields = document.getElementsByName("fieldName")
		var fieldsTxt = document.getElementsByName("fieldText")
		var fieldsOpt = document.getElementsByName("fieldOperator")
		var fieldlgopt = document.getElementsByName("logicOperator")
		
		for (var k=0;k<fieldlgopt.length;k++){
			if(!chkCombo(document.getElementById(fieldlgopt[k].id),'Logic operator')) return false;
		}
		
		for (var k=0;k<fields.length;k++){
			if(!chkCombo(document.getElementById(fields[k].id),'Field Name')) return false;
			if(!chkCombo(document.getElementById(fieldsOpt[k].id),'Field Operator')) return false;
			if(!chkCombo(document.getElementById(fieldsTxt[k].id),'Field Text')) return false;
		}
		return true;
	}
	
	function validateSearch(){
		var object =  document.getElementById('dynatableSearch');
		if(object.rows.length > 0)
			if(! validateMoreAttachCondition())return false;	
	}
</Script>
<Script>

function chkCombo(obj,label){
	if(obj.value==0 || obj.value == null || obj.value == "" )	{	
		alert("Please Select "+label);	
		obj.focus();
		return false;
	}
	return true;
}

</Script>
<%
rs1.close();
stmt.close();
conn.close();%>
</html>