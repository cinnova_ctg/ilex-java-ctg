<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ILEX</title>
<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<script>

function alertNotAvailable() {
	alert("This feature will be available in future releases!");
}
</script>

</head>

<body>
<html:form action="MetaDataList">




	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td class="headerrow" height="19">&nbsp;</td></tr>
			<tr>
				<td background="images/content_head_04.jpg" height="21" width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Administration</a>
										 <a href="#">MetaData Manager</a>
										 		
				</td>
				
			</tr>
			<tr><td><h2>Meta Data List</h2></td></tr>
	</table>
	
		
		
		


	<table border="0" align="left" cellpadding="1" cellspacing="1">
		<tr>
			<td>&nbsp;</td>
			<td bgcolor="#EEEEEE" class="Ntryb" colspan="2">Field&nbsp;Name</td>
			<td bgcolor="#EEEEEE" class="Ntryb">Field Type</td>
			<td bgcolor="#EEEEEE" class="Ntryb">Description</td>
			<td bgcolor="#EEEEEE" class="Ntryb">Default Value</td>
			<td bgcolor="#EEEEEE" class="Ntryb">Taxonomy Group</td>
		</tr>
		<c:set var="idx" value="0" />
		<c:forEach items="${requestScope.mdList.fields}" var="field"
			varStatus="status">
				<c:choose>
				<c:when test="${(idx%'2')=='0'}">
					<c:set var="tdClass" value="Ntextoleftalignnowrap"/>
				</c:when>	
				<c:otherwise>
					<c:set var="tdClass" value="Ntexteleftalignnowrap"/>
				</c:otherwise>
			</c:choose>
			
			<tr>
				<td>&nbsp;</td>
				<td align = "left" class = <c:out value="${tdClass}" />>
				<a href="#" onclick="alertNotAvailable();"><c:out value="${field.field_name}" /></a><br>
				</td>
				<td align = "right" class = <c:out value="${tdClass}" />>
					[
					<a href = "#" onclick="alertNotAvailable();">Details</a>&nbsp;|
					<a href = "#" onclick="alertNotAvailable();">Modify</a>&nbsp;|
					<a href = "#" onclick="alertNotAvailable();">Delete</a>
					]	
					<c:set var="idx" value="${idx+1}" />
		<%-- 
				<img src="docm/images/details.gif" /> <img
					src="docm/images/modify.gif" /> 
				<img src="docm/images/delete.gif" /></td>   --%>
				<td align = "right" class = <c:out value="${tdClass}" />>
				<c:out value="${field.field_type}" /></td>

				<td align = "right" class = <c:out value="${tdClass}" />>
				<c:out value="${field.description}" /></td>

				<td align = "right" class = <c:out value="${tdClass}" />>
				<c:out value="${field.default_value}" /></td>

				<td align = "right" class = <c:out value="${tdClass}" />>
				<c:out value="${field.tax_group}" /></td>
			</tr>
		</c:forEach>
		<tr>
			<td>&nbsp;</td>
			<td colspan="7" class = "colLight" align ="center"><input type="button" Class="button_c"
				name="Submit" value="Add Field"  onclick="alertNotAvailable();"/></td>
		</tr>
		<%-- 
		<tr>
			<td>&nbsp;</td>
			<td colspan="9" align="right"><a href="#" class="textlink">Previous&nbsp;&nbsp;Page
			1 of 3&nbsp;&nbsp;Next</a></td>
		</tr>  --%>
	</table>
</html:form>
</body>
</html>
