<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ page import = "com.mind.docm.*" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<bean:define id = "menustring" name = "menustring" scope = "request"/>
<bean:define id = "currState" name = "currState" scope = "request"/>
<bean:define id = "expansionState" name = "expansionState" scope = "request"/>

<bean:define id = "element" name = "element" scope = "request"/>
<bean:define id = "controller" name = "controller" scope = "request"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DocM Left Tree Menu</title>
<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/summary.css" rel="stylesheet" type="text/css" />
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/ce_menu.js" type="text/javascript"></script>

<script>
	function openModules(module,imdId)
	{
		var plus="Expand.gif";
		var minus="Collapse.gif";
		var imgSrc=document.getElementById(imdId).src;
		var idx=document.getElementById(imdId).src.lastIndexOf("/");
		var ImgSrcPath=imgSrc.substring(0,idx+1);
		var imgName=imgSrc.substring(idx+1,imgSrc.length);
		if(imgName==plus){
		document.getElementById(imdId).src=ImgSrcPath+minus;
			document.getElementById(module).style.display="block";
		}else if(imgName==minus){
		document.getElementById(imdId).src=ImgSrcPath+plus;
			document.getElementById(module).style.display="none";
		}
	}
</script>
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 10px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 0px;
    	white-space : nowrap;
    }

    /* Link Styles */
    a  {
    	color : #000000;
    }

    a:link  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:active  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:hover  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:visited  {
    	text-decoration : underline;
    	color : #000000;
    }
    
    .m2 {
        font-family : "Verdana";
        font-size : 10px;
        line-height: 10px;
    }    
</style>
</head>
<body class="body"  background="docm/images/sidebg.jpg">
<html:form action="LibraryList">
	<table  align="left" cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr><td>
			<table  align="left" cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td width="9%"><a onclick="openModules('admin','img1')"><img id='img1' src="docm/images/Expand.gif" border="0" /></a></td>
					<td class=dblabel valign = top >Administration</td>
				</tr>
				<tr><td colspan="2">
					<div style='display:none;' id='admin'>
						<table>
							<tr><td class=dbvaluesmall >&nbsp;&nbsp;&nbsp;&nbsp;<a  href="MetaDataList.do?hmode=unspecified" target="ilexmain">MetaData Manager</a></td></tr>					
							<tr><td class=dbvaluesmall >&nbsp;&nbsp;&nbsp;&nbsp;<a  href="LibraryManager.do?hmode=unspecified" target="ilexmain">Library Manager</a></td></tr>
							<tr><td class=dbvaluesmall >&nbsp;&nbsp;&nbsp;&nbsp;<a  href="TransferDocument.do?synchronize=true" target="ilexmain">Synchronize</a></td></tr>
						</table>
					</div>
				</td></tr>
				<tr><td><a onclick="openModules('libraries','img2')"><img id='img2' src="docm/images/Expand.gif" border="0" /></a></td>
					<td class=dblabel>Libraries</td>
				</tr>
				<tr><td colspan="2">
					<div style='display:none;' id='libraries'>
						<table>
							<c:forEach items="${requestScope.libraryList}" var="lib" varStatus="status" >
								<tr><td class=dbvaluesmall>&nbsp;&nbsp;&nbsp;&nbsp;<a href="DocumentMaster.do?hmode=unspecified&status=approved&libId=<c:out value="${lib.libraryId}" />" target="ilexmain" title="<c:out value="${lib.libraryName}" />"><c:out value="${lib.libraryChar}" /></a></td></tr>
							</c:forEach>
						</table>
					</div>
				</td></tr>
				<tr><td><a onclick="openModules('docmaction','img3')"><img id='img3' src="docm/images/Expand.gif" border="0" /></a></td>
					<td class=dblabel>Document Actions</td>
				</tr>
				<tr><td colspan="2">
					<div style='display:none;' id='docmaction'>
						<table>
							<tr><td class=dbvaluesmall>&nbsp;&nbsp;&nbsp;&nbsp;<a  href="DocumentAdd.do?hmode=unspecified&Source=DocM" target="ilexmain">Add</a></td></tr>
							<tr><td class=dbvaluesmall>&nbsp;&nbsp;&nbsp;&nbsp;<a  href="DocMSearch.do" target="ilexmain">Search</a></td></tr>
						</table>
					</div>
				</td></tr>
			</table>
		</td></tr>
		<%-- 
		<tr><td>
			<table>
			<tr><td>
					<%
						DocMMenuElement me = ((DocMMenuTree)(element)).getRootElement();
						DocMMenuController mc =((DocMMenuTree)(element)).getDocMMenuController();
						String value = me.drawDirect(0,mc,new StringBuffer());
					%>
						<%=value%>
						<script language="JavaScript">
							document.getElementById('line0').className = 'm2';
							document.getElementById('line0').style.visibility = 'visible';
							
							currState="<%=((DocMMenuTree)(element)).getCurrState()%>";
							expansionState="<%=expansionState%>";
							initNavigatorServer('UM');
						</script>	
			</td></tr>			
		   </table>
		</td></tr>		
		--%>
	</table>	
</html:form>
</body>
</html>
















