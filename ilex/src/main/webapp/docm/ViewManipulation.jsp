<!DOCTYPE HTML>
<HTML>
<HEAD>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page  language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<% response.setHeader("Pragma","No-Cache");
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setDateHeader("Expires",0);%>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META name="GENERATOR" content="IBM WebSphere Studio">
<script type="text/javascript" language="JavaScript1.2" src="docm/javascript/prototype.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="docm/javascript/ViewManipulation.js"></script>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<script type='text/javascript' src='./dwr/interface/ViewManupulationDao.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>
<TITLE>part.jsp</TITLE>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<script>
var sortingCombo = '<select  name = "selectedColumns"  Class="select" ><c:forEach var="list" items="${requestScope.columnList}" varStatus="index"><option  value="<c:out value='${list[0]}' />"><c:out value="${list[1]}" /></option></c:forEach></select>';
var sortDirrection = '<select  name = "sortDirrection"  Class="select" ><option  value="1" >Ascending</option><option  value="-1" >Descending</option></select> ';
var filterCriteria_1 = '	 <select  name = "filterCondition"  Class="select" onChange="changefilterCondition(this,\'filterTable\');"><option value="="> Equals</option><option value="!="> Not Equals</option><option value="greaterThan"> Greater than</option><option value="lessThan"> Less Than</option><option value="like"> Contains</option><option value="not like"> Not Contains</option><option value="is Empty"> Is Empty</option><option value="is null"> Is Null</option></select>';
var filterCriteria_2 = '	 <select  name = "filterCondition"  Class="select" onChange="changefilterCondition(this,\'filterTable\');"> <option value="="> Equals</option><option value="!="> Not Equals</option><option value="greaterThan"> Greater than</option><option value="lessThan"> Less Than</option><option value="is Empty"> Is Empty</option><option value="is null"> Is Null</option></select>';
var filterCriteria_3 = '	 <select  name = "filterCondition"  Class="select" onChange="changefilterCondition(this,\'filterTable\');"><option value="="> Equals</option><option value="!="> Not Equals</option><option value="greaterThan"> Greater than</option><option value="lessThan"> Less Than</option></select>';
var filterCriteria_4 = '	 <select  name = "filterCondition"  Class="select" onChange="changefilterCondition(this,\'filterTable\');"><option value="="> Equals</option><option value="!="> Not Equals</option></select>';
var logicalCondition = '<select  name = "logicalCondition"  Class="select"><option value="and"> AND</option><option value="or"> OR</option></select>   ';
function makeSelect(bName)
{
return '<select  name = "'+bName+'" Class="select" ><c:forEach var="list" items="${requestScope.columnList}" varStatus="index"><option  value="<c:out value='${list[0]}' />"><c:out value="${list[1]}" /></option></c:forEach></select>';
}

function makeFilterSelect(bName)
{
return '<select  name = "'+bName+'" Class="select" onchange="chngCtrls(this,\'filterTable\');"><c:forEach var="list" items="${requestScope.filterColumnList}" varStatus="index"><option  value="<c:out value='${list[10]}' />"><c:out value="${list[1]}" /></option></c:forEach></select>';
}
</script>
<script>

var toSubmit=false;
function OnSave(previousViewName)
{
	var isCheck= false;
	trimFields();
	checkViewExists();
	for(var i=0;i<document.all.columnCheckValue.length;i++)
		{
			if(document.all.show[i].checked!=true) {
				document.all.columnCheckValue[i].value = 'N';
			} else {
				document.all.columnCheckValue[i].value = 'Y';
			}
			if(document.all.show[i].checked==true)
			{
				isCheck = true;
			}
			document.forms[0].selectedColumns[i].selected = 'selected';
						//alert(document.forms[0].selectedColumns[i].selected)
		}
	if(!isCheck){
		alert('Please check one of the fields');
		return false;
	}
	if(document.forms[0].viewName.value=='')
	{
		alert('Please enter View Name');
		return false;
	}
	//alert(trim(document.forms[0].viewName.value))
	if( previousViewName!= 'Default View' &&  document.forms[0].viewName.value == 'Default View') 
	{
		alert("Please enter view name other than 'Default View'");
		return false;
	}
	if(!toSubmit){
		document.getElementById('viewExist').innerText = 'View already exists';
		return false;
	}
	else{
		//alert("isCheck=" + isCheck + "\ntoSubmit=" + toSubmit);
		toSubmit=false;
		document.forms[0].Save.disabled = true;
		document.forms[0].viewName.disabled=false;
		var url = "ViewManupulation.do?hmode=insertData&libId=<c:out value='${param.libId}'/>";
		document.forms[0].action = url;	
		document.forms[0].submit();
	}
	
	function checkViewExists() {
		ViewManupulationDao.isViewExist(
			document.forms[0].libId.value,
			document.forms[0].viewName.value,
			document.forms[0].viewId.value,
			'<c:out value="${ViewManupulationBean.function}"/>',
			{
				callback:function(response){
					toSubmit=!isDataExist(response);
				}
			,
			async:false
			}
		);
	}
}
</script>
<script>
function isDataExist(obj)
{
	if(obj==true){
		return true;
	}
	else{
		return false;
	}
}

function OnDelete()
{
	var returnVal  = confirm('Do you really want to delete.');
	if(returnVal > 0)
	{
		document.forms[0].Save.disabled = true;
		var url = "ViewManupulation.do?hmode=deleteData&libId=<c:out value='${param.libId}'/>";
		document.forms[0].action = url;	
		document.forms[0].submit();
	}
	else
		return false;
}
function OnViewUpdate()
{	
	document.forms[0].viewUpdate.disabled = true;
	var url = "ViewManupulation.do?hmode=addView&function=update&viewName=<c:out value='${ViewManupulationBean.viewName}'/>&viewId=<c:out value='${ViewManupulationBean.viewId }'/>";
	document.forms[0].action = url;	
	document.forms[0].submit();
}
</script>


</HEAD>
<BODY onload="showshdmore();showbidmore();fun_HideShowBlock(0);<c:if test="${ViewManupulationBean.function ne 'view' && ViewManupulationBean.function ne 'delete' && ViewManupulationBean.function ne 'update'}"><c:if test="${empty ViewManupulationBean.selectedSortColumns[0]}">addSortRow();</c:if><c:if test="${empty ViewManupulationBean.selectedGroupColumns[0]}">addGroupRow();</c:if></c:if>">
<html:form action="ViewManupulation.do" >
<input type='hidden' name="viewId" value="<c:out value='${param.viewId}'/>"/>
<input type='hidden' name="libName" value="<c:out value='${ViewManupulationBean.libName}'/>"/>
<input type="hidden" name="function" value="<c:out value='${ViewManupulationBean.function }'/>"/>
<input type='hidden' name="libId" value="<c:out value='${param.libId}'/>"/>
<table cellpadding="0" cellspacing="0" border="0" width="100%" border=1>
	<tr>
		<td class="headerrow" height="19" width="100%">&nbsp;</td>
	</tr>
	<tr>
		<td background="images/content_head_04.jpg" height="21"
			width="100%">
		<div id="breadCrumb"><a href="#" >Administration</a>
		 					 <a href="#">Library Manager</a>
		 					 <a href="#"><c:out value="${ViewManupulationBean.libName}"/></a>
		 					 <a href="#"><c:out value="${param.viewName}"/></a>
		 </div>					 	
		</td>
	</tr>
	<tr>
		<td colspan="6">
		<h2><c:if test="${ViewManupulationBean.function eq 'add'}"> 'View' Add </c:if>
		<c:if test="${ViewManupulationBean.function eq 'update'}"> 'View' Update </c:if>
		<c:if test="${ViewManupulationBean.function eq 'Delete'}"> 'View' Delete </c:if>
		<c:if test="${ViewManupulationBean.function eq 'view'}"> 'View' Detail</c:if></h2>
		</td>
	</tr>
</table>
<TABLE WIDTH="100%" cellspacing="1" cellpadding="1">
	<tr><td>
	<TABLE WIDTH="680" id='main' cellspacing="0" cellpadding="0">
		<c:if test="${not empty requestScope.updated}">
			<tr>
				<td class="message"><c:if test="${requestScope.updated eq 'update'}"> Your Information is Updated.</c:if>
					<c:if test="${requestScope.updated eq 'save'}"> Your Information is Saved.</c:if>
				</td>
			</tr>
		</c:if>
		
		<tr>
			<td class="message" id="viewExist">
			</td>
		</tr>
		
		<tr>
			<td width="100%"> 
				
			</td>
		</tr>
		<tr>
			<td width="100%">
				<table width="100%">
					<tr>
						<td class='Ntextoleftalignnowrap' width="10%">View Name</td>
						<td class='Ntexteleftalignnowrap' width="90%"><c:if test="${ ViewManupulationBean.viewName ne 'Default View' && (ViewManupulationBean.function ne 'view' && ViewManupulationBean.function ne 'delete') }"><input class="text" type="text" name="viewName" size="100" maxlength="<c:out value='${requestScope.viewFieldSize}'/>"  value="<c:out value='${ViewManupulationBean.viewName}'/>"/></c:if>
							<c:if test="${ViewManupulationBean.function eq 'view' || ViewManupulationBean.function eq 'delete'}"><c:out value='${ViewManupulationBean.viewName}'/></c:if>
							<c:if test= "${ViewManupulationBean.viewName eq 'Default View' && (ViewManupulationBean.function ne 'view' && ViewManupulationBean.function ne 'delete') }"><input class="text" type="text" name="viewName" size="100" disabled="disabled" maxlength="<c:out value='${requestScope.viewFieldSize}'/>"  value="<c:out value='${ViewManupulationBean.viewName}'/>"/></c:if></td>
					</tr>
				</table>
			</td>
				
		</tr>
		<TR>
			<TD valign="TOP" >	
				<TABLE WIDTH="70%" cellspacing="0" cellpadding="0" >
					<TR>
						<td width='20%' class='leftRightBorder'>
							<table cellpadding="0" cellspacing="0" width='100%'>
								<tr>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(0);" align="center" width="2px"
										nowrap><img src="docm/images/left-cor.gif" border="0"></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(0);" align="center" width="80%" class='drop'
										nowrap bgcolor="#B5C8D9"><bean:message key="view.column" /></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(0);" align="center" width="2px"
										nowrap><img src="docm/images/right-cor.gif" border="0"></td>
								</tr>
							</table>
						</td>
						<td width='20%' class='leftRightBorder'>
							<table cellpadding="0" cellspacing="0" width='100%'>
								<tr>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(1);" align="center" width="2px"
										nowrap><img src="docm/images/left-cor.gif" border="0"></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(1);" align="center" width="80%" class='drop'
										nowrap bgcolor="#B5C8D9"><bean:message
										key="view.columnsequence" /></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(1);" align="center" width="2px"
										nowrap><img src="docm/images/right-cor.gif" border="0"></td>
								</tr>
							</table>
						</td>
						<td width='20%' class='leftRightBorder'>
							<table cellpadding="0" cellspacing="0" width='100%'>
								<tr>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(2);" align="center" width="2px"
										nowrap><img src="docm/images/left-cor.gif" border="0"></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(2);" align="center" width="80%" class='drop'
										nowrap bgcolor="#B5C8D9"><bean:message key="view.sorting" /></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(2);" align="center" width="2px"
										nowrap><img src="docm/images/right-cor.gif" border="0"></td>
								</tr>
							</table>
						</td>
						<td width='20%' class='leftRightBorder'>
							<table cellpadding="0" cellspacing="0" width='100%'>
								<tr>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(3);" align="center" width="2px"
										nowrap><img src="docm/images/left-cor.gif" border="0"></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(3);" align="center" width="80%" class='drop'
										nowrap bgcolor="#B5C8D9"><bean:message key="view.grouping" /></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(3);" align="center" width="2px"
										nowrap><img src="docm/images/right-cor.gif" border="0"></td>
								</tr>
							</table>
						</td>
						<td width='20%' class='leftRightBorder'>
							<table cellpadding="0" cellspacing="0" width='100%'>
								<tr>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(4);" align="center" width="2px"
										nowrap><img src="docm/images/left-cor.gif" border="0"></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(4);" align="center" width="80%" class='drop'
										nowrap bgcolor="#B5C8D9"><bean:message
										key="view.filtercriteria" /></td>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock(4);" align="center" width="2px"
										nowrap><img src="docm/images/right-cor.gif" border="0"></td>
								</tr>
							</table>
						</td>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD>	
<!-- tab[0]-->
					<table  width='100%' 
						style="border-color: #D9DFEF;border-width: 4px;border-style: solid;"
					 cellspacing="0" cellpadding="0"><tr><td valign='top' width='100%' height='300'>
		<TABLE width="100%" valign='top'  cellspacing="0" cellpadding="0" >
			<TR id="tab" style="display:none">
				<TD width="680" >
					<TABLE width="100%" cellspacing="1" cellpadding="1" >
						<TR>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Field</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Description</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Taxonomy Group</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap align="center"> Show</TD>
						</TR>
						<c:if test="${ViewManupulationBean.function ne 'view' && ViewManupulationBean.function ne 'delete'}">
							<c:forEach var="list" items="${requestScope.columnList}" varStatus="count">
								<input type="hidden" name="idValues" value="<c:out value="${list[0]}"/>"/>
								<input type="hidden" name="columnCheckValue" value="Y" />
								<TR id="bid">
									<TD <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value="${list[2]}"/></TD>
									<TD <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value="${list[3]}"/></TD>
									<TD <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value="${list[6]}"/></TD>
									<TD align="center" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <input name="show" value="<c:out value="${list[0]}"/>" align="center" class="chkbx" type="checkBox"  <c:if test="${list[11] eq 'Y'}"> checked="checked"</c:if>/></TD>
								</TR>
							</c:forEach>
						</c:if>
						<c:if test="${ViewManupulationBean.function eq 'view' || ViewManupulationBean.function eq 'delete'}">
							<c:forEach var="list" items="${requestScope.columnList}" varStatus="count">
								<TR id = "bid">
									<TD <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value="${list[2]}"/></TD>
									<TD <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value="${list[3]}"/></TD>
									<TD <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value="${list[6]}"/></TD>
									<TD align="center"<c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:if test="${list[11] eq 'Y'}"> Yes</c:if><c:if test="${list[11] eq 'N'}"> No</c:if></TD>
								</TR>
							</c:forEach>
						</c:if>
						<tr>
						<td colspan="4" width="100%">
						<table  width="100%">
							<TR>
								<TD align="left" width="50%" onclick="showbidback()"  id="bidback" class="hypertext"><a href="#" title="Previous Page">&laquo;back</a></TD>
								<TD align="right" width="50%" onclick="showbidmore()" id="bidmore" class="hypertext"><a href="#" title="Next Page">more&raquo;</a></TD>				
							</TR>	
						</table>
						</td>
					</tr>
					</TABLE>
				</TD>
			</TR>
	
			
				 
<!-- tab[1]-->
			<TR id="tab" style="display:none">
				<TD width="680" >
					<TABLE width="100%" cellspacing="1" cellpadding="1" >
							<c:if test="${ViewManupulationBean.function ne'view' && ViewManupulationBean.function ne 'delete'}">
							<TR>			
							<TD width="20%">   
								 <select  id="selectedCols" name = "selectedColumns"  Class="select" size="10" multiple="multiple">
									 <c:forEach var="list" items="${requestScope.selectedColumList}" varStatus="index">
										<option  value="<c:out value='${list[0]}' />" ><c:out value="${list[1]}" /></option>
									 </c:forEach>
								</select>  
							</TD>
							<TD align="left">
								<TABLE cellpadding="0" cellspacing="0" border="0">
									<TR>
										<TD><img src="./images/offFilter2.gif" style="cursor:hand;" alt="Move Up" onclick="moveOptionsUp('selectedCols')" /></TD>
									</TR>
									<TR>
										<TD><img src="./images/offFilter2.gif" style="cursor:hand;" alt="Move Up" onclick="moveOptionsUp('selectedCols')" /></TD>
									</TR>
									<TR>
										<TD><img src="./images/offFilter2.gif" style="cursor:hand;" alt="Move Up" onclick="moveOptionsUp('selectedCols')" /></TD>
									</TR>
									<TR>
										<TD>&nbsp;</TD>
									</TR>
									<TR>
										<TD>&nbsp;</TD>
									</TR>
									<TR>
										<TD>&nbsp;</TD>
									</TR>
									<TR>
										<TD><img src="./images/showFilter.gif" style="cursor:hand;" alt="Move Down" onclick="moveOptionsDown('selectedCols')" /></TD>
									</TR>
									<TR>
										<TD><img src="./images/showFilter.gif" style="cursor:hand;" alt="Move Down" onclick="moveOptionsDown('selectedCols')" /></TD>
									</TR>
									<TR>
										<TD><img src="./images/showFilter.gif" style="cursor:hand;" alt="Move Down" onclick="moveOptionsDown('selectedCols')" /></TD>
									</TR>
								</TABLE> 
							</TD>
							</TR>
							</c:if>
							<c:if test="${ViewManupulationBean.function eq 'view' || ViewManupulationBean.function eq 'delete'}">
							 		<c:forEach var="list" items="${requestScope.selectedColumList}" varStatus="index">
							 		<c:if test='${index.first}'>
									<TR>
										<TD bgcolor="#EEEEEE" class = "Ntryb"  width="5%">S.No.</TD>
										<TD bgcolor="#EEEEEE" class = "Ntryb" >Field Name</TD>
									</TR>
									</c:if>
							 		<TR id="shd">			
										<TD <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> 
											<c:out value='${index.index+1}'/>
										</TD>
										<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
											<c:out value="${list[1]}" />
										</td>
									</TR>
									</c:forEach>
									<TR valign="bottom" >
										<td colspan=2>
											<table width="100%">
												<tr>
													<TD align="left" width="50%" onclick="showshdback()"  id="shdback" class="hypertext"><a href="#" title="Previous Page">&laquo;back</a></TD>
													<TD align="right" width="50%" onclick="showshdmore()" id="shdmore" class="hypertext"><a href="#" title="Next Page">more&raquo;</a></TD>				
												</tr>
											</table>
										</td>
									</TR>	
							</c:if>
						
					</TABLE>
				</TD>
			</TR>			
		
<!-- tab[2]-->
			<TR id="tab" style="display:none">
				<TD width="680" >
					<TABLE ID="sort" width="100%" cellspacing="1" cellpadding="1" >
						<c:if test="${ViewManupulationBean.function ne'view' && ViewManupulationBean.function ne 'delete'}">
							<TR>
								<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> S.No.</TD>
								<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Field Name</TD>
								<TD bgcolor="#EEEEEE" class = "Ntryb" nowrap> Order</TD>
								<TD bgcolor="#EEEEEE" class = "Ntryb"  width="15%" nowrap align="center"> Delete</TD>
							</TR>
							<c:forEach var="colItr" items="${ViewManupulationBean.selectedSortColumns}" varStatus="outerIndex">
								<input type="hidden" name="detailIdForSortColumn" value="<c:out value='${ViewManupulationBean.detailIdForSortColumn[outerIndex.index]}'/>"/>
								<TR>
									<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value='${outerIndex.index+1}'/></TD>
									<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
										 <select  name = "selectedSortColumns" Class="select">
										 <c:forEach var="list" items="${requestScope.columnList}" varStatus="innerIndex">
											<option  value="<c:out value='${list[0]}' />" <c:if test='${ViewManupulationBean.selectedSortColumns[outerIndex.index] eq list[0]}'>selected = "selected"</c:if>><c:out value='${list[1]}' />
										</option>
										 </c:forEach>
										</select>   
									</TD>
									<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
										<select  name = "sortDirrection"  Class="select">
											<option  value="1" <c:if test="${ViewManupulationBean.sortDirrection[outerIndex.index] gt '0'}"> selected = 'selected'</c:if>>Ascending</option>
											<option  value="-1" <c:if test="${ViewManupulationBean.sortDirrection[outerIndex.index] lt '0'}">selected = 'selected'</c:if>>Descending</option>
									</TD>
									<TD  align="center" <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><img   align="center" src="docm/images/delete.gif" name="sortImage" onclick="Rowdelete('sort',this)"></TD>
								</TR>
							</c:forEach>
						</c:if>
						<c:if test="${ViewManupulationBean.function eq 'view' || ViewManupulationBean.function eq 'delete'}">
							<c:if test="${empty ViewManupulationBean.selectedSortColumns}">
								<TR><td class="Ntextoleftalignnowrap" align="center">
								 No Order selected.
								 </td></TR>
							</c:if>
							<c:if test="${not empty ViewManupulationBean.selectedSortColumns}">
								<c:forEach var="colItr" items="${ViewManupulationBean.selectedSortColumns}" varStatus="outerIndex">
									<c:if test='${outerIndex.first}'>
									<TR>
										<TD bgcolor="#EEEEEE" class = "Ntryb"  width="5%" nowrap> S.No.</TD>
										<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Field Name</TD>
										<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Order</TD>
									</TR>
									</c:if>
									<TR>
										<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value='${outerIndex.index+1}'/></TD>
										<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <c:forEach var="list" items="${requestScope.columnList}" varStatus="innerIndex">
												<c:if test='${ViewManupulationBean.selectedSortColumns[outerIndex.index] eq list[0]}'><c:out value='${list[1]}' /></c:if>
											 </c:forEach>
										</TD>
										<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
												<c:if test="${ViewManupulationBean.sortDirrection[outerIndex.index] gt 0}">Ascending</c:if>
												<c:if test="${ViewManupulationBean.sortDirrection[outerIndex.index] lt 0}">Descending</c:if>
										</TD>
									</TR>
								</c:forEach>
							</c:if>
						</c:if>
						</TABLE>
						<c:if test="${ViewManupulationBean.function ne 'view' && ViewManupulationBean.function ne 'delete'}">
							<TABLE>
								<TR>
									<td  align="center" valign="middle" colspan="4">
									<input type="button" name="addMore" class="button_c" value="Add Row &darr;"
										style="WIDTH: 100px"  onClick="addSortRow();">
									
		
									</td>
								</TR>
							</TABLE>
						</c:if>
					</TD>
			</TR>			
			
			
			
<!-- tab[3]-->
			<TR id="tab" style="display:none">
				<TD width="680">
					<TABLE ID="group" width="100%" cellspacing="1" cellpadding="1" >
					<c:if test="${ViewManupulationBean.function ne'view' && ViewManupulationBean.function ne 'delete'}">
						<TR>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> S.No.</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Field Name</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap align="center"> Delete</TD>
						</TR>
							<c:forEach var="colItr" items="${ViewManupulationBean.selectedGroupColumns}" varStatus="outerIndex">
							<input type="hidden" name="detailIdForGroupColumn" value="<c:out value='${ViewManupulationBean.detailIdForGroupColumn[outerIndex.index]}'/>">
								<TR>
									<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value='${outerIndex.index+1}'/></TD>
									<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
										 <select  id="selectedGroupColumns" name = "selectedGroupColumns" class="select">
										 	
										 <c:forEach var="list" items="${requestScope.columnList}" varStatus="innerIndex">
											<option  value="<c:out value='${list[0]}' />" <c:if test='${ViewManupulationBean.selectedGroupColumns[outerIndex.index] eq list[0]}'>selected = "selected"</c:if>><c:out value="${list[1]}" /></option>
										 </c:forEach>
										</select>   
									</TD>
									<TD  align="center" <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><img  align="center" src="docm/images/delete.gif" name="groupImage" onclick="Rowdelete('group',this)"/></TD>
								</TR>
							</c:forEach>
						</c:if>
						<c:if test="${ViewManupulationBean.function eq 'view' || ViewManupulationBean.function eq 'delete'}">
							<c:if test="${empty ViewManupulationBean.selectedGroupColumns}">
								<tr><td class="Ntextoleftalignnowrap" align="center">
									No Group selected.
								</td></tr>
							</c:if>
							<c:if test="${not empty ViewManupulationBean.selectedGroupColumns}">
								<c:forEach var="colItr" items="${ViewManupulationBean.selectedGroupColumns}" varStatus="outerIndex">
									<c:if test='${outerIndex.first}'>
										<TR>
											<TD bgcolor="#EEEEEE" class = "Ntryb"  width="5%" nowrap> S.No.</TD>
											<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Field Name</TD>
										</TR>
									</c:if>
									<TR>
										<TD width="2%" <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> <c:out value='${outerIndex.index+1}'/></TD>
										<TD <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <c:forEach var="list" items="${requestScope.columnList}" varStatus="innerIndex">
												<c:if test='${ViewManupulationBean.selectedGroupColumns[outerIndex.index] eq list[0]}'><c:out value="${list[1]}" /></c:if>
											 </c:forEach>
										</TD>
									</TR>
								</c:forEach>
							</c:if>
						</c:if>
						</TABLE>
						<c:if test="${ViewManupulationBean.function ne 'view' && ViewManupulationBean.function ne 'delete'}">
						<TABLE>
							<TR>
								<td align="center" valign="middle" colspan="4" class = "colLight">
								<input type="button" name="addMore" class="button_c" value="Add Row &darr;"
										style="WIDTH: 100px"  onClick="addGroupRow();">
	
								</td>
							</TR>
						</TABLE>
						</c:if>
					</TD>
			</TR>			

<!-- tab[4]-->
			<TR id="tab" style="display:none">
				<TD width="680">
					<TABLE id="filterTable" width="100%" cellspacing="1" cellpadding="1" >						
					<c:if test="${ViewManupulationBean.function ne'view' && ViewManupulationBean.function ne 'delete'}">
						<TR>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> &nbsp;</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Field Name</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Condition</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Value</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap align="center"> Delete</TD>
						</TR>
							<c:forEach var="colItr" items="${ViewManupulationBean.selectedFilterColumns}" varStatus="outerIndex" >
								<input type="hidden" name="filterId" value="<c:out value='${ViewManupulationBean.filterId[outerIndex.index]}'/>">
								<TR>
									<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> Where</TD>
									<td <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
										 <select  name = "selectedFilterColumns" Class="select" onchange="chngCtrls(this,'filterTable');">
										 	<option value="Any" <c:if test='${empty ViewManupulationBean.selectedFilterColumns[outerIndex.index] }'>selected = "selected"</c:if>> Any </option>
										 <c:forEach var="list" items="${requestScope.filterColumnList}" varStatus="innerIndex">
											<option  value="<c:out value='${list[10]}' />" <c:if test='${ViewManupulationBean.selectedFilterColumns[outerIndex.index] eq list[10]}'>selected = "selected"</c:if> ><c:out value="${list[1]}" /></option>
										 </c:forEach>
										</select>   
									</td>
									<c:if test="${ViewManupulationBean.fieldTypeId[outerIndex.index] eq '0' || (ViewManupulationBean.fieldTypeId[outerIndex.index] >= 6 && ViewManupulationBean.fieldTypeId[outerIndex.index] <= 12 )}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <select  name = "filterCondition"  Class="select" onChange="changefilterCondition(this,'filterTable');">
											 	<option value="=" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '='}"> selected = "selected" </c:if>> Equals</option>
											 	<option value="!=" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '!='}"> selected = "selected" </c:if>> Not Equals</option>
											 	<option value="greaterThan" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'greaterThan'}"> selected = "selected" </c:if>> Greater than</option>
											 	<option value="lessThan" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'lessThan'}"> selected = "selected" </c:if>> Less Than</option>
											 	<option value="like" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'like'}"> selected = "selected" </c:if>> Contains</option>
											 	<option value="Not like" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'Not like'}"> selected = "selected" </c:if>> Not Contains</option>
											 	<option value="is Empty" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'is Empty'}"> selected = "selected" </c:if>> Is Empty</option>
											 	<option value="is null" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'is null'}"> selected = "selected" </c:if>> Is Null</option>
											</select>   
										</TD>
									</c:if>
									<c:if test="${ViewManupulationBean.fieldTypeId[outerIndex.index] eq '4'}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <select  name = "filterCondition"  Class="select" onChange="changefilterCondition(this,'filterTable');">
											 	<option value="=" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '='}"> selected = "selected" </c:if>> Equals</option>
											 	<option value="!=" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '!='}"> selected = "selected" </c:if>> Not Equals</option>
											 	<option value="greaterThan" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'greaterThan'}"> selected = "selected" </c:if>> Greater than</option>
											 	<option value="lessThan" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'lessThan'}"> selected = "selected" </c:if>> Less Than</option>
											 	<option value="is Empty" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'is Empty'}"> selected = "selected" </c:if>> Is Empty</option>
											 	<option value="is null" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'is null'}"> selected = "selected" </c:if>> Is Null</option>
											</select>   
										</TD>
									</c:if>
									<c:if test="${ViewManupulationBean.fieldTypeId[outerIndex.index] eq '1' || ViewManupulationBean.fieldTypeId[outerIndex.index] eq '2' || ViewManupulationBean.fieldTypeId[outerIndex.index] eq '5'}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <select  name = "filterCondition"  Class="select" onChange="changefilterCondition(this,'filterTable');">
											 	<option value="=" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '='}"> selected = "selected" </c:if>> Equals</option>
											 	<option value="!=" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '!='}"> selected = "selected" </c:if>> Not Equals</option>
											 	<option value="greaterThan" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'greaterThan'}"> selected = "selected" </c:if>> Greater than</option>
											 	<option value="lessThan" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'lessThan'}"> selected = "selected" </c:if>> Less Than</option>
											</select>   
										</TD>
									</c:if>
									<c:if test="${ViewManupulationBean.fieldTypeId[outerIndex.index] eq '3'}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <select  name = "filterCondition"  Class="select" onChange="changefilterCondition(this,'filterTable');">
											 	<option value="=" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '='}"> selected = "selected" </c:if>> Equals</option>
											 	<option value="!=" <c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '!='}"> selected = "selected" </c:if>> Not Equals</option>
											</select>   
										</TD>
									</c:if>
									<c:if test="${ViewManupulationBean.fieldTypeId[outerIndex.index] eq '4'}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><input class="text" type="text" name="filterType" size="40" readonly="true" maxlength="40" onblur="validateData(this);" value="<c:out value='${ViewManupulationBean.filterType[outerIndex.index]}'/>"/>&nbsp;<img  src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].filterType[<c:out value='${outerIndex.index}'/>], document.forms[0].filterType[<c:out value='${outerIndex.index}'/>], 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img></TD>
									</c:if>
									<c:if test="${ViewManupulationBean.fieldTypeId[outerIndex.index] ne '4'}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><input class="text" type="text" name="filterType" size="40" onblur="validateData(this);" maxlength="40" value="<c:out value='${ViewManupulationBean.filterType[outerIndex.index]}'/>"/></TD>
										<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'is Empty' ||  ViewManupulationBean.filterCondition[outerIndex.index] eq 'is null'}">
											<script>if(document.all.filterType[<c:out value="${outerIndex.index}"/>]==null)
														document.all.filterType.readOnly=true
													else
														document.all.filterType[<c:out value="${outerIndex.index}"/>].readOnly=true
														</script>
										</c:if>
											
									</c:if>
									<TD valign=top  align="center" <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><img  align="center" src="docm/images/delete.gif" name="filterImage" onclick="Rowdelete('filterTable',this)"/></TD>
								</TR>
								<c:if test="${!outerIndex.last}">	
									<tr>
										<td colspan=5 <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
										  <select  name = "logicalCondition"  Class="select" >
										 	<option value="and" <c:if test="${ViewManupulationBean.logicalCondition[(outerIndex.index)+1] eq 'and'}"> selected = "selected" </c:if>> AND</option>
										 	<option value="or" <c:if test="${ViewManupulationBean.logicalCondition[(outerIndex.index)+1] eq 'or'}"> selected = "selected" </c:if>> OR</option>
										  </select>
									  	</td>   
								 	</tr> 	
								</c:if>								
							</c:forEach>
						</c:if>
						<c:if test="${ViewManupulationBean.function eq 'view' || ViewManupulationBean.function eq 'delete'}">
							<c:if test="${empty ViewManupulationBean.selectedFilterColumns}"> 
								<tr><td class="Ntextoleftalignnowrap" align="center">
								No Filter Criteria selected.
								</td></tr>
							</c:if>
							<c:if test="${not empty ViewManupulationBean.selectedFilterColumns}">
								<c:forEach var="colItr" items="${ViewManupulationBean.selectedFilterColumns}" varStatus="outerIndex">
								<c:if test='${outerIndex.first}'>
									<TR>
										<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> &nbsp;</TD>
										<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Field Name</TD>
										<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Condition</TD>
										<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Value</TD>
									</TR>
								</c:if>
									<TR>
										<TD valign=top width="2%" <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> Where</TD>
										<td <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
											<c:if test='${not empty ViewManupulationBean.selectedFilterColumns[outerIndex.index]}'>
												<c:forEach var="list" items="${requestScope.filterColumnList}" varStatus="innerIndex">
													<c:if test='${ViewManupulationBean.selectedFilterColumns[outerIndex.index] eq list[10]}'><c:out value="${list[1]}" /></c:if> 
									 			</c:forEach>
								 			</c:if>
								 			<c:if test='${ViewManupulationBean.selectedFilterColumns[outerIndex.index] eq "0"}'>
									 			Any
								 			</c:if>
										</td>
												
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>  
												<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'greaterThan'}"> GreaterThan </c:if>
												<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'lessThan'}"> LessThan </c:if>
												<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '='}"> Equals </c:if>
												<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq '!='}"> Not Equals </c:if>
												<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'like'}"> Contains </c:if>
												<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'Not like'}"> Not Contains </c:if>
												<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'is Empty'}"> Is Empty </c:if>
												<c:if test="${ViewManupulationBean.filterCondition[outerIndex.index] eq 'is null'}"> Is Null</c:if>
										</TD>
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><c:out value="${ViewManupulationBean.filterType[outerIndex.index]}"/></TD>
									</TR>
									<c:if test="${!outerIndex.last}">	
										<tr>
											<td colspan=4 <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><c:out value="${ViewManupulationBean.logicalCondition[outerIndex.index+1]}"/> 
											</td>
										</tr>
									</c:if>	
								</c:forEach>
							</c:if>
						</c:if>
						</TABLE>
						<c:if test="${ViewManupulationBean.function ne 'view' && ViewManupulationBean.function ne 'delete'}">
							<TABLE>
								<TR>
									<td class = "colLight" align="center" valign="middle" colspan="4">
									<input type="button" name="addMore" class="button_c" value="Add Row &darr;"
										style="WIDTH: 100px"  onClick="addFilterCriteriaRow();">
									</td>
								</TR>
							</TABLE>
						</c:if>
					</TD>
			</TR>			

		</TABLE>
	</td></tr></table>
		</TD>
	</TR>
		<c:if test="${ViewManupulationBean.function eq 'add' || ViewManupulationBean.function eq 'update'}">
			<TR>
		
				<TD align="center">
				<c:if test="${ViewManupulationBean.function eq 'update'}">
					<input type="hidden" name="function" value="update"/>
					<input  type="button" class="button_c" name="Save" value="Update" onClick="OnSave('<c:out value='${ViewManupulationBean.viewName}'/>');">
				</c:if>
				<c:if test="${ViewManupulationBean.function eq 'add'}">
					<input type="hidden" name="function" value="add"/>
					<input  type="button" class="button_c" name="Save" value="Save" onClick="OnSave();">
				</c:if>
				<input  type="reset" class="button_c" value="Cancel">
					
					
				</TD>
			</TR>
		</c:if>
		<c:if test="${ViewManupulationBean.function eq 'view'}">
			<TR>
		
				<TD align="center">
					<input  name="viewUpdate"  value="Modify"
						 type="button" onClick="OnViewUpdate();" class="button_c">
					
				</TD>
			</TR>
		</c:if>
		<c:if test="${ViewManupulationBean.function eq 'delete'}">
			<TR>
		
				<TD align="center">
					<input type="button"  name="Save" value="Delete"
						  onClick="OnDelete();" class="button_c">
					<input type="hidden" name="function" value="delete"/>&nbsp;&nbsp;&nbsp;&nbsp;
					<input  type="reset" class="button_c" value="Cancel">
					
				</TD>
			</TR>
		</c:if>
</TABLE>
</td></tr></TABLE>
</html:form>
</BODY>
<!--/html:form-->
</HTML>
