<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<bean:define id="dcdocm" name="dcdocm"  scope="request"/>
<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<script type="text/javascript" language="JavaScript1.2" src="docm/javascript/ViewManipulation.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="docm/javascript/prototype.js"></script>
<script type='text/javascript' src='./dwr/interface/DocMSeacrhDao.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<title>Document Search Page</title>
<script>
var filterCriteria_1 = '	 <select  name = "filterCondition"  Class="select"><option value="="> Equals</option><option value="!="> Not Equals</option><option value="greaterThan"> Greater than</option><option value="lessThan"> Less Than</option><option value="like"> Contains</option><option value="not like"> Not Contains</option><option value="is Empty"> Is Empty</option><option value="is null"> Is Null</option></select>';
var filterCriteria_2 = '	 <select  name = "filterCondition"  Class="select"><option value="="> Equals</option><option value="!="> Not Equals</option><option value="greaterThan"> Greater than</option><option value="lessThan"> Less Than</option><option value="is Empty"> Is Empty</option><option value="is null"> Is Null</option></select>';
var filterCriteria_3 = '	 <select  name = "filterCondition"  Class="select"><option value="="> Equals</option><option value="!="> Not Equals</option><option value="greaterThan"> Greater than</option><option value="lessThan"> Less Than</option></select>';
var filterCriteria_4 = '	 <select  name = "filterCondition"  Class="select"><option value="="> Equals</option><option value="!="> Not Equals</option></select>';
var logicalCondition = '<select  name = "logicalCondition"  Class="select"><option value="and"> AND</option><option value="or"> OR</option></select>   ';
function makeFilterSelect(bName)
{
	return '<select  name = "'+bName+'" Class="select" onchange="chngCtrlsSearch(this,\'filterTable\');"><c:forEach var="list" items="${requestScope.filterColumnList}" varStatus="index"><option  value="<c:out value='${list[10]}' />"><c:out value="${list[1]}" /></option></c:forEach></select>';
}
function openAddPage(){
		var through;
		var appendixId = '<c:out value="${param.appendixId}"/>';
		var poId = '<c:out value="${param.poId}"/>';
		var msaId = '<c:out value="${param.msaId}"/>';
		var devId = '<c:out value="${param.devId}"/>'
		if('<c:out value="${param.view}"/>'=='po'){
			through='PO';
			var url="./EntityHistory.do?msaId="+msaId+"&entityId="+poId+"&poId="+poId+"&linkTo=PO&fromPage=PO&linkLibName=PO&entityType=Engineering Support&function=addSupplement&through="+through+"&devId="+devId;
			}
		else{
			through='MPO';
			var url="./EntityHistory.do?msaId="+msaId+"&entityId="+appendixId+"&appendixId="+appendixId+"&linkTo=appendix&fromPage=Appendix&linkLibName=Appendix&entityType=Engineering Support&function=addSupplement&through="+through;
		}
		//child = open(url,'popupwindow','width=800,height=600,scrollbars=yes,status=yes');
		//child.opener = window.opener;
		window.document.location=url;
	}
</script>
<script>
	
	function ViewFile(url)
	{
    document.forms[0].target = "_self";
	document.forms[0].action = url;
	document.forms[0].submit();
	return true;
	}

	function getValue(url,val){
	opener.document.forms[0].LinkedDoc.value=url+"&abc=y";
	DocMSeacrhDao.getTitleFromDocId(val,{callback:function(str){ret=str;},async:false});
	opener.document.all.LinkDocName.innerHTML=ret;
	opener.document.forms[0].LinkToOtherDocId.value=val;
	window.close ();
	}
	function insertDocForPo(val1,val2)
	{
		opener.insertDoc(val1,val2);
		window.close ();
	}
</script>
<script>
	function sendToMpoPage(){
		var len = document.getElementsByName("mpoSearch").length;
		var isCheck = false;
		if(!len || len == 1)
		{
			if(document.all.mpoSearch.checked == true)
			{
				isCheck = true;
			}
		}
		else
		{
			for(var i=0;i<len;i++)
			{
				if(document.all.mpoSearch[i].checked == true)
				{
					isCheck = true;
					break;
				}
			}
		}
		if(isCheck == false)
		{
			alert('Please select at least one of the documents');
			return false; 
		}
		var checkedDoc=document.getElementsByName("mpoSearch");
		opener.callOpener(checkedDoc);
		window.close ();
	}
</script>
<script>
	function submitForm(url){
		document.forms[0].action=url;
		document.forms[0].submit();
	}
	
function searchAgain(){
	var length = document.all.filterType.length;
	var check;
	var multicheck = true;
	if(!length){
		check = validateDataSearch(document.all.filterType);
		if(check == false)multicheck = false;
	}else{
		for(var i=0;i<length;i++){
		check = validateDataSearch(document.all.filterType[i]);
		if(check == false)multicheck = false;
		}
	}
	if(document.getElementById('search_formdiv').style.display == "block")
		document.forms[0].isShow.value="Y";
	if(multicheck){
		document.forms[0].action = "";
		//document.forms[0].submit();
		return true;
	}else
		return false;
	
}	
	
function showHideSearchForm() {

	var checkShowExists = '<bean:write name = "DocMSearchForm" property = "linkLibNameFromIlex" />';
	if(checkShowExists == 'DocM'){
		var arg = showHideSearchForm.arguments;
		var isShow = 0;
		if(arg!=null && arg.length>0) {
			isShow = arg[0];
		}
		var object= document.getElementById('search_formdiv');
		//alert(object);
		if(isShow==1 || object.style.display=='none') {
			object.style.display="block";
			document.getElementById('showhidesearch1').value='Hide Search Criteria';
		} else {
			object.style.display="none";
			document.getElementById('showhidesearch1').value='Show Search Criteria';
		}
	}	
	return;
}

function setShowAllColumns() {
	var control = document.getElementById('showAllColumns');
	if(control.checked) {
		control.value='1';
	} else {
		control.value='0';
	}
}

function checkLastRow() {
	var object= document.all.filterTable;
	var i = object.rows.length ;
	if(i<=2) {
		//disable the search button
		document.getElementById('searchNow').disabled=true;
	} else {
		//enable the search button
		document.getElementById('searchNow').disabled=false;
	}
}
function checkNoRow() {
	//enable the search button
	document.getElementById('searchNow').disabled=false;
}
</script>
<script>	
function addFilterCriteriaRow(){
	var object= document.all.filterTable;
	var i = object.rows.length ;
	var oRow;
	if(i>1){
		oRow=object.insertRow(-1);
		oCell=oRow.insertCell(0);
		oCell.innerHTML=logicalCondition;
		oCell.colSpan = '5'
		setStyleForCells(i-1,oCell);
	}
	
	oRow=object.insertRow(-1);
	oCell=oRow.insertCell(0);
	oCell.vAlign='top';
	setStyleForCells(i,oCell);
	oCell.innerText = 'Where';
	oCell=oRow.insertCell(1);
	oCell.innerHTML = makeFilterSelect('selectedFilterColumns')
	oCell.vAlign='top';
	setStyleForCells(i,oCell);
	/*oCell.innerHTML = '<table id=innerTable cellpedding=0 cellspacing=0><tr><td>'+makeFilterSelect('selectedFilterColumns')+'</td></tr><tr>'	// oCell.innerHTML = makeSelect('selectedFilterColumns');
	if(i>1){
		var iTab=document.all.innerTable[i-2];
		var iRow=iTab.insertRow();
			iRow.insertCell(0).innerHTML=logicalCondition;
	}*/	
	oCell=oRow.insertCell(2);
	oCell.vAlign='top';
	setStyleForCells(i,oCell);
	oCell.innerHTML=filterCriteria_1;
	oCell=oRow.insertCell(3);
	oCell.vAlign='top';
	setStyleForCells(i,oCell);
	oCell.innerHTML='<input type="text" name="filterType" maxLength="40" size="40" class="text" ><input type="hidden" name="filterId" value="" /><input type="hidden" name="fieldTypeId" value="">'
	oCell=oRow.insertCell(4);
	oCell.vAlign='top';
	setStyleForCells(i,oCell);
	oCell.innerHTML='<img  align="center" src="docm/images/delete.gif" name="filterImage" onclick="checkLastRow();Rowdelete(\'filterTable\',this)"/>';
	oCell.align = 'center';
}
</script>
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<script>
jQuery(document).ready(function (
		
		){
	<c:if test='${empty DocMSearchForm.searchNow}'>addFilterCriteriaRow();</c:if><c:if test='${not empty DocMSearchForm.searchNow}'>showHideSearchForm(<c:if test="${not empty param.view && empty requestScope.isShow}">'0'</c:if><c:if test="${empty param.view || not empty requestScope.isShow}">'1'</c:if>);showbidmore();</c:if>
});
</script>
</head>

<body>
	<html:form method="post" action="/DocMSearch">
	<html:hidden name="DocMSearchForm" property="linkToDoc"/>
	<html:hidden name="DocMSearchForm" property="linkLibNameFromIlex"/>
	<html:hidden name="DocMSearchForm" property="isShow"/>
<logic:equal name = "DocMSearchForm" property = "linkLibNameFromIlex" value = "Appendix">
	<logic:equal name = "DocMSearchForm" property = "appendixStatusChecking" value = "Inwork">
	
		<logic:equal name = "DocMSearchForm" property = "appendixType" value = "3">
			<%@ include  file="/NetMedXDashboardMenu.inc" %>
		</logic:equal>
		
		<logic:notEqual name = "DocMSearchForm" property = "appendixType" value = "3">
			<%@ include  file="/AppedixDashboardMenu.inc" %>
		</logic:notEqual>
		
	</logic:equal>
</logic:equal>	
	
	

<logic:equal name ="DocMSearchForm" property = "linkLibNameFromIlex" value = "DocM">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td class="headerrow" height="19">&nbsp;</td></tr>
			<tr>
				<td background="images/content_head_04.jpg" height="21" width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone"></a>
										 <a href="#"></a>
										 		
				</td>
			</tr>
	</table>
</logic:equal>
<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>		 
	
	<logic:equal name = "DocMSearchForm" property = "linkLibNameFromIlex" value = "MSA">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<%-- <%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )){%>
								 <%if(msastatus.equals( "Cancelled" )){%>
						        	<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
						        <%}else{ %>
									<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
						        <%}%>
							  <%}else{%>
		                 			 <td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}%>
								<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
								<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
								<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
						<%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>		
					</tr>
					<tr>
					          <td background="images/content_head_04.jpg" height="21" colspan="7" width="100%">
				          		<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
				         	 </td>
					</tr>
					<tr>
							<td colspan="6">
								<h2>Supplement History: <bean:write name  ="DocMSearchForm" property ="msaName"/></h2>
							</td>
					</tr>
		</table>
	</logic:equal>
	<logic:equal name = "DocMSearchForm" property = "linkLibNameFromIlex" value = "Appendix">
					<logic:equal name = "DocMSearchForm" property = "appendixStatusChecking" value = "Inwork">
						<logic:equal name = "DocMSearchForm" property = "appendixType" value = "3">
						<!--  Menu Change  -->
						<div id="menunav">

						<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          <a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocMSearchForm" property="appendixId"/>">Search</a>
  		</li>
  		</ul>
		</li>

	</div>
	<table cellpadding="0" cellspacing="0" border="0" width = "100%">
	<tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
								          		<div id="breadCrumb">&nbsp;</div>
								        </td>
							        </tr>
							        <tr>
							        	<td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.metmedxdashboardformsa"/>:&nbsp;<bean:write name="DocMSearchForm" property="msaName"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="DocMSearchForm" property="appendixName"/></h2></td>
							        </tr> 
		      				</table>
	
	
	
	
	
	
	
	
	
	
	
	
	
						
						<!-- End of Menu -->
							<%--  <table cellpadding="0" cellspacing="0" border="0" width = "100%">
							        <tr>
							        	<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage1</center></a></td>
										<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
										<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocMSearchForm" property="appendixId"/>" style="width: 120px"><center>Search</center></a></td>
										<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
									</tr>
							        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
								          		<div id="breadCrumb">&nbsp;</div>
								        </td>
							        </tr>
							        <tr>
							        	<td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.metmedxdashboardformsa"/>:&nbsp;<bean:write name="DocMSearchForm" property="msaName"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="DocMSearchForm" property="appendixName"/></h2></td>
							        </tr> 
		      				</table> --%>
						</logic:equal>
						<logic:notEqual name = "DocMSearchForm" property = "appendixType" value = "3">
						<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li><a href="JobEditAction.do?ref=inscopeview&amp;appendix_Id=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;Job_Id=0">Add Job</a></li>
        		<li>
        			<a href="#"><span>Bulk Job</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=<%= appendixid %>">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=<%= appendixid %>">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li>
				
	        		<% if( contractDocMenu != "" ) { %>
	        		<li>
	        			<a href="#"><span>Contract Documents</span></a>
	        			<ul>
							<%= contractDocMenu %>
	        			</ul>
	        		</li>
				<% } %>
				
        		<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Custom Fields</a></li>
        		<li><a href="AddCustRef.do?typeid=<%= appendixid %>&amp;type=Appendix&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Customer Reference</a></li>
        		<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">External Sales Agent</a></li>
        		<li>
        			<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;initialCheck=true&amp;pageType=jobsites">Job Sites</a></li>
        		<li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=<%= appendixid %>&amp;type=P&amp;page=appendixdashboard&amp;pageid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Update</a></li>
						
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Special Instructions/Conditions</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">Team</a></li>
        		<li><a href="OnlineBid.do?appendixId=<%= appendixid %>&amp;fromPage=Appendix">Online Bid</a></li>
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
        		<li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li>
				<li><a href="MSPCCReportAction.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">MSP C/C Report</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid %>"><center>Search</center></a></td>
						
  		</li>
  	
	</ul>
</div>
<table cellpadding="0" cellspacing="0" border="0" width = "100%">
		<tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
							          		<div id="breadCrumb">&nbsp;</div>
							        </td>
						        </tr>
						        <tr><td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.appendixdashboardfor"/>&nbsp;<bean:write name="DocMSearchForm" property="msaName" />&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name="DocMSearchForm" property="appendixName" /></h2></td></tr> 
					      </table> 						
						
							<%-- <table cellpadding="0" cellspacing="0" border="0" width = "100%">
						        <tr> 
							          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage2</center></a></td>
							          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
							     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocMSearchForm" property="appendixId"/>&msaId=<bean:write name="DocMSearchForm" property="msaId"/>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
							          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
								</tr>
						        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
							          		<div id="breadCrumb">&nbsp;</div>
							        </td>
						        </tr>
						        <tr><td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.appendixdashboardfor"/>&nbsp;<bean:write name="DocMSearchForm" property="msaName" />&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name="DocMSearchForm" property="appendixName" /></h2></td></tr> 
					      </table> --%>
						</logic:notEqual>
					</logic:equal>
					<logic:notEqual name = "DocMSearchForm" property = "appendixStatusChecking" value = "Inwork">
					<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<%-- 	<tr>
								        <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )){%>
								        	<%if(appendixStatus.equals( "Cancelled" )){%>
								        		<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
								        	<%}else{ %>
												<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
								        	<%}%>
										<%}else{%>	
												<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
											<%}%>	
						
										<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
										<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
										<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
							        </tr> --%>
							    	<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>	    
						        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
								         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="DocMSearchForm" property ="msaId"/>&firstCall=true"><bean:write name  ="DocMSearchForm" property ="msaName"/></a></div>
								    </td>
				 				</tr>
				 			<tr>
									<td colspan="6">
										<h2>Supplement History: <bean:write name  ="DocMSearchForm" property ="appendixName"/></h2>
									</td>
							</tr>
			</table>
			</logic:notEqual>
	</logic:equal>
	<%-- <logic:equal name = "DocMSearchForm" property = "linkLibNameFromIlex" value = "Addendum">
	<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Job1</span></a>
  			<ul>
				
     				<li><a href="#">Change Status</a></li>
					
 				<li>
 					<a href="#"><span>Addendum</span></a>
					<ul>
						
     						<li><a href="javascript:addendumSendEmail();">Send</a></li>
							
     					<li>
     						<a href="#"><span>View</span></a>
     						<ul>
								
									<li><a href="javascript:addendumViewContractDocument('pdf');">PDF</a></li>
										
     						</ul>
     					</li>
     					<li><a href="ManageUpliftAction.do?ref=addendumUplift&amp;Job_Id=<c:out value='${requestScope.Job_Id }'/>&amp;Appendix_Id=<%= appendixid %>">Manage Uplifts</a></li>
					</ul>
				</li>
 				<li><a href="javascript:alert('Schedule Cannot Be Updated For A Default Job / Addendum')">Update Schedule</a></li>
				<li>
 					<a href="#"><span>Documents</span></a>
					<ul>
	 					<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.Appendix_Id }'/>&amp;entityType=Addendum&amp;function=viewHistory&amp;appendixid=<%= appendixid %>">Addendum History</a></li>
	 					<li>
		 					<a href="#"><span>Support Documents</span></a>
		 					<ul>
			 					<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.Appendix_Id }'/>&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement&amp;linkLibName=Addendum">Upload</a>
			 					</li><li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;linkLibName=Addendum&amp;function=supplementHistory">History</a>
							</li></ul>
						</li>
					</ul>
				</li>
				
					<li><a href="javascript:alert('An Addendum can only be uploaded when it is in the - Pending Customer Review - status.\nThis feature is only available to support a signed or markup Addendum originally generated from Ilex or \nan alternative document provided by the customer.')">Upload File</a></li>
					
  			</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:addendumManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:addendumSendStatusReport()">Send</a></li>
      			<li><a href="javascript:addendumManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>
 <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			      <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
					 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
					  <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
					  <a><span class="breadCrumb1">Document History</a>  
			    </div></td>
			</tr>
	      <tr><td colspan="6"><h2>Supplement History: <bean:write name  ="DocMSearchForm" property ="jobName"/></h2></td></tr>
		</tbody>
		</table>				
	
	
	
	 --%>
	
	
	
	
	<%-- <table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
				<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
				<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
				<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
		</tr>
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
		          <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
					 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
					  <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
					  <a><span class="breadCrumb1">Document History 1</a> 
   				</td>
		</tr>
		<tr><td colspan="6"><h2>Supplement History: <bean:write name  ="DocMSearchForm" property ="jobName"/></h2></td></tr>
	</table> --%>
	<%-- </logic:equal> --%>
<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">	
	<c:if test="${DocMSearchForm.linkLibNameFromIlex eq 'Job' && requestScope.jobType ne 'Default'}">
	<!--  Start Menu  -->
	<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Job</span></a>
  			<ul>
				
     				<li><a href="#">Change Status</a></li>
					
 				<li>
 					<a href="#"><span>Addendum</span></a>
					<ul>
						
     						<li><a href="javascript:addendumSendEmail();">Send</a></li>
							
     					<li>
     						<a href="#"><span>View</span></a>
     						<ul>
								
									<li><a href="javascript:addendumViewContractDocument(<c:out value='${requestScope.Appendix_Id }'/>,'pdf');">PDF</a></li>
										
     						</ul>
     					</li>
     					<li><a href="ManageUpliftAction.do?ref=addendumUplift&amp;Job_Id=<c:out value='${requestScope.Job_Id }'/>&amp;Appendix_Id="<c:out value='${requestScope.Appendix_Id }'/>>Manage Uplifts</a></li>
					</ul>
				</li>
 				<li><a href="javascript:alert('Schedule Cannot Be Updated For A Default Job / Addendum')">Update Schedule</a></li>
				<li>
 					<a href="#"><span>Documents</span></a>
					<ul>
	 					<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.Appendix_Id }'/>&amp;entityType=Addendum&amp;function=viewHistory&amp;appendixid=<c:out value='${requestScope.Appendix_Id }'/>">Addendum History</a></li>
	 					<li>
		 					<a href="#"><span>Support Documents</span></a>
		 					<ul>
			 					<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.Appendix_Id }'/>&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement&amp;linkLibName=Addendum">Upload</a>
			 					</li><li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;linkLibName=Addendum&amp;function=supplementHistory">History</a>
							</li></ul>
						</li>
					</ul>
				</li>
				
					<li><a href="javascript:alert('An Addendum can only be uploaded when it is in the - Pending Customer Review - status.\nThis feature is only available to support a signed or markup Addendum originally generated from Ilex or \nan alternative document provided by the customer.')">Upload File</a></li>
					
  			</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:addendumManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:addendumSendStatusReport()">Send</a></li>
      			<li><a href="javascript:addendumManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>
 <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			      <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
					 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.Appendix_Id }'/>"><c:out value='${requestScope.appendixName }'/></a>
					  <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
					  <a><span class="breadCrumb1">Document History</a>  
			    </div></td>
			</tr>
	      <tr><td colspan="6"><h2>Supplement History: <bean:write name  ="DocMSearchForm" property ="jobName"/></h2></td></tr>
		</tbody>
		</table>				
	
	
	
	
	
	
	<!-- End Of comment -->
	<%-- 	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
					<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
					<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
					<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
				  <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
					 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.Appendix_Id }'/>"><c:out value='${requestScope.appendixName }'/></a>
					  <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
					  <a><span class="breadCrumb1">Document History 2</a>
   				</td>
					
			</tr>
			<!-- <tr><td colspan="6"><h2>Supplement History: <bean:write name  ="DocMSearchForm" property ="jobName"/></h2></td></tr> -->
			<tr><td colspan="6" height = 20></td></tr>
		</table> --%>
	</c:if>
	<c:if test="${requestScope.jobType eq 'Default'}">
	
		<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Job</span></a>
  			<ul>
				
     				<li><a href="#">Change Status</a></li>
					
 				<li>
 					<a href="#"><span>Addendum</span></a>
					<ul>
						
     						<li><a href="javascript:addendumSendEmail();">Send</a></li>
							
     					<li>
     						<a href="#"><span>View</span></a>
     						<ul>
								
									<li><a href="javascript:addendumViewContractDocument('pdf');">PDF</a></li>
										
     						</ul>
     					</li>
     					<li><a href="ManageUpliftAction.do?ref=addendumUplift&amp;Job_Id=<c:out value='${requestScope.Job_Id }'/>">Manage Uplifts</a></li>
					</ul>
				</li>
 				<li><a href="javascript:alert('Schedule Cannot Be Updated For A Default Job / Addendum')">Update Schedule</a></li>
				<li>
 					<a href="#"><span>Documents</span></a>
					<ul>
	 					<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.Appendix_Id }'/>&amp;entityType=Addendum&amp;function=viewHistory&amp;appendixid=<c:out value='${requestScope.Appendix_Id }'/>">Addendum History</a></li>
	 					<li>
		 					<a href="#"><span>Support Documents</span></a>
		 					<ul>
			 					<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.Appendix_Id }'/>&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement&amp;linkLibName=Addendum">Upload</a>
			 					</li><li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;linkLibName=Addendum&amp;function=supplementHistory">History</a>
							</li></ul>
						</li>
					</ul>
				</li>
				
					<li><a href="javascript:alert('An Addendum can only be uploaded when it is in the - Pending Customer Review - status.\nThis feature is only available to support a signed or markup Addendum originally generated from Ilex or \nan alternative document provided by the customer.')">Upload File</a></li>
					
  			</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:addendumManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:addendumSendStatusReport()">Send</a></li>
      			<li><a href="javascript:addendumManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>
 <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			      <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
					 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.Appendix_Id }'/>"><c:out value='${requestScope.appendixName }'/></a>
					  <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
					  <a><span class="breadCrumb1">Document History</a>  
			    </div></td>
			</tr>
	      <tr><td colspan="6"><h2>Supplement History: <bean:write name  ="DocMSearchForm" property ="jobName"/></h2></td></tr>
		</tbody>
		</table>				
	
	
	
	
	
		<%-- <table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
					<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
					<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
					<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
						<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
					 	<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.Appendix_Id }'/>"><c:out value='${requestScope.appendixName }'/></a>
					  	<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
					  	<a><span class="breadCrumb1">Document History</a>
   						</td>
					</tr>
			</tr>
			<!-- <tr><td colspan="6"><h2>Supplement History: <bean:write name  ="DocMSearchForm" property ="jobName"/></h2></td></tr> -->
			<tr><td colspan="6" height = 20></td></tr>
		</table> --%>
	</c:if>
</c:if>
<c:if test="${requestScope.appendixStatus eq 'pmAppendix'}">
	 <table border="0" cellspacing="1" cellpadding="0" width="100%">
		<tr><td class="headerrow" height="19" width = "100%" >&nbsp;</td></tr>
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
         			
    				</td>
		</tr>
		<tr><td><h2>Supplement History</h2></td></tr>
	</table>
</c:if>
<logic:equal name ="DocMSearchForm" property = "linkLibNameFromIlex" value = "DocM">
<div id="search_formdiv">
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td><h2>Search Criteria</h2></td></tr>
		</table>
	
	<table width = "680" cellspacing="1" cellpadding="1" >	
		<tr>
			<td>
				<table width = "680" cellspacing="0" cellpadding="0" >	<tr>
						<td  class = "colDark" >Search in Library</td>
						<td  class = "colLight"> 
							 <html:select  name = "DocMSearchForm" property="selectedMSA" styleClass="select">
									<html:optionsCollection name = "dcdocm"  property = "liblist"  label = "label"  value = "value" />
								</html:select>   
						</td>
						<td class = "colLight" align="right" width='2'>
							<table border='0' 
								style="padding: 0px;border-spacing: 1px;"
									><tr class = "colLight" >
								<td valign="bottom" align='right'>
									<input type="checkbox" class="chkbx" 
										name="showAllColumns"
										value="<c:out value="${DocMSearchForm.showAllColumns}"/>" 
										<c:if test="${DocMSearchForm.showAllColumns == '1'}">
											checked="checked"
										</c:if> 
										onchange="setShowAllColumns();" id="showAllColumns"
									/>
								</td>
								<td nowrap valign="bottom" align='left' >Show All Columns In Result</td>
							</tr></table></td>
				</tr></table>
			</td>
		</tr>
	</table>
	<TABLE  cellspacing="0" cellpadding="1">
		<TR id="tab" >
				<TD width="680">
					<TABLE id="filterTable" width="100%" cellspacing="1" cellpadding="1" >						
						<TR>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> S.No.</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Field Name</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Condition</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap> Value</TD>
							<TD bgcolor="#EEEEEE" class = "Ntryb"  nowrap align="center"> Delete</TD>
						</TR>
							<c:forEach var="colItr" items="${DocMSearchForm.selectedFilterColumns}" varStatus="outerIndex" >
								<input type="hidden" name="filterId" value="<c:out value='${DocMSearchForm.filterId[outerIndex.index]}'/>">
								 <input type="hidden" name="fieldTypeId" value="<c:out value='${DocMSearchForm.fieldTypeId[outerIndex.index]}'/>">
								<TR>
									<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>> Where</TD>
									<td <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
										 <select  name = "selectedFilterColumns" Class="select" onchange="chngCtrlsSearch(this,'filterTable');">
										 <c:forEach var="list" items="${requestScope.filterColumnList}" varStatus="innerIndex">
											<option  value="<c:out value='${list[10]}' />" <c:if test='${DocMSearchForm.selectedFilterColumns[outerIndex.index] eq list[10]}'>selected = "selected"</c:if> ><c:out value="${list[1]}" /></option>
										 </c:forEach>
										</select>   
									</td>
									<c:if test="${DocMSearchForm.fieldTypeId[outerIndex.index] eq '0' || (DocMSearchForm.fieldTypeId[outerIndex.index] >= 6 && DocMSearchForm.fieldTypeId[outerIndex.index] <= 12 )}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <select  name = "filterCondition"  Class="select" >
											 	<option value="=" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq '='}"> selected = "selected" </c:if>> Equals</option>
											 	<option value="!=" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq '!='}"> selected = "selected" </c:if>> Not Equals</option>
											 	<option value="greaterThan" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'greaterThan'}"> selected = "selected" </c:if>> Greater than</option>
											 	<option value="lessThan" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'lessThan'}"> selected = "selected" </c:if>> Less Than</option>
											 	<option value="like" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'like'}"> selected = "selected" </c:if>> Contains</option>
											 	<option value="Not like" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'Not like'}"> selected = "selected" </c:if>> Not Contains</option>
											 	<option value="is Empty" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'is Empty'}"> selected = "selected" </c:if>> Is Empty</option>
											 	<option value="is null" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'is null'}"> selected = "selected" </c:if>> Is Null</option>
											</select>   
										</TD>
									</c:if>
									<c:if test="${DocMSearchForm.fieldTypeId[outerIndex.index] eq '4'}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <select  name = "filterCondition"  Class="select" >
											 	<option value="=" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq '='}"> selected = "selected" </c:if>> Equals</option>
											 	<option value="!=" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq '!='}"> selected = "selected" </c:if>> Not Equals</option>
											 	<option value="greaterThan" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'greaterThan'}"> selected = "selected" </c:if>> Greater than</option>
											 	<option value="lessThan" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'lessThan'}"> selected = "selected" </c:if>> Less Than</option>
											 	<option value="is Empty" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'is Empty'}"> selected = "selected" </c:if>> Is Empty</option>
											 	<option value="is null" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'is null'}"> selected = "selected" </c:if>> Is Null</option>
											</select>   
										</TD>
									</c:if>
									<c:if test="${DocMSearchForm.fieldTypeId[outerIndex.index] eq '1' || DocMSearchForm.fieldTypeId[outerIndex.index] eq '2' || DocMSearchForm.fieldTypeId[outerIndex.index] eq '5'}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <select  name = "filterCondition"  Class="select" >
											 	<option value="=" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq '='}"> selected = "selected" </c:if>> Equals</option>
											 	<option value="!=" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq '!='}"> selected = "selected" </c:if>> Not Equals</option>
											 	<option value="greaterThan" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'greaterThan'}"> selected = "selected" </c:if>> Greater than</option>
											 	<option value="lessThan" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq 'lessThan'}"> selected = "selected" </c:if>> Less Than</option>
											</select>   
										</TD>
									</c:if>
									<c:if test="${DocMSearchForm.fieldTypeId[outerIndex.index] eq '3' }">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   
											 <select  name = "filterCondition"  Class="select" >
											 	<option value="=" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq '='}"> selected = "selected" </c:if>> Equals</option>
											 	<option value="!=" <c:if test="${DocMSearchForm.filterCondition[outerIndex.index] eq '!='}"> selected = "selected" </c:if>> Not Equals</option>
											</select>   
										</TD>
									</c:if>
									<c:if test="${DocMSearchForm.fieldTypeId[outerIndex.index] eq '4'}">
										<c:if test="${outerIndex.last == true && outerIndex.count eq '1'}">
											<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><input class="text" type="text" name="filterType" size="40" readonly="true" maxlength="40"  value="<c:out value='${DocMSearchForm.filterType[outerIndex.index]}'/>"/><img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].filterType, document.forms[0].filterType, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img></TD>
										</c:if>
										<c:if test="${! (outerIndex.last ==true && outerIndex.count eq '1')}">
											<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><input class="text" type="text" name="filterType" size="40" readonly="true" maxlength="40"  value="<c:out value='${DocMSearchForm.filterType[outerIndex.index]}'/>"/><img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].filterType[<c:out value='${outerIndex.index}'/>], document.forms[0].filterType[<c:out value='${outerIndex.index}'/>], 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img></TD>
										</c:if>
									</c:if>
									<c:if test="${DocMSearchForm.fieldTypeId[outerIndex.index] ne '4'}">
										<TD valign=top <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><input class="text" type="text" name="filterType" size="40"  maxlength="40" value="<c:out value='${DocMSearchForm.filterType[outerIndex.index]}'/>"/></TD>
									</c:if>
									<TD valign=top  align="center" <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><img  align="center" src="docm/images/delete.gif" name="filterImage" onclick="checkLastRow();Rowdelete('filterTable',this)"/></TD>
								</TR>
								<c:if test="${!outerIndex.last}">	
									<tr>
										<td colspan=5 <c:if test="${(outerIndex.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(outerIndex.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
										  <select  name = "logicalCondition"  Class="select" >
										 	<option value="and" <c:if test="${DocMSearchForm.logicalCondition[outerIndex.index] eq 'and'}"> selected = "selected" </c:if>> AND</option>
										 	<option value="or" <c:if test="${DocMSearchForm.logicalCondition[outerIndex.index] eq 'or'}"> selected = "selected" </c:if>> OR</option>
										  </select>
									  	</td>   
								 	</tr> 	
								</c:if>								
							</c:forEach>
						</TABLE>
							<TABLE cellspacing="1" cellpadding="1">
								<TR>
									<td class = "colLight" align="left" width = "680">
										<TABLE  cellspacing="0" cellpadding="0" width='100%'><tr>
											<td align='left'>
												<html:button property = "addMore" styleClass="button_c" onclick="checkNoRow();addFilterCriteriaRow();">Add Row &darr;</html:button>
											</td>
											<td align='left'>
												<html:submit property="searchNow" styleClass="button_c" onclick ="return searchAgain();" styleId="searchNow">Search Now</html:submit>
											</td>
										</tr></TABLE>
									</td>
								</TR>
							</TABLE>
					</TD>
			</TR>
		</TABLE>
	</div>
	</logic:equal>
	<TABLE  cellspacing="0" cellpadding="1">
		<TR>
				<TD width="680">
							<TABLE cellspacing="1" cellpadding="1" width='100%'>
								<c:if test="${not empty requestScope.documentList}">
									<logic:equal name ="DocMSearchForm" property = "linkLibNameFromIlex" value = "DocM">
									<TR>
										<td width = "680">
											<TABLE cellspacing="0" cellpadding="0" width='100%'><tr>
												
												<td align='left' width='80%'>
													<h2>Search Result:</h2>
												</td>
												<td 'align='right'>
													<html:button styleId="showhidesearch1" property = "showhidesearch" styleClass="button_c" onclick="showHideSearchForm();">Hide Search Criteria</html:button>
												</td>
											</tr></TABLE>										
										</td>
									</TR>
									</logic:equal>
								</c:if>
								<logic:equal name ="DocMSearchForm" property = "linkLibNameFromIlex" value = "DocM">
								<tr>
									<td><c:if test="${not empty requestScope.libMap}">
										<table border="0" cellspacing="1" cellpadding="1" align="left"  width = "10%">
											<tr align="left" >
												<td  align="left" bgcolor="#EEEEEE" class = "Ntryb"  nowrap width = "100%" colspan='2'>Document Search Summary</td>
											</tr>
											<c:forEach var="map" items="${requestScope.libMap}"
												varStatus="status">
												<tr>
													<td  <c:if test="${(status.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(status.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   &nbsp;&nbsp;<c:out value="${map.key}" /></td>
													<td  align="right" <c:if test="${(status.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(status.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>   &nbsp;&nbsp;<c:out value="${map.value}" /> documents</td>
												</tr>
											</c:forEach>
										</table>
									</c:if></td>
								</tr>
								</logic:equal>
							</TABLE>
					</TD>
			</TR>
		</TABLE>
		<div id="loadingdisplay" style="padding: 4px;margin: 0px;">&nbsp;&nbsp;<img src='./docm/images/waiting_tree.gif'>
		</div>
		 
		<table width='100%' border="0" align="left" cellpadding="1" cellspacing="1">
          <c:set var="docid" value="0"/>
          <c:set var="fileId" value="0"/>
          <c:set var="MimeType" value=""/>
          <c:set var="fileName" value=""/>
          <c:set var="viewOnWeb" value=""/>
          <c:set var="controlType" value=""/>
        <tr valign = "top"><td valign = "top">
			  <table  cellpadding="1" cellspacing="1">
				<tr>
					<td valign = "top">
				 <c:forEach items="${requestScope.documentList}" var="row" varStatus="status" >
				           <c:if test="${status.index== '0'}">
					           <tr>
									<c:forEach items="${row}" var="rowfield" varStatus="index">
										<c:if test="${index.index gt '5'}">
											<c:if test="${index.index eq '6'}">
												<td class="Ntryb" colspan="2"><c:out value="${rowfield}" /></td>
											</c:if>
											<c:if test="${index.index ne '6'}">
												<td class="Ntryb"><c:out value="${rowfield}" /></td>
											</c:if>
										</c:if>
										<c:if test="${index.last}">
									      <c:set var="colcount" value="${index.count}"/>
									    </c:if>
									</c:forEach>
								</tr>
						    </c:if>
						    <c:set var="idx" value="0"/>
						    
						    <c:if test="${status.index != '0'}">
					           <tr id="bid">
						           <c:forEach items="${row}" var="rowfield" varStatus="ind">
							            <c:if test="${ind.index == '0'}"><c:set var="docid" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '1'}"><c:set var="fileId" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '2'}"><c:set var="MimeType" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '3'}"><c:set var="fileName" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '4'}"><c:set var="libId" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '5'}"><c:set var="statusType" value="${rowfield}"/></c:if>
										<c:if test="${ind.index gt '5'}">
											<c:if test="${ind.index eq '6'}">
												<td 
													<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
													<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
													 >
													 <c:set var="source" value=""/>
												    <c:if test="${empty param.Source}">
												    	<c:set var="source" value="${requestScope.SourceType}"/>
												    </c:if>
												    <c:if test="${empty requestScope.SourceType}">
												    	<c:set var="source" value="${param.Source}"/>
												    </c:if>
												    <c:if test="${empty requestScope.SourceType && empty param.Source}">
												    	<c:set var="source" value="DocM"/>
												    </c:if>
													 <c:if test="${DocMSearchForm.linkToDoc eq 'Yes'}">
														<input type="radio" name="LinkDoc" onclick="getValue('DocumentAdd.do?hmode=viewDocument&libId=<c:out value="${libId}"/>&docId=<c:out value="${docid}"/>&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&Source=<c:out value='${source}'/>','<c:out value="${docid}"/>');" value="<c:out value="${docid}"/>">
													</c:if>
													<c:if test="${param.page eq 'mpo' && empty param.dev}">
														<input type="checkbox" name="mpoSearch" class="chkbx" style="margin-bottom: 1px;" value="<c:out value="${docid}"/>" />
													</c:if>
													 <c:if test="${param.page eq 'mpo' && not empty param.dev}">
														<input type="radio" name="poDevDocId" onclick="insertDocForPo('<c:out value="${param.devId}"/>','<c:out value="${docid}"/>');" value="<c:out value="${docid}"/>">
													</c:if>
													<c:set var="originalFieldValue" value="${rowfield}" scope="session"/>
													<%
														//********************************
														String originalFieldValue = (String) session.getAttribute("originalFieldValue");
														String displayFieldValue = "";
														if(originalFieldValue!=null && originalFieldValue.length()>40) {
															displayFieldValue = originalFieldValue.subSequence(0,37) + "...";
															%>
															<p title="<%=originalFieldValue%>">
																<%=displayFieldValue%>
															</p>
															<%															
														} else {
															%>
															<c:out value="${rowfield}"/>
															<%															
														}
														//********************************
													%>
												</td>
												<td align="center" 
									       			<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
									       			<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
									       			 >
													[
													<c:if test="${not empty fileId}">
														<a href = "#" onclick="ViewFile('DocumentMaster.do?hmode=openFile&fileId=<c:out value="${fileId}"/>&libId=<c:out value="${libId}"/>')">Download</a>
													</c:if>
													<c:if test="${empty fileId}">Download1</c:if>
													
													<c:if test="${(DocMSearchForm.linkToDoc ne 'Yes' && param.page ne 'mpo' && DocMSearchForm.sourceType ne 'Ilex')}">
													|
													<a href="#" onclick = "submitForm('DocumentAdd.do?hmode=viewDocument&statusType=<c:out value="${statusType}"/>&libId=<c:out value="${libId}"/>&docId=<c:out value="${docid}"/>&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&Source=<c:out value="${source}"/>');">Details</a>&nbsp;|
													<a href ='#' onclick = "submitForm('DocumentHistory.do?hmode=unspecified&documentId=<c:out value="${docid}"/>&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&Source=<c:out value="${source}"/>');">Revisions</a>&nbsp;|
													<c:if test="${statusType ne 'Deleted' && statusType ne 'Superseded' }"><a  href = "#" onclick = "submitForm('DocumentAdd.do?hmode=modifyDocument&libId=<c:out value="${libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&Source=<c:out value='${source}'/>');">Modify</a></c:if><c:if test="${statusType eq 'Deleted' || statusType eq 'Superseded'}">Modify</c:if>&nbsp;| 
													<c:if test="${statusType ne 'Deleted' && statusType ne 'Superseded'}"><a href = "#" onclick = "submitForm('DocumentAdd.do?hmode=viewDocument&libId=<c:out value="${libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&delete=DeleteNow&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&Source=<c:out value='${source}'/>');">Delete</a></c:if><c:if test="${statusType eq 'Deleted' || statusType eq 'Superseded'}">Delete</c:if>
													]
													</c:if>
													<c:if test="${(DocMSearchForm.sourceType eq 'Ilex' && DocMSearchForm.entityType ne 'Job' && DocMSearchForm.entityType ne 'Addendum')}">
													|
													<a href = "DocumentAdd.do?hmode=viewDocument&statusType=<c:out value="${statusType}"/>&libId=<c:out value="${libId}"/>&docId=<c:out value="${docid}"/>&entityId=<c:out value="${DocMSearchForm.entityId}"/>&entityType=<c:out value="${DocMSearchForm.entityType}"/> Supporting Documents&Source=Ilex&linkLibName=<c:out value="${DocMSearchForm.entityType}"/>">Details</a>&nbsp;|
													<a href = 'DocumentHistory.do?hmode=unspecified&documentId=<c:out value="${docid}"/>&entityId=<c:out value="${DocMSearchForm.entityId}"/>&entityType=<c:out value="${DocMSearchForm.entityType}"/> Supporting Documents&Source=Ilex&linkLibName=<c:out value="${DocMSearchForm.entityType}"/>'>Revisions</a>&nbsp;|
													<c:if test="${statusType ne 'Deleted' && statusType ne 'Superseded' }"><a  href = "DocumentAdd.do?hmode=modifyDocument&libId=<c:out value="${libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&entityId=<c:out value="${DocMSearchForm.entityId}"/>&entityType=<c:out value="${DocMSearchForm.entityType}"/> Supporting Documents&Source=Ilex&linkLibName=<c:out value="${DocMSearchForm.entityType}"/>">Modify</a></c:if><c:if test="${statusType eq 'Deleted' || statusType eq 'Superseded'}">Modify</c:if>&nbsp;| 
													<c:if test="${statusType ne 'Deleted' && statusType ne 'Superseded'}"><a href = "DocumentAdd.do?hmode=viewDocument&libId=<c:out value="${libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&delete=DeleteNow&entityId=<c:out value="${DocMSearchForm.entityId}"/>&entityType=<c:out value="${DocMSearchForm.entityType}"/> Supporting Documents&Source=Ilex&linkLibName=<c:out value="${DocMSearchForm.entityType}"/>">Delete</a></c:if><c:if test="${statusType eq 'Deleted' || statusType eq 'Superseded'}">Delete</c:if>
													]
													</c:if>
													<c:if test="${(DocMSearchForm.sourceType eq 'Ilex' && ( DocMSearchForm.entityType eq 'Job' || DocMSearchForm.entityType eq 'Addendum'))}">
													|
													<a href = "DocumentAdd.do?hmode=viewDocument&statusType=<c:out value="${statusType}"/>&libId=<c:out value="${libId}"/>&docId=<c:out value="${docid}"/>&entityId=<c:out value="${DocMSearchForm.entityId}"/>&entityType=Deliverables&Source=Ilex&linkLibName=<c:out value="${DocMSearchForm.entityType}"/>">Details</a>&nbsp;|
													<a href = 'DocumentHistory.do?hmode=unspecified&documentId=<c:out value="${docid}"/>&entityId=<c:out value="${DocMSearchForm.entityId}"/>&entityType=Deliverables&Source=Ilex&linkLibName=<c:out value="${DocMSearchForm.entityType}"/>'>Revisions</a>&nbsp;|
													<c:if test="${statusType ne 'Deleted' && statusType ne 'Superseded' }"><a  href = "DocumentAdd.do?hmode=modifyDocument&libId=<c:out value="${libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&entityId=<c:out value="${DocMSearchForm.entityId}"/>&entityType=Deliverables&Source=Ilex&linkLibName=<c:out value="${DocMSearchForm.entityType}"/>">Modify</a></c:if><c:if test="${statusType eq 'Deleted' || statusType eq 'Superseded'}">Modify</c:if>&nbsp;| 
													<c:if test="${statusType ne 'Deleted' && statusType ne 'Superseded'}"><a href = "DocumentAdd.do?hmode=viewDocument&libId=<c:out value="${libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&delete=DeleteNow&entityId=<c:out value="${DocMSearchForm.entityId}"/>&entityType=Deliverables&Source=Ilex&linkLibName=<c:out value="${DocMSearchForm.entityType}"/>">Delete</a></c:if><c:if test="${statusType eq 'Deleted' || statusType eq 'Superseded'}">Delete</c:if>
													]
													</c:if>
									            </td>
											</c:if>
											<c:if test="${ind.index gt '6'}">
												<td  
													<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
													<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
													 > 
													<c:set var="originalFieldValue" value="${rowfield}" scope="session"/>
													<%
														//********************************
														String originalFieldValue = (String) session.getAttribute("originalFieldValue");
														String displayFieldValue = "";
														if(originalFieldValue!=null && originalFieldValue.length()>40) {
															displayFieldValue = originalFieldValue.subSequence(0,37) + "...";
															%>
															<p title="<%=originalFieldValue%>">
																<%=displayFieldValue%>
															</p>
															<%															
														} else {
															%>
															<c:out value="${rowfield}"/>
															<%															
														}
														//********************************
													%>
												</td>
											</c:if>
										</c:if>
						       	</c:forEach>
					       </tr>
					   </c:if>
					   <c:if test="${status.last}">
					      <c:set var="rowcount" value="${status.count}"/>
					    </c:if>
			         </c:forEach>
			        </td>
			      </tr>
			     
				<c:if test="${rowcount lt '2'}">
					<tr>
						<td class='Ntexteleftalignnowrap' colspan='<c:out value="${colcount}"/>'>No document found!</td>
					</tr>
    			</c:if>
			    </table>
		 <c:if test="${(not empty DocMSearchForm.searchNow) && (not (rowcount lt '2'))}">
		     <table valign="bottom"  width="680"  cellspacing="0" cellpadding="0" >
				<TR>
					<TD align="left" valign="bottom" width="50%" onclick="showbidback()"  id="bidback" class="hypertext"><a href="#" title="Previous Page">&laquo;back</a></TD>
					<TD align="right" valign="bottom" width="50%" onclick="showbidmore()" id="bidmore" class="hypertext"><a href="#" title="Next Page">more&raquo;</a></TD>				
				</TR>	
			</table>
		</c:if>
        </td>
       </tr>
        <c:if test="${(param.page eq 'mpo')}">
        	<tr >
        		<td>
        			<table valign="bottom"  width="680"  cellspacing="0" cellpadding="0" >
        				<tr>
        					<td>
        						<c:if test="${param.page eq 'mpo' && empty param.dev}"><c:if test="${not (rowcount lt '2')}"><input type="button" name="save" onclick='sendToMpoPage();' class="button_c" value="Select" />&nbsp;&nbsp;&nbsp;&nbsp;</c:if></c:if>
        						<input type="button" name="newUpload" onclick="openAddPage();" class="button_c" value="New Upload" />
        					</td>
        				</tr>
        			</table>
        		</td>
        	</tr>
        </c:if>

	</table>
	<logic:equal name = "DocMSearchForm" property = "linkLibNameFromIlex" value = "MSA">
		<%@ include  file="/MSAMenuScript.inc" %>
	</logic:equal>
	<logic:equal name = "DocMSearchForm" property = "linkLibNameFromIlex" value = "Appendix">
		<logic:equal name = "DocMSearchForm" property = "linkLibNameFromIlex" value = "Appendix">
				<logic:equal name = "DocMSearchForm" property = "appendixStatusChecking" value = "Inwork">
					<logic:equal name = "DocMSearchForm" property = "appendixType" value = "3">
						<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
					</logic:equal>
					<logic:notEqual name = "DocMSearchForm" property = "appendixType" value = "3">
						<%@ include  file="/AppedixDashboardMenuScript.inc" %>
					</logic:notEqual>
				</logic:equal>
				<logic:notEqual name = "DocMSearchForm" property = "appendixStatusChecking" value = "Inwork">
					<%@ include  file="/AppendixMenuScript.inc" %>
				</logic:notEqual>
			</logic:equal>
	</logic:equal>
	<logic:equal name = "DocMSearchForm" property = "linkLibNameFromIlex" value = "Addendum">
		<%@ include  file="/AddendumMenuScript.inc" %>
	</logic:equal>
	<c:if test="${DocMSearchForm.linkLibNameFromIlex eq 'Job' && requestScope.jobType ne 'Default'}">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
	

<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
		<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/DefaultJobMenuScript.inc" %>
</c:if>
	
</c:if>
	</html:form>		
	<script>
		document.getElementById("loadingdisplay").innerHTML="";
		document.getElementById("loadingdisplay").style.display="none";
	</script>
</body>
</html>