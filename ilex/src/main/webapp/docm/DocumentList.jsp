<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<script type="text/javascript" language="JavaScript1.2" src="docm/javascript/prototype.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ILEX</title>
<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<script>
function ViewFile(url)
{
    document.forms[0].target = "_self";
	document.forms[0].action = url;
	document.forms[0].submit();
	return true;
}
</script>
<script>
function ShowDiv(){
	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";
	return false;
}
function hideDiv(){
	document.getElementById("filter").style.visibility="hidden";
	return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
}
function showDocumentsWithStatus(type){
	if(type == 'SD')
		document.forms[0].action = 'DocumentMaster.do?hmode=unspecified&status=marked&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />&viewId=<c:out value="${param.viewId}" />';
	else if(type == 'AD')
		document.forms[0].action = 'DocumentMaster.do?hmode=unspecified&status=approved&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />&viewId=<c:out value="${param.viewId}" />';
	document.forms[0].submit();
	return true;	
}
function showDocumentsWithView(viewId){
	document.forms[0].action = 'DocumentMaster.do?hmode=unspecified&status=<c:out value="${param.status}" />&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />'
		+ '&viewId=' + viewId;
	document.forms[0].submit();
	return true;	
}
function showData()
{
	document.forms[0].action = 'DocumentMaster.do?hmode=unspecified&status=<c:out value="${param.status}" />&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />&isClick=1&viewId=<c:out value="${DocumentMasterBean.viewId}"/>';
	alert(document.forms[0].action);
	document.forms[0].submit();
	return true;	
}
/*function toggleImage(obj1,obj2,obj3,obj4)
{
	if(document.getElementById(obj2).style.display=="none"){
		obj1.src = "./images/Collapse.gif";
		document.getElementById(obj2).style.display="";
		showData(obj1,obj2,obj3,obj4);
		}
		else{
			
			obj1.src = "./images/Expand.gif";
			document.getElementById(obj2).style.display="none"
		}
}*/
function toggleImage(obj1,obj2,obj3,obj4,obj5)
{
	if(document.getElementById(obj5).style.display=="none"){
		document.getElementById(obj1).src = "./images/Collapse.gif";
		document.getElementById(obj1).title = "Collapse";
		document.getElementById(obj5).style.display="";
		document.getElementById(obj2).style.display="";
		document.getElementById(obj2).innerHTML="<img src='./docm/images/waiting_tree.gif'>";
		//var obj4_Value = obj4.toString();
		//obj4_Value = obj4_Value.replace("'","&apos;");
		showData(obj1,obj2,obj3,obj4,obj5);
		//alert('this is a check for checking'.replace(/check/g,'ok'));
	}
	else{
		document.getElementById(obj1).src  = "./images/Expand.gif";
		document.getElementById(obj1).title = "Expand";
		document.getElementById(obj5).style.display="none";
		document.getElementById(obj2).style.display="none";
	}
}
function showData(obj1,obj2,obj3,obj4,obj5)
{
	var url = 'DocumentMaster.do?';
	var tempArray = obj4.split("&");
	obj4 = escape(obj4);
	obj1 = escape(obj1);
	obj3 = escape(obj3);
	obj2 = escape(obj2);
	obj5 = escape(obj5);
	var pars = 'hmode=unspecified&status=<c:out value="${param.status}" />&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />&isClick=1&viewId=<c:out value="${DocumentMasterBean.viewId}"/>&divId='+obj2+'&fieldName='+obj3+'&fieldVal='+obj4+'&imgId='+obj1+'&rowId='+obj5;
	var myAjax = new Ajax.Updater(
					{success: obj2}, 
					url, 
					{
						method: 'get', 
						parameters: pars, 
						onFailure: reportError
					});
	
	
}
function reportError(request)
{
	document.getElementById(obj2).innerHTML="<font color='brown' size='1'>Connection/data failure! Please retry!</font>";
}
</script>
</head>

<body>
<html:form method="post" action="DocumentMaster" name="DocumentMasterBean" type="com.mind.docm.formbean.DocumentMasterBean">
<html:hidden property="hmode" value=""/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="top" width="100%">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="headerrow" height="19" width="100%">&nbsp;</td>
				</tr>
				<tr>
					<td background="images/content_head_04.jpg" height="21"
						width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Library</a> <a
						href="#"><c:out value="${requestScope.libName}" /></a> <a href="#"><c:out value="${requestScope.viewName}" /></a></div>
					</td>
				</tr>
				
				<tr>
					<td colspan="6"><%-- 		<h2>Documents&nbsp;in&nbsp;Library&nbsp;<c:out value="${param.libName}"/>&nbsp;for&nbsp;Default View<u></u></h2>  --%>
					<h2>Document List</h2>
					</td>
				</tr>
				<c:choose>
					<c:when test="${! empty  requestScope.Comment}">
						<TR>
							<TD  class="message" height=5 />
								<c:out value="${requestScope.Comment}"/>
							</TD>
						</TR>
					</c:when>	
				</c:choose>	
			</table>
			</td>

			<td width="35" valign="top"><img id="content_head_02"
				src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
			<td valign="top" width="155">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="155" height="39" class="headerrow" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="150">
							<table width="100%" cellpadding="0" cellspacing="3" border="0">
								<tr>
									<td><img src="images/disbmonth.gif" width="31"
										id="Image10" height="30" border="0" /></td>
									<td><img src="images/disbweek.gif" name="Image11"
										width="31" height="30" border="0" id="Image11" /></td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td height="21" background="images/content_head_04.jpg" width="140">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td nowrap="nowrap" colspan="2">
							<div id="featured1">
							<div id="featured">
							<li name="closeNow" id="closeNow"><a
								href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My
							Views <img src="images/showFilter.gif" title="Open View"
								border="0" onClick="ShowDiv();" /></a><a href="javascript:void(0)"
								class="imgstyle"><img src="images/offFilter2.gif" border="0"
								title="Close View" onClick="hideDiv();" /></a></li>
							</div>
							</div>
							<span>
							<div id="filter" class="divstyle">
							<table width="250" cellpadding="0" class="divtable">
								<tr>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Views</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText">
											   <c:forEach items="${requestScope.libraryView}" var="vwLibrary" varStatus="status" >
													<input name="viewIds" id="<c:out value="${vwLibrary.libraryViewId}"/>" type="radio" 
														onclick = "showDocumentsWithView('<c:out value="${vwLibrary.libraryViewId}"/>');" 
												   		<c:if test="${DocumentMasterBean.viewId eq vwLibrary.libraryViewId}">
															checked="true" 
														 </c:if>
													/><c:out value="${vwLibrary.libraryViewName}"/><br>
										       </c:forEach>
											</td>
										</tr>
									</table>
									</td>
									<td><img src="images/filterLine.gif" width="1px"
										height="30" class="imagefilter" /></td>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Show Status</td>
										</tr>
										<tr>
											<td class="tabNormalText">
											 <c:if test="${param.status eq 'approved'}">
											 	<input name="ASD" type="radio"  onclick = "showDocumentsWithStatus('AD');" checked = "true" />Approved / Draft<br>
												<input name="ASD" type="radio"  onclick = "showDocumentsWithStatus('SD');" />Superseded / Deleted
											 </c:if>
											<c:if test="${param.status eq 'marked'}">
											 	<input name="ASD" type="radio"  onclick = "showDocumentsWithStatus('AD');" />Approved / Draft<br>
												<input name="ASD" type="radio"  onclick = "showDocumentsWithStatus('SD');" checked = "true"/>Superceded / Deleted
											</c:if>
											</td>
										</tr>
									</table>
									</td>
								</tr>
							</table>
							</div>
							</span></td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

	
    <table border="0" width='100%' align="left" cellpadding="1" cellspacing="1">
          <c:set var="docid" value="0"/>
          <c:set var="fileId" value="0"/>
          <c:set var="MimeType" value=""/>
          <c:set var="fileName" value=""/>
          <c:set var="viewOnWeb" value=""/>
          <c:set var="controlType" value=""/>
           <c:set var="fn" value=""/>
           <c:set var="fv" value=""/>
          <c:forEach items="${requestScope.documentGroupNodes}" var="dgn" varStatus="index">
          <c:if test="${index.index  eq '0' }">
	          <c:set var="fn" value="${dgn.fieldName}" />
	          <c:set var="fv" value="${dgn.fieldValue}" />
	      </c:if>
			 <tr width='100%' align='left'>
	          	<td align='left' width='2'>
	          		<c:set var="originalFieldValue" value="${dgn.fieldValue}" scope="session"/>
	          		<%
						//********************************
						String originalFieldValue = (String) session.getAttribute("originalFieldValue");
		          		originalFieldValue = originalFieldValue.replaceAll("'","\\\\'");
						//********************************
					%>
	          		<img id="img1" src="images/Expand.gif" 
	          			title="Expand"
	          			onclick="toggleImage('img1','root1','<c:out value="${dgn.fieldName}" />','<%=originalFieldValue%>','row1')"/>
	          	</td> 
				<td class='Ntexteleftalignnowrap' align='left'><c:out value="${dgn.fieldName}" /></td>
			 </tr>
			 <tr width='100%' align='left' style="display:none" id="row1">
          		<td>&nbsp;
          		</td>
          		<td>
          			<div id="root1" style="display:none" valign = "top" align = "left">
          			</div>
          		</td>
	          </tr>
		 </c:forEach>
		 <script>
		 	toggleImage('img1','root1','<c:out value="${fn}" />','<c:out value="${fv}" />','row1');
		 </script>
          <%--<tr>
          	<td>
          		<img src="images/Expand.gif" onclick="showData(this,'root1')"/>
          	</td> 
          	<td>
          		Documents
          	</td>                   
          </tr>
          <tr>
          		<td>
          		
          		</td>
          		<td>
          			<div id="root1">
          			</div>
          		</td>
          </tr>--%>
          
         
     </table>
    <%-- 
     <c:if test="${param.status eq 'approved'}">
  		 <td colspan="2"><a href="DocumentMaster.do?hmode=unspecified&status=marked&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />" class="textlink">Superseded/Deleted</a> </td>
    </c:if>
    <c:if test="${param.status eq 'marked'}">
    <td colspan="2"><a href="DocumentMaster.do?hmode=unspecified&status=approved&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />" class="textlink">Approved/Draft</a> </td>
    </c:if>
    </tr>
    --%>
    <%-- 
     <tr>
        <td colspan="2">
        <table width="220" border="0" align="right" cellpadding="2" cellspacing="0">
        <tr>
         <td width="72" align="center"><a href="#" class="textlink">Previous</a> </td>
          <td width="81" align="right" class="prenext">Page 1 of 3 </td>
          <td width="55" align="right"><a href="#" class="textlink">Next</a></td>
        </tr>
      </table></td>
    </tr>
   --%> 
</html:form>
</body>
</html>
