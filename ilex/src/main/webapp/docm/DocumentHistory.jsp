<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document History</title>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
   <script language = "JavaScript" src = "javascript/functions.js"></script>
	<link rel="stylesheet" href="styles/style.css" type="text/css">
	<meta http-equiv="X-UA-Compatible" content="IE=9"/>

<script>
function ViewFile(url)
{
    document.forms[0].target = "_self";
	document.forms[0].action = url;
	document.forms[0].submit();
	return true;
}
function submitForm(url){
		document.forms[0].action=url;
		document.forms[0].submit();
	}
</script>
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include file = "/NMenu.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>  
</head>
<body >
<html:form action="DocumentHistory">
<html:hidden name="DocumentHistoryBean" property="linkLibNameFromIlex"/>
<logic:equal name = "DocumentHistoryBean" property = "type" value = "Appendix">
	<logic:equal name = "DocumentHistoryBean" property = "appendixStatusChecking" value = "Inwork">
	
		<logic:equal name = "DocumentHistoryBean" property = "appendixTypeId" value = "3">
			<%@ include  file="/NetMedXDashboardMenu.inc" %>
		</logic:equal>
		
	</logic:equal>
</logic:equal>
<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>		 
		
		<c:if test="${requestScope.SourceType eq 'Ilex'}">
			<logic:equal name = "DocumentHistoryBean" property = "type" value = "MSA">
			
		
			
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
								<tr>
									<%-- <%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )){%>
											 <%if(msastatus.equals( "Cancelled" )){%>
									        	<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
									        <%}else{ %>
												<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
									        <%}%>
										  <%}else{%>
					                 			 <td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
											<%}%>
											<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
											<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
											<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
							<%@ include file = "/Menu_MSA_Appendix_Msa.inc" %> 
								</tr>
								<tr>
								          <td background="images/content_head_04.jpg" height="21" colspan="7" width="100%">
							          		<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
							         	 </td>
								</tr>
								<tr>
										<td colspan="6">
											<h2><c:out value="${requestScope.libName}" /> Document History: <bean:write name  ="DocumentHistoryBean" property ="msaName"/></h2>
										</td>
								</tr>
					</table>
   					
			</logic:equal>
			<logic:equal name = "DocumentHistoryBean" property = "type" value = "Appendix">
				<logic:equal name = "DocumentHistoryBean" property = "appendixStatusChecking" value = "Inwork">
					<logic:equal name = "DocumentHistoryBean" property = "appendixTypeId" value = "3">
<div id="menunav">
  
<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span>
        	</a>
        	
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
      
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	

				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
        	
        		
				<!-- <li><a href="ReportScheduleWindow.do?report_type=redline&typeid=3970&fromPage=NetMedX">Redline Report</a></li>
				<li><a href="report_type=detailsummary&typeid=3970&fromPage=NetMedX">Detailed Summary</a></li>
				<li><a href="ReportScheduleWindow.do?report_type=CSVSummary&typeid=3970&fromPage=NetMedX">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="ReportGeneration.do?firsttime=true">Update Report</a></li></ul></li></ul></li>
        	 -->
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocumentHistoryBean" property="appendixId"/>">Search</a>
  		</li>
  	
	</ul>
</li>

</div>
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			        <div id="breadCrumb">&nbsp;</div>
			    </div></td>
			</tr>
	        <tr>
		     <td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.metmedxdashboardformsa"/>:&nbsp;<bean:write name="DocumentHistoryBean" property="msaName"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="DocumentHistoryBean" property="appendixName"/></h2></td>
						        </tr> 
		</tbody></table>					
					
					
						 <%-- <table cellpadding="0" cellspacing="0" border="0" width = "100%">
						        <tr>
						        	<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
									<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
									<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocumentHistoryBean" property="appendixId"/>" style="width: 120px"><center>Search</center></a></td>
									<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
								</tr>
						        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
							          		<div id="breadCrumb">&nbsp;</div>
							        </td>
						        </tr>
						        <tr>
						        	<td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.metmedxdashboardformsa"/>:&nbsp;<bean:write name="DocumentHistoryBean" property="msaName"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="DocumentHistoryBean" property="appendixName"/></h2></td>
						        </tr> 
	      				</table> --%>
					</logic:equal>
					<logic:notEqual name = "DocumentHistoryBean" property = "appendixTypeId" value = "3">
						<%@ include  file="/AppendixDashboardMenuScript.inc" %>
						<table cellpadding="0" cellspacing="0" border="0" width = "100%">
					        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
						          		<div id="breadCrumb">&nbsp;</div>
						        </td>
					        </tr>
					        <tr><td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.appendixdashboardfor"/>&nbsp;<bean:write name="DocumentHistoryBean" property="msaName" />&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name="DocumentHistoryBean" property="appendixName" /></h2></td></tr> 
				      </table>
					</logic:notEqual>
				</logic:equal>
					<logic:notEqual name = "DocumentHistoryBean" property = "appendixStatusChecking" value = "Inwork">
				
					
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
					        <%-- <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )){%>
					        	<%if(appendixStatus.equals( "Cancelled" )){%>
					        		<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					        	<%}else{ %>
									<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					        	<%}%>
							<%}else{%>	
									<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
								<%}%>	
			
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td> --%>
							<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>
					        </tr>
					        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
							         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="DocumentHistoryBean" property ="msaId"/>&firstCall=true"><bean:write name  ="DocumentHistoryBean" property ="msaName"/></a></div>
							    </td>
			 				</tr>
			 			<tr>
								<td colspan="6">
									<h2><c:out value="${requestScope.libName}" /> Document History: <bean:write name  ="DocumentHistoryBean" property ="appendixName"/></h2>
								</td>
						</tr>
				</table>
			
				</logic:notEqual>
			</logic:equal>
			<c:if test="${requestScope.jobType eq 'Addendum'}">
				<logic:equal name = "DocumentHistoryBean" property = "type" value = "Addendum">
					<%@ include  file="/Menu_Addendum.inc" %>
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
						         <div id="breadCrumb">&nbsp;</a></div>
						    </td>
						</tr>
						<tr><td colspan="6"><h2><c:out value="${requestScope.libName}" /> Document History: <bean:write name  ="DocumentHistoryBean" property ="jobName"/></h2></td></tr>
					</table>
				</logic:equal>
			</c:if>
			<c:if test="${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
				<%@ include  file="/Menu_ProjectJob.inc" %>
				<logic:equal name = "DocumentHistoryBean" property = "type" value = "Job">
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
						         <div id="breadCrumb">&nbsp;</a></div>
						    </td>
						</tr>
						<tr><td colspan="6"><h2><c:out value="${requestScope.libName}" /> Document History: <bean:write name  ="DocumentHistoryBean" property ="jobName"/></h2></td></tr>
					</table>
				</logic:equal>
			</c:if>
			<c:if test="${requestScope.jobType eq 'Default'}">
				<%@ include  file="/Menu_DefaultJob.inc" %>
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb">&nbsp;</a></div>
					    </td>
					</tr>
					<tr><td colspan="6"><h2><c:out value="${requestScope.libName}" /> Document History: <bean:write name  ="DocumentHistoryBean" property ="jobName"/></h2></td></tr>
				</table>
			</c:if>
			<logic:equal name = "DocumentHistoryBean" property = "type" value = "PO">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19">&nbsp;</td></tr>
					<tr>
				          <td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
								 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
								 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
								  <a><span class="breadCrumb1">Document History</a>
					    </td>
					</tr>
					<tr>
							<td colspan="6">
								<h2><c:out value="${requestScope.libName}" /> Document History</h2>
							</td>
					</tr>
				</table>
			</logic:equal>
			<logic:equal name = "DocumentHistoryBean" property = "type" value = "WO">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19">&nbsp;</td></tr>
					<tr>
				          <td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
								 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
								 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
								  <a><span class="breadCrumb1">Document History</a>
					    </td>
					</tr>
					<tr>
							<td colspan="6">
								<h2><c:out value="${requestScope.libName}" /> Document History</h2>
							</td>
					</tr>
				</table>
			</logic:equal>
			
			
			
			<logic:equal name = "DocumentHistoryBean" property = "type" value = "MSA">
				<%@ include  file="/MSAMenuScript.inc" %>
			</logic:equal>
			
			<logic:equal name = "DocumentHistoryBean" property = "type" value = "Appendix">
				<logic:equal name = "DocumentHistoryBean" property = "appendixStatusChecking" value = "Inwork">
					<logic:equal name = "DocumentHistoryBean" property = "appendixTypeId" value = "3">
						<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
					</logic:equal>
					<logic:notEqual name = "DocumentHistoryBean" property = "appendixTypeId" value = "3">
						<%@ include  file="/AppedixDashboardMenuScript.inc" %>
					</logic:notEqual>
				</logic:equal>
				<logic:notEqual name = "DocumentHistoryBean" property = "appendixStatusChecking" value = "Inwork">
					<%@ include  file="/AppendixMenuScript.inc" %>
				</logic:notEqual>
			</logic:equal>
			
			<logic:equal name = "DocumentHistoryBean" property = "type" value = "Addendum">
			<%@ include  file="/AddendumMenuScript.inc" %>
			</logic:equal>
			<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
			<c:if test="${requestScope.jobType ne 'Default'}">
				<logic:equal name = "DocumentHistoryBean" property = "type" value = "Job">
				<%@ include  file="/ProjectJobMenuScript.inc" %>
				</logic:equal>
			</c:if>
			</c:if>
		</c:if>

	<c:if test="${requestScope.SourceType eq 'DocM'}">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr>
				          <td background="images/content_head_04.jpg" height="21">
				          <div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
						 					 <a href="#">History</a>
						 </div>	
				          </td>
				</tr>
				<tr>
						<td colspan="6">
							<h2><c:out value="${requestScope.libName}" /> Document History</h2>
						</td>
				</tr>
	</table>
	 </c:if>

<c:if test="${requestScope.appendixStatus eq 'pmAppendix'}">
	 <table border="0" cellspacing="1" cellpadding="0" width="100%">
		<tr><td class="headerrow" height="19" width = "100%" >&nbsp;</td></tr>
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
         			
    				</td>
		</tr>
		<tr><td><h2>Document History</h2></td></tr>
	</table>
	 </c:if>
 
 <table width='100%' border="0" align="left" cellpadding="1" cellspacing="1" >
          <c:set var="docid" value="0"/>
          <c:set var="fileId" value="0"/>
          <c:set var="MimeType" value=""/>
          <c:set var="fileName" value=""/>
          <c:set var="viewOnWeb" value=""/>
          <c:set var="controlType" value=""/>
          	<tr valign = "top"><td valign = "top">
			   <table valign = "top">
				 <c:forEach items="${requestScope.documentHistoryList}" var="row" varStatus="count" >
				           <c:if test="${count.index== '0'}">
					           <tr><td>&nbsp;</td>
									<c:forEach items="${row}" var="rowfield" varStatus="index">
										<c:if test="${index.index gt '4'}">
											<c:if test="${index.index eq '5'}">
												<td class="Ntryb" colspan="2"><c:out value="${rowfield}" /></td>
											</c:if>
											<c:if test="${index.index ne '5'}">
												<td class="Ntryb"><c:out value="${rowfield}" /></td>
											</c:if>
										</c:if>
									</c:forEach>
								</tr>
						    </c:if>
						    <c:set var="idx" value="0"/>
						    <c:if test="${count.index != '0'}">
					           <tr>
					           	   <td>&nbsp;</td>
						           <c:forEach items="${row}" var="rowfield" varStatus="ind">
							            <c:if test="${ind.index == '0'}"><c:set var="docid" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '1'}"><c:set var="fileId" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '2'}"><c:set var="MimeType" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '3'}"><c:set var="fileName" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '4'}"><c:set var="Delstatus" value="${rowfield}"/></c:if>
										<c:if test="${ind.index gt '4'}">
											<c:if test="${ind.index == '5'}">
												<td 
													<c:if test="${(count.index % '2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
													<c:if test="${(count.index % '2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
													 >
													 <c:out value="${rowfield}"/>
												</td>
									       		<td align="center" 
									       			<c:if test="${(count.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
									       			<c:if test="${(count.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
									       			 >
													[
													<c:if test="${not empty fileId}">
														<a href = "#" onclick="ViewFile('DocumentMaster.do?hmode=openFile&fileId=<c:out value="${fileId}"/>&libId=<c:out value="${libId}"/>')">Download</a>
													</c:if>
													<c:if test="${empty fileId}">Download</c:if>
													| 
									       			 <c:if test="${(count.index lt requestScope.documentHistoryListSize-1) }">
														<a href = "#" onclick = "submitForm('DocumentAdd.do?hmode=viewDocument&linkLibName=<c:out value="${DocumentHistoryBean.linkLibNameFromIlex}"/>&Source=<c:out value='${requestScope.SourceType}'/>&entityType=<c:out value="${param.entityType}"/>&entityId=<c:out value="${param.entityId}"/>&MSA_ID=<c:out value="${requestScope.MSA_ID}"/>&appendixid=<c:out value="${requestScope.appendixid}"/>&state=marked&libId=<c:out value="${requestScope.libId}"/>&docId=<c:out value="${docid}"/>&docVersion=<c:out value="${rowfield}"/>');">Details</a>&nbsp;|
														Modify&nbsp;|
														Delete
														]
													</c:if>
									       			 <c:if test="${(count.index eq requestScope.documentHistoryListSize-1) }">
									       			 	<a href =  "#" onclick = "submitForm('DocumentAdd.do?hmode=viewDocument&linkLibName=<c:out value="${DocumentHistoryBean.linkLibNameFromIlex}"/>&Source=<c:out value='${requestScope.SourceType}'/>&entityType=<c:out value="${requestScope.entityType}"/>&entityId=<c:out value="${requestScope.entityId}"/>&appendixid=<c:out value="${requestScope.appendixid}"/>&MSA_ID=<c:out value="${requestScope.MSA_ID}"/>&state=<c:out value="${Delstatus}"/>&libId=<c:out value="${requestScope.libId}"/>&docId=<c:out value="${docid}"/>');">Details</a>&nbsp;|
									       			 
														<c:if test="${Delstatus ne 'Deleted' && Delstatus ne 'Superseded' && requestScope.msastatus ne 'Signed'}">
															<a  href = "#" onclick = "submitForm('DocumentAdd.do?hmode=modifyDocument&linkLibName=<c:out value="${DocumentHistoryBean.linkLibNameFromIlex}"/>&Source=<c:out value='${requestScope.SourceType}'/>&entityType=<c:out value="${requestScope.entityType}"/>&entityId=<c:out value="${requestScope.entityId}"/>&appendixid=<c:out value="${requestScope.appendixid}"/>&MSA_ID=<c:out value="${requestScope.MSA_ID}"/>&libId=<c:out value="${requestScope.libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>');">Modify</a></c:if><c:if test="${Delstatus eq 'Deleted' || Delstatus eq 'Superseded' || requestScope.msastatus eq 'Signed'}">Modify</c:if>&nbsp;|
														<c:if test="${Delstatus ne 'Deleted' && Delstatus ne 'Superseded' && requestScope.msastatus ne 'Signed' && (requestScope.SourceType eq 'DocM' || requestScope.entityType eq 'Supplement')}">
															<a href = "#" onclick = "submitForm('DocumentAdd.do?hmode=viewDocument&linkLibName=<c:out value="${DocumentHistoryBean.linkLibNameFromIlex}"/>&Source=<c:out value='${requestScope.SourceType}'/>&MSA_ID=<c:out value="${requestScope.MSA_ID}"/>&appendixid=<c:out value="${requestScope.appendixid}"/>&entityId=<c:out value="${requestScope.entityId}"/>&libId=<c:out value="${requestScope.libId}"/>&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&entityType=<c:out value="${requestScope.entityType}"/>&delete=DeleteNow');">Delete</a></c:if>
															<c:if test="${(Delstatus eq 'Deleted' || Delstatus eq 'Superseded' || requestScope.msastatus eq 'Signed' || (requestScope.SourceType eq 'Ilex' && requestScope.entityType ne 'Supplement'))}">Delete</c:if>
														]

													</c:if>
									            </td>
											</c:if>
											<c:if test="${ind.index gt '5'}">
												<td  
													<c:if test="${(count.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
													<c:if test="${(count.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
													 > 
													 <c:out value="${rowfield}"/>
												</td>
											</c:if>
										</c:if>
						       	</c:forEach>
					       </tr>
					   </c:if>
			         </c:forEach>
				</table>
        	</td></tr>
        	<tr>
        		<td valign="top" width="100%">
        			<table width='100%' border="0" align="left" cellpadding="0" cellspacing="8" >
        				<tr><td>
        				<input type="button"  name="back" onclick="history.go(-1);"  class="button_c" value="Back"/>
        				</td></tr>
        			</table>
        		</td>
        	</tr>
         
     </table>
	

</html:form>
</body>

</HTML>