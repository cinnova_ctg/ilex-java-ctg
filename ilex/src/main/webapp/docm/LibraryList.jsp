<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ILEX</title>
<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<script>
function ViewLibrary(url)
{
    document.forms[0].target = "_self";
	document.forms[0].action = url;
	document.forms[0].submit();
	return true;
}
</script>
<script>

function alertNotAvailable() {
	alert("This feature will be available in future releases!");
}
</script>
</head>

<body >
<html:form action="LibraryManager">

	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td class="headerrow" height="19">&nbsp;</td></tr>
			<tr>
				<td background="images/content_head_04.jpg" height="21" width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Administration</a>
										 <a href="#">Library Manager</a>
										 		
				</td>
				
			</tr>
			<tr><td><h2>Library List</h2></td></tr>
	</table>
	
		
		
		
      <table border="0" align="left" cellpadding="1" cellspacing="1">
          <tr>
          	<td>&nbsp;</td>
            <td class = "Ntryb" colspan="2">Library Name </td>
            <td class = "Ntryb"  >Notify E-mail  </td>
            <td class = "Ntryb"  >Is Notifiable  </td>
          </tr>
          <c:set var="idx" value="0"/>
          <c:forEach items="${requestScope.libraryList}" var="library" varStatus="status" >
          <tr>
          <c:choose>
				<c:when test="${(idx%'2')=='0'}">
					<c:set var="tdClass" value="Ntextoleftalignnowrap"/>
				</c:when>	
				<c:otherwise>
					<c:set var="tdClass" value="Ntexteleftalignnowrap"/>
				</c:otherwise>
		 </c:choose>
	          	<td>&nbsp;</td>
	            <td class = <c:out value="${tdClass}" />>
	            	<a href="ViewLibrary.do?hmode=unspecified&libId=<c:out value="${library.libraryId}"/>" ><c:out value="${library.libraryName}"/></a>
	           	</td>
	             <td class = <c:out value="${tdClass}" />>
<%--             
	            <img src="docm/images/details.gif" onclick="javascript:ViewLibrary('ViewLibrary.do?hmode=unspecified&libName=<c:out value="${library.libraryName}"/>&notifyMailId=<c:out value="${library.notifyMailId}"/>&libId=<c:out value="${library.libraryId}"/>');" />
	            <img src="docm/images/modify.gif" />
	            <img src="docm/images/delete.gif" />
				<c:set var="idx" value="${idx+1}"/> 
	            <img src="docm/images/setup.gif" onclick="javascript:ViewLibrary('EntryFieldSetUp.do?hmode=unspecified&libName=<c:out value="${library.libraryName}"/>&notifyMailId=<c:out value="${library.notifyMailId}"/>&libId=<c:out value="${library.libraryId}"/>');" />	            	            
--%>
					[
					<a href ="#" onclick="javascript:ViewLibrary('ViewLibrary.do?hmode=unspecified&libId=<c:out value="${library.libraryId}"/>');">Details</a>&nbsp;|
					<a href = "#" onclick="alertNotAvailable();">Modify</a>&nbsp;|
					<a href = "#" onclick="alertNotAvailable();">Delete</a>&nbsp;|
					<a href ="#" 
						onclick="javascript:ViewLibrary('EntryFieldSetUp.do?hmode=unspecified&libId=<c:out value="${library.libraryId}"/>');">Setup</a>
					]
	            </td>
	            <td class = <c:out value="${tdClass}" />><c:out value="${library.notifyMailId}"/></td>
	             <td class = <c:out value="${tdClass}" />><input type="checkbox" class="chkbx"  value="" disabled = "disabled"/></td>
	          </tr>
	          <c:set var="idx" value="${idx+1}" />
	       </c:forEach>
	       <tr>
				<td>&nbsp;</td>
				<td colspan="7" class = "colLight" align ="center"><input type="button" Class="button_c" name="Submit" value="Add Library"  onclick="alertNotAvailable();"/></td>
		   </tr>
		   <%-- 
		   <tr>
				<td>&nbsp;</td>
				<td colspan="3" align="right"><a href="#" class="textlink">Previous&nbsp;&nbsp;Page
				1 of 3&nbsp;&nbsp;Next</a></td>
			</tr>
			--%>
         </table>
         
</html:form>
</body>
</html>
