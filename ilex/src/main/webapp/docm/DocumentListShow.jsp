<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <script type='text/javascript' src='./dwr/interface/AjaxCall.js'></script>
    <script type='text/javascript' src='./dwr/engine.js'></script>
    <script type='text/javascript' src='./dwr/util.js'></script>
    <script type='text/javascript' src='./script/prototype.js'></script>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>ILEX</title>
	<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<script>
function ViewFile(url)
{
    document.forms[0].target = "_self";
	document.forms[0].action = url;
	document.forms[0].submit();
	return true;
}
</script>
<script>
function ShowDiv(){
	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";
	return false;
}
function hideDiv(){
	document.getElementById("filter").style.visibility="hidden";
	return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
}
function showDocuments(type){
	if(type == 'SD')
		document.forms[0].action = 'DocumentMaster.do?hmode=unspecified&status=marked&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />';
	else if(type == 'AD')
		document.forms[0].action = 'DocumentMaster.do?hmode=unspecified&status=approved&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />';
	document.forms[0].submit();
	return true;	
}
function getGroup(id,grpList,libId,cond){

	AjaxCall.getGroupChildNode(grpList,libId,cond,{callback:function(str){ret=str;},async:false});
	var table=document.getElementById("tab"+id);
	var oTHead = document.createElement("THEAD");
    table.appendChild(oTHead);
    var oTBody = document.createElement("TBODY");
    table.appendChild(oTBody);
    var result=AddRow(oTBody,id,grpList,ret,libId);
   	var abc=changeImage(id);
	return true;
 }
 
 function AddRow(oTBody,id,grpList, ret,libId)
   {
     var oCell;
     var oRow;
     alert(ret);
     var nextGroup=ret.substring(ret.indexOf(':')+1,ret.indexOf(','));
	 var condition=ret.substring(ret.lastIndexOf(':')+1,ret.length);
     
     // Create the row
     oRow = document.createElement("TR");
     oTBody.appendChild(oRow);
     
     oCell = document.createElement("TD");
     oCell.className = 'Ntrybleft';
     oCell.innerHTML = "<a onclick='getGroup('"+id+"','"+grpList+"' ,'"+libId+"' , '"+condition+"' ) '> <img id='img"+id+"' src='docm/images/Expand.gif' border='0' /> </a>"+nextGroup;
     oRow.appendChild(oCell);
   }

	function changeImage(imdId)
	{
		var plus="Expand.gif";
		var minus="Collapse.gif";
		var imgSrc=document.getElementById("img"+imdId).src;
		var idx=imgSrc.lastIndexOf("/");
		var ImgSrcPath=imgSrc.substring(0,idx+1);
		var imgName=imgSrc.substring(idx+1,imgSrc.length);
		if(imgName==plus){
			document.getElementById("img"+imdId).src=ImgSrcPath+minus;
			document.getElementById("tab"+imdId).style.display="block";
		}else if(imgName==minus){
		document.getElementById("img"+imdId).src=ImgSrcPath+plus;
			document.getElementById("tab"+imdId).style.display="none";
		}
	}
 


</script>
</head>
 
<body >
<html:form action="DocumentMaster">
<html:hidden property="hmode" value=""/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="top" width="100%">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="headerrow" height="19" width="100%">&nbsp;</td>
				</tr>
				<tr>
					<td background="images/content_head_04.jpg" height="21"
						width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Library</a> <a
						href="#"><c:out value="${param.libName}" /></a> <a href="#">Default
					View</a></div>
					</td>
				</tr>
				<tr>
					<td colspan="6"><%-- 		<h2>Documents&nbsp;in&nbsp;Library&nbsp;<c:out value="${param.libName}"/>&nbsp;for&nbsp;Default View<u></u></h2>  --%>
					<h2>Document List</h2>
					</td>
				</tr>

			</table>
			</td>

			<td width="35" valign="top"><img id="content_head_02"
				src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
			<td valign="top" width="155">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="155" height="39" class="headerrow" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="150">
							<table width="100%" cellpadding="0" cellspacing="3" border="0">
								<tr>
									<td><img src="images/disbmonth.gif" width="31"
										id="Image10" height="30" border="0" /></td>
									<td><img src="images/disbweek.gif" name="Image11"
										width="31" height="30" border="0" id="Image11" /></td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td height="21" background="images/content_head_04.jpg" width="140">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td nowrap="nowrap" colspan="2">
							<div id="featured1">
							<div id="featured">
							<li name="closeNow" id="closeNow"><a
								href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My
							Views <img src="images/showFilter.gif" title="Open View"
								border="0" onClick="ShowDiv();" /></a><a href="javascript:void(0)"
								class="imgstyle"><img src="images/offFilter2.gif" border="0"
								title="Close View" onClick="hideDiv();" /></a></li>
							</div>
							</div>
							<span>
							<div id="filter" class="divstyle">
							<table width="250" cellpadding="0" class="divtable">
								<tr>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Views</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText"><input name="DefaultView"
												type="radio" checked="true" />Default View</td>
										</tr>
									</table>
									</td>
									<td><img src="images/filterLine.gif" width="1px"
										height="30" class="imagefilter" /></td>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Show Status</td>
										</tr>
										<tr>
											<td class="tabNormalText">
											 <c:if test="${param.status eq 'approved'}">
											 	<input name="ASD" type="radio"  onclick = "showDocuments('AD');" checked = "true" />Approved / Draft<br>
												<input name="ASD" type="radio"  onclick = "showDocuments('SD');" />Superceded / Deleted
											 </c:if>
											<c:if test="${param.status eq 'marked'}">
											 	<input name="ASD" type="radio"  onclick = "showDocuments('AD');" />Approved / Draft<br>
												<input name="ASD" type="radio"  onclick = "showDocuments('SD');" checked = "true"/>Superceded / Deleted
											</c:if>
											</td>
										</tr>
									</table>
									</td>
								</tr>
							</table>
							</div>
							</span></td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

	<c:set var='rowid' value='0'/>
    <table border="0" align="left" id="table" cellpadding="1" cellspacing="1">
		<c:forEach items="${requestScope.grpData}" var="group" varStatus="status">
			<c:forEach items="${requestScope.grpList}" var="groupList" varStatus="index">
				<c:if test="${index.index ne '0'}">
				<c:set var="rowid" value="${rowid+1}"/>
				<tr id='row<c:out value="${rowid}"/>'>
						<td class="Ntrybleft"  align="left" nowrap="nowrap"> 
							<a onclick='getGroup("<c:out value='${rowid}'/>","<c:out value='${groupList}' />" , "<c:out value='${param.libId}' />" , "<c:out value='${group.value}'/>" ) '> <img id='img<c:out value="${rowid}"/>' src="docm/images/Expand.gif" border="0" /> </a><c:out value="${group.name}"/>
						</td>
					</tr>
					<tr>
						<td>
							<table  id='tab<c:out value="${rowid}"/>'> 
							</table>
						</td>
					</tr>
				</c:if>
			</c:forEach>
		</c:forEach>
     </table>
    <%-- 
     <c:if test="${param.status eq 'approved'}">
  		 <td colspan="2"><a href="DocumentMaster.do?hmode=unspecified&status=marked&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />" class="textlink">Superseded/Deleted</a> </td>
    </c:if>
    <c:if test="${param.status eq 'marked'}">
    <td colspan="2"><a href="DocumentMaster.do?hmode=unspecified&status=approved&libId=<c:out value="${param.libId}" />&libName=<c:out value="${param.libName}" />" class="textlink">Approved/Draft</a> </td>
    </c:if>
    </tr>
    --%>
    <%-- 
     <tr>
        <td colspan="2">
        <table width="220" border="0" align="right" cellpadding="2" cellspacing="0">
        <tr>
         <td width="72" align="center"><a href="#" class="textlink">Previous</a> </td>
          <td width="81" align="right" class="prenext">Page 1 of 3 </td>
          <td width="55" align="right"><a href="#" class="textlink">Next</a></td>
        </tr>
      </table></td>
    </tr>
   --%> 
</html:form>
</body>
</html>
