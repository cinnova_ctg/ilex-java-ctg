<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ILEX</title>
<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<script>
	function openTxnGroup(txngroup,imdId)
	{
		var plus="Expand.gif";
		var minus="Collapse.gif";
		var imgSrc=document.getElementById(imdId).src;
		var idx=document.getElementById(imdId).src.lastIndexOf("/");
		var ImgSrcPath=imgSrc.substring(0,idx+1);
		var imgName=imgSrc.substring(idx+1,imgSrc.length);
		if(imgName==plus){
		document.getElementById(imdId).src=ImgSrcPath+minus;
			document.getElementById(txngroup).style.display="block";
		}else if(imgName==minus){
		document.getElementById(imdId).src=ImgSrcPath+plus;
			document.getElementById(txngroup).style.display="none";
		}
	}
</script>
<script>

function alertNotAvailable() {
	alert("This feature will be available in future releases!");
}
</script>
</head>

<body>
<html:form action="EntryFieldSetUp">
	
	
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td class="headerrow" height="19">&nbsp;</td></tr>
			<tr>
				<td background="images/content_head_04.jpg" height="21" width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Administration</a>
										 <a href="#">Library Manager</a>
										 		
				</td>
				
			</tr>
			<tr><td><h2>Field Setup for Library: <c:out value="${requestScope.libName}"/></h2></td></tr>
	</table>
	

<table  align="left" cellpadding="1" cellspacing="1">
<tr>
   <td>&nbsp;</td>
   <td width="100%">
   <c:set var="idx" value="0" />
   <c:set var="ind" value="0" />
   <c:set var="imgId" value="0" />
   <c:forEach items="${requestScope.txnGROUP}" var="txnGROUP" varStatus="status" >
   <table width="100%"  align="center" cellpadding="0" cellspacing="0">
   		<tr>
			<td class="Ntrybleft" valign="middle" align="left" nowrap="nowrap">
	 	  		<table cellpadding="0" cellspacing="0"><tr><td class="Ntrybleft" ><a onclick="openTxnGroup('<c:out value="${ind+1}"/>','img<c:out value="${imgId+1}"/>')"><img id='img<c:out value="${imgId+1}"/>' src="docm/images/Expand.gif" border="0" /></a>
	 	  		</td><td class="Ntrybleft" height="20" >
	 	  		<c:out value="${txnGROUP}"/>
	 	  		</td></tr></table>
	 	  	</td>
	 	</tr>
	 	<tr width="100%">
	   		<td width="100%"  >
	 			<div width="100%"  style='display:none;'  id='<c:out value="${ind+1}"/>' title='<c:out value="${txnGROUP}"/>'><c:set var="ind" value="${ind+1}" />
	   			<table cellpadding="0" cellspacing="0"><tr>
	   				<td>&nbsp;&nbsp;&nbsp;</td>
	   				<td>
	    			<table width="100%"  >
				          <tr width="100%"  >
				            <td height="20" class = "Ntryb" >Field&nbsp;Name </td>
				            <td height="20" class = "Ntryb" >Description </td>
				            <td height="20" class = "Ntryb" >Entry&nbsp;Field  </td>
				            <td height="20" class = "Ntryb" >Compulsory&nbsp;Field </td>
				            <td height="20" class = "Ntryb" >Unique&nbsp;Across&nbsp;Revision </td>
				            <td height="20" class = "Ntryb">Unique&nbsp;Across&nbsp;Library </td>
				          </tr>
				 	   	  <c:forEach items="${requestScope.entryField}" var="entryField" varStatus="indx" >
						  <c:if  test ="${txnGROUP eq (entryField.taxonomyGroup)}">
							 <tr>
					 		      <c:if test="${entryField.entryField eq 'N'}"><c:set var="entryField1" value="0"/></c:if>
							      <c:if test="${entryField.entryField eq 'Y'}"><c:set var="entryField1" value="1"/></c:if>
						          <c:if test="${entryField.compulsoryField eq 'N'}"><c:set var="compulsoryField1" value="0"/></c:if>
						          <c:if test="${entryField.compulsoryField eq 'Y'}"><c:set var="compulsoryField1" value="1"/></c:if>
						          <c:if test="${entryField.uniqueAcrossRevision eq 'N'}"><c:set var="uniqueAcrossRevision1" value="0"/></c:if>
						          <c:if test="${entryField.uniqueAcrossRevision eq 'Y'}"><c:set var="uniqueAcrossRevision1" value="1"/></c:if>
						          <c:if test="${entryField.uniqueAcrossLibrary eq 'N'}"><c:set var="uniqueAcrossLibrary1" value="0"/></c:if>
						          <c:if test="${entryField.uniqueAcrossLibrary eq 'Y'}"><c:set var="uniqueAcrossLibrary1" value="1"/></c:if>
				            
					            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if> ><a href="#" onclick="alertNotAvailable();"><c:out value="${entryField.entryFieldName}"/></a></td>
					            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if>  ><c:out value="${entryField.description}"/></td>
					            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if> ><input type="checkbox" class="chkbx" value="<c:out value="${entryField1}"/>" disabled="disabled"  <c:if test="${entryField1 == '1'}">checked="checked"</c:if> /></td>
					            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if> ><input type="checkbox" class="chkbx" value="<c:out value="${compulsoryField1}"/>" disabled="disabled" <c:if test="${compulsoryField1 == '1'}">checked="checked"</c:if>/></td>
					            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if> ><input type="checkbox" class="chkbx" value="<c:out value="${uniqueAcrossRevision1}"/>" disabled="disabled" <c:if test="${uniqueAcrossRevision1 == '1'}">checked="checked"</c:if>/></td>
					            <td <c:if test="${(idx%'2')=='0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(idx%'2')=='1'}">class='Ntexteleftalignnowrap'</c:if> ><input type="checkbox" class="chkbx" value="<c:out value="${uniqueAcrossLibrary1}"/>" disabled="disabled" <c:if test="${uniqueAcrossLibrary1 == '1'}">checked="checked"</c:if>/></td>
							 	  <c:set var="idx" value="${idx+1}"/>
							</tr>
				          </c:if>
					      </c:forEach>
			       </table>
		 	   </td></tr></table>
		 	   </div>
						 	   <c:set var='imgId' value="${imgId+1}"/>
	 	  </td>
	 	</tr>
	</table>
					 
  </c:forEach>
  </td>
</tr>
<tr>
	<td>&nbsp;</td>
  	<td class = "colLight">
  	<input type="button" name="Submit" styleClass="button_c" value="Ok"  onclick="alertNotAvailable();"/>&nbsp;&nbsp;
  	<input type="button" name="Submit2" styleClass="button_c" value="Modify" onclick="alertNotAvailable();" />
</tr>
<%-- 
<tr>
	<td>&nbsp;</td>
	<td class = "textlink"><a href="#" class="textlink">Previous&nbsp;Page 1 of 3&nbsp;>Next</a></td>
</tr>	
--%>
</table>
</html:form>
</body>
</html>
