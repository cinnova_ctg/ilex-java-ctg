
<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<link href="docm/styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/docMstyle.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="JavaScript1.2" src="docm/javascript/prototype.js"></script>

</head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ILEX</title>

<body>
 <table width='100%' border="0" align="left" cellpadding="1" cellspacing="1" >
          <c:set var="docid" value="0"/>
          <c:set var="fileId" value="0"/>
          <c:set var="MimeType" value=""/>
          <c:set var="fileName" value=""/>
          <c:set var="viewOnWeb" value=""/>
          <c:set var="controlType" value=""/>
          <c:if test="${requestScope.size eq '1' && not empty requestScope.documentGroupNodes[0].documentList }">
          	<tr valign = "top"><td valign = "top">
			   <table valign = "top">
				 <c:forEach items="${requestScope.documentGroupNodes[0].documentList}" var="row" varStatus="status" >
				           <c:if test="${status.index== '0'}">
					           <tr><td>&nbsp;</td>
									<c:forEach items="${row}" var="rowfield" varStatus="index">
										<c:if test="${index.index gt '4'}">
											<c:if test="${index.index eq '5'}">
												<td class="Ntryb" colspan="2"><c:out value="${rowfield}" /></td>
											</c:if>
											<c:if test="${index.index ne '5'}">
												<td class="Ntryb"><c:out value="${rowfield}" /></td>
											</c:if>
										</c:if>
										<c:if test="${index.last}">
									      <c:set var="colcount" value="${index.count}"/>
									    </c:if>
					    			</c:forEach>
								</tr>
						    </c:if>
				           
						    <c:set var="idx" value="0"/>
						    <c:if test="${status.index != '0'}">
					           <tr>
					           	   <td>&nbsp;</td>
						           <c:forEach items="${row}" var="rowfield" varStatus="ind">
						           		<c:if test="${ind.index == '0'}"><c:set var="docid" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '1'}"><c:set var="fileId" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '2'}"><c:set var="MimeType" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '3'}"><c:set var="fileName" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index == '4'}"><c:set var="docVersion" value="${rowfield}"/></c:if>
							           	<c:if test="${ind.index gt '4'}">
											<c:if test="${ind.index == '5'}">
												<td 
													<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
													<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
													 >
													<c:set var="originalFieldValue" value="${rowfield}" scope="session"/>
													<%
														//********************************
														String originalFieldValue = (String) session.getAttribute("originalFieldValue");
														String displayFieldValue = "";
														if(originalFieldValue!=null && originalFieldValue.length()>40) {
															displayFieldValue = originalFieldValue.subSequence(0,37) + "...";
															%>
															<p title="<%=originalFieldValue%>">
																<%=displayFieldValue%>
															</p>
															<%															
														} else {
															%>
															<c:out value="${rowfield}"/>
															<%															
														}
														//********************************
													%>
												</td>
									       		<td align="center" 
									       			<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
									       			<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
									       			 >
									       		
													[
													<c:if test="${not empty fileId}">
														<a href = "#" onclick="ViewFile('DocumentMaster.do?hmode=openFile&fileId=<c:out value="${fileId}"/>&libId=<c:out value="${param.libId}"/>')">Download</a>
													</c:if>
													<c:if test="${empty fileId}">Download</c:if>
													
													|
													<a href = "DocumentAdd.do?hmode=viewDocument&state=<c:out value="${param.status}"/>&libId=<c:out value="${param.libId}"/>&docId=<c:out value="${docid}"/>&Source=DocM&docVersion=<c:out value="${docVersion}"/>">Details</a>&nbsp;|
													<a href = 'DocumentHistory.do?hmode=unspecified&Source=DocM&documentId=<c:out value="${docid}"/>'>Revisions</a>&nbsp;|
													<c:if test="${param.status ne 'marked'}"><a  href = "DocumentAdd.do?hmode=modifyDocument&libId=<c:out value="${param.libId}"/>&Source=DocM&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>">Modify</a></c:if><c:if test="${param.status eq 'marked'}">Modify</c:if>&nbsp;| 
													<c:if test="${param.status ne 'marked' }"><a href = "DocumentAdd.do?hmode=viewDocument&libId=<c:out value="${param.libId}"/>&Source=DocM&state=<c:out value="${param.status}"/>&docId=<c:out value="${docid}"/>&delete=DeleteNow">Delete</a></c:if><c:if test="${param.status eq 'marked' }">Delete</c:if>
													]
												
													
									            </td>
											</c:if>
											<c:if test="${ind.index gt '5'}">
												<td  
													<c:if test="${(status.index%'2')=='0'}">class='Ntexteleftalignnowrap'</c:if>
													<c:if test="${(status.index%'2')=='1'}">class='Ntextoleftalignnowrap'</c:if>
													 > 
													<c:set var="originalFieldValue" value="${rowfield}" scope="session"/>
													<%
														//********************************
														String originalFieldValue = (String) session.getAttribute("originalFieldValue");
														String displayFieldValue = "";
														if(originalFieldValue!=null && originalFieldValue.length()>40) {
															displayFieldValue = originalFieldValue.subSequence(0,37) + "...";
															%>
															<p title="<%=originalFieldValue%>">
																<%=displayFieldValue%>
															</p>
															<%															
														} else {
															%>
															<c:out value="${rowfield}"/>
															<%															
														}
														//********************************
													%>
												</td>
											</c:if>
										</c:if>
						       	</c:forEach>
					       </tr>
					   </c:if>
						<c:if test="${status.last}">
					      <c:set var="rowcount" value="${status.count}"/>
					    </c:if>
			         </c:forEach>
						<c:if test="${rowcount lt '2'}">
							<tr><td>&nbsp;</td>
								<td class='Ntexteleftalignnowrap' colspan='<c:out value="${colcount}"/>'>No document found!</td>
							</tr>
					    </c:if>
    				</table>
        	</td></tr>
        </c:if>
         <c:if test="${requestScope.size gt '0' && empty requestScope.documentGroupNodes[0].documentList}">
	           <c:forEach items="${requestScope.documentGroupNodes}" var="dgn" varStatus="index">
			 <tr width='100%' align='left'>
	          	<td align='left' width='2'>
	          		<c:set var="fn" value="${param.fieldName}~${dgn.fieldName}"/>
	          		<c:set var="fv" value="${param.fieldVal}~${dgn.fieldValue}"/>
	          		 <c:set var="row" value="${param.rowId}${index.index}" />
	          		<c:set var="originalFieldValue" value="${fv}" scope="session"/>
	          		<%
						//********************************
						String originalFieldValue = (String) session.getAttribute("originalFieldValue");
		          		originalFieldValue = originalFieldValue.replaceAll("'","\\\\'");
						//********************************
					%>
	          		<img id="<c:out value='${param.imgId}'/><c:out value='${index.index}'/>" src="images/Expand.gif" 
	          			title="Expand"
	          			onclick="toggleImage('<c:out value='${param.imgId}'/><c:out value="${index.index}"/>','<c:out value="${param.divId}" /><c:out value="${index.index}"/>','<c:out value="${fn}" />','<%=originalFieldValue%>','<c:out value="${row}"/>')" />
	          	</td> 
				<td class='Ntexteleftalignnowrap' align='left' >
					<div id="breadCrumb">
						<c:out value="${dgn.fieldDescription}" />&nbsp;:
						<a href="#" class="bgNone"><c:out value="${dgn.fieldValue}" /></a>
					</div>
				</td>
			 </tr>
			 <tr width='100%' valign = "top" align='left' style="display:none;" id="<c:out value='${row}'/>">
          		<td valign = "top">&nbsp;
          		</td>
          		<td valign = "top">
	          			<div  style="display:none" align="left" valign = "top" id="<c:out value='${param.divId}'/><c:out value="${index.index}"/>">
	          			</div>
          		</td>
	          </tr>
			 </c:forEach>
		 </c:if>
         
     </table>

</body>
</html>