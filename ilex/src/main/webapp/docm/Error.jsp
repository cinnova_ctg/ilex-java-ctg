<!DOCTYPE HTML>
<HTML>
<HEAD>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page 
language="java"
contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"
%>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<TITLE>Error.jsp</TITLE>
</HEAD>
<BODY>
	<table>
		<tr>
  			 <td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "90%"> 
  		    		   <tr>
			    			<td  colspan = "3" class = "message" height = "30">
			    				<c:out value="${requestScope.Comment}"/>
			    			</td>
			    		</tr>
			    		
			    		<tr>
			    			<td  colspan = "3" class = "message" height = "30">
			    				<input type="button" class="Button" value="Back" onclick="history.go(-1);"> 
			    			</td>
			    		</tr>
			    		
				 </table>
			  </td>
			</tr>
	</table>
</BODY>
</HTML>
