<!DOCTYPE HTML>
<HTML>
<HEAD>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ page import="java.util.*" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="Content-Style-Type" content="text/css">

<TITLE>DocumentAdd.jsp</TITLE>
	<HEAD>
		<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
	<link rel = "stylesheet" href="styles/base/ui.core.css" type="text/css">
	<link rel = "stylesheet" href="styles/base/ui.datepicker.css" type="text/css">
	<link rel = "stylesheet" href="styles/base/ui.theme.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language="JavaScript" src="javascript/jquery.ui.core.js"></script>
	<script language="JavaScript" src="javascript/jquery.ui.datepicker.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script type='text/javascript' src='./dwr/interface/DocMSeacrhDao.js'></script>
	<script type='text/javascript' src='./dwr/engine.js'></script>
	<script type='text/javascript' src='./dwr/util.js'></script>
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	.calImageStyle{
	background-color: transparent;
	border: none;
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
}
</style>
	<SCRIPT LANGUAGE="JavaScript">
		function fun_HideShowBlock(index)
		{
			if(document.forms[0].Source.value == 'Ilex'){
				document.forms[0].libName.value=document.forms[0].libId.value;
				document.forms[0].libName.disabled=true;
				}

			if(document.forms[0].libName.value != null 
				&& document.forms[0].libName.value!="" 
				&& document.forms[0].libName.value!="0"){
	        
	        var len=document.all.tab.length;
       		for(var id=0;id<len;id++){
       			if(document.all.tab[id].style.display=="block"){
       			document.all.tab[id].style.display="none";
       			}
       		}        
        	if(document.forms[0].FileName!=null)
				document.forms[0].FileName.disabled=true;
			document.all.tab[index].style.display="block";
			}
			else
			{
				document.all.buttonsTable.style.display="none";
			}
			
			buttonPressed(index);
		}
		
		function buttonPressed(idx)
		{
			for(var i=0;i<document.all.TOP_BUTTON.length/3;i++) {

				document.all.TOP_BUTTON[3*i+1].style.fontSize='10';
				document.all.TOP_BUTTON[3*i+1].width='150';
				document.all.TOP_BUTTON[3*i+1].style.fontFamily='Verdana|Arial|Helvetica|sans-serif';
				document.all.TOP_TAB[i].style.width='5';

				if(i==idx)
				{
					document.all.TOP_BUTTON[3*i].innerHTML='<img src="docm/images/left-cor-light.gif" width="5" height="18" border="0">';
					document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#D9DFEF';
					document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="docm/images/right-cor-light.gif" width="5" height="18"  border="0">';
					document.all.TOP_BUTTON[3*i+1].style.borderBottom = '0px #D9DFEF solid';
					document.all.TOP_BUTTON[3*i+1].style.fontWeight='bold';
				}
				else//Restore Remaining
				{
					document.all.TOP_BUTTON[3*i].innerHTML='<img src="docm/images/left-cor.gif" width="5" height="18"  border="0">';
					document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#B5C8D9';
					document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="docm/images/right-cor.gif" width="5" height="18"  border="0">';
				    document.all.TOP_BUTTON[3*i+1].style.borderBottom = '2px #B5C8D9 solid';
					document.all.TOP_BUTTON[3*i+1].style.fontWeight='normal';
				}
			}
		}
		function openLinkedDetailPage(url){
			child = open(url,'popupwindow','width=800,height=420,scrollbars=yes,status=yes');
		}
		function callOpenLinkedDetail(){
			openLinkedDetailPage(document.forms[0].LinkedDoc.value);
		}
		function onComboChange()
			{
				var through = '<c:out value="${param.through}"/>';
				if(document.forms[0].libName.value==0){
					document.forms[0].Type.value="";
					url="./DocumentAdd.do?hmode=unspecified";
					document.forms[0].action=url;
					document.forms[0].submit();
					//return false;
				}else{
					document.forms[0].libId.value=document.forms[0].libName.value;
					if(through == 'MPO'){
						window.close();
					}else{
						url="./DocumentAdd.do?hmode=unspecified&libId="+document.forms[0].libId.value+"&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&Source=<c:out value='${requestScope.SourceType}'/>";
						document.forms[0].action=url;
						document.forms[0].submit();
					}
					
					//return true;
				}
			}
		function is_empty(chkObj, message)
		{
			if(chkObj.value == '')
				{
					alert(message + ' is required.');
					chkObj.focus();
					return true;
				}
			return false;

		}
		
		function is_radio(chkObj, message)
		{
			if (!chkObj.checked) {
				alert(message +' is required');
				return true;
				}
			return false;
		}
		function getCheckedValue(radioObj) {
			if(!radioObj)
				return "";
			var radioLength = radioObj.length;
			if(radioLength == undefined)
				if(radioObj.checked)
					return radioObj;
				else
					return "";
			for(var i = 0; i < radioLength; i++) {
				if(radioObj[i].checked) {
					return radioObj[i];
				}
			}
			return "";
		}
		function setCheckedValue(radioObj, newValue) {
			if(!radioObj)
				return;
			var radioLength = document.getElementsByName(radioObj.name).length;
			radioObj=document.getElementsByName(radioObj.name);
			if(radioLength == undefined) {
				radioObj.checked = (radioObj.value == newValue.toString());
				return;
			}
			for(var i = 0; i < radioLength; i++) {
				radioObj[i].checked = false;
				if(radioObj[i].value == newValue.toString()) {
					radioObj[i].checked = true;
				}
			}
		}

		function validateIsEmpty(){
			var len=document.all.tab.length;
			for(var i=0;i<len;i++){
				var count=document.getElementById('tab'+i+'_fieldcount').value;
				for(var j=1;j<=count;j++){
					fun_HideShowBlock(i);
					var element =document.getElementById('tab'+i+'_notempty_'+j);
					if(element != null ){
					if(element.type=='radio'){
						var val=getCheckedValue(document.getElementsByName(element.name));
						if(is_radio(val,element.name)) return false;
					}
					else if(element.type!='radio'){
						if(is_empty(element,element.name)){
								return false;
						}
					}
					}
				}
			}
			return true;
		}

		function setType(){
			var len=document.forms[0].libName.options.length;
			for(i=0;i<len;i++){
				if(document.forms[0].libId.value!=0){
					if(document.forms[0].libName.options[i].selected ==true){
						document.forms[0].Type.value=document.forms[0].libName.options[i].text;
						
					}else if(document.forms[0].libName.value==0){
						document.forms[0].Type.value="";
					}
				}
			}
		}

		function saveData(){
		
			var through = '<c:out value="${param.through}"/>';
			//return false;
			if(document.forms[0].Source.value == 'Ilex'){
				document.forms[0].libName.disabled=false;
				}
			if(document.forms[0].libName.value==0){
				alert("Please select the Library.");
				return false;
			}else if (document.getElementById("docStatus").value == 'Superceded' ||  document.getElementById("docStatus").value  == 'Deleted'){
				alert("Documents with status Approved or Draft can be added.");
				return false;
			}
			else
			{
				document.forms[0].libId.value=document.forms[0].libName.value;	
				if(document.forms[0].FileName!=null)
					document.forms[0].FileName.disabled=false;
				
				if(!validateIsEmpty())
					return false;
				else if(validateIsEmpty()==true){
					if(through != ''){
						var devId = '<c:out value="${param.devId}"/>'
						url="./DocumentAdd.do?hmode=metaDataInsert&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&throughMPO="+through+"&devId="+devId;
					}
					else{
						url="./DocumentAdd.do?hmode=metaDataInsert&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>";
					}
					
					document.forms[0].action=url;
					document.forms[0].submit();
					return true;
				}
			}
		}
		function setText()
			{
			var a=document.forms[0].myfile.value;
			document.forms[0].myfilename.value=a.substring(a.lastIndexOf('\\')+1,a.lastIndexOf("."));
			document.forms[0].FileName.value=a.substring(a.lastIndexOf('\\')+1,a.length);
			document.forms[0].MIME.value=a.substring((a.lastIndexOf(".")+1),a.length);
		}
		
		
		/*
		function openSearchPage(){
			var url="./DocMSearch.do?linkToDoc=Yes";
			
			child = open(url,'popupwindow','width=800,height=600,scrollbars=yes,status=yes');
		}
		*/
		var newwindow = '';
		function openSearchPage() {
		var url="./DocMSearch.do?linkToDoc=Yes";
		
			if (!newwindow.closed && newwindow.location) {
				newwindow.location.href = url;
			}
			else {
				newwindow=window.open(url,'popupwindow1','width=800,height=600,scrollbars=yes,status=yes');
			if (!newwindow.opener) newwindow.opener = self;
			}
			if (window.focus) {newwindow.focus()}
			return false;
		}
		
	function currencyNumericOnly(e)
	{
		//alert(numeric only");
		var key;
		var keychar;

		if (window.event)
		   key = window.event.keyCode;
		else if (e)
		   key = e.which;
		else
		   return true;
		keychar = String.fromCharCode(key);
		keychar = keychar.toLowerCase();

		// control keys
		if ((key==null) || (key==0) || (key==8) ||
			(key==9) || (key==13) || (key==27) )
		   return true;

		// numbers
		else if ((("0123456789").indexOf(keychar) > -1)){
		   return true;
		   }
		else{
		alert("Only Numeric values allowed.");
		   return false;
		   }
	}//end of numericOnly
	function oncancel(){
		onComboChange();
	}
		function setLinkToOtherDocId(){
			var linkId;
			if(document.forms[0].LinkToOtherDocId){
				if(document.forms[0].LinkToOtherDocId.value==null)
					linkId="";
				else
				linkId=document.forms[0].LinkToOtherDocId.value;
			}
			else
				linkId="";
				var url="DocumentAdd.do?hmode=viewDocument&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&appendixId=<c:out value="${DocumentAddBean.appendixId}"/>&abc=y&libId="+document.forms[0].libId.value+"&docId="+linkId+"&Source=<c:out value='${requestScope.SourceType}'/>";
			if(linkId!=""){
				DocMSeacrhDao.getTitleFromDocId(linkId,{callback:function(str){ret=str;},async:false});
				if(ret!=null){
					document.all.LinkDocName.innerHTML=ret;	
					document.forms[0].LinkedDoc.value=url;
				}
			}
		}  
	</SCRIPT>
	<script>
jQuery(document).ready(function(){
	jQuery('.dateField').each(function(){
		jQuery(this).datepicker({
			buttonImage: 'images/calendar.gif',
				showOn : "both",
					yearRange: "-10:+5" ,
					disabled: true,
					changeMonth: true,
					changeYear: true
		}).next(".ui-datepicker-trigger").addClass("calImageStyle");
		jQuery('#ui-datepicker-div').css('display','none');
	});
	
});
</script>
		<bean:define id="libNameCombo" name = "libNameCombo" scope = "request" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
   <script language = "JavaScript" src = "javascript/functions.js"></script>
	<link rel="stylesheet" href="styles/style.css" type="text/css">
	<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
	</style>
	<%@ include  file="/MSAMenu.inc" %>
	<%@ include  file="/DashboardVariables.inc" %>
	<%@ include  file="/AppendixMenu.inc" %>
	<%@ include  file="/AddendumMenu.inc" %>
	<%@ include  file="/ProjectJobMenu.inc" %>
	<%@ include file="/DefaultJobMenu.inc" %>
	<%@ include file = "/NMenu.inc" %> 
</head>
	<BODY  onload="fun_HideShowBlock(0);setType();setLinkToOtherDocId();">
		<html:form  action="DocumentAdd"   name="DocumentAddBean" type="com.mind.docm.formbean.DocumentAddBean" enctype="multipart/form-data" >
		<html:hidden name="DocumentAddBean" property="libId"/>
		<html:hidden name="DocumentAddBean" property="myfilename" value=""/>
		<html:hidden name='DocumentAddBean'  property='appendixId' />
		<html:hidden name="DocumentAddBean" property="linkLibNameFromIlex"/>
		<html:hidden property="msaId" name='DocumentAddBean' />
		<html:hidden property="linkLibName" name='DocumentAddBean' />
			<logic:equal name = "DocumentAddBean" property = "type" value = "Appendix">
				<logic:equal name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
				
					<logic:equal name = "DocumentAddBean" property = "appendixType" value = "3">
						<%@ include  file="/NetMedXDashboardMenu.inc" %>
					</logic:equal>
					
					<logic:notEqual name = "DocumentAddBean" property = "appendixType" value = "3">
						<%@ include  file="/AppedixDashboardMenu.inc" %>
					</logic:notEqual>
					
				</logic:equal>
			</logic:equal>
			
			<html:hidden property="jobId" name='DocumentAddBean' />
			<input type="hidden" name="AddUser" value="<c:out value="${sessionScope.username}"/>"/>
			<input type="hidden" name="AddMethod" value="Explicit"/>
			<jsp:useBean id="now" class="java.util.Date" />
			<input type="hidden" name="AddDate" value="<fmt:formatDate value="${now}" pattern="dd-MMM-yyyy h:mm a" />"/>
		<c:if test="${empty requestScope.SourceType}">
			<input type='hidden' name='Source' value='DocM'/>
		</c:if>
		<c:if test="${not empty requestScope.SourceType}">
			<input type='hidden' name='Source' value='<c:out value="${requestScope.SourceType}"/>'/>
		</c:if>
		<input type="hidden" name="Category" value="Contract"/>
		<input type="hidden" name="MIME" value=""/>
		<input type="hidden" name="Application" value="FileType"/>
		
		<input type="hidden" name="Size" value=""/>
		 
			 
			<c:if test="${DocumentAddBean.job_Name ne 'Default Job' && requestScope.SourceType eq 'Ilex'}">
				<logic:equal name = "DocumentAddBean" property = "type" value = "MSA">			
	<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>
					<table border="0" cellspacing="0" cellpadding="0" width="100%">				
							
							<tr>
									<%-- 	<%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )){%>
												 <%if(msastatus.equals( "Cancelled" )){%>
										        	<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
										        <%}else{ %>
													<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
										        <%}%>
											  <%}else{%>
						                 			 <td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
												<%}%>
												<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
												<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
												<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
										<%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>			
												
									</tr>
									<tr>
									          <td background="images/content_head_04.jpg" height="21" colspan="7" width="100%">
								          		<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
								         	 </td>
									</tr>
									<tr>
											<td colspan="6">
												<h2>Document Add</h2>
											</td>
									</tr>
						</table>
				</logic:equal>
				<c:if test="${param.through ne 'MPO'}">
					<logic:equal name = "DocumentAddBean" property = "type" value = "Appendix">
	<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>				
					<logic:equal name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
						<logic:equal name = "DocumentAddBean" property = "appendixType" value = "3">
								<!--  Menu change -->
								
								<div id="menunav">

<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
         <a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
        	
        		
				<!-- <li><a href="ReportScheduleWindow.do?report_type=redline&typeid=3970&fromPage=NetMedX">Redline Report</a></li>
				<li><a href="report_type=detailsummary&typeid=3970&fromPage=NetMedX">Detailed Summary</a></li>
				<li><a href="ReportScheduleWindow.do?report_type=CSVSummary&typeid=3970&fromPage=NetMedX">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="ReportGeneration.do?firsttime=true">Update Report</a></li></ul></li></ul></li>
        	 -->
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocumentAddBean" property="appendixId"/>">Search</a>
  		</li>
  		
	</ul>
</li>
</div>
<table cellpadding="0" cellspacing="0" border="0" width = "100%">
							        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
								          		<div id="breadCrumb">&nbsp;</div>
								        </td>
							        </tr>
							        <td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.metmedxdashboardformsa"/>:&nbsp;<bean:write name="DocumentAddBean" property="msaName"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="DocumentAddBean" property="appendixName"/></h2></td> 
						      </table>								
								
								
								
								
								<!--  End of Menu  -->
								<%--  <table cellpadding="0" cellspacing="0" border="0" width = "100%">
								        <tr>
								        	<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
											<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
											<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocumentAddBean" property="appendixId"/>" style="width: 120px"><center>Search</center></a></td>
											<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
										</tr>
								        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
									          		<div id="breadCrumb">&nbsp;</div>
									        </td>
								        </tr>
								        <tr>
								        	<td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.metmedxdashboardformsa"/>:&nbsp;<bean:write name="DocumentAddBean" property="msaName"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="DocumentAddBean" property="appendixName"/></h2></td>
								        </tr> 
			      				</table> --%>
						</logic:equal>
							<logic:notEqual name = "DocumentAddBean" property = "appendixType" value = "3">
								<%@ include  file="/AppendixDashboardMenuScript.inc" %>
								<table cellpadding="0" cellspacing="0" border="0" width = "100%">
							        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
								          		<div id="breadCrumb">&nbsp;</div>
								        </td>
							        </tr>
							        <tr><td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.appendixdashboardfor"/>&nbsp;<bean:write name="DocumentAddBean" property="msaName" />&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name="DocumentAddBean" property="appendixName" /></h2></td></tr> 
						      </table>
							</logic:notEqual>
					</logic:equal>
					<logic:notEqual name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
						<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
								<tr>
									        <%-- <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )){%>
									        	<%if(appendixStatus.equals( "Cancelled" )){%>
									        		<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
									        	<%}else{ %>
													<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
									        	<%}%>
											<%}else{%>	
													<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
												<%}%>	
							
											<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
											<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
											<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td> --%>
									
									<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>			
											
								        </tr>
							        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
									         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="DocumentAddBean" property ="msaId"/>&firstCall=true"><bean:write name  ="DocumentAddBean" property ="msaName"/></a></div>
									    </td>
					 				</tr>
					 			<tr>
										<td colspan="6">
											<h2>Document Add</h2>
								</td>
								</tr>
							</table>
					</logic:notEqual>
					</logic:equal>
				</c:if>
				<logic:equal name = "DocumentAddBean" property = "type" value = "Addendum">
					<%@ include  file="/Menu_Addendum.inc" %>
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
						         <div id="breadCrumb">&nbsp;</a></div>
						    </td>
						</tr>
						<tr><td colspan="6"><h2>Document Add</h2></td></tr>
					</table>
				</logic:equal>
				<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
					<logic:equal name = "DocumentAddBean" property = "type" value = "Job">
						<%@ include  file="/Menu_ProjectJob.inc" %>
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
							         <div id="breadCrumb">&nbsp;</a></div>
							    </td>
							</tr>
							<tr><td colspan="6"><h2>Document Add</h2></td></tr>
						</table>
					</logic:equal>
				</c:if>
				<c:if test="${param.through eq 'MPO'|| param.through eq 'PO'}">
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr><td class="headerrow" height="19">&nbsp;</td></tr>
							<tr>
								<td background="images/content_head_04.jpg" height="21" width="100%">
									<div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
														 <a href="#">Add</a></div>
								</td>
							</tr>
							<tr><td colspan="6"><h2>Document Add</h2></td></tr>
					</table>
				</c:if>
				<logic:equal name = "DocumentAddBean" property = "type" value = "MSA">
					<%@ include  file="/MSAMenuScript.inc" %>
				</logic:equal>
				<c:if test="${param.through ne 'MPO'}">
					<logic:equal name = "DocumentAddBean" property = "type" value = "Appendix">
						<logic:equal name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
							<logic:equal name = "DocumentAddBean" property = "appendixType" value = "3">
								<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
							</logic:equal>
							<logic:notEqual name = "DocumentAddBean" property = "appendixType" value = "3">
								<%@ include  file="/AppedixDashboardMenuScript.inc" %>
							</logic:notEqual>
						</logic:equal>
						<logic:notEqual name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
							<%@ include  file="/AppendixMenuScript.inc" %>
						</logic:notEqual>
					</logic:equal>
				</c:if>
				<logic:equal name = "DocumentAddBean" property = "type" value = "Addendum">
				<%@ include  file="/AddendumMenuScript.inc" %>
				</logic:equal>
				<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
					<logic:equal name = "DocumentAddBean" property = "type" value = "Job">
						<%@ include  file="/ProjectJobMenuScript.inc" %>
					</logic:equal>
				</c:if>
			</c:if>
			<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
				<c:if test="${DocumentAddBean.job_Name eq 'Default Job' && requestScope.SourceType eq 'Ilex'}">
					<%@ include  file="/Menu_DefaultJob.inc" %>
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
							<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
									 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
									 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
									  <a><span class="breadCrumb1">Document Add</a>
			    				</td>
						</tr>
						<tr><td colspan="6"><h2>Document Add</h2></td></tr>
					</table>
				</c:if>
			</c:if>
			<c:if test="${requestScope.SourceType ne 'Ilex'}">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr><td class="headerrow" height="19">&nbsp;</td></tr>
						<tr>
							<td background="images/content_head_04.jpg" height="21" width="100%">
								<div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
													 <a href="#">Add</a>
													 		
							</td>
							
						</tr>
						<tr><td><h2>Document Add</h2></td></tr>
				</table>
			</c:if>
		
	 <c:if test="${requestScope.appendixStatus eq 'pmAppendix'}">
	 <table border="0" cellspacing="1" cellpadding="0" width="100%">
		<tr><td class="headerrow" height="19" width = "100%" >&nbsp;</td></tr>
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
         			
    				</td>
		</tr>
		<tr><td><h2>Document Add</h2></td></tr>
	</table>
	 </c:if>
<TABLE WIDTH="100%" cellspacing="1" cellpadding="1">
	<tr><td>
	<TABLE WIDTH="100%" cellspacing="0" cellpadding="0" border='0'>
			<c:choose>
				<c:when test="${! empty  requestScope.Comment}">
					<TR>
						<TD  class="message" height=5 &nbsp; />
							<c:out value="${requestScope.Comment}"/>
						</TD>
					</TR>
				</c:when>	
			</c:choose>	
				<TR>
				 	<TD >
						<TABLE width="30%"   cellspacing="1" cellpadding="1" border='0'>
							 <TR>
							 	<TD width="30%" class="colDark" nowrap="nowrap"> Add to Library</TD>
								 	<TD class="colLight" colspan= "4">
								 	<html:select styleClass="select"  name="DocumentAddBean" property="libName" size="1" onchange="return onComboChange();">
										<html:optionsCollection name="libNameCombo"  property = "librarylist"  label = "label"  value = "value" />
									</html:select>
								</TD>	
							  </TR>
						</TABLE>
				 	</TD>
				 </TR>
		
				<TR>
					<TD align='left' valign="TOP" >		
						<TABLE  cellspacing=0 cellpadding=0 border="0">
							<TR>
								<c:forEach items="${requestScope.taxonomy_group}" var="taxonomy_group" varStatus="index" >
									<td width='10%' id="TOP_TAB" class='leftRightBorder'>
										<table cellpadding="0" cellspacing="0" width='100%'>
											<tr >
												<td id="TOP_BUTTON" style="cursor:hand;"
													onClick="fun_HideShowBlock('<c:out value="${index.index}"/>');" align="center" width="2px"
													nowrap><img src="docm/images/left-cor.gif" border="0"></td>
												<td id="TOP_BUTTON" style="cursor:hand;"
													onClick="fun_HideShowBlock('<c:out value="${index.index}"/>');" align="center" width="80%" class='drop'
													nowrap bgcolor="#B5C8D9"><c:out value="${taxonomy_group[1]}"/></td>
												<td id="TOP_BUTTON" style="cursor:hand;"
													onClick="fun_HideShowBlock('<c:out value="${index.index}"/>');" align="center" width="2px"
													nowrap><img src="docm/images/right-cor.gif" border="0"></td>
											</tr>
										</table>
									</td>
								</c:forEach>
							</TR>
						</TABLE>
				 	</TD>
				 </TR>
				<TR>
					<TD valign="TOP" >	
					<table  width='100%' 
						style="border-color: #D9DFEF;border-width: 4px;border-style: solid;"
					 cellspacing=0 cellpadding=0 border="0"   valign="top" ><tr><td height='250' width='100%'  valign="top">
						<TABLE WIDTH="100%"  cellspacing=0 cellpadding=0 border="0"  valign="top">
							
							<c:forEach items="${requestScope.taxonomy_group}" var="taxonomy_group" varStatus="index" >
							
							<c:set var = "checkReadable" value = ""/>
							<c:if test = "${taxonomy_group[0] eq '4' && requestScope.SourceType ne 'DocM'}">
								<c:set var = "checkReadable" value = "readonly='readonly'"/>
							</c:if>
								<tr id="tab" style="display: none;" valign="top">
									<td colspan=100>
										<table width="100%"  cellspacing=1 cellpadding=1 >
										
										<c:set var="idx" value="0"/>
											
											<c:forEach items="${requestScope.metadata_field_list}" var="metadata_field_list" varStatus="status"> 
												<c:if test="${metadata_field_list[9] eq 'Y'}">
												<c:if test="${taxonomy_group[0] eq metadata_field_list[2]}">
														<c:forEach items="${metadata_field_list[8]}" var="metadata_field_type" varStatus="status"> 
															<c:if test="${metadata_field_list[9] eq 'Y'}">	
																<c:if test="${(idx%'2')=='0'}"><tr></c:if>
																	<c:set var="idx" value="${idx+1}"/>
																	<c:if test="${metadata_field_list[3] ne 'comment'}">
																		<td class='Ntextoleftalignnowrap' width='170' nowrap>
																		
																		<c:out value="${metadata_field_list[4]}"/>
																		<c:if test="${requestScope.view ne 'view'}">
																			<c:if test="${metadata_field_list[12] == 'Y'}">
																				<font class="redbold">*</font>
																			</c:if>
																		</c:if>
																		</td>
																	</c:if>
																	<c:if test="${metadata_field_list[3] ne 'comment'}">
																		<input type="hidden" name="comment" value=""/>
																	</c:if>
																		<td class='Ntexteleftalignnowrap'  nowrap>
																			<c:set var="fieldValue" value="${metadata_field_list[5]}"/>
																			<c:choose>
																				<c:when test="${metadata_field_type[0] eq 'textbox' && metadata_field_list[3] ne 'comment' 
																				&& metadata_field_list[3] ne 'LinkToOtherDocId' && metadata_field_type[2] ne 'max'}">
																					<input type="text" class='text' name="<c:out value="${metadata_field_list[3]}"/>" 
																					<c:if test="${metadata_field_type[1] == 'numeric'}">onkeypress="return currencyNumericOnly(this.event);"</c:if> 
																					id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>" 
																					<c:if test="${metadata_field_list[11] eq 'Y'  }">readonly="readonly"</c:if> <c:out value = "${checkReadable}"/> <c:if test="${metadata_field_type[1] ne ''}">maxlength="<c:out value="${metadata_field_type[2]}"/>"</c:if> 
																					 value="<c:out value="${fieldValue}"/>" />
																				</c:when>
																				
																				<c:when test="${metadata_field_type[0] eq 'textbox' && metadata_field_list[3] ne 'comment' && metadata_field_type[2] eq 'max'}">
																					<TEXTAREA name="<c:out value="${metadata_field_list[3]}"/>" onchange="callChange();" 
																					<c:if test="${metadata_field_type[1] == 'numeric'}">onkeypress="return currencyNumericOnly(this.event);"</c:if> 
																					id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>"  
																					<c:if test="${metadata_field_list[11] eq 'Y'}">readonly="readonly"</c:if>  
																					value="<c:out value="${fieldValue}"/>" <c:out value = "${checkReadable}"/>  cols="50" rows="3" ></TEXTAREA>								 		  
																				</c:when>
																				
																				<c:when test="${metadata_field_list[3] == 'LinkToOtherDocId'}">
																					<input type="hidden" name="LinkedDoc" />
																					<input type="hidden" name="<c:out value="${metadata_field_list[3]}"/>" value="<c:out value="${fieldValue}"/>"/>
																						<table width="100%">
																						<tr >
																							<td align="left" class='Ntexteleftalignnowrap' width="85%" >
																								<a href="#" id="LinkDocName" onclick="callOpenLinkedDetail();"></a>&nbsp;
																							</td>
																							<td  align="right" class='Ntexteleftalignnowrap' width="15%">	
									  															<input type="button" name="LinkToDoc"  onclick="openSearchPage();"  title="Click to Link with other Document" value="..." />&nbsp;&nbsp;
									  														</td>
									  													</tr>
									  												</table>
																				</c:when>
																				<c:when test="${metadata_field_type[0] eq 'checkbox'}">
																					<c:set var="check" value=""/>
																					<c:if test="${(metadata_field_list[5] eq '0')}">
																						<c:set var="check" value="0"/></c:if>
																					<c:if test="${(metadata_field_list[5] eq '1')}">
																						<c:set var="check" value="1"/></c:if>
																					<input type="radio"  name ="<c:out value="${metadata_field_list[3]}"/>" <c:out value = "${checkReadable}"/> 
																						<c:if test="${empty check && metadata_field_list[3] eq 'ViewableOnWeb'}">checked</c:if>
																					id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>"
																					 value="1" onchange="setCheckedValue(this,'1');" <c:if test="${check == '1'}">checked</c:if> /><span style="font-size: 11px;">Yes
																					<input type="radio" <c:if test="${metadata_field_list[3] == 'ChangeControlType'}">checked</c:if> <c:out value = "${checkReadable}"/> 
																										name ="<c:out value="${metadata_field_list[3]}"/>" 
																					id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>" 
																					value="0" onchange="setCheckedValue(this,'0');"/>
																						<span style="font-size: 11px;">No</span>
																					
																				</c:when>
																				
																				<c:when test="${metadata_field_type[0] eq 'datetime'}">
																				
																					<input type="text"  size="10" readonly="readonly" value="<c:out value="${fieldValue}"/>" 
																					id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>" 
																					  name="<c:out value="${metadata_field_list[3]}"/>" class="textbox dateField"></td>
																				</c:when>
																				
																				<c:when test="${metadata_field_type[0] eq 'combo'}">
																				
																					<select id="docStatus" name= "<c:out value="${metadata_field_list[3]}"/>" <c:out value = "${checkReadable}"/> 
																					id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/>
																					</c:if>" name="<c:out value="${metadata_field_list[3]}"/>" class = "select" >
																						<c:forEach items="${metadata_field_list[13]}" var="combo" varStatus="comboStatus">
																							<option value="<c:out value="${combo[1]}"/>" ><c:out value="${combo[1]}"/></option>
																						</c:forEach>
																					</select>
																				</c:when>
																				
																				<c:otherwise>
																					<input type="hidden" name="<c:out value="${metadata_field_list[3]}"/>" size="10"  value="<c:out value="${fieldValue}"/>"/>
																				</c:otherwise>
																			</c:choose>

																	</td>
																	<c:if test="${(idx%'2')=='0'}"></tr></c:if>
																</c:if>
															</c:forEach>
														</c:if>
													</c:if>
													<c:if test="${metadata_field_list[9] eq 'N'}">
														<c:if test="${taxonomy_group[0] eq metadata_field_list[2]}">
															<c:forEach items="${metadata_field_list[8]}" var="metadata_field_type" varStatus="status"> 
																	
																		<c:if test="${metadata_field_list[9] ne 'Y'}">
																			<c:if test="${fields.name eq metadata_field_list[3]}">
																				<input type="hidden" name="<c:out value="${metadata_field_list[3]}"/>" value="<c:out value="${metadata_field_list[5]}"/>" />
																			</c:if>
																		</c:if>
															</c:forEach>
														</c:if>
													</c:if>	
													
											</c:forEach>
												<input type='hidden' id='tab<c:out value="${index.index}"/>_fieldcount' value='<c:out value="${idx}"/>'/>
										</table>
									</td>
								</TR>
							</c:forEach>
						</table>
					  </td></tr></table>
				 	</TD>
				 </TR>
				<TR>
					<TD valign="TOP" >	
						<TABLE WIDTH="100%"  cellspacing=1 cellpadding=0 border="0" id="buttonsTable" >
								 <TR>
								 	<TD style="width: 3cm;" class="colDark" nowrap="nowrap"> Upload New File from </td>
									 	<TD  class = "colLight" >
									 	<html:file  property="myfile" style="width: 12cm;" styleClass="button_c" onchange="setText();"  onkeypress="return false;"/>
									</TD>	
								  </TR>
								<tr>
								  	<td class = "colLight" align="center" colspan='2'>
								  	<input type="button" name="button1" onclick="saveData();" class="button_c" value="Upload/Save" />&nbsp;&nbsp;
								   	<input type="reset" name="button2" class="button_c" onclick="oncancel();" value="Cancel" />
								</tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
</td></tr></TABLE>
 <c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
	
		<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/DefaultJobMenuScript.inc" %>
	</c:if>
	
</c:if>
		</html:form>
	</BODY>
</HTML>