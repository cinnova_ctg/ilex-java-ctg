<!DOCTYPE HTML>
<HTML>
<HEAD>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ page import="java.util.*" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
String MSA_Id ="";
String msastatus="";
String uploadstatus ="";
String status ="";

if(request.getAttribute("status")!=null){
	status = request.getAttribute("status").toString();
	msastatus = request.getAttribute("status").toString();
}
if(request.getAttribute("MSA_Id")!=null)
	MSA_Id= request.getAttribute("MSA_Id").toString();

if(request.getAttribute("uploadstatus")!=null)
	uploadstatus= request.getAttribute("uploadstatus").toString();
	
	
%>
<script type="text/javascript" language="JavaScript1.2" src="docm/javascript/prototype.js"></script>
<script type='text/javascript' src='./dwr/interface/DocMSeacrhDao.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>
<script language="javascript" src="javascript/JLibrary.js"></script>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="Content-Style-Type" content="text/css">

<TITLE></TITLE>
	<HEAD>
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language="JavaScript" src="javascript/date-picker.js"></script>
	<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
   <script language = "JavaScript" src = "javascript/functions.js"></script>
	<link rel="stylesheet" href="styles/style.css" type="text/css">
	<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
	</style>
	<script>
			function currencyNumericOnly(e)
			{

				var key;
				var keychar;
				if (window.event){
				   key = window.event.keyCode;
				   }
				else if (e){
				   key = e.which;
				   }
				else{
				   return true;
				   }
				keychar = String.fromCharCode(key);
				keychar = keychar.toLowerCase();
		
				// control keys
				if ((key==null) || (key==0) || (key==8) ||
					(key==9) || (key==13) || (key==27) ){
				   return true;
				   }
		
				// numbers
				else if ((("0123456789").indexOf(keychar) > -1)){
				   return true;
				   }
				else{
				alert("Only Numeric values allowed.");
				   return false;
		   }
	}//end of numericOnly
	</script>
		<SCRIPT LANGUAGE="JavaScript">
		function fun_HideShowBlock(index)
		{
       var len=document.all.tab.length;
        for(var id=0;id<len;id++){
        	if(document.all.tab[id].style.display=="block"){
        		document.all.tab[id].style.display="none";
        		}
        }
 		document.all.tab[index].style.display="block";
		if(document.forms[0].FileName!=null)
			document.forms[0].FileName.disabled=true;
		buttonPressed(index);
		}
		
		function buttonPressed(idx)
		{
			for(var i=0;i<document.all.TOP_BUTTON.length/3;i++) {

				document.all.TOP_BUTTON[3*i+1].style.fontSize='10';
				document.all.TOP_BUTTON[3*i+1].width='150';
				document.all.TOP_BUTTON[3*i+1].style.fontFamily='Verdana|Arial|Helvetica|sans-serif';
				document.all.TOP_TAB[i].style.width='5';

				if(i==idx)
				{
					document.all.TOP_BUTTON[3*i].innerHTML='<img src="docm/images/left-cor-light.gif" width="5" height="18" border="0">';
					document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#D9DFEF';
					document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="docm/images/right-cor-light.gif" width="5" height="18"  border="0">';
					document.all.TOP_BUTTON[3*i+1].style.borderBottom = '0px #D9DFEF solid';
					document.all.TOP_BUTTON[3*i+1].style.fontWeight='bold';
				}
				else//Restore Remaining
				{
					document.all.TOP_BUTTON[3*i].innerHTML='<img src="docm/images/left-cor.gif" width="5" height="18"  border="0">';
					document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#B5C8D9';
					document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="docm/images/right-cor.gif" width="5" height="18"  border="0">';
				    document.all.TOP_BUTTON[3*i+1].style.borderBottom = '2px #B5C8D9 solid';
					document.all.TOP_BUTTON[3*i+1].style.fontWeight='normal';
				}
			}
		}
		
		function openSearchPage(){
			var url="./DocMSearch.do?linkToDoc=Yes&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&Source=<c:out value='${requestScope.SourceType}'/>";
			child = open(url,'popupwindow','width=800,height=600,scrollbars=yes,status=yes');
		}
		function openLinkedDetailPage(url){
			
			child = open(url,'popupwindow','width=800,height=420,scrollbars=yes,status=yes');
		}
		function callOpenLinkedDetail(){
		
			openLinkedDetailPage(document.forms[0].LinkedDoc.value);
		}
		function is_empty(chkObj, message)
		{
			if(trim(chkObj.value) == '')
				{
					alert(message + ' is required.');
					chkObj.focus();
					return true;
				}
			return false;
		}
		
		function validateIsEmpty(){
			var len=document.all.tab.length;
			for(var i=0;i<len;i++){
				var count=document.getElementById('tab'+i+'_fieldcount').value;
				for(var j=1;j<=count;j++){
					fun_HideShowBlock(i);
					var element = document.getElementById('tab'+i+'_notempty_'+j);
					if(element != null && element.name!='FileName'){
						if(is_empty(element,element.name)){
							return false;
						}
					}
				}
			}
			return true;
		}
		var change=false;
		function callChange(){
			change=true;
		}
		function onMoveDocument()
			{
				document.forms[0].libName.disabled=false;
			}
		function onChangeLibrary()
			{
				if(change==true && document.forms[0].libName.value!=0){
					var flag=confirm("Changes will be lost.Do you want to continue? ");
 					if(flag==false) return false;
 				}
				if(document.forms[0].libName.value==0){
					document.forms[0].Type.value="";
					return false;
				}else if(document.forms[0].libName.value!=0) {
				document.forms[0].libName.disabled=false;
				document.forms[0].libId.value=document.forms[0].libName.value;
				var url="./DocumentAdd.do?hmode=moveDocument&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&libraryID=<c:out value='${requestScope.libId}'/>&libId="+document.forms[0].libId.value+"&state=<c:out value='${param.state}'/>&docId=<c:out value="${requestScope.docId}"/>&Source=<c:out value='${requestScope.SourceType}'/>&isLibraryChange=yes";
				document.forms[0].action=url;
				document.forms[0].submit();
				return true;
				}
			}
			
				
		function saveData(){
			if(document.forms[0].libName.value==0){
				alert("Please select the Library.");
				return false;
			}else if (document.getElementById("docStatus").value == 'Superceded' ||  document.getElementById("docStatus").value  == 'Deleted'){
				alert("Documents with status Approved or Draft can only be added.");
				return false;
			}else{
			document.forms[0].libId.value=document.forms[0].libName.value;
			var a;
			document.forms[0].myfilename.value=document.forms[0].FileName.value;
			<c:if test="${requestScope.move eq 'moveDocument'}">
				a="move=yes";
			</c:if>
			var docId="";
			if('<c:out value="${requestScope.docId}"/>'==''){
				docId='<c:out value="${param.docId}"/>';
			}else{
				docId='<c:out value="${requestScope.docId}"/>';
			}
			//a=a+"&Source=<c:out value='${requestScope.SourceType}'/>";
			url="DocumentAdd.do?hmode=metaDataInsert&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&docId="+docId+"&"+a+"&isLibraryChange=<c:out value='${requestScope.isLibraryChange}'/>";
			if(document.forms[0].FileName!=null)
				document.forms[0].FileName.disabled=false;
				
			var len=document.all.ChangeControlType.length;
			for(var i=0;i<len;i++){
				document.forms[0].ChangeControlType[i].disabled=false;
			}
			document.forms[0].button2.disabled=true;
			document.forms[0].action=url;
			if(!validateIsEmpty())
				return false;
			else if(validateIsEmpty()==true)
				document.forms[0].submit();
			}

		}
		
		
		function setText()
			{
			var a=document.forms[0].myfile.value;
			document.forms[0].myfilename.value=a.substring(a.lastIndexOf('\\')+1,a.lastIndexOf("."));
			document.forms[0].FileName.value=a.substring(a.lastIndexOf('\\')+1,a.length);
			document.forms[0].MIME.value=a.substring((a.lastIndexOf(".")+1),a.length);
		}
		
		function viewDocument(){
		var url="";

		 url="DocumentMaster.do?hmode=unspecified&status=approved&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&libId=<c:out value="${requestScope.libId}"/>&docId=<c:out value="${requestScope.docId}"/>&Source=<c:out value='${requestScope.SourceType}'/>";
		document.forms[0].action=url;
		document.forms[0].submit();
		}
		
		function modifyDocument(){

		var url="";
		url="DocumentAdd.do?hmode=modifyDocument&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&libId=<c:out value="${requestScope.libId}"/>&docId=<c:out value="${requestScope.docId}"/>&Source=<c:out value='${requestScope.SourceType}'/>";
		document.forms[0].action=url;
		document.forms[0].submit();
		}
		function moveToSource(source){
		var url="";
		if(source!='Ilex'){
			url="DocumentMaster.do?hmode=unspecified&status=approved&libId=<c:out value="${requestScope.libId}"/>&docId=<c:out value="${requestScope.docId}"/>";
		}else if('<c:out value="${DocumentAddBean.entityType}"/>'=='MSA' && source=='Ilex'){
			url="MSAUpdateAction.do";
		}else if('<c:out value="${DocumentAddBean.entityType}"/>'=='MSA Supporting Documents' && '<c:out value="${DocumentAddBean.linkLibNameFromIlex}"/>'=='MSA' && source=='Ilex'){
			url="MSAUpdateAction.do";
			}
		else if('<c:out value="${DocumentAddBean.entityType}"/>'=='Appendix' && source=='Ilex'){
			if('<c:out value="${DocumentAddBean.appendixStatusChecking}"/>' == 'Inwork'){
					url="AppendixHeader.do?function=view&fromPage=appendix&appendixid=<c:out value="${DocumentAddBean.entityId}"/>&entityType=<c:out value="${param.entityType}"/>";
				}else{
				url="AppendixUpdateAction.do?firstCall=true&MSA_Id=<c:out value="${DocumentAddBean.msaId}"/>";
				}
		}else if('<c:out value="${DocumentAddBean.entityType}"/>'=='Appendix Supporting Documents' && '<c:out value="${DocumentAddBean.linkLibNameFromIlex}"/>'=='Appendix' && source=='Ilex'){
			if('<c:out value="${DocumentAddBean.appendixStatusChecking}"/>' == 'Inwork'){
					url="AppendixHeader.do?function=view&fromPage=appendix&appendixid=<c:out value="${DocumentAddBean.entityId}"/>&entityType=<c:out value="${param.entityType}"/>";
				}else{
				url="AppendixUpdateAction.do?firstCall=true&MSA_Id=<c:out value="${DocumentAddBean.msaId}"/>";
				}
			}
		else if('<c:out value="${DocumentAddBean.entityType}"/>'=='Appendix Supporting Documents' && ('<c:out value="${DocumentAddBean.linkLibNameFromIlex}"/>'=='Addendum' || '<c:out value="${DocumentAddBean.linkLibNameFromIlex}"/>'=='Job') && source=='Ilex'){
			url="JobDashboardAction.do?hmode=unspecified&jobid=<c:out value="${DocumentAddBean.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&jobid=<c:out value="${DocumentAddBean.jobId}"/>&appendixId=<c:out value="${DocumentAddBean.appendixId}"/>";
		}
		else if('<c:out value="${DocumentAddBean.entityType}"/>'=='Addendum' && source=='Ilex'){
			url="JobDashboardAction.do?hmode=unspecified&jobid=<c:out value="${DocumentAddBean.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&MSA_ID=<c:out value="${DocumentAddBean.msaId}"/>&appendixId=<c:out value="${DocumentAddBean.appendixId}"/>";
		}else if('<c:out value="${DocumentAddBean.entityType}"/>'=='Supplement' && '<c:out value="${DocumentAddBean.linkLibNameFromIlex}"/>'=='Addendum' && source=='Ilex'){
			url="JobDashboardAction.do?hmode=unspecified&jobid=<c:out value="${DocumentAddBean.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&MSA_ID=<c:out value="${DocumentAddBean.msaId}"/>&appendixId=<c:out value="${DocumentAddBean.appendixId}"/>";
		}else if('<c:out value="${DocumentAddBean.entityType}"/>'=='Supplement' && '<c:out value="${DocumentAddBean.linkLibNameFromIlex}"/>'=='Job' && source=='Ilex'){
			url="JobDashboardAction.do?hmode=unspecified&jobid=<c:out value="${DocumentAddBean.entityId}"/>&appendixId=<c:out value="${DocumentAddBean.appendixId}"/>";
		}else if('<c:out value="${DocumentAddBean.entityType}"/>'=='Deliverables' && source=='Ilex'){
			url="JobDashboardAction.do?hmode=unspecified&jobid=<c:out value="${DocumentAddBean.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&jobid=<c:out value="${DocumentAddBean.jobId}"/>";
		}else if('<c:out value="${DocumentAddBean.entityType}"/>'=='PO' && source=='Ilex'){
			url="POAction.do?hmode=unspecified&appendixType=&tabId=0&appendix_Id=<c:out value="${DocumentAddBean.appendixId}"/>&jobId=<c:out value="${DocumentAddBean.jobId}"/>&poId=<c:out value="${DocumentAddBean.entityId}"/>";
		}else if('<c:out value="${DocumentAddBean.entityType}"/>'=='WO' && source=='Ilex'){
			url="POAction.do?hmode=unspecified&appendixType=&tabId=0&appendix_Id=<c:out value="${DocumentAddBean.appendixId}"/>&jobId=<c:out value="${DocumentAddBean.jobId}"/>&poId=<c:out value="${DocumentAddBean.entityId}"/>";
		}else{
			url="DocumentMaster.do?hmode=unspecified&status=approved&libId=<c:out value="${requestScope.libId}"/>&docId=<c:out value="${requestScope.docId}"/>";
		}
		
		document.forms[0].action=url;
		document.forms[0].submit();
		}
		
		function deleteDocument(){
		var url="DocumentAdd.do?hmode=viewDocument&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&libId=<c:out value="${requestScope.libId}"/>&docId=<c:out value="${requestScope.docId}"/>&delete=DeleteNow&Source=<c:out value='${requestScope.SourceType}'/>";
		document.forms[0].action=url;
		document.forms[0].submit();
		}
	
	function changeLibName(){
			var libName='<c:out value="${requestScope.libName}"/>';
			document.forms[0].Type.value=libName;
			var len=document.forms[0].libName.options.length;
			for(i=0;i<len;i++){
				if(document.forms[0].libName.options[i].text==libName){
					document.forms[0].libName.options[i].selected =true;
					document.forms[0].libId.value=document.forms[0].libName.value;
				}
			}	
		  }
		function setLinkToOtherDocId(){
			var linkId;
			
			if(document.forms[0].LinkToOtherDocId.value!=null)
				linkId=document.forms[0].LinkToOtherDocId.value;
				var url="DocumentAdd.do?hmode=viewDocument&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&appendixId=<c:out value="${DocumentAddBean.appendixId}"/>&abc=y&libId="+document.forms[0].libId.value+"&docId="+linkId+"&Source=<c:out value='${requestScope.SourceType}'/>";
			
			if(linkId!=""){
				DocMSeacrhDao.getTitleFromDocId(linkId,{callback:function(str){ret=str;},async:false});
				if(ret!=null){
					document.all.LinkDocName.innerHTML=ret;	
					document.forms[0].LinkedDoc.value=url;
				}
			}
		}  
		
	</SCRIPT>
	<script>
	function deleteNow(){
		var flag=confirm("Do you really want to delete the document? ");
 			if(flag==false)return false;
		
		var url="DocumentAdd.do?hmode=deleteDocument&linkLibName=<c:out value="${DocumentAddBean.linkLibNameFromIlex}"/>&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&libId=<c:out value="${requestScope.libId}"/>&docId=<c:out value="${requestScope.docId}"/>&Source=<c:out value='${requestScope.SourceType}'/>";
		document.forms[0].action=url;
		document.forms[0].submit();
		return true;
	}
		
	</script>
	<script>
		function resetPage()
		{
			url = 'DocumentAdd.do?hmode=modifyDocument&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&libId=<c:out value="${requestScope.libId}"/>&state=<c:out value="${param.state}"/>&docId=<c:out value="${requestScope.docId}"/>&Source=<c:out value='${requestScope.SourceType}'/>';
			document.forms[0].action = url;
			document.forms[0].submit();
		}
	
	</script>
<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>		 
		
<%-- <%@ include  file="/MSAMenu.inc" %> --%>
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
	<%@ include file="/DefaultJobMenu.inc" %>
<%@ include file = "/NMenu.inc" %> 



</head>
	<BODY  onload="fun_HideShowBlock(0);<c:if test="${DocumentAddBean.hmode eq 'modifyDocument' || DocumentAddBean.hmode eq 'moveDocument'}">changeLibName();</c:if>setLinkToOtherDocId();">
		<html:form method="post"  action="DocumentAdd" name="DocumentAddBean" type="com.mind.docm.formbean.DocumentAddBean" enctype="multipart/form-data">
		<html:hidden name="DocumentAddBean" property="libId" />
		<html:hidden name="DocumentAddBean" property="myfilename" />
		<html:hidden name="DocumentAddBean" property="hmode"/>
		<html:hidden name="DocumentAddBean" property="dm_doc_file_id"/>
		<html:hidden name="DocumentAddBean" property="MIME"/>
		<html:hidden name="DocumentAddBean" property="linkLibNameFromIlex"/>
		<html:hidden property="linkLibName" name='DocumentAddBean' />
		<logic:equal name = "DocumentAddBean" property = "type" value = "Appendix">
			<logic:equal name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
			
				<logic:equal name = "DocumentAddBean" property = "appendixType" value = "3">
					<%@ include  file="/NetMedXDashboardMenu.inc" %>
				</logic:equal>
				
				<logic:notEqual name = "DocumentAddBean" property = "appendixType" value = "3">
					<%@ include  file="/AppedixDashboardMenu.inc" %>
				</logic:notEqual>
				
			</logic:equal>
		</logic:equal>
		<html:hidden name='DocumentAddBean'  property='appendixId' />
		<html:hidden property="msaId" name='DocumentAddBean' />
		<html:hidden property="jobId" name='DocumentAddBean' />
		<input type="hidden" name="UpdateMethod" value="Explicit"/>
		<jsp:useBean id="now" class="java.util.Date" />
		<input type="hidden" name="UpdateDocumentUser" value="<c:out value="${sessionScope.username}"/>"/>
		<input type="hidden" name="UpdateDocumentDate" value="<fmt:formatDate value="${now}" pattern="dd-MMM-yyyy h:mm a" />"/>
		<input type="hidden" name="LinkedDoc" />
		<c:choose>
			<c:when test="${param.delete == 'DeleteNow'}">
				<input type="hidden" name="DeleteDate" value="<fmt:formatDate value="${now}" pattern="dd-MMM-yyyy h:mm a" />"/>
				<input type="hidden" name="DeleteUser" value="<c:out value="${sessionScope.username}"/>"/>
			</c:when>
		</c:choose>

		<c:if test="${DocumentAddBean.job_Name ne 'Default Job' && requestScope.SourceType eq 'Ilex' && param.abc ne 'y'}">
			<logic:equal name = "DocumentAddBean" property = "type" value = "MSA">			
				<table border="0" cellspacing="0" cellpadding="0" width="100%">				
						<tr>
								<%-- 	<%if(!msastatus.equals( "Draft") && !msastatus.equals( "Review" )){%>
											 <%if(msastatus.equals( "Cancelled")){%>
									        	<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
									        <%}else{ %>
												<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
									        <%}%>
										  <%}else{%>
					                 			 <td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
											<%}%>
											<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
											<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
											<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
							 <%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>			
								</tr>
								<tr>
								          <td background="images/content_head_04.jpg" height="21" colspan="7" width="100%">
							          		<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
							         	 </td>
								</tr>
								<tr>
										<td colspan="6">
											<h2>Document <c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
															 Modify: <bean:write name = "DocumentAddBean" property = "msaName" />
												</c:if>
												<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
																Move 
													</c:if>
												<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
													<c:if test="${param.delete != 'DeleteNow'}">
																 View: <bean:write name = "DocumentAddBean" property = "msaName" />
													</c:if>
													<c:if test="${param.delete == 'DeleteNow'}">
																 Delete
													</c:if>
												</c:if></h2>
										</td>
								</tr>
					</table>
			</logic:equal>
			<c:if test="${not empty requestScope.isLibraryChange }">
				 <table border="0" cellspacing="1" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19" width = "100%" >&nbsp;</td></tr>
					<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         			<div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
								<a href="#">View</a>
			    				</td>
					</tr>
					
				</table>
			 </c:if>	
			<logic:equal name = "DocumentAddBean" property = "type" value = "Appendix">
			<logic:equal name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
				<logic:equal name = "DocumentAddBean" property = "appendixType" value = "3">
						 <table cellpadding="0" cellspacing="0" border="0" width = "100%">
						        <tr>
						        	<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
									<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
									<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocumentAddBean" property="appendixId"/>" style="width: 120px"><center>Search</center></a></td>
									<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
								</tr>
						        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
							          		<div id="breadCrumb">&nbsp;</div>
							        </td>
						        </tr>
						        <tr>
						        	<td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.metmedxdashboardformsa"/>:&nbsp;<bean:write name="DocumentAddBean" property="msaName"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="DocumentAddBean" property="appendixName"/></h2></td>
						        </tr> 
	      				</table>
				</logic:equal>
					<logic:notEqual name = "DocumentAddBean" property = "appendixType" value = "3">
						<table cellpadding="0" cellspacing="0" border="0" width = "100%">
					        <tr> 
						          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage</center></a></td>
						          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
						     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="DocumentAddBean" property="appendixId"/>&msaId=<bean:write name="DocumentAddBean" property="msaId"/>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
						          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
							</tr>
					        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
						          		<div id="breadCrumb">&nbsp;</div>
						        </td>
					        </tr>
					        <tr><td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.appendixdashboardfor"/>&nbsp;<bean:write name="DocumentAddBean" property="msaName" />&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name="DocumentAddBean" property="appendixName" /></h2></td></tr> 
				      </table>
					</logic:notEqual>
			</logic:equal>
			<logic:notEqual name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
			<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>	
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<%-- <tr>
							        <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )){%>
							        	<%if(appendixStatus.equals( "Cancelled" )){%>
							        		<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
							        	<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
							        	<%}%>
									<%}else{%>	
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
										<%}%>	
					
									<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
									<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
									<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
						        </tr> --%>
						       	<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>	 
					        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
							         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="DocumentAddBean" property ="msaId"/>&firstCall=true"><bean:write name  ="DocumentAddBean" property ="msaName"/></a></div>
							    </td>
			 				</tr>
			 			<tr>
								<td colspan="6">
									<h2>Document 
									<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
												 Modify: <bean:write name = "DocumentAddBean" property = "appendixName" />
									</c:if>
									<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
													Move 
										</c:if>
									<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
										<c:if test="${param.delete != 'DeleteNow'}">
												 View: <bean:write name = "DocumentAddBean" property = "appendixName" />
									</c:if>
								<c:if test="${param.delete == 'DeleteNow'}">
												 Delete
									</c:if>
								</c:if></h2>
						</td>
						</tr>
					</table>
			</logic:notEqual>
			</logic:equal>
			
			<logic:equal name = "DocumentAddBean" property = "type" value = "Addendum">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
							<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
							<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
							
							<td id = "pop3" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td>
					</tr>
					<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb">&nbsp;</a></div>
					    </td>
					</tr>
					<tr><td colspan="6"><h2>Document <c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
															 Modify: <bean:write name = "DocumentAddBean" property = "jobName" />
															 
												</c:if>
												<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
																Move 
													</c:if>
												<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
													<c:if test="${param.delete != 'DeleteNow'}">
																 View: <bean:write name = "DocumentAddBean" property = "jobName" />
													</c:if>
													<c:if test="${param.delete == 'DeleteNow'}">
																 Delete
													</c:if>
												</c:if></h2></td></tr>
				</table>
			</logic:equal>
		<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
			<c:if test="${requestScope.jobType ne 'Default'}">
				<logic:equal name = "DocumentAddBean" property = "type" value = "Job">
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
								<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
								<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
								
								<td id = "pop3" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td>
						</tr>
						<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
						         <div id="breadCrumb">&nbsp;</a></div>
						    </td>
						</tr>
						<tr><td colspan="6"><h2>Document <c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
																 Modify: <bean:write name = "DocumentAddBean" property = "jobName" />
													</c:if>
													<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
																	Move 
														</c:if>
													<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
														<c:if test="${param.delete != 'DeleteNow'}">
																	 View: <bean:write name = "DocumentAddBean" property = "jobName" />
														</c:if>
														<c:if test="${param.delete == 'DeleteNow'}">
																	 Delete
														</c:if>
													</c:if></h2></td></tr>
					</table>
				</logic:equal>
			</c:if>
		</c:if>
			
		
			<logic:equal name = "DocumentAddBean" property = "type" value = "PO">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr>
					<td background="images/content_head_04.jpg" height="21" width="100%">
						<div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
							<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
										 Modify: <bean:write name = "DocumentAddBean" property = "jobName" />
							</c:if>
							<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
								<c:if test="${param.delete != 'DeleteNow'}">
											 <a href="#">View</a>
								</c:if>
								<c:if test="${param.delete == 'DeleteNow'}">
											 <a href="#">Delete</a>
								</c:if>
							</c:if>
					</td>
					
				</tr>
				<tr><td><h2>Document 
								<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
											 Modify
								</c:if>
								<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
												Move 
									</c:if>
								<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
									<c:if test="${param.delete != 'DeleteNow'}">
												 View
									</c:if>
									<c:if test="${param.delete == 'DeleteNow'}">
												 Delete
									</c:if>
								</c:if>
				</h2></td></tr>
				</table>
			</logic:equal>
			<logic:equal name = "DocumentAddBean" property = "type" value = "WO">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr>
					<td background="images/content_head_04.jpg" height="21" width="100%">
						<div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
							<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
										 <a href="#">Modify</a>
							</c:if>
							<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
								<c:if test="${param.delete != 'DeleteNow'}">
											 View: <bean:write name = "DocumentAddBean" property = "jobName" />
								</c:if>
								<c:if test="${param.delete == 'DeleteNow'}">
											 <a href="#">Delete</a>
								</c:if>
							</c:if>
					</td>
					
				</tr>
				<tr><td><h2>Document 
								<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
											 Modify
								</c:if>
								<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
												Move 
									</c:if>
								<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
									<c:if test="${param.delete != 'DeleteNow'}">
												 View
									</c:if>
									<c:if test="${param.delete == 'DeleteNow'}">
												 Delete
									</c:if>
								</c:if>
				</h2></td></tr>
				</table>
			</logic:equal>
			
			
			
			
			
			
			
			
			<logic:equal name = "DocumentAddBean" property = "type" value = "MSA">
				<%@ include  file="/MSAMenuScript.inc" %>
			</logic:equal>
			
			<logic:equal name = "DocumentAddBean" property = "type" value = "Appendix">
				<logic:equal name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
					<logic:equal name = "DocumentAddBean" property = "appendixType" value = "3">
						<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
					</logic:equal>
					<logic:notEqual name = "DocumentAddBean" property = "appendixType" value = "3">
						<%@ include  file="/AppedixDashboardMenuScript.inc" %>
					</logic:notEqual>
				</logic:equal>
				<logic:notEqual name = "DocumentAddBean" property = "appendixStatusChecking" value = "Inwork">
					<%@ include  file="/AppendixMenuScript.inc" %>
				</logic:notEqual>
			</logic:equal>

			<logic:equal name = "DocumentAddBean" property = "type" value = "Addendum">
			<%@ include  file="/AddendumMenuScript.inc" %>
			</logic:equal>	
			<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
				<c:if test="${requestScope.jobType ne 'Default'}">
					<logic:equal name = "DocumentAddBean" property = "type" value = "Job">
						<%@ include  file="/ProjectJobMenuScript.inc" %>
					</logic:equal>
				</c:if>
				</c:if>
			</c:if>
		<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
			<c:if test="${DocumentAddBean.job_Name eq 'Default Job' && requestScope.SourceType eq 'Ilex' && param.abc ne 'y'}">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
						<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
						<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
						<!-- <td id = "pop3"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center>Comments</center></a></td> -->
						<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td>
				</tr>
					<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					
	         			<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
							 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
							 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>&jobtype=<c:out value='${requestScope.jobType }'/>&job_status=<c:out value='${requestScope.jobStatus }'/>"><c:out value='${requestScope.jobName }'/></a>
							  <a><span class="breadCrumb1">Document View</a>
	    				</td>
					</tr>
				<tr><td colspan="7"><h2>Document 
								<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
											 Modify
								</c:if>
								<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
												Move 
									</c:if>
								<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
									<c:if test="${param.delete != 'DeleteNow'}">
												 View
									</c:if>
									<c:if test="${param.delete == 'DeleteNow'}">
												 Delete
									</c:if>
								</c:if>
				</h2></td></tr>
				</table>
			</c:if>
		</c:if>
		<c:if test="${requestScope.SourceType ne 'Ilex' || param.abc eq 'y'}">
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td class="headerrow" height="19">&nbsp;</td></tr>
			<tr>
				<td background="images/content_head_04.jpg" height="21" width="100%">
					<div id="breadCrumb"><a href="#" class="bgNone">Documents</a>
						<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
									 <a href="#">Modify</a>
						</c:if>
						<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
											 <a href="#">Modify</a>
								</c:if>
						<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
							<c:if test="${param.delete != 'DeleteNow'}">
										 <a href="#">View</a>
							</c:if>
							<c:if test="${param.delete == 'DeleteNow'}">
										 <a href="#">Delete</a>
							</c:if>
						</c:if>
				</td>
				
			</tr>
			<tr><td><h2>Document 
							<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
										 Modify
							</c:if>
							<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
											Move 
								</c:if>
							<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
								<c:if test="${param.delete != 'DeleteNow'}">
											 View
								</c:if>
								<c:if test="${param.delete == 'DeleteNow'}">
											 Delete
								</c:if>
							</c:if>
			</h2></td></tr>
			</table>
		</c:if>
	
	<c:if test="${requestScope.appendixStatus eq 'pmAppendix'}">
	 <table border="0" cellspacing="1" cellpadding="0" width="100%">
		<tr><td class="headerrow" height="19" width = "100%" >&nbsp;</td></tr>
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
         			
    				</td>
		</tr>
		<tr><td><h2>Document 
							<c:if test="${DocumentAddBean.hmode eq 'modifyDocument'}">
										 Modify
							</c:if>
							<c:if test="${DocumentAddBean.hmode eq 'moveDocument'}">
											Move 
								</c:if>
							<c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
								<c:if test="${param.delete != 'DeleteNow'}">
											 View
								</c:if>
								<c:if test="${param.delete == 'DeleteNow'}">
											 Delete
								</c:if>
							</c:if>
			</h2></td></tr>
	</table>
	 </c:if>	
<TABLE WIDTH="100%" cellspacing="1" cellpadding="1" border ="0">
	<tr><td>
	<TABLE WIDTH="100%" cellspacing="0" cellpadding="1" border='0'>
				<TR>
				 	<TD >
						<TABLE width="100%"  cellspacing="0" cellpadding="0" border='0'>
						<c:choose>
							<c:when test="${! empty  requestScope.Comment}">
								<TR>
									<TD  class="message" height=5 />
										<c:out value="${requestScope.Comment}"/>
									</TD>
								</TR>
							</c:when>	
							<c:when test="${! empty  requestScope.sourceNotValid}">
								<TR>
									<TD  class="message" height=5 />
										<c:out value="${requestScope.sourceNotValid}"/>
									</TD>
								</TR>
							</c:when>	
						</c:choose>	
							 <TR>
				 				<TD >
									<TABLE width="30" >
									 <TR>
									 <c:if test="${DocumentAddBean.hmode eq 'viewDocument'}">
										 	<TD width="30" class="colDark" nowrap="nowrap"> Library 
										 	</TD>
										 	<TD class="colLight"><c:out value="${requestScope.libName}"/></TD>	
									 </c:if>
									 <c:if test="${DocumentAddBean.hmode eq 'modifyDocument' || DocumentAddBean.hmode eq 'moveDocument'}">
							 			 	<TD width="30" class="colDark" nowrap="nowrap">
							 			 	Library 
								 			</TD>
								 			<bean:define id="libNameCombo" name = "libNameCombo" scope = "request" />
										 	<TD >
										 		<html:select  styleClass="colLight" onchange="onChangeLibrary();" name="DocumentAddBean" property="libName"  size="1"	 disabled="true">
													<html:optionsCollection name="libNameCombo"    property = "librarylist"  label = "label"  value = "value" />
												</html:select>
											</TD>
											<td colspan="60"></td>
											<c:if test="${requestScope.SourceType ne 'Ilex'}">
												<TD>
													<input type="button" title="Click to move document to other Library" name="button2" Class="button_c" onclick="onMoveDocument();" value="Change Library" />
												</TD>
											</c:if>
							 		</c:if>
							  </TR>
						</TABLE>
				 	</TD>
				 </TR>
				
				<TR>
					<TD align='left' valign="TOP" >		
						<TABLE cellspacing=0 cellpadding=0 border="0">
							<TR>
								<c:forEach items="${requestScope.taxonomy_group}" var="taxonomy_group" varStatus="index" >
									<td width='10%' id="TOP_TAB" class='leftRightBorder'>
										<table cellpadding="0" cellspacing="0" width='100%'>
											<tr>
												<td id="TOP_BUTTON" style="cursor:hand;"
													onClick="fun_HideShowBlock('<c:out value="${index.index}"/>');" align="center" width="2px"
													nowrap><img src="docm/images/left-cor.gif" border="0"></td>
												<td id="TOP_BUTTON" style="cursor:hand;"
													onClick="fun_HideShowBlock('<c:out value="${index.index}"/>');" align="center" width="80%" class='drop'
													nowrap bgcolor="#B5C8D9"><c:out value="${taxonomy_group[1]}"/></td>
												<td id="TOP_BUTTON" style="cursor:hand;"
													onClick="fun_HideShowBlock('<c:out value="${index.index}"/>');" align="center" width="2px"
													nowrap><img src="docm/images/right-cor.gif" border="0"></td>
											</tr>
										</table>
									</td>
								</c:forEach>
							</TR>
						</TABLE>
				 	</TD>
				 </TR>
				<TR>
					<TD valign="TOP" >	
					<table  width='100%' 
						style="border-color: #D9DFEF;border-width: 4px;border-style: solid;"
					 cellspacing=0 cellpadding=0 border="0"   width='100%'  valign="top" ><tr><td height='200' width='100%'  valign="top" >
							<TABLE WIDTH="100%"  cellspacing=0 cellpadding=0 border="0"  valign="top">
							
							<c:forEach items="${requestScope.taxonomy_group}" var="taxonomy_group" varStatus="index" >
							
							<c:set var = "checkReadable" value = ""/>
							<c:if test = "${taxonomy_group[0] eq '4' && requestScope.SourceType ne 'DocM'}">
								<c:set var = "checkReadable" value = "readonly='readonly'"/>
							</c:if>
							
								<tr id="tab" style="display: none;" valign="top">
									<td colspan=100>
										<table width="100%"  >
										
										<c:set var="idx" value="0"/>
											
										<c:forEach items="${requestScope.metadata_field_list}" var="metadata_field_list" varStatus="status"> 
											<c:out value="${fields.name}"/>
											<c:if test="${metadata_field_list[9] eq 'Y'}">
												<c:if test="${taxonomy_group[0] eq metadata_field_list[2]}">
								
													<c:forEach items="${metadata_field_list[8]}" var="metadata_field_type" varStatus="status"> 
													<c:if test="${(idx%'2')=='0'}"><tr></c:if>
														
															<c:forEach items="${requestScope.document.metaData.fields}" var="fields" varStatus="status">
																<c:if test="${metadata_field_list[9] eq 'Y'}">
																	<c:if test="${fields.name eq metadata_field_list[3]}">
																	<c:set var="idx" value="${idx+1}"/>
																		<td class='Ntextoleftalignnowrap' width='170' nowrap>
																			<c:out value="${metadata_field_list[4]}"/>
																			<c:if test="${metadata_field_list[12] == 'Y' && DocumentAddBean.hmode ne 'viewDocument'}">
																				<font class="redbold">*</font>
																			</c:if>
																		</td>
																		<c:if test="${DocumentAddBean.hmode eq 'viewDocument' && metadata_field_list[3] ne 'LinkToOtherDocId'}">
																			<td class='Ntexteleftalignnowrap' >
																				<c:if test="${(fields.value eq '1' && metadata_field_type[0] eq 'checkbox')}">Yes</c:if>
																				<c:if test="${(fields.value eq '0' && metadata_field_type[0] eq 'checkbox')}">No</c:if>
																				<c:if test="${(metadata_field_type[0] ne 'checkbox')}"><c:out value="${fields.value}"/></c:if>
																			</td>
																		</c:if>
																		<c:if test="${DocumentAddBean.hmode eq 'viewDocument' && metadata_field_list[3] eq 'LinkToOtherDocId'}">
																			<td class='Ntexteleftalignnowrap' >
																				<c:if test="${metadata_field_list[3] ne ''}"><a href="#" id="LinkDocName" ></a></c:if>
																				<input type="hidden" name="<c:out value="${metadata_field_list[3]}"/>" value="<c:out value="${fields.value}"/>"/>
																			</td>
																		</c:if>
																		
																		<c:if test="${DocumentAddBean.hmode == 'modifyDocument'  || DocumentAddBean.hmode eq 'moveDocument'}">
																			<td valign="top" class='Ntexteleftalignnowrap'>
																			<c:if test="${not empty fields.value}">
																				<c:set var="fieldValue" value="${fields.value}"/>
																			</c:if>
																			<c:if test="${ empty fields.value}">
																				<c:set var="fieldValue" value="${metadata_field_list[5]}"/>
																			</c:if>
																			<c:choose>																					
																				
																					<c:when test="${metadata_field_list[9] eq 'Y'}">
																						<c:if test="${metadata_field_type[0] eq 'textbox' && metadata_field_list[3] ne 'LinkToOtherDocId' && metadata_field_type[2] ne 'max'}">
																							<input type="text"  name="<c:out value="${metadata_field_list[3]}"/>" class='text' onchange="callChange();" 
																							<c:if test="${metadata_field_type[1] == 'numeric'}">onkeypress="return currencyNumericOnly(this.event);"</c:if> 
																							id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>"  
																							<c:if test="${metadata_field_list[11] eq 'Y' || metadata_field_list[3] == 'LinkToOtherDocId'}">readonly="readonly"</c:if> <c:if test="${metadata_field_type[1] ne ''}">maxlength="<c:out value="${metadata_field_type[2]}"/>"</c:if> 
																							<c:out value = "${checkReadable}"/> value="<c:out value="${fieldValue}"/>" />
																						</c:if>
																						
																						 <c:if test="${metadata_field_list[3] == 'LinkToOtherDocId'}">	
																						 	<input type="hidden" name="<c:out value="${metadata_field_list[3]}"/>" value="<c:out value="${fieldValue}"/>"/>
																								<table width="100%">
																									<tr >
																										<td align="left" class='Ntexteleftalignnowrap' width="85%" >
																											<a href="#" id="LinkDocName" onclick="callOpenLinkedDetail();"></a>&nbsp;
																										</td>
																										<td  align="right" class='Ntexteleftalignnowrap' width="15%">	
												  															<input type="button" name="LinkToDoc"  onclick="openSearchPage();"  title="Click to Link with other Document" value="..." />&nbsp;&nbsp;
												  														</td>
												  													</tr>
									  															</table>
																						</c:if>
																						
																						<c:if test="${metadata_field_type[0] eq 'textbox' && metadata_field_type[2] eq 'max'}">
																							<TEXTAREA name="<c:out value="${metadata_field_list[3]}"/>" onchange="callChange();" 
																							<c:if test="${metadata_field_type[1] == 'numeric'}">onkeypress="return currencyNumericOnly(this.event);"</c:if> 
																							id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>"  
																							<c:if test="${metadata_field_list[11] eq 'Y'}">readonly="readonly"</c:if>  
																							 cols="25" rows="3" > <c:out value = "${checkReadable}"/> <c:out value="${fieldValue}"/></TEXTAREA>								 		  
																						</c:if>
																						
																						<c:if test="${metadata_field_type[0] eq 'checkbox'}">
																							<input type="radio" <c:if test="${empty metadata_field_list[12] && metadata_field_list[3] == 'ViewableOnWeb'}">checked</c:if> <c:if test="${metadata_field_list[3] == 'ChangeControlType' && (param.libId != '9')}">disabled</c:if> <c:out value = "${checkReadable}"/> onchange="callChange();"  name ="<c:out value="${metadata_field_list[3]}"/>" 
																							id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>" 
																							value="1"  <c:if test="${fields.value == '1'}">checked</c:if>/><span style="font-size: 11px;">Yes</span>
																							<input type="radio" <c:if test="${metadata_field_list[3] == 'ChangeControlType' && (param.libId ne '9')}">disabled</c:if> onchange="callChange();"  <c:out value = "${checkReadable}"/> name ="<c:out value="${metadata_field_list[3]}"/>" 
																							id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>" 
																							value="0" <c:if test="${fields.value == '0'}">checked</c:if>/><span style="font-size: 11px;">No</span>
																							
																							
																							
																						</c:if>
																						
																						<c:if test="${metadata_field_type[0] eq 'datetime'}">
																							<input type="text" class='text' size="10" value="<c:out value="${fieldValue}"/>" onchange="callChange();" <c:out value = "${checkReadable}"/>
																							id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>"  
																							readonly="readonly" name="<c:out value="${metadata_field_list[3]}"/>" class="textbox"><img src = "images/calendar.gif" 
																							width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].<c:out 
																							value="${metadata_field_list[3]}"/>, document.forms[0].<c:out value="${metadata_field_list[3]}"/>, 'mm/dd/yyyy')" 
																							onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img></td>
																						</c:if>
																						
																						<c:if test="${metadata_field_type[0] eq 'combo'}">
																							<select  id="docStatus" name= "<c:out value="${metadata_field_list[3]}"/>"  onchange="callChange();" <c:out value = "${checkReadable}"/>
																							id="<c:if test="${metadata_field_list[12] == 'Y'}">tab<c:out value="${index.index}"/>_notempty_<c:out value="${idx}"/></c:if>"  
																							class = "combowhite" >
																								<c:forEach items="${metadata_field_list[13]}" var="combo" varStatus="comboStatus">
																									<option value="<c:out value="${combo[1]}"/>" 
																									<c:if test="${fieldValue eq combo[1]}">selected="selected"</c:if>> 
																									<c:out value="${combo[1]}"/></option>
																								</c:forEach>
																							</select>
																						</c:if>
																					</c:when>
																					
																					<c:otherwise>
																					</c:otherwise>
																			</c:choose>
																			</td>
																		</c:if>
																	</c:if>
																</c:if>
																
															</c:forEach>
													<c:if test="${(idx%'2')=='0'}"></tr></c:if>
												</c:forEach>
											</c:if>
									</c:if>
									<c:if test="${metadata_field_list[9] eq 'N'}">
										<c:if test="${taxonomy_group[0] eq metadata_field_list[2]}">
											<c:forEach items="${requestScope.document.metaData.fields}" var="fields" varStatus="status">
												<c:if test="${fields.name eq metadata_field_list[3]}">
													<input type="hidden"  name="<c:out value="${fields.name}"/>" value="<c:out value="${fields.value}"/>" />
												</c:if>
											</c:forEach>
										</c:if>
									</c:if>
								</c:forEach>
											<input type='hidden' id='tab<c:out value="${index.index}"/>_fieldcount' value='<c:out value="${idx}"/>'/>
										</table>
									</td>
								</TR>
							</c:forEach>
							</table>
							</td>
							</tr>
							</table>
							</td>
							</tr>
							<TR>
								<TD valign="TOP" >	
									<TABLE WIDTH="100%"  cellspacing=1 cellpadding=0 border="0">
										<c:if test="${DocumentAddBean.hmode == 'modifyDocument'  || DocumentAddBean.hmode eq 'moveDocument'}">
											<TR>
											 	<TD style="width: 3cm;" class="colDark" nowrap="nowrap"> Upload New File from</td>
												 	<TD>
												 	<html:file property="myfile" style="width: 12cm;" styleClass="button_c" onchange="setText();" onkeypress="return false;"/>
												</TD>	
											  </TR>
										</c:if>
										<tr>
												<c:if test="${DocumentAddBean.hmode == 'viewDocument' && param.delete != 'DeleteNow' }">
													
												  	<td class = "colLight" align="center" >
												  	<input type="button" name="button1" onclick="<c:if test="${param.abc ne 'y'}">moveToSource('<c:out value="${requestScope.SourceType}"/>');</c:if><c:if test="${param.abc eq 'y'}">window.close();</c:if>" Class="button_c" value="Ok" />
													  	<c:if test="${param.state ne 'marked' && param.state ne 'Deleted' && requestScope.SourceType ne 'Ilex' && param.abc ne 'y'}">
														  	<c:if test="${param.statusType ne 'Deleted' && param.statusType ne 'Superseded' }">
														  		<input type="button" name="button2" onclick="modifyDocument();" Class="button_c" value="Modify" />
																<input type="button" name="button3" onclick="deleteDocument();" Class="button_c" value="Delete" />
															</c:if>
														</c:if>	
													</td>
													<c:if test="${not empty DocumentAddBean.linkedDocument}">
													<td >
														<table>
															<tr><td class = "drop" align="left" >Linked Document</td></tr>
															<c:forEach items="${DocumentAddBean.linkedDocument}" var="linkToDoc" varStatus="index">
																<tr>
																	<td class = "colLight" align="left"><a href="#" onclick='openLinkedDetailPage("DocumentAdd.do?hmode=viewDocument&Source=<c:out value='${requestScope.SourceType}'/>&abc=y&docId=<c:out value="${linkToDoc[1]}"/>&entityId=<c:out value="${param.entityId}"/>&entityType=<c:out value="${param.entityType}"/>&libId=<c:out value="${requestScope.libId}"/>");' ><c:out value="${linkToDoc[0]}"/></a></td>
																</tr>
															</c:forEach>
														</table>
													</td>
													</c:if>
												</c:if>
								
												<c:if test="${DocumentAddBean.hmode == 'modifyDocument'  || DocumentAddBean.hmode eq 'moveDocument'}">
													
													<td class = "colLight" align="center" colspan='2'>
													<input type="button" name="button2" onclick="saveData();" class="button_c" 
													value="Upload/Save"/>
													<input type="button" name="button2" class="button_c" value="Cancel" onclick="resetPage();"/>
													</td>
												</c:if>
												<td class = "colLight" align="center" colspan='2'>
												<c:if test="${param.delete == 'DeleteNow' &&  empty  requestScope.sourceNotValid}">
													<input type="button" name="button3" onclick="deleteNow();"  class="button_c" value="Delete Now" />
												</c:if>
												<c:if test="${param.delete == 'DeleteNow' &&  !empty  requestScope.sourceNotValid}">
												  	<input type="button" name="button1" onclick="moveToSource('<c:out value="${requestScope.SourceType}"/>');" Class="button_c" value="Ok" />
												</c:if>
													</td>
												
											</tr>
									</TABLE>
								</TD>
							</TR>
					</TABLE>
</td></tr></TABLE>

	<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">	
		<c:if test = "${requestScope.jobType eq 'Default'}">
			<%@ include  file="/DefaultJobMenuScript.inc" %>
		</c:if>
	</c:if>
	

			</html:form>
		</BODY>

</HTML>