/*********************************************************
      Menu Functions
 *********************************************************/
 
 
 function displaynewcombo ( elem , i )
{
	if( i!= null )      //for modifying existing entries
	{
		if( i%2 == 0 )
		{
	  		document.getElementById ( elem.name + 'combo' ).className = 'comboevenvisible';
	  	
	  	}
	  	else
	  		document.getElementById ( elem.name + 'combo' ).className = 'combooddvisible';
	  
	   	document.getElementById ( elem.name + 'combo' ).focus();
 	}
 	return;
}



function displaycombo ( elem , i )
{
	if( i != null )      //for modifying existing entries
	{
		if( i%2 == 0 )
	  		document.getElementById ( elem.name + 'combo' + i ).className = 'combooddvisible';
	  	else
	  		document.getElementById ( elem.name + 'combo' + i ).className = 'comboevenvisible';
	  
	   	document.getElementById ( elem.name + 'combo' + i ).focus();
 	}
 	
 	return;
}
 
 
 
 
 
/*function displaycombo (elem,i)
{
	if( i!= null)      //for modifying existing entries
	{
		if(i%2 == 0)
	  		document.getElementById ( elem.name + 'combo' + i ).className = 'combooddvisible';
	  	else
	  		document.getElementById ( elem.name + 'combo' + i ).className = 'comboevenvisible';
	  
	   	document.getElementById ( elem.name + 'combo' + i ).focus();
 	}
 	
 	else              //for a new entry
 	{
 		document.getElementById ( elem.id + 'combo' ).className = 'combooddvisible';
 		document.getElementById( elem.id + 'combo' ).focus();
 	}
 	return;
}*/


function highlightTextFieldright ( elem )
{
	document.getElementById( elem.id ).className = 'textonmouseoverright';
 	return;
}

function unHighlightTextFieldright ( elem )
{
    document.getElementById ( elem.id ).className = 'textboxnumber';
	return;
}



function highlightTextField ( elem )
{
	document.getElementById( elem.id ).className = 'textonmouseover';
 	return;
}


function unHighlightTextField ( elem )
{
    document.getElementById ( elem.id ).className = 'textbox';
	return;
}




function hidecombo (elem)
{
	document.getElementById(elem.id).className = 'combooddhidden';
 	return;
}


function updatetext (elem , i)
{
	var textid = elem.id;
	var comboid = document.getElementById ( elem.id );
	
	if ( i!= null)
	{	
		textid = textid.substring ( 0,textid.indexOf ( 'combo' ) ) + textid.substring ( textid.indexOf ( 'combo' ) + 5 , textid.length );
		document.getElementById ( textid ).value = comboid.options[comboid.selectedIndex].text;
		comboid.className = 'combooddhidden';
	}
	else
	{
		textid = textid.substring (0, textid.indexOf ( 'combo' ) );
		document.getElementById ( textid ).value = comboid.options[comboid.selectedIndex].text;
		comboid.className = 'combooddhidden';
	}
	return;
}


function MM_reloadPage( init ) {  //reloads the window if Nav4 resized
  if (init == true) with ( navigator ) {if ((appName == "Netscape")&&(parseInt(appVersion) == 4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=  MM_reloadPage; }}
  else if (innerWidth!= document.MM_pgW || innerHeight!= document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}



function round_func( original_number )
{
	return round_decimals( original_number , 2 );
}


function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return pad_with_zeros(result3, decimals)
}

function pad_with_zeros(rounded_value , decimal_places) {

    // Convert the number to a string
    var value_string = rounded_value.toString()
    
    // Locate the decimal point
    var decimal_location = value_string.indexOf(".")

    // Is there a decimal point?
    if (decimal_location == -1) {
        
        // If no, then all decimal places will be padded with 0s
        decimal_part_length = 0
        
        // If decimal_places is greater than zero, tack on a decimal point
        value_string += decimal_places > 0 ? "." : ""
    }
    else {

        // If yes, then only the extra decimal places will be padded with 0s
        decimal_part_length = value_string.length - decimal_location - 1
    }
    
    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length
    
    if (pad_total > 0) {
        
        // Pad the string with 0s
        for (var counter = 1; counter <= pad_total; counter++) 
            value_string += "0"
        }
    return value_string
}



function findPosX(p)
{

	var obj = document.getElementById(p);

	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft+2;
}

function findPosY(p)
{
	var obj = document.getElementById(p);
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop+19;
}


function removeComma(val)
{
	val=val.split(",").join( "" );   // remove existing commas if present.
	
	return val;
}


function putComma( val )
{

	var dot=val.indexOf( "." );      // locate decmal
	if(dot<0)dot=val.length;       // use end if no decimal
	
	var r="";
	
	for(pos=dot-3;pos>=1;pos-=3)   // put commas in
	r=","+val.substr(pos,3)+r;
	r=val.substring(0,pos+3)+r;    // put start of string on
	dot=val.indexOf(".");          // check for decimal
	if(dot>0)r+=val.substring(dot);// put fraction part on
	
	return r;
}








