<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Transfer Document</title>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19" colspan="2">&nbsp;</td></tr>
					<tr><td background="images/content_head_04.jpg" height="21" width="100%" colspan="2">&nbsp;</td></tr>
					<c:if test="${not empty requestScope.inProgress}">
						<tr>
							<td class="message" colspan="2"><c:out value="${requestScope.inProgress}"/></td>
						</tr>
					</c:if>
					<tr>
	 					<td><h2> Initiate Transfer </h2></td>
	 					<td><a href = "TransferDocument.do?Type=all">Synchronize All</a></td>
	 				</tr>
</table>	
		<UL>Contract Documents
			<LI><a href = "TransferDocument.do?Type=MSA">MSA - Contract</a>
			<LI><a href = "TransferDocument.do?Type=Appendix">Appendix - Contract</a>
			<LI><a href = "TransferDocument.do?Type=Addendum">Addendum - Contract</a>
			<LI><a href = "TransferDocument.do?Type=PO">PO</a>
			<LI><a href = "TransferDocument.do?Type=WO">WO</a>
		</UL>
		
		<UL>Supporting Documents
			<LI><a href = "TransferDocument.do?Type=support">Documents</a>
		</UL>
</body>
</html>