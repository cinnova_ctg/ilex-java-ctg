<SCRIPT language=JavaScript1.2>
    if (isMenu) {
    arMenu1 = new Array(
            120,
            findPosX('pop1'), findPosY('pop1'),
            "", "",
            "", "",
            "", "",
            "Add Job", "JobEditAction.do?ref=inscopeview&appendix_Id=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&Job_Id=0", 0,
            "Bulk Job", "#", 3,
            "Contract Documents", "", 1,
            "Custom Fields", "AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>", 0,
            "Customer Reference", "AddCustRef.do?typeid=<%= appendixid %>&type=Appendix&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>", 0,
            "External Sales Agent", "ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&fromPage=Appendix", 0,
            "Documents", "", 1,
            "Job Sites", "SiteManagement.do?function=view&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&initialCheck=true&pageType=jobsites", 0,
            "Schedule", "#", 1,
            "Comments", "#", 1,
            "<bean:message bundle = "AM" key = "am.summary.Partnerinfo"/>", "#", 1,
            "Team", "AssignTeam.do?function=View&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&fromPage=Appendix", 0,
            "Online Bid", "OnlineBid.do?appendixId=<%= appendixid %>&fromPage=Appendix", 0,
            "MPO", "MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=appendix", 0,
            "Job Notes", "JobNotesAction.do?appendixid=<%= appendixid %>", 0,
            "Workflow Checklist Items", "ProjectLevelWorkflowAction.do?hmode=unspecified&appendixid=<%= appendixid %>", 0
            )

<% if (contractDocMenu != "") { %>
    <%= contractDocMenu %>
<% } %>

                arMenu1_2 = new Array(
                                    "Standard Add", "javascript:bulkJobCreation();", 0,
                                    "Large Deployment Add", "LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= appendixid %>", 0,
                                    "Change Job Owner/Scheduler", "ChangeJOSAction.do?ref=bukJobCreationView&appendixid=<%= appendixid %>", 0
                                    )

                            arMenu1_7 = new Array(
                                    "<bean:message bundle = "PRM" key = "prm.viewalldocuments"/>", "javascript:viewdocuments();", 0,
                                    "<bean:message bundle = "PRM" key = "prm.uploaddocuments"/>", "javascript:upload();", 0
                                    )

                            arMenu1_7 = new Array(
                                    "Appendix History", "EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=Appendix", 0,
                                    "Support Documents", "", 1
                                    )

                            arMenu1_7_2 = new Array(
                                    "Upload", "EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&fromPage=Appendix&linkLibName=Appendix&entityType=Appendix Supporting Documents&function=addSupplement", 0,
                                    "History", "EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&fromPage=Appendix&linkLibName=Appendix&function=supplementHistory", 0
                                    )


                            arMenu1_9 = new Array(
                                    "View", "CostSchedule.do?appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>", 0,
<% if (appendixcurrentstatus . equals("F")) { %>
                "Update", "javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotupdateprojectschedule"/>')", 0
<% } else { %>
                "Update", "ScheduleUpdate.do?function=update&typeid=<%= appendixid %>&type=P&page=appendixdashboard&pageid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>", 0

<% } %>
                )

                            arMenu1_10 = new Array(
                                    "View", "javascript:viewcomments();", 0,
                                    "Add", "javascript:addcomment();", 0
                                    )

                            arMenu1_11 = new Array(
                                    "<bean:message bundle = "AM" key="am.Partnerinfo.sow"/>", "PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>", 0,
                                    "<bean:message bundle = "AM" key="am.Partnerinfo.assumptions"/>", "PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>", 0,
                                    "<bean:message bundle = "AM" key="am.Partnerinfo.Inst_cond"/>", "PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>", 0
                                    )

                            arMenu2 = new Array(
                                    120,
                                    findPosX('pop2'), findPosY('pop2'),
                                    "", "",
                                    "", "",
                                    "", "",
                                    "<bean:message bundle = "PRM" key = "prm.appendix.redlinereport"/>", "javascript:openreportschedulewindow('redline')", 0,
                                    "<bean:message bundle = "PRM" key = "prm.appendix.detailedsummary"/>", "javascript:openreportschedulewindow('detailsummary')", 0,
                                    "<bean:message bundle = "PRM" key = "prm.appendix.csvsummary"/>", "/content/reports/csv_summary/", 0,
                                    "Status", "#", 1,
                                    "MSP C/C Report", "MSPCCReportAction.do?function=View&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&fromPage=Appendix", 0
                                    )

                            arMenu2_4 = new Array(
                                    "<bean:message bundle = "PRM" key = "prm.appendix.view1"/>", "javascript:managestatus( 'view' )", 0,
                                    "<bean:message bundle = "PRM" key = "prm.appendix.update"/>", "javascript:managestatus( 'update' )", 0,
                                    "<bean:message bundle = "PRM" key = "prm.appendix.send"/>", "javascript:sendstatusreport()", 0
                                    )
                            document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
                    }
</script>
