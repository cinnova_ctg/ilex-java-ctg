<%
String appendixStatus ="";
String latestaddendumid ="";

if(request.getAttribute("appendixId")!=null)
	appendixId= request.getAttribute("appendixId").toString();
	
if(request.getAttribute("appendixStatus")!=null)
	appendixStatus= request.getAttribute("appendixStatus").toString();
	
if(request.getAttribute("latestaddendumid")!=null)
	latestaddendumid= request.getAttribute("latestaddendumid").toString();	
	
if( request.getAttribute( "appendixType" ) != null)
	appendixType = (String) request.getAttribute( "appendixType" );
%>

<script>
var str = '';
function bulkJobCreation()
{
   
	document.forms[0].target = "_self";
	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= appendixId %>&MSA_Id=<%= msaId%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function appendixAddComments()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= msaId%>&Appendix_Id=<%= appendixId %>";
	document.forms[0].submit();
	return true;
}

function appendixViewComments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= appendixId%>";
	document.forms[0].submit();
	return true;	 
}

function appendixEdit()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= appendixId%>";
	document.forms[0].submit();
	return true;	
}

function appendixView( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= appendixId %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function appendixDel() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= msaId%>&Appendix_Id=<%= appendixId%>";
			document.forms[0].submit();
			return true;	
		}
}

function appendixSendEmail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= appendixId %>&MSA_Id=<%= msaId%>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function appendixUpload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= appendixId %>";
	document.forms[0].submit();
	return true;
}

function appendixViewDocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= appendixId %>";
	document.forms[0].submit();
	return true;
}
function appendixFormalDocDate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=msaId%>&Appendix_Id=<%=appendixId%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=appendixId%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=appendixId%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
</script>