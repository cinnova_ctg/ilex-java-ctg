<%

String jobStatus ="";
String job_type ="";
String appendixtype ="";
String Job_Id ="";
String chkadd ="";
String chkaddendum ="";
String chkaddendumactivity ="";
String addendum_id ="";
String from = "PM";

if(request.getAttribute("jobStatus")!=null)
	jobStatus= request.getAttribute("jobStatus").toString();
	
if(request.getAttribute("job_type")!=null)
	job_type= request.getAttribute("job_type").toString();
	
if(request.getAttribute("appendixtype")!=null)
	appendixtype= request.getAttribute("appendixtype").toString();	

if(request.getAttribute("Job_Id")!=null)
	Job_Id= request.getAttribute("Job_Id").toString();		
	
if(request.getAttribute("chkadd")!=null)
	chkadd= request.getAttribute("chkadd").toString();
	
if(request.getAttribute("chkaddendum")!=null)
	chkaddendum= request.getAttribute("chkaddendum").toString();	
	
if(request.getAttribute("chkaddendumactivity")!=null)
	chkaddendumactivity= request.getAttribute("chkaddendumactivity").toString();	
	
if(request.getAttribute("addendum_id")!=null)
	addendum_id= request.getAttribute("addendum_id").toString();	
	
	
%>

<script>
var str = '';

function jobAddComment()
{
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Job&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>';
	document.forms[0].submit();
	return true;	 
	 
}

function jobViewComments()
{
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Job&Id=<%= Job_Id %>';
	document.forms[0].submit();
	return true;	 
}

function jobDel() 
{
		<%if(job_type.equals("Default")|| jobStatus.equals("Approved"))
			{
				if(job_type.equals("Default")){%>
						alert( "<bean:message bundle = "pm" key = "job.detail.defaultjob"/>" );
						return false;
				<%}
				if(jobStatus.equals("Approved")){%>
						alert( "<bean:message bundle = "pm" key = "job.detail.approvedjob"/>" );
						return false;
				<%}%>	
		<%}else{%>
			
			var convel = confirm( "<bean:message bundle = "pm" key = "job.detail.delete.confirm"/>" );
			if( convel )
			{
				document.forms[0].action ="DeleteAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Job&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id%>";
				document.forms[0].submit();
				return true;	
			}	
		<%}%>
				
}

function jobUpload()
{
	<%if(job_type.equals("Default")){%>	
		document.forms[0].action = "EntityHistory.do?entityId=<%= Job_Id %>&jobId=<%= Job_Id %>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Job";
	<%}else{%>
		document.forms[0].action = "EntityHistory.do?entityId=<%= Job_Id %>&jobId=<%= Job_Id %>&entityType=Deliverables&function=addSupplement&linkLibName=Job";
	<%}%>
	document.forms[0].submit();
	return true;
}

function jobViewDocuments()
{
	<%if(job_type.equals("Default")){%>	
		document.forms[0].action = "EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%=Appendix_Id%>&linkLibName=Job&function=supplementHistory&jobId=<%= Job_Id %>";
	<%}else{%>
		document.forms[0].action = "EntityHistory.do?entityType=Deliverables&appendixId=<%=Appendix_Id%>&linkLibName=Job&function=supplementHistory&jobId=<%= Job_Id %>";
	<%}%>
	document.forms[0].submit();
	return true;
}


</script>