<!DOCTYPE HTML>

<%@page import="com.mind.util.WebUtil"%>
<%@ page import="org.apache.http.HttpRequest"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.security.MessageDigest"%>
<%@ page import="java.math.BigInteger"%>
<%@ page import="java.io.BufferedReader"%>
<%@ page import="java.io.InputStreamReader"%>
<%@ page import="java.io.FileReader"%>

<%
    int size = 0;
    if (session.getAttribute("size") != null) {
        size = Integer.parseInt(session.getAttribute("size").toString());
    }
%>

<html:html>
    <HEAD>
        <script type="text/javascript" src="css/js/jquery.js"></script>
        <script type="text/javascript" src="css/js/hoverIntent.js"></script>
        <script type="text/javascript" src="css/js/superfish.js"></script>

        <%!
            public String getMD5Hash(String val) throws Exception {
                byte[] bytes = val.getBytes();

                MessageDigest m = MessageDigest.getInstance("MD5");
                byte[] digest = m.digest(bytes);
                String hash = new BigInteger(1, digest).toString(16);
                // System.out.println(hash.length());
                if (hash.length() < 32) {
                    for (int i = hash.length(); i < 32; i++) {
                        hash = "0" + hash;
                    }
                }
                return hash;
            }
        %>
        
        <script>
            var ilex_url = document.URL;
            var medius_url = '<%=session.getAttribute("medius_url")%>';
            var menu_key = '<%=getMD5Hash(session.getAttribute("useremail").toString())%>';
            var userid = '<%=session.getAttribute("userid").toString()%>';
            var token = '<%=session.getAttribute("token").toString()%>';

            var jQ_ = jQuery.noConflict(true);
            (function ($) { //create closure so we can safely use $ as alias for jQuery
                jQ_(document).ready(function () {
                    // initialise plugin
                    var example = jQ_('#example').superfish({
                        //add options here if required
                    });
                });
            })(jQuery);
        </script>

        <script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
        <script>
            WebFont.load({
                custom: {
                    families: ['CalibriRegular'],
                    urls: ['css/ilex_top_menu.css']
                }
            });
        </script>
        <link rel="stylesheet" type="text/css" href="css/ilex_top_menu.css" />
        <link rel="stylesheet" type="text/css" href="css/top_menu.css" />
        <link rel="stylesheet" type="text/css" href="css/adminmain.css" />
        <link rel="stylesheet" type="text/css" href="css/css.css" />
        <link rel="stylesheet" type="text/css" href="css/medius_header.css" />

        <%@ include file="/Header.inc"%>

        <META name="GENERATOR" content="IBM WebSphere Studio">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META http-equiv="Content-Style-Type" content="text/css">
        <LINK href="styles/style.css" rel="stylesheet" type="text/css">
        <title>Untitled Document</title>
        <script type="text/javascript" src="javascript/JQueryMain.js"></script>
        <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
        <script language="JavaScript">

            $(document).ready(function () {
                if (window.location.href.indexOf("tableMainIndex.html") > -1) {
                    $("#submittername").text("network operations");
                }
                
                if (window.location.href.indexOf("=CM/Welcome.jsp") > -1) {
                    $("#submittername").text("recruiting");
                }
                
                if (window.location.href.indexOf("?param=Reporting/Welcome.jsp?firsttime=true") > -1) {
                    $("#submittername").text("reporting");
                }
                
                $.ajax({
                    type: "POST",
                    url: medius_url + "/top_menu_for_ilex.php?emp_id=" + menu_key,
                    dataType: "json",
                    success: function (res) {
                        if (typeof res !== 'undefined' && res !== null && res.status === 'success') {
                            $('#ilex_top_menu').html(getIlexMenu(res.data));
                        }
                    }
                });
            });
            
            function getIlexMenu(menu_data) {
                var output = "<ul class=\"sf-menu sf-js-enabled sf-arrows\">";

                for (var i = 0; i < menu_data.length; i++) {
                    var menu_link = getMenuURL(menu_data[i]);

                    if (menu_link !== "") {
                        output += "<li>";
                        output += menu_link;
                        
                        if (menu_data[i].children.length > 0) {
                            output += getIlexMenu(menu_data[i].children);
                        }
                        
                        output += "</li>";
                    }
                }

                output += "</ul>";
                return output;
            }
            
            function getMenuURL(menu_item) {
                var userIdText = "";
                if (menu_item.url.indexOf("Reporting") > -1) {
                    userIdText = "&userid=" + userid + "&token=" + token;
                } else if (menu_item.url.indexOf("/Ilex-WS/") > -1 || menu_item.url.indexOf("Ilex-Prm") > -1) {
                    userIdText = "?userid=" + userid + "&token=" + token;
                } else if (menu_item.url.indexOf("noc_calendar/") > -1) {
                    userIdText = "?user_id=" + userid;
                }
                
                var menu_link = "";
                if (menu_item.parent_id == "0" && menu_item.text != "Accounting") {
                    menu_link = "<a  href='#'>" + menu_item.text + "</a>";
                } else if (menu_item.url.indexOf("http://ilex.contingent.local/Ilex") > -1) {
                    menu_link = "<a  href='" + menu_item.url.replace("http://ilex.contingent.local/Ilex/", "") + userIdText + "'>" + menu_item.text + "</a>";
                } else if (menu_item.url.indexOf("http://ilex-prod2.contingent.local:8080/Ilex") > -1) {
                    menu_link = "<a  href='" + menu_item.url.replace("http://ilex-prod2.contingent.local:8080/Ilex/", "") + userIdText + "'>" + menu_item.text + "</a>";
                } else if (menu_item.url.indexOf("http://ilex.contingent.local") > -1) {
                    menu_link = "<a target='" + menu_item.target + "' href='" + menu_item.url.replace("http://ilex.contingent.local", "#") + "'>" + menu_item.text + "</a>";
                } else if (menu_item.url.indexOf("http://ilex-prod2.contingent.local:8080") > -1) {
                    menu_link = "<a target='" + menu_item.target + "' href='" + menu_item.url.replace("http://ilex-prod2.contingent.local:8080", "#") + "'>" + menu_item.text + "</a>";
                } else {
                    menu_link = "<a target='" + menu_item.target + "' href='" + menu_item.url + userIdText + "'>" + menu_item.text + "</a>";
                }
                
                return menu_link;
            }
        </script>
    </head>
    <body text="#000000" bgcolor="#f0f0f0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableIId">
            <tr style="background-color: white;">
                <td><img src="images/medius_logo.jpg" alt="" align="left" /></td>
                <td align="right" class="top_right_header" onclick="getSplashScreen();" style="cursor: pointer;">
                    <span id="submittername">ilex</span> <span class="top_right_header_sep">MEDIUS </span>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="ilex-menu" style="float: none">
                    <div id="header">
                        <div id="ilex_top_menu" style="float: left;"></div>
                        <ul class="main-nav main-nav-right">
                            <li style="font-family: verdana; font-size: 10px; padding-right: 5px; padding-top: 6px; white-space: nowrap;">Welcome
                                <b><%=session.getAttribute("username")%></b>
                            </li>
                            <li>
                                <a href="javascript:window.close();">
                                    <img border="0" src="images/logout.gif" alt="Logout" title="Logout" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html:html>
<script>
    function getSplashScreen() {
        // ilex_router.php will read the source url and redirect the user to the correct instance (verification or production).
        top.frames["ilexbottom"].location = "/content/landing_zone";
        $("#submittername").text("Ilex");
    }

    $(".topBar").click(function () {
        $.cookie("top_href", $(this).attr('href'));
    });

    $(document).ready(function () {
        var bottomFrameLocation = "";
        if (${pageContext.request.serverPort} == "80") {
            bottomFrameLocation = "http://${pageContext.request.serverName}${pageContext.request.contextPath}/Welcome.jsp";
        } else {
            bottomFrameLocation = "http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/Welcome.jsp";
        }

        if ("dashport" == parent.optValue) {
            top.frames["ilexbottom"].location = "/content/projects/dashport";
        } else {
            if ($.cookie("top_href") != null) {
                var menuHref = $('a[href=' + $.cookie("top_href") + ']').attr('href');
                top.frames["ilexbottom"].location = menuHref;
                swapImage(menuHref);
                $.cookie("top_href", null);
            } else if (top.frames["ilexbottom"].location == bottomFrameLocation) {
                top.frames["ilexbottom"].location = "/";
            }
        }
    });
</script>