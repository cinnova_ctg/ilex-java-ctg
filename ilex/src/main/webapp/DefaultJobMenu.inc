<script>
function viewdocumentsDefaultJob(jobid) {
	document.forms[0].target = "_self";
	document.forms[0].action = "EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= Appendix_Id %>&linkLibName=Job&function=supplementHistory&jobId="+jobid;
	document.forms[0].submit();
	return true;
}

function uploadDefaultJob(jobid) {
	document.forms[0].target = "_self";
	document.forms[0].action = "EntityHistory.do?entityId="+jobid+"&jobId="+jobid+"&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Job";
	document.forms[0].submit();
	return true;
}

function manageDefaultJobStatus(v) {
	document.forms[0].target = "_self";
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_job&id=<%=Job_Id%>";
	document.forms[0].submit();
	return true;	
}
function sendStatusReportDefaultJob() {
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=prm_job&Type1=prm_job&id=<%=Job_Id%>";
	document.forms[0].submit();
	return true;
}

function viewCommentsDefaultJob() {
	document.forms[0].target = "_self";
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}
function addCommentDefaultJob() {
	document.forms[0].target = "_self";
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Appendix_Id=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}
</script>