<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>



<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>





<html:html>
<HEAD>

<%@ include  file = "/Header.inc" %>
<META name = "GENERATOR" content = "IBM WebSphere Studio">

<META http-equiv = "Content-Style-Type" content = "text/css">
<LINK href = "styles/style.css" rel="stylesheet" type = "text/css">
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/summary.css" rel="stylesheet" type="text/css" />
<script language = "javascript" src = "javascript/JLibrary.js"></script>
<script language = "javascript" src = "javascript/ilexGUI.js"></script>
<%--<script language="JavaScript" src="javascript/popcalendar.js"></script>--%>
<title></title>

<%@ include  file = "/Menu.inc" %>
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
</head>  
<bean:define id = "soption" name = "ChecklistTabForm" property = "selectedoption" type = "java.lang.String" />



<%
	String backgroundclass = null;
	String backgroundclass1 = null;
	String centerclass = null;

	boolean csschooser = true;
	int i = 0;
	String[] option1 = null;
	String[] optionid1 = null;
	String[] partnername = null;
	String[] partnerid = null;
	
	int j = 0;
	int k = 0;
	int totalpartners = 0;
	int totalPartnersName=0;
	int pos = 80;
	int popwin = 0;
	String type="";
	
	
	String sitename = ( request.getAttribute( "sitename" ).toString() );
	
	if( request.getAttribute( "totalpartners" ) != null )
	{
		totalpartners = ( int ) Integer.parseInt( request.getAttribute( "totalpartners" ).toString() );
	}
	//	Changed for avoid the Internal Partner apppear in Process check list.
	if( request.getAttribute( "partnernamelistsize" ) != null )
	{
		totalPartnersName = ( int ) Integer.parseInt( request.getAttribute( "partnernamelistsize" ).toString() );
	}

	String partnernamelist = ( request.getAttribute( "partnernamelist" ).toString() );
	String partneridlist = ( request.getAttribute( "partneridlist" ).toString() );
	if( partnernamelist.length() > 0 )
	{
		partnername = partnernamelist.split( "~" );
	}

	if( partneridlist.length() > 0 )
	{
		partnerid = partneridlist.split( "~" );
	}
	if(request.getAttribute("popwin") != null)
	{	
		popwin = (int)Integer.parseInt(request.getAttribute("popwin").toString());
	}
	if(request.getAttribute("type") != null)
	{	
		type = request.getAttribute("type").toString();
	}

	String module = "";
	if(session.getAttribute("tRModule") != null) {
		module = session.getAttribute("tRModule").toString();
	}

	String netMedXAtt = "";
	if(request.getAttribute("NetMedX") != null) {
		netMedXAtt = request.getAttribute("NetMedX").toString();
	}
	
	String isPO = "";
	if(request.getAttribute("isPO") != null) {
		isPO = request.getAttribute("isPO").toString();
	}
%>

<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "setSelected('<%= soption %>');" >
<!--  change of menu  -->
<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Job</span></a>
  			<ul>
				
					<li>
						<a href="#"><span>Change Status</span></a>
						<ul>
							<li><a href="JobDashboardAction.do?isClicked=5&amp;tabId=5&amp;function=view&amp;appendixid=<c:out value='${requestScope.appendixId }'/>&amp;ticketType=&amp;jobid=<c:out value='${requestScope.Job_Id }'/>">Complete</a></li><li><a href="MenuFunctionChangeStatusAction.do?Type=prj_job&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;Status=H">Hold</a></li><li><a href="MenuFunctionChangeStatusAction.do?Type=prj_job&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;Status=C">Cancelled</a></li>
						</ul>
					</li>
					
		  		<li>
		        	<a href="#"><span>Documents</span></a>
		        	<ul>
		      			<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.Job_Id }'/>&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;entityType=Deliverables&amp;function=addSupplement&amp;linkLibName=Job">Upload</a></li>
		      			<li><a href="EntityHistory.do?entityType=Deliverables&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;linkLibName=Job&amp;function=supplementHistory">History</a></li>
		  			</ul>
		  		</li>
      			<li><a href="ChecklistTab.do?job_id=<c:out value='${requestScope.Job_Id }'/>&amp;page=jobdashboard&amp;viewjobtype=ION&amp;ownerId=1">Process Checklist</a></li>
      			<li><a href="InsuranceRequest.do?msaId=<c:out value='${requestScope.msaId }'/>&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;appendixId=<c:out value='${requestScope.appendixId }'/>">Email Insurance Certificate Request</a></li>
      			<li><a href="JobNotesAction.do?jobId=<c:out value='${requestScope.Job_Id }'/>&amp;appendixid=<c:out value='${requestScope.appendixId }'/>">Job Notes</a></li>
      			
		  		<li>
		        	<a href="#"><span>Test Indicator</span></a>
		        	<ul>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&amp;jobIndicator=None&amp;changeTestIndicator=yes&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;tabId=1">None</a></li>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&amp;jobIndicator=Demonstration&amp;changeTestIndicator=yes&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;tabId=1">Demonstration</a></li>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&amp;jobIndicator=Internal Test&amp;changeTestIndicator=yes&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;tabId=1">Internal Test</a></li>
					</ul>
		  		</li>	
  			</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:projectJobManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:projectJobSendStatusReport()">Send</a></li>
      			<li><a href="javascript:projectJobManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
  		
  		</ul>
</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			          <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
							 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
							<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
								  <a><span class="breadCrumb1">Quality Control Checklist</a>
			    </div></td>
			</tr>
	        <tr><td height="5">&nbsp;</td></tr>	
		</tbody></table>	






<!--  End of change  -->
<html:form action = "ChecklistTab" enctype = "multipart/form-data">
<!--  condition Test -->
  		<c:if test="${requestScope.isPO ne ''}">
			<input type="hidden" name="showJobDB" value="<c:out value="${requestScope.isPO}"/>" />   
			<input type="hidden" name="isPO" value="<c:out value="${requestScope.isPO}"/>" />   
		</c:if>
		
		<c:if test="${requestScope.isActionComplete ne ''}">
			<input type="hidden" name="isActionComplete" value="<c:out value="${requestScope.isActionComplete}"/>" />   
		</c:if>
		
		<c:if test="${requestScope.tabId ne ''}">
			<input type="hidden" name="tabId" value="<c:out value="${requestScope.tabId}"/>" />   
		</c:if>
		
		<c:if test="${requestScope.tickect_id ne ''}">
			<input type="hidden" name="ticket_id" value="<c:out value="${requestScope.ticket_id}"/>" />   
		</c:if>
		<c:if test="${requestScope.tickectType ne ''}">
			<input type="hidden" name="ticketType" value="<c:out value="${requestScope.ticketType}"/>" />   
		</c:if>
		
		<c:if test="${requestScope.jobCompleted ne ''}">
			<input type="hidden" name="jobCompleted" value="<c:out value="${requestScope.jobCompleted}"/>" />   
		</c:if>















<%-- <table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
			<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
			<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
			<td id = "pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
	</tr>
	<tr><td background="images/content_head_04.jpg" height="21" colspan="10">
	         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
							 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
							<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
								  <a><span class="breadCrumb1">Quality Control Checklist</a>
		    </td>
		</tr>
		<c:if test="${requestScope.isPO ne ''}">
			<input type="hidden" name="showJobDB" value="<c:out value="${requestScope.isPO}"/>" />   
			<input type="hidden" name="isPO" value="<c:out value="${requestScope.isPO}"/>" />   
		</c:if>
		
		<c:if test="${requestScope.isActionComplete ne ''}">
			<input type="hidden" name="isActionComplete" value="<c:out value="${requestScope.isActionComplete}"/>" />   
		</c:if>
		
		<c:if test="${requestScope.tabId ne ''}">
			<input type="hidden" name="tabId" value="<c:out value="${requestScope.tabId}"/>" />   
		</c:if>
		
		<c:if test="${requestScope.tickect_id ne ''}">
			<input type="hidden" name="ticket_id" value="<c:out value="${requestScope.ticket_id}"/>" />   
		</c:if>
		<c:if test="${requestScope.tickectType ne ''}">
			<input type="hidden" name="ticketType" value="<c:out value="${requestScope.ticketType}"/>" />   
		</c:if>
		
		<c:if test="${requestScope.jobCompleted ne ''}">
			<input type="hidden" name="jobCompleted" value="<c:out value="${requestScope.jobCompleted}"/>" />   
		</c:if>

</table> --%>

<table>
		<logic:present name = "changestatusflag" scope = "request">
			<logic:equal name = "changestatusflag" value = "-9100">
				<tr><td colspan = "4" class = "message" height = "30">Status change failure!!  All dispatches are not scheduled.</td></tr>
			</logic:equal>
			<logic:equal name = "changestatusflag" value = "0">
				<tr><td colspan = "4" class = "message" height = "30">Job Completed Successfully</td></tr>
				<% if(!module.equals("PRM") && !netMedXAtt.equals("")) { %>
					<script>parent.ilexleft.location.reload();</script>
				<% } %>	
			</logic:equal>
		</logic:present>	
</table>
		
<table>
	<logic:present name = "updateflag" scope = "request">
		  	<% pos = 110; %>
		  	<tr>
			  	<td colspan = "10" class = "message" >
				  	<logic:equal name = "updateflag" value = "0">
				  		<bean:message bundle = "PRM" key = "prm.checktab.checklistsuccess"/>
				  	</logic:equal>
				  	
				  	<logic:equal name = "updateflag" value = "-9001">
				  		<bean:message bundle = "PRM" key = "prm.checktab.errordeletingexistsite"/>
				  	</logic:equal>
				  	
				  	<logic:equal name = "updateflag" value = "-9002">
				  		<bean:message bundle = "PRM" key = "prm.checktab.errorinsitesave"/>
				  	</logic:equal>
				</td>
			</tr>
	</logic:present> 
	<logic:present name = "partnerupdateflag" scope = "request">
		<tr>
		  	<td colspan = "10" class = "message" >
				<logic:equal name = "partnerupdateflag" value = "0">
					<bean:message bundle = "PRM" key = "prm.checktab.partnerchecksavesucces"/>
				</logic:equal>
		
				<logic:equal name = "partnerupdateflag" value = "-9001">
					<bean:message bundle = "PRM" key = "prm.checktab.errordeletingoptions"/>
				</logic:equal>
				
				<logic:equal name = "partnerupdateflag" value = "-9002">
					<bean:message bundle = "PRM" key = "prm.checktab.errorsavingoptions"/>
				</logic:equal>
			</td>
		</tr>
	</logic:present>
</table>

<table border = "0" cellspacing = "0" cellpadding = "0">
	<c:if test="${empty requestScope.isPO}">
	
	  <tr height = "20"> 
	  		<td  width = "6" height = "0"></td>
			<td height = "20" class = "labeleboldhierrarchy" colspan="2">
				<bean:message bundle = "PRM" key = "prm.checktab.sitehypen"/>&nbsp;<%= sitename %>
			</td>
	  </tr>
	  <tr height = "20"> 
	  		<td  width = "6" height = "0"></td>
			<td height = "20" class = "labeleboldhierrarchy">
				<bean:message bundle = "PRM" key = "prm.checktab.workcompleted"/><FONT class="red">*</FONT>:&nbsp;
				<html:radio property = "workCompleted" value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
				<html:radio property = "workCompleted"   value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>
			</td>
	  </tr>
	  <tr height = "20"> 
	  		<td  width = "6" height = "0"></td>
			<td height = "20" class = "labeleboldhierrarchy">
				<bean:message bundle = "PRM" key = "prm.checktab.firstpartnervisitsuccess"/><FONT class="red">*</FONT>:&nbsp;
				<html:radio  property = "firstvisitsuccess"   value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
				<html:radio  property = "firstvisitsuccess"   value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>
			</td>
	  </tr>
	</c:if>
	<c:if test="${not empty requestScope.isPO && requestScope.isPO ne ''}">
		<html:hidden name = "ChecklistTabForm" property = "workCompleted" />
		<html:hidden name = "ChecklistTabForm" property = "firstvisitsuccess" />
	</c:if>
</table>

<table>
	<tr>
	  	<td width = "2" height = "0"></td>
	  	<td>
	<table  border = "0" cellspacing = "1" cellpadding = "0" >
	
	<logic:present name = "sitechecklist" scope = "request">
		
		<tr height = "30">
		<td class = "trybleft" colspan = "3"><b>Site POC Rating</b></td>
		</tr>
		
		<tr>
			<td  class = "hypereven"  >&nbsp;
				<bean:message bundle = "PRM" key = "prm.checktab.sitejobcompletedsuccess"/><Font class = "red">*</Font>
			</td>
			<td class = "hypereven" colspan = "2">
				<html:radio property = "siteJobCompleted" value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
				<html:radio property = "siteJobCompleted" value = "S" />Somewhat
				<html:radio property = "siteJobCompleted" value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>&nbsp;(60%)
				
			</td>	
		</tr>
		
		<tr>
			<td  class = "hyperodd">&nbsp;
				<bean:message bundle = "PRM" key = "prm.checktab.sitejobprofessional"/><Font class = "red">*</Font>
			</td>
			<td class = "hyperodd" colspan = "2">
				<html:radio property = "siteJobProfessional" value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
				<html:radio property = "siteJobProfessional" value = "S" />Somewhat
				<html:radio property = "siteJobProfessional" value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>&nbsp;(40%)
			</td>	
		</tr>
		<tr>
			<td  class = "hypereven">&nbsp;
				<bean:message bundle = "PRM" key = "prm.checktab.sitejobfollowup"/><Font class = "red">*</Font>
			</td>
			<td class = "hypereven"  colspan = "2">
				<html:radio property = "jobSiteFollowUp" value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
				<html:radio property = "jobSiteFollowUp" value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>
			</td>	
		</tr>
		<tr>
			<td  class = "hyperodd">&nbsp;
				<!-- POC releasing tech -->Local Site Contact releasing tech<Font class = "red">*</Font>:
			</td>
			<td class = "hyperodd" colspan = "2">
				<html:text property="pocTech" styleClass="textbox" />
			</td>
		</tr>
		<tr>
			<td  class = "hypereven">&nbsp;
<!-- 				Best number for POC -->
				Best number for Local Site Contact
				<Font class = "red">*</Font>:
			</td>
			<td class = "hypereven"  colspan = "2">
				<html:text property="pocNumber" styleClass="textbox"/>
			</td>
		</tr>
		<tr>
			<td  class = "hyperodd">&nbsp;
				<bean:message bundle = "PRM" key = "prm.powo.comments"/>:
			</td>
			<td class = "hyperodd" colspan = "2">
				<html:textarea property = "pocComments" cols="30" rows="2"/>
				
			</td>	
		</tr>
	</logic:present>
	<br>
	
	<% 
	//Changed for avoid the Internal Partner apppear in Process check list. 
	for( int g = 0; g < totalPartnersName; g++ )
	{
		i = 0;
	%> 
	
	<c:if test="${empty requestScope.isPO}">
		<tr>
			<td class = "labeleboldhierrarchy" bgcolor="red">&nbsp;</td>
	 	</tr>
	 </c:if>
	
	 
	<tr height = "30">
		<td class = "trybleft" colspan="3"><bean:message bundle = "PRM" key = "prm.checktab.qchecklisthypen"/>&nbsp;<%= partnername[g] %></td>
	</tr>
	<logic:present name = "partnerchecklist" scope = "request">
		<tr height="25"><td class = "labeleboldhierrarchy" colspan="3"><bean:message bundle = "PRM" key = "prm.checktab.partnerrating"/></td></tr>
		<tr>
			<td class = "hypereven">&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.checktab.techname"/><font class="red">*</font>:</td>
			<td class = "hypereven" colspan="2"><input type="text" name=<%="techName"+g%> value="<%= partnername[g] %>" size="41" Class="textbox"></td>
		</tr>
		<logic:iterate id = "P_checklist" name = "partnerchecklist">
		<html:hidden name = "P_checklist" property = "optionsperques" />
		<bean:define id = "noptions" name = "P_checklist" property = "optionsperques" type = "java.lang.String"/>
		<input type = "hidden"  name = "<%="optionsperques"+i+g%>" value="<%=noptions %>"/>
			<%	
				if ( csschooser == true ) 
				{
					
					backgroundclass = "hyperodd";
					backgroundclass1 = "readonlytextodd";
					csschooser = false;
					centerclass = "hyperodd";
				}
		
				else
				{
					backgroundclass = "hypereven";
					backgroundclass1 = "readonlytexteven";
					csschooser = true;	
					centerclass = "hypereven";	
				}
			%>
			<bean:define id = "opt" name = "P_checklist" property = "option" type = "java.lang.String"/>
			<bean:define id = "optId" name = "P_checklist" property = "optionid" type = "java.lang.String"/>
			<bean:define id = "opt_type" name = "P_checklist" property = "optiontype" type = "java.lang.String" />
			<bean:define id = "itemdesc"  name = "P_checklist" property = "itemname" type = "java.lang.String" />
			<tr>
				<td class = "<%= centerclass %>">
					&nbsp;&nbsp;<bean:write name = "P_checklist" property = "itemname" /><font class="red">*</font>
				</td>
				<td class = "<%= centerclass %>" colspan="2">
				<%
					if( opt != null )
					{
						option1 = opt.split( "-" );
						optionid1 = optId.split( "-" );

						for( j = 0; j < option1.length; j++ )
						{
							if(opt_type.equals( "S" ) )
							{
								if( j == 0 )
								{%>
									<input type = "radio"  name = "<%="option"+i+g%>"   value = "<%=optionid1[j] %>" /><%=option1[j] %>
								<%}
								else
								{ %>
									<input type="radio"   name="<%="option"+i+g%>" value="<%=optionid1[j] %>" indexed = "true" /><%=option1[j] %>
								<%}%>
								<input type = "hidden" name="<%="optioncheckbox"+g%>" value="0" />
							<%}
							else
							{ 
								if(j==0)
								{%>
								
									<input type="checkbox"  name="<%="optioncheckbox"+g%>" value="<%=optionid1[j] %>" /><%=option1[j] %>
								
								<%}
								else
								{ %>
									<input type="checkbox" name="<%="optioncheckbox"+g%>" value="<%=optionid1[j] %>" /><%=option1[j] %>
									
							
							<%  }%>
							
								<input type="hidden"  name="<%="option"+i+g%>" value="0"  /> 
							<%}
						k++;
						}
						%>
						&nbsp;(<bean:write  name = "P_checklist" property = "percent"/>%)
						</td>
						
						<% 
						j=0;
					}
					%>
			<%i++; %>
			</tr>
			</logic:iterate>
			
			<% if ( csschooser == true ) {
					centerclass = "hyperodd";
					csschooser = false;
			}
				else {
					centerclass = "hypereven";
					csschooser = true;
			}	%>
			<tr>
				<td  class = "<%= centerclass %>">
					&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.checktab.firstpartnervisitsuccess"/>?<font class="red">*</font>
				</td>
				<td class = "<%= centerclass %>" colspan = "2">
					<input type = "radio"  name = "<%= "firstvisitpartner"+g%>"   value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
					&nbsp;<input type = "radio"  name = "<%= "firstvisitpartner"+g%>"   value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>
				</td>
			</tr>
			<% if ( csschooser == true ) {
					centerclass = "hyperodd";
					csschooser = false;
			}
				else {
					centerclass = "hypereven";
					csschooser = true;
			}	%>
			
			<tr>
			<td  class = "<%= centerclass %>">&nbsp;
				<bean:message bundle = "PRM" key = "prm.checktab.recomandtechfuturejobs"/><FONT class="red">*</FONT>
			</td>
			<td  class = "<%= centerclass %>">
				<input type = "radio"  name = "<%= "recommadTechFutureJobs"+g%>"   value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
				&nbsp;<input type = "radio"  name = "<%= "recommadTechFutureJobs"+g%>"   value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>
			</td>
			</tr>
			
<!-- 			<tr> -->
<%-- 			<td  class = "<%= centerclass %>">&nbsp; --%>
<%-- 				<bean:message bundle = "PRM" key = "prm.checktab.user.rating"/><FONT class="red">*</FONT> --%>
<!-- 			</td> -->
<%-- 			<td  class = "<%= centerclass %>"> --%>
<%-- 				<input type = "radio"  name = "<%= "userRatingFlag"+g%>"   value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/> --%>
<%-- 				&nbsp;<input type = "radio"  name = "<%= "userRatingFlag"+g%>"   value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/> --%>
<!-- 			</td> -->
<!-- 			</tr> -->
			
			
           <% if ( csschooser == true ) {
					centerclass = "hyperodd";
			}
				else {
					centerclass = "hypereven";
			}	%>
			
			
			<tr>
			<td  class = "<%= centerclass %>">&nbsp;
				<bean:message bundle = "PRM" key = "prm.powo.comments"/>:
			</td>
			<td class = "<%= centerclass %>" colspan = "2">
<%-- 				<html:textarea property = "" value = "" cols="30" rows="2"/> --%>
			<textarea rows="3" cols="20" name = "<%= "operationRatingComments"+g%>" id = "<%= "operationRatingComments"+g%>"></textarea>
				
			</td>	
		</tr>
		<tr height="25"><td class = "labeleboldhierrarchy" colspan="3"><bean:message bundle = "PRM" key = "prm.checktab.recommenedaction"/></td></tr>
	
		<tr>
				<td  class = "hypereven">
					&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.checktab.letterofapology"/>
				</td>
				<td class = "hypereven" colspan = "2">
					<input type = "radio"  name = "<%= "letterOfApology"+g%>"   value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
					&nbsp;<input type = "radio"  name = "<%= "letterOfApology"+g%>"   value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>
				</td>
			</tr>
			
			<tr>
				<td  class = "hyperodd">
					&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.checktab.restricttech"/>
				</td>
				<td class = "hyperodd" colspan = "2">
					<input type = "radio"  name = "<%= "restrictTech"+g%>"   value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
					&nbsp;<input type = "radio"  name = "<%= "restrictTech"+g%>"   value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>
				</td>
			</tr>
			
				<tr>
				<td  class = "hypereven">
					&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.checktab.restrictcertpartner"/>
				</td>
				<td class = "hypereven" colspan = "2">
					<input type = "radio"  name = "<%= "restrictCertPartner"+g%>"   value = "Y" /><bean:message bundle = "PRM" key = "prm.checktab.yes"/>
					&nbsp;<input type = "radio"  name = "<%= "restrictCertPartner"+g%>"   value = "N" /><bean:message bundle = "PRM" key = "prm.checktab.no"/>
				</td>
			</tr>
			
			<tr>
				<td  class = "hyperodd">
					&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.checktab.otherrecommendations"/>
				</td>
				<td class = "hyperodd" colspan = "2">
					<html:textarea property = "otherRecomendations" value = "" cols="30" rows="2"/>
				</td>	
			</tr>
	</logic:present>
	
	<jsp:include page = '/Footer.jsp'>
	      <jsp:param name = 'colspan' value = '28'/>
	      <jsp:param name = 'helpid' value = 'pvsPartner_Edit'/>
	</jsp:include>
<%} %>
	<tr height = "25">
		<td>&nbsp;</td>
	</tr>
	<tr> 
	 	<td colspan = "3" class = "buttonrow"> 
	 	<% String validate = "javascript: return validateAll("+popwin +");";
	 	%>
	 	
	    	<html:submit property = "savelist" styleClass = "button" onclick ="<%= validate %>"><bean:message bundle = "PRM" key = "prm.checktab.save"/></html:submit>
	      	<html:reset property = "reset" styleClass = "button"><bean:message bundle = "PRM" key = "prm.checktab.reset"/></html:reset>
	    </td>
		 <td>&nbsp;</td>
	</tr> 
</table>
</td>
</tr>
</table>



<html:hidden name = "ChecklistTabForm" property = "job_id" />
<html:hidden name = "ChecklistTabForm" property = "viewjobtype" />
<html:hidden name = "ChecklistTabForm" property = "ownerId" />
<html:hidden name = "ChecklistTabForm" property = "appendix_id" />
<html:hidden name = "ChecklistTabForm" property = "page" />
<html:hidden name = "ChecklistTabForm" property = "selectedoption" />
<html:hidden name = "ChecklistTabForm" property = "quessize" />
<html:hidden name = "ChecklistTabForm" property = "partnername" />
<html:hidden name = "ChecklistTabForm" property = "no_of_partners" />
<html:hidden name = "ChecklistTabForm" property = "firstvisitpartnersuccess" />
<html:hidden name = "ChecklistTabForm" property = "recommadTechFutureJobs" />
<%-- <html:hidden name = "ChecklistTabForm" property = "userRatingFlag" /> --%>
<html:hidden name = "ChecklistTabForm" property = "operationRatingComments" />
<html:hidden name = "ChecklistTabForm" property = "firstvisitpartner" />
<html:hidden name = "ChecklistTabForm" property = "valuesuccess" />
<html:hidden name = "ChecklistTabForm" property = "type" />
<html:hidden name = "ChecklistTabForm" property = "siteJobCompleted" />
<html:hidden name = "ChecklistTabForm" property = "letterOfApology" />
<html:hidden name = "ChecklistTabForm" property = "restrictTech" />
<html:hidden name = "ChecklistTabForm" property = "restrictCertPartner" />
<html:hidden name = "ChecklistTabForm" property = "checkMsp" />
<html:hidden name = "ChecklistTabForm" property = "poId" />
</html:form>
</BODY>

<c:if test = "${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
</html:html>


<script>

function openpostincidentwindow( id , desc )
{
	str = 'AddIncident.do?descr='+desc+'&pid='+id+'&function=add&page=checklist&jobid='+document.forms[0].job_id.value+'&viewjobtype='+document.forms[0].viewjobtype.value+'&ownerId='+document.forms[0].ownerId.value;
	suppstrwin = window.open( str , '' , 'left = 300 , top = 100 , width = 600, height = 600 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}


function defaultTabdisplay() { //v3.0
	var tableRow = document.getElementsByTagName('div');
	
	if (tableRow.length==0) 
	{ 
		return; 
	}
	for (var k = tableRow.length-1; k >-1; k--) 
	{
		viewChange(tableRow[k].getAttributeNode( 'id' ).value );
	}
	viewChange('b0' );
	return true;
}

function viewChange() { //v3.0
	 MM_showHideLayers('b0','','hide');
	 var partners=document.forms[0].no_of_partners.value;
	 for(var i=1;i<=partners;i++)
	 {
 	 var divname="b"+i;
 	
 	 MM_showHideLayers(divname,'','hide');
 	 }
	 var args=viewChange.arguments;
	 MM_showHideLayers(args[0],'','show');
	 return true;
}

function MM_showHideLayers() { //v3.0

  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v;}
    obj.visibility=v;}
}



function validateAll()
{

	var arg = validateAll.arguments;
	var selectlist = "";
	var firstsuccess = "";
	var size = document.forms[0].quessize.value;
	var totalpartners = 0;
	var flag = 0;
	totalpartners = document.forms[0].no_of_partners.value;

	var check=false;
	var	techName = "";
	
	<c:if test="${empty requestScope.isPO}">
		if(!(document.forms[0].workCompleted[0].checked || document.forms[0].workCompleted[1].checked )){
			alert('Please complete all mandatory questions.');
			return false; 
		}
		
		if(!(document.forms[0].firstvisitsuccess[0].checked || document.forms[0].firstvisitsuccess[1].checked )){
			alert('Please complete all mandatory questions.');
			return false; 
		}
	</c:if>
	
	<c:if test="${not empty requestScope.sitechecklist}">
		if(!(document.forms[0].siteJobCompleted[0].checked || document.forms[0].siteJobCompleted[1].checked || document.forms[0].siteJobCompleted[2].checked)){
			alert('Please complete all mandatory questions.');
			return false; 
		}
		
		if(!(document.forms[0].siteJobProfessional[0].checked || document.forms[0].siteJobProfessional[1].checked || document.forms[0].siteJobProfessional[2].checked)){
			alert('Please complete all mandatory questions.');
			return false; 
		}
		
		if(!(document.forms[0].jobSiteFollowUp[0].checked || document.forms[0].jobSiteFollowUp[1].checked )){
			alert('Please complete all mandatory questions.');
			return false; 
		}
		if(document.forms[0].pocTech.value == "" || document.forms[0].pocTech.value == undefined){
			alert('Please complete all mandatory questions.');
			return false; 
		}
		if(document.forms[0].pocNumber.value == "" || document.forms[0].pocNumber.value == undefined){
			alert('Please complete all mandatory questions.');
			return false; 
		}
	</c:if>
	if( size != 0 )
	{
		document.forms[0].operationRatingComments.value = "";
// 		document.forms[0].userRatingFlag.value = "";
		document.forms[0].recommadTechFutureJobs.value = "";
		for(var f = 0; f < totalpartners; f++ )
		{	
			
			if(eval("document.forms[0].techName"+f).value==""){
				alert('Please enter Tech Name.');
				return false;
			}
			for( var j = 0; j < size; j++ )
			{					
				t3="document.forms[0].option"+j;
				
				if (eval(t3+f)[0].checked == false && eval(t3+f)[1].checked == false && eval(t3+f)[2].checked == false)
				{
					check = true;
					break;
				}
			}

			if( eval( "document.forms[0].firstvisitpartner"+f )[0].checked==false && eval( "document.forms[0].firstvisitpartner"+f )[1].checked==false )
				check = true;
			 if( eval( "document.forms[0].recommadTechFutureJobs"+f )[0].checked==false && eval( "document.forms[0].recommadTechFutureJobs"+f )[1].checked==false )
				check = true; 
// 			 if( eval( "document.forms[0].userRatingFlag"+f )[0].checked==false && eval( "document.forms[0].userRatingFlag"+f )[1].checked==false )
// 					check = true; 
			 
		}
	}

	if (check == true)
	{
		alert('Please complete all mandatory questions.');
		return false;
	}

	<c:if test="${empty requestScope.isPO}">
		if( document.forms[0].firstvisitsuccess[0].checked  || document.forms[0].firstvisitsuccess[1].checked )
		{
			if(  document.forms[0].firstvisitsuccess[0].checked )
				document.forms[0].valuesuccess.value = 'Y';
			else
				document.forms[0].valuesuccess.value = 'N';
		}
		else
	</c:if>
			document.forms[0].valuesuccess.value = 'T';
		
	
	if( size != 0 )
	{
		for( f = 0; f < totalpartners; f++ )
		{
			flag = 0;
			if( size == 1 )
			{
				
				if(eval("document.forms[0].optionsperques0"+f).value==1)
				{
					if( eval("document.forms[0].option0"+f).checked )
					{
						selectlist=selectlist+eval("document.forms[0].option0"+f).value+"-";
						flag = 1;
					}
									
				}
				else
				{
				
					for(var a=0;a<eval("document.forms[0].optionsperques0"+f).value;a++)
					{
						if( eval("document.forms[0].option0"+f)[a].checked )
						{
							selectlist=selectlist+eval("document.forms[0].option0"+f)[a].value+"-";
							flag = 1;
							break;
						}
						
					}
								
					
				}
				
				if(eval("document.forms[0].optioncheckbox"+f).length==1)
				{
					if( eval("document.forms[0].optioncheckbox"+f).checked )
						{
							
							selectlist = selectlist+eval("document.forms[0].optioncheckbox"+f).value+"-";
							flag = 1;
						}
				}
				else
				{
				
					for(var i = 0; i < eval("document.forms[0].optioncheckbox"+f).length; i++)
					{						
						if( eval("document.forms[0].optioncheckbox"+f)[i].checked )
						{
							selectlist = selectlist+eval("document.forms[0].optioncheckbox"+f)[i].value+"-";
							flag = 1;
						}
					}
				}
			}
			else
			{
				
				for( var j = 0; j < size; j++ )
				{
					
					t2="document.forms[0].optionsperques"+j;
					
					t3="document.forms[0].option"+j;
					
					if(eval(t2+f).value==1)
					{
						if( eval(t3+f).checked )
						{
							selectlist=selectlist+eval(t3+f).value+"-";
							flag = 1;
						}
						
					}
					else
					{
						
						for(var a=0;a<eval(t2+f).value;a++)
						{
							if( eval(t3+f)[a].checked )
							{
								selectlist=selectlist+eval(t3+f)[a].value+"-";
								flag = 1;
								break;
							}
						
						}
						
					}
								
				}
				
				for(var i = 0; i < eval("document.forms[0].optioncheckbox"+f).length; i++)
					{						
						if( eval("document.forms[0].optioncheckbox"+f)[i].checked )
						{
							
							selectlist=selectlist+eval("document.forms[0].optioncheckbox"+f)[i].value+"-";
							flag = 1;
						}
					}
			}
			
				
			if( selectlist.length > 0 && flag==1)
			{
				selectlist = selectlist.substring( 0 , selectlist.length - 1 );
				
			}
			
			selectlist = selectlist+"~";
			
			if( eval( "document.forms[0].firstvisitpartner"+f )[0].checked || eval( "document.forms[0].firstvisitpartner"+f )[1].checked )
			{
				if( eval( "document.forms[0].firstvisitpartner"+f )[0].checked )
					firstsuccess = 'Y' ;
				else
					firstsuccess = 'N' ;
				
				document.forms[0].firstvisitpartnersuccess.value = document.forms[0].firstvisitpartnersuccess.value + firstsuccess + '~';
			}
			else
			{ 
				document.forms[0].firstvisitpartnersuccess.value = document.forms[0].firstvisitpartnersuccess.value + '' + '~';;
			}	
			
			if( eval( "document.forms[0].recommadTechFutureJobs"+f )[0].checked || eval( "document.forms[0].recommadTechFutureJobs"+f )[1].checked )
			{
				if( eval( "document.forms[0].recommadTechFutureJobs"+f )[0].checked )
					firstsuccess = 'Y' ;
				else
					firstsuccess = 'N' ;
				
				document.forms[0].recommadTechFutureJobs.value = document.forms[0].recommadTechFutureJobs.value + firstsuccess + '~';
			}
			else
			{ 
				document.forms[0].recommadTechFutureJobs.value = document.forms[0].recommadTechFutureJobs.value + '' + '~';;
			}
			
			
			
			
// 			if( eval( "document.forms[0].userRatingFlag"+f )[0].checked || eval( "document.forms[0].userRatingFlag"+f )[1].checked )
// 			{
// 				if( eval( "document.forms[0].userRatingFlag"+f )[0].checked )
// 					firstsuccess = 'Y' ;
// 				else
// 					firstsuccess = 'N' ;
				
// 				document.forms[0].userRatingFlag.value = document.forms[0].userRatingFlag.value + firstsuccess + '~';
// 			}
// 			else
// 			{ 
// 				document.forms[0].userRatingFlag.value = document.forms[0].userRatingFlag.value + '' + '~';;
// 			}
			
			//operationRatingComments
			if(document.getElementById("operationRatingComments"+f).value =='' || document.getElementById("operationRatingComments"+f).value == null)
				document.getElementById("operationRatingComments"+f).value = ' ';
			document.forms[0].operationRatingComments.value = document.forms[0].operationRatingComments.value + document.getElementById("operationRatingComments"+f).value + '~|~'
			
		}
	}
	
	document.forms[0].selectedoption.value = selectlist;
	if(arg[0]== 1)
	{   
		document.forms[0].action = "ChecklistTab.do?job_id="+document.forms[0].job_id.value+"popwin=1";
		document.forms[0].action= "ChecklistTab.do?type="+document.forms[0].type.value+"&Status=F&job_id="+document.forms[0].job_id.value+"&popwin=1&page=jobdashboard&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
		document.forms[0].target = "ilexmain";
		window.close();
	}
	return true;

}




function setSelected(sopt)
{//changed
//	if( !(document.forms[0].firstvisitsuccess[0].checked) && !(document.forms[0].firstvisitsuccess[1].checked))
//		document.forms[0].firstvisitsuccess[1].checked = true;
	
		
	//defaultTabdisplay();
	var optionslist = document.forms[0].selectedoption.value;
	var totalpartners = 0;
	totalpartners = document.forms[0].no_of_partners.value;
	
	selectpartneropt = optionslist.split("~");
	
	var selectopt=new Array(selectpartneropt.length);
	if(selectpartneropt!=null)
	{
		for(t=0;t<selectpartneropt.length;t++)
			{	
				temp=selectpartneropt[t].split("-");
				selectopt[t]=new Array(temp.length);
				for(q=0;q<temp.length;q++)
				{
					selectopt[t][q]=temp[q];
				}
			}
	}
	selectPartnerOptLength = '<c:out value="${requestScope.selectedOptionSize}"/>';
	var size=document.forms[0].quessize.value;
	var selectlist="";
	if(eval(document.forms[0].no_of_partners.value) > 0) {
		if( size != 0 )
		{
			for(var f=0;f<selectpartneropt.length;f++)
			{
				temp=selectpartneropt[f].split("-");
				for(var k=0;k<temp.length;k++)
				{
					if( size == 1 )
					{
						if(eval("document.forms[0].optionsperques0"+f).value==1)
						{
							if( eval("document.forms[0].option0"+f).value==selectopt[f][k] )
							{
								eval("document.forms[0].option0"+f)=true;
							}										
						}
						else
						{
							for(var a=0;a<eval("document.forms[0].optionsperques0"+f).value;a++)
							{
								if( eval("document.forms[0].option0"+f)[a].value==selectopt[f][k] )
								{
									eval("document.forms[0].option0"+f)[a].checked=true;
									break;
								}							
							}
						}
						
						if(eval("document.forms[0].optioncheckbox"+f).length==1)
						{
							if( eval("document.forms[0].optioncheckbox"+f).value==selectopt[f][k] )
									{
										
										eval("document.forms[0].optioncheckbox"+f).checked=true;
									}
						}
						else
						{
							for(var i = 0; i < eval("document.forms[0].optioncheckbox"+f).length; i++)
							{
								
								if( eval("document.forms[0].optioncheckbox"+f)[i].value==selectopt[f][k] )
								{
									
									eval("document.forms[0].optioncheckbox"+f)[i].checked=true;
								}
							}
						}
							
					}
					else
					{
						
						for( var j = 0; j < size; j++ )
						{						
							t3="document.forms[0].optionsperques"+j;
							if(eval(t3+f).value==1)
							{
															
								t4=eval("document.forms[0].option"+j);
								if( eval(t4+f).value==selectopt[f][k] )
								{
									eval(t4+f).checked=true;
								}
								
							}
							else
							{
								
								t2="document.forms[0].optionsperques"+j;
								
								for(var a=0;a<eval(t2+f).value;a++)
								{
									
									var t1="document.forms[0].option"+j;
																	
									if( eval(t1+f)[a].value==selectopt[f][k])
									{
										eval(t1+f)[a].checked=true;
										break;
										
									}							
								}
															
							}
										
						}
						
						
						for(var i = 0; i < eval("document.forms[0].optioncheckbox"+f).length; i++)
							{
								
								if( eval("document.forms[0].optioncheckbox"+f)[i].value == selectopt[f][k] )
								{
									
									eval("document.forms[0].optioncheckbox"+f)[i].checked=true;
								}
							}										
					}
				}
			}
			
			
			var letterofapology = document.forms[0].letterOfApology.value;
		
			var letterofapologyarr = letterofapology.split("~");
		
			var restrictTech = document.forms[0].restrictTech.value;
			var restrictTecharr = restrictTech.split("~");
			var restrictCertPartner = document.forms[0].restrictCertPartner.value;
			var restrictCertPartnerarr = restrictCertPartner.split("~");
			
			
			//changed	
			for( var f = 0; f < totalpartners; f++ )
			{
			 if(letterofapologyarr[f]!='Y' && letterofapologyarr[f]!='N')
			  {
			  
			 	eval( "document.forms[0].letterOfApology"+f )[1].checked = true;
			  }
			  if(restrictTecharr[f]!='Y' && restrictTecharr[f]!='N')
			  {
			 	eval( "document.forms[0].restrictTech"+f )[1].checked = true;
			  }
			  if(restrictCertPartnerarr[f]!='Y' && restrictCertPartnerarr[f]!='N')
			  {
			 	eval( "document.forms[0].restrictCertPartner"+f )[1].checked = true;
			  }
			
			
			
			  optionslist = document.forms[0].firstvisitpartner.value;
				selectpartneropt = optionslist.split( "~" );
			 if(selectpartneropt[f]!='Y' && selectpartneropt[f]!='N')
			  {
			// 	eval( "document.forms[0].firstvisitpartner"+f )[1].checked = true;
			  }
			  else
			  {
				  for( var i = 0 ; i < 2; i++ )
				  { 
				  	  if( eval( "document.forms[0].firstvisitpartner"+f )[i].value == selectpartneropt[f] )
					  {
					  	  eval( "document.forms[0].firstvisitpartner"+f )[i].checked = true;
					  }
				  }
			   }
			 
// 			  optionslist = document.forms[0].userRatingFlag.value;
// 				selectpartneropt = optionslist.split( "~" );
// 			 if(selectpartneropt[f]!='Y' && selectpartneropt[f]!='N')
// 			  {
// 			// 	eval( "document.forms[0].userRatingFlag"+f )[1].checked = true;
// 			  }
// 			  else
// 			  {
// 				  for( var i = 0 ; i < 2; i++ )
// 				  { 
// 				  	  if( eval( "document.forms[0].userRatingFlag"+f )[i].value == selectpartneropt[f] )
// 					  {
// 					  	  eval( "document.forms[0].userRatingFlag"+f )[i].checked = true;
// 					  }
// 				  }
// 			   }
			 
			 
			  optionslist = document.forms[0].recommadTechFutureJobs.value;
				selectpartneropt = optionslist.split( "~" );
			 if(selectpartneropt[f]!='Y' && selectpartneropt[f]!='N')
			  {
			// 	eval( "document.forms[0].recommadTechFutureJobs"+f )[1].checked = true;
			  }
			  else
			  {
				  for( var i = 0 ; i < 2; i++ )
				  { 
				  	  if( eval( "document.forms[0].recommadTechFutureJobs"+f )[i].value == selectpartneropt[f] )
					  {
					  	  eval( "document.forms[0].recommadTechFutureJobs"+f )[i].checked = true;
					  }
				  }
			   }
			 
				optionslist = document.forms[0].operationRatingComments.value;
				selectpartneropt = optionslist.split( "~" );
				eval( "document.forms[0].operationRatingComments"+f ).value = selectpartneropt[f];

			}
		}
	}	
	return true;

}




</script>


