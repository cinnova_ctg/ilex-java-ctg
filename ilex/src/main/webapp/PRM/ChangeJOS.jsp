<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<html:html>
	<head>
		<title></title>
		<%@ include file = "../Header.inc" %>
		<%@ include file = "/NMenu.inc" %> 
		<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
		<link rel = "stylesheet" href="styles/content.css" type="text/css">
		<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
		<script language="JavaScript" src="javascript/popcalendar.js"></script>
		<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
</head>

<%
	String Appendix_Id = "";
	int checkSize=0;
	int div = 1;
	int i=0;
	int firstdivrowcount = 375;
	String newJobOwner="";
	String newJobScheduler="";
	//System.out.println("Re"+request.getAttribute( "isBukJobCreationView" ));
	String isBukJobCreationView = ( String )request.getAttribute( "isBukJobCreationView" );
	if(request.getAttribute( "Appendix_Id" ) != null) 
	{
		Appendix_Id = request.getAttribute( "Appendix_Id" ).toString();
		request.setAttribute( "Appendix_Id" , Appendix_Id );
	}
	//System.out.println("111"+Appendix_Id);	
	if(request.getAttribute( "appendixid" ) != null) 
	{
		Appendix_Id = request.getAttribute( "appendixid" ).toString();
		request.setAttribute( "appendixid" , Appendix_Id );
	}
	if(request.getAttribute( "joblistsize" ) != null) {
		checkSize=Integer.parseInt(request.getAttribute( "joblistsize" ).toString());
	}
	//System.out.println("111---"+request.getAttribute( "dyComboPRMOwner" ));
	//System.out.println("111---"+request.getAttribute( "dyComboPRMScheduler" ));
	if(request.getAttribute( "dyComboPRMOwner" ) != null) {
		newJobOwner = request.getAttribute( "dyComboPRMOwner" ).toString();
	} else {
		newJobOwner = "False";
	}
	if(request.getAttribute( "dyComboPRMScheduler" ) != null) {
		newJobScheduler = request.getAttribute( "dyComboPRMScheduler" ).toString();
	} else {
		newJobScheduler="False";	
	}
%>

<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());
//System.out.println("222"+jobOwnerListSize);

%>
	<logic:present name = "addendumjobnamelist" scope = "request">
		<% 
			firstdivrowcount = ( int ) Integer.parseInt( request.getAttribute( "firstdivrownum" ).toString() ) * 20 + 375;
			System.out.println("firstdivrownum"+request.getAttribute( "firstdivrownum" ));
		%>
	</logic:present>
<%@ include file = "/NMenu.inc" %> 					<!-- Include file for Menu CSS -->
<%@ include  file="/AjaxOwners.inc" %>  			<!-- Include file for Ajax call -->
<%@ include  file="/DashboardStatusScript.inc" %> 	<!-- Include file for JavaScript function -->
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "leftAdjLayers();" > 

<html:form action = "/ChangeJOSAction">

<html:hidden property = "ownerId" />
<html:hidden property = "viewjobtype" /> 
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property = "appendixid" />
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %> <!-- Include file get parameter and Script function for Appendix Dashboard Menu -->
<%@ include  file="/AppendixDashboardMenuScript.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
		        	<td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
		        <logic:present name="Updatepage" scope="request">
			        <td colspan="4">
			        	<h2><bean:message bundle = "PRM" key = "prm.BulkJobCreation.updateJobOwnerandScheduler"/>&nbsp;<bean:message bundle = "PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name = "ChangeJOSForm" property = "appendixName" /></h2>
			        </td>
			     </logic:present> 
				</tr>
				
				 <logic:notPresent name="Updatepage" scope="request">
					 <tr>
					 	<td height="15"></td>
					 </tr>
					 <tr>
					     <td class="message" colspan="4" style="padding-left: 15px;">
					     	There are no Jobs defined for this appendix.
					     </td>
				     </tr>
			     </logic:notPresent> 
				
	      </table>
   </td>
   <!-- Top sky blue: Start -->
	<td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<!-- Top sky blue: End -->
   <!-- View Selector: Start -->
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="155" height="39" class="headerrow" colspan="2">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="150">
						<table width="100%" cellpadding="0" cellspacing="3" border="0">
							<tr>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" 
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');" /></a>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td height="21" background="images/content_head_04.jpg" width="140">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="70%" colspan="2">&nbsp;</td>
						<td nowrap="nowrap" colspan="2">
							<div id="featured1">
								<div id="featured">
									<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img	src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();" /></a><a href="javascript:void(0)"
										class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();" /></a>
									</li>
								</div>
							</div>
							<span>
							<div id="filter" class="divstyle">
							<table width="250" cellpadding="0" class="divtable">
								<tr>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Status View</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText">
											<logic:present name="jobStatusList" scope="request">
												<logic:iterate id="list" name="jobStatusList">
													<bean:define id="statusName" name="list" property="statusdesc" type="java.lang.String" />
													<bean:define id="statusId" name="list" property="statusid" type="java.lang.String" />
													<%checkstatus = "javascript: checkingStatus('" + statusId + "');";
													if ((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
														addStatusRow++; addStatusSpace = true;
													}%>
													<%if (addStatusSpace) {%>
													&nbsp;&nbsp;&nbsp;&nbsp;
													<%}%>
													<html:multibox property="jobSelectedStatus" onclick="<%=checkstatus%>">
														<bean:write name="list" property="statusid" />
													</html:multibox>
													<bean:write name="list" property="statusdesc" />
													<br> 
													<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present> <img src="images/hrfilterLine.gif" width="100px" class="imagefilter" />
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name="ChangeJOSForm" property="jobMonthWeekCheck" value="week">This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name="ChangeJOSForm" property="jobMonthWeekCheck" value="month">This Month</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name="ChangeJOSForm"	property="jobMonthWeekCheck" value="clear">Clear</html:radio></td></tr>
											</table>
											</td>
										</tr>
									</table>
									</td>
									<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter" /></td>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table> 
									</td>
								</tr>
							</table>
							</div>
							</span>		
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</td>
	<!-- View Selector: End -->
</tr>
</table>
<!-- View Selecter tabel has ended -->
 
<!-- Update Job Combo boxes and Search Criteria tabel is started --> 
<logic:present name="Updatepage" scope="request">
	<table border ="0" cellspacing="0" cellpadding="0">

		<tr>
			<td style="padding-left: 10px;">
				<table>
				<logic:present name="Updated"  scope="request" >
					<tr>
						<td height="10"></td>
					</tr>
					<tr>
						<logic:equal name="Updated"  value="0">
							<td class="message">Jobs updated successfully.</td>
						</logic:equal>
						<logic:equal name="Updated"  value="-90060">
							<td class="message">Error has been Occured during Updation</td>
						</logic:equal>
					</tr>
					<tr>
						<td height ="5"></td>
					</tr>
				</logic:present>
				<logic:notPresent name="Updated"  scope="request" >
					<tr>
						<td height="20"></td>
					</tr>
				</logic:notPresent>
					<tr>
						<td class="textwhitetop"><b>Select New Job Owner and/or Scheduler</td>
					</tr>
					<tr>
						<td>
							<table>
								<tr>
									<td class="textwhite">Job Owner:&nbsp;
									</td>
									<logic:present name ="dyComboPRMOwner" scope="request">
										<td class="textwhite"><html:select property = "newOwner" size = "1" styleClass="combowhite">
														<html:optionsCollection name = "dyComboPRMOwner" property = "allOwnerName" value = "value" label = "label"/>    
													</html:select>
										</td>
									</logic:present>
									<logic:notPresent name = "dyComboPRMOwner" scope="request" >
										<td class="textwhiteFont">A team member in this role is not defined for the project.
										</td>
										<html:hidden property = "newOwner" />
									</logic:notPresent>
								</tr>
								<tr>
									<td class="textwhite">Scheduler:&nbsp;
									</td>
									<logic:present name = "dyComboPRMScheduler" scope="request">
										<td class="textwhite"><html:select property = "newScheduler" size = "1" styleClass="combowhite">
														<html:optionsCollection name = "dyComboPRMScheduler" property = "schedulerList" value = "value" label = "label"/>    
													</html:select>
										</td>
									</logic:present>
									<logic:notPresent name = "dyComboPRMScheduler" scope="request" >
										<td class="textwhiteFont">A team member in this role is not defined for the project.
										</td>
										<html:hidden property = "newScheduler" />
									</logic:notPresent>
								</tr>
								<tr>
									<td class="textwhite">Planned Start Date:</td>
									<td><html:text  styleClass="textbox" size="10" property="planedStartDate" readonly="true"/>
							   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].planedStartDate, document.forms[0].planedStartDate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- Table Job Owner end -->
	<!-- Table Search for Job is started -->
	<table border ="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="10">
			</td>
		</tr>
		<tr>
			<td style="padding-left=10px;">
				<table>
					<tr>
						<td class="textwhitetop" colspan="6"><b>Search for Jobs to Update
						</td>
					</tr>
					<tr>
						<td height="5">
						</td>
					</tr>
					<tr>
						<td class="textwhite" colspan="6" style="padding-left: 12px;"><b>Job Information
						</td>
					</tr>
					<tr>
						<td colspan="6" height="4"></td>
					</tr>
					<tr>
						<td class="textwhite" style="padding-left: 24px;">Job Owner:&nbsp;&nbsp;
						</td>
						
							<td class="textwhite"><html:select property = "newSearchOwner" size = "1" styleClass="combowhite">
														<html:optionsCollection name = "dyComboSearchOwner" property = "allOwnerName" value = "value" label = "label"/>    
													</html:select>&nbsp;&nbsp;
							</td>
							<td class="textwhite">Scheduler:&nbsp;&nbsp;
							</td>
							<td class="textwhite"><html:select property = "newSearchScheduler" size = "1" styleClass="combowhite">
														<html:optionsCollection name = "dyComboSearchScheduler" property = "schedulerList" value = "value" label = "label"/>    
													</html:select>&nbsp;&nbsp;
							</td>
						<td class="textwhite">Status:&nbsp;
						<td class="textwhite">
							<html:select property = "jobSelectStatus" size = "1" styleClass="combowhite">
													<html:optionsCollection name = "dcStatusList" property = "statusList" value = "value" label = "label"/>    
							</html:select>
						</td>
					</tr>
					<tr>
					<td height="15"></td>
					</tr>
					<tr>
						<td class="textwhite" colspan="6" style="padding-left: 12px;"><b>Site Information:
						</td>
					</tr>
					<tr>
						<td colspan="6" height="4"></td>
					</tr>
					<tr>
						<td class="textwhite" style="padding-left: 23px;">City:&nbsp;&nbsp;
						</td>
						<td class="textwhite"><html:text property = "city" styleClass = "textbox" size = "20"/>&nbsp;&nbsp;</td>
						<td class="textwhite" style="padding-left: 20px;">State:&nbsp;&nbsp;
						</td>
						<td class="textwhite">
							<logic:present name="initialStateCategory">
								<html:select property = "stateCmb"  styleClass="combowhite">
									<logic:iterate id="initialStateCategory" name="initialStateCategory" >
										<bean:define id="statecatname" name="initialStateCategory" property="countryName" />		   
											<%  if(!statecatname.equals("notShow")) { %>
												<optgroup class=labelboldwhite label="<%=statecatname%>">
											<% } %>
												<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
											<%  if(!statecatname.equals("notShow")) { %>	
												</optgroup> 
											<% } %>
									</logic:iterate>
								</html:select>
							</logic:present>
						</td>
						<td class="textwhite" style="padding-left: 20px;">Site Number:&nbsp;&nbsp;
						</td>
						<td class="textwhite"><html:text property = "siteNumber" styleClass = "textbox" size = "20"/>&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td class="textwhite" style="padding-left: 23px;">End Customer:&nbsp;&nbsp;
						</td>
						<td class = "textwhite"><html:select property="jobEndCustomer" size="1" styleClass="combowhite">
									 <html:optionsCollection name = "dcEndCustomerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>
						</td>
						<td class="textwhite" style="padding-left: 20px;">Brand:&nbsp;&nbsp;
						</td>
						<td colspan="3" align="left" class = "textwhite"><html:select  property="brand" size="1" styleClass="combowhite">
									 <html:optionsCollection name = "dcGroupList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
					<td height="10"></td>
					</tr>
					<tr >
						<td class="textwhite" >
							<html:submit property = "search" styleClass = "button" onclick ="return JobSearch();">Search</html:submit>
							<html:reset property = "resetbutton" styleClass = "button">Reset</html:reset>
						</td>
					</tr>
					<tr>
						<td colsapn="6" style="border-bottom-width: 5px;" height="10">
						</td>
					</tr>
			</td>
		</tr>
	</table>
</logic:present>
	<!-- Tabel Search for jobs is ended. -->
	<!-- Tabel Search result is started -->
	<logic:present name ="jobList" scope="request">
		<table>
			<tr>
				<td style="padding-left: 3px;">
					<table border="0" cellpadding="0" cellspacing="0" width="800" style="border-bottom: 1px solid black;">
						<tr>
							<td></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table border ="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="10">
				</td>
			</tr>
			<tr>
				<td style="padding-left=10px;">
					<table border="0" cellpadding="0" cellspacing="1">
						<tr>
							<td colspan="7" class="textwhite" ><b>Select Jobs to Update
							</td>
						</tr>
						<tr>
							<td height="10">
							</td>
						</tr>
						<tr> 
							<td class = "Ntrybgray" height="20">&nbsp;</td>
							<td class = "Ntrybgray" align = "center">Job Name</td>
							<td class = "Ntrybgray" align = "center">Site Number</td>
							<td class = "Ntrybgray" align = "center">Status</td>
							<td class = "Ntrybgray" align = "center">Job Owner</td>
							<td class = "Ntrybgray" align = "center">Scheduler</td>
							<td class = "Ntrybgray" align = "center">Planned Start<br> Date</td>
						</tr>
						<%
									  String bgcolor = "";
									  String bgcolor1 = "";
									  String readonlynumberclass = "";
									  String label = "";
									  boolean csschooser = true;
						%>
						<logic:iterate id = "jobList" name = "jobList" scope = "request">
						<% 
									if ( csschooser == true ) 
									{
										bgcolor = "hypereven";
										bgcolor1 = "hyperevencenter";
										readonlynumberclass = "readonlytextnumbereven";
										label = "labele";
										csschooser = false;
									} 
									else
									{
										bgcolor = "hyperodd";
										bgcolor1 = "hyperoddcenter";
										readonlynumberclass = "readonlytextnumberodd";
										csschooser = true;
										label = "labelo";
									}
		
						%>
						<tr>
							<td class = "<%= bgcolor1 %>" >
									    <html:multibox property = "jobIdCheck">
												<bean:write name = "jobList" property = "jobId"/>
									    </html:multibox>
							</td> 
							<td class = "<%= bgcolor %>" >
										<bean:write name = "jobList" property = "jobName"/>&nbsp;
							</td>
							<td class = "<%= bgcolor %>" >
										<bean:write name = "jobList" property = "jobSiteNumber"/>&nbsp;
							</td>
							<td class = "<%= bgcolor1 %>" >
										<bean:write name = "jobList" property = "jobStatus"/>&nbsp;
							</td>
							<td class = "<%= bgcolor1 %>" >
										<bean:write name = "jobList" property = "ownerJob"/>&nbsp;
							</td>
							<td class = "<%= bgcolor1 %>" >
										<bean:write name = "jobList" property = "jobScheduler"/>&nbsp;
							</td>
							<td class = "<%= bgcolor1 %>" >
										<bean:write name = "jobList" property = "resultPlannedStart"/>
							</td>
						</tr>
						</logic:iterate>
						<tr>
						<td height="5"></td>
						</tr>
						<tr>
							<td colspan="7" style="padding-left: 20px;">
								<html:submit property = "update" styleClass = "button" onclick ="return Update();">Update</html:submit>&nbsp;
								<html:reset property = "reset" styleClass = "button">Reset</html:reset>&nbsp;
								<html:button property = "cancel" styleClass = "button" onclick ="Checkall();">Checkall</html:button>&nbsp;
								<html:button property = "cancel" styleClass = "button" onclick ="unCheckall();">Uncheckall</html:button>&nbsp;
								
							</td>
						</tr>
					</table>
				</td> 
			</tr>
		</tabel>
	</logic:present>
	<logic:present name="NoJobs" scope="request">
		<table>
			<tr>
				<td style="padding-left: 3px;">
			
						<table border="0" cellpadding="0" cellspacing="0" width="800" style="border-bottom: 1px solid black;">
							<tr>
								<td></td>
							</tr>
						</table>
				</td>
			</tr>
		</table>
		<table>
			<tr>
					<td class="message"><b>No jobs found for this Criteria.
					</td>
			</tr>
		</table>
	</logic:present>
</html:form>

<script>
function viewChange() { //v3.0
		//alert('33333333');
	var tableRow = document.getElementsByTagName( 'div' );
	for (var k = 0; k < tableRow.length; k++) 
	{
	 	if(!(tableRow[k].getAttributeNode('id').value == '' || tableRow[k].getAttributeNode('id').value == 'menunav' || tableRow[k].getAttributeNode('id').value == 'div_sitesearch' || tableRow[k].getAttributeNode('id').value == 'div_savebutton' || tableRow[k].getAttributeNode('id').value == 'featured' || tableRow[k].getAttributeNode('id').value == 'featured1' || tableRow[k].getAttributeNode('id').value == 'filter' ) )
	 		MM_showHideLayers(tableRow[k].getAttributeNode( 'id' ).value , '' , 'hide' );
	}	
	
	var args = viewChange.arguments;
	
	MM_showHideLayers( args[0] , '' , 'show' );
	 leftAdjLayers();
	return true;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
 
  for (i=0; i<(args.length-2); i+=3)
   	if ((obj=MM_findObj(args[i]))!=null) 
  	{ 
  		v=args[i+2];
	    if (obj.style) 
	    { 
	    	obj=obj.style; 
	    	v=(v=='show')?'visible':(v='hide')?'hidden':v;
	    }
    	obj.visibility=v;
    }
  leftAdjLayers();
}
</script>
<script>

function JobSearch()
{
	//document.forms[0].action = "ChangeJOSAction.do?Appendix_Id=<%=Appendix_Id%>";
	//document.forms[0].submit();
}
function Checkall()
{
	var i=0;
	if(<%= checkSize %> > 0) {
		if(<%= checkSize %> == 1) {
			document.forms[0].jobIdCheck.checked=true;
		} else {
			for(i=0;i<<%=checkSize%>;i++) {
				document.forms[0].jobIdCheck[i].checked=true;
			}
		}
	}
	return true;
}
function unCheckall()
{
	var i=0;
	if(<%= checkSize %> > 0) {
		if(<%= checkSize %> == 1) {
			document.forms[0].jobIdCheck.checked=false;
		} else {
			for(i=0;i<<%=checkSize%>;i++) {
				document.forms[0].jobIdCheck[i].checked=false;
			}
		}
	}
	return true;
}

function Update() 
{
if((document.forms[0].newScheduler.value == '%' || document.forms[0].newScheduler.value == '') && (document.forms[0].newOwner.value == '%' || document.forms[0].newOwner.value == '') && (document.forms[0].planedStartDate.value == '')) {
	alert('No update criteria have been selected.\n Please review Job Owner, Scheduler or Planned Start Date.');
	return false;
}
else {
var check=false;
	if(<%= checkSize %> > 0) {
		if(<%= checkSize %> == 1) {
			if(document.forms[0].jobIdCheck.checked==true) {
			 	check=true;
			}
		}
		else {
			for(i=0;i<<%=checkSize%>;i++) {
				if(document.forms[0].jobIdCheck[i].checked==true) {
					check=true;
					break;
				}
			}
		}
		if(check == false) {
			alert('Please Select a Job to Update.');
			return false;
		}
	}
}
	
		//document.forms[0].action = "ChangeJOSAction.do?Appendix_Id=<%=Appendix_Id%>";
		//document.forms[0].submit();
}
</script>
<script>
/* Function for view selector: Start */
function changeFilter()
{
	document.forms[0].action="AssignTeam.do?function=View&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="AssignTeam.do?function=View&resetList=something";
	document.forms[0].submit();
	return true;

}
/* Function for view selector: End */
</script>
</body>
</html:html>