<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<html:html>
	<head>
		<title></title>
		<%@ include file = "../Header.inc" %>
		<%@ include file = "/NMenu.inc" %> 
		<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
		<link rel = "stylesheet" href="styles/content.css" type="text/css">
		<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
		<script language="JavaScript" src="javascript/popcalendar.js"></script>
		<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
		<script language="JavaScript" src="javascript/jquery-1.11.0.min.js"></script>
</head>

<%
	boolean csschooser = true;
	String Appendix_Id = "";
	String fromProposalMg = "No";
	String prevdivcheckid = "";
	int activity_size = ( int ) Integer.parseInt( request.getAttribute( "activityListSize" ).toString() ); 
	int div = 1;
	int i=0;
	int sitesSize = ( int ) Integer.parseInt( request.getAttribute( "sitesSize" ).toString() ); 
	int firstdivrowcount = 220;

	String isBukJobCreationView = ( String )request.getAttribute( "isBukJobCreationView" );
	
	if(request.getAttribute( "Appendix_Id" ) != null) 
	{
		Appendix_Id = request.getAttribute( "Appendix_Id" ).toString();
		request.setAttribute( "Appendix_Id" , Appendix_Id );
	}	
	if(request.getAttribute( "appendixid" ) != null) 
	{
		Appendix_Id = request.getAttribute( "appendixid" ).toString();
		request.setAttribute( "appendixid" , Appendix_Id );
	}

	if(request.getAttribute( "fromProposalMg" ) != null )
	{
		fromProposalMg = (String)request.getAttribute( "fromProposalMg" );
		request.setAttribute( "fromProposalMg" , "Yes" );
	}
%>

<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());

%>
	<logic:present name = "addendumjobnamelist" scope = "request">
		<% 
			firstdivrowcount = ( int ) Integer.parseInt( request.getAttribute( "firstdivrownum" ).toString() ) * 20 + 110;
		%>
	</logic:present>
	<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AjaxStateCountry.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "leftAdjLayers();return setoverheadcheck();" > 

<html:form action = "/BulkJobCreation" target = "ilexmain" enctype = "multipart/form-data">

<html:hidden property = "viewjobtype" /> 
<html:hidden property = "ownerId" /> 
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property ="state"/>
<html:hidden property ="country"/>

<%@ include  file="/AppendixDashboardMenuScript.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
			        	<td colspan="4">
			        		<h2><bean:message bundle = "PRM" key = "prm.BulkJobCreation.bulkJobCreation"/>&nbsp;<bean:message bundle = "PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name = "BulkJobCreationForm" property = "appendixname" /></h2>
			        	</td>
					</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="BulkJobCreationForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="BulkJobCreationForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="BulkJobCreationForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					           <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
<!-- Following is Outer Table -->
<table width = "100%" border = "0" cellspacing = "2" cellpadding = "1" align = "center" valign  = "top">  
<tr>
	<td width = "2" height = "0">
	</td>
	<td>
		<table border = "0" cellspacing = "1" cellpadding = "1" >     
			<tr>
				<td colspan = "7" class = "textwhite">
			    	<b><bean:message bundle = "PRM" key = "prm.jobname"/>:</b>&nbsp;&nbsp;
			        
			        <html:text property = "job_Name" styleClass = "textbox" size = "20"/>&nbsp;&nbsp;&nbsp;&nbsp;
			        	<bean:message bundle = "PRM" key = "prm.BulkJobCreation.use"/>&nbsp;&nbsp;&nbsp;
			        
			        <html:radio property = "site_name" value = "N" >
			        	<bean:message bundle = "PRM" key = "prm.BulkJobCreation.SiteName"/>
					</html:radio>&nbsp;
					
					<html:radio property = "site_name" value = "U">
						<bean:message bundle = "PRM" key = "prm.BulkJobCreation.SiteNumber"/>
					</html:radio>&nbsp;&nbsp;&nbsp;
					
					<bean:message bundle = "PRM" key = "prm.BulkJobCreation.as"/>
					<html:radio property = "prifix" value="P">
						<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Prefix"/>
					</html:radio>&nbsp;
								
					<html:radio property = "prifix" value = "S">
						<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Sufix"/>
					</html:radio>	
					
				</td>
			</tr>
			<tr>
			
				<td class = "textwhite" colspan = "7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<bean:message bundle="PRM" key="prm.BulkJobCreation.startDate"/>:&nbsp;&nbsp;
					<html:text  styleClass="textbox" size="10" property="startDate" readonly = "true"/>
				    <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].startDate, document.forms[0].startDate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
					&nbsp;&nbsp;&nbsp;
					<bean:message bundle="PRM" key="prm.BulkJobCreation.endDate"/>:&nbsp;&nbsp;
					<html:text  styleClass="textbox" size="10" property="endDate" readonly = "true"/>
				    <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].endDate, document.forms[0].endDate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
				</td>
			</tr>			
			
			<tr>
				<td>&nbsp;</td>
				<td colspan = "7" class = "textwhite"> 
					<logic:present name = "addendumjobnamelist" scope = "request">
						<logic:iterate id = "list" name = "addendumjobnamelist" scope = "request">
								<% i++; %>
								<a href = "#"  onclick = "viewChange2( 'div_<%= i %>' );" class = "tabtext">
									<bean:write name = "list" property = "addendumjobname"/>
								</a>
						</logic:iterate>
					</logic:present>
				</td>  
			</tr>
 
			<tr>
				<td>
				</td>
			</tr>
			<tr>
				<td width = "2" height = "0">
				</td>
			     	 
				<td>			
		          <!--   Started For Iterate Tag -->
		        
						<%
							  String bgcolor = "";
							  String bgcolor1 = "";
							  String readonlynumberclass = "";
							  String label = "";
						%>          
						<logic:iterate id = "activ" name = "activityList" scope = "request">		
						<% 
							if ( csschooser == true ) 
							{
								bgcolor = "hypereven";
								bgcolor1 = "hypereven";
								readonlynumberclass = "readonlytextnumbereven";
								label = "labele";
								csschooser = false;
							} 
							else
							{
								bgcolor = "hyperodd";
								bgcolor1 = "hyperodd";
								readonlynumberclass = "readonlytextnumberodd";
								csschooser = true;
								label = "labelo";
							}

						%>
						<bean:define id = "divcheckid" name = "activ" property = "tempjob_id" type = "java.lang.String"/>
						<% 
							if( !prevdivcheckid.equals( divcheckid ) )
							{	
								if( prevdivcheckid != "" )
								{
						%>	
								</table></td></tr>
						<%
								} 
								if (div == 1)
								{
						%>
								<div id = "div_<%= div %>" style = "display: block;" class="viewdiv">
						<%
								}
								else
								{ 
						%>
								<tr><td width = "2" height = "0"></td><td><div id = "div_<%= div %>" style = "display: none;" class="viewdiv">
						<%
								} 
						%>
						<table width = "100%">	
								<tr>
									<td>&nbsp;</td>
									<td class = "textwhitetop" colspan = "12">
										<b><bean:write name = "activ" property = "tempjob_name"/>&nbsp;Activities
									</td>
								</tr>
								
								<tr height = "20">
									<td>&nbsp;</td>
									<td class = "tryb" align = "center" width = "25%"> 
										<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Activity"/>
									</td>
				          			
				          			<td class = "tryb" width = "15%">
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Type"/>
				          			</td>
				          			
				          			<td class = "tryb" width = "5%">
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Qty"/>
				          			</td>
				          			
				          			<td class = "tryb" align = "center" width = "15%">
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Estimated"/><br>
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.totalCost"/>
				          			</td>
				          			
				          			<td class = "tryb" width = "10%">
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Extended"/><br>
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.SubTotal"/>
				          			</td>
				          			
				          			<td class = "tryb" width = "15%">
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Extended"/><br>
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Price"/>
				          			</td>
				          			
				          			<td class = "tryb" align = "center" width = "25%">
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.ProForma"/><br>
				          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Margin"/>
				          			</td>
								</tr>
						<%  
							div++; } 
						%>
					
						<tr height = "20">
							<td>
							    <html:multibox property = "activity_id">
										<bean:write name = "activ" property = "activity_id"/>
							    </html:multibox>
							</td> 
							
							<td class = "<%= bgcolor1 %>">
								<bean:write name = "activ" property = "activity"/>
							</td>	
							
							<td class = "<%= bgcolor1 %>">
								<bean:write name = "activ" property = "type"/>
							</td>
							
							<td class = "<%= readonlynumberclass %>">
								<bean:write name = "activ" property = "qty"/>
							</td>				  
							
							<td class = "<%= readonlynumberclass %>">
								<bean:write name = "activ" property = "estimated_total_cost"/>
							</td>	
							
							<td class = "<%= readonlynumberclass %>">
								<bean:write name = "activ" property = "extended_sub_total"/>
							</td>
							
							<td class = "<%= readonlynumberclass %>">
								<bean:write name = "activ" property = "extended_Price"/>
							</td>					
							
							<td class = "<%= readonlynumberclass %>">
								<bean:write name = "activ" property = "proforma_Margin"/>
							</td>									
			 		 				  
			 			</tr>
					 		<html:hidden name = "activ" property = "type" />
					 		<html:hidden name = "activ" property = "libraryId" />
					 		<html:hidden property = "fromProposalMg" value = "<%= fromProposalMg %>"/>
				 		<% 
							prevdivcheckid = divcheckid; 
						%>  
				 		</logic:iterate>
		     			</table></div>
		 		 
				</td>
			</tr>
			<tr height="15"><td colspan="2">&nbsp;</td></tr>
  			<tr>
	  			<td width = "2" height = "0">
			    </td>
				<td>
				
				<div id = "div_sitesearch" style = "visibility:visible;POSITION:relative;top:<%= firstdivrowcount %>;left:2;">
			 		<table> 
				     		<tr>
				               <td colspan = "7" class = "labeleboldwhite" height = "30">
				               		<bean:message bundle = "PRM" key = "prm.BulkJobCreation.SiteSearch"/>
				               </td>
							</tr>
					
							<tr>
						    	<td class = "textwhite"><bean:message bundle = "PRM" key = "prm.BulkJobCreation.city"/></td>
						     	<td class = "textwhite"><html:text property = "city" styleClass = "textbox"/></td>
						     	<td class = "textwhite"><bean:message bundle = "PRM" key = "prm.BulkJobCreation.State"/></td>
						<td class = "textwhite">	 		
						<logic:present name="initialStateCategory">
							<html:select property = "stateSelect"  styleClass="combowhite"  onchange = "ChangeCountry();">
								<logic:iterate id="initialStateCategory" name="initialStateCategory" >
									<bean:define id="statecatname" name="initialStateCategory" property="countryName" />		   
										<%  if(!statecatname.equals("notShow")) { %>
											<optgroup class=labelboldwhite label="<%=statecatname%>">
										<% } %>
											<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
										<%  if(!statecatname.equals("notShow")) { %>	
											</optgroup> 
										<% } %>
								</logic:iterate>
							</html:select>
						</logic:present></td>					 		
						<td class = "textwhite"><bean:message bundle = "PRM" key = "prm.BulkJobCreation.SiteNumber"/>  :</td>
						<td class = "textwhite"><html:text property = "site_Number" styleClass = "textbox" size = "20"/> </td>
						</tr>
								
						<tr>
							<td class = "textwhite"><bean:message bundle = "PRM" key = "prm.bulk.endcustomer" /> :</td>
							<td class = "textwhite"><html:select property="bulkJobEndCustomer" size="1" styleClass="combowhite">
									 <html:optionsCollection name = "dcEndCustomerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							
							<td class = "textwhite"><bean:message bundle = "PRM" key = "prm.bulk.brand" /> : </td>
							<td class = "textwhite"><html:select  property="brand" size="1" styleClass="combowhite">
									 <html:optionsCollection name = "dcGroupList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>&nbsp;&nbsp;</td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								
							<td class = "textwhite" colspan = "2"><html:button property = "sort" styleClass = "button" onclick = "siteSearch();">
						
									<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Go"/>
								</html:button></td>
						
						</tr>
						
				     
								<%if( sitesSize == 0 ) {
									if( isBukJobCreationView.equalsIgnoreCase( "B" ) ) { %>
								   <tr>
									<td class = "message" height = "30" colspan = "6">
								   		<bean:message bundle = "PRM" key = "prm.BulkJobCreation.NoSiteAvailableForBulkJob"/>
								   	</td>
	  				     		</tr>
							<% } } %>
				    <tr>
				    <td colspan = "6">
				    	<table border = "0">
				     		<tr>
				    			<td colspan = "6" class = "labeleboldwhite" height = "30">
				    				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.SiteSearchResults"/> 
				    			</td>
				      		</tr>
				      
				      		<tr>
					      		<td colspan = "5" class = "labelgraybold" height = "25"> 
					  				<bean:message bundle = "PRM" key = "prm.UploadCSV.SelectSites"/>
					  			</td>
							</tr> 
				      
					      	<tr>
								<td align = "center" class = "buttonrow" colspan = "0" width = "2%">&nbsp;
								</td>
								<td align = "center" class = "buttonrow" colspan ="2">
									<html:select property = "sortparam" size = "10" multiple = "true" styleClass = "sortmax" >
										<html:optionsCollection name = "BulkJobCreationForm" property = "sitedetaillist" value = "value" label = "label"/> 
								 	</html:select>
								</td>
				
								<td colspan = "0" align = "center" class = "buttonrow">
								<br>
									<html:button property = "singleforward" styleClass = "button" onclick = "return CopySelected();"><bean:message bundle = "pm" key = "msa.sort.singleforward"/></html:button><br>
									<html:button property = "doubleforward" styleClass = "button" onclick = "return CopyAll();"><bean:message bundle = "pm" key = "msa.sort.doubleforward"/></html:button><br><br>
									<html:button property = "singlebackward" styleClass = "button" onclick = "return SendSelected();"><bean:message bundle = "pm" key = "msa.sort.singlebackward"/></html:button><br>
									<html:button property = "doublebackward" styleClass = "button" onclick = "return SendAll();"><bean:message bundle = "pm" key = "msa.sort.doublebackward"/></html:button><br>
				    			</td>
								&nbsp;&nbsp;&nbsp;
									
								<td   align = "center" class = "buttonrow">
									<html:select property = "selectedparam" size = "10" multiple = "true" styleClass = "sortmax">
									</html:select>
								</td>
								<td width="7%" >&nbsp;</td>
							</tr>
							
							</table>
							</td>
						</tr>	
						</table>
    				</div>	
							
							
							<html:hidden property = "id" />
							<html:hidden property = "id1" />
							<html:hidden property = "ref" />
							<html:hidden property = "addendum_id" />
							<html:hidden property = "appendix_Id" />
		 
		  			<!-- for filter sorting -->
					<html:hidden property = "currentstatus" />
		  			<!-- for filter sorting -->
	  

					<tr>
						<td width = "2" height = "0">
						</td>
						<td> 
							<div id = "div_savebutton" style = "visibility:visible;POSITION:relative;top:<%= firstdivrowcount %>;left:2;">
								<table>
				  					
				  					<tr> 
				   		   				<td colspan = "3"> 
									  		<html:submit property = "sort" styleClass = "button" onclick = "return refresh();">
									  			<bean:message bundle = "PRM" key = "prm.add.save"/>
									  		</html:submit>
									  		<html:reset property = "cancel" styleClass = "button">
									  			<bean:message bundle = "PRM" key = "prm.reset"/>
									  		</html:reset>
										 	<%
										 		if( fromProposalMg.equalsIgnoreCase( "Yes" ) )
											 	{ 
											%>
												<html:button property = "back" styleClass = "button" onclick = "javascript:history.go(-1);">
													<bean:message bundle="PRM" key="prm.button.back"/>
												</html:button>
											<%
												}
												else
												{ 
											%>
										 			<html:button property = "back" styleClass = "button"  onclick = "return Backaction();">
										 				<bean:message bundle = "PRM" key = "prm.back"/>
										 			</html:button>
											<%
												} 
											%>
											<html:button property="sort1" styleClass="button" onclick = "return sortwindow();">
												<bean:message bundle="PRM" key="prm.netmedx.sort"/>
											</html:button>
						  				</td>
									</tr>
								</table>
							</div>
    					</td>
    				</tr>
			</table>  <!--  End Of Third Table -->
 		</td>
 	</tr>
</table> <!-- Following is the end of outer table -->


</html:form>
<!-- First Table -->

<script>
function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filter").style.visibility="visible";
<%}else{%>
document.getElementById("filter").style.visibility="hidden";
<%}%>
}

function leftAdjLayers(){
try{

		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;
		
		<%if(request.getAttribute("opendiv") != null && !request.getAttribute("opendiv").equals("")){%>
			document.getElementById("filter").style.visibility="visible";
		<%}else{%>
			document.getElementById("filter").style.visibility="hidden";
		<%}%>
		
		
	}
	catch(e)
	{
	}	
		
}
<%--
<%
if(request.getAttribute( "JobSaved" ) != null) 
	{%>
		parent.ilexleft.location.reload();
	<%}
%>
--%>
</script>

<script>

function setoverheadcheck()
{	
	//alert('111111111111');
    if(<%= activity_size %> >0)
    {	
	for( var i = 0; i < document.forms[0].activity_id.length;i++ )
	{	
		if( document.forms[0].type[i].value == 'Overhead' )
		{
			document.forms[0].activity_id[i].checked = true;
			//Following is added for disabling overhead
			document.forms[0].activity_id[i].disabled = true;
    		//Above is added for disabling overhead
		}
	}
	}
	defaultAddendumdisplay();
	return true;
}


function defaultAddendumdisplay() { //v3.0
	//alert('222222222222');
	var tableRow = document.getElementsByTagName('div');
	
	if (tableRow.length==0) 
	{ 
		return; 
	}
	
	for (var k = tableRow.length-1; k >-1; k--) 
	{
		viewChange2(tableRow[k].getAttributeNode( 'id' ).value );
	}
	
	viewChange2('div_1' );
	
	return true;
}

function viewChange2(id) {
	if (typeof id !== 'undefined') {
		$('.viewdiv').hide();
		$('#' + id).show();
	}
}	
	
function viewChange() { //v3.0
		//alert('33333333');
	var tableRow = document.getElementsByTagName( 'div' );
	for (var k = 0; k < tableRow.length; k++) 
	{
	 	if(!(tableRow[k].getAttributeNode('id').value == '' || tableRow[k].getAttributeNode('id').value == 'menunav' || tableRow[k].getAttributeNode('id').value == 'div_sitesearch' || tableRow[k].getAttributeNode('id').value == 'div_savebutton' || tableRow[k].getAttributeNode('id').value == 'featured' || tableRow[k].getAttributeNode('id').value == 'featured1' || tableRow[k].getAttributeNode('id').value == 'filter' ) )
	 		MM_showHideLayers(tableRow[k].getAttributeNode( 'id' ).value , '' , 'hide' );
	}	
	
	var args = viewChange.arguments;
	
	MM_showHideLayers( args[0] , '' , 'show' );
	 leftAdjLayers();
	return true;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
 
  for (i=0; i<(args.length-2); i+=3)
   	if ((obj=MM_findObj(args[i]))!=null) 
  	{ 
  		v=args[i+2];
	    if (obj.style) 
	    { 
	    	obj=obj.style; 
	    	v=(v=='show')?'visible':(v='hide')?'hidden':v;
	    }
    	obj.visibility=v;
    }
  leftAdjLayers();
}

function adjustpos( count )
{
	div_sitesearch.style.pixelTop = count * 20+90;
    div_savebutton.style.pixelTop = count * 20+90;
    return true;
}





function refresh() 
{

	var reqflag = 'false';
	//Following Is for Checking  if There is no activity for any Appendix Name
	if(<%= activity_size %> ==0)
	   {
	      alert("<bean:message bundle = "PRM" key = "prm.BulkJobCreation.warningIfThereIsNoActivity"/>");
	      return false;
	   }	
   //Above Is for Checking  if There is no activity for any Appendix Name   
  if(document.forms[0].job_Name.value=='')
   {
       document.forms[0].job_Name.focus();
       alert("<bean:message bundle = "PRM" key = "prm.BulkJobCreation.warningPleaseEnterJobName"/>");
      return false;
   }


	if(Date.parse(document.forms[0].startDate.value)>Date.parse(document.forms[0].endDate.value))
		{
			alert("<bean:message bundle = "PRM" key = "prm.BulkJobCreation.warningEndDateLessThanStart"/>");
  			return false;
  		}

	if( document.forms[0].selectedparam.length == 0 )
	{
		alert( "<bean:message bundle = "PRM" key = "prm.BulkJobCreation.warning"/>" );  
		return false;
	}
	
	//Start: This is for Check that at least one Required Activity should be checked  
	
    if(<%= activity_size %>  > 0)
    {
		/*if( document.forms[0].activity_id.length>0)
		{
			for( var i = 0; i < document.forms[0].activity_id.length;i++ )
			{
				if( document.forms[0].type[i].value == "Required" )
				{
					if( document.forms[0].activity_id[i].checked )
					{
					    reqflag = 'true';
					}
				}
			}
			
			if( reqflag == 'false' )
			{
			    alert( "<bean:message bundle = "PRM" key = "prm.BulkJobCreation.selectAtLeastOneRequiredActivity"/>" );  
				return false;
			}
		}
		else
		{
		     if( document.forms[0].type[i].value == "Required" )
			 {
				if( document.forms[0].activity_id[i].checked )
					{
					    reqflag = 'true';
					}
				if( reqflag == 'false' )
		     	{
			    	alert( "<bean:message bundle = "PRM" key = "prm.BulkJobCreation.selectAtLeastOneRequiredActivity"/>" );  
			     	return false;
			    }	
			}
		}*/
	}
	//End: This is for Check that at least one Required Activity should be checked	
	
	for( var i = 0 ; i < document.forms[0].selectedparam.length ; i++ )
	{
		document.forms[0].selectedparam.options[i].selected = true;
	}
	
	var i = document.forms[0].sortparam.length;
	
	if( i > 0 )
	{
		
		while( i > 0 )
		{
			if( document.forms[0].sortparam.options[i-1].selected )
			{
				document.forms[0].sortparam.options.remove( i -1 );
				//return false;
			}
			i--;
		}
	}
	
	//Following is for enabling overHead during save
	for( var i = 0; i < document.forms[0].activity_id.length;i++ )
	{	
		if( document.forms[0].type[i].value == 'Overhead' )
		{
			document.forms[0].activity_id[i].disabled = false;
		}
	}
	//Above is for enabling overhead
	document.forms[0].action = "BulkJobCreation.do?ref=saveSiteSearch";
	//document.forms[0].submit();
	return true;
}
function siteSearch()
{
	for(var i=0; i < document.forms[0].sortparam.length; i++)
	{
		document.forms[0].sortparam[i].selected = false;
	}
	document.forms[0].action = "BulkJobCreation.do?ref=siteSearch";
	document.forms[0].submit();
	return true;
}

function remove()
{
	var temp = document.forms[0].selectedparam.length;	
	while( temp > 0 ) 
	{
		document.forms[0].selectedparam.options.remove(0);
		temp--;
	}	
}

function CopyAll() 
{
	var temp = document.forms[0].selectedparam.length;	
	for( var i = 0 ; i < document.forms[0].sortparam.length ; i++ )
	{
		document.forms[0].selectedparam.options.add( new Option ( document.forms[0].sortparam.options[i].text , document.forms[0].sortparam.options[i].value ) , temp );
		temp++;
	}//for
	
	var temp = document.forms[0].sortparam.length;	
	while( temp > 0 ) 
	{
		document.forms[0].sortparam.options.remove( 0 );
		temp--;
	}
	
	return true;
}

function CopySelected() 
{
	var temp = document.forms[0].sortparam.length;	
	for( var i = 0 ; i < document.forms[0].sortparam.length ; i++ )
	{
		if( document.forms[0].sortparam.options[i].selected ) 
		{
			document.forms[0].selectedparam.options.add( new Option( document.forms[0].sortparam.options[i].text , document.forms[0].sortparam.options[i].value ) , temp );
			temp++;
			
		}//if
	}//for
		
		
	var i = document.forms[0].sortparam.length;	
	while( i > 0 ) 
	{
		if( document.forms[0].sortparam.options[i-1].selected ) 
		{
			document.forms[0].sortparam.options.remove( i - 1 );
		}//if
		i--;
	}
	return true;
}//function

function SendAll() 
{
	var temp = document.forms[0].sortparam.length;	
	for( var i = 0; i < document.forms[0].selectedparam.length ; i++ )
	{
		document.forms[0].sortparam.options.add( new Option( document.forms[0].selectedparam.options[i].text,document.forms[0].selectedparam.options[i].value ) , temp );
		temp++;
	}//for
	
	var temp = document.forms[0].selectedparam.length;	
	while( temp > 0 ) 
	{
		document.forms[0].selectedparam.options.remove( 0 );
		temp--;
	}
	return true;
}


function SendSelected() 
{
	var temp = document.forms[0].sortparam.length;	
	for( var i = 0; i<document.forms[0].selectedparam.length; i++ )
	{
		if( document.forms[0].selectedparam.options[i].selected ) 
		{
				document.forms[0].sortparam.options.add( new Option( document.forms[0].selectedparam.options[i].text , document.forms[0].selectedparam.options[i].value) , temp );
				temp++;	
		}//if
	}//for
	
	var i = document.forms[0].selectedparam.length;	
	while( i > 0 ) 
	{
		if( document.forms[0].selectedparam.options[i-1].selected ) 
		{
			document.forms[0].selectedparam.options.remove( i-1 );
		}//if
		i--;
	}//while
	
	return true;
}//function
function Backaction()
{
	document.forms[0].action = "AppendixHeader.do?function=view&appendixid=<bean:write name = "BulkJobCreationForm" property = "appendix_Id" />&viewjobtype=<bean:write name = "BulkJobCreationForm" property = "viewjobtype" />&ownerId=<bean:write name = "BulkJobCreationForm" property = "ownerId" />";
	document.forms[0].submit();
	return true;
}

function sortwindow() {
	if(<%=sitesSize%> == 0) {
		alert('Please search sites before sorting.');
	}	
	else {
		str = 'SortAction.do?Type=bulkJobCreation&appendixid=<bean:write name = "BulkJobCreationForm" property = "appendix_Id" />&viewjobtype=<bean:write name = "BulkJobCreationForm" property = "viewjobtype" />&ownerId=<bean:write name = "BulkJobCreationForm" property = "ownerId" />&sitenumber='+document.forms[0].site_Number.value+'&sitecity='+document.forms[0].city.value+'&stateid='+document.forms[0].state.value;
		p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
		/* p = showModelessDialog( str , window, 'status:false;dialogWidth:420px; dialogHeight:360px' );  */
		return true;
	}	
}

</script>
<script>
function changeFilter()
{
	document.forms[0].action="BulkJobCreation.do?ref=bukJobCreationView&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="BulkJobCreation.do?ref=bukJobCreationView&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = <%=Appendix_Id%>;
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>

</body>
</html:html>

