<!DOCTYPE HTML>
<%@ page import = "com.mind.common.*" %>
<%@ page import = "com.mind.common.dao.*" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id = "currState" name = "currState" scope = "request"/>
<bean:define id = "expansionState" name = "expansionState" scope = "request"/>
<bean:define id = "menustring" name = "menustring" scope = "request"/>

<bean:define id = "element" name = "element" scope = "request"/>
<bean:define id = "controller" name = "controller" scope = "request"/>

<%
String viewjobtype ="";
String ownerId = "0";

if(session.getAttribute("ownerId") != null)
	ownerId = (String)session.getAttribute("ownerId");
if(session.getAttribute("viewjobtype") != null)
	viewjobtype = (String)session.getAttribute("viewjobtype");
%>

<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>
<TITLE></TITLE>
</HEAD>

<title><bean:message bundle="RM" key="rm.hierarchynavigation"/></title>
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 10px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 0px;
    	white-space : nowrap;
    }

    /* Link Styles */
    a  {
    	color : #000000;
    }

    a:link  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:active  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:hover  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:visited  {
    	text-decoration : underline;
    	color : #000000;
    }
    
    .m2 {
        font-family : "Verdana";
        font-size : 10px;
        line-height: 10px;
    }    
</style>

<script language="JavaScript" src="javascript/ce_menu.js" type="text/javascript"></script>

<body onload="MM_preloadImages('images/rm_rsm_icon.gif','images/t_appendix.gif','images/t_job.gif','images/t_activity.gif');"  text="#000000" leftmargin="5" topmargin="0" marginwidth="0" marginheight="0" background="#F4F9FD">


<%
MenuElement me = ((MenuTree)(element)).getRootElement();
MenuController mc =((MenuTree)(element)).getMenuController();
//response.getWriter().print("<div style='visibility:hidden;' id='content1'><b>NetMedX:</b>&nbsp;&nbsp;<a href='TicketsPerMsaAction.do?ref=msaticketlist' target='ilexmain'>Summary</a>&nbsp;|&nbsp;<a href='AppendixHeader.do?function=view&appendixid=-1&nettype=dispatch&viewjobtype=&ownerId=0' target='ilexmain'>Dispatches</a></div>"); 
//response.getWriter().print("<div style='visibility:hidden;' id='content1'><b>NetMedX:</b>&nbsp;&nbsp;<a href='TicketsPerMsaAction.do?ref=msaticketlist' target='ilexmain'>Summary</a>&nbsp;|&nbsp;<a href='AppendixHeader.do?function=view&appendixid=-1&nettype=dispatch&viewjobtype="+viewjobtype+"&ownerId="+ownerId+"' target='ilexmain'>Dispatches</a></div>"); 
//response.getWriter().print("<div style='visibility:hidden;' id='content2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='NewWebTicketAction.do' target='ilexmain'>New Ticket</a></div>"); 
//response.getWriter().print("<div id='content3'>&nbsp;</div>" ); 
me.drawDirect(0,mc,response.getWriter());
%>
<script language="JavaScript">

/*
document.getElementById('content1').style.visibility = 'visible';
document.getElementById('content2').style.visibility = 'visible';  
document.getElementById('content3').style.visibility = 'visible'; */


document.getElementById('line0').className = 'm2';
document.getElementById('line0').style.visibility = 'visible';

currState="<%=((MenuTree)(element)).getCurrState()%>";
expansionState="<%= expansionState %>";
initNavigatorServer('PR');
</script>

</body>

</html:html>


