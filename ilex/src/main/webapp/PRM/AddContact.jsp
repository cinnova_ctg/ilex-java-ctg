<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id = "dcprm" name = "dynamiccombo" scope = "request"/>
<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>

<%
int addRow = -1;
boolean addSpace = false;
int rowHeight = 120;
int ownerListSize = 0;
int j=2;
String checkowner=null;

	String MSA_Id = "";

	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());
	
	if(request.getAttribute("MSA_Id")!=null)
	MSA_Id= request.getAttribute("MSA_Id").toString();
%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkstatus = null;
String status="";
/* String Appendix_Id="";
String appendixType=""; */

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());

String msastatus="";

/* String status =""; */

if(request.getAttribute("status")!=null){
	status = request.getAttribute("status").toString();
	msastatus = request.getAttribute("status").toString();
}



else if( request.getAttribute( "MSA_Id" ) != null)
	MSA_Id = (String) request.getAttribute( "MSA_Id" );

/* if( request.getParameter( "Appendix_Id" ) != null )
	Appendix_Id = request.getParameter( "Appendix_Id" );

else if( request.getAttribute( "Appendix_Id" ) != null)
	Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" );

if( request.getAttribute( "appendixType" ) != null)
	appendixType = (String) request.getAttribute( "appendixType" ); */

%>

<%@ include  file="/NMenu.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');leftAdjLayers();" >

<html:form action="AddContact" >
<html:hidden property="function" />
<html:hidden property="type" />
<html:hidden property="typeid" />
<html:hidden property="refresh" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "fromPage" />
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<html:hidden property="msaId"/> 
<html:hidden property = "name" />
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property="jobOwnerOtherCheck"/> 

<%if(request.getAttribute("viewflag").equals("0")) 
{%>
<!-- Menu/View selector:Start -->
<logic:equal name="AddContactForm" property="fromPage" value="Appendix">
	 <%@ include  file="/AppendixMenu.inc" %> 
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 	<tr>
    <td valign="top" width = "100%">
 <script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>     
    
      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
        <tr>
          <!-- Sub Navigation goes into this td -->
          
						<%-- <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>	
						
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td> --%>	
				<!--  New Menu Style  -->
				
			 <%-- <%@ include file = "/Menu_MSA_Appendix_Contact.inc" %> 	 --%>
<div id="menunav">
	<ul>
		<%
		if( !status.equals( "Draft" ) && !status.equals( "Review" )) {
			if(status.equals( "Cancelled" )) {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>
				<%
			} else {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>
				<%
			}
		} else {
			%>
			<li><a class="drop" href="AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>	
			<%
		}
		%>
		<li>
			<a href="#" class="drop"><span>Manage</span></a>
			<ul>
				<li>
					<%
					if((String) request.getAttribute("new_list") != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/></span></a>
						<ul>
							<%= (String) request.getAttribute("new_list") %>
						</ul>
						<%
					} else {
						%>
						<a href="#"><bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/></a>
						<%
					}
					%>
				</li>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.viewappendix"/></span></a>
					<ul>
						<li><a href="AppendixDetailAction.do?MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>">Details</a></li>
						<%
						if( status.equals( "Signed" ) || status.equals( "Approved" ) || status.equals( "Pending Customer Review" )) {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/></a></li>
							<li><a href="javascript:view('rtf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewrtf"/></a></li>
							<%
							if(request.getAttribute("new_addendumlist") != "" ) {
								%>
								<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
								<%
								if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) {
									%>
									<li>
										<a href="#"><span>History</span></a>
										<ul>
											<%= (String) request.getAttribute("new_addendumlist") %>
										</ul>
									</li>
									<li><a href="javascript:view('excel');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/></a></li>
									<%
								} else {
									%>
									<li>
										<a href="#"><span>History</span></a>
										<ul>
											<%= (String) request.getAttribute("new_addendumlist") %>
										</ul>
									</li>
									<%
								}
							} else {
								if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) {
									%>
									<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
									<li><a href="javascript:view('excel');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/></a></li>
									<%
								} else {
									%>
									<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
									<%
								}
							}
						}
						%>
					</ul>
				</li>
				<%
				if( !status.equals( "Draft" ) && !status.equals( "Cancelled" ) && !status.equals( "Review" )) {
					%>
					<li><a href="javascript:sendemail();"><bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert(appendixSend)"><bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/></a></li>
					<%
				}
				if( status.equals( "Signed" ) || status.equals( "Inwork" ) || status.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:setendcustomer();"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				}
// 				if( status.equals( "Signed" ) || status.equals( "Inwork" ) || status.equals( "Cancelled" )) {
					%>
					<%-- <li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li> --%>
					<%
// 				} else {
					%>
					<%-- <li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:setendcustomer();"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li> --%>
					<%
// 				}
				if(status.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:addcontact();"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				}
				%>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.comments"/></span></a>
					<ul>
						<li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "appendix.detail.menu.viewall"/></a></li>
						<%
						if(status.equals( "Cancelled" )) {
							%>
							<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						}
						%>
					</ul>
				</li>
				<%
				if( status.equals( "Pending Customer Review" )) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&entityType=Appendix"><bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert(appendixCRS)"><bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/></a></li>
					<%
				}
				if( !status.equals( "Draft" ) && !status.equals( "Review" )) {
					if(status.equals( "Cancelled" )) {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>
						<%
					} else {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>
						<%
					}
				} else {
					%>
					<li><a href="javascript:editappendix();"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>	
					<%
				}
				%>
				<li><a href="ESAEditAction.do?ref=view&appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>">External Sales Agent</a></li>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.menu.documents"/></span></a>
					<ul>
						<li><a href="EntityHistory.do?entityId=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&entityType=Appendix&function=viewHistory">Appendix History</a></li>
						<li>
							<a href="#"><span>Support Documents</span></a>
							<ul>
								<li><a href="EntityHistory.do?entityId=<%= Appendix_Id %>&appendixId=<%= Appendix_Id %>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Appendix">Upload</a></li>
								<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= Appendix_Id %>&linkLibName=Appendix&function=supplementHistory">History</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="javascript:del();"><bean:message bundle = "pm" key = "appendix.detail.menu.deleteappendix"/></a></li>
			</ul>
		</li>
		<li><a class="drop" href="JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>"><bean:message bundle = "pm" key = "appendix.detail.jobs"/></a></li>
	</ul>
</div>
	
			 			
          	<!-- End of Sub Navigation -->
          	
        </tr>
         <tr>
          <!-- BreadCrumb goes into this td -->
          <td background="images/content_head_04.jpg" height="21" colspan="23" width ="100%">
          				<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "AddContactForm" property = "msaId"/>"><bean:write name = "AddContactForm" property = "msaName"/></a></div>
          </td>
        </tr>
   		  <tr>    
						<td colspan = "5" height = "30" width = "800"><h2>
						 	<bean:message bundle="pm" key="common.projectmanger.title"/>:&nbsp;<bean:write name = "AddContactForm" property = "name"/>
						</h2></td>
					</tr>
      </table></td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
    <td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" conspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                      <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
	                			<div id="featured">
										<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
								</div>								
	                </div>		
				<span>
				<div id="filter" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
							    		<logic:present name ="statuslist" scope="request">
									    	<logic:iterate id = "list" name = "statuslist"> 
										 		<html:multibox property = "appendixSelectedStatus"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											</logic:iterate>
										</logic:present>
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
	    
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist"> 
										<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
										<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<%
										checkowner = "javascript: checking('"+ownerId+"');"; 
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 							    	
							    	
								 		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
									  <bean:write name ="olist" property = "ownerName"/>
									  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                         
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
                              
                            </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
</table>
<%@ include  file="/AppendixMenuScript.inc" %>
</logic:equal>

<logic:equal name="AddContactForm" property="fromPage" value="AppendixDashBoard">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>

	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr> 
			          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage</center></a></td>
			          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
			     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="AddContactForm" property="typeid"/>&ownerId=<bean:write name="AddContactForm" property="ownerId"/>&msaId=<bean:write name="AddContactForm" property="msaId"/>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
			          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr><td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.contact.addcontactfor"/>&nbsp;<bean:write name="AddContactForm" property="appendixname" /></h2></td></tr> 
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					          <span id = "OwnerSpanId"> 
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
<%@ include  file="/AppedixDashboardMenuScript.inc" %>
</logic:equal>

<logic:equal name = "AddContactForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr>
		        	<td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.contact.addcontactfor"/>&nbsp;<bean:write name="AddContactForm" property="appendixname" /></h2></td>
		        </tr> 
	      </table>
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        <tr>
	          <td width="155" height="39"  class="headerrow" colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" border="0">
		              <tr>
		                <td width="150">
		                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
			                   	<tr>
						            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
			                    </tr>
		                  	</table>
		                </td>
		              </tr>
		          </table>
	          </td>
	        </tr>
	        <tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>	
	                			<span>
									<div id="filter" class="divstyle">
					        			<table width="250" cellpadding="0" class="divtable">
						                     <tr>
					                            <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
															 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																		<bean:write name ="list" property = "statusid"/>
															 		</html:multibox>   
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="AddContactForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table></td>
		                      			    	<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
			                      			    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												          <span id = "OwnerSpanId"> 
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
								
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
															 			</html:multibox>   
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>											
						            </div>
				                </span>
							 </td>
	              		</tr>
            		</table>
            	</td>
        	</tr>
	    </table>
	</td>
 </tr>
</table>
<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
</logic:equal>

<!-- Menu/View selector:End -->

<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="200">
	  		<!-- for left tree refersh:start -->
    		<logic:present name = "clickShow" scope = "request">
		  		<logic:notEqual name = "clickShow" value = "">
					<script>
						parent.ilexleft.location.reload();
					</script>	
				</logic:notEqual>
			</logic:present> 	
			<logic:present name = "specialCase" scope = "request">
		  		<logic:notEqual name = "specialCase" value = "">
					<script>
						parent.ilexleft.location.reload();
					</script>	
				</logic:notEqual>
			</logic:present>
    		<!-- for left tree refersh:end -->
  			<logic:present name="addmessage">
			<logic:equal name="addmessage" value="-9001">
			<tr>
				<td  colspan="7" class="message" height="30" ><bean:message bundle="PRM" key="prm.contact.addfailed"/></td>
			</tr>
	 		</logic:equal>
	 		</logic:present>
			
	  		<!--<logic:notEqual name="AddContactForm" property="fromPage" value="Appendix">
	  		<tr>
	  			<td colspan="7" class="labeleboldwhite" height="30"><bean:message bundle="PRM" key="prm.contact.addcontactfor"/>&nbsp;<bean:write name="AddContactForm" property="appendixname" /></td>
	  		</tr>
	  		</logic:notEqual>-->
	  		<tr>
	  			
    			<td class = "formCaption" colspan = "7" height = "25">
    				<bean:message bundle="PRM" key="prm.contact.contingentcontacts"/>
    			</td>
			</tr>
  			
				<tr> 
	    			<td class = "Ntryb" rowspan = "2">&nbsp; </td>
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.name"/>
	    			</td>
	    			
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.phone1"/>
	    			</td>
	    
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.phone2"/>
	    			</td>
	    			
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.email1"/>
	    			</td>
		
					<td class = "Ntryb" rowspan = "2">
						<bean:message bundle="PRM" key="prm.contact.email2"/>
					</td>
		
					<td class = "Ntryb" rowspan = "2">
						<bean:message bundle="PRM" key="prm.contact.fax"/>
					</td>
							
				</tr>
				
			<tr>
				<td colspan="7">&nbsp; </td>
			</tr>
				
			
		    <html:hidden property="phone1"/>
		    <html:hidden property="phone2"/>
		    <html:hidden property="email1"/>
		    <html:hidden property="email2"/>
		    <html:hidden property="fax"/>
		    <html:hidden property="spocphone1"/>
		    <html:hidden property="spocphone2"/>
		    <html:hidden property="spocemail1"/>
		    <html:hidden property="spocemail2"/>
		    <html:hidden property="spocfax"/>
		    <html:hidden property="custphone1"/>
		    <html:hidden property="custphone2"/>
		    <html:hidden property="custemail1"/>
		    <html:hidden property="custemail2"/>
		    <html:hidden property="custfax"/>
		    <html:hidden property="businesspocphone1"/>
		    <html:hidden property="businesspocphone2"/>
		    <html:hidden property="businesspocemail1"/>
		    <html:hidden property="businesspocemail2"/>
		    <html:hidden property="businesspocfax"/>
		    <html:hidden property="contractpocphone1"/>
		    <html:hidden property="contractpocphone2"/>
		    <html:hidden property="contractpocemail1"/>
		    <html:hidden property="contractpocemail2"/>
		    <html:hidden property="contractpocfax"/>
		    <html:hidden property="billingpocphone1"/>
		    <html:hidden property="billingpocphone2"/>
		    <html:hidden property="billingpocemail1"/>
		    <html:hidden property="billingpocemail2"/>
		    <html:hidden property="billingpocfax"/>
		    
		    <tr> 
			    <td  class="Nhyperodd" ><bean:message bundle="PRM" key="prm.contact.sr.projectmgr"/></td>
			    <td class="Nreadonlytextodd" ><html:select property="cnspoc" size="1" styleClass="select" onchange = "return setValue();">
			    <html:optionsCollection name = "dcprm"  property = "cnspoclist"  label = "label"  value = "value" />
			    </html:select></td>
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"    property="phone1" /></td>
			    
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"  property="phone2" /></td>
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"  property="email1" /></td>
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"   property="email2" /></td>
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"  property="fax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhypereven"><bean:message bundle="PRM" key="prm.contact.businessDev"/></td>
			    <td class="Nreadonlytexteven"><html:select property="salespoc" size="1" styleClass="select" onchange = "return setValue();">
			    <html:optionsCollection name = "dcprm"  property = "salespoclist"  label = "label"  value = "value" />
			    </html:select></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="spocphone1" /></td>
			    
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="spocphone2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"   property="spocemail1" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="spocemail2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="spocfax" /></td>
		    </tr>
		    
		      <tr> 
			    <td  class="Nhyperodd" ><bean:message bundle="PRM" key="prm.contact.projectmgr"/></td>
			    <td class="Nreadonlytextodd" ><html:select property="cnsPrjManagerPoc" size="1" styleClass="select" onchange = "return setValue();">
			    <html:optionsCollection name = "dcprm"  property = "cnsPrjManagerList"  label = "label"  value = "value" />
			    </html:select></td>
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"    property="cnsPrjManagerPhone1" /></td>
			    
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"  property="cnsPrjManagerPhone2" /></td>
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"  property="cnsPrjManagerMail1" /></td>
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"   property="cnsPrjManagerMail2" /></td>
			    <td class="Nhyperodd" ><bean:write name="AddContactForm"  property="cnsPrjManagerFax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhypereven"><bean:message bundle="PRM" key="prm.contact.teamLead"/></td>
			    <td class="Nreadonlytexteven"><html:select property="teamLeadPoc" size="1" styleClass="select" onchange = "return setValue();">
			    <html:optionsCollection name = "dcprm"  property = "teamLeadList"  label = "label"  value = "value" />
			    </html:select></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="teamLeadPhone1" /></td>
			    
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="teamLeadPhone2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"   property="teamLeadMail1" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="teamLeadMail2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="teamLeadFax" /></td>
		    </tr>
		    
		    <tr><td height="5"></td></tr>
		    <tr>
    			<td class = "formCaption" colspan = "7" height = "25">
    				<bean:message bundle="PRM" key="prm.contact.customercontacts"/>
    			</td>
			</tr>
			
			</tr>
  	
				<tr> 
	    			<td class = "Ntryb" rowspan = "2">&nbsp; </td>
	    			<td class = "Ntryb" rowspan = "2" >
	    				<bean:message bundle="PRM" key="prm.contact.name"/>
	    			</td>
	    			
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.phone1"/>
	    			</td>
	    
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.phone2"/>
	    			</td>
	    			
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.email1"/>
	    			</td>
		
					<td class = "Ntryb" rowspan = "2">
						<bean:message bundle="PRM" key="prm.contact.email2"/>
					</td>
		
					<td class = "Ntryb" rowspan = "2">
						<bean:message bundle="PRM" key="prm.contact.fax"/>
					</td>
							
				</tr>
				
			<tr>
				<td colspan="7">&nbsp; </td>
			</tr>
				
			<tr> 
			    <td  class="Nhyperodd"><bean:message bundle="PRM" key="prm.contact.sr.projectmgr"/></td>
			    <td class="Nreadonlytextodd"><html:select property="custpoc" size="1" styleClass="select" onchange = "return setValue();">
				<html:optionsCollection name = "dcprm"  property = "custpoclist"  label = "label"  value = "value" />
				</html:select></td>
			    <td class="Nhyperodd"><bean:write  name="AddContactForm"  property="custphone1" /></td>
			    
			    <td class="Nhyperodd"><bean:write  name="AddContactForm"  property="custphone2" /></td>
			    <td class="Nhyperodd"><bean:write  name="AddContactForm"   property="custemail1" /></td>
			    <td class="Nhyperodd"><bean:write  name="AddContactForm"  property="custemail2" /></td>
			    <td class="Nhyperodd"><bean:write  name="AddContactForm"  property="custfax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhypereven"><bean:message bundle="PRM" key="prm.contact.businesspoc"/></td>
			    <td class="Nreadonlytexteven"><html:select property="businesspoc" size="1" styleClass="select" onchange = "return setValue();">
				<html:optionsCollection name = "dcprm"  property = "businesspoclist"  label = "label"  value = "value" />
				</html:select></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="businesspocphone1" /></td>
			    
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="businesspocphone2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"   property="businesspocemail1" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="businesspocemail2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="businesspocfax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhyperodd"><bean:message bundle="PRM" key="prm.contact.contractpoc"/></td>
			    <td class="Nreadonlytextodd"><html:select property="contractpoc" size="1" styleClass="select" onchange = "return setValue();">
				<html:optionsCollection name = "dcprm"  property = "contractpoclist"  label = "label"  value = "value" />
				</html:select></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm"  property="contractpocphone1" /></td>
			    
			    <td class="Nhyperodd"><bean:write name="AddContactForm"  property="contractpocphone2" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm"   property="contractpocemail1" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm"   property="contractpocemail2" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm"  property="contractpocfax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhypereven"><bean:message bundle="PRM" key="prm.contact.billingpoc"/></td>
			    <td class="Nreadonlytexteven"><html:select property="billingpoc" size="1" styleClass="select" onchange = "return setValue();">
				<html:optionsCollection name = "dcprm"  property = "billingpoclist"  label = "label"  value = "value" />
				</html:select></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="billingpocphone1" /></td>
			    
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="billingpocphone2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"   property="billingpocemail1" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"   property="billingpocemail2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm"  property="billingpocfax" /></td>
		    </tr>
	  
	 		 <tr> 
	 		 	  <td colspan="7" class="Nbuttonrow"> 
	 		      <html:submit property="save" styleClass="Nbutton" onclick = "return Validate();">
	 		      <bean:message bundle="PRM" key="prm.save" /></html:submit>
     			  <html:reset  property="reset" styleClass="Nbutton"><bean:message bundle="PRM" key="prm.reset" /></html:reset>
				   <logic:equal name="AddContactForm" property="fromPage" value="Appendix">
		 		      <html:button property="back" styleClass="Nbutton" onclick = "return Backaction('appendixdetail');"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
		 		  </logic:equal>
		 		  <logic:notEqual name="AddContactForm" property="fromPage" value="Appendix">
		 		      <html:button property="back" styleClass="Nbutton" onclick = "return Backaction('appendixdashboard');"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
		 		  </logic:notEqual>
				  </td>
			 </tr>
	  
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
<%}
else
{%>
<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="400">
	  		<logic:present name="addmessage">
			<logic:equal name="addmessage" value="0">
			<tr>
				<td  colspan="7" class="message" height="30" ><bean:message bundle="PRM" key="prm.contactaddsuccess"/></td>
			</tr>
	 		</logic:equal>
	 		</logic:present>
	  		
	  		<tr>
	  			<td colspan="7" class="labeleboldwhite" height="30"><bean:message bundle="PRM" key="prm.addcontactfor"/>&nbsp;<bean:write name="AddContactForm" property="appendixname" /></td>
	  		</tr>
	  		
	  		<tr>
	  			
    			<td class = "formCaption" colspan = "7" height = "25">
    				<bean:message bundle="PRM" key="prm.contact.contingentcontacts"/>
       			</td>
			</tr>
  	
				<tr> 
	    			<td class = "Ntryb" rowspan = "2">&nbsp; </td>
	    			<td class = "Ntryb" rowspan = "2" >
	    				<bean:message bundle="PRM" key="prm.contact.name"/>
	    			</td>
	    			
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.phone1"/>
	    			</td>
	    
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.phone2"/>
	    			</td>
	    			
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.email1"/>
	    			</td>
		
					<td class = "Ntryb" rowspan = "2">
						<bean:message bundle="PRM" key="prm.contact.email2"/>
					</td>
		
					<td class = "Ntryb" rowspan = "2">
						<bean:message bundle="PRM" key="prm.contact.fax"/>
					</td>
							
				</tr>
				
			<tr>
				<td colspan="7">&nbsp; </td>
			</tr>
				
			<tr> 
			    <td  class="Nhyperodd"><bean:message bundle="PRM" key="prm.contact.projectmgr"/></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="pocname" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="phone1" /></td>
			    
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="phone2" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="email1" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="email2" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="fax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhypereven"><bean:message bundle="PRM" key="prm.contact.salespoc"/></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="salespocname" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="spocphone1" /></td>
			    
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="spocphone2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="spocemail1" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="spocemail2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="spocfax" /></td>
		    </tr>
		    
		    <tr>
	  			
    			<td class = "formCaption" colspan = "7" height = "25">
    				<bean:message bundle="PRM" key="prm.contact.customercontacts"/>
    			</td>
			</tr>
  	
				<tr> 
	    			<td class = "Ntryb" rowspan = "2">&nbsp; </td>
	    			<td class = "Ntryb" rowspan = "2" >
	    				<bean:message bundle="PRM" key="prm.contact.name"/>
	    			</td>
	    			
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.phone1"/>
	    			</td>
	    
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.phone2"/>
	    			</td>
	    			
	    			<td class = "Ntryb" rowspan = "2">
	    				<bean:message bundle="PRM" key="prm.contact.email1"/>
	    			</td>
		
					<td class = "Ntryb" rowspan = "2">
						<bean:message bundle="PRM" key="prm.contact.email2"/>
					</td>
		
					<td class = "Ntryb" rowspan = "2">
						<bean:message bundle="PRM" key="prm.contact.fax"/>
					</td>
							
				</tr>
				
			<tr>
				<td colspan="7">&nbsp; </td>
			</tr>
				
			<tr> 
			    <td  class="Nhyperodd"><bean:message bundle="PRM" key="prm.contact.projectmgr"/></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="custpocname" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="custphone1" /></td>
			    
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="custphone2" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="custemail1" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="custemail2" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="custfax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhypereven"><bean:message bundle="PRM" key="prm.contact.businesspoc"/></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="businesspocname" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="businesspocphone1" /></td>
			    
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="businesspocphone2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="businesspocemail1" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="businesspocemail2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="businesspocfax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhyperodd"><bean:message bundle="PRM" key="prm.contact.contractpoc"/></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="contractpocname" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="contractpocphone1" /></td>
			    
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="contractpocphone2" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="contractpocemail1" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="contractpocemail2" /></td>
			    <td class="Nhyperodd"><bean:write name="AddContactForm" property="contractpocfax" /></td>
		    </tr>
		    
		    <tr> 
			    <td  class="Nhypereven"><bean:message bundle="PRM" key="prm.contact.billingpoc"/></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="billingpocname" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="billingpocphone1" /></td>
			    
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="billingpocphone2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="billingpocemail1" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="billingpocemail2" /></td>
			    <td class="Nhypereven"><bean:write name="AddContactForm" property="billingpocfax" /></td>
		    </tr>
  		
	 		 
	 		  <tr> 
	 		 	  <td colspan="7" class="buttonrow"> 
	 		 	  <logic:equal name="AddContactForm" property="fromPage" value="Appendix">
		 		      <html:button property="back" styleClass="button" onclick = "return Backaction('appendixdashboard');"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
		 		  </logic:equal>
		 		  <logic:notEqual name="AddContactForm" property="fromPage" value="Appendix">
		 		      <html:button property="back" styleClass="button" onclick = "return Backaction('appendixdetail');"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
		 		  </logic:notEqual>
				  </td>
			 </tr>
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
<%} %>
</html:form>
</table>
<body>

<script>
function Validate()
{
	
	if(document.forms[0].cnspoc.value=='0'&& document.forms[0].custpoc.value=='0')
	{
		alert("<bean:message bundle="PRM" key="prm.contact.selectnamefirst"/>");
		
		return false;
	}
		
	
}

function setValue()
{
	
	document.forms[0].refresh.value = "true";
	document.forms[0].action = "AddContact.do?function=refresh&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}

function Backaction(act)
{
	if(act == 'appendixdashboard'){
	document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getAttribute("typeid") %>&function=view&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	}
	if(act == 'appendixdetail'){
		document.forms[0].action = "AppendixDetailAction.do?Appendix_Id=<bean:write name = "AddContactForm" property = "typeid" />&MSA_Id=<bean:write name = "AddContactForm" property = "msaId" />";
	}
	document.forms[0].submit();
	return true;
}

function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="AddContact.do?typeid=<bean:write name = 'AddContactForm' property = 'typeid' />&function=add&Appendix_Id=<bean:write name = 'AddContactForm' property = 'typeid' />&type=P&Type=Appendix&viewjobtype=ION&ownerId=<bean:write name = 'AddContactForm' property = 'ownerId' />&firstCall=true&home=home&clickShow=true";
	
	document.forms[0].submit();
	return true;
}



function checking(checkboxvalue)
{
if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				document.forms[0].appendixSelectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				if(document.forms[0].appendixSelectedOwners[j].checked) {
				 	document.forms[0].appendixSelectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{

	for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
	{
			if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
			{
				document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="AddContact.do?typeid=<bean:write name = 'AddContactForm' property = 'typeid' />&Appendix_Id=<bean:write name ='AddContactForm' property ='typeid'/>&type=P&Type=Appendix&viewjobtype=ION&ownerId=<bean:write name = 'AddContactForm' property = 'ownerId' />&opendiv=0";
 				document.forms[0].submit();

			}
	}
	}
}



function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AddContact.do?typeid=<bean:write name = 'AddContactForm' property = 'typeid' />&Appendix_Id=<bean:write name ='AddContactForm' property ='typeid'/>&type=P&Type=Appendix&viewjobtype=ION&ownerId=<bean:write name = 'AddContactForm' property = 'ownerId' />&clickShow=true";
	document.forms[0].submit();
	return true;
}



function month_Appendix()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AddContact.do?typeid=<bean:write name = 'AddContactForm' property = 'typeid' />&Appendix_Id=<bean:write name ='AddContactForm' property ='typeid'/>&type=P&Type=Appendix&viewjobtype=ION&ownerId=<bean:write name = 'AddContactForm' property = 'ownerId' />&month=0&clickShow=true";
	document.forms[0].submit();
	return true;
}
function week_Appendix()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AddContact.do?typeid=<bean:write name = 'AddContactForm' property = 'typeid' />&Appendix_Id=<bean:write name ='AddContactForm' property ='typeid'/>&type=P&Type=Appendix&viewjobtype=ION&ownerId=<bean:write name = 'AddContactForm' property = 'ownerId' />&week=0&clickShow=true";
	document.forms[0].submit();
	return true;
}

</script>

<script>
function changeFilter()
{
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="AddContact.do?showList=something&typeid=<bean:write name = 'AddContactForm' property = 'typeid' />&Appendix_Id=<bean:write name ='AddContactForm' property ='typeid'/>&type=P&viewjobtype=ION&ownerId=<bean:write name = 'AddContactForm' property = 'ownerId' />&fromPage=<bean:write name = 'AddContactForm' property = 'fromPage' />";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="AddContact.do?resetList=something&typeid=<bean:write name = 'AddContactForm' property = 'typeid' />&Appendix_Id=<bean:write name ='AddContactForm' property ='typeid'/>&type=P&viewjobtype=ION&ownerId=<bean:write name = 'AddContactForm' property = 'ownerId' />&fromPage=<bean:write name = 'AddContactForm' property = 'fromPage' />";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].typeid.value;
 	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>




</html:html>