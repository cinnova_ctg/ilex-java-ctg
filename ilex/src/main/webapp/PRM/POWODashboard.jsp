<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%
String jobid = "";
String viewJob="frompartner";
int size = 0;
String partner_id = "0";
if(request.getAttribute("jobid")!=null) {
	jobid = ""+request.getAttribute("jobid");
}

if(request.getAttribute("listsize")!=null)
{
	size = Integer.parseInt(request.getAttribute("listsize").toString());
}
if(request.getAttribute("partner_id") != null) {
	partner_id = request.getAttribute("partner_id").toString();
}
if(request.getParameter("viewjobtype")!= null) {
	viewJob = request.getParameter("viewjobtype");
}
%>

<html:html>
<HEAD>
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</HEAD>

<%@ include  file="/Menu.inc" %>
<script>

function addpartner()
{
	document.forms[0].action = "Partner_Search.do?partnerid=0&formtype=powo&ref=search&typeid=<%=jobid%>&type=Add&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}

function viewpowo(powoid, type)
{
    window.location.href = "POWOAction.do?jobid=<%= jobid%>&function_type=view&order_type="+type+"&powoid="+powoid;
	return false;	
}

function sendpowoOnclick(powoid, type)
{
    window.location.href = "POWOAction.do?jobid=<%= jobid%>&function_type=Send POWO&order_type="+type+"&powoid="+powoid;
	return false;	
}


function del(powoid) 
{
		var convel = confirm( "<bean:message bundle = "PRM" key = "prm.powo.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?powoid="+powoid+"&jobid=<%=jobid%>&Type=POWO";
			document.forms[0].submit();
			return true;	
		}
}

function poWoHistory(powoid,type){
	if(type == 'po')
		document.forms[0].action="EntityHistory.do?entityId="+powoid+"&entityType=PO&function=viewHistory";
	else
		document.forms[0].action="EntityHistory.do?entityId="+powoid+"&entityType=WO&function=viewHistory";
		document.forms[0].submit();
		return true;
}

</script>

<script>
function toggleMenuSection(unique) {
	
	var divid="div_"+unique;

	thisDiv = document.getElementById(divid);
	
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	
	}
	
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
		
	}
	else {
		thisImage.src = "images/Expand.gif";
		
	}
	
}

function toggleDiv( divName ) {

	 var tcookie = document.cookie;

	
     var st = tcookie.indexOf ( "P1Db" );

     var cookievalue = "";
     
	  if( st != -1 )
	  {
	      st = st + 3;
	      st = tcookie.indexOf ( "=" , st );
	      en = tcookie.indexOf ( ";" , st );
	
		  if ( en > st )
	        {
	          cookievalue = tcookie.substring( st+1 , en ); //div's that is reopen
	        }

	 }
	 
	var id = document.getElementById( divName );
	
	if( id.style.display == "none" ) 
		{
				id.style.display = "block";
				//add it to the existing cookie opendiv paraneter
			
				if ( cookievalue.length > 0 )
					cookievalue = cookievalue + ',' + divName;
				else
					cookievalue = divName;
					
				if( cookievalue != "" )
				{ 
					// Save as cookie
					document.cookie = "PDb="+ cookievalue+" ; ";
	            }
		}

	else
		{
			id.style.display = "none";	
			var newcookievalue = '';
			//remove this div from the opendiv cookie
			if ( cookievalue.length > 0 )
			{
				var array = cookievalue.split( ',' );

				//if (array.length>1)
				if ( array.length > 0 )
				{
				   
					
					for( i = 0; i < array.length;i++ )
					{

						if( array[i] == divName )
						{

						}
						else
						{

							if( newcookievalue.length > 0 )
							{
								newcookievalue = newcookievalue + ',' + array[i];
							}
							else
							{
								newcookievalue = array[i];	
							}
		
						}

					}
				}
				
				//if(newcookievalue!="")
				//{ // Save as cookie

			 		document.cookie = "PDb="+ newcookievalue+" ; ";

			 	//}
			}

		}


	/*if( divName == 'div_1')
	{
		id = document.getElementById('div_13');
		if( id.style.display == "none" ) 
			id.style.display = "block";
		else
			id.style.display = "none";	
	
		id = document.getElementById('div_11');
		if( id.style.display == "none" ) 
			id.style.display = "block";
		else
			id.style.display = "none";	
	
	}*/
	

}


function toggleMenuSectionView( unique ) {
	
	var divid = "div_" + unique;
	thisDiv = document.getElementById( divid );
	
	try{
	if ( thisDiv == null ) { return ; }
	}catch( e ){
	
	}
	action = "toggleType = toggleDivView( 'div_" + unique + "' );";
	eval( action );
	action = "thisImage = document.getElementById( 'img_" + unique + "' );";
	eval( action );
	
	if ( document.getElementById( 'div_' + unique ).offsetHeight > 0 ) {
		thisImage.src = "images/Collapse.gif";
		
	}
	else {
		thisImage.src = "images/Expand.gif";
		
	}
	
	
}

function toggleDivView(divName) {
	
	 var tableRow = document.getElementsByTagName( 'tr' );
	if ( tableRow.length == 0 ) { return; }
	for ( var k = 0; k < tableRow.length; k++ ) {
	
		if ( tableRow[k].getAttributeNode( 'id' ).value == divName ) {
		
			if ( tableRow[k] ) {
				if ( tableRow[k].style.display == "none" ) {
					tableRow[k].style.display = "block";
				}
				
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert( errorString );
			}
		}
	
	}
}

// Added Start:By Amit This will be called on each href that opens other form
function createCookie()
{

 var tableRow = document.getElementsByTagName( 'tr' );

  var openDiv = "";
	if ( tableRow.length == 0 ) { return; }

	for ( var k = 0; k < tableRow.length; k++ ) {

		if( ( tableRow[k].getAttributeNode( 'id' ).value != "" )&&( tableRow[k].getAttributeNode( 'id' ).value != "account" ) )
		{


			if( tableRow[k].style.display == "block" ){
			openDiv = openDiv + tableRow[k].getAttributeNode( 'id' ).value + ",";
			}
		}
	}
		if(openDiv!="")
		{ // Save as cookie
		 document.cookie = "PDb="+ openDiv+" ; ";
		 }


}

function createCookie(lastDivForFocus)
{

 		document.cookie = "P1Db="+ lastDivForFocus+" ; ";

}




//following function would be called on OnLoad
function CookieGetOpenDiv()
{
  
  var tcookie = document.cookie;
	
  var unique;
  var st = tcookie.indexOf ("PDb");
  var cookievalue ="";
  if(st!= -1)
  {
      st = st+3;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);

	  if (en > st)
        {
          cookievalue = tcookie.substring(st+1, en);
        }
        

       if(cookievalue.length>0) 
       {

		var opendiv = cookievalue.split(',');
	

		if(opendiv.length>0)
		{

		  for (var j = 0; j < opendiv.length; j++)
		   {
		   		if(eval("document.getElementById('"+opendiv[j]+"')")!=null)	//Added This if condition by amit
		   		{
		  				eval("document.getElementById('"+opendiv[j]+"')").style.display = "block";
						unique = opendiv[j].substring(opendiv[j].indexOf("_")+1,opendiv[j].length);
						action = "thisImage = document.getElementById('img_" + unique +"');";
										
						eval(action);
					
		
					if (document.getElementById('div_' + unique).offsetHeight > 0)
					{
							thisImage.src = "images/Collapse.gif";
		   			}
					else 
					{
							thisImage.src = "images/Expand.gif";
					}
				}
				/*if(opendiv[j]=='div_1')
				{
						document.getElementById('div_11').style.display = "block";
						document.getElementById('div_13').style.display = "block";
				}	*/											
	
		  }
		
		}
	}
	


  }

	  openDiv = "";
	  //Save as cookie
	  document.cookie = "P1Db="+ openDiv+" ; "; 
	  //Start:Added For Focus		
	  var st1 = tcookie.indexOf ("P1Db");
	  var cookievalue1 ="";
	  if(st1!= -1)
	  {
	    	st1 = st1+4;
	      st1 = tcookie.indexOf ("=", st1);
	      en = tcookie.indexOf (";", st1);
	
		  if (en > st1)
	        {
	          cookievalue1 = tcookie.substring(st1+1, en);
	          
	        }
		}
	 
		 if(cookievalue1!="")
		 {  
		  		
		  		if(document.getElementById(cookievalue1)!=null)
		 		{
		 	      document.getElementById(cookievalue1).focus();
		    	}
		}
		document.cookie = "P1Db="+ openDiv+" ; ";

}


</script>



<html:form action = "POWOAction"> 
<html:hidden property = "inv_type" />
<html:hidden property = "inv_id" />
<html:hidden property = "inv_rdofilter" />
<html:hidden property = "nettype" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property="invoice_Flag"/>
<html:hidden property="invoiceNo"/>
<html:hidden property="partner_name"/>
<html:hidden property="powo_number"/>
<html:hidden property="from_date"/>
<html:hidden property="to_date"/>

<body onLoad = "MM_preloadImages( 'images/Expand.gif' , 'images/Collapse.gif' ); CookieGetOpenDiv();">

<TABLE>
         <TD>
            <TABLE cellspacing = 1 cellpadding = 0 class = "dbsummtable3" width = "1200" border = 0>
              <TBODY>
              <tr height = "30">
              	<td>&nbsp;</td>
              </tr>
              
              	<logic:present name = "updateflag" scope = "request">
	              	<tr>
						<td width = "100%" class = "message" colspan="5">&nbsp;&nbsp;&nbsp;&nbsp;
							<logic:equal name = "updateflag" value = "-9001">
								<bean:message bundle = "PRM" key = "prm.powo.update.failure1"/>
							</logic:equal>
							
							<logic:equal name = "updateflag" value = "-9002">
								<bean:message bundle = "PRM" key = "prm.powo.update.failure2"/>
							</logic:equal>
							
							<logic:equal name = "updateflag" value = "-9003">
								<bean:message bundle = "PRM" key = "prm.powo.update.failure3"/>
							</logic:equal>
							
							<logic:equal name = "updateflag" value = "-9004">
								<bean:message bundle = "PRM" key = "prm.powo.update.failure4"/>
							</logic:equal>
							
							<logic:equal name = "updateflag" value = "-9005">
								<bean:message bundle = "PRM" key = "prm.powo.update.failure5"/>
							</logic:equal>
							
							<logic:equal name = "updateflag" value = "0">
								<bean:message bundle = "PRM" key = "prm.powo.update.success"/>
							</logic:equal>
						</td>
					</tr>
				</logic:present>
				

				<logic:present name = "emailflag" scope = "request">
				
						<tr> 
							<td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
						    	<logic:equal name = "emailflag" value = "0">
									<bean:message bundle = "pm" key = "msa.detail.mailsent"/>
					    		</logic:equal>
				
					    		<logic:notEqual name = "emailflag" value = "0">
									<bean:message bundle = "pm" key = "msa.detail.mailsentfailure"/>
					    		</logic:notEqual>
					    	</td>
				    	</tr>
				</logic:present>
				<logic:present name="docMError" scope="request">
					<tr>
						<td colspan="4" class="message" height="30"><bean:message bundle = "pm" key = "exception.docmexception"/></td>
					</tr>
				</logic:present>
	            <tr>
					<td width = "100%" class = "labeleboldwhite" colspan = "16">&nbsp;&nbsp;&nbsp;&nbsp;
					<bean:message bundle = "PRM" key = "prm.powo.header"/>: <A href = "AppendixHeader.do?function=view&appendixid=<bean:write name="POWOForm" property="appendixid"/>&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />"><bean:write name="POWOForm" property="appendixname"/></A> -> <bean:message bundle="PRM" key="prm.powo.job"/>: <A href="JobDashboardAction.do?jobid=<bean:write name="POWOForm" property="jobid" />&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />"><bean:write name="POWOForm" property="jobname" /></A>
				</tr>
              
	              <tr height="20">
	             	 <td>&nbsp;</td>
	              </tr>     
                            
              <TR>
                <TD></TD>
                <TD class = trybtopbutton colspan = "2">
					<html:button property = "sort" styleClass = "button" onclick = "javascript:createCookie('sort');return sortwindow();">
						<bean:message bundle = "PRM" key = "prm.powo.sort"/>
					</html:button>
					<html:button property = "add" styleClass = "button" onclick = "return addpartner();">
						<bean:message bundle = "PRM" key = "prm.powo.add"/>
					</html:button>
					<logic:present name = "POWOForm" property = "inv_type">
						<html:button property = "back" styleClass = "button" onclick = "return Backaction();">
							<bean:message bundle = "PRM" key = "prm.button.back"/>
						</html:button>	 
					</logic:present>
					
					<logic:equal name = "POWOForm" property = "nettype" value = "dispatch">
						<html:button property = "back" styleClass = "button" onclick = "return Backaction();">
							<bean:message bundle = "PRM" key = "prm.button.back"/>
						</html:button>
					</logic:equal>
					
					<logic:present name = "partner_id" scope = "request">
										
					  <logic:notEqual name= "partner_id" value="0">
					  					
						<html:button property = "back" styleClass = "button" onclick = "return Backaction();">
							<bean:message bundle = "PRM" key = "prm.button.back"/>
						</html:button>	 
					 </logic:notEqual>	
					</logic:present>
				</TD>
                <TD class = trybtop  height = 20 align = "middle" colspan = 16>
                	<bean:message bundle = "PRM" key = "prm.powo.summaryheader"/>
				</TD>
			  </TR>
              <TR>
				<td  width = "1%" rowspan = "2">&nbsp; </td>
                <TD class = tryb rowSpan = 2 colspan = 2 width = "14%"><bean:message bundle = "PRM" key = "prm.powo.sourcecompanyname"/></TD>
                <TD class = tryb rowSpan = 2 width = "8%"><bean:message bundle = "PRM" key = "prm.powo.ponumber"/></TD>
                <TD class = tryb rowSpan = 2 width = "6%"><bean:message bundle = "PRM" key = "prm.powo.type"/></TD>
				<TD class = tryb rowSpan = 2 width = "4%"><bean:message bundle = "PRM" key = "prm.powo.bulk"/></TD>
				<TD class = tryb rowSpan = 2 width = "5%"><bean:message bundle = "PRM" key = "prm.powo.shipto"/></TD>
				<TD class = tryb rowSpan = 2 width = "6%"><bean:message bundle = "PRM" key = "prm.powo.wonumber"/></TD>
				<TD class = tryb rowSpan = 2 width = "6%"><bean:message bundle = "PRM" key = "prm.powo.authorizedtotal"/></TD>
				<TD class = tryb rowSpan = 2 width = "6%"><bean:message bundle = "PRM" key = "prm.powo.issuedate"/></TD>
				<TD class = tryb rowSpan = 2 width = "6%"><bean:message bundle = "PRM" key = "prm.powo.deliverbydate"/></TD>
                <TD class = tryb rowSpan = 2 width = "6%"><bean:message bundle = "PRM" key = "prm.powo.terms"/></TD>
				<TD class = tryb rowSpan = 2 width = "10%"><bean:message bundle = "PRM" key = "prm.powo.masterpoitem"/></TD>
				<TD class = tryb rowSpan = 2 width = "5%"><bean:message bundle = "PRM" key = "prm.powo.specialcondition"/></TD>
                <TD class = trybrecon colSpan = 5 width = "15%"><bean:message bundle = "PRM" key = "prm.powo.reconciliation"/></TD>
			  </TR>	
              <TR>
                <TD class = trybrecon width = "3%">
                  <DIV><bean:message bundle = "PRM" key = "prm.powo.invoicedtotal"/></DIV></TD>
                <TD class = trybrecon width = "3%">
                  <DIV><bean:message bundle = "PRM" key = "prm.powo.approvedtotal"/></DIV></TD>
                <TD class = trybrecon width = "3%">
                  <DIV><bean:message bundle = "PRM" key = "prm.powo.invoicedrecddate"/></DIV></TD>
                <TD class = trybrecon width = "3%">
                  <DIV><bean:message bundle = "PRM" key = "prm.powo.invoicednumber"/></DIV></TD>
                <TD class = trybrecon width = "3%">
                  <DIV><bean:message bundle = "PRM" key = "prm.powo.comments"/></DIV></TD>
              </TR>
            
            <% 
			if( size > 0 ){
			%>
              
              <logic:present name = "powolist">
              		<%
					String temp_str = "";
					String temp_str1 = "";
					String temp_str2 = "";
					String temp_str3 = "";
					String temp_str4 = "";
              		int i = 1;
              		int j = 1;

					String bgcolor = "";
					String bgcolorno = ""; 
					String bgcolortext1 = ""; 
					%>
				
					<logic:iterate id = "powolist" name = "powolist" >
					<bean:define id = "poid" name = "powolist" property = "powoid" />
					<bean:define id = "partnerid" name = "powolist" property = "partnerid" />
					<%
					temp_str = "";
					temp_str = "javascript:del( "+poid+" );"; 

					if( ( i%2 )== 0 )
					{
						bgcolor = "dbvalueeven";
						bgcolorno="numbereven";
						bgcolortext1="textlefteven";
					} 
					else
					{
						bgcolor = "dbvalueodd";
						bgcolorno = "numberodd";
						bgcolortext1 = "textleftodd";
					}
					%>
					
				
				<%
				if( size == i ) 
				{
					if( ( i%2 ) == 0 )
					{
						bgcolor = "dbvalueevenlastrow";
						bgcolorno = "numberevenlastrow";
						bgcolortext1 = "textleftevenlastrow";
					
					} 
					else
					{
						bgcolor = "dbvalueoddlastrow";
						bgcolorno = "numberoddlastrow";
						bgcolortext1 = "textleftoddlastrow";
					}
				} 
				%>
				
				<!-- Added By Amit id of TR for more/back-->
				<TR id = "account">

				

                <TD width = "20"><A href = "javascript:toggleMenuSection( 'j<%= i %>' );">
					<IMG id = img_j<%= i %> alt=+ src = "images/Expand.gif" border = 0></A> 
				</TD>
				
                <TD class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
					<bean:write name = "powolist" property = "srccompanyname"/>
				</TD>	
				<TD class="<%= bgcolortext1 %>" align = "right" >
					<!-- <div align=right valign="top">  -->
						[<A id='aa_j<%= i %>' href = "Partner_Search.do?partnerid=<%= partnerid %>&formtype=powo&ref=search&typeid=<%=jobid%>&type=Update&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />&powoid= <%=poid %>" onclick="Javascript:createCookie('aa_j<%= i %>');">Assign</A>]&nbsp;[<A href="<%= temp_str%>">Delete</A>]&nbsp;
					<!-- </div>  -->
				</TD>
                <TD id = "classchange2" class = "<%= bgcolor %>" >
					<A href = "javascript:viewpowo( <%=poid%> , 'po' );"><bean:write name = "powolist" property = "powono"/></A>&nbsp;&nbsp;&nbsp;&nbsp;[<A id='ba_j<%= i %>' href = "POWODetailAction.do?powoid=<%=poid%>&jobid=<%=jobid%>&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />" onclick="Javascript:createCookie('ba_j<%= i %>');">Edit</A>]
				</TD>
                <TD class = "<%= bgcolor %>"  >
                	<bean:write name = "powolist" property = "type"/>
                </TD>
                <TD class = "<%= bgcolor %>" >
                	<bean:write name = "powolist" property = "bulk"/>
                </TD>
                <TD class = "<%= bgcolor %>" >[<A href = "POWODetailAction.do?powoid=<%= poid %>&jobid=<%= jobid %>&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />">Edit</A>]</TD>
                <TD class = "<%= bgcolor %>" noWrap ><A id='ca_j<%= i %>' href = "javascript:viewpowo(<%= poid %>, 'wo');" onclick="Javascript:createCookie('ca_j<%= i %>');"><bean:write name="powolist" property="powono"/></A></TD>
                <TD class = "<%= bgcolorno %>" >
                	<bean:write name = "powolist" property = "authorizedtotal"/>
                </TD>
                
                <TD class = "<%= bgcolor %>" align = middle >
                	<bean:write name = "powolist" property = "issuedate"/>
                </TD>
                
                <TD class = "<%= bgcolor %>" align = middle >
                	<bean:write name = "powolist" property = "deliverbydate"/>
                </TD>
                
                <TD class = "<%= bgcolor %>" align = middle nowrap >
                	<bean:write name = "powolist" property = "terms"/>
                </TD>
                <TD class = "<%= bgcolor %>" align = middle nowrap>
                	<bean:write name = "powolist" property = "masterpoitem"/>
                </TD>
                <TD class = "<%= bgcolor %>" align = middle >[<A id='da_j<%= i %>' href = "POWODetailAction.do?powoid=<%= poid %>&jobid=<%= jobid %>&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />" onclick="Javascript:createCookie('da_j<%= i %>');">Edit</A>]</TD>
               
                <TD class = "<%= bgcolorno %>" >
                	<bean:write name = "powolist" property = "invoicetotal"/>
                </TD>
				
				<TD class = "<%= bgcolorno %>" >
					<bean:write name = "powolist" property = "approvedtotal"/>
				</TD>
				
				<TD class = "<%= bgcolor %>" >
					<bean:write name = "powolist" property = "invoicerecddate"/>
				</TD>
				
				<TD class = "<%= bgcolor %>" >
					<bean:write name = "powolist" property = "invoiceno"/>
				</TD>
				<TD class = "<%= bgcolor %>" align=middle >[<A id='ea_j<%= i %>' href = "POWODetailAction.do?powoid=<%= poid %>&jobid=<%= jobid %>&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />" onclick="Javascript:createCookie('ea_j<%= i %>');">Edit</A>]</TD>                
				</TR>


				<TR id = div_j<%= i %> style = "DISPLAY: none">
                <TD>&nbsp;</TD>
                <TD colSpan=18>
                  <TABLE cellSpacing = 0 cellPadding = 0>
                    <TBODY>
                    <TR>
                      <TD width = 20>&nbsp;&nbsp;&nbsp;&nbsp;</TD>
                      <TD>
                        <TABLE class = dbsummtable height = "100%" cellSpacing = 0 cellPadding = 0 width = "1100" border = "0">
                          <TBODY>
	                          <TR>
	                           <%
	          	                 	temp_str = "";
	   		       				    temp_str1 = "";
							   		temp_str2 = "";
							   		temp_str3 = "";
							   		temp_str4 = "";
									
									//temp_str = "createCookie('fa_j"+i+"'); sendpowoOnclick("+poid+", 'po');"; 
									//temp_str1 = "createCookie('ga_j"+i+"'); sendpowoOnclick("+poid+", 'wo');";
									//temp_str2 = "createCookie('ha_j"+i+"'); sendpowoOnclick("+poid+", 'powo');";

									temp_str = "sendpowoOnclick( "+poid+" , 'po' );"; 
									temp_str1 = "sendpowoOnclick( "+poid+" , 'wo' );";
									temp_str2 = "sendpowoOnclick( "+poid+" , 'powo' );";
									temp_str3 = "poWoHistory( "+poid+" , 'po' );";
									temp_str4 = "poWoHistory( "+poid+" , 'wo' );";
									
			
								
								%> 
								
								<TD class = trybtopsubbutton colspan = "4">
									<html:button property = "sendpo" styleClass = "button" onclick = "<%= temp_str %>">
										<bean:message bundle = "PRM" key = "prm.powo.sendpo"/>
									</html:button>
									<html:button property = "poHistory" styleClass = "button" onclick = "<%= temp_str3 %>">
										PO History
									</html:button>
									<html:button property = "sendwo" styleClass = "button" onclick = "<%= temp_str1 %>">
										<bean:message bundle = "PRM" key = "prm.powo.sendwo"/>
									</html:button>
									<html:button property = "woHistory" styleClass = "button" onclick = "<%= temp_str4 %>">
										WO History
									</html:button>
									<html:button property = "sendpowo" styleClass = "button" onclick = "<%= temp_str2 %>">
										<bean:message bundle = "PRM" key = "prm.powo.sendpowo"/>
									</html:button>
									

								</TD>
								<TD class = trybtopsub colSpan = "10" height = 20 align = middle> 
									<bean:message bundle = "PRM" key = "prm.powo.powodetails"/>
								</TD>
							  </TR>
	                          
	                           <TR>
								<TD class = tryb rowSpan = 2 width = "10%">
									<bean:message bundle = "PRM" key = "prm.powo.cnspartnum"/>
								</TD>
								
								<TD class = tryb rowSpan = 2 width = "20%">
									<bean:message bundle = "PRM" key = "prm.powo.resourcename"/>
								</TD>
								
								<TD class = tryb rowSpan = 2 width = "10%">
									<bean:message bundle = "PRM" key = "prm.powo.type"/>
								</TD>
								
								<TD class = tryb rowSpan = 2 width = "10%">
									<bean:message bundle = "PRM" key = "prm.powo.mfgpart"/>
								</TD>
								
								<TD class = tryb colSpan = 3 width = "20%">
									<bean:message bundle = "PRM" key = "prm.powo.newesm"/>
								</TD>
								
								<TD class = tryb colSpan = 3 width = "20%">
									<bean:message bundle = "PRM" key = "prm.powo.newauth"/>
								</TD>
								
								<TD class = tryb rowSpan = 2 width = "5%">
									<bean:message bundle = "PRM" key = "prm.powo.showonwo"/>
								</TD>
								<TD class = tryb rowSpan = 2 width = "5%">
									<bean:message bundle = "PRM" key = "prm.powo.dropshipped"/>
								</TD>
								
								<TD class = tryb rowSpan = 2 width = "5%">
									<bean:message bundle = "PRM" key = "prm.powo.pvsSupplied"/>
								</TD>
								<TD class = tryb rowSpan = 2 width = "5%">
									<bean:message bundle = "PRM" key = "prm.powo.creditcard"/>
								</TD>
								
								
							  </TR>	
							  
							  <TR>
								
								<TD class = tryb  width = "4%">
									 <DIV>
										<bean:message bundle = "PRM" key = "prm.powo.qty"/>
									 </DIV>
								</TD>
								
								<TD class = tryb width = "7%">
								  <DIV>
								  	<bean:message bundle = "PRM" key = "prm.powo.unit"/>
								  </DIV>
								</TD>
								<TD class = tryb width = "9%">
								  <DIV>
								  	<bean:message bundle = "PRM" key = "prm.powo.totaldash"/>
								  </DIV>
								</TD>
								
								<TD class = tryb width = "4%">
									 <DIV>
										<bean:message bundle = "PRM" key = "prm.powo.qty"/>
									 </DIV>
								</TD>
								
								<TD class = tryb width = "7%">
								  <DIV>
								  	<bean:message bundle = "PRM" key = "prm.powo.unit"/>
								  </DIV>
								</TD>
								<TD class = tryb width = "9%">
								  <DIV>
								  	<bean:message bundle = "PRM" key = "prm.powo.totaldash"/>
								  </DIV>
								</TD>
							  </TR>
							  <% j = 1; %>
							 <logic:iterate id = "poworesourcelist" name = "powolist" property = "resourcelist">
							 <%
								if( ( j%2 ) == 0 )
								{
									bgcolor = "dbvalueeven";
									bgcolorno = "numbereven";
									bgcolortext1 = "textleftevendash";
								} 
								else
								{
									bgcolor = "dbvalueodd";
									bgcolorno = "numberodd";
									bgcolortext1 = "textleftodddash";
								}
							%>
							  <TR height = 20>
							    <TD class = "<%= bgcolor %>">
							    	<bean:write name = "poworesourcelist" property = "cnspartnum"/>
							    </TD>
							   
							    <TD class = "<%= bgcolortext1 %>">
							    	<bean:write name = "poworesourcelist" property = "resourcename"/>
							    </TD>
				
								<TD class = "<%= bgcolortext1 %>">
									<bean:write name = "poworesourcelist" property = "resourcetype"/>
								</TD>
								
								<TD class = "<%= bgcolortext1 %>">
									<bean:write name = "poworesourcelist" property = "resourcemfgpart"/>
								</TD>
								
								<TD class = "<%= bgcolorno %>">
									<bean:write name = "poworesourcelist" property = "resourceqty"/>
								</TD>
								
								<TD class = "<%= bgcolorno %>">
									<bean:write name = "poworesourcelist" property = "estimatedunitcost"/>
								</TD>
								
								<TD class = "<%= bgcolorno %>">
									<bean:write name = "poworesourcelist" property = "estimatedtotalcost"/>
								</TD>
								
								<TD class = "<%= bgcolorno %>">
									<logic:equal name = "poworesourcelist" property = "type" value = "2">
									</logic:equal>
									
									<logic:notEqual name = "poworesourcelist" property = "type" value = "2">
										<bean:write name = "poworesourcelist" property = "aut_qty"/>
									</logic:notEqual>
								</TD>
								
								<TD class = "<%= bgcolorno %>">
									<logic:equal name = "poworesourcelist" property = "type" value = "2">
									</logic:equal>
									
									<logic:notEqual name = "poworesourcelist" property = "type" value = "2">
										<bean:write name = "poworesourcelist" property = "resourceathounitcost"/>
									</logic:notEqual>
								</TD>
								
								<TD class = "<%= bgcolorno %>">
									<logic:equal name = "poworesourcelist" property = "type" value = "2">
									</logic:equal>
									<logic:notEqual name = "poworesourcelist" property = "type" value = "2">
										<bean:write name = "poworesourcelist" property = "resourceathototalcost"/>
									</logic:notEqual>
								</TD>
								
								<TD class = "<%= bgcolor %>" align = middle>
									<bean:write name = "poworesourcelist" property = "resourceshowon"/>
								</TD>
								
								<TD class = "<%= bgcolor %>" align = middle>
									<bean:write name = "poworesourcelist" property = "resourcedropshift"/>
								</TD>
								<!--  changes made by Seema -->
								<TD class = "<%= bgcolor %>" align = middle>
									<bean:write name = "poworesourcelist" property = "resourcepvssupplied"/>
								</TD>
								
								<TD class = "<%= bgcolor %>" align = middle>
									<bean:write name = "poworesourcelist" property = "resourcecreditcard"/>
								</TD>
								
							</TR>
							<% 
								j++;
			 				%>
								</logic:iterate>
						</TBODY></TABLE></TD></TR></TBODY></TABLE></TD>
								
												
							</TR>
			<% i++;
			 %>
				</logic:iterate>
				
				
			</logic:present>
			<%
			}
			else {
			%>
					<tr>
		    			<TD></TD>
		    			<td  colspan = "10" class = "message" height = "30">
		    				No PO/WO defined. 
		    			</td>
		    		</tr>
		
			<% } %>
  			   </tbody> </table> </td> </table>
</body>
</html:form>
<script>
var str = '';

function sortwindow()
{
	str = 'SortAction.do?Type=prj_powo&powo_Jobid=<%= jobid %>';
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
	return true;
	
}


function Backaction()
{
 if("<%=viewJob%>" != "frompartner" )
	{	
		document.forms[0].action = "JobDashboardAction.do?function=view&jobid=<%= jobid %>&appendixid=<bean:write name="POWOForm" property="appendixid"/>";
	}
 else {
 	if (<%=partner_id%> != "0") {
 			document.forms[0].action = "JobInformationperPartner.do?pid=<%=partner_id%>&from=Partner_Edit";
 		}else { 
 			if( document.forms[0].nettype.value == 'dispatch' )
			document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype=dispatch&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />";
			else
			document.forms[0].action = "InvoiceJobDetail.do?type="+document.forms[0].inv_type.value+"&id="+document.forms[0].inv_id.value+"&rdofilter="+document.forms[0].inv_rdofilter.value+"&invoice_Flag="+document.forms[0].invoice_Flag.value+"&invoiceno="+document.forms[0].invoiceNo.value+"&partner_name="+document.forms[0].partner_name.value+"&powo_number="+document.forms[0].powo_number.value+"&from_date="+document.forms[0].from_date.value+"&to_date="+document.forms[0].to_date.value;
 		}
	}	
	document.forms[0].submit();
	return true;
	
}
</script>
</html:html>


