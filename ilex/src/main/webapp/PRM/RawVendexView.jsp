<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %> 
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<% 
String appendixid = "";
String msaid = "";
String appendix = "-1";
String netmedx = "-1";

if(request.getAttribute("appendix") !=null)
	appendix = request.getAttribute("appendix").toString();

if(request.getAttribute("netmedx") !=null)
	netmedx = request.getAttribute("netmedx").toString();

%>

<html:html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

	<title></title>
	<%@ include file="/Header.inc" %>
	<%@ include  file="/Menu.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<title></title>
</head>
<script>
	function abc() {
		if('<c:out value="${requestScope.searchedResult}"/>' == 'searchedResult') {
			onLoad()
		}
		
	}
</script>
<BODY onload="abc();">
<html:form action="/RawVendexViewAction"> 
<html:hidden property="msaId" />
 <table width="100%" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr> 
        
            <td  id="pop1" width="80" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="drop">MSA</a></td> 
            <td  id="pop2" width="100" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="drop">Search</a></td> 
            <td class = "toprow1" width = "400"></td>
             
        </tr>
 </table>
<SCRIPT language=JavaScript1.2>

if(isMenu) {
arMenu1=new Array(
120,
//1,16,
findPosX('pop1'),findPosY('pop1'),
"","",
"","",
"","",
"Sites","#",1
<% if(request.getAttribute("viewRawVendex")!= null && request.getAttribute("viewRawVendex").equals("viewRawVendex")){ %>
,"Raw Vendex View","RawVendexViewAction.do?msaid=<c:out value="${RawVendexViewForm.msaId}"/>&appendixid=0&function=<%=request.getParameter("function")%>&viewjobtype=<%=request.getParameter("viewjobtype")%>&ownerId=<%=request.getParameter("ownerId")%>&initialCheck=true&pageType=mastersites",0
<%}%>
)

arMenu1_1=new Array(
"View","SiteManagement.do?msaid=<c:out value="${RawVendexViewForm.msaId}"/>&appendixid=0&function=<%=request.getParameter("function")%>&viewjobtype=<%=request.getParameter("viewjobtype")%>&ownerId=<%=request.getParameter("ownerId")%>&initialCheck=true&pageType=mastersites",0,
"Upload","UploadCSV.do?msaid=<c:out value="${RawVendexViewForm.msaId}"/>&viewjobtype=<%=request.getParameter("viewjobtype")%>&ownerId=<%=request.getParameter("ownerId")%>&fromType=SiteMenu",0
)

arMenu2=new Array(
125,
//1,16,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"",""
<% if(!appendix.equals("0")){ %>
,"Job","AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=0&ownerId=All&fromPage=msa&msaId=<c:out value="${RawVendexViewForm.msaId}"/>",0
<%} %>
<% if(!netmedx.equals("0")){ %>
,"Ticket","AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=0&ownerId=All&msaId=<c:out value="${RawVendexViewForm.msaId}"/>&searchType=ticket",0
<%} %>
)

document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}
</script>


<table border="0" cellpading="0" cellspacing="0">
	<tr><td height="10"></td></tr>
	<tr>
		<td style="padding-left: 10px;">
			<table>
				<tr>
					<td colspan="7"><h1>Raw Vendex View - All Sites and All Partners for&nbsp;<c:out value='${requestScope.msaName }'/></h1></td>
				</tr>
				<tr>
					<td class="tabtexto">End Customer<font color="red">*</font>:&nbsp;</td>
					<td class="pvstexto">
						<html:select name ="RawVendexViewForm" property="endCustomer" size ="1" styleClass="select">
							<html:optionsCollection name ="RawVendexViewForm" property = "endCustomeList" label = "label"  value = "value" />
						</html:select>&nbsp;&nbsp;&nbsp;
					</td>
					<td class="tabtexto">Reference Zip Code<font color="red">*</font>:&nbsp;</td>
					<td class="pvstexto"><html:text name="RawVendexViewForm" property="zipCode" styleClass="text" size="10"/>&nbsp;&nbsp;&nbsp;</td>
					<td class="tabtexto">Radius<font color="red">*</font>:&nbsp;</td>
					<td class="pvstexto">
						<html:select name = "RawVendexViewForm" property="radius" size="1" styleClass="select">
							<html:optionsCollection name="RawVendexViewForm" property="radiusList" label="label" value="value"/>
						</html:select>&nbsp;&nbsp;
					</td>
					 <td>
						<html:button style="margin: 0px 0px 0px 0px;"  property="searchMap" styleClass="button_c" value ="Search" onclick="validate()">Search</html:button>
					</td>
				</tr>
				</table>
				<c:if test="${requestScope.searchedResult eq 'searchedResult'}">
					<table>
						<tr>
								<td colspan="7">
									<table border="0" cellpading="0" cellspacing="0">
										<tr>
											<td>
												<jsp:include page="RawvendexMapView.jsp"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
					</table>
				</c:if>
</html:form>
</body>
<script>
	function validate() {
	if(document.forms[0].endCustomer.value == ' ') {
		alert('Please select End Customer.')
		document.forms[0].endCustomer.focus();
		return false;
	}
	if(document.forms[0].zipCode.value == '') {
		alert('Please enter ZipCode.')
		document.forms[0].zipCode.focus();
		return false;
	}
		document.forms[0].action="RawVendexViewAction.do?hmode=SearchRawView&&appendixid=0&function=<%=request.getParameter("function")%>&viewjobtype=<%=request.getParameter("viewjobtype")%>&ownerId=<%=request.getParameter("ownerId")%>&initialCheck=true&pageType=mastersites"
		document.forms[0].submit();
		return true;
	}
</script>
</html:html>