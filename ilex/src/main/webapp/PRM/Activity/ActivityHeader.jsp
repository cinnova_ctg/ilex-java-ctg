<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id="type" name="type" />
<bean:define id="jobid" name="jobid" />
<%
String bgcolor=""; 
String bgcolortext="";
String bgcolortext1="";
String activityid = "";
String appendixid="";
String partner_id="0";
int addendumlistlength=0;
if( request.getParameter( "activityid" ) != null )
	activityid = request.getParameter( "activityid" );

if(request.getAttribute("appendixid")!=null)
	appendixid=request.getAttribute("appendixid").toString();

if(request.getAttribute("partner_id")!=null)
	partner_id=request.getAttribute("partner_id").toString();

addendumlistlength = ( int ) Integer.parseInt(request.getAttribute("addendumlistlength").toString());
%>


<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

</head>

<script>

function addaddendum(action, para)
{
	document.forms[0].action = "AddAddendumAction.do?type=activity&functionPara="+action+"&parameter="+para+"&activityid=<%= activityid %>";
	document.forms[0].target = "ilexmain";
	document.forms[0].submit();
	return true;	
}

function managestatus(v)
{
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_activity&id=<%= activityid %>";
	document.forms[0].submit();
	return true;	
}

function sendstatusreport()
{
	document.forms[0].action = "EmailAction.do?Type=prm_activity&Type1=prm_activity&id=<%= activityid %>";
	document.forms[0].submit();
	return true;
}



</script>


<%@ include  file="/Menu.inc" %>

<body>


<html:form action = "ActivityHeader">

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  class="toprow"> 
      <table  border="0" cellspacing="1" cellpadding="0" height="18">
        <tr> 
          <td id="pop1" width="120" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);popUp('elMenu1',event)" class="drop"><bean:message bundle="PRM" key="prm.manageActivity"/></a></td>
          <td  id="pop2" width="135" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" onmouseover="MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class="drop"><bean:message bundle="PRM" key="prm.managepartners"/></a></td>
                  
		  <td  id="pop3" width="170" class="toprow1"><a href="#"    onmouseout="MM_swapImgRestore(); popDown('elMenu3',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu3',event)" class="drop"><bean:message bundle="PRM" key="prm.managestatusreport"/></a></td>

		 <td  id="pop4" width="120" class="toprow1"><a href="#"    onmouseout="MM_swapImgRestore(); popDown('elMenu4',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu4',event)" class="drop"><bean:message bundle="PRM" key="prm.viewreports"/></a></td>
		  <td  id="pop5" width="148" class="toprow1"><a href="#"    onmouseout="MM_swapImgRestore(); popDown('elMenu5',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu5',event)" class="drop"><bean:message bundle="PRM" key="prm.manageaddendum"/></a></td>
		 
		   <td  id="pop6" width="148" class="toprow1"><a href="#"    onmouseout="MM_swapImgRestore(); popDown('elMenu6',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu6',event)" class="drop"><bean:message bundle="PRM" key="prm.goto"/></a></td>
			<td id = "pop7" width = "150" class = "toprow1"><a href = "MaterialUpdateAction.do?ref=View&Activity_Id=<%= activityid %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "drop"><bean:message bundle = "pm" key = "activity.detail.addresources"/></a></td>
		</tr>

      </table>
    </td>
  </tr>
</table>

<SCRIPT language=JavaScript1.2>

if (isMenu) {
 arMenu1=new Array(
110,
findPosX('pop1'),findPosY('pop1'),
"","",
"","",
"","",
<% if( request.getAttribute("jobtype").equals( "Default" )) {%>
	"<bean:message bundle = "PRM" key = "prm.changestatus"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotchanestatusfordefaultjobactivity"/>')",0,
	"<bean:message bundle = "PRM" key = "prm.updateschedule"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.CannotupdateJobschedulefordefaultjobactivity"/>')",0,
	"<bean:message bundle = "PRM" key = "prm.managesow"/>","ManageSOWAction.do?function=View&Type=PRM&Id=<%= activityid %>",0,
	"<bean:message bundle = "PRM" key = "prm.manageassumption"/>","ManageAssumptionAction.do?function=View&Type=PRM&Id=<%= activityid %>",0
<%}
else{%>
	<% if( ( String )request.getAttribute( "list" ) != "" ) {%>
	"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",1,
	<%} else {%>
	"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",0,
	<%}%>
	<% if( request.getAttribute("activitycurrentstatus").equals( "F" )) {%>
	"<bean:message bundle = "PRM" key = "prm.updateschedule"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotupdateactivityschedule"/>')",0,
	<%} else { %>
	"<bean:message bundle = "PRM" key = "prm.updateschedule"/>","ScheduleUpdate.do?function=update&typeid=<%=activityid%>&type=<%=type%>",0,
	<% } %>
	"<bean:message bundle = "PRM" key = "prm.managesow"/>","ManageSOWAction.do?function=View&Type=PRM&Id=<%= activityid %>",0,
	"<bean:message bundle = "PRM" key = "prm.manageassumption"/>","ManageAssumptionAction.do?function=View&Type=PRM&Id=<%= activityid %>",0
<%}%>
)

<% if( ( String )request.getAttribute( "list" ) != "" ) {%>
arMenu1_1=new Array(
<%= ( String ) request.getAttribute( "list" ) %>
)
<%} %>


arMenu2=new Array(
120,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
<% if( request.getAttribute("jobtype").equals( "Default" )) {%>
	"<bean:message bundle = "PRM" key = "prm.assignpartner"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotassignpartnerfordefaultjobactivity"/>')",0,
	"<bean:message bundle = "PRM" key = "prm.unassignpartner"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotunassignpartnerfordefaultactivities"/>')",0,
	"<bean:message bundle = "PRM" key = "prm.addponumber"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotaddponofordefaultjobactivity"/>')",0,
	"Add Partner Cost","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotaddpartnercostfordefault"/>')",0,
	"Manage Purchase Order","#",0,
	"Manage Work Order","#",0
<%}
else{%>
	<% if( request.getAttribute("activitycurrentstatus").equals( "F" )) {%>
	"<bean:message bundle = "PRM" key = "prm.assignpartner"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotassignpartneractivity"/>')",0,
	<%} else { %>
	"<bean:message bundle = "PRM" key = "prm.assignpartner"/>","Partner_Search.do?ref=search&typeid=<%=activityid%>&type=<%=type%>",0,
	<% } %>
	<% if(partner_id.equals("0")) {%>
	"<bean:message bundle = "PRM" key = "prm.unassignpartner"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.activitynotassigned"/>')",0,
	<%}else 
	{%>
	"<bean:message bundle = "PRM" key = "prm.unassignpartner"/>","UnassignPartner.do?typeid=<%= activityid%>&type=<%=type%>&pid=<%=partner_id%>",0,
	
	<%}%>
	"<bean:message bundle = "PRM" key = "prm.addponumber"/>","AddPoNumber.do?function=add&typeid=<%=activityid%>&type=<%=type%>",0,
	"Add Partner Cost","AddPartnerCost.do?type=<%=type%>&fromtype=Activity&id=<%= jobid%>&act_id=<%= activityid%>",0,
	"Manage Purchase Order","#",1,
	"Manage Work Order","#",1
<%}%>
)
arMenu2_5=new Array(
"View","javascript:viewpo('po');",0,
<% if( request.getAttribute( "checkforsendpowo" ).equals( "Y" ) ) { %>
"Send","javascript:alert( 'Cannot send PO as activity is complete' ) ;",0
<% }else {%>
"Send","javascript:sendpo( 'po' );",0
<%}%>
)

arMenu2_6=new Array(
"View","javascript:viewpo('wo');",0,
<% if( request.getAttribute( "checkforsendpowo" ).equals( "Y" ) ) { %>
"Send","javascript:alert( 'Cannot send WO as activity is complete' ) ;",0
<% }else {%>
"Send","javascript:sendpo( 'wo' );",0
<%}%>
)


arMenu3=new Array(
80,
findPosX('pop3'),findPosY('pop3'),
"","",
"","",
"","",
"Update","javascript:managestatus( 'update' )",0,
"View","javascript:managestatus( 'view' )",0,
"Send","javascript:sendstatusreport()",0
)


arMenu4=new Array(
115,
findPosX('pop4'),findPosY('pop4'),
"","",
"","",
"","",
"Redline Report","Under_Dev.jsp",0,
"Detailed Summary","Under_Dev.jsp",0,
"CVS Summary","Under_Dev.jsp",0
)

arMenu5=new Array(
90,
findPosX('pop5'),findPosY('pop5'),
"","",
"","",
"","",
<% if( request.getAttribute("jobtype").equals( "Default" )) {%>
	"<bean:message bundle = "PRM" key = "prm.view"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotviewfordefaultjobactivity"/>')",0
	

<%}
else{%>
	"<bean:message bundle = "PRM" key = "prm.view"/>","javascript:addaddendum( 'view' , 'view');",0
	
<%}%>

)

arMenu6=new Array(
140,
findPosX('pop6'),findPosY('pop6'),
"","",
"","",
"","",
"<bean:message bundle = "PRM" key = "prm.goto.appendixdashboard"/>","AppendixHeader.do?function=view&appendixid=<%=appendixid%>",0,
"<bean:message bundle = "PRM" key = "prm.goto.jobdashboard"/>","JobHeader.do?function=view&appendixid=<%=appendixid%>&jobid=<%= jobid%>",0

)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

function viewpo(t)
{
    window.location.href = "POAction.do?ordertype="+t+"&type=<%=type%>&fromtype=Activity&function_type=view&jobid=<%= jobid%>&activityid=<%= activityid%>";
	return false;	
}

function sendpo(t)
{
    window.location.href = "POAction.do?ordertype="+t+"&type=<%=type%>&fromtype=Activity&function_type=Send POWO&jobid=<%= jobid%>&activityid=<%= activityid%>";
	return false;	
}
</script>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="left" >
  <tr> 
   <td class="large" height="30"  colspan="3"><bean:message bundle="PRM" key="prm.activitydashboard"/><bean:write name="ActivityHeaderForm" property="appendixname" /><bean:message bundle="PRM" key="prm.job->"/><bean:write name="ActivityHeaderForm" property="jobname" /><bean:message bundle="PRM" key="prm.activity->"/><bean:write name="ActivityHeaderForm" property="activityname" /></font></td>
  </tr>

<tr>
<td width="30%" class="labellobold" >
	<table width="100%" border="0" cellspacing="1" cellpadding="1" align="left" class="textw">
	<tr>
	 <td class="labellobold" colspan="2"><bean:message bundle="PRM" key="prm.p&lsummay"/></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.materialscost"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="ActivityHeaderForm" property="materialscost" /></td>
    </tr>
    <tr>
	<td class="labelebold"><bean:message bundle = "PRM" key = "prm.cnslaborcost"/></td>
	<td class="readonlytextnumberodd"><bean:write name="ActivityHeaderForm" property="cnslaborcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold" ><bean:message bundle = "PRM" key = "prm.fieldlaborcost"/></td>
	 <td class="readonlytextnumberodd" ><bean:write name="ActivityHeaderForm" property="fieldlaborcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold" ><bean:message bundle = "PRM" key = "prm.freightcost"/></td>
	 <td class="readonlytextnumberodd" ><bean:write name="ActivityHeaderForm" property="freightcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold" ><bean:message bundle = "PRM" key = "prm.travelcost"/></td>
	 <td class="readonlytextnumberodd" ><bean:write name="ActivityHeaderForm" property="travelcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.estimatedcost"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="ActivityHeaderForm" property="estimatedcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.extendedsubtotal"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="ActivityHeaderForm" property="extendedsubtotal" /></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.extendedprice"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="ActivityHeaderForm" property="extendedprice" /></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.proformamargin"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="ActivityHeaderForm" property="proformamargin" /></td>
	</tr>
	<%if(addendumlistlength > 8)
		{ 
			for(int k=0;k<(addendumlistlength-8);k++)
			{%>
		 	<tr>
    	 	<td  class="labelebold" colspan="2">&nbsp;</td>
   		 	</tr>
   			<%}
		} %>
	<tr>
	 <td class="labelebold" colspan="2">&nbsp;</td>
	</tr>
	<tr>
	 <td class="labellobold" colspan="2"><bean:message bundle="PRM" key="prm.scheduleinformation"/></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle="PRM" key="prm.startdate"/></td>
	 <td class="readonlytextnumberodd"  ><bean:write name="ActivityHeaderForm" property="startdate" /></td>
     </tr>
	<tr>
	 <td class="labelebold"><bean:message bundle="PRM" key="prm.plannedenddate"/></td>
	 <td class="readonlytextnumberodd" ><bean:write name="ActivityHeaderForm" property="plannedenddate" /></td>
     </tr>
     <tr>
	 <td class="labelebold" colspan="2">&nbsp;</td>
	</tr>
   </table>
</td>


<td width="40%" class="labellobold" >
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="left" class="textw">
<tr>
 <td class="labellobold" colspan="4"><bean:message bundle="PRM" key="prm.changesummary"/></td>
</tr>
<tr>
 <td class="labelebold"><bean:message bundle="PRM" key="prm.resourceadded"/></td>
 <td class="labelebold"><bean:message bundle="PRM" key="prm.cost"/></td>
 <td class="labelebold"><bean:message bundle="PRM" key="prm.price"/></td>
 <td class="labelebold"><bean:message bundle="PRM" key="prm.margin"/></td>
</tr>

<logic:present  name="addmresourcelist">
    <logic:iterate id="arlist" name="addmresourcelist" >
<tr>
 <td  class="labelebold"><bean:write name="arlist" property="resourceaddendumname"/></td>
     <td  class="readonlytextnumberodd"><bean:write name="arlist" property="estimatedcost"/></td>
     <td  class="readonlytextnumberodd"><bean:write name="arlist" property="extendedprice"/></td>
     <td  class="readonlytextnumberodd"><bean:write name="arlist" property="proformamargin"/></td>
</tr>
</logic:iterate>
    </logic:present>

 <%if(addendumlistlength < 9)
		{ 
			for(int k=addendumlistlength;k<8;k++)
			{%>
		 	<tr>
    	 	<td  class="labelebold" colspan="4">&nbsp;</td>
   		 	</tr>
   			<%}
		} %> 

 <tr>
 <td class="labelebold" colspan="4">&nbsp;</td>
</tr> 


<tr>
 <td class="labellobold" colspan="4"><bean:message bundle="PRM" key="prm.partnerassigned"/></td>
</tr>
<tr>
 <td class="labelebold" colspan="4"><bean:write name="ActivityHeaderForm" property="partnerassigned" /></td>
</tr>

<tr>
 <td class="labelebold" colspan="4">&nbsp;</td>
</tr>
<tr>
 <td class="labelebold" colspan="4">&nbsp;</td>
</tr>






</table>

</td>

<td width="30%" class="labellobold" >
	<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td class="labellobold" ><bean:message bundle="PRM" key="prm.resourcestatussummary"/></td>
	</tr>
	<tr>
	<td>
	<IFRAME  SRC="ViewGraph.do?graphtype=resourcegraph&activityid=<%=activityid%>&jobid=<%=jobid%>" TITLE="" marginwidth=0 marginheight=0  frameborder=0 bgcolor="#ECECEC" height=200 width=290>
	</IFRAME>
	</td>
	</tr>
	</table>
</td>

</tr>
<tr>
 <td class="labele" colspan="2">&nbsp;</td>
 <td class="labelebold" ></td>
</tr>


<tr>
	<td colspan="3">
    	<table border = "0" cellspacing = "1" cellpadding = "1" >
    
	    <tr>
	    <td class="labeleboldwhite" colspan="13" height="30"><bean:message bundle = "PRM" key = "prm.material"/></td>
	    </tr>
		
		<tr> 
		    
		    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.name"/></td>
		    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.type"/></td>
			<td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.manufacturer"/></td>
			<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.cnspartno"/></td>
			<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.quantity"/></td>
			<td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.estimatedcost"/></td>
			<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.proformamargin"/></td>
		    <td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.extendedprice"/></td>
		    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.deliverdate"/></td>
			<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.status"/></b></td>
		  </tr>
		<tr> 
			 <td class="tryb"> 
		      <div align="center"><bean:message bundle = "PRM" key = "prm.name"/></div>
		    </td>
		    <td class="tryb"> 
		      <div align="center"><bean:message bundle = "PRM" key = "prm.partnumber"/></div>
		    </td>
		    <td class="tryb"> 
		      <div align="center"><bean:message bundle = "PRM" key = "prm.unit"/></div>
		    </td>
		    <td class="tryb"> 
		      <div align="center"><bean:message bundle = "PRM" key = "prm.total"/></div>
		    </td>
		    <td class="tryb"> 
		      <div align="center"><bean:message bundle = "PRM" key = "prm.unit"/></div>
		    </td>
		    <td class="tryb"> 
		      <div align="center"><bean:message bundle = "PRM" key = "prm.extended"/></div>
		    </td>
		</tr>
		<logic:present name="materiallist">
		<%int i=1;	%>
		
			<logic:iterate id="mlist" name="materiallist" >
			<%if((i%2)==0)
			{
				bgcolor="readonlytextnumbereven";
				bgcolortext="readonlytexteven"; 
				bgcolortext1="hypereven"; 
			
			} 
			else
			{
				bgcolor="readonlytextnumberodd";
				bgcolortext="readonlytextodd";
				bgcolortext1="hyperodd";  
			}%>
		
		<tr> 
		   
		    <td  class="<%=bgcolortext1 %>"><bean:write name="mlist" property="resourcename" /></td>
		    <td  class="<%=bgcolortext %>"><bean:write name="mlist" property="resourcetype" /></td>
		    <td  class="<%=bgcolortext %>"><bean:write name="mlist" property="manufacturername" /></td>
		    <td  class="<%=bgcolor %>"><bean:write name="mlist" property="manufacturerpartno" /></td>
		    <td  class="<%=bgcolor %>"><bean:write name="mlist" property="cnspartno" /></td>
		    <td  class="<%=bgcolor %>"><bean:write name="mlist" property="qty" /></td>
		    <td  class="<%=bgcolor %>"><bean:write name="mlist" property="ecostunit" /></td>
		    <td  class="<%=bgcolor %>"><bean:write name="mlist" property="ecosttotal" /></td>
			<td  class="<%=bgcolor %>"><bean:write name="mlist" property="proformamargin" /></td>
			<td  class="<%=bgcolor %>"><bean:write name="mlist" property="epriceunit" /></td>
			<td  class="<%=bgcolor %>"><bean:write name="mlist" property="epriceextended" /></td>
			<td  class="<%=bgcolor %>"></td>
			<td  class="<%=bgcolortext %>"><bean:write name="mlist" property="status" /></td>
			
		</tr>
		
				
		<%i++; %>
			</logic:iterate>
	    </logic:present>
		<tr class = "buttonrow"> 
		    <td colspan="13" height="30"> 
			  <html:button property = "sort" styleClass = "button" onclick = "return sortwindow(1);"><bean:message bundle = "pm" key = "msa.tabular.sort"/></html:button> 
		    </td>
		</tr> 
		 
		
	
	
	
    <tr>
    <td class="labeleboldwhite" colspan="13" height="30"><bean:message bundle = "PRM" key = "prm.labor"/></td>
    </tr>
	
	<tr> 
	    
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.name"/></td>
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.type"/></td>
		<td class="tryb" colspan="2" rowspan="2"><bean:message bundle = "PRM" key = "prm.cnspartno"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.qtyhours"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.unionuplift"/></td>
		<td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.estimatedcost"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.proformamargin"/></td>
	    <td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.extendedprice"/></td>
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.deliverdate"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.status"/></b></td>
	  </tr>
	<tr> 
		
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.hourlybase"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.total"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.unit"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.extended"/></div>
	    </td>
	</tr>
	<logic:present name="laborlist">
	<%int j=1;	%>
	
		<logic:iterate id="llist" name="laborlist" >
		<%if((j%2)==0)
		{
			bgcolor="readonlytextnumbereven";
			bgcolortext="readonlytexteven"; 
			bgcolortext1="hypereven";
		
		} 
		else
		{
			bgcolor="readonlytextnumberodd";
			bgcolortext="readonlytextodd"; 
			bgcolortext1="hyperodd";
		}%>
	
	<tr> 
	   
	    <td  class="<%=bgcolortext1 %>"><bean:write name="llist" property="resourcename" /></td>
	    <td  class="<%=bgcolortext %>"><bean:write name="llist" property="resourcetype" /></td>
	    <td  class="<%=bgcolor %>" colspan="2" ><bean:write name="llist" property="cnspartno" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="llist" property="qtyhours" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="llist" property="unionuplift" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="llist" property="ecosthourlybase" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="llist" property="ecosttotal" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="llist" property="proformamargin" /></td>
		<td  class="<%=bgcolor %>"><bean:write name="llist" property="epriceunit" /></td>
		<td  class="<%=bgcolor %>"><bean:write name="llist" property="epriceextended" /></td>
		<td  class="<%=bgcolor %>"></td></td>
		<td  class="<%=bgcolortext %>"><bean:write name="llist" property="status" /></td>
		
	</tr>
	
			
	<%j++; %>
		</logic:iterate>
    </logic:present> 
	<tr class = "buttonrow"> 
	    <td colspan="14" height="30"> 
		  <html:button property = "sort" styleClass = "button" onclick = "return sortwindow(2);"><bean:message bundle = "pm" key = "msa.tabular.sort"/></html:button> 
	    </td>
	</tr> 
	
		
	 
	
	
    <tr>
    <td class="labeleboldwhite" colspan="13" height="30"><bean:message bundle = "PRM" key = "prm.freight"/></td>
    </tr>
	
	<tr> 
	    
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.name"/></td>
	    <td class="tryb" colspan="2" rowspan="2"><bean:message bundle = "PRM" key = "prm.type"/></td>
		<td class="tryb" colspan="2" rowspan="2"><bean:message bundle = "PRM" key = "prm.cnspartno"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.quantity"/></td>
		<td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.estimatedcost"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.proformamargin"/></td>
	    <td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.extendedprice"/></td>
	   	<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.deliverdate"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.status"/></b></td>
	  </tr>
	<tr> 
		
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.unit"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.total"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.unit"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.extended"/></div>
	    </td>
	</tr>
	<logic:present name="freightlist">
	<%int k=1; %>
	
		<logic:iterate id="flist" name="freightlist" >
		<%if((k%2)==0)
		{
			bgcolor="readonlytextnumbereven";
			bgcolortext="readonlytexteven"; 
			bgcolortext1="hypereven"; 
		
		} 
		else
		{
			bgcolor="readonlytextnumberodd";
			bgcolortext="readonlytextodd"; 
			bgcolortext1="hyperodd";
		}%>
	
	<tr> 
	   
	    <td  class="<%=bgcolortext1 %>"><bean:write name="flist" property="resourcename" /></td>
	    <td  class="<%=bgcolortext %>" colspan="2" ><bean:write name="flist" property="resourcetype" /></td>
	    <td  class="<%=bgcolor %>" colspan="2" ><bean:write name="flist" property="cnspartno" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="flist" property="qty" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="flist" property="ecostunit" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="flist" property="ecosttotal" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="flist" property="proformamargin" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="flist" property="epriceunit" /></td>
		<td  class="<%=bgcolor %>"><bean:write name="flist" property="epriceextended" /></td>
		<td  class="<%=bgcolor %>"></td>
		<td  class="<%=bgcolortext %>"><bean:write name="flist" property="status" /></td>
		
	</tr>
	
			
	<%k++; %>
		</logic:iterate>
    </logic:present> 
	<tr class = "buttonrow"> 
	    <td colspan="14" height="30"> 
		  <html:button property = "sort" styleClass = "button" onclick = "return sortwindow(3);"><bean:message bundle = "pm" key = "msa.tabular.sort"/></html:button> 
	    </td>
	</tr> 
	
		
	
    <tr>
    <td class="labeleboldwhite" colspan="13" height="30"><bean:message bundle = "PRM" key = "prm.travel"/></td>
    </tr>
	
	<tr> 
	    
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.name"/></td>
		<td class="tryb" colspan="2" rowspan="2"><bean:message bundle = "PRM" key = "prm.type"/></td>
		<td class="tryb" colspan="2" rowspan="2"><bean:message bundle = "PRM" key = "prm.cnspartno"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.quantity"/></td>
		<td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.estimatedcost"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.proformamargin"/></td>
	    <td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.extendedprice"/></td>
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.deliverdate"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.status"/></b></td>
	  </tr>
	<tr> 
		 
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.unit"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.total"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.unit"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.extended"/></div>
	    </td>
	</tr>
	<logic:present name="travellist">
	<%int m=1; %>
	
		<logic:iterate id="tlist" name="travellist" >
		<%if((m%2)==0)
		{
			bgcolor="readonlytextnumbereven";
			bgcolortext="readonlytexteven"; 
			bgcolortext1="hypereven";
		
		} 
		else
		{
			bgcolor="readonlytextnumberodd";
			bgcolortext="readonlytextodd";
			bgcolortext1="hyperodd"; 
		}%>
	
	<tr> 
	   
	    <td  class="<%=bgcolortext1 %>"><bean:write name="tlist" property="resourcename" /></td>
	    <td  class="<%=bgcolortext %>" colspan="2" ><bean:write name="tlist" property="resourcetype" /></td>
	    <td  class="<%=bgcolor %>" colspan="2" ><bean:write name="tlist" property="cnspartno" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="tlist" property="qty" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="tlist" property="ecostunit" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="tlist" property="ecosttotal" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="tlist" property="proformamargin" /></td>
	    <td  class="<%=bgcolor %>"><bean:write name="tlist" property="epriceunit" /></td>
		<td  class="<%=bgcolor %>"><bean:write name="tlist" property="epriceextended" /></td>
		<td  class="<%=bgcolor %>"></td>
		<td  class="<%=bgcolortext %>"><bean:write name="tlist" property="status" /></td>
		
	</tr>
	
			
	<%m++; %>
		</logic:iterate>
    </logic:present> 
	<tr class = "buttonrow"> 
	    <td colspan="14" height="30"> 
		  <html:button property = "sort" styleClass = "button" onclick = "return sortwindow(4);"><bean:message bundle = "pm" key = "msa.tabular.sort"/></html:button> 
	    </td>
	</tr> 
	
	<jsp:include page = '/Footer.jsp'>
  		 <jsp:param name = 'colspan' value = '28'/>
    	 <jsp:param name = 'helpid' value = 'roleprivilege'/>
 	 </jsp:include>
	
	</table>
	</td>
	</tr>   
   </table>
  </td>
 </tr>
</table>



</body>
</html:form>
<script>
var str = '';

function sortwindow(rtype)
{
	if(rtype==1)
	{
		str = 'SortAction.do?Type=prj_materialresource&activityid=<%=activityid%>';
	}
	if(rtype==2)
	{
		str = 'SortAction.do?Type=prj_laborresource&activityid=<%=activityid%>';
	}
	if(rtype==3)
	{
		str = 'SortAction.do?Type=prj_freightresource&activityid=<%=activityid%>';
	}
	if(rtype==4)
	{
		str = 'SortAction.do?Type=prj_travelresource&activityid=<%=activityid%>';
	}
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); */ 
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	return true;
	
}
</script>

</html:html>