<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>


<html:html>

<head>

	
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	
</head>
<%
	String backgroundclass = null;
	String labelclass = null;
	boolean csschooser = true;
	String fromProposalMg="No";
	
	int i = 0;
	
	String msaid = "";

	if( request.getAttribute( "msaid" ) != null )
	{
		msaid = request.getAttribute( "msaid" ).toString();
	}

	if(request.getAttribute("fromProposalMg")!=null)
	{

		fromProposalMg = (String)request.getAttribute("fromProposalMg");
		request.setAttribute("fromProposalMg","Yes");

	}



	
	
%>






<table>
<html:form action = "/UploadCSV">
<html:hidden property="viewjobtype" />
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			<td width="2" height="0"></td>
  			 <td>
  			 	<table width = "80%" border = "0" cellspacing = "1" cellpadding = "1" width = "400"> 
					 
					 <tr height = "20">
					 	<td colspan = "5" />
					 </tr>
				<!-- Start :Added By Amit -->	 
				<tr>
	             <td colspan = "4" class = "labeleboldwhite" height = "30"><bean:message bundle = "PRM" key = "prm.UploadCSV.uploadSitesResult"/>&nbsp;<bean:message bundle="PRM" key="prm.dashboard.msa"/>&nbsp;<bean:write name="UploadCSVForm" property="msaname" /></td>
	             </tr> 
				<!-- End :Added By Amit -->	 					
					 
					 
					 <logic:present name = "errorlist" scope = "request">
	       		     	 	<tr>
	       		     	 		<td  width = "60" class = "tryb" height = "20"><bean:message bundle = "PRM" key = "prm.UploadCSV.rowNumber"/></td>
	       		     	 		<td  width = "340" class = "tryb"><bean:message bundle = "PRM" key = "prm.UploadCSV.result"/></td>
	       		     	 	</tr>
	       		     	 
	       		     	 
	       		     	 <logic:iterate id = "errorlist" name = "errorlist" scope = "request">
	       		     		
	       		     		<%
		       		     		if ( csschooser == true ) 
								{
									backgroundclass = "texto";
									
									csschooser = false;
									labelclass = "readonlytextodd";
									
								}
						
								else
								{
									csschooser = true;	
									backgroundclass = "texte";
									labelclass = "readonlytexteven";
								}
							%>
	       		     		
	       		     		
	       		     		
	       		     		<logic:equal name = "errorlist"  property = "rowerror" value = "success">
	       		     		<tr>
		       		     			<td class = "<%=  backgroundclass %>" height = "20">
		       		     				<bean:write name = "errorlist" property = "rowno" />
		       		     			</td>
		       		     			
		       		     			<td class = "<%=  labelclass %>">
		       		     				<bean:message bundle = "PRM" key = "prm.UploadCSV.rowInsertedSuccessfully"/>
		       		     			</td>
		       		     		</tr>
	       		     		</logic:equal>
	       		     		
	       		     		
	       		     		<logic:notEqual name = "errorlist"  property = "rowerror" value = "success">
	       		     		
		       		     		<tr>
		       		     			<td class = "<%=  backgroundclass %>" height = "20">
		       		     				<bean:write name = "errorlist" property = "rowno" />
		       		     			</td>
		       		     			
		       		     			<td class = "<%=  labelclass %>">
		       		     				<bean:write name = "errorlist" property = "rowerror" />
		       		     			</td>
		       		     		</tr>
		       		     	</logic:notEqual>
		       		     	
	       		     	</logic:iterate>
       		        </logic:present>		
		    			
	    	
					
					
				   <tr>
					    <td  height = "20" colspan = "2"> 
					    <%if(fromProposalMg.equalsIgnoreCase("Yes")){ %>
					    <html:button property="back" styleClass="button" onclick = "javascript:history.go(-1);"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
						<%}else{ %>
							<html:button property = "back" styleClass = "button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
						<%} %>
					   		<!--<html:button property = "print" styleClass = "button" onclick = "javascript:window.print();">Print</html:button> -->
					   </td>
					</tr>

					

				</table>
			</td>
		</tr>
	</table>

</table>
</html:form>
<script>
function Backactionjob()
{
	document.forms[0].action = "UploadCSV.do?msaid=<%= msaid %>";
	document.forms[0].submit();
	return true;
}
</script>

</html:html>