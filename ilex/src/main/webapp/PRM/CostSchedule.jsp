<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());

%>
<%@ include  file="/NMenu.inc" %>
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AjaxOwners.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');leftAdjLayers();" >
<html:form action="CostSchedule" >
<html:hidden property = "viewjobtype" />
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property="appendixid"/>
<%@ include  file="/AppendixDashboardMenuScript.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		       <tr>
	 				<td colspan="4"><h2><bean:message bundle="PRM" key="prm.scheduleinformation"/>&nbsp;<bean:message bundle="PRM" key="prm.forAppendix"/> :<bean:write name="CostScheduleForm" property="appendixname"/></h2></td>
	 		   </tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="CostScheduleForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="CostScheduleForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="CostScheduleForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					           <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>

<table  border="0" cellspacing="0" cellpadding="1" width="600">
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="0" width="540">
			     
			  <tr> 
				<td class="tryb" colspan="1" rowspan="2" width="60"><bean:message bundle="PRM" key="prm.projectname"/></td>
				<td class="tryb" colspan="1" rowspan="2" width="60"><bean:message bundle="PRM" key="prm.jobname"/></td>
			<!--  <td class="tryb" colspan="1" rowspan="2"><bean:message bundle="PRM" key="prm.activityname"/></td>
			  --> 
			    <td class="tryb" colspan="2" width="120"><bean:message bundle="PRM" key="prm.plannedschedule"/></td>
			    <td class="tryb" colspan="2" width="120"><bean:message bundle="PRM" key="prm.actualschedule"/></td>
				<td class="tryb" rowspan="2" width="60"><bean:message bundle="PRM" key="prm.status"/></td>
				<td class="tryb" rowspan="2" width="60"><bean:message bundle="PRM" key="prm.deviation"/></td>
			  </tr>
			  <tr> 
			   
			    <td class="tryb"> 
			      <div align="center" width="60"><bean:message bundle="PRM" key="prm.start"/></div>
			    </td>
				<td class="tryb"> 
			      <div align="center" width="60"><bean:message bundle="PRM" key="prm.end"/></div>
			    </td>
			    <td class="tryb"> 
			      <div align="center" width="60"><bean:message bundle="PRM" key="prm.start"/></div>
			    </td>
			    <td class="tryb"> 
			      <div align="center" width="60"><bean:message bundle="PRM" key="prm.end"/></div>
			    </td>
			  </tr>
			  
			  <logic:present  name="costschedulelist">
			  
			    <%int j=1;
				String bgcolor="";
				String bgcolortext="";
				String bgcolor1=""; %>
				
			   <logic:iterate id="cslist" name="costschedulelist" >
			  
					<%if((j%2)==0)
					{
						bgcolor="readonlytextnumbereven";
						bgcolortext="readonlytexteven";
						bgcolor1="texte";
					
					} 
					else
					{
						bgcolor="readonlytextnumberodd";
						bgcolortext="readonlytextodd";
						bgcolor1="texto";
					}%>
						  
			  <tr> 
			    <td class="<%=bgcolortext %>"><bean:write name="cslist" property="appendixname"/></td>
			   
			    <td class="<%=bgcolortext %>"><bean:write name="cslist" property="jobname"/></td>
			   
			    <html:hidden name="cslist" property="activityname"/>
			    
			    
			    <td class="<%=bgcolor1 %>"><bean:write name="cslist" property="plannedstartdate"/></td>
			   
			    <td class="<%=bgcolor1 %>"><bean:write name="cslist" property="plannedenddate"/></td>
			    
			    <td class="<%=bgcolor1 %>"><bean:write name="cslist" property="actualstartdate"/></td>
			    
			    <td class="<%=bgcolor1 %>"><bean:write name="cslist" property="actualenddate"/></td>
			   
			    <td class="<%=bgcolortext %>"><bean:write name="cslist" property="status"/></td>
			    
			    <td class="<%=bgcolor %>"><bean:write name="cslist" property="deviation"/></td>
			    
			   </tr>
			   
			      <%j++; %>
  				</logic:iterate>
	 		  </logic:present>
	  
	 		<tr> 
	 		 	  <td colspan="9" class="buttonrow"> 
	 		      <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  </td>
			 </tr>
		 	<jsp:include page = '/Footer.jsp'>
	  	 			<jsp:param name = 'colspan' value = '28'/>
	   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
	  		 </jsp:include>   		  
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
</html:form>
<body>
<script>
function Backaction()
{
	document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getParameter("appendixid") %>&function=view&viewjobtype="+document.forms[0].viewjobtype.value;
	document.forms[0].submit();
	return true;
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="CostSchedule.do?showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="CostSchedule.do?resetList=something";
	document.forms[0].submit();
	return true;

}
</script>

</html:html>