<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id="dcEndCustomerList" name="dcEndCustomerList" scope="request"/>
<bean:define id="dcGroupList" name="dcGroupList" scope="request"/>
<bean:define id="dcStatusList" name ="dcStatusList"  scope = "request"/>


<%
int listSize = 0;
int count = 0;

if(request.getAttribute("siteDetailListSize")!=null) {
	listSize = Integer.parseInt(request.getAttribute("siteDetailListSize").toString());
if(request.getAttribute("count")!=null)
	 count = Integer.parseInt(request.getAttribute("count").toString());

}
%>
<html:html>

<head>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ILEX</title>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<link rel = "stylesheet" href = "styles/content.css" type = "text/css">
<link rel = "stylesheet" href="styles/summary.css" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>


<script language="javascript">
function specialCase()
{
	var str = new String("");
	var newstr = new String("");
	str = document.forms[0].siteSearchEndCustomer.value;
	var newstr = str.replace('&','$');
	document.forms[0].specialEndCustomer.value = newstr;
}

function moveToCurrentPage()
{
	document.forms[0].next.value= "";
	document.forms[0].last.value= "";
	document.forms[0].first.value= "";
	document.forms[0].previous.value= "";
	
	
	
	if(document.forms[0].current_page_no.value!="")
		{
			
			var re2digit=/^\d+$/ 
			if(document.forms[0].current_page_no.value.search(re2digit)==-1) 
			{
			alert("Only positive numbers are allowed.");
			document.forms[0].current_page_no.value = "";
			document.forms[0].current_page_no.focus();
			return false;
			}
			
			document.forms[0].current_page_no.value = eval(document.forms[0].current_page_no.value);
			
			
			if(eval(document.forms[0].current_page_no.value) == 0 )
			{
				if(eval(document.forms[0].current_page_no.value) == eval(document.forms[0].total_page_size.value))
				return false;
			}
			
			if(eval(document.forms[0].current_page_no.value) > eval(document.forms[0].total_page_size.value))
			{
			alert("Entered page cannot be greater than total page size.");
			document.forms[0].current_page_no.value = "";
			document.forms[0].current_page_no.focus();
			return false; 
			}
			if(eval(document.forms[0].current_page_no.value=="1"))
			{
	
				return false;
			}
		}
		
	document.forms[0].action="SiteManagement.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>";
	document.forms[0].submit();
	return true;	
}


function onGo()
{
	
	document.forms[0].previous.value= "";
	document.forms[0].last.value= "";
	document.forms[0].next.value= "";
	document.forms[0].first.value= "";
	
	if(document.forms[0].lines_per_page.value!="")
	
		{
			var re2digit=/^\d+$/ 
			if (document.forms[0].lines_per_page.value.search(re2digit)==-1) 
			{
			alert("Only positive numbers are allowed.");
			document.forms[0].lines_per_page.value = "";
			document.forms[0].lines_per_page.focus();
			return false;
			}
			if(eval(document.forms[0].lines_per_page.value)>eval(document.forms[0].row_size.value))
			{
			}
			
					
		}
		
	document.forms[0].action="SiteManagement.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>&search=go";
	document.forms[0].submit();
	return true;	
}


function first_record()
{
	
	document.forms[0].first.value= "Yes";
	document.forms[0].last.value= "";	
	document.forms[0].next.value= "";
	document.forms[0].previous.value = "" ;
	if(eval(document.forms[0].current_page_no.value=="1"))
	{
		return false;
	}
	if(eval(document.forms[0].current_page_no.value=="0"))
	{
		return false;
	}
	document.forms[0].action="SiteManagement.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>";
	document.forms[0].submit();
	return true;	
}
function last_record()
{
	
	document.forms[0].last.value= "Yes";
	document.forms[0].first.value= "";
	document.forms[0].next.value= "";
	document.forms[0].previous.value= "";
	
	if(eval(document.forms[0].current_page_no.value)== eval(document.forms[0].total_page_size.value))
	{
	
		return false;
	}
	document.forms[0].action="SiteManagement.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>";
	document.forms[0].submit();
	return true;	
}
function next_record()
{
	
	
	document.forms[0].next.value= "Yes";
	document.forms[0].last.value= "";
	document.forms[0].first.value= "";
	document.forms[0].previous.value= "";
	
	
	if(eval(document.forms[0].current_page_no.value)== eval(document.forms[0].total_page_size.value))
	{

		return false;
	}
	
	
	if (eval(document.forms[0].current_page_no.value) >= eval(document.forms[0].total_page_size.value))
		return false; 
		
	document.forms[0].action="SiteManagement.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>";
	document.forms[0].submit();
	return true;	
}
function previous_record()
{
	
	document.forms[0].previous.value= "Yes";
	document.forms[0].last.value= "";
	document.forms[0].next.value= "";
	document.forms[0].first.value= "";
	
	
	if(eval(document.forms[0].current_page_no.value=="1"))
	{
	
		return false;
	}
	
	if (document.forms[0].current_page_no.value < 2)
		return false; 
		
	document.forms[0].action="SiteManagement.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>";
	document.forms[0].submit();
	return true;	
}

function sortwindow()
{
	
	str = 'SortAction.do?appendixid=<bean:write name="SiteManagementForm" property="appendixid"/>&countRow=<bean:write name = "SiteManagementForm" property = "countRow"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "siteSearchEndCustomer"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&site_msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&Type=siteManage';
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	return true;
}
function del(siteId) 
{	
		var ans = confirm( "Are you sure you want to delete the site?" );	
		if( ans )
		{
			document.forms[0].action = "SiteManage.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&siteID="+siteId+"&method=Delete";
			document.forms[0].submit();
			return true;	
		}
}

function AddSite()
{
document.forms[0].action = "SiteManage.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>&method=Add";
document.forms[0].submit();
return true;
}

function toggleMenuSection(unique) {
	
	var divid="div_" + unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	try{
	if (thisDiv==null) { return ; }
	}catch(e){ }
	
	
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) 	thisImage.src = "images/Collapse.gif";    
	else thisImage.src = "images/Expand.gif";
}
function toggleDiv(divName) {
	 var tcookie = document.cookie;
     var st = tcookie.indexOf ("A1Db");
     var cookievalue ="";
	  if(st!= -1) {
	      st = st+3;
	      st = tcookie.indexOf ("=", st);
	      en = tcookie.indexOf (";", st);
		  if (en > st) cookievalue = tcookie.substring(st+1, en); //div's that is reopen
	 }
	var id = document.getElementById(divName);
	if( id.style.display == "none" ) {
			id.style.display = "block";
			//add it to the existing cookie opendiv paraneter
			if (cookievalue.length>0)	cookievalue = cookievalue+','+divName;
			else	cookievalue = divName;
			if(cookievalue!="") { // Save as cookie
				document.cookie = "ADb="+ cookievalue+" ; ";
          } }
	else {
			id.style.display = "none";	
			var newcookievalue='';
			//remove this div from the opendiv cookie
			if (cookievalue.length>0) {
				var array = cookievalue.split(',');
				if (array.length>1) {
					for(i=0;i<array.length;i++) {
						if(array[i]==divName) { }
						else {
							if(newcookievalue.length > 0 )
								newcookievalue = newcookievalue+','+array[i];
							else
								newcookievalue = array[i];	
						}
					}
				}
				if(newcookievalue!="") { // Save as cookie
			 		document.cookie = "ADb="+ newcookievalue+" ; ";
			 	}
			}
		}
	if( divName == 'div_1')
	{
		id = document.getElementById('div_13');
		if( id.style.display == "none" ) id.style.display = "block";
		else id.style.display = "none";	
		id = document.getElementById('div_11');
		if( id.style.display == "none" ) id.style.display = "block";
		else id.style.display = "none";	
	}
}
function toggleMenuSectionView(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	action = "toggleType = toggleDivView('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) 	thisImage.src = "images/Collapse.gif";
	else thisImage.src = "images/Expand.gif";
}
function toggleDivView(divName) {
	 var tableRow = document.getElementsByTagName('tr');
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if (tableRow[k].getAttributeNode('id').value==divName) {
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	}
}

function createCookie()
{
 var tableRow = document.getElementsByTagName('tr');
  var openDiv="";
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if((tableRow[k].getAttributeNode('id').value != "")&&(tableRow[k].getAttributeNode('id').value != "account"))
		{
			if(tableRow[k].style.display == "block" ){
			openDiv=openDiv+tableRow[k].getAttributeNode('id').value+",";
			}
		}
	}
		if(openDiv!="") { // Save as cookie
			 document.cookie = "ADb="+ openDiv+" ; ";
		 }
}
function createCookie(lastDivForFocus) {
 		document.cookie = "A1Db="+ lastDivForFocus+" ; ";
}
//following function would be called on OnLoad
function CookieGetOpenDiv() {
  var tcookie = document.cookie;
  var unique;
  var st = tcookie.indexOf ("ADb");
  var cookievalue ="";
  if(st!= -1) {
      st = st+3;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);
	  if (en > st) {
          cookievalue = tcookie.substring(st+1, en);
        }
       if(cookievalue.length>0) {
		var opendiv = cookievalue.split(',');
		if(opendiv.length>0) {
		  for (var j = 0; j < opendiv.length; j++) {
		   		if(eval("document.getElementById('"+opendiv[j]+"')")!=null)	{ //Added This if condition by amit
		  				eval("document.getElementById('"+opendiv[j]+"')").style.display = "block";
						unique = opendiv[j].substring(opendiv[j].indexOf("_")+1,opendiv[j].length);
						action = "thisImage = document.getElementById('img_" + unique +"');";
						eval(action);
					if (document.getElementById('div_' + unique).offsetHeight > 0) {
							thisImage.src = "images/Collapse.gif";
		   			} else {
							thisImage.src = "images/Expand.gif";
					}
				}
				if(opendiv[j]=='div_1') {
						document.getElementById('div_11').style.display = "block";
						document.getElementById('div_13').style.display = "block";
				}												
		  }
		}
	}
  }
	  openDiv = "";
	
	  document.cookie = "A1Db="+ openDiv+" ; "; 
		
	  var st1 = tcookie.indexOf ("A1Db");
	  var cookievalue1 ="";
	  if(st1!= -1) {
	      st1 = st1+4;
	      st1 = tcookie.indexOf ("=", st1);
	      en = tcookie.indexOf (";", st1);
		  if (en > st1) {
	          cookievalue1 = tcookie.substring(st1+1, en);
	        }
		}
		 if(cookievalue1!="") {  
		  		if(document.getElementById(cookievalue1)!=null) {
		 	      document.getElementById(cookievalue1).focus();
		    	}
		}
		document.cookie = "A1Db="+ openDiv+" ; ";
}

function updateSiteInfo(siteId){
	
	var url ='SiteManage.do?method=Modify&countRow=<bean:write name= "SiteManagementForm" property= "countRow"/>&siteID='+siteId+'&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber='+ encodeURIComponent('<bean:write name = "SiteManagementForm" property = "siteSearchNumber" />')+'&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "SiteManagementForm" property = "current_page_no"/>&org_page_no=<bean:write name = "SiteManagementForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "SiteManagementForm" property = "total_page_size"/>&ownerId=<bean:write name = "SiteManagementForm" property = "ownerId"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "specialEndCustomer"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>';
	document.forms[0].action=url;
	document.forms[0].submit();
			
}



</script>
<SCRIPT>
function gettingAjaxDataForInsert(siteId){
//	 alert(siteId);
   var ajaxRequest;  // The variable that makes Ajax possible!
   try{
          // Opera 8.0+, Firefox, Safari
          ajaxRequest = new XMLHttpRequest();
   } catch (e){
          // Internet Explorer Browsers
          try{
                  ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
          } catch (e) {
                  try{
                          ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                  } catch (e){
                     
                          alert("Your browser broke!");
                          return false;
                  }
          }
   }
   
   
   // Create a function that will receive data sent from the server
   ajaxRequest.onreadystatechange = function(){
          if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
          		var response = ajaxRequest.responseText;
          		if(response!=""){
              		}
          }
   }
   var sitechk = document.getElementById("flag"+siteId).checked;
  // alert(sitechk);
   var sitestatus="";
	if(sitechk==true){
		  	ajaxRequest.open("POST", "SiteManagement.do?Psiteflag="+sitechk+"&SiteId="+siteId+"&add=Add", true);
			ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			
			ajaxRequest.send("add=Add&Psiteflag="+sitechk); 	
		
		
	}else{
		
		
	  	ajaxRequest.open("POST", "SiteManagement.do?Psiteflag="+sitechk+"&SiteId="+siteId+"&add=Del", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("add=Del&Psiteflag="+sitechk); 	
		
		
	}
	
  

	
}



</SCRIPT>

<%@ include  file="/NMenu.inc" %>
<%@ include  file="/AjaxOwners.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


%>
</head>




<BODY onLoad="MM_preloadImages('images/Expand.gif','images/Collapse.gif');leftAdjLayers();validate();" >

<html:form action="SiteManagement"> 
<html:hidden property="appendixid"/>
<html:hidden property="appendixname"/>
<html:hidden property="msaname"/>
<html:hidden property="countRow"/>
<html:hidden property ="ownerId"/>
<html:hidden property ="org_page_no"/>
<html:hidden property ="org_lines_per_page"/>
<html:hidden property ="first" />
<html:hidden property ="last" />
<html:hidden property ="previous" />
<html:hidden property ="next" />
<html:hidden property = "row_size"/>
<html:hidden property = "pageType"/>
<html:hidden property = "specialEndCustomer"/>
<html:hidden property="jobOwnerOtherCheck"/> 
<!--<html:hidden property = "siteSearchGroup"/> -->
<!--  <html:hidden property ="completeSiteList"/>   -->

<% int k = 1; %>

<logic:equal name = "SiteManagementForm"  property ="pageType" value = "jobsites">
<logic:notEqual name = "SiteManagementForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AppendixDashboardMenuScript.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
	 					<td colspan="4"><h2><bean:message bundle = "PRM" key = "prm.site.manage"/>&nbsp;<bean:write name="SiteManagementForm" property="msaname" />&nbsp;&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp;<a  href="AppendixHeader.do?function=view&viewjobtype=I&ownerId=<bean:write name = "SiteManagementForm" property = "ownerId"/>&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>"><bean:write name="SiteManagementForm" property="appendixname" /></a></h2></td>
	 				</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="SiteManagementForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="SiteManagementForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="SiteManagementForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					          <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
</logic:notEqual>

<logic:equal name = "SiteManagementForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
<!--  change Menu  -->    
    <div id="menunav">
 
<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        		<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        	
        	
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
   
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>">Search</a>
  		</li>
  		
	</ul>
</li>

</div>
    
 <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			       <div id="breadCrumb">&nbsp;</div>
			    </div></td>
			</tr>
	        <td colspan = "7" height="1"><h2><bean:message bundle = "PRM" key = "prm.site.manage"/>&nbsp;<bean:write name="SiteManagementForm" property="msaname" />&nbsp;&nbsp;->&nbsp;NetMedX:&nbsp;<a  href="AppendixHeader.do?function=view&viewjobtype=I&ownerId=<bean:write name = "SiteManagementForm" property = "ownerId"/>&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>"><bean:write name="SiteManagementForm" property="appendixname" /></a></h2></td>	
		</tbody></table>	   
    
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        <tr>
	          <td width="155" height="39"  class="headerrow" colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" border="0">
		              <tr>
		                <td width="150">
		                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
			                   	<tr>
						            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
			                    </tr>
		                  	</table>
		                </td>
		              </tr>
		          </table>
	          </td>
	        </tr>
	        <tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>	
	                			<span>
									<div id="filter" class="divstyle">
					        			<table width="250" cellpadding="0" class="divtable">
						                     <tr>
					                            <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
															 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																		<bean:write name ="list" property = "statusid"/>
															 		</html:multibox>   
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="SiteManagementForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="SiteManagementForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="SiteManagementForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table></td>
		                      			    	<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
			                      			    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												         <span id = "OwnerSpanId">
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
								
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
															 			</html:multibox>   
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>											
						            </div>
				                </span>
							 </td>
	              		</tr>
            		</table>
            	</td>
        	</tr>
	    </table>
	</td>
 </tr>
</table>
<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
</logic:equal>

</logic:equal>

<table border = "0">
<logic:notEqual name = "SiteManagementForm"  property ="pageType" value = "jobsites">
							<logic:present name ="message">
								<tr>
									<td colspan = "10" class = "message"  > 
									<logic:equal  name ="message" value="-10001">
											Site updated successfully
									</logic:equal>
									<logic:equal  name ="message" value="-90003">
											Update faliure! site already exists. 
									</logic:equal>
									<logic:equal  name ="message" value="-90001">
											Add failure! site already exists. 
									</logic:equal>
									<logic:equal  name ="message" value="10000">
											Site added successfully.
									</logic:equal>
									<logic:equal  name ="message" value="-10003">
											Site deleted successfully.
									</logic:equal>
									</td>
								</tr>
							</logic:present>
<tr><td class = "labeleboldwhite" height = "30"><bean:message bundle = "PRM" key = "prm.site.manage"/>&nbsp;<bean:write name="SiteManagementForm" property="msaname" /></td></tr>
</logic:notEqual>
</table>
	<table width = "1236" border = "0" cellspacing = "1" cellpadding = "1">	
	<tr><td valign = "top" width="2"><A href = "javascript:toggleMenuSection( '1' );">
	    	<IMG id = img_1 alt=+ src = "images/Expand.gif" border = 0></A>
		</td>
			<td width = 546>	
				<table width = 546 border = "1" height = "100%" class="dbsummtable" cellpadding=0 cellspacing=0>
				<tr><td   width=546 valign = "top" height = "20"  align="center" class = "trybtopclose" id="classchange1"><bean:message bundle = "pm" key = "site.filters"/></td></tr>  
					<tr id = "div_1" style="display: none">
						<td valign = "top" width=538 height="86">
								<table width=538 cellpadding=0 cellspacing=0 border=0>
									<tr>
										<td width="4">&nbsp;</td>
										<TD class=dbvaluesmall width=38><bean:message bundle = "pm" key = "site.name"/></td>
										<TD width=96><html:text property = "siteSearchName" styleClass = "textbox" size="16" maxlength="20"/></TD>
										<td width="8">&nbsp;</td>
										<TD class=dbvaluesmall width=91><bean:message bundle = "pm" key = "site.number"/></td>
										<TD width="129"><html:text property = "siteSearchNumber" styleClass = "textbox" size="16" maxlength="20"/>&nbsp;</TD>
										<td width="7">&nbsp;</td>
										<TD class=dbvaluesmall width=56><bean:message bundle = "pm" key = "site.city"/></td>
										<TD width="72" ><html:text property = "siteSearchCity" styleClass = "textbox" size="12" maxlength="20"/>&nbsp;</TD>
										
									</tr>
									<tr>
										<td width="2">&nbsp;</td>
										<TD class=dbvaluesmall width=37><bean:message bundle = "pm" key = "site.state"/></td>
										<TD width=96><html:text property = "siteSearchState" styleClass = "textbox" size="8" maxlength="20"/></TD>
										<td width="8">&nbsp;</td>
										<TD class=dbvaluesmall width=91><bean:message bundle = "pm" key = "site.zipcode"/></td>
										<TD width="129"><html:text property = "siteSearchZip" styleClass = "textbox" size="12" maxlength="20"/>&nbsp;</TD>
										<td>&nbsp;</td>
										<TD class=dbvaluesmall width=56><bean:message bundle = "pm" key = "site.country"/></td>
										<TD width="72" ><html:text property = "siteSearchCountry" styleClass = "textbox" size="6" maxlength="20"/>&nbsp;</TD>
										
									</tr>
									<tr>
										<td width="2">&nbsp;</td>
										<TD class=dbvaluesmall width=37><bean:message bundle = "pm" key = "site.group"/></td>
										<TD width=96>
											<html:select  name = "SiteManagementForm" property="siteSearchGroup" size="1" styleClass="combowhite">	
												<html:optionsCollection name = "dcGroupList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
											</html:select>
										</TD>
										<td width="8">&nbsp;</td>
										<TD class=dbvaluesmall width=91><bean:message bundle = "pm" key = "site.end.customer"/></td>
										<TD width="129">
										<!-- <html:text property = "siteSearchEndCustomer" styleClass = "textbox" size="16" maxlength="20"/>&nbsp;  -->
											<html:select  name = "SiteManagementForm" property="siteSearchEndCustomer" size="1" styleClass="combowhite" onchange ="specialCase();"> 
												<html:optionsCollection name = "dcEndCustomerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
											</html:select>
										</TD>
										<td>&nbsp;</td>
										<TD class=dbvaluesmall width=56><bean:message bundle = "pm" key = "site.status"/></td>
										<TD width="100">
										<html:select  name = "SiteManagementForm" property="siteSearchStatus" size="1" styleClass="combowhite">
										<html:optionsCollection name = "dcStatusList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
										</html:select></TD>
										
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td colspan="7">&nbsp;</td>
										<td class="dbvaluesmall" align= "right">
										<html:button property="search" styleClass = "button" onclick="return onGo();"><bean:message bundle = "pm" key = "site.go"/></html:button></td> 										
									</tr>
								</table>
						</td>
					</tr>
				</table>
					</td>
					
					<td width = 315 valign = "top">
						<table height = "100%" width = 313 border = "1" class="dbsummtable" cellpadding=0 cellspacing=0>
							<tr><td width = 281 height = "20" valign = "top"   class = "trybtopclose" align="center"  id="classchange2"><bean:message bundle = "pm" key = "site.customize"/></td></tr>
							<tr id = "div_11" style="display: none">  
								<td valign = "top">
									<table width = 315 height="100%" border = "0" cellspacing=0 cellpadding=0 >
										<tr>
											<TD class=dbvaluesmall width="85"><html:checkbox property = "siteNameForColumn" disabled = "true" /><bean:message bundle = "pm" key = "site.labelName"/></TD>
											<TD class=dbvaluesmall width="123"><html:checkbox property = "siteNumberForColumn" disabled = "true" /><bean:message bundle = "pm" key = "site.siteNumber"/>&nbsp;</TD>
											<TD class=dbvaluesmall width="107"><html:checkbox property = "siteAddressForColumn"/><bean:message bundle = "pm" key = "site.address"/></TD>
										</tr>
										<tr>
											<TD class=dbvaluesmall  width="85" ><html:checkbox property = "siteCityForColumn"/><bean:message bundle = "pm" key = "site.city"/>&nbsp;</TD>
											<TD class=dbvaluesmall ><html:checkbox property = "siteStateForColumn"/><bean:message bundle = "pm" key = "site.state"/></TD>
											<TD class=dbvaluesmall  ><html:checkbox property = "siteZipForColumn"/><bean:message bundle = "pm" key = "site.zipcode"/>&nbsp;</TD>
										</tr>
										
										<tr>
											<TD class=dbvaluesmall width="85"><html:checkbox property = "siteCountryForColumn"/><bean:message bundle = "pm" key = "site.country"/></TD>
											<TD class=dbvaluesmall ><bean:message bundle = "pm" key = "sites.lines.per.page"/>&nbsp;<html:text property = "lines_per_page" styleClass = "textbox" size="2"/>&nbsp;</TD>
											<td width="107" align = "right"><html:button property="go" styleClass = "button" onclick="return onGo();">Go</html:button>&nbsp;</td> 
										</tr>	
										<tr><td class="dbvaluesmall" colspan="3"><a href="javascript:void(0);" onclick="downloadSiteList();" style="text-decoration: none; color: gray;">Download Site List</a></td></tr> 
									</table>
								</td>
						 	</tr>  
						 	
						</table> 
					</td>

					<td valign = "top" height = "50%" width ="330">
						<table width = 149 height = "100%" border = "1"  class="dbsummtable" cellpadding=0 cellspacing=0  >
							<tr><td width =149 align = "center"  height = "20" align="center" class = "trybtopclose" id="classchange3"><bean:message bundle = "pm" key = "site.summary"/></td></tr>
					 		<tr id = "div_13" style="display: none"> 
								<td valign = "top" width = 149 >
									<table width = 146  cellpadding=0 cellspacing=0 border=0>
										<tr>
											<TD class=dbvaluesmall width ="100">&nbsp;<bean:message bundle = "pm" key = "site.active.sites"/></TD>
											<TD class=dbvaluesmall width ="46" align="right"><bean:write name= "SiteManagementForm" property= "active_sites"/></TD>
										</tr>	
										<tr>
											<TD class=dbvaluesmall width ="100">&nbsp;<bean:message bundle = "pm" key = "site.inactive.sites"/></TD>
											<TD class=dbvaluesmall width ="46" align="right"><bean:write name= "SiteManagementForm" property= "inactive_sites"/></TD>
										</tr>
										<tr><td><hr></td></tr>										
										<tr>
											<TD class=dbvaluesmall width ="100">&nbsp;<bean:message bundle = "pm" key = "site.total.sites"/></TD>
											<TD class=dbvaluesmall width ="46" align="right"><bean:write name= "SiteManagementForm" property= "total_sites"/></TD>
										</tr>
										<tr><td>&nbsp;</td></tr>	
										<tr>
										<logic:equal name = "SiteManagementForm" property = "pageType" value ="jobsites">
										</logic:equal>
										<logic:equal name= "SiteManagementForm" property = "pageType" value = "mastersites">
										<td class=dbvaluesmall width = "100">&nbsp;<a href = "UploadCSV.do?viewjobtype=I&ownerId=<bean:write name = "SiteManagementForm" property = "ownerId"/>&fromType=siteManagement&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "SiteManagementForm" property = "current_page_no"/>&org_page_no=<bean:write name = "SiteManagementForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "SiteManagementForm" property = "total_page_size"/>&ownerId=<bean:write name = "SiteManagementForm" property = "ownerId"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "siteSearchEndCustomer"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>">Upload Sites </a></td>
										</logic:equal>
										</tr>
									</table>
								</td>
						 </tr>  
						</table>
					</td>
		<tr>		

	</table>
	<table width = "1200" border = "0" cellspacing = "0" cellpadding = "1">
	<tr><td width="2"></td>
		<td>
	  		<table  id="myTable" name="myTable" border = "0" width = "1200" cellspacing = "0" cellpadding = "1">
	  		
	 	 		<tr>
	 			    <td width="22" ></td> 
	 			    <td colspan ="10" class="dblabel" align="left"> 
	 			     	<IMG id = img_page  src = "images/old-first.gif" border = 0 onclick="first_record();">
						<IMG id = img_page src = "images/old-prev.gif" border = 0 onclick="previous_record();"> 
						<bean:message bundle = "pm" key = "site.page"/>
						<html:text property = "current_page_no" styleClass = "textbox" size="2" onblur="moveToCurrentPage();"/> 
						<bean:message bundle = "pm" key = "site.of"/>
					 	<html:text property = "total_page_size" styleClass = "textboxnumberreadonlymiddle" size="2" readonly = "true"/>   
						<IMG id = img_page src = "images/old-next.gif" border = 0 onclick="next_record();">
						<IMG id = img_page src = "images/old-last.gif" border = 0 onclick="last_record();">
		 			</td>
	 			</tr>  
				<tr>
					<td width="2"></td>
					<logic:equal name = "SiteManagementForm" property = "pageType" value ="jobsites">
						<td class = "buttonrow" styleClass="button" colspan = "<%=count+4%>">
					</logic:equal>
					
					<logic:notEqual name = "SiteManagementForm" property = "pageType" value ="jobsites">
						<td class = "buttonrow" styleClass="button" colspan = "<%=count+3%>">
					</logic:notEqual>
						<%	if(listSize>0){ %>
						<html:button property="Sort"  styleClass="button" onclick = "javascript:return sortwindow();"><bean:message bundle = "pm" key = "site.sort"/></html:button>
						<%}else{ %>
						<%}%>	
							<logic:equal name = "SiteManagementForm" property = "pageType" value ="jobsites">
							</logic:equal>
							<logic:equal name = "SiteManagementForm" property = "pageType" value ="mastersites">
							<html:button property="New"  styleClass="button" onclick="return AddSite();"><bean:message bundle = "pm" key = "site.new"/></html:button>
							</logic:equal>	
							
							
					</td>
				
				</tr>
				<% if( listSize > 0 ){ %>  
				<tr>
					<td width="2"></td>
					
					
					<logic:equal name = "SiteManagementForm" property = "pageType" value ="jobsites">
					<TD class = "tryb" colspan = "<%=count+4%>" align = "middle"><bean:message bundle = "pm" key = "site.details"/></TD>
					</logic:equal>
					<logic:equal name = "SiteManagementForm" property = "pageType" value ="mastersites">
					<TD class = "tryb" colspan = "<%=count+3%>" align = "middle"><bean:message bundle = "pm" key = "site.details"/></TD>
					</logic:equal>
		        </tr>
            
            <tr> 
            	<td width="2"></td>
            	  <TD width="152"  class = "tryb" > 
			 		<div align = "center">Priority Site</div>
				    
				    </td>
            	<logic:equal name ="SiteManagementForm" property = "siteNumberForColumn" value ="on">
            	    <TD width="152"  class = "tryb" > 
			 	
				    <div align = "center"><bean:message bundle = "pm" key = "site.number"/></div>
				    </td>
				</logic:equal>    
				<logic:equal name ="SiteManagementForm" property = "siteNameForColumn" value ="on">
				      <TD width="141" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.name"/></div>
				    </td>
				</logic:equal>
				<logic:equal name ="SiteManagementForm" property = "siteAddressForColumn" value ="on">    
				   <TD width="236"  class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.address"/></div>
				    </td>
				</logic:equal>
				<logic:equal name ="SiteManagementForm" property = "siteCityForColumn" value ="on">    
				  <TD width="94" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.city"/></div>
				    </td>
				</logic:equal>
				<logic:equal name ="SiteManagementForm" property = "siteStateForColumn" value ="on">    
				    <TD width="33"  class = "tryb"  > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.state"/></div>
				    </td>
				</logic:equal> 
				<logic:equal name ="SiteManagementForm" property = "siteZipForColumn" value ="on">   
				  <TD width="73"  class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.zipcode"/></div>
				    </td>
				</logic:equal>
				<logic:equal name ="SiteManagementForm" property = "siteCountryForColumn" value ="on">    
				   <TD width="32" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.country"/></div>
				    </td>
			     </logic:equal>
			   <TD width="180" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.group"/></div>
				 </td>
				<TD width="180"  class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.end.customer"/></div>
				 </td>
				<TD width="180" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.status"/></div>
				 </td>

				<logic:equal name = "SiteManagementForm" property = "pageType" value ="jobsites"> 
					<td class = "tryb">
						<div align = "center">Job Name</div>
					</td>
				 </logic:equal>
            </tr>    
            
      
	<logic:present name="siteDetails">
	<td>
	
	<%
					String temp_str = "";
					String temp_str1 = "";
					String temp_str2 = "";
					int i = 1;
              		int j = 1;

					String bgcolor = "";
					String bgcolorno = ""; 
					String bgcolortext1 = ""; 
					String tempCategory = "";
					String bgcolordata = "";
					String divbgcolortext1="";
	%>
	
	<logic:iterate id="list" name="siteDetails" >
	<%-- <td><bean:write name="list" property="cboxsite"/>hello</td> --%>
		
	<%				temp_str = "";
					if( ( i%2 )== 0 ) {
						bgcolor = "dbvalueeven";
						bgcolorno="numbereven";
						bgcolortext1="textlefteven";
						divbgcolortext1="textlefteventop";
						bgcolordata ="textlefteven1";
					} 
					else
					{
						bgcolor = "dbvalueodd";
						bgcolorno = "numberodd";
						bgcolortext1 = "textleftodd";
						divbgcolortext1="textleftoddtop";
						bgcolordata ="textleftodd1";
					}
					
	%>
	
			<bean:define id = "siteID" name = "list" property = "siteID" type = "java.lang.String"/>
					<bean:define id = "siteJobId" name = "list" property = "siteJobId" />
			<tr>
				<TD width="2">
				<A href = "javascript:toggleMenuSection( 'j<%= i %>' );">
						<IMG id = img_j<%= i %> alt=+ src = "images/Expand.gif" border = 0></A> 
				</TD>
				<%-- "javascript:gettingAjaxDataForEmail('<bean:write name="Partner_EditForm" property="pid"/>');" --%>
				<%
				String temp_str_statusflag="";
				temp_str_statusflag = "gettingAjaxDataForInsert('"
						+ siteID +"' );";
				
				%>
		
					<td class="<%= bgcolortext1 %>" align = "left"  valign = middle >
					<%
					
						int countflag = 0;
						/* if(request.getAttribute("roleflag")!=null) { */
						countflag = Integer.parseInt(request.getAttribute("roleflag").toString());
						
						if(countflag==1){
						%>
					 <html:checkbox  styleId='<%="flag"+siteID%>' value="Y"   name="list"  property="cboxsite"   onclick="<%=temp_str_statusflag %>" /> 
<%}  else { %>	
				
  				<html:checkbox  styleId='<%="flag"+siteID%>' value="Y"   name="list"  property="cboxsite" onclick="return false" />
  		
<%} %>
			</td>
						
				<logic:equal name ="SiteManagementForm" property = "siteNumberForColumn" value ="on">	
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
			
					<A id='one<%= siteID%>' href ='#' onclick="updateSiteInfo(<bean:write name = "list" property = "siteID"/>);"><bean:write name="list" property="siteNumber"/></a>
					
					<logic:equal name ="list" property = "cboxsite"  value ="Y">	
					<IMG id ='two<%= siteID%>'  src = "images/priorities.jpg" border = 0   width="16" height="16">	
					</logic:equal>
					</td>
					
				</logic:equal>	
				<logic:equal name ="SiteManagementForm" property = "siteNameForColumn" value ="on">	
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
			<!-- <A id='two<%= siteID%>' href ="SiteManage.do?method=Modify&countRow=<bean:write name= "SiteManagementForm" property= "countRow"/>&siteID=<bean:write name = "list" property = "siteID"/>&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "SiteManagementForm" property = "current_page_no"/>&org_page_no=<bean:write name = "SiteManagementForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "SiteManagementForm" property = "total_page_size"/>&ownerId=<bean:write name = "SiteManagementForm" property = "ownerId"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "specialGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "specialEndCustomer"/>&siteSearchTimeZone=<bean:write name = "SiteManagementForm" property = "siteSearchTimeZone"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>"><bean:write name="list" property="siteName"/></a></td> -->
				<bean:write name="list" property="siteName"/></TD>
				</logic:equal>	
				
				<logic:equal name ="SiteManagementForm" property = "siteAddressForColumn" value ="on">	
				<TD  class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
								<bean:write name="list" property="siteAddress"/></td>
				</logic:equal>
				
				<logic:equal name ="SiteManagementForm" property = "siteCityForColumn" value ="on">	
				<TD class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="siteCity"/></td>
				</logic:equal>
				
				<logic:equal name ="SiteManagementForm" property = "siteStateForColumn" value ="on">	
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="state"/></td>
				</logic:equal>
				
				<logic:equal name ="SiteManagementForm" property = "siteZipForColumn" value ="on">		
				<TD   class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="siteZipCode"/></td>
				</logic:equal>	
				
				<logic:equal name ="SiteManagementForm" property = "siteCountryForColumn" value ="on">		
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="country"/></td>	
				</logic:equal>
				
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="site_group"/></td>	
				
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="site_end_customer"/></td>	
									
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="siteStatus"/></td>
									
				<logic:equal name = "SiteManagementForm" property = "pageType" value ="jobsites"> 
					<TD class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
							<%=siteJobId%>
					</TD>	
				</logic:equal>						
				
				
									
			<!--  	<TD class="<%= bgcolortext1 %>" align = "right">
				<A id='one<%= siteID%>' href = "SiteManage.do?method=Modify&countRow=<bean:write name= "SiteManagementForm" property= "countRow"/>&siteID=<bean:write name = "list" property = "siteID"/>&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>">Modify</A>&nbsp;|&nbsp;<A id='two<%= siteID%>' href = "javascript:del(<%=siteID%>)">Delete</A>&nbsp;|&nbsp;  --> 
			</tr>
	                <TR id = div_j<%= i %> style = "DISPLAY: none">
	                <TD>&nbsp;</TD>
	                <TD colSpan=11 width="1200">
	                  <TABLE cellSpacing = 0 cellPadding = 0 width="1200" border =0>
	                    <TBODY>
		                        <TR height =20>
								    <TD  class="<%= divbgcolortext1 %>  colSpan=10 height="60" width ="1200">
								      <table cellpadding=0 cellspacing=0 border="0" width="1200">
								   
								       <tr>
									    	<td  class = "dblabelsmall" width="111"><bean:message bundle = "pm" key = "site.worklocation"/>:</td>
									    	<td  class = "dbvaluesmall" colspan="3" valign="top"><bean:write name = "list" property = "siteWorkLocation" /></td>
									    	<td  class = "dblabelsmall" width="117"><bean:message bundle = "pm" key = "site.siteDirections"/>:</td>
									    	<td  class = "dbvaluesmall" colspan="3" valign="top"><bean:write name = "list" property = "siteDirections" /></td>
								    	</tr>
										<tr>
											<td  class = "dblabelsmall" width="111"><bean:message bundle = "pm" key = "site.pointOfContact"/></td>
											<td  class = "dblabelsmall" width="140"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
											<td  class = "dblabelsmall" width="144"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
											<td  class = "dblabelsmall" width="144"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
											<td  class = "dblabelsmall" width="117"><bean:message bundle = "pm" key = "site.specialConditions"/>:</td>
											<td  class = "dbvaluesmall" colspan="3" rowspan="3" valign="top"><bean:write name = "list" property = "siteNotes" /></td>
								    	</tr>
								    	<tr>
										    <td  class = "dbvaluesmall" width="111"><bean:message bundle = "pm" key = "site.pointOfContactPrimary"/></td>
										    <td  class = "dbvaluesmall" width="140"><bean:write name = "list" property = "sitePrimaryName" /></td>	    		
										    <td  class = "dbvaluesmall" width="144"><bean:write name = "list" property = "sitePrimaryPhone" /></td>	    		
										    <td  class = "dbvaluesmall" width="144"><bean:write name = "list" property = "sitePrimaryEmail" /></td>	    		
				    					</tr>
							  			<tr>	
						  					<td  class = "dbvaluesmall" width="111"><bean:message bundle = "pm" key = "site.pointOfContactSecondary"/></td>
											<td  class = "dbvaluesmall" width="140"><bean:write name = "list" property = "siteSecondaryName" /></td>	    		
											<td  class = "dbvaluesmall" width="140"><bean:write name = "list" property = "siteSecondayPhone" /></td>	    		
											<td  class = "dbvaluesmall" width="144"><bean:write name = "list" property = "siteSecondaryEmail"/></td>	    		
					  					</tr>
					  					<tr>
					  						<td  class = "dbvaluesmall" width="111"><bean:message bundle = "pm" key = "site.createdby"/></td>
					  						 <TD class=dbvaluesmall width=140><bean:write name = "list" property = "siteCreatedBy"/></TD>
					  						 <TD class=dbvaluesmall width=144><bean:message bundle = "pm" key = "site.createddate"/></TD>
					  						 <TD class=dbvaluesmall width=144><bean:write name = "list" property = "siteCreatedDate"/></TD>
					  						 <TD class=dbvaluesmall width=117><bean:message bundle = "pm" key = "site.changedby"/></TD>
					  						 <TD class=dbvaluesmall width=164><bean:write name = "list" property = "siteChangedBy"/></TD>
					  						 <TD class=dbvaluesmall width=113><bean:message bundle = "pm" key = "site.changeddate"/></TD>
					  						 <TD class=dbvaluesmall width=267><bean:write name = "list" property = "siteChangedDate"/></TD>  
					  					</tr>
								     </table>
								    </TD>
								    
								</TR>
							</TBODY></TABLE></TD>
						</TR>
				<% i++; %>
				</logic:iterate>
				</table>
	</td>
	</tr>
</logic:present>

 <table width="1200" >
		<tr>
				
				<td width="22" ></td> 
				<td class = "dblabelsmall" align="left" colspan="10"><bean:message bundle = "pm" key = "site.total"/>&nbsp;
										   <bean:write name = "SiteManagementForm" property = "row_size"/>
				</td>
		</tr>
</table>  

<% } else { %>
					<tr>
		    			<TD></TD>
		    			<td  colspan = "8" class = "message" height = "30">
		    				<bean:message bundle = "pm" key = "site.not.present"/>
		    			</td>
		    		</tr>
		
<% } %>
</table>
		</html:form>
		</body>
<script>
function changeFilter()
{
	document.forms[0].action="SiteManagement.do?showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="SiteManagement.do?resetList=something";
	document.forms[0].submit();
	return true;

}


function downloadSiteList(){
	var url = document.URL+"&isDownloadList=downloadTrue";
	window.location=url;
	
}
	
</script>		
		</html:html>
		
		
		