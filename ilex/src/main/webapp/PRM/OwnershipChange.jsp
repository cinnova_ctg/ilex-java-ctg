<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For managing email notification list for a form.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id="dcpvs" name="dcpvs" scope="request" />

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">


<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<title></title>

<%@ include  file="/Menu.inc" %>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >



<table  width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table  border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
          
         <td  width="200" align=left class="toprow1"><a href="Partner_Search.do?ref=search&type=v"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">Locate Partner</a></td>
		 
		 <td  width="200" align=left class="toprow1"><a href="MCSA_View.do"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">Manage MCSA</a></td>
           <td  width=400 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		  </tr>
      </table>
    </td>
  </tr>
</table>



<table>
<html:form action="Change_Notification">
<html:hidden  property="refresh" />
<table  border="0" cellspacing="1" cellpadding="1" >
<tr>
  <td width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" >
		
	    	
	    	<logic:present name = "Change_NotificationForm" property="updatemessage">
	
		
			<logic:equal name="Change_NotificationForm" property="updatemessage" value="0">
	
			<tr><td   colspan="2" class="message" height="30" ><bean:message bundle="PVS" key="pvs.notificationupdatedsuccess"/></td></tr>
	
			</logic:equal>
	
			<logic:equal name="Change_NotificationForm" property="updatemessage" value="-9001">
			<tr><td   colspan="2" class="message" height="30" ><bean:message bundle="PVS" key="pvs.notificationupdatefailed"/></td></tr>
		
			</logic:equal>
			</logic:present>
			
	    	
	  	<tr><td colspan="2"class="labeleboldwhite" height="30" >
	  	<bean:message bundle="PVS" key="pvs.changeemailnotification"/></td>
	  	</tr>
	  	
		<tr>
			<td   class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.formname"/></td>
			<td   class="labelo" height="20"></td>
		</tr>
		
		<tr> 
			<td  class="labeleboldtop" height="20"><bean:message bundle="PVS" key="pvs.notificationemails"/></td>
			<td  class="labele" height="20"><html:textarea styleClass="textbox" name="Change_NotificationForm" property="emaillist" rows='7' cols='35'></html:textarea></td>
		</tr>
		
		<tr> 
		    <td class="buttonrow"> 
		      <html:submit property="save" styleClass="button" onclick="return chkEmail();"><bean:message bundle="PVS" key="pvs.save"/></html:submit>
		      <html:reset property="reset" styleClass="button"><bean:message bundle="PVS" key="pvs.reset" /></html:reset>
		    </td>
		    <td class="buttonrow" align="right">
		      <html:submit property="select" styleClass="buttonrightalign" onclick="return opensearchcontactwindow();"><bean:message bundle="PVS" key="pvs.chnotification.select" /></html:submit>
		    </td>
  		</tr>
  		
     	<jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  		</jsp:include>
  		
 	</table>
 </td>
 </tr>
</table>

</html:form>
</table>
</BODY>

<script>

function opensearchcontactwindow()
{
	
	str = 'EmailNotificationSearch.do';
	
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 500 , height = 400 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();

	return true;
}

function setValue() 
{
	
	document.forms[0].refresh.value = "true";
	document.forms[0].submit();
	return true;
}

function chkEmail()
{
	email=document.forms[0].emaillist.value;
	list=email.split(',');
	for(i=0;i<list.length;i++)
	{ 
		list[i]=trimBetweenString(trim(list[i]));
		if(!isEmail(list[i]))	
		{	
			alert("Invalid Email: "+list[i]);	
			document.forms[0].emaillist.focus();
			return false;
		}
	}
	return true;
}

function trimBetweenString(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!="\n"){		    
			nstr=nstr+str.substring(i,i+1);
		}
	}// END OF FOR
	return nstr;
}

</script>

</html:html>
