<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>


<html:html>
<HEAD>
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</HEAD>
<%@ include  file="/Menu.inc" %>
<html:form action="FirstJobSuccess">
<html:hidden property = "type"/>
<html:hidden property="jobid"/>
<html:hidden property = "viewjobtype"/>

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>  <td width="2" height="0"></td>
  			 <td>
  			 	<table border = "0" cellspacing = "2" cellpadding = "2" width = "400"> 
					<tr>    
							<td colspan = "8" class = "labeleboldwhite" height = "30" >
									<bean:message bundle = "PRM" key = "prm.jobdashboard.markfirstjobstatus"/>
							</td>
					</tr> 
					
					
					<tr>
							 <td  class = "labelobold"><bean:message bundle = "PRM" key = "prm.jobdashboard.firstjobsuccess"/></td>
							    <td colspan = 4 class = "labelo">
							    <html:radio property="firstJobSuccess" value="Y">Yes</html:radio>&nbsp;&nbsp;&nbsp;
							    <html:radio property="firstJobSuccess" value="N">No</html:radio>
							 </td>
					 </tr>
				 
	  					<tr>
			  				<td colspan = "8" >
							<html:submit property = "submit" onclick="javascript:onsubmitpage();" styleClass = "button"><bean:message bundle = "PRM" key = "prm.jobdashboard.firstjobsuccess.ok"/></html:submit>	
						    <html:button property = "close"  styleClass="button" onclick = "javascript:closefunction();"><bean:message bundle="PRM" key="prm.button.close"/></html:button>
							</td>
						</tr>
  				
  					

				</table>
			</td>
		</tr>
</table>
</html:form>


<script>
function closefunction()
{
	window.close();
}

function onsubmitpage()
{
	document.forms[0].action = "FirstJobSuccess.do?jobid=<bean:write name="FirstJobSuccessForm" property="jobid"/>";
	document.forms[0].target = "ilexmain";
	window.close();
	return true;	
}

</script>

</body>
</html:html>


