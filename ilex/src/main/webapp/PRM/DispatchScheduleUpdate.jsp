<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>
<% 
int listSize =(int) Integer.parseInt( request.getAttribute( "listSize" ).toString()); 
int j = 0;
int rowno = 0;
String temp_str = null;
int rowcount = 0; 
%>


<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>

<%@ include  file="/Menu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="moreAttach('<%=listSize%>');">

<table>
<html:form action="DispatchScheduleUpdate" >

<html:hidden property="function" />
<html:hidden property="type" />
<html:hidden property="typeid" />
<html:hidden property="page" />
<html:hidden property="pageid" />
<html:hidden property="appendixname" />
<html:hidden property="jobname" />
<html:hidden property="nettype" />
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property="checknetmedxtype" /> 
<html:hidden property="rownum" /> 
<html:hidden property="deletenum" />
<html:hidden property="prevactarival"/>
<html:hidden property="prevdeparture"/> 
<html:hidden property = "scheduleNumber"/>
<input type="hidden" name="tabId" value="<c:out value='${param.tabId}'/>"  />

<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td>
 
	   <table id="dynatable" border="0" cellspacing="1" cellpadding="1" width="750">
	    <c:if test="${param.throughJobDashboard ne 'Y'}">
	  		<tr>
	  			<logic:present name = "DispatchScheduleUpdateForm" property="updatemessage">
					<logic:equal name="DispatchScheduleUpdateForm" property="updatemessage" value="-9001">
						<td  colspan="4" class="message" height="30" ><bean:message bundle="PRM" key="prm.schedule.updatefailed"/></td>
			 		</logic:equal>
		 		</logic:present>
		 		
		 		<logic:present name = "DispatchScheduleUpdateForm" property="deleteStatus">
					<logic:equal name="DispatchScheduleUpdateForm" property="deleteStatus" value="-9004">
						<td  colspan="4" class="message" height="30" ><bean:message bundle="PRM" key="prm.schedule.deletefailed"/></td>
			 		</logic:equal>
			 		
			 		<logic:notEqual name="DispatchScheduleUpdateForm" property="deleteStatus" value="-9004">
						<td  colspan="4" class="message" height="30" ><bean:message bundle="PRM" key="prm.schedule.deletesuccess"/></td>
			 		</logic:notEqual>
		 		</logic:present>
			</tr>
	  		
	  		<tr>
	  			<td colspan="4" class="labeleboldwhite" height="30">
		  				<bean:message bundle="PRM" key="prm.schedule.updateddisschedulefor"/>&nbsp;<bean:message bundle="PRM" key="prm.schedule.appendix"/>:&nbsp;<bean:write name="DispatchScheduleUpdateForm" property="appendixname" />
	  				<%
	  				if(request.getParameter("type").equals("J")||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ")) { 
			  		%>
			  		<bean:message bundle="PRM" key="prm.schedule.arrowmark"/><bean:message bundle="PRM" key="prm.schedule.job"/><bean:write name="DispatchScheduleUpdateForm" property="jobname" />
			  		<% } %>
	  			</td>
	  		</tr>
	  	</c:if>
<% rowno = 0;
   j = 0;	
   String label = null;
   String labelbold = null;
   String combo = null; 
   %>
	<logic:present name="dispatchScheduleList" scope="request">
		<logic:iterate id="DSL" name="dispatchScheduleList">
		<bean:define id="seid" name="DSL" property="scheduleId" />		
		<html:hidden name="DSL" property="scheduleId" />
		
		<bean:define id="seNumber" name="DSL" property="scheduleNumber" />
		<html:hidden name="DSL" property="scheduleNumber"  styleId="<%="scheduleNumber"+rowno%>" />
		
	<% 
	if ((j%2)==0){
			labelbold="labelobold";
			label="labelo";
			combo="comboo";
	}
	else{
			labelbold="labelebold";		
			label="labele";
			combo="comboe";			
	}
	%>	  		
	  		 
	  		
	  		<% if(rowno>0) {%>
	  		<tr height="20">
		  		<td>&nbsp;</td>
	  		</tr>
	  		<% } %>
	  		
	  		<tr>
	  			<td height="20" colspan="3" width="390" class="labeleboldbuttonrow">
	  				Dispatch <%= rowno+1%>
	  			</td>
	  			<td width="320">&nbsp;</td>
	  		</tr>	 
	  		<tr>
	   			  <td  width="100" class="<%=labelbold %>">
	   			 		<bean:message bundle="PRM" key="prm.schedule.schedulearrival"/>
	   			  </td>
	   			
	   			 <td class="<%=label %>" width="110">
		   			 <html:text  styleClass="textbox" size="10" name ="DSL" property="startdate" readonly="true" styleId="<%="startdate"+rowno%>"/>
		   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].<%="startdate"+rowno%>, document.forms[0].<%="startdate"+rowno%>, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
	   			 </td>
  		
  			      <td class = "<%=label %>" width="180"> 
				    <html:select styleId="<%="startdatehh"+rowno%>" name="DSL" property = "startdatehh" styleClass="<%=combo%>">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
					</html:select>
					<html:select styleId="<%="startdatemm"+rowno%>" name="DSL" property = "startdatemm" styleClass="<%=combo%>">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
					</html:select>
					<html:select styleId="<%="startdateop"+rowno%>" name="DSL" property = "startdateop" styleClass="<%=combo%>">
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
					</html:select>
				  </td>
				
				 <td width="320">
				 </td>
			</tr>
  <% j++; 
   if ((j%2)==0){
			labelbold="labelobold";
			label="labelo";
			combo="comboo";
	}
	else{
			labelbold="labelebold";		
			label="labele";
			combo="comboe";			
	}
	%>
			<tr>
	   			 <td class="<%=labelbold %>">
	   			 		<bean:message bundle="PRM" key="prm.schedule.actualarrival"/>
	   			 </td>
	   			 <td class="<%=label %>">
	   			 <html:text  styleClass="textbox" size="10" name ="DSL" property="actstartdate" readonly="true" styleId="<%="actstartdate"+rowno%>"/>
	   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].<%="actstartdate"+rowno%>, document.forms[0].<%="actstartdate"+rowno%>, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
					    <td class = "<%=label %>"> 
						<html:select styleId="<%="actstartdatehh"+rowno%>" name="DSL" property = "actstartdatehh" styleClass="<%=combo%>">  
							<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select styleId="<%="actstartdatemm"+rowno%>" name="DSL" property = "actstartdatemm" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select styleId="<%="actstartdateop"+rowno%>" name="DSL" property = "actstartdateop" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
				 <td></td>
			</tr>
			
	<% j++; 
	if ((j%2)==0){
			labelbold="labelobold";
			label="labelo";
			combo="comboo";
	}
	else{
			labelbold="labelebold";		
			label="labele";
			combo="comboe";			
	}
	%>
	 
	    	<tr> 
			     <td class="<%=labelbold %>">
			    		<bean:message bundle="PRM" key="prm.schedule.departure"/>
			    </td>
			    <td class="<%=label %>">
				<html:text  styleClass="textbox" size="10" name ="DSL" property="actenddate" readonly="true" styleId="<%="actenddate"+rowno%>"/>
				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].<%="actenddate"+rowno%>, document.forms[0].<%="actenddate"+rowno%>, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
					    <td class = "<%=label %>">
						<html:select styleId="<%="actenddatehh"+rowno%>" name="DSL" property = "actenddatehh" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select styleId="<%="actenddatemm"+rowno%>" name="DSL" property = "actenddatemm" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select styleId="<%="actenddateop"+rowno%>" name="DSL" property = "actenddateop" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
				<td></td>
			</tr>
	<% 
	j++; 
	rowno++;
	rowcount = rowno+1;
	temp_str = "deleteSchedule("+seid+")"; 
	%>
	
				<tr>
	 		 	      <td class="buttonrow" colspan="3"> 
					 <%	if(rowno==1) {
					 %>
			 		   <!-- Change on 27-09-06 -->
			 		      &nbsp;
   			 		   <!-- Change on 27-09-06 -->  
						  <% } else {%>  
				     			<html:button property="detele" styleClass="button" onclick = "<%= temp_str%>"><bean:message bundle="PRM" key="prm.button.delete"/></html:button> 
						  <% } 
						  %>
		 		      </td>
		 		      </tr>
		 		      
</logic:iterate>			
</logic:present>

<!-- Temp Dispatch Schedule List -->

<% j = 0;	
   label = null;
   labelbold = null;
   combo = null; %>
	<logic:present name="tempList" scope="request">
		<logic:iterate id="TL" name="tempList">
		<bean:define id="seid" name="TL" property="scheduleId" />		
		<html:hidden name="TL" property="scheduleId" />
		
		<bean:define id="seNumber" name="TL" property="scheduleNumber" />
		<html:hidden name="TL" property="scheduleNumber"  styleId="<%="scheduleNumber"+rowno%>" />
		
	<% 
	if ((j%2)==0){
			labelbold="labelobold";
			label="labelo";
			combo="comboo";
	}
	else{
			labelbold="labelebold";		
			label="labele";
			combo="comboe";			
	}
	%>	  		
	  		 
	  		
	  		
	  		<tr height="20">
		  		<td>&nbsp;</td>
	  		</tr>
	  			  		
	  		<tr>
	  			<td height="20" colspan="3" width="390" class="labeleboldbuttonrow">
	  				Dispatch <%= rowno+1%>
	  			</td>
	  			<td width="320">&nbsp;</td>
	  		</tr>	 
	  		<tr>
	   			  <td  width="100" class="<%=labelbold %>">
	   			 		<bean:message bundle="PRM" key="prm.schedule.schedulearrival"/>
	   			  </td>
	   			
	   			 <td class="<%=label %>" width="110">
		   			 <html:text  styleClass="textbox" size="10" name ="TL" property="startdate" readonly="true" styleId="<%="startdate"+rowno%>"/>
		   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].<%="startdate"+rowno%>, document.forms[0].<%="startdate"+rowno%>, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
	   			 </td>
  		
  			      <td class = "<%=label %>" width="180"> 
				    <html:select styleId="<%="startdatehh"+rowno%>" name="TL" property = "startdatehh" styleClass="<%=combo%>">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
					</html:select>
					<html:select styleId="<%="startdatemm"+rowno%>" name="TL" property = "startdatemm" styleClass="<%=combo%>">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
					</html:select>
					<html:select styleId="<%="startdateop"+rowno%>" name="TL" property = "startdateop" styleClass="<%=combo%>">
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
					</html:select>
				  </td>
				
				 <td width="320">
				 </td>
			</tr>
  <% j++; 
   if ((j%2)==0){
			labelbold="labelobold";
			label="labelo";
			combo="comboo";
	}
	else{
			labelbold="labelebold";		
			label="labele";
			combo="comboe";			
	}
	%>
			<tr>
	   			 <td class="<%=labelbold %>">
	   			 		<bean:message bundle="PRM" key="prm.schedule.actualarrival"/>
	   			 </td>
	   			 <td class="<%=label %>">
	   			 <html:text  styleClass="textbox" size="10" name ="TL" property="actstartdate" readonly="true" styleId="<%="actstartdate"+rowno%>"/>
	   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].<%="actstartdate"+rowno%>, document.forms[0].<%="actstartdate"+rowno%>, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
					    <td class = "<%=label %>"> 
						<html:select styleId="<%="actstartdatehh"+rowno%>" name="TL" property = "actstartdatehh" styleClass="<%=combo%>">  
							<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select styleId="<%="actstartdatemm"+rowno%>" name="TL" property = "actstartdatemm" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select styleId="<%="actstartdateop"+rowno%>" name="TL" property = "actstartdateop" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
				 <td></td>
			</tr>
			
	<% j++; 
	if ((j%2)==0){
			labelbold="labelobold";
			label="labelo";
			combo="comboo";
	}
	else{
			labelbold="labelebold";		
			label="labele";
			combo="comboe";			
	}
	%>
	 
	    	<tr> 
			     <td class="<%=labelbold %>">
			    		<bean:message bundle="PRM" key="prm.schedule.departure"/>
			    </td>
			    <td class="<%=label %>">
				<html:text  styleClass="textbox" size="10" name ="TL" property="actenddate" readonly="true" styleId="<%="actenddate"+rowno%>"/>
				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].<%="actenddate"+rowno%>, document.forms[0].<%="actenddate"+rowno%>, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
					    <td class = "labele"> 
						<html:select styleId="<%="actenddatehh"+rowno%>" name="TL" property = "actenddatehh" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select styleId="<%="actenddatemm"+rowno%>" name="TL" property = "actenddatemm" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select styleId="<%="actenddateop"+rowno%>" name="TL" property = "actenddateop" styleClass="<%=combo%>">
							<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
				<td></td>
			</tr>
	<% 
	j++; 
	rowno++;
	rowcount = rowno+1;
	temp_str = "deleteSchedule("+seid+")"; 
	%>
	
				<tr>
	 		 	      <td class="buttonrow" colspan="3"> 
			    			<html:button property="detele" styleClass="button" onclick = "<%= temp_str%>"><bean:message bundle="PRM" key="prm.button.delete"/></html:button> 
				      </td>
		 		      </tr>
		 		      
</logic:iterate>			
</logic:present>


			
			</table>
				 		 
	 		<table border="0" cellspacing="1" cellpadding="1" width="750">
	 		 <tr height="20"><td>&nbsp;</td></tr>
	 		 <tr>
	 		 	  <td class="buttonrow" colspan="2"> 
		 		      <html:button property="save" styleClass="button" onclick = "openDispatchSchedulepage();">
		 		      <bean:message bundle="PRM" key="prm.save" /></html:button>
	     			  <html:reset  property="reset" styleClass="button"><bean:message bundle="PRM" key="prm.reset" /></html:reset>
	     			  <%if(request.getParameter("from") != null && request.getParameter("from").equals("RPT")){%>
	     			  <html:button property="back" styleClass="button" onclick = "history.go(-1);"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
	     			  <%}else{ %>
					  <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					  <%} %>
				  </td>
				  <td align="right" class="buttonrow">
					   <!-- Change on 27-09-06 -->
					    <html:button property="addmore" styleClass="button" onclick = "return moreAttach('a', 'chkdate');">Add&nbsp;Dispatch</html:button>
				   </td>
				    <td width="330">
					 </td>
			 </tr>
			</table>
			</td>			 
		</tr>



		</table>
 	  </td>
 	</tr> 
</table>
</html:form>
</table>
</body>
<script>
function openDispatchSchedulepage()
	{
		var url = "DispatchScheduleUpdate.do?function=update&type=J&typeid=<%=request.getParameter("typeid") %>&page=jobdashboard&pageid=<%=request.getParameter("pageid") %>&save=Save&tabId=8";
		document.forms[0].action = url;
		document.forms[0].submit();
	}

function moreAttach(){
	
	
	// Change on 27-09-06 Start
	if(moreAttach.arguments[1] == 'chkdate') {
		for (var j=0;j<eval(document.forms[0].rownum.value);j++){
			if(document.getElementById('startdate'+j).value == "") {
					alert("Please Select Scheduled Arrival Date.");
				   	return false;
				}
		}
	}
	
	// Change on 27-09-06 End
		
	if(moreAttach.arguments[0] > 0)
		return;
		
	var tableLength = dynatable.rows.length;
	
	var i = dynatable.rows.length-2;
	var count = 0;
	var rowno = 0;
	var oRow;
	
	document.forms[0].deletenum.value = eval(document.forms[0].deletenum.value)+1;
	
	document.forms[0].rownum.value = eval(document.forms[0].rownum.value)+1;
	count = eval(document.forms[0].rownum.value);
	rowno = count-1; 
	//Add a save button while list is empty and click on Add Dispatch
	
	if(dynatable.rows.length == 6 && <%= listSize%> == 0) {
	oRow=dynatable.insertRow();
	oCell=oRow.insertCell(0);
	oCell.className="buttonrow";
	oCell.innerHTML="<input type='submit' id='save' name='Save' value='Save' class='button' onclick = \"return validate()\">";
	oCell.colSpan=3;
	}
		
	if(i!=0){
	oRow=dynatable.insertRow();
	
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.height=20;
	}
	
	oRow=dynatable.insertRow();
	
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labeleboldbuttonrow";
	oCell.height=20;
	oCell.width=390;
	oCell.innerHTML="Dispatch "+count;

	oCell=oRow.insertCell(1);
	oCell.width=320;
	//oCell.innerHTML="&nbsp;";
	oCell.innerHTML = "&nbsp;<input type = 'hidden' name = 'scheduleNumber' value = '0' id = 'scheduleNumber"+rowno+"'/>";
	
	oRow=dynatable.insertRow();
	
	oCell=oRow.insertCell(0);
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PRM" key="prm.schedule.schedulearrival"/>";
	
	oCell=oRow.insertCell(1);
	oCell.className="labelo";
	oCell.innerHTML="<input type='text' name='startdate' size='10' value='' readonly='true' class='textbox' id='startdate"+rowno+"'/> <img src ='images/calendar.gif' width = 19 height = 17 border = 0 align ='center' onclick =\"return popUpCalendar(document.forms[0].startdate"+rowno+",document.forms[0].startdate"+rowno+", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";	
	oCell=oRow.insertCell(2);

	oCell.className="labelo";
	oCell.innerHTML="<select id='startdatehh"+rowno+"' name='startdatehh' size='1' class='comboo'><option value='  '>Hour</option><% String k=null; for(j=1;j<=12;j++) { if(j<10) k="0"+j; else k=""+j;%> <option value=<%=k%>><%=k%></option><%}%></select> <select id='startdatemm"+rowno+"' name='startdatemm' size='1' class='comboo'><option value='  '>Minute</option><%  for(j=0;j<=59;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%></select> <select id='startdateop"+rowno+"' name='startdateop' size='1' class='comboo'><option value=AM>AM</option><option value=PM>PM</option></select>";	

	oCell=oRow.insertCell(3);
	oCell.width=320;
	oCell.innerHTML="&nbsp;";
	
	oRow=dynatable.insertRow();
	
	oCell=oRow.insertCell(0);
	oCell.className="labelebold";
	oCell.innerHTML="<bean:message bundle="PRM" key="prm.schedule.actualarrival"/>";

	oCell=oRow.insertCell(1);
	oCell.className="labele";
	oCell.innerHTML="<input type='text' id='actstartdate"+rowno+"' name='actstartdate' size='10' value='' readonly='true' class='textbox' /> <img src ='images/calendar.gif' width = 19 height = 17 border = 0 align ='center' onclick =\"return popUpCalendar(document.forms[0].actstartdate"+rowno+",document.forms[0].actstartdate"+rowno+", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";
	
	oCell=oRow.insertCell(2);
	oCell.className="labele";
	oCell.innerHTML="<select id='actstartdatehh"+rowno+"' name='actstartdatehh' size='1' class='comboe'><option value='  '>Hour</option><%  for(j=1;j<=12;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%></select> <select id='actstartdatemm"+rowno+"' name='actstartdatemm' size='1' class='comboe'><option value='  '>Minute</option><%  for(j=0;j<=59;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%></select> <select id='actstartdateop"+rowno+"' name='actstartdateop' size='1' class='comboe'><option value=AM>AM</option><option value=PM>PM</option></select>";	
	
	oCell=oRow.insertCell(3);
	oCell.width=320;
	oCell.innerHTML="&nbsp;";
	
	oRow=dynatable.insertRow();
	
	oCell=oRow.insertCell(0);
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PRM" key="prm.schedule.departure"/>";

	oCell=oRow.insertCell(1);
	oCell.className="labelo";
	oCell.innerHTML="<input type='text' id='actenddate"+rowno+"' name='actenddate' size='10' value='' readonly='true' class='textbox' /> <img src ='images/calendar.gif' width = 19 height = 17 border = 0 align ='center' onclick =\"return popUpCalendar(document.forms[0].actenddate"+rowno+",document.forms[0].actenddate"+rowno+", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";
	
	oCell=oRow.insertCell(2);
	oCell.className="labelo";
	oCell.innerHTML="<select id='actenddatehh"+rowno+"' name='actenddatehh' size='1' class='comboo'><option value='  '>Hour</option><% for(j=1;j<=12;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%></select> <select id='actenddatemm"+rowno+"' name='actenddatemm' size='1' class='comboo'><option value='  '>Minute</option><%  for(j=0;j<=59;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%></select> <select id='actenddateop"+rowno+"' name='actenddateop' size='1' class='comboo'><option value=AM>AM</option><option value=PM>PM</option></select>";	
	
	oCell=oRow.insertCell(3);
	oCell.width=320;
	oCell.innerHTML="&nbsp;";
	 
	if(i!=0){
	oRow=dynatable.insertRow();
	
	oCell=oRow.insertCell(0);
	oCell.className="buttonrow";
	oCell.innerHTML="<input type='button' id='delete"+rowno+"' name='Delete' value='Delete' class='button' onclick = \"DeleteRow(document.getElementById('dynatable'), 0, "+count+", 'js', "+tableLength+")\">";
	oCell.colSpan=3;
	}
	
	count = count+1;
}	// END OF FUNCTION moreAttach()

function DeleteRow(){
var j=6;
var i;
var deleteRowCount=0;
var object = DeleteRow.arguments[0];
var num = DeleteRow.arguments[1];

if(object.rows.length == 13) j=7; // while deleting second schdule, than deleted rows will be 7 including save button row. 

if(eval(document.getElementById('dynatable').rows.length) != 6)
		i = eval(document.getElementById('dynatable').rows.length)-1; // For single schedule
	else
		i = eval(document.getElementById('dynatable').rows.length); // For multiple schedule
		
	i = i/6;

if(eval(DeleteRow.arguments[4]) != 6)
		deleteRowCount = eval(DeleteRow.arguments[4])-1; // For single schedule
	else
		deleteRowCount = eval(DeleteRow.arguments[4]); // For multiple schedule
		
	deleteRowCount = deleteRowCount/6;
	deleteRowCount++;


/* for delete is a order */
if(eval(document.forms[0].deletenum.value) != deleteRowCount) {
alert("Please first delete last one!!")
return false;
}


if(DeleteRow.arguments[3] == 'js') {
	for(i=0; i<6; i++) {
	object.deleteRow(DeleteRow.arguments[4]);
 }
}
document.forms[0].deletenum.value = eval(document.forms[0].deletenum.value)-1;
}// END OF FUNCTION DeleteROW()

function deleteSchedule(){

var scheduleId = deleteSchedule.arguments[0];
document.forms[0].action = "DispatchScheduleUpdate.do?function=update&scheduleId="+scheduleId+"&delete=true&listSize=<%= listSize%>";

document.forms[0].submit();
return true;

}// END OF FUNCTION deleteSchedule()

function validate(){
	var i;
	var k = 0;
	var deleteCount;
	if (document.getElementById('dynatable').rows.length==0) return true;
	
	if(eval(document.getElementById('dynatable').rows.length) != 6)
		i = eval(document.getElementById('dynatable').rows.length)-1; // For single schedule
	else
		i = eval(document.getElementById('dynatable').rows.length); // For multiple schedule
		
	i = i/6;
	
	//alert('delete count'+deleteCount)
	
	for (var j=0;j<eval(document.forms[0].rownum.value);j++){
		
		k=j+1;
		
		if(document.getElementById('startdate'+j)!= null) {
		}
		else {
		continue;
		}
	
		
	
		 if(isBlank(document.getElementById('startdate'+j).value) && isBlank(document.getElementById('actstartdate'+j).value) && isBlank(document.getElementById('actenddate'+j).value))
			{
				alert("Dispatch "+k+": <bean:message bundle="PRM" key="prm.dispatch.schedule.selectdates"/>");
			   	return false;
			}
				
		if(document.getElementById('startdatehh'+j).value != "  ")
 		{
 			if(document.getElementById('startdate'+j).value == "") {
				alert("Dispatch "+k+": <bean:message bundle="PRM" key="prm.dispatch.schedule.selectstartdate"/>");	 			
 				document.getElementById('startdate'+j).focus();
 				return false;
 			}
 		}
 		
 		if(document.getElementById('actstartdatehh'+j).value != "  ")
 		{
 			if(document.getElementById('actstartdate'+j).value == "") {
				alert("Dispatch "+k+": <bean:message bundle="PRM" key="prm.dispatch.schedule.selectactualstartdate"/>");	 			
 				document.getElementById('actstartdate'+j).focus();
 				return false;
 			}
 		}
 		
 		if(document.getElementById('actenddatehh'+j).value != "  ")
 		{
 			if(document.getElementById('actenddate'+j).value == "") {
				alert("Dispatch "+k+": <bean:message bundle="PRM" key="prm.dispatch.schedule.selectactualenddate"/>");	 			
 				document.getElementById('actenddate'+j).focus();
 				return false;
 			}
 		}
 			
		if(document.getElementById('actstartdate'+j).value!="" && document.getElementById('actenddate'+j).value!="")
		{
			if (!compDate_mdy(document.getElementById('actstartdate'+j).value, document.getElementById('actenddate'+j).value)) 
			{
				document.getElementById('actenddate'+j).focus();
				alert("Dispatch "+k+": <bean:message bundle = "PRM" key = "prm.dispatch.actualdatevalidation"/>");
				return false;
			} 
		}
		
		
	}
	return true;
}
</script>

<script>
function Backaction()
{
	<%if(request.getParameter("type").equals("P"))
	          {%> 
	          document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getParameter("typeid") %>&function=view";
			  document.forms[0].submit();
			  return true;
	         
		  <%} %>
		  <%if(request.getParameter("type").equals("J")||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ"))
          {
	        if(request.getParameter("page").equals("appendixdashboard"))
          	{%>
          	if(document.forms[0].nettype.value == 'dispatch')
	          	document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	        else
	        	document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getParameter("pageid") %>&function=view&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
			document.forms[0].submit();
			return true; 
          	
		  	<%} 
			else
			{%>
			document.forms[0].action = "JobDashboardAction.do?jobid=<%=request.getParameter( "typeid" ) %>";
			document.forms[0].submit();
			return true; 
			
		  	<%} 
		  }%>
		  
		  <%if(request.getParameter("type").equals("A")||request.getParameter("type").equals("AA")||request.getParameter("type").equals("CA")||request.getParameter("type").equals("IA"))
          {
          	if(request.getParameter("page").equals("jobdashboard"))
          	{%> 
         	document.forms[0].action = "JobHeader.do?jobid=<%=request.getParameter("pageid") %>&function=view";
			document.forms[0].submit();
			return true; 
         	
		  	<%} 
			else
			{%>
			document.forms[0].action = "ActivityHeader.do?activityid=<%=request.getParameter("typeid") %>&function=view";
			document.forms[0].submit();
			return true;
				
			<%}
		  }%>
	
}
</script>
</html:html>