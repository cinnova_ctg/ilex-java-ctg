<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>

<html:html>
<% 
	
	String setvalue="";
	int size=0;
	if(request.getAttribute( "size" ).toString()!=null)
	{
		size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
	}
	
%>
<HEAD>


<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<title></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>

<%@ include  file="/Menu.inc" %>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<table>
<html:form action="TicketsPerMsaAction">
<html:hidden property="ref"/>

<table  border="0" cellspacing="1" cellpadding="1"  >
<tr>
  <td  width="2" height="0"></td>
    <td width="1100">
  	<table border="0" cellspacing="1" cellpadding="1" width="1090"> 
     <logic:present name="msaticketlist" scope="request">
	 <tr>
	    <td width="1090" class="labeleboldwhite" height="30" colspan="13"><bean:message bundle = "PRM" key = "prm.nts.ticketsummary"/></td>
		    
		    <td class = "labeleboldhierrarchyright" height="20" colspan="4" width=240>
		    	<bean:message bundle = "PRM" key = "prm.nts.selectmonth"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			    <html:select styleClass ="combowhite" property = "monthName" onchange = "return getFilteredList();">  
					<html:optionsCollection name = "codes" property = "monthname" value = "value" label = "label"/> 
				</html:select>
			</td>
	  </tr>
 <%if(size>0) 
 {%>
	    <tr> 
			<td class="tryb" width=130 rowspan="3"><bean:message bundle = "PRM" key = "prm.nts.msaname"/></td>
			<td class="tryb" width=960 colspan="16"><bean:write property = "monthValue" name="TicketsPerMsaForm"/>&nbsp;<bean:write property = "yearValue" name="TicketsPerMsaForm"/></td>
		</tr>
		
		<tr> 
			<td class="tryb" width=240 colspan="4"><bean:message bundle = "PRM" key = "prm.nts.inwork"/></td>
			<td class="tryb" width=240 colspan="4"><bean:message bundle = "PRM" key = "prm.nts.complete"/></td>
			<td class="tryb" width=240 colspan="4"><bean:message bundle = "PRM" key = "prm.nts.closed"/></td>
			<td class="tryb" width=240 colspan="4"><bean:message bundle = "PRM" key = "prm.nts.total"/></td>
		</tr>
		
		
		<TR>
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.totalticket"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.extendedprice"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.actualcost"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.actualvgp"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.totalticket"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.extendedprice"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.actualcost"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.actualvgp"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.totalticket"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.extendedprice"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.actualcost"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.actualvgp"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.totalticket"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.extendedprice"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.actualcost"/></DIV>
	  	    </TD>
	  	    
	  	    <TD class=tryb width=60>
	  	    	<DIV><bean:message bundle = "PRM" key = "prm.nts.actualvgp"/></DIV>
	  	    </TD>
	   	</TR>    
	
					
			
	  
	  <%int i=1;
	  			
		String bgcolor=""; 
		String bgcolor1 = "";
		String bgcolor2 = "";
		%>
		<logic:iterate id="mtlist" name="msaticketlist">
		
	 	<%if((i%2)==0)
			{
				bgcolor="texte";
				bgcolor1="textleftodd";
				bgcolor2 = "readonlytextnumbereven"; 
			} 
		  else
			{
				bgcolor="texto";
				bgcolor1="textlefteven";
				bgcolor2 = "readonlytextnumberodd"; 
			}
		%>
	

		  <tr>
		  	 
			 	<logic:equal name="mtlist" property="msaname" value = "Total">
					<td  class="<%=bgcolor2 %>" height="20">
						<b><bean:write name="mtlist" property="msaname"/>&nbsp;</b>
					</td>	
				</logic:equal>
					
				<logic:notEqual name="mtlist" property="msaname" value = "Total">	
					<td  class="<%=bgcolor1 %>" height="20">
						<a href="AppendixHeader.do?function=view&viewjobtype=A&appendixid=<bean:write name="mtlist" property="appendixid"/>&from_type=summary" ><bean:write name="mtlist" property="msaname"/></a>
					</td>	
				</logic:notEqual>
				
			 </td>
			 	 
			 <td  class="<%=bgcolor %>" height="20">
				 <bean:write name="mtlist" property="inwork"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="inwork_extprice"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="inwork_actcost"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="inwork_actvgp"/>
			 </td>
			 
			 <td  class="<%=bgcolor %>" height="20">
				 <bean:write name="mtlist" property="complete"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="complete_extprice"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="complete_actcost"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="complete_actvgp"/>
			 </td>
			 
			 <td  class="<%=bgcolor %>" height="20">
				 <bean:write name="mtlist" property="closed"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="closed_extprice"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="closed_actcost"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="closed_actvgp"/>
			 </td>			 
			 
			 <td  class="<%=bgcolor %>" height="20">
				 <bean:write name="mtlist" property="total"/>
			 </td>
			 
			  <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="total_extprice"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="total_actcost"/>
			 </td>
			 
			 <td  class="<%=bgcolor2 %>" height="20">
				 <bean:write name="mtlist" property="total_actvgp"/>
			 </td>			
		  </tr> 
		  
		    <%i++; %>
		</logic:iterate>
		
<%} 
else
{%>

	 <tr> 
	    <td class="message" width="1090" colspan="17">
	    	<bean:message bundle = "PRM" key = "prm.nts.nonetmedxmsapresent"/>
	    </td>
	 </tr>
	  

<%} %>
	
	
	

</logic:present>  


  </table>
 </td>
 </tr>
</table> 
</html:form>
</table>
</BODY>
<script>
function getFilteredList()
{
	document.forms[0].submit();
	return true;
}
</script>

</html:html>
