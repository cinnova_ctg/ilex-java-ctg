<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id="dcStateCM" name="dcStateCM"  scope="request"/>
<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>
<bean:define id = "ticnum" name = "TicketDetailForm" property="ticket_number" />
<bean:define id="dcLonDirec" name="dcLonDirec" scope="request"/>
<bean:define id="dcLatDirec" name="dcLatDirec" scope="request"/>
<bean:define id="dcGroupList" name="dcGroupList" scope="request"/>
<bean:define id="dcEndCustomerList" name="dcEndCustomerList" scope="request"/>
<bean:define id="dcTimeZoneList" name="dcTimeZoneList" scope="request"/>
<bean:define id="dcRegionList" name="dcRegionList" scope="request"/>
<bean:define id="dcCategoryList" name="dcCategoryList" scope="request"/>
<bean:define id="dcSiteStatus" name="dcSiteStatus" scope="request"/>

<%
int depotlistsize = 0;
String accountingDB ="";
int criticalitycatlistsize = 0;
if(request.getAttribute("depotlistsize")!=null) {
	depotlistsize = Integer.parseInt(request.getAttribute("depotlistsize").toString());
}

if(request.getAttribute("fromAccountingDB")!=null) {
	accountingDB = request.getAttribute("fromAccountingDB").toString();
}

if(request.getAttribute("criticalitycatlistsize")!=null) {
	criticalitycatlistsize = Integer.parseInt(request.getAttribute("criticalitycatlistsize").toString());
}

%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());
%>
<html:html>
<HEAD>
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</HEAD>
<%@ include  file="/NMenu.inc" %>
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<%@ include  file="/AjaxOwners.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<BODY onLoad="CookieGetOpenDiv();disableItems();leftAdjLayers();">
<html:form action = "/TicketDetailAction" enctype = "multipart/form-data">
<html:hidden property="ticket_id" />
<html:hidden property="msaid" />
<html:hidden property="appendixid" />
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property="ticket_number" />
<html:hidden property="site_id" />
<html:hidden property="id1" />
<html:hidden property="id2" />
<html:hidden property="ref" />
<html:hidden property="ref1" />
<html:hidden property="refresh" />
<html:hidden property="refreshprodlist" />
<html:hidden property="addquantity" />
<html:hidden property="nettype" />
<html:hidden property="lo_ot_id" />
<html:hidden property="dialog_number" />
<html:hidden property="jobid" />
<html:hidden property="lm_ticket_type" />
<html:hidden property="invoice_Flag"/>
<html:hidden property="invoiceno"/>
<html:hidden property="partner_name"/>
<html:hidden property="powo_number"/>
<html:hidden property="from_date"/>
<html:hidden property="to_date"/>
<html:hidden property ="refersh"/>
<html:hidden property ="sitelistid"/>
<html:hidden property="jobOwnerOtherCheck"/>
<html:hidden property="helpDeskCheck"/>
<html:hidden property="ticket_type"/>


<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<%if(!nettype.equals("dispatch")){ %>
					<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="TicketDetailForm" property="appendixid"/>&ownerId=<bean:write name="TicketDetailForm" property="ownerId"/>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					<%}else{%>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px">Reports</a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=-1&ownerId=<bean:write name="TicketDetailForm" property="ownerId"/>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=640 colspan="2">&nbsp;</td>
					<%} %>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr>
		        	<td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.netmedxformsa"/>:&nbsp;<bean:write name="TicketDetailForm" property="company_name"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="TicketDetailForm" property="appendixname"/></h2></td>
		        </tr> 
	      </table>
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        <tr>
	          <td width="155" height="39"  class="headerrow" colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" border="0">
		              <tr>
		                <td width="150">
		                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
			                   	<tr>
						            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
			                    </tr>
		                  	</table>
		                </td>
		              </tr>
		          </table>
	          </td>
	        </tr>
	        <tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>	
	                			<span>
									<div id="filter" class="divstyle">
					        			<table width="250" cellpadding="0" class="divtable">
						                     <tr>
					                            <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
															 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																		<bean:write name ="list" property = "statusid"/>
															 		</html:multibox>   
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="TicketDetailForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="TicketDetailForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="TicketDetailForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table></td>
		                      			    	<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
			                      			    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												        <span id = "OwnerSpanId"> 
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
								
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
															 			</html:multibox>   
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>											
						            </div>
				                </span>
							 </td>
	              		</tr>
            		</table>
            	</td>
        	</tr>
	    </table>
	</td>
 </tr>
</table>
<%@ include  file="/NetMedXDashboardMenuScript.inc" %>

<TABLE cellSpacing=1 cellPadding=1 align=left border="0">
  <TBODY>
  <TR>
    <TD width=2 height=0></TD>
    <TD>
      <TABLE cellSpacing=2 cellPadding=2 width="650" border="0">
        <TBODY>
	        <logic:present name = "retval" scope = "request">
		    	<tr>
					<td  colspan = "3" class = "message" height = "30">
			    	 <logic:equal name = "retval" value = "0">
			    		<bean:message bundle = "PRM" key = "prm.netmedx.success"/>
			    	</logic:equal>
			    	<logic:notEqual name = "retval" value = "0">
	    				<bean:message bundle = "PRM" key = "prm.netmedx.failure"/>
					</logic:notEqual>
					</td>
			      </tr>
		    </logic:present>
     
        <TR>
          <TD class=labeleboldwhite colSpan="9" height=30>
          	<!--<bean:message bundle="PRM" key="prm.netmedx.netmedxformsa"/>:&nbsp;<bean:write name="TicketDetailForm" property="company_name"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<a href="AppendixHeader.do?appendixid=<bean:write name="TicketDetailForm" property="appendixid"/>&function=view&viewjobtype=<bean:write name = "TicketDetailForm" property = "viewjobtype" />&ownerId=<bean:write name = "TicketDetailForm" property = "ownerId" />"><bean:write name="TicketDetailForm" property="appendixname"/></a>-->
          	
			<TABLE border="0">
			<TBODY>   
			<tr>
				<TD class=trybtopsmall colSpan="9" height=10><bean:message bundle="PRM" key="prm.netmedx.requestor"/></TD>
			</tr>

			<TR>
			 <TD class=dblabel><bean:message bundle="PRM" key="prm.netmedx.ticketlabel"/>:</TD>
			 <TD class=dbvalue>
				<bean:write name="TicketDetailForm" property="ticket_number"/>
				 <logic:present name="TicketDetailForm" property="lm_ticket_type" scope = "request">
		    	 <logic:equal name="TicketDetailForm" property="lm_ticket_type" value = "WEB">
					&nbsp;&nbsp;&nbsp;[<a href="#" onclick = "return viewSummaryPage('<bean:write name="TicketDetailForm" property="ticket_number"/>');" >Details</a>]
		    	</logic:equal>
		       </logic:present>
			 </TD>
			 
			 <TD class=dblabel><bean:message bundle="PRM" key="prm.netmedx.companyname"/>:</TD>
			 <TD class=dbvalue colSpan="6">
				<logic:equal name = "TicketDetailForm" property = "ticket_type" value = "firstticket">
					<html:text name="TicketDetailForm" property="company_name" size="40" styleClass = "textbox" readonly="true"/>
					&nbsp;&nbsp;
					<html:button property = "companyname" styleClass = "button" onclick = "return search('company_search');">Select</html:button>
				</logic:equal>
				<logic:notEqual name = "TicketDetailForm" property = "ticket_type" value = "firstticket">
					<bean:write name="TicketDetailForm" property="company_name" />
					<html:hidden name="TicketDetailForm" property="company_name" />
				</logic:notEqual>
			 </TD>
			</TR>
			
	         <TR>
			 <TD class=dblabel><bean:message bundle="PRM" key="prm.netmedx.requestor"/>:</TD>
			 <TD class=dbvalue>
				<html:select name="TicketDetailForm" property = "requestor_name" styleClass="combowhite" onchange="setRequestorEmail();">  
					<html:optionsCollection name = "codes" property = "requestorlist" value = "value" label = "label"/> 
				</html:select>
			 </TD>
			 
			 <TD class=dblabel><bean:message bundle="PRM" key="prm.netmedx.email"/>:</TD>
			 <TD class=dbvalue colSpan="6">
				<span><html:text property = "requestor_email"  size = "40" styleClass = "textbox"/></span>
			 </TD>
			 
			<tr>
				<TD class=trybtopsmall colSpan="9" height=20><bean:message bundle="PRM" key="prm.netmedx.siteinformation"/></TD>
			</tr>

			<TR>
				 <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.sitename"/>:
				 </TD>
				 <TD class=dbvalue>
					<span><html:text property = "site_name"  size = "20" styleClass = "textbox"/></span>&nbsp;&nbsp;
					<span><html:button property = "sitesearch" styleClass = "button" onclick = "return opensearchsitewindow();">Select</html:button></span>
				 </TD>
				 <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.sitenumber"/>:<font class="red"> *</font>
				 </TD>
				 <TD class=dbvalue colspan="2">
					<span><html:text property = "site_number"  size = "25" styleClass = "textbox"/></span>
				 </TD>
				 <td class=dblabel><bean:message bundle = "pm" key = "site.status"/></td>
				 <td class=dblabel colspan="3">
				 	<html:select property="sitestatus" size="1" styleClass="combowhite">
						<html:optionsCollection name = "dcSiteStatus"  property = "firstlevelcatglist"  label = "label"  value = "value" />
					</html:select>
				 </td>
		    </TR>
			<TR>
				 <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.address"/>:
				 </TD>
				 <TD class=dbvalue colSpan="2">
					<span><html:text property = "site_address"  size = "60" maxlength="128" styleClass = "textbox"/></span>
				 </TD>
				 <TD class=dblabel>
					 <bean:message bundle="PRM" key="prm.netmedx.city"/>:
				  </TD>
				  <TD class=dbvalue>
					<span><html:text property = "site_city"  size = "20" styleClass = "textbox"/></span>
				  </TD>
				  <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.state"/>:<font class="red"> *</font>
				  </TD>
				  <TD class=dbvalue>
				  			<logic:present name="initialStateCategory">
								<html:select property = "site_state"  styleClass="combowhite" onchange="return changeCountry();">
									<logic:iterate id="initialStateCategory" name="initialStateCategory" >
										<bean:define id="statecatname" name="initialStateCategory" property="siteCountryIdName" />		   
										<optgroup class=dblabel label="<%=statecatname%>">
											<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
										</optgroup> 
									</logic:iterate>
								</html:select>
							</logic:present>	
				
				</TD>
			</TR>
			<TR>
				<TD class=dblabel><bean:message bundle="PRM" key="prm.netmedx.zipcode"/>:</TD>
				<TD class=dbvalue>
					<span><html:text property = "site_zipcode"  size = "9" styleClass = "textbox"/></span>
				</TD>
				<TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.country"/>:<font class="red"> *</font>
				</TD>
				<TD class=dbvalue>
					<html:select  property="site_country" size="1" styleClass="combowhite">
						<html:optionsCollection name = "dcCountryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
					</html:select>
					<!-- <span><html:text property = "site_country"  size = "15" styleClass = "textbox"/></span>  -->
					
				</TD>
				<td class = "dblabel"><bean:message bundle = "pm" key = "site.category"/></td>
					<td class = "dblabel">
					   	<html:select  property="sitecategory" size="1" styleClass="combowhite">
							<html:optionsCollection name = "dcCategoryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
						</html:select>   
					</td> 
					   <td class = "dblabel"><bean:message bundle = "pm" key = "site.region"/></td>
					   <td class = "dblabel">
						<html:select  property="siteregion" size="1" styleClass="combowhite">
							<html:optionsCollection name = "dcRegionList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
						</html:select>  
					</td> 
				
				
				
			</TR>
			<tr>
  						<td class = "dblabel"><bean:message bundle = "pm" key = "site.latitude"/></td>
					    <td class = "dbvalue" colspan="1">
					    <html:text  property = "sitelatdeg" styleClass = "textbox" size="2" maxlength="4" />&nbsp;&deg;
					    &nbsp;<html:text  property = "sitelatmin" styleClass = "textbox" size="2" maxlength="2" />&nbsp;" 
					    <html:select  property="sitelatdirection" size="1" styleClass="combowhite">
							<html:optionsCollection name = "dcLatDirec"  property = "firstlevelcatglist"  label = "label"  value = "value" />
						</html:select>	
					    </td>
					    
					    <td class = "dblabel"><bean:message bundle = "pm" key = "site.longitude"/></td>
					    <td class = "dbvalue" colspan="1">
					    <html:text  property = "sitelondeg" styleClass = "textbox" size="2" maxlength="4" />&nbsp;&deg;
					    &nbsp;<html:text  property = "sitelonmin" styleClass = "textbox" size="2" maxlength="2"  />&nbsp;" 
					    <html:select  property="sitelondirection" size="1" styleClass="combowhite">
							<html:optionsCollection name = "dcLonDirec"  property = "firstlevelcatglist"  label = "label"  value = "value" />
						</html:select>
					    </td>
					    <td class = "dblabel"><bean:message bundle = "pm" key = "site.timezone"/></td>
					    <td class = "dblabel" colspan="2">
				      		 <html:select property="sitetimezone" size="1" styleClass="combowhite">
								<html:optionsCollection name = "dcTimeZoneList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>   
					    </td>
					    
					    
					    
  			</tr>
			<TR>
				 <TD class=dblabel>
				 	<bean:message bundle="PRM" key="prm.netmedx.localityuplift"/>:<font class="red"> *</font>
				 </TD>
				 <TD class=dbvalue>
					<span><html:text property = "locality_uplift"  size = "10" styleClass = "textbox"/></span>
				</TD>
	            <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.union"/>:
				</TD>
				<TD class=dbvalue >
					<html:radio property="union_site" value="Y">Yes</html:radio>&nbsp;&nbsp;&nbsp;
					<html:radio property="union_site" value="N">No</html:radio>
				</TD>
				<td class = "dblabel"><bean:message bundle = "pm" key = "site.group"/></td>
					     <td class = "dbvalue">
					     	<html:select  property="sitegroup" size="1" styleClass="combowhite">
					     		<html:optionsCollection name = "dcGroupList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					     </td>
					     <td class = "dblabel"><bean:message bundle = "pm" key = "site.end.customer"/></td>
					     <td class = "dbvalue">
					    	<html:select  property="siteendcustomer" size="1" styleClass="combowhite"> 
					    		<html:optionsCollection name = "dcEndCustomerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					     </td>
			</TR>
        
			<TR>
			  <TD class=dblabel>
				<bean:message bundle="PRM" key="prm.netmedx.worklocation"/>:
			  </TD>
			  <TD class=dbvalue colSpan=8>
				<span><html:text property = "site_worklocation"  size = "85" styleClass = "textbox" maxlength="128" /></span>
			  </TD>
			</TR>
			<TR>
			  <TD class=dblabel>
				<bean:message bundle="PRM" key="prm.netmedx.sitedirections"/>:
			  </TD>
			  <TD class=dbvalue colSpan=8>
				<span><html:text property = "site_direction"  size = "85" styleClass = "textbox" /></span>
			  </TD>
			 </TR>
			<TR>
			  <TD class=dblabel>
				<bean:message bundle="PRM" key="prm.netmedx.pointofcontact"/>
			  </TD>
			  <TD class=dblabel>
				<bean:message bundle="PRM" key="prm.netmedx.name"/>
			  </TD>
			  <TD class=dblabel colSpan=2>
				<bean:message bundle="PRM" key="prm.netmedx.phone"/>
			  </TD>
			  <TD class=dblabel colSpan=5>
				<bean:message bundle="PRM" key="prm.netmedx.email"/>
			  </TD>
			</TR>

			<TR>
			  <TD class=dblabel>
				<bean:message bundle="PRM" key="prm.netmedx.primary"/>
			  </TD>
			  <TD class=dbvalue>
				<span><html:text property = "primary_Name"  size = "15" styleClass = "textbox" /></span>
			  </TD>
			  <TD class=dbvalue colSpan=2>
				  <span><html:text property = "site_phone"  size = "20" styleClass = "textbox" /></span>
			  </TD>
			  <TD class=dbvalue colSpan=5>
				<span><html:text property = "primary_email"  size = "30" styleClass = "textbox" /></span>
			  </TD>
		  </TR>
			<TR>
			  <TD class=dblabel>
				<bean:message bundle="PRM" key="prm.netmedx.secondary"/>
			  </TD>
			  <TD class=dbvalue>
				<span><html:text property = "secondary_Name"  size = "15" styleClass = "textbox" /></span>
			  </TD>
			  <TD class=dbvalue colspan="2">
				<span><html:text property = "secondary_phone"  size = "20" styleClass = "textbox" /></span>
			  </TD>
			  <TD class=dbvalue colSpan="5">
				<span><html:text property = "secondary_email"  size = "30" styleClass = "textbox" /></span>
			  </TD>
			</TR>
			<TR>
			<TR>
			  <TD class=dblabeltop>
				<bean:message bundle="PRM" key="prm.netmedx.specialconditions"/>:
			  </TD>
			
			  <TD class=dbvalue colSpan="8">
				<html:textarea property = "site_notes" cols="120" rows="4" styleClass = "textbox" readonly="true"/>
			  </TD>
		    </TR>

			<tr>
				<TD class=trybtopsmall colSpan="9" height=20><bean:message bundle="PRM" key="prm.netmedx.requestdetails"/></TD>
			</tr>

			<TR>
			  <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.requesteddatetime"/>:<font class="red"> *</font>
			  </TD>
			  <TD class=dbvalue colSpan="2">
			  		<html:text  styleClass="textbox" size="10" property="requested_date" readonly="true"/>
	   				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].requested_date, document.forms[0].requested_date, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;" />
  				    &nbsp;&nbsp;&nbsp;
				    
				    <html:select name="TicketDetailForm" property = "requested_datehh" styleClass="combowhite">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
					</html:select>
					
					<html:select name="TicketDetailForm" property = "requested_datemm" styleClass="combowhite">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
					</html:select>
					
					<html:select name="TicketDetailForm" property = "requested_dateop" styleClass="combowhite">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
					</html:select>
			  </TD>
			    <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.standby"/>:
				</TD>
				<TD class=dbvalue>
					<html:radio property="standby" value="Y">Yes</html:radio>&nbsp;&nbsp;&nbsp;
					<html:radio property="standby" value="N">No</html:radio>
				</TD>
				
				<TD class=dblabel align="right">
					<bean:message bundle="PRM" key="prm.netmedx.msp"/>:
				</TD>
				<TD class=dbvalue>
					<html:radio property="msp" value="Y">Yes</html:radio>&nbsp;&nbsp;&nbsp;
					<html:radio property="msp" value="N">No</html:radio>
				</TD>
				
				<TD class=dblabel align="right">
					<bean:message bundle="PRM" key="prm.netmedx.helpdesk"/>:
				</TD>
				<TD class=dbvalue>
					<html:radio property="helpdesk" value="Y" onclick= " return getHelpDeskRefresh();">Yes</html:radio>&nbsp;&nbsp;&nbsp;
					<html:radio property="helpdesk" value="N" onclick= " return enabledResource();">No</html:radio>
				</TD>
			</TR>

			<TR>
			  <TD class=dblabel>
				<bean:message bundle="PRM" key="prm.netmedx.problemcategory"/>:<font class="red"> *</font>
			  </TD>
			  <TD class=dbvalue colspan="2">
			    <html:select name="TicketDetailForm" property = "category" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "problemcategory" value = "value" label = "label"/> 
				</html:select>  
				   <!-- <span><html:text property = "category_name"  size = "50" styleClass = "textbox" onclick ="javascript:problemcategorysearch();" readonly="true" /></span> --> 
			  </TD>
			  
			  <TD class=dblabel colSpan="2">
				<bean:message bundle="PRM" key="prm.netmedx.customerreferences"/>
			  </TD>
			  <TD class=dbvaluesmall colSpan="4">
				<span><html:text property = "customer_references"  size = "50" styleClass = "textbox"/></span>
				<br>
				<font color ="red" style="font-size:smaller;"><b><bean:message bundle="PRM" key="prm.netmedx.customerreference.message"/></b></font>

			  </td>
			  
			  
			</TR>
			
			<TR>
			  <TD class=dblabel colspan="3">
				<bean:message bundle="PRM" key="prm.netmedx.problemdescription"/>:<font class="red"> *</font>
			  </TD>
			  <TD class=dblabeltop colspan="6">
				<bean:message bundle="PRM" key="prm.netmedx.specialinstructions"/>:
			  </TD>
			   
		    </tr>	 

			<tr>
			  <TD class=dbvalue colSpan="3">
				<html:textarea property = "problem_description" cols="70" rows="4" styleClass = "textbox"/>
			  </TD>
			   <TD class=dbvalue colSpan="6">
				<html:textarea property = "special_instruction" cols="70" rows="4" styleClass = "textbox"/>
			  </TD>
		    </TR>
		  <!-- by hamid for rules start-->  
		  
		  <TR>
			  <TD class=dblabel colspan="3">
				<bean:message bundle="PRM" key="prm.netmedx.rules"/>:
			  </TD>
			  <TD class=dblabeltop colspan="6">
				&nbsp;
			  </TD>
		    </tr>	 

			<tr>
			  <TD class=dbvalue colSpan="3">
				<html:textarea property = "rules" cols="70" rows="4" styleClass = "textbox" readonly="true"/>
			  </TD>
			   <TD class=dbvalue colSpan="6">
				&nbsp;
			  </TD>
		    </TR>
		    		    
		    <!-- by hamid for rules  end-->  

			<tr>
			  <TD class=dblabeltop>
				<bean:message bundle="PRM" key="prm.netmedx.depotrequisition"/>:
			  </TD>
			    <TD class=dbvalue class=dbvalue colspan="2">
				   	<logic:present name="productcatlist">
	
					<html:select name="TicketDetailForm" property = "depot_requisition" size="8" multiple="true" styleClass="combowhite">
					<logic:iterate id="productcatlist" name="productcatlist" >
					
					<bean:define id="prodcatname" name="productcatlist" property="dp_cat_name" />		   
						<optgroup class=dblabel label="<%=prodcatname%>">
							<html:optionsCollection name = "productcatlist" property="prodList" value = "value" label = "label"/>
						</optgroup> 
				 
					</logic:iterate>
					</html:select>
					</logic:present>	
				</TD>
				<TD class=dblabelbottom colspan="6">
				  <span>
				  	<html:button property = "add_quantity" styleClass = "button" onclick = "return addQuantity();">
				  		Add&nbsp;Quantity
				  	</html:button>
				  </span>
			  </TD>			
		    </TR>
		    
    	   <logic:present name="TicketDetailForm" property="depoproductlist" scope="request">
		    <logic:iterate id="depoproductlist" name="TicketDetailForm" property="depoproductlist">
			    <html:hidden name="depoproductlist" property="de_product_id" />
			    <tr>
				<td>
					&nbsp;
				</td>
				
				<td class = "dbvaluesmall" colspan="2">
			      	<bean:write name="depoproductlist" property="de_product_name"/>
			    </td>
			    
			    <TD class=dbvalue colspan="6">
					<html:text property = "depot_quantity"  name="depoproductlist" size = "10" styleClass = "textbox"/>
	  		    </TD>
			    </tr>   	
		      	
		 	</logic:iterate>
 		  </logic:present>

			<tr>
				<td class = "dblabel">
   					<bean:message bundle = "PRM" key = "prm.netmedx.fileattachment1"/>
				</td>
				
				<td class = "dbvalue" colspan="8">
					<html:file property = "filename" styleClass = "button" />
				</td>
			</tr>

			<tr>
				<TD class=trybtopsmall colSpan="9" height=20><bean:message bundle="PRM" key="prm.netmedx.requestcriticality"/></TD>
			</tr>

			<TR>
			  <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.criticality"/>:<font class="red"> *</font>
			  </TD>
			  <TD class=dbvalue>
				    <html:select name="TicketDetailForm" property = "criticality" styleClass="combowhite" onchange="getRefresh();">  
						<html:optionsCollection name = "codes" property = "criticalityname" value = "value" label = "label"/> 
					</html:select>
			  </TD>

			  <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.periodofperformance"/>:<font class="red"> *</font>
			  </TD>
			  
			  <TD class=dbvalue colspan="3">
			  	  <logic:present name="criticalitycatlist" scope="request">
			  	     <logic:iterate id="clist" name="criticalitycatlist">
					    <bean:define id="cid" name="clist" property="crit_cat_id" />
					    <html:radio name= "TicketDetailForm" property = "crit_cat_id" value = "<%= ( String ) cid %>" onclick = "return getRefresh();"/>
					    	<bean:write name="clist" property="crit_cat_name"/>
				 	</logic:iterate>
		 		  </logic:present>
	 		  </TD>
	 		  
	 		  <TD class=dblabel>
					<bean:message bundle="PRM" key="prm.netmedx.resourcelevel"/>:<font class="red"> *</font>
			  </TD>
			  
			  <TD class=dbvalue colspan="2">
			  	 <logic:equal name="TicketDetailForm" property="helpdesk" value="N">
				  <html:select name="TicketDetailForm" property = "resourcelevel" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "resourcelevel" value = "value" label = "label"  /> 
				  </html:select>
				  </logic:equal>
				  <logic:equal name="TicketDetailForm" property="helpdesk" value="Y">
				  	<html:select name="TicketDetailForm" property = "resourcelevel" styleClass="combowhite" disabled="true" onfocus="return getResourceLevel();">  
						<html:optionsCollection name = "codes" property = "resourcelevel" value = "value" label = "label"  /> 
				  	</html:select>
				  </logic:equal>
			  </TD>
			</TR>

			<TR>
			  <TD class=dblabel colspan="9">
				<bean:message bundle="PRM" key="prm.netmedx.preferredarrivaltime"/>:<font class="red"> *</font>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		    		<html:text  styleClass="textbox" size="10" property="arrivaldate" readonly="true"/>
	   				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].arrivaldate, document.forms[0].arrivaldate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;" />
  				    &nbsp;&nbsp;&nbsp; 
				    
				    <html:select name="TicketDetailForm" property = "arrivaldatehh" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
					</html:select>
					
					<html:select name="TicketDetailForm" property = "arrivaldatemm" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
					</html:select>
					
					<html:select name="TicketDetailForm" property = "arrivaldateop" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
					</html:select>		
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					(<bean:message bundle="PRM" key="prm.netmedx.alltimeslocalsitetimes"/>)
			 	</TD>
			</TR>

			<TR>
			  <TD class=dblabel colspan="9">
					<bean:message bundle="PRM" key="prm.netmedx.preferredarrivalwindowbetween"/>:
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  		   
  		     		<html:text  styleClass="textbox" size="10" property="arrivalwindowstartdate" readonly="true"/>
	   				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].arrivalwindowstartdate, document.forms[0].arrivalwindowstartdate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;" />
  				    &nbsp;&nbsp;&nbsp; 
				    
				    <html:select name="TicketDetailForm" property = "arrivalwindowstartdatehh" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
					</html:select>
					
					<html:select name="TicketDetailForm" property = "arrivalwindowstartdatemm" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
					</html:select>
					
					<html:select name="TicketDetailForm" property = "arrivalwindowstartdateop" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
					</html:select>
			 	
			 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 		
					<html:text  styleClass="textbox" size="10" property="arrivalwindowenddate" readonly="true"/>
	   				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].arrivalwindowenddate, document.forms[0].arrivalwindowenddate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;" />
  				    &nbsp;&nbsp;&nbsp; 
				    
				    <html:select name="TicketDetailForm" property = "arrivalwindowenddatehh" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
					</html:select>
					
					<html:select name="TicketDetailForm" property = "arrivalwindowenddatemm" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
					</html:select>
					
					<html:select name="TicketDetailForm" property = "arrivalwindowenddateop" styleClass="combowhite">  
					<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
					</html:select>

			  </TD>
			</TR>
			
			<TR height=30>
				<TD align=left colSpan="9" id ='crit'>
					<html:submit styleClass = "button" property = "save" onclick = "return validate();">Save</html:submit>&nbsp;
					<html:reset property = "reset" styleClass = "button">Cancel</html:reset>&nbsp;
					<%if(accountingDB!=null) { 
						if(nettype.equals("webTicket")) {
					%>
					<html:button property = "back" styleClass = "button" onclick = "return Backaction();">Back</html:button>
						<%}
						else {
						 %>
					<html:button property = "back" styleClass = "button" onclick = "return BackHistory();">Back</html:button>
					<%} %>
										
					<%}else { %>
					<html:button property = "back" styleClass = "button" onclick = "return Backaction();">Back</html:button>
					<%} %>
	            </TD>
		   </TR>

		</TD></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<logic:present name = "refreshtree" scope = "request">
<script>
parent.ilexleft.location.reload();
</script>
</logic:present>
<table>


<script>
function changeCountry()
{
	
	document.forms[0].refersh.value="true";
	document.forms[0].action = "TicketDetailAction.do?jobid=<bean:write name="TicketDetailForm" property="jobid"/>&nettype=jobdashboard&ticket_id=<bean:write name="TicketDetailForm" property="ticket_id"/>&appendixid=<bean:write name="TicketDetailForm" property = "appendixid"/>";
	document.forms[0].submit();
	return true;
}


function getResourceLevel()
{
var j = 0 ;
for(var i = 0; i<document.forms[0].resourcelevel.options.length;i++) {
		if(document.forms[0].resourcelevel.options[i].text == 'Systems Engineer')
			j = i;
	}
	
		document.forms[0].resourcelevel[j].selected  = true;
}



function viewSummaryPage(ticket_number)
{
	str = "ViewTicketSummaryAction.do?ticket_number="+ticket_number;
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 400, height = 700 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}

function validate()
{
			if( document.forms[0].requestor_name.value == "" )
	 		{
	 			alert("<bean:message bundle="PRM" key="prm.netmedx.requestornamevalidation"/>");
	 			document.forms[0].requestor_name.focus();
				return false;
	 		}
	 		
 			if(!isEmail(document.forms[0].requestor_email.value)&&(!isBlank(document.forms[0].requestor_email.value)))
			{
				alert("<bean:message bundle="PRM" key="prm.netmedx.emailvalidation"/>");		
				document.forms[0].requestor_email.focus();
				return false;
			}
	 		
	 		if( document.forms[0].site_state.value == 0 )
		 		{
		 			alert("<bean:message bundle="PRM" key="prm.netmedx.statevalidation"/>");
		 			if(document.forms[0].site_state.disabled){}
		 			else
			 			document.forms[0].site_state.focus();
					return false;
		 		}
	 		
	 		if( document.forms[0].site_country.value == "" )
	 		{
	 			alert("<bean:message bundle="PRM" key="prm.netmedx.countryvalidation"/>");
	 			if(document.forms[0].site_country.disabled){}
		 		else
	 			document.forms[0].site_country.focus();
				return false;
	 		}
	 		
	 		if( !isFloat( document.forms[0].locality_uplift.value ) )
	 		{
	 			alert( "<bean:message bundle = "PRM" key = "prm.netmedx.validunionupliftfactor"/>" );
	 			document.forms[0].locality_uplift.value = "";
	 			document.forms[0].locality_uplift.focus();
				return false;
	 		}
	 		
	 		if( document.forms[0].locality_uplift.value == "" )
	  			{
	  				alert( "<bean:message bundle = "PRM" key = "prm.netmedx.unionupliftfactorenter"/>" );
	  				document.forms[0].locality_uplift.focus();
	  				return false;
	  			}
	  			
	  		if(!isEmail(document.forms[0].primary_email.value)&&(!isBlank(document.forms[0].primary_email.value)))
			{
				alert("<bean:message bundle="PRM" key="prm.netmedx.emailvalidation"/>");		
				document.forms[0].primary_email.focus();
				return false;
			}
			
			if(!isEmail(document.forms[0].secondary_email.value)&&(!isBlank(document.forms[0].secondary_email.value)))
			{
				alert("<bean:message bundle="PRM" key="prm.netmedx.emailvalidation"/>");		
				document.forms[0].secondary_email.focus();
				return false;
			}	
	  			
	 		if( document.forms[0].requested_date.value == "" )
	  			{
	  				alert("<bean:message bundle="PRM" key="prm.netmedx.requestdatevalidation"/>");
	  				document.forms[0].requested_date.focus();
	  				return false;
	  			}
	 		
	 		if( document.forms[0].category.value == -1 )
	 		{
	 			alert("<bean:message bundle="PRM" key="prm.netmedx.categoryvalidation"/>");
	 			document.forms[0].category.focus();
				return false;
	 		}	
	 		
	 		if (document.forms[0].customer_references.value == "" )
	 		{
	 			alert("<bean:message bundle="PRM" key="prm.netmedx.customerreference.validation"/>");
	 			document.forms[0].customer_references.focus();
				return false;
	 		}	
	 		
 			if( document.forms[0].problem_description.value == "" )
	 		{
	 			alert("<bean:message bundle="PRM" key="prm.netmedx.problemdescvalidation"/>");
	 			document.forms[0].problem_description.focus();
				return false;
	 		}

	
			if( <%= depotlistsize %> != 0 )
			{
				if( <%= depotlistsize %> == 1) {
					if(document.forms[0].depot_quantity.value == "")
		  			{
		  				document.forms[0].depot_quantity.value = "0";
		  			}
	 
			  		if( !isInteger(document.forms[0].depot_quantity.value ) )
			 		{
			 			alert( "<bean:message bundle = "PRM" key = "prm.netmedx.depotquantityvalidation"/>" );
			 			document.forms[0].depot_quantity.value = "";
			 			document.forms[0].depot_quantity.focus();
						return false;
			 		}
				}
				
				else {
					for( var i = 0; i<<%= depotlistsize %>; i++ )
					  	{
							if(document.forms[0].depot_quantity[i].value == "")
				  			{
				  				document.forms[0].depot_quantity[i].value = "0";
				  			}
			 
					  		if( !isInteger(document.forms[0].depot_quantity[i].value ) )
					 		{
					 			alert( "<bean:message bundle = "PRM" key = "prm.netmedx.depotquantityvalidation"/>" );
					 			document.forms[0].depot_quantity[i].value = "";
					 			document.forms[0].depot_quantity[i].focus();
								return false;
					 		}
						}
				}
				
			}	
			
			if( document.forms[0].criticality.value == -1 )
		 		{
		 			alert("<bean:message bundle="PRM" key="prm.netmedx.criticalityvalidation"/>");
		 			document.forms[0].criticality.focus();
					return false;
		 		}
		 	if( <%= criticalitycatlistsize %> != 0 )
			{
				var message = "0";
				if( <%= criticalitycatlistsize %> == 1) {
					if(!document.forms[0].crit_cat_id.checked)
		  			{
		  				alert( "<bean:message bundle="PRM" key="prm.netmedx.popvalidation"/>" );
		  				document.forms[0].crit_cat_id.focus();
						return false;
		  			}
	 
			  	}
				
				else {
					for( var i = 0; i<<%= criticalitycatlistsize %>; i++ )
					  	{
							if(document.forms[0].crit_cat_id[i].checked)
				  			{
				  				message = "1";
				  			}
						}
					if(message == "0")	{
						alert( "<bean:message bundle="PRM" key="prm.netmedx.popvalidation"/>" );
						document.forms[0].crit_cat_id[0].focus();
						return false;
					}	
				}
			} 
		 	if( document.forms[0].resourcelevel.value == -1 )
		 		{
		 			alert("<bean:message bundle="PRM" key="prm.netmedx.resourcelevelvalidation"/>");
		 			if(document.forms[0].resourcelevel.disabled==false)	
			 			document.forms[0].resourcelevel.focus();
					return false;
		 		}	
	 	
	 		if( document.forms[0].arrivaldate.value == "" )
	  			{
	  				alert("<bean:message bundle="PRM" key="prm.netmedx.preferreddatevalidation"/>");
	  				document.forms[0].arrivaldate.focus();
	  				return false;
	  			}
	 	
	 		
	 	   if(document.forms[0].requested_datehh.value != "  ")
	 		{
	 			if(document.forms[0].requested_date.value == "") {
					alert("<bean:message bundle="PRM" key="prm.netmedx.requestdatevalidation"/>");	 			
	 				document.forms[0].request_date.focus();
	 				return false;
	 			}
	 		}
	 		
	 		if(document.forms[0].arrivaldatehh.value != "  ")
	 		{
	 			if(document.forms[0].arrivaldate.value == "") {
					alert("<bean:message bundle="PRM" key="prm.netmedx.preferreddatevalidation"/>");	 			
	 				document.forms[0].arrivaldate.focus();
	 				return false;
	 			}
	 		}
	 		
	 		if(document.forms[0].arrivalwindowstartdatehh.value != "  ")
	 		{
	 			if(document.forms[0].arrivalwindowstartdate.value == "") {
					alert("<bean:message bundle="PRM" key="prm.netmedx.preferredwindowsrartdatevalidation"/>");	 			
	 				document.forms[0].arrivalwindowstartdate.focus();
	 				return false;
	 			}
	 		}
	 		
	 		if(document.forms[0].arrivalwindowenddatehh.value != "  ")
	 		{
	 			if(document.forms[0].arrivalwindowenddate.value == "") {
					alert("<bean:message bundle="PRM" key="prm.netmedx.preferredwindowenddatevalidation"/>");	 			
	 				document.forms[0].arrivalwindowenddate.focus();
	 				return false;
	 			}
	 		}
	 		
	 		
	 		
 			if(document.forms[0].arrivalwindowstartdate.value!="" && document.forms[0].arrivalwindowenddate.value!="") 
				{
					if (!compDate_mdy(document.forms[0].arrivalwindowstartdate.value, document.forms[0].arrivalwindowenddate.value)) 
					{
						alert("<bean:message bundle = "PRM" key = "prm.netmedx.datemessage"/>");
						document.forms[0].arrivalwindowenddate.focus();
						return false;
					} 
				}
				
				if(document.forms[0].sitelatmin.value!="")
		{
			var re2digit=/^\d+$/ 
			if (document.forms[0].sitelatmin.value.search(re2digit)==-1) 
			{
			alert("Please Enter Numeric Value for Latitude Minute");
			document.forms[0].sitelatmin.value = "";
			document.forms[0].sitelatmin.focus();
			return false;
			}
		}
		
		
		if(document.forms[0].sitelonmin.value!="")
		{
			var re2digit=/^\d+$/ 
			if (document.forms[0].sitelonmin.value.search(re2digit)==-1) 
			{
			alert("Please Enter Numeric Value for Longitude Minute");
			document.forms[0].sitelonmin.value = "";
			document.forms[0].sitelonmin.focus();
			return false;
			}
		}
	 	if(document.forms[0].sitelatdeg.value != "") {
			var strInt = "^[0-9-]{" + 0 + "," + "}(\\.[0-9]{0," + "})?$";
			var reg = new RegExp(strInt);
			var val=document.forms[0].sitelatdeg.value;
			if (val.match(reg) != null) 
			{}
			else
			{
				alert("Please enter a numeric value in lattitude degree." );
	 			document.forms[0].sitelatdeg.value = "";
	 			document.forms[0].sitelatdeg.focus();
	 			return false;
			}
		}
	 	
		if(document.forms[0].sitelondeg.value != "") {
			var strInt = "^[0-9-]{" + 0 + "," + "}(\\.[0-9]{0," + "})?$";
			var reg = new RegExp(strInt);
			var val=document.forms[0].sitelondeg.value;
			if (val.match(reg) != null) 
			{}
			else
			{
				alert("Please enter a numeric value in longitude degree." );
	 			document.forms[0].sitelondeg.value = "";
	 			document.forms[0].sitelondeg.focus();
	 			return false;
			}
		}		
				enableItems();
				
				
				
document.forms[0].ticket_type.value = 'saveticket';			
return true;
}

function search(search_type)
{
	str = "SearchProblemCategoryAction.do?search_type="+search_type+"&lo_ot_id="+document.forms[0].lo_ot_id.value;
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 450, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}

function opensearchsitewindow()
{
	document.forms[0].site_id.value = 0;
	var appendixid = document.forms[0].appendixid.value;

	str = "SiteSearch.do?lo_ot_id=<bean:write name = "TicketDetailForm" property = "lo_ot_id" />&ref=search&siteid=<bean:write name = "TicketDetailForm" property = "site_id" />&jobid=<bean:write name = "TicketDetailForm" property = "jobid" />&reftype=Add&appendixid="+appendixid+"&fromType=ticketDetail";
	
	suppstrwin = window.open( str , '' , 'left = 200 , top = 200 , width = 850 , height = 450 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();

	return true;
}
function BackHistory()
{
	history.go(-1);
}

function Backaction()
{


		
	if(document.forms[0].nettype.value == 'jobdashboard' ) {
		document.forms[0].action = "JobDashboardAction.do?jobid="+document.forms[0].jobid.value;
		
	}
	
	else if(document.forms[0].nettype.value == 'webTicket') {
	//	document.forms[0].action = "NetMedXDashboardAction.do";	
		document.forms[0].action ="NewWebTicketAction.do";
	}

	else {
		if(document.forms[0].nettype.value == 'dispatch')
			document.forms[0].action = "NetMedXDashboardAction.do?nettype="+document.forms[0].nettype.value+"&appendixid=-1&viewjobtype=<bean:write name = "TicketDetailForm" property = "viewjobtype" />&ownerId=<bean:write name = "TicketDetailForm" property = "ownerId" />";
		else 	
			document.forms[0].action = "NetMedXDashboardAction.do?nettype="+document.forms[0].nettype.value+"&appendixid="+document.forms[0].appendixid.value+"&viewjobtype=<bean:write name = "TicketDetailForm" property = "viewjobtype" />&ownerId=<bean:write name = "TicketDetailForm" property = "ownerId" />";
	}
	
	
	document.forms[0].submit();
	return true;
}


function setRequestorEmail()
{
var name = document.forms[0].requestor_name.value;
document.forms[0].requestor_email.value = name.substring(name.indexOf("~", name)+1, name.length+1);
}


function getpocList()
{
//alert(document.forms[0].lo_ot_id.value);
}


function getRefresh()
{

    var cookieval="crit";
    createCookie(cookieval);
    document.forms[0].refresh.value = "true";
	document.forms[0].refreshprodlist.value = "true";
	document.forms[0].addquantity.value = "";
	document.forms[0].dialog_number.value = eval(document.forms[0].dialog_number.value)+1;
	enableItems();
	document.forms[0].helpDeskCheck.value="false";
	document.forms[0].submit();
	return true;
}
function enableItems()
	{
				document.forms[0].site_name.disabled = false;
				document.forms[0].site_number.disabled = false;
				document.forms[0].site_address.disabled = false;
				document.forms[0].site_city.disabled = false;
				document.forms[0].site_state.disabled = false;
				document.forms[0].site_zipcode.disabled = false;
				document.forms[0].site_country.disabled = false;
				document.forms[0].sitecategory.disabled = false;
				document.forms[0].siteregion.disabled = false;
				document.forms[0].sitelatdeg.disabled = false;
				document.forms[0].sitelatmin.disabled = false;
				document.forms[0].sitelatdirection.disabled = false;
				document.forms[0].sitelondeg.disabled = false;
				document.forms[0].sitelonmin.disabled = false;
				document.forms[0].sitelondirection.disabled = false;
				document.forms[0].sitetimezone.disabled = false;
				document.forms[0].sitegroup.disabled = false;
				document.forms[0].siteendcustomer.disabled = false;
				document.forms[0].sitestatus.disabled = false;
				document.forms[0].resourcelevel.disabled=false;
			
	}

function addQuantity()
{
 	  if( document.forms[0].depot_requisition.value == "" )
 		{
 			alert("<bean:message bundle="PRM" key="prm.netmedx.productvalidation"/>");
 			document.forms[0].depot_requisition.focus();
			return false;
 		}
    document.forms[0].refresh.value = "true";
    document.forms[0].dialog_number.value = eval(document.forms[0].dialog_number.value)+1;
    document.forms[0].refreshprodlist.value = "false";
    document.forms[0].addquantity.value = "showquantity";
    enableItems();
	document.forms[0].submit();
	return true;
}
function getHelpDeskRefresh() {
document.forms[0].criticality.value=1;
<%if( criticalitycatlistsize > 0){%>
document.forms[0].crit_cat_id[0].checked=true;
<%}%>
//document.forms[0].crit_cat_id[0].checked=true;

enableItems();
document.forms[0].helpDeskCheck.value="true";
getResourceLevel();
document.forms[0].submit();
}



// Added Start:By Amit This will be called on each href that opens other form
function createCookie()
{
 var tableRow = document.getElementsByTagName('tr');

  var openDiv="";
	if (tableRow.length==0) { return; }

	for (var k = 0; k < tableRow.length; k++) {

		if(tableRow[k].getAttributeNode('id').value != "")
		{


			if(tableRow[k].style.display == "block" ){
			openDiv=openDiv+tableRow[k].getAttributeNode('id').value+",";
			}
		}
	}
		if(openDiv!="")
		{ // Save as cookie
		 document.cookie = "TDb="+ openDiv+" ; ";
		 }


}


function createCookie(lastDivForFocus)
{
 var tableRow = document.getElementsByTagName('tr');

  var openDiv="";
  
	if (tableRow.length==0) { return; }

	for (var k = 0; k < tableRow.length; k++) {

		if(tableRow[k].getAttributeNode('id').value != "")
		{


			if(tableRow[k].style.display == "block" ){
			openDiv=openDiv+tableRow[k].getAttributeNode('id').value+",";
			// Following line is for getting last focus on a href
			
			}
		}
	}

		if(openDiv!="")
		{ // Save as cookie
		 document.cookie = "TDb="+ openDiv+" ; ";
		 // Following line is for getting last focus on a href
		
		 }
 		document.cookie = "TDb1="+ lastDivForFocus+" ; ";
 		
		

}

//following function would be called on OnLoad
function CookieGetOpenDiv()
{

  var tcookie = document.cookie;
  var opendiv;
  var unique;
  var st = tcookie.indexOf ("TDb");
  var cookievalue ="";
  if(st!= -1)
  {
    	st = st+3;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);

	  if (en > st)
        {
          cookievalue = tcookie.substring(st+1, en);
        }

     var tableRow = document.getElementsByTagName('tr');
	if (tableRow.length==0) { return; }


	  for (var j = 0; j < tableRow.length; j++) {




		 		if(tableRow[j].getAttributeNode('id').value != "")
		 		{

		 		opendiv = cookievalue.substring(0,cookievalue.indexOf(","));

		 		if(opendiv.length>0)
	 			{
		 			if(tableRow[j].getAttributeNode('id').value==opendiv)
			 	 		{

			 	 			tableRow[j].style.display = "block";
			 	 			unique = opendiv.substring(opendiv.indexOf("_")+1,opendiv.length);
			 	 			action = "thisImage = document.getElementById('img_" + unique +"');";
							eval(action);

							if (document.getElementById('div_' + unique).offsetHeight > 0) {
								thisImage.src = "images/Collapse.gif";

							}
							else {
								thisImage.src = "images/Expand.gif";

							}
			 	 			if(opendiv=="div_1")
							{
								if(document.getElementById ( "classchange1" ).className =='trybtop')
								{
									document.getElementById ( "classchange1" ).className ='trybtopclose';
									document.getElementById ( "classchange2" ).className ='trybtopclose';
									document.getElementById ( "classchange3" ).className ='trybtopclose';
								}
								else
								{
									document.getElementById ( "classchange1" ).className = 'trybtop';
									document.getElementById ( "classchange2" ).className = 'trybtop';
									document.getElementById ( "classchange3" ).className = 'trybtop';
								}
							}
			 	 			 cookievalue =cookievalue.substring(cookievalue.indexOf(",")+1,cookievalue.length);

			 	 		}


		 	 	}

		 	 	//Next Increment for Open Div
		 	 }


	 	}


  }

  openDiv = "";
// Save as cookie
		 document.cookie = "TDb="+ openDiv+" ; ";
//Start:Added For Focus		
var st1 = tcookie.indexOf ("TDb1");
  var cookievalue1 ="";
  if(st1!= -1)
  {
    	st1 = st1+4;
      st1 = tcookie.indexOf ("=", st1);
      en = tcookie.indexOf (";", st1);

	  if (en > st1)
        {
          cookievalue1 = tcookie.substring(st1+1, en);
          
        }
}
 
 if(cookievalue1!=""){  
 	if(document.getElementById(cookievalue1)!=null)
 		{
    	  document.getElementById(cookievalue1).focus();
	    }
}
 document.cookie = "TDb1="+ openDiv+" ; ";
//End :Added For Focus
}


//End By Amit
function disableItems()
{
	document.forms[0].site_name.disabled = true;
	document.forms[0].site_number.disabled = true;
	document.forms[0].site_address.disabled = true;
	document.forms[0].site_city.disabled = true;
	document.forms[0].site_state.disabled = true;
	document.forms[0].site_zipcode.disabled = true;
	document.forms[0].site_country.disabled = true;
	document.forms[0].sitecategory.disabled = true;
	document.forms[0].siteregion.disabled = true;
	document.forms[0].sitelatdeg.disabled = true;
	document.forms[0].sitelatmin.disabled = true;
	document.forms[0].sitelatdirection.disabled = true;
	document.forms[0].sitelondeg.disabled = true;
	document.forms[0].sitelonmin.disabled = true;
	document.forms[0].sitelondirection.disabled = true;
	document.forms[0].sitetimezone.disabled = true;
	document.forms[0].sitegroup.disabled = true;
	document.forms[0].siteendcustomer.disabled = true;
	document.forms[0].sitestatus.disabled = true;
	
}
function enabledResource() {
		document.forms[0].resourcelevel.disabled=false;
}


</script>
<script>
function changeFilter()
{
	if(document.forms[0].nettype.value == 'dispatch'){
			document.forms[0].action = "NetMedXDashboardAction.do?nettype="+document.forms[0].nettype.value+"&appendixid=-1&viewjobtype=<bean:write name = "TicketDetailForm" property = "viewjobtype" />&ownerId=<bean:write name = "TicketDetailForm" property = "ownerId" />&showList=something";
	}else{
	document.forms[0].action="TicketDetailAction.do?appendixid=<bean:write name="TicketDetailForm" property="appendixid"/>&ticket_type=<bean:write name="TicketDetailForm" property="ticket_type"/>&viewjobtype=<bean:write name="TicketDetailForm" property="viewjobtype"/>&ownerId=<bean:write name="TicketDetailForm" property="ownerId"/>&fromPage=NetMedX&showList=something";
	}
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	if(document.forms[0].nettype.value == 'dispatch'){
			document.forms[0].action = "NetMedXDashboardAction.do?nettype="+document.forms[0].nettype.value+"&appendixid=-1&viewjobtype=<bean:write name = "TicketDetailForm" property = "viewjobtype" />&ownerId=<bean:write name = "TicketDetailForm" property = "ownerId" />&resetList=something";
	}else{
	document.forms[0].action="TicketDetailAction.do?appendixid=<bean:write name="TicketDetailForm" property="appendixid"/>&ticket_type=<bean:write name="TicketDetailForm" property="ticket_type"/>&viewjobtype=<bean:write name="TicketDetailForm" property="viewjobtype"/>&ownerId=<bean:write name="TicketDetailForm" property="ownerId"/>&fromPage=NetMedX&resetList=something";
	}
	document.forms[0].submit();
	return true;

}

</script>
</BODY>
</html:form>
</html:html>
