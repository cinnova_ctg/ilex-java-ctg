<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>
<% 
	String setvalue="";
	int size=0;
	String addSite = "";
	if(request.getAttribute( "size" ).toString()!=null) {
		size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
	}

	if(request.getAttribute("deleteTicket") != null){
		System.out.println("deleteTicket in JSP---"+request.getAttribute("deleteTicket").toString());
	}

	if(request.getAttribute("AddSite") != null){
		System.out.println("AddSite in JSP---"+request.getAttribute("AddSite").toString());
		addSite = request.getAttribute("AddSite").toString();
	}
%>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />

<title></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>

<%@ include  file="/Menu.inc" %>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<table>
<html:form action="NewWebTicketAction">

<table  border="0" cellspacing="2" cellpadding="1"  >
<tr>
  <td  width="2" height="0"></td>
    <td width="810">
  	<table border="0" cellspacing="1" cellpadding="0" width="800"> 
  	 <logic:present name="deleteTicket" scope="request">
	  	<tr>
	  		<td width="800" class="message" height="15" colspan="11">
	  			<logic:equal name="deleteTicket" value="0">
		  			<bean:message bundle = "PRM" key = "prm.webticket.deletesuccess"/>
		  		</logic:equal>
		  		
		  		<logic:notEqual name="deleteTicket" value="0">
		  			<bean:message bundle = "PRM" key = "prm.webticket.deletefailure"/>
		  		</logic:notEqual>
	  		</td>
	  	</tr>
	  </logic:present>
	  
	  <logic:present name="AddSite" scope="request">
	  	<tr>
	  		<td width="800" class="message" height="15" colspan="11">
	  			<logic:equal name="AddSite" value="0">
		  			<bean:message bundle = "PRM" key = "prm.webTicket.addSitesuccess"/>
		  		</logic:equal>
		  		
		  		<logic:equal name="AddSite" value="-90001">
		  			<bean:message bundle="PRM" key="prm.webTicket.siteexistfailure"/>
		  		</logic:equal>
		  		
		  		<% if(!(addSite.equals("0") || addSite.equals("-90001"))) { %>
		  			<bean:message bundle = "PRM" key = "prm.webTicket.addSitefailure"/>
		  		<%} %>
	  		</td>
	  	</tr>
	  </logic:present>
	  
     <logic:present name="webticketlist" scope="request">
	 <tr>
	    <td width="800" class="labeleboldwhite" height="40" colspan="11">
	    	Receive Secure Tickets
	    </td>
	  </tr>
	  
 <% if(size>0) {
	String prevPrManager = "";
	String bgcolor=""; 
	int i=1;
 %>
	<logic:iterate id="wtlist" name="webticketlist">
		<bean:define id ="site_number" name="wtlist" property="site_number" />
		<bean:define id ="prManager" name="wtlist" property="projectManager" type="java.lang.String"/>
			
		<% if(!(prevPrManager.equals(prManager))) {
			if(i > 1) { %>
				<tr height="10"><td></td></tr>	
		<%	} %>
			<tr>
				<td class="texthNoBorderLeftBold" colspan="8" style="padding-bottom:5px; padding-top:5px;">
					Project Manager:&nbsp;<bean:write name="wtlist" property="projectManager"/>
				</td>
			</tr>
		<% 	prevPrManager = prManager; %>
	    <tr> 
			<td class="texthNoBorder" rowspan="2" width="100">Customer</td>
			<td class="texthNoBorder" rowspan="2" width="140">Ticket #</td>
			<td class="texthNoBorder" rowspan="2" width="80">Site Number</td>
			<td class="texthNoBorder" rowspan="2" width="100">Requestor</td>
			<td class="texthNoBorder" colspan="2" width="120">Request Received</td>
			<td class="texthNoBorder" colspan="2" width="120">Preferred Arrival</td>
		<%--	<td class="texthNoBorder" rowspan="2" width="80"></td> --%>
		
		</tr>
		<TR>
	  	    <TD class="texthNoBorder" width="60"><DIV>Date</DIV></TD>
	  	    <TD class="texthNoBorder" width="60"><DIV>Time</DIV></TD>
	  	    <TD class="texthNoBorder" width="60"><DIV>Date</DIV></TD>
	  	    <TD class="texthNoBorder" width="60"><DIV>Time</DIV></TD>
	  	</TR>    
	  	<%} %>
		
	 	<%if((i%2)==0) {
				bgcolor = "Ntextoleftalignnowrap";
			}  else {
				bgcolor = "Ntexteleftalignnowrap";
			}
		%>
	

		  <tr>
			 <td class="<%=bgcolor %>" ><bean:write name="wtlist" property="msa_name"/></td>	
			<td class="<%=bgcolor %>" align="center"><bean:write name="wtlist" property="ticket_number"/>&nbsp;&nbsp;[<A href="JobDashboardAction.do?webTicketType=webTicket&appendixid=<bean:write name="wtlist" property="appendix_id"/>&jobid=<bean:write name="wtlist" property="jobid"/>&ticket_id=<bean:write name="wtlist" property="ticket_id"/>&ticket_type=existingTicket">Receive</A>]&nbsp;&nbsp;[<a href="#" onclick = "return viewSummaryPage('<bean:write name="wtlist" property="ticket_number"/>');" >Details</a>]
			 </td>
	 
			<html:hidden name="wtlist" property="site_name"/>
		
			 <td  class="<%=bgcolor %>" align="center">
			 	<% if(site_number.equals("")) { %>
			 		[<a href="SiteManage.do?method=Add&appendixid=<bean:write name="wtlist" property="appendix_id"/>&jobid=<bean:write name="wtlist" property="jobid"/>&ticketNumber=<bean:write name="wtlist" property="ticket_number"/>&pageType=webTicket">Add Site</a>] 
			 	<%} else { %>
				 	<bean:write name="wtlist" property="site_number"/>
				 <%} %>
			 </td>
			 
			 <td  class="<%=bgcolor %>"><bean:write name="wtlist" property="requestor_name"/></td>
			 <td  class="<%=bgcolor %>" align="center"><bean:write name="wtlist" property="request_received_date"/></td>
			 <td  class="<%=bgcolor %>" align="center"><bean:write name="wtlist" property="request_received_time"/></td>
			 <td  class="<%=bgcolor %>" align="center"><bean:write name="wtlist" property="preferred_arrival_date"/></td>
			 <td  class="<%=bgcolor %>" align="center"><bean:write name="wtlist" property="preferred_arrival_time"/></td>

<%--
			 <td  class="<%=bgcolor %>">	 
				 [<a href="#" onclick ="javascript:del('<bean:write name="wtlist" property="ticket_number"/>');">Delete</a>]  
			 </td>
--%>			 
			</tr>
			
		    <%i++; %>
		</logic:iterate>

<% } else { %>
	 <tr> 
	    <td class="message" width="710" colspan="10">
	    	<bean:message bundle = "PRM" key = "prm.nts.nonetmedxmsapresent"/>
	    </td>
	 </tr>
<% } %>

</logic:present>  
  </table>
 </td>
 </tr>
</table> 
<script>
function viewSummaryPage(ticket_number)
{
	str = "ViewTicketSummaryAction.do?ticket_number="+ticket_number;
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 400, height = 700 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}

function del(ticketNumber)
{
	var conval = confirm( "Do you want to delete this ticket?" );
	if(conval) {
		document.forms[0].action = "NewWebTicketAction.do?ticketNumber="+ticketNumber+"&function=delete";
		document.forms[0].submit();
		return true;	
	}
}

</script>

</html:form>
</table>
</BODY>
</html:html>
