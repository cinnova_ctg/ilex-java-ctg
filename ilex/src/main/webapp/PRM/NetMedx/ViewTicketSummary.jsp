<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<title><bean:message bundle = "PRM" key = "prm.viewticketsummary.viewticketsummary" /></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<%@ include  file="/Menu.inc" %>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table>
<html:form action="ViewTicketSummaryAction">
<table  border="0" cellspacing="1" cellpadding="1"  >
<tr>
  <td  width="2" height="0"></td>
    <td width="370">
  	<table border="0" cellspacing="1" cellpadding="1" width="360"> 
		<tr>    
			<td colspan = "2" class = "labeleboldhierrarchy" height = "30"><bean:message bundle = "PRM" key = "prm.viewticketsummary.summaryforticket" /><bean:write name = "ViewTicketSummaryForm" property = "ticket_number"/></td>
		</tr> 
		<tr> 
			<td colspan = "2" class = "labellobold" height = "30"><bean:message bundle = "PRM" key = "prm.viewticketsummary.siteinformation" /></td>
		</tr>
		<tr>	
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.sitename" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "site_name"/></td>
		</tr>
 		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.sitenumber" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "site_number"/></td>
		</tr>
		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.sitephone" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "site_phone"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.siteworklocation" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "site_worklocation"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.siteaddress" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "site_address"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.sitecity" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "site_city"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.sitestate" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "site_state"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.sitezipcode" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "site_zipcode"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.siteprimarypocname" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "primary_Name"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.siteprimarypocphone" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "pri_site_phone"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.sitesecondarypocname" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "secondary_Name"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.sitesecondarypocphone" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "secondary_phone"/></td>
  		</tr>
  		<tr> 
			<td colspan = "2" class = "labellobold" height = "30"><bean:message bundle = "PRM" key = "prm.viewticketsummary.general" /></td>
		</tr>
		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.ponumber" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "po_number"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.requestorname" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "contact_person"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.requestoremail" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "email"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.requestorphone" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "contact_phone"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.requesteddate" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "requested_date"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.criticality" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "criticality"/></td>
  		</tr>
		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.arrivaldate" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "arrivaldate"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.arrivalwindowstartdate" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "arrivalwindowstartdate"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.arrivalwindowenddate" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "arrivalwindowenddate"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.problemdescription" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "problem_description"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.specialinstructions" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "sp_instructions"/></td>
  		</tr>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.standby" /></td>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "stand_by"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.customerspecificfields" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "cust_specific_fields"/></td>
  		</tr>
  	<%--	<bean:define id="other_email_info" name = "ViewTicketSummaryForm" property = "other_email_info"/> --%>
  		<tr>
		   <td class = "labelobold" height = "20" width = "180">Other Details</td>
	<%--   <td class = "labelo" width = "180"><%= other_email_info%></td> --%>
		   <td class = "labelo" width = "180"><bean:write name = "ViewTicketSummaryForm" property ="other_email_info"/></td>
  		</tr>
  		<tr>
		   <td class = "labelebold" height = "20" width = "180"><bean:message bundle = "PRM" key = "prm.viewticketsummary.createddate" /></td>
		   <td class = "labele" width = "180"><bean:write name = "ViewTicketSummaryForm" property = "created_date"/></td>
  		</tr>
  		<tr> 
			<td  class="buttonrow" height="20" colspan="2"><html:button property="close" styleClass="button" onclick="return window.close();"><bean:message bundle = "PRM" key = "prm.viewticketsummary.close" /></html:button></td>
	    </tr>
  </table>
 </td>
 </tr>
</table> 
</html:form>
</table>
</BODY>
</html:html>
