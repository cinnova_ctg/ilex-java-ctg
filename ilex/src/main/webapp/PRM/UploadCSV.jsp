<!--
THIS FILE WILL BE USED AS AN INTERFACE FOR FILE UPLOAD
-->
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<bean:define id="MSAEndCustomerList" name="MSAEndCustomerList" scope="request"/>
<bean:define id="dcTimeZoneList" name="dcTimeZoneList" scope="request"/>
<html:html>
<head>
<% 
boolean csschooser = true;
String path = "";
String MSA_Id="";
String fromProposalMg="No";
String isBlank="";
if(request.getAttribute("path")!=null)
{
	path = ""+request.getAttribute("path");
}
if(request.getAttribute("fromProposalMg")!=null)
{
	fromProposalMg = (String)request.getAttribute("fromProposalMg");
	request.setAttribute("fromProposalMg","Yes");
}
//If CSV File Is Blank
if(request.getAttribute("BalnkCSV")!=null)
{
 isBlank=(String)request.getAttribute("BalnkCSV");
}
//If CSV is Blank
%>
<title>ILEX</title>
<%@ include file = "/Header.inc" %>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<link rel = "stylesheet" href="styles/summary.css" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css">  
</head>

<%@ include file = "/Menu.inc" %>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0">
<table>
<html:form action = "/UploadCSV" enctype = "multipart/form-data">
<html:hidden property = "msaid" /> 
<html:hidden property="ownerId" />
<html:hidden property="viewjobtype" />
<html:hidden property="fromProposalMg" value="<%= fromProposalMg %>"/>
<html:hidden property="siteSearchName"/>
<html:hidden property="siteSearchNumber"/>
<html:hidden property="siteSearchCity"/>
<html:hidden property="siteSearchState"/>
<html:hidden property="siteSearchZip"/>
<html:hidden property="siteSearchCountry"/>
<html:hidden property="siteSearchGroup"/>
<html:hidden property="siteSearchEndCustomer"/>
<html:hidden property="siteSearchStatus"/>
<html:hidden property="current_page_no"/>
<html:hidden property="total_page_size"/>
<html:hidden property="org_page_no"/>
<html:hidden property="org_lines_per_page"/>
<html:hidden property="siteNameForColumn"/>
<html:hidden property="siteNumberForColumn"/>
<html:hidden property="siteAddressForColumn"/>
<html:hidden property="siteCityForColumn"/>
<html:hidden property="siteStateForColumn"/>
<html:hidden property="siteZipForColumn"/>
<html:hidden property="siteCountryForColumn"/>
<html:hidden property ="fromType"/>
<html:hidden property ="pageType"/>

	<table width = "100%" border = "0" cellspacing="1" cellpadding = "1" align = "center" valign  = "top">
		     <tr><td width="2" height="0"></td> 
		     	 <td><table border="0" cellspacing="1" cellpadding="1" > 
		     	  <% if(isBlank.equalsIgnoreCase("balnkcsv")) { %>
		     	  <tr><td td class = "message" height = "30" colspan="5">
			        <bean:message bundle = "PRM" key = "prm.UploadCSV.csvFileisBlank"/>
  		          </td></tr>
			      <% } %>
			       
		     	 <tr>
		     	 <%if(fromProposalMg.equalsIgnoreCase("Yes")){ %>
				 <td  height = "30"><h1><bean:message bundle = "PRM" key = "prm.UploadCSV.new.uploadSites"/>&nbsp;<bean:write name="UploadCSVForm" property="msaname" /></h1></td>		     	 
		     	 <%} else { %>
	             <td class = "Nlabeleboldwhite" height = "30"><bean:message bundle = "PRM" key = "prm.UploadCSV.new.uploadSites"/>&nbsp;<bean:write name="UploadCSVForm" property="msaname" /></td>
	             <% } %>
	             </tr>
       		 
       		     <tr>
			   		 <td class = "Ntextwhite" colspan = "2">
			   			 <bean:message bundle = "PRM" key = "prm.UploadCSV.siteData"/> <html:file property = "sitefilename" styleClass = "Nbutton" />
		    		 </td>
				 </tr>
				 
				 <tr>
		    		 <td class = "Ntextwhite" colspan = "2">
						<bean:message bundle="PRM" key="prm.UploadCSV.endcustomer"/><font class="red"> *</font>
			    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<html:select  name = "UploadCSVForm" property="endCustomer" size="1" styleClass="select">
						<html:optionsCollection name = "MSAEndCustomerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
						</html:select>
					</td>
				</tr>
				
 				 <tr>
				  <td>
			       <html:submit property="upload" styleClass="button_c" onclick="javascript:return uploadCheck();"><bean:message bundle="PRM" key="prm.button.upload"/></html:submit>
			       <html:reset property="reset" styleClass="button_c"><bean:message bundle="PRM" key="prm.button.Cancel"/></html:reset>
			      </td>			            
			     </tr>  
			      </table>   
				 </td>
				</tr> 
			        	<tr><td width="2" height="0"></td> 
		     				<td>
		     				<table width = "60%" border = "0" cellspacing="1" cellpadding = "1">
					        <tr>
						        <td class = "Ntextwhite" colspan="3">
						   		<bean:message bundle = "PRM" key = "prm.UploadCSV.csvDirection"/>
					    		</td>
					        </tr>
					        <tr>
					             <td class = "colCaptions"><bean:message bundle = "PRM" key = "prm.UploadCSV.FieldName"/></td>
					             <td class = "colCaptions"><bean:message bundle = "PRM" key = "prm.UploadCSV.Format"/></td>
				             	 <td class = "colCaptions"><bean:message bundle = "PRM" key = "prm.UploadCSV.Mandatory"/></td>
			       			</tr>
					<%
					      String labelForFieldName="";
						  String labelForDataType="";
						  String labelForFiieldLength="";
						  String readonlynumberclass="";
						  String labelIsMendatory="";%>          
			       
					<logic:iterate id = "activ" name = "tableData" scope = "request">
						<%if ( csschooser == true )  {
								labelForFieldName="rowDark";
								labelForDataType="rowDark";
								labelForFiieldLength="rowDark";
								labelIsMendatory="rowDark";
								csschooser = false;
							}  else {
							  labelForFieldName="rowLight";
							  labelForDataType="rowLight";
				    		  csschooser = true;
							  labelForFiieldLength="rowLight";
							  labelIsMendatory="rowLight";
							}
							%>
			  				<tr>
			  					<td class = "<%= labelForFieldName %>">
			  					<bean:write name = "activ" property = "fieldName"/>
			  					</td>	
								<td class = "<%= labelForDataType %>">
								<bean:write name = "activ" property = "dataType"/>
								<bean:write name = "activ" property = "fieldNameLength"/>
								</td>				  
							    <td class = "<%= labelIsMendatory %>"><bean:write name = "activ" property = "fieldNameMandatory"/></td>				  
			 				</tr>
					</logic:iterate>			
   		      
			    <tr>
    		       <td class="rowLight"  colspan="3"> 
				   <span class="dbvalue"><a href="javascript:openTemplete();"><bean:message bundle="PRM" key="prm.button.downLoadTemplate"/></a></span>
				   </td>
				</tr>

				<tr>
				    <td class="rowLight" align="center"  colspan="3"> 
					<%if(fromProposalMg.equalsIgnoreCase("Yes")){ %>
					<html:button property="back" styleClass="button_c" onclick = "javascript:history.go(-1);"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					<%}else{ %>
					<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					<%} %>
				    </td>
				</tr>
	 			 </table>   
				 </td>
				 </tr> 
			  </table>		   	
	</td>
	</tr> 
</table>
				
</html:form>
</table>
</body>
<script>
function uploadCheck()
{
	if( document.forms[0].sitefilename.value == "" )
	{
		alert("<bean:message bundle = "PRM" key = "prm.UploadCSV.pleaseAttachAFile"/>");
		document.forms[0].sitefilename.focus();
		return false;
	}
}
function searchsite()
{
	str = "SiteSearch.do?ref=viewAllSites&MSA_Id="+document.forms[0].msaid.value+"&appendixid=0";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600 , height = 400 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();
	return true;
}

function Backactionjob()
{

	
	if(document.forms[0].fromType.value=="siteManagement")
		{
			document.forms[0].action = "SiteManagement.do?viewjobtype=I&appendixid=0&ownerId=<bean:write name = "UploadCSVForm" property = "ownerId"/>&siteSearchName=<bean:write name = "UploadCSVForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "UploadCSVForm" property = "siteSearchNumber"/>&lines_per_page=<bean:write name = "UploadCSVForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "UploadCSVForm" property = "current_page_no"/>&org_page_no=<bean:write name = "UploadCSVForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "UploadCSVForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "UploadCSVForm" property = "total_page_size"/>&ownerId=<bean:write name = "UploadCSVForm" property = "ownerId"/>&siteNameForColumn=<bean:write name = "UploadCSVForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "UploadCSVForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "UploadCSVForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "UploadCSVForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "UploadCSVForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "UploadCSVForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "UploadCSVForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "UploadCSVForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "UploadCSVForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "UploadCSVForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "UploadCSVForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "UploadCSVForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "UploadCSVForm" property = "siteSearchEndCustomer"/>&siteSearchStatus=<bean:write name = "UploadCSVForm" property = "siteSearchStatus"/>&pageType=<bean:write name = "UploadCSVForm" property = "pageType"/>&msaid=<bean:write name = "UploadCSVForm" property = "msaid"/>";
		}
	if (document.forms[0].fromType.value=="SiteMenu")
		{
			document.forms[0].action="SiteMenu.do?viewjobtype=I&ownerId=<bean:write name = "UploadCSVForm" property = "ownerId"/>&msaid=<bean:write name = "UploadCSVForm" property = "msaid"/>";
		}
	
		
	document.forms[0].submit();
	return true;
}

function openTemplete() {
	document.forms[0].action = "UploadCSV.do?appendixid=0&view_templete=true";
	document.forms[0].submit();
	return true;
}
</script>
</html:html>


