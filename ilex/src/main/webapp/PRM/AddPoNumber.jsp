<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<% 
int activity_size=0;
if(request.getAttribute( "size" )!=null)
{
	activity_size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
}
 
String typeid=request.getAttribute( "typeid" ).toString();

String type=request.getAttribute( "type" ).toString();
%>
<html:html>


<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>

<%@ include  file="/Menu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing=1" cellpadding="0" height="18">
        <tr align="left"> 
          <td class="toprow1" width=200>
          
		  <%if(request.getAttribute( "type" ).equals("J")||request.getAttribute( "type" ).equals("AJ")||request.getAttribute( "type" ).equals("CJ")||request.getAttribute( "type" ).equals("IJ"))
          {%> 
          <a href="JobHeader.do?jobid=<%=typeid %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtojobdashboard"/></a></td>
		  <%} %>
		  <%if(request.getAttribute( "type" ).equals("A")||request.getAttribute( "type" ).equals("AA")||request.getAttribute( "type" ).equals("CA")||request.getAttribute( "type" ).equals("IA"))
          {%> 
          <a href="ActivityHeader.do?activityid=<%=typeid %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtoactivitydashboard"/></a></td>
		  <%} %></td>
		   <td  width=600 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>

<table>
<html:form action="AddPoNumber">
<html:hidden property="type" />
<html:hidden property="typeid" />
<html:hidden property="function" />
<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="300">
	
	  		<%if(request.getAttribute("assignmessage").equals("0") )
	  		{%>
	  			
	  		  
	  			<logic:present name = "AddPoNumberForm" property="updatemessage">
				<tr><td class="labeleboldwhite" height="30">&nbsp;</td>
				<td  colspan="6" class="message" height="30" >
		 		<logic:equal name="AddPoNumberForm" property="updatemessage" value="0">
		 		
					<bean:message bundle="PRM" key="prm.poupdatedsuccessfully"/>
		 		</logic:equal>
		 		<logic:equal name="AddPoNumberForm" property="updatemessage" value="-9001">
		 		
					<bean:message bundle="PRM" key="prm.insertfailed"/>
		 		</logic:equal>
		 		<logic:equal name="AddPoNumberForm" property="updatemessage" value="-9002">
		 		
					<bean:message bundle="PRM" key="prm.insertfailed"/>
		 		</logic:equal>
		 		<logic:equal name="AddPoNumberForm" property="updatemessage" value="-9003">
		 		
					<bean:message bundle="PRM" key="prm.insertfailed"/>
		 		</logic:equal>
		 		</td></tr>
				</logic:present>
			  			
	  		  
				
			  <tr> 
			   <td class="labeleboldwhite" height="30">&nbsp;</td>
			    <td colspan="6" class="labeleboldwhite" height="30" ><bean:message bundle="PRM" key="prm.addponumber"/></td>
			  </tr> 
			  <%
			  if(request.getAttribute( "type" ).equals("J")||request.getAttribute( "type" ).equals("AJ")||request.getAttribute( "type" ).equals("CJ")||request.getAttribute( "type" ).equals("IJ")) 
				{%>
				
				  <tr>
					<td>&nbsp;</td>
					<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.jobname"/></td>
					<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.partnercompany"/></td>
					<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.estimatedcost"/></td>
					<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.invoicedcost"/></td>
				 	<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.ponumber"/></td>
				 	<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.wonumber"/></td>
					
				</tr>
				
				<logic:present name="jobdetaillist" scope="request">
					<logic:iterate id="jlist" name="jobdetaillist">
				<tr>
					<td   class="labelboldwhite" height="20"><html:multibox property="ckboxjob" ><bean:write  name="jlist" property = "jobid"/></html:multibox></td>
					<td   class="labelo" height="20"><bean:write  name="jlist" property="jobname" /></td>
					<td   class="labelo" height="20"><bean:write  name="jlist" property="partnername" /></td>
					<td   class="readonlytextnumberodd" height="20"><bean:write  name="jlist" property="estimatedcost" /></td>
					<td   class="readonlytextnumberodd" height="20"><bean:write  name="jlist" property="invoicedcost" /></td>
					<td   class="readonlytextnumberodd" height="20"><html:text  name="jlist" property="ponumber" styleClass = "textboxnumber" size="8" onblur = "return copyval( this , 'po' );"/></td>
					<td   class="readonlytextnumberodd" height="20"><html:text  name="jlist" property="wonumber" styleClass = "textboxnumber" size="8" onblur = "return copyval( this , 'wo' );"/></td>
					
				</tr>
				</logic:iterate>
					</logic:present> 
			<%} %>
			
			
			<logic:present name="Partnercostlist" scope="request">	
			
				<tr> 
				   <td class="labeleboldwhite" height="30">&nbsp;</td>
				    <td colspan="6" class="labeleboldwhite" height="30" ><bean:message bundle="PRM" key="prm.activitylist"/></td>
				  </tr> 	 
				 
				  
				  	
				<tr>
					<td>&nbsp;</td>
					<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.activityname"/></td>
					<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.partnercompany"/></td>
					<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.estimatedcost"/></td>
					<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.invoicedcost"/></td>
				 	<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.ponumber"/></td>
				 	<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.wonumber"/></td>
					
				</tr>

			 <%
			 	int i=1;
				String bgcolor=""; 
				String bgcolor1=""; 
				String valstr = null;
				String valstr1 = null;%>
				
				<logic:iterate id="plist" name="Partnercostlist">
				<html:hidden name = "plist" property = "hid_actid"/>
				
				<% valstr = "javascript: checkchkbox( this ,'po','"+i+"' );";  
				valstr1 = "javascript: checkchkbox( this ,'wo','"+i+"' );";  %>
			 	<%
			 		if((i%2)==0)
					{
						bgcolor = "hypereven"; 
						bgcolor1 = "readonlytextnumbereven";
					} 
				    else
					{
						bgcolor = "hyperodd";
						bgcolor1 = "readonlytextnumberodd";
					}
					%>
			
			<tr>
				<td   class="labelboldwhite" height="20"><html:multibox property="ckboxactivity"><bean:write name = "plist" property = "activityid"/></html:multibox></td>
				<td   class="<%=bgcolor %>" height="20"><bean:write name="plist" property="activityname" /></td>
				<td   class="<%=bgcolor %>" height="20"><bean:write name="plist" property="partnername" /></td>
				<td   class="<%=bgcolor1 %>" height="20"><bean:write name="plist" property="estimatedcost" /></td>
				<td   class="<%=bgcolor1 %>" height="20"><bean:write name="plist" property="invoicedcost" /></td>
				<td   class="<%=bgcolor %>" height="20"><html:text name="plist" property="ponumberact" styleClass = "textboxnumber" size="8" onchange = "<%=valstr%>"/></td>
				<td   class="<%=bgcolor %>" height="20"><html:text name="plist" property="wonumberact" styleClass = "textboxnumber" size="8" onchange = "<%=valstr1%>"/></td>
				
			</tr>
				<%i++; %>
				</logic:iterate>
				</logic:present> 
				 
			  <tr> 
			  	<td>&nbsp;</td>
			    <td colspan="6" class="ButtonRow"> 
			      <html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="PRM" key="prm.save" /></html:submit>
        		  <html:reset property="reset" styleClass="button"><bean:message bundle="PRM" key="prm.reset" /></html:reset>
			    </td>
			  </tr>
			  <%} 

			  
			  else{%>
			  
			  	<%if(request.getAttribute( "type" ).equals("J")||request.getAttribute( "type" ).equals("AJ")||request.getAttribute( "type" ).equals("CJ")||request.getAttribute( "type" ).equals("IJ")) 
				{%>
				
				  <tr>
				  <td>&nbsp;</td>
				  <td  colspan="6" class="message" height="30" >
				  <logic:present  name="AddPoNumberForm" property="message">
					<logic:equal  name="AddPoNumberForm" property="message" value="-9001">
					<bean:message bundle="PRM" key="prm.jobnotyetassigned"/>
			 		</logic:equal>
				  </logic:present>
				  <logic:present  name="AddPoNumberForm" property="message">
					<logic:equal  name="AddPoNumberForm" property="message" value="-9002">
					<bean:message bundle="PRM" key="prm.activitiesalreadyassigned"/>
			 		</logic:equal>
				  </logic:present>
				  </td>
				  </tr>
				  <%} %>
				  <%if(request.getAttribute( "type" ).equals("A")||request.getAttribute( "type" ).equals("AA")||request.getAttribute( "type" ).equals("CA")||request.getAttribute( "type" ).equals("IA")) 
				{%>
				
				  <tr>
				  <td>&nbsp;</td>
				  <td  colspan="6" class="message" height="30" >
				  <logic:present  name="AddPoNumberForm" property="message">
					<logic:equal  name="AddPoNumberForm" property="message" value="-9001">
					<bean:message bundle="PRM" key="prm.activitynotyetassigned"/>
			 		</logic:equal>
				  </logic:present>
				  <logic:present  name="AddPoNumberForm" property="message">
					<logic:equal  name="AddPoNumberForm" property="message" value="-9002">
					<bean:message bundle="PRM" key="prm.jobalreadyassigned"/>
			 		</logic:equal>
				  </logic:present>
				  
				  </td>
				  </tr>
				  <%} %>
				  
			  <%} %>
						   
			  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
  			  
			</table>
			 </td>
			 </tr> 
			</table>
			</html:form>
			
</table>
</body>
<script>
function validate( )
{
	var checkflag=0;
	if(document.forms[0].type.value == "J"||document.forms[0].type.value == "AJ"||document.forms[0].type.value == "CJ"||document.forms[0].type.value == "IJ")
	{
			if( document.forms[0].ckboxjob.checked)
			{
				if((document.forms[0].ponumber.value == 0||document.forms[0].ponumber.value == "")&&(document.forms[0].wonumber.value == 0||document.forms[0].wonumber.value == ""))
				{
					alert('po number and wo number cannot be blank for a checked checkbox');
					return false;
				}
				
				
			}
			
			else
			{
				if( <%= activity_size %> != 0 )
				{
					if( <%= activity_size %> == 1 )
					{
						if(document.forms[0].ckboxactivity.checked)
							{
								checkflag=1;
								if((document.forms[0].ponumberact.value == 0||document.forms[0].ponumberact.value == "")&&(document.forms[0].wonumberact.value == 0||document.forms[0].wonumberact.value == ""))
								{
									alert('po number and wo number cannot be blank for a checked checkbox');
									return false;
								}
								
							}
							
						
					}
					else
					{
						for ( var i = 0 ; i < document.forms[0].ckboxactivity.length; i++ )
						{
							
							if(document.forms[0].ckboxactivity[i].checked)
							{
								
								checkflag=1;
								if((document.forms[0].ponumberact[i].value == 0||document.forms[0].ponumberact[i].value == "")&&(document.forms[0].wonumberact[i].value == 0||document.forms[0].wonumberact[i].value == ""))
								{
									alert('po number and wo number cannot be blank for a checked checkbox');
									return false;
								}
								
							}
							
						}
					}
					
					
					if(checkflag==0)
					{
						alert('select a record first');
						return false;
					}
				}
				
			}
		return true;
	}
	if(document.forms[0].type.value == "A"||document.forms[0].type.value == "AA"||document.forms[0].type.value == "CA"||document.forms[0].type.value == "IA")
	{
		if( <%= activity_size %> != 0 )
		{
			
			if(document.forms[0].ckboxactivity.checked)
			{
				checkflag=1;
				if((document.forms[0].ponumberact.value == 0||document.forms[0].ponumberact.value == "")&&(document.forms[0].wonumberact.value == 0||document.forms[0].wonumberact.value == ""))
				{
					alert('po number and wo number cannot be blank for a checked checkbox');
					return false;
				}
				
			}
			
			if(!checkflag)
			{
				alert('select a record first');
				return false;
			}
		
		}	
	}
			
	return true;
}

function check( )
{
	alert('XXXX '+document.forms[0].ckboxjob.checked);
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( document.forms[0].ckboxjob.checked)
			{
				document.forms[0].ckboxjob.checked = true;
				document.forms[0].ckboxactivity.checked = true;
				document.forms[0].ponumberact.readOnly = true;
				document.forms[0].wonumberact.readOnly = true;
				
			}
			
			else
			{
				document.forms[0].ckboxjob.checked = false;
				document.forms[0].ckboxactivity.checked = false;
				document.forms[0].ponumberact.readOnly = false;
				document.forms[0].wonumberact.readOnly = false;
				
			}
		}
		else
		{
			
			if( document.forms[0].ckboxjob.checked)
			{
				document.forms[0].ckboxjob.checked = true;
				for ( var i = 0 ; i < document.forms[0].ckboxactivity.length; i++ )
				{
					document.forms[0].ckboxactivity[i].checked = true;
					document.forms[0].ponumberact[i].readOnly = true;
					document.forms[0].wonumberact[i].readOnly = true;
					
				}
			}
			
			else
			{
				document.forms[0].ckboxjob.checked = false;
				for ( var i = 0 ; i < document.forms[0].ckboxactivity.length; i++ )
				{
					
					document.forms[0].ckboxactivity[i].checked = false;
					document.forms[0].ponumberact[i].readOnly = false;
					document.forms[0].wonumberact[i].readOnly = false;
					
				}
			}
			
		}
	}
	return false;
}
function copyval( name , flag )
{
	
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( flag == 'po' )
			{
				document.forms[0].ponumberact.value = name.value;
				document.forms[0].ckboxjob.checked = true;
				//document.forms[0].ckboxactivity.checked = 'true';
				document.forms[0].ponumberact.readOnly = true;
			}
			
			if( flag == 'wo' )
			{
				document.forms[0].wonumberact.value = name.value;
				document.forms[0].ckboxjob.checked = true;
				//document.forms[0].ckboxactivity.checked = 'true';
				document.forms[0].wonumberact.readOnly = true;
			}
		}
		else
		{
			
			if( flag == 'po' )
			{	
				
				for ( var i = 0 ; i < document.forms[0].ckboxactivity.length; i++ )
				{
					document.forms[0].ponumberact[i].value = name.value;
					
					if((name.value==0||name.value=="")&&!(document.forms[0].ckboxjob.checked))
					{
						
						document.forms[0].ckboxjob.checked = false;
						document.forms[0].ponumberact[i].readOnly = false;
					}
					else
					{
					
						document.forms[0].ckboxjob.checked = true;
						document.forms[0].ponumberact[i].readOnly = true;
					}
				}
					return;
			}
			
			if( flag == 'wo' )
			{
				for ( var i = 0 ; i < document.forms[0].ckboxactivity.length; i++ )
				{
					document.forms[0].wonumberact[i].value = name.value;
					
					if((name.value==0||name.value=="")&&!(document.forms[0].ckboxjob.checked))
					{
						
							document.forms[0].ckboxjob.checked = false;
							document.forms[0].wonumberact[i].readOnly = false;
					}
					else
					{
						
						document.forms[0].ckboxjob.checked = true;
							document.forms[0].wonumberact[i].readOnly = true;
					}
					
					
					
				}
			}
		}
	}
	return false;
}


function checkchkbox(name , flag ,index )
{
	var j=index-1;
	if( <%= activity_size %> == 1 )
		{
			if( flag == 'po' )
			{
				if((name.value==0||name.value=="")&&(document.forms[0].wonumberact.value==0||document.forms[0].wonumberact.value==""))
				{
					
					document.forms[0].ckboxactivity.checked = false;
					
				}
				else
				{
				
					document.forms[0].ckboxactivity.checked = true;
					
				}
			
				return;
				
			}
			
			if( flag == 'wo' )
			{
				if((name.value==0||name.value=="")&&(document.forms[0].ponumberact.value==0||document.forms[0].ponumberact.value==""))
				{
					
					document.forms[0].ckboxactivity.checked = false;
					
				}
				else
				{
				
					document.forms[0].ckboxactivity.checked = true;
					
				}
			
				return;
				
			}
		}
	else
		{
			if( flag == 'po' )
			{	
			 
				if((name.value==0||name.value=="")&&(document.forms[0].wonumberact[j].value==0||document.forms[0].wonumberact[i].value==""))
				{
					
					document.forms[0].ckboxactivity[j].checked = false;
					
				}
				else
				{
				
					document.forms[0].ckboxactivity[j].checked = true;
					
				}
			
				return;
			}
			
			if( flag == 'wo' )
			{
					
					
				if((name.value==0||name.value=="")&&(document.forms[0].ponumberact[i].value==0||document.forms[0].ponumberact[i].value==""))
				{
					
					document.forms[0].ckboxactivity[j].checked = false;
						
				}
				else
				{
					
					document.forms[0].ckboxactivity[j].checked = true;
					
				}
					
					
			}
			return false;
		}
}
</script> 			
</html:html>