<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>

<%@ include  file="/Menu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing=1" cellpadding="0" height="18">
        <tr align="left"> 
         <td class="toprow1" width=200>
          
		  <%if(request.getParameter("type").equals("J")||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ"))
          {%> 
          <a href="JobHeader.do?jobid=<%=request.getParameter("typeid") %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtojobdashboard"/></a></td>
		  <%} %>
		  <%if(request.getParameter("type").equals("A")||request.getParameter("type").equals("AA")||request.getParameter("type").equals("CA")||request.getParameter("type").equals("IA"))
          {%> 
          <a href="ActivityHeader.do?activityid=<%=request.getParameter("typeid") %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtoactivitydashboard"/></a></td>
		  <%} %></td>
		   <td  width=600 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>



<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="300">
	  		<tr>
	  			<logic:present name = "unassignmessage" >
				<logic:equal name = "unassignmessage"  value="-9001">
				<table>
					<tr><td  width="100%" class="message" height="30" ><bean:message bundle="PVS" key="pvs.errorunassigningpartner"/></td></tr>
				</table> 
				</logic:equal>
				<logic:equal name = "unassignmessage"  value="0">
				<table>
					<tr><td  width="100%" class="message" height="30" ><bean:message bundle="PVS" key="pvs.unassignedsuccessfully"/></td></tr>
				</table> 
				</logic:equal>
				</logic:present> 
			</tr>
	  		
	  		
	  
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>

</table>
</body>

</html:html>