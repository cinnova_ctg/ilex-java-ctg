<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<%
	String Appendix_Id = "";
	if(request.getAttribute("Appendix_Id")!=null)
		Appendix_Id= request.getAttribute("Appendix_Id").toString();	
%>


<bean:define id="dcStateCM" name="dcStateCM"  scope="request"/>
<bean:define id="dcLonDirec" name="dcLonDirec" scope="request"/>
<bean:define id="dcLatDirec" name="dcLatDirec" scope="request"/>
<bean:define id="dcCountryList" name="dcCountryList" scope="request"/>
<bean:define id="dcSiteStatus" name="dcSiteStatus" scope="request"/>
<bean:define id="dcGroupList" name="dcGroupList" scope="request"/>
<bean:define id="dcEndCustomerList" name="dcEndCustomerList" scope="request"/>
<bean:define id="dcTimeZoneList" name="dcTimeZoneList" scope="request"/>
<bean:define id="dcRegionList" name="dcRegionList" scope="request"/>
<bean:define id="dcCategoryList" name="dcCategoryList" scope="request"/>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ILEX</title>
 <style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
    </style>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<link rel = "stylesheet" href="styles/summary.css" type="text/css">
<link rel = "stylesheet" href="styles/content.css" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/EditableCombo.js"></script>
<script language = "JavaScript" src = "javascript/CreateProject.js"></script>
</script>
    <script>
    function disableItems()
    {
    	if(document.forms[0].pageType.value=='jobsites')
    	{

    		document.forms[0].siteName.disabled = true;
			document.forms[0].siteNumber.disabled = true;
			document.forms[0].siteAddress.disabled = true;
			document.forms[0].siteCity.disabled = true;
			document.forms[0].stateState.disabled = true;
			document.forms[0].siteZipCode.disabled = true;
			document.forms[0].country.disabled = true;
			document.forms[0].siteCategory.disabled = true;
			document.forms[0].siteRegion.disabled = true;
			
			document.forms[0].siteTimeZone.disabled = true;
			document.forms[0].siteGroup.disabled = true;
			document.forms[0].siteEndCustomer.disabled = true;
			document.forms[0].siteStatus.disabled = true;
			document.forms[0].siteLocalityFactor.disabled = true;
			document.forms[0].siteUnion.disabled = true;
			document.forms[0].siteWorkLocation.disabled = true;
			document.forms[0].siteDirections.disabled = true;
			document.forms[0].sitePrimaryPhone.disabled = true;
			document.forms[0].sitePrimaryEmail.disabled = true;
			document.forms[0].sitePrimaryName.disabled = true;
			document.forms[0].siteSecondaryName.disabled = true;
			document.forms[0].siteSecondayPhone.disabled = true;
			document.forms[0].siteSecondaryEmail.disabled = true;
			document.forms[0].siteSpecialConditions.disabled = true;		
   		 }	
    }
    </script>

<%@ include  file="/NMenu.inc" %>
<%@ include  file="/JobMenu.inc" %>
<%@ include  file="/AjaxStateCountry.inc" %>
</head>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onload="leftAdjLayers();">
<html:form action = "SiteManage">
<html:hidden property="siteID"/>     
<html:hidden property="appendixid"/>
<html:hidden property="appendixname"/>
<html:hidden property="msaid"/>
<html:hidden property="msaname"/>
<html:hidden property="countRow"/>
<html:hidden property="siteSearchName"/>
<html:hidden property="siteSearchNumber"/>
<html:hidden property="siteSearchCity"/>
<html:hidden property="siteSearchState"/>
<html:hidden property="siteSearchZip"/>
<html:hidden property="siteSearchCountry"/>
<html:hidden property="siteSearchGroup"/>
<html:hidden property="siteSearchEndCustomer"/>
<html:hidden property="siteSearchTimeZone"/>
<html:hidden property="current_page_no"/>
<html:hidden property="total_page_size"/>
<html:hidden property="org_page_no"/>
<html:hidden property="org_lines_per_page"/>
<html:hidden property="siteNameForColumn"/>
<html:hidden property="siteNumberForColumn"/>
<html:hidden property="siteAddressForColumn"/>
<html:hidden property="siteCityForColumn"/>
<html:hidden property="siteStateForColumn"/>
<html:hidden property="siteZipForColumn"/>
<html:hidden property="siteCountryForColumn"/>
<html:hidden property="refersh"/>
<html:hidden property ="pageType"/>
<html:hidden property ="jobid"/>
<html:hidden property ="fromType"/>
<html:hidden property ="jobName"/>
<html:hidden property ="state"/>

<logic:equal name ="SiteManagementForm" property ="fromType" value ="yes">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
    				<tr>
			    	     <%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
						   	<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>  
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
					</tr>
					
					<tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "SiteManagementForm" property = "msaid"/>&Appendix_Id=<bean:write name = "SiteManagementForm" property = "appendixid"/>"><bean:write name = "SiteManagementForm" property = "msaname"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "SiteManagementForm" property = "appendixid"/>&Name=<bean:write name = "SiteManagementForm" property = "appendixname"/>&ref=<%=chkaddendum%>"><bean:write name = "SiteManagementForm" property = "appendixname"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
	 					<td colspan="7"><h2>Job&nbsp;<bean:message bundle = "PRM" key ="prm.new.job.site"/>:&nbsp;<bean:write name ="SiteManagementForm" property ="jobName"/></h2></td>
	 				</tr>
	      		</table>
   				</td>
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>

<logic:empty  name = "SiteManagementForm" property = "fromType">
	<table border="0" cellspacing="0" cellpadding="0" width="1252">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr>
				          <td background="images/content_head_04.jpg" height="21">&nbsp;</td>
				</tr>
				<tr>
									<td colspan = "10" ><h1> 
										<logic:equal name = "SiteManagementForm"  property ="pageType" value = "mastersites">
											<logic:equal name= "SiteManagementForm" property = "siteID" value="0">
												<bean:message bundle = "pm" key = "add.site.for.msa"/>&nbsp;<bean:write name="SiteManagementForm" property="msaname" />
											</logic:equal>
											<logic:notEqual name= "SiteManagementForm" property = "siteID" value="0">
												<bean:message bundle = "pm" key = "update.site.for.msa"/>&nbsp;<bean:write name="SiteManagementForm" property="msaname" />
											</logic:notEqual>
										</logic:equal>
										
										<logic:equal name = "SiteManagementForm"  property ="pageType" value = "webTicket">
											<logic:equal name= "SiteManagementForm" property = "siteID" value="0">
												<bean:message bundle = "pm" key = "add.site.for.msa"/>&nbsp;<bean:write name="SiteManagementForm" property="msaname" />
											</logic:equal>
										</logic:equal>
										</h1>
									</td>
								</tr>	
					<tr>
						<td colspan = "10" ><h1> 
							<logic:notPresent name ="SiteManagementForm" property ="jobid">
								<logic:notEqual name = "SiteManagementForm"  property ="pageType" value = "mastersites">
									Site Details for&nbsp;<bean:write name="SiteManagementForm" property="msaname" />
								</logic:notEqual>
							</logic:notPresent>
						</h1></td>	
					</tr> 
	</table>
</logic:empty>



<logic:equal name ="SiteManagementForm" property ="fromType" value ="yes">
		<%@ include  file="/JobMenuScript.inc" %>
</logic:equal>  

	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>  <td width="2" height="0"></td>
  			 <td>
  			 	<table border = "0" cellspacing = "2" cellpadding = "2" width = "800"> 
								
				</table>
					
				 <table border="0" cellspacing="1" cellpadding="1" >	
				 	<tr><td colspan = "6" class = "formCaption"><bean:message bundle = "pm" key = "site.general"/></td></tr>
				 	
					<tr>
					 	<td  class = "colDark"><bean:message bundle = "pm" key = "site.labelName"/></td>
					    <td  class = "colLight" colspan = "5" ><html:text name= "SiteManagementForm" property = "siteName" styleClass = "textbox" size="20" /></td>
					</tr>
					
					<tr>	    
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.siteNumber"/><font class="red"> *</font></td>
					    <td class = "colLight" colspan = "5"><html:text name = "SiteManagementForm" property = "siteNumber" styleClass = "textbox" /></td>
					</tr>
					
					
				    <logic:notEqual name= "SiteManagementForm" property = "siteID" value="0">
					    <tr>    
						    <td class = "colDark" ><bean:message bundle = "pm" key = "site.status"/></td>
							<td class ="colLight" colspan = "5">	
								<html:select  name = "SiteManagementForm" property="siteStatus" size="1" styleClass="select">
										<html:optionsCollection name = "dcSiteStatus"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>
							</td>
						</tr>
					</logic:notEqual>	

  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.address"/></td>
					    <td class = "colLight" colspan = "5" ><html:text name = "SiteManagementForm" property = "siteAddress"  styleClass = "text" size="40" maxlength="128"/></td>
					</tr>
					
					<tr>    
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.city"/><font class="red"> *</font></td>
					    <td class = "colLight" colspan = "5" ><html:text name ="SiteManagementForm" property = "siteCity" styleClass = "text" size="15"  /></td>
					</tr>

					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.state"/><font class="red"> *</font></td>
						<td class = "colLight" colspan = "5">
					    	<logic:present name="initialStateCategory">
								<html:select name ="SiteManagementForm" property = "stateSelect"  styleClass="select" onchange="ChangeCountry();">
									<logic:iterate id="initialStateCategory" name="initialStateCategory" >
										<bean:define id="statecatname" name="initialStateCategory" property="siteCountryIdName" />		   
										<%  if(!statecatname.equals("notShow")) { %>
										<optgroup class=select label="<%=statecatname%>">
										<% } %>
											<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
										<%  if(!statecatname.equals("notShow")) { %>	
										</optgroup> 
										<% } %>
									</logic:iterate>
								</html:select>
							</logic:present>	
					    </td>
					</tr>
					
					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.zipcode"/></td>
					    <td class = "colLight" colspan = "5"><html:text name = "SiteManagementForm" property = "siteZipCode" styleClass = "text" size="10"/></td>
					</tr>
					
					<tr>    
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.country"/><font class="red"> *</font></td>
						<td class = "colLight" colspan = "5" >
							<html:select  name = "SiteManagementForm" property="country" size="1" styleClass="select" onchange = "changeRegionCategory();">
								<html:optionsCollection name = "dcCountryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					    </td>
					</tr>
					
					<tr>
					   	<td class = "colDark"><bean:message bundle = "pm" key = "site.category"/></td>
						<td class = "colLight" colspan = "5">
					    	<html:select  name = "SiteManagementForm" property="siteCategory" size="1" styleClass="select">
								<html:optionsCollection name = "dcCategoryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>   
					    </td> 
					</tr>
					
					<tr>    
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.region"/></td>
						<td class = "colLight" colspan = "5">
							<html:select  name = "SiteManagementForm" property="siteRegion" size="1" styleClass="select">
								<html:optionsCollection name = "dcRegionList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>  
					    </td> 
  					</tr>
  					
					 <tr>
					    <td class = "colDark">Time Zone</td>
					    <td class = "colLight" colspan="5">
				      		 <html:select  name = "SiteManagementForm" property="siteTimeZone" size="1" styleClass="select">
								<html:optionsCollection name = "dcTimeZoneList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>   
					    </td>
  					</tr>
  					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.localityuplift"/></td>
					    <td class = "colLight" colspan="5"><html:text name ="SiteManagementForm" property = "siteLocalityFactor" styleClass = "text" size="10"/></td>
					</tr>
					
					<tr>    
						<td class = "colDark"><bean:message bundle = "pm" key = "site.union"/></td>
					    <td class = "colLight" colspan="5">
						    <html:radio name ="SiteManagementForm" property="siteUnion" value="Y">Yes</html:radio>&nbsp;&nbsp;&nbsp;
						    <html:radio name ="SiteManagementForm" property="siteUnion" value="N">No</html:radio>
					    </td>
					</tr>
					
					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.group"/></td>
					    <td class = "colLight" colspan="5" >
								<html:select  name = "SiteManagementForm" property="siteGroup" size="1" styleClass="select"
										 onkeydown="fnKeyDownHandler(this, event);" 
										 onkeyup="fnKeyUpHandler_A(this, event); return false;" 
										 onkeypress = "return fnKeyPressHandler_A(this, event);"  
										 onchange="fnChangeHandler_A(this, event);"
                                         onfocus="clearFirstElement(document.forms[0].siteGroup);" 
                                         onblur="return isValidAlphaNumeric(document.forms[0].siteGroup);" >
								<html:optionsCollection name = "dcGroupList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					    </td>
					 </tr>

					 <tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.end.customer"/><font class="red"> *</font></td>
					     <td class = "colLight" colspan="5">
							<html:select  name = "SiteManagementForm" property="siteEndCustomer" size="1" styleClass="select">
							<html:optionsCollection name = "dcEndCustomerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					    </td>
  					</tr>
  					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.worklocation"/></td>
					    <td class = "colLight" colspan="5"><html:text  name ="SiteManagementForm" property = "siteWorkLocation" styleClass = "text" size="85" maxlength="128"/></td>
  					</tr>
  					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.siteDirections"/></td>
					    <td class = "colLight" colspan="5" ><html:text  name ="SiteManagementForm" property = "siteDirections" styleClass = "text" size="85"/></td>
  					</tr>
  					
  					<tr><td  class = "formCaption" colspan = "6"><bean:message bundle = "pm" key = "site.pointOfContact"/>&nbsp;-&nbsp;<bean:message bundle = "pm" key = "site.pointOfContactPrimary"/></td></tr>
  					
  					<tr>
	  					 <td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
	  					 <td colspan = "5" class = "colLight"><html:text  name ="SiteManagementForm" property = "sitePrimaryName" styleClass = "text" size="15"/></td>	    		
  					</tr>
  					
  					<tr>
  						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
  						<td colspan = "5" class = "colLight"><html:text  name ="SiteManagementForm" property = "sitePrimaryPhone" styleClass = "text" size="15"/></td>	    		
  					</tr>
  					
  					<tr>
  						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
  						<td colspan = "5" class = "colLight"><html:text  name ="SiteManagementForm" property = "sitePrimaryEmail" styleClass = "text" size="30"/></td>	    		
  					</tr>
  					
  					<tr><td colspan = "6" class = "formCaption"><bean:message bundle = "pm" key = "site.pointOfContact"/>&nbsp;-&nbsp;<bean:message bundle = "pm" key = "site.pointOfContactSecondary"/></td></tr>   
						
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
						<td colspan = "5" class = "colLight"><html:text  name ="SiteManagementForm" property = "siteSecondaryName" styleClass = "text" size="15"/></td>	    		
					</tr>
					
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
						<td colspan = "5" class = "colLight"><html:text  name ="SiteManagementForm" property = "siteSecondayPhone" styleClass = "text" size="15"/></td>	    		
					</tr>
					  
					<tr>
						<td  class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
					    <td colspan = "5" class = "colLight"><html:text  name ="SiteManagementForm" property = "siteSecondaryEmail" styleClass = "text" size="30"/></td>	    		
					</tr>  
					
					
					
					
					
					
					
					
					
					</tr><td colspan = "6" class = "formCaption"><bean:message bundle = "pm" key = "site.pointOfContact"/>&nbsp;-&nbsp;<bean:message bundle = "pm" key = "site.clientHD"/></td>
						 
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
					 	<td  class = "colLight" colspan="5" ><html:text property = "clientHD_Name" styleClass = "text" size="15" /></td>	
					</tr>
					
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
					 	<td class = "colLight" colspan="5" ><html:text property = "clientHD_phone" styleClass = "text" size="20" /></td>	    		
					</tr>
					
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
					 	 <td  class = "colLight" colspan="5" ><html:text property = "clientHD_email" styleClass = "text" size="30" /></td>	    		
					</tr>
					
					
					
					
					
					
					
					
					
					
					
					
					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.specialConditions"/></td>
					    <td class = "colLight" colspan = "5"><html:textarea  name ="SiteManagementForm" property = "siteSpecialConditions" styleClass = "textarea" cols="70" rows="5" /></td>
  					</tr>
  					
  					<tr>
			  			<td colspan = "6" class = "colLight" align ="center">
			  				<logic:equal name ="SiteManagementForm" property ="jobid" value="">
			  					<html:button property="back" styleClass="button_c" onclick = "return BackAction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			  				</logic:equal>
			  				
					  		 <logic:notEqual name ="SiteManagementForm" property ="jobid" value ="">
				  			 	<html:button property="newadd" styleClass="button_c" onclick ="addnewsite();">Add</html:button>&nbsp;	
				  			 		
				  			 	<logic:equal name="SiteManagementForm" property="pageType" value="webTicket">
				  			 		<html:button property="back" styleClass="button_c" onclick = "return BackActionWebTicketPage();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  			 	</logic:equal>
				  			 	
				  			 	<logic:notEqual name="SiteManagementForm" property="pageType" value="webTicket">
					  			 	<html:button property="back" styleClass="button_c" onclick = "return BackActionSearchPage();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  			 	</logic:notEqual>
				  			 	
				  			 </logic:notEqual>
			  			 
				  			<logic:equal name ="SiteManagementForm" property ="pageType" value="jobsites">
				  			</logic:equal>
				  			
				  			<logic:equal name ="SiteManagementForm" property ="pageType" value="mastersites">
					  			<html:submit property="save" styleClass = "button_c" onclick ="return validate();">Save</html:submit>
					  			
	 				  				<logic:notEqual name= "SiteManagementForm" property = "siteID" value="0">
					  			 		<html:button property="back" styleClass="button_c" onclick="return del();">Delete</html:button>
				  			 		</logic:notEqual>
				  			 		
			  			 	</logic:equal>	
			  			 	
  						</td>
  					</tr>
  					
  					<jsp:include page = '/Footer.jsp'>
		      			<jsp:param name = 'colspan' value = '37'/>
		      			<jsp:param name = 'helpid' value = 'sitepage'/>
		 			</jsp:include>	
				</table>
			</td>
		</tr>
</table>
</td>
</tr>
</table>
</html:form>
<script>
function validate()
	{
		if( document.forms[0].siteNumber.value == "" )
		{
			alert("Please enter Site Number.");
			document.forms[0].siteNumber.focus();
			return false;
		}
		if( document.forms[0].siteAddress.value == "" )
		{
			alert("Please enter Site Address.");
			document.forms[0].siteAddress.focus();
			return false;
		}
		if( document.forms[0].siteCity.value == "" )
		{
			alert("Please enter Site City.");
			document.forms[0].siteCity.focus();
			return false;
		}
		
		if( document.forms[0].stateSelect.value == "0" )
		{
			alert( "Please select Site State." );
			document.forms[0].stateSelect.focus();
			return false;
		}
		
		if( document.forms[0].country.value == " " )
		{
			alert( "Please select Site Country." );
			document.forms[0].country.focus();
			return false;
		}
		
	 	if( document.forms[0].siteEndCustomer.value == " " )
		{
			alert( "Please select Site End Customer Name." );
			document.forms[0].siteEndCustomer.focus();
			return false;
		}
	 	
	 	if((document.forms[0].siteZipCode.value =="") && document.forms[0].country.value == 'US')
		{
			alert('Please Enter Zip Code');
			document.forms[0].siteZipCode.focus();	
			return false;
		}
		
  		if(!isEmail(document.forms[0].sitePrimaryEmail.value)&&(!isBlank(document.forms[0].sitePrimaryEmail.value)))
		{
			alert("<bean:message bundle="PRM" key="prm.netmedx.emailvalidation"/>");		
			document.forms[0].sitePrimaryEmail.focus();
			return false;
		}

		if(!isEmail(document.forms[0].siteSecondaryEmail.value)&&(!isBlank(document.forms[0].siteSecondaryEmail.value)))
		{
			alert("<bean:message bundle="PRM" key="prm.netmedx.emailvalidation"/>");		
			document.forms[0].siteSecondaryEmail.focus();
			return false;
		}	
	 	
		document.forms[0].action = "SiteManage.do?method=Save&countRow=<bean:write name="SiteManagementForm" property = "countRow" />&siteID=<bean:write name ="SiteManagementForm" property  = "siteID"/>&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "SiteManagementForm" property = "current_page_no"/>&org_page_no=<bean:write name = "SiteManagementForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "SiteManagementForm" property = "total_page_size"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "siteSearchEndCustomer"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>";
		return true;
		
	}
	
function BackAction()
	{
// 		document.forms[0].action = "SiteManagement.do?&countRow=<bean:write name="SiteManagementForm" property = "countRow" />&siteID=<bean:write name = "SiteManagementForm" property = "siteID"/>&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "SiteManagementForm" property = "current_page_no"/>&org_page_no=<bean:write name = "SiteManagementForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "SiteManagementForm" property = "total_page_size"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "siteSearchEndCustomer"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>";
// 		document.forms[0].submit();
// 		return true;
		history.go(-1);
	}	

function BackActionSearchPage()
	{
		document.forms[0].action = "SiteSearch.do?ref=search";
		document.forms[0].submit();
		return true;
	}	
	
function BackActionWebTicketPage()
	{
		document.forms[0].action = "NewWebTicketAction.do";
		document.forms[0].submit();
		return true;
	}	
	
function del() 
{	
		var ans = confirm( "Are you sure you want to delete the Site?" );	
		if( ans )
		{
			document.forms[0].action = "SiteManage.do?method=Delete&countRow=<bean:write name="SiteManagementForm" property = "countRow" />&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "SiteManagementForm" property = "current_page_no"/>&org_page_no=<bean:write name = "SiteManagementForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "SiteManagementForm" property = "total_page_size"/>&row_size=<bean:write name = "SiteManagementForm" property = "row_size"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "siteSearchEndCustomer"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>";
			document.forms[0].submit();
			return true;	
		}
}

function changeRegionCategory()
	{
	document.forms[0].refersh.value="true";
	document.forms[0].action = "SiteManage.do?method=Modify&siteID=<bean:write name = "SiteManagementForm" property = "siteID"/>&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "SiteManagementForm" property = "current_page_no"/>&org_page_no=<bean:write name = "SiteManagementForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "SiteManagementForm" property = "total_page_size"/>&ownerId=<bean:write name = "SiteManagementForm" property = "ownerId"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "siteSearchEndCustomer"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>&check=country";
	document.forms[0].submit();
	return true;
	}

function addnewsite()
	{

		if( document.forms[0].siteNumber.value == "" )
		{
			alert("Please enter Site Number.");
			document.forms[0].siteNumber.focus();
			return false;
		}
		if( document.forms[0].siteCity.value == "" )
		{
			alert("Please enter Site City.");
			document.forms[0].siteCity.focus();
			return false;
		}
		
		if( document.forms[0].stateSelect.value == "0" )
		{
			alert( "Please select Site State." );
			document.forms[0].stateSelect.focus();
			return false;
		}
		
		if( document.forms[0].country.value == " " )
		{
			alert( "Please select Site Country." );
			document.forms[0].country.focus();
			return false;
		}
		
	 	if( document.forms[0].siteEndCustomer.value == " " )
		{
			alert( "Please select Site End Customer Name." );
			document.forms[0].siteEndCustomer.focus();
			return false;
		}
	 	
	 	if((document.forms[0].siteZipCode.value =="") && document.forms[0].country.value == 'US')
		{
			alert('Please Enter Zip Code');
			document.forms[0].siteZipCode.focus();	
			return false;
		}
		
		if(!isEmail(document.forms[0].sitePrimaryEmail.value)&&(!isBlank(document.forms[0].sitePrimaryEmail.value)))
		{
			alert("<bean:message bundle="PRM" key="prm.netmedx.emailvalidation"/>");		
			document.forms[0].sitePrimaryEmail.focus();
			return false;
		}

		if(!isEmail(document.forms[0].siteSecondaryEmail.value)&&(!isBlank(document.forms[0].siteSecondaryEmail.value)))
		{
			alert("<bean:message bundle="PRM" key="prm.netmedx.emailvalidation"/>");		
			document.forms[0].siteSecondaryEmail.focus();
			return false;
		}	
	
		//document.forms[0].target = "ilexmain";
		
		if(document.forms[0].fromType.value == 'ticketDetail') {
			document.forms[0].action = "SiteManage.do?method=newSite&isTicket=y";
			
		} else {
			document.forms[0].target = "ilexmain";
			document.forms[0].action = "SiteManage.do?method=newSite";
		}
		document.forms[0].submit();
		var newwindow = '';
		if(document.forms[0].fromType.value != 'ticketDetail') {
			if (!newwindow.opener) {
				newwindow.opener = self;
				window.close();
			}	
		} 

	return true;
	}		
</script>
<script>
function ShowDiv()
{
if(document.forms[0].fromType.value =='yes')
{
	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";
}
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
if(document.forms[0].fromType.value =='yes')
{
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;

}
}
</script>

</body>
</html:html>
