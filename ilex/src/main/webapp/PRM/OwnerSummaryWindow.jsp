<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<html:html>
<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>

<% 
	int summarysize = ( int ) Integer.parseInt( request.getAttribute( "summarysize" ).toString() ); 
	String readonlynumberclass = null;
	String backgroundclass = null;
	boolean csschooser = true;
	String prevmsaid = "";
	String prevownerid = "";
%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


%>
<%@ include  file = "/NMenu.inc" %>
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0"  onLoad = "MM_preloadImages( 'images/sales1b.gif' , 'images/services1b.gif' , 'images/about1b.gif' , 'images/cust-serv1b.gif' , 'images/offers1b.gif' );leftAdjLayers();" >
<html:form action = "OwnerSummaryWindowAction" >
<html:hidden property="jobOwnerOtherCheck"/>
<html:hidden property="ownerId"/>
<bean:define id = "mcombo" name = "msacombo" scope = "request" />

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<%if(nettype.equals("dispatch")){ %>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px">Reports</a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=-1&ownerId=<bean:write name="ReportScheduleWindowForm" property="ownerId"/>&nettype=<%=nettype %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=640 colspan="2">&nbsp;</td>
					<%} %>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr>
		        	<td colspan = "7" height="1"><h2><bean:message bundle = "PRM" key = "prm.reportschedule.ownerreportschedule"/></h2></td>
		        </tr> 
	      </table>
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        <tr>
	          <td width="155" height="39"  class="headerrow" colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" border="0">
		              <tr>
		                <td width="150">
		                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
			                   	<tr>
						            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
			                    </tr>
		                  	</table>
		                </td>
		              </tr>
		          </table>
	          </td>
	        </tr>
	        <tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>	
	                			<span>
									<div id="filter" class="divstyle">
					        			<table width="250" cellpadding="0" class="divtable">
						                     <tr>
					                            <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
																		 <html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="ReportScheduleWindowForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="ReportScheduleWindowForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="ReportScheduleWindowForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table></td>
		                      			    	<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
			                      			    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												        <span id = "OwnerSpanId"> 
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
								
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
																 		</html:multibox>
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>											
						            </div>
				                </span>
							 </td>
	              		</tr>
            		</table>
            	</td>
        	</tr>
	    </table>
	</td>
 </tr>
</table>
<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
<table>

<table  border = "0" cellspacing = "1" cellpadding = "1" >
	<tr>
	  <td  width = "2" height = "0"></td>
	  <td>
	  	<table border = "0" cellspacing = "1" cellpadding = "1" width = "400">
	  		<tr> 
			     <td  class = "labelobold">Owner</td>
			     <td class = "labelo" colspan = "10">
					<html:select property = "owner"  styleClass = "comboo">
						<html:optionsCollection name = "mcombo"  property = "allOwnerName"  label = "label"  value = "value" />
					</html:select> 
				</td>
			</tr>
			<tr>	
			    <td  class = "labelebold">Account</td>
			    <td class = "labele" colspan = "10">
					<html:select property = "msa"  styleClass = "comboo">
						<html:optionsCollection name = "mcombo"  property = "msalist"  label = "label"  value = "value" />
					</html:select>&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			 </tr>

	  		 <tr>
	   			 <td class = "labelobold"><bean:message bundle = "PRM" key = "prm.reportschedule.startdate"/></td>
	   			 <td class = "labelo" colspan = "10">
	   			 	<html:text  styleClass = "textbox" size = "10" property = "startdate" readonly = "true"/>
	   			 	<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].startdate , document.forms[0].startdate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
	   			 </td>
	   		</tr>
	 		<tr>
  			  	 <td  class = "labelebold">
  			  	 	<bean:message bundle = "PRM" key = "prm.reportschedule.enddate"/>
  			  	 </td>	
	  		  	 <td class = "labele" colspan = "10">
			     	<html:text  styleClass = "textbox" size = "10" property = "enddate" readonly = "true"/>
				 	<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].enddate, document.forms[0].enddate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
				 </td>
			</tr>
			<tr>
				<td class="buttonrow" colspan="11">
					<html:submit property = "save" styleClass = "button" onclick = "return validate();">
						View Report
					</html:submit> 
					<html:button property = "back" styleClass = "button" onclick = "javascript:history.go(-1);">Back</html:button>
				</td>
			</tr>
			</table>
			<table border = "0" cellspacing = "1" cellpadding = "1" width = "800">	
			<logic:present name = "accountlist" scope = "request">
				<% if( ( request.getAttribute( "list" ) != null ) && (  summarysize == 0 ) )  {	%>
					<tr>
				    	<td colspan = "5" class = "message" height = "30">No Records Available</td>
				    </tr>
				<% } else { %>
				<tr>  
					<td class = "tryb" rowspan = "2" width = "7%">
						<bean:message bundle = "PRM" key = "prm.accountsummary.owner"/>
					</td>
					
					<td class = "tryb" rowspan = "2" width = "7%">
						<bean:message bundle = "PRM" key = "prm.accountsummary.msa"/>
					</td>
					
					<td class = "tryb" rowspan = "2" width = "7%">
						<bean:message bundle = "PRM" key = "prm.accountsummary.appendix"/>
					</td>
					
					<td class = "tryb" colspan = "2" width = "20%">
						<bean:message bundle = "PRM" key = "prm.accountsummary.inwork"/>
					</td>
					
					<td class = "tryb" colspan = "2" width = "20%">
						<bean:message bundle = "PRM" key = "prm.accountsummary.complete"/>
					</td>
					
					<td class = "tryb" colspan = "2" width = "20%">
						<bean:message bundle = "PRM" key = "prm.accountsummary.closed"/>
					</td>
					
					<td class = "tryb" colspan = "2" width = "20%">
						<bean:message bundle = "PRM" key = "prm.accountsummary.total"/>
					</td>
				</tr>  
				
				<tr> 
    				<td class = "tryb" width = "10%"> 
      					<bean:message bundle = "PRM" key = "prm.accountsummary.inworkrevenue"/>
    				</td>
    				
    				<td class = "tryb" width = "10%"> 
      					<bean:message bundle = "PRM" key = "prm.accountsummary.inworkvgp"/>
    				</td>
    
    				<td class = "tryb" width = "10%"> 
      					<bean:message bundle = "PRM" key = "prm.accountsummary.completerevenue"/>
    				</td>
    
    				<td class = "tryb" width = "10%"> 
      					<bean:message bundle = "PRM" key = "prm.accountsummary.completevgp"/>
    				</td>
    				
    				<td class = "tryb" width = "10%"> 
      					<bean:message bundle = "PRM" key = "prm.accountsummary.closedrevenue"/>
    				</td>
    				
    				<td class = "tryb" width = "10%"> 
      					<bean:message bundle = "PRM" key = "prm.accountsummary.closedvgp"/>
    				</td>
    
    				<td class = "tryb" width = "10%"> 
      					<bean:message bundle = "PRM" key = "prm.accountsummary.totalrevenue"/>
    				</td>
    				
    				<td class = "tryb" width = "10%"> 
      					<bean:message bundle = "PRM" key = "prm.accountsummary.totalvgp"/>
    				</td>
    			</tr>
    			
    			
    			<logic:iterate id = "acclist" name = "accountlist">
    			<bean:define id = "msa_id" name = "acclist" property = "msaid" type = "java.lang.String"/>
				 <bean:define id = "owner_id" name = "acclist" property = "ownerid" type = "java.lang.String"/>
			
    				<% 
						if ( csschooser == true ) 
						{
							backgroundclass = "labelonowrap";
							readonlynumberclass = "readonlytextnumberodd";
							csschooser = false;
						}
				
						else
						{
							csschooser = true;	
							readonlynumberclass = "readonlytextnumbereven";
							backgroundclass = "labelenowrap";
						}
    				%>
    				<tr height = "20">
	    				<td class = "<%= backgroundclass %>"> 
	    					<% 
								if( !prevownerid.equals( owner_id ) )
								{
							%>
	    						<bean:write name = "acclist" property = "ownername" />
	    					<% 
								}
	    					%>
	    				</td>
	    				<td class = "<%= backgroundclass %>"> 
	    					<% 
								if( !prevmsaid.equals( msa_id ) )
								{
							%>
								<bean:write name = "acclist" property = "msaname" />
							<%
								} 
							%>
	    				</td>
	    				
	    				<td class = "<%= backgroundclass %>"> 
	    					<bean:write name = "acclist" property = "appendixname" />
	    				</td>
	    				
	    				
	    				<td class = "<%= readonlynumberclass %>"> 
	    					<bean:write name = "acclist" property = "inworkrevenue" />
	    				</td>
	    				
	    				<td class = "<%= readonlynumberclass %>"> 
	    					<bean:write name = "acclist" property = "inworkvgp" />
	    				</td>
	    				
	    				
	    				<td class = "<%= readonlynumberclass %>"> 
	    					<bean:write name = "acclist" property = "completerevenue" />
	    				</td>
	    				
	    				<td class = "<%= readonlynumberclass %>"> 
	    					<bean:write name = "acclist" property = "completevgp" />
	    				</td>
	    				
	    				<td class = "<%= readonlynumberclass %>"> 
	    					<bean:write name = "acclist" property = "closedrevenue" />
	    				</td>
	    				
	    				<td class = "<%= readonlynumberclass %>"> 
	    					<bean:write name = "acclist" property = "closedvgp" />
	    				</td>
	    				
	    				<td class = "<%= readonlynumberclass %>"> 
	    					<bean:write name = "acclist" property = "incompclorevenue" />
	    				</td>
	    				
	    				<td class = "<%= readonlynumberclass %>"> 
	    					<bean:write name = "acclist" property = "incompclovgp" />
	    				</td>
	    			</tr>
	    			<% 
						prevmsaid = msa_id;
						prevownerid = owner_id;
					%> 
    			</logic:iterate>
    			<%} %>
			</logic:present>
		
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
</html:form>
</table>
</body>
<script>
function validate()
{
	if( document.forms[0].startdate.value == "" ) 
 	{
		alert( "<bean:message bundle = "PRM" key = "prm.reportschedule.selectstartdate"/>" );	 			
 		document.forms[0].startdate.focus();
 		return false;
 	}
 		
	if( document.forms[0].enddate.value == "" ) 
	{
		alert( "<bean:message bundle = "PRM" key = "prm.schedule.selectenddate"/>" );	 			
	 	document.forms[0].enddate.focus();
	 	return false;
	 }
	 return true;	
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="OwnerSummaryWindowAction.do?nettype=dispatch&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="OwnerSummaryWindowAction.do?nettype=dispatch&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = -1;
 	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>

</html:html>