<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%
String appendixname = "";
String pagetitle = "";
String successmessage = "";
String label = "";
if(request.getAttribute("appendixname")!=null) {
	appendixname = ""+request.getAttribute("appendixname");
}

if(appendixname.equalsIgnoreCase("NetMedX")) {
	pagetitle = "prm.installnotes.dispatchncomments";
	successmessage = "prm.dispatchnotes.addedsuccessfully";
	label = "prm.dispatchnotes.dispatchnotes";
}	
else { 
	pagetitle = "prm.installnotes.installationncomments";
	successmessage = "prm.installnotes.addedsuccessfully";
	label = "prm.installnotes.installnotes";
}	
%>

<html:html>
<head>
	<title></title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0">

<html:form action = "/AddInstallationNotes" target = "ilexmain">
<html:hidden property="function" />
<html:hidden property="appendixid" />
<html:hidden property="jobid" />
<html:hidden property="from" />
<html:hidden property="nettype" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />

<table  border = "0" cellspacing = "1" cellpadding = "1" align = "left" valign  = "top">
	  	  <tr>
	  <td  width="2" height="0"></td>
	  <td>
	  <table border="0" cellspacing="1" cellpadding="1" width="750">
			  <tr> 
	    		<td colspan="3" class = "labeleboldwhite" height = "30">
		    		<bean:message bundle="PRM" key="<%=pagetitle%>"/><bean:write name="AddInstallationNotesForm" property="appendixname" />
		    		<bean:message bundle="PRM" key="prm.installnotes.job"/><bean:write name="AddInstallationNotesForm" property="jobname" />
	    		</td>
	    		
	 		  </tr> 
 		  
	 		  <logic:present name="addmessage"  scope = "request">
	 		 	 <logic:equal name = "addmessage"   value = "0">
	 		 		 <tr>
		 				<td colspan="3" class="message" height="30" >
		 			   		 <bean:message bundle="PRM" key="<%=successmessage%>"/>
		 	  			</td>	 	  		
		  	 	     </tr>
		  	 	 </logic:equal>    
		 	  </logic:present>
	 		
	 		  <tr>
	 		  	<td class = "labeloboldtop" width="200"> 
	 		  		<bean:message bundle="PRM" key="<%=label%>"/>
	 		  	</td> 
	 		  	<td class = "labelo" width="400">
	 		  		<html:textarea property = "installnotes" rows = "10" cols = "75" styleClass = "textbox"/>
	 		 	</td>
	 		  	<td width="150"></td>
	 		 </tr>
 		
 		  <html:hidden property = "appendixid" />
 		  
	 		  <tr>
		 		  	<td colspan = "2" class = "buttonrow">
		 		  		<html:submit property = "save" styleClass = "button" onclick = "return validate();"> <bean:message bundle="PRM" key="prm.save"/></html:submit>
		 		  		<html:reset property = "reset" styleClass = "button"><bean:message bundle="PRM" key="prm.installnotes.Cancel"/></html:reset>
		 		  		<html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			 		<td></td>	
		 		</tr>
 		  </table>
 		 </td>
 		</tr>
</table>
			
</html:form>

 		  
 		  
</body>

<script>

function trimFields() 
{
	var field = document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='textarea') 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}


function validate()
{
	trimFields();
	
	if( document.forms[0].installnotes.value == "" )
	{
		alert( "<bean:message bundle="PRM" key="prm.installnotes.entercomments"/>");
		return false;
	}
	
	return true;
}
</script>

<script>
function Backaction()
{
	if( document.forms[0].from.value == 'appendixdashboard' )
	{
		if(document.forms[0].nettype.value == 'dispatch')
			document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
		else	
			document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getAttribute("appendixid") %>&function=view&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	}	
	else
	{
		document.forms[0].action = "JobDashboardAction.do?jobid=<%=request.getAttribute("jobid") %>&viewjobtype="+document.forms[0].viewjobtype.value;
	}
	
	document.forms[0].submit();
	return true;
}
</script>
</html:html>