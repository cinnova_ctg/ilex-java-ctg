<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.tabular.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "javaScript" src = "javascript/JLibrary.js"></script>
</head>
<%@ include  file="/NMenu.inc" %>
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/JobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>


<%
int sitesJobSize = 0;

Appendix_Id ="";
if(request.getAttribute("Appendix_Id")!=null)
	Appendix_Id= request.getAttribute("Appendix_Id").toString();

if( request.getAttribute( "sitesJobSize" ) != null )
{
	sitesJobSize =( int ) Integer.parseInt( request.getAttribute( "sitesJobSize" ).toString()); 

}
else
{
	sitesJobSize = 0;
}
	
	String backgroundclass = "texto";
	String labelClass = null;
	String valstr =null;
	
	String backnew = "";
	int i = 0;

	boolean csschooser = true;
	
%>

<body onLoad="MM_preloadImages('images/Expand.gif','images/Collapse.gif');leftAdjLayers();">
<html:form action = "/SiteHistoryAction">
<html:hidden property = "jobid" />
<html:hidden property = "siteId" />
<html:hidden property = "from" />
<html:hidden property = "appendixid" />
<html:hidden property = "viewjobtype"/>
<html:hidden property = "page"/>
<html:hidden property = "ref1"/>
<html:hidden property = "ownerid"/>
<html:hidden property = "msaid"/>
<html:hidden property = "msaname"/>
<html:hidden property = "appendixName"/>
<html:hidden property = "checkRef1"/>


<logic:equal name = "SiteHistoryForm" property = "checkRef1" value ="true">
	
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
    				<tr>
			    	     <%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
						   	<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>  
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
					</tr>
					
					<tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "SiteHistoryForm" property = "msaid"/>&Appendix_Id=<bean:write name = "SiteHistoryForm" property = "appendixid"/>"><bean:write name = "SiteHistoryForm" property = "msaname"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "SiteHistoryForm" property = "appendixid"/>&Name=<bean:write name = "SiteHistoryForm" property = "appendixName"/>&ref=<%=chkaddendum%>"><bean:write name = "SiteHistoryForm" property = "appendixName"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
	 					<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;Job&nbsp;<bean:message bundle="PRM" key="prm.sitehistory.title"/>: <bean:write name ="SiteHistoryForm" property ="jobName"/></h1></td>
	 				</tr>
	      		</table>
   				</td>
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
	
</logic:equal>
<logic:notEqual name = "SiteHistoryForm" property = "checkRef1" value ="true">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
          	<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
			<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
			<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <!-- <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										 <a ><c:out value='${requestScope.jobName }'/></a>
										  <a><span class="breadCrumb1">Site History</span>
										  </a>
										   -->
				    </td>
			</tr>
		</table>
</logic:notEqual>

<logic:equal name = "SiteHistoryForm" property = "checkRef1" value ="true">
		<%@ include  file="/JobMenuScript.inc" %>
</logic:equal>                                           
	
<!-- <table border="0" cellspacing = "0" cellpadding = "0"> --> <!-- Table 6 start -->
<!-- <tr height="20"><td>&nbsp;</td></tr> -->
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0" align = "center"><!-- Table 4 Start -->
  			
  			<%if(sitesJobSize == 0) {%>
  			<tr>
  			<td>&nbsp; </td>
				<td class = "message"><bean:message bundle = "PRM" key = "prm.sitehistory.nositeisassociated"/></td>
			</tr>	
				<%}else{ %>
  			<logic:notEqual name ="SiteHistoryForm" property ="from" value ="site">
	  			<tr>
		 			<td>&nbsp; </td>
		 			<td colspan = "10" ><h1><bean:message bundle="PRM" key="prm.sitehistory.historydetail"/></h1></td>
	 			</tr>	
 			</logic:notEqual>
 			<%}%>
</table> 			
		
<table border ="0" cellspacing = "0" cellpadding = "0">
	<tr>
		<td></td>
		<td> 
			<%if(sitesJobSize > 1000) {%>
			<table border = "0" cellpadding=0 cellspacing=0>
				<tr>
					<td valign = "top">
					 <a href="javascript:toggleMenuSection('1');"><img id="img_1" src="images/Expand.gif" border="0" alt="+"  /></a>
				</td>
				<td valign = "top" height = "100%" >
						<table  border = "0" height = "100%" class="dbsummtable" width = "700" cellpadding=0 cellspacing=0><!-- Table 3 Start -->
							<tr>
								<td  colspan="8" valign = "top" height = "20"  align="center" class = "trybtopclose" id="classchange1">	
									<bean:message bundle="PRM" key="prm.sitehistory.title"/>
								</td>
							</tr>
							<tr id = "div_1" style="display: none"> <!-- newtr -->
								<td valign = "top" height = "100%" width=380>
									<table  height = "100%" border = "0" cellspacing="1" cellpadding="1"><!-- Table2 Start -->
										<tr >
											<td  valign = "top">
												<table width = "500" cellspacing = "2" cellpadding = "2" border=0><!-- Table1 Start -->

													<tr>
	    		    										<td  valign = "top"  class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.labelName"/></td>
		    	    									    <td  valign = "top"  class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_name"/>&nbsp;&nbsp;</td>
    													    <td  valign = "top"  class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.siteNumber"/></td>
		    											    <td  valign = "top"  class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_number"/></td>
													</tr>
													
													<tr>
														    <td  valign = "top"  class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.address"/></td>
														    <td colspan = 9 class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_address"/></td>
								  					</tr>
								  					<tr>
													
														    <td class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.city"/></td>
														    <td class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_city"/></td>
														    <td class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.state"/></td>
														    <td class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_state"/></td>
													</tr>	    
												    <tr>		    
														    <td class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.zipcode"/></td>
														    <td class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_zipcode"/></td>
														    <td class = "dblabel""><bean:message bundle = "PRM" key = "prm.sitehistory.site.country"/></td>
														    <td class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_country"/></td>
													    
								  					</tr>
								  					<tr>
														    <td class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.localityuplift"/></td>
														    <td class = "dbvalue"><bean:write name="SiteHistoryForm" property="locality_uplift"/></td>
															<td class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.union"/></td>
														    <td colspan=5 class = "dbvalue"><bean:write name="SiteHistoryForm" property="union_site"/></td>
								  					</tr>
								  					<tr>
														    <td class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.worklocation"/></td>
														    <td colspan=7 class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_worklocation"/></td>
								  					</tr>
								  					<tr>
														    <td class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.siteDirections"/></td>
														    <td colspan=7 class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_direction"/></td>
								  					</tr>
								  					<tr>
														    <td colspan = 1 class = "dblabel"><bean:message bundle = "pm" key = "site.pointOfContact"/></td>
														    <td colspan = 2 class = "dblabel"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
															<td colspan = 1 class = "dblabel"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
														    <td colspan = 4 class = "dblabel"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
								  					</tr>
								  					<tr>
														    <td colspan = 1 class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.pointOfContactPrimary"/></td>
														    <td colspan = 2 class = "dbvalue"><bean:write name="SiteHistoryForm" property="primary_Name"/></td>	    		
														    <td colspan = 1 class = "dbvalue"><bean:write name="SiteHistoryForm" property="site_phone"/></td>	    		
														    <td colspan = 4 class = "dbvalue"><bean:write name="SiteHistoryForm" property="primary_email"/></td>	    		
												    </tr>
													<tr>
															 <td colspan = 1 class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.pointOfContactSecondary"/></td>
														     <td colspan = 2 class = "dbvalue"><bean:write name="SiteHistoryForm" property="secondary_Name"/></td>	    		
														     <td colspan = 1 class = "dbvalue"><bean:write name="SiteHistoryForm" property="secondary_phone"/></td>	    		
														     <td colspan = 4 class = "dbvalue"><bean:write name="SiteHistoryForm" property="secondary_email"/></td>	    		
								  					</tr>
								  					<tr>
														    <td class = "dblabel"><bean:message bundle = "PRM" key = "prm.sitehistory.site.specialConditions"/></td>
														    <td class = "dbvalue" colspan = "7"><bean:write name="SiteHistoryForm" property="site_notes"/></td>
								  					</tr>
  					
												</table><!-- Table1 End -->
											</td>
										</tr>
									</table><!-- Table2 End -->
								</td>
							</tr><!-- newtr -->
    		            </table><!-- Table 3 End -->
    			</td>
    </tr><!-- new Tr1 -->
    <tr></tr>
</table><!-- Table 4 End -->  
<%} %>

<table width=1252>	<!-- Table 7 start -->
	<%if(sitesJobSize > 0) {%>


	<tr>
	
		<td width=1250> 
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1"> <!-- Table 5 Start -->
  		<tr> 
	    		<td rowspan = "2">&nbsp;</td>
	    		
				<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.customerName"/></td>    		
	    		<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.appendixName"/></td>
	    		<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.jobName"/></td>
	    		<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.partnerName"/></td>
	    		
	    		
	    		
	    		<logic:notEqual name = "SiteHistoryForm" property="from" value="netmedx">
	    		

	    		<td class = "Ntryb" colspan = "3"><bean:message bundle = "PRM" key = "prm.sitehistory.schedule"/></td>
	    		<td class = "Ntryb" colspan = "3"><bean:message bundle = "PRM" key = "prm.sitehistory.actual"/></td>
	    		<td class = "Ntryb" colspan = "5"><bean:message bundle = "PRM" key = "prm.sitehistory.costpricesummary"/></td>
	    		</logic:notEqual>
	    		
	    		<logic:equal name = "SiteHistoryForm" property="from" value="netmedx">

	    		<td class = "Ntryb" colspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.requestReceivedTicket"/></td>
	    		<td class = "Ntryb" colspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.scheduledArrivalTicket"/></td>
	    		<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.criticality"/></td>
	    		<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.resource"/></td>
	    		<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.msp"/></td>
	    		<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.standby"/></td>
	    		<td class = "Ntryb" colspan = "3"><bean:message bundle = "PRM" key = "prm.sitehistory.hourly"/></td>
	    		</logic:equal>
	    		
				<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.owner"/></td>
				<td class = "Ntryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.sitehistory.status"/></td>
		</tr>
		<tr> 
		<logic:notEqual name = "SiteHistoryForm" property="from" value="netmedx">
		

				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.schedulestart"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.schedulestarttime"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.schedulestartcomplete"/></div></td>
				
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.actualstart"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.actualstarttime"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.actualstartend"/></div></td>
				
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.estimatedcost"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.extendedprice"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.proformavgp"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.actualcost"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.actualvgp"/></div></td>
		</logic:notEqual>
		
		<logic:equal name = "SiteHistoryForm" property="from" value="netmedx">

				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.requestReceivedDateTicket"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.requestReceivedTimeTicket"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.scheduledArrivalDate"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.schedulaedArrivalTime"/></div></td>
				
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.hourlycost"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.hourlyprice"/></div></td>
				<td  class = "Ntryb"><div><bean:message bundle="PRM" key="prm.sitehistory.hourlyvgp"/></div></td>
		</logic:equal>
        </tr>
  

	<logic:present name = "siteJobList"> 	
 	<logic:iterate id = "slist" name = "siteJobList">
	
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass = "Ntexto";
				labelClass= "Ntextoleftalignnowrap";
				csschooser = false;
				
				
			}
	
			else
			{
				csschooser = true;	
				labelClass ="Ntexteleftalignnowrap";
				backgroundclass = "Ntexte";
				
			}
		%>
		
		<tr>	
					<td >&nbsp; </td>
					
					<td class = "<%= labelClass %>"><bean:write name = "slist" property = "customerName"/></td>
				    <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "appendixName"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "jobName"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "partnerName"/></td>
	                
	                <logic:notEqual name = "SiteHistoryForm" property="from" value="netmedx">

	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "schedulestartdate"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "schedulestarttime"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "schedulecomplete"/></td>
	        
	                
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualstartdate"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualstartdatetime"/></td>
                    <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualend"/></td>
                    
                    
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "estimatedcost"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "extendedprice"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "proformavgp"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualcost"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualvgp"/></td>
	                </logic:notEqual>
	                
	                <logic:equal name = "SiteHistoryForm" property="from" value="netmedx">

	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "requestReceivedDate_Ticket"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "requestReceivedTime_Ticket"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "scheduledArrivalDate_Ticket"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "scheduleArrivalTime_Ticket"/></td>
   	                
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "criticality"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "resource"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "msp"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "standby"/></td>
   	                
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "hourlyCost"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "hourlyPrice"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "hourlyVGP"/></td>
   	                
					</logic:equal>
					
				    <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "owner"/></td>
			        <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "status"/></td>
    	</tr>
	  <% i++;  %>
	  </logic:iterate>
  	  </logic:present>
 	
		
		<tr>
					<td>&nbsp; </td>
					
				  	<td colspan=17 class = "colLight" align ="left">
				  	<html:button property = "sort" styleClass = "button_c" onclick = "javascript:return sortwindow();"><bean:message bundle = "AM" key = "am.tabular.sort"/></html:button>
				  	<logic:present name = "SiteHistoryForm" property="from" scope = "request">

					  				<logic:equal name = "SiteHistoryForm" property="page" value="appendixdashboard">
										<html:button property="back" styleClass="button_c" onclick = "return Backaction('appendixdashboard');"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>
									</logic:equal>
									
								    <logic:equal name = "SiteHistoryForm" property="page" value="siteeditpage">
										<html:button property="back" styleClass="button_c" onclick = "return Backaction('siteeditpage');"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>    
									</logic:equal >
									
									<logic:equal name = "SiteHistoryForm" property="page" value="jobdashboard">
										<html:button property="back" styleClass="button_c" onclick = "return Backaction('jobdashboard');"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>    
									</logic:equal >
									
									<logic:equal name = "SiteHistoryForm" property="page" value="netmedxdashboard">
										<html:button property="back" styleClass="button_c" onclick = "return Backaction('netmedxdashboard');"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>    
									</logic:equal >
					</logic:present>
							
					
					
				
		</tr>
		<%} else{%>
		<tr>
					<td>&nbsp; </td>
					<logic:present name = "SiteHistoryForm" property="from" scope = "request">

					  				<logic:equal name = "SiteHistoryForm" property="page" value="appendixdashboard">
										<html:button property="back" styleClass="button_c" onclick = "return Backaction('appendixdashboard');"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>
									</logic:equal>
									
								    <logic:equal name = "SiteHistoryForm" property="page" value="siteeditpage">
										<html:button property="back" styleClass="button_c" onclick = "return Backaction('siteeditpage');"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>    
									</logic:equal >
									
									<logic:equal name = "SiteHistoryForm" property="page" value="jobdashboard">
										<html:button property="back" styleClass="button_c" onclick = "return Backaction('jobdashboard');"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>    
									</logic:equal >
									
									<logic:equal name = "SiteHistoryForm" property="page" value="netmedxdashboard">
										<html:button property="back" styleClass="button_c" onclick = "return Backaction('netmedxdashboard');"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>    
									</logic:equal >
					</logic:present>
							
					
					
				
		</tr>
		<%} %>
		</table><!-- Table 5 End -->
	</td>
	</tr>
	</table>	<!-- Table 7 End -->
 	</td>
 	</tr>
 </table> <!-- Table 6 end -->
  
</html:form>

<script>
function toggleMenuSection(unique) {
	
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	
	}
	
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
		
	}
	else {
		thisImage.src = "images/Expand.gif";
		
	}
	
	if(divid=="div_1")
	{
		if(document.getElementById ( "classchange1" ).className == 'trybtop')
		{
			document.getElementById ( "classchange1" ).className = 'trybtopclose';
			
		}
		else
		{
			document.getElementById ( "classchange1" ).className = 'trybtop';
			
		}
	}
}

function toggleDiv(divName) {
	
	 var tableRow = document.getElementsByTagName('tr');
	 
	 
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
	
		if (tableRow[k].getAttributeNode('id').value==divName) {
		 
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
				else {
					tableRow[k].style.display = "none";
				}
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	
	}
}

function toggleMenuSectionView(unique) {
	
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	
	}
	action = "toggleType = toggleDivView('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
		
	}
	else {
		thisImage.src = "images/Expand.gif";
		
	}
	
	
}

function toggleDivView(divName) {
	
	 var tableRow = document.getElementsByTagName('tr');
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
	
		if (tableRow[k].getAttributeNode('id').value==divName) {
		
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
				
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	
	}
}

function sortwindow()
{
	str = "SortAction.do?Type=siteHistory&job_id=<bean:write name = "SiteHistoryForm" property = "jobid" />&siteId=<bean:write name = "SiteHistoryForm" property = "siteId" />&from=<bean:write name = "SiteHistoryForm" property = "from" />&page=<bean:write name = "SiteHistoryForm" property = "page" />&viewjobtype=<bean:write name = "SiteHistoryForm" property = "viewjobtype" />&appendix_id=<bean:write name = "SiteHistoryForm" property = "appendixid" />&ref1=<bean:write name = "SiteHistoryForm" property = "ref1" />&ownerId=<bean:write name = "SiteHistoryForm" property = "ownerid" />";
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
	return true;
}

function Backaction(page)
{
	history.go(-1);
}


</script>
<script>
function ShowDiv()
{
if(document.forms[0].checkRef1.value =='true')
{
	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";
}
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
if(document.forms[0].checkRef1.value =='true')
{

		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;

}
}

</script>


<%@ include  file="/ProjectJobMenuScript.inc" %>
</body>
</html:html>