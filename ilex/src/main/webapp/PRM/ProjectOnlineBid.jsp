<!DOCTYPE HTML>


<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<html:html>
<head>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  		 pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Summary of Online Bid</title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	
	<%@ include file="/Header.inc" %>
	<%@ include file = "/NMenu.inc" %> 
	<%@ include  file="/AjaxOwners.inc" %>
	<%@ include  file="/DashboardStatusScript.inc" %>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>
<%
	String backgroundclass = null;
	String readonlynumberclass = null;
	String centerAlign = null;
	boolean csschooser = true;
	int size = 0;
	if(request.getAttribute("bidInfoSize") != null) {
		size = (int) Integer.parseInt(request.getAttribute("bidInfoSize").toString());
	}

	int addStatusRow = -1;
	int addOwnerRow = -1;
	boolean addStatusSpace = false;
	boolean addOwnerSpace = false;
	String checkowner = null;
	String checkstatus = null;
	int rowHeight = 120;
%>
	

<body>
<html:form action ="/OnlineBid">
<html:hidden property ="appendixid"/>
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />


<logic:notEqual name = "ProjectOnlineBidForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
	<%@ include  file="/AppedixDashboardMenu.inc" %>
	<%@ include  file="/AppendixDashboardMenuScript.inc" %>
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	
	 <tr>
	    <td valign="top" width = "100%">
		      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		<div id="breadCrumb">&nbsp;</div>
				        </td>
 			        </tr>
 			         <tr>
			        	<td colspan = "4"><h2 style="padding-bottom: 4px; padding-top: 8px"><bean:message bundle = "PRM" key = "prm.project.onlinebid.title"/></h2></td>
			         </tr>
			         <tr>
			         	<td colspan="4"><h2 style="padding-bottom: 8px; padding-top: 3px"><bean:message bundle = "PRM" key = "prm.project.onlinebid.status"/>
							<logic:equal name="ProjectOnlineBidForm" property="status" value="1">Enabled</logic:equal>
							<logic:equal name="ProjectOnlineBidForm" property="status" value="0">Disabled</logic:equal>
							</h2></td>
			        </tr> 
		      </table>
	   </td>
	     
	    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
		<td valign="top" width="155">
	    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tr>
			          <td width="155" height="39"  class="headerrow" colspan="2">
				          <table width="100%" cellpadding="0" cellspacing="0" border="0">
				              <tr>
				                <td width="150">
				                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
					                   	<tr>
								            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
		  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
											<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
											<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
					                    </tr>
				                  	</table>
				                </td>
				              </tr>
				          </table>
			          </td>
			        </tr>
	        		<tr>
	          		<td height="21" background="images/content_head_04.jpg" width="140">
	          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
	              		<td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
		                			<div id="featured1">
			                			<div id="featured">
												<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
										</div>								
		                			</div>		
							<span>
									<div id="filter" class="divstyle">
		        			<table width="250" cellpadding="0" class="divtable">
		                    <tr>
			                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
			                          <tr>
	                           	 <td colspan="3" class="filtersCaption">Status View</td>
	                          </tr>
	                          <tr>	
	                          	  <td></td>
		                          	  <td class="tabNormalText">
											<logic:present name ="jobStatusList" scope="request">
										    	<logic:iterate id = "list" name = "jobStatusList"> 
										    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
										    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
	
													    	<%
												    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
															 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																	addStatusRow++;
																	addStatusSpace = true;
													    		}
													    	%>
													    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
											 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
														<bean:write name ="list" property = "statusid"/>
											 		</html:multibox>   
												 	<bean:write name ="list" property = "statusdesc"/><br> 
												 	<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present>		
											<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="ProjectOnlineBidForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="ProjectOnlineBidForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
												<tr><td class="tabNormalText"><html:radio name ="ProjectOnlineBidForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
											</table>	
										</td>
		    					</tr>
	                        </table></td>
	                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
	                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
	                          <tr>
	                            <td colspan="3" class="filtersCaption">User View</td>
	                          </tr>
	                          <tr>
						         <td class="tabNormalText">
						          <span id = "OwnerSpanId">
						   			<logic:present name ="jobOwnerList" scope="request">
								    	<logic:iterate id = "olist" name = "jobOwnerList">
								    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
								    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
									    		<% 
		
												checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
												if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
													addOwnerRow++;
													addOwnerSpace = true;
												}	
									    		if(addOwnerRow == 0) { %> 
										    		<br/>
										    		<span class="ownerSpan">
													&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
													<br/>								
												<% } %>
								    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
								    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
												<bean:write name ="olist" property = "ownerId"/>
									 		</html:multibox>   
											  <bean:write name ="olist" property = "ownerName"/>
											  <br/> 
										</logic:iterate>
										</span>
									</logic:present>
									</span>
							    </td>
	                          </tr>
	                         
	                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
	                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
	                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
	                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
	                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
	                              </table></td>
	                        </tr>
	  			</table>											
			</div>
		</span>
					</td>
		
	              </tr>
	            </table></td>
	        </tr>
	      </table>
	      </td>
	   
	 </tr>
	 </table>


</logic:notEqual>
	
<table border="0" cellspacing="0" cellpadding="1" width=100%>
		<tr>
		   <td>	
			<table border="0" cellspacing="1" cellpadding="1" > 
				<logic:present name = "retBidFlagStatus">
					<logic:equal name= "retBidFlagStatus" value = "0">
						<tr height="25">
							<td width="1" height="0">&nbsp;</td>
							<td class = "messagewithwrap"><bean:message bundle = "PRM" key = "prm.project.onlinebid.success"/></td>
						</tr>
					</logic:equal>
					
					<logic:notEqual name="retBidFlagStatus" value="0">
						<tr height="25">
							<td width="1" height="0">&nbsp;</td>
							<td class = "messagewithwrap"><bean:message bundle = "PRM" key = "prm.project.onlinebid.failure"/></td>
						</tr>
					</logic:notEqual>
				</logic:present>
				
			</table>
		 </td>	
		</tr>
</table>
 
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
<tr>
	<td width="3" height="0">&nbsp;</td>
	<td>
		<table border = "0" cellspacing = "1" cellpadding = "1" width="1100">
			<tr>
				<td class = "Ntryb" height = 20 align = "middle" rowspan = "2"><bean:message bundle = "PRM" key = "prm.project.onlinebid.activity"/></td>
				<td class = "Ntryb" height = 20 align = "middle" rowspan = "2"><bean:message bundle = "PRM" key = "prm.project.onlinebid.quantity"/></td>
				<td class = "Ntryb" height = 20 align = "middle" rowspan = "2"><bean:message bundle = "PRM" key = "prm.project.onlinebid.sow"/></td>
				<td class = "Ntryb" height = 20 align = "middle" rowspan = "2"><bean:message bundle = "PRM" key = "prm.project.onlinebid.assumptions"/></td>
				<td class = "Ntryb" height = 20 align = "middle" colspan="3"><bean:message bundle = "PRM" key = "prm.project.onlinebid.labelCost"/></td>
			</tr>
			
			<tr>
				<td class = "Ntryb"><bean:message bundle = "PRM" key = "prm.project.onlinebid.estimatedCost"/></td>
				<td class = "Ntryb"><bean:message bundle = "PRM" key = "prm.project.onlinebid.minutemanCost"/></td>
				<td class = "Ntryb"><bean:message bundle = "PRM" key = "prm.project.onlinebid.speedpayCost"/></td>
			</tr>

		<% if(size > 0) { %>			
			<logic:present name="projectOnlineBidInfo" scope = "request">
				<logic:iterate id = "bidInfo" name="projectOnlineBidInfo">
										<%	
							if ( csschooser == true ) 
							{
								backgroundclass ="Ntextowithwrap";
								readonlynumberclass = "Nnumberodd";
								csschooser = false; 
							}
					
							else
							{
								csschooser = true;	
								readonlynumberclass = "Nnumbereven";
								backgroundclass = "Ntextewithwrap";
							}
						%>
						<tr>
							<html:hidden name= "bidInfo" property="activityId"/>
							<td width = "290" class = "<%= backgroundclass %>" align = "left"  valign = "middle"><bean:write name = "bidInfo" property = "activityName" /></td>
							<td width = "25" class = "<%= readonlynumberclass %>" align = "center"  valign = "middle"><bean:write name = "bidInfo" property = "quantity" /></td>
							<td width = "450" class = "<%= backgroundclass %>" align = "left"  valign = "middle"><bean:write name = "bidInfo" property = "sow" /></td>
							<td width = "450" class = "<%= backgroundclass %>" align = "left"  valign = "middle"><bean:write name = "bidInfo" property = "assumption" /></td>
							<td class = "<%= readonlynumberclass %>" align = "left"  valign = "middle">$<bean:write name = "bidInfo" property = "estimatedCost" /></td>
							<td class = "<%= readonlynumberclass %>" align = "left"  valign = "middle">$<bean:write name = "bidInfo" property = "minutemanCost" /></td>
							<td class = "<%= readonlynumberclass %>" align = "left"  valign = "middle">$<bean:write name = "bidInfo" property = "speedpayCost" /></td>																					
						</tr>
				</logic:iterate>
				
				<tr>
					<td class="Nbuttonrow" colspan="7" height="20">&nbsp;
						<html:submit property = "enableBid" styleClass = "button_c"><bean:message bundle = "PRM" key = "prm.project.onlinebid.enableBid"/></html:submit>&nbsp;
						<html:submit property = "disableBid" styleClass = "button_c"><bean:message bundle = "PRM" key = "prm.project.onlinebid.disableBid"/></html:submit>
					</td>
				</tr>
			</logic:present>
			
		<% } else { %>
			<tr><td class="message" colspan="7">&nbsp;</td></tr>
			<tr height="20">
		  		<td class="message" colspan="7"><bean:message bundle = "PRM" key = "prm.project.onlinebid.noActicities"/></td>
		  	</tr>
		<% } %>
		
			<tr height="35">
				<td class="textwhiteFontwithWrap" colspan="7">Notes:</td>
			</tr>

			<tr height="30">
				<td class="textwhiteFontwithWrap" colspan="7"><bean:message bundle = "PRM" key = "prm.project.onlinebid.comments"/></td>
			</tr>
			
		</table>
	</td>
</tr>
</table>

</html:form>
</body>

<script>
function changeFilter()
{
	document.forms[0].action="OnlineBid.do?showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="OnlineBid.do?resetList=something";
	document.forms[0].submit();
	return true;

}
</script>

</html:html>