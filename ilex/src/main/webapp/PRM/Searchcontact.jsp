<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<% int contact_size =( int ) Integer.parseInt( request.getAttribute( "contactlistsize" ).toString() ); %>

<%
	String backgroundclass = null;
	boolean csschooser = true;
	int i = 0;
	String j = "";
	String setvalue = "";
%>


<html:html>
<body>
	<HEAD>
		<%@ include  file = "/Header.inc" %>
		<META http-equiv = "Content-Style-Type" content = "text/css">
		<LINK href = "styles/style.css" rel = "stylesheet" type = "text/css">
		<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
		<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
		<%@ include  file = "/Menu.inc" %>
	
	</head>
	<html:form action = "/SearchcontactAction" >
	<html:hidden property = "type" />	
	<html:hidden property = "appendixid" />	
  					<table border = "0" cellspacing = "1" cellpadding = "1" > 
	  					<tr>
		  					<td class = "labeleboldwhite">
		  						<bean:message bundle = "CM" key = "cm.search.searchcontacts"/>
		  					</td>
	  					</tr>
  						<tr class = "labelobold">
	  						<td><bean:message bundle = "CM" key = "cm.search.organization"/></td>
							<td  colspan = "1" >
								<bean:message bundle = "CM" key = "cm.search.division"/>
							</td>
						</tr>
		
						<tr>
							<td>
								<html:text property = "orgsearch" size = "20" styleClass = "textbox" />
							</td>
							
							<td>
								<html:text property = "divsearch" size = "20" styleClass = "textbox" />
							</td>
							
							<td>
								<html:submit property = "search" styleClass = "button">
									<bean:message bundle = "CM" key = "cm.search.search"/>
								</html:submit>
							</td>
						</tr>
					</table>

					
					
					<% if( contact_size > 0 ) { %>
					<table>
						<TR>
							<TD class = labeleboldwhite>&nbsp;</TD>
							<TD class = tryb><bean:message bundle = "CM" key = "cm.search.organization"/></TD>
							<TD class = tryb><bean:message bundle = "CM" key = "cm.search.division"/></TD>
							<TD class = tryb><bean:message bundle = "CM" key = "cm.search.city"/></TD>
							<TD class = tryb><bean:message bundle = "CM" key = "cm.search.state"/></TD>
								
  				   		</TR>
						
						<logic:iterate id = "list" name = "SearchcontactForm" property = "contactlist">
						<%	
							if ( csschooser == true ) 
							{
								backgroundclass = "readonlytexteven";
								csschooser = false;
							}
					
							else
							{
								backgroundclass = "readonlytextodd";
								csschooser = true;		
							}
							j =  "'"+i+"'";
						
							if( contact_size == 1 ) 
							{ 
								setvalue = "javascript:assignaddress( '' );";
							}
							else
							{
								setvalue = "javascript:assignaddress( '"+i+"' );";
							}
						%>
					
							<tr>
								<td class = "<%= backgroundclass %>">
									<html:radio property = "id" value = "<%= j %>" onclick = "<%= setvalue %>"/>
								</td>
								
								<td class = "<%= backgroundclass %>">
									<bean:write name = "list" property = "org_name" />
									<html:hidden name = "list" property = "org_name" />
								</td>
								
								<td class = "<%= backgroundclass %>">
									<bean:write name = "list" property = "org_division" />
									<html:hidden name = "list" property = "org_division" />
								</td>
								
								<td class = "<%= backgroundclass %>">
									<bean:write name = "list" property = "city" />
									<html:hidden name = "list" property = "city" />
								</td>
								
								<td class = "<%= backgroundclass %>">
									<bean:write name = "list" property = "state" />
									<html:hidden name = "list" property = "state" />
								</td>
							</tr>
					
					 <% i++;  %>
					</logic:iterate>
					</table>
					<%} %>
					
				
					<table>
						<tr>
							<td colspan = "2">
								<html:reset property = "reset" styleClass = "button"><bean:message bundle = "CM" key = "cm.search.cancel"/></html:reset>
								<html:button property = "close" styleClass = "button" onclick = "javascript:window.close();"><bean:message bundle = "CM" key = "cm.search.close"/></html:button>
							</td>
						</tr>
					
					</table>
		
				
	</html:form>
<body>
<script>

function assignaddress( temp )
{
	var address = '';
	
	
	if( temp == '' )
	{
		if( document.forms[0].type.value == 'P' )
			document.forms[0].org_division.value + '\n' + document.forms[0].city.value + '\n' + document.forms[0].state.value;
		else
			address = document.forms[0].org_name.value + '\n' + document.forms[0].org_division.value + '\n' + document.forms[0].city.value + '\n' + document.forms[0].state.value;
		
		window.opener.document.forms[0].shiptoaddress.value = address;
	}
	
	else
	{
		if( document.forms[0].type.value == 'P' )
			address = document.forms[0].org_division[temp].value + '\n' + document.forms[0].city[temp].value + '\n' + document.forms[0].state[temp].value;
		else
			address = document.forms[0].org_name[temp].value + '\n' + document.forms[0].org_division[temp].value + '\n' + document.forms[0].city[temp].value + '\n' + document.forms[0].state[temp].value;
		
		
		window.opener.document.forms[0].shiptoaddress.value = address;
	}
	 
	window.close();
	
}

</script>



</html:html>
