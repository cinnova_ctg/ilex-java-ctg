<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %> 
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<% 
String appendixid = "";
String msaid = "";
String appendix = "-1";
String netmedx = "-1";

if(request.getAttribute("appendix") !=null)
	appendix = request.getAttribute("appendix").toString();

if(request.getAttribute("netmedx") !=null)
	netmedx = request.getAttribute("netmedx").toString();

%>

<html:html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


	<title></title>
	<%@ include file="/Header.inc" %>
	<%@ include  file="/Menu.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<title></title>
</head>
<body>
	<html:form action="/SiteManagement">
		<div id="menunav">
		    <ul>
		  		<li>
		        <a href="#" class="drop"><span>MSA</span></a>
		  			<ul>
		  				<li>
				            <a href="#"><span>Sites</span></a>
				            <ul>
				      			<li><a href="SiteManagement.do?msaid=<%=request.getParameter("msaid")%>&appendixid=0&function=<%=request.getParameter("function")%>&viewjobtype=<%=request.getParameter("viewjobtype")%>&ownerId=<%=request.getParameter("ownerId")%>&initialCheck=true&pageType=mastersites">View</a></li>
				            	<li><a href="UploadCSV.do?msaid=<%=request.getParameter("msaid")%>&viewjobtype=<%=request.getParameter("viewjobtype")%>&ownerId=<%=request.getParameter("ownerId")%>&fromType=SiteMenu">Upload</a></li>
			      			</ul>
			        	</li>
			        	<li>
				            <a href="https://medius.contingent.com/admin/crmf_turnover_kickoff.php?customer=<%=request.getParameter("msaid")%>" target="_blank" >Project General Data</a>
				        </li>    
			            <% 
		            	if(request.getAttribute("viewRawVendex")!= null && request.getAttribute("viewRawVendex").equals("viewRawVendex")) {
		            		%>
		  					<li><a href="RawVendexViewAction.do?msaid=<%=request.getParameter("msaid")%>&appendixid=0&function=<%=request.getParameter("function")%>&viewjobtype=<%=request.getParameter("viewjobtype")%>&ownerId=<%=request.getParameter("ownerId")%>&initialCheck=true&pageType=mastersites"><span>Raw Vendex View</span></a></li>
							<%
						}
						%>
		  			</ul>
		  		</li>
		  		<li>
		    		<a href="#" class="drop"><span>Search</span></a>
		  			<ul>
		  				<%
		  				if(!appendix.equals("0")) {
		  					%>
		  					<li><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=0&ownerId=All&fromPage=msa&msaId=<%=request.getParameter("msaid")%>">Job</a></li>
							<%
						}
						
		  				if(!netmedx.equals("0")) {
		  					%>
							<li><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=0&ownerId=All&msaId=<%=request.getParameter("msaid")%>&searchType=ticket">Ticket</a></li>
							<%
						}
						%>
	  				</ul>
	  			</li>
	  		</ul>
		</div>
	</html:form>
</body>
</html:html>