<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<html:html>
	<head>
		<title></title>
		<%@ include file = "../Header.inc" %>
		<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
		<link rel = "stylesheet" href="styles/content.css" type="text/css">
		<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
		<script language="JavaScript" src="javascript/popcalendar.js"></script>
		<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
		<script language="JavaScript" src="javascript/jquery-1.11.0.min.js"></script>
</head>

<%
	boolean csschooser = true;
	String Appendix_Id = "";
	String fromProposalMg = "No";
	String prevdivcheckid = "";
	
	int activity_size = 0;
	if(request.getAttribute( "activityListSize" )!=null) {
		activity_size=( int ) Integer.parseInt( request.getAttribute( "activityListSize" ).toString() ); 
	}
	int div = 1;
	int i=0;
	int firstdivrowcount = 360;
	String isBukJobCreationView = ( String )request.getAttribute( "isBukJobCreationView" );
	
	if(request.getAttribute( "Appendix_Id" ) != null) 
	{
		Appendix_Id = request.getAttribute( "Appendix_Id" ).toString();
		request.setAttribute( "Appendix_Id" , Appendix_Id );
	}
	if(request.getAttribute( "appendixid" ) != null) 
	{
		Appendix_Id = request.getAttribute( "appendixid" ).toString();
		request.setAttribute( "appendixid" , Appendix_Id );
	}

	if(request.getAttribute( "fromProposalMg" ) != null )
	{
		fromProposalMg = (String)request.getAttribute( "fromProposalMg" );
		request.setAttribute( "fromProposalMg" , "Yes" );
	}
%>

<% 
int addStatusRow = -1;
int addOwnerRow = -1;
int sizeofifrstdiv=280;
int sizeofmessage=0;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;
int jobCount=0;
int sitenotfound=0;
int siteinput=0;
int errorJob=0;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());

if(request.getAttribute( "createdJobListSize" ) != null )
{
	jobCount= Integer.parseInt(request.getAttribute("createdJobListSize").toString());
	
} 
if(request.getAttribute( "siteNotFoundErrorListSize" ) != null )
{
	sitenotfound= Integer.parseInt(request.getAttribute("siteNotFoundErrorListSize").toString());;
}
if(request.getAttribute( "siteInputErrorListSize" ) != null )
{
	siteinput= Integer.parseInt(request.getAttribute("siteInputErrorListSize").toString());
}
if(request.getAttribute( "notCreatedJobListSize" ) != null )
{
	errorJob= Integer.parseInt(request.getAttribute("notCreatedJobListSize").toString());;
}

%>
	<logic:present name = "addendumjobnamelist" scope = "request">
		<% 
			firstdivrowcount = ( int ) Integer.parseInt( request.getAttribute( "firstdivrownum" ).toString() ) * 20 + 375;
			//System.out.println("firstdivrownum"+request.getAttribute( "firstdivrownum" ));
		%>
	</logic:present>
		<logic:present name = "Sizeoffirstdiv" scope = "request">
		<% 
			sizeofmessage=( int ) Integer.parseInt( request.getAttribute( "Sizeoffirstdiv" ).toString() ) * 20;
			sizeofifrstdiv = sizeofmessage+ 250;
			//System.out.println("sizeofifrstdiv---"+sizeofifrstdiv +"---"+ request.getAttribute( "Sizeoffirstdiv" ).toString());
		%>
	</logic:present>
	<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "leftAdjLayers();return setoverheadcheck();" > 

<html:form action = "/LargeDeplBulkJobCreateAction" target = "ilexmain">

<html:hidden property = "viewjobtype" /> 
<html:hidden property = "ownerId" /> 
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property = "addendum_id" />
<html:hidden property = "appendix_Id" />
<%@ include  file="/AppendixDashboardMenuScript.inc" %>
 <%@ include  file="/AppedixDashboardMenu.inc" %> 		
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        	<tr>
			        	<td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		  	<span id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
											 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixid }'/>"><c:out value='${requestScope.appendixName }'/></a>
											 <a ><span class="breadCrumb1">Large Deployment Bulk Job/PO Create</span></a>
									</span>		
				        </td>
				    </tr>
			     <tr>
			        <td colspan="4">
			        	<!--  <h2>Large Deployment Bulk Job Creation for&nbsp;<bean:message bundle = "PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name = "BulkJobCreationForm" property = "appendixname" /></h2>-->
			        	<h2>Define Job, Site and Activity</h2>
			        </td>
				</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="BulkJobCreationForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="BulkJobCreationForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="BulkJobCreationForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					           <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
<!-- Following is Outer Table -->
<table border=0 cellspacing=0 cellpadding=0>
	<tr><td height="10"></td>
	</tr>
	<tr>
		<td height = "0" style="padding-left:8px"></td>
		
			<td>
				<table>
					<logic:present name="CreateJob" scope="request">
						<%if (jobCount>0 || siteinput>0 || sitenotfound>0 || errorJob>0 )  {%>
							<tr>
									<td class="message">Creation Summary:
									</td>
							</tr>
							<tr>
									<td class="textwhiteFont">Jobs Created: <%=jobCount %>
									</td>
							</tr>
						<% } %>
					</logic:present>
					<logic:present name="createdJobList" scope="request">
						<logic:notPresent name="error" scope="request">
							<tr>
								<td class="textwhiteFont">Error Summary: No errors to report.
								</td>
							</tr>
							<tr>
								<td height="5">
								</td>
							</tr>
						</logic:notPresent>
					</logic:present>
			</table>
				</td>
	
	</tr>
<logic:present name="error" scope="request">
		<tr>
			<td height = "0" style="padding-left:8px"></td>
			<td>
				<table>
					<tr>
						<td class="textwhiteFont">Error Summary:
						</td>
					</tr>
				</table>
			</td>
		</tr>
</logic:present >
<logic:present name="error" scope="request">
	<tr>
		<td height = "0" style="padding-left:12px"></td>
			<td>
				<table>
					<tr>
						<td class="textwhiteFont" style="padding-left:12px">Site/Schedule Input Errors:&nbsp;<%=siteinput %></td>
					</tr>
					<tr>
						<td class="textwhiteFont" style="padding-left:12px">Make sure your dates are formatted as M/d/Y (Example: 02/12/2014)</td>
					</tr>
					<logic:present name="siteInputErrorList" scope="request">
						<logic:iterate id="siteInputErrorList" name="siteInputErrorList" scope="request">
								<tr>
									<td class = "textwhitetop" style="padding-left: 15px;" >
												<bean:write name = "siteInputErrorList" property = "errorrSiteNumber"/>&nbsp;
									</td>
									<td class = "textwhitetop"  >
												<bean:write name = "siteInputErrorList" property = "errorPlannedStartDate"/>&nbsp;
									</td>
								</tr>
						</logic:iterate>
					</logic:present>
					</table>
				</td>
	</tr>
	<tr>
		<td height = "0" style="padding-left:10px"></td>
		<td>
			<table>
				<tr>
					<td class="textwhiteFont" style="padding-left:12px">Site Not Found:&nbsp;<%= sitenotfound%>
					</td>
				</tr>
			<logic:present name="siteNotFoundErrorList" scope="request">
				<logic:iterate id="siteNotFoundErrorList" name="siteNotFoundErrorList" scope="request">
						<tr>
							<td class = "textwhitetop" style="padding-left: 15px;" >
										<bean:write name = "siteNotFoundErrorList" property = "errorrSiteNumber"/>&nbsp;
							</td>
							<td class = "textwhitetop"  >
										<bean:write name = "siteNotFoundErrorList" property = "errorPlannedStartDate"/>&nbsp;
							</td>
						</tr>
				</logic:iterate>
			</logic:present>
			</table>
			</td>
	</tr>
	<tr>
		<td height = "0" style="padding-left:10px"></td>
			<td>
				<table>
					<tr>
							<td class="textwhiteFont" style="padding-left:12px">Unable to create Job:&nbsp;<%=errorJob %>
							</td>
					</tr>
					<logic:present name="notCreatedJobList" scope="request">
						<logic:iterate id="notCreatedJobList" name="notCreatedJobList" scope="request">
								<tr>
									<td class = "textwhitetop" style="padding-left: 15px;" >
												<bean:write name = "notCreatedJobList"/>
									</td>
								</tr>
						</logic:iterate>
					</logic:present>
				</table>
			</td>
	</tr>
	<tr>
		<td height="5">
		</td>
	</tr>
</logic:present>
	<tr>
		<td height = "0" style="padding-left:8px"></td>
		<td>
			<table border = "0" cellspacing = "1" cellpadding = "1" >
				<tr>
					<td class = "textwhitetop" colspan = "2">
					<b>Define Job Name
					</td>
				</tr>
				<tr><td height="4"></td>
				</tr>
				<tr>
						<td class = "textwhitetop">Job Name Base:&nbsp;&nbsp;&nbsp;<html:text property = "job_NewName" styleClass = "textbox" size = "20"/>&nbsp;&nbsp;&nbsp;&nbsp;(This name will be repeated in all jobs. See examples below)</td>
						<td></td>
				</tr>
				<tr><td height="2"></td>
				</tr>
				<tr>
					<td class = "textwhitetop">Append Site Number As:&nbsp;&nbsp;<html:radio property = "prifix" value = "S">Suffix&nbsp;&nbsp;&nbsp;&nbsp;(Job Name Example: Job Name Site Number)</html:radio></td>
					<td class = "textwhitetop"></td>
				</tr>
				<tr>
					<td class = "textwhitetop" align="right" style="padding-left: 145px">
					<html:radio property = "prifix" value="P">Prefix&nbsp;&nbsp;&nbsp;&nbsp;(Job Name Example: Site Number Job Name)</html:radio></td>
					<td class = "textwhitetop"></td>
				</tr>
				<tr>
					<td class = "textwhitetop">Customer Reference:&nbsp;&nbsp;<html:text property="custRef" styleClass = "textbox" size = "20"/></td>
					<td class = "textwhitetop"></td>
				</tr>
			</table>	
		</td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
	<tr><td height="10"></td>
	</tr>
	<tr>
		<td  height = "0" style="padding-left:8px"></td>
		<td>
			<table border = "0" cellspacing = "1" cellpadding = "1">
				<tr>
					<td class = "textwhitetop" colspan = "8">
					<b>Select Activities
					</td>
				</tr>
				<tr><td height=10></td></tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan = "7" class = "textwhite"> 
						<logic:present name = "addendumjobnamelist" scope = "request">
							<logic:iterate id = "list" name = "addendumjobnamelist" scope = "request">
									<% i++; %>
									<a href = "#"  onclick = "viewChange2( 'div_<%= i %>' );" class = "tabtext">
										<bean:write name = "list" property = "addendumjobname"/>
									</a>
							</logic:iterate>
						</logic:present>
					</td>  
				</tr>
				<tr>
					<td class="textwhite" height="10">&nbsp;</td>
				</tr>
				<tr>
					<td width = "2" height = "0">
					</td>
					<td>			
		          		<!--   Started For Iterate Tag -->
		        
							<%
								  String bgcolor = "";
								  String bgcolor1 = "";
								  String readonlynumberclass = "";
								  String label = "";
							%>          
							<logic:iterate id = "activ" name = "activityList" scope = "request">		
							<% 
								if ( csschooser == true ) 
								{
									bgcolor = "hypereven";
									bgcolor1 = "hypereven";
									readonlynumberclass = "readonlytextnumbereven";
									label = "labele";
									csschooser = false;
								} 
								else
								{
									bgcolor = "hyperodd";
									bgcolor1 = "hyperodd";
									readonlynumberclass = "readonlytextnumberodd";
									csschooser = true;
									label = "labelo";
								}
	
							%>
							<bean:define id = "divcheckid" name = "activ" property = "tempjob_id" type = "java.lang.String"/>
							<% 
								if( !prevdivcheckid.equals( divcheckid ) )
								{	
									if( prevdivcheckid != "" )
									{
							%>	
									</table></td></tr>
							<%
									} 
									if (div == 1)
									{
							%>
									<div id = "div_<%= div %>" style = "display: block;" class="viewdiv">
							<%
									}
									else
									{ 
							%>
									<tr><td width = "2" height = "0"></td><td><div id = "div_<%= div %>" style = "display: none;" class="viewdiv">
							<%
									} 
							%>
							<table width = "100%" border="0" cellpadding="1" cellspacing="1">	
									<tr>
										<td>&nbsp;</td>
										<td class = "textwhitetop" colspan = "12">
											<b><bean:write name = "activ" property = "tempjob_name"/>&nbsp;Activities
										</td>
									</tr>
									
									<tr height = "20">
										<td>&nbsp;</td>
										<td class = "tryb" align = "center"> 
											<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Activity"/>
										</td>
					          			
					          			<td class = "tryb">
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Type"/>
					          			</td>
					          			
					          			<td class = "tryb">
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Qty"/>
					          			</td>
					          			
					          			<td class = "tryb" align = "center">
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Estimated"/>
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.totalCost"/>
					          			</td>
					          			
					          			<td class = "tryb">
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Extended"/>
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.SubTotal"/>
					          			</td>
					          			
					          			<td class = "tryb">
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Extended"/>
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Price"/>
					          			</td>
					          			
					          			<td class = "tryb" align = "center">
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.ProForma"/>
					          				<bean:message bundle = "PRM" key = "prm.BulkJobCreation.Margin"/>
					          			</td>
									</tr>
							<%  
								div++; } 
							%>
						
							<tr height = "20">
								<td class ="smallSelect">
								    <html:multibox property = "activity_id">
											<bean:write name = "activ" property = "activity_id"/>
								    </html:multibox>
								</td> 
								
								<td class = "<%= bgcolor1 %>">
									<bean:write name = "activ" property = "activity"/>
								</td>	
								
								<td class = "<%= bgcolor1 %>">
									<bean:write name = "activ" property = "type"/>
								</td>
								
								<td class = "<%= readonlynumberclass %>">
									<bean:write name = "activ" property = "qty"/>
								</td>				  
								
								<td class = "<%= readonlynumberclass %>">
									<bean:write name = "activ" property = "estimated_total_cost"/>
								</td>	
								
								<td class = "<%= readonlynumberclass %>">
									<bean:write name = "activ" property = "extended_sub_total"/>
								</td>
								
								<td class = "<%= readonlynumberclass %>">
									<bean:write name = "activ" property = "extended_Price"/>
								</td>					
								
								<td class = "<%= readonlynumberclass %>">
									<bean:write name = "activ" property = "proforma_Margin"/>
								</td>									
				 		 				  
				 			</tr>
						 		<html:hidden name = "activ" property = "type" />
						 		<html:hidden name = "activ" property = "libraryId" />
						 		<html:hidden property = "fromProposalMg" value = "<%= fromProposalMg %>"/>
					 		<% 
								prevdivcheckid = divcheckid; 
							%>  
					 		</logic:iterate>
			     			</table></div>
			 		 
					</td>
				</tr>
			</table>
<div id = "div_sitesearch" style = "visibility:visible;POSITION:absolute;top:<%= firstdivrowcount+sizeofmessage %>;left:2;">
	<table border=0 cellspacing=0 cellpadding=0>
		<tr><td height="10"></td>
		</tr>
		<tr>
			<td height = "0" style="padding-left:8px"></td>
			<td>
				<table border = "0" cellspacing = "0" cellpadding = "0" >
					<tr>
						<td class = "textwhitetop" colspan = "2" >
						<b>Select Job Owner and Scheduler
						</td>
					</tr>
					<tr><td height="10"></td>
					</tr>
					<tr>
						<td colspan = "2" >
							<table border = "0" cellspacing = "1" cellpadding = "1">
								<tr>
									<td class = "textwhitetop" >Job Owner:&nbsp;&nbsp;
									</td>
									<logic:present name ="dyComboPRMOwner" scope="request">
									<td class="textwhite"><html:select property = "newOwner" size = "1" styleClass="combowhite">
										<html:optionsCollection name = "dyComboPRMOwner" property = "allOwnerName" value = "value" label = "label"/>    
									</html:select></td>
									</logic:present>
									<logic:notPresent name = "dyComboPRMOwner" scope="request" >
										<td class="textwhiteFont">A team member in this role is not defined for the project.
										</td>
										<html:hidden property = "newOwner" />
									</logic:notPresent>
								</tr>
								<tr><td height="1"></td></tr>
								<tr>
									<td class = "textwhite" >Scheduler:&nbsp;&nbsp;
									</td>
									<logic:present name = "dyComboPRMScheduler" scope="request">
									<td class="textwhite"><html:select property = "newScheduler" size = "1" styleClass="combowhite">
										<html:optionsCollection name = "dyComboPRMScheduler" property = "schedulerList" value = "value" label = "label"/>    
									</html:select></td>
									</logic:present>
									<logic:notPresent name = "dyComboPRMScheduler" scope="request" >
										<td class="textwhiteFont">A team member in this role is not defined for the project.
										</td>
										<html:hidden property = "newScheduler" />
									</logic:notPresent>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr><td height="30"></td></tr>
					<tr>
						<td class = "textwhitetop" colspan = "2"><b>Input Sites and Scheduled Start Date</td>
					</tr>
					<tr><td  height="8"></td></tr>
					<tr>
						<td class = "textwhitetop" colspan = "2">Copy and paste list of sites and scheduled start dates in to the form field below.</td>
					</tr>
					<tr><td height="8"></td></tr>
					<tr>
						<td class = "textwhitetop"><html:textarea  property = "paragraph_content" cols="70" rows="10" styleClass = "textarea"></html:textarea>&nbsp;</td>
						<td class = "textwhitetop"><b>Important:</b><br><br>Copy and paste from an Excel spread sheet.<br>
						Do not update or change once this information<br>is entered in the form field. If a change is <br>
						required, make the change in the spread sheet <br>and then copy and paste all of the information<br>
						again.<br><br>Site numbers must match preloaded sites. Any<br>sites which could not be found will be ignored<br>
						 in the job creation process.</td>
					</tr>
					<tr>
						<td height="6"></td>
					</tr>
					<tr>
						<td class = "textwhitetop">
							<html:submit property = "save" styleClass = "button_c" onclick ="return createJobs();">Create Jobs</html:submit>
							<html:button property = "createMPO" styleClass = "button_c" onclick ="addMPO();">Add MPO and Partner</html:button>
							<html:reset property = "cancel" styleClass = "button_c">Cancel</html:reset>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</html:form>
<!-- First Table -->

<script>
function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filter").style.visibility="visible";
<%}else{%>
document.getElementById("filter").style.visibility="hidden";
<%}%>
}
function createJobs() {
	trimFields();
	var reqflag = 'false';
	//Check  that There is no activity for any Appendix Name
	var check = 0;
	 check=<%= activity_size %> ;
	if(check ==0)
	   {
	      alert("<bean:message bundle = "PRM" key = "prm.BulkJobCreation.warningIfThereIsNoActivity"/>");
	      return false;
	   }	
   // Closed Check  that There is no activity for any Appendix Name  
	if(document.forms[0].job_NewName.value=='')
   {
       document.forms[0].job_NewName.focus();
       alert("<bean:message bundle = "PRM" key = "prm.BulkJobCreation.warningPleaseEnterJobName"/>");
      return false;
   }
	
	var chekActivity=true;
	for( var i = 0; i < document.forms[0].activity_id.length;i++ )
	{	
		if( document.forms[0].activity_id[i].checked == true )
		{
			chekActivity=false;
			break;
		} else {
			chekActivity=true;
		}
	}
		if(chekActivity==true) {
		alert('Please select an Activity');
		return false;
	}
	//alert(document.forms[0].paragraph_content.value);
		if(document.forms[0].paragraph_content.value=='')
   {
       document.forms[0].paragraph_content.focus();
       alert("Please Enter Sites and Scheduled Start Date");
      return false;
   }
	//Start code block for enabling overHead during save
	for( var i = 0; i < document.forms[0].activity_id.length;i++ )
	{	
		if( document.forms[0].type[i].value == 'Overhead' )
		{
			document.forms[0].activity_id[i].disabled = false;
		}
	}

	// closed code block for enabling overHead during save
	//document.forms[0].save.value="true";
	//alert('tyu');
	document.forms[0].action = "LargeDeplBulkJobCreateAction.do?ref=saveSiteSearch";
	document.forms[0].save.value="Create Job";
	document.forms[0].submit();
	return true;
}
function addMPO () {

trimFields();
	var reqflag = 'false';
	//Check  that There is no activity for any Appendix Name
	var check = 0;
	 check=<%= activity_size %> ;
	if(check ==0)
	   {
	      alert("<bean:message bundle = "PRM" key = "prm.BulkJobCreation.warningIfThereIsNoActivity"/>");
	      return false;
	   }	
   // Closed Check  that There is no activity for any Appendix Name  
	if(document.forms[0].job_NewName.value=='')
   {
       document.forms[0].job_NewName.focus();
       alert("<bean:message bundle = "PRM" key = "prm.BulkJobCreation.warningPleaseEnterJobName"/>");
      return false;
   }
	
	var chekActivity=true;
	for( var i = 0; i < document.forms[0].activity_id.length;i++ )
	{	
		if( document.forms[0].activity_id[i].checked == true )
		{
			chekActivity=false;
			break;
		} else {
			chekActivity=true;
		}
	}
		if(chekActivity==true) {
		alert('Please select an Activity');
		return false;
	}
	//alert(document.forms[0].paragraph_content.value);
		if(document.forms[0].paragraph_content.value=='')
   {
       document.forms[0].paragraph_content.focus();
       alert("Please Enter Sites and Scheduled Start Date");
      return false;
   }
	//Start code block for enabling overHead during save
	for( var i = 0; i < document.forms[0].activity_id.length;i++ )
	{	
		if( document.forms[0].type[i].value == 'Overhead' )
		{
			document.forms[0].activity_id[i].disabled = false;
		}
	}
	if("<c:out value = '${requestScope.mpoListSize}' />" <= 0) {
		alert('This Appendix does not have Approved MPOs.');
		return false;
	}
	document.forms[0].action = "LargeDeplBulkJobCreateAction.do?form=MPOandPartner";
	document.forms[0].submit();
	return true;
}

function leftAdjLayers(){
try{

		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;
		
		<%if(request.getAttribute("opendiv") != null && !request.getAttribute("opendiv").equals("")){%>
			document.getElementById("filter").style.visibility="visible";
		<%}else{%>
			document.getElementById("filter").style.visibility="hidden";
		<%}%>
		
		
	}
	catch(e)
	{
	}	
		
}
<%--
<%
if(request.getAttribute( "JobSaved" ) != null) 
	{%>
		parent.ilexleft.location.reload();
	<%}
%>
--%>
</script>

<script>

function setoverheadcheck()
{	
	if(<%= activity_size %> >0)
    {	
	for( var i = 0; i < document.forms[0].activity_id.length;i++ )
	{	
		if( document.forms[0].type[i].value == 'Overhead' )
		{
			document.forms[0].activity_id[i].checked = true;
			//Following is added for disabling overhead
			document.forms[0].activity_id[i].disabled = true;
    		//Above is added for disabling overhead
		}
	}
	}
	defaultAddendumdisplay();
	return true;
}

function defaultAddendumdisplay() { //v3.0
	//alert('222222222222');
	var tableRow = document.getElementsByTagName('div');
	
	if (tableRow.length==0) 
	{ 
		return; 
	}
	
	for (var k = tableRow.length-1; k >-1; k--) 
	{
		viewChange2(tableRow[k].getAttributeNode( 'id' ).value );
	}
	
	viewChange2('div_1');
	
	return true;
}

function viewChange2(id) {
	if (typeof id !== 'undefined') {
		$('.viewdiv').hide();
		$('#' + id).show();
	}
}
	
function viewChange() { //v3.0
		//alert('33333333');
	var tableRow = document.getElementsByTagName( 'div' );
	for (var k = 0; k < tableRow.length; k++) 
	{
	 	if(!(tableRow[k].getAttributeNode('id').value == '' || tableRow[k].getAttributeNode('id').value == 'menunav' || tableRow[k].getAttributeNode('id').value == 'div_sitesearch' || tableRow[k].getAttributeNode('id').value == 'div_savebutton' || tableRow[k].getAttributeNode('id').value == 'featured' || tableRow[k].getAttributeNode('id').value == 'featured1' || tableRow[k].getAttributeNode('id').value == 'filter' ) )
	 		MM_showHideLayers(tableRow[k].getAttributeNode( 'id' ).value , '' , 'hide' );
	}	
	
	var args = viewChange.arguments;
	
	MM_showHideLayers( args[0] , '' , 'show' );
	 leftAdjLayers();
	return true;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
 
  for (i=0; i<(args.length-2); i+=3)
   	if ((obj=MM_findObj(args[i]))!=null) 
  	{ 
  		v=args[i+2];
	    if (obj.style) 
	    { 
	    	obj=obj.style; 
	    	v=(v=='show')?'visible':(v='hide')?'hidden':v;
	    }
    	obj.visibility=v;
    }
  leftAdjLayers();
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = <%=Appendix_Id%>;
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
function adjustpos( count,sizeofmessage )
{
	//alert(div_sitesearch.style.pixelTop);
	div_sitesearch.style.pixelTop = count * 20+375+sizeofmessage;
   //alert(div_sitesearch.style.pixelTop );
    return true;
}
 
</script>
</body>
</html:html>

