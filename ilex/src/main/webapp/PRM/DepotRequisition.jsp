<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>

<%
int depotlistsize = 0;
if(request.getAttribute("depotlistsize")!=null) {
	depotlistsize = Integer.parseInt(request.getAttribute("depotlistsize").toString());
}
%>
<script>
	function addQuantity()
	{
 	  if( document.forms[0].depot_requisition.value == "" )
 		{
 			alert("<bean:message bundle="PRM" key="prm.netmedx.productvalidation"/>");
 			document.forms[0].depot_requisition.focus();
			return false;
 		}
	    //document.forms[0].refresh.value = "true";
	    //document.forms[0].dialog_number.value = eval(document.forms[0].dialog_number.value)+1;
	    //document.forms[0].refreshprodlist.value = "false";
	    document.forms[0].addquantity.value = "showquantity";
	   // enableItems();
	    document.forms[0].action="DepotRequisition.do";
		document.forms[0].submit();
		return true;
	}
		function onSave()
		{
			
			
			if( <%= depotlistsize %> != 0 )
			{
				if( <%= depotlistsize %> == 1) {
					if(document.forms[0].depot_quantity.value == "")
		  			{
		  				document.forms[0].depot_quantity.value = "0";
		  			}
	 
			  		if( !isInteger(document.forms[0].depot_quantity.value ) )
			 		{
			 			alert( "<bean:message bundle = "PRM" key = "prm.netmedx.depotquantityvalidation"/>"+"." );
			 			document.forms[0].depot_quantity.value = "";
			 			document.forms[0].depot_quantity.focus();
						return false;
			 		}
				}
				
				else {
					for( var i = 0; i<<%= depotlistsize %>; i++ )
					  	{
							if(document.forms[0].depot_quantity[i].value == "")
				  			{
				  				document.forms[0].depot_quantity[i].value = "0";
				  			}
			 
					  		if( !isInteger(document.forms[0].depot_quantity[i].value ) )
					 		{
					 			alert( "<bean:message bundle = "PRM" key = "prm.netmedx.depotquantityvalidation"/>"+"." );
					 			document.forms[0].depot_quantity[i].value = "";
					 			document.forms[0].depot_quantity[i].focus();
								return false;
					 		}
						}
				}
				
			}
			
			
			document.forms[0].action="DepotRequisition.do?hmode=save";
			document.forms[0].submit();
		}
		function addQuantity()
			{
				if( document.forms[0].depot_requisition.value == "" )
		 		{
		 			alert("<bean:message bundle="PRM" key="prm.netmedx.productvalidation"/>"+".");
		 			document.forms[0].depot_requisition.focus();
					return false;
		 		}
			 	document.forms[0].action="DepotRequisition.do?addquantity=showquantity";
				document.forms[0].submit();
				return true;
			}
			
		
</script>
</head>
<%-- <%@ include  file="/NMenu.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<%@ include  file="/AjaxOwners.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %> --%>


	<body>
		<form action="DepotRequisition" method="post" >
		<html:hidden name="DepotRequisitionForm" property="ticket_id"/>
		<html:hidden name="DepotRequisitionForm" property="jobid"/>
	<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Job</span></a>
  			<ul>
				
					<li>
						<a href="#"><span>Change Status</span></a>
						<ul>
							<li><a href="JobDashboardAction.do?isClicked=5&amp;tabId=5&amp;function=view&amp;appendixid=<c:out value='${requestScope.appendixId }'/>&amp;ticketType=&amp;jobid=<c:out value='${requestScope.Job_Id }'/>">Complete</a></li>
							<li><a href="MenuFunctionChangeStatusAction.do?Type=prj_job&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;Status=H">Hold</a></li>
							<li><a href="MenuFunctionChangeStatusAction.do?Type=prj_job&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;Status=C">Cancelled</a></li>
						</ul>
					</li>
					
		  		<li>
		        	<a href="#"><span>Documents</span></a>
		        	<ul>
		      			<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.Job_Id }'/>&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;entityType=Deliverables&amp;function=addSupplement&amp;linkLibName=Job">Upload</a></li>
		      			<li><a href="EntityHistory.do?entityType=Deliverables&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;linkLibName=Job&amp;function=supplementHistory">History</a></li>
		  			</ul>
		  		</li>
      			<li><a href="ChecklistTab.do?job_id=<c:out value='${requestScope.Job_Id }'/>&amp;page=jobdashboard&amp;viewjobtype=ION&amp;ownerId=1">Process Checklist</a></li>
      			<li><a href="InsuranceRequest.do?msaId=<c:out value='${requestScope.msaId }'/>&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;appendixId=<c:out value='${requestScope.appendixId }'/>">Email Insurance Certificate Request</a></li>
      			<li><a href="JobNotesAction.do?jobId=<c:out value='${requestScope.Job_Id }'/>&amp;appendixid=<c:out value='${requestScope.appendixId }'/>">Job Notes</a></li>
      			
      				<li><a href="DepotRequisition.do?appendixid=<c:out value='${requestScope.appendixId }'/>&amp;Job_Id=<c:out value='${requestScope.Job_Id }'/>">Depot Requisition</a></li>
					
		      			<li><a href="TicketChangeCriticality.do?msaId=<c:out value='${requestScope.msaId }'/>&amp;Job_Id=<c:out value='${requestScope.Job_Id }'/>&amp;appendixId=<c:out value='${requestScope.appendixId }'/>">Change HelpDesk/ Criticality</a></li>
		      			<li><a href="ChangeTicketRequestType.do?msaId=<c:out value='${requestScope.msaId }'/>&amp;Job_Id=<c:out value='${requestScope.Job_Id }'/>">Change Request Type</a></li>
						
					
      					<li><a href="javascript:cancelTicket('null');">Cancel Ticket</a></li>
						
		  		<li>
		        	<a href="#"><span>Test Indicator</span></a>
		        	<ul>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&amp;jobIndicator=None&amp;changeTestIndicator=yes&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;tabId=1">None</a></li>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&amp;jobIndicator=Demonstration&amp;changeTestIndicator=yes&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;tabId=1">Demonstration</a></li>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&amp;jobIndicator=Internal Test&amp;changeTestIndicator=yes&amp;jobid=<c:out value='${requestScope.Job_Id }'/>&amp;tabId=1">Internal Test</a></li>
					</ul>
		  		</li>	
  			</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:projectJobManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:projectJobSendStatusReport()">Send</a></li>
      			<li><a href="javascript:projectJobManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			          <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
									 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
										  <a><span class="breadCrumb1"><bean:message bundle="PRM" key="prm.netmedx.depotrequisition"/></a>
			    </div></td>
			</tr>
	        <tr><td height="5">&nbsp;</td></tr>	
		</tbody></table>	
		
		
		
		
		<%-- <table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
					<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
					<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
					
							<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
									 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
										  <a><span class="breadCrumb1"><bean:message bundle="PRM" key="prm.netmedx.depotrequisition"/></a>
			    </td>
			</tr>	       
		</table> --%>
			<table border="0" cellpadding="0" cellspacing="1" class="stdPagePadding">
				<tr><td height="15"></td></tr>
				<tr>
					    <TD class=dbvalue  colspan="2">
						   	<logic:present name="productcatlist">
			
							<html:select name="DepotRequisitionForm" property = "depot_requisition" size="8" multiple="true" styleClass="combowhite">
							<logic:iterate id="productcatlist" name="productcatlist" >
							
							<bean:define id="prodcatname" name="productcatlist" property="dp_cat_name" />		   
								<optgroup class=dblabel label="<%=prodcatname%>">
									<html:optionsCollection name = "productcatlist" property="prodList" value = "value" label = "label"/>
								</optgroup> 
						 
							</logic:iterate>
							</html:select>
							</logic:present>	
						</TD>
						<TD class=dblabelbottom colspan="6">
						  <span>
						  	<html:button property = "add_quantity" styleClass = "button_c1" onclick = " addQuantity();">
						  		Add&nbsp;Quantity
						  	</html:button>
						  </span>
					  </TD>			
				    </TR>
				    
		    	   <logic:present name="DepotRequisitionForm" property="depoproductlist" scope="request">
				    <logic:iterate id="depoproductlist" name="DepotRequisitionForm" property="depoproductlist">
					    <html:hidden name="depoproductlist" property="de_product_id" />
					    <tr>
						
						<td class = "dbvaluesmall" colspan="2">
					      	<bean:write name="depoproductlist" property="de_product_name"/>
					    </td>
					    
					    <TD class=dbvalue colspan="6">
							<html:text property = "depot_quantity"  name="depoproductlist" size = "10" styleClass = "textbox"/>
			  		    </TD>
					    </tr>   	
				      	
				 	</logic:iterate>
		 		  </logic:present>
				<tr>
					<td>
						<table>
							<tr>
								<td>
									<html:button property = "save" styleClass = "button_c1" onclick = "onSave();">
								  		Save
								  	</html:button>&nbsp;
								  	<html:button property = "cancel" styleClass = "button_c1" onclick = "addQuantity();">
								  		Cancel
								  	</html:button>&nbsp;
								  	
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<c:if test = "${requestScope.jobType eq 'Addendum'}">
				<%@ include  file="/AddendumMenuScript.inc" %>
			</c:if>
			<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
				<%@ include  file="/ProjectJobMenuScript.inc" %>
			</c:if>	
		</form>
	</body>
</html>