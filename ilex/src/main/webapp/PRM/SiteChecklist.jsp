<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on partners.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%
String typeid = "";
String type = "";

if( request.getAttribute( "typeid" ) != null )
	typeid = request.getAttribute( "typeid" ).toString();

if( request.getAttribute( "type" ) != null )
	type = request.getAttribute( "type" ).toString();

if( request.getAttribute( "page" ) != null )
	type = request.getAttribute( "page" ).toString();

if( request.getAttribute( "pageid" ) != null )
	type = request.getAttribute( "pageid" ).toString();

if( request.getAttribute( "fromtype" ) != null )
	type = request.getAttribute( "fromtype" ).toString();


%>


<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>


<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<title></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>

<%@ include  file="/Menu.inc" %>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<!--  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  class="toprow"> 
      <table width="800" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="left"> 
        <td  width="200" class="toprow1">
        <a href="JobHeader.do?jobid=<%=typeid%>&function=view"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.site.backtojobdashboard"/></td>
        <td   width=600 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
		</tr>
      </table>
    </td>
  </tr>
</table>-->

<table>
<html:form action="SiteChecklist">
<html:hidden property="typeid"/>
<html:hidden property="type"/>
<html:hidden property="siteid"/>
<html:hidden property="function"/>
<html:hidden property="page"/>
<html:hidden property="pageid"/>
<html:hidden property="fromtype"/>
<html:hidden property="nettype"/>
<html:hidden property="viewjobtype"/>
<html:hidden property="ownerId"/>

<%if(request.getAttribute("saveflag").equals("0") )
{%>
	<table  border="0" cellspacing="1" cellpadding="1"  >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" > 
	  
	   		<logic:present name="checklist" scope="request">
	   			<logic:present  name="checklistsize" scope="request">
	   				<logic:notEqual name="checklistsize" value="0">
	   			
					  <tr>
					  	<%if(request.getAttribute("fromtype").equals("S"))
						{ %>
					    <td  class="labeleboldwhite" height="30" colspan="4"><bean:message bundle="PRM" key="prm.sitechecklistfor"/>&nbsp;<bean:write name="SiteChecklistForm" property="sitename"/><html:hidden name="SiteChecklistForm" property="sitename"/></td>
					  <%}
					  	else
						{ %>
						<td   class="labeleboldwhite" height="30" colspan="4"><bean:message bundle="PRM" key="prm.qcchecklistfor"/>&nbsp;<bean:write name="SiteChecklistForm" property="sitename"/><html:hidden name="SiteChecklistForm" property="sitename"/></td>
						<%} %>
					  </tr>
					  <tr > 
					    <td width="20"></td>
					    <td class="tryb"  width="140"><bean:message bundle="PRM" key="prm.checklistname"/></td>
						<td class="tryb" width="140"><bean:message bundle="PRM" key="prm.itemname"/></td>
					    <td class="tryb" width="250"><bean:message bundle="PRM" key="prm.itemdescription"/></td>
					  </tr>
					  
					   
					 	<%int i=1;
						String bgcolor=""; %>
						
						
						<logic:iterate id="clist" name="checklist">
						
						
					 	<%if((i%2)==0)
							{
								bgcolor="labele"; 
							
							} 
							else
							{
								bgcolor="labelo";
							}%>
					 	
					 	
						  
					      <tr> 
						  	 <td   height="20"><html:multibox property="sitecheckbox"><bean:write  name="clist" property = "checklistidtype"/></html:multibox><html:hidden  name="clist" property = "checklistitemid"/><html:hidden name="clist" property="checklisttype"/></td>
							 <td  class="<%=bgcolor %>" height="20"><bean:write name="clist" property="checklistname"/><html:hidden name="clist" property="checklistname"/></td>
							 <td  class="<%=bgcolor %>" height="20"><bean:write name="clist" property="itemname"/><html:hidden name="clist" property="itemname"/></td>
							 <td  class="<%=bgcolor %>" height="20"><bean:write name="clist" property = "itemdescription"/><html:hidden name="clist" property = "itemdescription"/></td>
						  </tr> 
						 
						 <%i++; %>	
					
						</logic:iterate>
						
						
						<tr> 
							<td   height="20">
					    	<td  class="buttonrow" colspan="6"> 
					        <html:submit property="save" styleClass="button" ><bean:message bundle="PRM" key="prm.save" /></html:submit>
					        <html:reset property="reset" styleClass="button" ><bean:message bundle="PRM" key="prm.cancel" /></html:reset>
					        <%if(request.getAttribute("page").equals("appendixdashboard"))
							{ %>
					        <html:button property="back" styleClass="button" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					    	<%} %>
					        <%if(request.getAttribute("page").equals("jobdashboard"))
							{ %>
					        <html:button property="back" styleClass="button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					    	<%} %>
						    
					    	</td>
					    </tr>
			    	</logic:notEqual> 
			    </logic:present> 
			    
			</logic:present> 
	
			<jsp:include page = '/Footer.jsp'>
		  	 <jsp:param name = 'colspan' value = '28'/>
		     <jsp:param name = 'helpid' value = 'roleprivilege'/>
		   </jsp:include>
	   
		 </tr>
	    </table>
	   </td>
	  </tr>
	</table> 
<%}
else
{%>
	<table  border="0" cellspacing="1" cellpadding="1"  >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" > 
	  
	   		<logic:present name="savedchecklist" scope="request">
	   			
	   			<logic:present name="addmessage" scope="request">
	   				<logic:equal name="addmessage" value="0">
	   					<tr>
					    <td  class="message" height="30" colspan="3"><bean:message bundle="PRM" key="prm.addedsuccessfully"/></td>
					  	</tr>
	   				</logic:equal>
	   			</logic:present> 
	   				  
					  <tr>
					  	<%if(request.getAttribute("fromtype").equals("S"))
						{ %>
					   <td   class="labeleboldwhite" height="30" colspan="3"><bean:message bundle="PRM" key="prm.sitechecklistfor"/>&nbsp;<bean:write name="SiteChecklistForm" property="sitename"/></td>
					 	<%}
					  	else
						{ %>
						<td   class="labeleboldwhite" height="30" colspan="3"><bean:message bundle="PRM" key="prm.qcchecklistfor"/>&nbsp;<bean:write name="SiteChecklistForm" property="sitename"/></td>
						<%} %>
					    
					  </tr>
					  <tr > 
					    
					    <td class="tryb"  width="140"><bean:message bundle="PRM" key="prm.checklistname"/></td>
						<td class="tryb" width="140"><bean:message bundle="PRM" key="prm.itemname"/></td>
					    <td class="tryb" width="250"><bean:message bundle="PRM" key="prm.itemdescription"/></td>
					  </tr>
					  
					   
					 	<%int i=1;
						String bgcolor=""; %>
						
						
						<logic:iterate id="sclist" name="savedchecklist">
						
						
					 	<%if((i%2)==0)
							{
								bgcolor="labele"; 
							
							} 
							else
							{
								bgcolor="labelo";
							}%>
					 	
					 	
						  
					      <tr> 
						  	 
							 <td  class="<%=bgcolor %>" height="20"><bean:write name="sclist" property="checklistname"/></a></td>
							 <td  class="<%=bgcolor %>" height="20"><bean:write name="sclist" property="itemname"/></td>
							 <td  class="<%=bgcolor %>" height="20"><bean:write name="sclist" property = "itemdescription"/></td>
						  </tr> 
						 
						 <%i++; %>	
					
						</logic:iterate>
						
			    
			    
			</logic:present> 
			<tr> 
	 		 	  <td colspan="3" class="buttonrow"> 
	 		 	  <%if(request.getAttribute("page").equals("appendixdashboard"))
					{ %>
			        <html:button property="back" styleClass="button" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    	<%} %>
			        <%if(request.getAttribute("page").equals("jobdashboard"))
					{ %>
			        <html:button property="back" styleClass="button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    	<%} %>
	 		      
				  </td>
			 </tr>
	
			<jsp:include page = '/Footer.jsp'>
		  	 <jsp:param name = 'colspan' value = '28'/>
		     <jsp:param name = 'helpid' value = 'roleprivilege'/>
		   </jsp:include>
	   
		 </tr>
	    </table>
	   </td>
	  </tr>
	</table> 

<%} %>

<logic:present name="notfoundmessage" scope="request">
	<logic:equal name="notfoundmessage" value="1">
	  <table>
		<tr><td  class="message" height="30" ><bean:message bundle="PRM" key="prm.siteyetnotassigned"/></td></tr>
		<tr><td> <%if(request.getAttribute("page").equals("appendixdashboard"))
					{ %>
			        <html:button property="back" styleClass="button" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    	<%} %>
			        <%if(request.getAttribute("page").equals("jobdashboard"))
					{ %>
			        <html:button property="back" styleClass="button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    	<%} %>
			    	</td>
	   </tr>
	  </table>
	</logic:equal>
</logic:present> 

<logic:present name="checklistsize" scope="request">
	<logic:equal name="checklistsize" value="0">
	  <table>
		<tr><td  class="message" height="30" ><bean:message bundle="PRM" key="prm.nochecklist"/></td></tr>
		<tr><td> <%if(request.getAttribute("page").equals("appendixdashboard"))
					{ %>
			        <html:button property="back" styleClass="button" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    	<%} %>
			        <%if(request.getAttribute("page").equals("jobdashboard"))
					{ %>
			        <html:button property="back" styleClass="button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    	<%} %>
			    	</td>
	   </tr>
	  </table>
	</logic:equal>
</logic:present> 


</html:form>
</table>
</BODY>

<script>
function validate()
{
	var checkflag=0;
			for ( var i = 0 ; i < document.forms[0].sitecheckbox.length; i++ )
			{
							
				if(document.forms[0].sitecheckbox[i].checked)
				{
							
					checkflag=1;
				}
			}
			
			
			if(!checkflag)
			{
				alert('select a record first');
				return false;
			}
		

			
	return true;

	
}

function Backactionappendix()
{
	if(document.forms[0].nettype.value == 'dispatch')
		document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	else		
		document.forms[0].action = "AppendixHeader.do?function=view&appendixid=<%=request.getAttribute("pageid") %>&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
function Backactionjob()
{
	document.forms[0].action = "JobDashboardAction.do?jobid=<%= typeid %>" ;
	document.forms[0].submit();
	return true;
}
</script>

</html:html>
