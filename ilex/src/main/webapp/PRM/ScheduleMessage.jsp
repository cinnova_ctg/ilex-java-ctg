<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>
<html:html>
<bean:define id = "sform" name = "scheduleform"  scope = "request"/>

<bean:define id = "typeid" name = "scheduleform"  property="typeid" scope = "request"/>
<bean:define id = "function" name = "scheduleform"  property="function" scope = "request"/>
<bean:define id = "pageid" name = "scheduleform"  property="pageid" scope = "request"/>



<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>

<%@ include  file="/Menu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >


<!--  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing="1" cellpadding="0" height="18" >
        <tr align="left"> 
          <td class="toprow1" width=200>
           <%if(request.getAttribute("type").equals("P"))
          {%> 
          <a href="AppendixHeader.do?appendixid=<%=typeid %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtoappendixdashboard"/></a></td>
		  <%} %>
		  <%if(request.getAttribute("type").equals("J")||request.getAttribute("type").equals("AJ")||request.getAttribute("type").equals("CJ")||request.getAttribute("type").equals("IJ"))
          {
          	if(request.getAttribute("page").equals("appendixdashboard"))
          	{%> 
          	<a href="AppendixHeader.do?appendixid=<%=pageid %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtoappendixdashboard"/></a></td>
		  	<%} 
		 	else
		 	{%>
		  	<a href="JobHeader.do?jobid=<%=typeid %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtojobdashboard"/></a></td>
		 	<%} 
		  }%>
		  <%if(request.getAttribute("type").equals("A")||request.getAttribute("type").equals("AA")||request.getAttribute("type").equals("CA")||request.getAttribute("type").equals("IA"))
          {
          	if(request.getAttribute("page").equals("jobdashboard"))
          	{%> 
          	<a href="JobHeader.do?jobid=<%=pageid %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtoappendixdashboard"/></a></td>
		  	<%} 
		 	else
		 	{%> 
         	 <a href="ActivityHeader.do?activityid=<%=typeid %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtoactivitydashboard"/></a></td>
		  	<%} 
		  }%></td>
		   <td  width=600 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>-->

<table>
<html:form action="ScheduleUpdate" >
<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="400">
	  		<tr>
	  			<logic:present name = "sform" property="updatemessage">
				
		 		<logic:equal name="sform" property="updatemessage" value="0">
				<td  colspan="3" class="message" height="30" ><bean:message bundle="PRM" key="prm.schedule.updatedsuccessfully"/></td>
		 		</logic:equal>
				</logic:present>
			  			
	  		</tr>
	  		
	  		<tr>
	  			<td colspan="3" class="labeleboldwhite" height="30"><bean:message bundle="PRM" key="prm.schedule.updatedschedule"/></td>
	  		</tr>
	  		 
	  		<tr>
	   			 <td class="labelobold"><bean:message bundle="PRM" key="prm.schedule.scheduledstart"/></td>
	   			 <td class="labelo"><html:text  styleClass="textbox" size="10" property="startdate" readonly="true"/>
	   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].startdate, document.forms[0].startdate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
  				    <td class = "labelo"> 
				    <html:select name="ScheduleUpdateForm" property = "startdatehh" styleClass="comboo">  
					<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
					</html:select>
					<html:select name="ScheduleUpdateForm" property = "startdatemm" styleClass="comboo">  
					<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
					</html:select>
					<html:select name="ScheduleUpdateForm" property = "startdateop" styleClass="comboo">  
					<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
					</html:select>
				    </td>
			</tr>
	 
	    	<tr> 
			    <td class="labelebold"><bean:message bundle="PRM" key="prm.schedule.scheduledend"/></td>
			    <td class="labele"><html:text  styleClass="textbox" size="10" property="plannedenddate" readonly="true"/>
				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].plannedenddate, document.forms[0].plannedenddate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
					    <td class = "labele"> 
					    <html:select name="ScheduleUpdateForm" property = "plannedenddatehh" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "plannedenddatemm" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "plannedenddateop" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
			</tr>
			
			
			<tr>
	   			 <td  width="20%" class="labelobold"><bean:message bundle="PRM" key="prm.schedule.actualstart"/></td>
	   			 <td class="labelo"><html:text  styleClass="textbox" size="10" property="actstartdate" readonly="true"/>
	   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].actstartdate, document.forms[0].actstartdate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
	  	
					    <td class = "labelo"> 
					    <html:select name="ScheduleUpdateForm" property = "actstartdatehh" styleClass="comboo">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "actstartdatemm" styleClass="comboo">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "actstartdateop" styleClass="comboo">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
			</tr>
			
	 
	    	<tr> 
			    <td  class="labelebold"><bean:message bundle="PRM" key="prm.schedule.actualend"/></td>
			    <td class="labele"><html:text  styleClass="textbox" size="10" property="actenddate" readonly="true"/>
				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].actenddate, document.forms[0].actenddate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
				
					    <td class = "labele"> 
					    <html:select name="ScheduleUpdateForm" property = "actenddatehh" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "actenddatemm" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "actenddateop" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
						</td>		  		
				</tr>		
				
	
	  		 <tr> 
	    		 <td  class="labelobold" height="20"><bean:message bundle="PRM" key="prm.schedule.noofdays"/></td>
	   			 <td  class="labelo" height="20" colspan="2"><bean:write name="sform" property="totaldays"/></td>
	 		 </tr>
	 		 
	 		  <tr> 
	 		 	  <td colspan="3" class="buttonrow"> 
	 		      <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  </td>
			 </tr>
	 		 <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>

</html:form>
</table>	
</body>

<script>
function Backaction()
{
	<%if(request.getAttribute("type").equals("P"))
          {%> 
          document.forms[0].action = "AppendixHeader.do?appendixid=<%=typeid %>&function=view";
		  document.forms[0].submit();
		  return true;
         
		  <%} %>
		  <%if(request.getAttribute("type").equals("J")||request.getAttribute("type").equals("AJ")||request.getAttribute("type").equals("CJ")||request.getAttribute("type").equals("IJ"))
          {
          	if(request.getAttribute("page").equals("appendixdashboard"))
          	{%> 
          	document.forms[0].action = "AppendixHeader.do?appendixid=<%=pageid %>&function=view";
		    document.forms[0].submit();
			return true;
          	
		  	<%} 
		 	else
		 	{%>
		  	document.forms[0].action = "JobHeader.do?jobid=<%=typeid %>&function=view";
			document.forms[0].submit();
			return true;
		  	
		 	<%} 
		  }%>
		  <%if(request.getAttribute("type").equals("A")||request.getAttribute("type").equals("AA")||request.getAttribute("type").equals("CA")||request.getAttribute("type").equals("IA"))
          {
          	if(request.getAttribute("page").equals("jobdashboard"))
          	{%> 
          	document.forms[0].action = "JobHeader.do?jobid=<%=pageid %>&function=view";
			document.forms[0].submit();
			return true;
          	
		  	<%} 
		 	else
		 	{%> 
         	document.forms[0].action = "ActivityHeader.do?activityid=<%=typeid %>&function=view";
			document.forms[0].submit();
			return true;
         	
		  	<%} 
		  }%>
	
}
</script>
</html:html>