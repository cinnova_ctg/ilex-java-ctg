<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<script language = "JavaScript" src = "docm/javascript/ViewManipulation.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<%@ include file = "/NMenu.inc" %>
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %> 
<%-- <%@ include  file="/NetMedXDashboardMenu.inc" %> --%>
	<script>
		function checkDigitAfterDot(element){
			var seq = element.value;
			if((seq.indexOf(".")!=-1) && ((seq.length-(seq.indexOf(".")+1))>1)){
				alert("Only decimal number up to one digit is allowed.");
				element.focus();
				return false;
			}
			else if( seq.indexOf(".")==-1 && seq.length > 3){
				alert("Only three digit number is allowed.");
				element.focus();
				return false;
			}
			
		}
	
		function onModify(){
			var seq=document.getElementsByName('seq');
			var desc=document.getElementsByName('desc');
			for(var i=0;i<seq.length;i++){
				if(is_empty(seq[i],"Sequence Number")){
					return false;
				}
				if(is_empty(desc[i],"Description")){
					return false;
				}
			}
			document.forms[0].action="ProjectLevelWorkflowAction.do?hmode=save";
			document.forms[0].submit();
		}
		function onRefresh(){
			document.forms[0].action="ProjectLevelWorkflowAction.do";
			document.forms[0].submit();
		}
		function setStyleForCells(counter,cellObject)
			{
				if(counter % 2 == 0){
						cellObject.className = 'Ntextoleftalignnowrap';
					}
					else{
						cellObject.className = 'Ntexteleftalignnowrap';
					}
			}
		function addFilterCriteriaRow(){
			var object= document.all.checkList;
			var i = object.rows.length ;
			var oRow;
				oRow=object.insertRow();
				oCell=oRow.insertCell(0);
				oCell.innerHTML='<input type="text" name="seq" maxlength="5" style="width: 40px;" class="textalingrigh"  onkeypress="return currencyNumericOnly(this.event);" onblur="checkDigitAfterDot(this);"><input type="hidden" name="item_id" value="0" />';
				oCell.align='center';
				setStyleForCells(i-1,oCell);
				oCell=oRow.insertCell(1);
				oCell.innerHTML='<input type="text" name="desc" maxlength="200" style="width: 550px;" class="text" ><input type="hidden" name="filterId" value="" />'
				oCell.align='center';
				setStyleForCells(i-1,oCell);
				oCell=oRow.insertCell(2);
				oCell.innerHTML='<img  align="center" src="docm/images/delete.gif" name="filterImage" onclick="Rowdelete(\'checkList\',this)"/>';
				oCell.align = 'center';
				setStyleForCells(i-1,oCell);
			}
		function Rowdelete(tableId,imageId)
			{
				var tableObj = document.getElementById(tableId);
				if(tableId=='checkList'){
					var length = document.all.filterImage.length;
					if(!length)
					{
						tableObj.deleteRow(1);
					}
					for(var i=0;i<length;i++)
					{
						if(document.all.filterImage[i]==imageId)
						{	
							var index = (i+1);
							tableObj.deleteRow(index);
						}	
					}
				}
			}

	</script>
		<script>
		function is_empty(chkObj, message)
			{
				if(chkObj.value == '')
					{
						alert(message + ' is required.');
						chkObj.focus();
						return true;
					}
				return false;
	
			}
		function currencyNumericOnly(e)
			{
				var key;
				var keychar;
				if (window.event){
				   key = window.event.keyCode;
				   }
				else if (e){
				   key = e.which;
				   }
				else{
				   return true;
				   }
				keychar = String.fromCharCode(key);
				keychar = keychar.toLowerCase();
		
				// control keys
				if ((key==null) || (key==0) || (key==8) ||
					(key==9) || (key==13) || (key==27) ){
				   return true;
				   }
		
				// numbers
				else if ((("0123456789.").indexOf(keychar) > -1)){
				   return true;
				   }
				else{
				alert("Only Numeric values are allowed.");
				   return false;
		   }
	}//end of numericOnly
	// function add for Reports 
	/* function openreportschedulewindow(report_type,appendixId) {
	alert();
	str = "ReportScheduleWindow.do?report_type="+report_type+"&typeid="appendixId&fromPage=NetMedX";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 500, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
} */
	
	
	
	</script>
	
</head>
	<body>
		<form action="ProjectLevelWorkflowAction" method="post">
		<input type="hidden" name="appendixId" value="<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>"/>
		<input type="hidden" name="jobid" value="<c:out value='${ProjectLevelWorkflowForm.jobId}'/>"/>
		
		<c:if test="${ empty requestScope.isNetMedX}">
			<%@ include  file="/DashboardVariables.inc" %>
			 <%@ include  file="/AppedixDashboardMenu.inc" %> 
			<!--  Menu change  -->
			
			<div id="menunav" >
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul style="visiblilty:visible;">
        		<li><a href="JobEditAction.do?ref=inscopeview&amp;appendix_Id=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;Job_Id=0">Add Job</a></li>
        		<li>
        			<a href="#"><span>Bulk Job</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=<%= appendixid %>">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=<%= appendixid %>">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li>
				
	        		<% if( contractDocMenu != "" ) { %>
	        		<li>
	        			<a href="#"><span>Contract Documents</span></a>
	        			<ul>
							<%= contractDocMenu %>
	        			</ul>
	        		</li>
				<% } %>
				
        		<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Custom Fields</a></li>
        		<li><a href="AddCustRef.do?typeid=<%= appendixid %>&amp;type=Appendix&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Customer Reference</a></li>
        		<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">External Sales Agent</a></li>
        		<li>
        			<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;initialCheck=true&amp;pageType=jobsites">Job Sites</a></li>
        		<li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=<%= appendixid %>&amp;type=P&amp;page=appendixdashboard&amp;pageid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Update</a></li>
						
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Special Instructions/Conditions</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">Team</a></li>
        		<li><a href="OnlineBid.do?appendixId=<%= appendixid %>&amp;fromPage=Appendix">Online Bid</a></li>
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
        		<li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li>
				<li><a href="MSPCCReportAction.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">MSP C/C Report</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid %>">Search</a></td>
						
  		</li>
  	
	</ul>
</div>
<%-- <table cellpadding="0" cellspacing="0" border="0" width = "100%">
<tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
								 	<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixid }'/><c:out value='${requestScope.appendixName }'/></a>
									<a><span class="breadCrumb1">Project Level Workflow Checklist Items</a></div>
				        </td>
			       </tr>
</table>	 --%>	
			
			
			
			
			
			
			
			
			<%-- <table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr> 
			          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage</center></a></td>
			          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
			     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%=appendixid%>&ownerId=<%=ownerid%>&msaId=<%=msaid%>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
			          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
								 	<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixid }'/>"><c:out value='${requestScope.appendixName }'/></a>
									<a><span class="breadCrumb1">Project Level Workflow Checklist Items</a></div>
				        </td>
			       </tr>
		      </table> --%>
		    <%--   <%@ include  file="/AppedixDashboardMenuScript.inc" %> --%>
	      </c:if>
    <c:if test="${ not empty requestScope.isNetMedX}">
	<%@ include  file="/DashboardVariables.inc" %>
<%-- <%@ include  file="/DashboardVariables.inc" %> --%>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
	
		<div id="menunav">
		<%if(!nettype.equals("dispatch")){ %>  
		<ul>
  		<li>
        	<a class="drop" href="#">Manage</a>
      
       	<ul>
        <li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&appendixId=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        	
        		
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&fromPage=NetMedX">Team</a></li>
    
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
       	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=0&ownerId=All&msaId=<c:out value='${requestScope.msaId }'/>&searchType=ticket">Search</a>
  		</li>
  		
	</ul>
</li>
<%}else{%>
<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        		
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
         </ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=0&ownerId=All&msaId=<c:out value='${requestScope.msaId }'/>&searchType=ticket">Search</a>
  		</li>
 	</li>	
	<%} %>
</div>
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			        <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${ProjectLevelWorkflowForm.appendixId}'/>"><c:out value='${requestScope.appendixName }'/></a>
										 <a ><span class="breadCrumb1">Project Level Workflow Checklist Items </span></a>
								</div>		
			    </div></td>
			</tr>
	       
		</tbody>
		</table>	
		
		
	     </c:if>	
		
		
		
		
		
		
		
		
		
		
			<%--  <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<%if(!nettype.equals("dispatch")){ %>
					<td class=Ntoprow1 id=pop1 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>" style="width: 120px"><center>Search5</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					<%}else{%>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px">Reports</a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>&nettype=<%=nettype %>" style="width: 120px"><center>Search6</center></a></td>
					<td class=Ntoprow1 id=pop4 width=640 colspan="2">&nbsp;</td>
					<%} %>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		  <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										 <a ><span class="breadCrumb1">Project Level Workflow Checklist Items 2</span></a>
								</div>		
			        </td>
		        </tr>
		         
	      </table> --%>
	   <%--    <%@ include  file="/NetMedXDashboardMenuScript.inc" %> --%>
	 
			<table border="0" cellpadding="0" cellspacing="1" class="stdPagePadding" >
			
				
				<tr>
					<td>
						<TABLE  cellspacing="0" cellpadding="1">
							<TR id="tab" >
								<TD width="680">
									<TABLE id="checkList" width="100%" cellspacing="1" cellpadding="1" >
									<c:if test="${requestScope.updatedSuccess eq 'updatedSuccess'}">
										<tr>
												<td class="message" colspan = "3" style="padding-left: 2px;">
													Workflow Checklist Items updated successfully.
												</td>
										</tr>

									</c:if>						
										<TR>
											<TD bgcolor="#EEEEEE" class = "Ntryb" style="width: 5px;text-align: center;" > Sequence Number</TD>
											<TD bgcolor="#EEEEEE" class = "Ntryb" width="80%"  nowrap> Description</TD>
											<TD bgcolor="#EEEEEE" class = "Ntryb" width="10%" nowrap align="center"> Delete</TD>
										</TR>
										<c:forEach items="${requestScope.projectWorkflowItemList}" var="projectWorkflowItem" varStatus="index">
											<TR >
												<input type="hidden" name="item_id"  value="<c:out value='${projectWorkflowItem.itemId}'/>"/>
												<TD valign=top align="center" width="10%" <c:if test = "${index.index % '2' eq 0}">class='Ntextoleftalignnowrap'</c:if><c:if test = "${index.index % '2' eq 1}">class='Ntexteleftalignnowrap'</c:if>> 
													<input type="text"   maxlength="5" style="width: 40px;" class="textalingrigh" name="seq"  value="<c:out value='${projectWorkflowItem.sequence}'/>" onkeypress="return currencyNumericOnly(this.event);" onblur="checkDigitAfterDot(this);"/>
												</TD>
												<td align="center" width="80%"  <c:if test = "${index.index % '2' eq 0}">class='Ntextoleftalignnowrap'</c:if><c:if test = "${index.index % '2' eq 1}">class='Ntexteleftalignnowrap'</c:if>>
													<input type="text"   maxlength="200" style="width: 550px;"  class="text" name="desc" value="<c:out value='${projectWorkflowItem.description}'/>"/>	
												</TD>
												<TD valign=top  align="center" width="10%"  <c:if test = "${index.index % '2' eq 0}">class='Ntextoleftalignnowrap'</c:if><c:if test = "${index.index % '2' eq 1}">class='Ntexteleftalignnowrap'</c:if>><img  align="center" src="docm/images/delete.gif" name="filterImage" onclick="Rowdelete('checkList',this)"/></TD>
											</TR>
										</c:forEach>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
				    </td>
				</tr>
				<tr>
					<td >
						<TABLE  cellspacing="0" cellpadding="0" width='100%' >
							<tr>
								<td class = "colLight">
									<table  border="0">
										<tr>
											<td align='left'>
												<input type="button" name="addMore" value="Add Row &darr;" onclick="addFilterCriteriaRow();" class="button_c">
											</td>
											<td align='left'>
												<input type="button" name="update" value="Update" onclick="onModify();" class="button_c">
											</td>
											<td align='left'>
												<input type="reset" name="reset" value="Cancel" onclick="onRefresh();" class="button_c">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</TABLE>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>