<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html:html>
<head>

<meta http-equiv="Content-Type"
content="text/html;charset=UTF-8" />
	<title></title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	
	
</head>

<%@ include  file="/Menu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<bean:define id="critcombo" name="criticalitycombo" scope = "request" />
<bean:define id="stcombo" name="statuscombo" scope = "request" />
<bean:define id="rqcombo" name="requestorcombo" scope = "request" />
<bean:define id="cdcombo" name="critdesccombo" scope = "request" />
<bean:define id="mpcombo" name="mspcombo" scope = "request" />
<bean:define id="hdcombo" name="helpdeskcombo" scope = "request" />
<bean:define id="mcombo" name="msacombo" scope = "request" />
<bean:define id="appcombo" name="appendixcombo" scope = "request" />

<table>
<html:form action="ReportScheduleWindow" >
<html:hidden property="report_type" />
<html:hidden property="typeid" />
<html:hidden property="fromPage" />
<html:hidden property="appendixname"/>


	<logic:present name="ActivityError" scope="request">
	<table>
		<tr>
		  <td  width="2" height="0"></td>
		  <td class="message"><bean:message bundle="PRM" key="prm.reportschedule.activityerror"/></td>	
		</tr>	
	</table>
	</logic:present>

<table  border="0" cellspacing="1" cellpadding="1">
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="560">
	  		<tr>
	  			<td colspan="3" class="labeleboldhierrarchy" height="30" width="560">
	  			<%--Start:Modified By Vishal 08/01/2007  --%>	
	  			<logic:equal name="ReportScheduleWindowForm" property="fromPage" value="dispatch"> 
			  		    <bean:message bundle="PRM" key="prm.reportschedule.reportschedule"/>&nbsp;<bean:message bundle="PRM" key="prm.reportschedule.forappendix"/>&nbsp;<bean:write name="ReportScheduleWindowForm" property="appendixname"/>
				</logic:equal>	
			  	
			  	<logic:notEqual name="ReportScheduleWindowForm" property="fromPage" value="dispatch"> 
			    		<bean:message bundle="PRM" key="prm.reportschedule.reportschedule"/>&nbsp;<bean:message bundle="PRM" key="prm.reportschedule.changeappendix"/>&nbsp;<bean:write name="ReportScheduleWindowForm" property="msaname"/>&nbsp;<bean:message bundle="PRM" key="prm.reportschedule.completename"/>&nbsp;<bean:write name="ReportScheduleWindowForm" property="appendixname"/>&nbsp; 
			 	</logic:notEqual>
			 	<%--End: --%>
			 	<logic:present name="jobname">
		  			<bean:message bundle="PRM" key="prm.reportschedule.job"/>&nbsp;<bean:write name="ReportScheduleWindowForm" property="jobname"/>
			 	</logic:present>		
	  			</td>
	  		</tr>
	  		
	  		<logic:equal name="ReportScheduleWindowForm" property="report_type" value="CSVSummary">
		  		<logic:equal name="ReportScheduleWindowForm" property="fromPage" value="dispatch">
		  		 <tr> 
				    <td  class="labelobold">MSA</td>
				    <td  align=left class="labelo" >
						<html:select property="msa"  styleClass="comboo" onchange="javascript:appendixPopulate();">
						<html:optionsCollection name = "mcombo"  property = "msalist"  label = "label"  value = "value" /></html:select>
					</td>	
					<td width="280"></td>
				 </tr>
				 
				  <tr> 
				    <td  class="labelebold">APPENDIX</td>
				    <td  align=left class="labele">
						<html:select property="appendix"  styleClass="comboe" onchange="javascript:setAppendix();">
						<html:optionsCollection name = "appcombo"  property = "appendixlist"  label = "label"  value = "value" /></html:select>
					</td>	
				 </tr>
				</logic:equal>
				
				<logic:notEqual name="ReportScheduleWindowForm" property="fromPage" value="dispatch">
					<html:hidden property="msa" />
					<html:hidden property="appendix" />
				</logic:notEqual>
				
				<tr>
		   			 <td width="80" class="labelobold"><bean:message bundle="PRM" key="prm.reportschedule.startdate"/></td>
		   			 <td class="labelo" width="200"><html:text  styleClass="textbox" size="10" property="startdate" readonly="true"/>
		   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].startdate, document.forms[0].startdate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
		   			 <td width="280"></td>
				</tr>
				<tr> 
				    <td class="labelebold"><bean:message bundle="PRM" key="prm.reportschedule.enddate"/></td>
				    <td class="labele"><html:text  styleClass="textbox" size="10" property="enddate" readonly="true"/>
					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].enddate, document.forms[0].enddate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
				</tr>
				
				<tr> 
		 		 	  <td width="60" class="labelobold">Context</td>
		 		 	  <td class="labelobold"> 
		 		 	  	<html:checkbox property="dateOnSite" value="OS">On Site</html:checkbox>&nbsp;&nbsp;&nbsp;
		 		 	  	<html:checkbox property="dateOffSite" value="FS">Off Site</html:checkbox>&nbsp;&nbsp;&nbsp;
		 		 	  	<html:checkbox property="dateComplete" value="CO">Complete</html:checkbox>&nbsp;&nbsp;&nbsp;
		 		 	  	<html:checkbox property="dateClosed" value="CL">Closed</html:checkbox>&nbsp;&nbsp;
		 		 	 </td>
				</tr>
				
				 <tr> 
				    <td width="60" class="labelebold" ><bean:message bundle="PRM" key="prm.reportschedule.msp"/></td>
				    <td  align=left class="labele">
						<html:select property="msp"  styleClass="comboe" >
						<html:optionsCollection name = "mpcombo"  property = "yesnolist"  label = "label"  value = "value" /></html:select>
					</td>	
			 	</tr>
				 
				 <tr> 
		 		 	  <td colspan="2" class="labelobold"> 
		 		 	  <html:radio property="version" value="I"><bean:message bundle="PRM" key="prm.reportschedule.internalversion" /></html:radio>&nbsp;&nbsp;&nbsp;
					  <html:radio property="version" value="E"><bean:message bundle="PRM" key="prm.reportschedule.externalversion" /></html:radio></td>
				</tr>
					  <tr>
						  <td colspan="2" class="buttonrow">
			 		      	<html:submit property="submitreport" styleClass="button" onclick="return submitpage();">
			 		      	<bean:message bundle="PRM" key="prm.reportschedule.submitreport" /></html:submit>
		     			  </td>
					  </tr>
	  		 </logic:equal>
	  		 
	  		 <%--Start:Added By Vishal 07/01/07 --%>
	  		 <logic:equal name="ReportScheduleWindowForm" property="report_type" value="appendixCsvSummary">
	  		 	<tr> 
		 		 	  <td colspan="2" class="labelobold"> 
		 		 	 	 <html:radio property="version" value="I"><bean:message bundle="PRM" key="prm.reportschedule.internalversion" /></html:radio>&nbsp;&nbsp;&nbsp;
					  	<html:radio property="version" value="E"><bean:message bundle="PRM" key="prm.reportschedule.externalversion" /></html:radio>
					  </td>
				</tr>
				<tr>
					  <td colspan="2" class="buttonrow">
		 		      	<html:submit property="submitreport" styleClass="button" onclick="javascript:submitpage();">
		 		      	<bean:message bundle="PRM" key="prm.reportschedule.submitreport" /></html:submit>
	     			  </td>
				 </tr>
					  
			</logic:equal>
	  		 <%--End: --%> 		 
	  		<bean:define id="rtype" name="ReportScheduleWindowForm" property="report_type" scope = "request" />
	  		 
	  		<%if(!rtype.equals("CSVSummary")&&(!rtype.equals("appendixCsvSummary"))) 
	  		{%>
	  		<tr>
	   			 <td  width="100" class="labelobold"><bean:message bundle="PRM" key="prm.reportschedule.startdate"/></td>
	   			 <td class="labelo" width="110"><html:text  styleClass="textbox" size="10" property="startdate" readonly="true"/>
	   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].startdate, document.forms[0].startdate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
  			    <td width="320"></td>
			</tr>
			<tr> 
			    <td  class="labelebold"><bean:message bundle="PRM" key="prm.reportschedule.enddate"/></td>
			    <td class="labele"><html:text  styleClass="textbox" size="10" property="enddate" readonly="true"/>
				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].enddate, document.forms[0].enddate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
				<td width="320"></td>
			</tr>
	 		
	 		 <tr> 
			    <td  class="labelobold" height="25"><bean:message bundle="PRM" key="prm.reportschedule.criticality"/></td>
			    <td  align=left class="labelo">
				<html:select property="criticality"  styleClass="comboo" multiple = "true">
					<html:optionsCollection name = "critcombo"  property = "criticalitylist"  label = "label"  value = "value" /></html:select></td>	
			 </tr>
			 
			 <tr> 
			    <td  class="labelebold" ><bean:message bundle="PRM" key="prm.reportschedule.status"/></td>
			    <td  align=left class="labele">
				<html:select property="status"  styleClass="comboe" >
					<html:optionsCollection name = "stcombo"  property = "jobstatuslist"  label = "label"  value = "value" /></html:select></td>	
			 </tr>
			 
			  <tr> 
			    <td  class="labelobold" ><bean:message bundle="PRM" key="prm.reportschedule.requestor"/></td>
			    <td  align=left class="labelo">
				<html:select property="requestor"  styleClass="comboo" >
					<html:optionsCollection name = "rqcombo"  property = "requestorlist"  label = "label"  value = "value" /></html:select></td>	
			 </tr>
			 
			  <tr> 
			    <td  class="labelebold" ><bean:message bundle="PRM" key="prm.reportschedule.arrivaloption"/></td>
			    <td  align=left class="labele">
				<html:select property="critdesc"  styleClass="comboe" >
					<html:optionsCollection name = "cdcombo"  property = "critdesclist"  label = "label"  value = "value" /></html:select></td>	
			 </tr>
			 
			 
			 <tr> 
			    <td  class="labelobold" ><bean:message bundle="PRM" key="prm.reportschedule.msp"/></td>
			    <td  align=left class="labelo">
				<html:select property="msp"  styleClass="comboo" >
					<html:optionsCollection name = "mpcombo"  property = "yesnolist"  label = "label"  value = "value" /></html:select></td>	
			 </tr>
			 
			 <tr> 
			    <td  class="labelebold" ><bean:message bundle="PRM" key="prm.reportschedule.helpdesk"/></td>
			    <td  align=left class="labele">
				<html:select property="helpdesk"  styleClass="comboe" >
					<html:optionsCollection name = "hdcombo"  property = "yesnolist"  label = "label"  value = "value" /></html:select></td>	
			 </tr>

	 		 <tr> 
	 		 	  <td colspan="2" class="buttonrow"> 
	 		      <html:submit property="save" styleClass="button" onclick = "return validate();">
	 		      <bean:message bundle="PRM" key="prm.reportschedule.viewreport" /></html:submit>
     			  <html:reset  property="reset" styleClass="button"><bean:message bundle="PRM" key="prm.reportschedule.reset" /></html:reset>
				  <!--<html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>-->
				  </td>
				  <td></td>
			 </tr>
	  <% } %>
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
</html:form>
</table>
</body>
<script>
function validate()
{
			if(document.forms[0].startdate.value == "") 
 			{
				alert("<bean:message bundle="PRM" key="prm.reportschedule.selectstartdate"/>");	 			
 				document.forms[0].startdate.focus();
 				return false;
 			}
 		
	 		if(document.forms[0].enddate.value == "") 
	 		{
				alert("<bean:message bundle="PRM" key="prm.schedule.selectenddate"/>");	 			
	 			document.forms[0].enddate.focus();
	 			return false;
	 		}
		
	
}

function Backaction()
{
	<%//if(request.getParameter("type").equals("P"))
	        //  {%> 
	          document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getParameter("typeid") %>&function=view";
			  document.forms[0].submit();
			  return true;
	         
		  <%//} %>
		  
	 <%//if(request.getParameter("type").equals("J")||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ"))
        //  {%>
          	  document.forms[0].action = "JobDashboardAction.do?jobid=<%=request.getParameter( "typeid" ) %>";
			  document.forms[0].submit();
			  return true; 
	        
		 <% //}%>
	
} 

function appendixPopulate()
{
	
	document.forms[0].typeid.value=0;
	document.forms[0].appendix.value=0;	
	//document.forms[0].submitreport.disabled=true;
	document.forms[0].submit();	
	return true; 
}

function setAppendix()
{
	//document.forms[0].submitreport.disabled=false;
	document.forms[0].typeid.value=document.forms[0].appendix.value;
	return true; 
	
}
<%--Start:Added by vishal 04/01/2007--%>
function submitpage()
{
		
		
		if(document.forms[0].appendix.value == "0") {
		alert("Appendix is required !");
		document.forms[0].appendix.focus();
		return false;
		}
		
		if(document.forms[0].startdate.value!="" && document.forms[0].enddate.value!="") 
		{
			if (!compDate_mdy(document.forms[0].startdate.value, document.forms[0].enddate.value)) 
			{
				alert("End date should be greater than start date");
				document.forms[0].enddate.focus();
				return false;
			} 
		}
	
		document.forms[0].submit();	
		return true;
	}
<%--End:--%>
</script>
</html:html>