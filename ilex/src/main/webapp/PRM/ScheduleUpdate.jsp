<!DOCTYPE HTML>

<% 
response.setHeader("pragma","no-cache");//HTTP 1.1 
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Cache-Control","no-store"); 
response.addDateHeader("Expires", -1); 
response.setDateHeader("max-age", 0); 
response.setIntHeader ("Expires", -1); //prevents caching at the proxy server 
response.addHeader("cache-Control", "private"); 
%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>
<html:html>

<head>
	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language="JavaScript" src="javascript/prototype.js"></script>
</head>

<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());

%>

<%@ include  file="/NMenu.inc" %>
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');leftAdjLayers();" >
<html:form action="ScheduleUpdate" >
<html:hidden property="function" />
<html:hidden property="type" />
<html:hidden property="typeid" />
<html:hidden property="page" />
<html:hidden property="pageid" />
<html:hidden property="appendixname" />
<html:hidden property="jobname" />
<html:hidden property="nettype" />
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property="checknetmedxtype" /> 
<html:hidden property="scheduleid" />
<html:hidden property="jobOwnerOtherCheck"/>
<input type="hidden" name="tabId" value="<c:out value='${param.tabId}'/>"  />

<c:if test="${param.throughJobDashboard ne 'Y'}">
<logic:equal name="ScheduleUpdateForm" property="page" value = "appendixdashboard">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AppendixDashboardMenuScript.inc" %>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
	 					<td colspan="4"><h2>
	 						<logic:equal name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
		  						<bean:message bundle="PRM" key="prm.schedule.updateddisschedulefor"/>&nbsp;<bean:message bundle="PRM" key="prm.schedule.appendix"/>:&nbsp;<bean:write name="ScheduleUpdateForm" property="appendixname" />
		  					</logic:equal>
		  			
		  					<logic:notEqual name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
			  					<bean:message bundle="PRM" key="prm.schedule.updatedschedulefor"/>&nbsp;<bean:message bundle="PRM" key="prm.schedule.appendix"/>:&nbsp;<bean:write name="ScheduleUpdateForm" property="appendixname" />
							</logic:notEqual>  				
			  				<%
			  				if(request.getParameter("type").equals("J")||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ")) { 
					  		%>
					  		<bean:message bundle="PRM" key="prm.schedule.arrowmark"/><bean:message bundle="PRM" key="prm.schedule.job"/><bean:write name="ScheduleUpdateForm" property="jobname" />
					  		<% } %>
						</h2></td>
	 				</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="ScheduleUpdateForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="ScheduleUpdateForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="ScheduleUpdateForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					           <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>

 </table>
	
</logic:equal>

<logic:notEqual name="ScheduleUpdateForm" property="page" value = "appendixdashboard">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
			<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
			<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>

					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
	</tr>
	<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
	         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
							 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
								<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
								  <a><span class="breadCrumb1">Schedule</a>
	    </td>
	</tr>
       <tr><td height="5" >&nbsp;</td></tr>	
</table>
<table>
<tr>
<td><h2>
	
  </h2>		
 </td>
 </tr> 		
</table>  
<c:if test = "${requestScope.jobType eq 'Addendum'}">
	<%@ include  file="/AddendumMenuScript.inc" %>
</c:if>
<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
	<%@ include  file="/ProjectJobMenuScript.inc" %>
</c:if>			
</logic:notEqual>
</c:if> 
<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="750">
	  		<tr>
	  			<logic:present name = "ScheduleUpdateForm" property="updatemessage">
					<logic:equal name="ScheduleUpdateForm" property="updatemessage" value="-9001">
						<td  colspan="4" class="message" height="30" ><bean:message bundle="PRM" key="prm.schedule.updatefailed"/></td>
			 		</logic:equal>
		 		</logic:present>
			</tr>
	  		
	  		<!--  	<tr>
	  			<td colspan="4" class="labeleboldwhite" height="30">
		  			<logic:equal name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
		  				<bean:message bundle="PRM" key="prm.schedule.updateddisschedulefor"/>&nbsp;<bean:message bundle="PRM" key="prm.schedule.appendix"/>:&nbsp;<bean:write name="ScheduleUpdateForm" property="appendixname" />
		  			</logic:equal>
		  			
		  			<logic:notEqual name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
			  			<bean:message bundle="PRM" key="prm.schedule.updatedschedulefor"/>&nbsp;<bean:message bundle="PRM" key="prm.schedule.appendix"/>:&nbsp;<bean:write name="ScheduleUpdateForm" property="appendixname" />
					</logic:notEqual>  				
	  				<%
	  				if(request.getParameter("type").equals("J")||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ")) { 
			  		%>
			  		<bean:message bundle="PRM" key="prm.schedule.arrowmark"/><bean:message bundle="PRM" key="prm.schedule.job"/><bean:write name="ScheduleUpdateForm" property="jobname" />
			  		<% } %>
	  			</td>
	  		</tr>-->
	  		 
	  		<tr>
	   			 <td  width="100" class="labelobold">
	   			 	<logic:equal name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
	   			 		<bean:message bundle="PRM" key="prm.schedule.schedulearrival"/>
	   			 	</logic:equal>
	   			 	
	   			 	<logic:notEqual name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
	   			 		<bean:message bundle="PRM" key="prm.schedule.scheduledstart"/>
	   			 	</logic:notEqual>
	   			 	
	   			 	
	   			 
	   			 
	   			 </td>
	   			 <td class="labelo" width="110"><html:text  styleClass="textbox" size="10" property="startdate" onfocus="onDateChange();" readonly="true"/>
	   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].startdate, document.forms[0].startdate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
	   			 	<html:hidden name="ScheduleUpdateForm" property="currentSchDate"/>
	   			 </td>
  				    <td class = "labelo" width="180"> 
				    <html:select name="ScheduleUpdateForm" property = "startdatehh" styleClass="comboo" onchange="showReshecule();">  
					<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
					</html:select>
					<html:select name="ScheduleUpdateForm" property = "startdatemm" styleClass="comboo" onchange="showReshecule();">  
					<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
					</html:select>
					<html:select name="ScheduleUpdateForm" property = "startdateop" styleClass="comboo" onchange="showReshecule();">   
					<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
					</html:select>
				    </td>
				    <td width="320"></td>
			</tr>
	 		<tr id="hideTR1" style="display: none;">
				<td colspan="5" style="padding: 0px 0px 0px 65px"><span id="reschedule"></span></td>
			</tr>
			
	    	 <logic:equal name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
	    	 	<html:hidden property = "plannedenddate" />
	    	 	<html:hidden property = "plannedenddatehh" />
	    	 	<html:hidden property = "plannedenddatemm" />
	    	 	<html:hidden property = "plannedenddateop" />
	      	 </logic:equal>
	    	 
	    	  <logic:notEqual name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
		    	<tr> 
				    <td  class="labelebold"><bean:message bundle="PRM" key="prm.schedule.scheduledend"/></td>
				    <td class="labele"><html:text  styleClass="textbox" size="10" property="plannedenddate" readonly="true"/>
					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].plannedenddate, document.forms[0].plannedenddate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
						    <td class = "labele"> 
						    <html:select name="ScheduleUpdateForm" property = "plannedenddatehh" styleClass="comboe">  
							<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
							</html:select>
							<html:select name="ScheduleUpdateForm" property = "plannedenddatemm" styleClass="comboe">  
							<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
							</html:select>
							<html:select name="ScheduleUpdateForm" property = "plannedenddateop" styleClass="comboe">  
							<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
							</html:select>
						    </td>
						    <td></td>
				</tr>
			</logic:notEqual>
			
			<tr>
	   			 <td  class="labelobold">
	   			 	<logic:equal name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
	   			 		<bean:message bundle="PRM" key="prm.schedule.actualarrival"/>
	   			 	</logic:equal>
	   			 	
	   			 	<logic:notEqual name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
	   			 		<bean:message bundle="PRM" key="prm.schedule.actualstart"/>
	   			 	</logic:notEqual>	
	   			 		
	   			 		
	   			 
	   			 
	   			 </td>
	   			 <td class="labelo"><html:text  styleClass="textbox" size="10" property="actstartdate" readonly="true"/>
	   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].actstartdate, document.forms[0].actstartdate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
	  	
					    <td class = "labelo"> 
					    <html:select name="ScheduleUpdateForm" property = "actstartdatehh" styleClass="comboo">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "actstartdatemm" styleClass="comboo">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "actstartdateop" styleClass="comboo">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
				 <td></td>
			</tr>
	 
	    	<tr> 
			    <td  class="labelebold">
			    	<logic:equal name="ScheduleUpdateForm" property="appendixname" value = "NetMedX">
			    		<bean:message bundle="PRM" key="prm.schedule.departure"/>
			    	</logic:equal>
			    	
			    	
			    	<logic:notEqual name="ScheduleUpdateForm" property="appendixname" value = "NetMedX">
			    		<bean:message bundle="PRM" key="prm.schedule.actualend"/>
			    	</logic:notEqual>
			    	
			    	
			    
			    
			    </td>
			    <td class="labele"><html:text  styleClass="textbox" size="10" property="actenddate" readonly="true"/>
				<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].actenddate, document.forms[0].actenddate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
				
					    <td class = "labele"> 
					    <html:select name="ScheduleUpdateForm" property = "actenddatehh" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "actenddatemm" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="ScheduleUpdateForm" property = "actenddateop" styleClass="comboe">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
				<td></td>
			</tr>
	
			
	  		 <logic:equal name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
	  		 	<html:hidden property="totaldays"/>
	  		 </logic:equal>
	  		 <%--
	  		 <logic:notEqual name="ScheduleUpdateForm" property="checknetmedxtype" value = "1">
		  		 <tr> 
		    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.schedule.noofdays"/></td>
		   			 <td  class="labelo" colspan="2"><html:text  styleClass="textbox" size="5" property="totaldays"/></td>
		   			 <td></td>
		 		 </tr>
		 	</logic:notEqual>
		 	--%>
	 		 <html:hidden property="totaldays"/>
	 		
	  
	 		 <tr> 
	 		 	  <td colspan="3" class="buttonrow"> 
	 		      <html:submit property="save" styleClass="button" onclick = "return validate();" >
	 		      <bean:message bundle="PRM" key="prm.save" /></html:submit>
     			  <html:reset  property="reset" styleClass="button" onclick="hideReReshecule();" ><bean:message bundle="PRM" key="prm.reset" /></html:reset>
     				 <c:if test="${param.throughJobDashboard ne 'Y'}">
     			  <%
     				  if(request.getParameter("from") != null && request.getParameter("from").equals("RPT")){%> 
     			  }
     			  <html:button property="back" styleClass="button" onclick = "history.go(-1);"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
     			  <%}else{ %>
				  <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  <%} 
				  %>
				  </c:if>
				  </td>
				  <td></td>
			 </tr>
	  
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
</html:form>
</table>
</body>

<script>
/* These function is used for Reschedule Jobs */
/* Call Ajax for Reschedule: Start*/
var rseCheck = false;
function onDateChange(){
	if(document.forms[0].currentSchDate.value != document.forms[0].startdate.value){
		showReshecule();
	}
}
function hideReReshecule(){	
	getResponse('ScheduleUpdate.do?function=Reschedule&hide=hide');
	document.getElementById("hideTR1").style.display = 'none';
	rseCheck = false;
}

function showReshecule(){
	if(rseCheck == false){
		var scheduledDate = document.forms[0].startdate.value;
		var scheduledHH = document.forms[0].startdatehh.value;
		var scheduledMM = document.forms[0].startdatemm.value;
		if(scheduledHH == '  ') {scheduledHH = '00';}
		var scheduledAAA = document.forms[0].startdateop.value;
		var jobId = document.forms[0].typeid.value;
		getResponse('ScheduleUpdate.do?function=Reschedule&hide=show&date='+scheduledDate+'&hh='+scheduledHH+'&mm='+scheduledMM+'&aa='+scheduledAAA+'&jobId='+jobId);
	}
}

function getResponse(partnerURL){
/* This function call Ajax which is inmplemented into /javascript/prototype.js */	 
	var url = partnerURL;
	var pars = '';		
	var myAjax = new Ajax.Updater({success:'reschedule'},url,{method:'get', parameters:pars, onSuccess:disableStatus, onFailure:disableStatus, evalScripts: true});	
	//document.getElementById("hideTR1").style.display = '';
	rseCheck = true;
}
function disableStatus(res){
	document.getElementById("reschedule").innerHTML = res.response.Text;
}		
</script>

<script>
function validate()
{
		if(document.forms[0].startdatehh.value != "  ")
 		{
 			if(document.forms[0].startdate.value == "") {
				alert("<bean:message bundle="PRM" key="prm.schedule.selectstartdate"/>");	 			
 				document.forms[0].startdate.focus();
 				return false;
 			}
 		}
 		
 		if(document.forms[0].checknetmedxtype.value != "1")  {
 			if(document.forms[0].plannedenddatehh.value != "  ")
	 		{
	 			if(document.forms[0].plannedenddate.value == "") {
		 			alert("<bean:message bundle="PRM" key="prm.schedule.selectplannedenddate"/>");	 			
	 				document.forms[0].plannedenddate.focus();
	 				return false;
	 			}
	 		}
 		}
 		if(document.forms[0].actstartdatehh.value != "  ")
 		{
 			if(document.forms[0].actstartdate.value == "") {
				alert("<bean:message bundle="PRM" key="prm.schedule.selectactualstartdate"/>");	 			
 				document.forms[0].actstartdate.focus();
 				return false;
 			}
 		}
 		
 		if(document.forms[0].actenddatehh.value != "  ")
 		{
 			if(document.forms[0].actenddate.value == "") {
				alert("<bean:message bundle="PRM" key="prm.schedule.selectactualenddate"/>");	 			
 				document.forms[0].actenddate.focus();
 				return false;
 			}
 		}	
	
	if(document.forms[0].startdate.value!="" && document.forms[0].plannedenddate.value!="") 
	{
		if (!compDate_mdy(document.forms[0].startdate.value, document.forms[0].plannedenddate.value)) 
		{
							//alert("22222222222222");
			document.forms[0].plannedenddate.focus();
			alert("<bean:message bundle = "PRM" key = "prm.datevalidation"/>");
			return false;
		} 
	}
	
	
	if(document.forms[0].actstartdate.value!="" && document.forms[0].actenddate.value!="") 
	{
		if (!compDate_mdy(document.forms[0].actstartdate.value, document.forms[0].actenddate.value)) 
		{
			document.forms[0].actenddate.focus();
			alert("<bean:message bundle = "PRM" key = "prm.actualdatevalidation"/>");
			return false;
		} 
	}
	
	/* For Job Reschedule: Start */
	try{
		return validateRse(); // - for validation of reschedule.
	}catch(e){}
	/* For Job Reschedule: Start */
	
	document.forms[0].method= 'get';
	openSchedulepage();
}
function openSchedulepage()
	{
		var url = "ScheduleUpdate.do?function=update&type=J&typeid=<%=request.getParameter("typeid") %>&page=jobdashboard&pageid=<%=request.getParameter("pageid") %>&save=Save&tabId=7";
		document.forms[0].action = url;
		document.forms[0].submit();
	}
function Backaction()
{
	<%if(request.getParameter("type").equals("P"))
	          {%> 
	          document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getParameter("typeid") %>&function=view";
			  document.forms[0].submit();
			  return true;
	         
		  <%} %>
		  <%if(request.getParameter("type").equals("J")||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ"))
          {
	        if(request.getParameter("page").equals("appendixdashboard"))
          	{%>
          	if(document.forms[0].nettype.value == 'dispatch')
	          	document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	        else
	        	document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getParameter("pageid") %>&function=view&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	        //	document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getParameter("pageid") %>&function=view&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
			document.forms[0].submit();
			return true; 
          	
		  	<%} 
			else
			{%>
			document.forms[0].action = "JobDashboardAction.do?jobid=<%=request.getParameter( "typeid" ) %>";
			document.forms[0].submit();
			return true; 
			
		  	<%} 
		  }%>
		  
		  <%if(request.getParameter("type").equals("A")||request.getParameter("type").equals("AA")||request.getParameter("type").equals("CA")||request.getParameter("type").equals("IA"))
          {
          	if(request.getParameter("page").equals("jobdashboard"))
          	{%> 
         	document.forms[0].action = "JobHeader.do?jobid=<%=request.getParameter("pageid") %>&function=view";
			document.forms[0].submit();
			return true; 
         	
		  	<%} 
			else
			{%>
			document.forms[0].action = "ActivityHeader.do?activityid=<%=request.getParameter("typeid") %>&function=view";
			document.forms[0].submit();
			return true;
				
			<%}
		  }%>
	
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="ScheduleUpdate.do?showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="ScheduleUpdate.do?resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	if(document.forms[0].type.value=='IJ' || document.forms[0].type.value=='J')
		id = document.forms[0].pageid.value;
	else
		id = document.forms[0].typeid.value;
 	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>


</html:html>
