<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<html:html>
<HEAD>
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='./dwr/interface/JobSetUpDao.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>

</HEAD>
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>

<bean:define id="dyComboPRMOwner" name="dyComboPRMOwner"  scope="request"/>

<html:form action="AssignOwner">

<html:hidden property="jobId"/>
<html:hidden property="viewjobtype"/>
<html:hidden property="nettype"/>
<html:hidden property="appendix_Id"/>
<html:hidden property="function"/>
<html:hidden property="changeFilter"/>
<html:hidden property="fromPage"/>

<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
					<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
					<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>

							<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
									 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
									 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
									  <a><span class="breadCrumb1">Team</a>
			    </td>
			</tr>
	</table>

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>  <td width="2" height="0"></td>
  			 <td>
  			 	<table border = "0" cellspacing = "2" cellpadding = "2" width = "500"> 
					<tr>    
					
						<td colspan = "10" class = "labeleboldwhite" height = "30" >
						<logic:notEqual name="AssignOwnerForm" property="from" value = "ownerforaappendix">
								<bean:message bundle = "PRM" key = "prm.appendix.ownerAssignment"/>
						</logic:notEqual> 
						<logic:equal name="AssignOwnerForm" property="from" value = "ownerforaappendix"> 
								<bean:message bundle = "PRM" key = "prm.appendix.selectowner"/>
						</logic:equal>
						</td>

					</tr> 
					
					
					<tr>
					<logic:notEqual name="AssignOwnerForm" property="from" value = "ownerforaappendix">
					 <td  class = "labelobold"><bean:message bundle = "PRM" key = "prm.appendix.ownerName"/></td>
					    <td colspan = 4 class = "labelo"><html:text property = "ownerName" styleClass = "textbox" size="20" readonly="true"/>&nbsp;&nbsp;
					 </td></tr>
					  </logic:notEqual>  
					 <tr>   
					    <td class = "labelebold">
					    <logic:notEqual name="AssignOwnerForm" property="from" value = "ownerforaappendix">
							    <bean:message bundle = "PRM" key = "prm.appendix.newOwnerName"/>
					    </logic:notEqual>
					    <logic:equal name="AssignOwnerForm" property="from" value = "ownerforaappendix"> 
								<bean:message bundle = "PRM" key = "prm.appendix.ownerName"/>
						</logic:equal>
					    </td>
					    <td colspan = 4 class = "labele">
					    <logic:notEqual name="AssignOwnerForm" property="from" value = "ownerforaappendix">
						    <html:select property = "newOwner" size = "1" styleClass = "labele">
							<html:optionsCollection name = "dyComboPRMOwner" property = "allOwnerName" value = "value" label = "label"/>    
							</html:select>
						</logic:notEqual>
						
						 <logic:equal name="AssignOwnerForm" property="from" value = "ownerforaappendix">
						    <html:select property = "newOwner" size = "1" styleClass = "labele">
							<html:optionsCollection name = "dyComboPRMOwner" property = "allOwnerName" value = "value" label = "label"/>    
							</html:select>
						</logic:equal>
						
						
					    </td>
						
  					</tr>
  					
  					<!-- For New Scheduler: Start -->
  					<logic:notEqual name="AssignOwnerForm" property="from" value = "ownerforaappendix">
  					<tr>
  						<td  class = "labelobold"><bean:message bundle = "PRM" key = "prm.appendix.schedulerName"/></td>
					    <td colspan = 4 class = "labelo">
					    	<html:text property = "currentScheduler" styleClass = "textbox" size="20" readonly="true"/> 
					    </td>
  					</tr>
  					
  					<tr>
  						<td  class = "labelebold"><bean:message bundle = "PRM" key = "prm.appendix.newSchedulerName"/></td>
					    <td colspan = 4 class = "labele">
					    	<html:select name="AssignOwnerForm" property="newScheduler" styleClass="labele">
					    		<html:optionsCollection name = "AssignOwnerForm" property = "schedulerList" value = "value" label = "label"/>    
					    	</html:select> 
					    </td>
  					</tr>
  					</logic:notEqual>
  					<!-- For New Scheduler: End -->
  					
  					<tr>
		  				<td colspan = "8" >
		  				<logic:notEqual name="AssignOwnerForm" property="from" value = "ownerforaappendix">
							<html:submit property = "save" styleClass = "button"><bean:message bundle = "PRM" key = "prm.save"/></html:submit>	
							<html:button property = "back"  styleClass="button" onclick = "javascript:history.go(-1);"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
						</logic:notEqual>
						<logic:equal name="AssignOwnerForm" property="from" value = "ownerforaappendix">
						<html:button property = "submit1" onclick="javascript:onsubmitpage();" styleClass = "button"><bean:message bundle = "PRM" key = "prm.submit"/></html:button>	
					    <html:button property = "close"  styleClass="button" onclick = "javascript:closefunction();"><bean:message bundle="PRM" key="prm.button.close"/></html:button>
						</logic:equal>
							</td>
						</tr>
  				
  					

				</table>
			</td>
		</tr>
</table>
<table>
	<tr>
		<td><html:text name = "AssignOwnerForm" styleClass="textboxdummy" size="1" readonly="true" property="textboxdummy" /></td>
	</tr>
</table>
	<c:if test = "${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob'}">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
	
</html:form>


<script>
function closefunction()
{
	window.close();
}

function onsubmitpage()
{
	if( document.forms[0].newOwner.value == "0" )
	{
		alert( "<bean:message bundle = "PRM" key = "prm.add.pleaseselectowner"/>" );
		document.forms[0].newOwner.focus();
		return false;
	}
	else
	{
		document.forms[0].action = "AppendixHeader.do?function=view&fromjobofaOwner=showmyjob&changeFilter=<bean:write name = "AssignOwnerForm" property = "changeFilter" />&viewjobtype=<bean:write name = "AssignOwnerForm" property = "viewjobtype" />&nettype=<bean:write name = "AssignOwnerForm" property = "nettype" />&appendixid=<bean:write name = "AssignOwnerForm" property = "appendix_Id" />&ownerId="+document.forms[0].newOwner.value;
		document.forms[0].target = "ilexmain";
		document.forms[0].submit();
		window.close();
		return true;	
	}
}

</script>

</body>
</html:html>


