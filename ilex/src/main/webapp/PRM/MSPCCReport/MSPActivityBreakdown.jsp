<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>

 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import = "com.mind.MSP.business.*, java.util.*, java.lang.*" %>

<title>Insert title here</title>
<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<%
ArrayList enabledMsaList = (ArrayList)request.getAttribute("mspReportEnabledMSAs");
ArrayList enabledAppendixList = (ArrayList)request.getAttribute("mspReportEnabledAppendixes");
%>						
						
</head>
<script>
var customizeView = false;
	function toggleMenuSection(unique, titleId,name) {
		var divid="div_"+unique;
		thisDiv = document.getElementById(divid);
		try{
		if (thisDiv==null) { return ; }
		}catch(e){
		}
		action = "toggleType = toggleDiv('div_" + unique + "', titleId, name);";
		eval(action);
		action = "thisImage = document.getElementById('img_" + unique + "');";
		eval(action);
		if (document.getElementById('div_' + unique).offsetHeight > 0) {
			thisImage.src = "images/Collapse.gif";
		}
		else {
			thisImage.src = "images/Expand.gif";
		}
	}
	function toggleDiv(divId, titleId,name) {
		var div  = document.getElementById(divId);
		var tableObj = document.getElementById(divId);
		if (div.offsetHeight > 0) {
			div.style.display = "none";
		} else {
			if(customizeView) {
				setChickFilAjaxMarker(titleId, div,name);
			} else {
				setAjaxMarker(titleId, div,name);
			}
			div.style.display = "block";
		}
	}
	
	function setAjaxMarker(titleId, div,name){
		$.ajax({
		    type: "post",
		    dataType: "json",
		    url: "MSPCCReportAction.do?hmode=getSectonBInfo&appendixId=<%=request.getAttribute("appendixid")%>&titleId="+titleId+"&name="+name+"&timestamp="+(new Date()*1),
		    async: false,
		    timeout: 10000,
		    error: function() {},
		    success: function(data) {setMarker(data, div,name)}
		});
	}


	function setChickFilAjaxMarker(titleId, div,name){
		$.ajax({
		    type: "post",
		    dataType: "json",
		    url: "MSPCCReportAction.do?hmode=getSectonBInfo&appendixId=<%=request.getAttribute("appendixid")%>&titleId="+titleId+"&name="+name+"&timestamp="+(new Date()*1),
		    async: false,
		    timeout: 10000,
		    error: function() {},
		    success: function(data) {setChickFilMarker(data, div,name)}
		});
	}
	
	function setChickFilMarker(data, div,name){
		var divRowsLength = div.rows.length;
		if(divRowsLength  > 1) {
			for(var i=1; i < div.rows.length; ) {
				div.deleteRow(i);
			}
		}
		var jobTitle = '';
		var isdataFound = false;
		$.each(data, function(i, j){
			    var tr = div.insertRow();
			    var td1 = tr.insertCell(0);

			    td1.innerHTML ="<a href='JobDashboardAction.do?appendix_Id=<%=request.getAttribute("appendixid")%>&Job_Id="+j.JobId+"'>"+ j.Name+"</a>";
			    td1.className = 'Nhyperevenfont';
			    var td2 = tr.insertCell(1);
			    td2.innerHTML = j.col0;
			    td2.className = 'Nhyperevenfont';
			    
			    var td3 = tr.insertCell(2);
			    td3.innerHTML = j.col1;
			    td3.className = 'Nhyperevenfont';

				if(name != 'Pre-Qualification of Customer Sites (Provisioning).')
				{
				    var td4 = tr.insertCell(3);
				    td4.innerHTML = j.col2;
				    td4.className = 'Nhyperevenfont';
				    var td5 = tr.insertCell(4);
				} else {
					 var td5 = tr.insertCell(3);
				}
				 
				 
			    if(j.Actions != '' && j.Actions.length > 35) {
			    	jobTitle = j.Actions.substring(0,35);
			    } else {
			    	jobTitle = j.Actions;
			    }

			    if(name=="Build Board for Store and Ship (Logistics). Enter shipping number in ticket"){
			    	td5.innerHTML = '<a href="javascript:callCheckListData('+j.JobId+',\''+j.Name+'\','+div.id+','+i+')">'+jobTitle+'</a>'+' | <a href="javascript:shippingInstructionData('+j.JobId+',\''+j.Name+'\','+div.id+','+i+')">Shipping Insturctions</a>';
			    }else{
			    	td5.innerHTML = '<a href="javascript:callCheckListData('+j.JobId+',\''+j.Name+'\','+div.id+','+i+')">'+jobTitle+'</a>';
				    }
			    td5.className = 'Nhyperevenfont';
			    isdataFound = true;
			});
		if(isdataFound == false) {
			var tr = div.insertRow();
		    var td0 = tr.insertCell(0);
		    td0.innerHTML ="No Data Available.";
		    td0.className = 'messagesmallfont';
		    if(name != 'Pre-Qualification of Customer Sites (Provisioning).')
			{
		    	td0.colspan = "4";
			} else {
		    	td0.colspan = "5";
			}
		}
	}


	
	function setMarker(data, div,name){		
		var divRowsLength = div.rows.length;
		if(divRowsLength  > 1) {
			for(var i=1; i < div.rows.length; ) {
				div.deleteRow(i);
			}
		}
		var jobTitle = '';
		var isdataFound = false;
		$.each(data, function(i, j){
			    var tr = div.insertRow();
			    var td1 = tr.insertCell(0);

			    td1.innerHTML ="<a href='JobDashboardAction.do?appendix_Id=<%=request.getAttribute("appendixid")%>&Job_Id="+j.JobId+"'>"+ j.Name+"</a>";
			    td1.className = 'Nhyperevenfont';
			    
			    var td2 = tr.insertCell(1);
			    td2.innerHTML = j.Date;
			    td2.className = 'Nhyperevenfont';
			    
			    var td3 = tr.insertCell(2);
			    td3.innerHTML = j.JobOwner;
			    td3.className = 'Nhyperevenfont';
			    
			    var td4 = tr.insertCell(3);
			    td4.innerHTML = j.ErrorCondition;
			    td4.className = 'Nhyperevenfont';
			    
			    var td5 = tr.insertCell(4);
			    if(j.Actions != '' && j.Actions.length > 35) {
			    	jobTitle = j.Actions.substring(0,35);
			    } else {
			    	jobTitle = j.Actions;
			    }

			    if(name=="Build Board for Store and Ship (Logistics). Enter shipping number in ticket"){
			    	td5.innerHTML = '<a href="javascript:callCheckListData('+j.JobId+',\''+j.Name+'\','+div.id+','+i+')">'+jobTitle+'</a>'+' | <a href="javascript:shippingInstructionData('+j.JobId+',\''+j.Name+'\','+div.id+','+i+')">Shipping Insturctions</a>';
			    }else{
			    	td5.innerHTML = '<a href="javascript:callCheckListData('+j.JobId+',\''+j.Name+'\','+div.id+','+i+')">'+jobTitle+'</a>';
				    }
			    td5.className = 'Nhyperevenfont';
			    isdataFound = true;
			});
		if(isdataFound == false) {
			var tr = div.insertRow();
		    var td0 = tr.insertCell(0);
		    td0.innerHTML ="No Data Available.";
		    td0.className = 'messagesmallfont';
		    td0.colspan = "5";
		}
	}
			
	function callCheckListData(jobId,jobName,div,i) {
		blockUnblockUI();
		i = $('#'+div.id+' tr' + ' td :contains(' + jobName  + ')').closest('tr').prevAll().length;
		var url = 'ManageCheckListDataAction.do?appendixId=<%=request.getAttribute("appendixid")%>&jobId='+jobId+'&div='+div.id+'&rowNumber='+i;
		$("#jobTitle").text(jobName);
		$("#checkListValue").load(url+"&timestamp="+(new Date()*1));		 

	}
	function shippingInstructionData(jobId,jobName,div,i) {
		blockUnblockShippingUI();
		i = $('#'+div.id+' tr' + ' td :contains(' + jobName  + ')').closest('tr').prevAll().length;
		var url = 'ManageShippingInstructionAction.do?appendixId=<%=request.getAttribute("appendixid")%>&jobId='+jobId+'&div='+div.id+'&rowNumber='+i;
		$("#checkListShippingValue").load(url+"&timestamp="+(new Date()*1));		 

	}

	function updateCheckListAndAjaxCall(divId,rowNumber,itemId) {
		setCheckListUsingAjax(itemId)
		document.getElementById(divId).deleteRow(rowNumber+1);
	}
	
	function blockUnblockUI() {
			$.blockUI(
				{ 
					message: $('#checkListDataModal'),
					css: { 
						width: '400px',
						height:'500px',
						color:'lightblue' 
						},
					overlayCSS:  { 
				        backgroundColor: '#f0f0f0', 
				        opacity:         0.3 
				    },
				    centerX: true,  
				    centerY: true,
				    fadeIn:  0,
				    fadeOut: 0
				}
				);
	}


	function blockUnblockShippingUI() {
		$.blockUI(
			{ 
				message: $('#checkShippingDataModal'),
				css: { 
					width: '400px',
					height:'500px',
					color:'lightblue' 
					},
				overlayCSS:  { 
			        backgroundColor: '#f0f0f0', 
			        opacity:         0.3 
			    },
			    centerX: true,  
			    centerY: true,
			    fadeIn:  0,
			    fadeOut: 0
			}
			);
}
	
function closeShippingWindow () {
        $.unblockUI(); 
		$("#checkListShippingValue").empty().html('<img src="./images/waiting_tree.gif" />');
		document.getElementById("jobTitle1").innerHTML = '';		
       // return false; 
}
	
</script>
<body>
	<table width="750" border=0>
	<%int divId =0; %>
		<logic:iterate name="MSPCCReportForm" id="completedActivityList" property="sectionBHeaderList" indexId="count">
			<tr>
			<logic:greaterThan name ="completedActivityList" property="count" value="0">
				<td  style="text-align: left;" width="15" height="15"><a href="javascript:toggleMenuSection('<%=divId %>','<bean:write name="completedActivityList" property="value"/>','<bean:write name="completedActivityList" property="name"/>');"><img id='img_<%=divId %>' src="images/Expand.gif" border="0" alt="+"  /></a></td>
				<td colspan="5" class="texthdNoBorder" style="text-align: left;"><b><bean:write name="completedActivityList" property="name"/> (<bean:write name="completedActivityList" property="count"/>)</b></td>
			</logic:greaterThan>
			<logic:equal name ="completedActivityList" property="count" value="0">
				<td></td>
				<td colspan="5" class="texthdNoBorder" style="text-align: left;"><bean:write name="completedActivityList" property="name"/></td>
			</logic:equal>
			</tr>
			<tr>
				<td></td>
				<td colspan=5 valign="top">
					<table width="100%" id = 'div_<%=divId %>'  style="display: none" border=0>
						<bean:define id="msaName" name="MSPCCReportForm" property="msaName" type="java.lang.String"/>
						<bean:define id="appendixName" name="MSPCCReportForm" property="appendixName"  type="java.lang.String" />
						<%if (enabledMsaList.contains(msaName) && enabledAppendixList.contains(appendixName)) { %>
						<script>customizeView = true;</script>
							<tr>
								<td class="Nhyperoddfont" align="center"><b>Job Title</b></td>
									<% String[] categoryColumns = ((MSPCCReportBean)completedActivityList).getCategoryColumns();
									for(int i=0; i<categoryColumns.length; i++){%>
										<td class="Nhyperoddfont" align="center"><b><%=categoryColumns[i] %></b></td>
									<%} %>
								<td class="Nhyperoddfont" align="center"><b>Actions</b></td>
	 						</tr>		
						<% } else { %>
							<tr>
								<td class="Nhyperoddfont" align="center"><b>Job Title</b></td>
								<td class="Nhyperoddfont" align="center"><b>Reference Date</b></td>
								<td class="Nhyperoddfont" align="center"><b>Job Owner</b></td>
								<td class="Nhyperoddfont" align="center"><b>Error Condition</b></td>
								<td class="Nhyperoddfont" align="center"><b>Actions</b></td>
	 						</tr>		
					  <%} %>
					</table>
				</td>
			</tr>
			<%divId=divId+1; %>
		</logic:iterate>
	</table>
	<div id="checkListDataModal" style="display: none; cursor: default; border: 1px; border-color: red" > 
        <div  style="border: 2px; width=400px; background-color: blue">
        	<table border='0' cellspacing="0" cellpadding="0" class="Ntextoleftalignnowrappaleyellowborder2" width="100%">
        		<tr>
        			<td align="left" id="jobTitle" class="headerModal" height="10"><font style="font-weight: bold; font-size: 14px; text-align: right; padding-left: 4px;"></font></td>
        			<td align="right"><img height="15" width="15" alt="Close" src="images/delete.gif" id="close"></td>
        		</tr>
        	</table>
        </div>
        <div id="checkListValue"  class="modalValue" style=" padding-top:4px; overflow: visible; height:450px; overflow-x:auto; overflow-y:auto;"><img src="./images/waiting_tree.gif" /></div>
        
	</div> 
	
	<div id="checkShippingDataModal" style="display: none; cursor: default; border: 1px; border-color: red" > 
        <div  style="border: 2px; width=450px; background-color: blue">
        	<table border='0' cellspacing="0" cellpadding="0" class="Ntextoleftalignnowrappaleyellowborder2" width="100%">
        		<tr>
        			<td align="left" id="jobTitle1" class="headerModal" height="10" nowrap="nowrap"><font style="font-weight: bold; font-size: 14px; text-align: right; padding-left: 4px;"></font></td>
        			<td align="right"><img height="15" width="15" alt="Close" src="images/delete.gif" id="closeShipping" onclick="closeShippingWindow();"></td>
        		</tr>
        	</table>
        </div>
        <div id="checkListShippingValue"  class="modalValue" style=" padding-top:4px; overflow: visible; height:450px; overflow-x:auto; overflow-y:auto;"><img src="./images/waiting_tree.gif" /></div>
        
	</div> 
	 
	
	
	
</body>
</html>
