<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html:html>
<head>
	<title>MSP C/C Report CheckList</title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
</head>
	<script>
		function saveWorkflowChecklist() {
			//dialogArguments.updateCheckListAndAjaxCall('<%=request.getAttribute("div")%>',<%=request.getAttribute("rowNumber")%>,document.forms[0].item_code.value);
			setCheckListUsingAjax(document.getElementById("item_code").value)
			var divId =  '<%=request.getAttribute("div")%>';
			var rowNumber = <%=request.getAttribute("rowNumber")%>;
			$('#'+divId+' tr:eq('+rowNumber+')').remove();
		}
		
		function setCheckListUsingAjax(itemId){
			$.ajax({
			    type: "post",
			    dataType: "html",
			    url: "MSPCCReportAction.do?hmode=setCheckListWorkFlow&itemId="+itemId+"&timestamp="+(new Date()*1),
			    async: false,
			    timeout: 10000,
			    error: function() {},
			    success: function(data) {
					$('#item_status_0').text('Complete'); 
					$('.chkbx1:input').attr('disabled', true);
				}
			});
		}
		
		setCustomerData = function (){
			$("#dispalyMessage").hide();
			var custmereValues = '';
			var appendixCustomerIds = '';
			<c:forEach items="${ManageCheckListDataForm.customerInfoList}" var="requiredDataList" varStatus="index">
				<c:if test="${not index.last}">
					custmereValues = custmereValues + document.getElementById("custValue"+<c:out value='${index.index}'/>).value + '~'
					appendixCustomerIds = appendixCustomerIds + document.getElementById("appendixCustInfoId"+<c:out value='${index.index}'/>).value + '~'
				</c:if>
				<c:if test="${index.last}">
					custmereValues = custmereValues + document.getElementById("custValue"+<c:out value='${index.index}'/>).value
					appendixCustomerIds = appendixCustomerIds + document.getElementById("appendixCustInfoId"+<c:out value='${index.index}'/>).value
				</c:if>
			</c:forEach>
			getResponse('JobExecuteAction.do?hmode=setCustomerDate&jobId=<c:out value='${ManageCheckListDataForm.jobId}'/>&appendixId=<c:out value='${ManageCheckListDataForm.appendixId}'/>&custmereValues='+escape(encodeURI(custmereValues))+'&appendixCustomerIds='+appendixCustomerIds);
		}

		getResponse = function (partnerURL){
		/* This function call Ajax which is inmplemented into /javascript/prototype.js */	 
			var url = partnerURL;
			var pars = '';		
			//var myAjax = new Ajax.Updater({success:'CustomerFields'},url,{method:'get', parameters:pars, onSuccess:disableStatus, onFailure:disableStatus, evalScripts: true});
			$.ajax({
			    type: "post",
			    dataType: "html",
			    cache:false,
			    url: partnerURL+"&timestamp="+(new Date()*1),
			    async: false,
			    timeout: 10000,
			    error: function(xhr, status, error) {
					var errMsg = status;
		            try
		            {
		                errMsg = xhr.statusText;
		            }
		            catch(err){}
			    	getError(errMsg)},
			    success: function(data) {getSuccess()}
			});	
		}
		function getSuccess() {
			//alert('Disabled status called');
			$("#dispalyMessage").fadeIn('slow');
			//alert(<%=request.getAttribute("statusvalue")%>)
			document.getElementById("dispalyMessage").style.display='';
			document.getElementById("dispalyMessage").innerHTML = "&nbsp;&nbsp;<b>Updated Successfully.</b>";
			document.getElementById("dispalyMessage").style.color='Green';
			//document.getElementById("dispalyMessage").style.font.weight='Bold';
			document.getElementById("dispalyMessage").style.fontSize = 11;
			
		}
		function getError(errMsg) {
			$("#dispalyMessage").fadeIn('slow');
			//alert(<%=request.getAttribute("statusvalue")%>)
			document.getElementById("dispalyMessage").style.display='';
			document.getElementById("dispalyMessage").innerHTML = "<b>&nbsp;&nbsp;Error occured during updating. " + errMsg + "</b>";
			document.getElementById("dispalyMessage").style.color='Red';
			//document.getElementById("dispalyMessage").style.font.weight='Bold';
			document.getElementById("dispalyMessage").style.fontSize = 11;
			
		}


		
	</script>
<body>
<html:form action="ManageCheckListDataAction.do?">

<html:hidden name="ManageCheckListDataForm" property="appendixId"/>
<table  cellspacing="0" cellpadding="0" border="0" width="380" >
	<tr>
	<td style="padding-left: 10px; padding-right: 5px;">
			<table cellspacing="1" cellpadding="0" border="0" width="100%">
				<tr>
					<td colspan=3 class="texthdNoBorder" align="center"><B>Job Workflow</B></td>
				</tr>
				<tr>
					<td class="texthdNoBorder" align="center" colspan=2><b>Status</b></td>
					<td class="texthdNoBorder" align="center"><b>Checklist Item</b></td>
				</tr>
				<input type="hidden" id="item_code" name="item_code" value="<c:out value='${ManageCheckListDataForm.jobWorkflowItemId[0]}'/>"/>
					<c:forEach items="${ManageCheckListDataForm.jobWorkflowItemId}" var="jobWorkFlowChecklist" varStatus="status" >
					<c:if test = "${ManageCheckListDataForm.jobWorkflowItemDescription[status.index] ne '' 
					   				&& not empty ManageCheckListDataForm.jobWorkflowItemDescription[status.index]}">
						<tr id="initialPendingData">
							<td  width="11" align="center" valign="middle">
								<c:if test = "${status.index eq 0}">
									<input id="job1" type="checkbox"  class="chkbx1" onclick="saveWorkflowChecklist();" name="job1"/>
								</c:if>
								<c:if test = "${status.index ne 0}">
									&nbsp;&nbsp;&nbsp;
								</c:if>
							</td>
					   		<td width="76" id="item_status_<c:out value="${status.index}"/>" 
					   			<c:if test = "${status.index % '2' eq 0}">class = "hypereven"</c:if>
					   			<c:if test = "${status.index % '2' ne 0}">class = "dbvaluesmall"</c:if> 
					   			style="padding: 3px; vertical-align: text-top;" >
					   			<c:if test = "${ManageCheckListDataForm.jobWorkflowItemStatus[status.index] eq ''}">
					   				-
					   			</c:if>
					   			<c:if test = "${ManageCheckListDataForm.jobWorkflowItemStatus[status.index] ne ''}">
					   				<c:out value="${ManageCheckListDataForm.jobWorkflowItemStatus[status.index]}"/>
					   			</c:if>
					   		</td>	
							<td width="293" id="item_description_<c:out value="${status.index}"/>" 
								<c:if test = "${status.index % '2' eq 0}">class = "hypereven"</c:if>
								<c:if test = "${status.index % '2' ne 0}">class = "dbvaluesmall"</c:if> 
					   			style="padding: 3px;" >
					   			<c:if test = "${ManageCheckListDataForm.jobWorkflowItemDescription[status.index] eq '' 
					   				|| empty ManageCheckListDataForm.jobWorkflowItemDescription[status.index]}">
					   				-
					   			</c:if>
					   			<c:if test = "${ManageCheckListDataForm.jobWorkflowItemDescription[status.index] ne '' 
					   				&& not empty ManageCheckListDataForm.jobWorkflowItemDescription[status.index]}">
					   				<c:out value="${ManageCheckListDataForm.jobWorkflowItemDescription[status.index]}"/>
					   			</c:if> 
							</td>
						</tr>
						</c:if>	
						</c:forEach>

			</table>
	</td>
	</tr>
	
	<tr>
	<td style="padding-left: 10px;padding-right: 5px; padding-top: 4px;">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="Ntextoleftalignnowrappaleyellowborder2">
				<tr>
				<td>
					<table cellspacing="1" cellpadding="0" border="0" width="100%" class="Ntextoleftalignnowrappaleyellowborder2">
						<tr>
							<td id="customerDataHeading" class="texthdNoBorder" colspan=2><b>Customer Required Data/Reporting</b></td>
						</tr>
						<tr>
						   	<td class = "texthdNoBorder" align="center"><b>Customer Data</b></td>
						   	<td class = "texthdNoBorder"  align="center"><b>Value</b></td>
						</tr>
						<c:if test="${empty ManageCheckListDataForm.customerInfoList}">
						<tr>
							<td class="Nhyperevenfont"  style="padding-left: 10px;" colspan="2">
								No Parameters Defined.
							</td>
						</tr>
						</c:if>	
						
						<c:forEach items="${ManageCheckListDataForm.customerInfoList}" var="requiredDataList" varStatus="index">
							<tr>
								<td <c:if test="${(index.index % 2)eq '0'}">class='Nhyperevenfont'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Nhyperoddfont'</c:if> width="100">
									<c:out value='${requiredDataList.infoParameter}'/>
								</td>
								<td <c:if test="${(index.index % 2)eq '0'}">class='Nhyperevenfont'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Nhyperoddfont'</c:if>>
									<input type="hidden" name="appendixCustInfoId<c:out value='${index.index}'/>" value="<c:out value='${requiredDataList.infoId}'/>"/>
									<input type="text" class="text" size="40" name="custValue<c:out value='${index.index}'/>" value="<c:out value='${requiredDataList.value}'/>"/>
								</td>
							</tr>
						</c:forEach>
						<tr>
							<td></td>
							<td><table border="0" cellspacing=0 cellpadding=0><tr>
								<td style="padding-left:3px;  display: none;" id ="dispalyMessage" width="60%" nowrap>&nbsp;Updated Successfully.</td>
								<td width="1%">&nbsp;</td>
								<td align="right"><input type="button" value="Update" class = "button_c" onclick="setCustomerData();" <c:if test="${empty ManageCheckListDataForm.customerInfoList}">disabled="disabled"</c:if>/></td>
							</tr></table></td>
							
						</tr>

						
					</table>
				</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</html:form>
</body>
</html:html>
