<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html:html>

<head>
	<title>MSP C/C Report</title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>
 	<script language = "JavaScript" src = "javascript/UtilityFunction.js?timestamp="+(new Date()*1)></script>
</head>
<body>
<html:form action="MSPCCReportAction.do?function=View">
<jsp:include page = '/PRM/ViewSelectorWithBrCrumbPRM.jsp'>
	<jsp:param name = 'bean' value = 'MSPCCReportForm'/>
	<jsp:param name = 'projectType' value = '<%=(String)request.getParameter("fromPage")%>'/>
	<jsp:param name = 'pageName' value = 'MSPCCReport.jsp'/>
</jsp:include>

<html:hidden name="MSPCCReportForm" property="appendixid"/>
<html:hidden name="MSPCCReportForm" property="appendixName"/>
<html:hidden name="MSPCCReportForm" property="msaName"/>
<table  cellspacing="0" cellpadding="0" border="0">
	<!-- for link show discounted rates end-->
	<!-- class="Ntextoleftalignnowrappaleyellowborder2" -->
	<tr>
		<td style="padding-left: 10px; padding-top: 0px; padding-right: 5px; padding-bottom: 5px;">
			<table border=0 cellspacing="0" cellpadding="0">
			
			<%if(request.getAttribute("isConfigured").toString() == "isConfigured") { %>
			
				<tr>
					<td class="headertop">Tasks Completed</td>
					<td class="headertop">Pending and Complete Tasks</td>
					<td class="headertop">Tasks Trends</td>
				</tr>
				<tr>
					<td valign="top" style="padding-right: 4px;">
						<div id="mspactivityCompleted" >
						</div>
					</td>
					<td valign="top" style="padding-right: 4px;">
						<div id="mspTopLevelBreakdown">
						</div>	
					</td>				
					<td valign="top">
						<div id="mspActivityTrends">
						</div>
					</td>
				</tr>
				<tr>
					<td Colspan=3 align=left><h1 style="font-size: 14px; white-space: nowrap;">Task Breakdown</h1></td>
				</tr>
				<tr>
					<td colspan=3>
						<table>
							<tr>
								<td>
									<jsp:include page="/PRM/MSPCCReport/MSPActivityBreakdown.jsp"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<%}  else {%>
				<tr>
					<td class="message" colspan=3>The Appendix is not configured to support this option.</td>
				</tr>
			<%} %>
			</table>
		</td>
	</tr>
</table>
</html:form>
</body>

</html:html>
<script>
	var refreshTimer1 = 300000; // - 60 Seconds		-Monitoring Summary

	$(document).ready(function() {

		$('#close').click(function() {
	        $.unblockUI(); 
			$("#checkListValue").empty().html('<img src="./images/waiting_tree.gif" />');		
	       // return false; 
	    }

    );
	//if('<%=request.getAttribute("isConfigured").toString()%>' == 'isConfigured' ) { 
		$("#mspactivityCompleted").load("./MSPCCReportAction.do?hmode=taskCompleted&appendixid="+document.forms[0].appendixid.value+"&timestamp="+(new Date()*1));
		$("#mspTopLevelBreakdown").load("./MSPCCReportAction.do?hmode=pendingAndCompleteTask&appendixid="+document.forms[0].appendixid.value+"&timestamp="+(new Date()*1));
		$("#mspActivityTrends").load("./MSPCCReportAction.do?hmode=activityTrends&appendixid="+document.forms[0].appendixid.value+"&timestamp="+(new Date()*1));
	
		setInterval(function(){
			ajaxCall(
						"./MSPCCReportAction.do?hmode=taskCompleted&appendixid="+document.forms[0].appendixid.value,
						"mspactivityCompleted",
						"./MSPCCReportAction.do?hmode=taskCompleted&appendixid="+document.forms[0].appendixid.value
					);
			}, 
			refreshTimer1
		);
		
		setInterval(function(){
			ajaxCall(
						"./MSPCCReportAction.do?hmode=pendingAndCompleteTask&appendixid="+document.forms[0].appendixid.value,
						"mspTopLevelBreakdown",
						"./MSPCCReportAction.do?hmode=pendingAndCompleteTask&appendixid="+document.forms[0].appendixid.value
					);
			}, 
			refreshTimer1
		);
		setInterval(function(){
			ajaxCall(
						"./MSPCCReportAction.do?hmode=activityTrends&appendixid="+document.forms[0].appendixid.value,
						"mspActivityTrends",
						"./MSPCCReportAction.do?hmode=activityTrends&appendixid="+document.forms[0].appendixid.value
					);
			}, 
			refreshTimer1
		);
//	}
    
});

	function onAjaxBeforeSend(elementId){
		
	}//End

	function onAjaxComplete(elementId){
	}//End

	function onAjaxSuccess(elementId){
	}//End

	function onAjaxError(elementId){
	}


/* Function for view selector: Start */
function changeFilter()
{
	document.forms[0].action="MSPCCReportAction.do?function=View&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="MSPCCReportAction.do?function=View&resetList=something";
	document.forms[0].submit();
	return true;

}
/* Function for view selector: End */
</script>