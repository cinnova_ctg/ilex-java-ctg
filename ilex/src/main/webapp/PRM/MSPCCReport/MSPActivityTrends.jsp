<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<title>Insert title here</title>
</head>
<body>
	<table class="Ntextoleftalignnowrappaleyellowborder2" border=0 cellspacing="0" cellpadding="0">
		<tr>
			<td style="padding-left: 2px;">
				<table border=0 cellspacing="1" cellpadding="0">
					<tr>
						<td colspan=2 class="texthdNoBorder" style="text-align: left;"><b>Tasks in Work by Group</b></td>
					</tr>
					<logic:iterate name="MSPCCReportForm" id="workGroupList" property="sectionCList" indexId="count">
						<tr>
							<td class="Nhyperevenfont"><bean:write name="workGroupList" property="name"/></td>
							<td class="Nhyperevenfont" align="right"><bean:write name="workGroupList" property="value"/></td>
						</tr>
					</logic:iterate>
					
					<tr>
						<td colspan=2 class="texthdNoBorder" style="text-align: left;"><b>Total Actions Taken</b></td>
					</tr>
					<logic:iterate name="MSPCCReportForm" id="actionTakenList" property="sectionDList" indexId="count">
						<tr>
							<td class="Nhyperevenfont"><bean:write name="actionTakenList" property="name"/></td>
							<td class="Nhyperevenfont" align="right"><bean:write name="actionTakenList" property="value"/></td>
						</tr>
					</logic:iterate>
					
					<tr>
						<td colspan=2 class="texthdNoBorder" style="text-align: left;"><b>Errors</b></td>
					</tr>
					<logic:iterate name="MSPCCReportForm" id="errorsList" property="sectionEList" indexId="count">
						<tr>
							<td class="Nhyperevenfont"><bean:write name="errorsList" property="name"/></td>
							<td class="Nhyperevenfont" align="right"><bean:write name="errorsList" property="value"/></td>
						</tr>
					</logic:iterate>
				</table>
				
			</td>
		</tr>
	</table>
</body>
</html>
