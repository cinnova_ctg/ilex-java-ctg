<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html:html>
<head>
	<title>Shipping Instructions</title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>

<%
	int resSize = 0;
	if(request.getAttribute("resourceSize") != null) {
		resSize = Integer.parseInt(request.getAttribute("resourceSize").toString());
	}
%>
<script>
//$("#jobTitle1").text('<bean:write name="ManageShippingInstructionForm" property="siteNumber"/>');
	document.getElementById("jobTitle1").innerHTML = 'Shipping Request and Instructions for '+'<bean:write name="ManageShippingInstructionForm" property="siteNumber"/>'+'&nbsp;';
</script>
</head>
<body>
<html:form action="ManageShippingInstructionAction.do?">

<html:hidden name="ManageShippingInstructionForm" property="appendixId"/>
<table class="Ntextoleftalignnowrappaleyellowborder2" border=0 cellspacing="0" cellpadding="0" width="100%" style="background-color: #FFFFFF;">
		<tr>
			<td style="padding-left: 3px; padding-right: 3px;">
<table  cellspacing="0" cellpadding="0" border="0" width="100%" class="Nhyperevenfont">
	<tr>
	<td style="padding-left: 0px; padding-right: 5px;">
			<table cellspacing="1" cellpadding="0" border="0" width="100%">
				<tr>
					<td colspan=1 class="headertopWithSmallFont">
						<B>Delivery Date and Method</B></td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr>
					<td colspan=1 class="texthdNoBorderNoBackGround" style="text-align: left; padding-left: 0px; ">
						<B>Required Delivery Date:</B> 
						<bean:write name="ManageShippingInstructionForm" property="reqDeliveryDate"/>
					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr>
					<td colspan=1 class="texthdNoBorderNoBackGround" style="text-align: left; padding-left: 0px;">
						<B>Shipping Method:</B> <font class="NWithLessSpaces">Lowest cost shipping method satisfying the <font style="font-weight:bold; font-size:10px;">Required<br/>Delivery Date</font>.
						If no <font style="font-weight:bold; font-size:10px;">Required Delivery Date</font> is specified, check with<br/> project team immediately.</font>
						</td>
				</tr>
				<tr><td height="5"></td></tr>
			</table>
			
			<table cellspacing="1" cellpadding="0" border="0" width="100%">
				<tr>
					<td colspan=1 class="headertopWithSmallFont">
						<B>Site Address and Contact</B>
					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr>
					<td colspan=1 class="texthdNoBorderNoBackGround" style="text-align: left; padding-left: 0px;">
						<B>Site:</B>&nbsp;<font class="NWithLessSpaces"><bean:write name="ManageShippingInstructionForm" property="siteNumber"/></font></td>
				</tr>
				<tr><td height="0"></td></tr>
				<tr>
					<td colspan=1 class="texthdNoBorderNoBackGround" style="text-align: left; padding-left: 0px;">
						<B>Delivery Address:</B>
					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr>
					<td class="Nhyperevenfont" style="padding-left: 15px;">
						<table border="0">
							<tr>
								<td class="NWithLessSpaces"><bean:write name="ManageShippingInstructionForm" property="msaName"/></td>
							</tr>
							<tr>
								<td class="NWithLessSpaces"><bean:write name="ManageShippingInstructionForm" property="siteAddress"/></td>
							</tr>
							<tr>	
								<td class="NWithLessSpaces">
									<bean:write name="ManageShippingInstructionForm" property="siteCity"/>,&nbsp;
									<bean:write name="ManageShippingInstructionForm" property="siteState"/>,&nbsp;
									<bean:write name="ManageShippingInstructionForm" property="siteZip"/>
								</td>
							</tr>
							<tr><td height="5"></td></tr>
							<tr>
								<td class="NWithLessSpaces">Attn:&nbsp;<bean:write name="ManageShippingInstructionForm" property="sitePrimaryPoc"/></td>
							</tr>
							<tr><td height="5"></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="Nhyperevenfont" style="padding-left: 0px;">
						Clearly mark package with:  Hold for CNS Technician
					</td>
				</tr>
			</table>
			
			<table cellspacing="1" cellpadding="0" border="0" width="100%">
				<tr><td height="5"></td></tr>
				<tr>
					<td class="headertopWithSmallFont"><b>Materials</b></td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr>
					<td class="Nhyperevenfont" style="padding-left: 0px;">Include the following materials based on the circuit type listed below:</td>
				</tr>
				<tr><td height="5"></td></tr>
				
				
				<tr>
					<td colspan=1 class="texthdNoBorderNoBackGround" style="text-align: left; padding-left: 0px; ">
						<B>Circuit Type:</B> 
						<bean:define id="potsValue" name="ManageShippingInstructionForm" property="potsType" type="java.lang.String"/>
						<logic:equal name="ManageShippingInstructionForm" property="circuitType" value="ADSL">
							<bean:write name="ManageShippingInstructionForm" property="circuitType"/>/<bean:write name = "ManageShippingInstructionForm" property="potsType"/>
						</logic:equal>
						
						<logic:notEqual name="ManageShippingInstructionForm" property="circuitType" value="ADSL">
							<bean:write name="ManageShippingInstructionForm" property="circuitType"/>
						</logic:notEqual>
					</td>
				</tr>
				<%if(resSize > 0) {	%>
					<logic:iterate id="resources" name="ManageShippingInstructionForm" property="resourceArr">
						<tr>
							<td class="NWithLessSpaces" style="text-align: left; padding-left: 8px; ">
								<bean:write name="resources"/>
							</td>
						</tr>
					</logic:iterate>
				<%} %>
			</table>
	</td>
	</tr>
</table>
</td>
</tr>
</table>
</html:form>
</body>
</html:html>
