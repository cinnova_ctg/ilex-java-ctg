<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>


<title>Insert title here</title>
</head>
<body>
	<table class="Ntextoleftalignnowrappaleyellowborder2" border=0 cellspacing="0" cellpadding="0">
		<tr>
			<td style="padding-left: 2px;">
				<table border=0 cellspacing="1" cellpadding="0">
					<tr valign="top">
						<td align="center" class="texthdNoBorder"><b>Task</b></td>
						<td align="center" class="texthdNoBorder"><b>Completed</b></td>
						<td align="center" class="texthdNoBorder"><b>Errors</b></td>
					</tr>
					<% String classname="";
						int i = 0;
					%>
					<logic:iterate name="MSPCCReportForm" id="completedActivityList" property="sectionAList" indexId="count">
						<%  
							if(i%2==0) {
								classname = "Nhyperoddfont";
							} else {
								classname = "Nhyperevenfont";
							}
							i++;
						%>
						<tr>
							<td align="left"  class="<%=classname %>"><bean:write name="completedActivityList" property="name"/></td>
							<td align="right" class="<%=classname %>"><bean:write name="completedActivityList" property="value"/></td>
							<td align="right" class="<%=classname %>"><bean:write name="completedActivityList" property="errorValue"/></td>
						</tr>
					</logic:iterate>
					<tr>
						<td nowrap="nowrap">
							<table width="100%" border=0 cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding-left: 2px;">
										<font style="font-size:10px; font-weight: bold;"><bean:write name="MSPCCReportForm" property="strtDateName"/>:</font>
										<font style="font-size: 10px;"><bean:write name="MSPCCReportForm" property="strtDateValue"/></font>&nbsp;&nbsp;&nbsp;
									</td>
									<td align="right"><font style="font-size:10px; font-weight: bold;">Totals:&nbsp;</font></td>
								</tr>
							</table>
						</td>
						<td align="right" class="<%=classname %>"><bean:write name="MSPCCReportForm" property="totalCompleted"/></td>
						<td align="right" class="<%=classname %>"><bean:write name="MSPCCReportForm" property="totalErrors"/></td>
					</tr>
					<tr>
						<td colspan=3 nowrap="nowrap" style="padding-left: 2px;"><font style="text-align: right; font-size:10px; font-weight: bold;"><bean:write name="MSPCCReportForm" property="plannedDateName"/>:</font>&nbsp;<font style="font-size: 10px;"><bean:write name="MSPCCReportForm" property="plannedDateValue"/></font></td>
					</tr>
					<tr>
						<td colspan=3>
							<table border =0 cellspacing="0" cellpadding="0" width="100%">
								<tr>
									<td nowrap="nowrap" style="padding-left: 2px;"><font style="text-align: right; font-size:10px; font-weight: bold;"><bean:write name="MSPCCReportForm" property="siteCountName"/>:</font>&nbsp;<font style="font-size: 11px;"><bean:write name="MSPCCReportForm" property="siteCountValue"/>&nbsp;&nbsp;</font></td>
									<td nowrap="nowrap" align="right" style="padding-right: 2px;">
										<font style="text-align: right; font-size:10px; font-weight: bolder;"><bean:write name="MSPCCReportForm" property="lastUpdateDateName"/>:</font>&nbsp;<font style="font-size: 10px;"><bean:write name="MSPCCReportForm" property="lastUpdateDateValue"/></font>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
