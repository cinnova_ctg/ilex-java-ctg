<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>


<html>
<%
String strXMLSystem="";
String[] color = {"aeaeff","008000","c3ffc3","ff0000"};
strXMLSystem = "<graph pieRadius = '60' bgColor='dbe4f1' showNames='1' showValues='0' showPercentageInLabel='0' decimalPrecision='0'>";
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<script type="text/javascript" src="MSP/MspJavaScript/FusionCharts.js"></script>


<title>Insert title here</title>
</head>
<%@ include file="/MSP/FusionCharts.jsp" %>
<body>

<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width ="100%" class="Ntextoleftalignnowrappaleyellowborder2">
				<tr>
					<td style="padding: 3px;">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
							<% 	int i =0;
								boolean isDataExist = false;
							%>
							<logic:iterate name="MSPCCReportForm" id="xmlList" property="sectionFList" indexId="count">
								<bean:define id="nameLabel" name="xmlList" property="name" type="java.lang.String"></bean:define>
								<bean:define id="valueLabel" name="xmlList" property="value" type="java.lang.String"></bean:define>
								<%
									if(valueLabel != null && !valueLabel.equals("") && !valueLabel.equals("0")) {
										strXMLSystem = strXMLSystem + "<set name='" +nameLabel+"' value='"+ valueLabel+"' color='"+color[i]+"'/>";
										isDataExist = true;
									}
									i++;
								%>
								
							</logic:iterate>
							<%strXMLSystem = strXMLSystem + "</graph>"; %>
							<%if(isDataExist) { %>
								<%
									String chartCodeSystem=createChart("MSP/FCF_Pie3D.swf", "", strXMLSystem, "2", 355, 150, false,false);
								%>
								<%=chartCodeSystem%>
							<%} else {%>
								<table border="1" cellspacing="0" cellpadding="0" width="355" height="150" bgcolor="dbe4f1">
									<tr>
										<td class="messagesmallfontwithbackground" style="padding-left: 5px; padding-top: 5px;" height="10">No Data Available.</td>
									</tr>
									<tr>
										<td></td>
									</tr>
								</table>
							<%} %>
							</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-top: 4px;">
			<table class="Ntextoleftalignnowrappaleyellowborder2" width="100%" height="95">
				<tr>
					<td valign="top"><b>Earned Values:&nbsp;1.0</b></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
