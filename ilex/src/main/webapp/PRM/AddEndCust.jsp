<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<html:html>
<head>
	<title><bean:message bundle = "pm" key = "common.addendcust.title"/></title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	
<script>

<%
int addRow = -1;
boolean addSpace = false;
int rowHeight = 65;
int ownerListSize = 0;
int j=2;
String checkowner=null;

	String MSA_Id = "";

	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());
	
	rowHeight += ownerListSize*18;
	
	if(request.getAttribute("MSA_Id")!=null)
	MSA_Id= request.getAttribute("MSA_Id").toString();


	
%>

function getMSAName()
{
	if(document.forms[0].check.checked){
		document.forms[0].endcust.value="<bean:write name = "AddEndCustForm" property = "msaname"/>";
		document.forms[0].endcust.disabled="true";
		
	}
	else
	{
		document.forms[0].endcust.value="";
		document.forms[0].endcust.disabled=false;
		document.forms[0].endcust.focus();
	}
}

function backtoappendix()
{
	document.forms[0].action = "AppendixDetailAction.do?Appendix_Id=<bean:write name = "AddEndCustForm" property = "typeId" />&MSA_Id=<bean:write name = "AddEndCustForm" property = "msaid" />";
	document.forms[0].submit();
	return true;
	
}
function getMSANameOnload()
{
	if(document.forms[0].check.checked){
		document.forms[0].endcust.value="<bean:write name = "AddEndCustForm" property = "msaname" />";
		document.forms[0].endcust.disabled="true";
		
	}
}
</script>

	
</head>
<%
	String from = "";

	if( request.getAttribute( "from" ) != null )
	{
		from = request.getAttribute( "from" ).toString();
	}

%>
<%-- <%@ include  file="/NMenu.inc" %> --%>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onload = "getMSANameOnload();OnLoad();">

<html:form action = "AddEndCust">
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property = "type" />
<html:hidden property = "typeId" />
<html:hidden property = "appendixid" />
<html:hidden property = "from" />
<html:hidden property ="msaname"/>
<html:hidden property ="fromPage"/>
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<html:hidden property="msaid"/> 
<html:hidden property = "name" />
<html:hidden property="appendixOtherCheck"/> 

<!-- Menu/View selector:Start -->
<logic:equal name="AddEndCustForm" property="fromPage" value="Appendix">
	
	<%@ include  file="/AppendixMenu.inc" %>
	
<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>					
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 	<tr>
    <td valign="top" width = "100%">
      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
       <%--  <tr>
          <!-- Sub Navigation goes into this td -->
          
						<%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>	
						
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
          	<!-- End of Sub Navigation -->
        </tr> --%>
      <%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>  
        
         <tr>
          <!-- BreadCrumb goes into this td -->
          <td background="images/content_head_04.jpg" height="21" colspan="23" width ="100%">
          	<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "AddEndCustForm" property = "msaid"/>"><bean:write name = "AddEndCustForm" property = "msaname"/></a></div>
          </td>
        </tr>
   		  <tr>    
			<td colspan = "5" height = "30" width = "800"><h2>
			 	<bean:message bundle="pm" key="common.endcust.title"/>:&nbsp;<bean:write name = "AddEndCustForm" property = "name"/>
			</h2></td>
		 </tr>
      </table></td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
    <td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" conspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                      <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
            			<div id="featured">
								<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
						</div>								
	                </div>		
					<span>
					<div id="filterAppendix" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
	                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
	                          <tr>
	                            <td colspan="3" class="filtersCaption">Status View</td>
	                          </tr>
	                          
	                          <tr>	
	                          	  <td></td>
		                          	  <td class="tabNormalText">
								    		<logic:present name ="statuslist" scope="request">
										    	<logic:iterate id = "list" name = "statuslist"> 
											 		<html:multibox property = "appendixSelectedStatus"> 
														<bean:write name ="list" property = "statusid"/>
											 		</html:multibox>   
												 	<bean:write name ="list" property = "statusdesc"/><br> 
												</logic:iterate>
											</logic:present>
											<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="AddEndCustForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="AddEndCustForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
												<tr><td class="tabNormalText"><html:radio name ="AddEndCustForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
											</table>	
										</td>
		    					</tr>
	                        </table></td>
	                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
	                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
	                          <tr>
	                            <td colspan="3" class="filtersCaption">User View</td>
	                          </tr>
	                          <tr>
						         <td class="tabNormalText">
						    		<logic:present name ="ownerlist" scope="request">
								    	<logic:iterate id = "olist" name = "ownerlist"> 
											<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
											<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<%
											checkowner = "javascript: checking('"+ownerId+"');"; 
											if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
												addRow++;
												addSpace = true;
											}	
												
								    		if(addRow == 0) { %> 
									    		<br/>
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
								    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 							    	
								    	
									 		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
												<bean:write name ="olist" property = "ownerId"/>
									 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
										</logic:iterate>
									</logic:present>
							    </td>
	                          </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
	                          </table></td>
	                        </tr>
	  					</table>											
					</div>
				</span>
				</td>
              </tr>
            </table></td>
        </tr>
      </table>
     </td>
 </tr>
</table>
<%@ include  file="/AppendixMenuScript.inc" %>
</logic:equal>
<!-- Menu/View selector:End -->


<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center" valign  = "top">
	  	  <tr><td width="2" height="0"></td>
			<td>
			<table id = "dynatable" border = "0" cellspacing = "1" cellpadding = "1" width = "480"> 
    		<!-- for left tree refersh:start -->
    		<logic:present name = "clickShow" scope = "request">
		  		<logic:notEqual name = "clickShow" value = "">
					<script>
						parent.ilexleft.location.reload();
					</script>	
				</logic:notEqual>
			</logic:present> 	
			<logic:present name = "specialCase" scope = "request">
		  		<logic:notEqual name = "specialCase" value = "">
					<script>
						parent.ilexleft.location.reload();
					</script>	
				</logic:notEqual>
			</logic:present>
    		<!-- for left tree refersh:end -->
    		<logic:notEqual name="AddEndCustForm" property="fromPage" value="Appendix">
	    		<logic:equal name = "AddEndCustForm"  property = "type" value = "Appendix">
	    		<tr> 
	    		<td colspan="2" class = "labeleboldwhite" height = "30">
	    		
	    				<bean:message bundle = "pm" key = "common.addendcust.endcust"/> <bean:message bundle = "pm" key = "common.addcommentpage.for"/> <bean:write name = "AddEndCustForm" property = "name" /> <bean:message bundle = "pm" key = "common.addcommentpage.appendix"/>
	    		</td>
	 		  </tr>
	    		</logic:equal>
    		</logic:notEqual>
    		
    		<!--<logic:present name = "AddEndCustForm" property = "type">	
    			<logic:equal name = "AddEndCustForm"  property = "type" value = "Appendix">
    				<bean:message bundle = "pm" key = "common.addendcust.endcust"/> <bean:message bundle = "pm" key = "common.addcommentpage.for"/> <bean:write name = "AddEndCustForm" property = "name" /> <bean:message bundle = "pm" key = "common.addcommentpage.appendix"/>
    			</logic:equal>
	    	</logic:present>-->
 		  <tr>
 		  	<td class = "colDark"><bean:message bundle = "pm" key = "common.addendcust.customer"/></td> 
 		  	<td class = "colDark">
 		  		<html:text property = "endcust" size = "40" styleClass = "text"/>
 		  		  <td class = "colDark"><html:checkbox name="AddEndCustForm" property = "check" onclick="getMSAName();">Same as MSA</html:checkbox></td>
 		  	</td>
 		 
 		  </tr>
		<tr>
		
		</tr>			
 		  <tr>
 		  	<td colspan = "3" class = "Nbuttonrow">
 		  		<html:submit property = "add" styleClass = "Nbutton" onclick = "return validate();"><bean:message bundle = "pm" key = "common.addcommentpage.add"/></html:submit>
 			  	<html:button property = "back" styleClass = "Nbutton" onclick = "return backtoappendix('appendixdetail');">Back</html:button>
 		  		
 		  	</td>
 		  	</tr>

	
</table>
			</td>
		</tr> 
	</table>
</html:form>
 		  
</body>

<script>

function trimFields() 
{
	var field = document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='textarea') 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}


function validate()
{
	trimFields();
	if( document.forms[0].endcust.value == "" )
	{
		alert( "Please enter End Customer." );
		document.forms[0].endcust.focus();
		return false;
	}
	document.forms[0].endcust.disabled=false;
	return true;
}

function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="AddEndCust.do?typeid=<bean:write name = 'AddEndCustForm' property = 'typeId' />&Appendix_Id=<bean:write name = 'AddEndCustForm' property = 'appendixid' />&Type=Appendix&firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}

function checking(checkboxvalue)
{
if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				document.forms[0].appendixSelectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				if(document.forms[0].appendixSelectedOwners[j].checked) {
				 	document.forms[0].appendixSelectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{

	for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
	{
			if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
			{
				document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="AddEndCust.do?typeid=<bean:write name ='AddEndCustForm' property ='typeId'/>&Appendix_Id=<bean:write name = 'AddEndCustForm' property = 'typeId'/>&MSA_Id=<bean:write name = "AddEndCustForm" property = "msaid" />&type=Appendix&viewjobtype=ION&Type=Appendix&opendiv=0";
 				document.forms[0].submit();

			}
	}
	}
}

function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filterAppendix").style.visibility="visible";
<%}%>
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filterAppendix").style.left=leftAdj;
		
}

/*function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AddEndCust.do?typeid=<bean:write name ='AddEndCustForm' property ='typeId'/>&Appendix_Id=<bean:write name = 'AddEndCustForm' property = 'typeId'/>&type=Appendix&viewjobtype=ION&Type=Appendix&clickShow=true";
	document.forms[0].submit();
	return true;
}
*/

function show()
{
	document.forms[0].go.value="true";
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action = "AddEndCust.do?typeid=<bean:write name = 'AddEndCustForm' property = 'typeId' />&MSA_Id=<bean:write name = "AddEndCustForm" property = "msaid" />&Appendix_Id=<bean:write name = 'AddEndCustForm' property = 'typeId' />&Type=Appendix&clickShow=true";
	
	document.forms[0].submit();
	return true;
}

function ShowDiv()
{
	leftAdjLayers();
	document.getElementById("filterAppendix").style.visibility="visible";
	return false;
}

function hideDiv()
{
	document.getElementById("filterAppendix").style.visibility="hidden";
	return false;
}
function month_Appendix()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AddEndCust.do?typeid=<bean:write name = 'AddEndCustForm' property = 'typeId' />&Appendix_Id=<bean:write name = 'AddEndCustForm' property = 'typeId' />&viewjobtype=ION&Type=Appendix&month=0&clickShow=true&Type=Appendix";
	document.forms[0].submit();
	return true;
}
function week_Appendix()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AddEndCust.do?typeid=<bean:write name = 'AddEndCustForm' property = 'typeId' />&Appendix_Id=<bean:write name = 'AddEndCustForm' property = 'typeId' />&week=0&clickShow=true&Type=Appendix";
	document.forms[0].submit();
	return true;
}


</script>


</html:html>