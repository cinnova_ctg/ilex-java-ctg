<!DOCTYPE HTML>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<meta http-equiv="X-UA-Compatible" content="IE=9"/>


<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
<%@ include  file="/NMenu.inc" %>
<%@ include  file="/AjaxOwners.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>

	<script>
		function onUpdate(){
			document.forms[0].action="JobNotesAction.do?hmode=save";
			document.forms[0].submit();
		}
	</script>
	
	
</head>
	<body>

		<html:form action="JobNotesAction">
	  	<input type="hidden" name="appendixId" value="<c:out value='${JobNotesForm.appendixId}'/>"/>  
		<input type="hidden" name="jobId" value="<c:out value='${JobNotesForm.jobId}'/>"/>
		<c:if test="${not empty JobNotesForm.jobId}">
		<!--  change file  -->
		<%@ include  file="/DashboardVariables.inc" %>
		<%@ include  file="/AppedixDashboardMenu.inc" %>
		<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li><a href="JobEditAction.do?ref=inscopeview&amp;appendix_Id=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>&amp;Job_Id=0">Add Job</a></li>
        		<li>
        			<a href="#"><span>Bulk Job</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=<c:out value='${JobNotesForm.appendixId}'/>">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li>
				
	        		<% if( contractDocMenu != "" ) { %>
	        		<li>
	        			<a href="#"><span>Contract Documents</span></a>
	        			<ul>
							<%= contractDocMenu %>
	        			</ul>
	        		</li>
				<% } %>
				
        		<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>">Custom Fields</a></li>
        		<li><a href="AddCustRef.do?typeid=<c:out value='${JobNotesForm.appendixId}'/>&amp;type=Appendix&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>">Customer Reference</a></li>
        		<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>&amp;fromPage=Appendix">External Sales Agent</a></li>
        		<li>
        			<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<c:out value='${JobNotesForm.appendixId}'/>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<c:out value='${JobNotesForm.appendixId}'/>&amp;appendixId=<c:out value='${JobNotesForm.appendixId}'/>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<c:out value='${JobNotesForm.appendixId}'/>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>&amp;initialCheck=true&amp;pageType=jobsites">Job Sites</a></li>
        		<li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=<c:out value='${requestScope.ownerId }'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=<c:out value='${requestScope.ownerId }'/>&amp;type=P&amp;page=appendixdashboard&amp;pageid=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>">Update</a></li>
						
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<c:out value='${JobNotesForm.appendixId}'/>&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>">Manage Special Instructions/Conditions</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>&amp;fromPage=Appendix">Team</a></li>
        		<li><a href="OnlineBid.do?appendixId=<c:out value='${JobNotesForm.appendixId}'/>&amp;fromPage=Appendix">Online Bid</a></li>
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<c:out value='${JobNotesForm.appendixId}'/>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
        		<li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li>
				<li><a href="MSPCCReportAction.do?function=View&amp;appendixid=<c:out value='${JobNotesForm.appendixId}'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>&amp;fromPage=Appendix">MSP C/C Report</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${JobNotesForm.appendixId}'/>&ownerId=<c:out value='${requestScope.ownerId }'/>">Search</center></a></td>
						
  		</li>
  	
	</ul>
</div>
		
		
		<!--  -->
		<div id="menunav">
   
<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<c:out value='${JobNotesForm.appendixId}'/>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<c:out value='${requestScope.appendixid }'/>&amp;viewjobtype=ION&amp;ownerId=7">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<c:out value='${requestScope.appendixid }'/>&amp;type=Appendix&amp;appendixid=<c:out value='${requestScope.appendixid }'/>&amp;viewjobtype=ION&amp;ownerId=7">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<c:out value='${requestScope.appendixid }'/>&amp;viewjobtype=ION&amp;ownerId=7&amp;fromPage=Appendix">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.appendixid }'/>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.appendixid }'/>&amp;appendixId=<c:out value='${requestScope.appendixid }'/>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<c:out value='${requestScope.appendixid }'/>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<c:out value='${requestScope.appendixid }'/>&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites"> View Job Sites</a></li>
        		<!-- <li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<c:out value='${requestScope.appendixid }'/>&viewType=sow&fromType=prm_act&nettype=&ownerId=<c:out value='${requestScope.ownerId }'/>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<c:out value='${requestScope.appendixid }'/>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<c:out value='${requestScope.ownerId }'/>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<c:out value='${requestScope.appendixid }'/>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<c:out value='${requestScope.appendixid }'/>&amp;viewjobtype=ION&amp;ownerId=<c:out value='${requestScope.ownerId }'/>&amp;fromPage=Appendix">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<c:out value='${requestScope.appendixid }'/>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<c:out value='${requestScope.appendixid }'/>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<c:out value='${requestScope.ownerId }'/>">Workflow Checklist Items</a></li>-->
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=0&ownerId=All&msaId=<c:out value='${requestScope.msaId }'/>&searchType=ticket">Search</a>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&amp;viewjobtype=jobSearch&amp;fromLink=jobSearch&amp;appendixid=<c:out value='${requestScope.appendixId }'/>&amp;ownerId=<c:out value='${requestScope.ownerId }'/>&amp;apiAccess=apiAccess">API Access</a>
  		</li>
	</ul>
</li>
</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		<%-- <table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
					<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
					<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>

							<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
									 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
										  <a><span class="breadCrumb1">Job Notes</a>
			    </td>
			</tr>	       
		</table> --%>
		</c:if>
		<c:if test="${ empty JobNotesForm.jobId && empty requestScope.isNetMedX}">
			<%@ include  file="/DashboardVariables.inc" %>
			<%@ include  file="/AppedixDashboardMenu.inc" %>
			<%@ include  file="/AppendixDashboardMenuScript.inc" %>
			
			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
								 	<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${JobNotesForm.appendixId}'/>"/><c:out value='${requestScope.appendixName }'/></a>
									<a><span class="breadCrumb1">Project Notes</a></div>
				        </td>
			       </tr>
		      </table>
	      </c:if>
	      <c:if test="${ empty JobNotesForm.jobId && not empty requestScope.isNetMedX}">
	    <%@ include  file="/DashboardVariables.inc" %>
		<%@ include  file="/NetMedXDashboardMenu.inc" %>
	<div id="menunav">

	<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
         
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
    <li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<c:out value='${requestScope.ownerId }'/>">Search</a>
  		</li>
  		
	</ul>
</li>       
</div>	
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         


<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			            <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${JobNotesForm.appendixId}'/>"/><c:out value='${requestScope.appendixName }'/></a>
										 <a ><span class="breadCrumb1">Project Notes</span></a>
			        </td>
		        </tr>
		         
	      </tbody></table>
	      </td>
	      </tr>
	      </tbody>
	      </table>





		
			 <%-- <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<%if(!nettype.equals("dispatch")){ %>
					<td class=Ntoprow1 id=pop1 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					<%}else{%>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px">Reports</a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>&nettype=<%=nettype %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=640 colspan="2">&nbsp;</td>
					<%} %>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		  <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										 <a ><span class="breadCrumb1">Project Notes</span></a>
								</div>		
			        </td>
		        </tr>
		         
	      </table> --%>
	      <%@ include  file="/NetMedXDashboardMenuScript.inc" %>
	      </c:if>
		<table class="stdPagePadding" cellpadding="0" cellspacing="0">					
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="1">								

									<tr >
										<td >
											<textarea cols="80" rows="8" name="notes" class="textarea"><c:out value='${JobNotesForm.notes}'/></textarea>
										</td>
									</tr>
								</TABLE>
								</TD>
							</tr>
							
							<tr >
								<td >
									<table width="100%">
										<tr valign="top">
											<td width="2%" align="left" valign="bottom">
									    		<input type="button" width="40"  name="update" value="Update" onclick="onUpdate();"  class="button_c1" />
									    	</td>
										
											<td width="2%" align="left" valign="bottom">
									    		<html:reset value="Cancel" onclick="onUpdate();"  styleClass="button_c1"/>
									    	</td>
									    	<td width="96%">&nbsp;</td>
							    		</tr>
							    	</table>
							    </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<c:if test = "${requestScope.jobType eq 'Addendum'}">
				<%@ include  file="/AddendumMenuScript.inc" %>
			</c:if>
				<c:if test = "${requestScope.jobType eq 'Default'}">
				<%@ include  file="/DefaultJobMenuScript.inc" %>
			</c:if>
			<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
				<%@ include  file="/ProjectJobMenuScript.inc" %>
			</c:if>
		</html:form>
	</body>
</html>