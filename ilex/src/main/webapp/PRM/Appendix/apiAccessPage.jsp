<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<bean:define id="latestaddendumid" name="latestaddendumid"
	scope="request" />
<bean:define id="dcJobOwnerList" name="dcJobOwnerList" scope="request" />
<bean:define id="appendixList" name="appendixList" scope="request" />

<%@ include file="/DashboardVariables.inc"%>
<%@ include file="/AppedixDashboardMenu.inc"%>

<%
	int i = 1;
	String bgcolor = "", bgcolorno = "", bgcolortext1 = "", viewType = "", searchPage = "", fromPage = "";
	String testAppendix = "";
	int size = 0;

	if (request.getParameter("msaid") != null)
		msaid = request.getParameter("msaid");
	//else if(request.getAttribute("msaid")!=null)  appendixid = ""+request.getAttribute("msaid");

	if (request.getParameter("appendixid") != null)
		appendixid = request.getParameter("appendixid");
	//else if(request.getAttribute("appendixid")!=null)  appendixid = ""+request.getAttribute("appendixid");

	if (request.getAttribute("listsize") != null)
		size = Integer.parseInt(request.getAttribute("listsize")
				.toString());
	if (request.getAttribute("loginUserId") != null)
		loginUserId = "" + request.getAttribute("loginUserId");
	if (request.getAttribute("viewjobtype") != null)
		viewType = "" + request.getAttribute("viewjobtype");
	//if(request.getAttribute("viewjobtype")!=null) 	viewjobtype = ""+request.getAttribute("viewjobtype"); 
	if (request.getAttribute("searchPage") != null)
		searchPage = "" + request.getAttribute("searchPage").toString();
	if (request.getAttribute("testAppendix") != null)
		testAppendix = ""
				+ request.getAttribute("testAppendix").toString();
%>

<html:html>
<HEAD>
<title></title>
<%@ include file="/Header.inc"%>
<link rel="stylesheet" href="styles/style.css" type="text/css">
<link rel="stylesheet" href="styles/summary.css" type="text/css">
<link rel="stylesheet" href="styles/content.css" type="text/css">
<script language="JavaScript" src="javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/functions.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
</HEAD>
<%@ include file="/NMenu.inc"%>
<%@ include file="/AjaxOwners.inc"%>
<%@ include file="/DashboardStatusScript.inc"%>

<style>

div.tableWrapper {
	margin-top: 0px;
	margin-bottom: 20px;
	margin-right: 10px;
	margin-left: 0px;
	border: 2px solid #e7edf6;
	outline: 2px solid #faf8cc;
	background: #E0E0DC;
	display: inline-block;
	height: 300px;
	overflow: auto;
}

div.tableWrapper table {
	table-layout: fixed;
}

div.tableWrapper table tr td {
	white-space: wrap;
	word-break: break-all;
	word-wrap: break-word;
	font-size:11px;
}

div.tableWrapper table tr:nth-child(2n+1) {
	background-color: #ffffff;
}

div.tableWrapper table tr:nth-child(2n+2) {
	background-color: #e8eef7;
}

div.tableWrapper table tr:nth-child(n+1) td {
	border-bottom: 1px solid transparent;
	font-size: 11px;
	font-weight: normal;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	vertical-align: top;
	text-align: left;
	padding-left: 4px;
	padding-top: 2px;
	padding-right: 4px;
	padding-bottom: 2px;
}

div.tableWrapper table tr:nth-child(n+1) td:last-child:hover {
	background-image: url('../images/highlight.jpg');
	background-repeat: no-repeat;
	background-position: center;
	background-size: 40% 60%;
	margin-right: 4px;
}

div.tableWrapper table tr:nth-child(n+1):hover td {
	border-bottom: 1px solid #d7c960;
}




</style>


<script>
    
//Added By Amit:Added For pop up window for all owner of the MSA
var canNotSendNonApprovedAddendum = '<bean:message bundle = "PRM" key = "prm.addendum.sendemail.noprivilegestatus"/>';
function popUpOwnerScreen() {
	str = "AssignOwner.do?&appendix_Id=<%=appendixid%>&function=view&changeFilter=Y&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&from=ownerforaappendix&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600 , height = 400 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();
	return true;
}
function uploadjob(jobid) {
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromJobDashboard&ref1=view&path=appendixdashboard&appendix_Id=<%=appendixid%>&Id="+jobid+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
function addaddendumjob(ref) {
	document.forms[0].target = "_self";
	document.forms[0].action = "JobUpdateAction.do?ref="+ref+"&appendixid=<%=appendixid%>";
	document.forms[0].target = "ilexmain";
	document.forms[0].submit();
	return true;	
}
function view(v) {
    if(v=='html')   document.forms[0].target = "_blank";
	else  	        document.forms[0].target = "_self";
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PRM&Id=<%=appendixid%>&jobId=<%=latestaddendumid%>";
	document.forms[0].submit();
	return true;	
}
function viewaddendumpdf(v ) {
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PRM&Id=<%=appendixid%>";
	document.forms[0].submit();
}
function viewdocuments() {
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdashboarddocument&ref1=view&Id=<%=appendixid%>&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
function toggleMenuSection(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){ }
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) 	thisImage.src = "images/Collapse.gif";    
	else thisImage.src = "images/Expand.gif";
}
function toggleDiv(divName) {
	 var tcookie = document.cookie;
     var st = tcookie.indexOf ("A1Db");
     var cookievalue ="";
	  if(st!= -1) {
	      st = st+3;
	      st = tcookie.indexOf ("=", st);
	      en = tcookie.indexOf (";", st);
		  if (en > st) cookievalue = tcookie.substring(st+1, en); //div's that is reopen
	 }
	var id = document.getElementById(divName);
	if( id.style.display == "none" ) {
			id.style.display = "block";
			//add it to the existing cookie opendiv paraneter
			if (cookievalue.length>0)	cookievalue = cookievalue+','+divName;
			else	cookievalue = divName;
			if(cookievalue!="") { // Save as cookie
				document.cookie = "ADb="+ cookievalue+" ; ";
          } }
	else {
			id.style.display = "none";	
			var newcookievalue='';
			//remove this div from the opendiv cookie
			if (cookievalue.length>0) {
				var array = cookievalue.split(',');
				if (array.length>1) {
					for(i=0;i<array.length;i++) {
						if(array[i]==divName) { }
						else {
							if(newcookievalue.length > 0 )
								newcookievalue = newcookievalue+','+array[i];
							else
								newcookievalue = array[i];	
						}
					}
				}
				if(newcookievalue!="") { // Save as cookie
			 		document.cookie = "ADb="+ newcookievalue+" ; ";
			 	}
			}
		}
	if( divName == 'div_1')
	{
		id = document.getElementById('div_13');
		if( id.style.display == "none" ) id.style.display = "block";
		else id.style.display = "none";	
		id = document.getElementById('div_11');
		if( id.style.display == "none" ) id.style.display = "block";
		else id.style.display = "none";	
	}
}
function toggleMenuSectionView(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	action = "toggleType = toggleDivView('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) 	thisImage.src = "images/Collapse.gif";
	else thisImage.src = "images/Expand.gif";
}
function toggleDivView(divName) {
	 var tableRow = document.getElementsByTagName('tr');
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if (tableRow[k].getAttributeNode('id').value==divName) {
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	}
}
// Added Start:By Amit This will be called on each href that opens other form
function createCookie()
{
 var tableRow = document.getElementsByTagName('tr');
  var openDiv="";
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if((tableRow[k].getAttributeNode('id').value != "")&&(tableRow[k].getAttributeNode('id').value != "account"))
		{
			if(tableRow[k].style.display == "block" ){
			openDiv=openDiv+tableRow[k].getAttributeNode('id').value+",";
			}
		}
	}
		if(openDiv!="") { // Save as cookie
			 document.cookie = "ADb="+ openDiv+" ; ";
		 }
}
function createCookie(lastDivForFocus) {
 		document.cookie = "A1Db="+ lastDivForFocus+" ; ";
}
//following function would be called on OnLoad
function CookieGetOpenDiv() {
  var tcookie = document.cookie;
  var unique;
  var st = tcookie.indexOf ("ADb");
  var cookievalue ="";
  if(st!= -1) {
      st = st+3;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);
	  if (en > st) {
          cookievalue = tcookie.substring(st+1, en);
        }
       if(cookievalue.length>0) {
		var opendiv = cookievalue.split(',');
		if(opendiv.length>0) {
		  for (var j = 0; j < opendiv.length; j++) {
		   		if(eval("document.getElementById('"+opendiv[j]+"')")!=null)	{ //Added This if condition by amit
		  				eval("document.getElementById('"+opendiv[j]+"')").style.display = "block";
						unique = opendiv[j].substring(opendiv[j].indexOf("_")+1,opendiv[j].length);
						action = "thisImage = document.getElementById('img_" + unique +"');";
						eval(action);
					if (document.getElementById('div_' + unique).offsetHeight > 0) {
							thisImage.src = "images/Collapse.gif";
		   			} else {
							thisImage.src = "images/Expand.gif";
					}
				}
				if(opendiv[j]=='div_1') {
						if(document.forms[0].viewjobtype.value!='jobSearch'){
						document.getElementById('div_11').style.display = "block";
						document.getElementById('div_13').style.display = "block";
						}
				}												
		  }
		}
	}
  }
	  openDiv = "";
	  //Save as cookie
	  document.cookie = "A1Db="+ openDiv+" ; "; 
	  //Start:Added For Focus		
	  var st1 = tcookie.indexOf ("A1Db");
	  var cookievalue1 ="";
	  if(st1!= -1) {
	      st1 = st1+4;
	      st1 = tcookie.indexOf ("=", st1);
	      en = tcookie.indexOf (";", st1);
		  if (en > st1) {
	          cookievalue1 = tcookie.substring(st1+1, en);
	        }
		}
		 if(cookievalue1!="") {  
		  		if(document.getElementById(cookievalue1)!=null) {
		  		if(document.getElementById(cookievalue1).style.visibility == 'visible')
		 	      	document.getElementById(cookievalue1).focus();
		    	}
		    
		}
		document.cookie = "A1Db="+ openDiv+" ; ";
}
//End By Amit

function checkTrue(){
	
	
	if (document.getElementById("aPIAccessPasswordId").innerHTML != ""){
		document.getElementById('apiAccessChkBoxId2').checked = true;
		document.getElementById("apiAccessChkBoxId").value = "true";
		
	}else {
		
		document.getElementById('apiAccessChkBoxId2').checked = false;
		document.getElementById("apiAccessChkBoxId").value = "false";
	}
}


</script>
<%
	int addStatusRow = -1;
		int addOwnerRow = -1;
		boolean addStatusSpace = false;
		boolean addOwnerSpace = false;
		String checkowner = null;
		String checkstatus = null;
		int rowHeight = 120;

		int jobOwnerListSize = 0;
		if (request.getAttribute("jobOwnerListSize") != null)
			jobOwnerListSize = Integer.parseInt(request.getAttribute(
					"jobOwnerListSize").toString());
%>
<BODY
	onLoad="MM_preloadImages('images/Expand.gif','images/Collapse.gif');CookieGetOpenDiv();leftAdjLayers(); checkTrue();">
	<html:form action="AppendixHeader">
		<html:hidden property="viewjobtype" />
		<html:hidden property="ownerId" />
		<html:hidden property="inv_type" />
		<html:hidden property="inv_id" />
		<html:hidden property="inv_rdofilter" />
		<html:hidden property="invoice_Flag" />
		<html:hidden property="invoiceno" />
		<html:hidden property="partner_name" />
		<html:hidden property="powo_number" />
		<html:hidden property="from_date" />
		<html:hidden property="to_date" />
		<html:hidden property="msaId" />
		<html:hidden property="fromPage" />
		<html:hidden property="jobOwnerOtherCheck" />
		<html:hidden property="function" value= "view" />
		<!--  Menu id  -->
			
		<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li><a href="JobEditAction.do?ref=inscopeview&amp;appendix_Id=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;Job_Id=0">Add Job</a></li>
        		<li>
        			<a href="#"><span>Bulk Job</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=<%= appendixid %>">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=<%= appendixid %>">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li>
				
	        		<% if( contractDocMenu != "" ) { %>
	        		<li>
	        			<a href="#"><span>Contract Documents</span></a>
	        			<ul>
							<%= contractDocMenu %>
	        			</ul>
	        		</li>
				<% } %>
				
        		<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Custom Fields</a></li>
        		<li><a href="AddCustRef.do?typeid=<%= appendixid %>&amp;type=Appendix&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Customer Reference</a></li>
        		<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">External Sales Agent</a></li>
        		<li>
        			<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;initialCheck=true&amp;pageType=jobsites">Job Sites</a></li>
        		<li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=<%= appendixid %>&amp;type=P&amp;page=appendixdashboard&amp;pageid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Update</a></li>
						
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Special Instructions/Conditions</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">Team</a></li>
        		<li><a href="OnlineBid.do?appendixId=<%= appendixid %>&amp;fromPage=Appendix">Online Bid</a></li>
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
        		<li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li>
				<li><a href="MSPCCReportAction.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">MSP C/C Report</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid %>"><center>Search</center></a></td>
						
  		</li>
  	
	</ul>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody>
 <tr>
    <td width="100%" valign="top">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr>
		   									<td colspan="7" height="1"><h2>
											<bean:message bundle="PRM" key="prm.appendixdashboardfor" />
											&nbsp;
											<bean:write name="AppendixHeaderForm" property="msaname" />
											&nbsp;->&nbsp;
											<bean:message bundle="PRM" key="prm.dashboard.appendix" />
											&nbsp;
											<bean:write name="AppendixHeaderForm" property="appendixname" />
										</h2>
										
										
										</td>
										
				</tr>						
										      
	      </tbody>
	      </table>
	</td>
	<!--  below area  ---> 
							<td width="35" valign="top"><img id="content_head_02"
							src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
						<td valign="top" width="155">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="155" height="39" class="headerrow" colspan="2">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="150">
													<table width="100%" cellpadding="0" cellspacing="3"
														border="0">
														<tr>
															<td><a href="#" class="imgstyle"><img
																	src="images/Icon_Scheduled.gif" width="31" alt="Month"
																	id="Image10" title="Scheduled" height="30" border="0"
																	onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)"
																	onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)"
																	onclick="setStatus('II');" /></a></td>
															<td><a href="#" class="imgstyle"><img
																	src="images/Icon_Schedule_Overdued.gif" width="31"
																	alt="Month" id="Image10" title="Scheduled OverDue"
																	height="30" border="0"
																	onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)"
																	onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)"
																	onclick="setStatus('IO');" /></a></td>
															<td><a href="#" class="imgstyle"><img
																	src="images/Icon_Onsite.gif" width="31" alt="Month"
																	id="Image10" title="OnSite" height="30" border="0"
																	onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)"
																	onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)"
																	onclick="setStatus('ION');" /></a></td>
															<td><a href="#" class="imgstyle"><img
																	src="images/Icon_Ofsite.gif" width="31" alt="Month"
																	id="Image10" title="Offsite" height="30" border="0"
																	onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)"
																	onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)"
																	onclick="setStatus('IOF');" /></a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td height="21" background="images/content_head_04.jpg"
										width="140">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="70%" colspan="2">&nbsp;</td>
												<td nowrap="nowrap" colspan="2">
													<div id="featured1">
														<div id="featured">
															<li name="closeNow" id="closeNow"><a
																href="javascript:void(0)" title="Open View"
																onClick="ShowDiv();">My Views <img
																	src="images/showFilter.gif" title="Open View"
																	border="0" onClick="ShowDiv();" /></a><a
																href="javascript:void(0)" class="imgstyle"><img
																	src="images/offFilter2.gif" border="0"
																	title="Close View" onClick="hideDiv();" /></a></li>
														</div>
													</div> <span>
														<div id="filter" class="divstyle">
															<table width="250" cellpadding="0" class="divtable">
																<tr>
																	<td width="50%" valign="top"><table width="100%"
																			cellpadding="0" cellspacing="0" border="0">
																			<tr>
																				<td colspan="3" class="filtersCaption">Status
																					View</td>
																			</tr>
																			<tr>
																				<td></td>
																				<td class="tabNormalText"><logic:present
																						name="jobStatusList" scope="request">
																						<logic:iterate id="list" name="jobStatusList">
																							<bean:define id="statusName" name="list"
																								property="statusdesc" type="java.lang.String" />
																							<bean:define id="statusId" name="list"
																								property="statusid" type="java.lang.String" />

																							<%
																								checkstatus = "javascript: checkingStatus('"
																																+ statusId + "');";
																														if ((!statusName.equals("Inwork"))
																																&& (!statusName.equals("Complete"))
																																&& (!statusName.equals("Closed"))
																																&& (!statusName.equals("Cancelled"))
																																&& (!statusName.equals("Hold"))) {
																															addStatusRow++;
																															addStatusSpace = true;
																														}
																							%>
																							<%
																								if (addStatusSpace) {
																							%>&nbsp;&nbsp;&nbsp;&nbsp;<%
																								}
																							%>
																							<html:multibox property="jobSelectedStatus"
																								onclick="<%=checkstatus%>">
																								<bean:write name="list" property="statusid" />
																							</html:multibox>
																							<bean:write name="list" property="statusdesc" />
																							<br>
																							<%
																								addStatusSpace = false;
																							%>
																						</logic:iterate>
																					</logic:present> <img src="images/hrfilterLine.gif" width="100px"
																					class="imagefilter" />
																					<table cellpadding="0">
																						<tr>
																							<td class="tabNormalText"><html:radio
																									name="AppendixHeaderForm"
																									property="jobMonthWeekCheck" value="week">This Week</html:radio></td>
																						</tr>
																						<tr>
																							<td class="tabNormalText"><html:radio
																									name="AppendixHeaderForm"
																									property="jobMonthWeekCheck" value="month">This Month</html:radio></td>
																						</tr>
																						<tr>
																							<td class="tabNormalText"><html:radio
																									name="AppendixHeaderForm"
																									property="jobMonthWeekCheck" value="clear">Clear</html:radio></td>
																						</tr>
																					</table></td>
																			</tr>
																		</table></td>
																	<td><img src="images/filterLine.gif" width="1px"
																		height="<%=rowHeight%>" class="imagefilter" /></td>
																	<td width="50%" valign="top"><table width="100%"
																			cellpadding="0" cellspacing="0" border="0">
																			<tr>
																				<td colspan="3" class="filtersCaption">User
																					View</td>
																			</tr>
																			<tr>
																				<td class="tabNormalText"><span
																					id="OwnerSpanId"> <logic:present
																							name="jobOwnerList" scope="request">
																							<logic:iterate id="olist" name="jobOwnerList">
																								<bean:define id="jobOwnerName" name="olist"
																									property="ownerName" type="java.lang.String" />
																								<bean:define id="jobOwnerId" name="olist"
																									property="ownerId" type="java.lang.String" />
																								<%
																									checkowner = "javascript: checkingOwner('"
																																	+ jobOwnerId + "');";
																															if ((!jobOwnerName.equals("Me"))
																																	&& (!jobOwnerName.equals("All"))
																																	&& (!jobOwnerName.equals("Other"))) {
																																addOwnerRow++;
																																addOwnerSpace = true;
																															}
																															if (addOwnerRow == 0) {
																								%>
																								<br />
																								<span class="ownerSpan"> &nbsp;<bean:message
																										bundle="pm" key="msa.tabular.other" /> <br />
																									<%
																										}
																									%> <%
 	if (addOwnerSpace) {
 %>&nbsp;&nbsp;&nbsp;&nbsp;<%
 	}
 %> <html:multibox property="jobSelectedOwners"
																										onclick="<%=checkowner%>">
																										<bean:write name="olist" property="ownerId" />
																									</html:multibox> <bean:write name="olist" property="ownerName" />
																									<br />
																							</logic:iterate></span> </logic:present> </span></td>
																			</tr>

																			<tr>
																				<td colspan="3"><img src="images/spacer.gif"
																					height="34" /></td>
																			</tr>
																			<tr>
																				<td colspan="3" align="right"><a href="#"><img
																						src="images/showBtn.gif" width="47" height="15"
																						border="0" class="divbutton"
																						onclick="changeFilter();" /><br></a></td>
																			</tr>
																			<tr>
																				<td colspan="3"><img src="images/spacer.gif"
																					width="2" /></td>
																			</tr>
																			<tr>
																				<td colspan="3" align="right"><a href="#"><img
																						src="images/reset.gif" width="47" height="15"
																						border="0" class="divbutton"
																						onclick="resetFilter();" /><br></a></td>
																			</tr>
																			<tr>
																				<td colspan="3"><img src="images/spacer.gif"
																					height="8" /></td>
																			</tr>
																		</table></td>
																</tr>
															</table>
														</div>
												</span>
												</td>

											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
	
	
	
				
	</tr>
	</tbody>
	</table>		


		<!--  -->
		<logic:notEqual name="AppendixHeaderForm" property="fromPage"
			value="msa">
			<logic:notEqual name="AppendixHeaderForm" property="fromPage"
				value="viewselector">
				<html:hidden property="appendixid" />
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">

					<tr>
						<td valign="top" width="100%">
						<!--  Change for Api Access  -->
						
						
						
						
						
						<!-- End Of Change  -->
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td id="pop1" width="120" class="Ntoprow1" align="center"><a
										href="#"
										onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"
										onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)"
										class="menufont" style="width: 120px"><center>Manage</center></a></td>
									<td id="pop2" width="120" class="Ntoprow1" align="center"><a
										href="#"
										onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"
										onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)"
										class="menufont" style="width: 120px"><center>
												<bean:message bundle="PRM" key="prm.appendix.reports" />
											</center></a></td>
									<td id="pop5" width="120" class="Ntoprow1" align="center"><a
										href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="AppendixHeaderForm" property="appendixid"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&msaId=<bean:write name="AppendixHeaderForm" property="msaId"/>&fromPage=viewselector"
										class="menufont" style="width: 120px"><center>Search</center></a></td>
									<td id="pop3" width="120" class="Ntoprow1" align="center"><a
										href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="AppendixHeaderForm" property="appendixid"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&apiAccess=apiAccess"
										class="menufont" style="width: 120px"><center>API
												Access</center></a></td>
								</tr>
								<tr>
									<td background="images/content_head_04.jpg" height="21"
										colspan="4" width="100%">
										<div id="breadCrumb">&nbsp;</div>
									</td>
								</tr>
								<tr>
									<td colspan="7" height="1"><h2>
											<bean:message bundle="PRM" key="prm.appendixdashboardfor" />
											&nbsp;
											<bean:write name="AppendixHeaderForm" property="msaname" />
											&nbsp;->&nbsp;
											<bean:message bundle="PRM" key="prm.dashboard.appendix" />
											&nbsp;
											<bean:write name="AppendixHeaderForm" property="appendixname" />
										</h2></td>
								</tr>
							</table>
						</td>

						<td width="35" valign="top"><img id="content_head_02"
							src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
						<td valign="top" width="155">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="155" height="39" class="headerrow" colspan="2">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="150">
													<table width="100%" cellpadding="0" cellspacing="3"
														border="0">
														<tr>
															<td><a href="#" class="imgstyle"><img
																	src="images/Icon_Scheduled.gif" width="31" alt="Month"
																	id="Image10" title="Scheduled" height="30" border="0"
																	onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)"
																	onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)"
																	onclick="setStatus('II');" /></a></td>
															<td><a href="#" class="imgstyle"><img
																	src="images/Icon_Schedule_Overdued.gif" width="31"
																	alt="Month" id="Image10" title="Scheduled OverDue"
																	height="30" border="0"
																	onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)"
																	onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)"
																	onclick="setStatus('IO');" /></a></td>
															<td><a href="#" class="imgstyle"><img
																	src="images/Icon_Onsite.gif" width="31" alt="Month"
																	id="Image10" title="OnSite" height="30" border="0"
																	onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)"
																	onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)"
																	onclick="setStatus('ION');" /></a></td>
															<td><a href="#" class="imgstyle"><img
																	src="images/Icon_Ofsite.gif" width="31" alt="Month"
																	id="Image10" title="Offsite" height="30" border="0"
																	onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)"
																	onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)"
																	onclick="setStatus('IOF');" /></a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td height="21" background="images/content_head_04.jpg"
										width="140">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="70%" colspan="2">&nbsp;</td>
												<td nowrap="nowrap" colspan="2">
													<div id="featured1">
														<div id="featured">
															<li name="closeNow" id="closeNow"><a
																href="javascript:void(0)" title="Open View"
																onClick="ShowDiv();">My Views <img
																	src="images/showFilter.gif" title="Open View"
																	border="0" onClick="ShowDiv();" /></a><a
																href="javascript:void(0)" class="imgstyle"><img
																	src="images/offFilter2.gif" border="0"
																	title="Close View" onClick="hideDiv();" /></a></li>
														</div>
													</div> <span>
														<div id="filter" class="divstyle">
															<table width="250" cellpadding="0" class="divtable">
																<tr>
																	<td width="50%" valign="top"><table width="100%"
																			cellpadding="0" cellspacing="0" border="0">
																			<tr>
																				<td colspan="3" class="filtersCaption">Status
																					View</td>
																			</tr>
																			<tr>
																				<td></td>
																				<td class="tabNormalText"><logic:present
																						name="jobStatusList" scope="request">
																						<logic:iterate id="list" name="jobStatusList">
																							<bean:define id="statusName" name="list"
																								property="statusdesc" type="java.lang.String" />
																							<bean:define id="statusId" name="list"
																								property="statusid" type="java.lang.String" />

																							<%
																								checkstatus = "javascript: checkingStatus('"
																																+ statusId + "');";
																														if ((!statusName.equals("Inwork"))
																																&& (!statusName.equals("Complete"))
																																&& (!statusName.equals("Closed"))
																																&& (!statusName.equals("Cancelled"))
																																&& (!statusName.equals("Hold"))) {
																															addStatusRow++;
																															addStatusSpace = true;
																														}
																							%>
																							<%
																								if (addStatusSpace) {
																							%>&nbsp;&nbsp;&nbsp;&nbsp;<%
																								}
																							%>
																							<html:multibox property="jobSelectedStatus"
																								onclick="<%=checkstatus%>">
																								<bean:write name="list" property="statusid" />
																							</html:multibox>
																							<bean:write name="list" property="statusdesc" />
																							<br>
																							<%
																								addStatusSpace = false;
																							%>
																						</logic:iterate>
																					</logic:present> <img src="images/hrfilterLine.gif" width="100px"
																					class="imagefilter" />
																					<table cellpadding="0">
																						<tr>
																							<td class="tabNormalText"><html:radio
																									name="AppendixHeaderForm"
																									property="jobMonthWeekCheck" value="week">This Week</html:radio></td>
																						</tr>
																						<tr>
																							<td class="tabNormalText"><html:radio
																									name="AppendixHeaderForm"
																									property="jobMonthWeekCheck" value="month">This Month</html:radio></td>
																						</tr>
																						<tr>
																							<td class="tabNormalText"><html:radio
																									name="AppendixHeaderForm"
																									property="jobMonthWeekCheck" value="clear">Clear</html:radio></td>
																						</tr>
																					</table></td>
																			</tr>
																		</table></td>
																	<td><img src="images/filterLine.gif" width="1px"
																		height="<%=rowHeight%>" class="imagefilter" /></td>
																	<td width="50%" valign="top"><table width="100%"
																			cellpadding="0" cellspacing="0" border="0">
																			<tr>
																				<td colspan="3" class="filtersCaption">User
																					View</td>
																			</tr>
																			<tr>
																				<td class="tabNormalText"><span
																					id="OwnerSpanId"> <logic:present
																							name="jobOwnerList" scope="request">
																							<logic:iterate id="olist" name="jobOwnerList">
																								<bean:define id="jobOwnerName" name="olist"
																									property="ownerName" type="java.lang.String" />
																								<bean:define id="jobOwnerId" name="olist"
																									property="ownerId" type="java.lang.String" />
																								<%
																									checkowner = "javascript: checkingOwner('"
																																	+ jobOwnerId + "');";
																															if ((!jobOwnerName.equals("Me"))
																																	&& (!jobOwnerName.equals("All"))
																																	&& (!jobOwnerName.equals("Other"))) {
																																addOwnerRow++;
																																addOwnerSpace = true;
																															}
																															if (addOwnerRow == 0) {
																								%>
																								<br />
																								<span class="ownerSpan"> &nbsp;<bean:message
																										bundle="pm" key="msa.tabular.other" /> <br />
																									<%
																										}
																									%> <%
 	if (addOwnerSpace) {
 %>&nbsp;&nbsp;&nbsp;&nbsp;<%
 	}
 %> <html:multibox property="jobSelectedOwners"
																										onclick="<%=checkowner%>">
																										<bean:write name="olist" property="ownerId" />
																									</html:multibox> <bean:write name="olist" property="ownerName" />
																									<br />
																							</logic:iterate></span> </logic:present> </span></td>
																			</tr>

																			<tr>
																				<td colspan="3"><img src="images/spacer.gif"
																					height="34" /></td>
																			</tr>
																			<tr>
																				<td colspan="3" align="right"><a href="#"><img
																						src="images/showBtn.gif" width="47" height="15"
																						border="0" class="divbutton"
																						onclick="changeFilter();" /><br></a></td>
																			</tr>
																			<tr>
																				<td colspan="3"><img src="images/spacer.gif"
																					width="2" /></td>
																			</tr>
																			<tr>
																				<td colspan="3" align="right"><a href="#"><img
																						src="images/reset.gif" width="47" height="15"
																						border="0" class="divbutton"
																						onclick="resetFilter();" /><br></a></td>
																			</tr>
																			<tr>
																				<td colspan="3"><img src="images/spacer.gif"
																					height="8" /></td>
																			</tr>
																		</table></td>
																</tr>
															</table>
														</div>
												</span>
												</td>

											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table> --%>
				<%@ include file="/AppedixDashboardMenuScript.inc"%>
			</logic:notEqual>

</logic:notEqual>
			<table>
				<tr>

					<td
						style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold;">&nbsp;&nbsp;
						<html:checkbox property="apiCheckBox" 
							styleId="apiAccessChkBoxId2" onclick="changeValue(this.checked)"></html:checkbox>
						API Access <html:hidden property="apiAccessChkBox"
							styleId="apiAccessChkBoxId" value="false" /> &nbsp;&nbsp;
							<html:submit property="apiAccessBtn" styleId="apiAccessBtn" onclick="return isCheckBoxSelected();" styleClass="button" style="margin-left: 5px;cursor:pointer">Get Access</html:submit>
							
							
						<%-- 				<bean:message bundle="PRM" key="prm.appendix.closed"/> --%>
					</td>
					<!-- 						</tr> -->
					<!-- 						<tr> -->

					<%-- 							<td>&nbsp;&nbsp;<html:submit property="apiAccessBtn" styleId="apiAccessBtn" disabled="true" --%>
					<%-- 									styleClass="button" style="margin-left: 5px;">Get Access</html:submit></td> --%>
				</tr>
				<tr>
					<td>

						<div class="tableWrapper"
							style="height: 125px;; overflow: inherit;">
							<div style="width: 445px; margin: 0px;" class="containerTitle">
								<div style="padding: 4px; margin: 0px;" class="divFloatLeft">
									NetMedX Appendix:
									<bean:write name="AppendixHeaderForm" property="appendixname" />
								</div>
							</div>

							<!-- 				<table class="container display dataTable" border="0" width="480px" -->
							<!-- 				<thead> -->
							<!-- 							<tr> -->
							<!-- 								<th class="headStyling" colspan="2" style="height: 13px;"><c:out -->
							<%-- 										value="${MasterSiteForm.siteName}" /></th> --%>
							<!-- 							</tr> -->
							<!-- 						</thead> -->

							<!-- 				</table> -->

							<div class="dataTables_wrapper"
								style="overflow-y: auto; width: 480px; overflow-x: -moz-hidden-unscrollable; height: 100px;">
								<table class="container display dataTable" border="0"
									width="480px" cellpadding="0" cellspacing="1" align="left">
									<!-- 						<thead> -->
									<!-- 							<tr> -->
									<%-- 								<th class="headStyling" colspan="2" style="height: 13px;"><c:out --%>
									<%-- 										value="${MasterSiteForm.siteName}" /></th> --%>
									<!-- 							</tr> -->
									<!-- 						</thead> -->
									<tbody>
										<tr>
											<td valign="middle">API Doc</td>
											<td valign="middle">http://</td>
										</tr>
										<tr>
											<td valign="middle">User Name</td>
											<%-- 								<td valign="middle"><c:out  value="${MasterSiteForm.phoneNumber}" /></td> --%>
											<td valign="middle"><bean:write
													name="AppendixHeaderForm" property="apiUserName" /></td>
										</tr>
										<tr>
											<td valign="middle">Password</td>
											<td valign="middle" id="aPIAccessPasswordId"><bean:write
													name="AppendixHeaderForm" property="aPIAccessPassword"  /></td>
										</tr>
										<tr>
											<td valign="middle">Project Id</td>
											<!-- 								<td valign="middle"><c:out -->
											<%-- 										value="${MasterSiteForm.primaryContactName}" /></td> --%>
											<td valign="middle"><bean:write
													name="AppendixHeaderForm" property="appendixid" /></td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>


					</td>

				</tr>
			</table>
			
</html:form>
</BODY>


<script>
function viewpowo(jobid, powoid, type) {
    window.location.href = "POWOAction.do?jobid="+jobid+"&function_type=view&order_type="+type+"&powoid="+powoid;
	return false;	
}
var str = '';
function sortwindow() {
	/*for filter sort*/
	var currentstatus=document.forms[0].viewjobtype.value;
	if(currentstatus=="%") {
		currentstatus="A";
	}

	if(document.forms[0].viewjobtype.value == 'jobSearch') {
		if(document.forms[0].dateOnSite.checked)
			document.forms[0].dateOnSite.value ="OS";
		else
			document.forms[0].dateOnSite.value ="";
			
		if(document.forms[0].dateOffSite.checked)
			document.forms[0].dateOffSite.value ="FS";
		else
			document.forms[0].dateOffSite.value ="";
			
		if(document.forms[0].dateComplete.checked)
			document.forms[0].dateComplete.value ="CO";
		else
			document.forms[0].dateComplete.value ="";	
			
		if(document.forms[0].dateClosed.checked)
			document.forms[0].dateClosed.value ="CL";
		else
			document.forms[0].dateClosed.value ="";	

	
	}	

		str = 'SortAction.do?Type=prj_job&appendixid=<%=appendixid%>&status='+currentstatus+'&ownerId='+document.forms[0].ownerId.value+'&viewjobtype='+document.forms[0].viewjobtype.value;
		p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
		/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
	return true;
}
function Backaction() {
	document.forms[0].target = "_self";
	document.forms[0].action = "InvoiceJobDetail.do?type="+document.forms[0].inv_type.value+"&id="+document.forms[0].inv_id.value+"&rdofilter="+document.forms[0].inv_rdofilter.value+"&invoice_Flag="+document.forms[0].invoice_Flag.value+"&invoiceno="+document.forms[0].invoiceno.value+"&partner_name="+document.forms[0].partner_name.value+"&powo_number="+document.forms[0].powo_number.value+"&from_date="+document.forms[0].from_date.value+"&to_date="+document.forms[0].to_date.value;
	document.forms[0].submit();
	return true;
}
function searchJobs()
{
	
	//alert("Hello from Appendix");	
// 	if(Date.parse(document.forms[0].job_search_date_from.value)>Date.parse(document.forms[0].job_search_date_to.value))
// 	{
// 	alert("\"From Date\" should be less than \"To Date\"");
// 	return false;
// 	}
		if(document.forms[0].dateOnSite.checked)
			document.forms[0].dateOnSite.value ="OS";
		else
			document.forms[0].dateOnSite.value ="";
			
		if(document.forms[0].dateOffSite.checked)
			document.forms[0].dateOffSite.value ="FS";
		else
			document.forms[0].dateOffSite.value ="";
			
		if(document.forms[0].dateComplete.checked)
			document.forms[0].dateComplete.value ="CO";
		else
			document.forms[0].dateComplete.value ="";	
			
		if(document.forms[0].dateClosed.checked)
			document.forms[0].dateClosed.value ="CL";
		else
			document.forms[0].dateClosed.value ="";	

	document.forms[0].action ="AppendixHeader.do?function=view&viewjobtype=jobSearch&appendixid=<%=appendixid%>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&fromPage="+document.forms[0].fromPage.value;	
	return true;

}

function emptySearchCriteria()
{
	
		document.forms[0].job_search_date_to.value="";
		document.forms[0].job_search_date_from.value="";
		document.forms[0].dateOnSite.checked =false;
		document.forms[0].dateOffSite.checked =false;
		document.forms[0].dateComplete.checked =false;
		document.forms[0].dateClosed.checked =false;
		document.forms[0].job_search_name.value ="";	
		document.forms[0].job_search_site_number.value ="";	
		document.forms[0].job_search_city.value ="";	
		document.forms[0].job_search_state.value ="0";	
		document.forms[0].job_search_owner.value =" ";

}

function changeOwner()
{
document.forms[0].job_search_owner.value = "";
document.forms[0].action = "AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid="+document.forms[0].appendixid.value+"&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&fromPage=msa&msaId=<bean:write name="AppendixHeaderForm" property="msaId"/>";
document.forms[0].submit();
return true;
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="AppendixHeader.do?function=view&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	alert("from api Appendix");
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="AppendixHeader.do?function=view&resetList=something";
	document.forms[0].submit();
	return true;

}
function changeValue(value){
 	if(value){
 		document.getElementById("apiAccessChkBoxId").value = "true";
 	}
 	else{
 		document.getElementById("apiAccessChkBoxId").value = "false";
	}	OpenTicket()
	
}
function isCheckBoxSelected(){
	
	if(document.getElementById("aPIAccessPasswordId").innerHTML != ""
	&& document.getElementById("apiAccessChkBoxId2").checked == true)	{		
				alert("Password is already generated");
				return false;
	
		
	}
if(document.getElementById("aPIAccessPasswordId").innerHTML == "" && document.getElementById("apiAccessChkBoxId2").checked == false){		
		alert("Select the checkbox first");
		return false; 		
 	}
// return searchJobs();
 	
}


</script>
</html:html>
