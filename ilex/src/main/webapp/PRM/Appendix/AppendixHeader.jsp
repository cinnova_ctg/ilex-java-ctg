<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %> <%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %> <%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id = "latestaddendumid" name = "latestaddendumid" scope = "request" />
<bean:define id="dcJobOwnerList" name="dcJobOwnerList" scope="request"/>
<bean:define id="appendixList" name="appendixList" scope="request"/>

<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>

<% int i=1;
String bgcolor="", bgcolorno="", bgcolortext1="", viewType = "",  searchPage = "", fromPage = "";
String testAppendix = "";
int size=0;

if( request.getParameter( "msaid" ) != null ) msaid = request.getParameter( "msaid" );
if( request.getParameter( "appendixid" ) != null ) appendixid = request.getParameter( "appendixid" );
if(request.getAttribute("listsize")!=null)  size=Integer.parseInt(request.getAttribute("listsize").toString()); 
if(request.getAttribute("loginUserId")!=null) 	loginUserId = ""+request.getAttribute("loginUserId");  
if(request.getAttribute("viewjobtype")!=null) 	viewType = ""+request.getAttribute("viewjobtype"); 
if(request.getAttribute("searchPage")!=null) searchPage = ""+request.getAttribute("searchPage").toString(); 
if(request.getAttribute("testAppendix")!=null) testAppendix = ""+request.getAttribute("testAppendix").toString(); 
%>

<html:html>
<HEAD>
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/functions.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</HEAD>
<%@ include  file="/NMenu.inc" %>
<%@ include  file="/AjaxOwners.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<script>
    
//Added By Amit:Added For pop up window for all owner of the MSA
var canNotSendNonApprovedAddendum = '<bean:message bundle = "PRM" key = "prm.addendum.sendemail.noprivilegestatus"/>';
function popUpOwnerScreen() {
	str = "AssignOwner.do?&appendix_Id=<%= appendixid%>&function=view&changeFilter=Y&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&from=ownerforaappendix&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600 , height = 400 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();
	return true;
}
function uploadjob(jobid) {
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromJobDashboard&ref1=view&path=appendixdashboard&appendix_Id=<%= appendixid%>&Id="+jobid+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
function addaddendumjob(ref) {
	document.forms[0].target = "_self";
	document.forms[0].action = "JobUpdateAction.do?ref="+ref+"&appendixid=<%= appendixid%>";
	document.forms[0].target = "ilexmain";
	document.forms[0].submit();
	return true;	
}
function view(v) {
    if(v=='html')   document.forms[0].target = "_blank";
	else  	        document.forms[0].target = "_self";
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PRM&Id=<%= appendixid %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}
function viewaddendumpdf(v ) {
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PRM&Id=<%= appendixid %>";
	document.forms[0].submit();
}
function viewdocuments() {
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdashboarddocument&ref1=view&Id=<%= appendixid %>&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
function toggleMenuSection(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){ }
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) 	thisImage.src = "images/Collapse.gif";    
	else thisImage.src = "images/Expand.gif";
}
function toggleDiv(divName) {
	 var tcookie = document.cookie;
     var st = tcookie.indexOf ("A1Db");
     var cookievalue ="";
	  if(st!= -1) {
	      st = st+3;
	      st = tcookie.indexOf ("=", st);
	      en = tcookie.indexOf (";", st);
		  if (en > st) cookievalue = tcookie.substring(st+1, en); //div's that is reopen
	 }
	var id = document.getElementById(divName);
	if( id.style.display == "none" ) {
			id.style.display = "block";
			//add it to the existing cookie opendiv paraneter
			if (cookievalue.length>0)	cookievalue = cookievalue+','+divName;
			else	cookievalue = divName;
			if(cookievalue!="") { // Save as cookie
				document.cookie = "ADb="+ cookievalue+" ; ";
          } }
	else {
			id.style.display = "none";	
			var newcookievalue='';
			//remove this div from the opendiv cookie
			if (cookievalue.length>0) {
				var array = cookievalue.split(',');
				if (array.length>1) {
					for(i=0;i<array.length;i++) {
						if(array[i]==divName) { }
						else {
							if(newcookievalue.length > 0 )
								newcookievalue = newcookievalue+','+array[i];
							else
								newcookievalue = array[i];	
						}
					}
				}
				if(newcookievalue!="") { // Save as cookie
			 		document.cookie = "ADb="+ newcookievalue+" ; ";
			 	}
			}
		}
	if( divName == 'div_1')
	{
		id = document.getElementById('div_13');
		if( id.style.display == "none" ) id.style.display = "block";
		else id.style.display = "none";	
		id = document.getElementById('div_11');
		if( id.style.display == "none" ) id.style.display = "block";
		else id.style.display = "none";	
	}
}
function toggleMenuSectionView(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	action = "toggleType = toggleDivView('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) 	thisImage.src = "images/Collapse.gif";
	else thisImage.src = "images/Expand.gif";
}
function toggleDivView(divName) {
	 var tableRow = document.getElementsByTagName('tr');
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if (tableRow[k].getAttributeNode('id').value==divName) {
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	}
}
// Added Start:By Amit This will be called on each href that opens other form
function createCookie()
{
 var tableRow = document.getElementsByTagName('tr');
  var openDiv="";
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if((tableRow[k].getAttributeNode('id').value != "")&&(tableRow[k].getAttributeNode('id').value != "account"))
		{
			if(tableRow[k].style.display == "block" ){
			openDiv=openDiv+tableRow[k].getAttributeNode('id').value+",";
			}
		}
	}
		if(openDiv!="") { // Save as cookie
			 document.cookie = "ADb="+ openDiv+" ; ";
		 }
}
function createCookie(lastDivForFocus) {
 		document.cookie = "A1Db="+ lastDivForFocus+" ; ";
}
//following function would be called on OnLoad
function CookieGetOpenDiv() {
  var tcookie = document.cookie;
  var unique;
  var st = tcookie.indexOf ("ADb");
  var cookievalue ="";
  if(st!= -1) {
      st = st+3;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);
	  if (en > st) {
          cookievalue = tcookie.substring(st+1, en);
        }
       if(cookievalue.length>0) {
		var opendiv = cookievalue.split(',');
		if(opendiv.length>0) {
		  for (var j = 0; j < opendiv.length; j++) {
		   		if(eval("document.getElementById('"+opendiv[j]+"')")!=null)	{ //Added This if condition by amit
		  				eval("document.getElementById('"+opendiv[j]+"')").style.display = "block";
						unique = opendiv[j].substring(opendiv[j].indexOf("_")+1,opendiv[j].length);
						action = "thisImage = document.getElementById('img_" + unique +"');";
						eval(action);
					if (document.getElementById('div_' + unique).offsetHeight > 0) {
							thisImage.src = "images/Collapse.gif";
		   			} else {
							thisImage.src = "images/Expand.gif";
					}
				}
				if(opendiv[j]=='div_1') {
						if(document.forms[0].viewjobtype.value!='jobSearch'){
						document.getElementById('div_11').style.display = "block";
						document.getElementById('div_13').style.display = "block";
						}
				}												
		  }
		}
	}
  }
	  openDiv = "";
	  //Save as cookie
	  document.cookie = "A1Db="+ openDiv+" ; "; 
	  //Start:Added For Focus		
	  var st1 = tcookie.indexOf ("A1Db");
	  var cookievalue1 ="";
	  if(st1!= -1) {
	      st1 = st1+4;
	      st1 = tcookie.indexOf ("=", st1);
	      en = tcookie.indexOf (";", st1);
		  if (en > st1) {
	          cookievalue1 = tcookie.substring(st1+1, en);
	        }
		}
		 if(cookievalue1!="") {  
		  		if(document.getElementById(cookievalue1)!=null) {
		  		if(document.getElementById(cookievalue1).style.visibility == 'visible')
		 	      	document.getElementById(cookievalue1).focus();
		    	}
		    
		}
		document.cookie = "A1Db="+ openDiv+" ; ";
}
//End By Amit
</script>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


%>
<BODY onLoad="MM_preloadImages('images/Expand.gif','images/Collapse.gif');CookieGetOpenDiv();leftAdjLayers();">
<html:form action="AppendixHeader"> 
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property="inv_type" />
<html:hidden property="inv_id" />
<html:hidden property="inv_rdofilter" />
<html:hidden property="invoice_Flag"/>
<html:hidden property="invoiceno"/>
<html:hidden property="partner_name"/>
<html:hidden property="powo_number"/>
<html:hidden property="from_date"/>
<html:hidden property="to_date"/>
<html:hidden property = "msaId"/>
<html:hidden property = "fromPage"/>
<html:hidden property="jobOwnerOtherCheck"/> 



<logic:notEqual name = "AppendixHeaderForm" property = "fromPage" value="msa">
<logic:notEqual name = "AppendixHeaderForm" property = "fromPage" value="viewselector">
<html:hidden property="appendixid"/>
		<%@ include  file="/AppendixDashboardMenuScriptAPIAccess.inc" %>
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr><td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.appendixdashboardfor"/>&nbsp;<bean:write name="AppendixHeaderForm" property="msaname" />&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp;<bean:write name="AppendixHeaderForm" property="appendixname" /></h2></td></tr> 
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
												    		if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="AppendixHeaderForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="AppendixHeaderForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="AppendixHeaderForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id = "OwnerSpanId"> 
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
</logic:notEqual>

<table>
<tr height="20"><td>&nbsp;</td></tr>
<logic:present name = "changeStatusFlag" scope = "request">
	<login:equal name = "changeStatusFlag" value = "true">
		<script>parent.ilexleft.location.reload();</script>
	</login:equal>	
</logic:present>

<logic:present name = "docMException" scope = "request">
	<tr> 
			<td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
				<bean:message bundle = "pm" key = "error.nodocument.docm"/>
			</td>
	</tr>		
</logic:present>

<logic:present name = "emailflag" scope = "request">
		<tr><td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
		    	<logic:equal name = "emailflag" value = "0"><bean:message bundle = "pm" key = "msa.detail.mailsent"/></logic:equal>
	    		<logic:notEqual name = "emailflag" value = "0"><bean:message bundle = "pm" key = "msa.detail.mailsentfailure"/></logic:notEqual>
	    </td></tr>
</logic:present>
<logic:present name = "bulkuploadflag" scope = "request">
		<tr><td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
		    	<logic:equal name = "bulkuploadflag" value = "0">Jobs Created Successfully<script>parent.ilexleft.location.reload();</script></logic:equal>
	    </td></tr>
</logic:present>
<logic:present name = "addendumflag" scope = "request">
		<tr><td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
		    	<logic:equal name = "addendumflag" value = "0">Addendum Created Successfully<script>parent.ilexleft.location.reload();</script></logic:equal>
	    </td></tr>
</logic:present>

<logic:present name = "addFlag" scope = "request">
<logic:equal name = "addFlag" value="0">
	<tr><td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
		Special Instruction/Condition&nbsp;<bean:message bundle = "pm" key = "common.sa.add.success"/>
	</td></tr>
</logic:equal>

<logic:notEqual name = "addFlag" value="0">
	<tr><td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
		<bean:message bundle = "pm" key = "common.sa.delete.failure"/>&nbsp;Special Instruction/Condition
	</td></tr>	
</logic:notEqual>
</logic:present>
<logic:notEqual name ="AppendixHeaderForm" property = "viewjobtype" value = "jobSearch">

<logic:present name="changeJobNameFlag" scope="request">
	<logic:equal name="changeJobNameFlag" value="0">
		<tr valign="middle"><td colspan="4" class="message" height="25">&nbsp;&nbsp;&nbsp;&nbsp;
			Job Name Changed Successfully.
		</td></tr>
	</logic:equal>
</logic:present>

<tr>
<td>
<table >
	<tr><td width=2 > &nbsp;</td>
		<td> 
	<tr>
		<td width = "100%" valign = "top">-->
			<table border = "0" cellpadding=0 cellspacing=0>
				<tr>
					<td valign = "top"><a href="javascript:toggleMenuSection('1');"><img id="img_1" src="images/Expand.gif" border="0" alt="+"  /></a></td>
					<td valign = "top" height = "100%" >
						<table width = 390 border = "0" height = "100%" class="dbsummtable" cellpadding=0 cellspacing=0>
							<tr>
								<td   width=390 valign = "top" height = "20"  align="center" class = "trybtopclose" id="classchange1"><bean:message bundle="PRM" key="prm.appendix.appendixdetails"/>
								<% if(testAppendix.equals("1")) { %> &nbsp; (Test) <%}%>
								</td>
							</tr> 
							
							 <tr id = "div_1" style="display: none">
								<td valign = "top" height = "100%" width=380>
									<table width = "380" height = "100%" border = "0" cellspacing=2 cellpadding=2>
										<tr >
											<td valign = "top">
												<table width = "380" cellpadding=0 cellspacing=0 border=0 >
													<tr>
														<td valign = "top"  class = "dbvaluesmall" colspan = "2" ><b><bean:message bundle="PRM" key="prm.appendix.type"/></b>&nbsp;<span class = "dbvaluesmall"><b><bean:write name="AppendixHeaderForm" property="appendix_type" /></b></span></td>
														<td valign = "top"  class = "dbvaluesmall" colspan = "2" ><b><bean:message bundle="PRM" key="prm.appendix.status"/></b>&nbsp;<span class = "dbvaluesmall"><bean:write name="AppendixHeaderForm" property="statusdesc" /></span></td>
													</tr>
													<tr><b> <TD valign=top class=dbvaluesmall colspan="4" ><b><bean:message bundle="PRM" key="prm.appendix.sites"/></b>&nbsp;<span class = "dbvaluesmall">
													    <bean:message bundle="PRM" key="prm.appendix.inwork"/>&nbsp;<span class = "dbvaluesmall"><bean:write name="AppendixHeaderForm" property="job_inwork"/></span>&nbsp;<span class = "dbvaluesmall">
													    <bean:message bundle="PRM" key="prm.appendix.complete1"/>&nbsp;<span class = "dbvaluesmall"><bean:write name="AppendixHeaderForm" property="job_complete"/></span>&nbsp;<span class = "dbvaluesmall">
													    <bean:message bundle="PRM" key="prm.appendix.closed"/>&nbsp;<span class = "dbvaluesmall"><bean:write name="AppendixHeaderForm" property="job_closed"/>&nbsp;<span class = "dbvaluesmall">
													    <bean:message bundle="PRM" key="prm.appendix.total"/>&nbsp;<span class = "dbvaluesmall"><bean:write name="AppendixHeaderForm" property="job_total"/>&nbsp;<span class = "dbvaluesmall"></td>
													</tr>
													
													<tr><td valign = "top"  class = "dbvaluesmall" colspan="4"><b><bean:message bundle="PRM" key="prm.appendix.customerreference"/></b>&nbsp;<span class = "dbvaluesmall"><bean:write name="AppendixHeaderForm" property="custref" /></span></td></tr>  
													    
													<tr><td valign = "top"  class = "dbvaluesmall" colspan = "1"><b><bean:message bundle="PRM" key="prm.appendix.contactinfo"/></b>&nbsp;
													 <logic:equal name = "AppendixHeaderForm" property="editContactList" value="true">
													 <a id ='adiv_1'  href = "AddContact.do?function=add&typeid=<bean:write name="AppendixHeaderForm" property="appendixid" />&type=P&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&fromPage=AppendixDashBoard" onclick="Javascript:createCookie('adiv_1');"><bean:message bundle="PRM" key="prm.appendix.edit"/>
													</a>
													</logic:equal>
													</td>
													     <TD valign=top class=dbvaluesmallCenter><bean:message bundle="PRM" key="prm.appendix.name"/></td>
														 <td valign=top class = "dbvaluesmall" >&nbsp;</td>
														 <TD valign=top class=dbvaluesmallCenter><bean:message bundle="PRM" key="prm.appendix.phone"/></td>
													</tr>					  
													<tr>
													   <TD valign=top class=dbvaluesmall>&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.contingentcontacts"/></td>
													</tr>
														
													<tr>
													 <TD valign=top class=dbvaluesmall nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.business_dev"/></td>
														
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="appendixcnspoc" /></td>
														<td td valign = "top" class=dbvaluesmall  width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="appendixcnspocphone" /></td>
													</tr>
													<tr>	 
														 <TD valign=top class=dbvaluesmall nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sr. Project Manager:</td>
														 <logic:equal name = "AppendixHeaderForm" property="cnsprojectmgrId" value="0">
														 	<TD valign=top class=dbvaluesmall nowrap><b><font color="RED"><bean:message bundle="PRM" key="prm.appendix.notassigned"/></b></font></TD>
														 </logic:equal>
														 <logic:notEqual name = "AppendixHeaderForm" property="cnsprojectmgrId" value="0">
														 	<TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="cnsprojectmgr" /></td>
														 </logic:notEqual>
															<td  valign = "top" class=dbvaluesmall  width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="cnsprojectmgrphone" /></td>
													</tr>
														<tr>	 
														 <TD valign=top class=dbvaluesmall nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.contact.projectmgr"/>:</td>
														 <logic:equal name = "AppendixHeaderForm" property="cnsPrjManagerId" value="0">
														 	<TD valign=top class=dbvaluesmall nowrap><b><font color="RED"><bean:message bundle="PRM" key="prm.appendix.notassigned"/></b></font></TD>
														 </logic:equal>
														 <logic:notEqual name = "AppendixHeaderForm" property="cnsPrjManagerId" value="0">
														 	<TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="cnsPrjManagerName" /></td>
														 </logic:notEqual>
															<td  valign = "top" class=dbvaluesmall  width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="cnsPrjManagerPhone" /></td>
													</tr>
														<tr>	 
														 <TD valign=top class=dbvaluesmall nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.contact.teamLead"/>:</td>
														 <logic:equal name = "AppendixHeaderForm" property="teamLeadId" value="0">
														 	<TD valign=top class=dbvaluesmall nowrap><b><font color="RED"><bean:message bundle="PRM" key="prm.appendix.notassigned"/></b></font></TD>
														 </logic:equal>
														 <logic:notEqual name = "AppendixHeaderForm" property="teamLeadId" value="0">
														 	<TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="teamLeadName" /></td>
														 </logic:notEqual>
															<td  valign = "top" class=dbvaluesmall  width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="teamLeadPhone" /></td>
													</tr>
													<tr>
														 <TD valign=top class=dbvaluesmall>&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.customercontacts"/></td>
													</tr>	
													<tr>
														 <TD valign=top class=dbvaluesmall nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.projectmgr"/></td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="custprojectmgr" /></td>
														 <td   valign = "top" class=dbvaluesmall  width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="custprojectmgrphone" /></td>
													</tr>
													<tr>
														 <TD valign=top class=dbvaluesmall nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.business"/></td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="appendixbusinesspoc" /></td>
														 <td   valign = "top" class=dbvaluesmall  width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="appendixbusinesspocphone" /></td>
													</tr>
													<tr>
														 <TD valign=top class=dbvaluesmall nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.contract"/></td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="appendixcontractpoc" /></td>
														 <td   valign = "top" class=dbvaluesmall  width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="appendixcontractpocphone" /></td>
													</tr>
													<tr>
													     <TD valign=top class=dbvaluesmall nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.billing"/></td >
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="appendixbillingpoc" /></td>
														 <td  valign = "top" class=dbvaluesmall   width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall nowrap><bean:write name="AppendixHeaderForm" property="appendixbillingpocphone" /></td>
													</tr>
													<tr><td colspan = "2" height = "8"></td></tr>
													    
													<tr><TD valign=top class=dbvaluesmall colspan = "1"><b><bean:message bundle="PRM" key="prm.appendix.schedule"/><b></td>
													    <TD valign=top class=dbvaluesmall><bean:message bundle="PRM" key="prm.appendix.scheduled1"/></td>
														<td class = "dbvaluesmall" valign = "top"  >&nbsp;</td>
														<TD valign=top class=dbvaluesmall><bean:message bundle="PRM" key="prm.appendix.actual1"/></td>
													</tr>
													<tr>
														 <TD valign=top class=dbvaluesmall>&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.earliest_start"/></td>
													     <TD valign=top class=dbvaluesmall><bean:write name="AppendixHeaderForm" property="job_earliest_start" /></td>
														 <td valign = "top" class=dbvaluesmall  width="15">&nbsp;</td>
														 <TD valign=top class=dbvaluesmall><bean:write name="AppendixHeaderForm" property="job_earliest_end" /></td>
													</tr>
													<tr>
														<TD valign=top class=dbvaluesmall>&nbsp;&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.latest_finish"/></td>
														<TD valign=top class=dbvaluesmall><bean:write name="AppendixHeaderForm" property="job_latest_start" /></td>
														<td  valign = "top" class=dbvaluesmall width="15">&nbsp;</td>
														<TD valign=top class=dbvaluesmall><bean:write name="AppendixHeaderForm" property="job_latest_end" /></td>
														
													</tr>
													<tr>
														<TD valign=top class=dbvaluesmall>&nbsp;&nbsp;&nbsp;Reschedules:</td>
														<TD valign=top class=dbvaluesmall colspan="3"><bean:write name="AppendixHeaderForm" property="rseCount" /></td>
														
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width=1> &nbsp;</td>
					<td valign = "top" height = "100%" >
						<table width = 230 height = "100%" border = "0" class="dbsummtable" cellpadding=0 cellspacing=0>
							<tr><td width = 230 height = "20" valign = "top"   class = "trybtopclose" align="center"  id="classchange2"><bean:message bundle="PRM" key="prm.appendix.appendixcostsummary"/></td></tr>
							<tr id = "div_11" style="display: none">
								<td valign = "top">
									<table width = 230 height="100%" border = "0" cellspacing=0 cellpadding=0 class = "appendixsubheadercolor" >
										<tr>
											<td valign = "top">
												<table  width = 228 cellspacing=2 cellpadding=2 border=0>
													<tr>
														<td width = 130 valign = "top"  class = "dbvaluesmall"><b><bean:message bundle="PRM" key="prm.appendix.extendedprice"/></b></td>
														<td valign = "top" class = "dbvaluerightsmall">$<bean:write name="AppendixHeaderForm" property="extendedprice" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvaluesmall" colspan = "2"><b><bean:message bundle="PRM" key="prm.appendix.estimatedcost"/></b> <bean:message bundle="PRM" key="prm.appendix.yourbudget"/><b>:</b></td>
													</tr>													
													<tr>
														<td valign = "top" class = "dbvaluesmall" >&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.fieldlabor"/></td>
														<td valign = "top" class = "dbvaluerightsmall" >$<bean:write name="AppendixHeaderForm" property="appendixfieldlabor" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvaluesmall" >&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.materials"/></td>
														<td valign = "top" class = "dbvaluerightsmall" >$<bean:write name="AppendixHeaderForm" property="appendixmaterials" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvaluesmall" >&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.freight"/></td>
														<td valign = "top" class = "dbvaluerightsmall" >$<bean:write name="AppendixHeaderForm" property="appendixfreight" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvaluesmall" >&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.travel"/></td>
														<td valign = "top" class = "dbvaluerightsmall" >$<bean:write name="AppendixHeaderForm" property="appendixtravel" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvaluesmall" >&nbsp;&nbsp;<b><bean:message bundle="PRM" key="prm.appendix.total"/></b></td>
														<td valign = "top" class = "dbvaluerightsmall">$<bean:write name="AppendixHeaderForm" property="estimatedcost" /></td>
													</tr>
													
													<tr>
														<td valign = "top" class = "dbvaluesmall" ><b><bean:message bundle="PRM" key="prm.appendix.proformavgpm"/></b></td>
														<td valign = "top" class  = "dbvaluerightsmall" ><bean:write name="AppendixHeaderForm" property="proformamargin" /></td>
													</tr>
													<tr><td colspan = "2" height = "4"></td></tr>
													<tr>
														<td valign = "top" class = "dbvaluesmall" ><b><bean:message bundle="PRM" key="prm.appendix.actualcosts"/></b></td>
														<td valign = "top" class  = "dbvaluerightsmall" >$<bean:write name="AppendixHeaderForm" property="actualcosts" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvaluesmall"><b><bean:message bundle="PRM" key="prm.appendix.actualvgpm"/></b></td>
														<td valign = "top" class  = "dbvaluerightsmall" ><bean:write name="AppendixHeaderForm" property="actualvgpm" /></td>
													</tr>
													
													<tr>
														<td valign = "top" class = "dbvaluesmall"><b><bean:message bundle="PRM" key="prm.appendix.actualvgp"/></b></td>
														<td valign = "top" class  = "dbvaluerightsmall" >$<bean:write name="AppendixHeaderForm" property="actualvgp" /></td>
													</tr>
													
													<tr>
														<td valign = "top" class = "dbvaluesmall"><b><bean:message bundle="PRM" key="prm.appendix.pmvipp"/></b></td>
														<td valign = "top" class  = "dbvaluerightsmall" ><bean:write name="AppendixHeaderForm" property="pmvipp" /></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width=1 > &nbsp;</td>
					<td valign = "top" height = "100%" >
						<table width = 300 height = "100%" border = "0"  class="dbsummtable" cellpadding=0 cellspacing=0  >
							<tr><td width = 298 align = "center"  height = "20" align="center" class = "trybtopclose" id="classchange3"><bean:message bundle="PRM" key="prm.appendix.jobsummary"/></td></tr>
							<tr id = "div_13" style="display: none">
								<td valign = "top" width = 300>
									<table width = 298  cellpadding=0 cellspacing=0 border=0>
										<tr><td valign = "top" width = 298>
										  <IFRAME  SRC="ViewGraph.do?graphtype=jobgraph&appendixid=<%= appendixid%>" TITLE="" marginwidth=0 marginheight=0  frameborder=0  height=210 width=295></IFRAME>	
										</td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
	    </td>
	</tr>
</table>
</logic:notEqual>
</logic:notEqual>
<logic:equal name ="AppendixHeaderForm" property = "viewjobtype" value ="jobSearch">
<!-- chnage for search Menu -->	
	
	<table>
		<logic:equal name = "AppendixHeaderForm" property = "appendixid" value = "0">
			<tr><td width = "100%" class = "labeleboldwhite">&nbsp;&nbsp;&nbsp;Job Search Page for&nbsp;<bean:write name="AppendixHeaderForm" property="msaname" /></td></tr>
		</logic:equal>
		
		<logic:notEqual name = "AppendixHeaderForm" property = "appendixid" value = "0">
			<tr><td width = "100%" class = "labeleboldwhite">&nbsp;&nbsp;&nbsp;Job Search Page for&nbsp;<bean:write name="AppendixHeaderForm" property="msaname" />&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp; <a href ="AppendixHeader.do?appendixid=<bean:write name = "AppendixHeaderForm" property = "appendixid"/>&function=view&viewjobtype=I&fromPage=appendix&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>"><bean:write name="AppendixHeaderForm" property="appendixname" /></a></td></tr>				
		</logic:notEqual>
	</table>
	
			<logic:equal name = "AppendixHeaderForm" property = "fromPage" value="msa">
				<table>
					<tr><td width=2>&nbsp;</td>
					<td class="labeloboldwhite" colspan = "1">&nbsp;&nbsp;Appendix Name&nbsp;</td>
					<td><html:select name="AppendixHeaderForm" property = "appendixid" size = "1" styleClass="combowhite" onchange = "return changeOwner();">
						<html:optionsCollection name = "appendixList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
					</html:select>
					</td></tr>
				</table>
			</logic:equal>
		
		<logic:notEqual name ="AppendixHeaderForm" property = "fromPage" value="msa">
				<html:hidden property = "appendixid" />
		</logic:notEqual>
	
		
		<table width = 1000>
			<tr><td width=2>&nbsp;</td>
				<td class="labeloboldwhite" width  ="150">&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.jobname"/>&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="job_search_name"/></td>
				<td class="labeloboldwhite" width  ="150"><bean:message bundle="PRM" key="prm.appendix.job.site"/>&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="job_search_site_number"/></td>
				<td class="labeloboldwhite" width  ="550"><bean:message bundle="PRM" key="prm.appendix.city"/>&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="job_search_city"/>&nbsp;&nbsp;
		  	 	<bean:message bundle="PRM" key="prm.appendix.state"/>&nbsp;&nbsp;
				<logic:present name="initialStateCategory">
								<html:select name ="AppendixHeaderForm" property = "job_search_state"  styleClass="combowhite">
									<logic:iterate id="initialStateCategory" name="initialStateCategory" >
										<bean:define id="statecatname" name="initialStateCategory" property="siteCountryName" />		   
										<%  if(!statecatname.equals("notShow")) { %>
										<optgroup class=labeloboldwhite label="<%=statecatname%>">
										<% } %>
											<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
										<%  if(!statecatname.equals("notShow")) { %>	
										</optgroup> 
										<% } %>
									</logic:iterate>
								</html:select>
				</logic:present>
			  &nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.owner"/>&nbsp;&nbsp;
							<html:select  name = "AppendixHeaderForm" property="job_search_owner" size="1" styleClass="combowhite">
							<html:optionsCollection name = "dcJobOwnerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					    </td>	
			</tr>
			<tr><td width=2>&nbsp;</td>
				<td class="labeloboldwhite" width  ="170" >&nbsp;&nbsp;<bean:message  bundle="PRM" key = "prm.appendix.date.from"/>&nbsp;<html:text  styleClass="textbox" size="10" property ="job_search_date_from" readonly = "true"/><img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].job_search_date_from, document.forms[0].job_search_date_from, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img></td>
            	<td class="labeloboldwhite" width  ="170" ><bean:message  bundle="PRM" key = "prm.appendix.date.to"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="job_search_date_to" readonly = "true"/><img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].job_search_date_to, document.forms[0].job_search_date_to, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img></td>
				<td class="labeloboldwhite">&nbsp;&nbsp;<html:checkbox property="dateOnSite" value="OS"></html:checkbox><bean:message bundle="PRM" key="prm.appendix.onsite"/>&nbsp;&nbsp;
				<html:checkbox property="dateOffSite" value="FS"></html:checkbox><bean:message bundle="PRM" key="prm.appendix.offsite"/>&nbsp;&nbsp;
				<html:checkbox property="dateComplete" value="CO"></html:checkbox><bean:message bundle="PRM" key="prm.appendix.complete"/>&nbsp;&nbsp;
				<html:checkbox property="dateClosed" value="CL"></html:checkbox><bean:message bundle="PRM" key="prm.appendix.closed"/></td>
			</tr>
			<tr>
				<td width=2>&nbsp;</td>
				<td>&nbsp;&nbsp;<html:submit property="job_search" styleClass="button" onclick="return searchJobs();"><bean:message bundle="PRM" key ="prm.appendix.search"/></html:submit>
				&nbsp;&nbsp;<html:button property="cancel" styleClass="button" onclick ="emptySearchCriteria();"><bean:message bundle = "PRM" key = "prm.button.Cancel"/></html:button></td>
			</tr>
		</table>	
</logic:equal>

<table width=1368 >	
	<tr><td width=2> &nbsp;</td>
		<td width=1366> 
		<% 	boolean show = true;
			if(size>0){ 

			if(size>=100 && viewType.equals("jobSearch")) show = false;	
		
			if(show){%>  
			<table width=1366 cellspacing=1 cellpadding=0 class="dbsummtable3" >
				<tr><td></td>
					<td colspan = "2" class="trybtopbutton">
						<html:button property = "sort" styleId = "sort" styleClass = "button" onclick = "javascript:createCookie('sort');return sortwindow();"><bean:message bundle = "PRM" key = "prm.sort"/></html:button>
							<logic:notEqual name ="AppendixHeaderForm" property = "viewjobtype" value = "jobSearch">
								<logic:present name = "AppendixHeaderForm" property="inv_type">
									<html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>	 
								</logic:present>	
							</logic:notEqual>	
					</td>
					<td colspan = "20" class="trybtop" height="20"><bean:message bundle="PRM" key="prm.appendix.jobdetails"/></td>		
				</tr>
				<tr> 
					<td  width=20 rowspan = "2">&nbsp; </td>
					<td width=150 class="tryb" rowspan = "2"><bean:message bundle="PRM" key="prm.appendix.jobname"/></td>
					<td width=108 class="tryb" rowspan = "2"><bean:message bundle="PRM" key="prm.appendixtab.status"/></td>
					<td width=57 class="tryb" rowspan = "2"><bean:message bundle="PRM" key="prm.appendix.scope"/></td>
					<td class="tryb" colspan = "4"><bean:message bundle="PRM" key="prm.appendix.siteinfo"/></td>
					<td width=80 class="tryb" rowspan = "2"><bean:message bundle="PRM" key="prm.appendix.partners"/></td>

				<% if(viewType.equals("jobSearch")) {%>
						<td class="tryb" colspan = "3">Actual</td>
				<%} else {%>
						<td class="tryb" colspan = "3"><bean:message bundle="PRM" key="prm.appendix.schedule"/></td>
				<%} %>
					<td width=314 class="tryb" colspan = "5"><bean:message bundle="PRM" key="prm.appendix.costsummary"/></td>
					<td width=59 class="tryb" rowspan = "2"><bean:message bundle="PRM" key="prm.appendix.sitechecklist"/></td>
					<td width=59 class="tryb" rowspan = "2"><bean:message bundle="PRM" key="prm.appendix.customer_reference"/></td>
					<td width=108 class="tryb" rowspan = "2" colspan = "2"><bean:message bundle="PRM" key="prm.appendix.cnsinvoice"/></td>
					<td width=108 class="tryb" rowspan = "2" colspan = "2"><bean:message bundle="PRM" key="prm.appendix.owner"/></td>
				</tr>
				<tr> 
				    <td width=57 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.site"/></div></td>
				    <td width=57 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.city"/></div></td>
				    <td width=57 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.state"/></div></td>
					<td width=57 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.zipcode"/></div></td>
					<td width=60 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.start"/></div></td>
					<td width=60 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.time"/></div></td>
					<% if(viewType.equals("jobSearch")) {%>
						<td width=60 class = "tryb"><div>End</div></td>
					<%} else {%>
						<td width=60 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.complete"/></div></td>
					<%} %>
					
					<td width=64 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.estimatedbrcost"/></div></td>
					<td width=64 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.extendedbrprice"/></div></td>
				    <td width=64 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.proformabrvgp"/></div></td>
				    <td width=64 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.actualbrcost"/></div></td>
					<td width=64 class = "tryb"><div><bean:message bundle="PRM" key="prm.appendix.actualbrvgp"/></div></td>
				 </tr>
				<logic:present  name="joblist">
					<logic:iterate id="jlist" name="joblist" >
					<% if((i%2)==0) { bgcolor="dbvalueeven";		bgcolorno="numbereven";		bgcolortext1="textlefteven";
						} else {	  bgcolor="dbvalueodd";			bgcolorno="numberodd";		bgcolortext1="textleftodd";
					}  if(size==i) {
						if((i%2)==0) { bgcolor="dbvalueevenlastrow";	 bgcolorno="numberevenlastrow";		bgcolortext1="textleftevenlastrow"; } 
						else {	   bgcolor="dbvalueoddlastrow";		 bgcolorno="numberoddlastrow";		bgcolortext1="textleftoddlastrow"; }
				} %>
				<bean:define id="jid" name="jlist" property="jobid" />
				<tr>
					<td>&nbsp;
						<!-- Comments on 20061108 -->
					</td>
					<td class="<%=bgcolortext1 %>" >
						<a id ='aa_j<%=jid%>' href="JobDashboardAction.do?appendix_Id=<bean:write name="jlist" property="appendixid"/>&Job_Id=<bean:write name="jlist" property="jobid"/>"><bean:write name="jlist" property="jobname"/></a>
						&nbsp;&nbsp;<a id ='au_j<%=jid%>' href="JobNameChangeAction.do?appendix_Id=<bean:write name="jlist" property="appendixid"/>&ref=inscopeview&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&Job_Id=<bean:write name="jlist" property="jobid"/>" onclick="Javascript:createCookie('aa_j<%=jid%>');">[Edit]</a>
					</td>

					<td  class="<%=bgcolor %>"><bean:write name="jlist" property="status"/></td>
					<td class="<%=bgcolor %>" ><bean:write name="jlist" property="scope"/></td>
					
					<bean:define id="sno" name="jlist" property="siteno" />
					
					<%if(sno.equals("")) {%>
					<td class="<%=bgcolortext1 %>" >&nbsp;</td>
					<%} else { %>
					<td class="<%=bgcolortext1 %>" ><bean:write name="jlist" property="siteno"/>
					<logic:equal name ="jlist" property = "site_prop"  value ="Y">	
					<IMG id = img_2  src = "images/priorities.jpg" border = 0  width="16" height="16" >		
					</logic:equal>
					</td>
					<%} %>
					<bean:define id="scity" name="jlist" property="sitecity" />
					<%if(scity.equals("")) {%>
					<td class="<%=bgcolortext1 %>" >&nbsp;</td>
					<%} else { %>
					<td class="<%=bgcolortext1 %>" ><bean:write name="jlist" property="sitecity" /></td>
					<%} %>	
					<bean:define id="sstate" name="jlist" property="sitestate" />
					<%if(sstate.equals("")) {%>
					<td class="<%=bgcolor %>" >&nbsp;</td>
					<%} else { %>
					<td class="<%=bgcolor %>" ><bean:write name="jlist" property="sitestate" /></td>
					<%} %>	
					<bean:define id="szp" name="jlist" property="sitezipcode" />
					<%if(szp.equals("")) {%>
					<td class="<%=bgcolortext1 %>" >&nbsp;</td>
					<%} else  { %>
					<td class="<%=bgcolortext1 %>" ><bean:write name="jlist" property="sitezipcode"/></td>
					<%} %>	
						<td class="<%=bgcolor %>" ><a href = "JobDashboardAction.do?appendix_Id=<bean:write name="jlist" property="appendixid"/>&Job_Id=<bean:write name="jlist" property="jobid"/>"><bean:message bundle="PRM" key="prm.appendix.assign"/></a></td>
					<bean:define id="estrt" name="jlist" property="startdate" />
					<%if(estrt.equals(""))  {%>
					<td  align=center class="<%=bgcolor %>" >&nbsp;</td>
					<%} else { %>
					<td   align=center class="<%=bgcolor %>"><a id='ca_j<%=jid%>' href="JobDashboardAction.do?tabId=7&isClicked=1&function=update&jobid=<bean:write name="jlist" property="jobid"/>&typeid=<bean:write name="jlist" property="jobid"/>&type=<bean:write name="jlist" property="jobtype"/>&page=appendixdashboard&pageid=<bean:write name="jlist" property="appendixid"/>&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>" onclick="Javascript:createCookie('ca_j<%=jid%>');"><bean:write name="jlist" property="startdate"/></a></td>
					<%} %>
					<td class="<%=bgcolor %>" ><a id='da_j<%=jid%>' href="JobDashboardAction.do?tabId=7&isClicked=1&function=update&jobid=<bean:write name="jlist" property="jobid"/>&typeid=<bean:write name="jlist" property="jobid"/>&type=<bean:write name="jlist" property="jobtype"/>&page=appendixdashboard&pageid=<bean:write name="jlist" property="appendixid"/>&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>" onclick="Javascript:createCookie('da_j<%=jid%>');"><bean:write name="jlist" property="startdatetime"/></a></td>
					<bean:define id="ecomp" name="jlist" property="plannedenddate" />
					<%if(ecomp.equals("")) {%>
					<td  align=center class="<%=bgcolor %>" >&nbsp;</td>
					<%} else { %>
					<td   align=center class="<%=bgcolor %>"><a id='ea_j<%=jid%>' href="JobDashboardAction.do?tabId=5&isClicked=1&function=update&jobid=<bean:write name="jlist" property="jobid"/>&typeid=<bean:write name="jlist" property="jobid"/>&type=<bean:write name="jlist" property="jobtype"/>&page=appendixdashboard&pageid=<bean:write name="jlist" property="appendixid"/>&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>" onclick="Javascript:createCookie('ea_j<%=jid%>');"><bean:write name="jlist" property="plannedenddate"/></a></td>
					<%} %>
					<bean:define id="ttl" name="jlist" property="total" />
					<%if(ttl.equals("")) {%>
					<td  align=center class="<%=bgcolorno %>" >&nbsp;</td>
					<% } else { %>
					<td  align=center class="<%=bgcolorno %>" ><bean:write name="jlist" property="total"/></td>
					<%} %>
					<bean:define id="eprice" name="jlist" property="extendedprice" />
					<%if(eprice.equals("")) {%>
					<td  align=center class="<%=bgcolorno %>" >&nbsp;</td>
					<% }else { %>
					<td  align=center class="<%=bgcolorno %>" ><bean:write name="jlist" property="extendedprice"/></td>
					<% } %>
					<bean:define id="emgn" name="jlist" property="pmargin" />
					<%if(emgn.equals("")) {%>
					<td  align=center class="<%=bgcolorno %>" >&nbsp;</td>
					<% } else { %>
					<td  align=center class="<%=bgcolorno %>" ><bean:write name="jlist" property="pmargin"/></td>
					<% } %>
					<bean:define id="acost" name="jlist" property="actualcosts" />
					<%if(acost.equals("")) { %>
					<td  align=center class="<%=bgcolorno %>" >&nbsp;</td>
					<% } else { %>
					<td  align=center class="<%=bgcolorno %>" ><bean:write name="jlist" property="actualcosts"/></td>
					<% } %>
					<bean:define id="avgp" name="jlist" property="actualvgp" />
					<%if(avgp.equals("")) {%>
					<td  align=center class="<%=bgcolorno %>" >&nbsp;</td>
					<% } else { %>
					<td  align=center class="<%=bgcolorno %>" ><bean:write name="jlist" property="actualvgp"/></td>
					<%} %>
					<td  class="<%=bgcolor %>"><a id='ga_j<%=jid%>' href="ChecklistTab.do?job_id=<bean:write name="jlist" property="jobid"/>&page=appendixdashboard&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>" onclick="Javascript:createCookie('ga_j<%=jid%>');"><bean:message bundle="PRM" key="prm.appendix.process"/></a></td>
					<td  class="<%=bgcolor %>" ><bean:write name="jlist" property="custref"/></td>
					<td  class="<%=bgcolor %>" ><bean:write name="jlist" property="invoice_no"/></td>		
					<td  class="<%=bgcolor %>" ><a id='ia_j<%=jid%>' href = "InvoiceJobDetail.do?function=view&type=Appendix&rdofilter=A&id=<bean:write name="jlist" property="appendixid"/>&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>" onclick="Javascript:createCookie('ia_j<%=jid%>');"><bean:message bundle="PRM" key="prm.appendix.view"/></a></td>
					<td  class="<%=bgcolortext1%>"><bean:write name="jlist" property="jobcreatedby"/></td>
					<td  class="<%=bgcolortext1%>"><a id='za_j<%=jid%>' href="AssignOwner.do?jobid=<bean:write name="jlist" property="jobid"/>&currentOwner=<bean:write name="jlist" property="jobcreatedby"/>&appendix_Id=<bean:write name="jlist" property="appendixid"/>&function=view&viewjobtype=<bean:write name="AppendixHeaderForm" property="viewjobtype"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>" onclick="Javascript:createCookie('za_j<%=jid%>');"><bean:message bundle="PRM" key="prm.appendix.edit"/></a></td>
				</tr>
					<%i++; %>
					</logic:iterate>
				</logic:present>
			</table>
		<%}else{%>
				 <tr><td  colspan = "10" class = "message" height = "30"><bean:message bundle="PRM" key="prm.filter.criteria"/></td></tr>
			<%} %>	
		<%}else{%>
				<% if(viewType.equals("jobSearch") && (searchPage.equals("jobSearch"))) {%>
				<tr><td colspan = "10" class = "message" height = "30">&nbsp;&nbsp;<bean:message bundle="PRM" key="prm.appendix.nojobspresent"/></td></tr> 	
				<%}%>
			<%}%>
		</td>
	</tr>
</table>
</td>
</tr>
</table>
</BODY>
</html:form>
<script>
function viewpowo(jobid, powoid, type) {
    window.location.href = "POWOAction.do?jobid="+jobid+"&function_type=view&order_type="+type+"&powoid="+powoid;
	return false;	
}
var str = '';
function sortwindow() {
	/*for filter sort*/
	var currentstatus=document.forms[0].viewjobtype.value;
	if(currentstatus=="%") {
		currentstatus="A";
	}

	if(document.forms[0].viewjobtype.value == 'jobSearch') {
		if(document.forms[0].dateOnSite.checked)
			document.forms[0].dateOnSite.value ="OS";
		else
			document.forms[0].dateOnSite.value ="";
			
		if(document.forms[0].dateOffSite.checked)
			document.forms[0].dateOffSite.value ="FS";
		else
			document.forms[0].dateOffSite.value ="";
			
		if(document.forms[0].dateComplete.checked)
			document.forms[0].dateComplete.value ="CO";
		else
			document.forms[0].dateComplete.value ="";	
			
		if(document.forms[0].dateClosed.checked)
			document.forms[0].dateClosed.value ="CL";
		else
			document.forms[0].dateClosed.value ="";	

	
	}	

	str = 'SortAction.do?Type=prj_job&appendixid=<%= appendixid%>&status='+currentstatus+'&ownerId='+document.forms[0].ownerId.value+'&viewjobtype='+document.forms[0].viewjobtype.value;
	p = window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' ); 
	return true;
}
function Backaction() {
	document.forms[0].target = "_self";
	document.forms[0].action = "InvoiceJobDetail.do?type="+document.forms[0].inv_type.value+"&id="+document.forms[0].inv_id.value+"&rdofilter="+document.forms[0].inv_rdofilter.value+"&invoice_Flag="+document.forms[0].invoice_Flag.value+"&invoiceno="+document.forms[0].invoiceno.value+"&partner_name="+document.forms[0].partner_name.value+"&powo_number="+document.forms[0].powo_number.value+"&from_date="+document.forms[0].from_date.value+"&to_date="+document.forms[0].to_date.value;
	document.forms[0].submit();
	return true;
}
function searchJobs()
{

	if(Date.parse(document.forms[0].job_search_date_from.value)>Date.parse(document.forms[0].job_search_date_to.value))
	{
	alert("\"From Date\" should be less than \"To Date\"");
	return false;
	}
		if(document.forms[0].dateOnSite.checked)
			document.forms[0].dateOnSite.value ="OS";
		else
			document.forms[0].dateOnSite.value ="";
			
		if(document.forms[0].dateOffSite.checked)
			document.forms[0].dateOffSite.value ="FS";
		else
			document.forms[0].dateOffSite.value ="";
			
		if(document.forms[0].dateComplete.checked)
			document.forms[0].dateComplete.value ="CO";
		else
			document.forms[0].dateComplete.value ="";	
			
		if(document.forms[0].dateClosed.checked)
			document.forms[0].dateClosed.value ="CL";
		else
			document.forms[0].dateClosed.value ="";	

	document.forms[0].action ="AppendixHeader.do?function=view&viewjobtype=jobSearch&appendixid=<%=appendixid%>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&fromPage="+document.forms[0].fromPage.value;	
	return true;

}

function emptySearchCriteria()
{
		
		document.forms[0].job_search_date_to.value="";
		document.forms[0].job_search_date_from.value="";
		document.forms[0].dateOnSite.checked =false;
		document.forms[0].dateOffSite.checked =false;
		document.forms[0].dateComplete.checked =false;
		document.forms[0].dateClosed.checked =false;
		document.forms[0].job_search_name.value ="";	
		document.forms[0].job_search_site_number.value ="";	
		document.forms[0].job_search_city.value ="";	
		document.forms[0].job_search_state.value ="0";	
		document.forms[0].job_search_owner.value =" ";

}

function changeOwner()
{
document.forms[0].job_search_owner.value = "";
document.forms[0].action = "AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid="+document.forms[0].appendixid.value+"&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&fromPage=msa&msaId=<bean:write name="AppendixHeaderForm" property="msaId"/>";
document.forms[0].submit();
return true;
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="AppendixHeader.do?function=view&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="AppendixHeader.do?function=view&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
</html:html>
