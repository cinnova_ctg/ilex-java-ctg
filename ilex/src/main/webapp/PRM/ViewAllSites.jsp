<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>



<html:html>
<% 
	
	String setvalue = "";
	String selectedradio = "";
	int k=0;
	int size=0;
	if(request.getAttribute( "size" ).toString()!=null)
	{
		size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
	}
	
%>
<HEAD>


<%@ include  file="/Header.inc" %>


<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<title></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>

<%@ include  file="/Menu.inc" %>

</head>

<body>

<table>
<html:form action="SiteSearch">
<html:hidden property="ref"/>
<html:hidden property="msa_id"/>
<html:hidden property="jobid"/>
<html:hidden property="appendixid"/>
<html:hidden property = "reftype"/>
<html:hidden property = "lo_ot_id" />


<html:hidden property="siteid"/>

<table  border="0" cellspacing="1" cellpadding="1"  >
  <tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  <tr>
    <td colspan="4"class="labeleboldwhite" height="30" >
    <bean:message bundle = "PRM" key = "prm.UploadCSV.uploadedSites"/>
    </td>
  </tr> 
  
  <tr> 
      <td  class="labelebold" height="20" ><bean:message bundle="PRM" key="prm.sitesearch.sitename" /></td>
	<td  class="labele" height="20" ><html:text  styleClass="textbox" size="20" name="SiteSearchForm" property="site_name" maxlength="50"/></td>
	<td  class="labelebold" height="20" ><bean:message bundle="PRM" key="prm.sitesearch.sitenumber" /></td>
	<td  class="labele" height="20" ><html:text  styleClass="textbox" size="20" name="SiteSearchForm" property="site_number" maxlength="50"/></td> 
 </tr>
  
  
  <tr> 
	<td  class="buttonrow" height="20" colspan="4">
		<html:submit property="search" styleClass="button">
			<bean:message bundle="PRM" key="prm.sitesearch.search" />
		</html:submit>
		<html:reset property="reset" styleClass="button">
			<bean:message bundle="PRM" key="prm.sitesearch.reset" />
		</html:reset>
		<html:button property = "close" styleClass = "button" onclick = "javascript:window.close();"><bean:message bundle = "PRM" key = "prm.UploadCSV.close"/></html:button>		
	</td> 
 	
  </tr>
  </table>
 </td>
 </tr>
</table>
  

<table  border="0" cellspacing="1" cellpadding="1"  >
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
   

<logic:present name="sitesearchlist" scope="request">
 <tr>
    <td  width="100%" class="labeleboldwhite" height="30" colspan="7"><bean:message bundle="PRM" key="prm.sitesearch.basicsearchresults" /></td>
  </tr>
 
  <tr > 
    <td  width="20"></td>
    <td class="tryb" width="20"><bean:message bundle="PRM" key="prm.sitesearch.sr#" /></td>
    <td class="tryb"  width="120"><bean:message bundle="PRM" key="prm.sitesearch.sitename" /></td>
    <td class="tryb"  width="120"><bean:message bundle="PRM" key="prm.sitesearch.sitenumber" /></td>
    <td class="tryb" width="150"><bean:message bundle="PRM" key="prm.sitesearch.address" /></td>
	
	<td class="tryb" width="100"><bean:message bundle="PRM" key="prm.sitesearch.city" /></td>
    <td class="tryb"  width="100"><bean:message bundle="PRM" key="prm.sitesearch.state" /></td>
	<td class="tryb"  width="100"><bean:message bundle="PRM" key="prm.sitesearch.zipcode" /></td>
    
	
  </tr>
  
  <%int i=1;
  	int j = 0;
	int selectedradiobutton = 0;
	String bgcolor=""; %>
	
	<logic:iterate id="sslist" name="sitesearchlist">
	
 	<%if((i%2)==0)
		{
			bgcolor="texte"; 
		
		} 
	  else
		{
			bgcolor="texto";
		}
	%>
		
		
		<html:hidden name = "sslist" property = "sitelocalityfactor" />
		<html:hidden name = "sslist" property = "unionsite" />
		<html:hidden name = "sslist" property = "sitedesignator" />
		<html:hidden name = "sslist" property = "siteworklocation" />
		<html:hidden name = "sslist" property = "sitecountry" />
		<html:hidden name = "sslist" property = "sitesecphone" />
		<html:hidden name = "sslist" property = "sitephone" />
		<html:hidden name = "sslist" property = "sitedirection" />
		<html:hidden name = "sslist" property = "sitepripoc" />
		<html:hidden name = "sslist" property = "sitesecpoc" />
		<html:hidden name = "sslist" property = "sitepriemail" />
		<html:hidden name = "sslist" property = "sitesecemail" />
		<html:hidden name = "sslist" property = "sitenotes" />
		<html:hidden name = "sslist" property = "installerpoc" />
		<html:hidden name = "sslist" property = "sitepoc" />
		
		
		
 	
	<bean:define id="slid" name="sslist" property="sitelistid" />
	  <tr>

	<td   height="20">
	     <td  class="<%=bgcolor %>" height="20"><%=i %></td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitename"/>
			 <html:hidden name = "sslist" property = "sitename" />
			
		 </td>
		
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitenumber"/>
			 <html:hidden name = "sslist" property = "sitenumber" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="siteaddress"/>
			 <html:hidden name = "sslist" property = "siteaddress" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitecity"/>
			 <html:hidden name = "sslist" property = "sitecity" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitestatedesc"/>
			 <html:hidden name = "sslist" property = "sitestatedesc" />
			 <html:hidden name = "sslist" property = "sitestate" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitezipcode"/>
			 <html:hidden name = "sslist" property = "sitezipcode" /> 
		 </td>
		 

	  </tr> 
	  
	    <%i++;j++; %>
	</logic:iterate>
	

	<%if(i==1)
	{ %>
		<tr>
			<td>&nbsp;</td>
			<td  class="message" height="30" colspan="7"><bean:message bundle="PRM" key="prm.UploadCSV.NoUploadedSites" /></td>
		</tr>
	<%} %>

</logic:present>  


	
	
	


  </table>
 </td>
 </tr>
 
</table> 
</html:form>
</table>
</BODY>

<script>
</script>
</html:html>
