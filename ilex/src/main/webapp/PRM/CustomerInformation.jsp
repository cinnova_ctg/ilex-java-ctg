<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>


<html:html>
<%
	int size=0;
	/* For Snapon: Start */
	boolean checkSnapon = false;
	String colspan = "3";
	if(request.getAttribute("checkSnapon") !=null && request.getAttribute("checkSnapon").toString().equals("true")){
		checkSnapon = true;
		colspan = "4";
	}
	/* For Snapon: End */
%>
<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

</head>

<%@ include  file="/Menu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >


<!--  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing=1" cellpadding="0" height="18">
        <tr align="left"> 
         <td class="toprow1" width=200>
           
          <a href="AppendixHeader.do?appendixid=<%=request.getAttribute("typeid") %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtoappendixdashboard"/></a></td>
		  
		  </td>
		   <td  width=600 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>-->

<table>
<html:form action="CustomerInformation" >

<html:hidden property="function" />
<html:hidden property="type" />
<html:hidden property="typeid" />
<html:hidden property="appendixid" />
<html:hidden property="from" />
<html:hidden property="appendixname" />
<html:hidden property="jobname" />
<html:hidden property="nettype" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />

<%if(request.getAttribute("viewflag").equals("0")) 
{%>

<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="750">
	  		
  			<logic:present name="addmessage">
			<logic:equal name="addmessage" value="-9001">
			<tr>
				<td  colspan="5" class="message" height="30" ><bean:message bundle="PRM" key="prm.custinfo.errorindeleting"/></td>
				
			</tr>
	 		</logic:equal>
	 		</logic:present>
			
	  		
	  		<tr>
	  			<td colspan="5" class="labeleboldwhite" height="30"><bean:message bundle="PRM" key="prm.custinfo.customerinfofor"/>&nbsp;<bean:message bundle="PRM" key="prm.custinfo.appendix"/><bean:write name="CustomerInformationForm" property="appendixname" />&nbsp;<bean:message bundle="PRM" key="prm.custinfo.arrowmark"/><bean:message bundle="PRM" key="prm.custinfo.job"/><bean:write name="CustomerInformationForm" property="jobname" /></td>
	  			
	  		</tr>
	  		
	  		<logic:notEqual name="CustomerInformationForm" property="custParameter1" value="">
	  		<tr> 
			    <td class="labelebold" width="10"><bean:message bundle="PRM" key="prm.custinfo.1"/></td>
			    <td class="custlabele" width="150"><html:hidden property="appendixcustinfoid1" /><html:hidden property="custParameter1" /><bean:write name="CustomerInformationForm"  property="custParameter1" /></td>
			    <td class="custlabele" width="190"><html:text  styleClass="textbox" size="25" property="custvalue1" /></td>
			    <%if(checkSnapon){ %>
			    <td class="custlabele" width="250"><bean:write name="CustomerInformationForm"  property="snCustvalue1" /></td>
			    <%} %>
				<td width="340"></td>
			</tr>
			<%size++; %>
			</logic:notEqual>
		    
		    <logic:notEqual name="CustomerInformationForm" property="custParameter2" value="">
		     <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.2"/></td>
	   			 <td  class="labelo"><html:hidden property="appendixcustinfoid2" /><html:hidden property="custParameter2" /><bean:write name="CustomerInformationForm"  property="custParameter2" /></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="25" property="custvalue2" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue2" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
 
	
			<logic:notEqual name="CustomerInformationForm" property="custParameter3" value="">
	  		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.3"/></td>
	   			 <td  class="custlabele"><html:hidden property="appendixcustinfoid3" /><html:hidden property="custParameter3" /><bean:write name="CustomerInformationForm"  property="custParameter3" /></td>
	   			 <td  class="custlabele"><html:text  styleClass="textbox" size="25" property="custvalue3" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue3" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
	 	<!-- start --> 
	 		<logic:notEqual name="CustomerInformationForm" property="custParameter4" value="">
	  		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.4"/></td>
	   			 <td  class="labelo"><html:hidden property="appendixcustinfoid4" /><html:hidden property="custParameter4" /><bean:write name="CustomerInformationForm"  property="custParameter4" /></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="25" property="custvalue4" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue4" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
	 		 
	 		 <logic:notEqual name="CustomerInformationForm" property="custParameter5" value="">
	  		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.5"/></td>
	   			 <td  class="custlabele"><html:hidden property="appendixcustinfoid5" /><html:hidden property="custParameter5" /><bean:write name="CustomerInformationForm"  property="custParameter5" /></td>
	   			 <td  class="custlabele"><html:text  styleClass="textbox" size="25" property="custvalue5" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue5" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
	 		 
	 		 <logic:notEqual name="CustomerInformationForm" property="custParameter6" value="">
	  		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.6"/></td>
	   			 <td  class="labelo"><html:hidden property="appendixcustinfoid6" /><html:hidden property="custParameter6" /><bean:write name="CustomerInformationForm"  property="custParameter6" /></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="25" property="custvalue6" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue6" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
	 		 
	 		 <logic:notEqual name="CustomerInformationForm" property="custParameter7" value="">
	  		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.7"/></td>
	   			 <td  class="custlabele"><html:hidden property="appendixcustinfoid7" /><html:hidden property="custParameter7" /><bean:write name="CustomerInformationForm"  property="custParameter7" /></td>
	   			 <td  class="custlabele"><html:text  styleClass="textbox" size="25" property="custvalue7" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue7" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
	 		 
	 		 <logic:notEqual name="CustomerInformationForm" property="custParameter8" value="">
	  		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.8"/></td>
	   			 <td  class="labelo"><html:hidden property="appendixcustinfoid8" /><html:hidden property="custParameter8" /><bean:write name="CustomerInformationForm"  property="custParameter8" /></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="25" property="custvalue8" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue8" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
	 		 
	 		 <logic:notEqual name="CustomerInformationForm" property="custParameter9" value="">
	  		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.9"/></td>
	   			 <td  class="custlabele"><html:hidden property="appendixcustinfoid9" /><html:hidden property="custParameter9" /><bean:write name="CustomerInformationForm"  property="custParameter9" /></td>
	   			 <td  class="custlabele"><html:text  styleClass="textbox" size="25" property="custvalue9" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue9" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
	 		 
	 		 <logic:notEqual name="CustomerInformationForm" property="custParameter10" value="">
	  		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.10"/></td>
	   			 <td  class="labelo"><html:hidden property="appendixcustinfoid10" /><html:hidden property="custParameter10" /><bean:write name="CustomerInformationForm"  property="custParameter10" /></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="25" property="custvalue10" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue10" /></td>
			     <%} %>
	   			 <td></td>
	 		 </tr>
	 		 <%size++; %>
	 		 </logic:notEqual>
	 		 
	 	<!-- over -->
	 		<%if(size==0)
			{ %>
	 		 <tr>
	 		 	<td  colspan="4" class="message" height="30" ><bean:message bundle="PRM" key="prm.custinfo.noparameters"/></td>
	 		 </tr>
	 		 <tr>
	 		 	<td colspan="3"><html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button></td>
	 		 	<td></td>
	 		 </tr>
	 		 <%} 
	 		 else
	 		 {%>
	 		  <tr> 
	 		 	  <td colspan="<%=colspan%>" class="buttonrow"> 
	 		      <html:submit property="save" styleClass="button" >
	 		      <bean:message bundle="PRM" key="prm.save" /></html:submit>
     			  <html:reset  property="reset" styleClass="button"><bean:message bundle="PRM" key="prm.reset" /></html:reset>
				  <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  </td>
				  <td></td>
			 </tr>
	 		 	
	 		 <%} %>
	 		 
	 		 
	 		 
	 		
	 		<!--  <tr> 
	 		 	  <td colspan="3" class="buttonrow"> 
	 		      <html:submit property="save" styleClass="button" >
	 		      <bean:message bundle="PRM" key="prm.save" /></html:submit>
     			  <html:reset  property="reset" styleClass="button"><bean:message bundle="PRM" key="prm.reset" /></html:reset>
				  <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  </td>
			 </tr> -->
	  
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
<%}
else
{%>
<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="750">
	  		<logic:present name="addmessage">
			<logic:equal name="addmessage" value="0">
			<tr>
				<td  colspan="4" class="message" height="30" ><bean:message bundle="PRM" key="prm.custinfo.addedsuccessfully"/></td>
			</tr>
	 		</logic:equal>
	 		</logic:present>
	  		
	  		<tr>
	  			<td colspan="4" class="labeleboldwhite" height="30"><bean:message bundle="PRM" key="prm.custinfo.customerinfofor"/><bean:message bundle="PRM" key="prm.custinfo.appendix"/><bean:write name="CustomerInformationForm" property="appendixname" /><bean:message bundle="PRM" key="prm.custinfo.arrowmark"/><bean:message bundle="PRM" key="prm.custinfo.job"/><bean:write name="CustomerInformationForm" property="jobname" /></td>
	  		</tr>
	  		 
	  		<logic:notEqual name="CustomerInformationForm" property="custParameter1" value="">
	  		<tr>
	   			 <td class="labelobold" height="20" width="10"><bean:message bundle="PRM" key="prm.custinfo.1"/></td>
	   			 <td class="labelo" height="20" width="150"><bean:write name="CustomerInformationForm" property="custParameter1" /></td>
	   			 <td class="labelo" height="20" width="190"><bean:write name="CustomerInformationForm" property="custvalue1" /></td>
	   			 <%if(checkSnapon){ %>
			     <td class="labelo" width="200"><bean:write name="CustomerInformationForm"  property="snCustvalue1" /></td>
			     <%} %>
	   			 <td width="340"></td>
	  			 
	  		</tr>
	  		</logic:notEqual>
	 
	    	<logic:notEqual name="CustomerInformationForm" property="custParameter2" value="">
	    	<tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.2"/></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custParameter2" /></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custvalue2" /></td>
			    <%if(checkSnapon){ %>
			    <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue2" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    
		    <logic:notEqual name="CustomerInformationForm" property="custParameter3" value="">
		    <tr> 
			    <td class="labelobold" height="20"><bean:message bundle="PRM" key="prm.custinfo.3"/></td>
			    <td class="labelo" height="20"><bean:write name="CustomerInformationForm" property="custParameter3" /></td>
			    <td class="labelo" height="20"><bean:write name="CustomerInformationForm" property="custvalue3" /></td>
			    <%if(checkSnapon){ %>
			    <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue3" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
	<!-- start -->
			<logic:notEqual name="CustomerInformationForm" property="custParameter4" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.4"/></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custParameter4" /></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custvalue4" /></td>
			    <%if(checkSnapon){ %>
			    <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue4" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="CustomerInformationForm" property="custParameter5" value="">
		    <tr> 
			    <td class="labelobold" height="20"><bean:message bundle="PRM" key="prm.custinfo.5"/></td>
			    <td class="labelo" height="20"><bean:write name="CustomerInformationForm" property="custParameter5" /></td>
			    <td class="labelo" height="20"><bean:write name="CustomerInformationForm" property="custvalue5" /></td>
			    <%if(checkSnapon){ %>
			    <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue5" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="CustomerInformationForm" property="custParameter6" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.6"/></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custParameter6" /></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custvalue6" /></td>
			    <%if(checkSnapon){ %>
			    <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue6" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="CustomerInformationForm" property="custParameter7" value="">
		    <tr> 
			    <td class="labelobold" height="20"><bean:message bundle="PRM" key="prm.custinfo.7"/></td>
			    <td class="labelo" height="20"><bean:write name="CustomerInformationForm" property="custParameter7" /></td>
			    <td class="labelo" height="20"><bean:write name="CustomerInformationForm" property="custvalue7" /></td>
			    <%if(checkSnapon){ %>
			    <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue7" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="CustomerInformationForm" property="custParameter8" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.8"/></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custParameter8" /></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custvalue8" /></td>
			    <%if(checkSnapon){ %>
			    <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue8" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="CustomerInformationForm" property="custParameter9" value="">
		    <tr> 
			    <td class="labelobold" height="20"><bean:message bundle="PRM" key="prm.custinfo.9"/></td>
			    <td class="labelo" height="20"><bean:write name="CustomerInformationForm" property="custParameter9" /></td>
			    <td class="labelo" height="20"><bean:write name="CustomerInformationForm" property="custvalue9" /></td>
			    <%if(checkSnapon){ %>
			    <td class="labelo"><bean:write name="CustomerInformationForm"  property="snCustvalue9" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="CustomerInformationForm" property="custParameter10" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.10"/></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custParameter10" /></td>
			    <td class="custlabele" height="20"><bean:write name="CustomerInformationForm" property="custvalue10" /></td>
			    <%if(checkSnapon){ %>
			    <td class="custlabele"><bean:write name="CustomerInformationForm"  property="snCustvalue10" /></td>
			    <%} %>
				<td></td>
		    </tr>
		    </logic:notEqual>
	
	<!-- over -->
	  		  <tr> 
	 		 	  <td colspan="<%=colspan%>" class="buttonrow"> 
	 		      <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  </td>
				  <td></td>
			 </tr>
	  		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
<%} %>
</html:form>
</table>
<body>

<script>


function Backaction()
{
	if( document.forms[0].from.value == 'jobdashboard' )
	{
		document.forms[0].action = "JobDashboardAction.do?jobid=<bean:write name = "CustomerInformationForm" property = "typeid" />";
	}
	else
	{
		if(document.forms[0].nettype.value == 'dispatch')
			document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype=dispatch&viewjobtype=<bean:write name = "CustomerInformationForm" property = "viewjobtype" />&ownerId=<bean:write name = "CustomerInformationForm" property = "ownerId" />";
		else
			document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getAttribute("appendixid") %>&function=view&viewjobtype=<bean:write name = "CustomerInformationForm" property = "viewjobtype" />&ownerId=<bean:write name = "CustomerInformationForm" property = "ownerId" />";
	}
	document.forms[0].submit();
	return true;
}
</script>
</html:html>