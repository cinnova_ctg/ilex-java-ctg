<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@page import="java.util.Date"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<script language = "JavaScript" src = "javascript/datetimepicker_css.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = 'JavaScript' src = 'javascript/jquery-1.11.0.min.js'></script>
<script language = 'JavaScript' src = 'javascript/jquery.blockUI-2.66.0.js'></script>
<script language = 'JavaScript' src = 'javascript/jquery.selectboxes.min.js'>

</script>	
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project Manager view selector</title>
</head>



<% 
/* Variables of View Selector */
	
	String prjType = "";
	String pageName = "";
	int addStatusRow = -1;
	int addOwnerRow = -1;
	boolean addStatusSpace = false;
	boolean addOwnerSpace = false;
	String checkowner = null;
	String checkstatus = null;
	int rowHeight = 120;
	if(request.getParameter("projectType") != null){ prjType = request.getParameter("projectType"); }
	if(request.getParameter("pageName") != null){ pageName = request.getParameter("pageName"); }
	String formBean = request.getParameter("bean");
%>

<!-- For Assign Team: Start -->
<%@ include file = "/NMenu.inc" %> 					<!-- Include file for Menu CSS -->
<%@ include  file="/AjaxOwners.inc" %>  			<!-- Include file for Ajax call -->
<%@ include  file="/DashboardStatusScript.inc" %> 	<!-- Include file for JavaScript function -->

<%if(prjType.equals("Appendix")) { // - For Appendix: Start%>
<!-- Include file get parameter and Script function for Appendix Dashboard Menu -->
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %> 	
<Body onLoad="MM_preloadImages('images/Expand.gif','images/Collapse.gif'); CookieGetOpenDiv(); showaccountmore();leftAdjLayers();" >
<!-- Hidden element for menu: Start -->
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property = "msaId" />
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property="fromPage"/>





<!-- Hidden element for menu: End -->

<!-- Table for Menu and View Selector: Start -->
<!--  Menu change color -->
<%@ include  file="/AppedixDashboardMenu.inc" %>
	<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li><a href="JobEditAction.do?ref=inscopeview&amp;appendix_Id=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;Job_Id=0">Add Job</a></li>
        		<li>
        			<a href="#"><span>Bulk Job</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=<%= appendixid %>">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=<%= appendixid %>">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li>
				
	        		<% if( contractDocMenu != "" ) { %>
	        		<li>
	        			<a href="#"><span>Contract Documents</span></a>
	        			<ul>
							<%= contractDocMenu %>
	        			</ul>
	        		</li>
				<% } %>
				
        		<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Custom Fields</a></li>
        		<li><a href="AddCustRef.do?typeid=<%= appendixid %>&amp;type=Appendix&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Customer Reference</a></li>
        		<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">External Sales Agent</a></li>
        		<li>
        			<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;initialCheck=true&amp;pageType=jobsites">Job Sites</a></li>
        		<li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=<%= appendixid %>&amp;type=P&amp;page=appendixdashboard&amp;pageid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Update</a></li>
						
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Special Instructions/Conditions</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">Team</a></li>
        		<li><a href="OnlineBid.do?appendixId=<%= appendixid %>&amp;fromPage=Appendix">Online Bid</a></li>
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
        		<li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li>
				<li><a href="MSPCCReportAction.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">MSP C/C Report</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid %>">Search</center></a></td>
						
  		</li>
  	
	</ul>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	     <table cellpadding="0" cellspacing="0" border="0" width = "100%">
			<tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		
								 	
				        </td>
			       <%if(pageName.equals("AssignTeam.jsp")) {%>
						<tr>
							<td colspan ="4" ><h2><bean:message bundle="PRM" key="prm.assign.team.netmedex"/>&nbsp;<bean:write name ="<%=formBean%>" property="appendixName" /></h2></td>
						</tr>
					<%} %>
			       </tr>
			       
			       
		</table>		
   </td>
      
    <td width="35" valign="top"><img width="35" height="60" alt="" src="images/content_head_02.jpg" id="content_head_02"></td>
	<td width="155" valign="top">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr>
		          <td width="155" height="39" colspan="2" class="headerrow">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tbody><tr>
			                <td width="150">
			                	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				                   	<tbody><tr>
							            <td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('II');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" title="Scheduled" id="Image10" alt="Month" src="http://localhost:8080/Ilex/images/Icon_Scheduled.gif"></a></td>
	  									<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IO');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" title="Scheduled OverDue" id="Image10" alt="Month" src="images/Icon_Schedule_Overdued.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('ION');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" title="OnSite" id="Image10" alt="Month" src="images/Icon_Onsite.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IOF');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" title="Offsite" id="Image10" alt="Month" src="images/Icon_Ofsite.gif"></a></td>
				                    </tr>
			                  	</tbody>
			                  	</table>
			                </td>
			              </tr>
			          </tbody></table>
		          </td>
		        </tr>
        		<tr>
          		<td width="140" height="21" background="images/content_head_04.jpg">
          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              		<tbody><tr>
              		<td width="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li id="closeNow" name="closeNow"><a onclick="ShowDiv();" title="Open View" href="javascript:void(0)">My Views <img border="0" onclick="ShowDiv();" title="Open View" src="images/showFilter.gif"></a><a class="imgstyle" href="javascript:void(0)"><img border="0" onclick="hideDiv();" title="Close View" src="images/offFilter2.gif"></a></li>
									</div>								
	                			</div>		
						<span>
								<div class="divstyle" id="filter" style="">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tbody><tr>
		                      <td width="50%" valign="top">
		                      <!--   -->
		                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Status View</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText">
											<logic:present name="jobStatusList" scope="request">
												<logic:iterate id="list" name="jobStatusList">
													<bean:define id="statusName" name="list" property="statusdesc" type="java.lang.String" />
													<bean:define id="statusId" name="list" property="statusid" type="java.lang.String" />
													<%checkstatus = "javascript: checkingStatus('" + statusId + "');";
													if ((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
														addStatusRow++; addStatusSpace = true;
													}%>
													<%if (addStatusSpace) {%>
													&nbsp;&nbsp;&nbsp;&nbsp;
													<%}%>
													<html:multibox property="jobSelectedStatus" onclick="<%=checkstatus%>">
														<bean:write name="list" property="statusid" />
													</html:multibox>
													<bean:write name="list" property="statusdesc" />
													<br> 
													<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present> <img src="images/hrfilterLine.gif" width="100px" class="imagefilter" />
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="week">This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="month">This Month</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>"	property="jobMonthWeekCheck" value="clear">Clear</html:radio></td></tr>
											</table>
											</td>
										</tr>
									</table>
		                      
		                      
		                      <!--   -->
		                      <%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                          <tbody><tr>
                           	 <td class="filtersCaption" colspan="3">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										
									    	 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('AI');" value="AI" name="jobSelectedStatus">   
											 	Inwork<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IS');" value="IS" name="jobSelectedStatus">   
											 	To Be Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('II');" checked="checked" value="II" name="jobSelectedStatus">   
											 	Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IO');" checked="checked" value="IO" name="jobSelectedStatus">   
											 	Scheduled Overdue<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('ION');" checked="checked" value="ION" name="jobSelectedStatus">   
											 	Onsite<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IOF');" checked="checked" value="IOF" name="jobSelectedStatus">   
											 	Offsite<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('F');" value="F" name="jobSelectedStatus">   
											 	Complete<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('O');" value="O" name="jobSelectedStatus">   
											 	Closed<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('H');" value="H" name="jobSelectedStatus">   
											 	Hold<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('C');" value="C" name="jobSelectedStatus">   
											 	Cancelled<br> 
											 	
											
												
										<img width="100px" class="imagefilter" src="images/hrfilterLine.gif">
										<table cellpadding="0">
											<tbody><tr><td class="tabNormalText"><input type="radio" value="week" name="jobMonthWeekCheck" onclick="checkCustomSelection()">This Week</td></tr>
											<tr><td class="tabNormalText"><input type="radio"  checked="checked" value="month" name="jobMonthWeekCheck"  onclick="checkCustomSelection()">This Month</td></tr>	
											<tr><td class="tabNormalText"><input type="radio"  value="custom" name="jobMonthWeekCheck"  onclick="checkCustomSelection()">Custom</td></tr>	
											<tr id="drange" ><td class="tabNormalText" >
											 
												<table style="border: 0px">
													<tr>
													  <td  class="tabNormalText">From Date:</td>
													  <td  class="tabNormalText">
													
								<input class="textbox" type="text"  value="<%=new java.text.SimpleDateFormat("MM-dd-yyyy").format(new Date()) %>" readonly="true" name="fromDate" id="fromDate" size="10">
								
								<img style="cursor:pointer" onclick="javascript:NewCssCal('fromDate')" src="images/calendar.gif">
								
								</td>
													</tr>
													<tr>
													  <td  class="tabNormalText">To Date</td>
													 <td  class="tabNormalText">
								<input class="textbox" type="text"  value="<%=new java.text.SimpleDateFormat("MM-dd-yyyy").format(new Date()) %>" readonly="true" name="toDate" id="toDate" size="10">
								
								<img style="cursor:pointer" onclick="javascript:NewCssCal('toDate')" src="images/calendar.gif"></td>
													</tr>
												</table> 
												
											</td></tr>
											
											
											 
										</tbody></table>	
									</td>
	    					</tr>
                        </tbody></table> --%>
                        
                        
                        </td>
                      <td><img width="1px" height="120" class="imagefilter" src="images/filterLine.gif"></td>
                      <td width="50%" valign="top">
                    <!--  Add code  -->
                   <table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table> 
									</td>
								</tr>
							</table>
                    
                    <!--  End  -->  
                   <%--    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td class="filtersCaption" colspan="3">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id="OwnerSpanId"> 
					   			
							    	
							    		
							    		
								    		
							    			 
							    		<input type="checkbox" onclick="javascript: checkingOwner('<%=ownerId%>');" checked="checked" value="7" name="jobSelectedOwners">   
										  Me
										  <br> 
									
									</span>
								
								
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img height="34" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="changeFilter();" class="divbutton" src="images/showBtn.gif"><br></a></td></tr>
                              <tr><td colspan="3"><img width="2" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="resetFilter();" class="divbutton" src="images/reset.gif"><br></a></td></tr>                              
                              <tr><td colspan="3"><img height="8" src="images/spacer.gif"></td></tr>
                              </tbody></table> --%>
                              
                              
                              
                              
                              </td>
                        </tr>
  			</tbody></table>											
		</div>
	</span>
				</td>
	
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      </td>
 </tr>
 </tbody>
 </table>

<%-- <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			         
						
			    </td>
			</tr>
	        <td colspan ="4" ><h2><bean:message bundle="PRM" key="prm.assign.team.appendix"/>&nbsp;<bean:write name ="<%=formBean%>" property="appendixName" /></h2></td>
		</tbody></table> --%>	
			       
			      
			       
			       
		
			


<!--  End of Change -->
<%-- <table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
<tr>
	<!-- Top Menu: Start -->
	<td valign="top" width = "100%">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		<tr>
			<td valign="top" width = "100%">
				<%if(prjType.equals("Appendix")) {%>
				<table cellpadding="0" cellspacing="0" border="0" width = "100%">
					<tr> 
						<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
						<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
						<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid %>" style="width: 120px"><center>Search</center></a></td>
						<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					</tr>
					<tr>
						<td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%"><div id="breadCrumb">&nbsp;</div></td>
					</tr>
					<%if(pageName.equals("AssignTeam.jsp")) {%>
						<tr>
							<td colspan ="4" ><h2><bean:message bundle="PRM" key="prm.assign.team.appendix"/>&nbsp;<bean:write name ="<%=formBean%>" property="appendixName" /></h2></td>
						</tr>
					<%} %>
				</table>
				<%} %>
			</td>
		</tr>
	</table>
	</td>
	<!-- Top Menu: End -->
	
	<!-- Top sky blue: Start -->
	<td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<!-- Top sky blue: End -->
	
	<!-- View Selector: Start -->
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="155" height="39" class="headerrow" colspan="2">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="150">
						<table width="100%" cellpadding="0" cellspacing="3" border="0">
							<tr>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" 
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');" /></a>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td height="21" background="images/content_head_04.jpg" width="140">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="70%" colspan="2">&nbsp;</td>
						<td nowrap="nowrap" colspan="2">
							<div id="featured1">
								<div id="featured">
									<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img	src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();" /></a><a href="javascript:void(0)"
										class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();" /></a>
									</li>
								</div>
							</div>
							<span>
							<div id="filter" class="divstyle">
							<table width="250" cellpadding="0" class="divtable">
								<tr>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Status View</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText">
											<logic:present name="jobStatusList" scope="request">
												<logic:iterate id="list" name="jobStatusList">
													<bean:define id="statusName" name="list" property="statusdesc" type="java.lang.String" />
													<bean:define id="statusId" name="list" property="statusid" type="java.lang.String" />
													<%checkstatus = "javascript: checkingStatus('" + statusId + "');";
													if ((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
														addStatusRow++; addStatusSpace = true;
													}%>
													<%if (addStatusSpace) {%>
													&nbsp;&nbsp;&nbsp;&nbsp;
													<%}%>
													<html:multibox property="jobSelectedStatus" onclick="<%=checkstatus%>">
														<bean:write name="list" property="statusid" />
													</html:multibox>
													<bean:write name="list" property="statusdesc" />
													<br> 
													<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present> <img src="images/hrfilterLine.gif" width="100px" class="imagefilter" />
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="week">This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="month">This Month</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>"	property="jobMonthWeekCheck" value="clear">Clear</html:radio></td></tr>
											</table>
											</td>
										</tr>
									</table>
									</td>
									<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter" /></td>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table> 
									</td>
								</tr>
							</table>
							</div>
							</span>		
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</td>
	<!-- View Selector: End -->
</tr>
</table> --%>
<!-- Table for Menu and View Selector: End -->

<!-- Inclue file for Menu Script -->
<%@ include  file="/AppedixDashboardMenuScript.inc" %> <!-- Include file for appendix menu script -->
<%}	//- For Appendix: End%>

<%if(prjType.equals("NetMedX")) { //- For NetMedX: Start%>
<!-- Include file get parameter and Script function for Appendix Dashboard Menu -->
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %> 	

<!-- Hidden element for menu: Start -->
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property = "msaId" />
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property="fromPage"/> 
<!-- Hidden element for menu: End -->

<!-- Table for Menu and View Selector: Start -->
<div id="menunav">
  
<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
        	
        		
				<!-- <li><a href="ReportScheduleWindow.do?report_type=redline&typeid=3970&fromPage=NetMedX">Redline Report</a></li>
				<li><a href="report_type=detailsummary&typeid=3970&fromPage=NetMedX">Detailed Summary</a></li>
				<li><a href="ReportScheduleWindow.do?report_type=CSVSummary&typeid=3970&fromPage=NetMedX">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="ReportGeneration.do?firsttime=true">Update Report</a></li></ul></li></ul></li>
        	 -->
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>">Search</a>
  		</li>
  		
	</ul>
</li>

</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	     <table cellpadding="0" cellspacing="0" border="0" width = "100%">
			<tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		
								 	
				        </td>
			       <%if(pageName.equals("AssignTeam.jsp")) {%>
						<tr>
							<td colspan ="4" ><h2><bean:message bundle="PRM" key="prm.assign.team.netmedex"/>&nbsp;<bean:write name ="<%=formBean%>" property="appendixName" /></h2></td>
						</tr>
					<%} %>
			       </tr>
			       
			       
		</table>		
   </td>
      
    <td width="35" valign="top"><img width="35" height="60" alt="" src="images/content_head_02.jpg" id="content_head_02"></td>
	<td width="155" valign="top">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr>
		          <td width="155" height="39" colspan="2" class="headerrow">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tbody><tr>
			                <td width="150">
			                	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				                   	<tbody><tr>
							            <td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('II');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" title="Scheduled" id="Image10" alt="Month" src="http://localhost:8080/Ilex/images/Icon_Scheduled.gif"></a></td>
	  									<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IO');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" title="Scheduled OverDue" id="Image10" alt="Month" src="images/Icon_Schedule_Overdued.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('ION');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" title="OnSite" id="Image10" alt="Month" src="images/Icon_Onsite.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IOF');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" title="Offsite" id="Image10" alt="Month" src="images/Icon_Ofsite.gif"></a></td>
				                    </tr>
			                  	</tbody>
			                  	</table>
			                </td>
			              </tr>
			          </tbody></table>
		          </td>
		        </tr>
        		<tr>
          		<td width="140" height="21" background="images/content_head_04.jpg">
          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              		<tbody><tr>
              		<td width="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li id="closeNow" name="closeNow"><a onclick="ShowDiv();" title="Open View" href="javascript:void(0)">My Views <img border="0" onclick="ShowDiv();" title="Open View" src="images/showFilter.gif"></a><a class="imgstyle" href="javascript:void(0)"><img border="0" onclick="hideDiv();" title="Close View" src="images/offFilter2.gif"></a></li>
									</div>								
	                			</div>		
						<span>
								<div class="divstyle" id="filter" style="">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tbody><tr>
		                      <td width="50%" valign="top">
		                      
		                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Status View</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText">
											<logic:present name="jobStatusList" scope="request">
												<logic:iterate id="list" name="jobStatusList">
													<bean:define id="statusName" name="list" property="statusdesc" type="java.lang.String" />
													<bean:define id="statusId" name="list" property="statusid" type="java.lang.String" />
													<%checkstatus = "javascript: checkingStatus('" + statusId + "');";
													if ((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
														addStatusRow++; addStatusSpace = true;
													}%>
													<%if (addStatusSpace) {%>
													&nbsp;&nbsp;&nbsp;&nbsp;
													<%}%>
													<html:multibox property="jobSelectedStatus" onclick="<%=checkstatus%>">
														<bean:write name="list" property="statusid" />
													</html:multibox>
													<bean:write name="list" property="statusdesc" />
													<br> 
													<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present> <img src="images/hrfilterLine.gif" width="100px" class="imagefilter" />
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="week">This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="month">This Month</html:radio></td></tr>
											<!--  	<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>"	property="jobMonthWeekCheck" value="clear">Clear</html:radio></td></tr>-->
											<!-- add zahid ghafoor code for new changes   -->	
																<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck"  value="custom" onclick="checkCustomSelection()" />Custom</td></tr>
															
																<tr id="drange" ><td class="tabNormalText" >
											 
																<table style="border: 0px">
																<tr>
													  			<td  class="tabNormalText">From Date:</td>
													  			<td  class="tabNormalText">
													
																<input class="textbox" type="text"  value="<%=new java.text.SimpleDateFormat("MM-dd-yyyy").format(new Date()) %>" readonly="true" name="fromDate" id="fromDate" size="10">
								
																<img style="cursor:pointer" onclick="javascript:NewCssCal('fromDate')" src="images/calendar.gif">
								
															   </td>
															   </tr>
																<tr>
													  			<td  class="tabNormalText">To Date</td>
													 			<td  class="tabNormalText">
												  				<input class="textbox" type="text"  value="<%=new java.text.SimpleDateFormat("MM-dd-yyyy").format(new Date()) %>" readonly="true" name="toDate" id="toDate" size="10">
								
												  			<img style="cursor:pointer" onclick="javascript:NewCssCal('toDate')" src="images/calendar.gif"></td>
												  			</tr>
												  			
												  			</table> 
												
															</td>
															</tr>
															<!--  End  of changes of zahid ghafoor  -->
											
											
											
											</table>
											</td>
										</tr>
									</table>
		                   <%--    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                          <tbody><tr>
                           	 <td class="filtersCaption" colspan="3">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										
									    	 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('AI');" value="AI" name="jobSelectedStatus">   
											 	Inwork<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IS');" value="IS" name="jobSelectedStatus">   
											 	To Be Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('II');" checked="checked" value="II" name="jobSelectedStatus">   
											 	Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IO');" checked="checked" value="IO" name="jobSelectedStatus">   
											 	Scheduled Overdue<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('ION');" checked="checked" value="ION" name="jobSelectedStatus">   
											 	Onsite<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IOF');" checked="checked" value="IOF" name="jobSelectedStatus">   
											 	Offsite<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('F');" value="F" name="jobSelectedStatus">   
											 	Complete<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('O');" value="O" name="jobSelectedStatus">   
											 	Closed<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('H');" value="H" name="jobSelectedStatus">   
											 	Hold<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('C');" value="C" name="jobSelectedStatus">   
											 	Cancelled<br> 
											 	
											
												
										<img width="100px" class="imagefilter" src="images/hrfilterLine.gif">
										<table cellpadding="0">
											<tbody><tr><td class="tabNormalText"><input type="radio" value="week" name="jobMonthWeekCheck" onclick="checkCustomSelection()">This Week</td></tr>
											<tr><td class="tabNormalText"><input type="radio"  checked="checked" value="month" name="jobMonthWeekCheck"  onclick="checkCustomSelection()">This Month</td></tr>	
											<tr><td class="tabNormalText"><input type="radio"  value="custom" name="jobMonthWeekCheck"  onclick="checkCustomSelection()">Custom</td></tr>	
											<tr id="drange" ><td class="tabNormalText" >
											 
												<table style="border: 0px">
													<tr>
													  <td  class="tabNormalText">From Date:</td>
													  <td  class="tabNormalText">
													
								<input class="textbox" type="text"  value="<%=new java.text.SimpleDateFormat("MM-dd-yyyy").format(new Date()) %>" readonly="true" name="fromDate" id="fromDate" size="10">
								
								<img style="cursor:pointer" onclick="javascript:NewCssCal('fromDate')" src="images/calendar.gif">
								
								</td>
													</tr>
													<tr>
													  <td  class="tabNormalText">To Date</td>
													 <td  class="tabNormalText">
								<input class="textbox" type="text"  value="<%=new java.text.SimpleDateFormat("MM-dd-yyyy").format(new Date()) %>" readonly="true" name="toDate" id="toDate" size="10">
								
								<img style="cursor:pointer" onclick="javascript:NewCssCal('toDate')" src="images/calendar.gif"></td>
													</tr>
												</table> 
												
											</td></tr>
											
											
											 
										</tbody></table>	
									</td>
	    					</tr>
                        </tbody></table> --%>
                        
                        
                        
                        </td>
                      <td><img width="1px" height="120" class="imagefilter" src="images/filterLine.gif"></td>
                      <td width="50%" valign="top">
                     <!--  add code  -->
                     
                     
                     
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table> 
									</td>
								</tr>
							</table>
						<!--  End  -->			 
                     <%--  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td class="filtersCaption" colspan="3">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id="OwnerSpanId"> 
					   			
							    	
							    		
							    		
								    		
							    			 
							    		<input type="checkbox" onclick="javascript: checkingOwner('<%=ownerId%>');" checked="checked" value="7" name="jobSelectedOwners">   
										  Me
										  <br> 
									
									</span>
								
								
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img height="34" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="changeFilter();" class="divbutton" src="images/showBtn.gif"><br></a></td></tr>
                              <tr><td colspan="3"><img width="2" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="resetFilter();" class="divbutton" src="images/reset.gif"><br></a></td></tr>                              
                              <tr><td colspan="3"><img height="8" src="images/spacer.gif"></td></tr>
                              </tbody></table> --%>
                              
                              
                              
                              </td>
                        </tr>
  			</tbody></table>											
		</div>
	</span>
				</td>
	
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      </td>
 </tr>
 </tbody>
 </table>



<!-- Table for Menu and View Selector: End -->

<!-- Inclue file for Menu Script -->
<%@ include  file="/NetMedXDashboardMenuScript.inc" %> <!-- Include file for appendix menu script -->
<%} //- For NetMedX: End%>
<!-- For Assign Team: End -->
<script type="text/javascript">

function checkCustomSelection(){
	var radios = document.getElementsByName('jobMonthWeekCheck');

	for (var i = 0, length = radios.length; i < length; i++) {
	    if (radios[i].checked) {
	        if(radios[i].value=='custom'){
	        	dateRangeShow();
	        }else{
	        	dateRangeHide();
	        }
	        break;
	    }
	}
    
}

function dateRangeHide(){
	document.getElementById('drange').style.display = "none";
}
function dateRangeShow(){
	document.getElementById('drange').style.display = "block";
}






</script>
<script>
function toggleMenuSection(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){	}
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
	}
	else {
		thisImage.src = "images/Expand.gif";
	}
}
function toggleDiv(divName) {
	 var tcookie = document.cookie;
     var st = tcookie.indexOf ("N1Db");
     var cookievalue ="";
	  if(st!= -1) {
	      st = st+3;
	      st = tcookie.indexOf ("=", st);
	      en = tcookie.indexOf (";", st);
		  if(en > st) {
	          cookievalue = tcookie.substring(st+1, en); //div's that is reopen
	      }  }
	var id = document.getElementById(divName);
	if( id.style.display == "none" ) {
			id.style.display = "block";
			//add it to the existing cookie opendiv paraneter
			if (cookievalue.length>0)
				cookievalue = cookievalue+','+divName;
			else
				cookievalue = divName;
			if(cookievalue!="") { 
				// Save as cookie
				document.cookie = "NDb="+ cookievalue+" ; ";
            } }
	else {
			id.style.display = "none";	
			var newcookievalue='';
			//remove this div from the opendiv cookie
			if (cookievalue.length>0) {
				var array = cookievalue.split(',');
				if (array.length>1) {
					for(i=0;i<array.length;i++) {
						if(array[i]==divName) { }
						else {
							if(newcookievalue.length > 0 )
								newcookievalue = newcookievalue+','+array[i];
							else
								newcookievalue = array[i];	
					} } }
				if(newcookievalue!="")
				{ // Save as cookie

			 		document.cookie = "NDb="+ newcookievalue+" ; ";
			 	}
			}
		}
	if( divName == 'div_1')
	{
		id = document.getElementById('div_13');
		if( id.style.display == "none" ) 
			id.style.display = "block";
		else
			id.style.display = "none";	
		id = document.getElementById('div_11');
		if( id.style.display == "none" ) 
			id.style.display = "block";
		else
			id.style.display = "none";	
	}
}
function toggleMenuSectionView(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){ }
	action = "toggleType = toggleDivView('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
	}
	else {
		thisImage.src = "images/Expand.gif";
	}
}
function toggleDivView(divName) {
	 var tableRow = document.getElementsByTagName('tr');
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if (tableRow[k].getAttributeNode('id').value==divName) {
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	
	}
}
// Added Start:By Amit This will be called on each href that opens other form
function createCookie() {
 var tableRow = document.getElementsByTagName('tr');
  var openDiv="";
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if((tableRow[k].getAttributeNode('id').value != "")&&(tableRow[k].getAttributeNode('id').value != "accountNM"))
		{
			if(tableRow[k].style.display == "block" ){
			openDiv=openDiv+tableRow[k].getAttributeNode('id').value+",";
			}
		}
	}
		if(openDiv!="") { // Save as cookie
		 document.cookie = "NDb="+ openDiv+" ; ";
		 }
}
function createCookie(lastDivForFocus) {
 		document.cookie = "N1Db="+ lastDivForFocus+" ; ";
}
//following function would be called on OnLoad
function CookieGetOpenDiv(){
 	var tcookie = document.cookie;
  	var unique;
  	var st = tcookie.indexOf ("NDb");
  	var cookievalue ="";
  	if(st!= -1){
		st = st+3;
      	st = tcookie.indexOf ("=", st);
      	en = tcookie.indexOf (";", st);
	  	if (en > st){cookievalue = tcookie.substring(st+1, en);}
		if(cookievalue.length>0){
			var opendiv = cookievalue.split(',');
			if(opendiv.length>0){
		  		for (var j = 0; j < opendiv.length; j++){
		   			if(eval("document.getElementById('"+opendiv[j]+"')")!=null)	//Added This if condition by amit
		   			{
		  				eval("document.getElementById('"+opendiv[j]+"')").style.display = "block";
						unique = opendiv[j].substring(opendiv[j].indexOf("_")+1,opendiv[j].length);
						action = "thisImage = document.getElementById('img_" + unique +"');";
						eval(action);
					if (document.getElementById('div_' + unique).offsetHeight > 0) { thisImage.src = "images/Collapse.gif"; }
					else {thisImage.src = "images/Expand.gif"; }
					}
					if(opendiv[j]=='div_1')
					{
							if(document.forms[0].viewjobtype.value != 'jobSearch') {
								document.getElementById('div_11').style.display = "block";
								document.getElementById('div_13').style.display = "block";
							}
					}												
		  }		
			}
		}
 }
	openDiv = "";	  	 
	document.cookie = "N1Db="+ openDiv+" ; ";    //Save as cookie	 	
	var st1 = tcookie.indexOf ("N1Db");	 //Start:Added For Focus	
	var cookievalue1 ="";
	if(st1!= -1){
	    st1 = st1+4;
	    st1 = tcookie.indexOf ("=", st1);
	    en = tcookie.indexOf (";", st1);
		if (en > st1){ cookievalue1 = tcookie.substring(st1+1, en);}
	}
	if(cookievalue1!=""){
		if(document.getElementById(cookievalue1)!=null){document.getElementById(cookievalue1).focus();}
	}
	document.cookie = "N1Db="+ openDiv+" ; ";
}
function checkCustomSelection(){
	var radios = document.getElementsByName('jobMonthWeekCheck');

	for (var i = 0, length = radios.length; i < length; i++) {
	    if (radios[i].checked) {
	        if(radios[i].value=='custom'){
	        	dateRangeShow();
	        }else{
	        	dateRangeHide();
	        }
	        break;
	    }
	}
    
}
function dateRangeHide(){
	document.getElementById('drange').style.display = "none";
}
function dateRangeShow(){
	document.getElementById('drange').style.display = "block";
}
</script>
<script>
function compDate_mdy_hif(from, to){
    tmp1 = new Date();
    tmp2 = new Date();

    p1 = from.indexOf("-");
    mstr = from.substring(0, p1);
    p2 = from.indexOf("-", p1+1);
    dstr = from.substring(p1+1, p2);
    ystr = from.substring(p2+1);
    tmp1.setYear(parseInt(ystr, 10));
    tmp1.setMonth(parseInt(mstr, 10) - 1);
    tmp1.setDate(parseInt(dstr, 10));

    p1 = to.indexOf("-");
    mstr = to.substring(0, p1);
    p2 = to.indexOf("-", p1+1);
    dstr = to.substring(p1+1, p2);
    ystr = to.substring(p2+1);
    tmp2.setYear(parseInt(ystr, 10));
    tmp2.setMonth(parseInt(mstr, 10) - 1);
    tmp2.setDate(parseInt(dstr, 10));

    if (tmp1 <= tmp2) {
        return true;
    } else {
        return false;
    }
}

function changeFilter()
{
	if (!compDate_mdy_hif(document.forms[0].fromDate.value, document.forms[0].toDate.value)) {
		alert("To Date should be greater than From Date");
		return false;
	} 
	
	document.forms[0].action="NetMedXDashboardAction.do?function=view&viewjobtype=ION&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="NetMedXDashboardAction.do?function=view&viewjobtype=ION&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
var accountval=0;
var accountnum=14;
function showaccountmore() {
	if(!document.all.account) {
		return; //if the account array is not present
	}
	if(accountval==0) {					//if accountval is zero then back tag is disabled otherwise enabled
		document.all.accountback.disabled=true;
	}
	else {
		document.all.accountback.disabled=false;
	}
	account_HideShowBlock();  // hide all the account elements
	for(var i=0;i<accountnum;i++){  // only fourteen elements are shown 
		if(document.all.account[accountval]) {
	   		document.all.account[accountval].style.display="block";
	   		document.all.accountcm[accountval].style.display="block";
	   		document.all.accountavg[accountval].style.display="block";
	   		document.all.accountttm[accountval].style.display="block";
	   		accountval++;
	   	}	
		else if(!document.all.account.length){
		   		document.all.account.style.display="block";
		   		document.all.accountcm.style.display="block";
		   		document.all.accountavg.style.display="block";
		   		document.all.accountttm.style.display="block";
   				document.all.accountmore.disabled=true;
   				return;		   		
	   		}	   	
	}
	if(accountval==document.all.account.length){	// if accountval is equal to account array length then the more is disabled otherwise enabled.
		document.all.accountmore.disabled=true;
	} else{
		document.all.accountmore.disabled=false;
	}	
}
function showaccountback() {
	if(!accountval==document.all.account.length){      // if accountval is equal to account array length then more is disabled otherwise enabled.
		document.all.accountmore.disabled=true;
	} else{
		document.all.accountmore.disabled=false;
	}		
	if((accountval%accountnum)!=0)				          // if accountval%accountnum is not zero
		accountval=accountval-(accountval%accountnum);	
	accountval= accountval-accountnum;						// take the accountval to back accountnum
	account_HideShowBlock();
	for(var i=0;i<accountnum;i++){						// show the last accountval
   		document.all.account[accountval].style.display="block";
   		document.all.accountcm[accountval].style.display="block";
   		document.all.accountavg[accountval].style.display="block";
   		document.all.accountttm[accountval].style.display="block";
   		accountval++;
 	}
	accountval= accountval-accountnum;						// set the accountval to back accountnum
	if(accountval==0){							// if accountval=0 then back is disabled and show the first div.
		showaccountmore();	
		document.all.accountback.disabled=true;
	} else{
		document.all.accountback.disabled=false;
	}
}
function account_HideShowBlock() {		// hide all the divs.
        var len=document.all.account.length;
        for(var id=0;id<len;id++) { 
    		document.all.account[id].style.display="none";
    		document.all.accountcm[id].style.display="none";
    		document.all.accountavg[id].style.display="none";
    		document.all.accountttm[id].style.display="none";
    	}	
}


$(document).ready(function () {
    $("#drange").hide();   
    $("#custom").click(function () {
        $("#drange").show();
        
    });
});


</script>
</body>
</html>



