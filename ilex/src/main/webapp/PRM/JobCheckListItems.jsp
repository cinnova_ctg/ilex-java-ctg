<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<script language = "JavaScript" src = "docm/javascript/ViewManipulation.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />

<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>

<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
	<script>
		function onModify(){
			document.forms[0].action="JobCheckListAction.do";
			document.forms[0].submit();
		}
		function setStyleForCells(counter,cellObject)
			{
				if(counter % 2 == 0){
						cellObject.className = 'Ntextoleftalignnowrap';
					}
					else{
						cellObject.className = 'Ntexteleftalignnowrap';
					}
			}
		function addFilterCriteriaRow(){
			var object= document.all.filterTable;
			var i = object.rows.length ;
			var oRow;
				oRow=object.insertRow();
				oCell=oRow.insertCell(0);
				oCell.innerHTML='<input type="text" name="sequence" style="width: 40px;" class="text" >';
				oCell.align='center';
				setStyleForCells(i-1,oCell);
				oCell=oRow.insertCell(1);
				oCell.innerHTML='<input type="text" name="desc" style="width: 550px;" class="text" ><input type="hidden" name="filterId" value="" />'
				oCell.align='center';
				setStyleForCells(i-1,oCell);
				oCell=oRow.insertCell(2);
				oCell.innerHTML='<img  align="center" src="docm/images/delete.gif" name="filterImage" onclick="Rowdelete(\'filterTable\',this)"/>';
				oCell.align = 'center';
				setStyleForCells(i-1,oCell);
		}
		function Rowdelete(tableId,imageId)
			{
				var tableObj = document.getElementById(tableId);
				if(tableId=='filterTable'){
					var length = document.all.filterImage.length;
					if(!length)
					{
						tableObj.deleteRow(1);
					}
					for(var i=0;i<length;i++)
					{
						if(document.all.filterImage[i]==imageId)
						{	
								var index = (i+1);
								tableObj.deleteRow(index);
						}	
					}
				}
			}

	</script>
</head>
	<body>
		<form action="JobCheckListAction">
			<table border="0" cellpadding="0" cellspacing="1">
				<tr>
					<td>
					<!--  Menu change for netmedx  -->	
					
					
					
					<div id="menunav">
<%if(!nettype.equals("dispatch")){ %>  
<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=3225&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="NetMedXDashboardForm" property="appendixid"/>&ownerId=<bean:write name="NetMedXDashboardForm" property="ownerId"/>">Search</a>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="NetMedXDashboardForm" property="appendixid"/>&ownerId=<bean:write name="NetMedXDashboardForm" property="ownerId"/>&apiAccess=apiAccess">API Access</a>
  		</li>
	</ul>
</li>
<%}else{%>
<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="NetMedXDashboardForm" property="appendixid"/>&ownerId=<bean:write name="NetMedXDashboardForm" property="ownerId"/>">Search</a>
  		</li>
  		</li>
	<%} %>
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			          		<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
											 	<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixid }'/>"><c:out value='${requestScope.appendixName }'/></a>
												<a><span class="breadCrumb1">Job CheckList Items</a></div>
			        </td>
		        </tr>
		         
	      </tbody></table>
						
					<!--  End of  -->
			
						<%-- <table cellpadding="0" cellspacing="0" border="0" width = "100%">
						        <tr> 
						          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage</center></a></td>
						          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
						     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%=appendixid%>&ownerId=<%=ownerid%>&msaId=<%=msaid%>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
						          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
						        </tr>
						        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
							          		<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
											 	<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixid }'/>"><c:out value='${requestScope.appendixName }'/></a>
												<a><span class="breadCrumb1">Job CheckList Items</a></div>
							        </td>
						       </tr>
					      </table> --%>
		      				
				<TR>
					<td><h2 style="font-size: 14px;padding: 12px 0px 0px 0px;">Job CheckList Items</h2></td>
				</tr>
				<tr>
					<td>
						<TABLE  cellspacing="0" cellpadding="1">
							<TR id="tab" >
								<TD width="680">
									<TABLE id="filterTable" width="100%" cellspacing="1" cellpadding="1" >						
										<TR>
											<TD bgcolor="#EEEEEE" class = "Ntryb" width="10%"  nowrap> Sequence Number</TD>
											<TD bgcolor="#EEEEEE" class = "Ntryb" width="80%"  nowrap> Description</TD>
											<TD bgcolor="#EEEEEE" class = "Ntryb" width="10%" nowrap align="center"> Delete</TD>
										</TR>
										<TR>
											<TD valign=top align="center" width="10%" class='Ntextoleftalignnowrap'> 
												<input type="text" style="width: 40px;" class="text" name="sequence"/>
											</TD>
											<td align="center" width="80%"  class='Ntextoleftalignnowrap'>
												<input type="text"  style="width: 550px;"  class="text"  name="desc"/>	
											</TD>
											<TD valign=top  align="center" width="10%"  class='Ntextoleftalignnowrap'><img  align="center" src="docm/images/delete.gif" name="filterImage" onclick="Rowdelete('filterTable',this)"/></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
				    </td>
				</tr>
				<tr>
					<td>
						<TABLE  cellspacing="0" cellpadding="0" width='100%'>
							<tr>
								<td>
									<table>
										<tr>
											<td align='left'>
												<input type="button" name="addMore" value="Add Row &darr;" onclick="addFilterCriteriaRow();" class="button_c">
											</td>
											<td align='left'>
												<input type="button" name="update" value="Update" onclick="onModify();" class="button_c">
											</td>
											<td align='left'>
												<input type="button" name="reset" value="Cancel" onclick="onModify();" class="button_c">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</TABLE>
					</td>
				</tr>
			</table>
			<%@ include  file="/AppedixDashboardMenuScript.inc" %>
		</form>
	</body>
</html>