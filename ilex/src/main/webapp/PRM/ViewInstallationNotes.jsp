<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%String appendixname = "";
String accountingDB ="";
			String pagetitle = "";
			String label = "";
			if (request.getAttribute("appendixname") != null) {
				appendixname = "" + request.getAttribute("appendixname");
			}

			if (appendixname.equalsIgnoreCase("NetMedX")) {
				pagetitle = "prm.installnotes.dispatchncomments";
				label = "prm.dispatchnotes.dispatchnotes";
			} else {
				pagetitle = "prm.installnotes.installationncomments";
				label = "prm.installnotes.installnotes";
			}
			int size = 0;
			if (request.getAttribute("listsize") != null) {
				size = Integer.parseInt(request.getAttribute("listsize")
						.toString());
			}
			if(request.getAttribute("fromAccountingDB")!=null)
			{
				accountingDB = request.getAttribute("fromAccountingDB").toString();
			}
						
			%>
<html:html>
<head>
<title></title>
<link rel="stylesheet" href="styles/style.css" type="text/css"><script
	language="JavaScript" src="javascript/ilexGUI.js"></script>
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<title><bean:message bundle="PRM" key="prm.uplift.manageupliftfactors"/></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
</head>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">


<table cellspacing="0" cellpadding="0">
	<html:form action="/ViewInstallationNotes" target="ilexmain">
		<html:hidden property="function" />
		<html:hidden property="appendixid" />
		<html:hidden property="jobid" />
		<html:hidden property="from" />
		<html:hidden property="viewjobtype" />
		<html:hidden property="ownerId" />
		<html:hidden property="nettype" />
		<c:if test="${ViewInstallationNotesForm.from eq 'jobdashboard'}">
			<tr>
				<td colspan="2">
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
			          	<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
						<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
						<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
						</tr>
						<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
						         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
									 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
									  <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
				  						<a><span class="breadCrumb1">Installation Notes</a>
						    </td>
						</tr>
				        <tr><td height="5" >&nbsp;</td></tr>	
					</table>
				</td>
			</tr>
		</c:if>
		<table width="100%" border="0" cellspacing="1" cellpadding="1"
			align="center" valign="top">
			<tr>
				<td width="2" height="0"></td>
				<td>
				<table border="0" cellspacing="1" cellpadding="1" width="660">

					<tr>
						<td colspan="3" class="labeleboldwhite" height="30"><bean:message
							bundle="PRM" key="<%=pagetitle%>" /><bean:write
							name="ViewInstallationNotesForm" property="appendixname" /> <bean:message
							bundle="PRM" key="prm.installnotes.job" /> <bean:write
							name="ViewInstallationNotesForm" property="jobname" /></td>
					</tr>

					<logic:present name="noteslist" scope="request">
						<%if (size > 0) {

				%>
						<tr>
							<td class="tryb" width="140"><bean:message bundle="PRM"
								key="prm.installnotes.date" /></td>
							<td class="tryb" width="400"><bean:message bundle="PRM"
								key="<%=label%>" /></td>
							<td class="tryb" width="120"><bean:message bundle="PRM"
								key="prm.installnotes.user" /></td>
						</tr>

						<%int i = 0;
				String bgcolor = "";
				String bgcolor1 = "";%>
						<logic:iterate id="nlist" name="noteslist" scope="request">

							<%if ((i % 2) == 0) {
					bgcolor = "labeletop";
					bgcolor1 = "labeletopnowrap";
				} else {
					bgcolor = "labelotop";
					bgcolor1 = "labelotopnowrap";

				}%>
							<tr>								
								
								<td class="<%= bgcolor1 %>">
									<logic:notEqual name="nlist" value="0" property="typeId">
										 <b>
									</logic:notEqual>
										<bean:write name="nlist" property="createdatetime" />
									<logic:notEqual name="nlist" value="0" property="typeId">
									 	</b>
									</logic:notEqual>
								</td><!-- word-break:break-all;word-wrap:break-word; -->
								<td class="<%= bgcolor %>" style="">
								<bean:write name="nlist"
									property="installnotes" filter="false" /></td>
								<td class="<%= bgcolor1 %>"><bean:write name="nlist"
									property="createdby" /></td>
							</tr>
							<%i++;

			%>

						</logic:iterate>
						<%if(accountingDB==null){ %>
						<tr>
							<td colspan="3" class="buttonrow"><html:button property="back"
								styleClass="button" onclick="return Backaction();">
								<bean:message bundle="PRM" key="prm.button.back" />
							</html:button></td>
						</tr>
						<%}
						else{%>
						<tr><td>
						<html:button property = "back" styleClass = "button" onclick = "return BackHistory();">Back</html:button>
						</td></tr>
					<%	} } else {if(accountingDB!=null) {%>
					<tr><td>
					<html:button property = "back" styleClass = "button" onclick = "return BackHistory();">Back</html:button>
					</td></tr>
					<%}else{ %>
						<tr>
							<td colspan="3" class="message" height="30"><bean:message
								bundle="PRM" key="prm.installnotes.nocomments" /></td>
						</tr>
						<tr>
							<td colspan="3"><html:button property="back" styleClass="button"
								onclick="return Backaction();">
								<bean:message bundle="PRM" key="prm.button.back" />
							</html:button></td>
						</tr>
						<%}}

			%>
					</logic:present>
					<tr>
						<td><img src="images/spacer.gif" width="1" height="5"></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
			<c:if test = "${requestScope.jobType eq 'Addendum'}">
				<%@ include  file="/AddendumMenuScript.inc" %>
			</c:if>
			<c:if test = "${requestScope.jobType eq 'Default'}">
				<%@ include  file="/DefaultJobMenuScript.inc" %>
			</c:if>
			<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
			<%@ include  file="/ProjectJobMenuScript.inc" %>
			</c:if>
	</html:form>
</table>

	
</body>
<script>
function Backaction()
{
	
	if( document.forms[0].from.value=="appendixdashboard" )
	{
		
		if(document.forms[0].nettype.value == "dispatch")
			document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
		else	
			document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getAttribute("appendixid") %>&function=view&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	}
	else
	{
		document.forms[0].action = "JobDashboardAction.do?jobid=<%=request.getAttribute( "jobid" ) %>";
	}
	
	document.forms[0].submit();
	
	return true;
}
function BackHistory()
{
	history.go(-1);
}


</script>
</html:html>
