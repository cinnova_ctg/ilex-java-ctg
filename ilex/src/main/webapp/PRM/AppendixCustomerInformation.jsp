<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>


<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

</head>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());

%>
<%@ include  file="/NMenu.inc" %>
<%@ include  file="/AjaxOwners.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');leftAdjLayers();" >
<html:form action="AppendixCustomerInformation" >
<html:hidden property="function" />
<html:hidden property="appendixid" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property="jobOwnerOtherCheck"/> 

<logic:notEqual name = "AppendixCustomerInformationForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AppendixDashboardMenuScript.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
	  				<td colspan ="4" ><h2><bean:message bundle="PRM" key="prm.custinfo.customerinfofor"/>&nbsp;<bean:message bundle="PRM" key="prm.custinfo.appendix"/><bean:write name="AppendixCustomerInformationForm" property="appendixname" /></h2></td>
	  			</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="AppendixCustomerInformationForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="AppendixCustomerInformationForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="AppendixCustomerInformationForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					           <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
</logic:notEqual>

<logic:equal name = "AppendixCustomerInformationForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<!--  New Menu for Customer Appendix Information  -->
<div id="menunav">

<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>">Search</a>
  		</li>
  		
	</ul>
</li>

</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		   <td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.custinfo.customerinfofor"/>&nbsp;NetMedX:&nbsp;<bean:write name="AppendixCustomerInformationForm" property="appendixname"/></h2></td>      
	      </tbody></table>
   </td>
      
    <td width="35" valign="top"><img width="35" height="60" alt="" src="images/content_head_02.jpg" id="content_head_02"></td>
	<td width="155" valign="top">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr>
		          <td width="155" height="39" colspan="2" class="headerrow">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tbody><tr>
			                <td width="150">
			                	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				                   	<tbody><tr>
							            <td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('II');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" title="Scheduled" id="Image10" alt="Month" src="http://localhost:8080/Ilex/images/Icon_Scheduled.gif"></a></td>
	  									<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IO');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" title="Scheduled OverDue" id="Image10" alt="Month" src="images/Icon_Schedule_Overdued.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('ION');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" title="OnSite" id="Image10" alt="Month" src="images/Icon_Onsite.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IOF');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" title="Offsite" id="Image10" alt="Month" src="images/Icon_Ofsite.gif"></a></td>
				                    </tr>
			                  	</tbody>
			                  	</table>
			                </td>
			              </tr>
			          </tbody></table>
		          </td>
		        </tr>
        		<tr>
          		<td width="140" height="21" background="images/content_head_04.jpg">
          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              		<tbody><tr>
              		<td width="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li id="closeNow" name="closeNow"><a onclick="ShowDiv();" title="Open View" href="javascript:void(0)">My Views <img border="0" onclick="ShowDiv();" title="Open View" src="images/showFilter.gif"></a><a class="imgstyle" href="javascript:void(0)"><img border="0" onclick="hideDiv();" title="Close View" src="images/offFilter2.gif"></a></li>
									</div>								
	                			</div>		
						<span>
								<div class="divstyle" id="filter" style="">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tbody><tr>
		                      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		                          <tbody><tr>
                           	 <td class="filtersCaption" colspan="3">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										
									    	 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('AI');" value="AI" name="jobSelectedStatus">   
											 	Inwork<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IS');" value="IS" name="jobSelectedStatus">   
											 	To Be Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('II');" checked="checked" value="II" name="jobSelectedStatus">   
											 	Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IO');" checked="checked" value="IO" name="jobSelectedStatus">   
											 	Scheduled Overdue<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('ION');" checked="checked" value="ION" name="jobSelectedStatus">   
											 	Onsite<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IOF');" checked="checked" value="IOF" name="jobSelectedStatus">   
											 	Offsite<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('F');" value="F" name="jobSelectedStatus">   
											 	Complete<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('O');" value="O" name="jobSelectedStatus">   
											 	Closed<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('H');" value="H" name="jobSelectedStatus">   
											 	Hold<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('C');" value="C" name="jobSelectedStatus">   
											 	Cancelled<br> 
											 	
											
												
										<img width="100px" class="imagefilter" src="images/hrfilterLine.gif">
										<table cellpadding="0">
											<tbody><tr><td class="tabNormalText"><input type="radio" value="week" name="jobMonthWeekCheck">This Week</td></tr>
											<tr><td class="tabNormalText"><input type="radio" value="month" name="jobMonthWeekCheck">This Month</td></tr>	
											<tr><td class="tabNormalText"><input type="radio" checked="checked" value="clear" name="jobMonthWeekCheck">Clear</td></tr>	
										</tbody></table>	
									</td>
	    					</tr>
                        </tbody></table></td>
                      <td><img width="1px" height="120" class="imagefilter" src="images/filterLine.gif"></td>
                      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td class="filtersCaption" colspan="3">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id="OwnerSpanId"> 
					   			
							    	
							    		
							    		
								    		
							    			 
							    		<input type="checkbox" onclick="javascript: checkingOwner('7');" checked="checked" value="7" name="jobSelectedOwners">   
										  Me
										  <br> 
									
									</span>
								
								
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img height="34" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="changeFilter();" class="divbutton" src="images/showBtn.gif"><br></a></td></tr>
                              <tr><td colspan="3"><img width="2" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="resetFilter();" class="divbutton" src="images/reset.gif"><br></a></td></tr>                              
                              <tr><td colspan="3"><img height="8" src="images/spacer.gif"></td></tr>
                              </tbody></table></td>
                        </tr>
  			</tbody></table>											
		</div>
	</span>
				</td>
	
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      </td>
 </tr>
 </tbody>
 </table>



<!-- End of menu  -->
<%-- <table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
		<!--  			<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>-->
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr>
		        	<td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.custinfo.customerinfofor"/>&nbsp;NetMedX:&nbsp;<bean:write name="AppendixCustomerInformationForm" property="appendixname"/></h2></td>
		        </tr> 
	      </table>
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        <tr>
	          <td width="155" height="39"  class="headerrow" colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" border="0">
		              <tr>
		                <td width="150">
		                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
			                   	<tr>
						            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
			                    </tr>
		                  	</table>
		                </td>
		              </tr>
		          </table>
	          </td>
	        </tr>
	        <tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>	
	                			<span>
									<div id="filter" class="divstyle">
					        			<table width="250" cellpadding="0" class="divtable">
						                     <tr>
					                            <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
															 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																		<bean:write name ="list" property = "statusid"/>
															 		</html:multibox>   
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="AppendixCustomerInformationForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="AppendixCustomerInformationForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="AppendixCustomerInformationForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table></td>
		                      			    	<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
			                      			    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												          <span id = "OwnerSpanId">
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
								
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
															 			</html:multibox>   
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>											
						            </div>
				                </span>
							 </td>
	              		</tr>
            		</table>
            	</td>
        	</tr>
	    </table>
	</td>
 </tr>
</table> --%>
<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
</logic:equal>
<table>
<%if(request.getAttribute("viewflag").equals("0")) 
{ %>

<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="750">
	  		
  			<logic:present name="addmessage">
			<logic:equal name="addmessage" value="-9001">
			<tr>
				<td  colspan="3" class="message" height="30" nowrap;><bean:message bundle="PRM" key="prm.custinfo.errorindeleting"/></td>
			</tr>
	 		</logic:equal>
	 		</logic:present>
	  		 
	  		<tr> 
			    <td class="labelebold" width="20"><bean:message bundle="PRM" key="prm.custinfo.1"/></td>
			    <td class="labele" width="300"><html:text  styleClass="textbox" size="30" property="custParameter1" /></td>
			    <td width="430"></td> 
			    <html:hidden property="custParameterId1" />
			</tr>
		    
		     <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.2"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter2" /></td>
	   			 <td></td>
	   			 <html:hidden property="custParameterId2" /> 
	 		 </tr>
 
	  		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.3"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter3" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId3" />
	 		 </tr>
	<!-- start -->
	 		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.4"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter4" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId4" />
	 		 </tr>
	 		 
	 		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.5"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter5" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId5" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.6"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter6" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId6" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.7"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter7" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId7" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.8"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter8" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId8" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.9"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter9" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId9" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.10"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter10" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId10" />
	 		 </tr>
	 		 
	 		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.11"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter11" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId11" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.12"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter12" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId12" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.13"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter13" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId13" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.14"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter14" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId14" />
	 		 </tr>
	 		 <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.15"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter15" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId15" />
	 		 </tr>
	 		 
	 		  <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.16"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter16" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId16" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.17"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter17" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId17" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.18"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter18" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId18" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.19"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter19" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId19" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.20"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter20" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId20" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.21"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter21" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId21" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.22"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter22" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId22" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.23"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter23" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId23" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.24"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter24" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId24" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.25"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter25" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId25" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.26"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter26" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId26" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.27"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter27" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId27" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.28"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter28" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId28" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelebold"><bean:message bundle="PRM" key="prm.custinfo.29"/></td>
	   			 <td  class="labele"><html:text  styleClass="textbox" size="30" property="custParameter29" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId29" />
	 		 </tr>
	 		  <tr> 
	    		 <td  class="labelobold"><bean:message bundle="PRM" key="prm.custinfo.30"/></td>
	   			 <td  class="labelo"><html:text  styleClass="textbox" size="30" property="custParameter30" /></td>
	   			 <td></td> 
	   			 <html:hidden property="custParameterId30" />
	 		 </tr>
	 		 
	 	<!-- over -->	 
	 		
	 		 <tr> 
	 		 	  <td colspan="2" class="buttonrow"> 
	 		      <html:submit property="save" styleClass="button" >
	 		      <bean:message bundle="PRM" key="prm.save" /></html:submit>
     			  <html:reset  property="reset" styleClass="button"><bean:message bundle="PRM" key="prm.reset" /></html:reset>
				  <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  </td>
				  <td></td> 
			 </tr>
	  
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
<%}
else
{%>
<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="750">
	  		<logic:present name="addmessage">
			<logic:equal name="addmessage" value="0">
			<tr>
				<td  colspan="3" class="message" height="30" ><bean:message bundle="PRM" key="prm.custinfo.addedsuccessfully"/></td>
			</tr>
	 		</logic:equal>
	 		</logic:present>
	  		
	  		<tr>
	  			<td colspan="3" class="labeleboldwhite" height="30"><bean:message bundle="PRM" key="prm.custinfo.customerinfofor"/>&nbsp;<bean:message bundle="PRM" key="prm.custinfo.appendix"/><bean:write name="AppendixCustomerInformationForm" property="appendixname" /></td>
	  		</tr>
	  		 
	  		<logic:notEqual name="AppendixCustomerInformationForm" property="custParameter1" value="">
	  		<tr>
	   			 <td class="labelebold" height="20" width="10"><bean:message bundle="PRM" key="prm.custinfo.1"/></td>
	   			 <td class="labele" height="20" width="240"><bean:write name="AppendixCustomerInformationForm" property="custParameter1" /></td>
	   			 <td width="500"></td> 
	  		</tr>
	  		</logic:notEqual>
	 
	    	<logic:notEqual name="AppendixCustomerInformationForm" property="custParameter2" value="">
	    	<tr> 
			    <td class="labelobold" height="20"><bean:message bundle="PRM" key="prm.custinfo.2"/></td>
			    <td class="labelo" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter2" /></td>
			   	<td></td>
		    </tr>
		    </logic:notEqual>
		    
		    <logic:notEqual name="AppendixCustomerInformationForm" property="custParameter3" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.3"/></td>
			    <td class="labele" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter3" /></td>
				<td></td>
		    </tr>
		    </logic:notEqual>
	
	<!-- start -->
	 		 <logic:notEqual name="AppendixCustomerInformationForm" property="custParameter4" value="">
		    <tr> 
			    <td class="labelobold" height="20"><bean:message bundle="PRM" key="prm.custinfo.4"/></td>
			    <td class="labelo" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter4" /></td>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="AppendixCustomerInformationForm" property="custParameter5" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.5"/></td>
			    <td class="labele" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter5" /></td>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="AppendixCustomerInformationForm" property="custParameter6" value="">
		    <tr> 
			    <td class="labelobold" height="20"><bean:message bundle="PRM" key="prm.custinfo.6"/></td>
			    <td class="labelo" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter6" /></td>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="AppendixCustomerInformationForm" property="custParameter7" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.7"/></td>
			    <td class="labele" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter7" /></td>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="AppendixCustomerInformationForm" property="custParameter8" value="">
		    <tr> 
			    <td class="labelobold" height="20"><bean:message bundle="PRM" key="prm.custinfo.8"/></td>
			    <td class="labelo" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter8" /></td>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="AppendixCustomerInformationForm" property="custParameter9" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.9"/></td>
			    <td class="labele" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter9" /></td>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    <logic:notEqual name="AppendixCustomerInformationForm" property="custParameter10" value="">
		    <tr> 
			    <td class="labelebold" height="20"><bean:message bundle="PRM" key="prm.custinfo.10"/></td>
			    <td class="labele" height="20"><bean:write name="AppendixCustomerInformationForm" property="custParameter10" /></td>
				<td></td>
		    </tr>
		    </logic:notEqual>
		    
	 	<!-- over -->	 
	
	
	
	  		  <tr> 
	 		 	  <td colspan="2" class="buttonrow"> 
	 		      <html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
				  </td>
				  <td></td>
			 </tr>
	  		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		</table>
 	  </td>
 	</tr> 
</table>
<%} %>
</html:form>
</table>
<body>

<script>


function Backaction()
{
	//alert("Hello from AppendixCustomerInformation.jsp");
	document.forms[0].action = "AppendixHeader.do?appendixid=<%=request.getAttribute("appendixid") %>&function=view&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="AppendixCustomerInformation.do?function=add&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="AppendixCustomerInformation.do?function=add&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
</html:html>