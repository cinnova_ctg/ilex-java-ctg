<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>


<%
	int history_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString()); 

%>
<html:html>
<head>
	<title></title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<tr>
    	<td class = "toprow"> 
	    	<table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
	        	<tr align = "center"> 
		    		
		    		
	        	</tr>
	      	</table>
    	</td>
	</tr>
</table>

<table>
<html:form action = "/TicketHistoryAction" target = "ilexmain">
<html:hidden property = "jobid" />
<html:hidden property = "nettype" />
<html:hidden property = "from" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />

<table width = "100%" border = "0" cellspacing="1" cellpadding = "1" align = "center" valign  = "top">
	<tr><td width="2" height="0"></td>
		<td>
		<table  border = "0" cellspacing = "1" cellpadding = "1" width = "660"> 
	  
		<tr> 
	    	<td colspan = "9" class = "labeleboldwhite" height = "30" >
	    		Ticket History For Job:<bean:write name = "TicketHistoryForm" property = "jobname" />
	    	
	    	</td>
	 	</tr>
	 	
		<logic:present name = "tickethistory"  scope = "request">
		<%if( history_size > 0 )
			{ %> 
			
			<tr>
				<td class = "tryb">Ticket Number</td>
				<td class = "tryb">Requestor Name</td>
				<td class = "tryb">Request Date</td>
				<td class = "tryb">Customer Reference</td>
				<td class = "tryb">Problem Description</td>
				<td class = "tryb">Criticality</td>
				<td class = "tryb">Status</td>
				<td class = "tryb">Completed Date</td>
				<td class = "tryb">Final Price</td>
			</tr>   
			
			<%int i=0; 
			  String bgcolor="";
			  String bgcolor1="";
			  String bgcolornumber="";
			%>
			<logic:iterate id = "tickethistory" name = "tickethistory" scope = "request">
				
			<%if((i%2)==0)
				{
					bgcolor="labeletop";
					bgcolor1="labeletopnowrap";
					bgcolornumber = "numberodd";
				
				} 
			 else
				{
					bgcolor="labelotop";
					bgcolor1="labelotopnowrap";
					bgcolornumber = "numbereven";
				}%>
			<tr>
				<td class = "<%= bgcolor1 %>"><bean:write name = "tickethistory" property = "ticketnum"/></td>	
				<td class = "<%= bgcolor1 %>"><bean:write name = "tickethistory" property = "ticketrequestorname"/></td>
				<td class = "<%= bgcolor1 %>"><bean:write name = "tickethistory" property = "ticketrequesteddate"/></td>
				<td class = "<%= bgcolor1 %>"><bean:write name = "tickethistory" property = "ticketcustreference"/></td>	
				<td class = "<%= bgcolor1 %>"><bean:write name = "tickethistory" property = "problemdesc"/></td>
				<td class = "<%= bgcolor1 %>"><bean:write name = "tickethistory" property = "criticality"/></td>
				<td class = "<%= bgcolor1 %>"><bean:write name = "tickethistory" property = "status"/></td>	
				<td class = "<%= bgcolor1 %>"><bean:write name = "tickethistory" property = "completed_date"/></td>	
				<td class = "<%= bgcolornumber %>"><bean:write name = "tickethistory" property = "final_price"/></td>	
	 		</tr>
	 		<%i++; %>
		 		
		 	</logic:iterate>
		 	<tr>
	 		  	<td colspan = "9" class = "buttonrow">
	 		  		<html:button property="back" styleClass="button" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
	 			</td>
	 		</tr>
	 		<%} 
 		 else 
	 		{%>
	 		<tr>
	 			<td colspan="9" class="message" height="30" >No history Available </td>
	 		</tr>
	 		<tr>
	 		  	<td colspan = "9" >
	 		  		<html:button property="back" styleClass="button" onclick = "javascript:history.go(-1);"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
	 			</td>
	 		</tr>
	 		<%} %>
	 	 </logic:present>
			<tr> 
		    	<td><img src = "images/spacer.gif" width = "1" height = "5"></td>
			</tr>
	 	 </table>
		</td>
	  </tr> 
	</table>
	</html:form>
</table>
   
</body>
<script>
function Backaction()
{
	if( document.forms[0].from.value == 'netmedxdashboard' )
	{
		if( document.forms[0].nettype.value == 'dispatch' )
			document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype=dispatch&viewjobtype=<bean:write name = "TicketHistoryForm" property = "viewjobtype" />&ownerId=<bean:write name = "TicketHistoryForm" property = "ownerId" />";
		else
			document.forms[0].action = "AppendixHeader.do?appendixid=<bean:write name="TicketHistoryForm" property="appendixid"/>&function=view&viewjobtype=<bean:write name = "TicketHistoryForm" property = "viewjobtype" />&ownerId=<bean:write name = "TicketHistoryForm" property = "ownerId" />";
	}
	else
	{
		document.forms[0].action = "JobDashboardAction.do?jobid=<bean:write name = "TicketHistoryForm" property = "jobid" />";
	}
	document.forms[0].submit();
	return true;
}

</script>
</html:html>