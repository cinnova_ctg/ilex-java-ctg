<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:define id = "refvar" name = "SOWForm" property = "ref" />

<% int status_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 
	
%> 
<html:html>
<head>
	<title><bean:message bundle = "PRM" key = "prm.managestatus.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>
<%
	String backgroundclass = "texto";
	int i = 0;
	boolean csschooser = true;
	String schdayscheck = null;	
	String readonlyclass = "readonlytextodd";
%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


%>



<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body onload = "leftAdjLayers();" >

<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>

<html:form action = "/ManageStatusAction">

<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "appindixName" />
<html:hidden property = "jobName" />
<html:hidden property="jobOwnerOtherCheck"/> 

<logic:equal name = "SOWForm" property = "type" value = "prm_appendix">
<%@ include  file="/AppendixDashboardMenuScript.inc" %>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
						<td colspan="4"><h2>
							<logic:equal name = "SOWForm" property = "ref" value = "update">
								<logic:equal name = "SOWForm" property = "type" value = "prm_appendix">
									<bean:message bundle = "PRM" key = "prm.managestatus.viewstatusreport"/>&nbsp;<bean:message bundle = "PRM" key = "prm.managestatus.forAppendix"/> :<bean:write name="SOWForm" property="appindixName" /> 
								</logic:equal>
							</logic:equal>
							<logic:equal name = "SOWForm" property = "ref" value = "view">
								<% if( status_size == 0 ){%> 
										<bean:message bundle = "PRM" key = "prm.managestatus.nostatus"/>
								<%}else { %>
									<logic:equal name = "SOWForm" property = "type" value = "prm_appendix">
										<bean:message bundle = "PRM" key = "prm.managestatus.viewstatusreport"/>&nbsp;
									</logic:equal>
								<%} %>
							</logic:equal>
						</h2></td>
				</tr>		
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="SOWForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="SOWForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="SOWForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id = "OwnerSpanId"> 
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
</logic:equal>
<logic:equal name = "SOWForm" property = "type" value = "prm_job">
	<c:if test = "${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/Menu_Addendum.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/Menu_DefaultJob.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/Menu_ProjectJob.inc" %>
	</c:if>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
		         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
								 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
								<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
								  <a><span class="breadCrumb1">Job Dashboard</a>
		    </td>
		</tr>
        <tr><td height="5" >&nbsp;</td></tr>	
       <%-- <bean:message bundle = "PRM" key = "prm.managestatus.viewstatusreport"/>&nbsp;<bean:message bundle = "PRM" key = "prm.managestatus.jobName"/> :<bean:write name="SOWForm" property="jobName" /> --%>
	</table>
</logic:equal>
<logic:equal name = "SOWForm" property = "ref" value = "update">

<%--<table>
		<tr>
			<td>&nbsp;</td>
			<td class = "labeleboldwhite" height = "30">
			<logic:equal name = "SOWForm" property = "type" value = "prm_job">
			         <bean:message bundle = "PRM" key = "prm.managestatus.viewstatusreport"/>&nbsp;<bean:message bundle = "PRM" key = "prm.managestatus.jobName"/> :<bean:write name="SOWForm" property="jobName" /> 
			</logic:equal>
			</td>
		</tr>
	</table>--%>
	
</logic:equal>
<logic:equal name = "SOWForm" property = "ref" value = "view">

<%--<table>
		<tr>
			<td>&nbsp;</td>
			<td class = "labeleboldwhite" height = "30">
			<logic:equal name = "SOWForm" property = "type" value = "prm_job">
				
			</logic:equal>
			</td>
		</tr>
	</table>--%>
</logic:equal>

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
	<tr>
	 	
	  	<td>
	  		<table border = "0" cellspacing = "1" cellpadding = "1">
				<logic:present name = "deleteflag" scope = "request">
    				<logic:equal name = "deleteflag" value = "0">
    					<tr> <td>&nbsp; </td>
    						<td  colspan = "5" class = "message" height = "30">
    							<bean:message bundle = "PRM" key = "prm.managestatus.delete.success"/>
    						</td>
    					</tr>
    					
    				</logic:equal>
    			
    				<logic:notEqual name = "deleteflag" value = "0">
    					<tr> <td>&nbsp; </td>
    						<td colspan = "5" class = "message" height = "30">
    							<bean:message bundle = "PRM" key = "prm.managestatus.delete.failure"/>
    						</td>
    					</tr>
    				</logic:notEqual>
    			</logic:present>
    			
    			<logic:present name = "addflag" scope = "request">
	    			<logic:equal name = "addflag" value = "0">
	    				<tr> <td>&nbsp; </td>
			    			<td  colspan = "5" class = "message" height = "30">
			    				<bean:message bundle = "PRM" key = "prm.managestatus.add.success"/>
			    			</td>
	    				</tr>
	    			</logic:equal>
	    			
	    			<logic:notEqual name = "addflag" value = "0">
    					<tr> <td>&nbsp; </td>
    						<td colspan = "5" class = "message" height = "30">
    							<bean:message bundle = "PRM" key = "prm.managestatus.add.failure"/>
    						</td>
    					</tr>
    				</logic:notEqual>
    			</logic:present>
    				
    			<logic:present name = "updateflag" scope = "request">
			    	<logic:equal name = "updateflag" value = "0">
						<tr> <td>&nbsp; </td>
							<td  colspan = "5" class = "message" height = "30">
								<bean:message bundle = "PRM" key = "prm.managestatus.update.success"/>
							</td>
						</tr>
		    		</logic:equal>
		    		
		    		<logic:notEqual name = "updateflag" value = "0">
						<tr> <td>&nbsp; </td>
							<td  colspan = "5" class = "message" height = "30">
								<bean:message bundle = "PRM" key = "prm.managestatus.update.failure"/>
							</td>
						</tr>
		    		</logic:notEqual>
    			</logic:present>
		
  
<logic:present name = "managestatuslist" scope = "request" > 
	<logic:iterate id = "ms" name = "managestatuslist">
		
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass = "labelo";
				csschooser = false;
				readonlyclass = "readonlytextodd";
			}
	
			else
			{
				csschooser = true;	
				backgroundclass = "labele";
				readonlyclass = "readonlytexteven";
			}
		%>	
				<tr>		
					
					<% if( refvar.equals( "update" ) ){ %>
					<td class = "labeleboldwhite" width = "5%">
						<html:multibox property = "check">
							<bean:write name = "ms" property = "sow_Id"/>
						</html:multibox>
    				</td>
    				<%}else{ %>
    				<td class = "labeleboldwhite" width = "5%">&nbsp;</td>
    				<%} %>
    				<html:hidden  name = "ms" property = "sow_Id"/>
					
					
		<%  
			if( status_size == 1 )
			{
				schdayscheck = "javascript: document.forms[0].check.checked = true;";	
			}
			else if( status_size > 1 )
			{
				schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;";
			}
		%>				

					<% if( refvar.equals( "update" ) ){ %>
					<td class = "<%= backgroundclass %>" width = "95%" colspan = "2">
		   				<html:textarea styleId = '<%= "sow"+i%>' name = "ms" property = "sow" rows = "5" cols = "120" styleClass = "textbox" onmouseover = "highlightTextField( this );" onmouseout = "unHighlightTextField( this );" onclick = "<%= schdayscheck %>" />
		   			</td>
		   			<%}else { %>
		   			<td class = "<%= readonlyclass %>" width = "95%" colspan = "2">
		   				<html:textarea styleId = '<%= "sow"+i%>' name = "ms" property = "sow" rows = "5" cols = "120" styleClass = "<%= readonlyclass %>"  readonly = "true" />
		   			</td>
		   			<%} %>
		   			
		   		</tr>
		   	 <% i++;  %>
	</logic:iterate>
</logic:present>

		<%	
		  	if( status_size != 0 )
		  	{
		  		if( status_size % 2 == 1 )
		  		{
		  			backgroundclass = "labele";
		  			i = 2;
				}
		  		else
		  		{
		  			backgroundclass = "labelo";
		  			i = 1;
		  		}
		  	}
		  	else
			{
				backgroundclass = "labelo";
				i = 1;
			}
	    %>	
				<html:hidden property = "id" />
				<html:hidden property = "type" />
				<html:hidden property = "ref" />
					<% if( refvar.equals( "update" ) ){ %>
					<tr> 
			    		<td class = "labeleboldwhite" width = "5%">
			    			<html:checkbox property = "newsow_Id" />
			   			</td>
			   			
			   			<td class = "<%= backgroundclass %>" width = "95%" colspan = "2">
			   				<html:textarea styleId = "newsow"  property = "newsow" rows = "5" cols = "120" styleClass = "textbox" onclick = "document.forms[0].newsow_Id.checked = true;" onmouseover = "highlightTextField( this );" onmouseout = "unHighlightTextField( this );" />
			   			</td>
					</tr>	
					
					
					
					<tr>
						<td width = "5%">&nbsp;</td>
			  			<td class = "buttonrow" width = "88%">
			  				<html:submit property = "submitFrm" styleClass = "button" onclick = "return validate( 'Submit' );">
			  					<bean:message bundle = "pm" key = "common.sow.submit"/>
			  				</html:submit>
						<% if( status_size != 0 ){ %>
			  				<html:submit property = "submitFrm" styleClass = "button" onclick = "return validate( 'Delete' );">
			  					<bean:message bundle = "pm" key = "common.sow.delete"/>
			  				</html:submit>
			  			<%} %>
			  				<logic:equal name = "SOWForm" property = "type" value = "prm_appendix">
		    				<html:submit property="back" styleClass="button" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:submit>
		    				
		    				</logic:equal>
			  				<logic:equal name = "SOWForm" property = "type" value = "prm_job">
		    				<html:submit property="back" styleClass="button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:submit>
		    				
		    				</logic:equal>
			  		
			  				<logic:equal name = "SOWForm" property = "type" value = "prm_activity">
		    				<html:submit property="back" styleClass="button" onclick = "return Backactionactivity();"><bean:message bundle="PRM" key="prm.button.back"/></html:submit>
		    				
		    				</logic:equal>
		    		
			  			</td>
						<td class = "buttonrow" width = "7%">		
							<html:reset property = "cancel" styleClass = "button">
								<bean:message bundle = "pm" key = "common.sow.cancel"/>
							</html:reset>
						</td>
					</tr>
					<% } 
					else{%>
					<tr> 
	 		 	 	 <td width = "5%">&nbsp;</td>
	 		 	 	 <td colspan="7"> 
	 		     	 <logic:equal name = "SOWForm" property = "type" value = "prm_appendix">
		    				<html:submit property="back" styleClass="button" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:submit>
		    				
		    				</logic:equal>
			  				<logic:equal name = "SOWForm" property = "type" value = "prm_job">
		    				<html:submit property="back" styleClass="button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:submit>
		    				
		    				</logic:equal>
			  		
			  				<logic:equal name = "SOWForm" property = "type" value = "prm_activity">
		    				<html:submit property="back" styleClass="button" onclick = "return Backactionactivity();"><bean:message bundle="PRM" key="prm.button.back"/></html:submit>
		    				
		    				</logic:equal>
				 	 </td>
				 	</tr>
				 	<%} %>
				
			</table>
		</td>
	</tr>
</table>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
</html:form>
</body>

<script>

function validate( temp )
{
	var submitflag = 'false';
	document.forms[0].ref.value = temp;
	
	if( <%= status_size %> != 0 )
	{
		if( <%= status_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newsow_Id.checked ) )
			{
				if( temp == 'Submit' )
				{
					alert( "<bean:message bundle = "PRM" key = "prm.managestatus.selectstatusupdate"/>" );
					return false;
				}
				
				else
				{
					alert( "<bean:message bundle = "PRM" key = "prm.managestatus.selectstatusdelete"/>" );
					return false;
				}
			}
			
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newsow_Id.checked ) )
		  	{
		  		if( temp == 'Submit' )
				{
			  		alert( "<bean:message bundle = "PRM" key = "prm.managestatus.selectstatusupdate"/>" );
					return false;
				}
				else
				{
					alert( "<bean:message bundle = "PRM" key = "prm.managestatus.selectstatusdelete"/>" );
					return false;
				}
		  	}
		}
	}
	
	else
	{
		if( !document.forms[0].newsow_Id.checked )
		{
			alert( "<bean:message bundle = "PRM" key = "prm.managestatus.newstatus"/>" );
			return false;
		}
	}
	
	
	
	if( <%= status_size %> != 0 )
	{	
		if( <%= status_size %> == 1 )
		{
			if( document.forms[0].check.checked ) 
	  		{
	  			if( document.forms[0].sow.value == "" )
		  		{
		  			alert( "<bean:message bundle = "PRM" key = "prm.managestatus.enterstatusname"/>" );
 					document.forms[0].sow.focus();
 					return false;
		  		}
	 		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			if( document.forms[0].sow[i].value == "" )
		  			{
		  				alert( "<bean:message bundle = "PRM" key = "prm.managestatus.enterstatusname"/>" );
		  				document.forms[0].sow[i].focus();
 						return false;
		  			}
		 			
		 		}
		 	}
		 }
 	}

 	if( document.forms[0].newsow_Id.checked )
 	{
 		if( document.forms[0].newsow.value == "" )
 		{
 			alert( "<bean:message bundle = "PRM" key = "prm.managestatus.enterstatusname"/>" );
 			document.forms[0].newsow.focus();
 			return false;
 		}	
 	}
	
	return true;
}


function Backactionappendix()
{
	document.forms[0].action = "AppendixHeader.do?function=view&appendixid=<bean:write name = "SOWForm" property = "id" />&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	
	//document.forms[0].submit();
	
	return true;
}
function Backactionjob()
{
	document.forms[0].action = "JobDashboardAction.do?jobid=<bean:write name = "SOWForm" property = "id" />";
	//document.forms[0].submit();
	return true;
}

function Backactionactivity()
{
	document.forms[0].action = "ActivityHeader.do?function=view&activityid=<bean:write name = "SOWForm" property = "id" />";
	//document.forms[0].submit();
	return true;
}


</script>
<script>
function changeFilter()
{
	document.forms[0].action="ManageStatusAction.do?function=view&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="ManageStatusAction.do?function=view&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].id.value;
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>


</html:html>

  
  