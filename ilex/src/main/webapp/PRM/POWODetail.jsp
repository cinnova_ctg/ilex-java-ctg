<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>
<html:html>
<HEAD>
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</HEAD>
<bean:define id = "masterpoitem" name = "POWOForm" property = "masterpoitem" type = "java.lang.String" />
<bean:define id = "type" name = "POWOForm" property = "type" type = "java.lang.String" />
<% int resource_size =( int ) Integer.parseInt( request.getAttribute( "resource_size" ).toString() ); %>
<% String check_fieldsheet=request.getAttribute( "checkFieldsheet" ).toString();%>
<%
String temp_jobid = "";
if(request.getAttribute( "temp_jobid" ) != null) 
	temp_jobid = ""+request.getAttribute( "temp_jobid" );
	String backgroundclass = null;
	String numberclass = null;
	boolean csschooser = true;
	int i = 0;
	String multiplyquantity = "";
	String textnumber = "";
	String backgroundclassmiddle = null;
	String tempcolspan = "4";
	String showhide = "";
	String showhide1 = "";
	String showPvs_supplied = "";
	String adjustcost = "";
	String computetotalcost = "";
	String prevacttitle = "";
	String widthtab = "";
	String poauthclass = "";
	String withoutSave="";
	//changed by Seema
	String showcredit_card = "";
	String costSpan = "";
	if( masterpoitem.equals( "1" ) || masterpoitem.equals( "3" ) || masterpoitem.equals( "4" ) ) {
		widthtab = "1100";
		costSpan = "5";
	} else {
		widthtab = "1000";
		costSpan = "2";
	}

	if(request.getAttribute("unSave") != null) {
		withoutSave="true";
	}
%>
<%@ include  file = "/Menu.inc" %>
<script>
</script>
<html:form action = "POWODetailAction"> 
<html:hidden property = "jobid" />
<html:hidden property = "appendixid" />
<html:hidden property = "powoid" />
<html:hidden property = "firsttime" />
<html:hidden property = "refflag" />
<html:hidden property = "partnerid" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "tempspeccond" />
<html:hidden property = "fcost"/>
<html:hidden property = "check"/>
<html:hidden property = "changePoPayment"/>
<html:hidden property = "checkDefault"/>
<html:hidden property = "partnerIsMinuteman"/>
<html:hidden property = "refbyonchange"/>

<body onload= "getChange('<%=withoutSave%>'); checkpvstypeFirst(); getDisabled(); return setUnitCostZero();">

<table width = "100%" border="0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
			<td width="2" height="0"></td>
  			 <td> <!-- width = "900" -->
  			 	<table border="0" cellspacing = "1" cellpadding = "1" width = "900"> 
					
					<logic:present name = "poPayment">
						<tr height = "30">
							<td align = "center" colspan = "15" class = "textwhitemiddle"><B><bean:message bundle = "PRM" key = "prm.powo.paymentvalue"/></B></td>
						</tr>
					</logic:present>
					
					<logic:notPresent name = "poPayment">
					 <tr height="20">
		        	      <td>&nbsp;</td>
        		     </tr>
					</logic:notPresent>
					
					<tr height="15">    
						<td colspan = "8" class = "labeleboldwhite" height = "30">
							<bean:message bundle="PRM" key="prm.powo.powo"/> <bean:message bundle="PRM" key="prm.powo.appendix"/>: <A href="AppendixHeader.do?function=view&appendixid=<bean:write name="POWOForm" property="appendixid"/>&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />"><bean:write name="POWOForm" property="appendixname"/></A> -> <bean:message bundle="PRM" key = "prm.powo.job"/>: <A href="JobDashboardAction.do?jobid=<bean:write name="POWOForm" property="jobid" />&viewjobtype=<bean:write name = "POWOForm" property = "viewjobtype" />&ownerId=<bean:write name = "POWOForm" property = "ownerId" />"><bean:write name = "POWOForm" property = "jobname" /></A>
						</td>
					</tr> 

					<tr height="30">   
						<td class = "textwhite">
							<bean:message bundle="PRM" key="prm.powo.partner"/>: 
						</td>
						<td class = "textwhite">
							<a href = "POWOAction.do?jobid=<bean:write name = "POWOForm" property = "jobid" />"><bean:write name = "POWOForm" property = "partnername" /></a>
						</td>
						<td class = "textwhite">
							<span><bean:message bundle = "PRM" key = "prm.powo.poc"/>:</span>
						</td>
						<td class = "textwhite">
							<span>
								<html:select property = "partnerpoc" styleClass = "combowhite">
									<html:optionsCollection name = "POWOForm" property = "partnerpoclist" value = "value" label = "label"/> 
								</html:select>
							</span>
						</td>
						<td class = "textwhite">
							<span><bean:message bundle="PRM" key = "prm.powo.ponumber"/>:</span>						
						</td>

						<td class = "textwhite">
						<span><html:text property = "powono"  size = "10" readonly = "true" styleClass = "textbox"/></span>
						</td>

						<td class = "textwhite">
							<span><bean:message bundle = "PRM" key = "prm.powo.workordernumber"/>:</span>
						</td>
						
						<td class = "textwhite">
							<span><html:text property = "powono"  size = "10" readonly = "true" styleClass = "textbox"/></span>
						</td>
					</tr> 
					<!-- seema -->
					<% boolean disbutton = false;  %>
					
					<logic:equal name = "POWOForm" property = "firsttime" value = "true">
						<% disbutton = true; %>
					</logic:equal>
							
					<logic:notEqual name = "POWOForm" property = "firsttime" value = "true">
						<% disbutton = false; %>
					</logic:notEqual>
					
					<%
					String masterp="";
							
					if(disbutton==true)
						masterp = "javascript: return refform(true,true)";
						
					else
						masterp="javascript: return refform(false,true)";
					%>		
					
					<tr height = "30">    
					
					  <td class = "textwhite">
							<bean:message bundle = "PRM" key = "prm.powo.type"/>:
						</td>
						<td class = "textwhite">
							<span>
							<!-- changed by Seema-13/12/2006 -->
 								<html:select property = "type" styleClass = "combowhite" onchange = "checkpvstype();return changeTerms(true);"> 
									<html:optionsCollection name = "POWOForm" property = "typelist" value = "value" label = "label"/> 
								</html:select>
							</span>
						</td>
					
					
						<td class = "textwhite">
							<span><bean:message bundle = "PRM" key = "prm.powo.masterpoitem"/>:</span>
						</td>

						<td class = "textwhite">
							<span>
								<html:select property = "masterpoitem" styleClass = "combowhite" onchange = "<%= masterp %>">
									<html:optionsCollection name = "POWOForm" property = "masterpritemlist" value = "value" label = "label"/> 
								</html:select>
							</span>
						</td>
			
		
						<td class = "textwhite">
							<bean:message bundle = "PRM" key = "prm.powo.terms"/>:
						</td>
						
						<td class = "textwhite">
							<span>
								<html:select property = "terms" styleClass = "combowhite">
									<html:optionsCollection name = "POWOForm" property = "termslist" value = "value" label = "label"/> 
									</html:select>
							</span>
						</td>
						
						<td class = "textwhite">
							<span><bean:message bundle = "PRM" key = "prm.powo.bulk"/>:</span>
						</td>
						<td class = "textwhite">	
							<span>
							<select name = "bulk" class = "combowhite" disabled = "true">
							<option value="N" selected>No</option>
							<option value="Y">Yes</option>
							</select>
							</span>
						</td>

						<td class = "textwhite">
							&nbsp;
						</td>
					</tr> 
					<tr height = "30">    
						<td class = "textwhite">
							<bean:message bundle = "PRM" key = "prm.powo.issuedatedeatil"/>:
						</td>

						<td class = "textwhite">
							<html:text  styleClass = "textbox" size = "10" property = "issuedate" readonly = "true"/>
		   			 		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" 
		   			 		onclick = "return popUpCalendar( document.forms[0].issuedate , document.forms[0].issuedate , 'mm/dd/yyyy')" 
		   			 		onmouseover = "window.status = 'Date Picker';return true;" 
		   			 		onmouseout = "window.status = '';return true;">
	   			 		</td>
						
						<td class = "textwhite">
							<bean:message bundle="PRM" key="prm.powo.deliverbydatedetail"/>:
						</td>
						
						<td class = "textwhite" colspan = "3">
							<html:text  styleClass = "textbox" size = "10" property = "deliverbydate" readonly = "true"/>
		   			 		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" 
		   			 		onclick = "return popUpCalendar( document.forms[0].deliverbydate , document.forms[0].deliverbydate , 'mm/dd/yyyy')" 
		   			 		onmouseover = "window.status = 'Date Picker';return true;" 
		   			 		onmouseout = "window.status = '';return true;">
		   			 		
		   			 		&nbsp;&nbsp;&nbsp;
				    
						    <html:select name = "POWOForm" property = "deliverbydatehh" styleClass = "combowhite">  
							<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
							</html:select>
							
							<html:select name = "POWOForm" property = "deliverbydatemm" styleClass = "combowhite">  
							<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
							</html:select>
							
							<html:select name = "POWOForm" property = "deliverbydateop" styleClass = "combowhite">  
							<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
							</html:select>
	   			 		</td>
	   			 		
	   			 		<td class = "textwhite">
							<bean:message bundle = "PRM" key = "prm.powo.deliverable"/>:
						</td>
						
						<td class = "textwhite">
							<span>
								<html:select property = "deliverable" styleClass = "combowhite">
									<html:optionsCollection name = "POWOForm" property = "deliverableList" value = "value" label = "label"/> 
									</html:select>
							</span>
						</td>
					</tr> 

					<tr height = "30">    
						<td class = "textwhitetop">
							<bean:message bundle = "PRM" key = "prm.powo.shiptoaddress"/>:
						</td>

						<td class = "textwhite" colspan = "3">
							<html:textarea property = "shiptoaddress" cols = "50" rows = "5" styleClass = "textbox"></html:textarea>
						</td>
						
						<td class = "textwhite" width = "4" align = "left" colspan = "4">
							<html:button  styleClass = "button" property = "contacts" onclick = "return opensearchcontactwindow();">Select from Contacts" </html:button>
						</td>
					</tr>
					<tr height ="3"><td></td></tr>
					<tr height = "30">    
						<td class = "textwhitetop">
							<bean:message bundle = "PRM" key = "prm.powo.specialcondition1"/>
						</td>

						<td class = "textwhite" colspan = "6">
							<html:textarea property = "speccond" cols = "120" rows = "4" styleClass = "textbox" ></html:textarea>
						</td>
						
						<td align = "right" valign="top">
							<html:submit styleClass = "buttontop" property = "resetSPCond" onclick = "return checkpowo();" disabled = "<%= disbutton %>"><bean:message bundle = "PRM" key = "prm.powo.reset"/></html:submit>
						</td>
					</tr>
					<tr height="5"><td></td></tr>
					<tr height = "30">    
						<td class = "textwhitetop">
							<bean:message bundle = "PRM" key = "prm.powo.sow"/>
						</td>
						<html:hidden property = "problemdesc"/>
						<td class = "textwhite" colspan = "6">
							<html:textarea property = "sow" cols = "120" rows = "6" styleClass = "textbox" readonly = "<%= disbutton %>"></html:textarea>
						</td>
						
						<td align = "right" valign="top">
							<html:submit styleClass = "buttontop" property = "resetsow" onclick = "return checkpowo();" disabled = "<%= disbutton %>"><bean:message bundle = "PRM" key = "prm.powo.reset"/></html:submit>
							<br><br><html:submit styleClass = "buttontop" property = "defaultSow" onclick = "return checkpowo();" disabled = "<%= disbutton %>"><bean:message bundle = "PRM" key = "prm.powo.default"/></html:submit>
						</td>
					</tr>
					<tr height ="5"><td></td></tr>
					<tr height = "30">
						<td class = "textwhitetop">
							<bean:message bundle = "PRM" key = "prm.powo.assumption"/>
						</td>
						<td colspan = "6" class = "textwhitetop">
							<html:textarea property = "assumption" cols = "120" rows = "4" styleClass = "textbox" readonly = "<%= disbutton %>"></html:textarea>
						</td>
						
						<td align = "right" valign="top">
							<html:submit styleClass = "buttontop" property = "resetassumption" onclick = "return checkpowo();" disabled = "<%= disbutton %>"><bean:message bundle = "PRM" key = "prm.powo.reset"/></html:submit>
							<br><br><html:submit styleClass = "buttontop" property = "defaultAssumption" onclick = "return checkpowo();" disabled = "<%= disbutton %>"><bean:message bundle = "PRM" key = "prm.powo.default"/></html:submit>
						</td>
					</tr>
					<tr height = "5"><td></td></tr>
					<tr height = "30">
						<td class = "textwhitetop">
							<bean:message bundle = "PRM" key = "prm.powo.specialinstruction1"/>
						</td>
						<td class = "textwhite" colspan = "6">
							<html:textarea property = "specialinstruction" cols = "120" rows = "5" styleClass = "textbox"></html:textarea>
						</td>
						<td align = "right" valign="top">
							<html:submit styleClass = "buttontop" property = "resetSPInst" onclick = "return checkpowo();" disabled = "<%= disbutton %>"><bean:message bundle = "PRM" key = "prm.powo.reset"/></html:submit>
						</td>						
					</tr>
					<tr height="10"><td></td></tr>
			</table>	
		
			      <table border = "0" cellspacing = "0" cellpadding = "0" width = "<%= widthtab %>">
					<TR>
						<TD rowspan = "2" width = "3%">&nbsp; </TD>
						<TD class = tryb rowspan = "2" width = "11%">
							<bean:message bundle = "PRM" key = "prm.powo.activity"/>
						</TD>
						<TD class = tryb rowspan = "2" width = "11%">
							<bean:message bundle = "PRM" key = "prm.powo.resource"/>
						</TD>
						<TD class = tryb colspan = "3" width = "25%">Quantity</TD>
						<TD class = tryb colspan = "2" width = "18%">Estimated</TD>

					 	<% if( masterpoitem.equals( "1" ) || masterpoitem.equals( "3" ) || masterpoitem.equals( "4" ) ) { %>
							<TD class = tryb colspan = "3" width = "30%">Authorized</TD>
						<%}%>
		
						<TD class = tryb rowspan = "2" width = "9%">
							<bean:message bundle = "PRM" key = "prm.powo.showonwo"/>
						</TD>
						<TD class = tryb rowspan = "2" width = "7%">
							<bean:message bundle = "PRM" key = "prm.powo.dropshipped"/>
						</TD>
						<TD class = tryb rowspan = "2" width = "7%">
							<bean:message bundle = "PRM" key = "prm.powo.pvsSupplied"/>
						</TD>
						<TD class = tryb rowspan = "2" width = "7%">
							<bean:message bundle = "PRM" key = "prm.powo.creditcard"/>
						</TD>
  				   </TR>
  				   
  				     <tr> 
    					<td class = "tryb" width = "7%"> 
      						<bean:message bundle = "PRM" key = "prm.powo.resource" />
    					</td>
    					
    					<td class = "tryb" width = "9%"> 
      						<bean:message bundle = "PRM" key = "prm.powo.activity" />
    					</td>
    					
    					<td class = "tryb" width = "9%"> 
      						<bean:message bundle = "PRM" key = "prm.powo.total" />
    					</td>
    					
    					<td class = "tryb" width = "9%"> 
      						<bean:message bundle = "PRM" key = "prm.powo.newunitcost"/>
    					</td>
    					
    					<td class = "tryb"  width = "9%"> 
      						<bean:message bundle = "PRM" key = "prm.powo.newsubtotalcost"/>
    					</td>
    					
    					<% if( masterpoitem.equals( "1" ) || masterpoitem.equals( "3" ) || masterpoitem.equals( "4" ) ) { %>
	    					<td class = "tryb" width = "10%"> 
	      						<bean:message bundle = "PRM" key = "prm.powo.newauthquantity"/>
	    					</td>
	    					
	    					<td class = "tryb" width = "10%"> 
	      						<bean:message bundle = "PRM" key = "prm.powo.newauthunitcost"/>
	    					</td>
	    					
	    					<td class = "tryb" width = "10%"> 
	      						<bean:message bundle = "PRM" key = "prm.powo.newauthsubtotalcost"/>
	    					</td>	
	    				<%} %>
    				</tr>
  				   
  				   
  				   		   
				  <logic:iterate id = "resourcelist" name = "POWOForm" property = "resourcelist" >
				   <bean:define id = "val" name = "resourcelist" property = "resource_id" type = "java.lang.String"/>
				   <bean:define id = "act_title" name = "resourcelist" property = "act_name" type = "java.lang.String"/>

				   <%	
						if ( csschooser == true ) 
						{
							backgroundclass = "textleftodddash";
							backgroundclassmiddle = "textcenterodddashpo"; 
							csschooser = false;
							numberclass = "numberodd";
							textnumber = "readonlytextnumbereven";
							poauthclass = "readonlytextnumberevenpo";
						}
				
						else
						{
							csschooser = true;	
							backgroundclass = "textleftevendash";
							numberclass = "numbereven";
							textnumber = "readonlytextnumberodd";
							backgroundclassmiddle = "textcenterevendashpo"; 
						
						}
					%>
					
										 
					<%  
					if( resource_size == 1 )
					{
						multiplyquantity = "javascript: return mulquantity( null ); ";
						showhide = "javascript:return checkfirstbox( null );";
						showhide1 = "javascript:return checksecondbox( null );";
						adjustcost = "javascript:return adjustfixedcost( null ); ";
						//Start :Added By Amit
						showPvs_supplied = "javascript:return checkPvsSuppliedbox( null );";
						//End : Added By Amit
                        //changed by Seema
						showcredit_card = "javascript:return checkcreditcardbox( null );";
						
						
					}
					else if( resource_size > 1 )
					{
						multiplyquantity = "javascript: return mulquantity( '"+i+"' );";
						showhide = "javascript:return checkfirstbox( '"+i+"' );";
						showhide1 = "javascript:return checksecondbox( '"+i+"' );";	
						adjustcost = "javascript:return adjustfixedcost( '"+i+"' );";
						//Start :Added By Amit
						showPvs_supplied = "javascript:return checkPvsSuppliedbox( '"+i+"' );";
						//End : Added  By Amit
						//changed by Seema
						showcredit_card = "javascript:return checkcreditcardbox( '"+i+"' );";
											}	
					%>
					
					   <tr>
							<td>
								<html:multibox property = "resource_id" onclick = "<%= adjustcost %>">
									<bean:write name = "resourcelist" property = "resource_id" />
								</html:multibox>
							</td>
							
							<html:hidden name = "resourcelist" property = "resource_tempid" />
							<html:hidden name = "resourcelist" property = "resource_type" />
							<html:hidden name = "resourcelist" property = "resourceSubCat" />
							<bean:define id = "resource_type" name = "resourcelist" property = "resource_type" type = "java.lang.String" />
							<bean:define id = "resourceSubCat" name = "resourcelist" property = "resourceSubCat" type = "java.lang.String" />
							
							<td class = "<%= backgroundclass %>">
								<% 
									if( !prevacttitle.equals( act_title ) )
									{
								%>
									<bean:write name = "resourcelist" property = "act_name" /> 
								<%
									} 
								%>
							</td>
							
							<td class = "<%= backgroundclass %>">
								<bean:write name = "resourcelist" property = "resource_name" />
							</td>
							
							<td class = "<%= numberclass %>">
								<bean:write name = "resourcelist" property = "resource_qty" />
								<html:hidden name = "resourcelist" property = "resource_qty" /> 
							</td>
							
							<td class = "<%= numberclass %>">
								<bean:write name = "resourcelist" property = "act_qty" />
								<html:hidden name = "resourcelist" property = "act_qty" /> 
							</td>
							
							<td class = "<%= numberclass %>">
								<bean:write name = "resourcelist" property = "resource_act_qty" />
								<html:hidden name = "resourcelist" property = "resource_act_qty" /> 
							</td>
							
							<td class = "<%= numberclass %>">
							<html:text name = "resourcelist" property = "unitcost" styleClass = "<%= textnumber %>" size = "10" readonly = "true"/>
							</td>
							
							<td class = "<%= numberclass %>">
								<html:text name = "resourcelist" property = "resource_subtotalcost" styleClass = "<%= textnumber %>" size = "10" readonly = "true"  />
							</td>
							

							<% if( masterpoitem.equals( "1" ) || masterpoitem.equals( "3" ) || masterpoitem.equals( "4" ) ){ %>
								<td class = "<%= numberclass %>">
									<html:text name = "resourcelist" property = "resource_authorizedqty" styleClass = "textboxnumber" size = "10" onchange = "<%= multiplyquantity %>" />
								</td>
								
								<% //if(type.equals("S")) { %>
							<!-- 	<td class = "<%= numberclass %>">
									<html:text name = "resourcelist" property = "resource_authorizedunitcost" styleClass = "textboxnumber" size = "10"  readonly = "true" onchange = "<%= multiplyquantity %>" />
								</td>
						-->		
								<%// } 	else 
								if(type.equals("M") || type.equals("S")) {
									if(resource_type.equals("Contract Labor") || resource_type.equals("Corporate Labor") || (resource_type.equals("Travel") && (resourceSubCat.equals("Hourly")))) {  %>
								<td class = "<%= numberclass %>">
									<html:text name = "resourcelist" property = "resource_authorizedunitcost" styleClass = "textboxnumber" size = "10"  readonly = "true"  />
								</td>
								<%}else { %>
								<td class = "<%= numberclass %>">
									<html:text name = "resourcelist" property = "resource_authorizedunitcost" styleClass = "textboxnumber" size = "10"   onchange = "<%= multiplyquantity %>" />
								</td>
								<%}
								}else { %>
								<td class = "<%= numberclass %>">
									<html:text name = "resourcelist" property = "resource_authorizedunitcost" styleClass = "textboxnumber" size = "10"   onchange = "<%= multiplyquantity %>" />
								</td>
								<%} %>
								
								<td class = "<%= numberclass %>">
									<html:text name = "resourcelist" property = "resource_authorizedtotalcost" styleClass = "<%= textnumber %>" size = "10" readonly = "true"  />
								</td>	
										
							<%}else { %>
									<html:hidden name = "resourcelist" property = "resource_authorizedqty" />
									<html:hidden name = "resourcelist" property = "resource_authorizedunitcost" />
									<html:hidden name = "resourcelist" property = "resource_authorizedtotalcost" />
							<% } %>
								
							<td class = "<%= backgroundclassmiddle %>">
								<html:multibox property = "resource_showonwo" onclick = "<%= showhide %>">
									<bean:write name = "resourcelist" property = "resource_id" />
								</html:multibox>
								
								<html:hidden name = "resourcelist" property = "resource_tempshowonwo" />
							</td>
							
							<td class = "<%= backgroundclassmiddle %>">
								<html:multibox property = "resource_dropshift" onclick = "<%= showhide1 %>">
									<bean:write name = "resourcelist" property = "resource_id" />
								</html:multibox>
								
								<html:hidden name = "resourcelist" property = "resource_tempdropshift" />
							</td>
							
							<!-- Start : Added By Amit For PVS Supplied -->
							<td class = "<%= backgroundclassmiddle %>">
								<html:multibox property = "pvs_supplied" onclick = "<%= showPvs_supplied %>">
									<bean:write name = "resourcelist" property = "resource_id" />
								</html:multibox>
								
								<html:hidden name = "resourcelist" property = "resource_temp_pvs_supplied" />
							</td>							
							
							<!-- End : For PVS Supplied -->
							<!-- changes made by Seema -->
							<td class = "<%= backgroundclassmiddle %>">
								<html:multibox property = "credit_card" onclick = "<%= showcredit_card %>">
									<bean:write name = "resourcelist" property = "resource_id" />
								</html:multibox>
								
								<html:hidden name = "resourcelist" property = "resource_temp_credit_card" />
							</td>					
							
						</tr>
						
						<% 
							i++;
							prevacttitle = act_title;
						%> 
					</logic:iterate>
					
					<% 
						if( resource_size != 0 ) 
						{
					%>
							<tr>
								<td class = "textwhiteleft">
									 &nbsp;		
								</td>
		
								<td align = "left" colspan = "20">
									<html:button styleClass = "button" property = "selectall " onclick = "return selectAll();">Select&nbsp;All</html:button>
									&nbsp;
									<html:button styleClass = "button" property = "deselectall " onclick = "return deSelectAll();">Deselect&nbsp;All</html:button>
								</td>	
							</tr>
					<% 
						} 
					%>		
					<tr>

						<td class = "textwhiteleft">
							 &nbsp;		
						</td>
						
						<td class = "textwhiteleftitalic" colspan = "<%= tempcolspan %>">
							<bean:message bundle = "PRM" key = "prm.powo.resourcemessage"/>
						</td>
						
						<% 	if(!type.equals("M") && !type.equals("S")) { %>
						<td class = "textwhiteright" colspan = "2">
							<b><bean:message bundle = "PRM" key = "prm.powo.newesttotalcost"/> :				
						</td>
						<td class = "textwhiteright">
							<html:text property = "totalcost"  size = "13" styleClass = "labelowhiterightred" readonly = "true"/>
						</td>
						<html:hidden property = "contractEstTotal" />
						<% } else{ %>
						<html:hidden property = "totalcost" />
						<td class = "textwhiteright" colspan = "<%= costSpan %>">
							<b>Contract Labor Total Cost:	&nbsp;			
						</td>
						<td class = "textwhiteright">
							<html:text property = "contractEstTotal"  size = "13" styleClass = "labelowhiteright" readonly = "true"/> 
						</td>
						<% }
						
						if( masterpoitem.equals( "1" ) || masterpoitem.equals( "3" ) || masterpoitem.equals( "4" ) ) 
						{
						 	if(!type.equals("M") && !type.equals("S")) { %>
								<td class = "textwhiteright" colspan = "3">
								<b><bean:message bundle = "PRM" key = "prm.powo.authorizedtotalcost"/> :
								<html:text property = "authorizedtotal"  size = "13" styleClass = "labelowhiteright" readonly = "true"/>
								</td>
						<%	 } 
						 }

							if( masterpoitem.equals( "2" ) && (!type.equals("M") && !type.equals("S")))
							{
							%>
								<td class = "labeloredright">
									Meet or Beat
								</td>
							<% 
							}
							else {
							%>
					
						<td>
							&nbsp;
						</td>
						<% } %>
					</tr>
					<!-- begin -->
					<% if(type.equals("M") || type.equals("S")) { %>
					<tr>
						<td class = "textwhiteleft" colspan = "5">
			 					&nbsp;		
						</td>
		
							<td class = "textwhiteright" colspan = "<%=costSpan %>">
								<b>Estimated Non-Contract Labor Total Cost: &nbsp;
							</td>	
							
							
							<% if(masterpoitem.equals("2")) { %>
								<td class = "textwhiteright">
									<html:text  property = "nonContractEstTotal"  size = "11" styleClass = "textboxnumber"  onchange = "return getAuthorizedTotal()"/> 
								</td>
							<% } else {%>
								<html:hidden property = "nonContractEstTotal"/> 
							<%} %>
							 
							
							
							<% if(masterpoitem.equals("2")) { %>
							 <td class = "textwhiteright">
								&nbsp;	<html:text  property = "nonContractEstLabelTotal"  size = "11" styleClass = "labeloredalignright"  readonly = "true"/>&nbsp;</td> <td class = "labeloredleft">&nbsp; Beat or Meet
							 </td>
							<%} else { %>
							 <td class = "textwhiteright"> 
								 <html:text  property = "nonContractEstLabelTotal"  size = "11" styleClass = "labelowhiteright"  readonly = "true"/> 
							<%} %>
							 </td>	
							
							
					</tr>
					<% } else {
					 %>
					<html:hidden property = "nonContractEstTotal"/>
					<html:hidden property = "nonContractEstLabelTotal"/>
					
					<%} %>
					
					<!-- end -->
					<tr>
						<td class = "textwhiteleft" colspan = "5">
			 				&nbsp;		
						</td>
					
					<!-- seema -->				
					<% 
						if( masterpoitem.equals( "0" ) ) 
						{
					%>
							<td class = "textwhiteright" colspan = "2">
							<% if(type.equals("S")) { %>
									<b><bean:message bundle = "PRM" key = "prm.powo.speedauthorizedtotalcost"/> : &nbsp;
								<% } else if(type.equals("M")) { %>
								 	<b><bean:message bundle = "PRM" key = "prm.powo.minauthorizedtotalcost"/> : &nbsp;
								 <% } else { %>
								 	<b><bean:message bundle = "PRM" key = "prm.powo.authorizedtotalcost"/> : &nbsp;
								 <%} %>	
							</td>
							<td class = "textwhiteright">
							<% if(type.equals("S") || type.equals("M")) {%>
									<html:text  property = "authorizedtotal"  size = "11" styleClass = "labelowhiteright" readonly = "true"/> 
							<%}else { %>
								<html:text  property = "authorizedtotal"  size = "11" styleClass = "textboxnumber" />
							<%} %>
							</td>
					<% }
						if( masterpoitem.equals( "2" ) ) 
						{
					%>
							<td class = "textwhiteright" colspan = "2">
							<% if(type.equals("S")) {%>
									<b><bean:message bundle = "PRM" key = "prm.powo.speedauthorizedtotalcost"/> : &nbsp;
							<% } else if(type.equals("M")) { %>
								 	<b><bean:message bundle = "PRM" key = "prm.powo.minauthorizedtotalcost"/> : &nbsp;
							<% } else {%>
								 	<b><bean:message bundle = "PRM" key = "prm.powo.authorizedtotalcost"/> : &nbsp;
							<%} %>	
							</td>
							<td class = "textwhiteright">
							<% if(type.equals("S") || type.equals("M")) {%>
								<html:text  property = "authorizedtotal"  size = "11" styleClass = "labelowhiteright"  readonly = "true" onchange="return fixedcost()"/> 
							<%}else { %>
								<html:text  property = "authorizedtotal"  size = "11" styleClass = "textboxnumber" onchange="return fixedcost()"/>
							<%} %>
							</td>
					<% }
						if( masterpoitem.equals( "1" ) || masterpoitem.equals( "3" ) || masterpoitem.equals( "4" ) )
						{
							if(!type.equals("M") && !type.equals("S")) {
					%>
						<!-- 	<html:hidden property = "authorizedtotal" /> -->
							<td class = "textwhiteleft" colspan = "2">
			 					&nbsp;		
							</td>
							<td class = "labeloredright" >
									Meet or Beat
							</td>
					<% } else {	%>
						  <td class = "textwhiteright" colspan = "<%=costSpan %>">
						<% if(type.equals("S")) {%>
							<b><bean:message bundle = "PRM" key = "prm.powo.speedauthorizedtotalcost"/> : &nbsp;
						<% } else if(type.equals("M")) { %>
							<b><bean:message bundle = "PRM" key = "prm.powo.minauthorizedtotalcost"/> : &nbsp;
						<% } %>
						</td>
						<td class = "textwhiteright">
							<html:text  property = "authorizedtotal"  size = "11" styleClass = "labelowhiteright" readonly = "true"/> 
						</td>
				<%	}
				}%>
						<td>
							&nbsp;
						</td>
					</tr>
						
					
			 </table>
			 
		
			 <table border="0" cellspacing = "1" cellpadding = "1" width = "800">
			 <tr>
					<td colspan = "20" class = "labeleboldwhite" height = "40">
						<bean:message bundle = "PRM" key = "prm.powo.reconciliationdetails"/>:
					</td>
			 </tr>

			 <tr height = "30">    
				<td class = "textwhite">
					<bean:message bundle = "PRM" key = "prm.powo.invoicenumber"/>:
				</td>

				<td class = "textwhite">
					<html:text property = "invoiceno"  size = "10" styleClass = "textbox"/>
				</td>

				<td class = "textwhite">
					<bean:message bundle = "PRM" key = "prm.powo.invoicereceiveddate"/>:
				</td>
				
				<td class = "textwhite">
					<html:text  styleClass = "textbox" size = "10" property = "invoicerecddate" readonly = "true"/>
   			 		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" 
   			 		onclick = "return popUpCalendar( document.forms[0].invoicerecddate , document.forms[0].invoicerecddate , 'mm/dd/yyyy')" 
   			 		onmouseover = "window.status = 'Date Picker';return true;" 
   			 		onmouseout = "window.status = '';return true;">
	   			</td>
				<td class = "textwhite" colspan="16">
					&nbsp;
				</td>
			</tr> 
				<tr height = "30">    
					<td class = "textwhitetop" >
						<bean:message bundle = "PRM" key = "prm.powo.comments"/>:
					</td>

					<td class = "textwhite" colspan = "3">
						<html:textarea property = "invoicecomments" cols = "50" rows = "5" styleClass = "textbox"></html:textarea>
					</td>

					<td>
					<table border="0">
					<tr>
						<td class = "textwhite">
							<bean:message bundle = "PRM" key = "prm.powo.invoicedtotal"/>:
						</td>
						
						<td class = "textwhite">
							<html:text property = "invoicetotal"  size = "10" styleClass = "textboxnumber"/>
						</td>
					</tr>
					<tr>
						<td class = "textwhite">
							<bean:message bundle = "PRM" key = "prm.powo.approvedtotal"/>:
						</td>
						
						<td class = "textwhite">
							<html:text property = "approvedtotal"  size = "10" styleClass = "textboxnumber"/>
						</td>
					</tr>
					</table>
					</td>

					<td class = "textwhite" colspan = "15">
						&nbsp;
					</td>
				</tr>
			<logic:notPresent name = "poPayment">
				<tr height = "40"> 
						<td align = "middle" colspan = "20">
							<logic:equal name = "POWOForm" property = "firsttime" value = "true">
								<html:submit styleClass = "button" property = "next" onclick = "return checkpowo();">Next</html:submit>
							</logic:equal>
							
							<logic:notEqual name = "POWOForm" property = "firsttime" value = "true">
								<html:submit styleClass = "button" property = "save" onclick = "return validate();">Save</html:submit>
							</logic:notEqual>
							
							&nbsp;&nbsp;
							
							
							<html:cancel styleClass = "button">Cancel</html:cancel>
							
							&nbsp;&nbsp;<input type="button" class="Button" value="Back" onclick = "return Backaction();">
						</td>
				</tr>
			</logic:notPresent>
			<logic:present name = "poPayment">
				<tr height = "30">
					<td align = "center" colspan = "20" class = "textwhitemiddle"><B><bean:message bundle = "PRM" key = "prm.powo.paymentvalue"/></B></td>
				</tr>
				<tr height = "30">
					<td align = "center" colspan = "19"><input type="button" class="Button" value="Back" onclick = "return Backaction();" /></td>
				</tr>
			</logic:present>
			 </table>

			  </td> 
		</tr>
</table>		
</body>

<script>


function fixedcost()
{
  if( document.forms[0].authorizedtotal.value == '' || !(isFloat(document.forms[0].authorizedtotal.value) ) )
   {
  	   //alert( 'Please enter numeric value' );
   	  document.forms[0].authorizedtotal.value = '0.00';    
   }
  document.forms[0].fcost.value = document.forms[0].authorizedtotal.value;
  
}


function selectAll()
{
	document.forms[0].totalcost.value = '0.00';
	document.forms[0].contractEstTotal.value = '0.00';
	document.forms[0].nonContractEstLabelTotal.value = '0.00';

	if( <%= resource_size %> != 0 )
	{
		if( <%= resource_size %> == 1 )
		{
			document.forms[0].resource_id.checked = true;
			document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );		
			if((document.forms[0].type.value=="S" || document.forms[0].type.value=="M") && document.forms[0].masterpoitem.value == 2)
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
						{
							document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
						} else {
							document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
						}
							getTotalNonContractLabor('0');
						
			} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
			}
			
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				document.forms[0].authorizedtotal.value = '0.00';
				document.forms[0].resource_authorizedqty.value = document.forms[0].resource_act_qty.value;
				document.forms[0].resource_authorizedunitcost.value =document.forms[0].unitcost.value;
				document.forms[0].resource_authorizedtotalcost.value = round_func( parseFloat( document.forms[0].resource_authorizedqty.value ) + parseFloat( document.forms[0].resource_authorizedunitcost.value ) );
	
				if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
						} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
					}
						getTotalNonContractLabor('0');
					//document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat( document.forms[0].nonContractEstLabelTotal.value ) );
				} else 
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost.value ) );
			}
		} 
		else
		{
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				document.forms[0].authorizedtotal.value = '0.00';
			}
			for( var i = 0; i < document.forms[0].resource_id.length; i++ )
		  	{
				if((document.forms[0].type.value=="P" || document.forms[0].type.value=="S" || document.forms[0].type.value=="M") && document.forms[0].resource_type[i].value == "Corporate Labor" ) { }
				else {	
				document.forms[0].resource_id[i].checked = true;
		  		document.forms[0].resource_subtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_act_qty[i].value ) * parseFloat( document.forms[0].unitcost[i].value ) );		
		  		
		  		if((document.forms[0].type.value=="S" || document.forms[0].type.value=="M") && document.forms[0].masterpoitem.value == 2)
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
						{
							document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
						} else {
							document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
						}
				} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
		  		}
		  		
		  		if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
				{
					document.forms[0].resource_authorizedqty[i].value = document.forms[0].resource_act_qty[i].value;
				    document.forms[0].resource_authorizedunitcost[i].value =document.forms[0].unitcost[i].value;
					document.forms[0].resource_authorizedtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_authorizedqty[i].value ) * parseFloat( document.forms[0].resource_authorizedunitcost[i].value ) );	
				//begin
				if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
						} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					}
				}else
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost[i].value ) );
				}
		  	 }
		  	}
		  
		  	if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		  	{
		  		getTotalNonContractLabor('0');
		  	}
		}
	}
	else
	{
		alert( 'Resource not available' );
		return false;
	}
	return;
}

function deSelectAll()
{
	if( <%= resource_size %> != 0 )
	{
		document.forms[0].totalcost.value = '';
		document.forms[0].contractEstTotal.value = '0.00';
		document.forms[0].nonContractEstTotal.value = '0.00';
		document.forms[0].nonContractEstLabelTotal.value = '0.00';
		
		if(document.forms[0].masterpoitem.value != 0)
			{ document.forms[0].authorizedtotal.value = '0.00';
			  document.forms[0].fcost.value = '0.00'; }
		if( <%= resource_size %> == 1 )
		{
			document.forms[0].resource_id.checked = false;
			document.forms[0].resource_subtotalcost.value = '';
	
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				document.forms[0].resource_authorizedqty.value = '';
				document.forms[0].resource_authorizedunitcost.value = '';
				document.forms[0].resource_authorizedtotalcost.value = '';
			}
		} 
		else
		{
			for( var i = 0; i < document.forms[0].resource_id.length; i++ )
		  	{
		  		document.forms[0].resource_id[i].checked = false;
				document.forms[0].resource_subtotalcost[i].value = '';	
				if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
				{
					document.forms[0].resource_authorizedqty[i].value = '';
					document.forms[0].resource_authorizedunitcost[i].value = '';
					document.forms[0].resource_authorizedtotalcost[i].value = '';
				}
		  	}
		  	document.forms[0].sow.value = '';	
		  	document.forms[0].assumption.value = '';	
		}
	}
	else
	{
		alert( 'Resource not available' );
		return false;
	}
}
function checkpowo()
{
	document.forms[0].type.disabled=false;
	var submitflag = 'false';
	if( <%= resource_size %> != 0 )
	{
		if( <%= resource_size %> == 1 )
		{
			if( !( document.forms[0].resource_id.checked ) )
			{
				alert( 'Please select a resource' );
				return false;
			}
		} 
		else
		{
			for( var i = 0; i<document.forms[0].resource_id.length; i++ )
		  	{
		  		if( document.forms[0].resource_id[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' )
		  	{
		  		alert( 'Please select a resource' );
				return false;
		  	}
		}
	}
	else
	{
		alert( 'Resource not available' );
		return false;
	}
	
	return true;
}

function checkfirstbox( index )
{
	if( index != null )
	{	
		if( !document.forms[0].resource_id[index].checked )
		{
			document.forms[0].resource_showonwo[index].checked = false;
		}
	}
	
	else
	{
		if( !document.forms[0].resource_id.checked )
		{
			document.forms[0].resource_showonwo.checked = false;
		}
	}
	return true;
}
function getDisabled() {
		<logic:present name="POWOForm" property="partnerIsMinuteman" scope = "request">
		    	 <logic:equal name="POWOForm" property="partnerIsMinuteman" value = "yyy">	
		    	 	document.forms[0].type.disabled=true;
		    	 </logic:equal>
		</logic:present>
				
}
function getChange(withoutSave) {
		if(document.forms[0].checkDefault.value == "true" && withoutSave=="true" ) {
		document.forms[0].checkDefault.value="false";
		checkpvstype();
		changeTerms("false");
		}
}

function checksecondbox( index )
{
	if( index != null )
	{	
		if( !document.forms[0].resource_id[index].checked )
		{
			document.forms[0].resource_dropshift[index].checked = false;
		}
	}
	
	else
	{
		if( !document.forms[0].resource_id.checked )
		{
			document.forms[0].resource_dropshift.checked = false;
		}
	}
	return true;
}

function checkPvsSuppliedbox( index )
{
	if( index != null )
	{	
		if( !document.forms[0].resource_id[index].checked )
		{
			document.forms[0].pvs_supplied[index].checked = false;
		}
	}
	
	else
	{
		if( !document.forms[0].resource_id.checked )
		{
			document.forms[0].pvs_supplied.checked = false;
		}
	}
	return true;
}


function checkcreditcardbox( index )
{
	if( index != null )
	{	
		if( !document.forms[0].resource_id[index].checked )
		{
			document.forms[0].credit_card[index].checked = false;
		}
	}
	
	else
	{
		if( !document.forms[0].resource_id.checked )
		{
			document.forms[0].credit_card.checked = false;
		}
	}
	return true;
}
//This function will be called when user select the checkbox


function adjustfixedcost( val )
{

if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
{
	document.forms[0].authorizedtotal.value = '0.00';
}	
	if( val != null )
	{
		if( document.forms[0].resource_id[val].checked )
		{
			document.forms[0].resource_subtotalcost[val].value = round_func( parseFloat( document.forms[0].resource_act_qty[val].value ) * parseFloat( document.forms[0].unitcost[val].value ) );	
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				 document.forms[0].resource_authorizedqty[val].value =document.forms[0].resource_act_qty[val].value;
				 document.forms[0].resource_authorizedunitcost[val].value =document.forms[0].unitcost[val].value;
					if( document.forms[0].resource_authorizedqty[val].value == '' || !(isFloat(document.forms[0].resource_authorizedqty[val].value) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedqty[val].value = '0.00';	
					}
				
					if( document.forms[0].resource_authorizedunitcost[val].value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost[val].value ) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedunitcost[val].value = '0.00';	
					}				
				document.forms[0].resource_authorizedtotalcost[val].value = round_func( parseFloat( document.forms[0].resource_authorizedqty[val].value ) * parseFloat( document.forms[0].resource_authorizedunitcost[val].value ) );	
			}
		} else {
			document.forms[0].resource_subtotalcost[val].value = '';
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
			
				document.forms[0].resource_authorizedqty[val].value = '';
				document.forms[0].resource_authorizedunitcost[val].value = '';
				document.forms[0].resource_authorizedtotalcost[val].value = '';
			}
		}
		
	document.forms[0].totalcost.value = '0.00';
	document.forms[0].contractEstTotal.value = '0.00';
	document.forms[0].nonContractEstLabelTotal.value = '0.00';
		
		for( var i = 0; i < document.forms[0].resource_id.length; i++ )
		{
		if( document.forms[0].resource_id[i].checked )
		{
			if(document.forms[0].masterpoitem.value == 2 || document.forms[0].masterpoitem.value == 0)
		 	{
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
					}
				} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
				}  // for 2 and p/n
		 	}else {
		 		document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					}
				} else {
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost[i].value ) );
		 		}
		 	}   /// 1,3,4
		}
	  }
	  	if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		  	{
		  		getTotalNonContractLabor('0');
		  	}	
	} 
	else				// only single record.
	{
		if( document.forms[0].resource_id.checked )
		{
			document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				 document.forms[0].resource_authorizedqty.value = round_func( parseFloat(document.forms[0].resource_act_qty.value));
				 document.forms[0].resource_authorizedunitcost.value =round_func( parseFloat(document.forms[0].unitcost.value));
					 if( document.forms[0].resource_authorizedqty.value == '' || !(isFloat(document.forms[0].resource_authorizedqty.value) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedqty.value = '0.00';	
					}
					if( document.forms[0].resource_authorizedunitcost.value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost.value ) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedunitcost.value = '0.00';	
					}	
				document.forms[0].resource_authorizedtotalcost.value = round_func( parseFloat( document.forms[0].resource_authorizedqty.value ) * parseFloat( document.forms[0].resource_authorizedunitcost.value ) );	
			}
		}else {
			document.forms[0].resource_subtotalcost.value = '';
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				document.forms[0].resource_authorizedqty.value = '';
				document.forms[0].resource_authorizedunitcost.value = '';
				document.forms[0].resource_authorizedtotalcost.value = '';
			}
		}
		
		if( document.forms[0].resource_id.checked )
		{
			if(document.forms[0].masterpoitem.value == 2)
		 	{
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
					}
				} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
				}  // for 2 and p/n
		 	}else {
		 		document.forms[0].authorizedtotal.value = '0.00';
		 		document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
					}
				} else {
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost.value ) );
		 		}
		 	}   /// 1,3,4
		}else{
			document.forms[0].totalcost.value = '0.00';
			document.forms[0].contractEstTotal.value = '0.00';
			document.forms[0].nonContractEstLabelTotal.value = '0.00';
		}
		
		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		  	{
		  		getTotalNonContractLabor('0');
		  	}	
	}
}



function mulquantity( val )
{
	if(val!=null)
	{
	document.forms[0].resource_id[val].checked = true;
	document.forms[0].resource_subtotalcost[val].value = round_func( parseFloat( document.forms[0].resource_act_qty[val].value ) * parseFloat( document.forms[0].unitcost[val].value ) );	
	
	if( document.forms[0].resource_authorizedqty[val].value == '' || !(isFloat(document.forms[0].resource_authorizedqty[val].value) ) )
       {
         document.forms[0].resource_authorizedqty[val].value = '0.00';    
       }
    if( document.forms[0].resource_authorizedunitcost[val].value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost[val].value ) ) )
       {
        document.forms[0].resource_authorizedunitcost[val].value = '0.00';
	   }
	   document.forms[0].resource_authorizedtotalcost[val].value = round_func( parseFloat( document.forms[0].resource_authorizedqty[val].value ) * parseFloat( document.forms[0].resource_authorizedunitcost[val].value ) );    
	}
	 else 
	 {
	 	document.forms[0].resource_id.checked = true;
		document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	

		if( document.forms[0].resource_authorizedqty.value == '' || !(isFloat(document.forms[0].resource_authorizedqty.value) ) )
       	{
       	  document.forms[0].resource_authorizedqty.value = '0.00';    
       	}

    	if( document.forms[0].resource_authorizedunitcost.value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost.value ) ) )
       	{
       	 document.forms[0].resource_authorizedunitcost.value = '0.00';
	   	}
	   	  
	   	document.forms[0].resource_authorizedtotalcost.value = round_func( parseFloat( document.forms[0].resource_authorizedqty.value ) * parseFloat( document.forms[0].resource_authorizedunitcost.value ) );    
	 }
	 
	
	if( val != null )
	{
		if( document.forms[0].type.value=="P" || document.forms[0].type.value=="S" || document.forms[0].type.value=="M"){
			if( document.forms[0].resource_type[val].value == "Corporate Labor") {
				document.forms[0].resource_id[val].checked = false;
			}
		}	
		else
			document.forms[0].resource_id[val].checked = true;		
	}
	else
	{
		if( document.forms[0].type.value=="P" || document.forms[0].type.value=="S" || document.forms[0].type.value=="M")	{  
			if( document.forms[0].resource_type.value == "Corporate Labor") {
				document.forms[0].resource_id.checked = false;
			}
		}
		else
			document.forms[0].resource_id.checked = true;		
	}
	getTotal();
		
	return true;
}



function refform(disbutton,checkRef)
{ 
 //if(disbutton==true)

 /*
 	document.forms[0].contractEstTotal.value = '0.00';
	document.forms[0].nonContractEstTotal.value = '0.00';
	document.forms[0].nonContractEstLabelTotal.value = '0.00';
				 
   if( <%= resource_size %> != 0 ) 
	{ 
		if( <%= resource_size %> == 1 ) 
		{
			if( !document.forms[0].resource_id.checked )
			{
				document.forms[0].resource_subtotalcost.value = '';
			}
			else
			{
				document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	
				if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
						{
							document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
						} else {
							document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
						}
				if( document.forms[0].nonContractEstTotal.value == '' ||  !(isFloat( document.forms[0].nonContractEstTotal.value ) ) )
  				{
   	   					document.forms[0].nonContractEstTotal.value = '0.00';
   				}	
   				if( document.forms[0].contractEstTotal.value == '' ||  !(isFloat( document.forms[0].contractEstTotal.value ) ) )
   				{
   	   					document.forms[0].contractEstTotal.value = '0.00';
   				}				
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat(document.forms[0].nonContractEstTotal.value ));
				} else {
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].resource_subtotalcost.value ));
			 }
			
			//before
				//document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	
				//document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].resource_subtotalcost.value ));
			}
		}
		else 
		{
			document.forms[0].totalcost.value = '0';
			for( var i = 0; i<document.forms[0].resource_id.length; i++ )
		  	{
				if( !document.forms[0].resource_id[i].checked )
				{
					document.forms[0].resource_subtotalcost[i].value = '';
				}
		  		else
		  		{
		  			document.forms[0].resource_subtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_act_qty[i].value ) * parseFloat( document.forms[0].unitcost[i].value ) );	
			  		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
					{
						if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
							{
								document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
							} else {
								document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
							}
					} else {
			  			document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ));
				   }
		  		
		  		//before
		  		//	document.forms[0].resource_subtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_act_qty[i].value ) * parseFloat( document.forms[0].unitcost[i].value ) );	
		  		//	document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ));
		  		
		  		}
			}
			if( document.forms[0].nonContractEstTotal.value == '' ||  !(isFloat( document.forms[0].nonContractEstTotal.value ) ) )
   			{
   	   			document.forms[0].nonContractEstTotal.value = '0.00';
   			}	
   			if( document.forms[0].contractEstTotal.value == '' ||  !(isFloat( document.forms[0].contractEstTotal.value ) ) )
   			{
   	   			document.forms[0].contractEstTotal.value = '0.00';
   			}
   			if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M") {				
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat(document.forms[0].nonContractEstTotal.value ));
			}
			//before
		}	
		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M") {
			document.forms[0].authorizedtotal.value = document.forms[0].totalcost.value ;
		}
	}
	
	if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
		{
		document.forms[0].authorizedtotal.value = '0.00';
		
			if( <%= resource_size %> == 1 ) 
			{
				if( !document.forms[0].resource_id.checked )
					{
						document.forms[0].resource_authorizedqty.value= '';
						document.forms[0].resource_authorizedunitcost.value = '';
						document.forms[0].resource_authorizedtotalcost.value = '';
					}
				else
					{	
				 		document.forms[0].resource_authorizedqty.value =document.forms[0].resource_act_qty.value;
			   			document.forms[0].resource_authorizedunitcost.value =document.forms[0].unitcost.value;
			   	    	document.forms[0].resource_authorizedtotalcost.value = round_func( parseFloat( document.forms[0].resource_authorizedqty.value ) * parseFloat( document.forms[0].resource_authorizedunitcost.value ) );	
			  			document.forms[0].authorizedtotal.value = round_func(parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost.value )); 
					}
			}
			
			else 
			{
					for( var i = 0; i< document.forms[0].resource_id.length; i++ )
		  			{
						if( !document.forms[0].resource_id[i].checked )
						{
							document.forms[0].resource_authorizedqty[i].value = '';
							document.forms[0].resource_authorizedunitcost[i].value = '';
							document.forms[0].resource_authorizedtotalcost[i].value = '';
			  			}
			  			else
			  			{	
			  				document.forms[0].resource_authorizedqty[i].value =document.forms[0].resource_act_qty[i].value;
			 				document.forms[0].resource_authorizedunitcost[i].value =document.forms[0].unitcost[i].value;
			  				document.forms[0].resource_authorizedtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_authorizedqty[i].value ) * parseFloat( document.forms[0].resource_authorizedunitcost[i].value ) );	
	   					  	document.forms[0].authorizedtotal.value = round_func(parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost[i].value)); 
			  		  	}
			  	
		  			}
		  		
		  	}
		 
	 }	
 }
 */
 if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
	{
 		document.forms[0].authorizedtotal.value = '0.00';
 	}
 	
	if( <%= resource_size %> != 0 ) 
	{
		if( <%= resource_size %> == 1 ) 
		{
		if( document.forms[0].resource_id.checked )
		{
			document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				 document.forms[0].resource_authorizedqty.value =document.forms[0].resource_act_qty.value;
				 document.forms[0].resource_authorizedunitcost.value = document.forms[0].unitcost.value;
					
					 if( document.forms[0].resource_authorizedqty.value == '' || !(isFloat(document.forms[0].resource_authorizedqty.value) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedqty.value = '0.00';	
					}
				
					if( document.forms[0].resource_authorizedunitcost.value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost.value ) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedunitcost.value = '0.00';	
					}				
				document.forms[0].resource_authorizedtotalcost.value = round_func( parseFloat( document.forms[0].resource_authorizedqty.value ) * parseFloat( document.forms[0].resource_authorizedunitcost.value ) );	
			}
		}else {
			document.forms[0].resource_subtotalcost.value = '';
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				document.forms[0].resource_authorizedqty.value = '';
				document.forms[0].resource_authorizedunitcost.value = '';
				document.forms[0].resource_authorizedtotalcost.value = '';
			}
		}
		
			document.forms[0].totalcost.value = '0';
			document.forms[0].contractEstTotal.value = '0.00';
			document.forms[0].nonContractEstLabelTotal.value = '0.00';
		
		if( document.forms[0].resource_id.checked )
		{
			if(document.forms[0].masterpoitem.value == 2)
		 	{
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
					}
				} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
				}  // for 2 and p/n
		 	}else {
		 		document.forms[0].authorizedtotal.value = '0.00';
		 		document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
					}
				} else {
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost.value ) );
		 		}
		 	}   /// 1,3,4
		
		
		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		  	{
		  		getTotalNonContractLabor('0');
		  	}	
		}
		else {			// not a single resource checked	
				document.forms[0].totalcost.value = '0.00';
				document.forms[0].contractEstTotal.value = '0.00';
				document.forms[0].nonContractEstLabelTotal.value = '0.00';
				document.forms[0].authorizedtotal.value = '0.00';
		}
	} else {    // more than 1 resource
			document.forms[0].totalcost.value = '0';
			document.forms[0].contractEstTotal.value = '0.00';
			document.forms[0].nonContractEstLabelTotal.value = '0.00';
			
		for( var i = 0; i < document.forms[0].resource_id.length; i++ )
		{
		if( document.forms[0].resource_id[i].checked )
		{
			document.forms[0].resource_subtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_act_qty[i].value ) * parseFloat( document.forms[0].unitcost[i].value ) );	
		
			if(document.forms[0].masterpoitem.value == 2)
		 	{
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
					}
				} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
				}  // for 2 and p/n
		 	}else {
			 	
		 		document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
		 		 document.forms[0].resource_authorizedqty[i].value =document.forms[0].resource_act_qty[i].value;
				 document.forms[0].resource_authorizedunitcost[i].value = document.forms[0].unitcost[i].value;
		
				 if( document.forms[0].resource_authorizedqty[i].value == '' || !(isFloat(document.forms[0].resource_authorizedqty[i].value) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedqty[i].value = '0.00';	
					}
					if( document.forms[0].resource_authorizedunitcost[i].value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost[i].value ) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedunitcost[i].value = '0.00';	
					}
				 document.forms[0].resource_authorizedtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_authorizedqty[i].value ) * parseFloat( document.forms[0].resource_authorizedunitcost[i].value ) );	
				
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					}
				} else {
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost[i].value ) );
		 		}
		 	}   /// 1,3,4
		}
		else {
			document.forms[0].resource_subtotalcost[i].value = '';
			if(document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4)
			{
			document.forms[0].resource_authorizedqty[i].value = '';
			document.forms[0].resource_authorizedunitcost[i].value = '';
			document.forms[0].resource_authorizedtotalcost[i].value = '';
			}
		}
	  }
	  	if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		  	{
		  		getTotalNonContractLabor('0');
		  	}
	   }	
	}


if( disbutton == false && document.forms[0].masterpoitem.value == 2 && (document.forms[0].type.value != "S" && document.forms[0].type.value != "M"))
{
		document.forms[0].authorizedtotal.value =document.forms[0].fcost.value;
}			
	//else 
	//{
	 //	mspochange();
   	//}
  
	
	document.forms[0].check.value = 'false';						
	document.forms[0].refflag.value = 'true';
	document.forms[0].type.disabled=false;
	if(document.forms[0].partnerIsMinuteman.value=="yyy" && checkRef == "false") {
	} else {
	document.forms[0].refbyonchange.value='true';
	document.forms[0].type.disabled=false;
	document.forms[0].submit();
	}	
	return true;
}


function changeTerms(check)
{
 var checkRefresh=check;
 var par = document.forms[0].firsttime.value;
 
	if(document.forms[0].type.value == 'P') {
		document.forms[0].terms.value = 6; 
	}
	
	if(document.forms[0].type.value == 'N') {
		document.forms[0].terms.value = 1;
	}
	
	if(document.forms[0].type.value == 'S' ) { 
		document.forms[0].terms.value = 8;
		document.forms[0].masterpoitem.value = 2;
	}
	 if(document.forms[0].type.value == 'M') {
		document.forms[0].terms.value = 8;
		document.forms[0].masterpoitem.value = 1;
	}
	
	document.forms[0].check.value = 'false';
	refform(par,checkRefresh);
	
}

function opensearchcontactwindow()
{
	if( document.forms[0].type.value == 0 )
	{
		alert( 'Please select type' );
		document.forms[0].type.focus();
		return false;
	}
	//alert(document.forms[0].appendixid.value);
	//str = 'SearchcontactAction.do?partnername=<bean:write name = "POWOForm" property = "partnername" />&appendixid='+document.forms[0].appendixid.value+'&type='+document.forms[0].type.value+'&partnerid='+document.forms[0].partnerid.value;
	str = 'SearchcontactAction.do?appendixid='+document.forms[0].appendixid.value+'&type='+document.forms[0].type.value+'&partnerid='+document.forms[0].partnerid.value;
	
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 500 , height = 400 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();

	return true;
}

function Backaction()
{

	 document.forms[0].action = "POWOAction.do?jobid=<%=temp_jobid%>&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	 document.forms[0].submit();
	 return true;
}


function validate()
{
	document.forms[0].bulk.disabled = false;
	document.forms[0].type.disabled=false;
	if( document.forms[0].masterpoitem.value == '0' )
	{
		alert( "<bean:message bundle = "PRM" key = "prm.powo.selectmasterpoitem"/>" );
		document.forms[0].masterpoitem.focus();
		return false;
	}
	
	if( document.forms[0].terms.value == '0' )
	{
		alert( "<bean:message bundle = "PRM" key = "prm.powo.selectterms"/>" );
		document.forms[0].terms.focus();
		return false;
	}
	
	
	if( document.forms[0].type.value == '0' )
	{
		alert( "<bean:message bundle = "PRM" key = "prm.powo.selecttype"/>" );
		document.forms[0].type.focus();
		return false;
	}
	
	if( document.forms[0].authorizedtotal.value == '' )
	{	
		alert(document.forms[0].authorizedtotal.value);
		document.forms[0].authorizedtotal.value = '0.00';
	}
	
	if( !isFloat( document.forms[0].authorizedtotal.value ) )
	{
		alert( "<bean:message bundle = "PRM" key = "prm.powo.numericauthorizedtotal"/>" );
		document.forms[0].authorizedtotal.focus();
		return false;
	}
	
	if( document.forms[0].invoicetotal.value != "")
	{
		if( !isFloat( document.forms[0].invoicetotal.value ) )
		{
			alert( "<bean:message bundle = "PRM" key = "prm.powo.numericinvoicetotal"/>" );
			document.forms[0].invoicetotal.value = '';
			document.forms[0].invoicetotal.focus();
			return false;
		}
	}

	if( document.forms[0].approvedtotal.value != "" )
	{
		if( !isFloat( document.forms[0].approvedtotal.value ) )
		{
			alert( "<bean:message bundle = "PRM" key = "prm.powo.numericapprovedtotal"/>" );
			document.forms[0].approvedtotal.value = '';
			document.forms[0].approvedtotal.focus();
			return false;
		}
	}
	
	var returnval = true;
	returnval = getTotalNonContractLabor('1');
	if(returnval == true)
	{
		document.forms[0].check.value ='true';
	}
	return returnval;
	
	
	return true;
	
}


function setUnitCostZero()
{
if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
	{
		document.forms[0].authorizedtotal.value = '0.00';
	}
	
	document.forms[0].totalcost.value = '0.00';
	document.forms[0].contractEstTotal.value = '0.00';
	document.forms[0].nonContractEstLabelTotal.value = '0.00';
		
	if( <%= resource_size %> != 0 ) 
	{
		if( <%= resource_size %> == 1 ) 
		{
		if( document.forms[0].resource_id.checked )
		{
			document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
			//now added
			if(document.forms[0].check.value == 'false') {
			document.forms[0].resource_authorizedqty.value = round_func( parseFloat( document.forms[0].resource_act_qty.value )) ;	
			document.forms[0].resource_authorizedunitcost.value = round_func( parseFloat( document.forms[0].unitcost.value ) );	
			}
					 if( document.forms[0].resource_authorizedqty.value == '' || !(isFloat(document.forms[0].resource_authorizedqty.value) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedqty.value = '0.00';	
					}
				
					if( document.forms[0].resource_authorizedunitcost.value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost.value ) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedunitcost.value = '0.00';	
					}				
				document.forms[0].resource_authorizedtotalcost.value = round_func( parseFloat( document.forms[0].resource_authorizedqty.value ) * parseFloat( document.forms[0].resource_authorizedunitcost.value ) );	
			}
		}else {
			document.forms[0].resource_subtotalcost.value = '';
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				document.forms[0].resource_authorizedqty.value = '';
				document.forms[0].resource_authorizedunitcost.value = '';
				document.forms[0].resource_authorizedtotalcost.value = '';
			}
		}
		
		if( document.forms[0].resource_id.checked )
		{
			if(document.forms[0].masterpoitem.value == 2)
		 	{
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
					}
				} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
				}  // for 2 and p/n
		 	}else {
		 		document.forms[0].authorizedtotal.value = '0.00';
		 		document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
					}
				} else {
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost.value ) );
		 		}
		 	}   /// 1,3,4
		
		
		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		  	{
		  		getTotalNonContractLabor('0');
		  	}	
		}
		else {			// not a single resource checked	
				document.forms[0].totalcost.value = '0.00';
				document.forms[0].contractEstTotal.value = '0.00';
				document.forms[0].nonContractEstLabelTotal.value = '0.00';
				document.forms[0].authorizedtotal.value = '0.00';
		     }
		} else {   																 // more than 1 resource
			document.forms[0].totalcost.value = '0.00';
			document.forms[0].contractEstTotal.value = '0.00';
			document.forms[0].nonContractEstLabelTotal.value = '0.00';

	for( var i = 0; i < document.forms[0].resource_id.length; i++ )
	{
		if( document.forms[0].resource_id[i].checked )
		{
			document.forms[0].resource_subtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_act_qty[i].value ) * parseFloat( document.forms[0].unitcost[i].value ) );	
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
			if(document.forms[0].check.value == 'false') {
			document.forms[0].resource_authorizedqty[i].value = document.forms[0].resource_act_qty[i].value;	
			document.forms[0].resource_authorizedunitcost[i].value = document.forms[0].unitcost[i].value;	
			}
					if( document.forms[0].resource_authorizedqty[i].value == '' || !(isFloat(document.forms[0].resource_authorizedqty[i].value) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedqty[i].value = '0.00';	
					}
				
					if( document.forms[0].resource_authorizedunitcost[i].value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost[i].value ) ) )
					{
						alert( 'Please enter numeric value' );
						document.forms[0].resource_authorizedunitcost[i].value = '0.00';	
					}				
				document.forms[0].resource_authorizedtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_authorizedqty[i].value ) * parseFloat( document.forms[0].resource_authorizedunitcost[i].value ) );	
			}
		}else {
			document.forms[0].resource_subtotalcost[i].value = '';
			if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
			{
				document.forms[0].resource_authorizedqty[i].value = '';
				document.forms[0].resource_authorizedunitcost[i].value = '';
				document.forms[0].resource_authorizedtotalcost[i].value = '';
			}
		}
	}	

		for( var i = 0; i < document.forms[0].resource_id.length; i++ )
		{
		if( document.forms[0].resource_id[i].checked )
		{
			if(document.forms[0].masterpoitem.value == 2)
		 	{
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
					}
				} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
				}  // for 2 and p/n
		 	}else {
		 		
		 		document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					}
				} else {
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost[i].value ) );
		 		}
		 	}   /// 1,3,4
		}
		else {
			document.forms[0].resource_subtotalcost[i].value = '';
			if(document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4)
			{
			document.forms[0].resource_authorizedqty[i].value = '';
			document.forms[0].resource_authorizedunitcost[i].value = '';
			document.forms[0].resource_authorizedtotalcost[i].value = '';
			}
		}
	  }
	  	if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		  	{
		  		getTotalNonContractLabor('0');
		  	}
	   }	
	}
	document.forms[0].check.value = 'true';
}


/*

function setUnitCostZero() 
{
// for fixed prices services (2)
	
	if( <%= resource_size %> != 0 ) 
	{ 
	
		if( <%= resource_size %> == 1 ) 
		{
			if( !document.forms[0].resource_id.checked )
			{
				document.forms[0].resource_subtotalcost.value = '';
				document.forms[0].contractEstTotal.value = '0.00';
				document.forms[0].nonContractEstTotal.value = '0.00';
				document.forms[0].nonContractEstLabelTotal.value = '0.00';
			}
			else
			{
			
				document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	
				if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
						{
							document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
						} else {
							document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
						}
				if( document.forms[0].nonContractEstTotal.value == '' ||  !(isFloat( document.forms[0].nonContractEstTotal.value ) ) )
  				{
   	   					document.forms[0].nonContractEstTotal.value = '0.00';
   				}	
   				if( document.forms[0].contractEstTotal.value == '' ||  !(isFloat( document.forms[0].contractEstTotal.value ) ) )
   				{
   	   					document.forms[0].contractEstTotal.value = '0.00';
   				}				
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat(document.forms[0].nonContractEstTotal.value ));
				} else {
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].resource_subtotalcost.value ));
			 }
			}
		}
		else 
		{
			document.forms[0].totalcost.value = '0';
			document.forms[0].contractEstTotal.value = '0.00';
			document.forms[0].nonContractEstLabelTotal.value = '0.00';
			
			for( var i = 0; i<document.forms[0].resource_id.length; i++ )
		  	{
				if( !document.forms[0].resource_id[i].checked )
				{
					document.forms[0].resource_subtotalcost[i].value = '';
			  	}
			  	else
			  	{	
			  	document.forms[0].resource_subtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_act_qty[i].value ) * parseFloat( document.forms[0].unitcost[i].value ) );	
			  	if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
						{
							document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
						} else {
							document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
						}
				} else {
			  		document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ));
				   }			  	
			  	}
		  	}
		  	
		  	if( document.forms[0].nonContractEstTotal.value == '' ||  !(isFloat( document.forms[0].nonContractEstTotal.value ) ) )
   			{
   	   			document.forms[0].nonContractEstTotal.value = '0.00';
   			}	
   			if( document.forms[0].contractEstTotal.value == '' ||  !(isFloat( document.forms[0].contractEstTotal.value ) ) )
   			{
   	   			document.forms[0].contractEstTotal.value = '0.00';
   			}
   			if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M") {				
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat(document.forms[0].nonContractEstTotal.value ));
			}		
		}
		
		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M") {
			document.forms[0].authorizedtotal.value = document.forms[0].totalcost.value ;
		}	
	}

	if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
	{ document.forms[0].authorizedtotal.value = '0.00';
		if( <%= resource_size %> == 1 ) 
		{
			if( !document.forms[0].resource_id.checked )
			{
				document.forms[0].resource_authorizedqty.value= '';
				document.forms[0].resource_authorizedunitcost.value = '';
				document.forms[0].resource_authorizedtotalcost.value = '';
			}
			else
			{	
					if( document.forms[0].resource_authorizedqty.value == '' || !(isFloat(document.forms[0].resource_authorizedqty.value) ) )
    				{
      				 document.forms[0].resource_authorizedqty.value = '0.00';    
    				}
  	
   					if( document.forms[0].resource_authorizedunitcost.value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost.value ) ) )
   					{
   	   				document.forms[0].resource_authorizedunitcost.value = '0.00';
   					}			
   					  			    
				document.forms[0].authorizedtotal.value = round_func(parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost.value )); 
			  	
			}
		}
		else 
		{
			for( var i = 0; i< document.forms[0].resource_id.length; i++ )
		  	{
				if( !document.forms[0].resource_id[i].checked )
				{
					document.forms[0].resource_authorizedqty[i].value = '';
					document.forms[0].resource_authorizedunitcost[i].value = '';
					document.forms[0].resource_authorizedtotalcost[i].value = '';
			  	}
			  	else
			  	{	
			  			if( document.forms[0].resource_authorizedqty.value == '' || !(isFloat(document.forms[0].resource_authorizedqty.value) ) )
    					{
       					document.forms[0].resource_authorizedqty.value = '0.00';    
    					}
  	
   						if( document.forms[0].resource_authorizedunitcost.value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost.value ) ) )
   						{
   	   					document.forms[0].resource_authorizedunitcost.value = '0.00';
   						}
   						
			  	document.forms[0].authorizedtotal.value = round_func(parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost[i].value)); 
			  	}
			  	
		  	}
		  	
		  		  		
		}
	}
	
}	
*/

//changes made by Seema

function checkpvstype() 
{
	if( <%= resource_size %> != 0 ) 
	{
		if( <%= resource_size %> == 1 ) 
		{
			if( document.forms[0].type.value=="P" || document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
			{  
				if( document.forms[0].resource_type.value == "Corporate Labor")
				{		
					document.forms[0].resource_id.checked = false;
					document.forms[0].resource_id.disabled = true;
					document.forms[0].resource_subtotalcost.value = '';
	
					if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
					{
						document.forms[0].resource_authorizedqty.value = '';
						document.forms[0].resource_authorizedunitcost.value = '';
						document.forms[0].resource_authorizedtotalcost.value = '';
					}
					
				}
				else	
				{ 
				    document.forms[0].resource_id.disabled = false;
				}
			}
			else
			{
				document.forms[0].resource_id.disabled = false; //alert('not P for single');
			}
			
			if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
			{
				if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						if(document.forms[0].type.value=="S"){
							document.forms[0].unitcost.value = '24.00';
						}
						
						if(document.forms[0].type.value=="M"){
							document.forms[0].unitcost.value = '18.00';
						}
					}
			}
		}	
		else 
		{ 
			for( var i = 0; i<document.forms[0].resource_id.length; i++ )
		  	{
				if( document.forms[0].type.value=="P" || document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{ 
					if( document.forms[0].resource_type[i].value == "Corporate Labor" )
					{	
						document.forms[0].resource_id[i].checked = false;
						document.forms[0].resource_id[i].disabled = true; 
						document.forms[0].resource_subtotalcost[i].value = '';
	
						if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
						{
							document.forms[0].resource_authorizedqty[i].value = '';
							document.forms[0].resource_authorizedunitcost[i].value = '';
							document.forms[0].resource_authorizedtotalcost[i].value = '';
						}
						
					}
					else	
					{
				    	document.forms[0].resource_id[i].disabled= false; //alert('enabled for multiple'); 
					}
				}	
				else
					{
				   		document.forms[0].resource_id[i].disabled = false;  //alert(' not entered p for multiple'); 
					}
					
			if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
			{
				if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
				{
					if(document.forms[0].type.value=="S"){
						document.forms[0].unitcost[i].value = '24.00';
					}
					
					if(document.forms[0].type.value=="M"){
						document.forms[0].unitcost[i].value = '18.00';
					}
				}
			 }

			}
	
		}
 }


getTotal(); //Get Total
}

//Change by Avinash 26-12-06

function checkpvstypeFirst() {
	if(<%=check_fieldsheet %>=='1') {
		document.forms[0].deliverable.value= "B";
	}
	if( <%= resource_size %> != 0 ) {
		if( <%= resource_size %> == 1 ) {
			if( document.forms[0].type.value=="P" || document.forms[0].type.value=="S" || document.forms[0].type.value=="M") {  
				if( document.forms[0].resource_type.value == "Corporate Labor") {	
					if(!document.forms[0].resource_id.checked) {
						document.forms[0].resource_id.disabled = true;
						document.forms[0].resource_subtotalcost.value = '';
					}
	
					if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
					{
						if(!document.forms[0].resource_id.checked) {
							document.forms[0].resource_authorizedqty.value = '';
							document.forms[0].resource_authorizedunitcost.value = '';
							document.forms[0].resource_authorizedtotalcost.value = '';
						}	
					}
				}
				else { 
				    document.forms[0].resource_id.disabled = false;
				}
			}	
			else
			{
				document.forms[0].resource_id.disabled = false; //alert('not P for single');
			}
			
			if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
			{
				if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
					{
						if(document.forms[0].type.value=="S"){
							document.forms[0].unitcost.value = '24.00';
						}
						
						if(document.forms[0].type.value=="M"){
							document.forms[0].unitcost.value = '18.00';
						}
					}
			}
		}	
		else 
		{ 
			for( var i = 0; i<document.forms[0].resource_id.length; i++ )
		  	{ 
				if( document.forms[0].type.value=="P" || document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{ 
					if( document.forms[0].resource_type[i].value == "Corporate Labor" )
					{	
						if(!document.forms[0].resource_id[i].checked) {
							document.forms[0].resource_id[i].disabled = true; 
							document.forms[0].resource_subtotalcost[i].value = '';
						}	
	
						if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
						{
							if(!document.forms[0].resource_id[i].checked) {
								document.forms[0].resource_authorizedqty[i].value = '';
								document.forms[0].resource_authorizedunitcost[i].value = '';
								document.forms[0].resource_authorizedtotalcost[i].value = '';
							}
						}
						
					}
					else	
					{
				    	document.forms[0].resource_id[i].disabled= false; //alert('enabled for multiple'); 
					}
				}	
				else
					{
				   		document.forms[0].resource_id[i].disabled = false;  //alert(' not entered p for multiple'); 
					}
				
			if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
			 {
			
				if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						if(document.forms[0].type.value=="S"){
							document.forms[0].unitcost[i].value = '24.00';
						}
						
						if(document.forms[0].type.value=="M"){
							document.forms[0].unitcost[i].value = '18.00';
						}
					}
			 }
			}
		}
 }
}

// Created by Avinash 29/11/06

function getTotal() {

document.forms[0].totalcost.value = 0.0;
document.forms[0].contractEstTotal.value = 0.0;
document.forms[0].nonContractEstLabelTotal.value = 0.0;

if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
{    	document.forms[0].authorizedtotal.value = '0.00'; }

if( <%= resource_size %> != 0 )
	{
	if( <%= resource_size %> == 1 )
		{
	if( document.forms[0].resource_id.checked )
	{
		if(document.forms[0].masterpoitem.value == 2)
	 	{
	 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
			{
				if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
				{
					document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
				} else {
					document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
				}
			} else {
				document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
			}  // for 2 and p/n
	 	}else {
		 	document.forms[0].authorizedtotal.value = '0.00';
		 	document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost.value ) );
		 	if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
			{
				if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
				{
					document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
				} else {
					document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost.value));
				}
			} else {
				document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost.value ) );
		 	}
		 }   /// 1,3,4
	}
		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		 {
		 	getTotalNonContractLabor('0');
		  }
  }
	else {
		for( var i = 0; i < document.forms[0].resource_id.length; i++ )
		{
		if( document.forms[0].resource_id[i].checked )
		{
			if(document.forms[0].masterpoitem.value == 2)
		 	{
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
					}
				} else {
					document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
				}  // for 2 and p/n
		 	}else {
		 		document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ) );
		 		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
					{
						document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					} else {
						document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_authorizedtotalcost[i].value));
					}
				} else {
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost[i].value ) );
		 		}
		 	}   /// 1,3,4
		}
	  }
	  	if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
		  	{
			  	getTotalNonContractLabor('0');
		  	}
	  }
	}	
}

function mspochange()
{
	if( <%=resource_size %>!=0 )
	{ 
		if( document.forms[0].masterpoitem.value == 2)
		{
			document.forms[0].authorizedtotal.value =document.forms[0].fcost.value;
		
		if( <%= resource_size %> == 1 ) 
		{
			if( !document.forms[0].resource_id.checked )
			{
				document.forms[0].resource_subtotalcost.value = '';
				document.forms[0].contractEstTotal.value = '0.00';
				document.forms[0].nonContractEstTotal.value = '0.00';
				document.forms[0].nonContractEstLabelTotal.value = '0.00';
			}
			else
			{
				document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	
				if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
				{
					if( document.forms[0].resource_type.value == "Contract Labor" || ((document.forms[0].resource_type.value == "Travel" && document.forms[0].resourceSubCat.value == "Hourly")) )
						{
							document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
						} else {
							document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost.value));
						}
				if( document.forms[0].nonContractEstTotal.value == '' ||  !(isFloat( document.forms[0].nonContractEstTotal.value ) ) )
  				{
   	   					document.forms[0].nonContractEstTotal.value = '0.00';
   				}	
   				if( document.forms[0].contractEstTotal.value == '' ||  !(isFloat( document.forms[0].contractEstTotal.value ) ) )
   				{
   	   					document.forms[0].contractEstTotal.value = '0.00';
   				}				
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat(document.forms[0].nonContractEstTotal.value ));
				} else {
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].resource_subtotalcost.value ));
			 }
			//	document.forms[0].resource_subtotalcost.value = round_func( parseFloat( document.forms[0].resource_act_qty.value ) * parseFloat( document.forms[0].unitcost.value ) );	
			//	document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].resource_subtotalcost.value ));
			}
		}
		else 
		{
			document.forms[0].totalcost.value = '0';
			document.forms[0].contractEstTotal.value = '0.00';
			document.forms[0].nonContractEstLabelTotal.value = '0.00';
			
			for( var i = 0; i<document.forms[0].resource_id.length; i++ )
		  	{
				if( !document.forms[0].resource_id[i].checked )
				{
					document.forms[0].resource_subtotalcost[i].value = '';
			  	}
			  	else
			  	{
			  		document.forms[0].resource_subtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_act_qty[i].value ) * parseFloat( document.forms[0].unitcost[i].value ) );	
			  		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M")
					{
						if( document.forms[0].resource_type[i].value == "Contract Labor" || ((document.forms[0].resource_type[i].value == "Travel" && document.forms[0].resourceSubCat[i].value == "Hourly")) )
							{
								document.forms[0].contractEstTotal.value =round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
							} else {
								document.forms[0].nonContractEstLabelTotal.value = round_func(parseFloat(document.forms[0].nonContractEstLabelTotal.value) + parseFloat(document.forms[0].resource_subtotalcost[i].value));
							}
					} else {
			  			document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ));
				   }			
			  		//document.forms[0].resource_subtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_act_qty[i].value ) * parseFloat( document.forms[0].unitcost[i].value ) );	
			  		//document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].totalcost.value ) + parseFloat( document.forms[0].resource_subtotalcost[i].value ));
			  	}
		  	}
		  	
		  	if( document.forms[0].nonContractEstTotal.value == '' ||  !(isFloat( document.forms[0].nonContractEstTotal.value ) ) )
   			{
   	   			document.forms[0].nonContractEstTotal.value = '0.00';
   			}	
   			if( document.forms[0].contractEstTotal.value == '' ||  !(isFloat( document.forms[0].contractEstTotal.value ) ) )
   			{
   	   			document.forms[0].contractEstTotal.value = '0.00';
   			}
   			if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M") {				
				document.forms[0].totalcost.value = round_func(parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat(document.forms[0].nonContractEstTotal.value ));
			}	
		}
		if(document.forms[0].type.value=="S" || document.forms[0].type.value=="M") {
			document.forms[0].authorizedtotal.value = document.forms[0].totalcost.value ;
		}	
			
	} 
	
	
	if( document.forms[0].masterpoitem.value == 1 || document.forms[0].masterpoitem.value == 3 || document.forms[0].masterpoitem.value == 4 )
		{
			if(<%=resource_size %>==1)
			{
			  	 if( document.forms[0].resource_id.checked )
				  {
					document.forms[0].resource_authorizedqty.value =document.forms[0].resource_act_qty.value;
			 		 document.forms[0].resource_authorizedunitcost.value =document.forms[0].unitcost.value;
			    	
						if( document.forms[0].resource_authorizedqty.value == '' || !(isFloat(document.forms[0].resource_authorizedqty.value) ) )
						{
							document.forms[0].resource_authorizedqty.value = '0.00';	
						}
				
						if( document.forms[0].resource_authorizedunitcost.value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost.value ) ) )
						{
						document.forms[0].resource_authorizedunitcost.value = '0.00';
						}
			
					document.forms[0].resource_authorizedtotalcost.value = round_func( parseFloat( document.forms[0].resource_authorizedqty.value ) * parseFloat( document.forms[0].resource_authorizedunitcost.value ) );	
			  	}
				else
			  	{
					document.forms[0].resource_authorizedqty.value = '';
					document.forms[0].resource_authorizedunitcost.value = '';
					document.forms[0].resource_authorizedtotalcost.value = '';
			  	}
					document.forms[0].authorizedtotal.value = '0.00';
			
					if( document.forms[0].resource_id.checked )
					{
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost.value ) );
					}
			}
			else
			{
				document.forms[0].authorizedtotal.value = '0.00';
			 	for(var i=0;i<document.forms[0].resource_id.length;i++)
			  	{
			  	 if( document.forms[0].resource_id[i].checked )
				  {
					 document.forms[0].resource_authorizedqty[i].value =document.forms[0].resource_act_qty[i].value;
 			 		 document.forms[0].resource_authorizedunitcost[i].value =document.forms[0].unitcost[i].value;
			 		
					if( document.forms[0].resource_authorizedqty[i].value == '' || !(isFloat(document.forms[0].resource_authorizedqty[i].value) ) )
					{
						document.forms[0].resource_authorizedqty[i].value = '0.00';	
					}
				
					if( document.forms[0].resource_authorizedunitcost[i].value == '' ||  !(isFloat( document.forms[0].resource_authorizedunitcost[i].value ) ) )
					{
						document.forms[0].resource_authorizedunitcost[i].value = '0.00';	
					}				
					document.forms[0].resource_authorizedtotalcost[i].value = round_func( parseFloat( document.forms[0].resource_authorizedqty[i].value ) * parseFloat( document.forms[0].resource_authorizedunitcost[i].value ) );	
					document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].authorizedtotal.value ) + parseFloat( document.forms[0].resource_authorizedtotalcost[i].value ) );
					
				  }
				 else
				  {
					document.forms[0].resource_authorizedqty[i].value = '';
					document.forms[0].resource_authorizedunitcost[i].value = '';
					document.forms[0].resource_authorizedtotalcost[i].value = '';
				  }
				}
			  }
		}
	}
}
	

	
//over
function getAuthorizedTotal()
{
	if( document.forms[0].nonContractEstTotal.value == '' || !(isFloat(document.forms[0].nonContractEstTotal.value) ) )
	{
		document.forms[0].nonContractEstTotal.value = '0.00';	
	}

var nonContractEstTotal = document.forms[0].nonContractEstLabelTotal.value; 
if( document.forms[0].nonContractEstTotal.value > ((110*nonContractEstTotal)/100) )
	{
		alert('The authorized cost you entered for non-contract labor/hourly travel resources exceeds Contingent cost policy. \n Please review and adjust the cost or consult your supervisor for assistance.');
		document.forms[0].nonContractEstTotal.value = '0.00';
		document.forms[0].authorizedtotal.value = '0.00';
		document.forms[0].authorizedtotal.value = round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].nonContractEstTotal.value)) ;
		document.forms[0].nonContractEstTotal.focus();
		return false;
	}
document.forms[0].authorizedtotal.value = '0.00';
document.forms[0].authorizedtotal.value = round_func(parseFloat(document.forms[0].contractEstTotal.value) + parseFloat(document.forms[0].nonContractEstTotal.value)) ;
return true;
}

function getTotalNonContractLabor(val)
{
var fromSave = 0; 
 if(document.forms[0].type.value == "S" || document.forms[0].type.value == "M")
 {
	if((document.forms[0].masterpoitem.value == 2) || (document.forms[0].masterpoitem.value == 0))
	{
		var nonContractTotalCost = 0.00;
		nonContractTotalCost = round_func( parseFloat((110*document.forms[0].nonContractEstLabelTotal.value)/100));
		if(parseFloat(document.forms[0].nonContractEstTotal.value) > parseFloat(nonContractTotalCost))
		{
			alert('The authorized cost you entered for non-contract labor/hourly travel resources exceeds Contingent cost policy. \n Please review and adjust the cost or consult your supervisor for assistance.');
			document.forms[0].nonContractEstTotal.value = '0.00';
			document.forms[0].nonContractEstTotal.focus();
			if(val == 1)
			{
				fromSave = 1;
			}
		}

		document.forms[0].totalcost.value = '0.00';
		document.forms[0].totalcost.value = round_func( parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat( document.forms[0].nonContractEstTotal.value ) );
		document.forms[0].authorizedtotal.value = '0.00';
		document.forms[0].authorizedtotal.value = round_func( parseFloat(document.forms[0].totalcost.value));
		
		if(fromSave ==1)
		{
			return false;
		}
	}else {
		var nonContractTotalCost = 0.00;
		nonContractTotalCost = round_func( parseFloat((110*document.forms[0].totalcost.value)/100));
		if(parseFloat(document.forms[0].nonContractEstLabelTotal.value) >parseFloat(nonContractTotalCost))
		{
			alert('The authorized cost you entered for non-contract labor/hourly travel resources exceeds Contingent cost policy. \n Please review and adjust the cost or consult your supervisor for assistance.');
			if(val == 1)
			{
				fromSave = 1;
			}
		}
		document.forms[0].authorizedtotal.value = '0.00';
		document.forms[0].authorizedtotal.value = round_func( parseFloat( document.forms[0].contractEstTotal.value ) + parseFloat( document.forms[0].nonContractEstLabelTotal.value ) );
		if(fromSave ==1)
		{
			return false;
		}
	}
	
  }	
  return true;
}





</script>





</html:form>
</html:html>