<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>

<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />

<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>

	<script>
		function createJObsWithMPO() {
			var selecetd = false;
			if(!document.forms[0].mpoIdSelected) {
				alert('Please select MPO.')
				return false;
			} else if (document.forms[0].mpoIdSelected.length > 0) {
				for(var i=0;i<document.forms[0].mpoIdSelected.length;i++) {
					if(document.forms[0].mpoIdSelected[i].checked == true) {
						selecetd = true;
						break;
					}
				}
			} else {
				if(document.forms[0].mpoIdSelected.checked == true) {
						selecetd = true;
				}
			}
			if(selecetd == false) {
				alert('Please select MPO.');
				return false;
			}
			var partnerSelected = false;
			if('<c:out value="${requestScope.partnerSize}"/>' > 0) {
				if('<c:out value="${requestScope.partnerSize}"/>' == 1) {
					if(document.forms[0].parnerIdList.checked == true) {
							partnerSelected = true;
						}
				} else {
					for(var j =0; j< '<c:out value="${requestScope.partnerSize}"/>' ;j++) {
						if(document.forms[0].parnerIdList[j].checked == true) {
							partnerSelected = true;
							break;
						}
					}
				}
			
			}
			if(partnerSelected == false && '<c:out value="${requestScope.partnerSize}"/>' > 0) {
				alert('Please select Partner.');
				return false;
			}
			//alert(document.forms[0].partnerId.length)
			document.forms[0].action = "LargeDeplBulkJobCreateAction.do?ref=saveSiteSearch&form=MPOandPartner";
			document.forms[0].submit();
			return true;
		}
		function searchPartnerRecord() {
			trimFields();
			if(document.forms[0].partnerName.value == '') {
				alert('Please enter Partner Name.');
				document.forms[0].partnerName.focus();
				return false;
			}
			if(document.forms[0].keyWords.value == '') {
				document.forms[0].keyWords.focus();
				return false;
			}
			
			document.forms[0].action = "LargeDeplBulkJobCreateAction.do?form=MPOandPartner&searchPartner=Y";
			document.forms[0].submit();
			return true;
		}
		function disableCombo(partnertype) {
			if(partnertype == 'Minuteman') {
				document.forms[0].poType.value = 0;
				document.forms[0].poType.disabled=true;
			} else {
				if(document.forms[0].poType.disabled == true) {
					document.forms[0].poType.disabled = false;
				}
			}
		}
	</script>
	
	
</head>
	<body>

		<html:form action = "/LargeDeplBulkJobCreateAction" target = "ilexmain" enctype = "multipart/form-data">
		<input type="hidden" name="appendixId" value="<c:out value='${BulkJobCreationForm.appendix_Id}'/>"/>

		<c:if test="${ empty BulkJobCreationForm.jobId && empty requestScope.isNetMedX}">
			<%@ include  file="/DashboardVariables.inc" %>
			<%@ include  file="/AppedixDashboardMenu.inc" %>
			
			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr> 
			          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage</center></a></td>
			          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
			     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%=appendixid%>&ownerId=<%=ownerid%>&msaId=<%=msaid%>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
			          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
								 	<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixid }'/>"><c:out value='${requestScope.appendixName }'/></a>
									<a><span class="breadCrumb1">Large Deployment Bulk Job/PO Create - Step 2</a></div>
				        </td>
			       </tr>
		      </table>
		      <%@ include  file="/AppedixDashboardMenuScript.inc" %>
	      </c:if>
	      <c:if test="${ empty BulkJobCreationForm.jobId && not empty requestScope.isNetMedX}">
			<%@ include  file="/DashboardVariables.inc" %>
		<%@ include  file="/NetMedXDashboardMenu.inc" %>
		<!--  menu add for MASTER Purchase Order Screen -->
		<%-- <div id="menunav">
   
<ul>
  	
  		<%if(!nettype.equals("dispatch")){ %>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="javascript:createCookie('new');OpenTicket();">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('115922', 'pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=3225&amp;type=Appendix&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;fromPage=Appendix">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=3225&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=3225&amp;appendixId=3225&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=3225&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites"> View Job Sites</a></li>
        		<!-- <li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=3225&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=7">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=3225&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=7">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=3225&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=7">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;fromPage=Appendix">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=3225&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=3225">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=3225">Workflow Checklist Items</a></li>-->
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>">Search</a>
  		</li>
  		
  		<%}else{%>
  		
  		<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>">Search</a>
  		</li>
  		
  		
  		
  		
  		
  		
  		
  		<!-- <li>
        	<a class="drop" href="AppendixHeader.do?function=view&amp;viewjobtype=jobSearch&amp;fromLink=jobSearch&amp;appendixid=3225&amp;ownerId=7&amp;apiAccess=apiAccess">API Access</a>
  		</li> -->
	</ul>
</li>
</div> --%>
<div id="menunav">
<%if(!nettype.equals("dispatch")){ %>  
<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
        	
        		
				<!-- <li><a href="ReportScheduleWindow.do?report_type=redline&typeid=3970&fromPage=NetMedX">Redline Report</a></li>
				<li><a href="report_type=detailsummary&typeid=3970&fromPage=NetMedX">Detailed Summary</a></li>
				<li><a href="ReportScheduleWindow.do?report_type=CSVSummary&typeid=3970&fromPage=NetMedX">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="ReportGeneration.do?firsttime=true">Update Report</a></li></ul></li></ul></li>
        	 -->
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>">Search</a>
  		</li>
  	
	</ul>
</li>
<%}else{%>
<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        	
        	
        	
				<!-- <li><a href="ReportScheduleWindow.do?report_type=redline&typeid=3970&fromPage=NetMedX">Redline Report</a></li>
				<li><a href="report_type=detailsummary&typeid=3970&fromPage=NetMedX">Detailed Summary</a></li>
				<li><a href="ReportScheduleWindow.do?report_type=CSVSummary&typeid=3970&fromPage=NetMedX">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="ReportGeneration.do?firsttime=true">Update Report</a></li></ul></li></ul></li>
        	 -->
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>">Search</a>
  		</li>
  		</li>
	<%} %>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		   <td colspan = "7" height="1"><h2><bean:message bundle="PRM" key="prm.netmedx.metmedxdashboardformsa"/>:&nbsp;<bean:write name="NetMedXDashboardForm" property="msaname"/>&nbsp;->&nbsp;<bean:message bundle="PRM" key="prm.netmedx.netmedxappendix"/>:&nbsp;<bean:write name="NetMedXDashboardForm" property="appendixname"/></h2></td>      
	      </tbody></table>
   </td>
      
    <td width="35" valign="top"><img width="35" height="60" alt="" src="images/content_head_02.jpg" id="content_head_02"></td>
	<td width="155" valign="top">
    	<%-- 	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr>
		          <td width="155" height="39" colspan="2" class="headerrow">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tbody><tr>
			                <td width="150">
			                	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				                   	<tbody><tr>
							            <td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('II');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" title="Scheduled" id="Image10" alt="Month" src="http://localhost:8080/Ilex/images/Icon_Scheduled.gif"></a></td>
	  									<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IO');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" title="Scheduled OverDue" id="Image10" alt="Month" src="images/Icon_Schedule_Overdued.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('ION');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" title="OnSite" id="Image10" alt="Month" src="images/Icon_Onsite.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IOF');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" title="Offsite" id="Image10" alt="Month" src="images/Icon_Ofsite.gif"></a></td>
				                    </tr>
			                  	</tbody>
			                  	</table>
			                </td>
			              </tr>
			          </tbody></table>
		          </td>
		        </tr>
        		<tr>
          		<td width="140" height="21" background="images/content_head_04.jpg">
          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              		<tbody><tr>
              		<td width="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li id="closeNow" name="closeNow"><a onclick="ShowDiv();" title="Open View" href="javascript:void(0)">My Views <img border="0" onclick="ShowDiv();" title="Open View" src="images/showFilter.gif"></a><a class="imgstyle" href="javascript:void(0)"><img border="0" onclick="hideDiv();" title="Close View" src="images/offFilter2.gif"></a></li>
									</div>								
	                			</div>		
						<span>
								<div class="divstyle" id="filter" style="">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tbody><tr>
		                      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		                          <tbody><tr>
                           	 <td class="filtersCaption" colspan="3">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										
									    	 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('AI');" value="AI" name="jobSelectedStatus">   
											 	Inwork<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IS');" value="IS" name="jobSelectedStatus">   
											 	To Be Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('II');" checked="checked" value="II" name="jobSelectedStatus">   
											 	Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IO');" checked="checked" value="IO" name="jobSelectedStatus">   
											 	Scheduled Overdue<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('ION');" checked="checked" value="ION" name="jobSelectedStatus">   
											 	Onsite<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IOF');" checked="checked" value="IOF" name="jobSelectedStatus">   
											 	Offsite<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('F');" value="F" name="jobSelectedStatus">   
											 	Complete<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('O');" value="O" name="jobSelectedStatus">   
											 	Closed<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('H');" value="H" name="jobSelectedStatus">   
											 	Hold<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('C');" value="C" name="jobSelectedStatus">   
											 	Cancelled<br> 
											 	
											
												
										<img width="100px" class="imagefilter" src="images/hrfilterLine.gif">
										<table cellpadding="0">
											<tbody><tr><td class="tabNormalText"><input type="radio" value="week" name="jobMonthWeekCheck">This Week</td></tr>
											<tr><td class="tabNormalText"><input type="radio" value="month" name="jobMonthWeekCheck">This Month</td></tr>	
											<tr><td class="tabNormalText"><input type="radio" checked="checked" value="clear" name="jobMonthWeekCheck">Clear</td></tr>	
										</tbody></table>	
									</td>
	    					</tr>
                        </tbody></table></td>
                      <td><img width="1px" height="120" class="imagefilter" src="images/filterLine.gif"></td>
                      <td width="50%" valign="top">
                      
                     <!--  add code  --> 
                     <table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table> 
                     
                     <!--  End  -->
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td class="filtersCaption" colspan="3">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id="OwnerSpanId"> 
					   			
							    	
							    		
							    		
								    		
							    			 
							    		<input type="checkbox" onclick="javascript: checkingOwner('<%=ownerId%>');" checked="checked" value="7" name="jobSelectedOwners">   
										  Me
										  <br> 
									
									</span>
								
								
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img height="34" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="changeFilter();" class="divbutton" src="images/showBtn.gif"><br></a></td></tr>
                              <tr><td colspan="3"><img width="2" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="resetFilter();" class="divbutton" src="images/reset.gif"><br></a></td></tr>                              
                              <tr><td colspan="3"><img height="8" src="images/spacer.gif"></td></tr>
                              </tbody></table>
                              
                              
                              </td>
                        </tr>
  			</tbody></table>											
		</div>
	</span>
				</td>
	
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table> --%>
      </td>
 </tr>
 </tbody>
 </table>
		
		
		<!-- End  -->
		
		
		
		
			 <%-- <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<%if(!nettype.equals("dispatch")){ %>
					<td class=Ntoprow1 id=pop1 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					<%}else{%>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px">Reports</a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>&nettype=<%=nettype %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=640 colspan="2">&nbsp;</td>
					<%} %>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		  	<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										 <a ><span class="breadCrumb1">Project Notes</span></a>
								</div>		
			        </td>
		        </tr>
		         
	      </table> --%>
	      <%@ include  file="/NetMedXDashboardMenuScript.inc" %>
	      </c:if>
	      <html:hidden property = "appendix_Id"/>
	      <html:hidden property = "formatedActivityList" />
	      <html:hidden property = "paragraph_content"/>
	      <html:hidden property = "job_NewName"/>
	      <html:hidden property = "prifix"/>
	      <html:hidden property = "custRef"/>
	      <html:hidden property = "newOwner"/>
	      <html:hidden property = "newScheduler"/>

			<table class="stdPagePadding" cellpadding="0" cellspacing="0">					
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="1">	
							<tr>
								<td colspan="2" class = "message">
									<h2 style="font-size: 14px;padding-left: 0px;">Define PO and Select Partner</h2>
								</td>
							</tr>
							<tr>
								<td colspan="1" class = "labeleboldhierrarchynowrap">Deliver By Date:</td>
								<td colspan="1"><input type="text"  class="textbox" size="10" name="deliverDate" value="<c:out value='${BulkJobCreationForm.deliverDate}'/>" readonly="true"/>
								<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].deliverDate, document.forms[0].deliverDate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;" /></td>
							</tr>	
							<tr>
								<td colspan="1" class = "labeleboldhierrarchynowrap">Issue Date:</td>
								<td colspan="1">
									<input type="text"  class="textbox" size="10" name="scheduleDate" value="<c:out value='${BulkJobCreationForm.scheduleDate}'/>" readonly="true" />
								</td>
							</tr>
							<tr>
								<td colspan="2" class = "message">
									<h2 style="font-size: 14px;padding-left: 0px;">Select Master Purchase Order</h2>
									</td>
							</tr>
							<tr>
								<td colspan=2>
									<table>
										<tr>
											<td  class = "texthdNoBorder" colspan="1"></td>
											<td  class = "texthdNoBorder" colspan="1"><b>Master Purchase Orders</td>
											<td class = "texthdNoBorder" colspan="1"><b>Master PO Item</td>
											<td class = "texthdNoBorder" colspan="1"><b>Estimated Total Cost</td>
											<td class = "texthdNoBorder" colspan="1"><b>Status</td>
										</tr>
										<%
											String backgroundclass = null;
											String masterItemName=null;
											boolean csschooser = true;
											
											int i = 0;
										%>
										<logic:iterate id = "ms" name = "BulkJobCreationForm" property = "mpoList">
										<bean:define id = "valuMPO" name = "ms" property = "mpoId" type = "java.lang.String"/>
										<bean:define id = "itemname" name = "ms" property = "masterPOItem" type = "java.lang.String"/>
										<tr>
											<c:set var="mpoItemName" value=""/>
											<%	
												if ( csschooser == true ) 
												{
													backgroundclass = "Ntexteleftalignnowrap";
													csschooser = false;
													
												}
										
												else
												{
													csschooser = true;	
													backgroundclass = "Ntextoleftalignnowrap";
													
													
												}
											if(itemname.equals("fp")) {
												masterItemName = "Fixed Price";
											}
											if(itemname.equals("ht")) {
												masterItemName = "Hourly Type";
											}
											if(itemname.equals("mt")) {
												masterItemName = "Materials";
											}
											if(itemname.equals("si")) {
												masterItemName = "Show Items";
											}
											%>
											
											<td class="<%=backgroundclass %>" colspan="1"><html:radio  property="mpoIdSelected" styleId = "<%=valuMPO%>" value="<%=valuMPO %>"/></td>
											<td  class="<%=backgroundclass %>" colspan="1"><bean:write name = "ms" property = "mpoName" /></td>
											<td class="<%=backgroundclass %>" colspan="1"><%=masterItemName %></td>
											<td class="<%=backgroundclass %>" colspan="1"><bean:write name = "ms" property = "estimatedTotalCost" /></td>
											<td class="<%=backgroundclass %>" colspan="1"><bean:write name = "ms" property = "status" /></td>
										</tr>
										</logic:iterate>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan=2 colspan="2" class = "message">
									<h2 style="font-size: 14px;padding-left: 0px;">Search and Select Partner
								</td>
							</tr>
							<tr>
								<td class = "labeleboldhierrarchynowrap">Partner:</td>
								<td>
									<html:text property="partnerName" size="20" styleClass="textbox"></html:text>
								</td>
							</tr>
							<tr>
								<td class = "labeleboldhierrarchynowrap">Key Words:</td>
								<td>
									<html:text property="keyWords" size="20" styleClass="textbox"></html:text>&nbsp;&nbsp;<html:button property="searchPartner" styleClass="button_c" onclick="searchPartnerRecord();">Search</html:button>
								</td>
							</tr>
							<tr>
								<td colspan =2>
									<table>
									<c:if  test="${requestScope.partnerSize eq 0}">
										<tr>
											<td colspan="5" class = "message">
												No Records Found.
											</td>
										</tr> 
									</c:if>
										<c:forEach items="${BulkJobCreationForm.partnerList}" var="partnerList" varStatus="index">
											<tr>
												<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> colspan="1"><input type="radio" name="parnerIdList" class="radio"  value="<c:out value='${partnerList.partnerId}'/>" onclick=" disableCombo('<c:out value='${partnerList.partnerType}'/>');"> </td>
												<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> colspan="1"><c:out value='${partnerList.namePartner}'/></td>
												<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> colspan="1"><c:out value='${partnerList.partnerCity}'/>,
												 <c:out value='${partnerList.partnerState}'/></td>
												<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> colspan="1"><c:out value='${partnerList.pocFirstName}'/>
												<c:out value='${partnerList.pocLastName}'/></td>
											</tr>
										</c:forEach>
									</table>
								</td>
							</tr>
							<tr>
							<td class = "labeleboldhierrarchynowrap">PO Type:</td>
							<td>
								<html:select property = "poType" size = "1" styleClass="combowhite">
									<html:optionsCollection property = "poTypeList" value = "value" label = "label"/>    
								</html:select>
							</td>
							</tr>
							<tr><td colspan=2><table>
								<tr>
									<td><html:button property="craeteJobs" styleClass="button_c" onclick="createJObsWithMPO();">Create Jobs with PO and Partners</html:button></td>
									<td><html:reset property="cancel" styleClass="button_c">Cancel</html:reset></td>
								</tr>
							</table></td></tr>
					
													
							</table>
						</td>
					</tr>
			</table>
			<c:if test = "${requestScope.jobType eq 'Addendum'}">
				<%@ include  file="/AddendumMenuScript.inc" %>
			</c:if>
				<c:if test = "${requestScope.jobType eq 'Default'}">
				<%@ include  file="/DefaultJobMenuScript.inc" %>
			</c:if>
			<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
				<%@ include  file="/ProjectJobMenuScript.inc" %>
			</c:if>
		</html:form>
	</body>
</html>