<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id="jid" name="jobid" />
<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

<table  border = "0" cellspacing = "1" cellpadding = "1" >
 <tr>
  <td><img src = "images/spacer.gif" width = "2" height = "0"></td>
  <td><table border = "0" cellspacing = "1" cellpadding = "1" >
	<logic:present name="activitylist">
	<tr> 
	    
		
	    <td class="tryb" rowspan="2" ><bean:message bundle = "PRM" key = "prm.activity"/></b></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.type"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.quantity"/></td>
		<td class="tryb" colspan="6"><bean:message bundle = "PRM" key = "prm.estimatedcost"/></td>
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.extendedprice"/></td>
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.proformamargin"/></td>
		<td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.estimatedschedule"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.status"/></b></td>
	  </tr>
	<tr> 
		 <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.materials"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.cnslabor"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.contractlabor"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.freight"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.travel"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.total"/></div>
	   
		<td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.start"/></b></div>
	    </td>
		<td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.complete"/></b></div>
	    </td>
	</tr>
	
	<%int i=1;
	String bgcolor=""; %>
	
		<logic:iterate id="alist" name="activitylist" >
		<%if((i%2)==0)
		{
			bgcolor="texte"; 
		
		} 
		else
		{
			bgcolor="texto";
		}%>
	
	<tr> 
	   
	    <td  class="<%=bgcolor %>"><a href="JobWorkArea.do?function=view&jobid=<bean:write name="alist" property="jobid"/>&activityid=<bean:write name="alist" property="activityid"/>" target="ilexmain"><bean:write name="alist" property="activityname"/></a></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="type"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="quantity"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="materials"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="cnslabor"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="contractlabor"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="freight"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="travel"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="total"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="extendedprice"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="pmargin"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="estart"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="ecomplete"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="status"/></td>
	</tr>
	
			
	<%i++; %>
		</logic:iterate>
  
	<tr class = "buttonrow"> 
	    <td colspan="14" height="30"> 
		  <html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "pm" key = "msa.tabular.sort"/></html:button> 
	    </td>
	</tr> 
	</logic:present> 
	<tr  align="right"> 
	    <td colspan="16" height="37"><br>
	       <a href="#" class="pagelink"><bean:message bundle = "PRM" key = "prm.logout"/></a>|<a href="#" class="pagelink"> <bean:message bundle = "PRM" key = "prm.help"/></a> 
	    </td>
	</tr>
	  
   </table>
  </td>
 </tr>
</table>



</body>
<script>
var str = '';

function sortwindow()
{
	str = 'SortAction.do?Type=prj_activity&jobid=<%=jid%>';
	
//	p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	return true;
	
}
</script>

</html:html>
