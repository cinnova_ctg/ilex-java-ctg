<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<LINK href="styles/content.css" rel="stylesheet" type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%-- <%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>  --%>
<script language="JavaScript" src="javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script type='text/javascript'
	src='./dwr/interface/JobCompleteAction.js'></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<script>
	function getAjaxRequestObjectInstallNotes() {
		var ajaxRequest; // The variable that makes Ajax possible!
		try {
			// Opera 8.0+, Firefox, Safari
			ajaxRequest = new XMLHttpRequest();
		} catch (e) {
			// Internet Explorer Browsers
			try {
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					// Something went wrong
					alert("Your browser broke!");
					return false;
				}
			}
		}
		return ajaxRequest;
	}

	function addCommentsForInstallationNotes() {
		/*
		
		//alert(document.all.installationNotes.value);
		//alert(encodeURI(document.all.installationNotes.value));
		installationNotes = escape(encodeURI(installationNotes));
		
		
		 */						//(CKEDITOR.instances.installationNotes.getData());

		 var installationNotes = document.all.installationNotes.value;
		 installationNotes=installationNotes.trim();
		 if(installationNotes ==''){
			alert("Add installation notes.");
			return false;
		}
			
		

	//	var isSendEmail = window.confirm("Do you wish to notify the customer?");
		
		var installationNotes = escape(encodeURI(document.all.installationNotes.value));
		var ajaxRequest = getAjaxRequestObjectInstallNotes();
		ajaxRequest.onreadystatechange = function() {
			if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
				var xmlDoc = ajaxRequest.responseXML;
				
				var id;
				id = xmlDoc.getElementsByTagName("installNotesInfo")[0];
				var latestInstallNotesDate = id.getElementsByTagName("latestInstallNotesDate")[0].firstChild.nodeValue;
				var latestInstallNotes = id.getElementsByTagName("latestInstallNotes")[0].firstChild.nodeValue;
				var latestInstallNoteType = id.getElementsByTagName("latestInstallNotesType")[0].firstChild.nodeValue;

				var secondLatestInstallNotesDate = id.getElementsByTagName("secondLatestInstallNotesDate")[0].firstChild.nodeValue;
				var secondLatestInstallNotes = id.getElementsByTagName("secondLatestInstallNotes")[0].firstChild.nodeValue;
				var secondLatestInstallNoteType = id.getElementsByTagName("secondLatestInstallNotesType")[0].firstChild.nodeValue;
				var installCount = id.getElementsByTagName("installNotesCount")[0].firstChild.nodeValue;

				document.getElementById("latestInstallNotesDate").innerHTML = "";
				document.getElementById("latestInstallNotes").innerHTML = "";
				document.getElementById("secondLatestInstallNotesDate").innerHTML = "";
				document.getElementById("secondLatestInstallNotes").innerHTML = "";
				document.getElementById("installNotesCountId").innerHTML = "";
				
				if(latestInstallNoteType != "0"){
					latestInstallNotesDate = "<b>" + latestInstallNotesDate + "</b>";
				}

				document.getElementById("latestInstallNotesDate").innerHTML = latestInstallNotesDate;
				document.getElementById("latestInstallNotes").innerHTML = latestInstallNotes;
				
				if(secondLatestInstallNoteType != "0"){
					secondLatestInstallNotesDate = "<b>" + secondLatestInstallNotesDate + "</b>";				
				}
				
				document.getElementById("installNotesCountId").innerHTML = installCount;
				document.getElementById("secondLatestInstallNotesDate").innerHTML = secondLatestInstallNotesDate;
				document.getElementById("secondLatestInstallNotes").innerHTML = secondLatestInstallNotes;
				document.all.installationNotes.value = "";
				//CKEDITOR.instances.installationNotes.setData("");				
			}
		}
		var instalationTypeId = 0;

		if (document.getElementById("changeOrderReq").checked) {
			instalationTypeId = 1;
			ChangeOrderAction(installationNotes);
		} else if (document.getElementById("changeOrderResp").checked) {
			//document.forms[0].changeOrderReq.checked = false;			
			instalationTypeId = 2;
			document.getElementById("changeOrderResp").checked = false;
		} else {
			instalationTypeId = 0;
		}

		var url = "JobDBCompleteAction.do";
		var params = "hmode=addComments&jobId=<c:out value='${JobSetUpForm.jobId}'/>&ticketId=<c:out value='${JobSetUpForm.ticket_id}'/>&requestorEmail=<c:out value='${JobSetUpForm.requestorEmail}'/>&installationNotes="
				+ installationNotes + "&instalationTypeId=" + instalationTypeId;

		ajaxRequest.open("POST", url, true);
		ajaxRequest.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
		ajaxRequest.send(params);

	}

	function ChangeOrderAction(installationNotes) {

		/*if (document.forms[0].installationNotes.value == "") {
			document.forms[0].installationNotes.focus();
			alert("Please give order change details.");
			return false;
		}*/		
		var url;
		
		//url = "JobExecuteAction.do?hmode=unspecified&type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=1&saveJobActions=1";							
		url = "ChangeOrderAction.do?hmode=unspecified&type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&installNote="+ unescape(installationNotes);
		document.forms[0].action = url;
		document.forms[0].submit();
	}

	function changeOrderReqChkBoxStatus() {
		if (document.getElementById("changeOrderReq").checked) {
			document.getElementById("changeOrderResp").checked = false;
		}
	}

	function changeOrderRespChkBoxStatus() {
		if (document.getElementById("changeOrderResp").checked) {
			document.getElementById("changeOrderReq").checked = false;
		}
	}
</script>

<%
	response.setHeader("Pragma", "No-Cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setDateHeader("Expires", 0);
%>
<title>Ilex</title>
</head>
<body>
	<table cellpadding="0" cellspacing="0" border="0" style="margin-left: 0px; margin-top: 10px;" width="40">
		<tr>
			<td style="padding-left: 0px; padding-top: 0px; padding-bottom: 3px;"
				class="dbvaluesmallFontBold" valign="top">Installation
				Notes&nbsp;&nbsp;&nbsp;<span class="dbvalue">(<a
					href="ViewInstallationNotes.do?function=view&from=jobdashboard&from=Jobdashboard&jobid=<c:out value='${JobSetUpForm.jobId}'/>&appendixid=<c:out value='${JobSetUpForm.appendixId}'/>"><span id="installNotesCountId"><c:out value='${JobSetUpForm.installNoteCount}'/></span></a>)
			</span>
			</td>
		</tr>
		<tr>
			<td>
				<table class="Ntextoleftalignnowrappaleyellowborder2"
					cellspacing="2" cellpadding="2">
					<tr>
						<td valign="top" colspan="2">
						<!--<ckeditor:editor basePath="resources/lib/ckeditor/" editor="installationNotes" />-->
 						<textarea 
 								name="installationNotes" id="installationNotes" cols="90" rows="6" class="textarea"></textarea> 
						</td>
					</tr>
					<tr align="right">
						<td>
							<!-- <input type="button" name="Change_Order" value="Change Order" class="button_c1" onclick="ChangeOrderAction();"/> -->
							<input type="checkbox" name="changeOrderReq"
							title="Change Order" id="changeOrderReq" value="changeOrderReq"
							onclick="changeOrderReqChkBoxStatus()" />Change Order &nbsp;&nbsp;&nbsp; <input
							type="checkbox" name="changeOrderResp"
							title="Change Order Response" id="changeOrderResp"
							value="changeOrderResp" onclick="changeOrderRespChkBoxStatus()" />Change Order Response &nbsp;
							<input type="button" name="Add" value="Add"
							onclick="addCommentsForInstallationNotes();" class="button_c1" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" style="margin-left: 0px; margin-top: 10px;" width="500">
		<tr>
			<td class="dbvaluesmallFontBold" style="padding-left: 2px;" valign="top">Recent Notes&nbsp;</td>
		</tr>
		<tr valign="bottom">
			<td align="right" cellspacing="0" cellpadding="0">
				<table width="100%" border="1" style="border-width: 1px; border-collapse: collapse;" bordercolor="#D9DFEF" class="Ntextoleftalignnowrapblue" cellspacing="0" cellpadding="0">
					<tr>
						<td width="25%" valign="top" align="center" id="latestInstallNotesDate">
							<c:if test="${JobSetUpForm.latestInstallNotesType ne '0'}" >
								<b>
							</c:if>		
							<c:out value='${JobSetUpForm.latestInstallNotesDate}' />
							<c:if test="${JobSetUpForm.latestInstallNotesType ne '0'}" >
								</b>
							</c:if>		
						</td>
						<td width="75%" valign="top" align="left" id="latestInstallNotes" style="white-space: normal;"><c:out value='${JobSetUpForm.latestInstallNotes}' escapeXml="false" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr valign="top">
			<td cellspacing="0" cellpadding="0">
				<table width="100%" border="1"
					style="border-width: 1px; border-collapse: collapse;"
					bordercolor="#D9DFEF" class="Ntexteleftalignnowrapnoback"
					cellspacing="0" cellpadding="0">
					<tr>
						<td width="25%" align="center" valign="top"
							id="secondLatestInstallNotesDate"><c:if
								test="${JobSetUpForm.secondLatestInstallNotesType ne '0'}">
								<b>
							</c:if> 
							<c:out value='${JobSetUpForm.secondLatestInstallNotesDate}' />
							<c:if test="${JobSetUpForm.secondLatestInstallNotesType ne '0'}">
								</b>
							</c:if></td>
						<td width="75%" align="left" valign="top"
							id="secondLatestInstallNotes"
							style="word-wrap: normal; word-break: normal; white-space: normal;"><c:out
								value='${JobSetUpForm.secondLatestInstallNotes}'
								escapeXml="false" /></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
<%-- 	<ckeditor:replace replace="installationNotes" basePath="ckeditor/"  /> --%>

</body>
</html>
