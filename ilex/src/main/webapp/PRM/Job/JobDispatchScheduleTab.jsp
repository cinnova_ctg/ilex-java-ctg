<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<link rel = "stylesheet" href="styles/content.css" type="text/css">

<script language = "JavaScript" src = "PRM/Job/js/schedule.js"></script>

<script>
    // API Settings
    var api_url = '<%= (String) request.getAttribute("api_url") %>';
    var api_user = '<%= (String) request.getAttribute("api_user") %>';
    var api_pass = '<%= (String) request.getAttribute("api_pass") %>';
    
    var job_id = '<c:out value='${JobSetUpForm.jobId}'/>';
    
    var user_id = '<c:out value='${sessionScope.userid}'/>'; 
</script>
<table class="Ntextoleftalignnowrappalenoborder" border="0" width="940">
    <tr>
        <td colspan="2">
            <div id="schedule_element_area" style="padding: 10px;"></div>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td> 
            <table cellpadding="0" cellspacing="1" border="0" width="100%">
                <tr>
                    <td>
                        <button id="save_schedules" class="button_c">Save</button>
                        <button id="reset_schedules" class="button_c">Reset</button>
                    </td>
                    <td align="right">
                        <button id="add_dispatch" class="button_c">Add Dispatch</button>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>