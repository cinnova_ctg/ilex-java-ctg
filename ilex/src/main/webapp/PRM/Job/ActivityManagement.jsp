<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<% 
	String backgroundclass = null; //- td style class
	boolean csschooser = true;	
	String backgroundclas = null; //- td style class
	boolean csschoosers = true;// - for alternet change color of rows
%>
<head>
	<script>

		function callActivity(val){

			var appendixType = escape("<c:out value='${JobSetUpForm.appendixType}'/>");
			var currentOwner =escape( "<c:out value='${JobSetUpForm.createdBy}'/>");
			
			var url="";
			if(val=='add_act'){
				url="CopyAddendumActivityAction.do?tabId=<c:out value="${JobSetUpForm.tabId}"/>&Appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&from=PRM&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>";
			}if(val=='uplift'){
				url="ManageUpliftAction.do?tabId=<c:out value="${JobSetUpForm.tabId}"/>&ref=activityUplift&appendixType="+appendixType+"&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&Appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>";
			}if(val=='manage_act'){	
				url="ActivityUpdateAction.do?tabId=<c:out value="${JobSetUpForm.tabId}"/>&ref=inscopeactivity&from=jobdashboard&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>";
			}/*if(val=='out_act')	{
				url="EditOOSActivityAction.do?Activity_Id=<c:out value='${JobSetUpForm.activity_Id}'/>&jobId=<c:out value='${JobSetUpForm.jobId}'/>&Appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>";
			}*/
			if(val=='commissions'){	
				url="ESAActivityCommissionAction.do?ref=unspecified&jobRef=inscopejob&appendixId=<c:out value='${JobSetUpForm.appendixId}'/>&jobId=<c:out value='${JobSetUpForm.jobId}'/>";
			}
			
				document.forms[0].action = url;
				document.forms[0].submit();
			}

		function onSubmit(){
			document.forms[0].optactivity.value="Optactivity";
			var url="JobSetUpAction.do?hmode=addOutOfScope&type=1&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=&ticket_id=&isClicked=1";
			document.forms[0].action=url;
			document.forms[0].submit();
		}

	</script>
</head>

	<table width="100%" >
	<html:hidden property="optactivity" value=""/>
		<tr>
			<td   nowrap><h2 style="font-size: 14px;padding-left: 0px;;padding-top: 2px;padding-bottom: 0px;">Activity Management</h2></td>
		</tr>
		<tr valign="top">
			<td>
				<table cellspacing="8" cellpadding="0"  width="100%" class="Ntextoleftalignnowrappaleyellowborder2" border="0">	
					<tr>
						<td valign="top">
							<table cellpadding=0 cellspacing=1 border="0" > 
								<tr>
									<td class = "texthdNoBorder" rowspan = "2" colspan="2">
										<table border="0" width="100%">
											<tr>
												<td class = "texthdNoBorder"><b><bean:message bundle = "pm" key = "activity.tabular.name"/></b></td>
												<td align="right">&nbsp;</td>
											</tr>
										</table>
									</td>
								   	<c:if test="${requestScope.tabStatus eq 'job'}">
								   	<td class = "texthdNoBorder"  rowspan = "2" width="9%"><b>Scope</td>
								   	</c:if>
								   	<td class = "texthdNoBorder" rowspan = "2" width="9%"><b>Uplifts </td>
								   	<td class = "texthdNoBorder" rowspan = "2" width="6%"><b>Qty </td>
								   	<td class = "texthdNoBorder" colspan = "2" width="18%"><b>Estimated Cost </td>
								   	<td class = "texthdNoBorder" colspan = "2" width="18%"><b>Extended Price </td>
								</tr>
								<tr> 
								    <td width=57 class = "texthdNoBorder"><div><b>Unit</div></td>
								    <td width=57 class = "texthdNoBorder"><div><b>Total</div></td>
								    <td width=57 class = "texthdNoBorder"><div><b>Unit</div></td>
									<td width=57 class = "texthdNoBorder"><div><b>Total</div></td>
								</tr>
								<c:if test="${not empty JobSetUpForm.activity_Id}">
								<c:forEach items="${JobSetUpForm.activity_Id}" var="activityList" varStatus="status" >
								<%
									if ( csschoosers == true ){
										backgroundclas = "Nhyperevenfont";
										csschoosers = false;
									}else{
										csschoosers = true;	
										backgroundclas = "Nhyperoddfont";
									}
								%>
								<tr>
									<td class="<%=backgroundclas %>">
									<c:if test="${JobSetUpForm.activity_lx_ce_type[status.index] eq 'A'}"><c:set var="ref" value="detailactivity"/></c:if>
									<c:if test="${JobSetUpForm.activity_lx_ce_type[status.index] eq 'IA'}"><c:set var="ref" value="Viewinscope"/></c:if>
									<c:if test="${JobSetUpForm.activity_lx_ce_type[status.index] eq 'AA'}"><c:set var="ref" value="detailaddendumactivity"/></c:if>
									<c:if test="${JobSetUpForm.activity_lx_ce_type[status.index] eq 'CA'}"><c:set var="ref" value="detailchangeorderactivity"/></c:if>
									<c:choose>
										<c:when test = "${JobSetUpForm.activityType[status.index] eq 'V' && JobSetUpForm.extendedTotalCost[status.index] eq '0.00'}">
											<c:out value='${JobSetUpForm.activityName[status.index]}'/>
										</c:when>
										<c:when test = "${JobSetUpForm.activityType[status.index] ne 'V'}">
											<c:choose>
												<c:when test="${requestScope.tabStatus eq 'Default' && requestScope.isTicket eq 'Ticket'}">
													<a href ="ResourceListAction.do?resourceListType=A&ref=detailactivity&type=PM&from=jobdashboard&Activitylibrary_Id=<c:out value='${JobSetUpForm.activity_Id[status.index]}'/>&Activity_Id=<c:out value='${JobSetUpForm.activity_Id[status.index]}'/>">
												</c:when>
												<c:otherwise>
													<a href ="ActivityDetailAction.do?ref=inscopedetailactivity&type=PM&from=jobdashboard&addendum_id=&Activitylibrary_Id=<c:out value='${JobSetUpForm.activity_Id[status.index]}'/>&Activity_Id=<c:out value='${JobSetUpForm.activity_Id[status.index]}'/>">
												</c:otherwise>
											</c:choose>
											<c:out value='${JobSetUpForm.activityName[status.index]}'/></a>
										</c:when>
									</c:choose>
									
									&nbsp;<c:if test="${JobSetUpForm.activity_oos_flag[status.index] eq 'Y'}">
											<c:choose>
												<c:when test="${JobSetUpForm.jobStatus eq 'Closed'}">Edit</c:when>
												<c:otherwise><a href="EditOOSActivityAction.do?Activity_Id=<c:out value='${JobSetUpForm.activity_Id[status.index]}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&Appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>">Edit</a>
												</c:otherwise>
											</c:choose>
										</c:if>

									<%-- For Default Job --%>
									</td>
									<td class="<%=backgroundclas %>">	
										<c:choose>
											<c:when test = "${JobSetUpForm.activityType[status.index] eq 'V' && JobSetUpForm.extendedTotalCost[status.index] eq '0.00'}">
												[Resources]
											</c:when>
											<c:when test = "${JobSetUpForm.activityType[status.index] ne 'V'}">
												<c:if test="${not(requestScope.tabStatus eq 'Default' && requestScope.isTicket eq 'Ticket')}">
													[<a href ="ResourceListAction.do?resourceListType=A&Activity_Id=<c:out value='${JobSetUpForm.activity_Id[status.index]}'/>&ref=inscopedetailactivity&from=jobdashboard">Resources</a>]
												</c:if>
											</c:when>
										</c:choose>
									</td>
									<c:if test="${requestScope.tabStatus eq 'job'}">
								   	<td class = "<%=backgroundclas %>" align="center"><c:out value='${JobSetUpForm.scope[status.index]}'/></td>
								   	</c:if>
								   	<td class = "<%=backgroundclas %>" align="right"><fmt:formatNumber value='${JobSetUpForm.uplifts[status.index]}' type='currency' pattern='$#,###,##0.00'/></td>
								   	<td class = "<%=backgroundclas %>" align="right"><c:out value='${JobSetUpForm.quantity[status.index]}'/></td>
								   	<td class = "<%=backgroundclas %>" align="right"><fmt:formatNumber value='${JobSetUpForm.estimatedUnitcost[status.index]}' type='currency' pattern='$#,###,##0.00'/></td>
								   	<td class = "<%=backgroundclas %>" align="right"><fmt:formatNumber value='${JobSetUpForm.estimatedTotalCost[status.index]}' type='currency' pattern='$#,###,##0.00'/></td>
								   	<td class = "<%=backgroundclas %>" align="right"><fmt:formatNumber value='${JobSetUpForm.extendedUnitcost[status.index]}' type='currency' pattern='$#,###,##0.00'/></td>
								   	<td class = "<%=backgroundclas %>" align="right"><fmt:formatNumber value='${JobSetUpForm.extendedTotalCost[status.index]}' type='currency' pattern='$#,###,##0.00'/></td>
								</tr>
								</c:forEach>
								
								<tr >
									<c:if test="${requestScope.tabStatus eq 'job'}">
								   	<td colspan="5" class="dbvalue" align="right"><b>Totals:</b></td>
								   	</c:if>
								   	<c:if test="${requestScope.tabStatus eq 'Default' || requestScope.tabStatus eq 'Addendum'}">
									   	<td colspan="4" class="dbvalue" align="right"><b>Totals:</b></td>
								   	</c:if>
								   	<%--<td class = "dbtopbordertd" align="right"><c:out value='${JobSetUpForm.totalQuantity}'/></td>--%>
								   	<td class = "dbtopbordertd" align="right"><fmt:formatNumber value='${JobSetUpForm.totalEstimatedUnitCost}' type='currency' pattern='$#,###,##0.00'/> </td>
								   	<td class = "dbtopbordertd" align="right"><fmt:formatNumber value='${JobSetUpForm.totalEstimatedCost}' type='currency' pattern='$#,###,##0.00'/> </td>
								   	<td class = "dbtopbordertd" align="right"><fmt:formatNumber value='${JobSetUpForm.totalExtendedUnitPrice}' type='currency' pattern='$#,###,##0.00'/></td>
								   	<td class = "dbtopbordertd" align="right"><fmt:formatNumber value='${JobSetUpForm.totalExtendedPrice}' type='currency' pattern='$#,###,##0.00'/></td>
								</tr>
								</c:if>
								<c:if test="${empty JobSetUpForm.activity_Id}">
								<tr>
									<td colspan="9" class="Nhyperevenfont">No Activities</td> 
								</tr>
								</c:if>
							</table>
						</td>
						<td align="center" valign="top">
							<table cellpadding=0 cellspacing=1 border="0" >
							<c:if test="${requestScope.tabStatus ne 'Default' && requestScope.isTicket ne 'Ticket'}">
								
								<c:choose>
									<c:when test ="${requestScope.tabStatus ne 'Addendum' || ( requestScope.tabStatus eq 'Addendum' && JobSetUpForm.isAddendumApproved ne 'Y')}">
										<tr valign="top">
											<td  align="center" style="padding: 2px;">
								    	<%-- 	<input type="button" style="width: 110;"  name="Add" value="Add Activities" onclick="callActivity('add_act');" class="button_c2"/>
								    	--%>
								    			<input type="button" style="width: 110;" name="Add" value="Add Activities" <c:if test="${JobSetUpForm.jobStatus eq 'Closed'}">disabled="disabled"</c:if> onclick="callActivity('add_act');" class="button_c2"/>
								    		</td>
										</tr>
									</c:when>
								</c:choose>
							
							</c:if>	
									<tr valign="top">
										<td align="center"  style="padding: 2px;" >
											<input type="button" style="width: 110;" name="manage" value="Manage Activities" <c:if test="${JobSetUpForm.jobStatus eq 'Closed'}">disabled="disabled"</c:if> onclick="callActivity('manage_act');" class="button_c2"/>								
								    	</td>
									</tr>
								<c:if test="${requestScope.tabStatus ne 'Default' && requestScope.tabStatus ne 'Addendum'}">
									<tr valign="top">
										<td  align="center" style="padding: 2px;" >
								    		<input type="button" style="width: 110;" name="Uplifts" value="Uplifts" <c:if test="${JobSetUpForm.jobStatus eq 'Closed'}">disabled="disabled"</c:if> onclick="callActivity('uplift');" class="button_c2"/>
								    	</td>
									</tr>
									<c:if test="${requestScope.isTicket ne 'Ticket'}">
										<tr valign="top"  style="padding: 2px;" >
											<td  align="center" >
								    			<input type="button" style="width: 110;" name="outofscope" value="Add Out of Scope" <c:if test="${JobSetUpForm.jobStatus eq 'Closed'}">disabled="disabled"</c:if> onclick="onSubmit();" class="button_c2"/>
								    		</td>
										</tr>
									</c:if>	
								</c:if>
								<tr valign="top">
									<td align="center" style="padding: 2px;">
										<c:choose>
											<c:when test="${not empty requestScope.countESA and requestScope.countESA > 0}" >
												<input type="button" style="width: 110;" name="commissions" value="Commissions" <c:if test="${((JobSetUpForm.jobStatus eq 'Closed' or JobSetUpForm.jobStatus eq 'Cancelled' or JobSetUpForm.jobStatus eq 'Complete' or (not empty JobSetUpForm.jobIndicator and JobSetUpForm.jobIndicator ne 'None')) and requestScope.tabStatus eq 'job') or (requestScope.tabStatus eq 'Addendum' and JobSetUpForm.addendumStatus eq 'Cancelled')}">disabled="disabled"</c:if> onclick="callActivity('commissions');" class="button_c2" />
											</c:when>
											<c:otherwise>
												<input type="button" style="width: 110;" name="commissions" value="Commissions" disabled="disabled" class="button_c2" />
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	 