<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id="type" name="type" />
<%
String jobid = "";
String appendixid = "";
String partner_id = "0";
int addendumlistlength=0;

if( request.getParameter( "jobid" ) != null )
	jobid = request.getParameter( "jobid" );
if(request.getAttribute("appendixid")!=null)
	appendixid=request.getAttribute("appendixid").toString();
if(request.getAttribute("partner_id")!=null)
	partner_id=request.getAttribute("partner_id").toString();

addendumlistlength = ( int ) Integer.parseInt(request.getAttribute("addendumlistlength").toString());

%>
<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

</head>


<script>

function addaddendum(action, para)
{
	document.forms[0].action = "AddAddendumAction.do?type=job&functionPara="+action+"&parameter="+para+"&jobid=<%= jobid %>";
	document.forms[0].target = "ilexmain";
	document.forms[0].submit();
	return true;	
}


function managestatus(v)
{
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_job&id=<%= jobid %>";
	document.forms[0].submit();
	return true;	
}

function sendstatusreport()
{
	document.forms[0].action = "EmailAction.do?Type=prm_job&Type1=prm_job&id=<%= jobid %>";
	document.forms[0].submit();
	return true;
}


function upload()
{
	document.forms[0].action = "UploadAction.do?ref=FromJobDashboard&ref1=view&Id=<%= jobid %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].action = 'ViewuploadedocumentAction.do?ref=Viewjobdashboarddocument&ref1=view&Id=<%= jobid %>';
	document.forms[0].submit();
	return true;
}
</script>

<%@ include  file="/Menu.inc" %>



<html:form action = "JobHeader">

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  class="toprow"> 
      <table  width="100%" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr> 
          <td id="pop1" width="100" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);popUp('elMenu1',event)" class="drop"><bean:message bundle="PRM" key="prm.managejob"/></a></td>
          <td id="pop2" width="110" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" onmouseover="MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class="drop"><bean:message bundle="PRM" key="prm.managepartners"/></a></td>
          <td id="pop3" width="105" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu3',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu3',event)" class="drop"><bean:message bundle="PRM" key="prm.managesite"/></a></td>
          <td id="pop4" width="165" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu4',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu4',event)" class="drop"><bean:message bundle="PRM" key="prm.managestatusreport"/></a></td>
          <td id="pop5" width="100" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu5',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu5',event)" class="drop"><bean:message bundle="PRM" key="prm.viewreports"/></a></td>
		  <td id="pop6" width="119" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu6',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu6',event)" class="drop"><bean:message bundle="PRM" key="prm.manageaddendum"/></a></td>
		  <% if( request.getAttribute("jobtype").equals( "Default" )) {%>
			<td  width="100" class="toprow1"><a href="#"  onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.newactivity"/></a></td>
 		 <%}
		 else{%>
		  <td  width="100" class="toprow1"><a href="CopyActivityAction.do?Job_Id=<%= jobid %>&from=inscopejob&ref=view"  onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.newactivity"/></a></td>
		 <%} %>
		  <td  id="pop7" width="100" class="toprow1"><a href="#"    onmouseout="MM_swapImgRestore(); popDown('elMenu7',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu7',event)" class="drop"><bean:message bundle="PRM" key="prm.goto"/></a></td>
		</tr>
      </table>
    </td>
  </tr>
</table>

<SCRIPT language=JavaScript1.2>

if (isMenu) {
 arMenu1=new Array(
110,
findPosX('pop1'),findPosY('pop1'),
"","",
"","",
"","",
<% if( request.getAttribute("jobtype").equals( "Default" )) {%>
	"<bean:message bundle = "PRM" key = "prm.changestatus"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotchanestatusfordefault"/>')",0,
	"<bean:message bundle = "PRM" key = "prm.updateschedule"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.CannotupdateJobschedulefordefault"/>')",0,

<%}
else{%>

	<% if( ( String )request.getAttribute( "list" ) != "" ) {%>
	"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",1,
	<%} else {%>
	"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",0,
	<%}%>
	<% if( request.getAttribute("jobcurrentstatus").equals( "F" )) {%>
	"<bean:message bundle = "PRM" key = "prm.updateschedule"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.CannotupdateJobschedule"/>')",0,
	<%} else { %>
	"<bean:message bundle = "PRM" key = "prm.updateschedule"/>","ScheduleUpdate.do?function=update&typeid=<%=jobid%>&type=<%=type%>",0,
	<% } %>
<% } %>
"<bean:message bundle = "PRM" key = "prm.job.manage.document"/>","#",1
)

<% if( ( String )request.getAttribute( "list" ) != "" ) {%>
arMenu1_1=new Array(
<%= ( String ) request.getAttribute( "list" ) %>
)
<%} %>

arMenu1_3 = new Array(
"<bean:message bundle = "PRM" key = "prm.job.manage.document.view"/>","javascript:viewdocuments();",0,
"<bean:message bundle = "PRM" key = "prm.job.manage.document.upload"/>","javascript:upload();",0
)

arMenu2=new Array(
130,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
<% if( request.getAttribute("jobtype").equals( "Default" )) {%>
	"<bean:message bundle = "PRM" key = "prm.assignpartner"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotassignpartnerfordefault"/>')",0,
	"<bean:message bundle = "PRM" key = "prm.unassignpartner"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotunassignpartnerfordefault"/>')",0,
	"<bean:message bundle = "PRM" key = "prm.addponumber"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotaddponofordefault"/>')",0,
	"Add Partner Cost","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotaddpartnercostfordefault"/>')",0,
	"Manage Purchase Order","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotmanagepurchasefordefault"/>')",0,
	"Manage Work Order","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotmanageworkfordefault"/>')",0

<%}
else{%>
	<% if( request.getAttribute("jobcurrentstatus").equals( "F" )) {%>
	"<bean:message bundle = "PRM" key = "prm.assignpartner"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotassignpartner"/>')",0,
	
	<%} else { %>
	"<bean:message bundle = "PRM" key = "prm.assignpartner"/>","Partner_Search.do?fromprm=Y&ref=search&typeid=<%=jobid%>&type=<%=type%>",0,
	
	<% } %>
	<% if(partner_id.equals("0")) {%>
	"<bean:message bundle = "PRM" key = "prm.unassignpartner"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.jobnotassigned"/>')",0,
	<%}else 
	{%>
	"<bean:message bundle = "PRM" key = "prm.unassignpartner"/>","UnassignPartner.do?typeid=<%=jobid%>&type=<%=type%>&pid=<%=partner_id%>",0,
	
	<%}%>
	"<bean:message bundle = "PRM" key = "prm.addponumber"/>","AddPoNumber.do?function=add&typeid=<%=jobid%>&type=<%=type%>",0,
	"Add Partner Cost","AddPartnerCost.do?type=<%=type%>&fromtype=Job&id=<%= jobid%>",0,
	"Manage Purchase Order","#",1,
	"Manage Work Order","#",1
<%}%>
)


arMenu2_5=new Array(
"View","javascript:viewpo('po');",0,
<% if( request.getAttribute( "checkforsendpowo" ).equals( "Y" ) ) { %>
"Send","javascript:alert( 'Cannot send PO as job is complete' ) ;",0
<% }else {%>
"Send","javascript:sendpo( 'po' );",0
<%}%>
)


arMenu2_6=new Array(
"View","javascript:viewpo('wo');",0,
<% if( request.getAttribute( "checkforsendpowo" ).equals( "Y" ) ) { %>
"Send","javascript:alert( 'Cannot send WO as job is complete' ) ;",0
<% }else {%>
"Send","javascript:sendpo( 'wo' );",0
<%}%>
)



arMenu3=new Array(
110,
findPosX('pop3'),findPosY('pop3'),
"","",
"","",
"","",
<% if( request.getAttribute("jobtype").equals( "Default" )) {%>
	"<bean:message bundle = "PRM" key = "prm.updatesite"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotupdatesitefordefault"/>')",0,
	"<bean:message bundle = "PRM" key = "prm.processchecklist"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotprocesschecklistfordefault"/>')",0

<%}
else{%>
"<bean:message bundle = "PRM" key = "prm.updatesite"/>","SiteAction.do?ref=Add&Job_Id=<%= jobid %>&ref1=fromprj",0,
"<bean:message bundle = "PRM" key = "prm.processchecklist"/>","SiteChecklist.do?function=update&typeid=<%=jobid%>&type=<%=type%>",0
<%}%>
)

arMenu4=new Array(
80,
findPosX('pop4'),findPosY('pop4'),
"","",
"","",
"","",
"Update","javascript:managestatus( 'update' )",0,
"View","javascript:managestatus( 'view' )",0,
"Send","javascript:sendstatusreport()",0)


arMenu5=new Array(
115,
findPosX('pop5'),findPosY('pop5'),
"","",
"","",
"","",
"Redline Report","Under_Dev.jsp",0,
"Detailed Summary","Under_Dev.jsp",0,
"CVS Summary","Under_Dev.jsp",0
)

arMenu6=new Array(
90,
findPosX('pop6'),findPosY('pop6'),
"","",
"","",
"","",
<% if( request.getAttribute("jobtype").equals( "Default" ) ) {%>
	"<bean:message bundle = "PRM" key = "prm.view"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotviewfordefault"/>')",0
	

<%}
else{%>
"<bean:message bundle = "PRM" key = "prm.view"/>","javascript:addaddendum( 'view' , 'view');",0

<%}%>
)

arMenu7=new Array(
140,
findPosX('pop7'),findPosY('pop7'),
"","",
"","",
"","",
"<bean:message bundle = "PRM" key = "prm.goto.appendixdashboard"/>","AppendixHeader.do?function=view&appendixid=<%=appendixid%>",0
)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

function viewpo(t)
{
    window.location.href = "POAction.do?ordertype="+t+"&type=<%=type%>&fromtype=Job&function_type=view&jobid=<%= jobid%>";
	return false;	
}


function sendpo(t)
{
    window.location.href = "POAction.do?ordertype="+t+"&type=<%=type%>&fromtype=Job&function_type=Send POWO&jobid=<%= jobid%>";
	return false;	
}

function addcost()
{
    window.location.href = "AddPartnerCost.do?type=J&fromtype=Job&jobid=<%= jobid%>";
	return false;	
}

</script>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="left" >
  <tr> 
   <td class="large" height="30"  colspan="3"><bean:message bundle="PRM" key="prm.jobdashboard"/><bean:write name="JobHeaderForm" property="appendixname" /><bean:message bundle="PRM" key="prm.job->"/><bean:write name="JobHeaderForm" property="jobname" /></td>
  </tr>

<tr>
<td width="35%" class="labellobold" >
	<table width="100%" border="0" cellspacing="1" cellpadding="1" align="left" class="textw">
	<tr>
	 <td class="labellobold" colspan="2"><bean:message bundle="PRM" key="prm.p&lsummay"/></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.materialscost"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="JobHeaderForm" property="materialscost" /></td>
    </tr>
    <tr>
	<td class="labelebold"><bean:message bundle = "PRM" key = "prm.cnslaborcost"/></td>
	<td class="readonlytextnumberodd"  width="50%"><bean:write name="JobHeaderForm" property="cnslaborcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold" width="50%"><bean:message bundle = "PRM" key = "prm.fieldlaborcost"/></td>
	 <td class="readonlytextnumberodd"  width="50%"><bean:write name="JobHeaderForm" property="fieldlaborcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold" width="50%"><bean:message bundle = "PRM" key = "prm.freightcost"/></td>
	 <td class="readonlytextnumberodd"  width="50%"><bean:write name="JobHeaderForm" property="freightcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold" width="50%"><bean:message bundle = "PRM" key = "prm.travelcost"/></td>
	 <td class="readonlytextnumberodd"  width="50%"><bean:write name="JobHeaderForm" property="travelcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.estimatedcost"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="JobHeaderForm" property="estimatedcost" /></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.extendedprice"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="JobHeaderForm" property="extendedprice" /></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle = "PRM" key = "prm.proformamargin"/></td>
	 <td class="readonlytextnumberodd"><bean:write name="JobHeaderForm" property="proformamargin" /></td>
	</tr>
	<%if(addendumlistlength > 7)
		{ 
			for(int k=0;k<(addendumlistlength-7);k++)
			{%>
		 	<tr>
    	 	<td  class="labelebold" colspan="2">&nbsp;</td>
   		 	</tr>
   			<%}
		} %>
	<tr>
	 <td class="labelebold" colspan="2">&nbsp;</td>
	</tr>
	<tr>
	 <td class="labellobold" colspan="2"><bean:message bundle="PRM" key="prm.scheduleinformation"/></td>
	</tr>
	<tr>
	 <td class="labelebold"><bean:message bundle="PRM" key="prm.startdate"/></td>
	 <td class="readonlytextnumberodd"  width="50%"><bean:write name="JobHeaderForm" property="startdate" /></td>
     </tr>
	<tr>
	 <td class="labelebold"><bean:message bundle="PRM" key="prm.plannedenddate"/></td>
	 <td class="readonlytextnumberodd"  width="50%"><bean:write name="JobHeaderForm" property="plannedenddate" /></td>
     </tr>
     <tr>
 	 <td class="labelebold" colspan="2">&nbsp;</td>
	 </tr>
   </table>
</td>


<td width="45%" class="labellobold" >
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="left" class="textw">
<tr>
 <td class="labellobold" colspan="4"><bean:message bundle="PRM" key="prm.changesummary"/></td>
</tr>
<tr>
 <td class="labelebold"><bean:message bundle="PRM" key="prm.activitiesadded"/></td>
 <td class="labelebold"><bean:message bundle="PRM" key="prm.cost"/></td>
 <td class="labelebold"><bean:message bundle="PRM" key="prm.price"/></td>
 <td class="labelebold"><bean:message bundle="PRM" key="prm.margin"/></td>
</tr>

<logic:present  name="addmactivitylist">
    <logic:iterate id="aalist" name="addmactivitylist" >
  
<tr>
 <td  class="labelebold"><bean:write name="aalist" property="activityaddendumname"/></td>
     <td  class="readonlytextnumberodd"><bean:write name="aalist" property="estimatedcost"/></td>
     <td  class="readonlytextnumberodd"><bean:write name="aalist" property="extendedprice"/></td>
     <td  class="readonlytextnumberodd"><bean:write name="aalist" property="proformamargin"/></td>
</tr>
</logic:iterate>
    </logic:present>

 
    <%if(addendumlistlength < 8)
		{ 
			for(int k=addendumlistlength;k<7;k++)
			{%>
		 	<tr>
    	 	<td  class="labelebold" colspan="4">&nbsp;</td>
   		 	</tr>
   			<%}
		} %> 

<tr>
 <td class="labelebold" colspan="4">&nbsp;</td>
</tr>
<tr>
 <td class="labellobold" colspan="4"><bean:message bundle="PRM" key="prm.partnerassigned"/></td>
</tr>
<tr>
 <td class="labelebold" colspan="4"><bean:write name="JobHeaderForm" property="partnerassigned" /></td>
</tr>
<tr>
 <td class="labelebold" colspan="4">&nbsp;</td>
</tr>
<tr>
 <td class="labelebold" colspan="4">&nbsp;</td>
</tr>





</table>

</td>

<td width="30%" class="labellobold" >
	<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td class="labellobold" ><bean:message bundle="PRM" key="prm.activitystatussummary"/></td>
	</tr>
	<tr>
	<td>
	<IFRAME  SRC="ViewGraph.do?graphtype=activitygraph&jobid=<%=jobid%>" TITLE="" marginwidth=0 marginheight=0  frameborder=0 bgcolor="#ECECEC" height=200 width=290>
	</IFRAME>
	</td>
	</tr>
	</table>
</td>

</tr>
<tr>
 <td class="labele" colspan="2">&nbsp;</td>
 <td class="labelebold" ></td>

</tr>


 <tr>
    <td colspan="3"><table border = "0" cellspacing = "1" cellpadding = "1" >
	<logic:present name="activitylist">
	<tr> 
	    
		
	    <td class="tryb" rowspan="2" ><bean:message bundle = "PRM" key = "prm.activity"/></b></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.type"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.quantity"/></td>
		<td class="tryb" colspan="6"><bean:message bundle = "PRM" key = "prm.estimatedcost"/></td>
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.extendedsubtotal"/></td>
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.extendedprice"/></td>
	    <td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.proformamargin"/></td>
		<td class="tryb" colspan="2"><bean:message bundle = "PRM" key = "prm.estimatedschedule"/></td>
		<td class="tryb" rowspan="2"><bean:message bundle = "PRM" key = "prm.status"/></b></td>
	  </tr>
	<tr> 
		 <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.materials"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.cnslabor"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.contractlabor"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.freight"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.travel"/></div>
	    </td>
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.total"/></div>
	   
		<td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.start"/></b></div>
	    </td>
		<td class="tryb"> 
	      <div align="center"><bean:message bundle = "PRM" key = "prm.complete"/></b></div>
	    </td>
	</tr>
	
	<%int i=1;
	String bgcolor="";
	String bgcolortext=""; 
	String bgcolortext1="";%>
	
		<logic:iterate id="alist" name="activitylist" >
		<%if((i%2)==0)
		{
			bgcolor="readonlytextnumbereven";
			bgcolortext="readonlytexteven";
			bgcolortext1="hypereven"; 
		
		} 
		else
		{
			bgcolor="readonlytextnumberodd";
			bgcolortext="readonlytextodd"; 
			bgcolortext1="hyperodd";
		}%>
	
	<tr> 
	   
	    <td  class="<%=bgcolortext1 %>"><a href="ActivityHeader.do?function=view&jobid=<bean:write name="alist" property="jobid"/>&activityid=<bean:write name="alist" property="activityid"/>" target="ilexmain"><bean:write name="alist" property="activityname"/></a></td>
	    <td  class="<%=bgcolortext %>"><bean:write name="alist" property="type"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="quantity"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="materials"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="cnslabor"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="contractlabor"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="freight"/></td>
	    <td  class="<%=bgcolor %>"><bean:write name="alist" property="travel"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="total"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="extendedsubtotal"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="extendedprice"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="pmargin"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="estart"/></td>
		<td  class="<%=bgcolor %>"><bean:write name="alist" property="ecomplete"/></td>
		<td  class="<%=bgcolortext %>"><bean:write name="alist" property="status"/></td>
	</tr>
	
			
	<%i++; %>
		</logic:iterate>
  
	<tr class = "buttonrow"> 
	    <td colspan="15" height="30"> 
		  <html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "pm" key = "msa.tabular.sort"/></html:button> 
	    </td>
	</tr> 
	</logic:present> 
	
	<jsp:include page = '/Footer.jsp'>
  		 <jsp:param name = 'colspan' value = '28'/>
    	 <jsp:param name = 'helpid' value = 'roleprivilege'/>
 	 </jsp:include>
	  
   </table>
  </td>
 </tr>
</table>



</body>
</html:form>
<script>
var str = '';


function sortwindow()
{
	str = 'SortAction.do?Type=prj_activity&jobid=<%=jobid%>';
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
	return true;
	
}
</script>

</html:html>