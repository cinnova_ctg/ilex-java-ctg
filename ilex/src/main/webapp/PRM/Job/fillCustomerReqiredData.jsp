<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<div id="errorDiv" class="MSERR0001" style="text-align: left;padding-bottom: 5px;padding-left:5px;"></div>
<html:form action="CustomerRequireDataAction" >
	<html:hidden property="jobId"/>
	<html:hidden property="appendixId"/>
	<html:hidden property="msaId"/>
	<div>
			<div id="firstVisitDiv">
				<div style="padding-bottom: 10px;padding-left:20px;padding-right:20px;">
					<table cellspacing="2px;" cellpadding="2px;" class="PCSTD0002">
						<tr>
							<td>1.First Visit Success ?</td>
							<td
								title="If Multiple visits,did we complete assigned tasks on each visit ? If Yes,choose Yes.">
								<html:radio property="firstVisitQuestion" value="true" name="CustomerRequireForm" onclick="firstVisitQuestionClick();"></html:radio>&nbsp;&nbsp;Yes
								<html:radio property="firstVisitQuestion" value="false" name="CustomerRequireForm" onclick="firstVisitQuestionClick();"></html:radio>&nbsp;&nbsp;No
							</td>
						</tr>
					</table>
				</div>
			</div>
		<div id="failedVisitDiv" style="display: none;">
		<div style="padding-bottom: 10px;padding-left:20px;padding-right:20px;">
				<table cellspacing="2px;" cellpadding="2px;" class="PCSTD0002">
					<tr>
						<td colspan="4">2.Who is responsible for the failed visit ?</td>
					</tr>
					<tr>
						<td colspan="4">Select all that apply:</td>
					</tr>
					<tr>
						<td>
						<html:checkbox property="failedVisitQuestion" value="Contingent" name="CustomerRequireForm" onclick="selectFailedVisitOption();" styleId="ContingentCheck"></html:checkbox>&nbsp;&nbsp;Contingent
						</td>
						<td>
						<html:checkbox property="failedVisitQuestion" value="Client" name="CustomerRequireForm" onclick="selectFailedVisitOption();" styleId="ClientCheck"></html:checkbox>&nbsp;&nbsp;Client
						</td>
						<td>
						<html:checkbox property="failedVisitQuestion" value="LEC" name="CustomerRequireForm" onclick="selectFailedVisitOption();" styleId="LECCheck"></html:checkbox>&nbsp;&nbsp;LEC
						</td>
						<td>
						<html:checkbox property="failedVisitQuestion" value="Other3rdParty" name="CustomerRequireForm" onclick="selectFailedVisitOption();" styleId="Other3rdPartyCheck"></html:checkbox>&nbsp;&nbsp;Other 3<sup>rd</sup>Party
						</td>
					</tr>
					<tr>
						<td>
								<html:select property="contingentFailVisitArray" name="CustomerRequireForm" styleClass="comboe failedVisitGroup" styleId="ContingentGroup" disabled="disabled">
							    <html:optionsCollection name = "CustomerRequireForm"  property = "contingentList"  label = "label"  value = "value"/>
							    </html:select>
						</td>
						<td><html:select property="clientFailVisitArray" name="CustomerRequireForm" styleClass="comboe failedVisitGroup" styleId="ClientGroup" disabled="disabled">
							    <html:optionsCollection name = "CustomerRequireForm"  property = "clientList"  label = "label"  value = "value" />
							    </html:select>
						</td>
						<td>
								<html:select property="lecFailVisitArray" name="CustomerRequireForm" styleClass="comboe failedVisitGroup" styleId="LECGroup" disabled="disabled">
							    <html:optionsCollection name = "CustomerRequireForm"  property = "lecList"  label = "label"  value = "value" />
							    </html:select>
						</td>
						<td><html:select property="otherPartyFailVisitArray" name="CustomerRequireForm" styleClass="comboe failedVisitGroup" styleId="Other3rdPartyGroup" disabled="disabled">
							    <html:optionsCollection name = "CustomerRequireForm"  property = "otherPartyList"  label = "label"  value = "value" />
							    </html:select>
						</td>
		
					</tr>
				</table>
			</div>
		</div>
		<div id="addSiteVisitDiv" style="display: none;">
			<div style="padding-bottom: 10px;padding-left:20px;padding-right:20px;">
				<table cellspacing="2px;" cellpadding="2px;" class="PCSTD0002">
					<tr>
						<td colspan="2">3.Did any of the POF's result in scheduling an
							additional site visit ?</td>
					</tr>
					<tr>
						<td>
							<html:radio property="addSiteVisitQuestion" value="true" name="CustomerRequireForm" onclick="selectUnplannedSite();"></html:radio>&nbsp;&nbsp;Yes
						</td>
						<td>
							<html:radio property="addSiteVisitQuestion" value="false" name="CustomerRequireForm" onclick="selectUnplannedSite();"></html:radio>&nbsp;&nbsp;No
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<html:select property="unplannedAddVisit" name="CustomerRequireForm" styleClass="comboe" styleId="addSitegroup" disabled="disabled">
						  	<html:option value="0">Select</html:option>
						  	<html:option value="1">1</html:option>
						  	<html:option value="2">2</html:option>
						  	<html:option value="3">3</html:option>
						  	<html:option value="4 or more">4 or more</html:option>
						</html:select>
						</td>
					</tr>
				</table>
			</div>
		</div>
			<div style="padding-left:30px;">
				<input type="button" value="OK" class="FMBUT0001"
					onclick="saveAllQuestions();"> <input type="button"
					value="Cancel" class="FMBUT0001" onclick="closeWindow();">
			</div>
	</div>
</html:form>
<script type="text/javascript">
jQuery(document).ready(function(){
	firstVisitQuestionClick();
	setFailedVisitResponsible();
})
	function firstVisitQuestionClick(){
		if(jQuery("input[name='firstVisitQuestion']:checked").val()=='false')
			{
			jQuery('#failedVisitDiv').css('display','');
			jQuery('#addSiteVisitDiv').css('display','');
			}
		else{
			jQuery('#failedVisitDiv').css('display','none');
			jQuery('#addSiteVisitDiv').css('display','none');
			}
	}
	function saveAllQuestions(){
				if(!jQuery("input[name='firstVisitQuestion']:checked").val())
				{
				alert("Please select value for firstVisit success");
				return false;
				}
				
				if(jQuery("input[name='firstVisitQuestion']:checked").val()=='false')
				{
						if(!jQuery("input[name='failedVisitQuestion']:checked").val())
						{
						alert("Please select who is responsible for failed visit");
						return false;
						}
						else{
							var status=true;
							jQuery("input[name='failedVisitQuestion']:checked").each(function(){
								var failedVisit=jQuery(this).val();
								if(!jQuery('#'+failedVisit+'Group').val())
									{
									alert('Please select '+failedVisit+' category');
									status=false;
									return false;
									}	
							});
							if(status==false)
							{
							return false;
							}
							
						}	
						
						if(!jQuery("input[name='addSiteVisitQuestion']:checked").val())
						{
						alert("Please select additional site visits");
						return false;
						}
						else{
							if(jQuery("input[name='addSiteVisitQuestion']:checked").val()=='true' && jQuery('#addSitegroup').val()=='0')
								{
								alert('Please select unplanned additional site visits');
								return false;
								}
						}	
					
				}
			updateCustomerRequiredData();	

	}
	
	function selectFailedVisitOption(){
		
		
		
		var selectedValues = jQuery("input[name=failedVisitQuestion]:checked").map(function () {
			return this.value;
		}
	).get().join(",");

		
		
					
		
		jQuery.each(selectedValues.split(','), function(i, val) {
			  jQuery('#'+val+'Group').removeAttr('disabled');
			  jQuery('#'+val+'Group').attr('multiple','multiple');
			});
		
}
	function selectUnplannedSite(){
		if(jQuery("input[name='addSiteVisitQuestion']:checked").val()=='true')
			{
			jQuery('#addSitegroup').removeAttr('disabled');
			}
		else
			{
			jQuery('#addSitegroup').attr('disabled','disabled');
			}
	}
	function updateCustomerRequiredData(){
		jQuery.ajax({
			type : 'POST',
			url  : './CustomerRequireDataAction.do?function=insert',
			data :	jQuery("form").serialize(),
			success: function(data) {
				if(data=='success')
				{
					closeWindow();
					submitJobForCompletion();
			}
			else{
				jQuery("#errorDiv").html(data);
				}	
		}
	});
		
	}
	function setFailedVisitResponsible(){
		if(jQuery("input[name='firstVisitQuestion']:checked").val()=='false')
			
		{
			<c:forEach var="category" items="${CustomerRequireForm.selectedCategoryValues}">
			   	 jQuery('#'+'${category.key}'+'Check').attr('checked','checked');
			   	 var categoryValues=new Array();
		   			<c:forEach var="categoryCode" items="${category.value}">
			   	 		categoryValues.push('${categoryCode.label}');
					</c:forEach>
					jQuery('#'+'${category.key}'+'Group').removeAttr('disabled');
					jQuery('#'+'${category.key}'+'Group').attr('multiple','multiple');
					jQuery('#'+'${category.key}'+'Group option').each(function(){
						if (jQuery.inArray(jQuery(this).attr("text"),categoryValues) > -1) 
							{ 
								jQuery(this).attr("selected","selected"); // select if same value
						     }
						else{
							jQuery(this).removeAttr("selected");
							}
				    });
	
			</c:forEach>
		}
	}
 </script>
