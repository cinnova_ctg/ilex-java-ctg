<%@ page import="com.mind.bean.newjobdb.JobDashboardTabType"%>
<jsp:include page="/common/IncludeGoogleKey.jsp"/>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%String size = "0";
if(request.getAttribute("size") != null )
	size = request.getAttribute("size")+"";
%>

<script>
function onLoadUpdatePOInfo() {
	var distance = "0.0";
	var duration = "0.0";

	var partnerLat = <%=request.getAttribute("partnerLatitude")== null? "0.0": request.getAttribute("partnerLatitude")%>;
	var partnerLong = <%=request.getAttribute("partnerLongitude")==null ? "0.0": request.getAttribute("partnerLongitude")%>;
	var siteLat = <%=request.getAttribute("siteLatitude")== null ? "0.0": request.getAttribute("siteLatitude") %>;
	var siteLong = <%=request.getAttribute("siteLongitude")== null ? "0.0": request.getAttribute("siteLongitude")%>;
	//if( (partnerLat != 0.0 && partnerLong != 0.0) && (siteLat != 0.0 && siteLong != 0.0) ){
		var coordinates = [];
		// Change the logic to assign partner in any case either Google API calculate the Distance or not.
		coordinates.push(siteLat+","+siteLong);	
		coordinates.push(partnerLat + "," + partnerLong);
		gDir = new google.maps.Directions();
		gDir.loadFromWaypoints(coordinates);
		google.maps.Event.addListener(gDir, "load", function(){
				distance = gDir.getDistance().meters;
				duration = gDir.getDuration().seconds;
				setTimeout("updateSiteInfo("+distance+","+duration+")",1000); 
			});
		
	//} 
}
function updateSiteInfo(distance, duration) {
	updatePoInfo("Partner_Search.do?ref=updatePOByAjax&distance="+distance+"&duration="+duration+"&powoId="+<%=request.getAttribute("purchaseOrderId")%>);
}
</script>
<script>
updatePoInfo = function (poURL){
	/* This function call Ajax which is inmplemented into /javascript/prototype.js */
		var pars = '';
		
		$.ajax({
			type: "POST",
			url: poURL,
			data: pars,
			error: disableStatus,
			success: disableStatus
        });
	
		/*var myAjax = new Ajax.Updater(
				{success:'CustomerFields'},
				poURL,
				{method:'post', parameters:pars, onSuccess:disableStatus, onFailure:disableStatus, evalScripts: true}
		);*/
	}
	function disableStatus() {
	}
</script>

<script>
	function callGmap(){
		if(<%=size%> > 0){
			if( <%=request.getAttribute("tabId")%>==10){
				onLoadUpdatePOInfo();
				try{onLoad();}catch(e){};
			}else{
				onLoadUpdatePOInfo();
			}
		}else{
			onLoadUpdatePOInfo();
		}
	}
</script>
<table>
	<tr>
		<td>
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
	<%if(Integer.parseInt(size) > 0){ %>
	<tr>
		<td>
			<jsp:include page="JobGmap.jsp"/>
		</td>
	</tr>
	<%}else{ %>
	<tr>
		<td>
			<b><font class="dbvaluesmallFont">No Vendex view available.</font></b>
		</td>
	</tr>
	<%} %>
</table>