<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<script>
function viewInstallNotes(){
	var url;
	url = "JobCloseAction.do?hmode=viewInstallNotes&type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=1&saveJobActions=1";							
	alert(url);
	document.location.href= url;
}
</script>

<table>
	<tr>
		<td>
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
	<tr>
		<td >
			<jsp:include page="JobWorkflow.jsp"/>
		</td>
	</tr>
	<c:if test="${requestScope.isTicket eq 'Ticket'}">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="50%" valign="top">
				<jsp:include page="TicketActivitySummary.jsp"/>
			</td>
			<td style="padding-right: 10px;">
				&nbsp;
			</td>
			<td width="50%" valign="top">
				<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
						<jsp:include page="JobActions.jsp"/>
					</td>
				</tr>
				<tr>
					<td height="10">
					</td>
				</tr>
				<tr>
					<td>
						<jsp:include page="ScheduleSummary.jsp"/>
					</td>
				</tr>
				</table>
			</td>
		</tr>	
	</table>
	</c:if>
	<tr>
		<td >
			<jsp:include page="PurchaseOrder.jsp"/>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0">
				<tr>
					<td width="50%" valign="top">
						<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 0px;">
							<c:if test="${requestScope.isTicket ne 'Ticket'}">
							<tr>
								<td  valign="top">
									<jsp:include page="JobActions.jsp"/>
								</td>
							</tr>
							</c:if>
							<tr>
                                                            <td  valign="bottom" style="padding-top:15;">
                                                                <jsp:include page="JobInstallationNotes.jsp"/>
                                                            </td>
							</tr>
							<tr>	
								<td  valign="top">
									<jsp:include page="JobPlanningNotes.jsp"/>
								</td>
							</tr>
						</table>
					</td>
					<td width="50%" valign="top">
						<jsp:include page="JobCustomerFields.jsp"/>
					</td>
				</tr>				
			</table>
		</td>		
	</tr>	
</table>
<c:if test ="${not empty requestScope.refershtree && requestScope.refershtree eq 'refershtree'}">
	<script>
			parent.ilexleft.location.reload();
	</script>
</c:if>