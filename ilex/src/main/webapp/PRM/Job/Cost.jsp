<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<table  border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td colspan="2"><h3>Cost</h3></td>
</tr>
<tr class="paddingLeft">
	<td colspan="2">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td class="dbvaluesmallFont" style="padding-left: 3px;">
				<b>Total Estimated Cost:</b>&nbsp;&nbsp;&nbsp;
			</td>
			<td class="dbvaluesmallFont" align="right">
				<c:out value='${JobSetUpForm.totalEstimatedCost}'></c:out>
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont" style="padding-left: 3px;" >
				<b>Total Actual Cost:</b>
			</td>
			<td class="dbvaluesmallFont" align="right">
				<c:out value='${JobSetUpForm.totalActualCost}'></c:out>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
	<td class="dbvaluesmallFont">
		<table  class="" id="CVPT" cellpadding="0" cellspacing="0" width="100%">
		<tr height="25">
			<td class="labelodarkbgnoborder" width="20%" ><b>Contract</b></td>
			<td class="labelodarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Estimated</b></td>
			<td class="labelodarkbgblueborder" width="20%" style="border-bottom: 0px;"><b>Actual</b></td>
		</tr>
		<tr>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Labor</b></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.contractLaborEstimatedCost}'></c:out></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.contractLaborActualCost}'></c:out></td>
		</tr>
		<tr>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Materials</b></td>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.contractMaterialEstimatedCost}'></c:out></td>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.contractMaterialActualCost}'></c:out></td>
		</tr>
		<tr>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Freight</b></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.contractFreightEstimatedCost}'></c:out></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.contractFreightActualCost}'></c:out></td>
		</tr>
		<tr>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Travel</b></td>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.contractTravelEstimatedCost}'></c:out></td>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.contractTravelActualCost}'></c:out></td>
		</tr>
		<tr>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;"><b>Total</b></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;"><c:out value='${JobSetUpForm.contractTotalEstimatedCost}'></c:out></td>
			<td class="labelorightwhitebgblueborder" width="20%"><c:out value='${JobSetUpForm.contractTotalActualCost}'></c:out></td>
		</tr>
		</table>
	</td>
	<td class="dbvaluesmallFont" style="padding-left: 10px;">
		<table  class="" id="CVPT" cellpadding="0" cellspacing="0" width="100%">
		<tr height="25">
			<td class="labelodarkbgnoborder" width="20%" ><b>Corporate</b></td>
			<td class="labelodarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Estimated</b></td>
			<td class="labelodarkbgblueborder" width="20%" style="border-bottom: 0px;"><b>Actual</b></td>
		</tr>
		<tr>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Labor</b></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.corporateLaborEstimatedCost}'></c:out></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.corporateLaborActualCost}'></c:out></td>
		</tr>
		<tr>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Materials</b></td>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.corporateMaterialEstimatedCost}'></c:out></td>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.corporateMaterialActualCost}'></c:out></td>
		</tr>
		<tr>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Freight</b></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.corporateFreightEstimatedCost}'></c:out></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.corporateFreightActualCost}'></c:out></td>
		</tr>
		<tr>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><b>Travel</b></td>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.corporateTravelEstimatedCost}'></c:out></td>
			<td class="labelorightdarkbgblueborder" width="20%" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.corporateTravelActualCost}'></c:out></td>
		</tr>
		<tr>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;"><b>Total</b></td>
			<td class="labelorightwhitebgblueborder" width="20%" style="border-right: 0px;"><c:out value='${JobSetUpForm.corporateTotalEstimatedCost}'></c:out></td>
			<td class="labelorightwhitebgblueborder" width="20%"><c:out value='${JobSetUpForm.corporateTotalActualCost}'></c:out></td>
		</tr>
		</table>
	</td>
</tr>
</table>
