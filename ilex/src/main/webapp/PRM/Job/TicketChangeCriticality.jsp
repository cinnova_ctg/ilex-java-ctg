<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/UtilityFunction.js"></script>
<script type='text/javascript' src='./dwr/interface/JobTicketAction.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>

<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<head>
<script>
function onCriticalityChange(){
	if(document.forms[0].helpDesk.value != 'Y')
		document.forms[0].submit();
}

function setResourceLeve(radioObj){
	/*	Check for HelpDesk radio option.
		if its 'Y' then set resource level to 'Systems Engineer' and disable the combo box.				
	*/
	if(radioObj.value == 'Y'){ // - When Help Desk is Yes
		if(document.forms[0].resource.length == 0){ // When Resouce comboBox has no value
			var value = getSelectedOption(document.forms[0].criticality,'1-Emergency');
			document.forms[0].criticality.value = value;
			fillResourceCombo(); // - Fill Resource comboBox.
		}
		if(document.forms[0].resource.length > 0){ // - When Resource comboBox has values.
			var value = getSelectedOption(document.forms[0].resource, 'Systems Engineer');		
				document.forms[0].resource.value = value;
				document.forms[0].resource.disabled = true; 
		}
	}
	else{ // - When Help Desk is No
		document.forms[0].resource.disabled = false;
	}
} // - End of setResourceLeve() function


</script>
<script>
function vailidateAll(){

	if(document.forms[0].criticality.value == '0'){
		alert('Please select a Criticality.');
		document.forms[0].criticality.focus();
		return false;
	}
	
	if(document.forms[0].resource.value == '0'){
		alert('Please select a Resource.');
		document.forms[0].resource.focus();
		return false;
	}
	enableAll();
	document.forms[0].action = "TicketChangeCriticality.do?hmode=Save&msaId=<c:out value='${TicketChangeCriticalityForm.msaId}'/>&Job_Id=<c:out value='${TicketChangeCriticalityForm.jobId}'/>";
	document.forms[0].submit();
}

function resetForm(){
	document.forms[0].criticality.value = null;
	document.forms[0].resource.value = null;
}

function enableAll(){
	if(document.forms[0].resource.disabled)
		document.forms[0].resource.disabled = false;
}
</script>
</head>
<html>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
	<td id = "pop1" height="19" class = "Ntoprow1">&nbsp;</td>
</tr>
<tr>
	<td background="images/content_head_04.jpg" height="21">
       <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null" class="bgNone"><c:out value='${requestScope.msaName }'/></a>
		 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
		 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
		 <a><span class="breadCrumb1">Update HelpDesk/Criticality/Resource</a>
	   </div>
    </td>
</tr>
</table>
<html:form action="/TicketChangeCriticality.do">
<html:hidden property="msaId"/>
<html:hidden property="jobId"/>
<html:hidden property="appendixId"/>

<table cellpadding="0" cellspacing="0" border="0">
	<c:if test="${requestScope.updateFlag eq '0'}">
	<tr>
		<td style="padding-left: 10px;">&nbsp;</td>
		<td class="message">
			Updated successfully.
		</td>
	</tr>
	</c:if>
</table>

<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<td style="padding-left: 8px;">
		<table cellpadding="0" cellspacing="0" border="0">
		<tr style=" padding-top: 8px;">
			<td>&nbsp;</td>
			<td >
				<table border="0" cellpadding="1" cellspacing="1" >
					<tr>
						<td class = "tabtexto"><b>Help Desk:</b>&nbsp;&nbsp;</td>
						<td class="pvstexto">
							<input type="radio" name="helpDesk" value="Y" align="middle" onclick="setHelpDeskCriticality(this);" <c:if test="${TicketChangeCriticalityForm.helpDesk eq 'Y'}">checked="checked"</c:if>/>Yes
							<input type="radio" name="helpDesk" value="N" align="middle" onclick="setHelpDeskCriticality(this);" <c:if test="${TicketChangeCriticalityForm.helpDesk eq 'N'}">checked="checked"</c:if>/>No
						</td>
					</tr>
					<tr>
						<td class = "tabtexto"><b>Criticality:</b>&nbsp;&nbsp;</td>
						<td class="pvstexto">
							
							<html:select name="TicketChangeCriticalityForm" property="criticality" styleClass="select" onchange="papulateDropDown(document.forms[0].appendixId.value,this);" >
								<html:optionsCollection name="TicketChangeCriticalityForm" property="criticalityCombo" label="label" value="value"/>
							</html:select>
						</td>
					</tr>
					<tr>
						<td class = "tabtexto"><b>Resources:</b>&nbsp;&nbsp;</td>
						<td class="pvstexto">
							<span id="resourceDropDown">
								<jsp:include page="/PRM/Job/DropDown.jsp">
									<jsp:param name = 'fromJsp' value = 'TicketChangeCriticality.jsp'/>
								</jsp:include>
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="pvstexto">
							<html:hidden property="update" value="Update"/>
							<html:submit property="update" value="Update" styleClass="button_c" onclick="return vailidateAll();">Update</html:submit>&nbsp;&nbsp;&nbsp;
							<html:submit property="cancel" value="Cancel" styleClass="button_c" onclick="return resetForm();">Cancel</html:submit>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</html:form>
</body>
</html>
<script>
<c:if test="${TicketChangeCriticalityForm.helpDesk eq 'Y'}"> 
	document.forms[0].resource.disabled = true;
</c:if>
</script>