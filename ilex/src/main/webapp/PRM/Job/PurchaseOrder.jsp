<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<LINK href="styles/content.css" rel="stylesheet" type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='./dwr/interface/JobExecuteAction.js'></script>
<head>
<script>
		function showWindow(e,pId) {
                    var div = $('#ajaxdiv');
                    var x = 0;
                    var y = 0;

                    if (e != '') {
                        x = e.clientX + document.all.pbody.scrollLeft;
                        y = e.clientY + document.all.pbody.scrollTop;
                    }

                    div.css('left', x - 180);
                    div.css('top', y);

                    $.ajax({
                        url: 'PartnerDetail.do?poId=' + pId,
                        success: function(response) {
                            $('#ajaxdiv').html(response);
                        }
                    });
		}
		function checkPOCompletionAllowed(poId, poMasterItem) {
			 var allowComplete = "";
			 
			 var ajaxRequest;  // The variable that makes Ajax possible!
             try{
	    	     // Opera 8.0+, Firefox, Safari
	             ajaxRequest = new XMLHttpRequest();
	         } catch (e){
	               // Internet Explorer Browsers
	         	try{
	            		ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
	            	} catch (e) {
	            		try{
	                			ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
	                		} catch (e){
						                	// Something went wrong
						                    alert("Your browser broke!");
						                    return false;
						                }
	          		  }
	       		}
	       		
	       		 // Create a function that will receive data sent from the server
			        ajaxRequest.onreadystatechange = function(){
			               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
			               		 var response = ajaxRequest.responseText;
 			               		 allowComplete = response;
			               }
			        }
			        
			        var url ="JobExecuteAction.do?hmode=isPOCompletionAllowed&poId="+poId+"&poMasterItem="+poMasterItem;
					ajaxRequest.open("POST", url, false);
					ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					ajaxRequest.send(null); 
					return allowComplete;
		}  // End of javascript method.
	</script>

<script>
		function POActions(poIndex, poid, poType)
		{
			var actionName = "";
			if(!document.forms[0].actionOnPO.length) {
				return;
			}
			var selectedIndex = document.getElementById("actionOnPO_" + poIndex).selectedIndex;
			actionName  = document.getElementById("actionOnPO_" + poIndex)[selectedIndex].value;			
			
			if(actionName=="0"){
				alert('please select one of the action.');
				return false;
			}else if(actionName=="Delete"){
				var del = confirm("Do you really want to delete the PO? ");
				if(del==false){
					document.getElementById("actionOnPO_" + poIndex).value = '0';
					return false;
				}
			}else if(actionName=="Reissue"){
				var reissue = confirm("Do you really want to Reissue the PO? ");
				if(reissue==false){
					document.getElementById("actionOnPO_" + poIndex).value = '0';
					return false;
				}
			}else if(actionName=="Cancel"){
				var undo = confirm("Do you really want to Cancel the PO? ");
				if(undo==false){
					document.getElementById("actionOnPO_" + poIndex).value = '0';
					return false;
				}
			}else if(actionName == "UndoNotAllowed"){
				alert("This PO can not be undone because its resources have been utilized by another PO.");
				document.getElementById("actionOnPO_" + poIndex).value = '0';
				return false;
			}
			if(actionName=="Process Checklist"){
				url = "ChecklistTab.do?job_id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&tabId=<c:out value='${JobSetUpForm.tabId}'/>&poId="+ poid+"&isPO=Y&page=jobdashboard&viewjobtype=ION&ownerId=<c:out value='${sessionScope.userid}'/>";
				document.forms[0].action = url;
				document.forms[0].submit();
				return true;
			}
			var poMasterItem = document.getElementById("poTypeId"+poIndex).innerHTML;
			if(actionName=="Complete" && poMasterItem=='Material'){
				var cnfComplete = confirm("Do you really want to Complete the PO? ");
				if(cnfComplete==false) {
					document.getElementById("actionOnPO_" + poIndex).value = '0';
					return false;
				} else {
					var jobStatus='${JobSetUpForm.jobStatus}';
					if(jobStatus=="Hold"){
						alert('Cannot change PO status to Complete as the job is on hold.');
						document.getElementById("actionOnPO_" + poIndex).value = '0';
						return false;
					}else{
					var completeAllowed = checkPOCompletionAllowed(poid, poMasterItem);
					if(completeAllowed == "0") {
						//url = "ChecklistTab.do?job_id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&tabId=<c:out value='${JobSetUpForm.tabId}'/>&poId="+ poid+"&isPO=Y&isActionComplete=Y&page=jobdashboard&viewjobtype=ION&ownerId=<c:out value='${sessionScope.userid}'/>";
						//document.forms[0].action = url;
						//document.forms[0].submit();
						url = "JobDashboardAction.do?hmode=POaction&type=<c:out value='${JobSetUpForm.tabId}'/>"
							+"&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>"						
							+"&poActionName=" + actionName
							+"&poId=" + poid
							+"&poMasterItem="+poMasterItem;
							document.forms[0].action = url;
							document.forms[0].submit();
						return true;
					} else {
						alert('Cannot change PO status to Complete as it has outstanding deliverables.');
						document.getElementById("actionOnPO_" + poIndex).value = '0';
						return false;
					}
				}
			}
			}else if(actionName=="Complete"){
				var cnfComplete = confirm("Do you really want to Complete the PO? ");
				if(cnfComplete==false) {
					document.getElementById("actionOnPO_" + poIndex).value = '0';
					return false;
				} else {
					var jobStatus='${JobSetUpForm.jobStatus}';
					if(jobStatus=="Hold"){
						alert('Cannot change PO status to Complete as the job is on hold.');
						document.getElementById("actionOnPO_" + poIndex).value = '0';
						return false;
					}else{
					var completeAllowed = checkPOCompletionAllowed(poid, '');
					if(completeAllowed == "0") {
						url = "ChecklistTab.do?job_id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&tabId=<c:out value='${JobSetUpForm.tabId}'/>&poId="+ poid+"&isPO=Y&isActionComplete=Y&page=jobdashboard&viewjobtype=ION&ownerId=<c:out value='${sessionScope.userid}'/>";
						document.forms[0].action = url;
						document.forms[0].submit();
						return true;
					} else {
						alert('Cannot change PO status to Complete as it has outstanding deliverables.');
						document.getElementById("actionOnPO_" + poIndex).value = '0';
						return false;
					}
				}
				}
			}
			
			/*var poMasterItem = document.getElementById("poTypeId"+poIndex).innerHTML;
			if(actionName=="Accept" && poMasterItem.indexOf("Material")!=-1){//!=-1 means both matches and condition is true in this case
				var cnfComplete = confirm("Do you really want to Complete the PO? ");
				if(cnfComplete==false) {
					document.getElementById("actionOnPO_" + poIndex).value = '0';
					return false;
				} else {
					var jobStatus='${JobSetUpForm.jobStatus}';
					if(jobStatus=="Hold"){
						alert('Cannot change PO status to Complete as the job is on hold.');
						document.getElementById("actionOnPO_" + poIndex).value = '0';
						return false;
					}else{
					var completeAllowed = checkPOCompletionAllowed(poid, '');
					if(completeAllowed == "0") {
						url = "ChecklistTab.do?job_id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&tabId=<c:out value='${JobSetUpForm.tabId}'/>&poId="+ poid+"&isPO=Y&isActionComplete=Y&page=jobdashboard&viewjobtype=ION&ownerId=<c:out value='${sessionScope.userid}'/>";
						document.forms[0].action = url;
						document.forms[0].submit();
						return true;
					} else {
						alert('Cannot change PO status to Complete as it has outstanding deliverables.');
						document.getElementById("actionOnPO_" + poIndex).value = '0';
						return false;
					}
				}
				}
			}*/

			if(actionName=="Process Deliverables"){
				url ="POAction.do?hmode=unspecified&type=<c:out value='${JobSetUpForm.tabId}'/>&poId="+ poid+"&type=3&tabId=3&mode=view&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&jobId=<c:out value='${JobSetUpForm.jobId}'/>"
			
			document.forms[0].action = url;
			document.forms[0].submit();
			}else if(actionName=="Send"){
				if(poType == null || poType == '')
				{
					alert("PO can not be send without a valid 'POType'");
					document.getElementById("actionOnPO_" + poIndex).value = '0';
					return false;
				}
				else
				{
					url = "JobDashboardAction.do?hmode=POaction&type=<c:out value='${JobSetUpForm.tabId}'/>"
					+"&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>"						
					+ "&poActionName=" + actionName
					+"&poId=" + poid;
					document.forms[0].action = url;
					document.forms[0].submit();
				}
			}else{
			url = "JobDashboardAction.do?hmode=POaction&type=<c:out value='${JobSetUpForm.tabId}'/>"
				+"&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>"						
				+"&poActionName=" + actionName
				+"&poId=" + poid;
			document.forms[0].action = url;
			document.forms[0].submit();
			}
		}
	</script>
<script>
		function createPO(tabId)
		{
			var appendixType = escape("<c:out value='${JobSetUpForm.appendixType}'/>");
			var url = "./POAction.do?hmode=buildPO&appendixType="+appendixType+"&isBuild=Y&tabId="+tabId+"&appendixId=<c:out value='${JobSetUpForm.appendixId}'/>&isNewPO=Y&jobId=<c:out value='${JobSetUpForm.jobId}'/>";
			document.forms[0].action = url;
			document.forms[0].submit();
		}
		function openPO(poId)
		{
			var appendixType = escape("<c:out value='${JobSetUpForm.appendixType}'/>");
			var url = "./POAction.do?hmode=unspecified&appendixType="+appendixType+"&tabId=0&appendixId=<c:out value='${JobSetUpForm.appendixId}'/>&jobId=<c:out value='${JobSetUpForm.jobId}'/>&poId="+poId;
			document.forms[0].action = url;
			document.forms[0].submit();
		}
		function viewpowo(jobid, powoid, type) {
			document.forms[0].target = "_self";
			document.forms[0].action = "POWOAction.do?jobid="+jobid+"&function_type=view&order_type="+type+"&powoid="+powoid;
			document.forms[0].submit();
		}
	</script>
<script>
		function CopyPO()
		{
			var appendixType = escape("<c:out value='${JobSetUpForm.appendixType}'/>");
			var url = "./CopyMpoToPoAction.do?hmode=openCopyMPO&appendixType="+appendixType+"&jobId=<c:out value='${JobSetUpForm.jobId}'/>&appendixId=<c:out value='${JobSetUpForm.appendixId}'/>";
			document.location = url;
		}
	</script>
</head>
<table width="100%" style="padding-left: 10px;">
	<tr>
		<td colspan="2">
			<table width="100%">
				<tr valign="top">
					<td width="6%" style="padding-left: 0px;" nowrap><h2
							style="font-size: 14px; padding-left: 0px; padding-top: 2px; padding-bottom: 0px;">Purchase
							Orders&nbsp;&nbsp;</h2></td>
					<td width="2%" align="left" valign="bottom"><c:if
							test="${requestScope.buyTab eq 'buyTab'}">
							<input type="button" width="40" name="copy" value="Copy"
								class="button_c1" onclick="CopyPO();"
								<c:if test="${requestScope.isResource ne 'isResource'}"> disabled</c:if> />

						</c:if></td>

					<td width="2%" align="left" valign="bottom"><c:if
							test="${requestScope.buyTab eq 'buyTab'}">
							<input type="button" width="40" name="build" value="Build"
								onclick="createPO('0');" class="button_c1"
								<c:if test="${requestScope.isResource ne 'isResource'}"> disabled</c:if> />
						</c:if></td>
					<td width="91%"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr valign="top">
		<td colspan="2">
			<table cellspacing="6" cellpadding="0" width="100%"
				class="Ntextoleftalignnowrappaleyellowborder2">
				<tr>
					<td>
						<table width="100%" height="100%" cellpadding=0 cellspacing=1>
							<tr>
								<td colspan="9"></td>
							</tr>
							<tr>
								<td class="texthdNoBorder"><b>PO/WO </td>
								<td class="texthdNoBorder"><b>Status</td>
								<td class="texthdNoBorder"><b>Del</td>
								<td class="texthdNoBorder"><b>Rev </td>
								<td class="texthdNoBorder"><b>PO Type</td>
								<td class="texthdNoBorder"><b>Master Item</td>
								<td class="texthdNoBorder"><b>Auth Cost </td>
								<td class="texthdNoBorder" width="50%"><b>Partner/Contact
								</td>
								<c:if
									test="${requestScope.buyTab eq 'buyTab' || requestScope.executeTab eq 'executeTab'}">
									<td class="texthdNoBorder" colspan=2><b>Actions </td>
								</c:if>
							</tr>
							<c:forEach items="${JobSetUpForm.poId}" var="poListItr"
								varStatus="index">
								<input type="hidden" name="poId"
									value="<c:out value="${JobSetUpForm.poId[index.index]}"/>" />
								<input type="hidden" name="poNumber"
									value="<c:out value="${JobSetUpForm.poNumber[index.index]}"/>" />
								<input type="hidden" name="woNumber"
									value="<c:out value="${JobSetUpForm.woNumber[index.index]}"/>" />
								<input type="hidden" name="status"
									value="<c:out value="${JobSetUpForm.status[index.index]}"/>" />
								<input type="hidden" name="revision"
									value="<c:out value="${JobSetUpForm.revision[index.index]}"/>" />
								<input type="hidden" name="costingType"
									value="<c:out value="${JobSetUpForm.costingType[index.index]}"/>" />
								<input type="hidden" name="authorizedCost"
									value="<c:out value="${JobSetUpForm.authorizedCost[index.index]}"/>" />
								<input type="hidden" name="partnerContact"
									value="<c:out value="${JobSetUpForm.partnerContact[index.index]}"/>" />
								<input type="hidden" name="previousStatus"
									value="<c:out value="${JobSetUpForm.previousStatus[index.index]}"/>" />
								<input type="hidden" name="cancelledPONumber"
									value="<c:out value="${JobSetUpForm.cancelledPONumber[index.index]}"/>" />
								<input type="hidden" name="customerDelCount"
									value="<c:out value="${JobSetUpForm.customerDelCount[index.index]}"/>" />
								<input type="hidden" name="internalDelCount"
									value="<c:out value="${JobSetUpForm.internalDelCount[index.index]}"/>" />
								<input type="hidden" name="totalDelCount"
									value="<c:out value="${JobSetUpForm.totalDelCount[index.index]}"/>" />
								<input type="hidden" name="commissionPO"
									value="<c:out value="${JobSetUpForm.commissionPO[index.index]}"/>" />

								<tr>
									<td
										<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
										align="center"><c:choose>
											<c:when
												test="${JobSetUpForm.commissionPO[index.index] eq '0'}">
												<a href="#"
													onclick="openPO('<c:out value="${JobSetUpForm.poId[index.index]}"/>')"><c:out
														value="${JobSetUpForm.poNumber[index.index]}" /></a>
											</c:when>
											<c:otherwise>
												<c:out value="${JobSetUpForm.poNumber[index.index]}" />
											</c:otherwise>
										</c:choose> <a href="#"
										onclick="viewpowo('<c:out value='${JobSetUpForm.jobId}'/>','<c:out value="${JobSetUpForm.poId[index.index]}"/>', 'po');"><img
											src="images/pdf-icon.gif" width="13" height="13" border="0"
											style="vertical-align: middle;" alt="PO.pdf" /></a> <span
										style="margin-left: 0px;">/</span> <span
										style="margin-left: -2px;"> <c:choose>
												<c:when
													test="${JobSetUpForm.commissionPO[index.index] eq '0'}">
													<a href="#"
														onclick="openPO('<c:out value="${JobSetUpForm.poId[index.index]}"/>')"><c:out
															value="${JobSetUpForm.woNumber[index.index]}" /></a>
												</c:when>
												<c:otherwise>
													<c:out value="${JobSetUpForm.woNumber[index.index]}" />
												</c:otherwise>
											</c:choose> <a href="#"
											onclick="viewpowo('<c:out value='${JobSetUpForm.jobId}'/>','<c:out value="${JobSetUpForm.poId[index.index]}"/>', 'wo');"><img
												src="images/pdf-icon.gif" width="13" height="13" border="0"
												style="vertical-align: middle;" alt="WO.pdf" /></a>
									</span></td>
									<td
										<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
										align="center"><c:out
											value="${JobSetUpForm.status[index.index]}" /></td>
									<td
										<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
										align="center"><c:if
											test="${JobSetUpForm.totalDelCount[index.index] ne '0'}">
											<c:choose>
												<c:when
													test="${JobSetUpForm.totalDelCount[index.index] eq '1' && JobSetUpForm.customerDelCount[index.index] eq '1'}">
													<img src="images/customer_del_single.gif" width="13"
														height="13" border="0" style="vertical-align: bottom;"
														alt="Single Customer Deliverable" />
												</c:when>
												<c:when
													test="${JobSetUpForm.totalDelCount[index.index] ne '0' && JobSetUpForm.totalDelCount[index.index] ne '1' && JobSetUpForm.customerDelCount[index.index] ne '0'}">
													<img src="images/customer_del_multiple.gif" width="13"
														height="13" border="0" style="vertical-align: bottom;"
														alt="Multiple Customer Deliverables" />
												</c:when>
												<c:when
													test="${JobSetUpForm.totalDelCount[index.index] eq '1' && JobSetUpForm.internalDelCount[index.index] eq '1'}">
													<img src="images/internal_del_single.gif" width="13"
														height="13" border="0" style="vertical-align: bottom;"
														alt="Single Internal Deliverable" />
												</c:when>
												<c:when
													test="${JobSetUpForm.totalDelCount[index.index] ne '0' && JobSetUpForm.totalDelCount[index.index] ne '1' && JobSetUpForm.internalDelCount[index.index] ne '0'}">
													<img src="images/internal_del_multiple.gif" width="13"
														height="13" border="0" style="vertical-align: bottom;"
														alt="Multiple Internal Deliverables" />
												</c:when>
											</c:choose>
										</c:if></td>

									<td
										<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
										align="center"><c:out
											value="${JobSetUpForm.revision[index.index]}" /></td>
									<td
										<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
										align="center"><c:out
											value="${JobSetUpForm.costingType[index.index]}" /></td>
									<td
										<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
										align="center"><span
										id='poTypeId<c:out value="${index.index}"/>'><c:out
												value="${JobSetUpForm.masterPOItemType[index.index]}" /></span></td>
									<td
										<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
										align="right"><fmt:formatNumber
											value='${JobSetUpForm.authorizedCost[index.index]}'
											type='currency' pattern='$###,##0.00' /></td>

									<td
										<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
										<c:if test="${JobSetUpForm.partnerContact[index.index] ne ''}">
											<a href="javascript:void(0)"
												onClick="if (window.event || document.layers) showWindow(event,<c:out value="${JobSetUpForm.poId[index.index]}"/>); else showWindow('',<c:out value="${JobSetUpForm.poId[index.index]}"/>);"><img
												name="icon" src="images/icon.gif" width="13" height="13"
												style="vertical-align: middle;" border="0"
												alt="Partner Detail" /></a>
										</c:if> <c:out value="${JobSetUpForm.partnerContact[index.index]}" />
										<c:out value="${JobSetUpForm.primaryContact[index.index]}" />
									</td>
									<c:if
										test="${requestScope.buyTab eq 'buyTab' || requestScope.executeTab eq 'executeTab'}">
										<c:if test="${JobSetUpForm.commissionPO[index.index] eq '0'}">
											<c:if test="${JobSetUpForm.status[index.index] ne ''}">
												<td colspan="2"
													<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
													<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
													style="padding-bottom: 0px; padding-top: 0px;"
													align="center"><c:if
														test="${requestScope.executeTab eq 'executeTab' and (JobSetUpForm.status[index.index] ne 'Draft' && JobSetUpForm.status[index.index] ne 'Cancelled' && JobSetUpForm.status[index.index] ne 'Partner Review')}">
														<SELECT id="actionOnPO_<c:out value="${index.index}"/>"
															class="select" name="actionOnPO" style="width: 110;"
															onchange="POActions( <c:out value="${index.index}"/>, <c:out value="${JobSetUpForm.poId[index.index]}"/>, '<c:out value="${JobSetUpForm.costingType[index.index]}"/>');">
															<c:forEach
																items="${JobSetUpForm.availableActions[index.index]}"
																var="list" varStatus="innerIndex">
																<option value="<c:out value='${list.value}'/>">
																	<c:out value='${list.label}' />
																</option>
															</c:forEach>
															<option value="Process Deliverables">Process
																Deliverables</option>
														</SELECT>
													</c:if> <c:if
														test="${requestScope.buyTab eq 'buyTab' and (JobSetUpForm.status[index.index] eq 'Draft' || JobSetUpForm.status[index.index] eq 'Cancelled' || JobSetUpForm.status[index.index] eq 'Partner Review')}">
														<SELECT id="actionOnPO_<c:out value="${index.index}"/>"
															class="select" name="actionOnPO" style="width: 90;"
															onchange="POActions( <c:out value="${index.index}"/>, <c:out value="${JobSetUpForm.poId[index.index]}"/>, '<c:out value="${JobSetUpForm.costingType[index.index]}"/>');">
															<c:forEach
																items="${JobSetUpForm.availableActions[index.index]}"
																var="list" varStatus="innerIndex">
																<option value="<c:out value='${list.value}'/>">
																	<c:out value='${list.label}' />
																</option>
															</c:forEach>
														</SELECT>
													</c:if></td>
											</c:if>
										</c:if>
										<c:if test="${JobSetUpForm.commissionPO[index.index] eq '1'}">
											<td colspan="2"
												<c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
												<c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>
												style="padding-bottom: 0px; padding-top: 0px;"
												align="center">&nbsp;</td>
										</c:if>
									</c:if>

								</tr>

							</c:forEach>
							<c:if test="${empty JobSetUpForm.poId}">
								<tr>
									<td colspan="10" class="Nhyperevenfont">No Purchase Orders</td>
								</tr>
							</c:if>


						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div id="ajaxdiv" style="position: absolute;"></div>