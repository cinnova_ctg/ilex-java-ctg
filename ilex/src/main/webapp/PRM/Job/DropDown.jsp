<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%
String jspName = "";
if(request.getParameter("fromJsp") != null){
	jspName = request.getParameter("fromJsp");
}
else if(request.getAttribute("fromJsp") != null){
	jspName = (String)request.getAttribute("fromJsp");
}

%>
<%if(jspName.equalsIgnoreCase("Ticket.jsp")){ %>
<html:select name="JobSetUpForm" property = "resource" styleClass = "select">
	<logic:present name="resourceList" scope="request">
		<logic:iterate id="optionGroup" name="resourceList">
		<optgroup label="<bean:write name = "optionGroup" property = "resName" />">
			<html:optionsCollection name = "optionGroup" property = "resourceList" value = "value" label = "label"/> 
		</optgroup>
		</logic:iterate>
	</logic:present>
</html:select>
<%} else if(jspName.equalsIgnoreCase("TicketChangeCriticality.jsp")){%>
<html:select name="TicketChangeCriticalityForm" property = "resource" styleClass = "select">
	<logic:present name="resourceList" scope="request">
		<logic:iterate id="optionGroup" name="resourceList">
		<optgroup label="<bean:write name = "optionGroup" property = "resName" />">
			<html:optionsCollection name = "optionGroup" property = "resourceList" value = "value" label = "label"/> 
		</optgroup>
		</logic:iterate>
	</logic:present>
</html:select>
<%} else{%>
<select id="resource" name="resource" class="select" >
	<option value=" ">select</option>
</select>
<%} %>