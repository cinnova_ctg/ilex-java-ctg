<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left: 4px;;padding-top: 2px;padding-bottom: 0px;" >	
	<tr valign="top" style="padding-top: 5px;">
		<td width="100%">
			<table>
				<tr valign="top">
					<td nowrap class = "dbvaluesmallFontBold" id="header" >Estimated Cost Variations by PO Type </td>
				</tr>
				<tr>
					<td>	
						<table  class="labelowhitebackgroundblueborder" id="CVPT" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td class="labelowhitebackgroundblueborder" width="20%" >&nbsp;
										</td>
										<td class="labelowhitebackgroundblueborder" width="20%" >Labor
										</td>
										<td class="labelowhitebackgroundblueborder" width="20%">Travel
										</td>
										<td class="labelowhitebackgroundblueborder" width="20%">Total
										</td>
										<td class="labelowhitebackgroundblueborder" width="20%"><b>Savings</b>
										</td>
									</tr>
									<tr>
										<td class="labelowhitebackgroundleftalignblueborder">Minuteman
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.minutemanTypeCLC}' type='currency' pattern='$###,##0.00'/>
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.minutemanTypeTHC}' type='currency' pattern='$###,##0.00'/>
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.minutemanTypeTC}' type='currency' pattern='$###,##0.00'/>
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.minutemanTypeCostSavings}' type='currency' pattern='$###,##0.00'/>
										</td>
									</tr>
									<tr>
										<td class="labelowhitebackgroundleftalignblueborder">Speedpay
										</td>
										<td class="labelorightwhitebackgroundblueborder" ><fmt:formatNumber value='${JobSetUpForm.speedpayTypeCLC}' type='currency' pattern='$###,##0.00'/>
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.speedpayTypeTHC}' type='currency' pattern='$###,##0.00'/>
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.speedpayTypeTC}' type='currency' pattern='$###,##0.00'/>
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.speedpayTypeCostSavings}' type='currency' pattern='$###,##0.00'/>
										</td>
									</tr>
									<tr>
										<td class="labelowhitebackgroundleftalignblueborder">PVS
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.PVSTypeCLC}' type='currency' pattern='$###,##0.00'/>
										</td>
										<td class="labelorightwhitebackgrounddblueborder" ><fmt:formatNumber value='${JobSetUpForm.PVSTypeTHC}' type='currency' pattern='$###,##0.00'/>
										</td >
										<td class="labelorightwhitebackgrounddblueborder"><fmt:formatNumber value='${JobSetUpForm.PVSTypeTC}' type='currency' pattern='$###,##0.00'/>
										</td>
										<td class="colDarkNoWidth" >&nbsp;
										</td>
									</tr>
								</table> 
							</td>
						</tr>
					</table>
				</td>			
				<td width="20%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>					
				<td width="30%" valign="top">
					<table cellpadding="0" cellspacing="0" width="80%">
						<tr valign="top">
							<td class="dbvaluesmallFont" valign="top">
								<b>Minuteman</b><br>
									<b>.</b>&nbsp;&nbsp;&nbsp;Hourly rate is $18.<br>
									<b>.</b>&nbsp;&nbsp;&nbsp;3 hour minimum.<br>
									<b>.</b>&nbsp;&nbsp;&nbsp;2 hour onsite and 1 hour travel.<br>
									<b>.</b>&nbsp;&nbsp;&nbsp;Automatic payment bi-weekly <br> &nbsp;&nbsp;&nbsp;&nbsp;following successful offsite and <br> &nbsp;&nbsp;&nbsp;&nbsp;PO completion.
							</td>
							<td width="20%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
							<td class="dbvaluesmallFont" valign="top"> 
								<b>Speedpay</b><br>
									<b>.</b>&nbsp;&nbsp;&nbsp;Hourly rate is $24.<br>
									<b>.</b>&nbsp;&nbsp;&nbsp;Travel - up to 1 hour in 15 <br> &nbsp;&nbsp;&nbsp;&nbsp;minute increments.<br>
									<b>.</b>&nbsp;&nbsp;&nbsp;Automatic payment bi-weekly <br> &nbsp;&nbsp;&nbsp;&nbsp;following successful offsite <br> &nbsp;&nbsp;&nbsp;&nbsp;and PO completion.
							</td>
						</tr>
					</table>
				</td>
			
		
	</tr>
</table>