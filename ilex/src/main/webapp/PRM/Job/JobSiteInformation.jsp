<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<LINK href="styles/content.css" rel="stylesheet" type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script language="JavaScript" src="javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script>

function saveIt(){
	enableAll();
	var url ="JobDashboardAction.do?type=9&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=7&update=update";
	
	document.forms[0].action = url;
	document.forms[0].submit();
	
}



function enableAll(){
	document.forms[0].siteendcustomer.disabled = false;
	document.forms[0].sitegroup.disabled = false;
	document.forms[0].site_country.disabled = false;
	document.forms[0].site_state.disabled = false;
	document.forms[0].sitetimezone.disabled = false;
	document.forms[0].site_zipcode.disabled = false;
	
	document.forms[0].site_number.disabled = false;
	document.forms[0].site_name.disabled = false;
	document.forms[0].site_address.disabled = false;
	document.forms[0].site_city.disabled = false;
	document.forms[0].site_notes.disabled = false;
}


function opensearchsitewindow()
{
	
//	str = "SiteSearch.do?ref=search&jobid=7374&siteid=5461&reftype=Update&appendixid="+appendixid;
	url = "SiteSearch.do?ref=search&jobid=<c:out value='${JobSetUpForm.jobId}'/>&reftype=Update&appendixid=<c:out value='${JobSetUpForm.appendixId}'/>&siteid=<c:out value='${JobSetUpForm.site_id}'/>";
	suppstrwin = window.open( url , '' , 'left = 200 , top = 200 , width = 870 , height = 400 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();
	return true;
}
</script>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left: 5px;padding-right: 5px;">
	<tr>
		<td colspan="3" style="padding-left: 0px;padding-bottom: 5px;" valign="top" nowrap><h2 style="font-size: 14px;padding-left: 7px;">Site Information&nbsp;&nbsp;<span class="dbvalue" style="vertical-align: middle;"><a href="SiteHistoryAction.do?jobid=<c:out value='${JobSetUpForm.jobId}'/>&siteid=<c:out value='${JobSetUpForm.site_id}'/>&from=job&page=jobdashboard&ownerId=1&Appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&viewjobtype=ION">Site History</a></span></h2>
		</td>
	</tr>	
	<tr>		
		<td  valign="top">
			<table cellpadding=0 cellspacing=1 style="padding-left: 5px;" >			
				<tr>
					<td  class = "dbvaluesmallFontBold" colspan="2">Address and Details
				</tr>
				<tr>
					<td class="dbvaluesmallFont" width="30%">Number:
					</td>
					<td>
						<input type="text" class="text1" size="20" name="site_number"  value="<c:out value='${JobSetUpForm.site_number}'/>" disabled/>&nbsp;&nbsp;
						<input type="button" height="15" width="40"  name="select_site" value="Select Site" class="button_c1" onclick="return opensearchsitewindow();"/>
						<input type="hidden" name="site_designator" value="<c:out value='${JobSetUpForm.site_designator}'/>" />
						<input type="hidden" name="id1" value="<c:out value='${JobSetUpForm.id1}'/>" />
						<input type="hidden" name="id2" value="<c:out value='${JobSetUpForm.id2}'/>" />
						
						<input type="hidden" name="siteregion" value="<c:out value='${JobSetUpForm.siteregion}'/>" />
						<input type="hidden" name="sitecategory" value="<c:out value='${JobSetUpForm.sitecategory}'/>" />
						<input type="hidden" name="sitelistid" value="<c:out value='${JobSetUpForm.sitelistid}'/>" />
													
						<input type="hidden" name="sitelatdeg" value="<c:out value='${JobSetUpForm.sitelatdeg}'/>" />
						<input type="hidden" name="sitelatmin" value="<c:out value='${JobSetUpForm.sitelatmin}'/>" />
						<input type="hidden" name="sitelatdirection" value="<c:out value='${JobSetUpForm.sitelatdirection}'/>" />
						<input type="hidden" name="sitelondeg" value="<c:out value='${JobSetUpForm.sitelondeg}'/>" />
						
						<input type="hidden" name="sitelonmin" value="<c:out value='${JobSetUpForm.sitelonmin}'/>" />
						<input type="hidden" name="sitelondirection" value="<c:out value='${JobSetUpForm.sitelondirection}'/>" />
						<input type="hidden" name="sitestatus" value="<c:out value='${JobSetUpForm.sitestatus}'/>" />
						
	
						
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont" >Name:
					</td>
					<td>
						<input type="text" class="text1" size="20" name="site_name" value="<c:out value='${JobSetUpForm.site_name}'/>" disabled/>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont" >Address:
					</td>
					<td>
						<input type="text" class="text1" size="40" name="site_address" value="<c:out value='${JobSetUpForm.site_address}'/>" disabled/>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont" >City:
					</td>
					<td>
					 	<input type="text" class="text1" size="40" name="site_city"  value="<c:out value='${JobSetUpForm.site_city}'/>" disabled/>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td  class="dbvaluesmallFont" >State:
					<td align="left" nowrap>
						<SELECT class="select" name="site_state" style="width:70;" disabled>							
							<c:forEach items="${requestScope.stateList}" var="stateList" varStatus="innerIndex">
									<option value="<c:out value='${stateList.value}'/>"  <c:if test="${JobSetUpForm.site_state eq stateList.label}">selected="selected"</c:if> > <c:out value='${stateList.label}' /></option>
							</c:forEach>
						</SELECT>
						<span class="dbvaluesmallFont">&nbsp;Country:</span>
						<SELECT class="select" name="site_country" style="width:70;" disabled>
							<c:forEach items="${requestScope.countryList}" var="countryList" varStatus="innerIndex">
									<option value="<c:out value='${countryList.value}'/>"  <c:if test="${JobSetUpForm.site_country eq countryList.label}">selected="selected"</c:if> > <c:out value='${countryList.label}' /></option>
							</c:forEach>
						</SELECT>
						<span class="dbvaluesmallFont">&nbsp;Zip Code:</span>
						<input type="text" class="text1" size="5" maxlength="5" name="site_zipcode" value="<c:out value='${JobSetUpForm.site_zipcode}'/>" disabled/>&nbsp;&nbsp;
						
					</td>
				</tr>
				<tr>
					<td  class="dbvaluesmallFont" >Brand:
					<td align="left">
						<SELECT class="select" name="sitegroup" style="width:110;" disabled>
							<c:forEach items="${requestScope.brandList}" var="brandList" varStatus="innerIndex">
								<option value="<c:out value='${brandList.value}'/>"  <c:if test="${JobSetUpForm.sitegroup eq brandList.label}">selected="selected"</c:if> > <c:out value='${brandList.label}' /></option>
							</c:forEach>
						</SELECT>
					</td>
				</tr>
				<tr>
					<td  class="dbvaluesmallFont" >End Customer:
					<td align="left">
						<SELECT class="select" name="siteendcustomer" style="width:110;" disabled>
							<c:forEach items="${requestScope.endCustomersList}" var="endCustomersList" varStatus="innerIndex">
								<option value="<c:out value='${endCustomersList.value}'/>"  <c:if test="${JobSetUpForm.siteendcustomer eq endCustomersList.label}">selected="selected"</c:if> > <c:out value='${endCustomersList.label}' /></option>
							</c:forEach>
						</SELECT>
					</td>
				</tr>
				<tr>
					<td  class="dbvaluesmallFont" valign="top">Special Conditions: </td>
					<td align="left" >
						<textarea cols="60" rows="5" class="textarea" name="site_notes" disabled><c:out value='${JobSetUpForm.site_notes}' /></textarea>
					</td>	
				</tr>
			</table>
		</td>
		<td width="20%">
			&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td valign="top">
			<table cellpadding=0 cellspacing=1  style="padding-left: 5px;" class="Ntextoleftalignnowrappalenoborder">
				<tr>
					<td  class = "dbvaluesmallFontBold" colspan="2">General Information
				</tr>
				<tr>
					<td width="30%" class="dbvaluesmallFont" >Work Location:
					<td align="left">
						<input type="text" class="text1" size="40" name="site_worklocation" value="<c:out value='${JobSetUpForm.site_worklocation}'/>"/>
					</td>
					 
				</tr>
				<tr>
					<td width="30%" class="dbvaluesmallFont" >Directions:
					<td align="left">
						<input type="text" class="text1" size="40" name="site_direction"  value="<c:out value='${JobSetUpForm.site_direction}'/>"/>
					</td>
				</tr>
				<tr>
					<td width="30%" class="dbvaluesmallFont" >Time Zone:
					<td align="left">
						<SELECT class="select" name="sitetimezone" style="width:110;"   value="<c:out value='${JobSetUpForm.sitetimezone}'/>" disabled/>
							<c:forEach items="${requestScope.timezoneList}" var="timezoneList" varStatus="innerIndex">
								<option value="<c:out value='${timezoneList.value}'/>"  <c:if test="${JobSetUpForm.sitetimezone eq timezoneList.value}">selected="selected"</c:if> > <c:out value='${timezoneList.label}' /></option>
							</c:forEach>
						</SELECT>
					</td>
				</tr>
				<tr>				
					<td  class = "dbvaluesmallFontBold" colspan="2"></br>Site Based Uplifts	
				</tr>
				<tr>
					<td width="30%" class="dbvaluesmallFont" >Locality Uplift:
					<td align="left">
						<input type="text" class="text1" size="5" name="locality_uplift"  value="<c:out value='${JobSetUpForm.locality_uplift}'/>"/>
					</td>
					 
				</tr>
				<tr>
					<td width="30%" class="dbvaluesmallFont" >Union:
					<td align="left" class="dbvaluesmallFont">
						<input type="radio"  name="union_site" value="Y" <c:if test="${JobSetUpForm.union_site eq 'Y'}">checked</c:if>/>Yes
						<input type="radio" name="union_site" value="N" <c:if test="${JobSetUpForm.union_site eq 'N'}">checked</c:if>/>No
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont" colspan="2">
						<font color="red"></br>These uplifts are applied on a site basis which will affect all activities.</br> The locality uplift will be applied to all resources. The union uplift will</br> be applied to contract labor only.</br></br> </font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="dbvaluesmallFont" colspan="3" width="440" style="white-space: normal;">
			</br>The above information can only be edited from the site master record under the governing MSA. Once edited, the updated record will need to be reselected to apply the changes to this job.</br></br>
		</td>
	</tr>
	<tr>
		<td>
			<table width="440" cellpadding=0 cellspacing=0 style="padding-left: 5px;padding-right: 5px;"  class="Ntextoleftalignnowrappalenoborder">
				<tr>
					<td  class = "dbvaluesmallFontBold"  colspan="2">Primary Contact
				</tr>
				<tr>
					<td class="dbvaluesmallFont"  width="26%">Name:
					</td>
					<td>
					 	<input type="text" class="text1" size="50" name="primary_Name" value="<c:out value='${JobSetUpForm.primary_Name}'/>"/>&nbsp;
					</td>	
				</tr>
				<tr>
					<td class="dbvaluesmallFont"  width="26%">Phone:
					</td>
					<td>
						<input type="text" class="text1" size="20" name="site_phone"  value="<c:out value='${JobSetUpForm.site_phone}'/>"/>&nbsp;&nbsp;Enter as XXX-XXX-XXXX
					</td>	
				</tr>
				<tr>
					<td class="dbvaluesmallFont"  width="26%">Email:
					</td>
					<td>	
						<input type="text" class="text1" size="50" name="primary_email"  value="<c:out value='${JobSetUpForm.primary_email}'/>"/>
					</td>	
				</tr>
				<tr>
					<td  class = "dbvaluesmallFontBold" colspan="2">&nbsp;
				</tr>
				<tr>
					<td  class = "dbvaluesmallFontBold" colspan="2">Secondary Contact
				</tr>
				<tr>
					<td class="dbvaluesmallFont"  width="26%">Name:
					</td>
					<td>	
					<input type="text" class="text1" size="50" name="secondary_Name"  value="<c:out value='${JobSetUpForm.secondary_Name}'/>"/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont"  width="26%">Phone:
					</td>
					<td>
						<input type="text" class="text1" size="20" name="secondary_phone"  value="<c:out value='${JobSetUpForm.secondary_phone}'/>"/>&nbsp;&nbsp;Enter as XXX-XXX-XXXX
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont"  width="26%">Email:
					</td>
					<td>				
						<input type="text" class="text1" size="50" name="secondary_email"  value="<c:out value='${JobSetUpForm.secondary_email}'/>"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
			</table>
		</td>
		
		<td>
		</td>
		
		<td>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			&nbsp;
		</td>
	</tr>	
	<tr>
		<td colspan="3" align="left">
		<input type="button" height="15" width="40"  name="save" value="Save" class="button_c1" onclick="saveIt()" <c:if test="${empty JobSetUpForm.site_id}">disabled="disabled"</c:if>/>
		<input type="reset" height="15" width="40"  name="cancel" value="Cancel" class="button_c1" onclick=""/>
		</br></br>
		</td>
	</tr>
</table>
