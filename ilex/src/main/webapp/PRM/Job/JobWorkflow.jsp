<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='./dwr/interface/JobSetUpDao.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>
<%
response.setHeader("Pragma","No-Cache");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setDateHeader("Expires",0);
%>
<script>
function getAjaxRequestObject(){
	var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }	
	return ajaxRequest;        
}



	function refreshWorkflowChecklist() {
	
	var ajaxRequest = getAjaxRequestObject();
	
    ajaxRequest.onreadystatechange = function(){
           if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){ 
           var xmlDoc=ajaxRequest.responseXML;        		
           		
           	var id;
			 id = xmlDoc.getElementsByTagName("pendingItems")[0];			    
			 var item_description_1=id.getElementsByTagName("jobWorkflowItemDescription_1")[0].firstChild.nodeValue;		
			 var item_description_2=id.getElementsByTagName("jobWorkflowItemDescription_2")[0].firstChild.nodeValue;		
			 
			 var item_id_1=id.getElementsByTagName("jobWorkflowItemId_1")[0].firstChild.nodeValue;		
			 var item_id_2=id.getElementsByTagName("jobWorkflowItemId_2")[0].firstChild.nodeValue;		
			 
			var item_status_1=id.getElementsByTagName("jobWorkflowItemStatus_1")[0].firstChild.nodeValue;		
			 var item_status_2=id.getElementsByTagName("jobWorkflowItemStatus_2")[0].firstChild.nodeValue;		
			
			var item_date_1=id.getElementsByTagName("jobWorkflowItemReferenceDate_1")[0].firstChild.nodeValue;		
			var item_date_2=id.getElementsByTagName("jobWorkflowItemReferenceDate_2")[0].firstChild.nodeValue;		
			
			var item_user_1=id.getElementsByTagName("jobWorkflowItemUser_1")[0].firstChild.nodeValue;		
			var item_user_2=id.getElementsByTagName("jobWorkflowItemUser_2")[0].firstChild.nodeValue;		
		
			/*alert(item_status_1);		
			if(item_status_1 == '-'){				
				document.getElementById("initialPendingData").innerHTML = "";
				document.getElementById("initialPendingData").innerHTML ='<td  width="100%" colspan="5">No Data Found</td>';
			}*/
			document.getElementById("item_status_0").innerHTML=item_status_1;
			document.getElementById("item_status_1").innerHTML=item_status_2;
			
			document.getElementById("item_description_0").innerHTML=item_description_1;
			document.getElementById("item_description_1").innerHTML=item_description_2;
			
			//document.getElementById("item_user_0").innerHTML=item_user_1;
			//document.getElementById("item_user_1").innerHTML=item_user_2;
			
			//document.getElementById("item_date_0").innerHTML=item_date_1;
			//document.getElementById("item_date_1").innerHTML=item_date_2;
			
			document.getElementById("item_code").value=item_id_1;
			document.getElementById("job1").checked=false;
			enableDisablePendingItems();			 
           }
    }
    var item_code = document.getElementById("item_code").value;
    
		var url ="JobSetUpAction.do?hmode=refreshJobWorkFlow&jobId=<c:out value='${JobSetUpForm.jobId}'/>&item_id="+item_code;
		
        ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send(null); 
	
	
	//alert(ajaxRequestObject);
		//JobSetUpDao.updateWorkflowChecklist(
		//	{callback:function(str){ret=str;},async:false}
		//);
	}
	
	function enableDisablePendingItems() {
		<c:if test="${JobSetUpForm.jobWorkflowItemId[0] ne '0'}">
			var itemcode= document.getElementById("item_code");
			if(!itemcode 
				|| itemcode.value=="" 
				|| itemcode.value=="-" 
				|| itemcode.value=="0" ) {
				document.getElementById("job1").style.visibility='hidden';
			} else {
				document.getElementById("job1").style.visibility='visible';
			}
		</c:if>
	}
</script>
	<table width="100%">
	<c:if test="${(JobSetUpForm.allItemStatus eq 'complete' || JobSetUpForm.allItemStatus ne 'complete') && JobSetUpForm.itemListSize ne '0'}">
		<tr>
			<td><span class = "dbvaluesmallFontBold">Job Workflow </span><span class="dbvalue" style="vertical-align: middle;">(<a href="JobWorkflow.do?jobId=<c:out value='${JobSetUpForm.jobId}'/>"><c:if test="${JobSetUpForm.allItemStatus ne 'complete'}">View</c:if><c:if test="${JobSetUpForm.allItemStatus eq 'complete'}">Complete</c:if></a>)</span></td>
		</tr>
	</c:if>
	<c:if test="${JobSetUpForm.jobWorkflowItemId[0] ne '0'}">
		<tr id="pendingItemDisplay">
			<td>
				<table width="100%" border = "0" height = "100%" class="dbsummtabletopborder" cellpadding=0 cellspacing=1>
					<tr>
						
					   	<td class = "dbvalue"  align="center" colspan="2" width="30%">&nbsp;&nbsp;Status </td>
					   	<td class = "dbvalue"  align="center">Checklist Item</td>
					   	<%--
					   	<td class = "dbvalue" align="center">User </td>
						<td class = "dbvalue" align="center">Date </td>
						--%>
					</tr>	
					<input type="hidden" id="item_code" name="item_code" value="<c:out value='${JobSetUpForm.jobWorkflowItemId[0]}'/>"/>			
					
					<c:forEach items="${JobSetUpForm.jobWorkflowItemId}" var="jobWorkFlowChecklist" varStatus="status" >
					<tr id="initialPendingData">
						<td  width="3%" align="center" valign="middle">
							<c:if test = "${status.index eq 0}">
								<input id="job1" type="checkbox"  class="chkbx1" onclick="refreshWorkflowChecklist();" name="job1"/>
							</c:if>
							<c:if test = "${status.index ne 0}">
								&nbsp;&nbsp;&nbsp;
							</c:if>
						</td>
				   		<td  width="10%" id="item_status_<c:out value="${status.index}"/>" 
				   			<c:if test = "${status.index % '2' eq 0}">class = "hypereven"</c:if>
				   			<c:if test = "${status.index % '2' ne 0}">class = "dbvaluesmall"</c:if> 
				   			style="padding: 3px;" >
				   			<c:if test = "${JobSetUpForm.jobWorkflowItemStatus[status.index] eq ''}">
				   				-
				   			</c:if>
				   			<c:if test = "${JobSetUpForm.jobWorkflowItemStatus[status.index] ne ''}">
				   				<c:out value="${JobSetUpForm.jobWorkflowItemStatus[status.index]}"/>
				   			</c:if>
				   		</td>	
						<td id="item_description_<c:out value="${status.index}"/>" 
							<c:if test = "${status.index % '2' eq 0}">class = "hypereven"</c:if>
							<c:if test = "${status.index % '2' ne 0}">class = "dbvaluesmall"</c:if> 
				   			style="padding: 3px;" >
				   			<c:if test = "${JobSetUpForm.jobWorkflowItemDescription[status.index] eq '' 
				   				|| empty JobSetUpForm.jobWorkflowItemDescription[status.index]}">
				   				-
				   			</c:if>
				   			<c:if test = "${JobSetUpForm.jobWorkflowItemDescription[status.index] ne '' 
				   				&& not empty JobSetUpForm.jobWorkflowItemDescription[status.index]}">
				   				<c:out value="${JobSetUpForm.jobWorkflowItemDescription[status.index]}"/>
				   			</c:if> 
						</td>
						<%--
						<td align="center" id="item_user_<c:out value="${status.index}"/>" 
						<c:if test = "${status.index % '2' eq 0}">class = "hyperevencenter"</c:if>
						<c:if test = "${status.index % '2' ne 0}">class = "dbvaluesmall"</c:if> 
			   			style="padding: 3px;" >
			   			<c:if test = "${JobSetUpForm.jobWorkflowItemUser[status.index] eq '' 
			   				|| empty JobSetUpForm.jobWorkflowItemUser[status.index]}">
			   				-
			   			</c:if>
			   			<c:if test = "${JobSetUpForm.jobWorkflowItemUser[status.index] ne '' 
			   				&& not empty JobSetUpForm.jobWorkflowItemUser[status.index]}">
			   				<c:out value="${JobSetUpForm.jobWorkflowItemUser[status.index]}"/>
			   			</c:if>
					</td>	
					<td align="center" id="item_date_<c:out value="${status.index}"/>"
						<c:if test = "${status.index % '2' eq 0}">class = "hyperevencenter"</c:if>
						<c:if test = "${status.index % '2' ne 0}">class = "dbvaluesmall"</c:if> 
			   			style="padding: 3px;" >
			   			<c:if test = "${JobSetUpForm.jobWorkflowItemReferenceDate[status.index] eq '' 
			   				|| empty JobSetUpForm.jobWorkflowItemReferenceDate[status.index]}">
			   				-
			   			</c:if>
			   			<c:if test = "${JobSetUpForm.jobWorkflowItemReferenceDate[status.index] ne '' 
			   				&& not empty JobSetUpForm.jobWorkflowItemReferenceDate[status.index]}">
			   				<c:out value="${JobSetUpForm.jobWorkflowItemReferenceDate[status.index]}"/>
			   			</c:if>
					</td>	
					--%>
					</tr>	
					</c:forEach>		
				</table>
			</td>
		</tr>
	</c:if>
	
<script>
	enableDisablePendingItems();
</script>