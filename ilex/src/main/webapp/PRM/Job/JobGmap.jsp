<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%String size = "0";
if(request.getAttribute("size") != null )
	size = request.getAttribute("size")+"";
%>
<script>
	var gicons = [];
	var baseIcon = new GIcon();
	
	gicons["Cred"] =  "images/red1_p.png";
	gicons["Mred"] =  "images/red1_m.png";
	gicons["Sred"] =  "images/red1_s.png";
	
	gicons["Cgreen"] =  "images/green1_p.png";
	gicons["Mgreen"] =  "images/green1_m.png";
	gicons["Sgreen"] =  "images/green1_s.png";
	
	gicons["Cyellow"] =  "images/yellow1_p.png";
	gicons["Myellow"] =  "images/yellow1_m.png";
	gicons["Syellow"] =  "images/yellow1_s.png";
	
	gicons["Corange"] =  "images/orange1_p.png";
	gicons["Morange"] =  "images/orange1_m.png";
	gicons["Sorange"] =  "images/orange1_s.png";
	
	gicons["Black"] =  "images/black_circle.gif";
	gicons["Blue"] =  "images/blue_img.png";
	gicons["Purple"] =  "images/Puple_img.png";
</script>

<script>	 
	function onLoad() {
	  if (GBrowserIsCompatible()) {
		var map = new GMap(document.getElementById("map"));
			map.addControl(new GLargeMapControl());
	  		map.addControl(new GMapTypeControl());
			<%if(Integer.parseInt(size) > 0){ // - marker list should greater then 0%>
	        <%if(request.getAttribute("siteXML") != null){ // - for Site All Partner,All Jobs: End%>
				var xmlDoc1 = GXml.parse(<%=request.getAttribute("siteXML").toString()%>);
	       		var site = xmlDoc1.documentElement.getElementsByTagName("site");
	       		//alert(site.length)
	        	if(site.length > 0){
		        	var dispStr = "";
		        	for (var i = 0; i < site.length; i++) {
			            var siteLat = parseFloat(site[i].getAttribute("lat"));
			            var siteLon = parseFloat(site[i].getAttribute("lon"));
			            if(i<1){
			        		map.setCenter(new GLatLng(siteLat,siteLon), 13);
					        map.setZoom(8);
			        	}
						var siteNumber = site[i].getAttribute("siteNumber");
						var siteAddress = site[i].getAttribute("siteAddress");
						var siteCity = site[i].getAttribute("siteCity");
						var siteState = site[i].getAttribute("siteState");
						var siteZipCode = site[i].getAttribute("siteZipCode");
						var icontype = site[i].getAttribute("icontype");
						var msaName = site[i].getAttribute("msaName");
						var appendixName = site[i].getAttribute("appendixName");
			            var point = new GLatLng(siteLat,siteLon);
			            map.setCenter(new GLatLng(siteLat,siteLon), 13);
			            
			            dispStr="<table border=0 Cellspacing=0 Cellpading=0>";
			            if(msaName != '') {
			            	dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" + msaName + "</td></tr>" ;					
			            }
			            if(appendixName != '') {
			            	dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" + appendixName + "</td></tr><tr><td height=10></td></tr>" ;					
			            }
			            if(siteNumber != '') {
			            	dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" + siteNumber + "</td></tr>" ;					
			            }
			            if(siteAddress != '') {
							dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" +siteAddress+ "</td></tr>" ;					            
			            }
			            if(siteCity != '' || siteState != '' || siteZipCode || '') {
			            	dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" + siteCity + " " + siteState + " " + siteZipCode+ "</td></tr>" ;					
			            }
			            dispStr=dispStr+"</table>";
			            
			            var marker = createMarker(point,dispStr,icontype);
			            map.addOverlay(marker);
		          	}
	          	}
        	<%} // - for Site All Partner,All Jobs: End%>
        	
        	<%if(request.getAttribute("partnerXML") != null){ // - fro Site All Partner: Start%>
	       	 	var xmlDoc = GXml.parse(<%=request.getAttribute("partnerXML").toString()%>); 
		        var partners = xmlDoc.documentElement.getElementsByTagName("partner");
		        //alert(partners.length)
	        	if(partners.length > 0){
		        	var str="";
		          	var styleStr="style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'";
			        for (var i = 0; i < partners.length; i++) {	        	
						var lat = parseFloat(partners[i].getAttribute("lat"));
			            var lon = parseFloat(partners[i].getAttribute("lon"));
			            if(i<1){
			        		map.setCenter(new GLatLng(lat,lon), 13);
					        map.setZoom(8);
			        	}
			            
						var partnerName = partners[i].getAttribute("partnerName");
						var partnerType = partners[i].getAttribute("partnerType");
						var partnerAddress1 = partners[i].getAttribute("partnerAddress1");
						var partnerAddress2 = partners[i].getAttribute("partnerAddress2");
						var partnerCity = partners[i].getAttribute("partnerCity");
						var partnerState = partners[i].getAttribute("partnerState");
						var partnerZipCode = partners[i].getAttribute("partnerZipCode");
						var partnerPhone = partners[i].getAttribute("partnerPhone");
						var partnerPriName = partners[i].getAttribute("partnerPriName");
						var partnerPriPhone = partners[i].getAttribute("partnerPriPhone");
						var partnerPriCell = partners[i].getAttribute("partnerPriCell");
						var partnerPriEmail = partners[i].getAttribute("partnerPriEmail");
						var partnerDistance = partners[i].getAttribute("partnerDistance");
						var icontype = partners[i].getAttribute("icontype");
						var partnerId = partners[i].getAttribute("partnerId");
						var point = new GLatLng(lat,lon);
							
						 str="<table border=0 cellpadding=0 cellspacing=0 ><tr><td "+styleStr+">" +partnerName+ "</td></tr>" +
												""+"<tr><td "+styleStr+">Partner Type:&nbsp;" +partnerType+ "</td></tr>";
						
		 				if(partnerAddress1 == '' && partnerAddress2 == '' ) {
		 				    str=str;
		 				    
		           		} else {
		           			str=str+"<tr><td "+styleStr+">" +partnerAddress1+ "&nbsp;" +partnerAddress2+ "</td></tr>";
		           		}	
			       		if(partnerCity  != '' || partnerState != '' || partnerZipCode != '') {
		           			str=str+"<tr><td "+styleStr+">" + partnerCity + ", " + partnerState + " " + partnerZipCode +  "</td></tr>";
		           		}
		          		if(partnerPhone != '') {
		           			str=str+"<tr><td "+styleStr+">"+ partnerPhone +  "</td>";
		           		}
						str=str+"</tr><tr height=5><td></td></tr><tr><td "+styleStr+"></td></tr><tr><td " +styleStr+">Primary Contact</td></tr><tr><td style='padding-left:12px'><table border=0 cellspacing=0 cellpadding=0>";
								
						
			           if(partnerPriName != '') {
		           			str=str+"<tr><td "+styleStr+">" + partnerPriName+ "</td></tr>";
		           		}
		           							
		           		if(partnerPriPhone == '-' || partnerPriPhone == '') {
		           			str=str;
		           		}
		           		else {
		           			str=str+"<tr><td "+styleStr+">" + partnerPriPhone+ "</td></tr>";
		           		}
		      			if(partnerPriCell != '' ) {
		           			str=str+"<tr><td "+styleStr+">"+ partnerPriCell+ " (Cell)</td></tr>";
		           		}
		           		if(partnerPriEmail != '') {
		           			str=str+"<tr><td "+styleStr+">" + partnerPriEmail+ "</td></tr>";
		           			
		           		}
		           			
						str=str+"</table></td></tr><tr height=5><td></td></tr><tr><td "+styleStr+">Distance:&nbsp;" + partnerDistance+ "</td></tr>";
						
			       	 	var xmlDocpo = GXml.parse(<%=request.getAttribute("poXml").toString()%>); 
				        var po = xmlDocpo.documentElement.getElementsByTagName("po");
				        if(po.length > 0) {
				        	str = str+"<tr><td "+styleStr+">Assign to PO: ";
				        	for( var j =0; j< po.length ;j++) {
				        		str = str+"<a href = \"#\" onclick=\"return assignpartner(" +partnerId+","+po[j].getAttribute("poId")+","+po[j].getAttribute("jobId")+");\">" + po[j].getAttribute("poId")+ "</a>";
				        		if(j!=(po.length)-1)
				        			str = str + ", ";
				        	}
				        	str = str + "</td></tr>";
				        }
				        str = str + "</table>";
				        
				        
		            	//str = str+"<tr><td "+styleStr+">Assign to PO: <a href = \"#\" onclick=\"return assignpartner(2344);\">" + 65656+ "</a></td></tr></table>";
		            	
			            var marker = createMarker(point,str,icontype);
			            map.addOverlay(marker);
			          
		         }
	         }
        <%}// - for Site All Partner: End %>
		 <%}else { // - when no marker present%>       
		 	
		 <%}%>
	  }
	}
</script>

<script>
	function createMarker(point,str,icontype) {
		baseIcon.image=gicons[icontype];
		if(icontype=="Black" ||icontype=="Blue" || icontype=="Purple") {
			baseIcon.shadow="http://www.google.com/mapfiles/shadow50.png";
			baseIcon.iconSize = new GSize(20, 20);
		 	baseIcon.shadowSize = new GSize(37, 20);
			baseIcon.iconAnchor = new GPoint(9, 20);
			baseIcon.infoWindowAnchor = new GPoint(9, 2);
			baseIcon.infoShadowAnchor = new GPoint(18, 25);
		}
		else {
			baseIcon.shadow= "images/star_shadow.png";
			baseIcon.iconSize = new GSize(20, 34);
			baseIcon.shadowSize = new GSize(37, 34);
			baseIcon.iconAnchor = new GPoint(9, 34);
			baseIcon.infoWindowAnchor = new GPoint(9, 2);
			baseIcon.infoShadowAnchor = new GPoint(18, 25);
		}
		
		var marker = new GMarker(point,baseIcon);
		GEvent.addListener(marker, "click", function() {marker.openInfoWindowHtml(str);} );
		return marker;
	}

</script>

<script>
	function mapViewChange(str){
		document.forms[0].action = 'JobDashboardAction.do?type=10&tabId=10&isClicked=isClicked&mapView='+str;
		document.forms[0].submit();
	}
	function assignpartner(pid,powoId,jobId)
	{
		/*document.forms[0].selectedpartner.value = pid;
		document.forms[0].currentPartner.value = pid;
		document.forms[0].isClicked.value="1";
		document.forms[0].ref.value = "assign";
		document.forms[0].hiddensave.value="save";
		document.forms[0].submit();*/
		document.forms[0].action = 'Partner_Search.do?ref=assign&tabId=3&hiddensave=save&isClicked=1&formtype=powo&typeid='+jobId+'&currentPartner='+pid+'&powoId='+powoId;
		document.forms[0].submit();
		return true;
		
	}
</script>
<table>
	<logic:notEqual name="JobSetUpForm" property="mapView" value="allJobsAllPartners">
	<tr>
		<td class = "dbvaluesmallFont" align="left">Site, All Partners</td>
		<td class = "dbvaluesmallFont" align="right"><a href="##" onclick="mapViewChange('allJobsAllPartners');" >Site, All Partners, All Jobs</<a></td>
	</tr>
	</logic:notEqual>
	
	<logic:equal name="JobSetUpForm" property="mapView" value="allJobsAllPartners">
	<tr>
		<td class = "dbvaluesmallFont" align="left">Site, All Partners, All Jobs&nbsp;&nbsp;&nbsp;&nbsp;
		<html:radio name = "JobSetUpForm" property="selection" value="notAll" onclick="mapViewChange('allJobsAllPartners');" >Only Current Appendix</html:radio>&nbsp;&nbsp;&nbsp;&nbsp;
		<html:radio name = "JobSetUpForm" property="selection" value="all" onclick="mapViewChange('allJobsAllPartners');" >All Jobs</html:radio>
		<!-- 
		<input type="radio" name = "selection" value="notAll" onclick="mapViewChange('allJobsAllPartners');"/> Only Current Appendix
		<input type="radio" name = "selection" value="all" onclick="mapViewChange('allJobsAllPartners');"/> All Jobs
		 -->
		</td>
		<td class = "dbvaluesmallFont" align="right"><a href="##" onclick="mapViewChange('allPartners');" >Site, All Partners</a></td>
	</tr>
	</logic:equal>
	<tr>
		<td colspan="2">		 	   	
			<div id="map" style="width: 950px; height: 400px"></div> 
		</td>
	</tr>
	<tr>
		<td colspan="2">		 	   	
			<table border="0" cellpadding="0" cellspacing="0" width=950>
		  	 	<tr>
		  	 		<td colspan=2 class = labellegend style= "padding-left: 6px;">Legend:</td>
		  	 	</tr>
		  	 	<tr >
		  	 		<td colspan=2></td>
		  	 	</tr>
		  	 	<tr>	
		  	 		<td class = labellegend style= "padding-left: 18px;">Rating: 0.0 -<IMG SRC="images/red1_no_title.png" WIDTH=15 HEIGHT=15 ALT="">- 3.5&nbsp;&nbsp;&nbsp;3.51 -<IMG SRC="images/yellow1_no_title.png" WIDTH=15 HEIGHT=15 ALT="">- 4.5&nbsp;&nbsp;&nbsp;4.51 -<IMG SRC="images/green1_no_title.png" WIDTH=15 HEIGHT=15 ALT="">- 5.0</td>
		  	 		<td class = labellegend style= "padding-left: 18px;">Restricted Partner: <IMG SRC="images/orange1_no-title.png" WIDTH=15 HEIGHT=15 ALT=""></td>  	 		
		  	 		<td class = labellegend  style= "padding-left: 18px;">Customer Site:</td>
			  	 	<td align="left"  style="padding-top: 3px"><IMG SRC="images/black_circle.gif" width=15 height=15 ALT=""></td>
			  	 	<td class = labellegend  style= "padding-left: 18px;">Additonal Sites:</td>
			  	 	<td align="left"  style="padding-top: 3px"><IMG SRC="images/blue_img.png" width=15 height=15 ALT="">&nbsp;</IMG></td>
			  	 	<td class = labellegend>(Current Project)</td>
			  	 	<td align="left"  style="padding-top: 3px"><IMG SRC="images/Puple_img.png" width=15 height=15 ALT="">&nbsp;</IMG></td>
			  	 	<td class = labellegend>(Other)</td>
		  	 	</tr>
		  	 	<tr>
		  	 		<td  class = labellegend style= "padding-left: 18px;">P = Certified Partner,  M = Minuteman,  S = Speedpay</td>
		  	 	</tr>
		  	 </table>
		</td>
	</tr>
</table>


