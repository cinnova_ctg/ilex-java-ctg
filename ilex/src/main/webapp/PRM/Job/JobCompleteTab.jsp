<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@page import="com.mind.newjobdb.formbean.JobDashboardForm"%>
<table border=0>
	<tr>
		<td valign="top">
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
	<tr>
		<td valign="top">
			<jsp:include page="JobWorkflow.jsp"/>
		</td>
	</tr>
	<tr>
		<td valign="bottom">
			<table border="0" width="100%">
				<tr>
					<td  valign="top" width="50%">
						<bean:define id="jobId" name="JobSetUpForm" property="jobId"></bean:define>
						<bean:define id="appendixId" name="JobSetUpForm" property="appendixId"></bean:define>
						<bean:define id="jobStatus" name="JobSetUpForm" property="jobStatus"></bean:define>
						<jsp:include page="/JobCompleteAction.do?" >
							<jsp:param name="jobid" value="<%=jobId %>" />
							<jsp:param name="appendixId" value="<%=appendixId %>" />
							<jsp:param name="jobStatus" value="<%=jobStatus %>" />
							<jsp:param name="throughJobDashboard" value="Y" />
							<jsp:param name="Status" value="F" />
						</jsp:include>
					</td>
					<td width="3%" valign="top">&nbsp;
					</td>
					<td valign="top" width="45%" style="padding-top: 2px">
						<table border="0" width="100%">
								<tr>
									<td valign="top">
									<jsp:include page="PurchaseOrder.jsp"/>
									</td>
								</tr>
								<c:if test="${JobSetUpForm.jobStatus eq 'Complete' and JobSetUpForm.msaName ne 'XO Communications'}">
									<tr>
										<td valign="top"  style="padding-left:5px;">
											<bean:define id="jobId" name="JobSetUpForm" property="jobId"></bean:define>
												<jsp:include page="/CustomerRequireDataAction.do?" >
												<jsp:param name="jobId" value="<%=jobId %>" />
												<jsp:param name="function" value="view" />
											</jsp:include>		
										</td>
									</tr>
								</c:if>
						</table>
					</td>
				</tr>		
			</table>			
		</td>
	</tr>
	<tr>
		<td  valign="top"  style="padding-top: 0px">
			<jsp:include page="JobInstallationNotes.jsp"/>					
		</td>
	</tr>
	<tr>		
		<td  valign="top">
			<jsp:include page="JobPlanningNotes.jsp"/>
		</td>
	</tr>
</table>