<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
<body>
<table border="0" style="padding-left: 5px;" cellpadding="0" cellspacing="0">
	<tr>
		<td class = "dbvaluesmallFontBold" style="padding-left: 5px;padding-bottom: 5px;" nowrap>Customer Required Data/Reporting
	</td>
	</tr>
	<tr >
		<td>
			<table cellspacing="1" cellpadding="0" class="Ntextoleftalignnowrappaleyellowborder2">
				<tr>
				<td>
					<table class="Ntextoleftalignnowrappaleyellowborder2">
						<tr>
						   	<td class = "texthdNoBorder" align="center"><b>Customer Data </td>
						   	<td class = "texthdNoBorder"  align="center"><b>Value</td>
						</tr>
						<c:if test="${empty requestScope.requiredDataList}">
						<tr>
							<td class="Nhyperevenfont"  style="padding-left: 10px;" colspan="2">
								No Parameters Defined.
							</td>
						</tr>
						</c:if>	
						
						<c:if test="${(requestScope.isSnapon ne true)}">
						<c:forEach items="${requestScope.requiredDataList}" var="requiredDataList" varStatus="index">
							<tr>
								<td <c:if test="${(index.index % 2)eq '0'}">class='Ntexteleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntextoleftalignnowrap'</c:if>>
									<c:out value='${requiredDataList.infoParameter}'/>
								</td>
								<td <c:if test="${(index.index % 2)eq '0'}">class='Ntexteleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntextoleftalignnowrap'</c:if>>
									<input type="hidden" name="appendixCustInfoId<c:out value='${index.index}'/>" value="<c:out value='${requiredDataList.infoId}' />"/>
									<input type="text" class="text"  readonly="readonly" size="30" name="custValue<c:out value='${index.index}'/>" value="<c:out value='${requiredDataList.value}' />" />
								</td>
							</tr>
						</c:forEach>
						</c:if>
						<c:if test="${(requestScope.isSnapon eq true)}">
						<c:forEach items="${requestScope.requiredDataList}" var="requiredDataList" varStatus="index">
							<tr valign="top">
								<td valign="top" <c:if test="${(index.index % 2)eq '0'}">class='Ntexteleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntextoleftalignnowrap'</c:if>>
									<c:out value='${requiredDataList.infoParameter}'/>
								</td>
								<td valign="top" <c:if test="${(index.index % 2)eq '0'}">class='Ntexteleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntextoleftalignnowrap'</c:if> width="175">
									<input type="hidden" name="appendixCustInfoId<c:out value='${index.index}'/>" value="<c:out value='${requiredDataList.infoId}'/>"/>
									<input type="text" readonly="readonly" class="text" size="30" name="custValue<c:out value='${index.index}'/>" value="<c:out value='${requiredDataList.value}'/>"/></br>
									
								</td>
							</tr>
						</c:forEach>
						</c:if>
						
					</table>
				</td>
				</tr>
			</table>
		</td>		
	</tr>
</table>	

</body>
</html>