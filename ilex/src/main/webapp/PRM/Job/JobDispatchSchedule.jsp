<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<table>
	<tr>
		<td>
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
	<tr>
		<td width="50%">
			<jsp:include page="JobDispatchScheduleTab.jsp"/>		
		</td>
	</tr>
</table>
