<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<link rel = "stylesheet" href="styles/content.css" type="text/css">

<script language = "JavaScript" src = "PRM/Job/js/schedule.js"></script>

<script>
    // API Settings
    var api_url = '<%= (String) request.getAttribute("api_url") %>';
    var api_user = '<%= (String) request.getAttribute("api_user") %>';
    var api_pass = '<%= (String) request.getAttribute("api_pass") %>';
    
    var job_id = '<c:out value='${JobSetUpForm.jobId}'/>';
    
    var user_id = '<c:out value='${sessionScope.userid}'/>';
    
function saveActions(){
	var url;
	var i;
	var k = 0;
	var deleteCount;
	if (document.getElementById('dynatable').rows.length==0) return true;
	
	if(eval(document.getElementById('dynatable').rows.length) != 8)
		i = eval(document.getElementById('dynatable').rows.length)-2; // For single schedule
	else
		i = eval(document.getElementById('dynatable').rows.length); // For multiple schedule
		
	i = i/7;
	
	/* For Job Reschedule: Start */
	try{
		if(!validateRse()){
			return false;
		} // - for validation of reschedule.
	}catch(e){}
	/* For Job Reschedule: End */
	for (var j=0;j<eval(document.forms[0].rownum.value);j++){
		k=j+1;
		
		if(document.getElementById('startdate'+j)!= null) {
		}
		else {
		continue;
		}
		/*
		if(isBlank(document.getElementById('startdate'+j).value) && isBlank(document.getElementById('actstartdate'+j).value) && isBlank(document.getElementById('actenddate'+j).value))
		{
			alert("Site Visit "+k+": <bean:message bundle="PRM" key="prm.dispatch.schedule.selectdates"/>");
		   	return false;
		}
		
		if(document.getElementById('scheduleEnddate'+j).value!="" && document.getElementById('scheduleEnddate'+j).value!="")
		{
			if (!compDate_mdy(document.getElementById('startdate'+j).value, document.getElementById('scheduleEnddate'+j).value)) 
			{
				alert("Site Visit "+k+": Schedule End Date should be greater then Schedule Start Date.");
				return false;
			} 
		}
		if(document.getElementById('actstartdate'+j).value!="" && document.getElementById('actenddate'+j).value!="")
		{
			if (!compDate_mdy(document.getElementById('actstartdate'+j).value, document.getElementById('actenddate'+j).value)) 
			{
				document.getElementById('actenddate'+j).focus();
				alert("Site Visit "+k+": Actual End Date should be greater then Actual Start Date.");
				return false;
			} 
		}
		*/
		
		if(document.getElementById('startdatehh'+j).value != "00")
 		{
 			if(document.getElementById('startdate'+j).value == "") {
				alert("Please Select Start Date.");	
 				return false;
 			}
 		}
 		
 		if(document.getElementById('scheduleEndhh'+j).value != "00")
 		{
 			if(document.getElementById('scheduleEnddate'+j).value == "") {
				alert("Please Select End Date.");	
 				return false;
 			}
 		}
 		
 		if(document.getElementById('actstartdatehh'+j).value != "00")
 		{
 			if(document.getElementById('actstartdate'+j).value == "") {
				alert("Please Select Actual Start Date.");	
 				return false;
 			}
 		}
 		
 		if(document.getElementById('actenddatehh'+j).value != "00")
 		{
 			if(document.getElementById('actenddate'+j).value == "") {
				alert("Please Select Actual End Date.");	
 				return false;
 			}
 		}
 		
 		if(document.getElementById('startdate'+j).value!="" && document.getElementById('scheduleEnddate'+j).value!="") 
		{
			if (!compDate_mdy(document.getElementById('startdate'+j).value, document.getElementById('scheduleEnddate'+j).value)) 
			{
				alert("Planned End Date Should be greater than Start Date");
				return false;
			} 
		}
		
		if(document.getElementById('actstartdate'+j).value!="" && document.getElementById('actenddate'+j).value!="") 
		{
			if (!compDate_mdy(document.getElementById('actstartdate'+j).value, document.getElementById('actenddate'+j).value)) 
			{
				alert("Actual End Should be greater than Actual Start");
				return false;
			} 
		}
	
		if(document.getElementById('actstartdate'+j).value!="" && document.getElementById('actenddate'+j).value!="") 
		{
			if (compDate_mdy_Greater_By_5(document.getElementById('actstartdate'+j).value, document.getElementById('actenddate'+j).value)) 
			{
				
				var answer = confirm("This date differs the onsite date by more than 5 days.  Are you sure this date is correct?")
				if (!answer){
					return false;
				}
			} 
			
		}
		
		//5 min difference check
		var alertDisplay=false;
		if(actualStartChangeStatus)
			{
			if (jQuery.inArray(j,actualStartDateChangeRows) > -1) {
				actualStartDateChangeRows.splice(jQuery.inArray(j, actualStartDateChangeRows),1);
				if(document.getElementById('actstartdate'+j).value!="" && document.getElementById('startdate'+j).value!="") 
				{
					var startdate=document.getElementById('startdate'+j).value;
					var startdatehh=document.getElementById('startdatehh'+j).value;
					var startdatemm=document.getElementById('startdatemm'+j).value;
					var startdateop=document.getElementById('startdateop'+j).value;
					
					var actstartdate=document.getElementById('actstartdate'+j).value;
					var actstartdatemm=document.getElementById('actstartdatemm'+j).value;
					var actstartdatehh=document.getElementById('actstartdatehh'+j).value;
					var actstartdateop=document.getElementById('actstartdateop'+j).value;	
					var status =compareStartEndDate(startdate,startdatehh,startdatemm,startdateop,actstartdate,actstartdatehh,actstartdatemm,actstartdateop);
					if (status) 
					{
					var z=j+1;
					alert("Site visit "+z+" may count as a late occurrence unless you have contacted the customer at least 30 minutes prior to the schedule start time and revise the schedule start time to the newly agreed upon time.");
					alertDisplay=true;
					}
				}
			}
		}
		if(!alertDisplay)
			{
			if(document.getElementById('actstartdate'+j).value!="" && document.getElementById('startdate'+j).value!="") 
			{
				var startdate=document.getElementById('startdate'+j).value;
				var startdatehh=document.getElementById('startdatehh'+j).value;
				var startdatemm=document.getElementById('startdatemm'+j).value;
				var startdateop=document.getElementById('startdateop'+j).value;
				
				var actstartdate=document.getElementById('actstartdate'+j).value;
				var actstartdatemm=document.getElementById('actstartdatemm'+j).value;
				var actstartdatehh=document.getElementById('actstartdatehh'+j).value;
				var actstartdateop=document.getElementById('actstartdateop'+j).value;
				var actstartdatePrev=document.getElementById('actstartdatePrev'+j).value;
				var status =compareStartEndDate(startdate,startdatehh,startdatemm,startdateop,actstartdate,actstartdatehh,actstartdatemm,actstartdateop);
				if (status && (actstartdate!=actstartdatePrev)) 
				{
				var z=j+1;
				alert("Site visit "+z+" may count as a late occurrence unless you have contacted the customer at least 30 minutes prior to the schedule start time and revise the schedule start time to the newly agreed upon time.");
				}
			}
			}
	
	}
	
	
		url = "JobScheduleAction.do?hmode=Save&type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=1&saveJobActions=1";							
		document.forms[0].action=url;
		document.forms[0].submit();
}
</script>

<table class="Ntextoleftalignnowrappalenoborder" border="0" width="940">
    <tr>
        <td colspan="2">
            <div id="schedule_element_area" style="padding: 10px;"></div>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td> 
            <table cellpadding="0" cellspacing="1" border="0" width="100%">
                <tr>
                    <td>
                        <button id="save_schedules" class="button_c">Save</button>
                        <button id="reset_schedules" class="button_c">Reset</button>
                    </td>
                    <td align="right">
                        <button id="add_site_visit" class="button_c">Add Site Visit</button>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>