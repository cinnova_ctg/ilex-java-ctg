<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<head>
</head>

<table cellpadding="0" cellspacing="1" border="0" width="100%">
	<tr>
		<td colspan="1" class="dbvaluesmallFont" width = 60%><b>Activity: Troubleshoot and Repair </b></td>
		<td colspan="3" class="dbvaluesmallFont" align="right"><b>Request Type: <c:out value='${JobSetUpForm.ticketType}'/></b></td>
	</tr>
	<tr>
		<td class = "texthdNoBorder">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td class = "texthdNoBorder">
					Resource 
				</td>
			<c:choose>
				<c:when test="${JobSetUpForm.jobStatus eq 'Closed'}">
				<td class = "texthdNoBorder">
					<a href ="#">Add</a>
					</c:when>
					<c:otherwise>
					<td class = "texthdNoBorder">
					<a href ="ResourceListAction.do?resourceListType=A&Activity_Id=<c:out value='${JobSetUpForm.ticketActivityId}'/>&ref=inscopedetailactivity&from=jobdashboard&jobType=inscopejob&editable=editable">Add</a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</table>
		</td>
		<td class = "texthdNoBorder">Quantity</td>
		<td class = "texthdNoBorder">Unit Price</td>
		<td class = "texthdNoBorder">Total</td>
	</tr>
	
	<c:forEach items="${JobSetUpForm.ticketResourceSummary}" var="resourceSummary" varStatus="index">
	<tr>
		<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
			<c:out value="${resourceSummary.resource_name}"/>
		</td>
		<td align="right" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
			<fmt:formatNumber value="${resourceSummary.resource_quantity}" type="currency" pattern="######0.0000"/>
		</td>
		<td align="right" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
			<fmt:formatNumber value="${resourceSummary.resource_unit_price}" type="currency" pattern="#,###,##0.00"/>
		</td>
		<td align="right" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>>
			<fmt:formatNumber value="${resourceSummary.resource_total_price}" type="currency" pattern="#,###,##0.00"/>
		</td>
		
	</tr>
	</c:forEach>
	<c:if test="${empty JobSetUpForm.ticketResourceSummary}">
		<tr>
			<td colspan="4" class="Nhyperevenfont">No Resource</td> 
		</tr>
	</c:if>
	<tr><td colspan="4" height="5"></td></tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" class="labelorightwhitebgblueborder" width="100%">
	<tr><td height="5"></td></tr>
	<tr>
		<td valign="top">
			<table cellpadding="0" cellspacing="0" border="0" align="left">
				<tr>
					<td class="dbvaluesmallFont">
						<b>Total Extended Price:</b>&nbsp;&nbsp;&nbsp;
					</td>
					<td class="dbvaluesmallFont" align="right">
						<fmt:formatNumber value="${JobSetUpForm.total_extended_price}" type="currency" pattern="#,###,##0.00"/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						<b>Actual Cost:</b>
					</td>
					<td class="dbvaluesmallFont" align="right">
						<fmt:formatNumber value="${JobSetUpForm.actual_cost}" type="currency" pattern="#,###,##0.00"/>
					</td>
				</tr>
				<tr>
					<td class="dbvaluesmallFont">
						<b>VGPM:</b>
					</td>
					<td class="dbvaluesmallFont" align="right">
						<fmt:formatNumber value="${JobSetUpForm.vgpm}" type="currency" pattern="######0.00"/>
					</td>
				</tr>
			</table>
		</td>
		<td style="padding-right: 10px;">
			&nbsp;
		</td>
		<td valign="top">
			<table cellpadding="0" cellspacing="0" border="0">
				<c:set var="uplitsApplied" value="0"/>
				<tr>
					<td rowspan="6"  class="dbvaluesmallFont" valign="top" colspan="2">
						<b>Uplifts Applied:</b>&nbsp;
					</td>
				</tr>
				
					<c:if test="${JobSetUpForm.international_uplift_factor ne '0.0'}"> 
						<tr>
							<td class="dbvaluesmallFont">
								&nbsp;
							</td>
							<td class="dbvaluesmallFont">International:&nbsp;
								<c:out value="${JobSetUpForm.international_uplift_factor}"></c:out>
							</td>
						</tr>
						<c:set var="uplitsApplied" value="1"/>
					</c:if>

					<c:if test="${JobSetUpForm.non_pps_uplift_factor ne '0.0'}"> 
						<tr>
							<td class="dbvaluesmallFont">
								&nbsp;
							</td>
							<td class="dbvaluesmallFont">Non PPS:&nbsp;
								<c:out value="${JobSetUpForm.non_pps_uplift_factor}"></c:out>
							</td>
						</tr>
						<c:set var="uplitsApplied" value="1"/>
					</c:if> 
					
					<c:if test="${JobSetUpForm.union_uplift_factor ne '0.0'}">
						<tr>
							<td class="dbvaluesmallFont">
								&nbsp;
							</td>
							<td class="dbvaluesmallFont">Union:&nbsp;
								<c:out value="${JobSetUpForm.union_uplift_factor}"></c:out>
							</td>
						</tr>
						<c:set var="uplitsApplied" value="1"/>
					</c:if> 
					
					<c:if test="${JobSetUpForm.expedite24_uplift_factor ne '0.0'}">
						<tr>
							<td class="dbvaluesmallFont">
								&nbsp;
							</td>
							<td class="dbvaluesmallFont">Expedite 24:&nbsp;
								<c:out value="${JobSetUpForm.expedite24_uplift_factor}"></c:out>
							</td>
						</tr>
						<c:set var="uplitsApplied" value="1"/>
					</c:if>
					
					<c:if test="${JobSetUpForm.expedite48_uplift_factor ne '0.0'}"> 
						<tr>
							<td class="dbvaluesmallFont">
								&nbsp;
							</td>
							<td class="dbvaluesmallFont">expedite48:&nbsp;
								<c:out value="${JobSetUpForm.expedite48_uplift_factor}"></c:out>
							</td>
						</tr>
						<c:set var="uplitsApplied" value="1"/>
					</c:if> 
					
					<c:if test="${uplitsApplied eq '0'}">
						<tr>
							<td class="dbvaluesmallFont">
								&nbsp;
							</td>
							<td class="dbvaluesmallFont">None</td>
						</tr>
					</c:if>
			</table>
		</td>
	</tr>
	<tr><td height="5"></td></tr>
</table>