<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
<script>
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};

setCustomerData = function (){
	var custmereValues = '';
	var appendixCustomerIds = '';
	var fieldcount=0;
	var notEmptyValueCount=0;
	<c:forEach items="${requestScope.requiredDataList}" var="requiredDataList" varStatus="index">
	fieldcount=fieldcount+1;
	var custValue=document.getElementById("custValue"+<c:out value='${index.index}'/>).value;
	 if(custValue.trim()!='')
		{
		 notEmptyValueCount=notEmptyValueCount+1;
		} 
	</c:forEach>
	if(fieldcount>=2)
		{
		if(notEmptyValueCount<2){
			alert('Please Enter values in at least two customer fields');
			return false;
		}
		}
	<c:forEach items="${requestScope.requiredDataList}" var="requiredDataList" varStatus="index">
		<c:if test="${not index.last}">
			custmereValues = custmereValues + document.getElementById("custValue"+<c:out value='${index.index}'/>).value.trim() + '~'
			appendixCustomerIds = appendixCustomerIds + document.getElementById("appendixCustInfoId"+<c:out value='${index.index}'/>).value + '~'
		</c:if>
		<c:if test="${index.last}">
			custmereValues = custmereValues + document.getElementById("custValue"+<c:out value='${index.index}'/>).value.trim()
			appendixCustomerIds = appendixCustomerIds + document.getElementById("appendixCustInfoId"+<c:out value='${index.index}'/>).value
		</c:if>
	</c:forEach>
	getResponse('JobExecuteAction.do?hmode=setCustomerDate&jobId=<c:out value='${JobSetUpForm.jobId}'/>&appendixId=<c:out value='${JobSetUpForm.appendixId}'/>&custmereValues='+escape(encodeURI(custmereValues))+'&appendixCustomerIds='+appendixCustomerIds);
}

getResponse = function (partnerURL){
/* This function call Ajax which is inmplemented into /javascript/prototype.js */	 
	var url = partnerURL;
	var pars = '';
	
	$.ajax({
		type: "GET",
		url: url,
		data: pars,
		error: disableStatus,
		success: disableStatus
    });
	
	//var myAjax = new Ajax.Updater({success:'CustomerFields'},url,{method:'get', parameters:pars, onSuccess:disableStatus, onFailure:disableStatus, evalScripts: true});	
}
disableStatus = function (res){
	document.getElementById("CustomerFields").innerHTML = res.response.Text;
}		

</script>
</head>
<body>
<span id="CustomerFields">
	<jsp:include flush="true" page="CustomerInfoFields.jsp"/>
</span>		
</body>
</html>