<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<c:if test="${requestScope.tabStatus eq 'job'}">
	<tr>
		<td colspan="3">
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
	</c:if>
	<tr height="10">
		<td colspan="3"></td>
	</tr>
	<tr>
		<td width="49%" valign="top" style="padding-left: 4px;">
			<jsp:include page="FinancialAnalysis.jsp"/>
		</td>
		<td style="padding-left: 25px;">&nbsp;</td>
		<td width="49%" valign="top"  style="padding-right: 4px;">
			<jsp:include page="FinancialBreakdown.jsp"/>
		</td>
	</tr>
	<tr height="10">
		<td colspan="3"></td>
	</tr>
</table>
