<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<html:html>
<% 
int i = 0;
boolean csschooser = true;
String bgcolor = "", bgcolornumber = "", bghyperlink = "";
int actlistSize =( int ) Integer.parseInt( request.getAttribute( "actlistSize" ).toString() ); 
String valstr = null;

String lableClass = "dbvalue";
boolean isTicket = false;
if(request.getAttribute("isTicket") != null && request.getAttribute("isTicket").equals("Ticket")){
	isTicket = true;
	lableClass = "dbvalueDisabled";
}

%>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<title><bean:message bundle="PRM" key="prm.uplift.manageupliftfactors"/></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='./dwr/interface/JobSetUpDao.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>
<%@ include  file="/Menu.inc" %>
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellspacing="0" cellpadding="0">
<html:form action="ManageUpliftAction" method="post">
<html:hidden property="jobId" />
<html:hidden property="appendixId" />
<html:hidden property="ref" />
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
			<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
			<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>

					<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
	</tr>
	<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
	         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
				 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
				 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
				  <a><span class="breadCrumb1">Uplift</a>
	    </td>
	</tr>
</table> 
<table  border="0" cellspacing="1" cellpadding="1"  >
<tr>
  <td width="2" height="0">&nbsp;</td>
    <td width="800">
  	<table border="0" cellspacing="1" cellpadding="1" width="800"> 
  	<logic:present name = "retval" scope = "request">
		<logic:equal name = "retval" value = "0">
			<tr>
				<TD>&nbsp;</TD>
				<td colspan = "10" class = "message" height = "25">Uplift factors updated successfully.</td>
			</tr>	
		</logic:equal>
		<logic:notEqual name = "retval" value = "0">
			<tr>
				<TD>&nbsp;</TD>
				<td colspan = "10" class = "message" height = "25">Uplift factors updated failure.</td>
			</tr>	
		</logic:notEqual>
	</logic:present>
	</table>
  	<table border="0" cellspacing="1" cellpadding="1" width="300"> 
	  <tr>
	   <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	  <td>
	  	  <table border="0" cellspacing="1" cellpadding="1" width="300"> 
	  	  <tr height="20"> 
	   		 <td  class="labeleboldhierrarchy" width="300">Uplift Factors</td>
	      </tr>
	      <tr> 
		    <td  class="dbvalue" width="150"><bean:message bundle="PRM" key="prm.uplift.union"/>:</td>
		    <td class="dbvalue" width="150"><bean:write name="ManageUpliftForm" property="unionUplift" /></td>
		  </tr>
		  <tr> 
		    <td  class="<%=lableClass%>" width="150"><bean:message bundle="PRM" key="prm.uplift.expedite24"/>:</td>
		    <td class="<%=lableClass%>" width="150"><bean:write name="ManageUpliftForm" property="expedite24Uplift" /></td>
		  </tr>
		  <tr> 
		    <td  class="<%=lableClass%>" width="150"><bean:message bundle="PRM" key="prm.uplift.expedite48"/>:</td>
		    <td class="<%=lableClass%>" width="150"><bean:write name="ManageUpliftForm" property="expedite48Uplift" /></td>
		  </tr>
		  <tr> 
		    <td  class="dbvalue" width="150">
		    	<%if(isTicket){%>Non-PPS:
		    	<%}else{ %>
		    	<bean:message bundle="PRM" key="prm.uplift.afterhours"/>:
		    	<%} %>
		    </td>
		    <td class="dbvalue" width="150"><bean:write name="ManageUpliftForm" property="afterHoursUplift" />
		    </td>
		  </tr>
		  <tr> 
		    <td  class="<%=lableClass%>" width="150"><bean:message bundle="PRM" key="prm.uplift.wh"/>:</td>
		    <td class="<%=lableClass%>" width="150"><bean:write name="ManageUpliftForm" property="whUplift" /></td>
		  </tr>
		  </table>
	  </td>
	  </tr>
	 
	  
	</table>	

  	<table border="0" cellspacing="1" cellpadding="1" width="800"> 
		<tr>  
		    <td rowspan = "2">&nbsp;</td>
			<td class = "tryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.activity.uplift.activity"/></td>
			<td class = "tryb" colspan = "5"><bean:message bundle = "PRM" key = "prm.activity.uplift.upliftfactors"/></td>
			<td class = "tryb" colspan = "2"><bean:message bundle = "PRM" key = "prm.activity.uplift.estcosts"/></td>
			<td class = "tryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.activity.uplift.extendedunitprice"/></td>
			<td class = "tryb" rowspan = "2"><bean:message bundle = "PRM" key = "prm.activity.uplift.extendedtotalprice"/></td>
		</tr>	  
		
		<tr> 
			<td class = "tryb"><div align = "center"><bean:message bundle = "PRM" key = "prm.activity.uplift.union"/></div></td>
			<td class = "tryb"><div align = "center"><bean:message bundle = "PRM" key = "prm.activity.uplift.expedite24"/></div></td>
			<td class = "tryb"><div align = "center"><bean:message bundle = "PRM" key = "prm.activity.uplift.expedite48"/></div></td>
			<td class = "tryb"><div align = "center">
				<%if(isTicket){%>Non-PPS
		    	<%}else{%><bean:message bundle = "PRM" key = "prm.activity.uplift.afterhours"/><%}%>
		    	</div>
			</td>
			<td class = "tryb"><div align = "center"><bean:message bundle = "PRM" key = "prm.activity.uplift.wh"/></div></td>
			<td class = "tryb"><div align = "center"><bean:message bundle = "PRM" key = "prm.activity.uplift.unit"/></div></td>
			<td class = "tryb"><div align = "center"><bean:message bundle = "PRM" key = "prm.activity.uplift.total"/></div></td>
		</tr>
  	  <logic:present name = "actlist" scope = "request">		
		<logic:iterate id = "actlist" name = "actlist">
		<%	if ( csschooser == true ) {
				csschooser = false; bgcolor = "dbvalueodd"; bgcolornumber = "numberodd"; bghyperlink = "textleftodd";
			} else {
				csschooser = true; bgcolor = "dbvalueeven"; bgcolornumber = "numbereven"; bghyperlink = "textlefteven";
			}
		   if(actlistSize == 1) {
			   valstr = "javascript: document.forms[0].activity_id.checked = true";
			}	
		   else if( actlistSize > 1 ) {
			   valstr = "javascript: document.forms[0].activity_id['"+i+"'].checked = true";
			}
		%>		
		    <TR>
		    <td class = "labeleboldwhite"> 
				<html:multibox property="activity_id">
					<bean:write name="actlist" property="activity_id" />
				</html:multibox>
			</td>
			<html:hidden name = "actlist" property = "activity_tempid"/>	
        	<TD class="<%= bghyperlink %>"><bean:write name = "actlist" property = "activity_name" /></TD>    
            <TD class="<%= bgcolor %>">
				<html:multibox property = "atUnionUplift" onclick = "<%= valstr %>">
					<bean:write name = "actlist" property = "activity_id" />
				</html:multibox>
            </TD>
             <TD class="<%= bgcolor %>">
				<html:multibox property = "atExpedite24Uplift" onclick = "<%= valstr %>" disabled="<%=isTicket%>">
					<bean:write name = "actlist" property = "activity_id" />
				</html:multibox>
            </TD>
             <TD class="<%= bgcolor %>">
				<html:multibox property = "atExpedite48Uplift" onclick = "<%= valstr %>" disabled="<%=isTicket%>">
					<bean:write name = "actlist" property = "activity_id" />
				</html:multibox>
            </TD>
             <TD class="<%= bgcolor %>">
				<html:multibox property = "atAfterHoursUplift" onclick = "<%= valstr %>">
					<bean:write name = "actlist" property = "activity_id" />
				</html:multibox>
            </TD>
             <TD class="<%= bgcolor %>">
				<html:multibox property = "atWhUplift" onclick = "<%= valstr %>" disabled="<%=isTicket%>">
					<bean:write name = "actlist" property = "activity_id" />
				</html:multibox>
            </TD>
            <TD class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "estimatedunitcost" /></TD>
            <TD class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "estimatedtotalcost" /></TD>
            <TD class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "extendedunitprice" /></TD>
            <TD class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "extendedtotalprice" /></TD>
  		   </TR>
		   <% i++;  %>
		</logic:iterate>
 	</logic:present>
		
		<tr>
		  <TD>&nbsp;</TD>
		  <td colspan="10" class="buttonrow">
			  <html:submit property="save" styleClass="button"  ><bean:message bundle="PRM" key="prm.uplift.save" /></html:submit>
			  <html:button property="back" styleClass="button" onclick = "javascript:return Back();"><bean:message bundle="PRM" key="prm.uplift.back" /></html:button>
		  </td>
	  </tr>
	  
  </table>
 </td>
 </tr>
</table> 
<script>
function Back() {
document.forms[0].action = "JobDashboardAction.do?Job_Id=<bean:write name = "ManageUpliftForm" property = "jobId" />&appendix_Id=<bean:write name = "ManageUpliftForm" property = "appendixId" />";
document.forms[0].submit();
}

</script>
	<c:if test = "${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob'}">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>	
</html:form>
</table>
</BODY>
</html:html>
