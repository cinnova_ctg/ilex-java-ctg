<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@page import="com.mind.newjobdb.formbean.JobDashboardForm"%>
<table border=0>
	<tr>
		<td >
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
	<tr>
		<td>
			<jsp:include page="JobScheduleTab.jsp"/>			
		</td>
	</tr>
	
</table>