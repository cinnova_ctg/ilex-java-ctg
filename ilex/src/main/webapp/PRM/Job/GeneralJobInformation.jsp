<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" ftype="text/css" />
<head>
	<script>
		function TeamAssign()
		{			
			
			var appendixType = escape("<c:out value='${JobSetUpForm.appendixType}'/>");
			var currentOwner =escape( "<c:out value='${JobSetUpForm.owner}'/>");
			
			var url = "AssignOwner.do?appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&appendixType="+appendixType+"&jobId=<c:out value='${JobSetUpForm.jobId}'/>&currentOwner="+currentOwner+"&fromPage=jobdashboard";
			document.location = url;
		}
	</script>
	<script>
		function addCustRef()
		{			
			
			var appendixType = escape("<c:out value='${JobSetUpForm.appendixType}'/>");
			var currentOwner =escape( "<c:out value='${JobSetUpForm.createdBy}'/>");
			var url = "AddCustRef.do?appendixid=<c:out value='${JobSetUpForm.appendixId}'/>&typeid=<c:out value='${JobSetUpForm.jobId}'/>&type=Job&jobId=<c:out value='${JobSetUpForm.jobId}'/>&fromPage=jobdashboard&tabId=<c:out value='${JobSetUpForm.tabId}'/>";
			document.location = url;
		}
	</script>
</head>
	<input type="hidden" name="jobId"/>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" >
			<tr valign="top">
				<td width="20%" align="left" colspan="3">
					<table  border="0">
						<tr  valign="top">
							<td  class = "dbvaluesmallFont" align="left"><b>Status</b>:&nbsp;<c:out value='${JobSetUpForm.jobStatus}'/>
							</td>
						</tr>
						<c:if test="${requestScope.tabId eq '1'}">
							<tr valign="top">
								<td class = "dbvaluesmallFont" nowrap="nowrap"><b>Customer Reference</b>:&nbsp;<c:out value='${JobSetUpForm.reference}'/> (<a   href = "#" onclick="addCustRef();" >Edit</a>)
								</td>				
							</tr>
						</c:if>
					</table>
				
				</td>
				<td width="20%" align="left" colspan="3">
					<table  border="0">
						<tr  valign="top">
							<td class = "dbvaluesmallFont" >
							<c:if test='${not empty JobSetUpForm.scheduleType}'><b><c:out value='${JobSetUpForm.scheduleType}'/></b>
							<c:if test="${JobSetUpForm.reScheduleClientCount  ne '0'}">
								<font color="red" style="font-weight: bold">(CLR<c:out value='${JobSetUpForm.reScheduleClientCount }'/>)</font>
							</c:if>
							<c:if test="${JobSetUpForm.reScheduleContingentCount  ne '0'}">
								<font color="red" style="font-weight: bold">(COR<c:out value='${JobSetUpForm.reScheduleContingentCount}'/>)</font>
							</c:if>
							<c:if test="${JobSetUpForm.reScheduleInternalCount  ne '0'}">
								<font color="red" style="font-weight: bold">(INR<c:out value='${JobSetUpForm.reScheduleInternalCount}'/>)</font>
							</c:if>
							</c:if>
							<c:if test='${empty JobSetUpForm.scheduleType}'><b>No schedule available</b></c:if></td>
							<c:if test="${(not empty JobSetUpForm.scheduleType) && (JobSetUpForm.scheduleType ne 'No schedule available')}">
								<td class='dbvaluesmallFont'><b>Start</b>:&nbsp;<c:out value='${JobSetUpForm.startDate}'/></td>
							</tr>
							<tr  valign="top">
								<td></td>
								<td class='dbvaluesmallFont'><b>End</b>:&nbsp;&nbsp;&nbsp;<c:out value='${JobSetUpForm.endDate}'/></td>
							</c:if>
						</tr>
					</table>
				</td>
				<td width="20%" align="left" colspan="3">
					<c:if test="${ requestScope.tabStatus eq 'job'}">
						<table  border="0">
							
								<tr  valign="top">
									<td class = "dbvaluesmallFont" ><b>Site</b>:</td>
									<td class='dbvaluesmallFont'><c:if test="${not empty JobSetUpForm.site_number}"><c:out value="${JobSetUpForm.site_number }"/></c:if>
														<c:if test="${empty JobSetUpForm.site_number}">Not Available</c:if>
							
								<logic:equal name ="JobSetUpForm" property = "site_priorities"  value ="Y">	 
								
									<IMG id = img_2  src = "images/priorities.jpg" border = 0  width="16" height="16">		
								 	</logic:equal>	 				
									</td>
								</tr>
								<tr  valign="top">
									<td></td>
									<td class='dbvaluesmallFont'><c:out value="${JobSetUpForm.site_city}"/><c:if test="${not empty JobSetUpForm.site_state}">, <c:out value="${JobSetUpForm.site_state }"/> </c:if><c:if test="${JobSetUpForm.site_country ne 'US' && not empty JobSetUpForm.site_country}">, <c:out value="${JobSetUpForm.site_country}"/></c:if></td>
								</tr>
							
						</table>
					</c:if>
				</td>
				<%-- Team Part: Start --%>
				<td>
					<table  border="0" cellpadding="0" cellspacing="0">
					<tr valign="top">
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr valign="top">
								<c:if test="${JobSetUpForm.jobStatus !='Closed'}">
									<td class = "dbvaluesmallFont" nowrap="nowrap" valign="top"><b>Team</b>:</td>
									<td class="dbvaluesmallFont"  valign="top"> (<a href="#" onclick="TeamAssign();">Edit</a>)&nbsp;</td>
								</c:if>
								<c:if test="${JobSetUpForm.jobStatus =='Closed'}">
									<td colspan="2">&nbsp;</td>
								</c:if>
								</tr>
							</table>
						</td>
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr valign="top">
									<td class='dbvaluesmallFont' width="10%">Owner:&nbsp;<c:out value='${JobSetUpForm.owner}'/></td>							
								</tr>					
								<tr valign="top">
									<td class='dbvaluesmallFont' width="10%" valign="top">Scheduler(s):
									<c:forEach items="${JobSetUpForm.schedulers}" var="schedulers" varStatus="index" >
										<c:out value="${schedulers}"/>&nbsp;
									</c:forEach>
									</td>	
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
				<%-- Team Part: End --%>
			</tr>	
			<tr>
				<td class="dbbottomborder1" colspan="30"  valign="top"><font style="font-size: 1px;">&nbsp;</font></td>
			</tr>			
		</table>
	