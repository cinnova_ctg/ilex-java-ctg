<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html:html>
<% 
%>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<title><bean:message bundle="PRM" key="prm.uplift.manageupliftfactors"/></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
<%@ include  file="/Menu.inc" %>
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<bean:define id="methodcombo" name="methodcombo" scope = "request" />
<table cellspacing="0" cellpadding="0">
<html:form action="EditOOSActivityAction">
<html:hidden property="activityId" />
<html:hidden property="jobId" />
<html:hidden property="appendixId" />
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
			<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
			<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>

					<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
	</tr>
	<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
	         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
				 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
				 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
				  <a><span class="breadCrumb1">Out of Scope Activity</a>
	    </td>
	</tr>
</table> 
<table  border="0" cellspacing="1" cellpadding="1">
<tr>
  <td width="2" height="0"></td>
    <td width="310">
  	<table border="0" cellspacing="1" cellpadding="1" width="800"> 
	 <logic:present name = "addflag" scope = "request">
	 <tr>
		 <td class = "message" height = "30">
		 	<logic:equal name = "addflag" value = "-90030"><bean:message bundle = "pm" key = "activity.tabular.add.failure9"/></logic:equal>
		 	<logic:equal name = "addflag" value = "-9099"><bean:message bundle = "pm" key = "activity.tabular.add.failure9"/></logic:equal>
			<logic:equal name = "addflag" value = "0"><bean:message bundle = "pm" key = "activity.tabular.add.success"/></logic:equal>
		 </td>
	 </tr>
	</logic:present>
 	   <tr>
		    <td width="800" class="labeleboldhierrarchy" height="30" colspan="2">
		    	<c:choose>
			  	<c:when test="${EditOOSActivityForm.flagOOA eq 'addOOA'}"><bean:message bundle="PRM" key="prm.activity.oos.addtitle"/></c:when>
			  	<c:when test="${EditOOSActivityForm.flagOOA ne 'addOOA'}"><bean:message bundle="PRM" key="prm.activity.oos.edittitle"/></c:when>
				</c:choose>
	   		</td>
	  </tr>
	</table>	
  	<table border="0" cellspacing="1" cellpadding="1" width="500"> 
      <tr> 
	    <td  class="labelobold" width="150">Name</td>
	    <td class="labelo" width="350"><html:text styleClass="textbox" size="40" maxlength="50" property="activityName" /></td>
	  </tr>
	  <tr> 
	    <td  class="labelebold" width="150">Date Notified</td>
	    <td class="labele" width="350"><html:text  styleClass="textbox" size="10" property="dateNotified" />
		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].dateNotified, document.forms[0].dateNotified, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
		</td>
	  </tr>
	  <tr> 
	    <td  class="labelobold" width="150">Notification Method</td>
	   	<td class="labelo" width="350">
		   	<html:select property="notificationMethod"  styleClass="comboo">
			<html:optionsCollection name = "methodcombo"  property = "notificationMethodList"  label = "label"  value = "value" /></html:select>
		</td>	
	 </tr>
	  <tr> 
	    <td  class="labelebold" width="150">Name of Customer</td>
	    <td class="labele" width="350"><html:text  styleClass="textbox" size="25" maxlength="50" property="custName" /></td>
	  </tr>
	  <tr> 
	    <td  class="labelobold" width="150">Date Approved</td>
	    <td class="labelo" width="350"><html:text  styleClass="textbox" size="10" property="dateApproved" />
		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].dateApproved, document.forms[0].dateApproved, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
		</td>
	  </tr>
	  <tr> 
	    <td  class="labeleboldtop" width="150">Description</td>
	    <td class="labele" width="350"><html:textarea  property="description" rows="4" cols="40" styleClass="textarea"></html:textarea></td>
	  </tr>
	  <tr>
		  <td colspan="2" class="buttonrow">
		  	<html:submit property="save" styleClass="button" onclick = "javascript:return validate();">
		  	<c:choose>
		  	<c:when test="${EditOOSActivityForm.flagOOA eq 'addOOA'}">Add</c:when>
		  	<c:when test="${EditOOSActivityForm.flagOOA ne 'addOOA'}">Save</c:when>
			</c:choose>
			</html:submit>
			<html:button property="back" styleClass="button" onclick = "javascript:return Back();">Back</html:button>
			<html:reset property="back" styleClass="button">Reset</html:reset>
		  </td>
	  </tr>
	  
  </table>
 </td>
 </tr>
</table> 
<script>


function validate(){

document.forms[0].activityName.value = trimFields(document.forms[0].activityName.value);

		if( trimFields(document.forms[0].activityName.value) == '') {
	    	alert('Please enter value in Name field');
    		document.forms[0].activityName.focus();
    		return false;
		}
}

function trimFields(str)
{
		var temp2="";
		var flag=0;
		if(str.indexOf(" ",str.length-1)>=0) {
			for(var j=str.length; j>0; j--){
			if((str.substring(j-1,j)==" ")&&(flag==0)){
				temp2=str.substring(0,j-1);
			} else {flag=1; break;}
		 }//for
		} else { temp2=str; }

		str=temp2;
		temp2="";
		flag=0;
		if(str.indexOf(" ")==0) {
			for(var j=0; j<str.length; j++){
			if((str.substring(j,j+1)==" ")&&(flag==0)){
			temp2=str.substring(j+1,str.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=str; }

		str = temp2;
	
return str;
}


function Back() {
document.forms[0].action = "JobDashboardAction.do?type=1&Job_Id=<bean:write name = "EditOOSActivityForm" property = "jobId" />&appendix_Id=<bean:write name = "EditOOSActivityForm" property = "appendixId" />";
document.forms[0].submit();
}

</script>
<c:if test = "${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob'}">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
</html:form>
</table>
</BODY>
</html:html>
