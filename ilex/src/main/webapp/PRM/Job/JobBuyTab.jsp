<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<table>
	<tr>
		<td>
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
	<c:if test="${requestScope.tabStatus eq 'job'}">
		<tr>
			<td>
				<jsp:include page="JobWorkflow.jsp"/>
			</td>
		</tr>
	</c:if>
	<tr>
		<td>
			<jsp:include page="PurchaseOrder.jsp"/>
		</td>
	</tr>
	<tr>
		<table>
			<tr>
					<td  valign="top">
						<jsp:include page="JobPlanningNotes.jsp"/>
					</td>
					<td valign="top">
								<jsp:include page="BuyJobCustomerFields.jsp"/>
					</td>
			</tr>
		</table>
		
	</tr>
	<tr>
		<td valign="top">
			<jsp:include page="CostBreakDownByPO.jsp"/>
		</td>
	</tr>
</table>
<c:if test ="${not empty requestScope.refershtree && requestScope.refershtree eq 'refershtree'}">
	<script>
			parent.ilexleft.location.reload();
	</script>
</c:if>