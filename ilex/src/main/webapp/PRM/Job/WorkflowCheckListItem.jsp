<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<script>
		function onUpdate(obj){
			var url ;
			var actioMode;
			if(obj == 'Complete')
				actioMode = "onComplete";
			else if(obj == 'Undo')
				actioMode = "undo";
			else if(obj == 'Skipped')
				actioMode = "skipped";
			else if(obj == 'NotApplicable')
				actioMode = "notApplicable";	
			document.forms[0].action = "JobWorkflow.do?mode="+actioMode+"&hmode=manipulateCheckList";
			document.forms[0].submit();
		}
	</script>
</head>
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
	<body>
		<html:form action="JobWorkflow">
		
		<input type="hidden" name="jobId" value="<c:out value='${JobWorkflowForm.jobId}'/>"/>
	
			<table width="100%"  cellpadding=0 cellspacing=0  >
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
								<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
								<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>

										<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
						</tr>
						<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
						         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
												 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
												<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
												  <a><span class="breadCrumb1">Job Workflow</a>
						    </td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
				<td >
				<table border="0" width="800" cellspacing="0" cellpadding="5">
					<tr>
						<td>
							<table width="80%" cellspacing="1" cellpadding="1" border="0">
								
								<tr>
									<td class="dbvalue" colspan="4" align="left"><h1 style="font-size: 11px;padding-left: 0px;">Complete</h1></td>
								 </tr>
								 <tr>
								   	<td align="center" class = "dbvalue" width="15%" cellspacing="0" cellpadding="0"><b>Status</b></td>
								   	<td align="center" class = "dbvalue" width="60%" cellspacing="0" cellpadding="0"><b>Checklist Item </b></td>
								   	<td align="center" class = "dbvalue" width="15%" cellspacing="0" cellpadding="0"><b>User </b></td>
								   	<td align="center" class = "dbvalue" width="10%" cellspacing="0" cellpadding="0"><b>Date </b></td>
								</tr>	
								  <tr>
									<td bgcolor = "#000000" class="dbvalue" colspan="4" align="left"></td>
								 </tr>
								 
								<c:forEach items="${JobWorkflowForm.completeItemList}" var="jobWorkflowItem" varStatus="index">
									<input type="hidden" name="itemId" value="<c:out value='${jobWorkflowItem.itemId}'/>"/>
									<input type="hidden" name="description" value="<c:out value='${jobWorkflowItem.description}'/>"/>
									<input type="hidden" name="sequence" value="<c:out value='${jobWorkflowItem.sequence}'/>"/>
									<input type="hidden" name="referenceDate" value="<c:out value='${jobWorkflowItem.referenceDate}'/>"/>
									<input type="hidden" name="user" value="<c:out value='${jobWorkflowItem.user}'/>"/>
									<input type="hidden" name="createdBy" value="<c:out value='${jobWorkflowItem.createdBy}'/>"/>
									<input type="hidden" name="updatedBy" value="<c:out value='${jobWorkflowItem.updatedBy}'/>"/>
									<input type="hidden" name="createdDate" value="<c:out value='${JobWorkflowForm.createdDate[index.index]}'/>"/>
									<input type="hidden" name="updatedDate" value="<c:out value='${JobWorkflowForm.updatedDate[index.index]}'/>"/>
									<input type="hidden" name="itemStatus" value="<c:out value='${jobWorkflowItem.itemStatus}'/>"/>
									<input type="hidden" name="itemStatusCode" value="<c:out value='${jobWorkflowItem.itemStatusCode}'/>"/>
									<input type="hidden" name="projectWorkflowItemId" value="<c:out value='${jobWorkflowItem.projectWorkflowItemId}'/>"/>
									<input type="hidden" name="lastItemStatusCode" value="<c:out value='${jobWorkflowItem.lastItemStatusCode}'/>"/>
									
									<tr>
										<td width="15%" align="center" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> > <c:out value="${jobWorkflowItem.itemStatus}"/></td>
									   	<td width="60%" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> > <c:out value="${jobWorkflowItem.description}"/></td>
									   	<td width="15%" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center" ><c:out value="${jobWorkflowItem.user}"/></td>
									   	<td width="10%" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"> <c:out value="${jobWorkflowItem.referenceDate}"/></td>
									 </tr>
									<c:if test="${JobWorkflowForm.completeItemListSize eq '0'}">
										<tr>
											<td class="dbvalue" style="font-size: 10px;">No completed items</td>
										 </tr> 
									</c:if>
			  					</c:forEach>
								 <tr>
									<td style="padding-top: 4px;" align="left" valign="bottom" colspan="4">
			  							<input type="button" onclick="onUpdate('Undo');"  style="width: 120px;" name="undo" value="Undo Last Action" class="button_c1" <c:if test="${JobWorkflowForm.completeItemListSize eq '0'}">disabled="disabled"</c:if>/>
			  						</td>
			  					</tr>
							</table>
						</td>
					</tr>
					
					<tr>
						<td>
							<table width="80%" cellspacing="1" cellpadding="1" border="0"> 
								<tr>
									<td class="dbvalue" colspan="4" align="left"><h1 style="font-size: 11px;padding-left: 0px;">Current CheckList Item</h1></td>
					 			</tr>
					 			 <tr>
								   	<td align="center" class = "dbvalue" width="15%" cellspacing="0" cellpadding="0"><b>Status</b></td>
								   	<td align="center" class = "dbvalue" width="60%" cellspacing="0" cellpadding="0"><b>Checklist Item </b></td>
								   	<td align="center" class = "dbvalue" width="15%" cellspacing="0" cellpadding="0"><b>User </b></td>
								   	<td align="center" class = "dbvalue" width="10%" cellspacing="0" cellpadding="0"><b>Date </b></td>
								</tr>
					 			 <tr>
									<td bgcolor = "#000000" class="dbvalue" colspan="4" align="left"></td>
								 </tr>
									
					 		<c:forEach items="${JobWorkflowForm.firstPenndingList}" var="jobWorkflowItem" varStatus="index">
					 				<input type="hidden" name="itemId" value="<c:out value='${jobWorkflowItem.itemId}'/>"/>
									<input type="hidden" name="description" value="<c:out value='${jobWorkflowItem.description}'/>"/>
									<input type="hidden" name="sequence" value="<c:out value='${jobWorkflowItem.sequence}'/>"/>
									<input type="hidden" name="referenceDate" value="<c:out value='${jobWorkflowItem.referenceDate}'/>"/>
									<input type="hidden" name="user" value="<c:out value='${jobWorkflowItem.user}'/>"/>
									<input type="hidden" name="createdBy" value="<c:out value='${jobWorkflowItem.createdBy}'/>"/>
									<input type="hidden" name="updatedBy" value="<c:out value='${jobWorkflowItem.updatedBy}'/>"/>
									<input type="hidden" name="createdDate" value="<c:out value='${JobWorkflowForm.createdDate[index.index]}'/>"/>
									<input type="hidden" name="updatedDate" value="<c:out value='${JobWorkflowForm.updatedDate[index.index]}'/>"/>
									<input type="hidden" name="itemStatus" value="<c:out value='${jobWorkflowItem.itemStatus}'/>"/>
									<input type="hidden" name="itemStatusCode" value="<c:out value='${jobWorkflowItem.itemStatusCode}'/>"/>
									<input type="hidden" name="projectWorkflowItemId" value="<c:out value='${jobWorkflowItem.projectWorkflowItemId}'/>"/>
									<input type="hidden" name="lastItemStatusCode" value="<c:out value='${jobWorkflowItem.lastItemStatusCode}'/>"/>
								<tr>
									<td width="15%" align="center" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap1'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>><c:out value="${jobWorkflowItem.itemStatus}"/></td>
									   	<td width="60%" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap1'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  > <c:out value="${jobWorkflowItem.description}"/></td>
									   	<td width="15%" align="center" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap1'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center" ><c:out value="${jobWorkflowItem.user}"/></td>
									   	<td width="10%" align="center" <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap1'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"> <c:out value="${jobWorkflowItem.referenceDate}"/></td>
								 </tr> 
							</c:forEach>
							<c:if test="${JobWorkflowForm.firstPenndingListSize eq '0'}">
								<tr>
									<td class="dbvalue" style="font-size: 10px;">No current item </td>
								 </tr> 
							</c:if>
								<tr>
								 	<td colspan="4">
								 		<table>
								 			<tr>
									 			<td  align="left" valign="bottom">
										    		<input type="button" style="width: 70px;"  name="Complete" onclick="onUpdate('Complete');" value="Complete" class="button_c1" <c:if test="${JobWorkflowForm.firstPenndingListSize eq '0'}">disabled="disabled" </c:if>/>
										    	</td>
												<td  align="left" valign="bottom">
										    		<input type="button" style="width: 60px;" name="Skipped" onclick="onUpdate('Skipped');" value="Skipped" class="button_c1" <c:if test="${JobWorkflowForm.firstPenndingListSize eq '0'}">disabled="disabled" </c:if>/>
										    	</td>
										    	<td align="left" valign="bottom">
										    		<input type="button" style="width: 100px;" name="NotApplicable" onclick="onUpdate('NotApplicable');" value="Not Applicable" class="button_c1" <c:if test="${JobWorkflowForm.firstPenndingListSize eq '0'}">disabled="disabled" </c:if>/>
										    	</td>
										    </tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="80%" cellspacing="1" cellpadding="1">
									
								<tr>
									<td class="dbvalue" colspan="4" align="left"><h1 style="font-size: 11px;padding-left: 0px;">Pending</h1></td>
								 </tr>
								 	 <tr>
								   	<td align="center" class = "dbvalue" width="15%" cellspacing="0" cellpadding="0"><b>Status</b></td>
								   	<td align="center" class = "dbvalue" width="60%" cellspacing="0" cellpadding="0"><b>Checklist Item </b></td>
								   	<td align="center" class = "dbvalue" width="15%" cellspacing="0" cellpadding="0"><b>User </b></td>
								   	<td align="center" class = "dbvalue" width="10%" cellspacing="0" cellpadding="0"><b>Date </b></td>
								</tr>
								 <tr>
									<td bgcolor = "#000000" class="dbvalue" colspan="4" align="left"></td>
								 </tr>
								
								 <c:forEach items="${JobWorkflowForm.pendingList}" var="jobWorkflowItem" varStatus="index">
								 	<input type="hidden" name="itemId" value="<c:out value='${jobWorkflowItem.itemId}'/>"/>
									<input type="hidden" name="description" value="<c:out value='${jobWorkflowItem.description}'/>"/>
									<input type="hidden" name="sequence" value="<c:out value='${jobWorkflowItem.sequence}'/>"/>
									<input type="hidden" name="referenceDate" value="<c:out value='${jobWorkflowItem.referenceDate}'/>"/>
									<input type="hidden" name="user" value="<c:out value='${jobWorkflowItem.user}'/>"/>
									<input type="hidden" name="createdBy" value="<c:out value='${jobWorkflowItem.createdBy}'/>"/>
									<input type="hidden" name="updatedBy" value="<c:out value='${jobWorkflowItem.updatedBy}'/>"/>
									<input type="hidden" name="createdDate" value="<c:out value='${JobWorkflowForm.createdDate[index.index]}'/>"/>
									<input type="hidden" name="updatedDate" value="<c:out value='${JobWorkflowForm.updatedDate[index.index]}'/>"/>
									<input type="hidden" name="itemStatus" value="<c:out value='${jobWorkflowItem.itemStatus}'/>"/>
									<input type="hidden" name="itemStatusCode" value="<c:out value='${jobWorkflowItem.itemStatusCode}'/>"/>
									<input type="hidden" name="projectWorkflowItemId" value="<c:out value='${jobWorkflowItem.projectWorkflowItemId}'/>"/>
									<input type="hidden" name="lastItemStatusCode" value="<c:out value='${jobWorkflowItem.lastItemStatusCode}'/>"/>
									<tr>
										<td width="15%" align = "center" <c:if test="${(index.index % 2)eq '0'}">class='dbvaluesmallFont'</c:if><c:if test="${(index.index % 2) eq '1'}">class='dbvaluesmallFont'</c:if> >  <c:out value="${jobWorkflowItem.itemStatus}"/></td>
									   	<td <c:if test="${(index.index % 2)eq '0'}">class='dbvaluesmallFont'</c:if><c:if test="${(index.index % 2) eq '1'}">class='dbvaluesmallFont'</c:if>  > <c:out value="${jobWorkflowItem.description}"/></td>
									 </tr>
								</c:forEach>
								<c:if test="${JobWorkflowForm.pendingListSize eq '0'}">
									<tr>
										<td class="dbvalue" style="font-size: 10px;">No pending items </td>
									 </tr> 
								</c:if>
				   			</table>
				   		</td>
				   	</tr>
				</table>
				</td>
			</tr>
		</table>
	
	</html:form>
				<c:if test = "${requestScope.jobType eq 'Addendum'}">
					<%@ include  file="/AddendumMenuScript.inc" %>
				</c:if>
				<c:if test = "${requestScope.jobType eq 'inscopejob'}">
					<%@ include  file="/ProjectJobMenuScript.inc" %>
				</c:if>	  
		</body>
	</html>