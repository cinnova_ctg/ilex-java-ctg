<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language="JavaScript">
	function addTxtFiled() {
		if (document.getElementById("addBtn").value == 'Add Other') {
			document.getElementById("addBtn").value = 'Remove';
			document.getElementById("alternateCustomerPOC").style.display = "";
			document.getElementById("customerPOC").disabled = true;
			document.getElementById("customerPOC").value = "SELECT";

		} else if (document.getElementById("addBtn").value == 'Remove') {
			document.getElementById("addBtn").value = 'Add Other';
			document.getElementById("alternateCustomerPOC").style.display = "none";
			document.getElementById("alternateCustomerPOC").value = "";
			document.getElementById("customerPOC").disabled = false;
			document.getElementById("customerPOC").value = "SELECT";
		}
	}

	function echeck(str) {

		var at = "@"
		var dot = "."
		var lat = str.indexOf(at)
		var lstr = str.length
		var ldot = str.indexOf(dot)
		if (str.indexOf(at) == -1) {
			return false
		}

		if (str.indexOf(at) == -1 || str.indexOf(at) == 0
				|| str.indexOf(at) == lstr) {
			return false
		}

		if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0
				|| str.indexOf(dot) == lstr) {
			return false
		}

		if (str.indexOf(at, (lat + 1)) != -1) {
			return false
		}

		if (str.substring(lat - 1, lat) == dot
				|| str.substring(lat + 1, lat + 2) == dot) {
			return false
		}

		if (str.indexOf(dot, (lat + 2)) == -1) {
			return false
		}

		if (str.indexOf(" ") != -1) {
			return false
		}

		return true
	}

	function submitNote() {
		if (document.forms[0].customerName.value == "") {
			document.forms[0].customerName.focus();
			alert("Please enter company name.");
			return false;
		}

		else if (document.forms[0].site_number.value == "") {
			document.forms[0].site_number.focus();
			alert("Please enter site number.");
			return false;
		}

		else if (document.getElementById("customerPOC").disabled == false
				&& document.forms[0].customerPOC.value == "SELECT") {
			document.forms[0].customerPOC.focus();
			alert("Please select customer POC.");
			return false;
		}

		else if (document.getElementById("customerPOC").disabled == true
				&& document.forms[0].alternateCustomerPOC.value == "") {
			document.forms[0].alternateCustomerPOC.focus();
			alert("Please enter customer POC in given text field. ");
			return false;
		}

		else if (document.forms[0].senderEmailId.value == "") {

			document.forms[0].senderEmailId.focus();
			alert("Please enter sender email id.");
			return false;
		}

		else if (echeck(document.forms[0].senderEmailId.value) == false) {
			document.forms[0].senderEmailId.focus();
			alert("Please enter valid email id.");
			return false;
		}

		else if (document.forms[0].estimatedCost.value == ""
				|| parseInt(document.forms[0].estimatedCost.value) <= 0) {
			document.forms[0].estimatedCost.focus();
			alert("Please enter estimated cost greater then 0");
			return false;
		}

		var url;
		url = "sendNotificationMailAction.do?type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=3";
		document.forms[0].action = url;
		document.forms[0].submit();

	}
	
	
	function addText(opt){
		var regexp = /--CRLF--/g;
			if(opt.value == 'Explicit Authorization Required')
			opt.form.standardNotification.value = 'This is a notification of a change order to be performed on an open job. If you are the recipient of this notification and you authorize this change order please reply to this notification prior to the scheduled time and date for the change listed above with authorization and any appropriate details. If the job is already underway you will have twenty (20) minutes from the time of receipt of this notification to respond with authorization before we depart the site. However, if you wish to cancel this change order you need not respond. We will assume no response indicates your desire to cancel the change order.'.replace(regexp, "\r\n");		
			if(opt.value == 'Silence is Consent')
			opt.form.standardNotification.value = 'This is a notification of a change order to be performed on an open job. If you are the recipient of this notification and you authorize this change order without exceptions you need not respond. However, if you wish to authorize with exceptions or cancel this change order please reply to this notification prior to the scheduled time and date for the change listed above with the appropriate details. If the job is already underway you will have twenty (20) minutes from the time of receiept of this notification to respond before we proceed.'.replace(regexp, "\r\n");		
		}
	
	function setSerderMailId(lst){
		if(lst.value == "SELECT"){
			document.getElementById("clientEmailId").value = "";
		}else{
			document.getElementById("clientEmailId").value = lst.value;	
		}
		
	}

</script>
<head>
<link rel="stylesheet" href="styles/style_common.css" type="text/css">
</head>
<form action="InvoicePriceAction">
	<body>
		<h1 class="PCSSH0002 PCSSH0001">Change Order</h1>

		<table cellpadding="0" cellspacing="0" class="WASTD0001">
			<tr>
				<td><bean:write name="JobDashboardForm" property="appendixid" />
				</td>
			</tr>
			<tr>
				<td class="PCLAB0001">Company Name:*</td>
				<td><input type="text" readonly="readonly"
					value="<c:out value='${JobSetUpForm.customerName}'/>"
					id="customerName" name="customerName" class="PCTXT0001" />
				</td>
			</tr>

			<tr>
				<td class="PCLAB0001">Project Name:</td>
				<td><input type="text" readonly="readonly"
					value="<c:out value='${JobSetUpForm.projectName}'/>"
					id="projectName" name="projectName" class="PCTXT0001" />
				</td>
			</tr>

			<tr>
				<td class="PCLAB0001">Site Number:*</td>
				<td><input type="text" readonly="readonly"
					value="<c:out value='${JobSetUpForm.site_number}'/>"
					id="site_number" name="site_number" class="PCTXT0001" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Site Address:</td>
				<td><input type="text" id="site_address" name="site_address"
					class="PCTXT0001" readonly="readonly"
					value="<c:out value='${JobSetUpForm.site_address}'/>" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Site City:</td>
				<td><input type="text" class="PCTXT0001" id="site_city"
					readonly="readonly" name="site_city"
					value="<c:out value='${JobSetUpForm.site_city}'/>" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Site State:</td>
				<td><input type="text" readonly="readonly"
					value="<c:out value='${JobSetUpForm.site_state}'/>" id="site_state"
					name="site_state" class="PCTXT0001" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Site ZIP Code:</td>
				<td><input type="text" class="PCTXT0001" id="site_zipcode"
					readonly="readonly" name="site_zipcode"
					value="<c:out value='${JobSetUpForm.site_zipcode}'/>" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Customer POC:*</td>

				<td><select id="customerPOC" name="customerPOC" class="PCCOMBO001" onchange="setSerderMailId(this);">
						<option value="SELECT" selected="selected">SELECT</option>
						<c:forEach items="${requestScope.pocList}" var="pocList"
							varStatus="innerIndex">
							<option value="<c:out value='${pocList.value}' />"><c:out value='${pocList.label}' /></option>
						</c:forEach>
				</select> <input type="text"
					value="<c:out value='${JobSetUpForm.alternateCustomerPOC}'/>"
					id="alternateCustomerPOC" class="PCTXT0001" name="alternateCustomerPOC"
					style="display: none" /> <input type="button" value="Add Other"
					id="addBtn" class="FMBUT0001" name="addBtn" style="margin-top: 0px"
					onclick="addTxtFiled()" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Client Email:</td>
				<td><input type="text" class="PCTXT0001" id="clientEmailId"
					name="clientEmailId"
					value="" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Sender Email:*</td>
				<td><input type="text" class="PCTXT0001" id="senderEmailId"
					name="senderEmailId"
					value="<c:out value='${JobSetUpForm.senderEmailId}'/>" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Sender Signature:</td>
				<td><textarea class="PCTXT0001" id="senderSignatures" style="text-align:left;"
						name="senderSignatures" value="<c:out value='${JobSetUpForm.senderSignatures}'/>" rows="6" cols="35"
						style="width: 269px;" ><c:out value='${JobSetUpForm.senderSignatures}'/></textarea></td>
			</tr>

			<tr>
				<td class="PCLAB0001">CC Email:</td>
				<td><input type="text" class="PCTXT0001" id="ccEmail"
					name="ccEmail" value="" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Date of intiate:</td>
				<td><input type="text" class="textbox" size="10"
					name="intiateDate"
					value="<c:out value='${JobSetUpForm.intiateDate}'/>"
					readonly="true" /> <img src="images/calendar.gif" width=19
					height=17 border=0 align="center"
					onclick="return popUpCalendar(document.forms[0].intiateDate, document.forms[0].intiateDate, 'mm/dd/yyyy')"
					onmouseover="window.status = 'Date Picker';return true;"
					onmouseout="window.status = '';return true;" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Time to intiate:</td>
				<td><select name="intiateHour" class="comboWhite">
						<c:forEach items="${requestScope.hourList}" var="hourList"
							varStatus="innerIndex">
							<option value="<c:out value='${hourList.value}'/>"
								<c:if test="${JobSetUpForm.intiateHour eq hourList.label}">selected="selected"</c:if>>
								<c:out value='${hourList.label}' />
							</option>
						</c:forEach>
				</select> <select name="intiateMinute" class="comboWhite">
						<c:forEach items="${requestScope.minuteList}" var="minuteList"
							varStatus="innerIndex">
							<option value="<c:out value='${minuteList.value}'/>"
								<c:if test="${JobSetUpForm.intiateMinute eq minuteList.label}">selected="selected"</c:if>>
								<c:out value='${minuteList.label}' />
							</option>
						</c:forEach>
				</select> <select name="intiateIsAm" class="comboWhite">
						<option value="AM"
							<c:if test="${JobSetUpForm.intiateIsAm eq '1'}">selected="selected"</c:if>>AM</option>
						<option value="PM"
							<c:if test="${JobSetUpForm.intiateIsAm eq '0'}">selected="selected"</c:if>>PM</option>
				</select></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Proposed change:</td>
				<td><textarea class="PCTXT0001" id="proposedChange"
						name="proposedChange" value="<c:out value='${JobSetUpForm.proposedChange}'/>" rows="6"
						cols="35" style="width: 269px;" ><c:out value='${JobSetUpForm.proposedChange}'/></textarea></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Estimated Cost Impact:*</td>
				<td><input type="text" class="PCTXT0001" id="estimatedCost"
					name="estimatedCost" value="0.0" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Estimated Schedule Impact:</td>
				<td><input type="text" class="PCTXT0001" id="estimatedScheduleImpact"
					name="estimatedScheduleImpact" value="" /></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Type:</td>
				<td><input type="radio" id="typeOption" name="typeOption"
					value="Explicit Authorization Required" onclick="addText(this)" />&nbsp;Explicit
					Authorization Required <input type="radio" id="typeOption"
					name="typeOption" value="Silence is Consent" onclick="addText(this)" />&nbsp;Silence is
					Consent</td>
			</tr>

			<tr>
				<td class="PCLAB0001">Standard Notification Copy:</td>
				<td><textarea class="PCTXT0001" id="standardNotification"
						name="standardNotification" value="Notification Here..." rows="6" cols="35"
						style="width: 269px;"></textarea></td>
			</tr>

			<tr>
				<td class="PCLAB0001">Attachment:</td>
				<td><input type="file" id="attachment" name="attachment"
					class="PCTXT0001" /></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="hidden" id="type"
					name="type" value="<c:out value='${JobSetUpForm.tabId}'/>" /> <input
					type="hidden" id="appendixId" name="appendixId"
					value="<c:out value='${JobSetUpForm.appendixId}'/>" /> <input
					type="hidden" id="jobId" name="jobId"
					value="<c:out value='${JobSetUpForm.jobId}'/>" /> <input
					type="hidden" id="ticketType" name="ticketType"
					value="<c:out value='${JobSetUpForm.ticketType}'/>" /> <input
					type="hidden" id="ticket_id" name="ticket_id"
					value="<c:out value='${JobSetUpForm.ticket_id}'/>" /> <input
					type="button" value="Submit Notification" class="FMBUT0001"
					onclick="submitNote();" /></td>
			</tr>
		</table>
	</body>
</form>

</html>