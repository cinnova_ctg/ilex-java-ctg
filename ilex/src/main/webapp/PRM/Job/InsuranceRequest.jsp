<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src="javascript/date-picker.js"></script>
	<script language = "JavaScript" src = "javascript/UtilityFunction.js"></script>
	
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
	
	$(document).ready(function() {
		$("input[name='send']").click(function(){ 
			
			if(jQuery.trim($("input[name='contactPerson']").val()) == ''){
				alert("Please enter Contact Person.");
				$("input[name='contactPerson']").focus();	
				return false;
			}
			if(jQuery.trim($("input[name='attentionEmailTo']").val()) == ''){
				alert("Please enter Email of <bean:write name="InsuranceRequestForm" property="attention"/>.");
				$("input[name='attentionEmailTo']").focus();	
				return false;
			}
			if(jQuery.trim($("input[name='attentionEmailTo']").val()) != ''){
				if( !isValidAllEmailAddresses(jQuery.trim($("input[name='attentionEmailTo']").val())) ){
					alert("Please enter valid Email of <bean:write name="InsuranceRequestForm" property="attention"/>.");
					$("input[name='attentionEmailTo']").focus();	
					return false;
				}
			}
			if(jQuery.trim($("input[name='attentionAdditionalCC']").val()) != ''){
				if( !isValidAllEmailAddresses(jQuery.trim($("input[name='attentionAdditionalCC']").val())) ){
					alert("Please enter valid email address in Additional CC.");
					$("input[name='attentionAdditionalCC']").focus();	
					return false;
				}
			}			
			if(jQuery.trim($("input[name='holderName']").val()) == ''){
				alert("Please enter Certificate Holder Name.");
				$("input[name='holderName']").focus();	
				return false;
			}
			
			if(jQuery.trim($("input[name='holderAddress']").val()) == ''){
				alert("Please enter Certificate Holder Address.");
				$("input[name='holderAddress']").focus();	
				return false;
			}
			
			if(jQuery.trim($("input[name='holderCity']").val()) == ''){
				alert("Please enter Certificate Holder City.");
				$("input[name='holderCity']").focus();	
				return false;
			}
			if(jQuery.trim($("select[name='holderState']").val()) == '0'){
				alert("Please enter Certificate Holder State.");
				$("select[name='holderState']").focus();	
				return false;
			}
			
			if(jQuery.trim($("input[name='holderZipCode']").val()) == ''){
				alert("Please enter Certificate Holder Zip Code.");
				$("input[name='holderZipCode']").focus();
				return false;	
			}
			
			if(jQuery.trim($("input[name='retName']").val()) == ''){
				alert("Please enter Return To Name.");
				$("input[name='retName']").focus();	
				return false;
			}
			if(jQuery.trim($("input[name='retEmail']").val()) == '' && jQuery.trim($("input[name='retDirectNumber']").val()) == '' && jQuery.trim($("input[name='retFax']").val()) == ''){
				alert("Enter at least one return mehtod.");
				$("input[name='retEmail']").focus();	
				return false;
			}
			
		});
		
		/*Copy Site Address into Holder section*/
		$("input[name='copySiteAddress']").click(function(){
			$("input[name='holderAddress']").val("<bean:write name='InsuranceRequestForm' property='address'/>");
			$("input[name='holderCity']").val("<bean:write name='InsuranceRequestForm' property='city'/>");
			$("select[name='holderState']").val("<bean:write name='InsuranceRequestForm' property='state'/>");
			$("input[name='holderZipCode']").val("<bean:write name='InsuranceRequestForm' property='zipCode'/>");
		});//End $("input[name='copySiteAddress']").click(function()
});
 
 
			
	</script>
	
	<title>Send Certificate of Insurance Request</title>
	
	<%@ include file = "/NMenu.inc" %>
	<%@ include  file="/JobDashboardVariables.inc" %>
	<%@ include  file="/ProjectJobMenu.inc" %> 
</head>
<html:html>
<body>
<html:form action="/InsuranceRequest.do?hmode=sendRequest" enctype = "multipart/form-data">
<html:hidden property="jobId"/>
<html:hidden property="appendixId"/>
<html:hidden property="msaId"/>
	<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Job</span></a>
  			<ul>
				
     				<li><a href="#">Change Status</a></li>
					
 				<li>
 					<a href="#"><span>Addendum</span></a>
					<ul>
						
     						<li><a href="javascript:addendumSendEmail();">Send</a></li>
							
     					<li>
     						<a href="#"><span>View</span></a>
     						<ul>
								
									<li><a href="javascript:addendumViewContractDocument('<%= appendixid %>','pdf');">PDF</a></li>
										
     						</ul>
     					</li>
     					<li><a href="ManageUpliftAction.do?ref=addendumUplift&amp;Job_Id=<%= appendixid %>&amp;Appendix_Id=<%= appendixid %>Manage Uplifts</a></li>
					</ul>
				</li>
 				<li><a href="javascript:alert('Schedule Cannot Be Updated For A Default Job / Addendum')">Update Schedule</a></li>
				<li>
 					<a href="#"><span>Documents</span></a>
					<ul>
	 					<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Addendum&amp;function=viewHistory&amp;appendixid=<%= appendixid %>Addendum History</a></li>
	 					<li>
		 					<a href="#"><span>Support Documents</span></a>
		 					<ul>
			 					<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;jobId=<%= appendixid %>&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement&amp;linkLibName=Addendum">Upload</a>
			 					</li><li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;jobId=<%= appendixid %>&amp;linkLibName=Addendum&amp;function=supplementHistory">History</a>
							</li></ul>
						</li>
					</ul>
				</li>
				
					<li><a href="javascript:alert('An Addendum can only be uploaded when it is in the - Pending Customer Review - status.\nThis feature is only available to support a signed or markup Addendum originally generated from Ilex or \nan alternative document provided by the customer.')">Upload File</a></li>
					
  			</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:addendumManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:addendumSendStatusReport()">Send</a></li>
      			<li><a href="javascript:addendumManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			        <div id="breadCrumb">
				<a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
				<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
				<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
				<a><span class="breadCrumb1">Email Certificate of Insurance Request</span></a>
			    </div></td>
			</tr>
	        <tr><td height="5">&nbsp;</td></tr>	
		</tbody></table>
<%-- <table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
	    <td id="pop1" width="120" height="19" class="Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Job</center></a></td>
		<td id="pop2" width="120" height="19" class="Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center>Status Report</center></a></td>
		<td id="pop3" width="100%" class ="Ntoprow1">&nbsp;</td>
	</tr>
	<tr>
		<td background="images/content_head_04.jpg" height="21" colspan="3">
			<div id="breadCrumb">
				<a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
				<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
				<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
				<a><span class="breadCrumb1">Email Certificate of Insurance Request</span></a>
			</div>
	    </td>
	</tr>
</table>
 --%>
<%@ include  file="/ProjectJobMenuScript.inc" %>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<td>&nbsp;&nbsp;</td>
	<td>
		<table border="0" cellspacing="1" cellpadding="0">
		<c:if test="${not empty requestScope.msg}">
		<tr>
			<td class="message" colspan="2"><c:out value="${requestScope.msg}"/></td>
		</tr>
		</c:if>
		<tr>
			<td colspan="2"><font style="font-size: 10px;">&nbsp;</font></td>
		</tr>
		<tr>
			<td colspan="2" class="labeleboldwhite">
				Certificate of Insurance Request Regarding: Contingent Network Services
			</td>
		</tr>
		<tr>
			<td colspan="2"><font style="font-size: 5px;">&nbsp;</font></td>
		</tr>
		<tr>
			<td class = "labeleboldwhite" nowrap="nowrap">
				<b>Contact Person<font color="red" style="font-weight: bold">*</font>:</b>&nbsp;
			</td>
			<td class = "labeleboldwhite">
				<html:text name="InsuranceRequestForm" property="contactPerson" size="50" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td colspan="2"><font style="font-size: 5px;">&nbsp;</font></td>
		</tr>
		<tr>
			<td class = "labeleboldwhite">
				<b>Today's Date:</b>&nbsp;
			</td>
			<td class = "labeleboldwhite">
				<bean:write name="InsuranceRequestForm" property="todayDate"/>
			</td>
		</tr>
		<tr>
			<td colspan="2"><font style="font-size: 10px;">&nbsp;</font></td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class = "colDarkSmall" style="padding-left: 10px;"><b>Attention:</b></td>
					<td class = "formCaption"><b>&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="InsuranceRequestForm" property="attention"/></b></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Email To<font color="red" style="font-weight: bold">*</font>:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="attentionEmailTo" size="50" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Direct:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="attentionDirect" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Fax:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="attentionFax" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Additional CC:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="attentionAdditionalCC" size="50" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td colspan="2"><font style="font-size: 5px;">&nbsp;</font></td>
		</tr>
		<tr>
			<td class = "formCaption" colspan="2">
				<b>Certificate Holder:</b>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Name<font color="red" style="font-weight: bold">*</font>:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="holderName" size="50" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Address<font color="red" style="font-weight: bold">*</font>:
			</td>
			<td class = "colLight">
				<b><html:text name="InsuranceRequestForm" property="holderAddress" size="50" styleClass="text"/></b>
				<html:button property="copySiteAddress" styleClass="addButton_c" style="pa">Copy Site Address</html:button>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				City<font color="red" style="font-weight: bold">*</font>:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="holderCity" size="50" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				State<font color="red" style="font-weight: bold">*</font>:
			</td>
			<td class = "colLight">
				<html:select name="InsuranceRequestForm" property = "holderState"  styleClass="select">
					<logic:iterate id="stateList" name="InsuranceRequestForm" property="holderStateList" >
						<logic:notEqual name="stateList" property="siteCountryIdName" value="notShow">
    					<optgroup class="select" label="<bean:write name = "stateList" property = "siteCountryIdName" />">
	    				</logic:notEqual>
						<html:optionsCollection name = "stateList" property="tempList" value = "value" label = "label" />
						<logic:notEqual name="stateList" property="siteCountryIdName" value="notShow">
	   					</optgroup>
	    				</logic:notEqual>
					</logic:iterate>
				</html:select>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Zip Code<font color="red" style="font-weight: bold">*</font>:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="holderZipCode" styleClass="text"/>
			</td>
		</tr>
		
		<tr>
			<td colspan="2"><font style="font-size: 5px;">&nbsp;</font></td>
		</tr>
		<tr>
			<td class = "formCaption" colspan="2">
				<b>Return To:</b>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Name<font color="red" style="font-weight: bold">*</font>:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="retName" size="50" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Email:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="retEmail" size="50" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Direct Number:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="retDirectNumber" styleClass="text"/>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Fax:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="retFax" styleClass="text"/>
			</td>
		</tr>
		
		<tr>
			<td colspan="2"><font style="font-size: 5px;">&nbsp;</font></td>
		</tr>
		<tr>
			<td class = "formCaption" colspan="2">
				<b>Job/Site/Insurance Details:</b>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Schedule Start:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="scheduleStart" size="10" styleClass="text" readonly="true"/>
				<img style="vertical-align: top;" src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].scheduleStart , document.forms[0].scheduleStart , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
			</td>
		</tr>
		
		<tr>
			<td class = "colDarkSmall">
				Schedule End:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="scheduleEnd" size="10" styleClass="text" readonly="true"/>			
				<img style="vertical-align: top;" src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].scheduleEnd , document.forms[0].scheduleEnd , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
			</td>
		</tr>
		
		<tr>
			<td class = "colDarkSmall">
				Site Number:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="siteNumber" size="50" styleClass="text"/>
			</td>
		</tr>
		
				<tr>
			<td class = "colDarkSmall">
				Address:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="address" size="50" styleClass="text"/>
			</td>
		</tr>
		
		<tr>
			<td class = "colDarkSmall">
				City:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="city" size="50" styleClass="text"/>
			</td>
		</tr>
		
		<tr>
			<td class = "colDarkSmall">
				State:
			</td>
			<td class = "colLight">
				<html:select name="InsuranceRequestForm" property = "state"  styleClass="select">
					<logic:iterate id="stateList" name="InsuranceRequestForm" property="holderStateList" >
						<logic:notEqual name="stateList" property="siteCountryIdName" value="notShow">
    					<optgroup class="select" label="<bean:write name = "stateList" property = "siteCountryIdName" />">
	    				</logic:notEqual>
						<html:optionsCollection name = "stateList" property="tempList" value = "value" label = "label" />
						<logic:notEqual name="stateList" property="siteCountryIdName" value="notShow">
	   					</optgroup>
	    				</logic:notEqual>
					</logic:iterate>
				</html:select>
			</td>
		</tr>
		<tr>
			<td class = "colDarkSmall">
				Zip Code:
			</td>
			<td class = "colLight">
				<html:text name="InsuranceRequestForm" property="zipCode" styleClass="text"/>
			</td>
		</tr>
		
		<tr>
			<td class = "colDarkSmall">
				Insurance<br>Requirements:
			</td>
			<td class = "colLight">
				<html:textarea name="InsuranceRequestForm" property="insuranceRequirements" cols="60" rows="4" styleClass="textarea"/>
			</td>
		</tr>
		
		<tr>
			<td class = "colDarkSmall">
				Additional<br>Information:
			</td>
			<td class = "colLight">
				<html:file name="InsuranceRequestForm" property="additionalInfo" styleClass="button_c" size="30"></html:file>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="colLight">
				<html:submit property="send" styleClass="button_c">Send Certificate Request</html:submit>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</html:form>
</body>
</html:html>