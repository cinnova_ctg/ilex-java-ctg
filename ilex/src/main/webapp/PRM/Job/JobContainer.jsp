<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.mind.bean.newjobdb.JobDashboardTabType"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<!--<jsp:include page="/common/IncludeGoogleKey.jsp"/>-->
<head>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/summary.css" rel="stylesheet" type="text/css" />
<link href="styles/jquery.ui.smoothness.css" rel="stylesheet" type="text/css" />

<script language='JavaScript' src='javascript/jquery-1.11.0.min.js'></script>
<script language='JavaScript' src='javascript/jquery-ui.min.js'></script>
<script language='JavaScript' src='javascript/noty/jquery.noty.js'></script>

<!-- noty layouts [Requires JQuery] -->
<script type="text/javascript" src="javascript/noty/layouts/bottom.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/bottomCenter.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/bottomLeft.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/bottomRight.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/center.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/centerLeft.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/centerRight.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/inline.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/top.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/topCenter.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/topLeft.js"></script>
<script type="text/javascript" src="javascript/noty/layouts/topRight.js"></script>

<!-- noty themes [Requires JQuery] -->
<script type="text/javascript" src="javascript/noty/themes/default.js"></script>

<title>Job Dashboard</title>

<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>

<script>
$(document).ready(function() {
    if (typeof selectedTab !== "undefined") {
        $('#'+selectedTab).addClass('active');
    }
});
     
function show_noty(type, layout, text) {
    var n = noty({
        layout: layout,
        theme: 'defaultTheme',
        text: text,
        type: type,
        timeout: 5000
    });
}
</script>
</head>
<body id="pbody"> 
<c:if test ="${not empty requestScope.changestatusflag && requestScope.changestatusflag eq '0' && not empty requestScope.isTicket && requestScope.isTicket eq 'Ticket'}">
    <script>
        parent.ilexleft.location.reload();
    </script>
</c:if>
<c:if test ="${not empty requestScope.refershtree && requestScope.refershtree eq 'refershtree'}">
    <script>
        parent.ilexleft.location.reload();
    </script>
</c:if>



	<form action="JobDashboardAction" method="post">
	<input type="hidden" name="appendixId" value="<c:out value='${JobSetUpForm.appendixId}'/>"/>
	<input type="hidden" name="jobId" value="<c:out value='${JobSetUpForm.jobId}'/>"/>
	<input type="hidden" name="site_id" value="<c:out value='${JobSetUpForm.site_id}'/>"/>
	<input type="hidden" name="ticket_id" value="<c:out value='${JobSetUpForm.ticket_id}'/>"/>
	<input type="hidden" name="isClicked" value="<c:out value='${JobSetUpForm.isClicked}'/>"/>
	<html:hidden name="JobSetUpForm" property="ticketNumber"/>
	<html:hidden name="JobSetUpForm" property="msaId"/>
	<input type="hidden" name="jobIndicator" value="<c:out value='${JobSetUpForm.jobIndicator}'/>"/>
	
	<c:if test="${requestScope.tabStatus eq 'Default'}" >
		<%@ include  file="/Menu_DefaultJob.inc" %>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
						 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
						 <a><span class="breadCrumb1"><c:out value='${requestScope.jobName }'/></span></a>
			    </td>
			</tr>
	        <tr><td height="5" >&nbsp;</td></tr>	
		</table>
	</c:if>
	<c:if test="${requestScope.tabStatus eq 'Addendum'}" >
		<%@ include  file="/Menu_Addendum.inc" %>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
						 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
						 <a><span class="breadCrumb1"><c:out value='${requestScope.jobName }'/></span></a>
			    </td>
			</tr>
	        <tr><td height="5" >&nbsp;</td></tr>	
		</table>

	</c:if>
	<c:if test="${  requestScope.tabStatus eq 'job' || (requestScope.isTicket eq 'Ticket' && requestScope.tabStatus eq 'existingTicket')}">
		<%@ include  file="/Menu_ProjectJob.inc" %>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
				         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										 <a ><c:out value='${requestScope.jobName }'/></a>
										  <a><span class="breadCrumb1"><c:if test="${ requestScope.tabStatus eq 'job'}">Job Dashboard<c:if test="${not empty JobSetUpForm.jobIndicator && JobSetUpForm.jobIndicator ne 'None'}"> (<c:out value='${JobSetUpForm.jobIndicator}'/>)</c:if></c:if>
										  								<c:if test="${requestScope.tabStatus eq 'existingTicket'}">Ticket Dashboard<c:if test="${not empty JobSetUpForm.jobIndicator && JobSetUpForm.jobIndicator ne 'None'}"> (<c:out value='${JobSetUpForm.jobIndicator}'/>)</c:if></c:if>
										  </a>
				    </td>
				</tr>
		        <tr><td height="5" >&nbsp;</td></tr>	
		</table>
	</c:if>
	<c:if test="${(requestScope.isTicket eq 'Ticket' && requestScope.tabStatus eq 'newTicket')}">
		<%@ include  file="/DashboardVariables.inc" %>
		<%@ include  file="/NetMedXDashboardMenu.inc" %>
		<!-- change menu  -->
	<div id="menunav">
<%if(!nettype.equals("dispatch")){ %>  
<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
        	
        		
				<!-- <li><a href="ReportScheduleWindow.do?report_type=redline&typeid=3970&fromPage=NetMedX">Redline Report</a></li>
				<li><a href="report_type=detailsummary&typeid=3970&fromPage=NetMedX">Detailed Summary</a></li>
				<li><a href="ReportScheduleWindow.do?report_type=CSVSummary&typeid=3970&fromPage=NetMedX">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="ReportGeneration.do?firsttime=true">Update Report</a></li></ul></li></ul></li>
        	 -->
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>&nettype=<%=nettype %>">Search</a>
  		</li>
  		
	</ul>
</li>
<%}else{%>
<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
			
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>&nettype=<%=nettype %>">Search</a>
  		</li>
  		</li>
	<%} %>
</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			     <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<%= appendixid %>"><c:out value='${requestScope.appendixName }'/></a>
										 <a ><span class="breadCrumb1">Ticket Dashboard</span></a>
								</div>	
			    </div></td>
			</tr>
	        <tr><td height="5">&nbsp;</td></tr>	
		</tbody></table>
		
		
		
		
		<!--  chnage menu  -->
			 <%-- <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<%if(!nettype.equals("dispatch")){ %>
					<td class=Ntoprow1 id=pop1 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					<%}else{%>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px">Reports</a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>&nettype=<%=nettype %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=640 colspan="2">&nbsp;</td>
					<%} %>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		  <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										 <a ><span class="breadCrumb1">Ticket Dashboard</span></a>
								</div>		
			        </td>
		        </tr>
		        <tr><td height="5" >&nbsp;</td></tr> 
	      </table> --%>
	    <%--   <%@ include  file="/NetMedXDashboardMenuScript.inc" %> --%>
	   </c:if>
  
	<table id="main">
		<tr>
			<td >
				<c:if test="${not empty requestScope.msg}">
					<table>
						<tr>
							<td class="message" style="padding-left: 10px;">
								<c:out value="${requestScope.msg}"/>
							</td>
						</tr>
					</table>
				</c:if>
				<c:if test="${not empty requestScope.SAVE}">
					<table>
						<tr>
							<td class="message" style="padding-left: 10px;">
								<c:out value="${requestScope.SAVE}"/>
							</td>
						</tr>
					</table>
				</c:if>
				<c:if test="${requestScope.isAccepted eq 'notAccepted'}">
					<table>
						<tr>
							<td class="message"  style="padding-left: 10px;">
								The Job Can't be marked onsite unless at least one of it's PO is in Accepted State.
							</td>
						</tr>
					</table>
				</c:if>
				<c:if test="${requestScope.reAssignFail eq 'reAssignFail'}">
					<table>
						<tr>
							<td class="message"  style="padding-left: 10px;">
								PO cannot be reassigned to same partner.
							</td>
						</tr>
					</table>
				</c:if>
				
				<logic:present name = "jobIndicatorFlag" scope = "request">
					<c:if test="${requestScope.jobIndicatorFlag eq '0'}">
						<table>
							<tr>
								<td class="message"  style="padding-left: 10px;">
									<bean:message bundle = "PRM" key = "prm.job.testIndicator.success"/>
								</td>
							</tr>
						</table>
					</c:if>
					<c:if test="${requestScope.jobIndicatorFlag ne '0'}">
						<table>
							<tr>
								<td class="message"  style="padding-left: 10px;">
									<bean:message bundle = "PRM" key = "prm.job.testIndicator.failure"/>
								</td>
							</tr>
						</table>
					</c:if>
				</logic:present>
				
				<logic:present name="addendumChangeStatus" scope="request">
					<c:if test="${requestScope.addendumChangeStatus eq '96004'}">
						<table>
							<tr>
								<td class="message"  style="padding-left: 10px;">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.mysql.failure13"/>
								</td>
							</tr>
						</table>
					</c:if>
					<c:if test="${requestScope.addendumChangeStatus ne '96004' && requestScope.addendumChangeStatus ne '0'}">
						<table>
							<tr>
								<td class="message"  style="padding-left: 10px;">
									The addendum could not be transitioned to In work due to a problem on the Web server.
								</td>
							</tr>
						</table>
					</c:if>
				</logic:present>
				
				<logic:present name = "addflag" scope = "request">
				
					<tr>
						<td colspan = "4" class = "message" height = "30">
				    		<logic:equal name = "addflag" value = "0"><bean:message bundle = "pm" key = "activity.tabular.add.success"/><script>parent.ilexleft.location.reload();</script></logic:equal>
							<logic:equal name = "addflag" value = "-90030"><bean:message bundle = "pm" key = "activity.tabular.add.failure9"/></logic:equal>
							<logic:equal name = "addflag" value = "-90018"><bean:message bundle = "pm" key = "activity.tabular.add.failure1"/></logic:equal>
							<logic:equal name = "addflag" value = "-90020"><bean:message bundle = "pm" key = "activity.tabular.add.failure2"/></logic:equal>
							<logic:equal name = "addflag" value = "-9002"><bean:message bundle = "pm" key = "activity.tabular.add.failure3"/></logic:equal>
							<logic:equal name = "addflag" value = "-9003"><bean:message bundle = "pm" key = "activity.tabular.add.failure4"/></logic:equal>
							<logic:equal name = "addflag" value = "-9005"><bean:message bundle = "pm" key = "activity.tabular.add.failure5"/></logic:equal>
							<logic:equal name = "addflag" value = "-9006"><bean:message bundle = "pm" key = "activity.tabular.add.failure6"/></logic:equal>
							<logic:equal name = "addflag" value = "-9007"><bean:message bundle = "pm" key = "activity.tabular.add.failure7"/></logic:equal>
							<logic:equal name = "addflag" value = "-9008"><bean:message bundle = "pm" key = "activity.tabular.add.failure8"/></logic:equal>
						</td>
					</tr>
				
				</logic:present>
				<table cellspacing="0" cellpadding="0"  border="0" style="padding-left: 12px;" >
					<tr>
						<td>
							<jsp:include page="JobTabSubContainer.jsp"/>
						</td>
					</tr>
					<tr>
						<td>
							<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;
		     	 				height="100%" cellspacing="0" cellpadding="0" width="100%">
								<tr id="tab">
									<td>
										<table>
											<tr>
												<c:if test="${requestScope.HidePage ne 'HidePage'}">
												<td>
													<% 
													String tabId = (String)request.getAttribute("tabId");
													String selectedTab = "";
													tabId = (tabId==null)?"":tabId;
													%>
													<%if(tabId.equals(""+JobDashboardTabType.FINANCIAL_TAB)){%>
														<jsp:include page="JobFinancialTab.jsp"/>
														<%
														selectedTab = "tab-0";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.SETUP_TAB)){%>
														<jsp:include page="JobSetUp.jsp"/>
														<%
														selectedTab = "tab-1";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.TICKET_TAB)){%>
														<jsp:include page="Ticket.jsp"/>
														<%
														selectedTab = "tab-2";
													} else {%>
 														<c:if test="${(requestScope.isTicket eq 'Ticket' && (requestScope.tabStatus eq 'newTicket' || JobSetUpForm.webTicket eq 'webTicket'))}">
															<jsp:include page="Ticket.jsp"/>
															<% 
															selectedTab = "tab-2";
															%>
														</c:if>
 													<%}%>
													<%if(tabId.equals(""+JobDashboardTabType.BUY_TAB)){%>
														<jsp:include page="JobBuyTab.jsp"/>
														<%
														selectedTab = "tab-3";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.EXECUTE_TAB)){%>
														<jsp:include page="JobExecuteTab.jsp"/>
														<%
														selectedTab = "tab-4";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.COMPLETE_TAB)){%>
														<jsp:include page="JobCompleteTab.jsp"/>
														<%
														selectedTab = "tab-5";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.CLOSE_TAB)){%>
														<jsp:include page="JobCloseTab.jsp"/>
														<%
														selectedTab = "tab-6";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.SCHEDULE_TAB)){%>
														<jsp:include page="ScheduleTab.jsp"/>
														<%
														selectedTab = "tab-7";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.DISPATCH_SCHEDULE_TAB)){%>
														<jsp:include page="JobDispatchSchedule.jsp"/>
														<%
														selectedTab = "tab-8";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.SITE_TAB)){%>
														<jsp:include page="JobSiteTab.jsp"/>
														<%
														selectedTab = "tab-9";
													}%>
													<%if(tabId.equals(""+JobDashboardTabType.VENDEX_TAB)){%>
														<jsp:include page="JobVendexTab.jsp"/>
														<%
														selectedTab = "tab-10";
													}%>
													<script>
														var selectedTab = '<%=selectedTab %>';
													</script>
												</td>
												</c:if>
												<c:if test="${requestScope.HidePage eq 'HidePage'}">
													<td>
														<jsp:include page="/UnAuthenticate.jsp">
															<jsp:param name = 'fromJsp' value = 'JobContainer.jsp'/>
														</jsp:include>
													</td>
												</c:if>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
</body>
</html>
<script>
<%
String tabId1 = (String)request.getAttribute("tabId");
tabId1 = (tabId1==null)?"":tabId1;%>
<%if(tabId1.equals(""+JobDashboardTabType.VENDEX_TAB)){%>
	window.onbeforeunload = callGmap();
<%}%>
</script>