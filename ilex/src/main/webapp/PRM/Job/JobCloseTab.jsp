<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script>
function viewInstallNotes(){
	var url;
	url = "JobCloseAction.do?hmode=viewInstallNotes&type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=1&saveJobActions=1";							
	alert(url);
	document.location.href= url;
}
</script>
<table width="100%">
	<tr>
		<td>
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
	<tr>
		<td >
			<jsp:include page="JobWorkflow.jsp"/>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td valign="top" style="padding-top: 9px">
									<jsp:include page="JobInvoiceSummary.jsp"/>
								</td>
							</tr>
							<tr>
								<td valign="top">
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td style="padding-left: 10px;">
						&nbsp;
					</td>
					<td valign="top">
						<jsp:include page="PurchaseOrder.jsp"/>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<jsp:include page="ViewInstallNotes.jsp"/>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<jsp:include page="JobPlanningNotes.jsp"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
