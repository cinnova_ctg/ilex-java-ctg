<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<table cellspacing="1" cellpadding="0" class="Ntextoleftalignpaleyellowborder2wrap">
				<tr>
				<td>
					<table class="Ntextoleftalignpaleyellowborder2wrap">
					<col width="250px;">
					<col width="350px;">
						<tr>
						   	<td class = "texthdNoBorder1wrap" align="center"><b>Customer Required Data </td>
						   	<td class = "texthdNoBorder1wrap"  align="center"><b>Value</td>
						</tr>
						<c:if test="${CustomerRequireForm.displayStatus eq false}">
						<tr>
							<td class="Nhyperevenfont1wrap"  style="padding-left: 10px;" colspan="2">
								No Parameters Defined.
							</td>
						</tr>
						</c:if>
						<c:if test="${CustomerRequireForm.displayStatus eq true}">
							<tr>
							<td class="Ntexteleftalignwrap">First Visit Success ?</td>
							<td class="Ntexteleftalignwrap">
							<logic:equal value="true" property="firstVisitQuestion" name="CustomerRequireForm">Yes</logic:equal>
							<logic:equal value="false" property="firstVisitQuestion" name="CustomerRequireForm">No</logic:equal>
							</td>
							</tr>
							<logic:equal value="false" property="firstVisitQuestion" name="CustomerRequireForm">
							<tr>
								<td class="Ntextoleftalignwrap">Who is responsible ?</td>
								<td class="Ntextoleftalignwrap">
								
								<c:forEach var="category"  items="${CustomerRequireForm.selectedCategoryValues}" varStatus="index">
										<b>${category.key}&nbsp;:&nbsp;</b>
										<c:forEach var="categoryvalues" items="${category.value}" varStatus="status">
											${categoryvalues.label}[${categoryvalues.value}]<c:if test="${status.last eq false}">,</c:if>&nbsp;
										</c:forEach>
										<c:if test="${index.last eq false}"><br/></c:if>
										
								</c:forEach>
								
							
								</td>
							</tr>	
							<tr>
							<td class="Ntexteleftalignwrap">Additional site visits ?</td>
							<td class="Ntexteleftalignwrap">
							<logic:equal value="true" property="addSiteVisitQuestion" name="CustomerRequireForm">Yes&nbsp;:&nbsp;
							Unplanned Additional Visits-${CustomerRequireForm.unplannedAddVisit}
							</logic:equal>
							<logic:equal value="false" property="addSiteVisitQuestion" name="CustomerRequireForm">No</logic:equal>
							</td>
							</tr>
							</logic:equal>
						</c:if>
					</table>
				</td>
				</tr>
	</table>
</body>
</html>