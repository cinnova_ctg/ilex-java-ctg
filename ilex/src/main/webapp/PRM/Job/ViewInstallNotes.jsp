<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<LINK href="styles/content.css" rel="stylesheet" type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<script language="JavaScript" src="javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script>
function viewInstallNotes(){
	var url;
	url = "JobCloseAction.do?hmode=viewInstallNotes&type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=1&saveJobActions=1";							
	alert(url);
	document.location.href= url;
}
</script>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td  class = "dbvaluesmallFontBold" style="padding-left: 0px;" valign="top">Installation Notes/Comments&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table cellpadding=0 cellspacing=1 class="Ntextoleftalignnowrappaleyellowborder7" width="100%">
				 <c:if test="${not empty requestScope.installNotesList}">
					 <c:forEach items="${requestScope.installNotesList}" var="list" varStatus="index">										   			
						<tr >
							<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignwrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignwrap'</c:if> width="100%" style="white-space: normal;"><c:out value='${list.date}'/> - <c:out value='${list.installNote}' escapeXml="false"/></td>	
						</tr>
					</c:forEach>
				</c:if>
				<c:if test="${empty requestScope.installNotesList}">
						<tr >
							<td class='Ntextoleftalignwrap' width="100%"> - </td>	
						</tr>
				</c:if>
			</table>
		</td>
	</tr>	
</table>