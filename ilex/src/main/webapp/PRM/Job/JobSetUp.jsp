<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<table>
	<c:choose>
	<c:when test="${requestScope.tabStatus eq 'job'}">
	<tr>
		<td>
			<jsp:include page="GeneralJobInformation.jsp"/>
		</td>
	</tr>
		<tr>
			<td>
				<jsp:include page="JobWorkflow.jsp"/>
			</td>
		</tr>
	</c:when>
	<c:when test="${requestScope.tabStatus eq 'Addendum'}">
	<tr>
		<td>
			<jsp:include page="GeneralAddendumInformation.jsp"/>
		</td>
	</tr>
		
	</c:when>
	</c:choose>
	<tr>
		<td>
			<jsp:include page="ActivityManagement.jsp"/>
		</td>
	</tr>
		<tr>
			<td >
				<c:if test="${requestScope.tabStatus eq 'job'||requestScope.tabStatus eq 'Default' || JobSetUpForm.isAddendumApproved eq 'Y' || requestScope.isTicket eq 'Ticket'}">
				<jsp:include page="JobPlanningNotes.jsp"/>
				</c:if>
			</td>
		</tr>
</table>