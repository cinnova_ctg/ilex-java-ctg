<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />

<table  border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td colspan="2"><h3>Labor Analysis</h3></td>
</tr>
<tr>
	<td class="dbvaluesmallFont"><b>Average Contract Labor Cost Analysis</b></td>
</tr>
<tr><td class="dbvaluesmallFont">&nbsp;</td></tr>
<tr>
	<td>
		<table  cellpadding="0" cellspacing="0" width="100%">
		<tr height="25">
			<td class="labelodarkbgblueborder" width="40%" style="border-right: 0px;border-bottom: 0px;"><b>Labor Resource</b></td>
			<td class="labelodarkbgblueborder" width="30%" style="border-right: 0px;border-bottom: 0px;"><b>Job</b></td>
			<td class="labelodarkbgblueborder" width="30%" style="border-bottom: 0px;"><b>Project</b></td>
		</tr>
		<c:forEach items="${JobSetUpForm.costAnalysis}" var="costAnalysis" varStatus="index">
		<tr>
			<c:if test="${not index.last}">
			<td class="labelorightwhitebgblueborder" style="border-right: 0px;border-bottom: 0px;"><c:out value='${costAnalysis.laborResource}'/></td>
			<td class=labelorightwhitebgblueborder style="border-right: 0px;border-bottom: 0px;"><fmt:formatNumber value='${costAnalysis.job}' type="currency" pattern="#,###,##0.00"/></td>
			<td class="labelorightwhitebgblueborder" style="border-bottom: 0px;"><fmt:formatNumber value='${costAnalysis.project}' type="currency" pattern="#,###,##0.00"/></td>
			</c:if>
			
			<c:if test="${index.last}">
			<td class="labelorightwhitebgblueborder" style="border-right: 0px;"><c:out value='${costAnalysis.laborResource}'/></td>
			<td class=labelorightwhitebgblueborder style="border-right: 0px;"><fmt:formatNumber value='${costAnalysis.job}' type="currency" pattern="#,###,##0.00"/></td>
			<td class="labelorightwhitebgblueborder"><fmt:formatNumber value='${costAnalysis.project}' type="currency" pattern="#,###,##0.00"/></td>
			</c:if>
			
		</tr>
		</c:forEach>
		</table>
	</td>
</tr>
<tr height="10"><td>&nbsp;</td></tr>
<tr>
	<td class="dbvaluesmallFont"><b>Mintuteman/Speedpay Usage and Analysis</b></td>
</tr>
<tr><td class="dbvaluesmallFont">&nbsp;</td></tr>
<tr>
	<td>
		<table  cellpadding="0" cellspacing="0" width="100%">
		<tr height="25">
			<td class="labelodarkbgnoborder" width="25%">&nbsp;</td>
			<td class="labelodarkbgblueborder" width="25%" style="border-right: 0px;border-bottom: 0px;"><b>Labor/Travel</b></td>
			<td class="labelodarkbgblueborder" width="25%" style="border-right: 0px;border-bottom: 0px;"><b>Usage</b></td>
			<td class="labelodarkbgblueborder" width="25%" style="border-bottom: 0px;"><b>Cost Saving</b></td>
		</tr>
		<tr>
			<td class="labelorightwhitebgblueborder" style="border-right: 0px;border-bottom: 0px; text-align: center;"><b>Minuteman</b></td>
			<td class="labelorightwhitebgblueborder" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.minutemanLaborTravel}'/></td>
			<td class="labelorightwhitebgblueborder" style="border-right: 0px;border-bottom: 0px;"><c:out value='${JobSetUpForm.minutemanUsage}'/></td>
			<td class="labelorightwhitebgblueborder" style="border-bottom: 0px;"><c:out value='${JobSetUpForm.minutemanCostSavings}'/></td>
		</tr>
		<tr>
			<td class="labelorightwhitebgblueborder" style="border-right: 0px; text-align: center;"><b>Speedpay</b></td>
			<td class="labelorightwhitebgblueborder" style="border-right: 0px;"><c:out value='${JobSetUpForm.speedpayLaborTravel}'/></td>
			<td class="labelorightwhitebgblueborder" style="border-right: 0px;"><c:out value='${JobSetUpForm.speedpayUsage}'/></td>
			<td class="labelorightwhitebgblueborder" ><c:out value='${JobSetUpForm.speedpayCostSavings}'/></td>
		</tr>
		</table>
	</td>
</tr>
</table>
