<%@ page import="com.mind.bean.newjobdb.JobDashboardTabType"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<head>
	<script>
		function fun_HideShowBlock(index,isClicked, tabId)
		{
		 	<%if(((String)request.getAttribute("isTicket")).equalsIgnoreCase("Ticket")){%>
				var tabIds = new Array(
							'<%=JobDashboardTabType.FINANCIAL_TAB%>',
							'<%=JobDashboardTabType.SETUP_TAB%>',
							'<%=JobDashboardTabType.TICKET_TAB%>',
							'<%=JobDashboardTabType.BUY_TAB%>',
							'<%=JobDashboardTabType.EXECUTE_TAB%>',
							'<%=JobDashboardTabType.COMPLETE_TAB%>',
							'<%=JobDashboardTabType.CLOSE_TAB%>',
							'<%=JobDashboardTabType.DISPATCH_SCHEDULE_TAB%>',
							'<%=JobDashboardTabType.SITE_TAB%>',
							'<%=JobDashboardTabType.VENDEX_TAB%>'
							);
			<%} else {%>
				var tabIds = new Array(
							'<%=JobDashboardTabType.FINANCIAL_TAB%>',
							'<%=JobDashboardTabType.SETUP_TAB%>',
							'<%=JobDashboardTabType.BUY_TAB%>',
							'<%=JobDashboardTabType.EXECUTE_TAB%>',
							'<%=JobDashboardTabType.COMPLETE_TAB%>',
							'<%=JobDashboardTabType.CLOSE_TAB%>',
							'<%=JobDashboardTabType.SCHEDULE_TAB%>',
							'<%=JobDashboardTabType.SITE_TAB%>',
							'<%=JobDashboardTabType.VENDEX_TAB%>'
							);
		 	<%}%>


			if(tabId=='') {
				//replace the tabId with index
				tabId=index;
				for(i=0;i<tabIds.length;i++) {
					if((tabIds[i])==tabId) {
						index = i;
						break;
					}
				}
			}
		
			<c:if test="${(requestScope.isTicket eq 'Ticket' && requestScope.tabStatus eq 'newTicket')}">
				if(index==2) {
					tabRefresh(index,isClicked, tabId);
				}
			</c:if>
			<c:if test="${requestScope.tabStatus ne 'newTicket' && requestScope.tabStatus ne 'Default' && requestScope.tabStatus ne 'Addendum'}">
				tabRefresh(index,isClicked, tabId);
			</c:if>
			<c:if test="${requestScope.tabStatus eq 'Default' || requestScope.tabStatus eq 'Addendum'}">
				if(index==0 || index==1 ) {
				tabRefresh(index,isClicked, tabId);
				}
			</c:if>
		}
	</script>
	<script>
		function tabRefresh(index,isClicked, tabId)
		{			
			if(isClicked!='')
				{
					var url;
					url = "JobDashboardAction.do?type="+tabId+"&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked="+isClicked;								
					
					document.forms[0].action=url;
					document.forms[0].submit();
				}

				else{
					buttonPressed(index);
				}
		}
	</script>
	<script>
		function buttonPressed(idx)
			{
				for(var i=0;i<document.all.TOP_BUTTON.length/3;i++) {
					document.all.TOP_BUTTON[3*i+1].style.fontSize='10';
					//document.all.TOP_BUTTON[3*i+1].width='150';
					document.all.TOP_BUTTON[3*i+1].width=document.all.TOP_BUTTON[3*i+1].innerHTML.length*8;
					document.all.TOP_BUTTON[3*i+1].style.fontFamily='Verdana|Arial|Helvetica|sans-serif';
					document.all.TOP_TAB[i].style.width='5';
	
					if(i==idx)
					{
						/*if(idx==0){
							document.all.TOP_BUTTON[3*i].style.display='block';
							document.all.TOP_BUTTON[3*i+1].style.display='block';
							document.all.TOP_BUTTON[3*i+2].style.display='block';
						}*/
						document.all.TOP_BUTTON[3*i].innerHTML='<img src="images/left-cor-light.gif" width="5" height="18" border="0">';
						document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#D9DFEF';
						document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="images/right-cor-light.gif" width="5" height="18"  border="0">';
						document.all.TOP_BUTTON[3*i+1].style.borderBottom = '0px #D9DFEF solid';
						document.all.TOP_BUTTON[3*i+1].style.fontWeight='bold';
					}
					else//Restore Remaining
					{
						/*if(idx==0){
							document.all.TOP_BUTTON[3*i].style.display='none';
							document.all.TOP_BUTTON[3*i+1].style.display='none';
							document.all.TOP_BUTTON[3*i+2].style.display='none';
						 }else{*/
							document.all.TOP_BUTTON[3*i].innerHTML='<img src="images/left-cor.gif" width="5" height="18"  border="0">';
							document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#B5C8D9';
							document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="images/right-cor.gif" width="5" height="18"  border="0">';
						    document.all.TOP_BUTTON[3*i+1].style.borderBottom = '2px #B5C8D9 solid';
							document.all.TOP_BUTTON[3*i+1].style.fontWeight='normal';
						//}
					}
				}
			}
	
	</script>
</head>
<div class="tab_nav">
	<ul>
		<% 
		String[][] tabNames = {
 			{"Financial",""+JobDashboardTabType.FINANCIAL_TAB},
 			{"Setup",""+JobDashboardTabType.SETUP_TAB},
 			{"Buy",""+JobDashboardTabType.BUY_TAB},
 			{"Execute",""+JobDashboardTabType.EXECUTE_TAB},
 			{"Complete",""+JobDashboardTabType.COMPLETE_TAB},
 			{"Close",""+JobDashboardTabType.CLOSE_TAB},
 			{"Schedule",""+JobDashboardTabType.SCHEDULE_TAB},
 			{"Site",""+JobDashboardTabType.SITE_TAB},
 			{"Vendex Views",""+JobDashboardTabType.VENDEX_TAB}
		};
		
		if(((String)request.getAttribute("isTicket")).equalsIgnoreCase("Ticket")) {
			String[][] tabNamesForNetMedX = {
	 			{"Financial",""+JobDashboardTabType.FINANCIAL_TAB},
	 			{"Setup",""+JobDashboardTabType.SETUP_TAB},
	 			{"Ticket",""+JobDashboardTabType.TICKET_TAB},
	 			{"Buy",""+JobDashboardTabType.BUY_TAB},
	 			{"Execute",""+JobDashboardTabType.EXECUTE_TAB},
	 			{"Complete",""+JobDashboardTabType.COMPLETE_TAB},
	 			{"Close",""+JobDashboardTabType.CLOSE_TAB},
	 			{"Dispatch Schedule",""+JobDashboardTabType.DISPATCH_SCHEDULE_TAB},
	 			{"Site",""+JobDashboardTabType.SITE_TAB},
	 			{"Vendex Views",""+JobDashboardTabType.VENDEX_TAB}
			};
	 		
			tabNames = tabNamesForNetMedX;
		}
		
		if(((String)request.getAttribute("tabStatus")).equalsIgnoreCase("Default") || ((String)request.getAttribute("tabStatus")).equalsIgnoreCase("Addendum")) {
			String[][] tabNamesForNetMedX = {
	 			{"Financial",""+JobDashboardTabType.FINANCIAL_TAB},
	 			{"Setup",""+JobDashboardTabType.SETUP_TAB}
			};
			tabNames = tabNamesForNetMedX;
		}
			
		for(int tabIndex = 0; tabIndex < tabNames.length; tabIndex++) {
			%>
			<li id="tab-<%=tabNames[tabIndex][1]%>"><a onclick="fun_HideShowBlock('<%=tabIndex%>','<%=tabIndex%>','<%=tabNames[tabIndex][1]%>');"><%=tabNames[tabIndex][0]%></a></li>
			<%
		}

		%>
                <c:if test="${(requestScope.isTicket ne 'Ticket' || requestScope.tabStatus ne 'newTicket')}">
                    <li id="tab-manage_emails"><a target="_blank" href="/content/ilex/#/job/<c:out value='${JobSetUpForm.jobId}'/>/email_recipients">Email Recipients</a></li>
                </c:if>
	</ul>
</div>
