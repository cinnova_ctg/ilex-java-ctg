<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />

<table  border="0" cellpadding="0" cellspacing="0" width="100%" >
<tr>
	<td colspan="2"><h3>Revenue and VGPM</h3></td>
</tr>
<tr class="paddingLeft" style="padding-left:10px;">
	<td class="Ntextoleftalignnowrappaleyellowborder2">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class = "dbvaluesmallFont" style="padding-left: 8px;">
					<b>Extended Price:</b>&nbsp;&nbsp;&nbsp;
				</td>
				<td class = "dbvaluesmallFont" align="right">
					<c:out value='${JobSetUpForm.extendedPrice}'></c:out>
				</td>
			</tr>
			<tr>
				<td class = "dbvaluesmallFont" style="padding-left: 8px;">
					<b>Job Discount:</b>
				</td>
				<td class = "dbvaluesmallFont" align="right">
					<c:out value='${JobSetUpForm.jobDiscount}'></c:out>
				</td>
			</tr>
			<tr>
				<td class = "dbvaluesmallFont" style="padding-left: 8px;">
					<b>Invoice Price:</b>
				</td>
				<td class = "dbvaluesmallFont" align="right">
					<c:out value='${JobSetUpForm.invoicePrice}'></c:out>
				</td>
			</tr>
			<tr>
				<td class = "dbvaluesmallFont" style="padding-left: 8px;">
					<b>Job Reserve:</b>
				</td>
				<td class = "dbvaluesmallFont" align="right">
					<c:out value='${JobSetUpForm.jobReserver}'></c:out>
				</td>
			</tr>
			
		</table> 
	</td>
	<td class="Ntextoleftalignnowrappaleyellowborder2">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class = "dbvaluesmallFont">
					<b>Pro Forma VGPM:</b>
				</td>
				<td class = "dbvaluesmallFont" align="right">
					<c:out value='${JobSetUpForm.proFormaVGPM}'></c:out>
				</td>
			</tr>
			<tr>
				<td class = "dbvaluesmallFont">
					<b>Actual VGPM:</b>
				</td>
				<td class = "dbvaluesmallFont" align="right">
					<c:out value='${JobSetUpForm.actualVGPM}'></c:out>
				</td>
			</tr>
			<tr>
				<td class = "dbvaluesmallFont">
					<b>Appendix Actual VGPM:</b>&nbsp;&nbsp;&nbsp;
				</td>
				<td class = "dbvaluesmallFont" align="right">
					<c:out value='${JobSetUpForm.appendixActualVGPM}'></c:out>
				</td>
			</tr>
			
		</table> 
	</td>
</tr>
</table>
