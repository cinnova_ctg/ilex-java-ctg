<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<LINK href="styles/content.css" rel="stylesheet" type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%
DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
String myDate = df.format(new Date()); 
%>

<script>
    $(document).ready(function() {
        $('#onsiteDate').datepicker();
        $('#offsiteDate').datepicker();
    });
    
function saveActions(){
	//alert("1");
	if(document.forms[0].onsiteDate.value =="") 
			//&& document.forms[0].offsiteDate.value !="")
	{
		document.forms[0].onsiteDate.focus();
		alert("Please Select Onsite Date");
		return false;
	}
	if(document.forms[0].onsiteDate.value !="" && document.forms[0].offsiteDate.value !="" )
		{
			if (!compDate_mdy(document.forms[0].onsiteDate.value, document.forms[0].offsiteDate.value)) 
			{
				document.forms[0].offsiteDate.focus();
				alert("Offsite Date should be greater than Onsite Date");
				return false;
			} 
	}
	
	//var isSendEmail =true;
	
	var url;
	url = "JobExecuteAction.do?hmode=unspecified&type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=1&saveJobActions=1&countDispatch=<c:out value='${JobSetUpForm.countDispatch}'/>";							
	document.forms[0].action=url;
	document.forms[0].submit();
}
</script>
<table width="100%" border="0"  cellspacing="0" cellpadding="0">
	<tr>
		<td  class = "dbvaluesmallFontBold" style="padding-left: 0px;padding-top: 2px;padding-bottom: 5px;" nowrap>Mark On/Off Site -&nbsp;
			<c:if test="${requestScope.isTicket ne 'Ticket'}">
			Site Visit
			</c:if>
			<c:if test="${requestScope.isTicket eq 'Ticket'}">
			Dispatch
			</c:if>
			<c:out value='${JobSetUpForm.countDispatch}'/>
		</td>
	</tr>
	<tr width="100%">
		<td width="100%" style="padding-left: 0px;">
			<table  width="48%" class="Ntextoleftalignnowrappaleyellowborder2">
					<tr >
						<td class="dbvaluesmallFont"><b>Onsite Date:&nbsp;</b>
					<%-- 	 <c:if test="${JobSetUpForm.onsiteDate eq  ' ' } " >
						<input type="text" class="textbox" size="10" name="onsiteDate"
								value="<c:out value='<%=myDate%>'/>" readonly="true" />
					    </c:if> 
						<c:if test="${JobSetUpForm.onsiteDate ne   ' ' } " >
						<input type="text" class="textbox" size="10" name="onsiteDate"
								value="<c:out value='${JobSetUpForm.onsiteDate}'/>" readonly="true" />
						
						</c:if>
						<input type="text" class="textbox" size="10" name="onsiteDate"
								value="<c:out value='<%=myDate%>'/>" readonly="true" /> --%>
							<input id="onsiteDate" type="text" class="textbox" size="10" name="onsiteDate" value="<c:out value='${JobSetUpForm.onsiteDate}'/>" readonly="true" />
							<select name="onsiteHour" class="comboWhite">
								<c:forEach items="${requestScope.hourList}" var="hourList" varStatus="innerIndex">
									<option value="<c:out value='${hourList.value}'/>"  <c:if test="${JobSetUpForm.onsiteHour eq hourList.label}">selected="selected"</c:if> > <c:out value='${hourList.label}' /></option>
								</c:forEach>
							</select>
							<select name="onsiteMinute" class="comboWhite">
								<c:forEach items="${requestScope.minuteList}" var="minuteList" varStatus="innerIndex">
									<option value="<c:out value='${minuteList.value}'/>"  <c:if test="${JobSetUpForm.onsiteMinute eq minuteList.label}">selected="selected"</c:if> > <c:out value='${minuteList.label}' /></option>
								</c:forEach>
							</select>
							<select name="onsiteIsAm" class="comboWhite">
								<option value="AM" <c:if test="${JobSetUpForm.onsiteIsAm eq '1'}">selected="selected"</c:if>>AM</option>
								<option value="PM" <c:if test="${JobSetUpForm.onsiteIsAm eq '0'}">selected="selected"</c:if>>PM</option>
							</select>						
						</td>
						<td>
						</td>
					</tr>							
					<tr>
						<td class="dbvaluesmallFont"><b>Offsite Date:</b>
							<input id="offsiteDate" type="text" class="textbox" size="10" name="offsiteDate" value="<c:out value='${JobSetUpForm.offsiteDate}'/>" readonly="true" />
							<select name="offsiteHour" class="comboWhite">
								<c:forEach items="${requestScope.hourList}" var="hourList" varStatus="innerIndex">
									<option value="<c:out value='${hourList.value}'/>"  <c:if test="${JobSetUpForm.offsiteHour eq hourList.label}">selected="selected"</c:if> > <c:out value='${hourList.label}' /></option>
								</c:forEach>
							</select>
							<select name="offsiteMinute" class="comboWhite">
								<c:forEach items="${requestScope.minuteList}" var="minuteList" varStatus="innerIndex">
									<option value="<c:out value='${minuteList.value}'/>"  <c:if test="${JobSetUpForm.offsiteMinute eq minuteList.label}">selected="selected"</c:if> > <c:out value='${minuteList.label}' /></option>
								</c:forEach>
							</select>
							<select name="offsiteIsAm" class="comboWhite">
								<option value="AM" <c:if test="${JobSetUpForm.offsiteIsAm eq '1'}">selected="selected"</c:if>>AM</option>
								<option value="PM" <c:if test="${JobSetUpForm.offsiteIsAm eq '0'}">selected="selected"</c:if>>PM</option>
							</select>&nbsp;&nbsp;&nbsp;&nbsp;  
						</td>
						<td align="left">	       					
							<input type="button" height="15" width="40"  name="save" value="Save" class="button_c1" onclick="saveActions();"
							<c:if test="${empty JobSetUpForm.scheduleid or JobSetUpForm.jobStatus eq 'Closed'}">disabled="true"</c:if> />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
			</table>
		</td>
	</tr>
</table>