<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.mind.common.*" %>
  
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<script language = "JavaScript" src = "javascript/UtilityFunction.js"></script>
<script type='text/javascript' src='./dwr/interface/JobTicketAction.js'></script>
<script type='text/javascript' src='./dwr/engine.js'></script>
<script type='text/javascript' src='./dwr/util.js'></script>
        
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script>
    $(document).ready(function() {
        var api_url = '<%= (String) request.getAttribute("api_url") %>';
        var api_user = '<%= (String) request.getAttribute("api_user") %>';
        var api_pass = '<%= (String) request.getAttribute("api_pass") %>';
        
        var appendix_id = '<c:out value='${JobSetUpForm.appendixId}'/>';
        var job_id = '<c:out value='${JobSetUpForm.jobId}'/>';
        if (job_id === '') {
            job_id = -1;
        }
        
        var ticket_type = '<c:out value='${JobSetUpForm.ticketType}'/>';
        
        var requestor = '<c:out value='${JobSetUpForm.requestor}'/>';
        
        var emailRecipients = {
            "Requestor": {},
            "Appendix Contact": {}
        };
        
        $.ajax({
            type: "GET",
            url: api_url + "ilex/poc/appendix/" + appendix_id + "/requestor/list",
            dataType: "json",
            headers: {
                "Authorization": "Basic " + btoa(api_user + ":" + api_pass)
            },
            success: function(res) {
                if (res.status === 'success') {
                    var output = '<option value="-1">-- Select --</option>';
                    $.each(res.data, function(i, field){
                        output += '<option value="' + field.id + '~' + field.email + '~' + field.name + '"';
                        
                        if (field.name === requestor) {
                            output+= ' selected';
                        }
                        
                        output += '>' + field.name + '</option>';
                    });
                    $('#requestor').html(output);
                } else {
                }
            },
            failure: function(errMsg) {
            }
        });
        
        $.ajax({
            type: "GET",
            url: api_url + "ilex/appendix/contact/" + appendix_id + "/list",
            dataType: "json",
            headers: {
                "Authorization": "Basic " + btoa(api_user + ":" + api_pass)
            },
            success: function(res) {
                if (res.status === 'success') {
                    var output = '<option value="-1">-- Select --</option>';
                    $.each(res.data, function(i, field){
                        output += '<option value="' + field.id + '~' + field.email + '~' + field.name + '">' + field.name + ': ' + field.email + '</option>';
                    });
                    $('#requestrec').html(output);
                } else {
                }
            },
            failure: function(errMsg) {
            }
        });
        
        if (job_id !== -1) {
            $.ajax({
                type: "GET",
                url: api_url + "ilex/job/"+job_id+"/recipient/list/",
                dataType: "json",
                headers: {
                    "Authorization": "Basic " + btoa(api_user + ":" + api_pass)
                },
                success: function(res) {
                    if (res.status === 'success') {
                        $.each(res.data, function(i, field){
                            if (emailRecipients.hasOwnProperty(field.type)) {
                                emailRecipients[field.type][field.id] = {
                                    id: field.id,
                                    email: field.email,
                                    name: field.name,
                                    type: field.type
                                };
                            }
                        });
                        
                        renderEmailRecipients();
                    } else {
                    }
                },
                failure: function(errMsg) {
                }
            });
        }
        
        $('#requestor').change(function() {
            parseAddEmail($(this).val(), 'Requestor');
        });
        
        $('#requestrec').change(function() {
            parseAddEmail($(this).val(), 'Appendix Contact');
        });
        
        function parseAddEmail(val, type) {
            var vals = val.split("~");
            
            if (vals.length == 3) {
                var id = vals[0];
                var email = vals[1];
                var name = vals[2];
        
                addEmail(email, id, name, type);
            }
        }
        
        function addEmail(email, id, name, type) {
            if (validateEmail(email)) {
                if (typeof emailRecipients[type] !== 'undefined') {
                    if (emailRecipients[type].hasOwnProperty(id) === false) {
                        emailRecipients[type][id] = {
                            id: id,
                            email: email,
                            name: name,
                            type: type
                        };

                        renderEmailRecipients();
                    }
                }
            }
        }
        
        function renderEmailRecipients() {
            var output = '<div style="padding-top: 1px;">None</div>';
            var hasOutput = false;
            
            for (var type in emailRecipients) {
                var csl = '';
                var list = emailRecipients[type];
                for (var id in list) {
                    if (!hasOutput) {
                        output = '';
                        hasOutput = true;
                    }
                    
                    var recipient = list[id];
                    if (csl !== '') {
                        csl += ',';
                    }
                    
                    csl += recipient.id;
                    
                    output += '<div>' + recipient.name + ': ' + recipient.email;
                    
                    if (ticket_type === 'newTicket') {
                        output += '&nbsp;&nbsp;'
                            + '<input class="button_c1 delete_recipient" type="button" data-id="' + id + '" data-type="' + type + '" value="Delete" <c:if test="${JobSetUpForm.isPOAssigned eq 'Y'}">disabled ="disabled"</c:if>>';
                    }
                    
                    output += '</div>';
                }
                    
                if (type === 'Requestor') {
                    $('#requestorIds').val(csl);
                } else {
                    $('#additionalRecipientIds').val(csl);
                }
            }
            
            $('#email_recipients').html(output);
        }
        
        function validateEmail(email) { 
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        
        $(document).on('click', '.delete_recipient', function() {
            var id = $(this).data('id');
            var type = $(this).data('type');
            
            if (typeof id === 'undefined' || typeof type === 'undefined') {
                return;
            }
            
            deleteRecipient(id, type);
        });
        
        function deleteRecipient(id, type) {
            delete emailRecipients[type][id];
            
            renderEmailRecipients();
        }
        
        $('#email_recipients').html('Loading...');
        $('#requestor').html('<option value="-1">Loading...</option>');
        $('#requestrec').html('<option value="-1">Loading...</option>');
        
        
        console.log($.ui.version);
        $('#requestedDate').datepicker();
        $("#preferredDate").datepicker();
        $("#windowFromDate").datepicker();
        $("#windowToDate").datepicker();
    });
    
	function enableValues(){
		if (document.forms[0].requestor.disabled == true)
				document.forms[0].requestor.disabled = false;
		if (document.forms[0].requestType.disabled == true)
				document.forms[0].requestType.disabled = false;
		if (document.forms[0].criticality.disabled == true)
				document.forms[0].criticality.disabled = false;
		if (document.forms[0].pps[0].disabled == true)
				document.forms[0].pps[0].disabled = false;
		if (document.forms[0].pps[1].disabled == true)
				document.forms[0].pps[1].disabled = false;
		if (document.forms[0].resource.disabled == true)
				document.forms[0].resource.disabled = false;										
	}
</script>

<script>
	function setRequestorEmail(caller)
		{
		var name;
		var requestorid=document.forms[0].requestorEmail.value;
	//	alert(requestorid.slice(-1));
		if(requestorid !='' && requestorid.slice(-1)!=",")
			requestorid=requestorid+",";
		
		if(caller =='requestor'){
		 name = document.forms[0].requestor.value;
			if(name!=0){
				document.forms[0].requestorEmail.value = requestorid + name.substring(name.indexOf("~", name)+1, name.length+1)+",";
			}else if(name==0){
				//document.forms[0].requestorEmail.value ="";
			}
		}else if(caller == 'email')
			{
			
			document.forms[0].requestorEmail.value =requestorid + document.forms[0].requestrec.value+",";
			}
		
		}
		
</script>










<script>
function setResourceLeve(radioObj){
	/*
	* When Help Desk get Selected
		Then if Criticality is no selected value then select 1-Emergency option.
			and Systems Engineer will selected into Resource combobox.
			and Resource combobox become disabled
				
	*/
	if(radioObj.value == 'Y'){ // - When Help Desk is Yes
		if(document.forms[0].resource.length == 0){ // When Resouce comboBox has no value
			var value = getSelectedOption(document.forms[0].criticality,'1-Emergency');
			document.forms[0].criticality.value = value;
			//fillResourceCombo(); // - Fill Resource comboBox.
			papulateDropDown('<c:out value='${JobSetUpForm.appendixId}'/>',document.forms[0].criticality);
		}
		if(document.forms[0].resource.length > 0){ // - When Resource comboBox has values.
			var value = getSelectedOption(document.forms[0].resource, 'Systems Engineer');		
				document.forms[0].resource.value = value;
				document.forms[0].resource.disabled = true; 
		}
	}
	<c:if test="${empty JobSetUpForm.jobId or JobSetUpForm.jobId eq '' or JobSetUpForm.jobId eq '0' or JobSetUpForm.updateCompleteTicket eq '1'}">
	else{ // - When Help Desk is No
		document.forms[0].resource.disabled = false;
	}
	</c:if>
} // - End of setResourceLeve() function

</script>

<script>
	function opensearchsitewindow()
	{
		//alert(1);
		url = "SiteSearch.do?ref=search&jobid=<c:out value='${JobSetUpForm.jobId}'/>&reftype=Update&appendixid=<c:out value='${JobSetUpForm.appendixId}'/>&siteid=<c:out value='${JobSetUpForm.site_id}'/>&isTicket=y&fromType=ticketDetail";
		//alert(url);
		window.open( url , '' , 'left = 200 , top = 200 , width = 870 , height = 400 , resizable = yes , scrollbars = yes' ); 
		
		return true;
	}
</script>


<script>
	function onTicketSave(tabId)
	{
		if(validate()){
			enableValues();
			document.forms[0].action= "JobTicketAction.do?hmode=ticketSave&type="+tabId+"&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&webTicketType=<c:out value='${JobSetUpForm.webTicket}'/>";
			document.forms[0].submit();
		}
	}
</script>
<script type="text/javascript">
	function validate(){
		if( document.forms[0].requestor.value == "0" ){
	 		alert("<bean:message bundle="PRM" key="prm.netmedx.requestornamevalidation"/>");
	 		document.forms[0].requestor.focus();
			return false;
	 	}
	 	
	 	if( document.forms[0].requestType.value == '0' ){
 			alert("Please select request type.");
 			document.forms[0].requestType.focus();
			return false;
 		}
 		if( document.forms[0].criticality.value == '0' ){
 			alert("<bean:message bundle="PRM" key="prm.netmedx.criticalityvalidation"/>");
 			document.forms[0].criticality.focus();
			return false;
 		}
	 	if( document.forms[0].resource.value == '0' ){
 			alert("<bean:message bundle="PRM" key="prm.netmedx.resourcelevelvalidation"/>");
 			document.forms[0].resource.focus();
			return false;
 		}
 		if (document.forms[0].customerReference.value == '' ){
 			alert("<bean:message bundle="PRM" key="prm.netmedx.customerreference.validation"/>");
 			document.forms[0].customerReference.focus();
			return false;
 		}
 		
 		if (document.forms[0].site.value == '' ){
 			alert("<bean:message bundle="PRM" key="prm.netmedx.site.validation"/>");
 			document.forms[0].site.focus();
			return false;
 		}
 		
		if( document.forms[0].requestedDate.value == ''){
  			alert("<bean:message bundle="PRM" key="prm.netmedx.requestdatevalidation"/>");
  			document.forms[0].requestedDate.focus();
  			return false;
  		}
  		if( document.forms[0].preferredDate.value == '' && (document.forms[0].windowFromDate.value == '' || document.forms[0].windowToDate.value == '')){
  			if(document.forms[0].preferredDate.value == '' && document.forms[0].windowFromDate.value=='' && document.forms[0].windowToDate.value == ''){
				alert("<bean:message bundle="PRM" key="prm.netmedx.preferreddatevalidation"/>");
				document.forms[0].preferredDate.focus();
			}else if(document.forms[0].windowFromDate.value == ''){
				alert("<bean:message bundle="PRM" key="prm.netmedx.preferredwindowsrartdatevalidation"/>");
				document.forms[0].windowFromDate.focus();
			}else if(document.forms[0].windowToDate.value == ''){
				alert("<bean:message bundle="PRM" key="prm.netmedx.preferredwindowenddatevalidation"/>");
				document.forms[0].windowToDate.focus();
			}
			return false;
		}
  			
 		if( document.forms[0].problemCategory.value == '0' ){
 			alert("<bean:message bundle="PRM" key="prm.netmedx.problemcategory"/>");
 			document.forms[0].problemCategory.focus();
			return false;
 		}	
 		
 		if( document.forms[0].problemDescription.value == '' ){
 			alert("<bean:message bundle="PRM" key="prm.netmedx.problemdescvalidation"/>");
 			document.forms[0].problemDescription.focus();
			return false;
 		}
                
	 	if(document.forms[0].requestor.value == '-1'){
                    alert("Please select a requestor.");
                    document.forms[0].requestor.focus();
                    return false;
 		}
                
	 	return true;
	}
</script>
<script>
	/*function opensearchsitewindow()
	{
		var appendixid = '<c:out value="${JobSetUpForm.appendixId}"/>';
		
		str = "SiteSearch.do?siteid=<bean:write name = "JobSetUpForm" property = "site_id" />&ref=search&jobid=<bean:write name = "JobSetUpForm" property = "jobId" />&appendixid="+appendixid+"&fromType=ticketDetail&through=ticket";
		
		suppstrwin = window.open( str , '' , 'left = 200 , top = 200 , width = 850 , height = 450 , resizable = yes , scrollbars = yes' ); 
		suppstrwin.focus();
		return true;
	}*/
	

	function onPreferrredHourSave()
	{	
		
		var Hour = document.forms[0].preferredHour.value;
		if(Hour=='00'){
			
			document.forms[0].preferredHour.value = "12";
		}
	}

	
</script>
<script>
	function openaddcontactwindow()
	{
	//	alert(1);
		url = "AddAppendixContact.do?reftype=&appendixid=<c:out value='${JobSetUpForm.appendixId}'/>&isTicket=y&fromType=ticketDetail";
	//	alert(url);
		window.open( url , 'myWindow' , 'left = 200 , top = 200 , width = 870 , height = 400 , resizable = yes , scrollbars = yes' ); 
	//	myWindow.close();
		return true;
	}
</script>
<c:choose>
    <c:when test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">
		<script>
			var hasJobId = true;
		</script>
    </c:when>
    <c:otherwise>
		<script>
			var hasJobId = false;
		</script>
    </c:otherwise>
</c:choose>
</head>
<body onload="onPreferrredHourSave();">
<c:if test ="${not empty requestScope.refershtree && requestScope.refershtree eq 'refershtree'}">
	<script>
			parent.ilexleft.location.reload();
	</script>
</c:if>
	<table width="100%"  >
		<tr>
			<td class="dbvalue" width="60%"><h2 style="font-size: 11px;padding-left: 0px;">Request</h2>
			</td>
			<td width="2%">&nbsp;</td>
			<td class="dbvalue" width="40%"><h2 style="font-size: 11px;padding-left: 0px;">Ticket Schedule</h2>
			</td>
		</tr>
		<input type="hidden" name="site_id" value="<c:out value='${JobSetUpForm.site_id}'/>" />
		<input type="hidden" name="sitelistid" value="<c:out value='${JobSetUpForm.sitelistid}'/>" />
		<input type="hidden" name="isPOAssigned" value="<c:out value='${JobSetUpForm.isPOAssigned}'/>" />
                
		<input type="hidden" id="requestorIds" name="requestorIds" value="<c:out value='${JobSetUpForm.requestorIds}'/>" />
		<input type="hidden" id="additionalRecipientIds" name="additionalRecipientIds" value="<c:out value='${JobSetUpForm.additionalRecipientIds}'/>" />

		<tr>
			<td width="60%">
				<table  class="Ntextoleftalignnowrappaleyellowborder3"  width="100%" height="200">
					<tr>
                                            <td>
                                                <table width="80%" class="Ntextoleftalignnowrappaleyellowborder2" >
                                                    <tr>
                                                        <td width="10%">Requestor<font color="red">*</font>:</td>
                                                        <td>
                                                            <select id="requestor" name="requestor" class="select" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled" </c:if>></select>
                                                        </td>
                                                        <td colspan="1" width="22%" nowrap="nowrap">Additional Recipient:</td>
                                                        <td colspan="2">
                                                            <select style="width: 180px;" id="requestrec" name="requestrec" class="select" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled" </c:if>></select>
                                                        </td>	
                                                        <td><a target="_blank" class="button_c1" href="/content/ilex/#/appendix/<c:out value='${JobSetUpForm.appendixId}'/>/contacts">Manage</a></td>
                                                    </tr>
                                                </table>
                                            </td>
					</tr>
					<tr>
                                            <td style="padding-left:6px;">
                                                <table width="80%" class="Ntextoleftalignnowrappaleyellowborder2" >
                                                    <tr>
                                                        <td width="10%" style="padding: 5px 0px; vertical-align: top;">Recipients:</td>
                                                        <td>
                                                            <div id="email_recipients"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
					</tr>
					<tr>
						<td>
							<table width="80%" class="Ntextoleftalignnowrappaleyellowborder2">
								<tr>
									<td colspan="1" width="22%" nowrap="nowrap">Request Type<font color="red">*</font>:
									</td>
								<%--	<td colspan="2"><select name="requestType" class="select" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0'}">disabled="disabled"</c:if>> --%>
								<td colspan="2"><select id="requestemail" name="requestType" class="select" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled"</c:if>>
													<c:forEach items="${JobSetUpForm.requestTypeList}" var="requestor" varStatus="index">
														<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.requestType eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
													</c:forEach>
													</select>
									</td>
									
									<%-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="text" name="requestorEmail" value="<c:out value='${JobSetUpForm.requestorEmail}'/>" size="40">&nbsp;&nbsp;(email)</td> --%>
									
								<!--  add   drop down for additional email reciptant -->	
									<!-- <td colspan="1" width="22%" nowrap="nowrap">Additional Recipient:
									</td> -->
								<%--	<td colspan="2"><select name="requestType" class="select" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0'}">disabled="disabled"</c:if>> --%>
								<%-- <td colspan="2">
								<select   name="requestrec" class="select"  onchange="setRequestorEmail('email');"<c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled" </c:if>>
													<c:forEach items="${JobSetUpForm.addRecipitant}" var="requestorAdd" varStatus="index">
														<option value="<c:out value='${requestorAdd.value}'/>" <c:if test="${JobSetUpForm.addRecipitant eq requestorAdd.value}">selected="selected"</c:if>><c:out value='${requestorAdd.label}'/></option>
													</c:forEach>
													</select>
									</td>	
									<td><input class="button_c1" type="button" name="AddRec" value="Add" <c:if test="${JobSetUpForm.isPOAssigned eq 'Y'}">disabled ="disabled"</c:if> onclick="return openaddcontactwindow();"></td> --%>
										
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<table  width="80%" >
								<tr>
									<TD>
										<TABLE class="Ntextoleftalignnowrappaleyellowborder2" >
											<TR>
												<td  width="10%" nowrap="nowrap" >Stand By:
													<input type="radio"  name="standBy" value="Y" <c:if test="${JobSetUpForm.standBy eq 'Y'}">checked="checked"</c:if>/>Yes
													<input type="radio"  name="standBy" value="N" <c:if test="${JobSetUpForm.standBy eq 'N'}">checked="checked"</c:if>/>No
												</td>
											</TR>
										</TABLE>
									</TD>
									<td>&nbsp;</td>
									<TD>
										<TABLE class="Ntextoleftalignnowrappaleyellowborder2">
											<TR>
												<td width="5%" nowrap="nowrap">MSP:
													<input type="radio"  name="msp" value="Y" <c:if test="${JobSetUpForm.msp eq 'Y'}">checked="checked"</c:if>/>Yes
													<input type="radio" name="msp" value="N" <c:if test="${JobSetUpForm.msp eq 'N'}">checked="checked"</c:if>/>No
												</td>
											</TR>
										</TABLE>
									</TD>
									<td>&nbsp;</td>
									<TD>
										<TABLE class="Ntextoleftalignnowrappaleyellowborder2">
											<TR>
												<td width="10%" nowrap="nowrap">Help Desk:
													<input type="radio" name="helpDesk" value="Y" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled"</c:if> onclick="setHelpDeskCriticality(this);" <c:if test="${JobSetUpForm.helpDesk eq 'Y'}">checked="checked"</c:if>/>Yes
													<input type="radio" name="helpDesk" value="N" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled"</c:if> onclick="setHelpDeskCriticality(this);"<c:if test="${JobSetUpForm.helpDesk eq 'N'}">checked="checked"</c:if>/>No
												</td>
											</TR>
										</TABLE>
									</TD>
								</tr>
								
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3" >
							<table class="Ntextoleftalignnowrappaleyellowborder2" width="100%">
								<tr>
									<td nowrap="nowrap">Criticality<font color="red">*</font>: </td>
									<td nowrap="nowrap">Period of Performance:</td>
									<td nowrap="nowrap">Resource<font color="red">*</font>:</td>
								</tr>
								<tr>
									<td><select name="criticality" class="select" onchange="papulateDropDown('<c:out value='${JobSetUpForm.appendixId}'/>',this);" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled"</c:if>>
											<c:forEach items="${JobSetUpForm.criticalityList}" var="requestor" varStatus="index" >
												<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.criticality eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
											</c:forEach>
										</select>
									</td>
									<td><input type="radio" name="pps" value="Y" <c:if test="${JobSetUpForm.pps eq 'Y'}">checked="checked"</c:if>  <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled"</c:if>>PPS
										<input type="radio" name="pps" value="N" <c:if test="${JobSetUpForm.pps eq 'N'}">checked="checked"</c:if>  <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled"</c:if>>Non-PPS
										<input type="hidden" name="periodOfPerformance">
									</td>
									<td>
										<span id="resourceDropDown">
											<jsp:include page="/PRM/Job/DropDown.jsp">
												<jsp:param name = 'fromJsp' value = 'Ticket.jsp'/>
											</jsp:include>
											<script>
												toggleCombo();
											</script>
										</span>
									<%--
									<select id="resource" name="resource" class="select" <c:if test="${not empty JobSetUpForm.jobId && JobSetUpForm.jobId ne '' && JobSetUpForm.jobId ne '0' && JobSetUpForm.updateCompleteTicket eq '0'}">disabled="disabled"</c:if> >
									
											<c:forEach items="${JobSetUpForm.resourceList}" var="requestor" varStatus="index">
												<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.resource eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
											</c:forEach>
										</select>
									--%>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<table class="Ntextoleftalignnowrappaleyellowborder2" width="80%">
								<tr>
									<td nowrap>Customer Reference<font color="red">*</font>: 		
									</td>
									<td><input type="text" class="text"  name="customerReference" value="<c:out value='${JobSetUpForm.customerReference}'/>"></td>
									<td>&nbsp;</td>
									<td>Site<font color="red">*</font>:</td><td><input type="text" class="text" name="site" value="<c:out value='${JobSetUpForm.site}'/>" readonly></td>
									<td><input class="button_c1" type="button" name="findSite" value="Find" <c:if test="${JobSetUpForm.isPOAssigned eq 'Y'}">disabled ="disabled"</c:if> onclick="opensearchsitewindow();"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="2%">&nbsp;</td>
			<td width="60%" >
				<table  class="Ntextoleftalignnowrappaleyellowborder3" width="100%">
					<tr>
						<td colspan="2">
							<table class="Ntextoleftalignnowrappaleyellowborder2" width="100%" border="0">
								<tr>
									<td width="10%">Submitted<font color="red">*</font>: </td>
									<td nowrap="nowrap" width="10%">
                                                                            <input type="text"  class="textbox" size="10" id="requestedDate" name="requestedDate" readonly="true"  value="<c:out value='${JobSetUpForm.requestedDate}'/>"/>
				   					</td>
				   					<td nowrap="nowrap" >
				   						<select name="requestedHour" class="select">
				   						<c:forEach items="${JobSetUpForm.requestedHourList}" var="requestor" varStatus="index" >
				   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.requestedHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
				   						</c:forEach>
				   						</select>
				   						<select name="requestedMinute" class="select">
				   						<c:forEach items="${JobSetUpForm.requestedMinuteList}" var="requestor" varStatus="index" >
				   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.requestedMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
				   						</c:forEach>
				   						</select>
				   					</td>
				   					<td nowrap="nowrap">
				   						<input type="radio" name="requestedIsAm" value="AM" <c:if test="${JobSetUpForm.requestedIsAm eq 'AM'}">checked="checked"</c:if> >AM
				   						<input type="radio" name="requestedIsAm" value="PM" <c:if test="${JobSetUpForm.requestedIsAm eq 'PM'}">checked="checked"</c:if>>PM
				   					</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp;Preferred<font color="red">*</font>: </td>
									<td nowrap="nowrap" width="10%">
                                                                            <input type="text" class="textbox" size="10" id="preferredDate" name="preferredDate" readonly="true" value="<c:out value='${JobSetUpForm.preferredDate}'/>"/>
				   					</td>
				   					<td nowrap="nowrap">
				   						<select name="preferredHour" class="select">
				   						<c:forEach items="${JobSetUpForm.preferredHourList}" var="requestor" varStatus="index" >
				   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.preferredHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
				   						</c:forEach>
				   						</select>
				   						<select name="preferredMinute" class="select">\
				   						<c:forEach items="${JobSetUpForm.preferredMinuteList}" var="requestor" varStatus="index" >
				   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.preferredMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
				   						</c:forEach>
				   						</select>
				   					</td>
				   					<td nowrap="nowrap">
				   						<input type="radio" name="preferredIsAm" value="AM" <c:if test="${JobSetUpForm.preferredIsAm eq 'AM'}">checked="checked"</c:if>>AM 
				   						<input type="radio" name="preferredIsAm" value="PM" <c:if test="${JobSetUpForm.preferredIsAm eq 'PM'}">checked="checked"</c:if>>PM
									</td>
								</tr>
								<tr>
									<td colspan="2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;or</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;Window: </td>
									<td nowrap="nowrap" width="10%">
                                                                            <input type="text"  class="textbox" size="10" id="windowFromDate" name="windowFromDate" readonly="true" value="<c:out value='${JobSetUpForm.windowFromDate}'/>"/>
				   					</td>
				   					<td nowrap="nowrap">
				   						<select name="windowFromHour" class="select">
				   						<c:forEach items="${JobSetUpForm.windowFromHourList}" var="requestor" varStatus="index" >
				   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.windowFromHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
				   						</c:forEach>
				   						</select>
				   						<select name="windowFromMinute" class="select">
				   						<c:forEach items="${JobSetUpForm.windowFromMinuteList}" var="requestor" varStatus="index" >
				   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.windowFromMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
				   						</c:forEach>
				   						</select>
				   					</td>
				   					<td nowrap="nowrap" >
				   						<input type="radio" name="windowFromIsAm" value="AM" <c:if test="${JobSetUpForm.windowFromIsAm eq 'AM'}">checked="checked"</c:if>>AM 
				   						<input type="radio" name="windowFromIsAm" value="PM" <c:if test="${JobSetUpForm.windowFromIsAm eq 'PM'}">checked="checked"</c:if>>PM
									</td>
								</tr>
								<tr>
									<td></td>
									<td nowrap="nowrap" width="10%">
                                                                            <input type="text"  class="textbox" size="10" id="windowToDate" name="windowToDate" readonly="true" value="<c:out value='${JobSetUpForm.windowToDate}'/>"/>
				   					</td>
				   					<td nowrap="nowrap">
				   						<select name="windowToHour" class="select">
				   						<c:forEach items="${JobSetUpForm.windowToHourList}" var="requestor" varStatus="index" >
				   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.windowToHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
				   						</c:forEach>
				   						</select>
				   						<select name="windowToMinute" class="select">
				   						<c:forEach items="${JobSetUpForm.windowToMinuteList}" var="requestor" varStatus="index" >
				   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.windowToMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
				   						</c:forEach>
				   						</select>
				   					</td>
				   					<td nowrap="nowrap">
				   						<input type="radio" name="windowToIsAm" value="AM" <c:if test="${JobSetUpForm.windowToIsAm eq 'AM'}">checked="checked"</c:if>>AM 
				   						<input type="radio" name="windowToIsAm" value="PM" <c:if test="${JobSetUpForm.windowToIsAm eq 'PM'}">checked="checked"</c:if>>PM
									</td>
								</tr>
								<tr>
									<td colspan="3" style="padding-left: 0px;">
										<table >
											<tr>
												<td class = "Ntextoleftalignnowrappaleyellowborder2" style="padding-left: 73px;">(All times local site times)
												</td>
											</tr>
											<tr>
												<td class = "Ntextoleftalignnowrappaleyellowborder2" style="padding-left: 0px;">Suggested Criticality/PPS: 
												</td>
											</tr>
											<tr><td class = "Ntextoleftalignnowrappaleyellowborder2" style="padding-left: -1px;">Received:
												<c:if test='${not empty JobSetUpForm.receivedDate}'><c:out value='${JobSetUpForm.receivedDate}'/> 
												<c:if test='${not empty JobSetUpForm.receivedHour}'><c:out value='${JobSetUpForm.receivedHour}'/>:<c:out value='${JobSetUpForm.receivedMinute}'/> <c:out value='${JobSetUpForm.receivedIsAm}'/></c:if></c:if>
												<c:if test='${empty JobSetUpForm.receivedDate}'>No value</c:if>
												</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="dbvalue"><h2 style="font-size: 11px;padding-left: 0px;">Nature Of Request</h2>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table class="Ntextoleftalignnowrappaleyellowborder3" width="100%">
					<tr>
						<td>
							<table class="Ntextoleftalignnowrappaleyellowborder2" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>Problem Category<font color="red">*</font>:
									</td>
									<td><select name="problemCategory" class="select">
											<c:forEach items="${JobSetUpForm.problemCategoriesList}" var="requestor" varStatus="index">
												<option value="<c:out value='${requestor.value}'/>" <c:if test="${JobSetUpForm.problemCategory eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
											</c:forEach>
										</select></td>
								</tr>
								<tr>
									<td colspan="2">Problem Description<font color="red">*</font>:
									</td>
								</tr>
								<tr>
									<td colspan="2"><textarea name="problemDescription" class="textarea" cols="90" rows="9"><c:out value='${JobSetUpForm.problemDescription}'/></textarea>
									</td>
								</tr>
								<tr>
									<td colspan="2">Supporting File Uploads: <c:if test="${JobSetUpForm.ticketType eq 'existingTicket'}"><a href="EntityHistory.do?entityType=Deliverables&jobId=<c:out value='${JobSetUpForm.jobId}'/>&linkLibName=Job&function=supplementHistory&ticket_type=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id }'/>"></c:if>View<c:if test="${JobSetUpForm.ticketType eq 'existingTicket'}"></a></c:if>
									</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table class="Ntextoleftalignnowrappaleyellowborder2" > 
								<tr>
									<td>Special Instructions:
									</td>
								</tr>
								<tr>
									<td><textarea name="specialInstructions" class="textarea" cols="65" rows="5" ><c:out value='${JobSetUpForm.specialInstructions}'/></textarea>
									</td>
								</tr>
								<tr>
									<td>Ticket Rules:
									</td>
								</tr>
								<tr>
									<td><textarea name="ticketRules" readonly="readonly" class="textarea" cols="65" rows="4"><c:out value='${JobSetUpForm.ticketRules}'/></textarea>
									</td>
								</tr>
							</table>
						</td>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2"><input type="button" class="button_c" name="Save" value="Save" <c:if test="${JobSetUpForm.isPOAssigned eq 'Y'}">disabled ="disabled"</c:if> onclick="onTicketSave('2');">&nbsp;&nbsp;
							<input type=reset class="button_c" name="Cancel" value="Cancel">
			</td>
		</tr>
	</table>
</body>

</html>