<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td class="" colspan="2"><h3>Financial BreakDown</h3></td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" width="100%">
			<div style="border : 0px solid #323e7a; padding : 5px 5px 5px 5px;">
				<img src="http://chart.apis.google.com/chart?cht=p3&amp;
				chd=t:<c:out value='${JobSetUpForm.chd}'/>&amp;
				chs=400x150&amp;
				chl=<c:out value='${JobSetUpForm.chl}'/>&amp;
				chco=<c:out value='${JobSetUpForm.chco}'/>"/>
			</div>
		</table> 
	</td>
</tr>
