<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<head>
<script>
function vailidateAll(){
	if(document.forms[0].requestType.value == '0'){
		alert('Please select a Request Type.');
		document.forms[0].requestType.focus();
		return false;
	}
	
	//documet.forms[0].sumbit();
}

function resetForm(){
	document.forms[0].requestType.value = '0';
}
</script>
</head>
<html>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
	<td id = "pop1" height="19" class = "Ntoprow1">&nbsp;</td>
</tr>
<tr>
	<td background="images/content_head_04.jpg" height="21">
       <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null" class="bgNone"><c:out value='${requestScope.msaName }'/></a>
		 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
		 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
		 <a><span class="breadCrumb1">Update Request Type</a>
	   </div>
    </td>
</tr>
</table>

<html:form action="/ChangeTicketRequestType.do">
<html:hidden property="msaId"/>
<html:hidden property="jobId"/>
<table cellpadding="0" cellspacing="0" border="0">
<c:if test="${requestScope.result eq '0'}">
<tr>
	<td style="padding-left: 10px;">&nbsp;</td>
	<td class="message" height="10">Updated successfully.</td>
</tr>
</c:if>
</table>

<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<td style="padding-left: 8px;">
		<table cellpadding="0" cellspacing="0" border="0">
		<tr style=" padding-top: 8px;">
			<td>&nbsp;</td>
			<td nowrap="nowrap">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class = "tabtexto"><b>Request Type:</b>&nbsp;&nbsp;</td>
						<td class="pvstexto">
							<html:select name="ChangeTicketRequestTypeForm" property="requestType" styleClass="select" >
								<html:optionsCollection name="ChangeTicketRequestTypeForm" property="requestTypeCombo" label="label" value="value"/>
							</html:select>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="pvstexto">
							<html:submit property="saveRequestType" styleClass="button_c" onclick="return vailidateAll();">Update</html:submit>&nbsp;&nbsp;&nbsp;
							<html:submit property="cancel" styleClass="button_c" onclick="resetForm();">Cancel</html:submit>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</html:form>
</body>
</html>
