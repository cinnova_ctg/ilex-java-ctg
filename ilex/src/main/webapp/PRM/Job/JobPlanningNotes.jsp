<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>

<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />

<%
response.setHeader("Pragma","No-Cache");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setDateHeader("Expires",0);
%>

<script>
function getAjaxRequestObjectPlanningNotes(){
	var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }	
	return ajaxRequest;        
}

<c:if test="${requestScope.tabId ne 1}">
	function addComment() {		
		var comments = escape(encodeURI(document.getElementById("comments").value));
		
		if(comments==''){
			alert("Add comments.");
			return false;
		}
		
		var ajaxRequest = getAjaxRequestObjectPlanningNotes();	
	    ajaxRequest.onreadystatechange = function(){
	           if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){ 
	           var xmlDoc=ajaxRequest.responseXML;
	           var id; 
	           id = xmlDoc.getElementsByTagName("CommentsInfo")[0];	
			   var latestCommentDate=id.getElementsByTagName("latestCommentsDate")[0].firstChild.nodeValue;					 
			   var latestComments=id.getElementsByTagName("latestCommentsNotes")[0].firstChild.nodeValue;
			   if(document.getElementById("latestCommentsDate") != null ) { 
				   document.getElementById("latestCommentsDate").innerHTML = "";
		           document.getElementById("latestComments").innerHTML = "";
		           
		           document.getElementById("latestCommentsDate").innerHTML = latestCommentDate;
		           document.getElementById("latestComments").innerHTML = latestComments;
		           
					if(document.getElementById("techTable").style.display == 'none') {
						document.getElementById("techTable").style.display = '';
					}
	           } else {
	           }
	           document.getElementById("comments").innerHTML = "";
	           }
	    }   
    
       // var isSendEmail = window.confirm("Do you want to add  comment?");
	    
		var url ="JobBuyAction.do";
		var parm = "hmode=addComments&jobId=<c:out value='${JobSetUpForm.jobId}'/>&ticketId=<c:out value='${JobSetUpForm.ticket_id}'/>&requestorEmail=<c:out value='${JobSetUpForm.requestorEmail}'/>&comments="+comments;		
		//var parm = encodeURI("hmode=addComments&jobId=<c:out value='${JobSetUpForm.jobId}'/>&comments="+comments);		
        ajaxRequest.open("POST", url, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send(parm); 
	
	
	}
</c:if>
</script>

<table border="0" cellpadding="0" cellspacing="0" align="left" style="margin-left: 0px;padding-top: 10px;" width="40" >
		<c:if test="${not empty JobSetUpForm.note || JobSetUpForm.note ne ''}">	
			<c:if test="${requestScope.tabId eq 1 || requestScope.tabId eq 3}">
				<tr>
					<td nowrap class = "dbvaluesmallFontBold" >
						Job Planning Notes/Budget Guidelines
					</td>
				</tr>
				</c:if>
			</c:if>	
		<c:if test="${not empty JobSetUpForm.note || JobSetUpForm.note ne '' }">
				<c:if test="${requestScope.tabId eq 1 || requestScope.tabId eq 3}">
					<tr valign="top" style="margin-top: 0px;">
						<td style="padding-left: 0px;">
								<textarea class="textarea" id="notes"  name="notes" cols="<c:if test="${requestScope.tabId ne 1}">92</c:if><c:if test="${requestScope.tabId eq 1}">100</c:if>" rows="6" name="note" readonly="readonly"><c:out value='${JobSetUpForm.note}'/></textarea>&nbsp;&nbsp;
						</td>
					</tr>
				</c:if>
			</c:if>
		<c:if test="${requestScope.tabId ne 1}">
				<tr valign="top" style="vertical-align: top;" >
						<td nowrap class = "dbvaluesmallFontBold" style="padding-left: 0px;padding-bottom: 0px;padding-top: 0px;">Internal Comments&nbsp;<span class="dbvalue" valign="bottom">&nbsp;(<a href="MenuFunctionViewCommentAction.do?from=jobdashboard&addendum_id=<c:out value='${JobSetUpForm.jobId}'/>&ref=view&Type=Jobdashboard&Id=<c:out value='${JobSetUpForm.jobId}'/>">View</a>)</span></td> 
				</tr>
		</c:if>
			<c:if test="${requestScope.tabId ne 1}">		
					<tr valign="top" style="margin-top: 0px;">
							
							<td style="padding-left: 0px;">
								<textarea cols="92" rows="6" id="comments" class="textarea" name="comments"></textarea>
							</td>					
					</tr>
			</c:if>
		<c:if test="${requestScope.tabId ne 1}">
		
			<tr valign="top">
				<td nowrap align="right" colspan="2"><input type="button" name="Add" value="Add" onclick="addComment();" class="button_c1"/></td>
			</tr>
			<tr valign="top"><td><table id="techTable" style ="display:none;" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td nowrap class = "dbvaluesmallFontBold" style="padding-left: 0px;padding-bottom: 0px;padding-top: 0px;" colspan = "2" valign="top">Previous Comment&nbsp;
						</td>
					</tr>
					<tr>
						<td height="2"></td>
					</tr>
					
					<tr valign="top">
						<td>
							<table width = "100%" border = 1 style="border-width:1px;border-collapse: collapse;" bordercolor="#D9DFEF"class="Ntextoleftalignnowrapblue"   cellspacing="0" cellpadding="0">
								<tr>
									<td width="25%"  valign="top" align="center" id="latestCommentsDate">
										<c:out value='${JobSetUpForm.latestCommentsDate}'/>
									</td>
									<td width="75%" valign="top" align="left" id="latestComments" style="white-space: normal;">
										<c:out value='${JobSetUpForm.latestComments}' escapeXml="false"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
			</table></td></tr>
		</c:if>
	</table>
	<script>
	if('<c:out value="${requestScope.tabId}"/>' != 1) {
		if('<c:out value="${JobSetUpForm.latestCommentsDate}"/>' == null || '<c:out value="${JobSetUpForm.latestCommentsDate}"/>' == '') {
			document.getElementById("techTable").style.display = 'none';
		} else {
			document.getElementById("techTable").style.display = '';
		}
	}
	</script>
