<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html:html>
<%-- <% 
	String setvalue = "";
	String selectedradio = "";
	int k=0;
	int size=0;
	String ref1 = "";

	if(request.getAttribute( "size" ).toString()!=null)
	{
		size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
	}

	String Appendix_Id = "";
	
	if( request.getAttribute( "Appendix_Id" ) != null )
		Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" );
	

%> --%>
<HEAD>
<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<title></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<%@ include  file="/NMenu.inc" %>
</head>

<body> 
<html:form action="AddAppendixContact.do">
<%-- <%@ include  file="/JobMenu.inc" %> --%>
<html:hidden property="ref"/>
<html:hidden property="msa_id"/>
<html:hidden property="jobid"/>
<html:hidden property="appendixid"/>
<html:hidden property = "reftype"/>
<html:hidden property = "lo_ot_id" />
<html:hidden property="siteid"/> 
<html:hidden property ="fromType"/>
<input type="hidden" name="isTicket" value="<c:out value='${param.isTicket }'/>" />
<table>
<table  border="0" cellspacing="1" cellpadding="1" >
  <tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
 
 <tr> 
  <tr> 
    <td  class = "colDark"  >Client Name:</td>
	<td  class = "colLight" ><html:text  styleClass="text" size="20" name="AddEmailContactForm" property="client_name" maxlength="50"/>
	</td>
  </tr>
  
<tr><td  class = "colDark"  >Email Address:</td>
<td  class = "colLight" ><html:text  styleClass="text" size="20" name="AddEmailContactForm" property="client_email" maxlength="50"/></td></tr>
</tr>
	<tr>
	<td  class="colDark"  >Type:</td>
	<td  class="colLight">
		<html:select   name="AddEmailContactForm" property="client_type" size="1" styleClass="select">
			 <html:optionsCollection name = "dcclienttypeList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
		</html:select>
	</td>
 
 </tr>
  
  
  <tr> 
	<td  class="colLight" colspan="8" align="center">
	
			<html:button property ="add" styleClass="button_c" onclick ="return addnew();">Add</html:button>

	</td>
  </tr>
  
  </table>
 </td>
 </tr>
</table>
  

</html:form>
</table>
</BODY>

<script>
function save(){
	
	if(document.forms[0].client_name.value == ''){
		alert(' Client Name  is required.');
		return false;
	}
	if(document.forms[0].client_email.value == ''){
		alert(' Client email  is required.');
		return false;
	}
	if(document.forms[0].client_email.value != '')
	{
		
		return emailvalidation(document.forms[0].client_email.value,document.forms[0].client_email.value);
		
	}
	return true;	
	
}
function addnew()
{
	if(save()){
		
		document.forms[0].submit();
		window.close();
		if (window.opener != null && !window.opener.closed) {
            window.opener.location.reload();
        }
		
	}
	//document.forms[0].action="SiteManage.do?method=Add&appendixid="+document.forms[0].appendixid.value+"&msaid="+document.forms[0].msa_id.value+"&jobid="+document.forms[0].jobid.value+"&fromType="+document.forms[0].fromType.value;
//document.forms[0].action="AddAppendixContact.do?appendixid="+document.forms[0].appendixid.value+"&isTicket=y&fromType=ticketDetail";
//var str = "SiteManage.do?method=Add&appendixid="+document.forms[0].appendixid.value+"&msaid="+document.forms[0].msa_id.value+"&jobid="+document.forms[0].jobid.value+"&fromType="+document.forms[0].fromType.value+"&search="+document.forms[0].search.value+"&site_name="+document.forms[0].site_name.value+"&site_number="+document.forms[0].site_number.value+"&site_city="+document.forms[0].site_city.value+"&site_brand="+document.forms[0].site_brand.value+"&site_end_customer="+document.forms[0].site_end_customer.value+"&site_state="+document.forms[0].site_state.value;
//document.forms[0].action = str;


return true;
}

function emailvalidation(emailvalue,emailinputfield )
{
	if( emailvalue == '' ){
		alert("Email should be in aa@bb.cc,xx@yy.zz format.");		
		emailinputfield.focus();
		return false;
	}else
		{
		var emailFilter2=/^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
		var finalemails = "";
		var emailValue2=emailvalue.trim();
		var emailadressess = new Array();
		if(emailValue2.indexOf(",")>-1)
			{
			emailadressess = emailValue2.split(",");
			for(var i = 0 ; i < emailadressess.length;i++ )
				{
				
				if(emailadressess[i].trim() != "")
				if (!(emailFilter2.test(emailadressess[i].trim())))	{
					
					alert("Email should be in aa@bb.cc,xx@yy.zz format.");
					emailinputfield.focus();
				return false;
				}else
				{
					if(i == emailadressess.length-1)
					finalemails += emailadressess[i].trim();
					else
						finalemails += emailadressess[i].trim()+",";
				}
				
				}
			}else if(emailValue2.indexOf(";")>-1)
			{
				emailadressess = emailValue2.split(";");
				for(var i = 0 ; i < emailadressess.length;i++ )
					{
					if(emailadressess[i].trim() != "")
					if (!(emailFilter2.test(emailadressess[i].trim())))	{
						
						alert("Email should be in aa@bb.cc,xx@yy.zz format.");
						emailinputfield.focus();
					return false;
					}else
					{
						if(i == emailadressess.length-1)
						finalemails += emailadressess[i].trim();
						else
							finalemails += emailadressess[i].trim()+",";
					}
					
					}
				
			}else if(emailValue2.indexOf(" ")>-1)
				{
				emailadressess = emailValue2.split(" ");
				for(var i = 0 ; i < emailadressess.length;i++ )
					{
					
					if(emailadressess[i] != "")
					if (!(emailFilter2.test(emailadressess[i].trim())))	{
						
						alert("Email should be in aa@bb.cc,xx@yy.zz format.");
						emailinputfield.focus();
					return false;
					}else
					{
						if(i == emailadressess.length-1)
						finalemails += emailadressess[i].trim();
						else
							finalemails += emailadressess[i].trim()+",";
					}
					
					}
				
				}else
					{
					if (!(emailFilter2.test(emailValue2)))	{
						
						alert("Email should be in aa@bb.cc,xx@yy.zz format.");
						emailinputfield.focus();
					return false;
					}else
					{
						finalemails += emailValue2.trim();
						
						
					}
					
					}
		
		emailinputfield.value = finalemails;
		}
	
	return true;
	
}







</script>

</html:html>
