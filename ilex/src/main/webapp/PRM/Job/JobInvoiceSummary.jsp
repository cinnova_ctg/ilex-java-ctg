<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3" style="padding-left: 0px;padding-bottom: 5px;" valign="top" nowrap><h2 style="font-size: 14px;padding-left: 0px;padding-top: 0px;">Invoice Summary</h2>			
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding=0 cellspacing=0 style="padding-left: 5px;"   class="Ntextoleftalignnowrappalenoborder">
				<tr>
					<td>
						<table>
						<tr>
							<td  class = "dbvaluesmallFontBold" width="60">Number: </td>
							<td  class = "dbvaluesmallFont"><c:out value="${JobSetUpForm.invNumber}"/></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>	
					<td>
						<table>
						<tr>
							<td  class = "dbvaluesmallFontBold" width="60">Date: </td>
							<td  class = "dbvaluesmallFont"><c:out value='${JobSetUpForm.invDate}'/></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table>
						<tr>	
							<td  class = "dbvaluesmallFontBold" width="60">Amount: </td>
							<td  class = "dbvaluesmallFont"><fmt:formatNumber value='${JobSetUpForm.invAmount}' type="currency" pattern="$#####0.00"/></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table>
						<tr>	
							<td  class = "dbvaluesmallFontBold" width="60">Discount: </td>
							<td  class = "dbvaluesmallFont"><c:out value='${JobSetUpForm.invDiscount}'/></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table>
						<tr>	
							<td  class = "dbvaluesmallFontBold" width="60">Age: </td>					
							<td  class = "dbvaluesmallFont"><c:out value='${JobSetUpForm.invAge}'/></td>	
						</tr>
						</table>
					</td>														
				</tr>	
				 			 
				<tr width="100%">
					<td nowrap colspan="2">&nbsp;
						<input type="button" width="40"  name="save" value="View" class="button_c1" disabled/>			
						<input type="button" width="40"  name="job_discount" value="Job Discount" class="button_c1" disabled/>			
						<input type="button" width="40"  name="approve" value="Approve" class="button_c1" disabled/>			
						<input type="button" width="40"  name="email" value="Email" class="button_c1" disabled/>								
						&nbsp;
					</td>	
				</tr>
				<tr>	
					<td  class = "texthd" colspan="2"></td>																				
				</tr>	
			</table>
		</td>
	</tr>	
</table>
</body>
</html>