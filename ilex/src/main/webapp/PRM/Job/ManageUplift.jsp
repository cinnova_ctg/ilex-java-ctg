<!DOCTYPE HTML>
<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html:html>
<% 
%>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/summary.css" rel="stylesheet" type="text/css" />
<title><bean:message bundle="PRM" key="prm.uplift.manageupliftfactors"/></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="javascript" src="javascript/JLibrary.js"></script>
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellspacing="0" cellpadding="0" border="0">
<html:form action="ManageUpliftAction">
<html:hidden property="jobId" />
<html:hidden property="appendixId" />
<html:hidden property="ownerId" />
<html:hidden property="viewjobtype" />
<html:hidden property="ref" />
<tr>
  	<td colspan="2">
  	
  	<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Job</span></a>
  			<ul>
				
     				<li><a href="#">Change Status</a></li>
				<li>
 					<a href="#"><span>Addendum</span></a>
					<ul>
						
     						<li><a href="javascript:addendumSendEmail();">Send</a></li>
							
     					<li>
     						<a href="#"><span>View</span></a>
     						<ul>
								
									<li><a href="javascript:addendumViewContractDocument('pdf');">PDF</a></li>
										
     						</ul>
     					</li>
     					<li><a href="ManageUpliftAction.do?ref=addendumUplift&amp;Job_Id=<c:out value='${requestScope.Job_Id }'/>">Manage Uplifts</a></li>
					</ul>
				</li>
 				<li><a href="javascript:alert('Schedule Cannot Be Updated For A Default Job / Addendum')">Update Schedule</a></li>
				<li>
 					<a href="#"><span>Documents</span></a>
					<ul>
	 					<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.appendixId }'/>&amp;entityType=Addendum&amp;function=viewHistory&amp;appendixid=<c:out value='${requestScope.appendixId }'/>Addendum History</a></li>
	 					<li>
		 					<a href="#"><span>Support Documents</span></a>
		 					<ul>
			 					<li><a href="EntityHistory.do?entityId=<c:out value='${requestScope.appendixId }'/>&amp;jobId=<c:out value='${requestScope.appendixId }'/>&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement&amp;linkLibName=Addendum">Upload</a>
			 					</li><li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;jobId=<c:out value='${requestScope.Job_Id }'/>&amp;linkLibName=Addendum&amp;function=supplementHistory">History</a>
							</li></ul>
						</li>
					</ul>
				</li>
				
					<li><a href="javascript:alert('An Addendum can only be uploaded when it is in the - Pending Customer Review - status.\nThis feature is only available to support a signed or markup Addendum originally generated from Ilex or \nan alternative document provided by the customer.')">Upload File</a></li>
					
  			</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:addendumManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:addendumSendStatusReport()">Send</a></li>
      			<li><a href="javascript:addendumManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
						 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
						 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
						 <a><span class="breadCrumb1">Uplifts</a>
			    </div></td>
			</tr>
	        <tr><td height="5">&nbsp;</td></tr>	
		</tbody></table>	
  	
  	
  	
  	
  	
  	
  	
 		<%-- <table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
			<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
			<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
						 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
						 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
						 <a><span class="breadCrumb1">Uplifts</a>
			    </td>
			</tr>
		    <tr><td height="5" >&nbsp;</td></tr>	
		</table> --%>
		
<table border="0">
	<tr><td width="1">&nbsp;</td>
		<td>
			<table  border="0" cellspacing="0" cellpadding="0"  >
			<tr>
			  <td width="2" height="0"></td>
			    <td width="310">
			  	<table border="0" cellspacing="0" cellpadding="0" width="800"> 
			  	<logic:present name = "retval" scope = "request">
					<logic:equal name = "retval" value = "0">
						<tr>
							<td colspan = "2" class = "message" height = "25">Uplift factors updated successfully.</td>
						</tr>	
					</logic:equal>
					<logic:notEqual name = "retval" value = "0">
						<tr>
							<td colspan = "2" class = "message" height = "25">Uplift factors updated failure.</td>
						</tr>	
					</logic:notEqual>
				</logic:present>
			 	  <tr>
					    <td width="800" class="labeleboldhierrarchy" height="30" colspan="2"><bean:message bundle="PRM" key="prm.uplift.manageupliftfactors"/>&nbsp;for Appendix:&nbsp;<a href="AppendixHeader.do?function=view&appendixid=<bean:write name = "ManageUpliftForm" property = "appendixId" />&viewjobtype=<bean:write name = "ManageUpliftForm" property = "viewjobtype" />&ownerId=<bean:write name = "ManageUpliftForm" property = "ownerId" />"><bean:write name = "ManageUpliftForm" property = "appendixName" /></a>&nbsp;->&nbsp;Job:&nbsp;<a href="JobDashboardAction.do?function=view&jobid=<bean:write name = "ManageUpliftForm" property = "jobId" />&appendixid=<bean:write name = "ManageUpliftForm" property = "appendixId" />"><bean:write name = "ManageUpliftForm" property = "jobName" /></a></td>
				  </tr>
				</table>	
			  	<table border="0" cellspacing="1" cellpadding="1" width="300"> 
			      <tr> 
				    <td  class="labelobold" width="150"><bean:message bundle="PRM" key="prm.uplift.union"/></td>
				    <c:if test="${not empty requestScope.addendumStatus && requestScope.addendumStatus ne 'Draft'}">
				    	<td class="labelo" width="150"><html:text  styleClass="textbox" size="4" property="unionUplift" readonly = "true"/></td>
				    </c:if>
				    <c:if test="${empty requestScope.addendumStatus || requestScope.addendumStatus eq 'Draft'}">
				    	<td class="labelo" width="150"><html:text  styleClass="textbox" size="4" property="unionUplift" /></td>
				   </c:if>
				  </tr>
				  <tr> 
				    <td  class="labelebold" width="150"><bean:message bundle="PRM" key="prm.uplift.expedite24"/></td>
				    <c:if test="${not empty requestScope.addendumStatus && requestScope.addendumStatus ne 'Draft'}">
				    	<td class="labele" width="150"><html:text  styleClass="textbox" size="4" property="expedite24Uplift" readonly = "true" /></td>
				    </c:if>
				     <c:if test="${empty requestScope.addendumStatus || requestScope.addendumStatus eq 'Draft'}">
				     	<td class="labele" width="150"><html:text  styleClass="textbox" size="4" property="expedite24Uplift" /></td>
				     </c:if>
				  </tr>
				  <tr> 
				    <td  class="labelobold" width="150"><bean:message bundle="PRM" key="prm.uplift.expedite48"/></td>
				    <c:if test="${not empty requestScope.addendumStatus && requestScope.addendumStatus ne 'Draft'}">
					    <td class="labelo" width="150"><html:text  styleClass="textbox" size="4" property="expedite48Uplift" readonly = "true" /></td>
					</c:if>
					<c:if test="${empty requestScope.addendumStatus || requestScope.addendumStatus eq 'Draft'}">
						<td class="labelo" width="150"><html:text  styleClass="textbox" size="4" property="expedite48Uplift" /></td>
					</c:if>
				  </tr>
				  <tr> 
				    <td  class="labelebold" width="150"><bean:message bundle="PRM" key="prm.uplift.afterhours"/></td>
				    <c:if test="${not empty requestScope.addendumStatus && requestScope.addendumStatus ne 'Draft'}">
					    <td class="labele" width="150"><html:text  styleClass="textbox" size="4" property="afterHoursUplift" readonly = "true" /></td>
					</c:if>
					<c:if test="${empty requestScope.addendumStatus || requestScope.addendumStatus eq 'Draft'}">
						 <td class="labele" width="150"><html:text  styleClass="textbox" size="4" property="afterHoursUplift" /></td>
					</c:if>
				  </tr>
				  <tr> 
				    <td  class="labelobold" width="150"><bean:message bundle="PRM" key="prm.uplift.wh"/></td>
				     <c:if test="${not empty requestScope.addendumStatus && requestScope.addendumStatus ne 'Draft'}">
					    <td class="labelo" width="150"><html:text  styleClass="textbox" size="4" property="whUplift" readonly = "true"/></td>
					 </c:if>
					<c:if test="${empty requestScope.addendumStatus || requestScope.addendumStatus eq 'Draft'}">
						<td class="labelo" width="150"><html:text  styleClass="textbox" size="4" property="whUplift" /></td>
					</c:if> 
				  </tr>
				  <tr>
					  <td colspan="2" class="buttonrow">
					  	<c:if test="${not empty requestScope.addendumStatus && requestScope.addendumStatus ne 'Draft'}">
						  <html:submit property="save" styleClass="button" disabled="true"><bean:message bundle="PRM" key="prm.uplift.save" /></html:submit>
						</c:if>
						<c:if test="${empty requestScope.addendumStatus || requestScope.addendumStatus eq 'Draft'}">
							 <html:submit property="save" styleClass="button" onclick = "javascript:return validate();"><bean:message bundle="PRM" key="prm.uplift.save" /></html:submit>
						</c:if>
						  <html:button property="back" styleClass="button" onclick = "javascript:return Back();"><bean:message bundle="PRM" key="prm.uplift.back" /></html:button>
					  </td>
				  </tr>
				  
			  </table>
			 </td>
			 </tr>
			</table> 
		</td>
	</tr>
</table>
<script>
function validate(){
		if( (document.forms[0].unionUplift.value == '') || (!isFloat(document.forms[0].unionUplift.value) ) ) {
	    	alert('Please enter a numeric value for Union Uplift');
    		document.forms[0].unionUplift.focus();
    		return false;
		}
		
		if( (document.forms[0].expedite24Uplift.value == '') || (!isFloat(document.forms[0].expedite24Uplift.value) ) ) {
	    	alert('Please enter a numeric value for Expedite 24 Uplift');
    		document.forms[0].expedite24Uplift.focus();
    		return false;
		}
	
		if( document.forms[0].expedite48Uplift.value == '' || !(isFloat(document.forms[0].expedite48Uplift.value) ) ) {
	    	alert('Please enter a numeric value for Expedite 48 Uplift');
    		document.forms[0].expedite48Uplift.focus();
    		return false;
		}
	
		if( document.forms[0].afterHoursUplift.value == '' || !(isFloat(document.forms[0].afterHoursUplift.value) ) ) {
	    	alert('Please enter a numeric value for After Hours Uplift');
    		document.forms[0].afterHoursUplift.focus();
    		return false;
		}
	
		if( document.forms[0].whUplift.value == '' || !(isFloat(document.forms[0].whUplift.value) ) ) {
	    	alert('Please enter a numeric value for Weekends/ Holidays Uplift');
    		document.forms[0].whUplift.focus();
    		return false;
		}
		return true;
}

function Back() {
document.forms[0].action = "JobDashboardAction.do?function=view&jobid=<bean:write name = "ManageUpliftForm" property = "jobId" />&appendixid=<bean:write name = "ManageUpliftForm" property = "appendixId" />";
document.forms[0].submit();
}

</script>
<c:if test = "${requestScope.jobType eq 'Addendum'}">
	
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
		<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/DefaultJobMenuScript.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
</html:form>
</table>
</BODY>
</html:html>
