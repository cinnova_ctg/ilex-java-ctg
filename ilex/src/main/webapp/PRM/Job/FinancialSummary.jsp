<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />

<table cellpadding="0" cellspacing="0" align="center" border="0">
	<tr>
		<td>
			<jsp:include page="CostBreakDownByJob.jsp"/>
		</td>
	</tr>
	<tr height="10">
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table  class="" cellpadding="0" cellspacing="0" width="100%" border="0">
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="left" valign="top">
									<jsp:include page="InvoiceStatus.jsp"/>
								</td>
								<td></td>
								<td align="right" valign="top">
									<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												<jsp:include page="PurchaseOrderForCompleteTab.jsp"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table> 
