<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<head>
</head>

<table cellpadding="0" cellspacing="0" border="0" class="labelorightwhitebgblueborder" width="100%">
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0" align="left">
		<tr>
			<td class="dbvaluesmallFont" height="20">
				<b>Total Time Onsite:</b>
				<c:out value="${JobSetUpForm.timeOnSite}" />
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont" height="20">
				<b>Time To Respond:</b>&nbsp;
				<fmt:formatNumber value="${JobSetUpForm.timeToRespond}" type="currency" pattern="######0.0"/>
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont" height="20">
				<b>Criticality:</b>&nbsp;&nbsp;&nbsp;&nbsp;
				Actual:&nbsp;<c:out value="${JobSetUpForm.criticalityActual}"/>&nbsp;&nbsp;&nbsp;
				Suggested:&nbsp;<c:out value="${JobSetUpForm.criticalitySuggested}"/>&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>