<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />

<table  class="labelowhitebackground" id="CVPT"  cellpadding="0" cellspacing="1">
	<tr>
		<td class="labelowhitebackground" >&nbsp;</td>
		<td class="labelowhitebackground" ><b>Estimated<br>Cost</b></td>
		<td class="labelowhitebackground" ><b>Pro Forma<br>VGPM</b></td>
		<td class="labelowhitebackground" ><b>Actual<br>Cost</b></td>
		<td class="labelowhitebackground" ><b>Extended<br>Price</b></td>
		<td class="labelowhitebackground" ><b>Job<br>Discount</b></td>
		<td class="labelowhitebackground" ><b>Invoice<br>Price</b></td>
		<td class="labelowhitebackground" ><b>Actual<br>VGPM</b></td>
		<td class="labelowhitebackground" ><b>Actual<br>VGP</b></td>
		<td class="labelowhitebackground" ><b>PMVIPP</b></td>
		<td class="labelowhitebackground" ><b>Project Averages<br>Actual Cost</b></td>
		
	</tr>
	<tr>
		<td class="labelorightwhitebackground" ><b>Field Labor</b></td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
	</tr>
	<tr>
		<td class="labelorightdarkbackground" ><b>Materials</b></td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
	</tr>
	<tr>
		<td class="labelorightwhitebackground"><b>Freight</b></td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
	</tr>
	<tr>
		<td class="labelorightdarkbackground" ><b>Travel</b></td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
		<td class="labelorightdarkbackground" >$XXX.XX</td>
	</tr>
	<tr>
		<td class="labelorightwhitebackground" ><b>Total</b></td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
		<td class="labelorightwhitebackground" >$XXX.XX</td>
	</tr>
</table>
			