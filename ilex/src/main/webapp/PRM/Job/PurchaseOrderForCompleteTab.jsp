<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
function openPO(poId)
{
	var appendixType = escape("<c:out value='${JobSetUpForm.appendixType}'/>");
	var url = "./POAction.do?hmode=unspecified&appendixType="+appendixType+"&tabId=0&appendixId=<c:out value='${JobSetUpForm.appendixId}'/>&jobId=<c:out value='${JobSetUpForm.jobId}'/>&poId="+poId;
	document.forms[0].action = url;
	document.forms[0].submit();
}
</script>
</head>
<body>

<table width="100%">	
	<tr>
		<td  class = "dbvaluesmallFontBold" style="padding-left: 0px;padding-top: 13px">PO Status
		</td>
	</tr>
	<tr>
		<td >
			<table  width="100%" height = "100%"  cellpadding=0 cellspacing=1 class="Ntextoleftalignnowrappaleyellowborder7">
				<tr>
				   	<td class = "texthdNoBorder"><b>PO/WO </td>
				   	<td class = "texthdNoBorder"><b>Type </td>
				   	<td class = "texthdNoBorder"><b>Cost </td>
				   	<td class = "texthdNoBorder" width="50%"><b>Partner/Contact </td>
				   	<td class = "texthdNoBorder"><b>Status</td>
				   	<td class = "texthdNoBorder"><b>Del</td>
				</tr>
				<c:forEach items="${JobSetUpForm.poId}" var="poListItr" varStatus="index">
					<input type="hidden" name="poId" value="<c:out value="${JobSetUpForm.poId[index.index]}"/>"/>
					<input type="hidden" name="poNumber" value="<c:out value="${JobSetUpForm.poNumber[index.index]}"/>"/>
					<input type="hidden" name="woNumber" value="<c:out value="${JobSetUpForm.woNumber[index.index]}"/>"/>
					<input type="hidden" name="status" value="<c:out value="${JobSetUpForm.status[index.index]}"/>"/>
					<input type="hidden" name="revision" value="<c:out value="${JobSetUpForm.revision[index.index]}"/>"/>
					<input type="hidden" name="costingType" value="<c:out value="${JobSetUpForm.costingType[index.index]}"/>"/>
					<input type="hidden" name="authorizedCost" value="<c:out value="${JobSetUpForm.authorizedCost[index.index]}"/>"/>
					<input type="hidden" name="partnerContact" value="<c:out value="${JobSetUpForm.partnerContact[index.index]}"/>"/>
					<input type="hidden" name="previousStatus" value="<c:out value="${JobSetUpForm.previousStatus[index.index]}"/>"/>
					<input type="hidden" name="cancelledPONumber" value="<c:out value="${JobSetUpForm.cancelledPONumber[index.index]}"/>"/>
					<input type="hidden" name="customerDelCount" value="<c:out value="${JobSetUpForm.customerDelCount[index.index]}"/>"/>
					<input type="hidden" name="internalDelCount" value="<c:out value="${JobSetUpForm.internalDelCount[index.index]}"/>"/>
					<input type="hidden" name="totalDelCount" value="<c:out value="${JobSetUpForm.totalDelCount[index.index]}"/>"/>
								
						<tr>
							<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  align="center"> <a href="#" onclick="openPO('<c:out value="${JobSetUpForm.poId[index.index]}"/>');"><c:out value="${JobSetUpForm.poNumber[index.index]}"/>/<c:out value="${JobSetUpForm.woNumber[index.index]}"/></a> </td>
							<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  align="center"> <c:out value="${JobSetUpForm.masterPOItemType[index.index]}"/></td>
						   	<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  align="right"> <fmt:formatNumber value='${JobSetUpForm.authorizedCost[index.index]}' type='currency' pattern='$###,##0.00'/></td>
						   	<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>   ><c:out value="${JobSetUpForm.partnerContact[index.index]}"/><c:out value="${JobSetUpForm.primaryContact[index.index]}"/> </td>
						   	<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  align="center" ><c:out value="${JobSetUpForm.status[index.index]}"/></td>
						   	<td <c:if test="${(index.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(index.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if>  align="center" >
						   		<c:if test="${JobSetUpForm.totalDelCount[index.index] ne '0'}">
							   		<c:choose>
								   		<c:when test="${JobSetUpForm.totalDelCount[index.index] eq '1' && JobSetUpForm.customerDelCount[index.index] eq '1'}">
								   			<img src="images/customer_del_single.gif" width="13" height="13" border="0" style="vertical-align: bottom;" alt="Single Customer Deliverable"/>
								   		</c:when>
								   		<c:when test="${JobSetUpForm.totalDelCount[index.index] ne '0' && JobSetUpForm.totalDelCount[index.index] ne '1' && JobSetUpForm.customerDelCount[index.index] ne '0'}">
								   			<img src="images/customer_del_multiple.gif" width="13" height="13" border="0" style="vertical-align: bottom;" alt="Multiple Customer Deliverables"/>
								   		</c:when>
								   		<c:when test="${JobSetUpForm.totalDelCount[index.index] eq '1' && JobSetUpForm.internalDelCount[index.index] eq '1'}">
								   			<img src="images/internal_del_single.gif" width="13" height="13" border="0" style="vertical-align: bottom;" alt="Single Internal Deliverable"/>
								   		</c:when>
								   		<c:when test="${JobSetUpForm.totalDelCount[index.index] ne '0' && JobSetUpForm.totalDelCount[index.index] ne '1' && JobSetUpForm.internalDelCount[index.index] ne '0'}">
								   			<img src="images/internal_del_multiple.gif" width="13" height="13" border="0" style="vertical-align: bottom;" alt="Multiple Internal Deliverables"/>
								   		</c:when>
							   		</c:choose>
						   		</c:if>
						   	</td>
						</tr>
						
					</c:forEach>
						<c:if test="${empty JobSetUpForm.poId}">
							<tr>
								<td colspan="9" class="Nhyperevenfont">No Purchase Orders</td> 
							</tr>
						</c:if>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>