$(document).ready(function () {
    // Temp variables
    var scheduleElements = [];
    var type = 'Site Visit';
    if ($('#add_dispatch').size() > 0) {
        type = 'Dispatch';
    }

    // Settings
    var scheduleElementArea = '#schedule_element_area';
    var row_names = [];
    if (type === 'Dispatch') {
        row_names = [
            {
                name: "scheduled_start_date",
                display_name: "Scheduled Arrival"
            },
            {
                name: "actual_start_date",
                display_name: "Actual Arrival"
            },
            {
                name: "actual_end_date",
                display_name: "Departure"
            }
        ];
    } else {
        row_names = [
            {
                name: "scheduled_start_date",
                display_name: "Scheduled Arrival"
            },
            {
                name: "scheduled_end_date",
                display_name: "Scheduled End"
            },
            {
                name: "actual_start_date",
                display_name: "Actual Start"
            },
            {
                name: "actual_end_date",
                display_name: "Actual End"
            }
        ];
    }

    // Event Handlers
    $('#add_dispatch,#add_site_visit').click(function (e) {
        e.preventDefault();

        addScheduleElement();
    });

    $('#save_schedules').click(function (e) {
        e.preventDefault();

        saveScheduleElements();
    });

    $(document).on('click', '.schedule_delete', function () {
        var id = $(this).data('id');
        if (id == 0) {
            return;
        }

        scheduleElements.splice(parseInt(id), 1);
        renderScheduleElements();
    });

    $(document).on('change', '[name="reschedule_type"]', function () {
        var id = $(this).data('id');
        var val = $(this).val();

        scheduleElements[id].reschedule_reason.reason = val;

        if (val === 'Internal') {
            $('#internal_justification_' + id).show();
        } else {
            $('#internal_justification_' + id).hide();
            $('#reschedule_reason_' + id + ' [name="rseInternalJustification"]').prop('checked', false);
            scheduleElements[id].reschedule_reason.internal_reason = -1;
        }
    });

    $(document).on('change', '[name="rseInternalJustification"]', function () {
        var id = $(this).data('id');
        var val = $(this).val();

        scheduleElements[id].reschedule_reason.internal_reason = val;
    });

    $(document).on('change', '.schedule_element_part_reschedule', function () {
        var se = $(this).closest('.scheduleElement');
        var id = $(se).data('id');

        if ($('[name="scheduleId"]', se).size() > 0) {
            var old = scheduleElements[id]['scheduled_start_date'];
            bindToModel();
            buildRescheduleBox(id, old, scheduleElements[id]['scheduled_start_date']);
        } else {
            bindToModel();
        }
    });

    $(document).on('change', '.schedule_element_part', function () {
        bindToModel();
    });

    $.ajax({
        type: "GET",
        url: api_url + "ilex/schedule/" + job_id + "/list",
        dataType: "json",
        headers: {
            "Authorization": "Basic " + btoa(api_user + ":" + api_pass)
        },
        success: function (res) {
            if (res.status === 'success') {
                scheduleElements = res.data;
                if (scheduleElements.length === 0) {
                    $.ajax({
                        type: "GET",
                        url: api_url + "ilex/job/" + job_id,
                        dataType: "json",
                        headers: {
                            "Authorization": "Basic " + btoa(api_user + ":" + api_pass)
                        },
                        success: function (resJob) {
                            if (resJob.status === 'success') {
                                if (resJob.data.planned_end_date !== '' && resJob.data.planned_end_date !== null && (new Date(resJob.data.planned_end_date)).getFullYear() > 1900) {
                                    addScheduleElement(resJob.data.planned_start_date, resJob.data.planned_end_date);
                                } else {
                                    addScheduleElement(resJob.data.planned_start_date);
                                }
                            } else {
                                addScheduleElement();
                            }
                        },
                        failure: function (errMsg) {
                        }
                    });
                }
                renderScheduleElements();
            }
        },
        failure: function (errMsg) {
        }
    });

    function saveScheduleElements() {
        if (verify_schedules()) {
            var data = {
                job_id: job_id,
                user_id: user_id,
                schedules: JSON.stringify(scheduleElements)
            };
            $.ajax({
                type: "POST",
                url: api_url + "ilex/schedule/save",
                dataType: "json",
                data: data,
                headers: {
                    "Authorization": "Basic " + btoa(api_user + ":" + api_pass)
                },
                success: function (res) {
                    if (res.status === 'success') {
                        show_noty('success', 'top', res.data);

                        var url = 'JobDashboardAction.do?type=7&Job_Id=' + job_id + '&isClicked=6';
                        if (type === 'Dispatch') {
                            var url = 'JobDashboardAction.do?type=8&Job_Id=' + job_id + '&isClicked=7';
                        }

                        setTimeout(function () {
                            if (typeof parent.ilexmain !== 'undefined') {
                                parent.ilexmain.location = url;
                            } else {
                                window.location = url;
                            }
                        }, 500);
                    } else {
                        show_noty('error', 'top', res.msg);
                    }
                },
                failure: function (errMsg) {
                }
            });
        }
    }

    function addScheduleElement(scheduled_start, scheduled_end) {
        var newScheduleElement = {
            scheduled_start_date: typeof scheduled_start !== 'undefined' ? scheduled_start : jsDateToSqlDateStr(),
            scheduled_end_date: typeof scheduled_end !== 'undefined' ? scheduled_end : '1900-01-01T05:00:00.000Z',
            actual_start_date: '1900-01-01T05:00:00.000Z',
            actual_end_date: '1900-01-01T05:00:00.000Z',
            created_by: user_id,
            order_index: scheduleElements.length + 1
        };

        scheduleElements.push(newScheduleElement);
        renderScheduleElements();
    }

    // HTML Builder Function
    function renderScheduleElements() {
        $(scheduleElementArea).empty();

        for (var k = 0; k < scheduleElements.length; k++) {
            var scheduleElement = scheduleElements[k];

            scheduleElement.order_index = k + 1;

            var output = '<div class="scheduleElement" data-id="' + k + '" style="display: block; margin-bottom: 8px; overflow: hidden; width: 940px;">';

            if (typeof scheduleElement.id !== 'undefined') {
                output += '    <input type="hidden" name="scheduleId" value="' + scheduleElement.id + '">'
                        + '    <input type="hidden" name="scheduleNumber" value="' + scheduleElement.id + '" id="scheduleNumber' + k + '">';
            }

            output += '<div style="border-bottom: 1px solid #7e7e7e; margin-bottom: 4px; margin-top: 8px; font-size: 14px;">' + type + ' ' + (k + 1) + '</div>';

            output += '<div style="float: left;">'
                    + '    <table id="scheduleElement_table" border="0" cellspacing="1" cellpadding="0">'
                    + '        <tbody>';

            for (var i = 0; i < row_names.length; i++) {
                var row_name = row_names[i];

                var extra_input_class = '';
                if (row_name.name === 'scheduled_start_date') {
                    extra_input_class = '_reschedule';
                }

                // Break apart time.
                var full_date = '';
                var hours = '00';
                var minutes = '00';
                var ampm = 'AM';
                if (typeof scheduleElement[row_name.name] !== 'undefined') {
                    // Sample: "1900-01-01T05:00:00.000Z"
                    var date = new Date(scheduleElement[row_name.name]);

                    var month = date.getMonth() + 1;
                    if (month < 10) {
                        month = '0' + month;
                    }

                    var day = date.getDate();
                    if (day < 10) {
                        day = '0' + day;
                    }

                    full_date = month + '/' + day + '/' + date.getFullYear();

                    if (full_date === '01/01/1900') {
                        full_date = '';
                    }

                    hours = date.getHours();
                    minutes = date.getMinutes();
                    ampm = hours >= 12 ? 'PM' : 'AM';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                }

                // Determine color
                var classColor = 'o';
                if (i % 2 === 0) {
                    classColor = 'e';
                }

                output += '            <tr>'
                        + '                <td class="label' + classColor + 'bold" nowrap="nowrap">' + row_name.display_name + ':&nbsp;</td>'
                        + '                <td class="label' + classColor + '" nowrap="nowrap">'
                        + '                    <input type="text" name="' + row_name.name + '" size="10" value="' + full_date + '" readonly="readonly" class="textbox schedule_element_part' + extra_input_class + '" id="' + row_name.name + k + '">&nbsp;'
                        + '                </td>'
                        + '                <td class="label' + classColor + 'bold nowrap">'
                        + '                    <select name="' + row_name.name + 'hh" class="comboo schedule_element_part' + extra_input_class + '" id="' + row_name.name + 'hh' + k + '">';

                for (var hh = 0; hh <= 12; hh++) {
                    var hh_out = hh;
                    if (hh < 10) {
                        hh_out = '0' + hh;
                    }
                    output += '    <option value="' + hh_out + '"';

                    if (hh === hours) {
                        output += ' selected';
                    }

                    output += '>' + hh_out + '</option>';
                }

                output += '                    </select>'
                        + '                    <select name="' + row_name.name + 'mm" class="comboo schedule_element_part' + extra_input_class + '" id="' + row_name.name + 'mm' + k + '">';

                for (var mm = 0; mm <= 59; mm++) {
                    var mm_out = mm;
                    if (mm < 10) {
                        mm_out = '0' + mm;
                    }
                    output += '    <option value="' + mm_out + '"';

                    if (mm === minutes) {
                        output += ' selected';
                    }

                    output += '>' + mm_out + '</option>';
                }

                output += '                    </select>'
                        + '                    <select name="' + row_name.name + 'op" class="comboo schedule_element_part' + extra_input_class + '" id="' + row_name.name + 'op' + k + '">'
                        + '                        <option value="AM"';

                if (ampm === 'AM') {
                    output += ' selected';
                }

                output += '>AM</option>'
                        + '                        <option value="PM"';

                if (ampm === 'PM') {
                    output += ' selected';
                }

                output += '>PM</option>'
                        + '                    </select>'
                        + '                </td>'
                        + '            </tr>';
            }

            if (k !== 0) {
                output += '<tr>'
                        + '    <td colspan="3">'
                        + '        <input type="button" name="delete" value="Delete" data-id="' + k + '" class="button_c schedule_delete' + extra_input_class + '" style="margin-bottom: 0px;">'
                        + '    </td>'
                        + '</tr>';
            }

            output += '        </tbody>'
                    + '    </table>'
                    + '</div>';

            output += '<div id="reschedule_cell_' + k + '" style="float: left;"></div>';

            output += '</div>';

            $(scheduleElementArea).append(output);

            renderRescheduleBox(k);

            for (var i = 0; i < row_names.length; i++) {
                var row_name = row_names[i];
                $('#' + row_name.name + k).datepicker();
            }
        }
    }

    function bindToModel() {
        for (var k = 0; k < scheduleElements.length; k++) {
            var scheduleElement = scheduleElements[k];
            scheduleElement.order_index = k + 1;

            for (var i = 0; i < row_names.length; i++) {
                var row_name = row_names[i];

                var full_date = $('#' + row_name.name + k).val();
                var hours = $('#' + row_name.name + 'hh' + k).val();
                var minutes = $('#' + row_name.name + 'mm' + k).val();
                var ampm = $('#' + row_name.name + 'op' + k).val();

                // Sample: "1900-01-01T05:00:00.000Z"
                var final_date = jsDateToSqlDateStr(full_date + ' ' + hours + ':' + minutes + ' ' + ampm);

                if (typeof scheduleElement[row_name.name] !== 'undefined') {
                    scheduleElement[row_name.name] = final_date;
                }
            }
        }
    }

    function jsDateToSqlDateStr(str) {
        var date = new Date();
        if (typeof str !== 'undefined') {
            date = new Date(str);
        }

        var final_date = '1900-01-01 00:00';
        if (date.toString() !== 'Invalid Date') {
            var month = date.getMonth() + 1;
            if (month < 10) {
                month = '0' + month;
            }

            var day = date.getDate();
            if (day < 10) {
                day = '0' + day;
            }

            final_date = date.getFullYear() + '-' + month + '-' + day + ' ';

            var hh = date.getHours();
            if (hh < 10) {
                hh = '0' + hh;
            }

            var mm = date.getMinutes();
            if (mm < 10) {
                mm = '0' + mm;
            }

            final_date += hh + ':' + mm;
        }

        return final_date;
    }

    function buildRescheduleBox(id, old_date, new_date) {
        scheduleElements[id].reschedule_reason = {
            old_date: old_date,
            new_date: new_date,
            reason: -1,
            internal_reason: -1
        };

        renderRescheduleBox(id);
    }

    function renderRescheduleBox(id) {
        var rechedule_reason = scheduleElements[id].reschedule_reason;
        if (typeof rechedule_reason === 'undefined') {
            return;
        }

        var output = '        <div id="reschedule_reason_' + id + '" style="width: 600px;">'
                + '            <input type="hidden" name="currentSchDate" value="' + rechedule_reason.new_date + '">'
                + '            <input type="hidden" name="oldSchStart" value="' + rechedule_reason.old_date + '">'
                + '            <div style="padding: 0px 0px 0px 65px">'
                + '                <table border="0" cellpadding="0" cellspacing="0" style="border: 3px solid #AAA;" bgcolor="white">'
                + '                    <tbody>'
                + '                        <tr>'
                + '                            <th style="background: #FAF8CC;font-family: \'Verdana\', Arial, Helvetica, \'sans-serif\';font-weight: bold;font-size: 11px;color: black;">Rescheduling</th>'
                + '                        </tr>'
                + '                        <tr>'
                + '                            <td style="padding: 5px 10px 0px 10px">'
                + '                                <table style="margin-top:5px;" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF" width="25px">'
                + '                                    <tbody>'
                + '                                        <tr>'
                + '                                            <td class="labelNoColor" colspan="3">Why?</td>'
                + '                                        </tr>'
                + '                                    </tbody>'
                + '                                </table>'
                + '                                <table style="border: 1px black solid;margin-top:5px;" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">'
                + '                                    <tbody>'
                + '                                        <tr>'
                + '                                            <td class="labelNoColor">'
                + '                                                <input type="radio" name="reschedule_type" data-id="' + id + '" value="Client"' + (rechedule_reason.reason === 'Client' ? ' selected' : '') + '>Client'
                + '                                            </td>'
                + '                                            <td class="labelNoColor">'
                + '                                                <input type="radio" name="reschedule_type" data-id="' + id + '" value="Contingent"' + (rechedule_reason.reason === 'Internal' ? ' selected' : '') + '>Contingent'
                + '                                            </td>'
                + '                                            <td class="labelNoColor">'
                + '                                                <input type="radio" name="reschedule_type" data-id="' + id + '" value="Internal"' + (rechedule_reason.reason === 'Internal' ? ' selected' : '') + '>Internal'
                + '                                            </td>'
                + '                                        </tr>'
                + '                                    </tbody>'
                + '                                </table>'
                + '                            </td>'
                + '                        </tr>'
                + '                        <tr height="3"><td></td></tr>'
                + '                        <tr id="hide1">'
                + '                            <td style="padding: 0px 10px 0px 10px">'
                + '                                <table style="margin-top:5px;" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">'
                + '                                    <tbody>'
                + '                                        <tr>'
                + '                                            <td class="labelNoColor" colspan="4">How to decide?</td>'
                + '                                        </tr>'
                + '                                    </tbody>'
                + '                                </table>'
                + '                                <table style="border: 1px black solid;margin-top:5px; margin-left:0px;margin-bottom:10px" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">'
                + '                                    <tbody>'
                + '                                        <tr style="padding-left: 35px">'
                + '                                            <td class="labelNoColor" style="padding-left: 8px;">'
                + '                                                Client:'
                + '                                            </td>'
                + '                                            <td class="labelNoColor" style="white-space: normal;">'
                + '                                                Anytime the client calls to reschedule an existing planned visit.'
                + '                                            </td>'
                + '                                        </tr>'
                + '                                        <tr style="padding-left: 35px;">'
                + '                                            <td class="labelNoColor" style="padding-left: 8px;">'
                + '                                                Contingent:'
                + '                                            </td>'
                + '                                            <td class="labelNoColor" style="white-space: normal;">'
                + '                                                Anytime Contingent requests a change to an existing planned visit where the client expects the tech to be on site at a specific time.'
                + '                                            </td>'
                + '                                        </tr>'
                + '                                        <tr style="padding-left: 35px">'
                + '                                            <td class="labelNoColor" style="padding-left: 8px;">'
                + '                                                Internal:'
                + '                                            </td>'
                + '                                            <td class="labelNoColor" style="white-space: normal;">'
                + '                                                Select when an error has been made when setting the planned start time;Adjustment needed to correct a temporary planned start time that was not conveyed to the client;Time is not a factor for the site visit.'
                + '                                            </td>'
                + '                                        </tr>'
                + '                                    </tbody>'
                + '                                </table>'
                + '                            </td>'
                + '                        </tr>'
                + '                        <tr id="internal_justification_' + id + '" style="display: none;">'
                + '                            <td style="padding: 0px 10px 10px 10px">'
                + '                                <table style="margin-top:5px; " cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">'
                + '                                    <tbody>'
                + '                                        <tr>'
                + '                                            <td class="labelNoColor" colspan="4">Internal Justification:</td>'
                + '                                        </tr>'
                + '                                    </tbody>'
                + '                                </table>'
                + '                                <table style="border: 1px black solid;margin-top:5px; margin-left:0px;margin-bottom:5px" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">'
                + '                                    <tbody>'
                + '                                        <tr>'
                + '                                            <td class="labelNoColor" style="padding-left: 3px;">'
                + '                                                <input type="radio" name="rseInternalJustification" data-id="' + id + '" value="1"' + (rechedule_reason.internal_reason === '1' ? ' selected' : '') + '>Scheduled start time error'
                + '                                            </td>'
                + '                                        </tr>'
                + '                                        <tr>'
                + '                                            <td class="labelNoColor" style="padding-left: 3px;padding-right: 5px;">'
                + '                                                <input type="radio" name="rseInternalJustification" data-id="' + id + '" value="2"' + (rechedule_reason.internal_reason === '2' ? ' selected' : '') + '>Temporary start time entered'
                + '                                            </td>'
                + '                                        </tr>'
                + '                                        <tr>'
                + '                                            <td class="labelNoColor" style="padding-left: 3px;">'
                + '                                                <input type="radio" name="rseInternalJustification" data-id="' + id + '" value="3"' + (rechedule_reason.internal_reason === '3' ? ' selected' : '') + '>Time is not a factor'
                + '                                            </td>'
                + '                                        </tr>'
                + '                                    </tbody>'
                + '                                </table>'
                + '                            </td>'
                + '                        </tr>'
                + '                    </tbody>'
                + '                </table>'
                + '            </div>'
                + '        </div>'
                + '    </td>'
                + '</tr>';

        $('#reschedule_cell_' + id).html(output);
    }

    function verify_schedules() {
        for (var k = 0; k < scheduleElements.length; k++) {
            var schedule = scheduleElements[k];
            if (typeof schedule.reschedule_reason !== 'undefined') {
                if (schedule.reschedule_reason.reason === -1) {
                    show_noty('error', 'top', 'A reason is required for the reschedule of ' + type + ' ' + (k + 1) + '.');
                    return false;
                }

                if (schedule.reschedule_reason.reason === 'Internal' && schedule.reschedule_reason.internal_reason === -1) {
                    show_noty('error', 'top', 'An internal justification is required for the reschedule of ' + type + ' ' + (k + 1) + '.');
                    return false;
                }
            }
        }

        return true;
    }
});