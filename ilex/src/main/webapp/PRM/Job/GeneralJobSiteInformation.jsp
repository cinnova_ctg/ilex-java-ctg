<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<table  width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td valign="top">
			<table border="0">
				<tr>
					<td class="dbvaluesmallFont"><b>Job Status:</b></td>
					<td class="dbvaluesmallFont"><c:out value='${JobSetUpForm.jobStatus}'/></td>
				</tr>
			</table>
		</td>
		<td valign="top">
			<table border="0">
				<tr>
					<td class="dbvaluesmallFont"><b>Actual Schedule:</b></td>
					<td class="dbvaluesmallFont">Start:&nbsp;<c:out value='${JobSetUpForm.startDate}'/></td>
					<td>&nbsp;&nbsp;</td>
					<td class="dbvaluesmallFont">End:&nbsp;<c:out value='${JobSetUpForm.endDate}'/></td>
				</tr>
			</table>
		</td>
		<td align="right" valign="top">
			<table width="100%" border="0" align="right">
				<tr>
					<td class="dbvaluesmallFont" valign="top" width="10%">
						<b>Site1:</b>
					</td>
					<td width="90%" align="right">
						<table class="labelowhitebackground" height="70" border="0" width="100%" >
							<tr>
								<td class="dbvaluesmallFont" align="left">
									<b>Name/Number:</b>&nbsp;<c:out value='${JobSetUpForm.siteName}'/>/<c:out value='${JobSetUpForm.site_number}'/>
								</td>
							</tr>
							<tr>
								<td class="dbvaluesmallFont" align="left" width="180">
									<b>Address:</b>&nbsp;<c:out value='${JobSetUpForm.siteAddress}'/>&nbsp;
									<c:out value='${JobSetUpForm.site_city}'/>&nbsp;
									<c:out value='${JobSetUpForm.site_state}'/>&nbsp;
									<c:out value='${JobSetUpForm.siteZip}'/>&nbsp;
									<c:out value='${JobSetUpForm.site_country}'/>
								</td>
							</tr>
							<tr>
								<td class="dbvaluesmallFont" align="left"><b>Primary POC:</b>&nbsp;
									<a href="mailto:<c:out value='${JobSetUpForm.sitePrimaryPocEmail}'/>"><c:out value='${JobSetUpForm.sitePrimaryPoc}'/></a>,
									<c:out value='${JobSetUpForm.sitePrimaryPocPhone}'/>
								</td>
							</tr>
							<tr>
								<td class="dbvaluesmallFont" align="left"align="left"><b>Secondary POC:</b>&nbsp;
									<a href="mailto:<c:out value='${JobSetUpForm.siteSecondaryPocEmail}'/>"><c:out value='${JobSetUpForm.siteSecondaryPoc}'/></a>,
									<c:out value='${JobSetUpForm.siteSecondaryPocPhone}'/>
								</td>
							</tr>
						</table>
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</br>