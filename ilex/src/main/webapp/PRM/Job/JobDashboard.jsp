<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %><%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %><%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html:html><HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1" />
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
</HEAD>
<%@ include  file="/Menu.inc" %>
<% 	String actid_str="", temp_comma = "", bgcolor = "", bgcolornumber = "", textclass = "";
	int i = 0, j = 0;
	boolean csschooser = true, cssinnerchooser = true;
	String bghyperlink = "", opendiv = "", bginnercolor = "", bginnercolornumber = "", bginnerhyperlink = "";
	String hyperlinkforresource = "", hyperlinkfornetmedxresource = "", hyperlinkforactivity = "", hyperlinkforeditquantity = "", refvar = "";
	if( request.getAttribute( "NetMedX" ) != null ) 	opendiv = "t1";
	else opendiv = "1";

	String typeValue = "";
	if(request.getAttribute("type") != null) { typeValue = request.getAttribute("type").toString() ; }

	String module = "";
	if(session.getAttribute("tRModule") != null) {
		module = session.getAttribute("tRModule").toString();
	}

	String netMedXAtt = "";
	if(request.getAttribute("NetMedX") != null) {
		netMedXAtt = request.getAttribute("NetMedX").toString();
	}
%> 
<SCRIPT>
/* by hamid start */
function viewContractDocument(id, docType) {
	if(docType=='html')   document.forms[0].target = "_blank";
	else   document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+id+"&View="+docType+"&Type=Appendix&from_type=PRM&Id=<bean:write name = "JobDashboardForm" property = "appendix_id" />";
	document.forms[0].submit();
}
function addcomment() {
	document.forms[0].target = "_self";
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=jobdashboard&addendum_id=<bean:write name = "JobDashboardForm" property = "jobid" />&ref=view&Type=Jobdashboard&Appendix_Id=<bean:write name = "JobDashboardForm" property = "appendix_id" />&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />';
	document.forms[0].submit();
	return true;	 
}
function viewcomments() {
	document.forms[0].target = "_self";
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?from=jobdashboard&addendum_id=<bean:write name = "JobDashboardForm" property = "jobid" />&ref=view&Type=Jobdashboard&Id=<bean:write name = "JobDashboardForm" property = "jobid" />';
	document.forms[0].submit();
	return true;	 
}
/* by hamid end */
function adddResource(act_id, refvartemp) {
	var actId=act_id;
	var temp=refvartemp;
	document.forms[0].target = "_self";
	document.forms[0].action = "ResourceListAction.do?ref="+temp+"&resourceListType=A&from=jobdashboard&fromPage=jobDBAddResrs&appendix_Id=<bean:write name = "JobDashboardForm" property = "appendix_id" />&MSA_Id=<bean:write name = "JobDashboardForm" property = "msa_id" />&Activity_Id="+act_id+"&Id="+document.forms[0].jobid.value;
	document.forms[0].submit();
	return true;
}
function uploadjob(jobid) {
	document.forms[0].target = "_self";
	document.forms[0].action = "EntityHistory.do?entityId="+jobid+"&jobId="+jobid+"&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Job";
	document.forms[0].submit();
	return true;
}
function viewdocumentsjob(jobid) {
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewjobdashboarddocument&ref1=view&path=jobdashboard&appendix_Id=<bean:write name = "JobDashboardForm" property = "appendix_id" />&Id="+jobid;
	document.forms[0].submit();
	return true;
}
function viewdefaultdocumentsjob(jobid) {
	document.forms[0].target = "_self";
	document.forms[0].action = "EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<bean:write name = "JobDashboardForm" property = "appendix_id" />&linkLibName=Job&function=supplementHistory&jobId="+jobid;
	document.forms[0].submit();
	return true;
}
function addaddendum(action, para) {
	document.forms[0].target = "_self";
	document.forms[0].action = "AddAddendumAction.do?type=job&functionPara="+action+"&parameter="+para+"&jobid=<bean:write name = "JobDashboardForm" property = "jobid" />&appendixid=<bean:write name = "JobDashboardForm" property = "appendix_id" />";
	document.forms[0].target = "ilexmain";
	document.forms[0].submit();
	return true;	
}
function viewpowo(jobid, powoid, type) {
    document.forms[0].target = "_self";
    window.location.href = "POWOAction.do?jobid="+jobid+"&function_type=view&order_type="+type+"&powoid="+powoid;
	return false;	
}
function addaddendumjob(ref) {
	document.forms[0].target = "_self";
	document.forms[0].action = "JobUpdateAction.do?ref="+ref+"&appendixid=<bean:write name = "JobDashboardForm" property = "appendix_id" />";
	document.forms[0].target = "ilexmain";
	document.forms[0].submit();
	return true;	
}
function view(v) {
    document.forms[0].target = "_self";
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PRM&Id=<bean:write name = "JobDashboardForm" property = "appendix_id" />";
	document.forms[0].submit();
	return true;	
}
function sendemail() {
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?addendumflag=true&Type=PRMAppendix&Appendix_Id=<bean:write name = "JobDashboardForm" property = "appendix_id" />&jobId="+<bean:write name = "JobDashboardForm" property = "jobid" />;
	document.forms[0].submit();
	return true;	
}
function managestatus(v) {
	document.forms[0].target = "_self";
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_job&id=<bean:write name = "JobDashboardForm" property = "jobid" />";
	document.forms[0].submit();
	return true;	
}
function sendstatusreport() {
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=prm_job&Type1=prm_job&&id=<bean:write name = "JobDashboardForm" property = "jobid" />";
	document.forms[0].submit();
	return true;
}
function sortwindow() {
	str = 'SortAction.do?Type=jobdashboard&jobid=<bean:write name = "JobDashboardForm" property = "jobid" />';
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); */ 
	return true;
}
function toggleMenuSection(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
	}
	else {
		thisImage.src = "images/Expand.gif";
	}
}
			<% if( request.getAttribute("jobtype").equals( "Default" )) {%>
			<%}else{%>
			<%}%>

			<% if( request.getAttribute("jobtype").equals( "Default" )) {%>
			<%}else{%>
			<%}%>
function toggleDiv(divName) {
	 var tcookie = document.cookie;
     var st = tcookie.indexOf ("J1Db");
     var cookievalue ="";
	  if(st!= -1)
	  {
	      st = st+3;
	      st = tcookie.indexOf ("=", st);
	      en = tcookie.indexOf (";", st);
		  if (en > st)
	        {
	          cookievalue = tcookie.substring(st+1, en); //div's that is reopen
	        }
	 }
	var id = document.getElementById(divName);
	if( id.style.display == "none" ) 
		{
				id.style.display = "block";
				//add it to the existing cookie opendiv paraneter
				if (cookievalue.length>0)
					cookievalue = cookievalue+','+divName;
				else
					cookievalue = divName;
				if(cookievalue!="")
				{ 
					// Save as cookie
					document.cookie = "JDb="+ cookievalue+" ; ";
	            }
		}
	else
		{
			id.style.display = "none";	
			var newcookievalue='';
			//remove this div from the opendiv cookie
			if (cookievalue.length>0)
			{
				var array = cookievalue.split(',');
				if (array.length>1)
				{
					for(i=0;i<array.length;i++)
					{
						if(array[i]==divName)
						{
						}
						else
						{
							if(newcookievalue.length > 0 )
								newcookievalue = newcookievalue+','+array[i];
							else
								newcookievalue = array[i];	
						}
					}
				}
				if(newcookievalue!="")
				{ // Save as cookie
			 		document.cookie = "JDb="+ newcookievalue+" ; ";
			 	}
			}
		}
	if( divName == 'div_1')
	{
		<% if( request.getAttribute("jobtype").equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ) ) {%>
			<%}else{%>
		id = document.getElementById('div_13');
		if( id.style.display == "none" ) 
			id.style.display = "block";
		else
			id.style.display = "none";	
		id = document.getElementById('div_11');
		if( id.style.display == "none" ) 
			id.style.display = "block";
		else
			id.style.display = "none";	
		id = document.getElementById('div_14');
		if( id.style.display == "none" ) 
			id.style.display = "block";
		else
			id.style.display = "none";
		<%}%>
	}
}
function toggleMenuSectionView(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	action = "toggleType = toggleDivView('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
	}
	else {
		thisImage.src = "images/Expand.gif";
	}
}
function toggleDivView(divName) {
	 var tableRow = document.getElementsByTagName('tr');
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if (tableRow[k].getAttributeNode('id').value==divName) {
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	
	}
}
// Added Start:By Amit This will be called on each href that opens other form
function createCookie()
{
 var tableRow = document.getElementsByTagName('tr');
  var openDiv="";
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
		if((tableRow[k].getAttributeNode('id').value != "")&&((tableRow[k].getAttributeNode('id').value != "account")||(tableRow[k].getAttributeNode('id').value != "accountPowo")))
		{
			if(tableRow[k].style.display == "block" ){
			openDiv=openDiv+tableRow[k].getAttributeNode('id').value+",";
			}
		}
	}
		if(openDiv!="")
		{ // Save as cookie
		 document.cookie = "JDb="+ openDiv+" ; ";
		 }
}
function createCookie(lastDivForFocus)
{
 		document.cookie = "J1Db="+ lastDivForFocus+" ; ";
}
function CookieGetOpenDiv()
{
  var tcookie = document.cookie;
  var unique;
  var st = tcookie.indexOf ("JDb");
  var cookievalue ="";
  if(st!= -1)
  {
      st = st+3;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);
	  if (en > st)
        {
          cookievalue = tcookie.substring(st+1, en);
        }
       if(cookievalue.length>0) 
       {
		var opendiv = cookievalue.split(',');
		if(opendiv.length>0)
		{
		  for (var j = 0; j < opendiv.length; j++)
		   {
		   		if(eval("document.getElementById('"+opendiv[j]+"')")!=null)	//Added This if condition by amit
		   		{
		  				eval("document.getElementById('"+opendiv[j]+"')").style.display = "block";
						unique = opendiv[j].substring(opendiv[j].indexOf("_")+1,opendiv[j].length);
						action = "thisImage = document.getElementById('img_" + unique +"');";
						eval(action);
					if (document.getElementById('div_' + unique).offsetHeight > 0) { thisImage.src = "images/Collapse.gif"; }
					else { thisImage.src = "images/Expand.gif"; }
				}
				if(opendiv[j]=='div_1') {
						<% if( request.getAttribute("jobtype").equals( "Default" ) || request.getAttribute( "jobtype" ).equals( "Addendum" ) ) {%>
					<%}else{%>
						if(document.getElementById('div_11')!=null)
						document.getElementById('div_11').style.display = "block";
						if(document.getElementById('div_13')!=null)
						document.getElementById('div_13').style.display = "block";
						if(document.getElementById('div_14')!=null)
						document.getElementById('div_14').style.display = "block";
					<%}%>
				}												
		  }
		}
	}
  }
	  openDiv = "";
	  //Save as cookie
	  document.cookie = "J1Db="+ openDiv+" ; "; 
	  //Start:Added For Focus		
	  var st1 = tcookie.indexOf ("J1Db");
	  var cookievalue1 ="";
	  if(st1!= -1) {
      	  st1 = st1+4;
	      st1 = tcookie.indexOf ("=", st1);
	      en = tcookie.indexOf (";", st1);
		  if (en > st1) { cookievalue1 = tcookie.substring(st1+1, en); }
		}
		 if(cookievalue1!="") {  
		  		if(document.getElementById(cookievalue1)!=null) { document.getElementById(cookievalue1).focus(); }
		}
		document.cookie = "J1Db="+ openDiv+" ; ";
}
//End By Amit
</SCRIPT>
<SCRIPT>
// Start :Added By Amit For more/back of PoWo Block
var accountvalPOWO=0;
var accountnumPOWO=8;
function showaccountmorePOWO(){
	if(!document.all.accountPowo){
		return; //if the accountPowo array is not present
	}
	if(accountvalPOWO==0){					//if accountvalPOWO is zero then back tag is disabled otherwise enabled
		document.all.accountbackPOWO.disabled=true;
	}
	else{
		document.all.accountbackPOWO.disabled=false;
	}
	account_HideShowBlockPOWO();  // hide all the accountPowo elements
	for(var i=0;i<accountnumPOWO;i++){  // only fourteen elements are shown 
		if(document.all.accountPowo[accountvalPOWO]) {
	   		document.all.accountPowo[accountvalPOWO].style.display="block";
	   		accountvalPOWO++;
	   	}	
		else
			if(!document.all.accountPowo.length){
		   		document.all.accountPowo.style.display="block";
   				document.all.accountmorePOWO.disabled=true;
   				return;		   		
	   		}	   	
	}
	if(accountvalPOWO==document.all.accountPowo.length){	// if accountvalPOWO is equal to accountPowo array length then the more is disabled otherwise enabled.
		document.all.accountmorePOWO.disabled=true;
	}
	else{
		document.all.accountmorePOWO.disabled=false;
	}	
}
function showaccountbackPOWO(){
	if(!accountvalPOWO==document.all.accountPowo.length){      // if accountvalPOWO is equal to accountPowo array length then more is disabled otherwise enabled.
		document.all.accountmorePOWO.disabled=true;
	}
	else{
		document.all.accountmorePOWO.disabled=false;
	}		
	if((accountvalPOWO%accountnumPOWO)!=0)				          // if accountvalPOWO%accountnumPOWO is not zero
		accountvalPOWO=accountvalPOWO-(accountvalPOWO%accountnumPOWO);	
	accountvalPOWO= accountvalPOWO-accountnumPOWO;						// take the accountvalPOWO to back accountnumPOWO
	account_HideShowBlockPOWO();
	for(var i=0;i<accountnumPOWO;i++){						// show the last accountvalPOWO
   		document.all.accountPowo[accountvalPOWO].style.display="block";
   		accountvalPOWO++;
 	}
	accountvalPOWO= accountvalPOWO-accountnumPOWO;						// set the accountvalPOWO to back accountnumPOWO
	if(accountvalPOWO==0){							// if accountvalPOWO=0 then back is disabled and show the first div.
		showaccountmorePOWO();	
		document.all.accountbackPOWO.disabled=true;
	}
	else{
		document.all.accountbackPOWO.disabled=false;
	}
}
function account_HideShowBlockPOWO()		// hide all the divs.
{
        var len=document.all.accountPowo.length;
        for(var id=0;id<len;id++) { 
    		document.all.accountPowo[id].style.display="none";
    	}	
}
// End :For more/back of PoWo Block
</SCRIPT>
<html:form action="JobDashboardAction">
<html:hidden property="inv_id" />
<html:hidden property="inv_rdofilter" />
<html:hidden property="inv_type" />
<html:hidden property="invoice_Flag"/>
<html:hidden property="invoiceno"/>
<html:hidden property="partner_name"/>
<html:hidden property="powo_number"/>
<html:hidden property="from_date"/>
<html:hidden property="to_date"/>
<html:hidden property = "nettype" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "jobid" />
<bean:define id = "job_status" name = "JobDashboardForm" property = "job_status" />

<!-- Start :Added By Amit -->
<html:hidden property = "stringChangeStatus"/>
<!-- End :Added By Amit -->
<BODY onload="MM_preloadImages('images/Expand.gif','images/Collapse.gif'); CookieGetOpenDiv();  showaccountmorePOWO();">
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TBODY>
  <TR><TD width=1 height=1></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TBODY>
  <TR>
    <TD class=toprow>
      <TABLE height=18 cellSpacing=1 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR>
          <TD class= toprow1 id=pop1 width=100><A class=drop  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#">Job</A></TD>
          <TD class=toprow1 id=pop2 width=130><A class=drop   onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#">Status Report</A></TD>
         <TD class=toprow1 id=pop3 width=130><A class=drop  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu3',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu3',event)"  href="#">Comments</A></TD>
	  <TD class=toprow1>&nbsp;</TD>
         </TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<SCRIPT language=JavaScript1.2>
var canNotSendNonApprovedAddendum = '<bean:message bundle = "PRM" key = "prm.addendum.sendemail.noprivilegestatus"/>';
var addendumCRS = '<bean:message bundle = "PRM" key = "addendum.detail.upload.state"/>'
if (isMenu) {
 arMenu1=new Array(
110,
findPosX('pop1'),findPosY('pop1'),
"","",
"","",
"","",
<% if( request.getAttribute("jobtype").equals( "Default" ) ){%> //|| request.getAttribute("jobtype").equals( "Addendum" ) ) //Comment on 06/12/06 Addendum now manage from PRM
	"<bean:message bundle = "PRM" key = "prm.changestatus"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotchanestatusfordefault"/>')",0,
<%} else {
	if(job_status.equals("Closed")) {%>
	"<bean:message bundle = "PRM" key = "prm.changestatus"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotchanestatusforclosed"/>')",0,
<%} else {
	if( ( String )request.getAttribute( "list" ) != "" ) {%>
"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",1,
<%} else {%>
"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",0,
<%}}}
if(request.getAttribute("jobtype").equals( "Addendum")) {%>
"Addendum","#",1,
<% } if( request.getAttribute( "jobtype" ).equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ) ) {%>
	"<bean:message bundle = "PRM" key = "prm.updateschedule"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.CannotupdateJobschedulefordefault"/>')",0,
<%} else{%>
"Schedule","#",1,
<%}
if(!request.getAttribute("jobtype").equals( "Addendum")) { %>
<%if( request.getAttribute( "NetMedX" ) != null ){ 
	if( request.getAttribute( "jobtype" ).equals( "Default" )){%>
"Add Activity","javascript:alert('Cannot add activity to default job')",0,
<%}else{%>
"Add Activity","ActivityEditAction.do?Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />&from=inscopejob&ref=inscopeactivity&Activity_Id=0",0,
<%}}else{%>
"Add Activity","CopyAddendumActivityAction.do?Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>&from=PRM&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />",0,
<%}
}%>
<%if( request.getAttribute( "NetMedX" ) != null ){ 
	if( request.getAttribute( "jobtype" ).equals( "Default" )){%>
	"Documents","#",1
	<%}else{%>
	"Documents","#",1,
	"Test Indicator","#",1,	
	"Process Checklist","ChecklistTab.do?job_id=<bean:write name = "JobDashboardForm" property = "jobid" />&page=jobdashboard&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />",0
	<%}}else{
	if( request.getAttribute( "jobtype" ).equals( "Default" ) || request.getAttribute( "jobtype" ).equals( "Addendum" ) )
	{
		if( request.getAttribute( "jobtype" ).equals( "Addendum" )){ %>
				"Documents","#",1,
			<%if(job_status.equals("Pending Customer Review")) {%>
				"Upload File","EntityHistory.do?function=modifyDocument&entityId=<bean:write name = "JobDashboardForm" property = "jobid" />&entityType=Addendum&appendixid=<bean:write name = "JobDashboardForm" property = "appendix_id" />",0
			<%}else{%>
				"Upload File","javascript:alert(addendumCRS)",0
			<%}%>
		<%}else{%>
			"Documents","#",1
		<%}%>
	<%}else{%>
	"Documents","#",1,
	"Test Indicator","#",1,	
	"Process Checklist","ChecklistTab.do?job_id=<bean:write name = "JobDashboardForm" property = "jobid" />&page=jobdashboard&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />",0
	<%}%>
<%}%>
)
<% 
if( ( String )request.getAttribute( "list" ) != "" ) {%>
arMenu1_1=new Array(
<%= ( String ) request.getAttribute( "list" ) %>
)
<% } %>
<% 
if(request.getAttribute("jobtype").equals( "Addendum")) { %>
arMenu1_2=new Array(
<%if(!job_status.equals("Draft") && !job_status.equals("Review") && !job_status.equals("Cancelled")) {%>
"Send","javascript:sendemail();",0,
<%} else {%> 
"Send","javascript:alert(canNotSendNonApprovedAddendum);",0,
<%}%>
"View","#",1,
"Add Activity","CopyAddendumActivityAction.do?Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>&from=PRM&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />",0,
"Manage Activities","ActivityUpdateAction.do?ref=inscopeactivity&fromPage=ManageAddendumActivity&Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />",0,
"Manage Uplifts","ManageUpliftAction.do?ref=addendumUplift&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />&Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />",0
)

<%if(!job_status.equals("Draft") && !job_status.equals("Review") && !job_status.equals("Cancelled")) {%>
arMenu1_2_2=new Array(
"PDF","javascript:viewContractDocument(<bean:write name = "JobDashboardForm" property = "jobid" />, 'pdf');",0
)
<%}else{%>
<% if(request.getAttribute("viewType").equals("All")) {%>
arMenu1_2_2=new Array(
"PDF","javascript:viewContractDocument(<bean:write name = "JobDashboardForm" property = "jobid" />, 'pdf');",0,
"RTF","javascript:viewContractDocument(<bean:write name = "JobDashboardForm" property = "jobid" />, 'rtf');",0,
"HTML","javascript:viewContractDocument(<bean:write name = "JobDashboardForm" property = "jobid" />, 'html');",0
)
<% } else {%>
arMenu1_2_2=new Array(
"PDF","javascript:viewContractDocument(<bean:write name = "JobDashboardForm" property = "jobid" />, 'pdf');",0,
"EXCEL","javascript:viewContractDocument(<bean:write name = "JobDashboardForm" property = "jobid" />, 'excel');",0
)
<% }}%>
<%}%>
<% if(!(request.getAttribute( "jobtype" ).equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ) )) { %>
arMenu1_2=new Array(
<% if( request.getAttribute( "NetMedX" ) != null ){ %>
"Update","DispatchScheduleUpdate.do?function=update&typeid=<bean:write name="JobDashboardForm" property="jobid"/>&type=J&page=jobdashboard&pageid=<bean:write name="JobDashboardForm" property="appendix_id"/>",0
<%}else { %>
"Update","ScheduleUpdate.do?function=update&typeid=<bean:write name="JobDashboardForm" property="jobid"/>&type=J&page=jobdashboard&pageid=<bean:write name="JobDashboardForm" property="appendix_id"/>",0
<% } %>
)
<% } %>

<% if(request.getAttribute("jobtype").equals( "Addendum")) { %>
	arMenu1_4=new Array(
	"Addendum History","EntityHistory.do?entityId=<bean:write name="JobDashboardForm" property="jobid"/>&entityType=Addendum&function=viewHistory&appendixid=<bean:write name = "JobDashboardForm" property = "appendix_id" />",0,	
	"Support Documents","#",1
	)
	
	arMenu1_4_2 = new Array(
	"Upload","EntityHistory.do?entityId=<bean:write name="JobDashboardForm" property="jobid"/>&jobId=<bean:write name="JobDashboardForm" property="jobid"/>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Addendum",0,
	"History","EntityHistory.do?entityType=Appendix Supporting Documents&jobId=<bean:write name="JobDashboardForm" property="jobid"/>&linkLibName=Addendum&function=supplementHistory",0
	)
	
<%}else{%>
	<% if(request.getAttribute("jobtype").equals( "Default")) { %>
		arMenu1_4=new Array(
		"View","javascript:viewdefaultdocumentsjob(<bean:write name = "JobDashboardForm" property = "jobid" />);",0,
		"Upload","javascript:uploadjob(<bean:write name = "JobDashboardForm" property = "jobid" />);",0
		)
	<%}else{%>
		arMenu1_4=new Array(
		"Upload","EntityHistory.do?entityId=<bean:write name="JobDashboardForm" property="jobid"/>&jobId=<bean:write name="JobDashboardForm" property="jobid"/>&entityType=Deliverables&function=addSupplement&linkLibName=Job",0,
		"History","EntityHistory.do?entityType=Deliverables&jobId=<bean:write name="JobDashboardForm" property="jobid"/>&linkLibName=Job&function=supplementHistory",0
		)
	<%}%>
	
<%}%>

arMenu1_5=new Array(
	"None","JobDashboardAction.do?jobIndicator=None&changeTestIndicator=yes&jobid=<bean:write name="JobDashboardForm" property="jobid"/>",0,
	"Demonstration","JobDashboardAction.do?jobIndicator=Demonstration&changeTestIndicator=yes&jobid=<bean:write name="JobDashboardForm" property="jobid"/>",0,
	"Internal Test","JobDashboardAction.do?jobIndicator=Internal Test&changeTestIndicator=yes&jobid=<bean:write name="JobDashboardForm" property="jobid"/>",0
)


<%-- if( request.getAttribute( "NetMedX" ) != null ){ %>
<%} else { %>
<% if(request.getAttribute("jobtype").equals( "Addendum")) { %>
<% } else { %>
arMenu1_5=new Array(
"Add","javascript:addaddendum('add', 'add');",0,
"Update","javascript:addaddendum('add', 'update');",0,
"View","javascript:addaddendum('view', 'view');",0
)
<% } %>
<%}--%>
arMenu2=new Array(
80,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
"Update","javascript:managestatus( 'update' )",0,
"Send","javascript:sendstatusreport()",0,
"View","javascript:managestatus( 'view' )",0
)
// by hamid 
arMenu3=new Array(
100,
findPosX( 'pop3' ),findPosY( 'pop3' ),
"","",
"","",
"","",
"<bean:message bundle = "pm" key = "job.detail.menu.viewall"/>","javascript:viewcomments();",0,
<% if(request.getAttribute("jobtype").equals( "Addendum") && job_status.equals("Cancelled")) {%> 
	"<bean:message bundle = "pm" key = "job.detail.menu.addcomment"/>","javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.addcomment.cancelled"/>')",0
<%}else{%>
	"<bean:message bundle = "pm" key = "job.detail.menu.addcomment"/>","javascript:addcomment();",0
<%}%>

)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}
</SCRIPT>
<table>
<tr height="20"><td>&nbsp;</td></tr>
<!-- FOR Refreshing Tree View On changing Status Of Job-->
<logic:present name = "changestatusflag" scope = "request">
	<logic:equal name = "changestatusflag" value = "0">
		<tr><td colspan = "4" class = "message" height = "30">Status changed successfully.</td></tr>
			<c:if test="${empty requestScope.NetMedX}">
				<%--<script>parent.ilexleft.location.reload();</script>--%>
			</c:if>
			<c:if test="${not empty requestScope.NetMedX}">
				<script>parent.ilexleft.location.reload();</script>
			</c:if>
	</logic:equal>
	<!-- Change on 27-09-06 start -->
	<logic:equal name = "changestatusflag" value = "-9100">
		<tr><td colspan = "4" class = "message" height = "30">Status change failure!!  All dispatches are not scheduled.</td></tr>
	</logic:equal>
		<!-- Change on 27-09-06 end -->
	    <!-- Change on 12-12-07 start -->
	<logic:equal name = "changestatusflag" value = "9600">
		<tr><td colspan = "4" class = "message" height = "30"> The addendum could not be Signed to In work due to a problem on the Web server.</td></tr>
	</logic:equal>
	<logic:equal name = "changestatusflag" value = "-1">
		<tr><td colspan = "4" class = "message" height = "30">Status change failure!! <br>
		</td></tr>
	</logic:equal>
	   <!-- Change on 12-12-07 end -->
</logic:present>
<!-- End :amit -->

		<logic:present name="documentInsertionInDocM" scope="request">
			<tr>
				<td colspan="4" class="message" height="30">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
					bundle="pm" key="exception.documentinsertion" /></td>
			</tr>
		</logic:present>
		<logic:present name="EntityDetailsNotFoundException" scope="request">
			<tr>
				<td colspan="4" class="message" height="30">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
					bundle="pm" key="exception.entitydetails" /></td>
			</tr>
		</logic:present>
		<logic:present name="ControlledTypeException" scope="request">
			<tr>
				<td colspan="4" class="message" height="30">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
					bundle="pm" key="exception.controlledtype" /></td>
			</tr>
		</logic:present>
		<logic:present name="DocMException" scope="request">
			<tr>
				<td colspan="4" class="message" height="30">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
					bundle="pm" key="exception.docmexception" /></td>
			</tr>
		</logic:present>
		<logic:present name="CouldNotAddToRepositoryException" scope="request">
			<tr>
				<td colspan="4" class="message" height="30">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
					bundle="pm" key="exception.couldnotaddtorepository" /></td>
			</tr>
		</logic:present>
		<logic:present name="CouldNotReplaceException" scope="request">
			<tr>
				<td colspan="4" class="message" height="30">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
					bundle="pm" key="exception.couldnotreplace" /></td>
			</tr>
		</logic:present>
		<logic:present name="CouldNotCheckDocumentException" scope="request">
			<tr>
				<td colspan="4" class="message" height="30">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
					bundle="pm" key="exception.couldcotcheckdocument" /></td>
			</tr>
		</logic:present>
		<logic:present name="DocumentIdNotUpdatedException" scope="request">
			<tr>
				<td colspan="4" class="message" height="30"><bean:message
					bundle="pm" key="exception.couldupdatedocumentid" /></td>
			</tr>
		</logic:present>
		<logic:present name="Exception" scope="request">
			<tr>
				<td colspan="4" class="message" height="30"><bean:message
					bundle="pm" key="exception.general" /></td>
			</tr>
		</logic:present>

		<logic:present name = "emailflag" scope = "request">
		<tr> 
			<td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
		    	<logic:equal name = "emailfslag" value = "0">
					<bean:message bundle = "pm" key = "msa.detail.mailsent"/>
	    		</logic:equal>
	    		<logic:notEqual name = "emailflag" value = "0">
					<bean:message bundle = "pm" key = "msa.detail.mailsentfailure"/>
	    		</logic:notEqual>
	    	</td>
    	</tr>
</logic:present>
<logic:present name = "oosretval" scope = "request">
		<logic:equal name = "oosretval" value = "0">
			<tr><td colspan = "2" class = "message" height = "25"><bean:message bundle="PRM" key="prm.activity.oos.updatemessage"/></td></tr>	
		</logic:equal>
		<logic:notEqual name = "oosretval" value = "0">
			<logic:equal name = "oosretval" value = "-9010">
				<tr><td colspan = "2" class = "message" height = "25"><bean:message bundle="PRM" key="prm.activity.oos.existmessage"/></td></tr>	
			</logic:equal>
			<logic:notEqual name = "oosretval" value = "-9010">
				<tr><td colspan = "2" class = "message" height = "25"><bean:message bundle="PRM" key="prm.activity.oos.failuremessage"/></td></tr>
			</logic:notEqual>	
		</logic:notEqual>
	 </logic:present>
<logic:present name = "addflag" scope = "request">
	<tr>
		<td colspan = "4" class = "message" height = "30">
    	<!-- <logic:equal name = "addflag" value = "0"><bean:message bundle = "pm" key = "activity.tabular.add.success"/><script>parent.ilexleft.location.reload();</script></logic:equal> -->
			<logic:equal name = "addflag" value = "0"><bean:message bundle = "pm" key = "activity.tabular.add.success"/></logic:equal>
			<logic:equal name = "addflag" value = "-90030"><bean:message bundle = "pm" key = "activity.tabular.add.failure9"/></logic:equal>
			<logic:equal name = "addflag" value = "-90018"><bean:message bundle = "pm" key = "activity.tabular.add.failure1"/></logic:equal>
			<logic:equal name = "addflag" value = "-90020"><bean:message bundle = "pm" key = "activity.tabular.add.failure2"/></logic:equal>
			<logic:equal name = "addflag" value = "-9002"><bean:message bundle = "pm" key = "activity.tabular.add.failure3"/></logic:equal>
			<logic:equal name = "addflag" value = "-9003"><bean:message bundle = "pm" key = "activity.tabular.add.failure4"/></logic:equal>
			<logic:equal name = "addflag" value = "-9005"><bean:message bundle = "pm" key = "activity.tabular.add.failure5"/></logic:equal>
			<logic:equal name = "addflag" value = "-9006"><bean:message bundle = "pm" key = "activity.tabular.add.failure6"/></logic:equal>
			<logic:equal name = "addflag" value = "-9007"><bean:message bundle = "pm" key = "activity.tabular.add.failure7"/></logic:equal>
			<logic:equal name = "addflag" value = "-9008"><bean:message bundle = "pm" key = "activity.tabular.add.failure8"/></logic:equal>
		</td>
	</tr>
</logic:present>
<logic:present name = "jobIndicatorFlag" scope = "request">
		<tr> 
			<td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
		    	<logic:equal name = "jobIndicatorFlag" value = "0">
					<bean:message bundle = "PRM" key = "prm.job.testIndicator.success"/>
	    		</logic:equal>
	    		<logic:notEqual name = "jobIndicatorFlag" value = "0">
					<bean:message bundle = "PRM" key = "prm.job.testIndicator.failure"/>
	    		</logic:notEqual>
	    	</td>
    	</tr>
</logic:present>
<logic:present name = "ticketUpdatedFailed" scope = "request">
		<tr> 
			<td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;&nbsp;&nbsp;
		    	<logic:equal name = "ticketUpdatedFailed" value = "failed">
					This ticket can not be updated as resource of this ticket is assigned to a PO.
	    		</logic:equal>
	    	</td>
    	</tr>
</logic:present>

<tr>
	<td width = "100%" class = "labeleboldwhite">&nbsp;&nbsp;&nbsp;&nbsp;
	<bean:message bundle = "PRM" key = "prm.jobdashboard.jobdashboardfor"/>&nbsp;<bean:message bundle="PRM" key="prm.dashboard.appendix"/>&nbsp;<A href="AppendixHeader.do?function=view&appendixid=<bean:write name="JobDashboardForm" property="appendix_id"/>&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />&msaId=<bean:write name = "JobDashboardForm" property = "msa_id" />"><bean:write name="JobDashboardForm" property="appendix_name"/></A>
	&nbsp;->&nbsp;<bean:message bundle = "PRM" key = "prm.jobdashboard.job"/>&nbsp;<bean:write name = "JobDashboardForm" property = "job_name" />
	</td>
</tr>
<tr>
<td>
<!--  <table>-->
<table width="1200" border ="0">
	<tr>
		<td width=2> &nbsp;</td>
		<td width = "1190"> 
			<table border = "0" cellpadding=0 cellspacing=0 >
				<logic:present name = "copyactivityflag" scope = "request">
				  		<tr><td>&nbsp;</td>
						<td colspan = "5" class = "message" height = "30">
					    	<logic:equal name = "copyactivityflag" value = "0"><bean:message bundle = "pm" key = "activity.tabular.copyactivity"/>
					    <!-- 	<script>parent.ilexleft.location.reload();</script> -->
					    	</logic:equal>
					    </td></tr>
				</logic:present>
				<logic:present name = "NetMedX" scope = "request">
					<TR>
							<td><a href="javascript:toggleMenuSection('t1');"><img id="img_t1" src="images/Expand.gif" border="0" alt="+"  /></a></td>
				  		    
				  		  <TD class=trybtop align=middle height=20 width = "290"><bean:message bundle = "PRM" key = "prm.jobdashboard.site"/>       
								<logic:notEqual name="JobDashboardForm" property="jobTestIndicator" value = "None">
									&nbsp; (<bean:write name = "JobDashboardForm" property = "jobTestIndicator"/>)
								</logic:notEqual>
						  </TD>
				  		    
							<TD class=trybtop align=middle height=20 width = "300"><bean:message bundle = "PRM" key = "prm.jobdashboard.problem"/></TD>
							<TD class=trybtop align=middle height=20 width = "300"><bean:message bundle = "PRM" key = "prm.jobdashboard.schedule"/></TD>
							<TD class=trybtop align=middle height=20 width = "300"><bean:message bundle = "PRM" key = "prm.jobdashboard.financial"/></TD>
					</TR>
						  <tr id = "div_t1"  style="display: none" ><td >&nbsp;</td>
							<TD width=1020 colSpan=17>
							  <TABLE cellSpacing=0 cellPadding=0 width=1190 border=0>
								<TBODY>
								<TR>
								  <TD width= colSpan=17>
									<TABLE class=dbsummtable height="100%" cellSpacing=0 cellPadding=0 width=1190>
									  <TBODY>
									  <TR>
										<TD class=dbsummtable vAlign=top width=290>
										  <TABLE height="100%" width=290 border=0 valign="top">
											<TBODY>
											<TR>
												<TD class=dblabel vAlign=top><bean:message bundle = "PRM" key = "prm.netmedx.companyname"/>:</TD>
												<TD class=dbvaluesmall vAlign=top colspan="2"><bean:write name="JobDashboardForm" property="lo_ot_name"/></TD>
											</TR>
											<TR>
												<TD class=dblabel vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.requestor"/></TD>
												<TD class=dbvaluesmall vAlign=top colspan="2"><bean:write name = "JobDashboardForm" property = "job_netrequestor" /></TD>
											</TR>
										   	<TR>
												<TD class=dblabel vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.cnsinvoice"/></TD>
												<TD class=dbvaluesmall vAlign=top colspan="2"><bean:write name="JobDashboardForm" property = "job_invoiceno"/></TD>
											</TR>
										   
										   <TR>
												<TD class=dblabel vAlign=top ><bean:message bundle = "PRM" key = "prm.jobdashboard.siteinformation"/></TD>
												<TD class=dbvaluesmall vAlign=top colSpan=2>
													<logic:equal name = "JobDashboardForm" property = "jobtype" value = "Default">
													</logic:equal>
													<logic:notEqual name = "JobDashboardForm" property = "jobtype" value = "Default">
													<a id='ja_j1' href = "TicketDetailAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>&nettype=jobdashboard&ticket_id=<bean:write name="JobDashboardForm" property="job_netticketid"/>&appendixid=<bean:write name="JobDashboardForm" property = "appendix_id"/>&ticket_type=existingticket" onclick="Javascript:createCookie('ja_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.siteinformationedit"/></a>
													<a id='ja_j1' href = "SiteHistoryAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>&siteid=<bean:write name="JobDashboardForm" property="site_id"/>&from=netmedx&page=jobdashboard&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />&Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />" onclick="Javascript:createCookie('ja_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.siteinformationhistory"/></a>
													</logic:notEqual>
												</TD>
										   </TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top colspan="3">
													<bean:message bundle = "PRM" key = "prm.netmedx.name"/> / <bean:message bundle = "PRM" key = "prm.netmedx.number"/>:&nbsp;&nbsp;&nbsp;
													<bean:write name="JobDashboardForm" property="job_sitename"/> 
													<logic:notEqual name="JobDashboardForm" property="job_sitenumber" value = "" >
													/ <bean:write name="JobDashboardForm" property="job_sitenumber"/>  
													</logic:notEqual>
												</TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top colspan="3"><bean:message bundle = "PRM" key = "prm.netmedx.address"/>:&nbsp;&nbsp;&nbsp;<bean:write name = "JobDashboardForm" property = "job_siteaddress" />&nbsp;&nbsp;<bean:write name = "JobDashboardForm" property = "job_sitecity" />  <bean:write name = "JobDashboardForm" property = "job_sitestate" />  <bean:write name = "JobDashboardForm" property = "job_sitezipcode" /></TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top colspan="3"><bean:message bundle="PRM" key="prm.appendix.poc"/>&nbsp;
													<bean:define id = "sppoc" name = "JobDashboardForm" property = "job_sitepoc" type="java.lang.String" />
													<bean:define id="spemail" name = "JobDashboardForm" property="job_priemailid" type="java.lang.String" />
													<bean:define id="spphone" name="JobDashboardForm" property="job_sitephoneno" type="java.lang.String" />
														<% 	if(!(spphone.equals(""))) temp_comma = ",";
														 if(!(sppoc.equals(""))) {
															if((spemail.equals(""))) {%>
																<bean:write name="JobDashboardForm" property="job_sitepoc"/>
															<%}	else { %>
																<a href="mailto:<bean:write name="JobDashboardForm" property="job_priemailid"/>"><bean:write name="JobDashboardForm" property="job_sitepoc"/></a><%=temp_comma%>
														<% temp_comma = ""; } } %>
															<bean:write name="JobDashboardForm" property="job_sitephoneno"/>
												</TD>
											</TR>
											<tr>
														<td valign = "top"    colspan = "2" class = "dbvaluesmall"><bean:message bundle="PRM" key="prm.appendix.secpoc"/>&nbsp;
														<bean:define id="sspoc" name="JobDashboardForm" property="job_sitesecpoc" type="java.lang.String" />
														<bean:define id="ssemail" name="JobDashboardForm" property="job_sitesecemailid" type="java.lang.String" />
														<bean:define id="ssphone" name="JobDashboardForm" property="job_sitesecphone" type="java.lang.String" />
														<% if(!(ssphone.equals(""))) temp_comma = ",";
														 if(!(sspoc.equals(""))) {
															if((ssemail.equals(""))) { %>
																<bean:write name="JobDashboardForm" property="job_sitesecpoc"/>
															<% } else { %>
																<a href="mailto:<bean:write name="JobDashboardForm" property="job_sitesecemailid"/>"><bean:write name="JobDashboardForm" property="job_sitesecpoc"/></a><%=temp_comma%>
														<% } } %>
															<bean:write name="JobDashboardForm" property="job_sitesecphone"/>
														</td>
													</tr>
											<TR>
												<TD class=dblabel vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.specialconditions"/></TD>
												<TD class=dbvaluesmall vAlign=top colspan="2">
													<logic:equal name = "JobDashboardForm" property = "jobtype" value = "Default">
													</logic:equal>
													<logic:notEqual name = "JobDashboardForm" property = "jobtype" value = "Default">
														<a id='ja_j1' href = "TicketDetailAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>&nettype=jobdashboard&ticket_id=<bean:write name="JobDashboardForm" property="job_netticketid"/>&appendixid=<bean:write name="JobDashboardForm" property = "appendix_id"/>&ticket_type=existingticket" onclick="Javascript:createCookie('ja_j1');">[View]</a>
													</logic:notEqual>
												</TD>
											</TR>
											<TR><TD class=dbvaluesmall vAlign=top colspan = "3"><bean:write name = "JobDashboardForm" property = "job_netspecialcondition" /></TD></TR>
											<TR><TD class=dblabel vAlign=top colspan="3"><bean:message bundle = "PRM" key = "prm.jobdashboard.customerappendixinformation"/>&nbsp;<span class = "dbvaluesmall"><a id='ra_j1' href = "CustomerInformation.do?from=jobdashboard&function=add&typeid=<bean:write name="JobDashboardForm" property="jobid"/>&appendixid=<bean:write name="JobDashboardForm" property="appendix_id"/>&type=J" onclick="Javascript:createCookie('ra_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.customerappendixinformationedit"/></a></TD></TR>
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata1" value="">
											<tr>
												<td  valign = "top"  class = "dbvaluesmall" ><bean:write name = "JobDashboardForm" property = "custinfodata1" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left" class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue1" /></td>
											</tr>
											</logic:notEqual>
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata2" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall" ><bean:write name = "JobDashboardForm" property = "custinfodata2" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left" class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue2" /></td>
											</tr>
											</logic:notEqual>
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata3" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfodata3" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue3" /></td>
											</tr>
											</logic:notEqual>
							<!-- start -->				
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata4" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfodata4" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue4" /></td>
											</tr>
											</logic:notEqual>
											
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata5" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfodata5" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue5" /></td>
											</tr>
											</logic:notEqual>
											
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata6" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfodata6" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue6" /></td>
											</tr>
											</logic:notEqual>
											
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata7" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfodata7" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue7" /></td>
											</tr>
											</logic:notEqual>
											
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata8" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfodata8" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue8" /></td>
											</tr>
											</logic:notEqual>
											
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata9" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfodata9" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue9" /></td>
											</tr>
											</logic:notEqual>
											
											<logic:notEqual name = "JobDashboardForm" property = "custinfodata10" value="">
											<tr>
												<td valign = "top"  class = "dbvaluesmall" height = "20"><bean:write name = "JobDashboardForm" property = "custinfodata10" />&nbsp;</td>
												<td valign = "top" colspan = "2" align = "left"  class = "dbvaluesmall"><bean:write name = "JobDashboardForm" property = "custinfovalue10" /></td>
											</tr>
											</logic:notEqual>
											
							<!-- over -->
											<TR><TD class=dblabel vAlign=top colspan="3">Uplifts:&nbsp;<span class = "dbvaluesmall">[<a id='oa_j1' href="ManageUpliftAction.do?ref=activityUplift&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />&Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>">Edit</a>]</TD></TR>
											<tr>
												<td valign = "top"   class = "dblabel" width = "220">
													<bean:message bundle = "PRM" key = "prm.jobdashboard.documents"/>
													<logic:equal name = "documentpresent"  value = "true">
														<img name = "checkuploaded"  src = "images/Upload1.gif" width = "14" height = "11" border = "0" alt = '<bean:message bundle = "PRM" key = "prm.netmedx.uploadeddocuments"/>'/>
													</logic:equal>
												</td>
												<td valign = "top"  class = "dbvaluesmall" width = "80"><a id='oa_j1' href = "javascript:viewdocumentsjob(<bean:write name="JobDashboardForm" property="jobid"/>);" onclick="Javascript:createCookie('oa_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.documentsview"/></a>&nbsp;<a id='pa_j1' href = "javascript:uploadjob(<bean:write name="JobDashboardForm" property="jobid"/>);" onclick="Javascript:createCookie('pa_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.documentsedit"/></a></td>
											</tr>
											<TR>
												<TD class=dblabel vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.depotrequisition"/></TD>
												<TD class=dbvaluesmall vAlign=top>
													<logic:equal name = "JobDashboardForm" property = "jobtype" value = "Default">
													</logic:equal>
													<logic:notEqual name = "JobDashboardForm" property = "jobtype" value = "Default">
														<a id='ja_j1' href = "TicketDetailAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>&nettype=jobdashboard&ticket_id=<bean:write name="JobDashboardForm" property="job_netticketid"/>&appendixid=<bean:write name="JobDashboardForm" property = "appendix_id"/>&ticket_type=existingticket" onclick="Javascript:createCookie('ja_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.depotrequisitionview"/></a>
													</logic:notEqual>
												</TD>
											</TR>
										  </TBODY>
										 </TABLE>
										</TD>
										<TD class=dbsummtable vAlign=top width=300>
										  <TABLE height="100%" width=300 border=0 valign="top">
											<TBODY>
											<TR><TD class=dblabel vAlign=top colspan="3"><bean:message bundle = "PRM" key = "prm.jobdashboard.problemsummary"/></TD></TR>
											<TR><TD class=dbvaluesmall vAlign=top colspan="3"><bean:write name = "JobDashboardForm" property = "job_netproblemdesc" /></TD></TR>
										   <TR>
												<TD class=dblabel vAlign=top colSpan="2"><bean:message bundle = "PRM" key = "prm.jobdashboard.specialinstructions"/></TD>
												<TD class=dbvaluesmall vAlign=top>
													<logic:equal name = "JobDashboardForm" property = "jobtype" value = "Default"></logic:equal>
													<logic:notEqual name = "JobDashboardForm" property = "jobtype" value = "Default">
														<a id='ja_j1' href = "TicketDetailAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>&nettype=jobdashboard&ticket_id=<bean:write name="JobDashboardForm" property="job_netticketid"/>&appendixid=<bean:write name="JobDashboardForm" property = "appendix_id"/>&ticket_type=existingticket" onclick="Javascript:createCookie('ja_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.specialinstructionsview"/></a>
													</logic:notEqual>
												</TD>
										   </TR>
											<TR><TD class=dbvaluesmall vAlign=top colspan="3"><bean:write name = "JobDashboardForm" property = "job_netspecialinstruct" /></TD></TR>
											<TR>
												<TD class=dblabel vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.category"/></TD>
												<TD class=dbvaluesmall vAlign=top colspan="2"><bean:write name = "JobDashboardForm" property = "job_netcategory" /></TD>
											</TR>
											<TR>
												<TD class=dblabel vAlign=top colspan="2"><bean:message bundle = "PRM" key = "prm.jobdashboard.dispatchnotes/comments"/></TD>
												<TD class=dbvaluesmall vAlign=top><a id='ka_j1' href ="ViewInstallationNotes.do?from=jobdashboard&function=view&appendixid=<bean:write name="JobDashboardForm" property="appendix_id"/>&jobid=<bean:write name="JobDashboardForm" property="jobid"/>" onclick="Javascript:createCookie('ka_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.dispatchnotes/commentsview"/></a>&nbsp;<a id='la_j1' href = "AddInstallationNotes.do?from=jobdashboard&function=add&appendixid=<bean:write name="JobDashboardForm" property="appendix_id"/>&jobid=<bean:write name="JobDashboardForm" property="jobid"/>" onclick="Javascript:createCookie('la_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.dispatchnotes/commentsedit"/></a></TD>
											</TR>
											<logic:iterate id = "ilist" name = "JobDashboardForm" property = "installationlist"> 
												<TR><TD class=dbvaluesmall vAlign=top colspan="3"><bean:write name = "ilist" property = "job_netinstallation_date" />  <bean:write name = "ilist" property = "job_netinstallation_time" /></TD></TR>
												<TR><TD colspan="3"></TD></TR>
											</logic:iterate>
										  </TBODY>
										 </TABLE>
										</TD>
										<TD class=dbsummtable vAlign=top width=300>
										  <TABLE height="100%" width=300 border=0 valign="top">
											<TBODY>
											<TR><TD class=dblabel vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.ticketschedule"/></TD>
												<TD class=dbvaluesmall vAlign=top>
													<logic:equal name = "JobDashboardForm" property = "jobtype" value = "Default">
													</logic:equal>
													<logic:notEqual name = "JobDashboardForm" property = "jobtype" value = "Default">
														<a id='ja_j1' href = "TicketDetailAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>&nettype=jobdashboard&ticket_id=<bean:write name="JobDashboardForm" property="job_netticketid"/>&appendixid=<bean:write name="JobDashboardForm" property = "appendix_id"/>&ticket_type=existingticket" onclick="Javascript:createCookie('ja_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.ticketscheduleedit"/></a>
													</logic:notEqual>
												</TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.preferredarrivaltime"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name = "JobDashboardForm" property = "job_netpreferred_arrival_start_date" /> <bean:write name = "JobDashboardForm" property = "job_netpreferred_arrival_start_time" /></TD>
											</TR>
											<TR><TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.or"/></TD></TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.preferredarrivalwindow"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name = "JobDashboardForm" property = "job_netpreferred_arrival_window_start_date" /> <bean:write name = "JobDashboardForm" property = "job_netpreferred_arrival_window_start_time" /></TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.receivedDateAndTime"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name = "JobDashboardForm" property = "request_received_date" /><bean:write name = "JobDashboardForm" property = "request_received_time" />&nbsp;</TD>
											</TR>
											<!-- End : -->
											<TR>
												<TD class=dbvaluesmall vAlign=top>&nbsp;</TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name = "JobDashboardForm" property = "job_vpreferred_arrival_window_end_date" /> <bean:write name = "JobDashboardForm" property = "job_netpreferred_arrival_window_end_time" /></TD>
											</TR>
											<TR>
												<TD class=dblabel vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.dispatchschedule"/></TD>
												<TD class=dbvaluesmall vAlign=top>
													<%if( request.getAttribute( "NetMedX" ) != null ){ %>
														<a id='qa_j1' href="DispatchScheduleUpdate.do?function=update&typeid=<bean:write name="JobDashboardForm" property="jobid"/>&type=J&page=jobdashboard&pageid=<bean:write name="JobDashboardForm" property="appendix_id"/>" onclick="Javascript:createCookie('qa_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.dispatchscheduleedit"/></a>
													<%}else { %>
														<a id='qa_j1' href="ScheduleUpdate.do?function=update&typeid=<bean:write name="JobDashboardForm" property="jobid"/>&type=J&page=jobdashboard&pageid=<bean:write name="JobDashboardForm" property="appendix_id"/>" onclick="Javascript:createCookie('qa_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.dispatchscheduleedit"/></a>
													<% } %>
												</TD>
											</TR>
												<bean:define id="schedule_total" name = "JobDashboardForm" property = "schedule_total" />
												<!-- Change on 27-09-06 -->
												  <%  if(Integer.parseInt(""+schedule_total) > 0) { %> 
												   <TR>
														<TD class=dbvaluesmall vAlign=top colspan="2">Dispatch&nbsp;<bean:write name = "JobDashboardForm" property = "schedule_position" />&nbsp;of&nbsp;<bean:write name = "JobDashboardForm" property = "schedule_total" /></TD>
													</TR>
												   <% } %>		
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.scheduledarrival"/></font></TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name = "JobDashboardForm" property = "sitescheduleplanned_startdate" />&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleplanned_startdatetime" /></TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.actualarrivaltime"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name = "JobDashboardForm" property = "sitescheduleactual_startdate" />&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleactual_startdatetime" /></TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.departure"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name = "JobDashboardForm" property = "sitescheduleactual_enddate" />&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleactual_enddatetime" /></TD>
											</TR>
												<% if(Integer.parseInt(""+schedule_total) > 0) { %> 
												    <TR>
														<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.netmedx.totaltimeonsite"/>:</TD>
														<TD class=dbvaluesmallRightAlign vAlign=top><bean:write name = "JobDashboardForm" property = "totaltime_onsite" />&nbsp;<bean:message bundle = "PRM" key = "prm.netmedx.hours"/>&nbsp;&nbsp;</TD>
													</TR>
												  <% } %>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.timeonsite"/></TD>
												<TD class=dbvaluesmallRightAlign vAlign=top><bean:write name = "JobDashboardForm" property = "time_onsite" />&nbsp;<bean:message bundle = "PRM" key = "prm.jobdashboard.hours"/>&nbsp;&nbsp;&nbsp;</TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.timetoarrive"/></TD>
												<TD class=dbvaluesmallRightAlign vAlign=top><bean:write name = "JobDashboardForm" property = "time_toarrive" />&nbsp;<bean:message bundle = "PRM" key = "prm.jobdashboard.hours"/>&nbsp;&nbsp;&nbsp;</TD>
											</TR>
										  </TBODY>
										 </TABLE>
										</TD>
										<TD class=dbsummtable vAlign=top width=300>
										  <TABLE height="100%" width=300 border=0 valign="top">
											<TBODY>
											<TR >
												<TD class=dblabel vAlign=top colspan="3"><bean:message bundle = "PRM" key = "prm.jobdashboard.purchaseorder/workorderinformation"/></TD>
												<TD class=dbvaluesmall vAlign=top><a id='qa_j1' href = "POWOAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>" onclick="Javascript:createCookie('qa_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.purchaseorder/workorderinformationedit"/></a></TD>
											</TR>
										   <TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.name"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.contact"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.po/wo"/></TD>
												<TD class=dbvaluesmall vAlign=top nowrap><bean:message bundle = "PRM" key = "prm.jobdashboard.qcchecklist"/></TD>
										   </TR>
										   		<logic:iterate id="pwlist"  name="powolist" scope = "request">
													<tr >
														<td valign = "top"  class = "dbvaluesmall" ><bean:write name="pwlist" property="partnername"/> <html:hidden name="pwlist" property="partnerid"/></td>
														<td valign = "top"  class = "dbvaluesmall" ><bean:write name="pwlist" property="partnerpocname"/>&nbsp;<bean:write name="pwlist" property="partnerpocphone"/></td>
														<td valign = "top"  class = "dbvaluesmall">
														<bean:define id="ponum" name="pwlist" property="ponumber" type="java.lang.String"/>
														<bean:define id="poType" name="pwlist" property="powoType" type="java.lang.String"/>
															<%if(!(ponum.equals(""))) { %> 
															<a href = "javascript:viewpowo(<bean:write name="JobDashboardForm" property="jobid"/>, <bean:write name="pwlist" property="ponumber"/>, 'po');"><bean:write name="pwlist" property="ponumber"/></a>/<a href = "javascript:viewpowo(<bean:write name="JobDashboardForm" property="jobid"/>, <bean:write name="pwlist" property="ponumber"/>, 'wo');"><bean:write name="pwlist" property="ponumber"/></a>
															<%}%>
														</td>
																						
														<td valign = "top"  class = "dbvaluesmall">
														<%
														if(!(poType.equals("N"))) { %>
														<a id='ta_j<%= i %>' href="ChecklistTab.do?job_id=<bean:write name="JobDashboardForm" property="jobid"/>&page=jobdashboard&partnerid=<bean:write name="pwlist" property="partnerid"/>&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />" onclick="Javascript:createCookie('ta_j<%= i %>');"><bean:message bundle="PRM" key="prm.appendix.process"/></a></td>
														<%} %>
														</tr>
													</logic:iterate>
											<TR><TD class=dbvaluesmall vAlign=top colspan="4"><bean:message bundle = "PRM" key = "prm.jobdashboard.customerreference"/>  <bean:write name="JobDashboardForm" property="job_netcust_reference"/></TD></TR>
											<TR><TD class=dbvaluesmall vAlign=top colspan="4"><bean:message bundle = "PRM" key = "prm.jobdashboard.cost/pricesummary"/></TD></TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top colspan="2"><bean:message bundle = "PRM" key = "prm.jobdashboard.authorizedtotalcost"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name="JobDashboardForm" property = "job_netauthorizedtotalcost"/></TD>
												<TD class=dbvaluesmall vAlign=top>&nbsp;</TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top colspan="2"><bean:message bundle = "PRM" key = "prm.jobdashboard.actualprice"/></TD>
												<TD class=dbvaluesmall vAlign=top><bean:write name="JobDashboardForm" property = "job_netactualprice"/></TD>
												<TD class=dbvaluesmall vAlign=top>&nbsp;</TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top colspan="2"><bean:message bundle = "PRM" key = "prm.jobdashboard.actualvgp"/></TD>
												<TD class=dbvaluesmall vAlign=top colspan="2"><bean:write name="JobDashboardForm" property = "job_netactualvgp"/></TD>
											</TR>
											<TR height="30"><TD class=dbvaluesmall vAlign=top colspan="4">&nbsp;</TD></TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.updatedby"/></TD>
												<TD class=dbvaluesmall vAlign=top colspan="3"><bean:write name = "JobDashboardForm" property = "updatedby" />&nbsp;<bean:write name = "JobDashboardForm" property = "updateddate" />&nbsp;<bean:write name = "JobDashboardForm" property = "updatedtime" /></TD>
											</TR>
											<TR>
												<TD class=dbvaluesmall vAlign=top><bean:message bundle = "PRM" key = "prm.jobdashboard.owner"/></TD>
												<TD class=dbvaluesmall vAlign=top colspan="3"><bean:write name = "JobDashboardForm" property = "jobcreatedby" />&nbsp;<bean:write name = "JobDashboardForm" property = "createdate" />&nbsp;<bean:write name = "JobDashboardForm" property = "createtime" /></TD>
											</TR>
										  </TBODY>
										 </TABLE>
										</TD>
						</TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>
				</logic:present>
				<logic:notPresent name = "NetMedX" scope = "request">
				<tr>
					<td valign = "top"><a href="javascript:toggleMenuSection('1');"><img id="img_1" src="images/Expand.gif" border="0" alt="+"  /></a></td>
					<td valign = "top" height = "100%" >
						<table width = 220 border = "0" height = "100%" class="dbsummtable" cellpadding=0 cellspacing=0 >
							<tr><td   width=220 valign = "top" height = "20"  align="center" class = "trybtopclose" id="classchange1"><bean:message bundle = "PRM" key = "prm.jobdashboard.jobcost/pricesummary"/></td></tr>
							<tr id = "div_1" style="display: none">
								<td valign = "top" height = "100%" width=380 bgcolor="#FBD6B2">
									<table width = "220" height = "100%" border = "0" cellspacing=2 cellpadding=2>
										<tr >
											<td valign = "top">
												<table width = "220" cellpadding=0 cellspacing=0 border=0>
													
													<tr>
														<td width = 110 valign = "top"   class = "dblabel"><bean:message bundle = "PRM" key = "prm.jobdashboard.extendedprice"/></td>
														<td width = 110 valign = "top"  class = "dbvalueright">&nbsp;&nbsp;&nbsp;$<bean:write name = "JobDashboardForm" property = "job_extendedprice" /></td>
													</tr>
													<tr>
														<td valign = "top"   class = "dbvalue" colspan="2"><b><bean:message bundle = "PRM" key = "prm.jobdashboard.estimatedcosts"/></b>&nbsp;<bean:message bundle="PRM" key="prm.appendix.yourbudget"/>:</td>
													</tr>
													
													<tr>
														<td valign = "top"   class = "dbvalue">&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.jobdashboard.fieldlabor"/></td>
														<td valign = "top"  class = "dbvalueright">$<bean:write name = "JobDashboardForm" property = "job_fieldlaborcost" /></td>
													</tr>
													<tr>
														<td valign = "top"  class = "dbvalue">&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.jobdashboard.materials"/></td>
														<td valign = "top"  class = "dbvalueright">$<bean:write name = "JobDashboardForm" property = "job_materialcost" /></td>
													</tr>
													<tr>
														<td valign = "top"   class = "dbvalue">&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.jobdashboard.freight"/></td>
														<td valign = "top"  class = "dbvalueright">$<bean:write name = "JobDashboardForm" property = "job_freightcost" /></td>
													</tr>
													<tr>
														<td valign = "top"   class = "dbvalue">&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.jobdashboard.travel"/></td>
														<td valign = "top"  class = "dbvalueright">$<bean:write name = "JobDashboardForm" property = "job_travelcost" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvalue" >&nbsp;&nbsp;<b><bean:message bundle="PRM" key="prm.appendix.total"/></b></td>
														<td valign = "top" class = "dbvalueright">$<bean:write name="JobDashboardForm" property="job_estimatedcost" /></td>
													</tr>
													
													<tr>
														<td valign = "top"   class = "dblabel"><bean:message bundle="PRM" key="prm.appendix.proformavgpm"/></td>
														<td valign = "top"  class = "dbvalueright"><bean:write name = "JobDashboardForm" property = "job_proformamarginvgp" /></td>
													</tr>
													<tr><td colspan = "2"  height = "10"></td></tr>
													<% if( request.getAttribute("jobtype").equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ) ) {%>
													<%}else{ %>
													<tr>
														<td valign = "top"   class = "dblabel"><bean:message bundle = "PRM" key = "prm.jobdashboard.actualcosts"/></td>
														<td valign = "top"  class = "dbvalueright">$<bean:write name = "JobDashboardForm" property = "job_actualcost" /></td>
													</tr>
													<tr>
														<td valign = "top"   class = "dblabel"><bean:message bundle="PRM" key="prm.appendix.actualvgpm"/></td>
														<td valign = "top"  class = "dbvalueright"><bean:write name = "JobDashboardForm" property = "job_actualvgp" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvalue"><b><bean:message bundle="PRM" key="prm.appendix.actualvgp"/></b></td>
														<td valign = "top" class  = "dbvalueright" >$<bean:write name = "JobDashboardForm" property = "job_actualvgp_new" /></td>
													</tr>
													<%} %>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width=1> &nbsp;</td>
					<% if( request.getAttribute("jobtype").equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" )) {%>
					<%}else{ %>
					<td valign = "top" height = "100%" >
						<table width = "300" border = "0" height = "100%" class="dbsummtable" cellpadding=0 cellspacing=0>
							<tr>
							<td   width="300" valign = "top" height = "20"  align="center" class = "trybtopclose" id="classchange2"><bean:message bundle = "PRM" key = "prm.jobdashboard.sitedetails"/>
								<logic:notEqual name="JobDashboardForm" property="jobTestIndicator" value = "None">
									&nbsp; (<bean:write name = "JobDashboardForm" property = "jobTestIndicator"/>)
								</logic:notEqual>
							</td>
							
							</tr>
							<tr id = "div_11" style="display: none">
								<td valign = "top" height = "100%" width=380>
									<table width = "300" height = "100%" border = "0" cellspacing=2 cellpadding=2>
										<tr >
											<td valign = "top">
												<table width = "300" cellpadding=0 cellspacing=0 border=0>
													<tr>
														<td valign = "top"   class = "dblabel" width = "200"><bean:message bundle = "PRM" key = "prm.jobdashboard.jobstatus"/></td>
														<td valign = "top"  class = "dbvalue" width = "100"><bean:write name = "JobDashboardForm" property = "job_status" /></td>
													</tr>
													<tr><td valign = "top"  class = "dblabel" colspan = "2" width = "300"><bean:message bundle="PRM" key="prm.appendix.customerreference"/>&nbsp;<span class = "dbvaluewrap"><bean:write name="JobDashboardForm" property="custref"/></span>&nbsp;<span class = "dbvalue"><a id ='adiv_1'  href = "AddCustRef.do?appendixid=<bean:write name="JobDashboardForm" property="appendix_id"/>&typeid=<bean:write name="JobDashboardForm" property="jobid"/>&type=Job" onclick="Javascript:createCookie('ra_j1');"><bean:message bundle="PRM" key="prm.appendix.edit"/></a></span></td></tr>
													<tr>
														<td valign = "top" class = "dblabel"><bean:message bundle = "PRM" key = "prm.jobdashboard.cnsinvoice"/></td>
														<td valign = "top" class = "dbvalue"><bean:write name = "JobDashboardForm" property = "job_invoiceno" /></td>													
													</tr>
													<tr>
														<td valign = "top"  colspan = "2" class = "dblabel" width = "300">
															<bean:message bundle = "PRM" key = "prm.jobdashboard.siteinformation"/>&nbsp;<span class = "dbvalue">
																<a id='ja_j1' href = "SiteAction.do?ref=Add&Job_Id=<bean:write name="JobDashboardForm" property="jobid"/>&ref1=fromjobdashboard&Appendix_Id=<bean:write name="JobDashboardForm" property = "appendix_id"/>" onclick="Javascript:createCookie('ja_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.siteinformationedit"/></a>
																<!-- Start :Added By Amit For site History -->
															<a id='zb_j1' href="SiteHistoryAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>&siteid=<bean:write name="JobDashboardForm" property="site_id"/>&from=job&page=jobdashboard&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />&Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />" onclick="Javascript:createCookie('zb_j1');">[<bean:message bundle="PRM" key="prm.sitehistory.historyhyperlink"/>]</a>
														</td>
													</tr>
													<tr>
														<td valign = "top"  colspan = "2"  class = "dbvalue" width = "300">
															<logic:equal name = "JobDashboardForm" property = "job_sitenumber" value = "">
																<bean:message bundle = "PRM" key = "prm.netmedx.name"/>:&nbsp;&nbsp;&nbsp;
															</logic:equal>
															<logic:notEqual name = "JobDashboardForm" property = "job_sitenumber" value = "">
																<bean:message bundle = "PRM" key = "prm.netmedx.name"/> / <bean:message bundle = "PRM" key = "prm.netmedx.number"/>:&nbsp;&nbsp;
															</logic:notEqual>
															<bean:write name = "JobDashboardForm" property = "job_sitename" />
															<logic:notEqual name = "JobDashboardForm" property = "job_sitenumber" value = "">
																/ <bean:write name = "JobDashboardForm" property = "job_sitenumber" />
															</logic:notEqual>
														</td>
													</tr>
													<tr><td valign = "top"  colspan = "2"  class = "dbvalue" width = "300"><bean:message bundle = "PRM" key = "prm.netmedx.address"/>:&nbsp;&nbsp;<bean:write name = "JobDashboardForm" property = "job_siteaddress" /></td></tr>
													<tr><td valign = "top"  colspan = "2"  class = "dbvalue" width = "300"><bean:write name = "JobDashboardForm" property = "job_sitecity" />  <bean:write name = "JobDashboardForm" property = "job_sitestate" />  <bean:write name = "JobDashboardForm" property = "job_sitezipcode" /></td></tr>
													<tr>
														<td valign = "top"  colspan = "2"  class = "dbvalue" width = "300"><bean:message bundle="PRM" key="prm.appendix.poc"/>&nbsp;
															<bean:define id = "sppoc" name = "JobDashboardForm" property = "job_sitepoc" type="java.lang.String" />
															<bean:define id="spemail" name = "JobDashboardForm" property="job_priemailid" type="java.lang.String" />
															<%if(!(sppoc.equals(""))) {
																if((spemail.equals(""))) {%>
																	<bean:write name="JobDashboardForm" property="job_sitepoc"/>
																<%} else { %>
																	<a href="mailto:<bean:write name="JobDashboardForm" property="job_priemailid"/>"><bean:write name="JobDashboardForm" property="job_sitepoc"/></a>
															<% } } %>
															<bean:define id="spphone" name="JobDashboardForm" property="job_sitephoneno" type="java.lang.String" />														
															<%if(!(spphone.equals("")))	{%>
																,<bean:write name="JobDashboardForm" property="job_sitephoneno"/>
															<%} %>
														</td>							
													</tr>
													<tr>
														<td valign = "top"    colspan = "2" class = "dbvalue"><bean:message bundle="PRM" key="prm.appendix.secpoc"/>&nbsp;
														<bean:define id="sspoc" name="JobDashboardForm" property="job_sitesecpoc" type="java.lang.String" />
														<bean:define id="ssemail" name="JobDashboardForm" property="job_sitesecemailid" type="java.lang.String" />
														<%if(!(sspoc.equals(""))) {
															if((ssemail.equals(""))) {%>
																<bean:write name="JobDashboardForm" property="job_sitesecpoc"/>
															<%} else { %>
																<a href="mailto:<bean:write name="JobDashboardForm" property="job_sitesecemailid"/>"><bean:write name="JobDashboardForm" property="job_sitesecpoc"/></a>
														<%	} } %>
														<bean:define id="ssphone" name="JobDashboardForm" property="job_sitesecphone" type="java.lang.String" />														
														<%if(!(ssphone.equals(""))) {%>
															,<bean:write name="JobDashboardForm" property="job_sitesecphone"/>
														<%} %> 
														</td>
													</tr>
													<tr><td colspan = "2" height= "10" width = "300">&nbsp;</td></tr>
													<tr>
														<td width = 220 valign = "top"   class = "dbvalue"><b><bean:message bundle = "PRM" key = "prm.jobdashboard.installationnotes/comments"/></td>
														<td width = 80 valign = "top"  class = "dbvalue"><a id='ka_j1' href ="ViewInstallationNotes.do?from=jobdashboard&function=view&appendixid=<bean:write name="JobDashboardForm" property="appendix_id"/>&jobid=<bean:write name="JobDashboardForm" property="jobid"/>" onclick="Javascript:createCookie('ka_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.installationnotes/commentsview"/></a>&nbsp;<a id='la_j1' href = "AddInstallationNotes.do?from=jobdashboard&function=add&appendixid=<bean:write name="JobDashboardForm" property="appendix_id"/>&jobid=<bean:write name="JobDashboardForm" property="jobid"/>" onclick="Javascript:createCookie('la_j1');"><bean:message bundle="PRM" key="prm.jobdashboard.installationnotes/commentsedit"/></a></td>
													</tr>
													<logic:iterate id = "ilist" name = "JobDashboardForm" property = "installationlist"> 
													<tr><td class=dbvaluesmall vAlign=top colspan="2"><bean:write name = "ilist" property = "job_netinstallation_date" />  <bean:write name = "ilist" property = "job_netinstallation_time" /></td></tr>
													<tr><td colspan="2" height="10"></td></tr>
													</logic:iterate>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width=1> &nbsp;</td>
					
					<td valign = "top" height = "100%" >
						<table width = 400 border = "0" height = "100%" class="dbsummtable" cellpadding=0 cellspacing=0>
							<tr>
								<td   width=400 valign = "top" height = "20"  align="center" class = "trybtopclose" id="classchange4"><bean:message bundle = "PRM" key = "prm.jobdashboard.purchaseorder/workorderinformation"/></td>
							</tr>
							<tr id = "div_13" style="display: none">
								<td valign = "top" height = "100%" width=300>
									<table width = "400" height = "100%" border = "0" cellspacing=2 cellpadding=2>
										<tr>
											<td valign = "top">
												<table width = "400" cellpadding=0 cellspacing=0 border=0>
													<tr name="backtotopJobDBPOWO">
														<td valign = "top"  colspan = "4" class = "dblabel" width = "400"><bean:message bundle = "PRM" key = "prm.jobdashboard.purchaseorder/workorderinformation"/> <span class = "dbvalue"><a id='qa_j1' href = "POWOAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>" onclick="Javascript:createCookie('qa_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.purchaseorder/workorderinformationedit"/></a></td>
													</tr>
													<tr><td valign = "top"  colspan = "4" class = "dblabel" width = "400">&nbsp;</td></tr>
													<tr>
														<td width=100 valign = "top"  class = "dbvaluecenter" >	<bean:message bundle = "PRM" key = "prm.jobdashboard.name"/></td>
														<td  width=150 valign = "top"  colspan = "2" class = "dbvaluecenter" align = "center"><bean:message bundle = "PRM" key = "prm.jobdashboard.contact"/></td>
														<td width=50 valign = "top"  class = "dbvaluecenter"><bean:message bundle = "PRM" key = "prm.jobdashboard.po/wo"/></td>
														<td width=100 valign = "top"  class = "dbvaluecenter"><bean:message bundle = "PRM" key = "prm.jobdashboard.qcchecklist"/></td>
													</tr>
													<logic:iterate id="pwlist"  name="powolist" scope = "request">
													<tr id="accountPowo">
														<td valign = "top"  class = "dbvalue" ><bean:write name="pwlist" property="partnername"/> <html:hidden name="pwlist" property="partnerid"/></td>
														<td valign = "top"  class = "dbvalue" colspan = "2"><bean:write name="pwlist" property="partnerpocname"/>&nbsp;<bean:write name="pwlist" property="partnerpocphone"/></td>
														<td valign = "top"  class = "dbvalue">
														<bean:define id="ponum" name="pwlist" property="ponumber" type="java.lang.String"/>
														<bean:define id="poType" name="pwlist" property="powoType" type="java.lang.String"/>
															<%if(!(ponum.equals(""))) { %>
															<a href = "javascript:viewpowo(<bean:write name="JobDashboardForm" property="jobid"/>, <bean:write name="pwlist" property="ponumber"/>, 'po');"><bean:write name="pwlist" property="ponumber"/></a>/<a href = "javascript:viewpowo(<bean:write name="JobDashboardForm" property="jobid"/>, <bean:write name="pwlist" property="ponumber"/>, 'wo');"><bean:write name="pwlist" property="ponumber"/></a>
															<%}%>
														</td>
														<td valign = "top"  class = "dbvalue">
														<%
														if(!(poType.equals("N"))) { %>
														<a id='ta_j<%= i %>' href="ChecklistTab.do?job_id=<bean:write name="JobDashboardForm" property="jobid"/>&page=jobdashboard&partnerid=<bean:write name="pwlist" property="partnerid"/>&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />" onclick="Javascript:createCookie('ta_j<%= i %>');"><bean:message bundle="PRM" key="prm.appendix.process"/></a></td>
														<%} %>
													</tr>
													</logic:iterate>
													<TR>
														<TD align="left"  onclick=showaccountbackPOWO() id="accountbackPOWO" class="dbnavigate" colspan = "3"><a href="#backtotopJobDBPOWO">&lt;&lt;<bean:message bundle="PRM" key="prm.jobdashboard.back"/></a></TD>
														<TD align="right" onclick=showaccountmorePOWO() id="accountmorePOWO" class="dbnavigate"><a href="#backtotopJobDBPOWO"><bean:message bundle="PRM" key="prm.jobdashboard.more"/>&gt;&gt;</a></TD>
													</TR>			
													<!-- End -->
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width=1> &nbsp;</td>
					<td valign = "top" height = "100%" >
						<table width = 350 border = "0" height = "100%" class="dbsummtable" cellpadding=0 cellspacing=0>
							<tr><td   width=250 valign = "top" height = "20"  align="center" class = "trybtopclose" id="classchange3">Site Schedule</td></tr>
							<tr id = "div_14" style="display: none">
								<td valign = "top" height = "100%" width=350>
									<table width = "350" height = "100%" border = "0" cellspacing=2 cellpadding=2>
										<tr >
											<td valign = "top">
												<table width = "350" cellpadding=0 cellspacing=0 border=0>
													<tr><td valign = "top"  border = "0" colspan = "3"  class = "dblabel"><bean:message bundle = "PRM" key = "prm.jobdashboard.siteschedule"/>&nbsp;<span class = "dbvalue"><a id='qa_j1' href="ScheduleUpdate.do?function=update&typeid=<bean:write name="JobDashboardForm" property="jobid"/>&type=J&page=jobdashboard&pageid=<bean:write name="JobDashboardForm" property="appendix_id"/>" onclick="Javascript:createCookie('qa_j1');"><bean:message bundle = "PRM" key = "prm.jobdashboard.sitescheduleedit"/></a></td></tr>
													<tr>
														<td width=100 valign = "top" class = "dbvalue" >&nbsp;</td>
														<td width=125 valign = "top"  class = "dbvaluecenter" ><bean:message bundle = "PRM" key = "prm.jobdashboard.siteschedulestart"/></td>
														<td width=125 valign = "top"  class = "dbvaluecenter"><bean:message bundle = "PRM" key = "prm.jobdashboard.sitescheduleend"/></td>
													</tr>
													<tr>
														<bean:define id="count" name="JobDashboardForm" property="jobRseCount" type="java.lang.String"/>
														<%if(Integer.parseInt(count) > 0) {%>
														<td valign = "top" class = "dbvalue">Scheduled&nbsp;<font color="red" style="font-weight: bold">(R<bean:write name = "JobDashboardForm" property = "jobRseCount" />)</font>:</td>
														<%}else{ %>
														<td valign = "top" class = "dbvalue"><bean:message bundle = "PRM" key = "prm.jobdashboard.scheduled"/></td>
														<%} %>
														<td valign = "top"  class = "dbvalue">&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleplanned_startdate" />&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleplanned_startdatetime" /></td>
														<td valign = "top"  class = "dbvalue">&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleplanned_enddate" />&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleplanned_enddatetime" /></td>
													</tr>
													<tr>
														<td valign = "top" class = "dbvalue" ><bean:message bundle = "PRM" key = "prm.jobdashboard.Actual"/></td>
														<td valign = "top"  class = "dbvalue">&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleactual_startdate" />&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleactual_startdatetime" /></td>
														<td valign = "top"  class = "dbvalue">&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleactual_enddate" />&nbsp;<bean:write name = "JobDashboardForm" property = "sitescheduleactual_enddatetime" /></td>
													</tr>
													<tr><td valign = "top" colspan = "3" class = "dblabel" ><bean:message bundle = "PRM" key = "prm.jobdashboard.customerappendixinformation"/>&nbsp;<span class = "dbvalue"><a id='ra_j1' href = "CustomerInformation.do?from=jobdashboard&function=add&typeid=<bean:write name="JobDashboardForm" property="jobid"/>&appendixid=<bean:write name="JobDashboardForm" property="appendix_id"/>&type=J" onclick="Javascript:createCookie('ra_j1');">[Edit]</a></td></tr>
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata1" value="">
													<tr>
														<td  valign = "top"  class = "dbvalue" ><bean:write name = "JobDashboardForm" property = "custinfodata1" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left" class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue1" /></td>
													</tr>
													</logic:notEqual>
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata2" value="">
													<tr>
														<td valign = "top"  class = "dbvalue" ><bean:write name = "JobDashboardForm" property = "custinfodata2" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left" class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue2" /></td>
													</tr>
													</logic:notEqual>
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata3" value="">
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfodata3" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue3" /></td>
													</tr>
													</logic:notEqual>
												<!-- start -->	
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata4" value="">
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfodata4" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue4" /></td>
													</tr>
													</logic:notEqual>
											
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata5" value="">
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfodata5" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue5" /></td>
													</tr>
													</logic:notEqual>
											
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata6" value="">
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfodata6" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue6" /></td>
													</tr>
													</logic:notEqual>
											
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata7" value="">
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfodata7" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue7" /></td>
													</tr>
													</logic:notEqual>
											
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata8" value="">
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfodata8" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue8" /></td>
													</tr>
													</logic:notEqual>
											
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata9" value="">
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfodata9" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue9" /></td>
													</tr>
													</logic:notEqual>
											
													<logic:notEqual name = "JobDashboardForm" property = "custinfodata10" value="">
													<tr>
														<td valign = "top"  class = "dbvalue" height = "20"><bean:write name = "JobDashboardForm" property = "custinfodata10" />&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "custinfovalue10" /></td>
													</tr>
													</logic:notEqual>
												<!-- over  -->
													<TR>
														<TD class=dblabel vAlign=top colspan="3">Uplifts:&nbsp;<span class = "dbvalue">[<a id='oa_j1' href="ManageUpliftAction.do?ref=activityUplift&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />&Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>">Edit</a>]</span></TD>
													</TR>
													<TR>
														<TD class=dblabel vAlign=top colspan="3">Team:&nbsp;<span class = "dbvalue">[<a id='oa_j1' href="AssignOwner.do?jobid=<bean:write name = "JobDashboardForm" property = "jobid" />&currentOwner=<bean:write name = "JobDashboardForm" property = "jobcreatedby" />&appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>&fromPage=jobdashboard">Edit</a>]</span></TD>
													</TR>
													<tr>
														<td valign = "top"  class = "dbvalue"></td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"></td>
													</tr>
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:message bundle = "PRM" key = "prm.jobdashboard.owner"/> &nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "jobcreatedby" /></td>
													</tr>
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:message bundle = "PRM" key = "prm.jobdashboard.scheduler"/>&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "jobScheduler" /></td>
													</tr>
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:message bundle = "PRM" key = "prm.jobdashboard.updatedby"/> &nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue">	<bean:write name = "JobDashboardForm" property = "updatedby" />&nbsp;<bean:write name = "JobDashboardForm" property = "updateddate" />&nbsp;<bean:write name = "JobDashboardForm" property = "updatedtime" /></td>
													</tr>
													<tr>
														<td valign = "top"  class = "dbvalue"><bean:message bundle = "PRM" key = "prm.jobdashboard.created"/>&nbsp;</td>
														<td valign = "top" colspan = "2" align = "left"  class = "dbvalue"><bean:write name = "JobDashboardForm" property = "createdate" />&nbsp;<bean:write name = "JobDashboardForm" property = "createtime" /></td>
													</tr>
													
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<%} %>
				</tr>
				</logic:notPresent>
			</table>
		</td>
	</tr>
</table>
<logic:present name = "NetMedX" scope = "request">

 <TABLE class=dbsummtable3 cellSpacing=1 cellPadding=0 width=1250 border=0>
<!--<TABLE> -->
	<TBODY>
				<TR> 
				<TD width=2>&nbsp;</TD>
					  <TD width=20 >&nbsp;</TD>	  
			                <TD width=290 class=trybtopbutton align = "left">
								<html:button property="sort" styleClass="button" onclick = "javascript:createCookie('sort');return sortwindow();">
									<bean:message bundle="PRM" key="prm.netmedx.sort"/>
								</html:button>
			                	<html:submit property="optactivity" styleClass="button" onclick = "javascript:createCookie('optactivity');">
									Add Out of Scope Activity
								</html:submit>
			                	<logic:present name = "JobDashboardForm" property="inv_type">
									<html:button property="back" styleClass="button" onclick = "return Backaction();">
										<bean:message bundle="PRM" key="prm.button.back"/>
									</html:button>
								</logic:present>
								<logic:equal name = "JobDashboardForm" property = "nettype" value = "dispatch">
									<html:button property="back" styleClass="button" onclick = "return Backaction();">
										<bean:message bundle="PRM" key="prm.button.back"/>
									</html:button>
								</logic:equal>		
			                </TD>
                			<TD width=430 class=trybtop height=20 align="right"><bean:message bundle="PRM" key="prm.jobdashboard.activitiessummary"/></TD>
                			 <TD width=428 >&nbsp;</TD></TR>
			<TR><TD width = 2>&nbsp;</TD>
          	<TD width=1328 colspan =6>
				<table width=1308 cellspacing=1 cellpadding=0 class="dbsummtable3" border=0 >
              		<TBODY>
              	        <TR>
                			<TD width=19 rowSpan=2>&nbsp;</TD>
			                <TD width=199 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.activity"/></TD>
			                <TD width=69 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.qty"/></TD>
			                <TD width=109 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.type"/></TD>
			                <TD width=49 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.scope"/></TD>
							<%-- if(!(request.getAttribute("jobtype").equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ))) { %>	
			                <TD width=70 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.upliftcosts"/><br>[<a href="ManageUpliftAction.do?ref=activityUplift&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />&Appendix_Id=<bean:write name="JobDashboardForm" property="appendix_id"/>">Edit</a>]</TD>
			                <% } --%>
			                <TD width=138 class=tryb colSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.estimatedcosts"/></TD>
			                <TD width=69 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.extendedunitprice"/></TD>
			                <TD width=69 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.extendedtotalprice"/></TD>
			                <TD width=49 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.proformamargin"/></TD>
			                <TD width=108 rowSpan=2>&nbsp;</td>
							<!-- <TD width=108 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.status"/></TD> -->
							<TD width=400 >&nbsp;</TD>
	      			   </TR>
		               <TR>
			                <TD width=70 class=tryb><DIV><bean:message bundle="PRM" key="prm.jobdashboard.unit"/></DIV></TD>
			                <TD width=70 class=tryb><DIV><bean:message bundle="PRM" key="prm.jobdashboard.total"/></DIV></TD>
			           </TR>
             		  <logic:iterate id = "actlist" name = "actlist">
             			<%	if ( csschooser == true ) {
								csschooser = false; bgcolor = "dbvalueodd"; bgcolornumber = "numberodd"; bghyperlink = "textleftodd"; textclass = "texte";
							} else {
								csschooser = true; bgcolor = "dbvalueeven"; bgcolornumber = "numbereven"; bghyperlink = "textlefteven"; textclass = "texto";
							}
						%>
             		<bean:define id = "activity_lx_ce_type" name = "actlist" property = "activity_lx_ce_type" type = "java.lang.String" />
             		<bean:define id = "act_id" name = "actlist" property = "activity_id" type = "java.lang.String" />
             		<bean:define id = "actitype" name = "actlist" property = "type" type = "java.lang.String" />
             		   <% 
					  		actid_str=act_id;
							hyperlinkforeditquantity = "ActivityUpdateAction.do?ref=inscopeactivity&from=jobdashboard&Activity_Id="+act_id;
        		   			if( activity_lx_ce_type.equals( "A" ) ) {
								hyperlinkforactivity = "ResourceListAction.do?resourceListType=A&type=PM&from=jobdashboard&ref=detailactivity&Activity_Id="+act_id+"&Activitylibrary_Id="+act_id;
								refvar = "View";
							} if( activity_lx_ce_type.equals( "IA" ) ) {
								hyperlinkforactivity = "ResourceListAction.do?resourceListType=A&type=PM&from=jobdashboard&ref=Viewinscope&Activity_Id="+act_id+"&Activitylibrary_Id="+act_id;
								refvar = "Viewinscope";
							} if( activity_lx_ce_type.equals( "AA" ) ) {
								hyperlinkforactivity = "ResourceListAction.do?resourceListType=A&type=PM&from=jobdashboard&ref=detailaddendumactivity&Activity_Id="+act_id+"&Activitylibrary_Id="+act_id;
								refvar = "ViewAddendum";
							} if( activity_lx_ce_type.equals( "CA" ) ) {
								hyperlinkforactivity = "ResourceListAction.do?resourceListType=A&type=PM&from=jobdashboard&ref=detailchangeorderactivity&Activity_Id="+act_id+"&Activitylibrary_Id="+act_id;
								refvar = "ViewChangeorder";
							}
             		   %>
             		   <TR>
	             		    <%-- if(!(request.getAttribute("jobtype").equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ))) { %>	
	             		    <td colspan = 12 width=1240>
							<table width=1250 cellspacing=0 cellpadding=0 class="dbsummtable3"  border=0>
	             		    <% } else { --%>
							<td colspan = 12 width=1320>
							<table width=1320 cellspacing=0 cellpadding=0 class="dbsummtable3"  border=0>
							<%-- } --%>
							 <tr>
	                			<td width = 20>
									<a href="javascript:toggleMenuSection( 'j<%= act_id %>' );"><img id="img_j<%= act_id %>" src="images/Expand.gif" border="0" alt="+"  /></a>
								</td>
				                <TD width = 200 class="<%= bghyperlink %>">
					                <A id='ua_j<%= act_id %>' href = "<%= hyperlinkforactivity %>&jobid=<bean:write name = "JobDashboardForm" property = "jobid" />" onclick="Javascript:createCookie('ua_j<%= act_id %>');"><bean:write name = "actlist" property = "activity_name" /></A>
					                &nbsp;<logic:equal name = "actlist" property = "activity_oos_flag" value = "Y"><a href="EditOOSActivityAction.do?Activity_Id=<%= act_id %>&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />&Appendix_Id=<bean:write name = "JobDashboardForm" property = "appendix_id" />">Edit</a></logic:equal>	
				                </TD>
				                <TD width = 70 class="<%= bgcolornumber %>">
				                	<bean:write name = "actlist" property = "quantity" />
				                	<!-- Commented the check part for NetMedx from bellow if block that was :- (&& request.getAttribute( "NetMedX" ) == null)-->
			                	<% if( !actitype.equals( "Overhead" ) ) {%>
			                	<a id='fa_j<%= act_id %>' href = "<%= hyperlinkforeditquantity %>&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />"  onclick="Javascript:createCookie('fa_j<%= act_id %>');">Edit</a>
			               		<% } else { %>
			               		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			               		<% } %> 
			                </TD>
			                <TD width = 100 class="<%= bgcolor %>"><bean:write name = "actlist" property = "type" /></TD>
			                <TD width = 50 class="<%= bgcolor %>"><bean:write name = "actlist" property = "scope" /></TD>
			                <%-- if(!(request.getAttribute("jobtype").equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ))) { %>	
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "atUpliftCost" /></TD>
			                <% } --%>
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "estimatedunitcost" /></TD>
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "estimatedtotalcost" /></TD>
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "extendedunitprice" /></TD>
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "extendedtotalprice" /></TD>
			                <TD width = 50 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "margin" /></TD>
			                <TD width = 79>&nbsp;</td>
                			<!-- <TD width = 109 class = "<%= bgcolor %>"><A id='va_j<%= act_id %>' href="<%= hyperlinkforactivity %>&jobid=<bean:write name = "JobDashboardForm" property = "jobid" />" onclick="Javascript:createCookie('va_j<%= act_id %>');"><bean:write name = "actlist" property = "status" />1111</A></TD> -->
                			<TD width=400 >&nbsp;</TD>
              		   </TR>
              		  <% actid_str="javascript:createCookie('addrers');return adddResource("+act_id+",'"+refvar+"')"; %>
              		   <tr id = "div_j<%= act_id %>"  style="display: none" ><TD>&nbsp; </TD>
                		<TD colSpan=11>
						<TABLE width=1220 class=dbsummtable1job  height="100%" cellSpacing=0 cellPadding=0  border=0>
							<TBODY>
				  			<TR width=1220 ><td colspan=16>
				  			<TABLE width=1220 class=dbsummtable1job  height="100%" cellSpacing=0 cellPadding=0  border=0 >
				  			<tr>
				  			<td width = 10 >&nbsp;</td>
				    			<% 
									if( request.getAttribute( "NetMedX" ) != null ) {
										if( request.getAttribute("jobtype").equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ) )
										{ }	else { %>
										    <TD width = 125 class = trybtopbutton align = "left">
											<html:button property = "addResource" styleClass = "button" onclick = "<%=actid_str%>">
												<bean:message bundle="PRM" key="prm.jobdashboard.addResource"/>
											</html:button>
											 </td>
										<% } } else	{
				    					if( request.getAttribute( "jobtype" ).equals( "Default" ) || request.getAttribute("jobtype").equals( "Addendum" ) )
										{ }	else { %>
												<logic:equal name = "actlist" property = "activity_oos_flag" value = "Y">
													<TD width = 125 class = trybtopbutton align = "left">
														<html:button property="addResource" styleClass="button" onclick = "<%=actid_str%>">
															<bean:message bundle="PRM" key="prm.jobdashboard.addResource"/>
														</html:button>
													</td>
												</logic:equal>
												<logic:notEqual name = "actlist" property = "activity_oos_flag" value = "Y">
												</logic:notEqual>
										<% } } %>
					    		<td width=705 class=trybtopsub  height=20><bean:message bundle="PRM" key="prm.jobdashboard.resourcessummary"/></TD> 
					    		<td width=1300 class=trybtopsub  height=20>&nbsp;</TD> 
				  			</tr>
				  			</table>
				  			</TD>
				  			</TR>
							  <TR width=1220 ><td colspan=16>
				  			<TABLE width=1220 class=dbsummtable1job  height="100%" cellSpacing=0 cellPadding=0  border=0 >
				  			<tr><td width = 10 >&nbsp;</td>
							     <TD width = 80 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.cnspartnumber"/></TD>
							     <TD width = 150 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.resourcename"/></TD>
							     <TD width = 120 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.qty"/></TD>
							     <TD width = 100 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.type"/></TD>
							     <TD width = 200 class=tryb colSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.source"/></TD>
							     <TD width = 200 class=tryb colSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.partner"/></TD>
							     <TD width = 140 class=tryb colSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.estimatedcosts"/></TD>
							     <TD width = 70 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.extendedunitprice"/></TD>
							     <TD width = 70 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.extendedtotalprice"/></TD>
							     <TD width = 50 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.proformamargin"/></TD>
							     <TD width = 70 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.dropshipped"/></TD>
							   </TR>
							   <TR> <td colspan = "2"> &nbsp;</td>
								<TD class=tryb width = 90><DIV><bean:message bundle="PRM" key="prm.jobdashboard.sourcename"/></DIV></TD>
								<TD class=tryb width = 110><DIV><bean:message bundle="PRM" key="prm.jobdashboard.partno"/></DIV></TD>
								<TD class=tryb width = 120><DIV><bean:message bundle="PRM" key="prm.jobdashboard.partnername"/></DIV></TD>
								<TD class=tryb width = 80><DIV><bean:message bundle="PRM" key="prm.jobdashboard.partnerassign"/></DIV></TD>
								<TD class=tryb width = 70><DIV><bean:message bundle="PRM" key="prm.jobdashboard.unit"/></DIV></TD>
								<TD class=tryb width = 70><DIV><bean:message bundle="PRM" key="prm.jobdashboard.total"/></DIV></TD>
							  </TR>
						<logic:iterate id = "rlist" name = "actlist" property = "resourcelist">
							  <%	if ( cssinnerchooser == true ) {
										cssinnerchooser = false; bginnercolor = "dbvalueodd"; bginnercolornumber = "numberodd"; bginnerhyperlink = "textleftodd";
									}else {
										cssinnerchooser = true; bginnercolor = "dbvalueeven"; bginnercolornumber = "numbereven"; bginnerhyperlink = "textlefteven";
							 } %>
							  <bean:define id = "res_lx_ce_type" name = "rlist" property = "resource_lx_ce_type" type = "java.lang.String" />
							  <bean:define id = "res_id" name = "rlist" property = "resource_id" type = "java.lang.String" />
							  <% if(  res_lx_ce_type.equals( "M" ) ) {
								  	hyperlinkforresource = "MaterialDetailAction.do?from=jobdashboard_resource&ref=View&Material_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if( res_lx_ce_type.equals( "IM" ) ) {
									hyperlinkforresource = "MaterialDetailAction.do?from=jobdashboard_resource&ref=Viewinscope&Material_Id="+res_id+"&Activity_Id="+act_id;
									hyperlinkfornetmedxresource = "ResourceListAction.do?resourceListType=M&from=jobdashboard_resource&ref=Viewinscope&Activity_Id="+act_id;
								 } if( res_lx_ce_type.equals( "AM" ) ) {
									hyperlinkforresource = "MaterialDetailAction.do?from=jobdashboard_resource&ref=ViewAddendum&Material_Id="+res_id+"&Activity_Id="+act_id;
								 } if( res_lx_ce_type.equals( "CM" ) ) {
									hyperlinkforresource = "MaterialDetailAction.do?from=jobdashboard_resource&ref=ViewChangeorder&Material_Id="+res_id+"&Activity_Id="+act_id;
								 } if(  res_lx_ce_type.equals( "L" ) ) {
								  	hyperlinkforresource = "LaborDetailAction.do?from=jobdashboard_resource&ref=View&Labor_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if(  res_lx_ce_type.equals( "IL" ) ) {
								  	hyperlinkforresource = "LaborDetailAction.do?from=jobdashboard_resource&ref=Viewinscope&Labor_Id="+res_id+"&Activity_Id="+act_id;
									hyperlinkfornetmedxresource = "ResourceListAction.do?resourceListType=L&from=jobdashboard_resource&ref=Viewinscope&Activity_Id="+act_id;
								 } if(  res_lx_ce_type.equals( "CL" ) ) {
								  	hyperlinkforresource = "LaborDetailAction.do?from=jobdashboard_resource&ref=ViewChangeorder&Labor_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if(  res_lx_ce_type.equals( "AL" ) ) {
								  	hyperlinkforresource = "LaborDetailAction.do?from=jobdashboard_resource&ref=ViewAddendum&Labor_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if(  res_lx_ce_type.equals( "F" ) ) {
								  	hyperlinkforresource = "FreightDetailAction.do?from=jobdashboard_resource&ref=View&Freight_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if(  res_lx_ce_type.equals( "IF" ) ) {
								  	hyperlinkforresource = "FreightDetailAction.do?from=jobdashboard_resource&ref=Viewinscope&Freight_Id="+res_id+"&Activity_Id="+act_id;
									hyperlinkfornetmedxresource = "ResourceListAction.do?resourceListType=F&from=jobdashboard_resource&ref=Viewinscope&Activity_Id="+act_id;
								 } if(  res_lx_ce_type.equals( "CF" ) ) {
								  	hyperlinkforresource = "FreightDetailAction.do?from=jobdashboard_resource&ref=ViewChangeorder&Freight_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if(  res_lx_ce_type.equals( "AF" ) ) {
								  	hyperlinkforresource = "FreightDetailAction.do?from=jobdashboard_resource&ref=ViewAddendum&Freight_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if(  res_lx_ce_type.equals( "T" ) ) {
								  	hyperlinkforresource = "TravelDetailAction.do?from=jobdashboard_resource&ref=View&Travel_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if(  res_lx_ce_type.equals( "IT" ) ) {
								  	hyperlinkforresource = "TravelDetailAction.do?from=jobdashboard_resource&ref=Viewinscope&Travel_Id="+res_id+"&Activity_Id="+act_id;
									hyperlinkfornetmedxresource = "ResourceListAction.do?resourceListType=T&from=jobdashboard_resource&ref=Viewinscope&Activity_Id="+act_id;
								 } if(  res_lx_ce_type.equals( "CT" ) ) {
								  	hyperlinkforresource = "TravelDetailAction.do?from=jobdashboard_resource&ref=ViewChangeorder&Travel_Id="+res_id+"&Activity_Id="+act_id;
							  	 } if(  res_lx_ce_type.equals( "AT" ) ) {
								  	hyperlinkforresource = "TravelDetailAction.do?from=jobdashboard_resource&ref=ViewAddendum&Travel_Id="+res_id+"&Activity_Id="+act_id;
							  	 }
							  %>
							  <TR height = "20"><td width = 20 > &nbsp;</td>
								 <TD class="<%= bginnercolornumber %>"><bean:write name = "rlist" property = "resource_cnspartnumber" /></TD>
								 <TD class="<%= bginnerhyperlink %>"><A id='wa_j<%= res_id %>' href = "<%= hyperlinkforresource %>&jobid=<bean:write name = "JobDashboardForm" property = "jobid" />" onclick="Javascript:createCookie('wa_j<%= res_id %>');"><bean:write name = "rlist" property = "resource_name" /></A> 
								 </TD>
								 <TD class="<%= bginnercolornumber %>">
								 	<bean:write name = "rlist" property = "resource_qty" />
								 	<logic:present name = "NetMedX" scope = "request"><a id='oa_j<%= res_id %>' href = "<%= hyperlinkfornetmedxresource %>&jobid=<bean:write name = "JobDashboardForm" property = "jobid" />"  onclick="Javascript:createCookie('oa_j<%= res_id %>');">Edit</a></logic:present>
								 </TD>
								 <TD class="<%= bginnerhyperlink %>"><bean:write name = "rlist" property = "resource_type" /></TD>
								 <TD class="<%= bginnerhyperlink %>"><bean:write name = "rlist" property = "resource_sourcename" /></TD>
								 <TD class="<%= bginnercolor %>"><bean:write name = "rlist" property = "resource_sourcepartno" /></TD>
								 <TD class="<%= bginnerhyperlink %>"><bean:write name = "rlist" property = "resource_partner_name" /> </TD>
								 <TD class="<%= bginnerhyperlink %>"><a id='xa_j<%= res_id %>' href = "POWOAction.do?jobid=<bean:write name="JobDashboardForm" property="jobid"/>" onclick="Javascript:createCookie('xa_j<%= res_id %>');">[Assign]</a> </TD>
								 <TD class="<%= bginnercolornumber %>"><bean:write name = "rlist" property = "resource_estimatedunitcost" /></TD>
								 <TD class="<%= bginnercolornumber %>"><bean:write name = "rlist" property = "resource_estimatedtotalcost" /></TD>
								 <TD class="<%= bginnercolornumber %>"><bean:write name = "rlist" property = "resource_extendedunitprice" /></TD>
								 <TD class="<%= bginnercolornumber %>"><bean:write name = "rlist" property = "resource_extendedtotalprice" /></TD>
								 <TD class="<%= bginnercolornumber %>"><bean:write name = "rlist" property = "resource_margin" /></TD>
							  	<TD class="<%= bginnercolor %>"><bean:write name = "rlist" property = "resource_drop_shipped" /></TD>
							  	<td></td>
							  </TR>
				  		<% j++; %>
				  		</logic:iterate>
</TABLE></td>
              			</tr>
              			</TBODY>
              			</TABLE>
 			  <!--added by shailja start -->
			  </TD> <!--missing in job dashboard -->
				</tr>
				</table>
					</td>
					</tr><!--missing in job dashboard -->
					<!-- shailja  end -->	
	 					<%i++; %>
	      				 </logic:iterate>
              		 </Tbody>
				</Table>
			</TD>
		</TR>
	</TBODY>
</TABLE>
</logic:present>
<logic:notPresent name = "NetMedX" scope = "request">
<TABLE class=dbsummtable3 cellSpacing=1 cellPadding=0 width=1250 border=0>
	<TBODY>
				<TR> 
							<TD width=2>&nbsp;</TD>
			                <TD width=290 class=trybtopbutton align = "left">
								<html:button property="sort" styleClass="button" onclick = "javascript:createCookie('sort');return sortwindow();">
									<bean:message bundle="PRM" key="prm.netmedx.sort"/>
								</html:button>
			                	<html:submit property="optactivity" styleClass="button" onclick = "javascript:createCookie('optactivity');">
									Add Out of Scope Activity
								</html:submit>
			                	<logic:present name = "JobDashboardForm" property="inv_type">
									<html:button property="back" styleClass="button" onclick = "return Backaction();">
										<bean:message bundle="PRM" key="prm.button.back"/>
									</html:button>
								</logic:present>
								<logic:equal name = "JobDashboardForm" property = "nettype" value = "dispatch">
									<html:button property="back" styleClass="button" onclick = "return Backaction();">
										<bean:message bundle="PRM" key="prm.button.back"/>
									</html:button>
								</logic:equal>		
			                </TD>
                			<TD width=430 class=trybtop height=20 align="right" colspan="8"><A href="JobDashboardActionOld.do?jobid=<bean:write name = 	"JobDashboardForm" property = "jobid" />&viewjobtype=ION&ownerId=I"><bean:message bundle="PRM" 				key="prm.jobdashboard.activitiessummary"/></a>
							</TD>
                			<TD width=428 >&nbsp;</TD>
			    </TR>
			    <TR>		
          	      			<TD width=19 rowSpan=2>&nbsp;</TD>
			                <TD width=199 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.activity"/></TD>
			                <TD width=69 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.qty"/></TD>
			                <TD width=109 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.type"/></TD>
			                <TD width=49 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.scope"/></TD>
							<TD width=138 class=tryb colSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.estimatedcosts"/></TD>
			                <TD width=69 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.extendedunitprice"/></TD>
			                <TD width=69 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.extendedtotalprice"/></TD>
			                <TD width=49 class=tryb rowSpan=2><bean:message bundle="PRM" key="prm.jobdashboard.proformamargin"/></TD>
			                <TD width=108 rowSpan=2>&nbsp;</td>
			                <TD width=400 >&nbsp;</TD>
	      		</TR>
		        <TR>
			                <TD width=70 class=tryb><DIV><bean:message bundle="PRM" key="prm.jobdashboard.unit"/></DIV></TD>
			                <TD width=70 class=tryb><DIV><bean:message bundle="PRM" key="prm.jobdashboard.total"/></DIV></TD>
			    </TR>
             		  <logic:iterate id = "actlist" name = "actlist">
             			<%	if ( csschooser == true ) {
								csschooser = false; bgcolor = "dbvalueodd"; bgcolornumber = "numberodd"; bghyperlink = "textleftodd"; textclass = "texte";
							} else {
								csschooser = true; bgcolor = "dbvalueeven"; bgcolornumber = "numbereven"; bghyperlink = "textlefteven"; textclass = "texto";
							}
						%>
             		<bean:define id = "activity_lx_ce_type" name = "actlist" property = "activity_lx_ce_type" type = "java.lang.String" />
             		<bean:define id = "act_id" name = "actlist" property = "activity_id" type = "java.lang.String" />
             		<bean:define id = "actitype" name = "actlist" property = "type" type = "java.lang.String" />
             		   <% 
					  		actid_str=act_id;
							hyperlinkforeditquantity = "ActivityUpdateAction.do?ref=inscopeactivity&from=jobdashboard&Activity_Id="+act_id;
        		   			if( activity_lx_ce_type.equals( "A" ) ) {
								hyperlinkforactivity = "ResourceListAction.do?resourceListType=A&type=PM&from=jobdashboard&ref=detailactivity&Activity_Id="+act_id+"&Activitylibrary_Id="+act_id;
								refvar = "View";
							} if( activity_lx_ce_type.equals( "IA" ) ) {
								hyperlinkforactivity = "ResourceListAction.do?resourceListType=A&type=PM&from=jobdashboard&ref=Viewinscope&Activity_Id="+act_id+"&Activitylibrary_Id="+act_id;
								refvar = "Viewinscope";
							} if( activity_lx_ce_type.equals( "AA" ) ) {
								hyperlinkforactivity = "ResourceListAction.do?resourceListType=A&type=PM&from=jobdashboard&ref=detailaddendumactivity&Activity_Id="+act_id+"&Activitylibrary_Id="+act_id;
								refvar = "ViewAddendum";
							} if( activity_lx_ce_type.equals( "CA" ) ) {
								hyperlinkforactivity = "ResourceListAction.do?resourceListType=A&type=PM&from=jobdashboard&ref=detailchangeorderactivity&Activity_Id="+act_id+"&Activitylibrary_Id="+act_id;
								refvar = "ViewChangeorder";
							}
             		   %>
             	<TR>
	             		   	<td width = 20>&nbsp;</td>
				            <TD width = 200 class="<%= bghyperlink %>">
					                <A id='ua_j<%= act_id %>' href = "<%= hyperlinkforactivity %>&jobid=<bean:write name = "JobDashboardForm" property = "jobid" />" onclick="Javascript:createCookie('ua_j<%= act_id %>');"><bean:write name = "actlist" property = "activity_name" /></A>
					                &nbsp;<logic:equal name = "actlist" property = "activity_oos_flag" value = "Y"><a href="EditOOSActivityAction.do?Activity_Id=<%= act_id %>&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />&Appendix_Id=<bean:write name = "JobDashboardForm" property = "appendix_id" />">Edit</a></logic:equal>	
				             </TD>
				             <TD width = 100 class="<%= bgcolornumber %>">
				                	<bean:write name = "actlist" property = "quantity" />
			                	<% if( !actitype.equals( "Overhead" ) && request.getAttribute( "NetMedX" ) == null) {%>
			                	<a id='fa_j<%= act_id %>' href = "<%= hyperlinkforeditquantity %>&Job_Id=<bean:write name = "JobDashboardForm" property = "jobid" />"  onclick="Javascript:createCookie('fa_j<%= act_id %>');">Edit</a>
			               		<% } else { %>
			               		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			               		<% } %> 
			                </TD>
			                <TD width = 100 class="<%= bgcolor %>"><bean:write name = "actlist" property = "type" /></TD>
			                <TD width = 50 class="<%= bgcolor %>"><bean:write name = "actlist" property = "scope" /></TD>
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "estimatedunitcost" /></TD>
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "estimatedtotalcost" /></TD>
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "extendedunitprice" /></TD>
			                <TD width = 70 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "extendedtotalprice" /></TD>
			                <TD width = 50 class="<%= bgcolornumber %>"><bean:write name = "actlist" property = "margin" /></TD>
			                <TD width = 109>&nbsp;</td>
                			<TD width=400 >&nbsp;</TD>
              	 </TR>
						<%i++; %>
	      				 </logic:iterate>
              		 </Tbody>
				</Table>
			</TD>
		</TR>
	</TBODY>
</TABLE>
</logic:notPresent>
</BODY>
<script>
function Backaction()
{
	if( document.forms[0].nettype.value == 'dispatch' ) {
			document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype=dispatch&viewjobtype=<bean:write name = "JobDashboardForm" property = "viewjobtype" />&ownerId=<bean:write name = "JobDashboardForm" property = "ownerId" />";
	}		
	else
		{
			document.forms[0].action = "InvoiceJobDetail.do?type="+document.forms[0].inv_type.value+"&id="+document.forms[0].inv_id.value+"&rdofilter="+document.forms[0].inv_rdofilter.value+"&invoice_Flag="+document.forms[0].invoice_Flag.value+"&invoiceno="+document.forms[0].invoiceno.value+"&partner_name="+document.forms[0].partner_name.value+"&powo_number="+document.forms[0].powo_number.value+"&from_date="+document.forms[0].from_date.value+"&to_date="+document.forms[0].to_date.value;
		}	
	document.forms[0].submit();
	return true;
}
function popUpChecklistTab(stringChangeStatus)
{
	str = "ChecklistTab.do?tempString="+stringChangeStatus+"&job_id="+document.forms[0].jobid.value+"&popwin=1&page=jobdashboard&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value+"&checkMsp=1";
	suppstrwin = window.open( str , '' , 'left = 130 , top = 130 , width = 650 , height = 600 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();
	return true;
}
</script
</html:form>
</html:html>
