<!DOCTYPE HTML>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <LINK href="styles/style.css" rel="stylesheet" type="text/css">
        <LINK href="styles/content.css" rel="stylesheet" type="text/css">

        <%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
        <%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
        <%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

        <%-- <%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>  --%>

        <link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
        <link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
        <link href="styles/summary.css" rel="stylesheet" type="text/css" />

        <title>Ilex</title>
    </head>
    <body>
        <table cellpadding="0" cellspacing="0" border="0" style="margin-left: 0px; margin-top: 10px;" width="40">
            <tr>
                <td style="padding-left: 0px; padding-top: 0px; padding-bottom: 3px;" class="dbvaluesmallFontBold" valign="top">Work Notes</td>
            </tr>
            <tr>
                <td>
                    <table class="Ntextoleftalignnowrappaleyellowborder2" cellspacing="2" cellpadding="2">
                        <tr>
                            <td valign="top" colspan="2">
                                <textarea name="installationNotes" id="installationNotes" cols="90" rows="6" class="textarea"></textarea> 
                            </td>
                        </tr>
                        <tr align="right">
                            <td>
                                <input type="checkbox" name="changeOrderReq" title="Change Order" id="changeOrderReq" value="changeOrderReq" onclick="changeOrderReqChkBoxStatus()" />Change Order
                                &nbsp;&nbsp;&nbsp;<input type="checkbox" name="changeOrderResp" title="Change Order Response" id="changeOrderResp" value="changeOrderResp" onclick="changeOrderRespChkBoxStatus()" />Change Order Response
                                &nbsp;<button id="add_work_notes" class="button_c1">Add</button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" style="margin-left: 0px; margin-top: 10px;" width="500">
            <tr>
                <td class="dbvaluesmallFontBold" style="padding-left: 2px;" valign="top">Recent Work Notes</td>
            </tr>
            <tr valign="bottom">
                <td align="right" cellspacing="0" cellpadding="0">
                    <table width="100%" border="1" style="border-width: 1px; border-collapse: collapse;" bordercolor="#D9DFEF" class="Ntextoleftalignnowrapblue" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="25%" valign="top" align="center" id="latestInstallNotesDate">01/26/2015 12:48 PM</td>
                            <td width="75%" valign="top" align="left" id="latestInstallNotes" style="white-space: normal;">Test Note</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
