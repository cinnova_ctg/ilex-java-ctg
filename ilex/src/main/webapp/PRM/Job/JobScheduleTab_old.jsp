<% 
response.setHeader("pragma","no-cache");//HTTP 1.1 
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Cache-Control","no-store"); 
response.addDateHeader("Expires", -1); 
response.setDateHeader("max-age", 0); 
response.setIntHeader ("Expires", -1); //prevents caching at the proxy server 
response.addHeader("cache-Control", "private"); 
%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<link rel = "stylesheet" href="styles/content.css" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>

<script>
/* These function is used for Reschedule Jobs */
/* Call Ajax for Reschedule: Start*/
var rseCheck = false;
function onDateChange(){
	if(document.forms[0].currentSchDate.value != document.forms[0].scheduleStartDate.value){
		showReshecule();
	}
}
function hideReReshecule(){	
	getResponse('JobScheduleAction.do?hmode=reschedule&hide=hide');
	document.getElementById("hideTR1").style.display = 'none';
	rseCheck = false;
}

function showReshecule(){
	if(rseCheck == false){
		var scheduledDate = document.forms[0].scheduleStartDate.value;
		var scheduledHH = document.forms[0].scheduleStartHour.value;
		var scheduledMM = document.forms[0].scheduleStartMinute.value;
		var scheduledAAA = document.forms[0].scheduleEndIsAm.value;
		var jobId = <c:out value='${JobSetUpForm.jobId}'/>;
		getResponse('JobScheduleAction.do?hmode=reschedule&hide=show&date='+scheduledDate+'&hh='+scheduledHH+'&mm='+scheduledMM+'&aa='+scheduledAAA+'&jobId='+jobId);
	}
}

function getResponse(partnerURL){
/* This function call Ajax which is inmplemented into /javascript/prototype.js */	 
	var url = partnerURL;
	var pars = '';
	
	$.ajax({
		type: "GET",
		url: url,
		data: pars,
		error: disableStatus,
		success: disableStatus
    });
	
	//var myAjax = new Ajax.Updater({success:'reschedule'},url,{method:'get', parameters:pars, onSuccess:disableStatus, onFailure:disableStatus, evalScripts: true});	
	//document.getElementById("hideTR1").style.display = '';
	rseCheck = true;
}
function disableStatus(res){
	document.getElementById("reschedule").innerHTML = res.response.Text;
}		
</script>
<script>
function saveActions(){
	if(validate()){
		var url;
		url = "JobScheduleAction.do?hmode=Save&type=<c:out value='${JobSetUpForm.tabId}'/>&appendix_Id=<c:out value='${JobSetUpForm.appendixId}'/>&Job_Id=<c:out value='${JobSetUpForm.jobId}'/>&ticketType=<c:out value='${JobSetUpForm.ticketType}'/>&ticket_id=<c:out value='${JobSetUpForm.ticket_id}'/>&isClicked=1&saveJobActions=1";							
		document.forms[0].action=url;
		document.forms[0].submit();
	}
}	
function validate(){
	if(document.forms[0].offsiteDate.value != '' && document.forms[0].onsiteDate.value == ''){
		alert('Please first set offsiteDate.');
		return false;
	}
	/* For Job Reschedule: Start */
	try{
		return validateRse(); // - for validation of reschedule.
	}catch(e){}
	/* For Job Reschedule: Start */
	//document.forms[0].method= 'get';
	return true;
}
</script>
<html:hidden name="JobSetUpForm" property="scheduleid"/>
<table width="60%">
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
	<table class="Ntextoleftalignnowrappalenoborder" width="100%">
		
		<tr>
			<td>
			<table cellpadding="0" cellspacing="1" border="0" >
				<tr>
					<td class="labelo"><b>Scheduled Start:</b></td>
					<td class="labelo">
						<html:hidden name="JobSetUpForm" property="currentSchDate"/>
						<html:text name="JobSetUpForm" property="scheduleStartDate" styleClass="text" size="10" onfocus="onDateChange();"></html:text>
						<img src="images/calendar.gif" width="19" height="17" border="0" align="center"
							onclick="return popUpCalendar(document.forms[0].scheduleStartDate, document.forms[0].scheduleStartDate, 'mm/dd/yyyy')"
							onmouseover="window.status = 'Date Picker';return true;"
							onmouseout="window.status = '';return true;" />
					</td>
					<td class="labelo">
						<html:select name="JobSetUpForm" property="scheduleStartHour" styleClass="comboo" onchange="showReshecule();">
							<html:optionsCollection name="codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
					</td>
					<td class="labelo">	
						<html:select name="JobSetUpForm" property="scheduleStartMinute" styleClass="comboo" onchange="showReshecule();">
							<html:optionsCollection name="codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
					</td>
					<td class="labelo">
						<select name="scheduleStartIsAm" class="comboo">
							<option value="AM" <c:if test="${JobSetUpForm.scheduleStartIsAm eq '1'}">selected="selected"</c:if>>AM</option>
							<option value="PM" <c:if test="${JobSetUpForm.scheduleStartIsAm eq '0'}">selected="selected"</c:if>>PM</option>
						</select>		
					</td>				
				</tr>
				<tr id="hideTR1" style="display: none;">
					<td colspan="5" style="padding: 0px 0px 0px 65px"><span id="reschedule"></span></td>
				</tr>
				<tr>
					<td class="labele"><b>Scheduled End:</b></td>
					<td class="labele">
						<html:text name="JobSetUpForm" property="scheduleEndDate" styleClass="text" size="10"></html:text>
						<img src="images/calendar.gif" width=19 height=17 border=0
							align="center"
							onclick="return popUpCalendar(document.forms[0].scheduleEndDate, document.forms[0].scheduleEndDate, 'mm/dd/yyyy')"
							onmouseover="window.status = 'Date Picker';return true;"
							onmouseout="window.status = '';return true;" />
					</td>	
					<td class="labele">
						<html:select name="JobSetUpForm" property="scheduleEndHour" styleClass="comboe">
							<html:optionsCollection name="codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
					</td>
					<td class="labele">
						<html:select name="JobSetUpForm" property="scheduleEndMinute" styleClass="comboe">
							<html:optionsCollection name="codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
					</td>
					<td class="labele">	
						<select name="scheduleEndIsAm" class="comboo">
							<option value="AM" <c:if test="${JobSetUpForm.scheduleEndIsAm eq '1'}">selected="selected"</c:if>>AM</option>
							<option value="PM" <c:if test="${JobSetUpForm.scheduleEndIsAm eq '0'}">selected="selected"</c:if>>PM</option>
						</select>		
					</td>				
				</tr>
				
				<tr>
					<td class="labelo"><b>Actual Start:</b></td>
					<td class="labelo">
						<html:text name="JobSetUpForm" property="onsiteDate" styleClass="text" size="10"></html:text>
						<img src="images/calendar.gif" width=19 height=17 border=0
							align="center"
							onclick="return popUpCalendar(document.forms[0].onsiteDate, document.forms[0].onsiteDate, 'mm/dd/yyyy')"
							onmouseover="window.status = 'Date Picker';return true;"
							onmouseout="window.status = '';return true;" />
					</td>	
					<td class="labele">
						<html:select name="JobSetUpForm" property="onsiteHour" styleClass="comboo">
							<html:optionsCollection name="codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
					</td>
					<td class="labelo">
						<html:select name="JobSetUpForm" property="onsiteMinute" styleClass="comboo">
							<html:optionsCollection name="codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
					</td>
					<td class="labelo">	
						<select name="onsiteIsAm" class="comboo">
							<option value="AM" <c:if test="${JobSetUpForm.onsiteIsAm eq '1'}">selected="selected"</c:if>>AM</option>
							<option value="PM" <c:if test="${JobSetUpForm.onsiteIsAm eq '0'}">selected="selected"</c:if>>PM</option>
						</select>		
					</td>				
				</tr>
				
				<tr>
					<td class="labele"><b>Actual End:</b></td>
					<td class="labele">
						<html:text name="JobSetUpForm" property="offsiteDate" styleClass="text" size="10"></html:text>
						<img src="images/calendar.gif" width=19 height=17 border=0
							align="center"
							onclick="return popUpCalendar(document.forms[0].offsiteDate, document.forms[0].offsiteDate, 'mm/dd/yyyy')"
							onmouseover="window.status = 'Date Picker';return true;"
							onmouseout="window.status = '';return true;" />
					</td>	
					<td class="labelo">
						<html:select name="JobSetUpForm" property="offsiteHour" styleClass="comboe">
							<html:optionsCollection name="codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
					</td>
					<td class="labelo">
						<html:select name="JobSetUpForm" property="offsiteMinute" styleClass="comboe">
							<html:optionsCollection name="codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
					</td>
					<td class="labele">	
						<select name="offsiteIsAm" class="comboe">
							<option value="AM" <c:if test="${JobSetUpForm.offsiteIsAm eq '1'}">selected="selected"</c:if>>AM</option>
							<option value="PM" <c:if test="${JobSetUpForm.offsiteIsAm eq '0'}">selected="selected"</c:if>>PM</option>
						</select>		
					</td>				
				</tr>
				
			</table>
		</td>
		</tr>
		<tr>
			<td>
				<html:button property="save" styleClass="button_c" onclick ="saveActions();" >Save&nbsp;</html:button>
				<input type="reset" name="Cancel" value="Cancel" class="button_c" onclick="hideReReshecule();"/>&nbsp;
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

