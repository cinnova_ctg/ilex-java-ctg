<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>

<head>

	<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

</head>

<%@ include  file="/Menu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing=1" cellpadding="0" height="18">
        <tr align="left"> 
         <td class="toprow1" width=300><a href="AppendixHeader.do?appendixid=<%=request.getParameter("typeid") %>&function=view" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.backtoappendixdashboard"/></a></td>
		 </td>
		   <td  width="100%" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>

<table>
<html:form action="AddPoNumber">
<table  border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="300">
			
			  <tr> 
			    <td colspan="4" class="labeleboldwhite" height="30" ><bean:message bundle="PRM" key="prm.viewponumber"/></td>
			  </tr> 
			  
			  	
			<tr>
				
			 	<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.ponumber"/></td>
				<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.partnercompany"/></td>
				<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.estimatedcost"/></td>
				<td   class="tryb" height="20"><bean:message bundle="PRM" key="prm.invoicedcost"/></td>
			</tr>
			<logic:present name="partnerlist" scope="request">
			 <%int i=1;
				String bgcolor=""; %>
				
				<logic:iterate id="plist" name="partnerlist">
				
			 	<%if((i%2)==0)
					{
						bgcolor="labele"; 
					
					} 
				  else
					{
						bgcolor="labelo";
					}%>
			
			<tr>
				
				<td   class="<%=bgcolor %>" height="20"></td>
				<td   class="<%=bgcolor %>" height="20"></td>
				<td   class="<%=bgcolor %>" height="20"></td>
				<td   class="<%=bgcolor %>" height="20"></td>
			</tr>
				<%i++; %>
				</logic:iterate>
				</logic:present> 
				 
			 
						   
			  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
  			  
			</table>
			 </td>
			 </tr> 
			</table>
</html:form>
<body>
</html:html>