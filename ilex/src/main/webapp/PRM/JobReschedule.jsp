<% 
response.setHeader("pragma","no-cache");//HTTP 1.1 
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Cache-Control","no-store"); 
response.addDateHeader("Expires", -1); 
response.setDateHeader("max-age", 0); 
response.setIntHeader ("Expires", -1); //prevents caching at the proxy server 
response.addHeader("cache-Control", "private"); 
%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<script language="javascript" type="text/javascript">
	document.getElementById("hideTR1").style.display = '';
</script>

<html:hidden name ="JobSetUpForm" property="oldSchStart"/>
<table border= "0" cellpadding="0" cellspacing="0" style="border: 3px solid #AAA;" bgcolor="white">
	<tr>
		<th style="background: #FAF8CC;font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-weight: bold;font-size: 11px;color: black;">Rescheduling</th>
	</tr>
	<tr>
		<td style="padding: 5px 10px 0px 10px">
		
				<table style="margin-top:5px;" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF" width="25px">
				<tr >
					<td class="labelNoColor" colspan="3" >Why?</td>
				</tr>
				</table>
				
				<table style="border: 1px black solid;margin-top:5px;" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
					<td class="labelNoColor">
						<html:radio name ="JobSetUpForm" property ="rseType" value ="Client" onclick="document.getElementById('hide2').style.display = 'none';clearRedio();">Client</html:radio>
					</td>
					<td class="labelNoColor">
						<html:radio name ="JobSetUpForm" property ="rseType" value ="Contingent" onclick="document.getElementById('hide2').style.display = 'none';clearRedio();">Contingent</html:radio>
					</td>
					<td class="labelNoColor">
						<html:radio name ="JobSetUpForm" property ="rseType" value ="Internal" onclick="document.getElementById('hide2').style.display = '';">Internal</html:radio>
					</td>	
				</tr>
			</table>
			
		</td>
	</tr>
	<tr height="3"><td></td></tr>
	
	<tr id="hide1" >
		<td style="padding: 0px 10px 0px 10px">
			<table style="margin-top:5px;" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
				<tr>
					<td class="labelNoColor" colspan="4">How to decide?</td>
				</tr>
			</table>
			<table style="border: 1px black solid;margin-top:5px; margin-left:0px;margin-bottom:10px" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
				<tr style="padding-left: 35px">
				<td class="labelNoColor" style="padding-left: 8px;">
					Client: 
					</td>
					<td class="labelNoColor" style="white-space: normal;">
					   <bean:message bundle="PRM" key="prm.rse.client.reason1"/>
					</td>
				</tr>	
				
				<tr style="padding-left: 35px;">
				<td class="labelNoColor" style="padding-left: 8px;">
					Contingent: 
					</td>
					<td class="labelNoColor" style="white-space: normal;">
					   <bean:message bundle="PRM" key="prm.rse.contingent.reason1"/>
					</td>
				</tr>
				<tr style="padding-left: 35px">
				<td class="labelNoColor" style="padding-left: 8px;">
					Internal: 
					</td>
					<td class="labelNoColor" style="white-space: normal;">
					   <bean:message bundle="PRM" key="prm.rse.internal.reason1"/>;<bean:message bundle="PRM" key="prm.rse.internal.reason2"/>;<bean:message bundle="PRM" key="prm.rse.internal.reason3"/>
					</td>
				</tr>
				</table>						
			
		</td>
	</tr>	
	
	<tr id="hide2" style="display: none;">
		<td style="padding: 0px 10px 10px 10px">
			<table style="margin-top:5px; " cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
				<tr>
					<td class="labelNoColor" colspan="4">Internal Justification:</td>
				</tr>
				</table>
				<table style="border: 1px black solid;margin-top:5px; margin-left:0px;margin-bottom:5px" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
				<tr >
					<td class="labelNoColor" style="padding-left: 3px;">
						<html:radio name ="JobSetUpForm" property ="rseInternalJustification" value ="1" onclick=""><bean:message bundle="PRM" key="prm.rse.internal.internaljustification.reason1"/></html:radio>
					</td>
				</tr>	
				<tr >
					<td class="labelNoColor" style="padding-left: 3px;padding-right: 5px;">
						<html:radio name ="JobSetUpForm" property ="rseInternalJustification" value ="2" onclick=""><bean:message bundle="PRM" key="prm.rse.internal.internaljustification.reason2"/></html:radio>
					</td>
				</tr>	
				<tr >
					<td class="labelNoColor" style="padding-left: 3px;">
						<html:radio name ="JobSetUpForm" property ="rseInternalJustification" value ="3" onclick=""><bean:message bundle="PRM" key="prm.rse.internal.internaljustification.reason3"/></html:radio>
					</td>
				</tr>
				</table>				
			</table>
		</td>
	</tr>
	<tr height="5"><td></td></tr>
</table>


