<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html:html>
<head>
	<title>Assign Team</title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<!-- Hidden form element -->
<html:form action="AssignTeam.do?function=View" >
<jsp:include page = '/PRM/ViewSelectorPRM.jsp'>
	<jsp:param name = 'bean' value = 'AssignTeamForm'/>
	<jsp:param name = 'projectType' value = '<%=(String)request.getParameter("fromPage")%>'/>
	<jsp:param name = 'pageName' value = 'AssignTeam.jsp'/>
</jsp:include>
<!-- For Menu & View Selector: End -->

<html:hidden name="AssignTeamForm" property="appendixid"/>
<html:hidden name="AssignTeamForm" property="appendixName"/>
<!-- Assign Team For Appendix: Start  --> 
<table border="0" cellpadding="0" cellspacing="0" style="padding-top: 8px">
	
	<logic:present name = "addFlag" scope = "request">
	<logic:equal name = "addFlag" value = "0">
	<tr>
		<td>&nbsp;&nbsp;</td>
		<td colspan = "3" class = "message" height = "30">Team Member Assigned Successfully</td>
    </tr>
    </logic:equal>
	</logic:present>
	
	<logic:present name = "deleteFlag" scope = "request">
	<logic:equal name = "deleteFlag" value = "0">
	<tr>
		<td>&nbsp;&nbsp;</td>
		<td colspan = "3" class = "message" height = "30">Team Member Deleted Successfully</td>
    </tr>
    </logic:equal>
    </logic:present>
	
	<tr>
		<td>&nbsp;&nbsp;</td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			   <tr>
					<td class="labeleboldwhite">
						Team Member:&nbsp;
					</td>
					<td>
						<!-- Team Combobox: Start -->
			          	<html:select name="AssignTeamForm" property = "pocId" style="width:180px" styleClass = "select"  onchange="AssignTeam();">
							<html:optionsCollection name = "AssignTeamForm" property = "pocList" value = "value" label = "label" /> 
						</html:select>	
						<!-- Team Combobox: End -->	
					</td>
					<td>&nbsp;&nbsp;&nbsp;</td>
					
					
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
					<tr>
						<td colspan="7">
						<table border="0" cellpadding="0" cellspacing="1">
							<tr>
								<th class = "Ntryb">Name</th>
								<th class = "Ntryb">Job Owner</th>
								<th class = "Ntryb">Scheduler</th>
								<th class = "Ntryb">Generate Invoice</th>
							</tr>
							<c:forEach items="${AssignTeamForm.assignTeamList}" var="list" varStatus="status" >
							<tr>
								<td class = "Nhyperodd">
									<label name="fname[${status.index}]" />${list.pocName}</label>	 		
									<input type="hidden" name="fname"	value="${list.pocId}"	/>				
								</td>
								<td align="center" bgcolor="#ffffff">
											 <input type='checkbox' name="jobOwner[${status.index }]"  <c:if test="${list.roleNames[1]=='Job Owner'}"><c:out value="Checked"/></c:if> />
								</td>
								<td align="center" bgcolor="#ffffff"> 
											<input type='checkbox' name="scheduler[${status.index }]" <c:if test="${list.roleNames[3]=='Scheduler'}"><c:out value="Checked"/></c:if> />
								</td>
								<td align="center" bgcolor="#ffffff">
												<input type='checkbox' name="invGenerator[${status.index }]" <c:if test="${list.roleNames[0]=='Invoice Generator'}"><c:out value="Checked"/></c:if> />
								</td>
							</tr>
							</c:forEach>
								<tr>
								<td class = "Nhyperodd"><label >Customer Help Desk Email Address</label></td>
								<td align="left" colspan="3" class = "Nhyperodd"><input id="customerEmail" name="customerEmail" value="${customerEmail}" style="width:233px" type="textbox" /></td>
								<td>&nbsp;</td>
								<td><html:button property="save" styleClass="Rbutton" onclick="AssignAllTeam();">Assign</html:button></td>
								</tr>

						</table>
						</td>
					</tr>
			</table>
		</td>
	</tr>
</table>
<!-- Assign Team For Appendix: End  --> 
</html:form>
<body>
</html:html>
<script>

/*** Email validation code **/
 function emailvalidation(emailvalue,emailinputfield )
{	
	if( emailvalue == '' ){
		//alert("Email should be in aa@bb.cc,xx@yy.zz format.");		
		emailinputfield.focus();
		return true;
	}else
		{
		var emailFilter2=/^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
		var finalemails = "";
		var emailValue2=emailvalue.trim();
		var emailadressess = new Array();
		if(emailValue2.indexOf(",")>-1)
			{
			emailadressess = emailValue2.split(",");
			for(var i = 0 ; i < emailadressess.length;i++ )
				{
				if(emailadressess[i].trim() != "")
				if (!(emailFilter2.test(emailadressess[i].trim())))	{
					alert("Email should be in aa@bb.cc,xx@yy.zz format.");
					emailinputfield.focus();
				return false;
				}else
				{
					if(i == emailadressess.length-1)
					finalemails += emailadressess[i].trim();
					else
						finalemails += emailadressess[i].trim()+",";
				}
				}
			}else if(emailValue2.indexOf(";")>-1)
			{
				emailadressess = emailValue2.split(";");
				for(var i = 0 ; i < emailadressess.length;i++ )
					{
					if(emailadressess[i].trim() != "")
					if (!(emailFilter2.test(emailadressess[i].trim())))	{
						alert("Email should be in aa@bb.cc,xx@yy.zz format.");
						emailinputfield.focus();
					return false;
					}else
					{
						if(i == emailadressess.length-1)
						finalemails += emailadressess[i].trim();
						else
							finalemails += emailadressess[i].trim()+",";
					}
					}
			}else if(emailValue2.indexOf(" ")>-1)
				{
				emailadressess = emailValue2.split(" ");
				for(var i = 0 ; i < emailadressess.length;i++ )
					{
					if(emailadressess[i] != "")
					if (!(emailFilter2.test(emailadressess[i].trim())))	{
						alert("Email should be in aa@bb.cc,xx@yy.zz format.");
						emailinputfield.focus();
					return false;
					}else
					{
						if(i == emailadressess.length-1)
						finalemails += emailadressess[i].trim();
						else
							finalemails += emailadressess[i].trim()+",";
					}
					}
				}else
					{
					if (!(emailFilter2.test(emailValue2)))	{
						alert("Email should be in aa@bb.cc,xx@yy.zz format.");
						emailinputfield.focus();
					return false;
					}else
					{
						finalemails += emailValue2.trim();
					}
					}
		emailinputfield.value = finalemails;
		}
	return true;
}
/* For Assign role*/
function AssignTeam()
{
	if(document.forms[0].pocId.value == '0'){
		alert('Please select a Team Member.');
		return false;
	}
// 	if(document.forms[0].roleId.value == '0'){
// 		alert('Please select a Role.');
// 		return false;
// 	}
	document.forms[0].action="AssignTeam.do?function=AssignToList";
	document.forms[0].submit();
	return true;
	}
function AssignAllTeam()
{
	//validate email	
	if(emailvalidation(document.forms[0].customerEmail.value, document.forms[0].customerEmail) == true ){
	document.forms[0].action="AssignTeam.do?function=Add";
	document.forms[0].submit();
	}
	return true;
}

/* For delete Assinged partner and its role. */
function Del(appendixId, roleId, pocId) 
{
	var convel = confirm( "Are you sure you want to delete the Team Member?" );
	if( convel )
	{	
		document.forms[0].action = "AssignTeam.do?function=Delete&appendixid="+appendixId+"&roleid="+roleId+"&pocid="+pocId;
		document.forms[0].submit();
		return true;	
	}
}

</script>

<script>
/* Function for view selector: Start */
function changeFilter()
{
	document.forms[0].action="AssignTeam.do?function=View&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="AssignTeam.do?function=View&resetList=something";
	document.forms[0].submit();
	return true;

}
/* Function for view selector: End */
</script>