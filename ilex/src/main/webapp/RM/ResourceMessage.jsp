<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Displaying item(4th level resource category) that is added.
*
-->

<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>


<bean:define id = "resf" name = "resourceform" />

<bean:define id = "resid" name = "resourceform" property="id3" />
<bean:define id = "resourceflag" name = "resourceform" property="resourceflag" scope = "request"/>

<%

String menu_label;
String resource_label;
String name_label;
String ident_label;

int resource=Integer.parseInt((String)request.getAttribute("resourceflag"));

switch(resource){
case 1:
{
	
	menu_label="rm.manageitems";
	resource_label="rm.material";
	name_label="rm.materialname";
	ident_label="rm.matidentifier";
	
	break;
}
case 2:
{
	menu_label="rm.manageitems";
	resource_label="rm.labor";
	name_label="rm.laborname";
	ident_label="rm.laboridentifier";
	break;
}
case 3:
{
	menu_label="rm.manageitems";
	resource_label="rm.freight";
	name_label="rm.frieghtname";
	ident_label="rm.frtidentifier";
	
	break;
}
case 4:
{
	menu_label="rm.manageitems";
	resource_label="rm.travel";
	name_label="rm.travelname";
	ident_label="rm.travelidentifier";
	
	break;
}
	default:
	{
		menu_label="rm.manageitems";
		resource_label="rm.material";
		name_label="rm.materialname";
		ident_label="rm.matidentifier";
		
	}

}
%>
<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>

<%@ include  file="/Menu.inc" %>
<script>
function del() 
{
			
			var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
			var convel = confirm( msg );
			if( convel )
			{
				document.forms[0].action = "ResourceAdd.do?f=<%= resourceflag%>&id=<%= request.getParameter("id")%>&id1=<%= request.getParameter("id1")%>&id2=<%= request.getParameter("id2")%>&id3=<%=resid%>&function=delete";
				document.forms[0].submit();
				return true;	
			}
			return false;
		
}

function checkstatus() 
{
			
					
		document.forms[0].action = "ResourceAdd.do?f=<%= resourceflag%>&id=<%= request.getParameter("id")%>&id1=<%= request.getParameter("id1")%>&id2=<%= request.getParameter("id2")%>&id3=<%=resid%>&function=update";
		document.forms[0].submit();
		return true;	
			
		
		
}


</script>

<script language="JavaScript" src="javascript/date-picker.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" bgcolor="#FFFFFF">
<div id="menunav">
	  
<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        	<li><a href="ResourceAdd.do?f=<%= resourceflag%>&id=<%= request.getParameter("id")%>&id1=<%= request.getParameter("id1")%>&id2=<%= request.getParameter("id2")%>&function=add">Add</a></li>
        	<li><a href="javascript:checkstatus();">Update</a></li>
        	<li><a href="javascript:del();">Delete</a></li>
           </ul>
		</li>
</ul>
</div>
<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
       <table  border="0" cellspacing="1" cellpadding="0" height="18">
	       <tr> 
		       <td  id="pop1" width=150  class="toprow1"><a   href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="drop"><bean:message bundle="RM" key="<%=menu_label%>"/></a></td>
		   	   <td  width=650 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		   </tr>
      </table>
    </td>
  </tr>
</table> --%>


<%-- <SCRIPT language=JavaScript1.2>
if (isMenu) {

arMenu1=new Array(
140,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",
"<bean:message bundle = "RM" key = "rm.add"/>","ResourceAdd.do?f=<%= resourceflag%>&id=<%= request.getParameter("id")%>&id1=<%= request.getParameter("id1")%>&id2=<%= request.getParameter("id2")%>&function=add",0,
"<bean:message bundle = "RM" key = "rm.update"/>","javascript:checkstatus();",0,
"<bean:message bundle = "RM" key = "rm.delete"/>","javascript:del();",0
)


 document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}
</script> --%>


<table>
<html:form action="ResourceAdd">

<html:hidden name="resf" property="status" />
<!--  add New Menu Here  -->


<table  border="0" cellspacing="1" cellpadding="1" >
 <tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" >
  
  
	    <logic:present name="ResourceAddForm" property="message">
			<logic:equal name="ResourceAddForm" property="message" value="-9001">
				<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="rm.itemdeletefailed"/></td></tr>
			</logic:equal>
			<logic:equal name="ResourceAddForm" property="message" value="-9003">
				<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="rm.iteminuse"/></td></tr>
			</logic:equal>
			<logic:equal name="ResourceAddForm" property="message" value="-9002">
				<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="rm.itemnotinuse"/></td></tr>
			</logic:equal>
			<logic:equal name="ResourceAddForm" property="message" value="-9004">
				<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="rm.iteminusebutdiscontinued"/></td></tr>
			</logic:equal>
		</logic:present>
		
		<logic:present name="ResourceAddForm" property="addmessage">
			<logic:equal name="ResourceAddForm" property="addmessage" value="0">
				<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="rm.itemaddedsucess"/></td></tr>
			</logic:equal>		
		</logic:present>
	
		<logic:present name="ResourceAddForm" property="updatemessage">
			<logic:equal name="ResourceAddForm" property="updatemessage" value="0">
				<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="rm.itemupdatedsucess"/></td></tr>
			</logic:equal>	
		</logic:present>
	  
  
	  <tr>
	    <td colspan="2" class="labeleboldwhite" height="30" ><bean:message bundle="RM" key="rm.viewitems"/><bean:write name="resf" property="firstcatg"  /><bean:message bundle="RM" key="rm.slash"/><bean:write name="resf" property="secondcatg"  /><bean:message bundle="RM" key="rm.slash"/><bean:write name="resf" property="thirdcatg"  /></td>
	  </tr> 
	  
	  <tr> 
	    <td  class="labelobold" height="20"><bean:message bundle="RM" key="<%=name_label %>"/></td>
	    <td  class="labelo"><bean:write name="resf" property="laborname"/></td>
	  </tr>
	  <tr> 
	    <td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.altidentifier"/></td>
	    <td  class="labele"><bean:write name="resf" property="laboridentifier" /></td>
	  </tr>
	   <tr> 
	    <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.status"/></td>
	    <td   class="labelo"><bean:write name="resf" property="status"/></td>
	  </tr>
	  
	  <tr> 
	    <td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.sellableqty"/></td>
	    <td  class="labele"><bean:write name="resf" property="sellableqty"/></td>
	  </tr>
	
	  <tr> 
	    <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.minimumqty"/></td>
	    <td  class="labelo"><bean:write name="resf" property="minimumqty"/></td>
	  </tr>
	
	  <tr> 
	    <td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.unit"/></td>
	    <td  class="labele"><bean:write name="resf" property="unit"/></td>
	  </tr>
	
	  <tr> 
	    <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.basecost"/></td>
	    <td  class="labelo"><bean:write name="resf" property="basecost"/>    </td>
	  </tr>
	  
	  <tr> 
	    <td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.cnspartno"/></td>
	    <td  class="labele"><bean:write name="resf" property="cnspartno"/></td>
	  </tr>
	  
	   <% if(resource==1)
	  	{%>
	  <tr> 
	    <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.manufacturerpartno"/></td>
	    <td  class="labelo"><bean:write name="resf" property="mfgpartno"/></td>
	  </tr>
	  <%} %>
	  
	  <% if(resource==2)
	  	{%>
	 
	  <logic:present name="Checkmanufacturer" scope="request">
	  	   <logic:equal name="Checkmanufacturer"  value="1">
			<tr> 
	  		  <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.criticality"/></td>
	  		  <td  class="labelo"><bean:write name="resf" property="criticalityname"/></td>
	 	    </tr>
		  </logic:equal>
	  </logic:present>
	  	
	  <%} %>
	    <tr> 
	    <td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.changedbydate"/></td>
	    <td  class="labele"><bean:write name="resf" property="changedby"/>&nbsp;<bean:write name="resf" property="changedate"/></td>
	  </tr>
	   <tr> 
	    <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.createdbydate"/></td>
	    <td  class="labelo"><bean:write name="resf" property="createdby"/>&nbsp;<bean:write name="resf" property="createdate"/></td>
	  </tr>
	   
	   <jsp:include page = '/Footer.jsp'>
	  	 <jsp:param name = 'colspan' value = '28'/>
	     <jsp:param name = 'helpid' value = 'roleprivilege'/>
	   </jsp:include>
	   
   
 	 </table>
   </td>
 </tr> 
</table>
</html:form>
</table>

<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>

</BODY>
</html:html>
