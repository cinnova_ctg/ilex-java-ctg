<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Displaying 2nd level resource category that is added.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<bean:define id = "maf" name = "manform" />
<bean:define id = "mfgid" name = "manform" property="id" />
<bean:define id = "mfgid1" name = "manform" property="id1" />


<%

String menu_label1;
String menu_label2;
String resource_label;
String category_label;
String mfg_label;

int resource=Integer.parseInt((String)request.getAttribute("resourceflag"));

switch(resource){
case 1:
{
	menu_label1="rm.maddsubcategory";
	menu_label2="rm.mmngmanufacturers";
	resource_label="rm.material";
	category_label="rm.mcategory";
	mfg_label="rm.msecondcategory";
	break;
}
case 2:
{
	menu_label1="rm.laddppstype";
	menu_label2="rm.lmanagesubcategory";
	resource_label="rm.labor";
	category_label="rm.lcategory";
	mfg_label="rm.lsecondcategory";
	break;
}
case 3:
{
	menu_label1="rm.faddsubsubcategory";
	menu_label2="rm.fmanagesubcategory";
	resource_label="rm.freight";
	category_label="rm.fcategory";
	mfg_label="rm.fsecondcategory";
	break;
}
case 4:
{
	menu_label1="rm.taddsubcategory";
	menu_label2="rm.tmanagetraveltype";
	resource_label="rm.travel";
	category_label="rm.tcategory";
	mfg_label="rm.tsecondcategory";
	break;
}
	default:
	{
		menu_label1="rm.maddsubcategory";
		menu_label2="rm.mmngmanufacturers";
		resource_label="rm.material";
		category_label="rm.mcategory";
		mfg_label="rm.msecondcategory";
	}

}
%>

<HEAD>


<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>
<title></title>

<%@ include  file="../Menu.inc" %>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<table width="100%%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
       <table width="" border="0" cellspacing="1" cellpadding="0" height="18">
	       <tr align="center"> 
	          <td   width=160  class="toprow1"><a href="PPSAdd.do?f=<%=request.getParameter("resourceflag")%>&id=<%= request.getParameter("id")%>&id1=<%=mfgid1 %>&function=add"  onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="RM" key="<%=menu_label1 %>"/> </a></td>
	          <td   id="pop1" width=200 class="toprow1"><a   href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="drop"><bean:message bundle="RM" key="<%=menu_label2 %>"/> </a></td>
			  <td  width=440 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		   </tr>
      </table>
    </td>
  </tr>
</table>


<SCRIPT language=JavaScript1.2>


if (isMenu) {

arMenu1=new Array(
120,
findPosX('pop1'),findPosY('pop1'),
"","",
"","",
"","",
"<bean:message bundle = "RM" key = "rm.add"/>","ManAdd.do?f=<%= request.getParameter("resourceflag")%>&id=<%= request.getParameter("id")%>&function=add",0,
"<bean:message bundle = "RM" key = "rm.update"/>","ManAdd.do?f=<%= request.getParameter("resourceflag")%>&id=<%= request.getParameter("id")%>&id1=<%=mfgid1%>&function=update",0

)

  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

</script>


<table  width="100%" border="0" cellspacing="1" cellpadding="1" >
	<tr>
	  <td width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" >
	  
	  
		    <logic:present name="ManAddForm" property="addmessage">
				<logic:equal name="ManAddForm" property="addmessage" value="0">
				<tr>
					<td   colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.addedsuccess"/></td>
			  	</tr>
				</logic:equal>
			</logic:present>
		
			<logic:present name="ManAddForm" property="updatemessage">
				<logic:equal name="ManAddForm" property="updatemessage" value="0">
				<tr>
					<td   colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.updatedsuccess"/></td>
			  	</tr>
				</logic:equal>
			</logic:present>
		  
		  
		  <tr> 
		    <td colspan="2" class="labeleboldwhite" height="30" ><bean:message bundle="RM" key="rm.view"/>&nbsp;&nbsp;<bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.under" />&nbsp;<bean:write name = "maf" property = "category"/></td>
		  </tr>
		
		  <tr> 
		    <td  class="labelobold" height="25" width=45%><bean:message bundle="RM" key="<%=category_label %>"/></td>
		    <td  class="labelo"><bean:write name = "maf" property = "category"/></td>
		  </tr>
		  
		  <tr> 
		    <td  class="labelebold" height="25"><bean:message bundle="RM" key="<%=mfg_label %>"/></td>
		    <td  class="labele"><bean:write name = "maf" property="subcatg" /></td>
		  </tr>
		  
		  <jsp:include page = '/Footer.jsp'>
		  	 <jsp:param name = 'colspan' value = '28'/>
		     <jsp:param name = 'helpid' value = 'roleprivilege'/>
		  </jsp:include>
		  
	    </table>
	 </td>
   </tr>
</table>

</BODY>

<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>

</html:html>

