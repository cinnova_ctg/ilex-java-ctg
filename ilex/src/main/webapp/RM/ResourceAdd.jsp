<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Adding item(4th level resource category).
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id = "dcrm" name = "dynamiccombo" scope = "request"/>
<bean:define id = "id" name = "resourceform" property="id" scope = "request"/>
<bean:define id = "id1" name = "resourceform" property="id1" scope = "request"/>
<bean:define id = "id2" name = "resourceform" property="id2" scope = "request"/>
<bean:define id = "resourceflag" name = "resourceform" property="resourceflag" scope = "request"/>

<%
String menu_label;
String resource_label;
String name_label;
String ident_label;

int resource=Integer.parseInt((String)request.getAttribute("resourceflag"));

switch(resource){
case 1:
{
	menu_label="rm.additems";
	resource_label="rm.material";
	name_label="rm.materialname";
	ident_label="rm.altidentifier";
	break;
}
case 2:
{
	menu_label="rm.additems";
	resource_label="rm.labor";
	name_label="rm.laborname";
	ident_label="rm.altidentifier";
	break;
}
case 3:
{
	menu_label="rm.additems";
	resource_label="rm.freight";
	name_label="rm.frieghtname";
	ident_label="rm.altidentifier";
	
	break;
}
case 4:
{
	menu_label="rm.additems";
	resource_label="rm.travel";
	name_label="rm.travelname";
	ident_label="rm.altidentifier";
	
	break;
}
	default:
	{
		menu_label="rm.additems";
		resource_label="rm.material";
		name_label="rm.materialname";
		ident_label="rm.altidentifier";
		
	}

}
%>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<TITLE></TITLE>
<%@ include  file="/Menu.inc" %>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td ><img src="/images/spacer.gif" width="1" height="1"></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
       <table width="" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
          <td  width=150 align=left class="toprow1"><a  href="ResourceAdd.do?f=<%=resourceflag %>&id=<%= id%>&id1=<%= id1%>&id2=<%= id2%>&function=add"    onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="RM" key="<%=menu_label%>"/></a></td>
		  <td  width=650 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>

<table>
<html:form action="ResourceAdd" >

<html:hidden  property="firstcatg"  />
<html:hidden  property="secondcatg"  />
<html:hidden  property="thirdcatg"  />
<html:hidden  property="flag"  />
<html:hidden property="id" />
<html:hidden property="id1"  />
<html:hidden name="ResourceAddForm" property="id2" />
<html:hidden property="id3"  />
<html:hidden property="resourceflag" />
<html:hidden property="function"  />
<html:hidden property="cnspartno"  />
<html:hidden property="updatecombovalue"  />

<table  border="0" cellspacing="1" cellpadding="1" >
<tr>
  <td  width="2" height="0">
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  
	    <logic:present name="ResourceAddForm" property="message">
			<logic:equal name="ResourceAddForm" property="message" value="0">
			<tr><td colspan="2"  class="message" height="30" ><bean:message bundle="RM" key="rm.deletedsuccess"/></td></tr>
			</logic:equal>
		</logic:present>
		
		<logic:present name="ResourceAddForm" property="addmessage">
		
			<logic:equal name="ResourceAddForm" property="addmessage" value="-9002">
			<tr><td colspan="2"  class="message" height="30" ><bean:message bundle="RM" key="rm.insertfailed"/></td></tr>
			</logic:equal>
			
			<logic:equal name="ResourceAddForm" property="addmessage" value="-9003">
			<tr><td colspan="2"  class="message" height="30" ><bean:message bundle="RM" key="rm.itemalreadyexists"/></td></tr>
			</logic:equal>	
			
			<logic:equal name="ResourceAddForm" property="addmessage" value="-90002">
			<tr><td colspan="2"  class="message" height="30" ><bean:message bundle="RM" key="rm.insertfailed"/></td></tr>
			</logic:equal>
					
		</logic:present>
	
		<logic:present name="ResourceAddForm" property="updatemessage">
		
			<logic:equal name="ResourceAddForm" property="updatemessage" value="-9001">
			<tr><td colspan="2"  class="message" height="30" ><bean:message bundle="RM" key="rm.updatefailed"/></td></tr>
			</logic:equal>
			
			<logic:equal name="ResourceAddForm" property="updatemessage" value="-9003">
			<tr><td colspan="2"  class="message" height="30" ><bean:message bundle="RM" key="rm.itemalreadyexists"/></td></tr>
			</logic:equal>	
			
			<logic:equal name="ResourceAddForm" property="updatemessage" value="-90001">
			<tr><td colspan="2"  class="message" height="30" ><bean:message bundle="RM" key="rm.updatefailed"/></td></tr>
			</logic:equal>
		
		</logic:present>
	  
	  
		  <tr>
		    <td colspan="2"class="labeleboldwhite" height="30" ><bean:message bundle="RM" key="rm.additemsunder"/><bean:write name="ResourceAddForm" property="firstcatg"  /><bean:message bundle="RM" key="rm.slash"/><bean:write name="ResourceAddForm" property="secondcatg"  /><bean:message bundle="RM" key="rm.slash"/><bean:write name="ResourceAddForm" property="thirdcatg"  /></td>
		  </tr> 
		  <tr><td colspan="2" ></td></tr>
		  <tr> 
		    <td  class="labelobold" height="20" ><bean:message bundle="RM" key="<%=name_label %>"/></td>
		    <td  class="labelo" ><html:text styleClass="textbox"  size="33" name="ResourceAddForm" property="laborname" maxlength="50"/></td>
		  </tr>
		  
		  <tr> 
		    <td  class = "labelebold" height="20"><bean:message bundle = "RM" key = "rm.altidentifier"/></td>
		    <td  class = "labele"><html:text  styleClass = "textbox" size = "12" name = "ResourceAddForm" property = "laboridentifier"  maxlength = "50"/></td>
		  </tr>
		  
		  <tr> 
		    <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.status"/></td>
		    <td  class="labelo"><html:select name="ResourceAddForm" property="status" size="1" styleClass="comboo" >
				 <html:optionsCollection name = "dcrm"  property = "statuslist"  label = "label"  value = "value" /></html:select></td>
		  </tr>
		  
		  <tr> 
		    <td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.sellableqty"/></td>
		    <td  class="labele"><html:text  styleClass="textbox" size="12" name="ResourceAddForm" property="sellableqty"  maxlength="9"/></td>
		  </tr>
		  
		  <tr> 
		    <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.minimumqty"/></td>
		    <td  class="labelo"><html:text styleClass="textbox"  size="12" name="ResourceAddForm" property="minimumqty"  maxlength="9"/></td>
		  </tr>
		
		  <tr> 
		    <td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.unit"/></td>
		    <td  class="labele"><html:text styleClass="textbox"  size="12" name="ResourceAddForm" property="unit"  maxlength="50"/></td>
		  </tr>
		
		  <tr> 
		    <td  class="labelobold" height="20"><bean:message bundle="RM" key="rm.basecost"/></td>
		    <td  class="labelo"><html:text styleClass="textbox"  size="12" name="ResourceAddForm" property="basecost"  maxlength="9"/>
		    </td>
		  </tr>
		  
	  
		  <% if(resource==1)
		  	{%>
		  <tr> 
		    <td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.manufacturerpartno"/></td>
		    <td  class="labele"><html:text  styleClass="textbox" size="12" name="ResourceAddForm" property="mfgpartno"  maxlength="50"/></td>
		  </tr>
		  <%} %>
		  
		   <% if(resource==2)
		  	{%>
		  	 <logic:present name="Checkmanufacturer" scope="request">
			   <logic:equal name="Checkmanufacturer"  value="1">
				<tr> 
			   		<td  class="labelebold" height="20"><bean:message bundle="RM" key="rm.criticality"/></td>
			   		<td  class="labele">
					    <logic:present name="criticalitylist" scope="request">
						    <logic:iterate id="clist" name="criticalitylist">
							    <bean:define id="cid" name="clist" property="criticalityid" />
							    <html:radio   name= "ResourceAddForm" property = "criticality" value = "<%= ( String ) cid %>"/><bean:write name="clist" property="criticalityname"/>
						 	</logic:iterate>
				 		</logic:present>
				 	</td> 
		  		</tr>
			  </logic:equal>
			</logic:present>
		  	
		 
		  <%} %>
		  <tr> 
		    <td colspan="2" class="buttonrow"> 
		     <html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="RM" key="rm.save"/></html:submit>
		     <html:reset property="reset" styleClass="button"><bean:message bundle="RM" key="rm.reset" /></html:reset>
		  </tr>  
		  
		   <jsp:include page = '/Footer.jsp'>
		  	 <jsp:param name = 'colspan' value = '28'/>
		     <jsp:param name = 'helpid' value = 'roleprivilege'/>
		   </jsp:include>
		  
 		</table>
 	  </td>
   </tr> 
</table>

<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>

</html:form>
</table>

</BODY>

<script>

function validate()
{
	trimFields();
	if(isBlank(document.forms[0].laborname.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].laborname,"<bean:message bundle="RM" key="<%=name_label%>"/>",errormsg);
		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].laborname,"<bean:message bundle="RM" key="<%=name_label%>"/>"))
		return false;
	}
	
	/*if(isBlank(document.forms[0].laboridentifier.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].laboridentifier,"<bean:message bundle="RM" key="<%=ident_label%>"/>",errormsg);
		
		return false;
	}*/
	
	if(! CheckQuotes(document.forms[0].laboridentifier,"<bean:message bundle="RM" key="<%=ident_label%>"/>"))
		return false;
	
	
	if(isBlank(document.forms[0].sellableqty.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].sellableqty,"<bean:message bundle="RM" key="rm.sellableqty"/>",errormsg);
		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].sellableqty,"<bean:message bundle="RM" key="rm.sellableqty"/>"))
		return false;
	}
	if(!isFloat(document.forms[0].sellableqty.value))
	{
		errormsg="<bean:message bundle="RM" key="rm.warnmsgint"/>";
		
		warningEmpty(document.forms[0].sellableqty,"<bean:message bundle="RM" key="rm.sellableqty"/>",errormsg);
		
		return false;
	}
	
	if(isBlank(document.forms[0].minimumqty.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].minimumqty,"<bean:message bundle="RM" key="rm.minimumqty"/>",errormsg);
		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].minimumqty,"<bean:message bundle="RM" key="rm.minimumqty"/>"))
		return false;
	}
	if(!isFloat(document.forms[0].minimumqty.value))
	{
		errormsg="<bean:message bundle="RM" key="rm.warnmsgint"/>";
		
		warningEmpty(document.forms[0].minimumqty,"<bean:message bundle="RM" key="rm.minimumqty"/>",errormsg);
		
		return false;
	}
	
	if( parseFloat( document.forms[0].minimumqty.value ) >= parseFloat( document.forms[0].sellableqty.value ) )
	{
		
		if( parseFloat( document.forms[0].minimumqty.value ) % parseFloat( document.forms[0].sellableqty.value ) != 0 )
		{
			alert( "<bean:message bundle = "RM" key = "rm.minmultiplesell"/>" );	
			document.forms[0].minimumqty.focus();
			return false;
		}
	}
	else
	{
		alert( "<bean:message bundle = "RM" key = "rm.minequalorgreatsell"/>" );	
		document.forms[0].minimumqty.focus();
		return false;
	}
	
	if(isBlank(document.forms[0].unit.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].unit,"<bean:message bundle="RM" key="rm.unit"/>",errormsg);
		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].unit,"<bean:message bundle="RM" key="rm.unit"/>"))
		return false;
	}
	
	if(isBlank(document.forms[0].basecost.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].basecost,"<bean:message bundle="RM" key="rm.basecost"/>",errormsg);
		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].basecost,"<bean:message bundle="RM" key="rm.basecost"/>"))
		return false;
	}
	if(!isFloat(document.forms[0].basecost.value))
	{
		errormsg="<bean:message bundle="RM" key="rm.warnmsgint"/>";
		
		warningEmpty(document.forms[0].basecost,"<bean:message bundle="RM" key="rm.basecost"/>",errormsg);
		
		return false;
	}
	
	if(<%=(resource==1)%>)
  	{
		if(isBlank(document.forms[0].mfgpartno.value))
		{
		
			errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
			
			warningEmpty(document.forms[0].mfgpartno,"<bean:message bundle="RM" key="rm.manufacturerpartno"/>",errormsg);
			
			return false;
		}
		else
		{
			if(! CheckQuotes(document.forms[0].mfgpartno,"<bean:message bundle="RM" key="rm.manufacturerpartno"/>"))
			return false;
		}
	}

}
</script>
<script>
function trimFields()
{

	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>

function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}

</script>
</html:html>




