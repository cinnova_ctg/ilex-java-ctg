<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Displaying 3rd level resource category that is added.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>

<bean:define id = "ppsf" name = "ppsform" />
<bean:define id = "ppsid" name = "ppsform" property="id" />
<bean:define id = "ppsid1" name = "ppsform" property="id1" />
<bean:define id = "ppsid2" name = "ppsform" property="id2" />

<%
String menu_label1;
String menu_label2;
String resource_label;
String mfg_label;
String pps_label;

int resource=Integer.parseInt((String)request.getAttribute("resourceflag"));


switch(resource){
case 1:
{
	
	menu_label1="rm.additems";
	menu_label2="rm.mmngsubcategory";
	resource_label="rm.material";
	mfg_label="rm.msecondcategory";
	pps_label="rm.msubcategory";
	
	break;
}
case 2:
{
	
	menu_label1="rm.additems";
	menu_label2="rm.lmngppstype";
	resource_label="rm.labor";
	mfg_label="rm.lsecondcategory";
	pps_label="rm.lppstype";
	
	
	break;
}
case 3:
{
	
	menu_label1="rm.additems";
	menu_label2="rm.fmngsubsubcategory";
	resource_label="rm.freight";
	mfg_label="rm.fsecondcategory";
	pps_label="rm.fsubsubcategory";
	
	
	break;
}
case 4:
{
	
	menu_label1="rm.additems";
	menu_label2="rm.tmngsubcategory";
	resource_label="rm.travel";
	mfg_label="rm.tsecondcategory";
	pps_label="rm.tsubcategory";
	
	
	break;
}
	default:
	{
		
		menu_label1="rm.additems";
		menu_label2="rm.mmngsubcategory";
		resource_label="rm.material";
		mfg_label="rm.msecondcategory";
		pps_label="rm.msubcategory";
		
	}

}
%>

<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">

<title></title>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<%@ include  file="/Menu.inc" %>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td ><img src="images/spacer.gif" width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
       <table width="" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
          
          <td  width=150 align=left class="toprow1"><a href="ResourceAdd.do?f=<%=request.getParameter("resourceflag")%>&id=<%= request.getParameter("id")%>&id1=<%=request.getParameter("id1") %>&id2=<%=ppsid2 %>&function=add"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','../images/about1b.gif',1);" class="drop"> <bean:message bundle="RM" key="<%=menu_label1 %>"/> </a></td>
        
		  <td  id="pop1" width="200" class="toprow1"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="drop"><bean:message bundle="RM" key="<%=menu_label2 %>"/> </a></td>
		  <td  width="450" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>

<SCRIPT language=JavaScript1.2>

if (isMenu) {
arMenu1=new Array(
100,
findPosX('pop1'),findPosY('pop1'),
"","",
"","",
"","",
"<bean:message bundle = "RM" key = "rm.add"/>","PPSAdd.do?f=<%= request.getParameter("resourceflag")%>&id=<%= request.getParameter("id")%>&id1=<%= request.getParameter("id1")%>&function=add",0,
"<bean:message bundle = "RM" key = "rm.update"/>","PPSAdd.do?f=<%= request.getParameter("resourceflag")%>&id=<%= request.getParameter("id")%>&id1=<%= request.getParameter("id1")%>&id2=<%=ppsid2 %>&function=update",0
)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

</script>
<table  border="0" cellspacing="1" cellpadding="1" >
 <tr> 
    <td width="2" height="0"></td>
    <td><table border="0" cellspacing="1" cellpadding="1" >
    
	    <logic:present name="PPSAddForm" property="addmessage">
			<logic:equal name="PPSAddForm" property="addmessage" value="0">
			<tr>
				<td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=pps_label %>"/>
		   		&nbsp;<bean:message bundle="RM" key="rm.addedsuccess"/></td>
		    </tr>
			</logic:equal>
		</logic:present>
	
		<logic:present name="PPSAddForm" property="updatemessage">
			<logic:equal name="PPSAddForm" property="updatemessage" value="0">
			<tr>
				<td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=pps_label %>"/>
		    	&nbsp;<bean:message bundle="RM" key="rm.updatedsuccess"/></td>
		    </tr>
			
			</logic:equal>
		</logic:present>
	    
	    
	    <tr>
	    	<td colspan="2" class="labeleboldwhite" height="30" ><bean:message bundle="RM" key="rm.view"/>&nbsp;<bean:message bundle="RM" key="<%=pps_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.under"/><bean:write name="ppsf" property="firstcatg"  /><bean:message bundle="RM" key="rm.slash"/><bean:write name="ppsf" property="secondcatg"  /></td>
	    </tr>
	 
	    <tr> 
	    	<td  class="labelobold" height="25" width=50%><bean:message bundle="RM" key="<%=mfg_label %>"/></td>
	    	<td  class="labelo"><bean:write name = "ppsf" property = "secondcatg"/></td>
	    </tr>
	  
	    <tr> 
	    	<td  class="labelebold" height="25"><bean:message bundle="RM" key="<%=pps_label %>"/></td>
	    	<td  class="labele"><bean:write name = "ppsf" property = "ppstype"/></td>
	    </tr>
	  
	    <jsp:include page = '/Footer.jsp'>
	  		<jsp:param name = 'colspan' value = '28'/>
	    	<jsp:param name = 'helpid' value = 'roleprivilege'/>
	    </jsp:include>
	   
 	</table>
   </td>
 </tr> 
</table>

</BODY>

<logic:present name = "refreshtree" scope = "request">
<script>
parent.ilexleft.location.reload();
</script>
</logic:present>

</html:html>
