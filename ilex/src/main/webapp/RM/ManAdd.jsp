<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Adding 2nd level resource category.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id = "dcrm" name = "dynamiccombo" scope = "request"/>
<bean:define id = "id" name = "manform" property="id" scope = "request"/>
<bean:define id = "resourceflag" name = "manform" property="resourceflag" scope = "request"/>


<%
String menu_label="";
String menu_label2="";
String resource_label="";
String category_label="";
String mfg_label="";
String select_label="";
int comboflag=0;



int resource=Integer.parseInt((String)request.getAttribute("resourceflag"));


switch(resource){
case 1:
{
	
	menu_label="rm.maddmanufacturers";
	menu_label2="rm.mdeletemanufacturers";
	resource_label="rm.material";
	category_label="rm.mcategory";
	select_label="rm.mecategory";
	mfg_label="rm.msecondcategory";
	break;
}
case 2:
{
	
	menu_label="rm.laddmanufacturers";
	menu_label2="rm.ldeletemanufacturers";
	resource_label="rm.labor";
	category_label="rm.lcategory";
	select_label="rm.lecategory";
	mfg_label="rm.lsecondcategory";
	
	break;
}
case 3:
{
	
	menu_label="rm.faddmanufacturers";
	menu_label2="rm.fdeletemanufacturers";
	resource_label="rm.freight";
	category_label="rm.fcategory";
	select_label="rm.fecategory";
	mfg_label="rm.fsecondcategory";
	
	break;
}
case 4:
{
	
	menu_label="rm.taddmanufacturers";
	menu_label2="rm.tdeletemanufacturers";
	resource_label="rm.travel";
	category_label="rm.tcategory";
	select_label="rm.tecategory";
	mfg_label="rm.tsecondcategory";
	
	break;
}
	default:
	{
		
		menu_label="rm.maddmanufacturers";
		menu_label2="rm.mdeletemanufacturers";
		resource_label="rm.material";
		category_label="rm.mcategory";
		select_label="rm.mecategory";
		mfg_label="rm.msecondcategory";
		
	}

}
%>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<TITLE></TITLE>

<script>
function del() 
{
		
		if(<%= request.getAttribute("id1")!=null %>)
		{
			var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
			var convel = confirm( msg );
			if( convel )
			{
				document.forms[0].refresh.value = "false";
				document.forms[0].action = "ManAdd.do?f=<%=resourceflag %>&id=<%=id%>&id1=<%= request.getAttribute("id1") %>&function=delete";
				document.forms[0].submit();
				return true;	
			}
			
		}
		else
		{
			alert( "<bean:message bundle="RM" key="rm.selectdeletion"/>" );
			
		}
}

</script>

<%@ include  file="../Menu.inc" %>



</head>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
	        <td  colspan=3 width=200 align=left class="toprow1" ><a href="ManAdd.do?f=<%=resourceflag %>&id=<%= id%>&function=add"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="RM" key="<%=menu_label %>"/> </a</td>
	        <td   width=200 align=left class="toprow1" ><a href="javascript:del();" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="RM" key="<%=menu_label2 %>"/> </a></td>
			<td  width=350 class="toprow1" >&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table>
<html:form      action="ManAdd">

<html:hidden  property="category" />
<html:hidden  property="refresh" />
<html:hidden property="flag"  />
<html:hidden property="id" value='<%=request.getParameter("id")%>' />
<html:hidden name="ManAddForm" property="resourceflag"  />
<html:hidden property="function" value='<%=request.getParameter("function")%>' />

<table border="0" cellspacing="1" cellpadding="1" >
<tr>
  <td width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  
	    <logic:present name = "ManAddForm" property="message">
		<logic:equal name="ManAddForm" property="message" value="0">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.deletedsuccess"/></td>
	  		</tr> 
		</logic:equal>
		<logic:equal name="ManAddForm" property="message" value="-9001">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.deletefailed"/></td>
		  	</tr> 
		</logic:equal>
		</logic:present>
	
		<logic:present name="ManAddForm" property="addmessage">
			<logic:equal name="ManAddForm" property="addmessage" value="-9002">
				<tr>
					<td   colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.insertfailed"/></td>
			    </tr> 
			</logic:equal>
			<logic:equal name="ManAddForm" property="addmessage" value="-9003">
				<tr>
					<td   colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.alreadyexists"/></td>
			 	</tr> 
			</logic:equal>
		</logic:present>
		
		<logic:present name="ManAddForm" property="updatemessage">
			<logic:equal name="ManAddForm" property="updatemessage" value="-9001">
				<tr>
					<td   colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.updatefailed"/></td>
			  	</tr> 
			</logic:equal>
			<logic:equal name="ManAddForm" property="updatemessage" value="-9003">
				<tr>
					<td   colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.alreadyexists"/></td>
			 	</tr> 
			</logic:equal>
		</logic:present>
  
		  <tr>  
		    <td colspan="2" class="labeleboldwhite" height="30" ><bean:message bundle="RM" key="rm.resourcemanager"/><bean:write name = "ManAddForm" property = "category"/></td>
		  </tr> 
		 
		  <tr> 
		    <td  class="labelobold" height="25"><bean:message bundle="RM" key="<%=category_label %>"/></td>
		    <td  class="labelo"><bean:write name = "ManAddForm" property = "category"/></td>
		  </tr>
		  
		  <tr> 
		    <td  class="labelebold" height="25"><bean:message bundle="RM" key="<%=select_label %>"/></td>
		    <td  align=left class="labele">
			<html:select name="ManAddForm" property="existcategory" size="1" styleClass="comboe" onchange = "return setValue();">
				<html:optionsCollection name = "dcrm"  property = "secondlevelcatglist"  label = "label"  value = "value" /></html:select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;or</td>	
		  </tr>
		  
		  <tr> 
		    <td  class="labelobold" height="25"><bean:message bundle="RM" key="<%=mfg_label %>"/></td>
		    <td  class="labelo">
			<html:text  styleClass="textbox" size="33" name="ManAddForm" property="subcatg" maxlength="50"/></td>
		  </tr>
		  
		  <tr> 
		    <td colspan="2" class="buttonrow"> 
		     <html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="RM" key="rm.save"/></html:submit>
		     <html:reset property="reset" styleClass="button"><bean:message bundle="RM" key="rm.reset" /></html:reset>
		  </tr>
		
		  <jsp:include page = '/Footer.jsp'>
		  	 <jsp:param name = 'colspan' value = '28'/>
		     <jsp:param name = 'helpid' value = 'roleprivilege'/>
		  </jsp:include>
  
  	  </table>
    </td>
  </tr>
</table>

<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>

<table>
	<tr>
		<td><html:text name = "ManAddForm" styleClass="textboxdummy" size="1" readonly="true" property="textboxdummy" /></td>
	</tr>
</table>

</html:form>
</table>
</BODY>

<script>

function setValue()
{
	
	comboflag=1;
	document.forms[0].refresh.value = "true";
	document.forms[0].submit();
	return true;
}


function validate()
{
	
	trimFields();
	
	if(isBlank(document.forms[0].subcatg.value))
	{
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].subcatg,"<bean:message bundle="RM" key="<%=mfg_label%>"/>",errormsg);
		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].subcatg,"<bean:message bundle="RM" key="<%=mfg_label%>"/>"))
		return false;
	}
	
	
}
</script>
<script>
function trimFields()
{

	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>

function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}

</script>

</html:html>
