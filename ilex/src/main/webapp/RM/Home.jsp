<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Displaying blank page at the click of Resource Manager.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>
<HEAD>



<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../styles/style.css" rel="stylesheet" type="text/css">
<TITLE></TITLE>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >




<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td  width="1" height="500"></td>
  </tr>
</table>

<br>

<table width="600" border="0" cellspacing="1" cellpadding="1" align="center">
 
   <jsp:include page = '/Footer.jsp'>
  	 <jsp:param name = 'colspan' value = '28'/>
     <jsp:param name = 'helpid' value = 'roleprivilege'/>
   </jsp:include>

</table>
</body>
</html:html>
