<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Adding 3rd level resource category.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id = "dcrm" name = "dynamiccombo" scope = "request"/>
<bean:define id = "id" name = "ppsform" property="id" scope = "request"/>
<bean:define id = "id1" name = "ppsform" property="id1" scope = "request"/>

<bean:define id = "resourceflag" name = "ppsform" property="resourceflag" scope = "request"/>
<%
String menu_label;
String menu_label2;
String resource_label;
String mfg_label;
String select_label;
String pps_label;


int resource=Integer.parseInt((String)request.getAttribute("resourceflag"));

switch(resource){
case 1:
{
	
	menu_label="rm.maddsubcategory";
	menu_label2="rm.mdeletesubcategory";
	resource_label="rm.material";
	mfg_label="rm.msecondcategory";
	select_label="rm.memfg";
	pps_label="rm.msubcategory";
	
	break;
}
case 2:
{
	
	menu_label="rm.laddppstype";
	menu_label2="rm.ldeleteppstype";
	resource_label="rm.labor";
	mfg_label="rm.lsecondcategory";
	select_label="rm.lemfg";
	pps_label="rm.lppstype";
	
	
	break;
}
case 3:
{
	
	menu_label="rm.faddsubsubcategory";
	menu_label2="rm.fdeletesubsubcategory";
	resource_label="rm.freight";
	mfg_label="rm.fsecondcategory";
	select_label="rm.femfg";
	pps_label="rm.fsubsubcategory";
	
	
	break;
}
case 4:
{
	
	menu_label="rm.taddsubcategory";
	menu_label2="rm.tdeletesubcategory";
	resource_label="rm.travel";
	mfg_label="rm.tsecondcategory";
	select_label="rm.temfg";
	pps_label="rm.tsubcategory";
	
	
	break;
}
	default:
	{
		
		menu_label="rm.maddsubcategory";
		menu_label2="rm.mdeletesubcategory";
		resource_label="rm.material";
		mfg_label="rm.msecondcategory";
		select_label="rm.memfg";
		pps_label="rm.msubcategory";
		
	}

}
%>

<html:html>
<HEAD>



<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>

<%@ include  file="/Menu.inc" %>
<script>
function del() 
{
		
		if(<%= request.getAttribute("id2")!=null %>)
		{
			var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
			var convel = confirm( msg );
			if( convel )
			{
				document.forms[0].refresh.value = "false";
				document.forms[0].action = "PPSAdd.do?f=<%=resourceflag %>&id=<%=id%>&id1=<%= id1%>&id2=<%= request.getAttribute("id2") %>&function=delete";
				document.forms[0].submit();
				return true;	
			}
			
		}
		else
		{
			alert( "<bean:message bundle="RM" key="rm.selectdeletion"/>" );
			
		}
}
</script>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
          <td  colspan=3 width=200 align=left class="toprow1"><a href="PPSAdd.do?f=<%=resourceflag %>&id=<%= id%>&id1=<%= id1%>&function=add"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="RM" key="<%=menu_label %>"/> </a</td>
          <td   width=200 align=left class="toprow1"><a href="javascript:del();"  onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="RM" key="<%=menu_label2 %>"/> </a></td>  
		  <td  width = 400 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
	 </table>
    </td>
  </tr>
 </table>



<table>
<html:form action="PPSAdd" >

<html:hidden  property="refresh" />
<html:hidden  property="firstcatg" />
<html:hidden  property="secondcatg" />
<html:hidden property="comboflag"  />
<html:hidden property="flag"  />
<html:hidden property="id" value='<%=request.getParameter("id")%>' />
<html:hidden property="id1" value='<%=request.getParameter("id1")%>' />
<html:hidden property="id2" value='<%=request.getParameter("id2")%>' />
<html:hidden property="resourceflag" value='<%=request.getParameter("f")%>' />
<html:hidden property="function" value='<%=request.getParameter("function")%>' />

<table  border="0" cellspacing="1" cellpadding="1" >
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" >
  
    <logic:present name = "PPSAddForm" property="message">
	<logic:equal name="PPSAddForm" property="message" value="0">
	<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=pps_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.deletedsuccess"/></td></tr> 
	</logic:equal>
	<logic:equal name="PPSAddForm" property="message" value="-9004">
	<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=pps_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.deletefailed"/></td></tr> 
	</logic:equal>
	</logic:present>

	<logic:present name="PPSAddForm" property="addmessage">
	<logic:equal name="PPSAddForm" property="addmessage" value="-9002">
	<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=pps_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.insertfailed"/></td></tr> 
	</logic:equal>
	<logic:equal name="PPSAddForm" property="addmessage" value="-9003">
	<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=pps_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.alreadyexists"/></td></tr> 
	</logic:equal>
	</logic:present>

	<logic:present name="PPSAddForm" property="updatemessage">
	<logic:equal name="PPSAddForm" property="updatemessage" value="-9001">
	<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=pps_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.updatefailed"/></td></tr> 
	</logic:equal>
	<logic:equal name="PPSAddForm" property="updatemessage" value="-9003">
	<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="RM" key="<%=pps_label %>"/>&nbsp;<bean:message bundle="RM" key="rm.alreadyexists"/></td></tr> 
	</logic:equal>
	</logic:present>
  
  	
  	 
  <tr> 
    <td colspan="2"class="labeleboldwhite" height="30" ><bean:message bundle="RM" key="rm.resourcemanager"/><bean:write name = "PPSAddForm" property = "firstcatg"/><bean:message bundle="RM" key="rm.slash"/><bean:write name = "PPSAddForm" property = "secondcatg"/></td>
  </tr> 
 
 
   <tr> 
    <td  class="labelobold" height="25"><bean:message bundle="RM" key="<%=mfg_label %>"/></td>
    <td  class="labelo"><bean:write name = "PPSAddForm" property = "secondcatg"/></td>
  </tr>
  
  <tr> 
    <td  class="labelebold" height="25"><bean:message bundle="RM" key="<%=select_label %>"/></td>
    <td  align=left class="labele">
	<html:select name="PPSAddForm" property="existmfg" size="1" styleClass="comboe" onchange = "return setValue();">
				<html:optionsCollection name = "dcrm"  property = "thirdlevelcatglist"  label = "label"  value = "value" />
				</html:select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;or</td>	
	
  </tr>
  
  <tr> 
    <td  class="labelobold" height="25"><bean:message bundle="RM" key="<%=pps_label %>"/></td>
     <td  class="labelo">
	<html:text  styleClass="textbox" size="33" name="PPSAddForm" property="ppstype" maxlength="50"/>
	</td>
  </tr>
  
<tr> 
    <td colspan="2" class="buttonrow"> 
      <html:submit property="save" onclick = "return validate();" styleClass="button"><bean:message bundle="RM" key="rm.save"/></html:submit>
      <html:reset property="reset" styleClass="button"><bean:message bundle="RM" key="rm.reset" /></html:reset></td>

  </tr>

  
  <jsp:include page = '/Footer.jsp'>
  	 <jsp:param name = 'colspan' value = '28'/>
     <jsp:param name = 'helpid' value = 'roleprivilege'/>
   </jsp:include>
 </table>
 </td>
 </tr>
</table>

<logic:present name = "refreshtree" scope = "request">
<script>
parent.ilexleft.location.reload();
</script>
</logic:present>
<table>
<tr>
	<td ><html:text name = "PPSAddForm" styleClass="textboxdummy" size="1"
readonly="true" property="textboxdummy" /></td>
</tr>
</table>
</html:form>

</table>


</BODY>
<script>
function setValue(temp)
{
	
	document.forms[0].refresh.value = "true";
	document.forms[0].submit();
	return true;
}
function validate()
{
	
	
	trimFields();
	
	if(isBlank(document.forms[0].ppstype.value))
	{
		
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].ppstype,"<bean:message bundle="RM" key="<%=pps_label%>"/>",errormsg);
		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].ppstype,"<bean:message bundle="RM" key="<%=pps_label%>"/>"))
		return false;
	}
	
}
</script>

<script>
function trimFields()
{

	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>

function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}

</script>

</html:html>
