<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Displaying menutree in the left .
*
-->
<!DOCTYPE HTML>
<%@ page import = "com.mind.common.*" %>
<%@ page import = "com.mind.common.dao.*" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id = "currState" name = "currState" scope = "request"/>
<bean:define id = "expansionState" name = "expansionState" scope = "request"/>
<bean:define id = "menustring" name = "menustring" scope = "request"/>

<bean:define id = "element" name = "element" scope = "request"/>
<bean:define id = "controller" name = "controller" scope = "request"/>

<!--  by hamid for tree search--> 
<%
	String firsttime = "";
	String searchcriteria = ""; 

	if( request.getAttribute( "firsttime" ) != null )
		firsttime = request.getAttribute( "firsttime" ).toString();

	if( request.getAttribute( "searchcriteria" ) != null )
		searchcriteria = request.getAttribute( "searchcriteria" ).toString();

	if( searchcriteria.equals( "null" ) )
		searchcriteria = "";
	
%>
<!--  by hamid for tree search--> 


<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>
<TITLE></TITLE>
</HEAD>

<title><bean:message bundle="RM" key="rm.hierarchynavigation"/></title>
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 10px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 0px;
    	white-space : nowrap;
    }

    /* Link Styles */
    a  {
    	color : #000000;
    }

    a:link  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:active  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:hover  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:visited  {
    	text-decoration : underline;
    	color : #000000;
    }
    
    .m2 {
        font-family : "Verdana";
        font-size : 10px;
        line-height: 10px;
    }    
</style>

<!--  by hamid for tree search*/--> 
<script>

function searchfunction()
{
	document.cookie = "searchrmm="+document.forms[0].searchtext.value+";";
	document.forms[0].firsttime.value = <%= firsttime %>;
	document.forms[0].action = 'MenuTreeAction.do?menucriteria=RM&firsttime=<%= firsttime %>';
	
	return true;
}

function showall()
{
	var str = '';
	document.cookie = "searchrmm="+str+";";
	document.forms[0].firsttime.value = 'false';
	document.forms[0].searchtext.value = '';
	document.forms[0].action = 'MenuTreeAction.do?menucriteria=RM&firsttime=false&searchtext=';
	document.forms[0].submit();
	return true;
}

function hideall()
{
	
	var str = 'null';
	document.cookie = "searchrmm="+str+";";
	document.forms[0].firsttime.value = 'false';
	document.forms[0].searchtext.value = '      ';	
	document.forms[0].action = 'MenuTreeAction.do?menucriteria=RM&firsttime=false&searchtext=null';
	document.forms[0].submit();
	return true;
}


function setdefault()
{
	
	
	var p = false;
	var tcookie = document.cookie;
	var st = tcookie.indexOf ("searchrmm=");
	var cookievalue;
	if( st != -1 )
	{
		st = st+9;
		st = tcookie.indexOf ( "=" , st);
      	var en = tcookie.indexOf ( ";" , st);
      	if (en > st)
        {
          cookievalue = tcookie.substring(st+1, en);
        }
        
        if( cookievalue == 'null' )
        	cookievalue = '';
        	
        document.forms[0].searchtext.value = cookievalue;
 	}
	else
		document.forms[0].searchtext.value = '';
	
	
	document.forms[0].searchtext.focus();
	
	
	
	
	var tableimg = document.getElementsByTagName('img');
	
	if( document.forms[0].searchtext.value != '' )
	{
		for (var k = 0; k < tableimg.length; k++) 
		{
			if(( tableimg[k].src.indexOf( "images/rm_materials.jpg" ) || tableimg[k].src.indexOf( "images/rm_mfg.gif" )|| tableimg[k].src.indexOf( "images/Red_Person.gif" )|| tableimg[k].src.indexOf( "images/Yellow_Person.gif" )|| tableimg[k].src.indexOf( "images/rm_sub.gif" )|| tableimg[k].src.indexOf( "images/rm_res_view.gif" )|| tableimg[k].src.indexOf( "images/rm_travel.jpg" )|| tableimg[k].src.indexOf( "images/rm_freight.jpg" ))!= -1 ) 
			{
				p = true;
				break;
			}
		}
	}
	else
	{
		p = true;
	}
	
	
	
	if( !p )
	{
		document.getElementById('content3').style.visibility = 'visible';
		document.getElementById('content3').style.display = 'block';
	}
	else
	{
		document.getElementById('content3').style.visibility = 'hidden';
		document.getElementById('content3').style.display = 'none';
	}
	
	
	
	return true;
	
}


</script>
<!--  by hamid for tree search--> 


<script language="JavaScript" src="javascript/ce_menu.js" type="text/javascript"></script>

<body onload="setdefault();MM_preloadImages('images/resource_manager_small.gif','images/rm_materials.jpg','images/rm_mfg.gif','images/Red_Person.gif','images/Yellow_Person.gif','images/rm_sub.gif','images/rm_res_view.gif','images/rm_travel.jpg','images/rm_freight.jpg');" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/sidebg.jpg">
<%
MenuElement me = ((MenuTree)(element)).getRootElement();
MenuController mc =((MenuTree)(element)).getMenuController();

/* by hamid for tree search*/

response.getWriter().print("<div><div style='visibility:hidden;' id='content1'><table><form method=get><input type='hidden' name='menucriteria' value='RM'><input type='hidden' name='firsttime' value=''><tr><td class = 'm2'><b>Name:</b>&nbsp;<input type = 'text' name = 'searchtext' size = '16' class = 'textbox' /></td><td><input name='go' type='submit' value = 'Search' class = 'button' onClick = 'return searchfunction();' /></td></tr></form></table></div>"); 
response.getWriter().print("<div style='visibility:hidden;display:none;' id='content3'><table><tr><td colspan = '2'><img name = 'Image1' src='images/2.gif' border='0' width='11' height='10'/>&nbsp;<span class = 'm2'>No records match the search criteria.</span></td></tr></table></div>"); 
response.getWriter().print("<div style='visibility:hidden;' id='content2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:hideall();'>Clear Search</a></div></div>"); 
/* by hamid for tree search*/

me.drawDirect(0,mc,response.getWriter());
%>
<script language="JavaScript">
/* by hamid for tree search*/
document.getElementById('content1').style.visibility = 'visible';
document.getElementById('content2').style.visibility = 'visible';
/* by hamid for tree search*/


document.getElementById('line0').className = 'm2';
document.getElementById('line0').style.visibility = 'visible';

currState="<%=((MenuTree)(element)).getCurrState()%>";
expansionState="<%=expansionState%>";
initNavigatorServer('RM');
</script>
</body>

</html:html>


