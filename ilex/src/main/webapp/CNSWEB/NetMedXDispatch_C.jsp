<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>
<%String ftype="D"; %>
<html:html>
<head>
	<title><bean:message bundle = "cw" key = "cw.title"/></title>
	<%@ include file = "../../Header.inc" %>
	<link rel = "stylesheet" href = "stylesCNSWEB/contingent.css" type = "text/css" />
	<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="javascript" src="javascript/JLibrary.js"></script>
	
	
	
</head>
<html:form action = "/NetMedXDispatchAction" enctype ="multipart/form-data">
<html:hidden property="lo_om_id"/>
<html:hidden property="userName"/>
<html:hidden property="lo_ad_id2"/>

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			 <td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width="50%"> 
  				    <tr>    
						<td colspan = "2" height = "30" ><H1><bean:message bundle = "cw" key = "cw.title"/></H1>
					</tr> 
					
					<tr>
  						<td colspan = "2"><img src = "images/spacer.gif" width = "1" height = "1"></td>
  					</tr>

				    <tr>    
						<td colspan = "2" class="tdTabs" style="padding: 4px;"><b><bean:message bundle = "cw" key = "cw.heading"/></b></td>
					</tr> 

					<tr>
					    <td class = "lColomn" height="20" width="20%">
					    <%if(! ftype.equals("B")){ %>
					    <bean:message bundle = "cw" key = "cw.name"/>
					    <%}else{ %>
					    <bean:message bundle = "cw" key = "cw.originatorname"/>
					    <% }%><font class="red">*</font></td>
					    <td class = "rColomn" width="30%"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_poc_name" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.companyname"/><font class="red">*</font></td>
					    <td class = "rColomn">
   					    <%if(ftype.equals("B")){ %>
   					    <html:select styleClass="SELECT" name="NetMedXDispatchForm" property = "lo_om_division">  
						<html:optionsCollection name = "codes" property = "statename" value = "value" label = "label"/> 
						</html:select>
						<%}else{ %>
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_om_division" maxlength="50" value=""/>					     
					     <%}%></td>
 					</tr>
				
					
					<tr>
						<td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.phone"/><font class="red">*</font></td>
						<td class = "rColomn">
					    <html:text  styleClass="text" size="20" name="NetMedXDispatchForm" property="lo_poc_phone" maxlength="50"/></td>
					    </tr>
				    
				    <tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.email"/><font class="red">*</font></td>
					    <td class = "rColomn">
					    <html:text  styleClass="text" size="20" name="NetMedXDispatchForm" property="lo_poc_email" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.sendrequest"/></td>
					    <td class = "rColomn">
  						<html:checkbox name="NetMedXDispatchForm" property ="lo_nd_send_copy" value="Y">
  						</html:checkbox>
					    </td>
 					</tr>
 					<%if(!ftype.equals("A")){ %>
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.copyemailrequest"/></td>
					    <td class = "rColomn">
					    <html:textarea  styleClass="TEXTAREA" cols="30" rows="5" name="NetMedXDispatchForm" property="lo_nd_send_email"/></td>
 					</tr>
 					<%}%>
 					<%if(!ftype.equals("B")){ %> 					
   				    <tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.sitelocation"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_site_location" maxlength="50"/></td>
 					</tr>
 					<%if(!ftype.equals("A")){ %>
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.pono"/></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_nd_po_no" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.custno"/></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_nd_cust_no" maxlength="50"/></td>
 					</tr>
 					<%}%>
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.address"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_address11" maxlength="50"/></td>
 					</tr>
 					<%}else{%>
				    <tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.customername"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_cust_name" maxlength="50"/></td>
 					</tr>

 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.aegis.ticketnumber"/></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_aegisticket_number" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.ponumber"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_po_number" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.custticketno"/></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_custticket_number" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.siteaddress"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_address11" maxlength="50"/></td>
 					</tr>
 					<%}%>
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.city"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_city1" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.state"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:select styleClass="SELECT" name="NetMedXDispatchForm" property = "lo_ad_state1">  
						<html:optionsCollection name = "codes" property = "statename" value = "value" label = "label"/> 
						</html:select>
 					</tr>

					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.zipcode"/><font class="red"> *</font></td>
					    <td class = "rColomn"><html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_zip_code1" maxlength="50"/></td>
					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.arrivaldate"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="8" name="NetMedXDispatchForm" property = "lo_nd_arrival_date" readonly = "true" />
 		  		        <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].lo_nd_arrival_date, document.forms[0].lo_nd_arrival_date, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
 		  		        </td>
 		  		    </tr>
 		  		    
 		  		    <tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.arrivaltime"/></td>
					    <td class = "rColomn"> 
					    <html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_time_hour">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_time_minute">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_time_option">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
					</tr>    
					<%if(!ftype.equals("A")){ %>
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.arrivalwin"/></td>
					    <td class = "rColomn"> 
					    <bean:message bundle = "cw" key = "cw.between"/></td>
					</tr>
					
					<tr>
					    <td class = "lColomn" height="20">&nbsp;</td>
					    <td class = "rColomn"> 
					    <html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_bw_hour">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_bw_minute">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_bw_option">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
					</tr>
					
					<tr>
					    <td class = "lColomn" height="20">&nbsp;</td>
					    <td class = "rColomn"> 
					    <bean:message bundle = "cw" key = "cw.and"/></td>
					</tr>
					
					<tr>
					    <td class = "lColomn" height="20">&nbsp;</td>
					    <td class = "rColomn">
					    <html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_and_hour">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_and_minute">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_and_option">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
						</td>
					</tr>
					<%} %>
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.callcriticality"/><font class="red">*</font></td>
					    <td class = "rColomn"><html:select name="NetMedXDispatchForm" property = "lo_nd_call_criticality">  
						<html:optionsCollection name = "codes" property = "criticalityname" value = "value" label = "label"/> 
						</html:select>
						</td>
					</tr>
 					<%if(ftype.equals("D")){ %> 										
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.itemcate"/><font class="red">*</font></td>
					    <td class = "rColomn"><html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_nd_item_category" maxlength="50"/></td>
					</tr>
					<%} %>
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.summary"/></td>
					    <td class = "rColomn">
					    <html:textarea  styleClass="TEXTAREA" cols="30" rows="5" name="NetMedXDispatchForm" property="lo_nd_summary"/>
					    </td>
 					</tr>
 					
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.specinst"/></td>
					    <td class = "rColomn">
					    <html:textarea  styleClass="TEXTAREA" cols="30" rows="5" name="NetMedXDispatchForm" property="lo_nd_spec_inst"/>
					    </td>
 					</tr>
 					<%if(ftype.equals("C") ||ftype.equals("D")){ %> 
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.invcust"/></td>
					    <td class = "rColomn">
  						<html:checkbox name="NetMedXDispatchForm" property = "lo_nd_invoice_cust"  value="Y">
  						</html:checkbox>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.altname"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_name2" maxlength="50"/>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.altaddress"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_address12" maxlength="50"/>
					    </td>
 					</tr>
					
					
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.altcity"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_city2" maxlength="50"/>
					    </td>
 					</tr>
					
					
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.altstate"/></td>
					    <td class = "rColomn"> 
					    <html:select name="NetMedXDispatchForm" property = "lo_ad_state2">  
						<html:optionsCollection name = "codes" property = "statename" value = "value" label = "label"/> 
						</html:select>
					    </td>
 					</tr>
					
					
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.altzipcode"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_zip_code2" maxlength="50"/>
					    </td>
 					</tr>
					<%} %>

					<%if(ftype.equals("B")) { %> 
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.underwarranty"/></td>
					    <td class = "rColomn">
  						<html:checkbox name="NetMedXDispatchForm" property = "lo_under_warranty"  value="Y">
  						</html:checkbox>
					    </td>
 					</tr>
 					<%} %> 					
					<tr>
					    <td class = "lColomnbold" height="20"><bean:message bundle = "cw" key = "cw.primarypoc"/></td>
					    <td class = "rColomn"> 
					    &nbsp;
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.primarypocname"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_first_name1" maxlength="50"/>
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_last_name1" maxlength="50"/>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.primarypocphone"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_phone11" maxlength="50"/>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomnbold" height="20">
					    <bean:message bundle = "cw" key = "cw.secpoc"/>
					    </td>
					    <td class = "rColomn"> 
					    &nbsp;
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.secpocname"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_first_name2" maxlength="50"/>
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_last_name2" maxlength="50"/>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.secpocphone"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_phone12" maxlength="50"/>
					    </td>
 					</tr>
					<%if(ftype.equals("A")) { %> 
  					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.standby"/></td>
					    <td class = "rColomn">
  						<html:checkbox name="NetMedXDispatchForm" property = "lo_Standby"  value="Y">
  						</html:checkbox>
					    </td>
 					</tr>
 					<%} %>
 					<%if(ftype.equals("B")) { %> 
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.invcust"/></td>
					    <td class = "rColomn">
  						<html:checkbox name="NetMedXDispatchForm" property = "lo_nd_invoice_cust">
  						</html:checkbox>
					    </td>
 					</tr>
 					<%} %>


 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.fileattach"/><%if(ftype.equals("A")){ %>  1<%}%></td>
					    <td class = "rColomn"> 
					    	<html:file property = "filename"/>
	    				</td>   
 					</tr>
 					<%if(ftype.equals("A")){ %> 
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.fileattach"/> 2</td>
					    <td class = "rColomn"> 
					    	<html:file property = "filename2"/>
	    				</td>   
 					</tr>

 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.fileattach"/> 3</td>
					    <td class = "rColomn"> 
					    	<html:file property = "filename3"/>
	    				</td>   
 					</tr>
 					
 					<% }%>
 					
 					<tr> 
					     <td class = "lColomn" height="20">&nbsp;</td>
					     <td class = "rColomn"> 
					     <html:submit property="save" styleClass="button"  onclick="return validate();">
					     <bean:message bundle="cw" key="cw.save"/></html:submit>
					     <html:reset property="reset" styleClass="button">
					     <bean:message bundle="cw" key="cw.cancel"/>
					     </html:reset>
					</tr>
 					

 		  		        
 		  		        
 		  		 
 					
 				</table>
			</td>
		</tr>
</table>
</body>
</html:form>
</html:html>
<script>
function validate()
{
	trimFields();
	if(! chkBlank(document.forms[0].lo_om_division,"<bean:message bundle="cw" key="cw.companyname"/>")) return false;		

	if(<%=!ftype.equals("B")%>)
		if(! chkBlank(document.forms[0].lo_site_location,"<bean:message bundle="cw" key="cw.sitelocation"/>"))return false;
	
	if(! chkBlank(document.forms[0].lo_nd_arrival_date,"<bean:message bundle="cw" key="cw.arrivaldate"/>")) return false;		
	if(! chkCombo(document.forms[0].lo_nd_arrival_time_hour,"<bean:message bundle="cw" key="cw.arrivaltime"/>"))return false;			
	if(! chkCombo(document.forms[0].lo_nd_arrival_time_minute,"<bean:message bundle="cw" key="cw.arrivaltime"/>"))return false;			

	if(! chkCombo(document.forms[0].lo_nd_call_criticality,"<bean:message bundle="cw" key="cw.callcriticality"/>"))return false;		

	if(<%=!ftype.equals("D")%>)
		if(! chkBlank(document.forms[0].lo_nd_item_category,"<bean:message bundle="cw" key="cw.itemcate"/>"))return false;		
		 
	if(! chkBlank(document.forms[0].lo_nd_summary,"<bean:message bundle="cw" key="cw.summary"/>")) return false;				
	
	return true;
}//end function validate()
</script>


<script>
function trimFields()
{
	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}


function trimBetweenString(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!=" ")	nstr=nstr+str.substring(i,i+1);
	}// END OF FOR
	return nstr;
}

function removeHiffen(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!="-")	nstr=nstr+str.substring(i,i+1);
	}// END OF FOR
	return nstr;
}


function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkInteger(obj,label){
	if(!isInteger(removeHiffen(obj.value)))	{	
		alert("Only numeric values are allowed in "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkAlphabetic(obj,label){
	if(!isAlphabetic(trimBetweenString(obj.value)))	{	
		alert("Only alphabets are allowed in "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkCombo(obj,label){
	if(obj.value<0)	{	
		alert("Please select "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkEmail(obj,label){
	if(!isEmail(obj.value))	{	
		alert("Invalid "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function chkRadio(obj,label){
	if(obj.value==""){	
		alert("Please select "+label);	
		//obj.focus();
		return false;
	}
	return true;
}
function CheckQuotes (obj,label){
	if(invalidChar(obj.value,label))
	{		
		obj.focus();
		return false;
	}
	return true;
}

</script>
