<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>


<html:html>
<head>
	<title><bean:message bundle = "cw" key = "cw.title"/></title>
	<%@ include file = "../../Header.inc" %>
	<link rel = "stylesheet" href = "stylesCNSWEB/contingent.css" type = "text/css" />
	<script language="JavaScript" src="javascript/popcalendar.js"></script>
</head>
<html:form action = "/CnsLogin">
<html:hidden property="responsepage" value="http://localhost/cnssite/f_cp_search_profile1.php" />
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			 <td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" > 
  				    <tr>    
						<td colspan = "2" height = "30" ><H1><bean:message bundle = "cw" key = "cw.title"/></H1>
					</tr> 
					
					<tr>
  						<td colspan = "2"><img src = "images/spacer.gif" width = "1" height = "1"></td>
  					</tr>
  					<tr><td  colspan="2"  height="30" >
  					
  					<logic:present  name="CnsLoginForm" property="passwordsmismatchmessage">
						<logic:equal  name="CnsLoginForm" property="passwordsmismatchmessage" value="9991">
						<bean:message bundle="cw" key="cw.passwordsdoesnotmatch"/></td>
		 				</logic:equal>
					</logic:present>
					
					<logic:present  name="CnsLoginForm" property="failmessage">
						<logic:equal  name="CnsLoginForm" property="failmessage" value="9992">
						<bean:message bundle="cw" key="cw.authenticationfailed"/></td>
		 				</logic:equal>
					</logic:present>
					</tr>

				    <tr>    
						<td colspan = "2" class="tdTabs" style="padding: 4px;"><b><bean:message bundle = "cw" key = "cw.loginform"/></b></td>
					</tr> 

					<tr>
					    <td class = "lColomn" height="20" ><bean:message bundle = "cw" key = "cw.selectusername"/><font class="red">*</font></td>
					    <td class = "rColomn" > 
					    <html:text  styleClass="text" size="20" name="CnsLoginForm" property="username" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.password"/><font class="red">*</font></td>
					    <td class = "rColomn">
					    <html:password  styleClass="text" size="20" name="CnsLoginForm" property="password" maxlength="50"/></td>
 					</tr>
				
					
					<tr>
						<td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.confirmpassword"/><font class="red">*</font></td>
						<td class = "rColomn">
					    <html:password  styleClass="text" size="20" name="CnsLoginForm" property="confirmpassword" maxlength="50"/></td>
					</tr>
				    
				    <tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.emailforpwdnotification"/><font class="red">*</font></td>
					    <td class = "rColomn">
					    <html:text  styleClass="text" size="25" name="CnsLoginForm" property="email" maxlength="50"/></td>
 					</tr>
 					
 					
 					
 					<tr> 
					     <td class = "lColomn" height="20">&nbsp;</td>
					     <td class = "rColomn"> 
					     <html:submit property="signup" styleClass="button">
					     <bean:message bundle="cw" key="cw.signup"/></html:submit>
					    
					</tr>
 					

 					
 				</table>
			</td>
		</tr>
</table>
</body>
</html:form>
</html:html>
