<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<bean:define id = "codes" name = "dynamiccomboCNSWEB" scope = "request"/>

<html:html>
<head>
	<title><bean:message bundle = "cw" key = "cw.title"/></title>
	<%@ include file = "../../Header.inc" %>
	<link rel = "stylesheet" href = "stylesCNSWEB/contingent.css" type = "text/css" />
	<script language="JavaScript" src="javascript/popcalendar.js"></script>
</head>
<html:form action = "/NetMedXDispatchAction">

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			 <td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width="50%"> 
  				    <tr>    
						<td colspan = "2" height = "30" ><H1><bean:message bundle = "cw" key = "cw.title"/></H1>
					</tr> 
					
					<tr>
  						<td colspan = "2"><img src = "images/spacer.gif" width = "1" height = "1"></td>
  					</tr>

				    <tr>    
						<td colspan = "2" class="tdTabs" style="padding: 4px;"><b><bean:message bundle = "cw" key = "cw.heading"/></b></td>
					</tr> 

					<tr>
					    <td class = "lColomn" height="20" width="20%"><bean:message bundle = "cw" key = "cw.originatorname"/><font class="red">*</font></td>
					    <td class = "rColomn" width="30%"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_origi_name" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.companyname"/><font class="red">*</font></td>
					    <td class = "rColomn">
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_comp_name" maxlength="50"/></td>
 					</tr>
				
					
					<tr>
						<td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.phone"/><font class="red">*</font></td>
						<td class = "rColomn">
					    <html:text  styleClass="text" size="20" name="NetMedXDispatchForm" property="lo_poc_phone" maxlength="50"/></td>
					    </tr>
				    
				    <tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.email"/><font class="red">*</font></td>
					    <td class = "rColomn">
					    <html:text  styleClass="text" size="20" name="NetMedXDispatchForm" property="lo_poc_email" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.sendrequest"/></td>
					    <td class = "rColomn">
  						<html:checkbox name="NetMedXDispatchForm" property = "lo_nd_send_copy">
  						</html:checkbox>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.copyemailrequest"/></td>
					    <td class = "rColomn">
					    <html:textarea  styleClass="TEXTAREA" cols="30" rows="5" name="NetMedXDispatchForm" property="lo_nd_send_email"/></td>
 					</tr>
 					
   				    <tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.customername"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_cust_name" maxlength="50"/></td>
 					</tr>

 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.aegis.ticketnumber"/></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_aegisticket_number" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.ponumber"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_po_number" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.custticketno"/></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_custticket_number" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.siteaddress"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text  styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_site_address" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.city"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_ad_city1" maxlength="50"/></td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.state"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:select styleClass="SELECT" name="NetMedXDispatchForm" property = "lo_ad_state1">  
						<html:optionsCollection name = "codes" property = "statename" value = "value" label = "label"/> 
						</html:select>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.arrivaldate"/><font class="red">*</font></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="8" name="NetMedXDispatchForm" property = "lo_nd_arrival_date" readonly = "true" />
 		  		        <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].lo_nd_arrival_date, document.forms[0].lo_nd_arrival_date, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
 		  		        </td>
 		  		    </tr>
 		  		    
 		  		    <tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.arrivaltime"/></td>
					    <td class = "rColomn"> 
					    <html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_time_hour">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_time_minute">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_time_option">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
					</tr>    
					
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.arrivalwin"/></td>
					    <td class = "rColomn"> 
					    <bean:message bundle = "cw" key = "cw.between"/></td>
					</tr>
					
					<tr>
					    <td class = "lColomn" height="20">&nbsp;</td>
					    <td class = "rColomn"> 
					    <html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_bw_hour">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_bw_minute">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_bw_option">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
					    </td>
					</tr>
					
					<tr>
					    <td class = "lColomn" height="20">&nbsp;</td>
					    <td class = "rColomn"> 
					    <bean:message bundle = "cw" key = "cw.and"/></td>
					</tr>
					
					<tr>
					    <td class = "lColomn" height="20">&nbsp;</td>
					    <td class = "rColomn">
					    <html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_and_hour">  
						<html:optionsCollection name = "codes" property = "hourname" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_and_minute">  
						<html:optionsCollection name = "codes" property = "minutename" value = "value" label = "label"/> 
						</html:select>
						<html:select name="NetMedXDispatchForm" property = "lo_nd_arrival_win_and_option">  
						<html:optionsCollection name = "codes" property = "optionname" value = "value" label = "label"/> 
						</html:select>
						</td>
					</tr>
					
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.callcriticality"/><font class="red">*</font></td>
					    <td class = "rColomn"><html:select name="NetMedXDispatchForm" property = "lo_nd_call_criticality">  
						<html:optionsCollection name = "codes" property = "criticalityname" value = "value" label = "label"/> 
						</html:select>
						</td>
					</tr>
					
					
					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.summary"/></td>
					    <td class = "rColomn">
					    <html:textarea  styleClass="TEXTAREA" cols="30" rows="5" name="NetMedXDispatchForm" property="lo_nd_summary"/>
					    </td>
 					</tr>
 					
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.specinst"/></td>
					    <td class = "rColomn">
					    <html:textarea  styleClass="TEXTAREA" cols="30" rows="5" name="NetMedXDispatchForm" property="lo_nd_spec_inst"/>
					    </td>
 					</tr>

 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.underwarranty"/></td>
					    <td class = "rColomn">
  						<html:checkbox name="NetMedXDispatchForm" property = "lo_under_warranty">
  						</html:checkbox>
					    </td>
 					</tr>

					<tr>
					    <td class = "lColomnbold" height="20"><bean:message bundle = "cw" key = "cw.primarypoc"/></td>
					    <td class = "rColomn"> 
					    &nbsp;
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.primarypocname"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_first_name1" maxlength="50"/>
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_last_name1" maxlength="50"/>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.primarypocphone"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_phone11" maxlength="50"/>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomnbold" height="20">
					    <bean:message bundle = "cw" key = "cw.secpoc"/>
					    </td>
					    <td class = "rColomn"> 
					    &nbsp;
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.secpocname"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_first_name2" maxlength="50"/>
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_last_name2" maxlength="50"/>
					    </td>
 					</tr>
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.secpocphone"/></td>
					    <td class = "rColomn"> 
					    <html:text styleClass="text" size="25" name="NetMedXDispatchForm" property="lo_pc_phone12" maxlength="50"/>
					    </td>
 					</tr>
 					
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.invcust"/></td>
					    <td class = "rColomn">
  						<html:checkbox name="NetMedXDispatchForm" property = "lo_nd_invoice_cust">
  						</html:checkbox>
					    </td>
 					</tr>
 					
					
 					
 					<tr>
					    <td class = "lColomn" height="20"><bean:message bundle = "cw" key = "cw.fileattach"/></td>
					    <td class = "rColomn"> 
					    	<html:file property = "filename"/>
	    				</td>   
 					</tr>
 					
 					<tr> 
					     <td class = "lColomn" height="20">&nbsp;</td>
					     <td class = "rColomn"> 
					     <html:button property="save" styleClass="button">
					     <bean:message bundle="cw" key="cw.save"/></html:button>
					     <html:reset property="reset" styleClass="button">
					     <bean:message bundle="cw" key="cw.cancel"/>
					     </html:reset>
					</tr>
 					

 		  		        
 		  		        
 		  		 
 					
 				</table>
			</td>
		</tr>
</table>
</body>
</html:form>
</html:html>
0