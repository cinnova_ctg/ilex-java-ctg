<%


if(request.getAttribute("appendixid")!=null) 	appendixid = request.getAttribute("appendixid").toString(); 
if(request.getAttribute("viewjobtype")!=null) 	viewjobtype = request.getAttribute("viewjobtype").toString(); 
if(request.getAttribute("ownerId")!=null) 	ownerId =  request.getAttribute("ownerId").toString(); 
if(request.getAttribute("nettype")!=null) 	nettype = request.getAttribute("nettype").toString(); 
if(request.getAttribute("loginUserId")!=null) loginUserId = request.getAttribute("loginUserId").toString(); 
/*
System.out.println("in appendixDashboardMenu appendixid = "+appendixid);
System.out.println("in appendixDashboardMenu viewjobtype = "+viewjobtype);
System.out.println("in appendixDashboardMenu ownerid = "+ownerId);
System.out.println("in appendixDashboardMenu nettype = "+nettype);
System.out.println("in appendixDashboardMenu loginUserId = "+loginUserId);
*/
%>

<script>
//functions for netmadixDashboard:Start
function viewdocuments() {
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdashboarddocument&ref1=view&Id=<%=appendixid%>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerId%>&fromPage=NetMedX";
	document.forms[0].submit();
	return true;
}
function upload() {
	document.forms[0].action = "UploadAction.do?ref=FromAppendixDashboard&ref1=view&Id=<%=appendixid%>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerId%>&fromPage=NetMedX";
	document.forms[0].submit();
	return true;
}
function sendemail() {
	document.forms[0].action = "EmailAction.do?Type=PRMAppendix&Appendix_Id=<%=appendixid%>&viewjobtype=<%=viewjobtype%>&ownerId=<%=ownerId%>&fromPage=NetMedX";
	document.forms[0].submit();
	return true;	
}

function openreportschedulewindow(report_type) {
	
	str = "ReportScheduleWindow.do?report_type="+report_type+"&typeid=<%= appendixid%>&fromPage=NetMedX";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 500, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}
function view(v) {
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=NetMedX&from_type=PRM&Id=<%= appendixid%>";
	document.forms[0].submit();
	return true;	
}

function openreportschedulewindowdispatch(report_type) {
	str = "ReportScheduleWindow.do?report_type="+report_type+"&fromPage=dispatch";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 500, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}

function summarywindow(report_type) {
	document.forms[0].target = "_self";
	document.forms[0].action = "SummaryWindow.do?nettype=<%=nettype%>&ownerId=<%=ownerId%>&report_type="+report_type;
	document.forms[0].submit();
	return;
}
function ownersummarywindow() {
	document.forms[0].target = "_self";
	document.forms[0].action = "OwnerSummaryWindowAction.do?nettype=<%=nettype%>&ownerId=<%=ownerId%>";
	document.forms[0].submit();
	return;
}
//function add for update report
function ReportRegenarate(){

 var url="ReportGeneration.do?firsttime=true";

 
	window.location.href=url;

}


//function for netmadixDashboard:End
</script>