<%
	String Labor_Id = "";

	if( request.getAttribute( "Labor_Id" ) != null )
		Labor_Id = ( String )request.getAttribute( "Labor_Id" );
	
%>

<script>

function laborDel() 
{
		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "ResourceListAction.do?type=Delete&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&Labor_Id=<%= Labor_Id %>&Activity_Id=<%= Activity_Id %>&viewL=L";
			document.forms[0].submit();
			return true;	
		}
}

function laborAddComment()
{

	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&addendum_id=<%=addendum_id%>&Type=pm_resL&ref=<%=chkaddendum%>&Resource_Id=<%= Labor_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}
	else
	{%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<%=addendum_id%>&Type=pm_resL&ref=<%=chkaddendum%>&Resource_Id=<%= Labor_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}%>
	
	document.forms[0].submit();
	return true;  
}

function laborManageSow()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'ManageSOWAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resL&Id=<%= Labor_Id %>&Activity_Id=<%= Activity_Id %>';
	
	<%}
	else
	{%>
		document.forms[0].action = 'ManageSOWAction.do?ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resL&Id=<%= Labor_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}%>
	
	document.forms[0].submit();
	return true;
}

function laborManageAssumption()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'ManageAssumptionAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resL&Id=<%= Labor_Id %>&Activity_Id=<%= Activity_Id %>'
	<%}
	else
	{%>
		document.forms[0].action = 'ManageAssumptionAction.do?ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resL&Id=<%= Labor_Id %>&Activity_Id=<%= Activity_Id %>'
	<%}%>
	
	document.forms[0].submit();
	return true;
}

function laborViewComment()
{
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<%=addendum_id%>&Type=pm_resL&ref=<%=chkaddendum%>&Id=<%= Labor_Id %>&Activity_Id=<%= Activity_Id %>';
	document.forms[0].submit();
	return true;
}

function laborViewDetails() {
		document.forms[0].action = 'LaborDetailAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Labor_Id=<%=Labor_Id%>&Activity_Id=<%=Activity_Id%>';
		document.forms[0].submit();
		return true;
}

</script>