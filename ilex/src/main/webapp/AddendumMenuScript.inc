<SCRIPT language = JavaScript1.2>
var canNotSendNonApprovedAddendum = '<bean:message bundle = "PRM" key = "prm.addendum.sendemail.noprivilegestatus"/>';
var addendumCRS = '<bean:message bundle = "PRM" key = "addendum.detail.upload.state"/>';
if (isMenu) {
arMenu1 = new Array(
120,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",

	<%if((String)request.getAttribute( "addendumStatusList" ) != "" ) {%>
		"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",1,
	<%}else{%>
		"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",0,
	<%}%>	
	"Addendum","#",1,
	"<bean:message bundle = "PRM" key = "prm.updateschedule"/>","javascript:alert('<bean:message bundle = "PRM" key = "prm.CannotupdateJobschedulefordefault"/>')",0,
	"Documents","#",1,
	<%if(job_Status.equals("Pending Customer Review")) {%>
				"Upload File","EntityHistory.do?function=modifyDocument&entityId=<%=Job_Id%>&entityType=Addendum&appendixid=<%=Appendix_Id%>",0
	<%}else{%>
				"Upload File","javascript:alert(addendumCRS)",0
	<%}%>	
	//"Job Notes","JobNotesAction.do?jobId=<%=Job_Id%>&appendixid=<%=Appendix_Id%>",0		
)


<%if((String)request.getAttribute( "addendumStatusList" ) != "" ) {%>
	arMenu1_1=new Array(
		<%= (String) request.getAttribute( "addendumStatusList" ) %>
	)
<%}%>

arMenu1_2=new Array(
<%if(!job_Status.equals("Draft") && !job_Status.equals("Review") && !job_Status.equals("Cancelled")) {%>
	"Send","javascript:addendumSendEmail();",0,
<%}else{%>
	"Send","javascript:alert(canNotSendNonApprovedAddendum);",0,
<%}%>	
	"View","#",1,
	"Manage Uplifts","ManageUpliftAction.do?ref=addendumUplift&Job_Id=<%=Job_Id%>&Appendix_Id=<%=Appendix_Id%>",0
)
<%if(!job_Status.equals("Draft") && !job_Status.equals("Review") && !job_Status.equals("Cancelled")) {%>
	arMenu1_2_2 = new Array(
		"PDF","javascript:addendumViewContractDocument(<%=Job_Id%>,'pdf');",0
	)

<%}else{%>
	<% if(jobViewtype.equals("All")) {%>
		arMenu1_2_2=new Array(
			"PDF","javascript:addendumViewContractDocument(<%=Job_Id%>, 'pdf');",0,
			"RTF","javascript:addendumViewContractDocument(<%=Job_Id%>, 'rtf');",0,
			"HTML","javascript:addendumViewContractDocument(<%=Job_Id%>, 'html');",0
		)
	<%}else{%>
		arMenu1_2_2=new Array(
			"PDF","javascript:addendumViewContractDocument(<%=Job_Id%>, 'pdf');",0,
			"EXCEL","javascript:addendumViewContractDocument(<%=Job_Id%>, 'excel');",0
		)
	<%}%>
<%}%>	

	arMenu1_4=new Array(
	"Addendum History","EntityHistory.do?entityId=<%=Job_Id%>&entityType=Addendum&function=viewHistory&appendixid=<%=Appendix_Id%>",0,	
	"Support Documents","#",1
	)
	
	arMenu1_4_2 = new Array(
	"Upload","EntityHistory.do?entityId=<%=Job_Id%>&jobId=<%=Job_Id%>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Addendum",0,
	"History","EntityHistory.do?entityType=Appendix Supporting Documents&jobId=<%=Job_Id%>&linkLibName=Addendum&function=supplementHistory",0
	)
	
arMenu2=new Array(
120,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
"Update","javascript:addendumManageStatus( 'update' )",0,
"Send","javascript:addendumSendStatusReport()",0,
"View","javascript:addendumManageStatus( 'view' )",0
)

arMenu3=new Array(
120,
findPosX( 'pop3' ),findPosY( 'pop3' ),
"","",
"","",
"","",
"<bean:message bundle = "pm" key = "job.detail.menu.viewall"/>","javascript:addendumViewComments();",0,
<%if(job_Status.equals("Cancelled")){%>
	"<bean:message bundle = "pm" key = "job.detail.menu.addcomment"/>","javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.addcomment.cancelled"/>')",0
<%}else{%>
	"<bean:message bundle = "pm" key = "job.detail.menu.addcomment"/>","javascript:addendumAddComment();",0
<%}%>
)

document.write("<SCRIPT LANGUAGE = 'JavaScript1.2' SRC = 'javascript/hierMenus.js'><\/SCRIPT>");
}
</SCRIPT>