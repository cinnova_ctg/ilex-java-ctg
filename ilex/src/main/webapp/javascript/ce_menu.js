// Global variables
var currState = "";
var highlight_line = "";
var expansionState = "";
var currID = 0;
var blockID = 0;

// images
var collapsedWidget = new Image ();
collapsedWidget.src = "images/ce_menu/oplus.gif";
var collapsedWidgetStart = new Image ();
collapsedWidgetStart.src = "images/ce_menu/oplusStart.gif";
var collapsedWidgetStartTop = new Image ();
collapsedWidgetStartTop.src = "images/ce_menu/oplusStartTop.gif";
var collapsedWidgetEnd = new Image ();
collapsedWidgetEnd.src = "images/ce_menu/oplusEnd.gif";

var expandedWidget = new Image ();
expandedWidget.src = "images/ce_menu/ominus.gif";
var expandedWidgetStart = new Image ();
expandedWidgetStart.src = "images/ce_menu/ominusStart.gif";
var expandedWidgetStartTop = new Image ();
expandedWidgetStartTop.src = "images/ce_menu/ominusStartTop.gif";
var expandedWidgetEnd = new Image ();
expandedWidgetEnd.src = "images/ce_menu/ominusEnd.gif";

var nodeWidget = new Image ();
nodeWidget.src = "images/ce_menu/onode.gif";
var nodeWidgetEnd = new Image ();
nodeWidgetEnd.src = "images/ce_menu/onodeEnd.gif";

var emptySpace = new Image ();
emptySpace.src = "images/ce_menu/oempty.gif";
var chainSpace = new Image ();
chainSpace.src = "images/ce_menu/ochain.gif";


/*********************************************************************/

// Switch state from expaned to/from collapsed
function swapState (currState, currVal, n)
{
 var newState = currState.substring(0,n);
 newState += currVal^1; // XOR - swap it
 newState += currState.substring(n+1,currState.length);
 return newState;
}

/*********************************************************************/

function getExpandedWidgetState (imgURL)
{

 if (imgURL.indexOf("StartTop") != -1)
   {
     return expandedWidgetStartTop.src;
   }
 if (imgURL.indexOf("Start") != -1)
   {
     return expandedWidgetStart.src;
   }
 if (imgURL.indexOf("End") != -1)
   {
     return expandedWidgetEnd.src;
   }
 return expandedWidget.src;
}

/*********************************************************************/

function getCollapsedWidgetState (imgURL)
{ 
 if (imgURL.indexOf("StartTop") != -1)
   {
     return collapsedWidgetStartTop.src;
   }

 if (imgURL.indexOf("Start") != -1)
   {
     return collapsedWidgetStart.src;
   }
 if (imgURL.indexOf("End") != -1)
   {
     return collapsedWidgetEnd.src;
   }
 return collapsedWidget.src;
}

/*********************************************************************/

// Expand or collapse block and switch image
function  toggle(img, blockNum,modname)
{
 var newString = "";
 var expanded, n;

 expanded = currState.charAt(blockNum);

	
	//if(blockNum!=0)
	 currState = swapState(currState,expanded, blockNum);
 
 
 
 // Save as cookie 
 document.cookie = "currState"+modname+" = " + currState + " ; ";
 if (expanded == 0)
  {
   document.getElementById("block"+blockNum).style.display = "block";

	
   img.src = getExpandedWidgetState (img.src)

  }
 else
  {
   document.getElementById("block"+blockNum).style.display = "none";
   img.src = getCollapsedWidgetState (img.src)
  } 
}

/*********************************************************************/

// Highlight selected line
function  sethighlight(highlight_line)
{
  // Save as cookie 
  //document.cookie = "highlight_line = " + highlight_line + " ; ";
}


/*********************************************************************/

// Initialize hierarchy state
function initExpand (modname,st)
{
var state=currState;
var prevIndex=0;
var blockNum=0;
try{
	while (state.indexOf('1') >= 0) {
	
	  blockNum=state.indexOf('1')+prevIndex;
	  document.getElementById("block"+blockNum).style.display = "block";
	    
	  prevIndex+=state.indexOf('1')+1;
	  state=state.substring(state.indexOf('1')+1);
	  }
	}
	catch(err)
	{}	 
}


/*********************************************************************/

//Initialize navigator server side
function initNavigatorServer (modname)
{ 

 var tcookie = document.cookie;
  var st = tcookie.indexOf ("currState"+modname);

  if (st != -1)
    {
	  st = st+11;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);
     
	  if( (en > st) && (currState != '' ) )
        {
        
          currState = tcookie.substring(st+1, en);
        }
    }
  var st = tcookie.indexOf ("highlight_line");
  if (st != -1)
    {
      st = st+14;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);
      if (en > st)
        {
          highlight_line = tcookie.substring(st+1, en);
        }
    }
	
  	initExpand(modname,st);
}
/*********************************************************************/

//Initialize navigator server side
function initNavigatorServerCheckCookie (modname)
{ 
var temp;
 var tcookie = document.cookie;
  var st = tcookie.indexOf ("currState"+modname); 
 	
  if (st != -1)
    {
	  st = st+11;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);
	  if (en > st)
        {
          temp = tcookie.substring(st+1, en);
          if(temp.length == currState.length )
          	currState = temp;
        }
    }
  var st = tcookie.indexOf ("highlight_line");
  if (st != -1)
    {
      st = st+14;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);
      if (en > st)
        {
          highlight_line = tcookie.substring(st+1, en);
        }
    }

  	initExpand(modname,st);
}

