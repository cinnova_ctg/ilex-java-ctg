/* Insert status row count with status label. */
function addStatusRowCount(detailTableClass, detailTableId, rowCountId, imageId){
	$("."+detailTableClass).each(function(j){
		var divCount = j+1;
		var rowCount = $("#"+detailTableId+divCount+" tr").size();
		if(rowCount > 0){
			$("#"+rowCountId+divCount).text("("+rowCount+")");
		 	$("#"+rowCountId+divCount).parent().css("font-weight","bold");
		 }else{
			 $("#"+imageId+divCount).css("display","none");
			 $("#"+imageId+divCount).parent().css("font-weight","normal");
		 }			
	});
}


/* Show hide Detail Table list */
function showHideDetailTable(imageId, detailTableId, unique) {
	var imageObj = document.getElementById(imageId + unique);
	var divObj  = document.getElementById(detailTableId + unique);
	toggleDiv(divObj, imageObj);
}

function toggleDiv(divObj, imageObj) {
	if (divObj.offsetHeight > 0) {
		imageObj.src = "images/Expand.gif";
		divObj.style.display = "none";
	} else {
		divObj.style.display = "block";
		imageObj.src = "images/Collapse.gif";
		
	}
}