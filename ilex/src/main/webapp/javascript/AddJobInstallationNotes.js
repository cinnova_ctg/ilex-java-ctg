/**
 This js is used for add and view job installation notes.
 Call Ajax to set and get notes.
*/

function getAjaxRequestObjectInstallNotes(){
	var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }	
	return ajaxRequest;        
}

function addCommentsForInstallationNotes(jobId)
{
	if(jobId != null && jobId!= ''){
		//var installationNotes = escape(encodeURI(document.all.installationNotes.value));
		var installationNotes = escape(encodeURI($("textarea[name='installationNotes']").val()));
		var ajaxRequest = getAjaxRequestObjectInstallNotes();	
		ajaxRequest.onreadystatechange = function(){
           if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){ 
	           var xmlDoc=ajaxRequest.responseXML;
	           var id;
			   id = xmlDoc.getElementsByTagName("installNotesInfo")[0];	
			   var installNotesCount=id.getElementsByTagName("installNotesCount")[0].firstChild.nodeValue;
			   var latestInstallNotesDate=id.getElementsByTagName("latestInstallNotesDate")[0].firstChild.nodeValue;					 
			   var latestInstallNotes=id.getElementsByTagName("latestInstallNotes")[0].firstChild.nodeValue;
			   
	           var secondLatestInstallNotesDate=id.getElementsByTagName("secondLatestInstallNotesDate")[0].firstChild.nodeValue;					 
			   var secondLatestInstallNotes=id.getElementsByTagName("secondLatestInstallNotes")[0].firstChild.nodeValue;
			   $("#installNotesCountDiv").html("");
			   $("#installNotesCountDiv").html(installNotesCount);
			   
	          
	           document.getElementById("latestInstallNotesDate").innerHTML = "";
	           document.getElementById("latestInstallNotes").innerHTML = "";
	           document.getElementById("secondLatestInstallNotesDate").innerHTML = "";
	           document.getElementById("secondLatestInstallNotes").innerHTML = "";
	           
	           document.getElementById("latestInstallNotesDate").innerHTML = latestInstallNotesDate;
	           document.getElementById("latestInstallNotes").innerHTML = removedLastSpecialChar(latestInstallNotes);
	           document.getElementById("secondLatestInstallNotesDate").innerHTML = secondLatestInstallNotesDate;
	           document.getElementById("secondLatestInstallNotes").innerHTML = removedLastSpecialChar(secondLatestInstallNotes);
	         //  document.all.installationNotes.value = "";
	         $("textarea[name='installationNotes']").val("");
          }// End of if
	    }
	    var url ="JobDBCompleteAction.do";
	    var params = "hmode=addComments&jobId="+jobId+"&installationNotes="+installationNotes
		ajaxRequest.open("POST", url, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send(params); 
	}// End of If
} // End addCommentsForInstallationNotes()

/*
 * This method is used to remove the all special character and in place of that char append the HTML value of it.
 */

function removedLastSpecialChar(string) {
	var temp = "";
	var finalTemp = "";
	for(var i =0; i < string.length; i++) {
		temp = string.substring(i,i+1);
		if(temp == '<') {
			temp = "&lt;";
		} else if(temp == '>') {
			temp = "&gt;";
		} else if(temp == '"') {
			temp = "&quot;";
		} else if(temp == '&') {
			temp = "&amp;";
		}
		finalTemp = finalTemp + temp;
	}
	return finalTemp;
}

