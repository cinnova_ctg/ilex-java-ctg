/*
	This Function is used when PO type is PVS or changed to PVS.
*/
function calForPVS()
	{	
		var subTotalVal = 0;
		var planSubTotal = 0;
	 	contractLabourCostGen = 0;
 		travelHourlyCostGen = 0;
 		PVScontractLabourCost = 0;
 		PVStravelHourlyCost = 0;
 		PVStravelFixedCost = 0;
 		travelFixedCostGen = 0;
		if(isExists(document.all.mpoActivityResource))
		{
			
			var len = document.all.mpoActivityResource.length;
				if(!len)
				{
					if(document.all.mpoActivityResource.checked == true)
					{
						
						if( document.all.mpoTypeName.value == 'Labor')
						{
							if(document.all.mpoActResourceSubName.value == 'Contract Labor' )
							{
								
								subTotalVal = (document.all.mpoRes.value * document.all.mpoAct.value) * document.forms[0].savedMpoUnit.value;
								contractLabourCostGen = eval(eval(contractLabourCostGen) + eval(subTotalVal));
								PVScontractLabourCost = contractLabourCostGen;
							}
						}
						else 
						{
							if(document.all.mpoActResourceSubName.value == 'Hourly'){
								subTotalVal = (document.all.mpoRes.value * document.all.mpoAct.value) * document.forms[0].savedMpoUnit.value;
								travelHourlyCostGen = eval(eval(travelHourlyCostGen) + eval(subTotalVal));
								PVStravelHourlyCost = travelHourlyCostGen;
							}
							else if(document.all.mpoActResourceSubName.value == 'Fixed'){
								travelFixedCostGen = eval(eval(travelFixedCostGen) + eval(document.all.mpoActivityPlanSubTotal.value));							
								PVStravelFixedCost=travelFixedCostGen;
							}
						}
					}
				}
				else
				{
					for(var index=0;index<len;index++)
					{
						if(document.all.mpoActivityResource[index].checked == true)
						{
							if( document.all.mpoTypeName[index].value == 'Labor')
							{
								if(document.all.mpoActResourceSubName[index].value == 'Contract Labor' )
								{
									subTotalVal = (document.all.mpoRes[index].value * document.all.mpoAct[index].value) * document.forms[0].savedMpoUnit[index].value;
									contractLabourCostGen = eval(eval(contractLabourCostGen) + eval(subTotalVal));
									PVScontractLabourCost = contractLabourCostGen;
								}
							}
							else 
							{
								if(document.all.mpoActResourceSubName[index].value == 'Hourly'){
									subTotalVal = (document.all.mpoRes[index].value * document.all.mpoAct[index].value) * document.forms[0].savedMpoUnit[index].value;
									travelHourlyCostGen = eval(eval(travelHourlyCostGen) + eval(subTotalVal));
									PVStravelHourlyCost = travelHourlyCostGen;
								}
								
								else if(document.all.mpoActResourceSubName[index].value == 'Fixed'){
									travelFixedCostGen = eval(eval(travelFixedCostGen) + eval(document.all.mpoActivityPlanSubTotal[index].value));							
									PVStravelFixedCost=travelFixedCostGen;
								}
							}
						}
							
					}
				}
			}
	
		PVSCostByHour = 0;
		PVSCostByContract = 0;
		var tableObj = document.getElementById('CVPT');
		if(isExists(tableObj))
		{
			PVSCostByContract =  contractLabourCostGen ;
			tableObj.rows[3].cells[1].innerText = formatCurrency(PVSCostByContract);
			
			//In the case of the Minuteman PO and Master PO Item "Fixed Price Services"
			if(document.forms[0].poType.value=='M'  && document.forms[0].masterPOItem.value=='2')
			{
				PVSCostByHour = eval((eval(travelHourlyCostGen)+eval(travelFixedCostGen)));
			}
			else
				{
				PVSCostByHour = travelHourlyCostGen;
				}
			
			tableObj.rows[3].cells[2].innerText = formatCurrency((PVSCostByHour));
			tableObj.rows[3].cells[3].innerText = formatCurrency((PVSCostByContract + PVSCostByHour));
		}
	}