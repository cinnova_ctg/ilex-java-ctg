/*
	This function is used for ......
*/
function changePOItem() {

		getAuthorizedCostInputText();
		var tableObj = document.getElementById('actTable');
		if(document.all.planned == null || document.all.planned == 'undefined')
			return true;
		if(document.all.mpoActivityResource){
			var length = document.all.planned.length;
			var totalLength = document.all.mpoActivityResource.length;
			document.getElementById("button").colSpan = "11";
			if(!totalLength)
			{
				if(document.all.mpoActivityResource.checked == true)
					{	
						if(document.all.mpoActivityPlanQuantity.value=='0.00' && (document.all.mpoActivityPlanUnitCost.value=='0.0000' || document.all.mpoActivityPlanUnitCost.value=='0.00')){
							document.all.mpoActivityPlanQuantity.value='';
							document.all.mpoActivityPlanUnitCost.value='';
						}
						if( isBlank(document.all.mpoActivityPlanQuantity.value) && isBlank(document.all.mpoActivityPlanUnitCost.value)){
								document.all.mpoActivityPlanQuantity.value = round_funcfor4digits(document.all.mpoTotal.value);
								document.all.mpoActivityPlanUnitCost.value = roundTo2Digit(eval(document.all.mpoUnit.value));
								tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
								document.all.mpoActivityPlanSubTotal.value = document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value;
						}
					}
			}
			else
			{
				for(var i=0;i<totalLength;i++)
				{
					if(document.all.mpoActivityResource[i].checked == true)
					{	
						if(document.all.mpoActivityPlanQuantity[i].value=='0.00' && (document.all.mpoActivityPlanUnitCost[i].value=='0.0000' || document.all.mpoActivityPlanUnitCost[i].value=='0.00')){
							document.all.mpoActivityPlanQuantity[i].value='';
							document.all.mpoActivityPlanUnitCost[i].value='';
						}
						if(isBlank(document.all.mpoActivityPlanQuantity[i].value) && isBlank(document.all.mpoActivityPlanUnitCost[i].value)){		
								document.all.mpoActivityPlanQuantity[i].value = round_funcfor4digits(document.all.mpoTotal[i].value);
								document.all.mpoActivityPlanUnitCost[i].value = roundTo2Digit(eval(document.all.mpoUnit[i].value));
								tableObj.rows[i+2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
								document.all.mpoActivityPlanSubTotal[i].value = document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value;
						}
					}
				}
			}
		}
		calCostByResType();
		
		//document.forms[0].estimatedCost.value = estimatedCost;
		setCost();
		calForPVS();
		calForGeneral('speed','24');
		calForGeneral('mm','18');
		
		calculateAll();
		calEstimateCost();
		checkNonInernalPOs();
}
/*
	This function is used when type of PO is changed.
*/

function setDisabledOnTypeChange(index) {
		var contractHourlyCheck = false;
		if( eval(index) < 1 ){
			//In the case the Activity Resources is equal to 0
			if(!document.forms[0].mpoActivityResource.length){
			if(document.all.mpoActResourceSubName.value == 'Contract Labor' || document.all.mpoActResourceSubName.value == 'Hourly'){ 
							contractHourlyCheck = true;
					}
					if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
						document.forms[0].mpoActivityPlanUnitCost.disabled = true;
					}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
						document.forms[0].mpoActivityPlanUnitCost.disabled = true;
					}else if(!document.forms[0].mpoActivityResource.disabled) {
						document.forms[0].mpoActivityPlanUnitCost.disabled = false;
					}	
				
			}
			//In the case the Activity Resources is greater than 0
			else
				{
			 if(document.all.mpoActResourceSubName[0].value == 'Contract Labor' || document.all.mpoActResourceSubName[0].value == 'Hourly'){ 
						contractHourlyCheck = true;
				}
				if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
					document.forms[0].mpoActivityPlanUnitCost[0].disabled = true;
				}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
					document.forms[0].mpoActivityPlanUnitCost[0].disabled = true;
				}else if(!document.forms[0].mpoActivityResource.disabled) {
					document.forms[0].mpoActivityPlanUnitCost[0].disabled = false;
				}
				}
		} else{
			if(document.all.mpoActResourceSubName[index].value == 'Contract Labor' || document.all.mpoActResourceSubName[index].value == 'Hourly'){
				contractHourlyCheck = true;
			}
			if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
				document.forms[0].mpoActivityPlanUnitCost[index].disabled = true;
			}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
				document.forms[0].mpoActivityPlanUnitCost[index].disabled = true;
			}else if(!document.forms[0].mpoActivityResource[index].disabled) {
				document.forms[0].mpoActivityPlanUnitCost[index].disabled = false;
			}
		}// - End Else
}
/*
	This function is used when type of PO is changed.
*/
function typeChanged(loadType)
{
  if(requestlistSize > 0)  {
	var len = document.forms[0].mpoActivityResource.length;
	tableObj = document.getElementById('actTable');
	checkNonInernalPOs();
	if(!len)
	{	
		/*Check for Contract Labor and Hourly: Start */
		var contractHourlyCheck = false;
		if(document.all.mpoActResourceSubName.value == 'Contract Labor' || document.all.mpoActResourceSubName.value == 'Hourly'){
			contractHourlyCheck = true;
		}
		/*Check for Contract Labor and Hourly: End */
		
		if(document.all.mpoActivityResource.checked == true)
			{
				if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
					document.forms[0].mpoUnit.value = roundTo2Digit(eval(minutemanUnitCost));
					document.forms[0].mpoActivityPlanUnitCost.value = minutemanUnitCost;
					document.forms[0].mpoActivityPlanUnitCost.disabled = true;
				}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
					document.forms[0].mpoUnit.value = roundTo2Digit(eval(speedPayUnitCost));
					document.forms[0].mpoActivityPlanUnitCost.value = speedPayUnitCost;
					document.forms[0].mpoActivityPlanUnitCost.disabled = true;
				}else if(loadType != 'onpageload'){
					document.forms[0].mpoUnit.value = roundTo2Digit(eval(document.forms[0].savedMpoUnit.value));
					document.forms[0].mpoActivityPlanUnitCost.disabled = false;
					document.forms[0].mpoActivityPlanUnitCost.value = roundTo2Digit(eval(document.forms[0].mpoUnit.value));
				}
				document.forms[0].mpoSubTotal.value = (eval (eval(document.forms[0].mpoUnit.value) * document.all.mpoTotal.value));
				tableObj.rows[2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal.value);
									
				tableObj.rows[2].cells[7].innerText = round_func(document.forms[0].mpoUnit.value);
				tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
				document.forms[0].mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
			} else {
				if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
					document.forms[0].mpoUnit.value = roundTo2Digit(eval(minutemanUnitCost));
					document.forms[0].mpoActivityPlanUnitCost.disabled = true;
				}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
					document.forms[0].mpoUnit.value = roundTo2Digit(eval(speedPayUnitCost));
					document.forms[0].mpoActivityPlanUnitCost.disabled = true;
				}else{
					document.forms[0].mpoUnit.value = roundTo2Digit(eval(document.forms[0].savedMpoUnit.value));
					document.forms[0].mpoActivityPlanUnitCost.disabled = false;
				}				
				tableObj.rows[2].cells[7].innerText = round_func(document.forms[0].mpoUnit.value);
				tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
				document.forms[0].mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
			}
	}
	else
	{
		for(var i=0;i<len;i++)
		{
			/*Check for Contract Labor and Hourly: Start */
			var contractHourlyCheck = false;
			if(document.all.mpoActResourceSubName[i].value == 'Contract Labor' || document.all.mpoActResourceSubName[i].value == 'Hourly'){
				contractHourlyCheck = true;
			}
			/*Check for Contract Labor and Hourly: End */
			if(document.all.mpoActivityResource[i].checked == true)
			{
				if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
					document.forms[0].mpoUnit[i].value = roundTo2Digit(eval(minutemanUnitCost));
					document.forms[0].mpoActivityPlanUnitCost[i].value = minutemanUnitCost;
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = true;
				}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
					document.forms[0].mpoUnit[i].value = roundTo2Digit(eval(speedPayUnitCost));
					document.forms[0].mpoActivityPlanUnitCost[i].value = speedPayUnitCost;
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = true;
					
				}else if(loadType != 'onpageload'){
					document.forms[0].mpoUnit[i].value = roundTo2Digit(eval(document.forms[0].savedMpoUnit[i].value));
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = false;
					document.forms[0].mpoActivityPlanUnitCost[i].value = roundTo2Digit(eval(document.forms[0].mpoUnit[i].value));
				}
				
				document.forms[0].mpoSubTotal[i].value = (eval (eval(document.forms[0].mpoUnit[i].value) * document.all.mpoTotal[eval(i)].value));
				tableObj.rows[i+2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal[i].value);
				
				tableObj.rows[i+2].cells[7].innerText = round_func(document.forms[0].mpoUnit[i].value);
				tableObj.rows[i+2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
				document.forms[0].mpoActivityPlanSubTotal[i].value = (document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
			} else {
				if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
					document.forms[0].mpoUnit[i].value = roundTo2Digit(eval(minutemanUnitCost));
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = true;
				}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
					document.forms[0].mpoUnit[i].value = roundTo2Digit(eval(speedPayUnitCost));
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = true;
				}else{
					document.forms[0].mpoUnit[i].value = roundTo2Digit(eval(document.forms[0].savedMpoUnit[i].value));
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = false;
				}				
				tableObj.rows[i+2].cells[7].innerText = round_func(document.forms[0].mpoUnit[i].value);
				tableObj.rows[i+2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
				document.forms[0].mpoActivityPlanSubTotal[i].value = (document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
			}
		}
	}
  }
	setCost();
	calCostByResType();
	calForPVS();
	calEstimateCost();
	calForGeneral('speed','24');
	calForGeneral('mm','18');
}
