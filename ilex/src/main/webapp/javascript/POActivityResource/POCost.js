	/*
		This Function is used for calculate the cost when Resource type is changed.
	*/
	function calCostByResType() {
			contractLabourCost = 0;
			corporateLabourCost = 0;
			materialCost = 0;
			frieghtCost = 0;
			travelFixedCost = 0;
			travelHourlyCost = 0;
			estimatedCost = 0 ;
			PVSCostByHour = 0;
			PVSCostByContract = 0;
			if(isExists(document.all.mpoActivityResource))
			{
				var len = document.all.mpoActivityResource.length;
				if(!len)
				{
					if(document.all.mpoActivityResource.checked == true)
					{
						if( document.all.mpoTypeName.value == 'Labor')
						{
							if(document.all.mpoActResourceSubName.value == 'Contract Labor' )
							{
								contractLabourCost = eval(eval(contractLabourCost) + eval(document.all.mpoActivityPlanSubTotal.value));
							}
							if( document.all.mpoActResourceSubName.value == 'Corporate Labor' )
							{
								corporateLabourCost = eval(eval(corporateLabourCost) + eval(document.all.mpoActivityPlanSubTotal.value));
							}
						}
						else if (document.all.mpoTypeName.value == 'Materials')
						{
							materialCost = eval(eval(materialCost) + eval(document.all.mpoActivityPlanSubTotal.value));
						}
						else if ( document.all.mpoTypeName.value == 'Freight')
						{
							frieghtCost = eval(eval(frieghtCost) + eval(document.all.mpoActivityPlanSubTotal.value));
						}
						else 
						{
							if(document.all.mpoActResourceSubName.value == 'Fixed'){
								travelFixedCost = eval(eval(travelFixedCost) + eval(document.all.mpoActivityPlanSubTotal.value));							
							}
							else
							{
								travelHourlyCost = eval(eval(travelHourlyCost) + eval(document.all.mpoActivityPlanSubTotal.value));
							}
							
						}
					}
				}
				else
				{
					for(var index=0;index<len;index++)
					{
						if(document.all.mpoActivityResource[index].checked == true)
						{
							if( document.all.mpoTypeName[index].value == 'Labor')
							{
								if(document.all.mpoActResourceSubName[index].value == 'Contract Labor' )
								{
									contractLabourCost = eval(eval(contractLabourCost) + eval(document.all.mpoActivityPlanSubTotal[index].value));
								}
								if( document.all.mpoActResourceSubName[index].value == 'Corporate Labor' )
								{
									corporateLabourCost = eval(eval(corporateLabourCost) + eval(document.all.mpoActivityPlanSubTotal[index].value));
								}
							}
							else if (document.all.mpoTypeName[index].value == 'Materials')
							{
									//materialCost = eval(eval(materialCost) + eval(document.all.mpoSubTotal[index].value));
									materialCost = eval(eval(materialCost) + eval(document.all.mpoActivityPlanSubTotal[index].value));
							}
							else if ( document.all.mpoTypeName[index].value == 'Freight')
							{
								frieghtCost = eval(eval(frieghtCost) + eval(document.all.mpoActivityPlanSubTotal[index].value));
							}
							else 
							{
								if(document.all.mpoActResourceSubName[index].value == 'Fixed'){
									travelFixedCost = eval(eval(travelFixedCost) + eval(document.all.mpoActivityPlanSubTotal[index].value));							
								}
								else
								{
									travelHourlyCost = eval(eval(travelHourlyCost) + eval(document.all.mpoActivityPlanSubTotal[index].value));
								}
							}
						}
							
					}
				}
			}
	}
/*
		This function is used for set cost of Cost table.
*/
function setCost() {
		var costTable = document.getElementById("costTable");
		if(isExists(costTable))
		{
			costTable.rows[1].cells[1].innerText = formatCurrency((contractLabourCost));
			costTable.rows[2].cells[1].innerText = formatCurrency((corporateLabourCost));
			
			costTable.rows[1].cells[2].innerText = '';
			costTable.rows[2].cells[2].innerText = '';
			costTable.rows[1].cells[3].innerText = '';
			costTable.rows[2].cells[3].innerText = '';
			costTable.rows[1].cells[4].innerText = '';
			costTable.rows[2].cells[4].innerText = '';	
			costTable.rows[1].cells[5].innerText = '';
			costTable.rows[2].cells[5].innerText = '';
			
			costTable.rows[3].cells[1].innerText = formatCurrency((eval(contractLabourCost) + eval(corporateLabourCost)));
			costTable.rows[1].cells[2].innerText = formatCurrency((eval(travelFixedCost) + eval(travelHourlyCost)));
			costTable.rows[3].cells[2].innerText = formatCurrency((eval(travelFixedCost) + eval(travelHourlyCost)));
			costTable.rows[1].cells[3].innerText = formatCurrency((materialCost));
			costTable.rows[3].cells[3].innerText = formatCurrency((materialCost));
			costTable.rows[1].cells[4].innerText = formatCurrency((frieghtCost));
			costTable.rows[3].cells[4].innerText = formatCurrency((frieghtCost));
			costTable.rows[3].cells[5].innerText = formatCurrency((eval(eval(contractLabourCost)+eval(corporateLabourCost)+eval(materialCost)+eval(frieghtCost)+eval(travelFixedCost)+eval(travelHourlyCost))))
		}
		
}
/*
	This function is used to create a text box when PO is Fixed Price PO.
*/
function getAuthorizedCostInputText() {
		if(requestlistSize > 0) {
			if(document.forms[0].masterPOItem.value == '2' && document.forms[0].appendixType.value != 'NetMedX') {
				document.getElementById('showAuthorizedCostInput').style.display = '';
			} else {
				document.getElementById('showAuthorizedCostInput').style.display = 'none';
			}
		}
}

/**
	This function is used to add a row dynamicaly 
	when PO Authorized cost difference is greater than 0 and PO type is fixed PO. 
*/
function showDiffAuthorisedRow() {
		if(poStatus != 'Draft' && document.forms[0].masterPOItem.value == 2) {
			if(roundTo2Digit(eval(poAuthorizedDiff)) == 0.00) {
				document.getElementById('showTr1').style.display = 'none';
				document.getElementById('showTr2').style.display = '';
			} else {
				document.getElementById('showTr1').style.display = '';
				document.getElementById('showTr2').style.display = 'none';
			}
		}
}
/**
	This function is used to set Estimated Cost. 
*/
function calEstimateCost() {
	estimatedCost=0;authorizedTotalCost=0;deltaCost=0;materialTotalCost=0;
		if(isExists(document.all.mpoActivityResource))
		{
			var len = document.all.mpoActivityResource.length;
			if(!len)
			{
				if(document.all.mpoActivityResource.checked == true)
				{
					estimatedCost = eval(eval(estimatedCost) + eval(document.forms[0].mpoSubTotal.value));
					authorizedTotalCost = eval(eval(authorizedTotalCost) + eval(document.forms[0].mpoActivityPlanSubTotal.value));
				}
			}
			else
			{
				//Used to add the constraint Material cost will be retricted to less than $500.00 per PO. 
				for(var i=0;i<len;i++)
				{
					if(document.all.mpoActivityResource[i].checked == true)
					{
					if(document.all.mpoTypeName[i].value=='Materials')
						{
						materialTotalCost = eval(eval(materialTotalCost)+eval(eval(document.forms[0].mpoActivityPlanSubTotal[i].value)));
						}
					}
				}
				for(var i=0;i<len;i++)
				{
					if(document.all.mpoActivityResource[i].checked == true)
					{
						estimatedCost = eval(eval(estimatedCost) + eval(document.forms[0].mpoSubTotal[i].value));
						authorizedTotalCost = eval(eval(authorizedTotalCost) + eval(document.forms[0].mpoActivityPlanSubTotal[i].value));
						//alert("authorizedTotalCost is "+authorizedTotalCost)
					}
				}
			}
			//deltaCost = authorizedTotalCost - estimatedCost;
			if(document.forms[0].masterPOItem.value == '2' && document.forms[0].appendixType.value != 'NetMedX') {
				deltaCost = estimatedCost - document.forms[0].authorizedCostInput.value;
			} else {
				deltaCost = estimatedCost - authorizedTotalCost;
			}
			
			var redColor = false;
			if(deltaCost<0) {
				redColor = true;
				deltaCost=-deltaCost;
			}
			document.forms[0].deltaCost.value = deltaCost;
			document.forms[0].authorizedTotalCost.value = authorizedTotalCost;
			document.forms[0].estimatedCost.value = estimatedCost;
			var tableObj = document.getElementById('actTable');
			var noOfRows = 0;
			noOfRows= eval(eval(requestlistSize)+2);
			
				tableObj.rows[noOfRows].cells[4].innerText = roundTo2Digit(estimatedCost)+'  ';
				tableObj.rows[noOfRows].cells[4].align = 'right';
				tableObj.rows[noOfRows].cells[4].className = 'Ntextoleftalignnowrappaleyellowborder2';
				tableObj.rows[noOfRows].cells[7].innerText = '  '+roundTo2Digit(authorizedTotalCost);
				tableObj.rows[noOfRows].cells[7].className = 'Ntextorightalignnowrappaleyellowborder2bold';
				tableObj.rows[noOfRows].cells[7].align = 'right';
			
			

			tableObj.rows[eval(eval(noOfRows)+2)].cells[2].innerText = ' '+roundTo2Digit(deltaCost);
			if(!redColor)
				tableObj.rows[eval(eval(noOfRows)+2)].cells[2].className ="Ntextorightalignnowrappaleyellowborder2bold";
			else
				tableObj.rows[eval(eval(noOfRows)+2)].cells[2].className ="dblabelwitjredcolor";
				
			var margin = 0.0;

			margin  = (estimatedCost*multiFactor)/100;
			
			if(redColor && deltaCost > margin){	}
			else{
				tableObj.rows[eval(eval(noOfRows)+3)].cells[2].innerText = '';
			}			
		}
}

/*
	This Function is used to calculate that As Planned cost should not exceed with X% of Estimated cost of individual resource.
	if(ticket)
		x = 20;
	else 
		x = 5 ;
*/
function exceedSubtotal(index,textObj)
{
//showDiffAuthorisedRow();
	var percentageEstimatedCost = 0;
	var percentageEstimatedTotalCost = 0;
	var planSubTotal = 0;
	var tableObj = document.getElementById('actTable');
	var noOfRows = 0;
	var len = document.all.mpoActivityResource.length;
	var alertMsg = '';
	var conditionCheck = 'pass';
	
	if(!len){
		if(document.all.mpoActivityPlanSubTotal.value != 0.0 && document.all.mpoSubTotal.value == 0.0) {
			alertMsg = 'The planned subtotal cannot be computed since there is no estimated subtotal cost. \nThis appears to be an hourly pricing scenario so only the planned unit cost should be entered';
			conditionCheck = 'fail'
		} else {
			planSubTotal = document.all.mpoActivityPlanUnitCost.value;
			percentageEstimatedCost = ( document.all.mpoUnit.value * multiFactor )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoUnit.value) + eval(percentageEstimatedCost);
			if(planSubTotal > percentageEstimatedTotalCost) {
				alertMsg = 'To be valid, the planned unit cost cannot \nexceed the corresponding estimated unit cost by more than '+multiFactor+'%';
				conditionCheck = 'fail';
			}
		}
		
		if(conditionCheck == 'pass') {
			if(document.all.mpoActivityPlanSubTotal.value != 0.0 && document.all.mpoSubTotal.value != 0.0) {
				planSubTotal = document.all.mpoActivityPlanSubTotal.value;
				percentageEstimatedCost = ( document.all.mpoSubTotal.value * multiFactor )/100;
				percentageEstimatedTotalCost = eval(document.all.mpoSubTotal.value) + eval(percentageEstimatedCost);
				if(planSubTotal > percentageEstimatedTotalCost) {
					alertMsg = 'To be valid, the planned unit and subtotal cost cannot \nexceed the corresponding estimated values by more than '+multiFactor+'%';
					conditionCheck = 'fail';
				}
			}
		}
	
	
	/*	if(document.all.mpoActivityPlanSubTotal.value == 0.0){
			planSubTotal = document.all.mpoActivityPlanUnitCost.value;
			percentageEstimatedCost = ( document.all.mpoUnit.value * multiFactor )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoUnit.value) + eval(percentageEstimatedCost);
		} else{
			planSubTotal = document.all.mpoActivityPlanSubTotal.value;
			percentageEstimatedCost = ( document.all.mpoSubTotal.value * multiFactor )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoSubTotal.value) + eval(percentageEstimatedCost);
		}
		*/
	} else{
	/*
		if(document.all.mpoSubTotal[index].value == 0.0 || document.all.mpoActivityPlanSubTotal[index].value == 0.0){
			planSubTotal = document.all.mpoActivityPlanUnitCost[index].value;
			percentageEstimatedCost = ( document.all.mpoUnit[index].value * multiFactor )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoUnit[index].value) + eval(percentageEstimatedCost);
		} else{
			planSubTotal = document.all.mpoActivityPlanSubTotal[index].value;
			percentageEstimatedCost = ( document.all.mpoSubTotal[index].value * multiFactor )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoSubTotal[index].value) + eval(percentageEstimatedCost);
		}
		*/
		
		if(document.all.mpoActivityPlanSubTotal[index].value != 0.0 && document.all.mpoSubTotal[index].value == 0.0) {
			alertMsg = 'The planned subtotal cannot be computed since there is no estimated subtotal cost. \nThis appears to be an hourly pricing scenario so only the planned unit cost should be entered';
			conditionCheck = 'fail'
		} else {
			planSubTotal = document.all.mpoActivityPlanUnitCost[index].value;
			percentageEstimatedCost = ( document.all.mpoUnit[index].value * multiFactor )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoUnit[index].value) + eval(percentageEstimatedCost);
			if(planSubTotal > percentageEstimatedTotalCost) {
				alertMsg = 'To be valid, the planned unit cost cannot \nexceed the corresponding estimated unit cost by more than '+multiFactor+'%';
				conditionCheck = 'fail';
			}
		}
		
		
		if(conditionCheck == 'pass') {
			if(document.all.mpoActivityPlanSubTotal[index].value != 0.0 && document.all.mpoSubTotal[index].value != 0.0) {
				planSubTotal = document.all.mpoActivityPlanSubTotal[index].value;
			percentageEstimatedCost = ( document.all.mpoSubTotal[index].value * multiFactor )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoSubTotal[index].value) + eval(percentageEstimatedCost);
				if(planSubTotal > percentageEstimatedTotalCost) {
					alertMsg = 'To be valid, the planned unit and subtotal cost cannot \nexceed the corresponding estimated values by more than '+multiFactor+'%';
					conditionCheck = 'fail';
				}
			}
		}
		
		
		
	}

	//if(planSubTotal > percentageEstimatedTotalCost)
	if(conditionCheck == 'fail')
	{
		//alert('Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by '+multiFactor+'%. ');		
		alert(alertMsg);
		noOfRows = eval(eval(requestlistSize)+2);
		tableObj.rows[eval(eval(noOfRows)+3)].cells[2].innerText = "Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by "+multiFactor+"%. "
		tableObj.rows[eval(eval(noOfRows)+3)].cells[2].innerText = alertMsg;
		tableObj.rows[eval(eval(noOfRows)+3)].cells[2].className ="Ntextoleftalignnowrappaleyellowborder4";
		textObj.focus();
	} else{
		noOfRows = eval(eval(requestlistSize)+2);
		tableObj.rows[eval(eval(noOfRows)+3)].cells[2].innerText = ""
		tableObj.rows[eval(eval(noOfRows)+3)].cells[2].className ="Ntextoleftalignnowrappaleyellowborder4";
	}
}