var estimatedCost = 0 ;
var contractLabourCost = 0;
var corporateLabourCost = 0;
var materialCost = 0;
var frieghtCost = 0;
var travelFixedCost = 0;
var travelHourlyCost = 0;
var PVSCostByContract = 0;
var PVSCostByHour = 0;
var speedPayByContract = 0;
var speedPayByHour = 0;
var mmByContract = 0;
var mmByHour = 0;
var contractLabourCostGen = 0;
var travelHourlyCostGen = 0;
var travelFixedCostGen = 0 ;
var PVScontractLabourCost = 0; 
var PVStravelHourlyCost = 0;

//Added New Travel Hourly Cost 
var PVStravelFixedCost =0;
var materialTotalCost=0;
var authorizedTotalCost = 0;
var deltaCost = 0;
var minutemanUnitCost = 18.00;
var speedPayUnitCost = 24.00;

var poAuthorized = 0.00;
var poAuthorizedDiff = 0.00;
/*
	This function is used for select all check boxes.
*/
function checkSelectAll(tableName) {
	PVSCostByContract = 0;
	contractLabourCost = 0;
	corporateLabourCost = 0;
	materialCost = 0;
	frieghtCost = 0;
	travelFixedCost = 0;
	travelHourlyCost = 0;
	estimatedCost = 0 ;
	PVSCostByHour = 0;
	tableObj = document.getElementById(tableName);
	var costTable = document.getElementById("costTable");
	var len = document.all.mpoActivityResource.length;
	var setResCostZero = false;
	
	if(!len)
	{
		if(document.forms[0].poType.value != 'N' && document.all.mpoActResourceSubName.value == 'Corporate Labor') { setResCostZero = true; }
		else {
			if(!document.all.mpoActivityResource.disabled)
				document.all.mpoActivityResource.checked = true;
				
			var activityId = getActivityId(document.all.activityResourceId.value)
			checkActivity(document.all.activityId.value);
			tableObj.rows[2].cells[6].innerText = round_funcfor4digits(eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
			document.all.mpoTotal.value = (eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
			tableObj.rows[2].cells[8].innerText = round_func(eval (document.forms[0].mpoUnit.value * eval (document.all.mpoTotal.value)));
			document.all.mpoSubTotal.value = (eval (document.forms[0].mpoUnit.value * eval (document.all.mpoTotal.value)));
			
			if((isBlank(document.all.mpoActivityPlanQuantity.value)||isBlank(document.all.mpoActivityPlanUnitCost.value))){
				document.all.mpoActivityPlanQuantity.value = round_funcfor4digits(eval(document.all.mpoTotal.value));
				document.forms[0].mpoActivityPlanUnitCost.disabled = false;
				document.all.mpoActivityPlanUnitCost.value = roundTo2Digit(eval(document.all.mpoUnit.value));
				/* Set Planned Unit Cost: Start */
				tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
				document.all.mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
			}
		}
		
		if(setResCostZero)
		{
			document.forms[0].mpoTotal.value = '';
			document.forms[0].mpoSubTotal.value = '';
			document.all.mpoActivityResource.checked = false;
			document.forms[0].mpoActivityPlanQuantity.value = '';
			document.forms[0].mpoActivityPlanUnitCost.value = '';
			tableObj.rows[2].cells[11].innerText = '0.00';
		}
	}   //End of if(!len)
	else
	{
		for(var i=0;i<len;i++)
		{
			
			setResCostZero = false;
			if(document.forms[0].poType.value != 'N' && document.all.mpoActResourceSubName[i].value == 'Corporate Labor') { setResCostZero = true; }
			else {
				if(!document.all.mpoActivityResource[i].disabled)
					document.all.mpoActivityResource[i].checked = true;
					
				var activityId = getActivityId(document.all.activityResourceId[i].value)
				checkActivity(activityId)
				tableObj.rows[i+2].cells[6].innerText = round_funcfor4digits(eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value));
				document.all.mpoTotal[i].value = (eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value));
				tableObj.rows[i+2].cells[8].innerText = round_func(eval (document.forms[0].mpoUnit[i].value * eval (document.all.mpoTotal[i].value)));
				document.all.mpoSubTotal[i].value = (eval (document.forms[0].mpoUnit[i].value * eval (document.all.mpoTotal[i].value)));
				
				if((isBlank(document.all.mpoActivityPlanQuantity[i].value)||isBlank(document.all.mpoActivityPlanUnitCost[i].value))){
					document.all.mpoActivityPlanQuantity[i].value = round_funcfor4digits(eval(document.all.mpoTotal[i].value));
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = false;
					document.all.mpoActivityPlanUnitCost[i].value = roundTo2Digit(eval(document.all.mpoUnit[i].value));
					/* Set Planned Unit Cost: End */
					
					tableObj.rows[i+2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
					document.all.mpoActivityPlanSubTotal[i].value = (document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
				}
				//calculateCost(i);
			}
			
			if(setResCostZero)
			{
				document.forms[0].mpoTotal[i].value = '';
				document.forms[0].mpoSubTotal[i].value = '';
				document.all.mpoActivityResource[i].checked = false;
				document.forms[0].mpoActivityPlanQuantity[i].value = '';
				document.forms[0].mpoActivityPlanUnitCost[i].value = '';
				tableObj.rows[i+2].cells[11].innerText = '0.00';
			}
		}
	}	
	calCostByResType();
	calForPVS();
	calEstimateCost();
	setCost();
	
	calForGeneral('speed','24');
	calForGeneral('mm','18');	
	typeChanged('');
	//showDiffAuthorisedRow();
}
/*
	This function is used for disable all Object of form.
*/
function disableAllText() {
	var len = document.forms[0].mpoActivityResource.length;
	tableObj = document.getElementById('actTable');
	if(!len)
	{	
		if(document.all.mpoActivityResource.checked == true)
		{
			/*Check for Contract Labor and Hourly: Start */
			var contractHourlyCheck = false;
			if(document.all.mpoActResourceSubName.value == 'Contract Labor' || document.all.mpoActResourceSubName.value == 'Hourly'){
				contractHourlyCheck = true;
			}
			/*Check for Contract Labor and Hourly: End */
			if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
					document.forms[0].mpoActivityPlanUnitCost.disabled = true;
				}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
					document.forms[0].mpoActivityPlanUnitCost.disabled = true;
				}else if(!document.all.mpoActivityResource.disabled){
					document.forms[0].mpoActivityPlanUnitCost.disabled = false;
				}	
		}
	}
	else
	{
		for(var i=0;i<len;i++)
		{
			if(document.all.mpoActivityResource[i].checked == true)
			{
				/*Check for Contract Labor and Hourly: Start */
				var contractHourlyCheck = false;
				if(document.all.mpoActResourceSubName[i].value == 'Contract Labor' || document.all.mpoActResourceSubName[i].value == 'Hourly'){
					contractHourlyCheck = true;
				}
				/*Check for Contract Labor and Hourly: End */
				
				if(document.forms[0].poType.value == 'M' && contractHourlyCheck){
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = true;
				}else if(document.forms[0].poType.value == 'S' && contractHourlyCheck){
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = true;
				}else if(!document.all.mpoActivityResource[i].disabled) {
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = false;
				}	
			}
		}
	}
}
/*
	This function is used for get activity.
*/
function getActivityId(activityResourceId) {
	var activityId = activityResourceId.substring(0,activityResourceId.lastIndexOf('~'))
	return activityId;
}
/*
	This is used for .......................
*/

function calculateAll() {
		var tableObj = document.getElementById('actTable');
	 	
	 	if(isExists(document.all.mpoActivityResource))
	 	{
	 		var len = document.all.mpoActivityResource.length;
		 	if(!len)
		 	{
			 		document.all.mpoTotal.value = eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value);
			 		tableObj.rows[2].cells[6].innerText = round_funcfor4digits(document.all.mpoTotal.value) ;
					document.forms[0].mpoSubTotal.value = eval (eval(document.forms[0].mpoUnit.value) * document.all.mpoTotal.value);
					tableObj.rows[2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal.value);
						document.all.mpoActivityPlanSubTotal.value = eval (eval(document.all.mpoActivityPlanQuantity.value) * eval(document.all.mpoActivityPlanUnitCost.value));	
						tableObj.rows[2].cells[11].innerText = round_func(document.all.mpoActivityPlanSubTotal.value) ;
		 	}
		 	else
		 	{
		 		for(i=0;i<len;i++)
		 		{
		 				document.all.mpoTotal[i].value = eval ((document.forms[0].mpoAct[i].value) * (document.forms[0].mpoRes[i].value));
						document.forms[0].mpoSubTotal[i].value = eval (eval(document.forms[0].mpoUnit[i].value) * eval(document.all.mpoTotal[i].value));
			 			tableObj.rows[i+2].cells[6].innerText = round_funcfor4digits(document.all.mpoTotal[i].value) ;
						tableObj.rows[i+2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal[i].value)
							document.all.mpoActivityPlanSubTotal[i].value = eval (eval(document.all.mpoActivityPlanQuantity[i].value ) * eval(document.all.mpoActivityPlanUnitCost[i].value));	
							tableObj.rows[i+2].cells[11].innerText = round_func(document.all.mpoActivityPlanSubTotal[i].value);
		 		}
		 	}
		 }
		calCostByResType();
		calEstimateCost();
		setCost();
		calForPVS();
		calForGeneral('speed','24');
		calForGeneral('mm','18');
}

/*
	This function is used for ......
*/	
function isExists(obj) {
		if(obj)
			return true;
		else
			return false;
}
/*
	This function is used for deselect all CheckBoxes.
*/	
function checkDeSelectAll(tableName) {
		contractLabourCost = 0;
		corporateLabourCost = 0;
		materialCost = 0;
		frieghtCost = 0;
		travelFixedCost = 0;
		travelHourlyCost = 0;
		estimatedCost = 0 ;
		PVSCostByHour = 0;
		PVSCostByContract = 0;
		authorizedTotalCost = 0;
		var activityId;
		tableObj = document.getElementById(tableName);
		var len = document.all.mpoActivityResource.length;
		if(!len)
		{
			document.all.mpoActivityResource.checked = false;
			activityId = getActivityId(document.all.activityResourceId.value);
			unCheckActivity(activityId);
			document.forms[0].mpoActivityPlanQuantity.value = '';
			document.forms[0].mpoActivityPlanUnitCost.value = ''
			tableObj.rows[2].cells[11].innerText = '0.00'
			document.forms[0].mpoActivityIsWOShow.checked = false;
		}
		else
		{
			for(var i=0;i<len;i++)
			{
				document.all.mpoActivityResource[i].checked = false;
				activityId = getActivityId(document.all.activityResourceId[i].value);
				unCheckActivity(activityId);
				document.forms[0].mpoActivityPlanQuantity[i].value = '';
				document.forms[0].mpoActivityPlanUnitCost[i].value = ''
				tableObj.rows[i+2].cells[11].innerText = '0.00'
				document.forms[0].mpoActivityIsWOShow[i].checked = false;
			}
		}
		calCostByResType();
		calForPVS();
		calEstimateCost();
		setCost();
		
		calForGeneral('speed','24');
		calForGeneral('mm','18');
		typeChanged('');
		//showDiffAuthorisedRow();
}
/*
	This function is used for ......
*/	
function calculateByActivity(obj,tableId) {
			var tableObj = document.getElementById(tableId);
			var actResId;
			var actId;
			var length = document.all.mpoActivityResource.length;
			
			var setResCostZero = false;
			if(!length)
			{
				actResId = document.all.activityResourceId.value;
				actId = getActivityId(actResId);
				if(obj.value == actId)
				{
					if(obj.checked == true)
					{
						if(document.forms[0].poType.value != 'N' && (document.all.mpoActResourceSubName.value == 'Corporate Labor' || document.all.mpoActResourceSubNamePre.value == 'Corporate Labor')) { setResCostZero = true; }
						else {
								document.all.mpoActivityResource.checked = true;
								tableObj.rows[2].cells[6].innerText = round_funcfor4digits(eval (document.all.mpoAct.value * document.all.mpoRes.value));
								document.all.mpoTotal.value = (eval (document.all.mpoAct.value * document.all.mpoRes.value));
								tableObj.rows[2].cells[8].innerText = round_func(eval (document.all.mpoUnit.value * document.all.mpoTotal.value));
								document.all.mpoSubTotal.value = (document.all.mpoUnit.value * document.all.mpoTotal.value);
								// commented for making page behaviour similar for all mpoTypes
								
								/* Set Planned Unit Cost: Start */
								document.forms[0].mpoActivityPlanUnitCost.disabled = false;
								document.forms[0].mpoActivityPlanUnitCost.value = roundTo2Digit(document.forms[0].mpoUnit.value);
								/* Set Planned Unit Cost: End */
								
								document.forms[0].mpoActivityPlanQuantity.value = round_funcfor4digits(document.forms[0].mpoTotal.value);
								tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
								document.forms[0].mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
							}
					} //if(obj.checked == true)
					
					if(obj.checked != true || setResCostZero)
					{
						document.forms[0].mpoTotal.value = '';
						document.forms[0].mpoSubTotal.value = '';
						document.all.mpoActivityResource.checked = false;
						document.forms[0].mpoActivityPlanQuantity.value = '';
						document.forms[0].mpoActivityPlanUnitCost.value = '';
						tableObj.rows[2].cells[11].innerText = '0.00';
						
						document.forms[0].mpoActivityIsWOShow.checked = false;
					}
					setDisabledOnTypeChange(0);
				}
			}
			else
			{
				for(var i=0;i<length;i++)
				{				
					actResId = document.all.activityResourceId[i].value;
					actId = getActivityId(actResId);
					setResCostZero = false;
					
					if(obj.value == actId)
					{
						if(obj.checked == true)
						{
							if(document.forms[0].poType.value != 'N' && (document.all.mpoActResourceSubName[i].value == 'Corporate Labor' || document.all.mpoActResourceSubNamePre[i].value == 'Corporate Labor')) { setResCostZero = true; }
							//if(document.forms[0].poType.value != 'N' && (document.all.mpoActResourceSubName[i].value == 'Corporate Labor' || (document.all.mpoActResourceSubName[i].value == 'Hourly' && document.all.checkInTransit[i].value == 'Y'))) { setResCostZero = true; }
							else {
									document.all.mpoActivityResource[i].checked = true;
									tableObj.rows[i+2].cells[6].innerText = round_funcfor4digits(eval (document.all.mpoAct[i].value * document.all.mpoRes[i].value));
									document.all.mpoTotal[i].value = (eval (document.all.mpoAct[i].value * document.all.mpoRes[i].value));
									tableObj.rows[i+2].cells[8].innerText = round_func(eval (document.all.mpoUnit[i].value * document.all.mpoTotal[i].value));
									document.all.mpoSubTotal[i].value = (document.all.mpoUnit[i].value * document.all.mpoTotal[i].value);
									
									/*Set Planned Unit Cost: Start */
									document.forms[0].mpoActivityPlanUnitCost[i].disabled = false;
									document.forms[0].mpoActivityPlanUnitCost[i].value = roundTo2Digit(document.forms[0].mpoUnit[i].value);
									/*Set Planned Unit Cost: End */				
												
									document.forms[0].mpoActivityPlanQuantity[i].value = round_funcfor4digits(document.forms[0].mpoTotal[i].value);
									tableObj.rows[i+2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
									document.forms[0].mpoActivityPlanSubTotal[i].value = (document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
							}
						}
						if(obj.checked != true || setResCostZero)
						{
							document.forms[0].mpoTotal[i].value = '';
							document.forms[0].mpoSubTotal[i].value = '';
							document.all.mpoActivityResource[i].checked = false;
							document.forms[0].mpoActivityPlanQuantity[i].value = '';
							document.forms[0].mpoActivityPlanUnitCost[i].value = '';
							tableObj.rows[i+2].cells[11].innerText = '0.00';
							document.forms[0].mpoActivityIsWOShow[i].checked = false;
						}
						
					}
					setDisabledOnTypeChange(i);
				}
			}
			calCostByResType();
			calForPVS();
			calEstimateCost();
			setCost();
			
			calForGeneral('speed','24');
			calForGeneral('mm','18');
}
/*
	This function is used for ......
*/	
function calMaterialCost(textObj)
{
	var len = document.all.mpoActivityResource.length; 
	if(!len)
		{
		alert("their is no resource");
		}
	else
		{
		
		}
}




function calCostForPlan(textObj) {
		var len = document.all.mpoActivityResource.length;
		var activityId;
		var tableObj = document.getElementById('actTable');
		if(!len)
		{
			if(!isFloat(document.all.mpoActivityPlanQuantity.value)){
					alert('Only numeric value is allowed.');
					document.all.mpoActivityPlanQuantity.focus();
					return false;
				}
				if(!isFloat(document.all.mpoActivityPlanUnitCost.value)){
					alert('Only numeric value is allowed.');
					document.all.mpoActivityPlanUnitCost.focus();
					return false;
				}
				if(textObj == document.all.mpoActivityPlanQuantity || textObj == document.all.mpoActivityPlanUnitCost)
				{
					document.all.mpoActivityResource.checked = true;
					activityId = getActivityId(document.all.activityResourceId.value);
					checkActivity(activityId);
					if(textObj == document.all.mpoActivityPlanQuantity  && isBlank(document.all.mpoActivityPlanUnitCost.value)){
						document.all.mpoActivityPlanUnitCost.value = 0;
					}
					if(textObj == document.all.mpoActivityPlanUnitCost  && isBlank(document.all.mpoActivityPlanQuantity.value)){
						document.all.mpoActivityPlanQuantity.value= round_funcfor4digits(0);
					}
					document.all.mpoTotal.value = eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value);
					document.forms[0].mpoSubTotal.value = eval (eval(document.forms[0].mpoUnit.value) * document.all.mpoTotal.value);
					document.all.mpoActivityPlanSubTotal.value = eval (eval(document.all.mpoActivityPlanQuantity.value) * eval(document.all.mpoActivityPlanUnitCost.value));	
					tableObj.rows[2].cells[6].innerText = round_funcfor4digits(document.all.mpoTotal.value);
					tableObj.rows[2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal.value);
					tableObj.rows[2].cells[11].innerText = round_func(document.all.mpoActivityPlanSubTotal.value);
					
				}	
				
		}
		else
		{
					for(var i=0;i < len ; i++)
			{				
				if(!isFloat(document.all.mpoActivityPlanQuantity[i].value)){
					alert('Only numeric value is allowed.');
					document.all.mpoActivityPlanQuantity[i].focus();
					return false;
				}
				if(!isFloat(document.all.mpoActivityPlanUnitCost[i].value)){
					alert('Only numeric value is allowed.');
					document.all.mpoActivityPlanUnitCost[i].focus();
					return false;
				}
				if(textObj == document.all.mpoActivityPlanQuantity[i] || textObj == document.all.mpoActivityPlanUnitCost[i])
				{
					document.all.mpoActivityResource[i].checked = true;
					activityId = getActivityId(document.all.activityResourceId[i].value);
					checkActivity(activityId);
					if(textObj == document.all.mpoActivityPlanQuantity[i]  && isBlank(document.all.mpoActivityPlanUnitCost[i].value)){
						document.all.mpoActivityPlanUnitCost[i].value = 0;
					}
					if(textObj == document.all.mpoActivityPlanUnitCost[i]  && isBlank(document.all.mpoActivityPlanQuantity[i].value)){
						document.all.mpoActivityPlanQuantity[i].value= round_funcfor4digits(0);
					}
					document.all.mpoTotal[i].value = eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value);
					document.forms[0].mpoSubTotal[i].value = eval (document.forms[0].mpoUnit[i].value * document.all.mpoTotal[eval(i)].value);
					document.all.mpoActivityPlanSubTotal[i].value = eval (document.all.mpoActivityPlanQuantity[i].value  * document.all.mpoActivityPlanUnitCost[i].value);	
					tableObj.rows[i+2].cells[6].innerText = round_funcfor4digits(document.all.mpoTotal[i].value);
					tableObj.rows[i+2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal[i].value);
					tableObj.rows[i+2].cells[11].innerText = round_func(document.all.mpoActivityPlanSubTotal[i].value);
				
				}
			}
		}
		calCostByResType();
		calForPVS();
		calEstimateCost();
		setCost();

}
/*
	This function is used for ......
*/
function unCheckActivity(actId) {
		//var length = document.all.mpoActivityResource.length;
		var actLength = document.all.activityId.length;
		var isCheck = false;
		if(!actLength){
			document.all.activityId.checked = false;
		}
		else
		{
			for(var i=0;i<length;i++)
			{
				var activityId = document.all.activityResourceId[i].value;
				activityId = getActivityId(activityId);
				if(actId == activityId)
				{
					if(document.all.mpoActivityResource[i].checked == true)
					{
						isCheck = true;
						break;
					}
				}
			}
			if(isCheck == false)
			{
				for(var i=0;i<document.all.activityId.length;i++)
				{
					if(actId == document.all.activityId[i].value){
						document.all.activityId[i].checked = false;
						break;
					}
				}
			}
		}
		
}
/*
	This function is used for calculate SubTotal.
*/	
function calculateSubTotal(resCheckObj,tableName) {
	var subTotal = 0;
	var len = document.forms[0].mpoActivityResource.length;
	tableObj = document.getElementById(tableName);
	//var len = tableObj.rows.length;
	var actId ;
	if(!len)
	{	
			actId = document.all.activityResourceId.value;
			actId = getActivityId(actId);
			checkActivity(actId);
			tableObj.rows[2].cells[6].innerText = round_funcfor4digits(eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
			document.forms[0].mpoTotal.value = (eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
			tableObj.rows[2].cells[8].innerText = round_func(eval (document.forms[0].mpoUnit.value * document.forms[0].mpoTotal.value));
			document.forms[0].mpoSubTotal.value = (document.forms[0].mpoUnit.value * document.forms[0].mpoTotal.value);
			document.forms[0].mpoActivityPlanUnitCost.disabled = false;
			document.forms[0].mpoActivityPlanUnitCost.value =roundTo2Digit(document.forms[0].mpoUnit.value);
			document.forms[0].mpoActivityPlanQuantity.value = round_funcfor4digits(document.forms[0].mpoTotal.value);
			tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
			document.forms[0].mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
			
			if(resCheckObj.checked != true)
			{
				unCheckActivity(actId);
				document.forms[0].mpoTotal.value = '';
				document.forms[0].mpoSubTotal.value = '';
				document.forms[0].mpoActivityPlanQuantity.value = '';
				document.forms[0].mpoActivityPlanUnitCost.value = ''
				document.all.activityId.checked = false;
				tableObj.rows[2].cells[11].innerText = '0.00';
				document.forms[0].mpoActivityIsWOShow.checked = false;
			}
			setDisabledOnTypeChange(0);
	}
	else
	{
		for(var i=0;i<len;i++)
		{
		
			if(resCheckObj == document.forms[0].mpoActivityResource[i])
			{
				actId = document.all.activityResourceId[i].value;
				actId = getActivityId(actId );
				checkActivity(actId);
				tableObj.rows[i+2].cells[6].innerText = round_funcfor4digits(eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value));
				document.all.mpoTotal[eval(i)].value = (eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value));
				tableObj.rows[i+2].cells[8].innerText = round_func(eval (eval(document.forms[0].mpoUnit[i].value) * document.all.mpoTotal[eval(i)].value));
				document.forms[0].mpoSubTotal[i].value = (eval (eval(document.forms[0].mpoUnit[i].value) * document.all.mpoTotal[eval(i)].value));
				document.forms[0].mpoActivityPlanQuantity[i].value = round_funcfor4digits(eval(document.forms[0].mpoTotal[i].value));
				document.forms[0].mpoActivityPlanUnitCost[i].disabled = false;
				document.forms[0].mpoActivityPlanUnitCost[i].value = roundTo2Digit(eval(document.forms[0].mpoUnit[i].value));
				tableObj.rows[i+2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
				document.forms[0].mpoActivityPlanSubTotal[i].value = (document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
				
				if(resCheckObj.checked != true)
				{
					unCheckActivity(actId);
					document.forms[0].mpoSubTotal[i].value = '';
					document.all.mpoTotal[eval(i)].value = '';
					tableObj.rows[i+2].cells[11].innerText = '0.00';
					document.forms[0].mpoActivityPlanQuantity[i].value = '';
					document.forms[0].mpoActivityPlanUnitCost[i].value = '';
					document.forms[0].mpoActivityIsWOShow[i].checked = false;
				}
				setDisabledOnTypeChange(i);
			}
		}
	}
	calCostByResType();
	calForPVS();
	calForGeneral('speed','24');
	calForGeneral('mm','18');
	calEstimateCost();
	setCost();
}


/*
	This function is used for check activity check box.
*/
function checkActivity(actId) {
	var len = document.all.activityId.length;
	if(!len){
			document.all.activityId.checked = true;
	} else {
			for(var i=0;i<len;i++)
			{
				if(actId == document.all.activityId[i].value)
				{
					document.all.activityId[i].checked = true;
					break;
				}
			}
		}
}

	
/*
	This function is used for round of the float value up to 2 places.
*/	
function round_func(num) {
		if(isNaN(num)) num = 0;
		num = parseFloat(num);
		num = num;
		num = num.toFixed(2);
		return num;
		
}
/*
	This function is used for round of the float value up to 2 places.
*/
function roundTo2Digit(num) {
		if(isNaN(num)) num = 0;
		num = parseFloat(num);
		num = num;
		num = num.toFixed(2);
		return num;
}
	
	
/*
	This function is used for .....
*/	
function calForGeneral(type,typeVal) {
		var subTotalVal = 0;
		var planSubTotal = 0;
		var defaultQuantity = 0;
		var estimatedQunatity = 0;
	 	contractLabourCostGen = 0;
 		travelHourlyCostGen = 0;
 		subTotalVal = 0;
 		speedPayByContract = 0;
 		speedPayByHour = 0;
 		mmByContract = 0;
		mmByHour = 0;
		var estimatedtototallaborQuantity = 0;
		var estimatedtototaltravelQuantity = 0;

		if(isExists(document.all.mpoActivityResource))
		{
			/* For Ticket only: Start */
			var len = document.all.mpoActivityResource.length;
			if(!len)
			{
				if(document.all.mpoActivityResource.checked == true)
				{
					estimatedQunatity = eval(eval(document.all.mpoRes.value) * eval(document.all.mpoAct.value));
					if( document.all.mpoTypeName.value == 'Labor')
					{
						if(document.all.mpoActResourceSubName.value == 'Contract Labor' )
						{
							if(eval(estimatedQunatity) < 2){defaultQuantity = 2;} 
							else{defaultQuantity = estimatedQunatity;}
							subTotalVal = defaultQuantity * typeVal;
							contractLabourCostGen = eval(eval(contractLabourCostGen) + eval(subTotalVal));
						}
					}
					else 
					{
						if(document.all.mpoActResourceSubName.value == 'Hourly' && (document.all.mpoActResourceSubNamePre.value == 'Contract Labor'  || document.all.mpoActResourceSubNamePre.value == 'Travel') ){
							if(eval(estimatedQunatity) < 1 && type=='mm'){defaultQuantity = 1;}
							else{defaultQuantity = estimatedQunatity;}
							subTotalVal = defaultQuantity * typeVal;
							travelHourlyCostGen = eval(eval(travelHourlyCostGen) + eval(subTotalVal));
						}
					}
				}
			}
			else
			{
				for(var index=0;index<len;index++) {
					if(document.all.mpoActivityResource[index].checked == true)
						{
							estimatedQunatity = eval(eval(document.all.mpoRes[index].value) * eval(document.all.mpoAct[index].value))
							if( document.all.mpoTypeName[index].value == 'Labor')
								{
									if(document.all.mpoActResourceSubName[index].value == 'Contract Labor' )
									{
										estimatedtototallaborQuantity =estimatedtototallaborQuantity + estimatedQunatity;
									}
								}
							else {
								if(document.all.mpoActResourceSubName[index].value == 'Hourly' && (document.all.mpoActResourceSubNamePre[index].value == 'Contract Labor' || document.all.mpoActResourceSubNamePre[index].value == 'Travel') ){
										estimatedtototaltravelQuantity = estimatedtototaltravelQuantity + estimatedQunatity;
								}
							}
						}
					}
					if(estimatedtototallaborQuantity < 2) {
						defaultQuantity = 2;
						subTotalVal = defaultQuantity * typeVal;
						contractLabourCostGen = eval(eval(contractLabourCostGen) + eval(subTotalVal));
					} else  {
						for(var index=0;index<len;index++) {
							if(document.all.mpoActivityResource[index].checked == true)
								{
									if( document.all.mpoTypeName[index].value == 'Labor')
										{
											if(document.all.mpoActResourceSubName[index].value == 'Contract Labor' )
											{	
												estimatedQunatity = eval(eval(document.all.mpoRes[index].value) * eval(document.all.mpoAct[index].value))
												defaultQuantity = estimatedQunatity;
												subTotalVal = defaultQuantity * typeVal;
												contractLabourCostGen = eval(eval(contractLabourCostGen) + eval(subTotalVal));
											}
										}
								}
						}
					}
					//alert('travel quantity type = '+type+' and '+estimatedtototaltravelQuantity)
					if(estimatedtototaltravelQuantity < 1) {
						if(type == 'mm') {
							defaultQuantity = 1;
						} else {
							defaultQuantity = estimatedtototaltravelQuantity;
						}
						subTotalVal = defaultQuantity * typeVal;
						travelHourlyCostGen = eval(eval(travelHourlyCostGen) + eval(subTotalVal));
					} else {
						for(var index=0;index<len;index++)
							{
								if(document.all.mpoActivityResource[index].checked == true)
								{
									if(document.all.mpoActResourceSubName[index].value == 'Hourly' && (document.all.mpoActResourceSubNamePre[index].value == 'Contract Labor' || document.all.mpoActResourceSubNamePre[index].value == 'Travel') ){
										estimatedQunatity = eval(eval(document.all.mpoRes[index].value) * eval(document.all.mpoAct[index].value))
										defaultQuantity = estimatedQunatity;
										subTotalVal = defaultQuantity * typeVal;
										travelHourlyCostGen = eval(eval(travelHourlyCostGen) + eval(subTotalVal));
									}
								}
							}
					}
			}
			/* For Ticket only: End */
		
		var tableObj = document.getElementById('CVPT');
		var display_savings;
		if(type=='speed'){
			speedPayByContract = contractLabourCostGen;
			speedPayByHour = travelHourlyCostGen;
			tableObj.rows[2].cells[1].innerText = formatCurrency((speedPayByContract));
			tableObj.rows[2].cells[2].innerText = formatCurrency((speedPayByHour));
			tableObj.rows[2].cells[3].innerText = formatCurrency((speedPayByContract + speedPayByHour));
			display_savings = formatCurrency((eval(PVScontractLabourCost+PVStravelHourlyCost) - eval(speedPayByContract + speedPayByHour )));
			if(display_savings.substring(0,1) == '-') {
				tableObj.rows[2].cells[4].style.color = "red";
				tableObj.rows[2].cells[4].innerText = "("+display_savings.substring(1,display_savings.length)+")";
			} else {
				tableObj.rows[2].cells[4].style.color = "black";
				tableObj.rows[2].cells[4].innerText = formatCurrency((eval(PVScontractLabourCost+PVStravelHourlyCost) - eval(speedPayByContract + speedPayByHour )));
			}
		}
		else
		{
			mmByContract = contractLabourCostGen;
			mmByHour = travelHourlyCostGen;
			tableObj.rows[1].cells[1].innerText =formatCurrency( (mmByContract));
			tableObj.rows[1].cells[2].innerText =formatCurrency( (mmByHour));
			tableObj.rows[1].cells[3].innerText = formatCurrency((mmByContract) + (mmByHour)) ;
			display_savings = formatCurrency((eval(PVScontractLabourCost+PVStravelHourlyCost) - eval(mmByContract + mmByHour )));
			if(display_savings.substring(0,1) == '-') {
				tableObj.rows[1].cells[4].style.color = "red";
				tableObj.rows[1].cells[4].innerText = "("+display_savings.substring(1,display_savings.length)+")";
			} else {
				tableObj.rows[1].cells[4].style.color = "black";
				tableObj.rows[1].cells[4].innerText = formatCurrency((eval(PVScontractLabourCost+PVStravelHourlyCost) - eval(mmByContract + mmByHour )));
			}
		}
	}
}

/*
	This function is used at the time of page load.
*/
function onPageLoad() {
	if(isExists(document.all.mpoActivityResource)){	
		if(document.forms[0].poType.value == 'M' || document.forms[0].poType.value == 'S'){
			disableAllText();
			typeChanged('onpageload');
		}
		if(checkForFP == 'Update Quantity'){
			inableTextForFP();
		}
	}
	showDiffAuthorisedRow();
	getAuthorizedCostInputText();
}


/*
	This function is used for inable text at the time of form saving.
*/
function inableTextForFP()
{
	var len = document.forms[0].mpoActivityResource.length;
	tableObj = document.getElementById('actTable');
	if(!len)
	{	
		if(document.all.mpoActivityResource.checked)
		{
			if(document.forms[0].mpoActivityPlanUnitCost.disabled){
				document.forms[0].mpoActivityPlanQuantity.disabled = false;
				document.forms[0].mpoActivityPlanUnitCost.disabled = true;
			}
		}
	}
	else
	{
		for(var i=0;i<len;i++)
		{
			
			if(document.all.mpoActivityResource[i].checked)
			{
				if(document.forms[0].mpoActivityPlanQuantity[i].disabled){
					//document.all.mpoActivityResource[i].disabled = false;
					document.forms[0].mpoActivityPlanQuantity[i].disabled = false;
					document.forms[0].mpoActivityPlanUnitCost[i].disabled = true;
				}
			}
		}
	}
}
/*
	This function is used to check Intransit resource quantity.
*/
function checkIntransitQty(transitValue, obj) {
	if(transitValue == 'Y' && eval(obj.value > 1)) {
		alert('PO In-Transit resource quantity can not be greater than 1.');
		obj.value = 0.0;
		obj.focus();
	}
	//showDiffAuthorisedRow(); 
}
/*
	This function is used to validate an object only for numeric value.
*/
function checkFloatAuthorizedinput(obj) {
		if(!isFloat(document.forms[0].authorizedCostInput.value)){
			alert('Only numeric value is allowed.');
			document.forms[0].authorizedCostInput.value = ''
			document.forms[0].authorizedCostInput.focus();
			return false;
		}
}
/*
	This function is used for ......
*/
function changeAuthorizedInput() {
		if(eval(document.forms[0].estimatedCost.value) > 0) {
			if(eval(document.forms[0].authorizedCostInput.value) > eval((105 * eval(document.forms[0].estimatedCost.value))/100)) {
						alert('Authorized Total cannot exceed Estimated Total Cost by 5%.');
						document.getElementById('AuthorizedInputExceeds').style.display = '';
						document.forms[0].authorizedCostInput.focus();
						return false;
			} else {
				document.getElementById('AuthorizedInputExceeds').style.display = 'none';
				return true;
			}
		} else {
			return true;;
		}
}
/*
	This function is used for ......
*/
function formatCurrency(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '$' + num + '.' + cents);
}
/*
	This function is used for ......
*/	
function round_funcfor4digits(num) {
		if(isNaN(num)) num = 0;
		num = parseFloat(num);
		num = num;
		num = num.toFixed(4);
		return num;
}
/*
	This function is used for Save form value.
*/	

function onFPSave(obj,SPMRole){
	if(onSave(obj,SPMRole) ){
		
	}
}


function onSave(obj,SPMRole)
{
	trimTextAreaFields();
	if(obj == '0')
	{
		var percentageEstimatedCost;
		var percentageEstimatedTotalCost;
		var activityPlanSubTotal = 0;
		var alertMsg = '';
		var conditionCheck = 'pass';
	
		if(document.all.poType.value == '0')
		{
			alert('Please select one of the POType.')
			return false;
		}
		if(document.all.poType.value == 'P')
			{
				if(document.all.pvsJustification.value=='0'){
					alert("Please Select PVS Justification");
					return false;
				}
				if(document.all.pvsJustification.value=='1'){
					alert("Please Select Non Not Applicable PVS Justification");
					return false;
				}
				
			}
		//added for geographic constraint
		if(document.all.poType.value == 'P' && document.all.pvsJustification.value=='5')
		{
			if(document.all.geographicConstraint.value=='0'){
				alert("Please Select Geographic Constraint");
				return false;
			}
		}
		//added check for core competency
		if(document.all.poType.value == 'P' && document.all.pvsJustification.value=='5' && document.all.geographicConstraint.value=='4')
		{
			if($('.competencyCheck:checked').val()==undefined)
				{
				alert('Please Select at least one core competency');
				return false;
				}
		}
				
var len = document.all.mpoActivityResource.length;
		if(!len)
		{
			if(!document.all.mpoActivityResource.checked)
				{
				if(eval(document.forms[0].authorizedCostInput.value) != eval(0.0))
					{
				alertMsg='Could Not Save PO. At Least One Resource should Be Checked';
				conditionCheck='fail';
				if(conditionCheck == 'fail')
				{
					//alert('Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by '+multiFactor+'%. ');		
					alert(alertMsg);
					noOfRows = eval(eval(requestlistSize)+2);
					//tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = "Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by "+multiFactor+"%. "
					tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = alertMsg;
					alert(tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText);
					tableObj.rows(eval(eval(noOfRows)+3)).cells(2).className ="Ntextoleftalignnowrappaleyellowborder4";
					textObj.focus();
				} else{
					noOfRows = eval(eval(requestlistSize)+2);
					tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = "";
					tableObj.rows(eval(eval(noOfRows)+3)).cells(2).className ="Ntextoleftalignnowrappaleyellowborder4";
				}
				return false;
				}
				}
		}
		else
			{
			var totalResourceSelected=0;
			for(var i=0;i<len;++i){
				if(document.all.mpoActivityResource[i].checked)
					{
					totalResourceSelected=totalResourceSelected+1;
					}
			}
				if(totalResourceSelected==0 && (eval(document.forms[0].authorizedCostInput.value) != eval(0.0)))
					{
					alertMsg='The PO cannot be saved.  Select all resources which drive the authorized cost entered below and click Save again.';
					conditionCheck='fail';
					if(conditionCheck == 'fail')
					{
						//alert('Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by '+multiFactor+'%. ');		
						alert(alertMsg);
						noOfRows = eval(eval(requestlistSize)+2);
						//tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = "Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by "+multiFactor+"%. "
						tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = alertMsg;
						alert(tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText);
						tableObj.rows(eval(eval(noOfRows)+3)).cells(2).className ="Ntextoleftalignnowrappaleyellowborder4";
						textObj.focus();
					} else{
						noOfRows = eval(eval(requestlistSize)+2);
						tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = "";
						tableObj.rows(eval(eval(noOfRows)+3)).cells(2).className ="Ntextoleftalignnowrappaleyellowborder4";
					}
					return false;
					}
			}
			
		if(materialTotalCost>500.00 && ((document.all.poType.value=='M')||(document.all.poType.value=='S')))
		{
			alertMsg="This MM or SP PO exceeds the $500 limit on material costs. \n Adjust the select materials or consult your supervisor for assistance.";
		conditionCheck='fail';
		if(conditionCheck == 'fail')
		{
			//alert('Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by '+multiFactor+'%. ');		
			alert(alertMsg);
			noOfRows = eval(eval(requestlistSize)+2);
			//tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = "Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by "+multiFactor+"%. "
			tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = alertMsg;
			alert(tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText);
			tableObj.rows(eval(eval(noOfRows)+3)).cells(2).className ="Ntextoleftalignnowrappaleyellowborder4";
			textObj.focus();
		} else{
			noOfRows = eval(eval(requestlistSize)+2);
			tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = ""
			tableObj.rows(eval(eval(noOfRows)+3)).cells(2).className ="Ntextoleftalignnowrappaleyellowborder4";
		}
		return false;
		}
		if(updatable != 'Update Quantity') {
			if(document.all.poType.disabled==true)
			{
				document.all.poType.disabled = false;
			}
		}
		if(document.all.terms.value == '0')
		{
			alert('Please select one of the Terms.')
			return false;
		}
		if(isExists(document.all.mpoActivityResource))
		{
			var len = document.all.mpoActivityResource.length;

			if(!len)
			{
				if( document.all.mpoActivityIsWOShow.checked == true && document.all.mpoActivityResource.checked != true ){
						alert('Firstly resource needs to be checked.')
						return false;
				}
				if(document.all.mpoActivityResource.checked != true ){
					document.all.resourceHidden.value=document.all.mpoActivityResource.value+"~"+"N";
				}
				else
				{
					document.all.resourceHidden.value=document.all.mpoActivityResource.value+"~"+"Y";
				}
				if(document.all.mpoActivityResource.checked == true ){
					if( document.all.mpoActivityIsWOShow.checked != true){
						document.all.isShowHidden.value='N'
					}
				}
				if(document.all.mpoActivityResource.checked == true)
				{
					if(document.all.mpoActivityPlanQuantity.value == ''){
						alert('Please fill Planned Quantity.');
						document.all.mpoActivityPlanQuantity.focus();
						return false;
					}
					if(document.all.mpoActivityPlanUnitCost.value == ''){
						alert('Please fill Planned Unit Cost.');
						document.all.mpoActivityPlanUnitCost.focus();
						return false;
					}
				}
					
					if(document.all.mpoActivityResource.checked == true)
					{
						if(document.all.mpoActivityPlanQuantity.value != '' && document.all.mpoActivityPlanUnitCost.value != ''){
							document.all.mpoTotal.value = (eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
							document.all.mpoSubTotal.value = (eval (document.forms[0].mpoUnit.value * eval (document.all.mpoTotal.value)));
							document.all.mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);

							/*
							if(eval(document.all.mpoSubTotal.value) == eval(0.0) || eval(document.all.mpoActivityPlanSubTotal.value) == eval(0.0)){
								percentageEstimatedCost = ( document.all.mpoUnit.value * multiFactor )/100;
								percentageEstimatedTotalCost = eval(document.all.mpoUnit.value) + eval(percentageEstimatedCost);
								activityPlanSubTotal = document.all.mpoActivityPlanUnitCost.value;
							} else{
								percentageEstimatedCost = ( document.all.mpoSubTotal.value * multiFactor )/100;
								percentageEstimatedTotalCost = eval(document.all.mpoSubTotal.value) + eval(percentageEstimatedCost);
								activityPlanSubTotal = document.all.mpoActivityPlanSubTotal.value;
							}
							*/
							
							if(document.all.mpoActivityPlanSubTotal.value != 0.0 && document.all.mpoSubTotal.value == 0.0) {
								alertMsg = 'The planned subtotal cannot be computed since there is no estimated subtotal cost. \nThis appears to be an hourly pricing scenario so only the planned unit cost should be entered';
								conditionCheck = 'fail'
							} else {
								planSubTotal = document.all.mpoActivityPlanUnitCost.value;
								percentageEstimatedCost = ( document.all.mpoUnit.value * multiFactor )/100;
								percentageEstimatedTotalCost = eval(document.all.mpoUnit.value) + eval(percentageEstimatedCost);
								if(planSubTotal > percentageEstimatedTotalCost) {
									alertMsg = 'To be valid, the planned unit cost cannot \nexceed the corresponding estimated unit cost by more than '+multiFactor+'%';
									conditionCheck = 'fail';
								}
							}
							
							if(conditionCheck == 'pass') {
								if(document.all.mpoActivityPlanSubTotal.value != 0.0 && document.all.mpoSubTotal.value != 0.0) {
									planSubTotal = document.all.mpoActivityPlanSubTotal.value;
									percentageEstimatedCost = ( document.all.mpoSubTotal.value * multiFactor )/100;
									percentageEstimatedTotalCost = eval(document.all.mpoSubTotal.value) + eval(percentageEstimatedCost);
									if(planSubTotal > percentageEstimatedTotalCost) {
										alertMsg = 'To be valid, the planned unit and subtotal cost cannot \nexceed the corresponding estimated values by more than '+multiFactor+'%';
										conditionCheck = 'fail';
									}
								}
							}
							
							//if(activityPlanSubTotal > percentageEstimatedTotalCost)
							if(conditionCheck == 'fail')
							{
								//alert('Planned Subtotal/Unit Cost cannot exceed Estimated Subtotal/Unit Cost by '+multiFactor+'%.');
								alert(alertMsg);
								//document.forms[0].Save.disabled = "true";
								if(!document.forms[0].mpoActivityPlanUnitCost.disabled){
									document.forms[0].mpoActivityPlanUnitCost.focus();
								}
								return false;
							}
							
							
						}
					}
			}
			else
			{
				var isCheck = false;
				for(var i=0;i<len;i++)
				{
					if( document.all.mpoActivityIsWOShow[i].checked == true && document.all.mpoActivityResource[i].checked != true ){
						alert('Firstly resource needs to be checked.')
						return false;
					}
					if(document.all.mpoActivityResource[i].checked != true ){
						document.all.resourceHidden[i].value=document.all.mpoActivityResource[i].value+"~"+"N";
					}
					else
					{
						isCheck = true;
						document.all.resourceHidden[i].value=document.all.mpoActivityResource[i].value+"~"+"Y";
					}
					if(document.all.mpoActivityResource[i].checked == true ){
						
						if( document.all.mpoActivityIsWOShow[i].checked != true){
							document.all.isShowHidden[i].value='N'
						}
					}
						if(document.all.mpoActivityResource[i].checked == true)
						{
							if(isBlank(document.all.mpoActivityPlanQuantity[i].value)){
								alert('Please fill Planned Quantity.');
								document.all.mpoActivityPlanQuantity[i].focus();
								return false;
							}
							if(isBlank(document.all.mpoActivityPlanUnitCost[i].value) ){
								alert('Please fill Planned Unit Cost.');
								document.all.mpoActivityPlanUnitCost[i].focus();
								return false;
							}
						}
						
						if(document.all.mpoActivityResource[i].checked == true)
						{
							if(document.all.mpoActivityPlanQuantity[i].value != '' && document.all.mpoActivityPlanUnitCost[i].value != ''){
								document.all.mpoTotal[i].value = (eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value));
								document.all.mpoSubTotal[i].value = (eval (document.forms[0].mpoUnit[i].value * eval (document.all.mpoTotal[i].value)));
								document.all.mpoActivityPlanSubTotal[i].value = (document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
								/*
								if(eval(document.all.mpoSubTotal[i].value) == 0.0 || eval(document.all.mpoActivityPlanSubTotal[i].value) == 0.0){
									percentageEstimatedCost = ( document.all.mpoUnit[i].value * multiFactor )/100;
									percentageEstimatedTotalCost = eval(document.all.mpoUnit[i].value) + eval(percentageEstimatedCost);
									activityPlanSubTotal = document.all.mpoActivityPlanUnitCost[i].value;
								} else{
									percentageEstimatedCost = ( document.all.mpoSubTotal[i].value * multiFactor )/100;
									percentageEstimatedTotalCost = eval(document.all.mpoSubTotal[i].value) + eval(percentageEstimatedCost);
									activityPlanSubTotal = document.all.mpoActivityPlanSubTotal[i].value;
								}
								*/
								

								if(document.all.mpoActivityPlanSubTotal[i].value != 0.0 && document.all.mpoSubTotal[i].value == 0.0) {
									alertMsg = 'The planned subtotal cannot be computed since there is no estimated subtotal cost. \nThis appears to be an hourly pricing scenario so only the planned unit cost should be entered';
									conditionCheck = 'fail'
								} else {
									planSubTotal = document.all.mpoActivityPlanUnitCost[i].value;
									percentageEstimatedCost = ( document.all.mpoUnit[i].value * multiFactor )/100;
									percentageEstimatedTotalCost = eval(document.all.mpoUnit[i].value) + eval(percentageEstimatedCost);
									if(planSubTotal > percentageEstimatedTotalCost) {
										alertMsg = 'To be valid, the planned unit cost cannot \nexceed the corresponding estimated unit cost by more than '+multiFactor+'%';
										conditionCheck = 'fail';
									}
								}

								if(conditionCheck == 'pass') {
									if(document.all.mpoActivityPlanSubTotal[i].value != 0.0 && document.all.mpoSubTotal[i].value != 0.0) {
										planSubTotal = document.all.mpoActivityPlanSubTotal[i].value;
										percentageEstimatedCost = ( document.all.mpoSubTotal[i].value * multiFactor )/100;
										percentageEstimatedTotalCost = eval(document.all.mpoSubTotal[i].value) + eval(percentageEstimatedCost);
											if(planSubTotal > percentageEstimatedTotalCost) {
												alertMsg = 'To be valid, the planned unit and subtotal cost cannot \nexceed the corresponding estimated values by more than '+multiFactor+'%';
												conditionCheck = 'fail';
											}
										}
									}

								
								//if(activityPlanSubTotal > percentageEstimatedTotalCost)
								if(conditionCheck == 'fail')
								{
									//alert('Planned Subtotal Cost cannot exceed Estimated Subtotal Cost by '+multiFactor+'%.');
									alert(alertMsg);
									if(document.forms[0].mpoActivityPlanUnitCost[i].disabled == false){
										document.forms[0].mpoActivityPlanUnitCost[i].focus();
									}else{
										document.forms[0].mpoActivityPlanQuantity[i].focus();
									}
									return false;
								}
								
								
							}
						}
				}
			}
		}
		if(document.forms[0].masterPOItem.value == '2' && document.forms[0].appendixType.value != 'NetMedX') {
			if(eval(document.forms[0].estimatedCost.value) > 0 && (isBlank(document.forms[0].authorizedCostInput.value) || eval(document.forms[0].authorizedCostInput.value) == eval(0.0))){
				alert('Please enter value for Authorized Cost.');
				document.forms[0].authorizedCostInput.focus();
				return false;
			}
			if(!changeAuthorizedInput()) {
				return false;
			}
		}
	}
	enableAllText();
	//author : Lalit Goyal
	//For SPR[108] Minumteman Minimum Cost PO Validation Method 
	//No resources are selected and cost is set to 0.0 then no other validation should be applied 
	//When document.all.mpoActivityResource.length = undefined
	if((!document.all.mpoActivityResource.length) && (!document.all.mpoActivityResource.checked))
		{
			if(eval(document.forms[0].authorizedCostInput.value) != eval(0.0)){
					if(document.forms[0].poType.value=='M' ){
					validateOverride(SPMRole);
					}
			}
		}
	//If the resources are their in PO
	else{
	if(!(totalResourceSelected==0 && (eval(document.forms[0].authorizedCostInput.value) == eval(0.0)))){
	if(document.forms[0].poType.value=='M' )
		{
		validateOverride(SPMRole);
		}
	}
}
			if(updatable == 'Update Quantity') {
				var url = "POAction.do?hmode=SaveFixedPriceQuantity&tabId="+obj;
			} else if(updatable != 'Update Quantity') {
				var url = "POAction.do?hmode=Save&tabId="+obj;
			}
		//enabled pvs justification and geographic constraint value
		document.all.pvsJustification.disabled=false;
		document.all.geographicConstraint.disabled=false;
		document.forms[0].action = url;
		document.forms[0].submit();
}
/*
	This function is used to enable All disabled object.
*/
function enableAllText()
{
	var len = document.forms[0].mpoActivityResource.length;
	tableObj = document.getElementById('actTable');
	if(!len)
	{	
		if(document.all.mpoActivityResource.checked == true)
		{
			if(document.forms[0].mpoActivityPlanUnitCost.disabled == true){
				document.forms[0].mpoActivityPlanUnitCost.disabled = false;
			}
		}
	}
	else
	{
		for(var i=0;i<len;i++)
		{
			if(document.forms[0].mpoActivityPlanUnitCost[i].disabled == true){
				document.forms[0].mpoActivityPlanUnitCost[i].disabled = false;
			}
			if(document.forms[0].mpoActivityPlanQuantity[i].disabled == true){
				document.forms[0].mpoActivityPlanQuantity[i].disabled = false;
			}
		}
	}
	if(updatable != 'Update Quantity') {
		if(document.forms[0].poType.disable){ document.forms[0].poType.disable = false; }
	}
	
}
/*
	This function is used to check whether PO is Internal or not.
*/

function checkNonInernalPOs(){
	if(isExists(document.all.mpoActivityResource)){
		var len = document.forms[0].mpoActivityResource.length;
		var tableObj = document.getElementById('actTable');
		if(!len){
		
			if(document.forms[0].poType.value == 'N'){
				if(isEditablePo) {
					document.all.mpoActivityResource.disabled = false;
					document.forms[0].mpoActivityPlanUnitCost.disabled = false;
					document.forms[0].mpoActivityPlanQuantity.disabled = false;
					document.forms[0].mpoActivityIsWOShow.disabled = false;
				}
			} else{
				if(document.all.mpoActResourceSubName.value == 'Corporate Labor' || document.all.mpoActResourceSubNamePre.value == 'Corporate Labor'){
					document.all.mpoActivityResource.checked = false;
					document.all.mpoActivityResource.disabled = true;
					document.forms[0].mpoActivityPlanUnitCost.value = ''
					document.forms[0].mpoActivityPlanQuantity.value = '';
					document.forms[0].mpoActivityPlanUnitCost.disabled = true;
					document.forms[0].mpoActivityPlanQuantity.disabled = true;
					document.forms[0].mpoActivityIsWOShow.disabled = true;
					document.forms[0].mpoActivityIsWOShow.checked = false;
					document.all.mpoActivityPlanSubTotal.value = 0;	
					tableObj.rows[2].cells[11].innerText = '0.00';
				}
			}
		} else{
		
			for(var i=0;i<len;i++){
				if(document.forms[0].poType.value == 'N'){
					if(isEditablePo) {
						document.all.mpoActivityResource[i].disabled = false;
						document.forms[0].mpoActivityPlanUnitCost[i].disabled = false;
						document.forms[0].mpoActivityPlanQuantity[i].disabled = false;
						document.forms[0].mpoActivityIsWOShow[i].disabled = false;
					}
				} else{
					if(document.all.mpoActResourceSubName[i].value == 'Corporate Labor' || document.all.mpoActResourceSubNamePre[i].value == 'Corporate Labor'){
						document.all.mpoActivityResource[i].disabled = true;
						document.all.mpoActivityResource[i].checked = false;
						document.forms[0].mpoActivityPlanUnitCost[i].value = '';
						document.forms[0].mpoActivityPlanUnitCost[i].disabled = true;
						document.forms[0].mpoActivityPlanQuantity[i].value = '';
						document.forms[0].mpoActivityPlanQuantity[i].disabled = true;
						document.forms[0].mpoActivityIsWOShow[i].disabled = true;
						document.forms[0].mpoActivityIsWOShow[i].checked = false;
						document.all.mpoActivityPlanSubTotal[i].value = 0;	
						tableObj.rows[i+2].cells[11].innerText = '0.00';
					}
				}
			}
		}
		setCost();
	}
}
//For SPR[108] Minumteman Minimum Cost PO Validation Method 
//author : Lalit Goyal
function validateOverride(SPMRole){
	
	if(document.forms[0].overrideMinumtemanCostChecked.checked){
		document.forms[0].overrideMinumtemanCostChecked.value=1;
		document.forms[0].overrideMinumtemanCostCheckedHidden.value=1;
	}else{
		document.forms[0].overrideMinumtemanCostChecked.value=0;
		document.forms[0].overrideMinumtemanCostCheckedHidden.value=0;
	}
	var totCostToOverRide =((eval(PVScontractLabourCost) + eval(PVStravelHourlyCost)+eval(PVStravelFixedCost)));
	//In the Case the User is Sr Program Manager or Program Manager
	if(SPMRole=='true'  && document.forms[0].masterPOItem.value=='2'){
		
		//Condition 1 
		if((totCostToOverRide<"54.00") && (document.forms[0].overrideMinumtemanCostChecked.checked==false)){
			alertMsg="The estimated labor and travel are not sufficient to meet the Minuteman minimum authorized cost\nConsult your Sr. PM to resolve this problem.";
			alert(alertMsg);
			noOfRows = eval(eval(requestlistSize)+2);
			tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = alertMsg;
			tableObj.rows(eval(eval(noOfRows)+3)).cells(2).className ="Ntextoleftalignnowrappaleyellowborder4";
			textObj.focus();
			return false;
		}
		//Condition 2
		else if((totCostToOverRide>="54.00") && (document.forms[0].overrideMinumtemanCostChecked.checked==false)){
			// if the override Minutean Minimum Cost Checkbox is not checked and Total PVS Cost labor and travel resources is >=$54.00
				updateAuthorizedCostInput();
				}
	}
	//In the Case the User is other than Sr Program Manager or Program Manager
	else if(SPMRole=='false'  && document.forms[0].masterPOItem.value=='2'){
	
		//Condition 1
		if(totCostToOverRide<"54.00" && (document.forms[0].overrideMinumtemanCostChecked.checked==false)){
			alertMsg='The estimated labor and travel are not sufficient to meet the Minuteman minimum authorized cost. \nConsult your Sr. PM to resolve this problem.';
			alert(alertMsg);
			noOfRows = eval(eval(requestlistSize)+2);
			tableObj.rows(eval(eval(noOfRows)+3)).cells(2).innerText = alertMsg;
			tableObj.rows(eval(eval(noOfRows)+3)).cells(2).className ="Ntextoleftalignnowrappaleyellowborder4";
			textObj.focus();
			return false;
		}	
		//Condition 2
		else if((totCostToOverRide>="54.00") && (document.forms[0].overrideMinumtemanCostChecked.checked==false)){
			// if the override Minutean Minimum Cost Checkbox is not checked and Total PVS Cost labor and travel resources is >=$54.00
				updateAuthorizedCostInput();
				}
	}
}
//For SPR[108] Minumteman Minimum Cost PO Validation Method 
function updateAuthorizedCostInput()
{
	var x;
	var tempAuthorizedCostInput;
	//Sum of the MaterialCost and FrieghtCost under the Authorized Cost Breakdown
	var totSumCostMaterialfreight =eval(eval(materialCost)+eval(frieghtCost));
	//Check whether the authorizedCostInput is blank or 0.0 
	if(isBlank(document.forms[0].authorizedCostInput.value) || eval(document.forms[0].authorizedCostInput.value) == eval(0.0)){
		tempAuthorizedCostInput=0.0;
		x=eval(eval(0.0)-eval(totSumCostMaterialfreight));
	}else{
		tempAuthorizedCostInput=eval(document.forms[0].authorizedCostInput.value);
		x=eval(eval(document.forms[0].authorizedCostInput.value)-eval(totSumCostMaterialfreight));
	}
	if(x<"54.00")
		{
		var y=eval(54.00)-eval(x);
		document.forms[0].authorizedCostInput.value=eval(eval(tempAuthorizedCostInput)+eval(y));
		}
	}
