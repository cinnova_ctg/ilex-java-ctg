/**
 @author MIND
 These are common function.
 */
/* Return matched ComboBox's text */
function getSelectedOption(selectObj, inputText) {
    for (var i = 0; i < selectObj.length; i++) {
        var text = selectObj.options[i].text;
        if (text == inputText) {
            return selectObj.options[i].value;
        }
    }
} // - End of getSelectedOption() function
/* Return value of selected redio button */
function getRedioValue(radioObj) {
    for (var i = 0; i < radioObj.length; i++) {
        if (radioObj[i].checked) {
            return radioObj[i].value;
        }
    }
} // - End of getRedioValue() function
/* Return index of selected redio button */
function getRedioSelectedIndex(radioObj) {
    for (var i = 0; i < radioObj.length; i++) {
        if (radioObj[i].checked) {
            return i;
        }
    }
} // - End of getRedioSelectedIndex() function
/* get Resourcelevel list based on criticality using Ajax. */
function appendAjaxData(obj, bool, data)
{
    dwr.util.removeAllOptions(obj);
    dwr.util.addOptions(obj, eval('[' + data + ']'), 'id', 'name');
    obj = dwr.util.byId(obj);
    obj.style.display = "block";
    if (bool) {
        for (var intLoop = 0; intLoop < obj.length; intLoop++) {
            if (obj[intLoop].value == '-1') {
                obj[intLoop].selected = true;
                //fillData(obj);
            }
        }
    }
    else {
        for (var intLoop = 0; intLoop < obj.length; intLoop++) {
            obj[0].selected = true;
            //fillData(obj);
            break;
        }
    }
}
/* Change Criticality to '1-Emergency' and resource level to 'Systems Engineer' when helpDesk is 'Y'.*/
function setHelpDeskCriticality(radioObj) {
    if (radioObj.value == 'Y') {
        var value = getSelectedOption(document.forms[0].criticality, '1-Emergency');
        document.forms[0].criticality.value = value;
    }
    setResourceLeve(radioObj);
}
function fillResourceCombo()
{
    if (document.all.criticality.value == '0')
    {
        dwr.util.removeAllOptions('resource');
        alert('Please select criticality.')
        return false;
    }
    JobTicketAction.fillCombo(document.all.appendixId.value, document.all.criticality.value, '0', {callback: function(response) {
            appendAjaxData('resource', false, response);
        }, async: false});
    checkResourceLevel();
}
function checkResourceLevel() {
    /* For Help Desk */
    var i = getRedioSelectedIndex(document.forms[0].helpDesk);
    setResourceLeve(document.forms[0].helpDesk[i]);

}
function showWindow(e, jobId) {
    var div = $('#ajaxdiv');
    var x = 0;
    var y = 0;

    if (e != '') {
        x = e.clientX;
        y = e.clientY + document.all.pbody.scrollTop;
    }

    div.css('left', x - 100);
    div.css('top', y);

    $.ajax({
        url: 'JobTicketInfo.do?jobId=' + jobId,
        success: function(response) {
            $('#ajaxdiv').html(response);
        }
    });
    //myWindow=window.open(str,'windowName','width=100,height=120,screenX=' + x + ',screenY=' + y + ',left=' + x + ',top=' + y);
    //myWindow.focus();
}
function disableStatus(res) {
    //document.all.ajaxdiv.innerText=res.response.Text;
}
/* Use for papulate combobox: Start */
function papulateDropDown(id, obj) {
    /* This function call Ajax which is inmplemented into /javascript/prototype.js */
    var url = "JobTicketAction.do?hmode=ResourceComboList&appendixId=" + id + "&criticalitiId=" + obj.value;
    var pars = "";
    $.ajax({
        type: "GET",
        url: url,
        data: pars,
        error: function(request, status, error) {
            $('#resourceDropDown').html("Error!");
        },
        success: function(text) {
            fillCombo(text);
        }
    });
}
function fillCombo(text) {
    var dropDownSpan = $("#resourceDropDown");

    dropDownSpan.html(text);

    toggleCombo();
}
function toggleCombo() {
    var dropDownSelect = $("#resourceDropDown select");

    if ($('input[name="helpDesk"]').val() == 'Y') {
        dropDownSelect.attr('disabled', 'disabled');
    } else {
        dropDownSelect.removeAttr('disabled');
    }

    if (typeof hasJobId !== "undefined" && hasJobId) {
        dropDownSelect.attr('disabled', 'disabled');
    } else {
        dropDownSelect.removeAttr('disabled');
    }
}
/*Use for papulate combobox: End*/
/*Vailidate Email Address: Start*/
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}
/*Vailidate Email Address: End*/
/*Vailidate All Address: Start*/
function isValidAllEmailAddresses(allEmailAddresses) {
    var arrayVar = new Array();
    arrayVar = allEmailAddresses.toString().split(";");
    for (var i = 0; i < arrayVar.length; i++) {
        if (arrayVar[i] != '') {
            if (!isValidEmailAddress(arrayVar[i])) {
                return false;
            }
        }
    }
    return true;
}
/*Vailidate All Address: End*/
/* Replaces all occurances of single quotes within a string. */
function replaceSingleQuotesinString(strval) {
    var strLen = strval.length;
    var finalStr = '';
    var index = -1;

    if (strval.length > 0) {
        index = strval.indexOf('\'');
        while (index >= 0) {
            if (index == 0) {
                finalStr = finalStr + '\\' + strval.substring(0, index + 1);

            } else {
                finalStr = finalStr + strval.substring(0, index) + '\\' + strval.substring(index, index + 1);
            }

            strval = strval.substring(index + 1);
            index = strval.indexOf('\'');
        }

        finalStr = finalStr + strval;
    }
    return finalStr;
}
