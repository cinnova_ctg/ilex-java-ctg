	function trimAll( strValue ) {
	/************************************************
	DESCRIPTION: Removes leading and trailing spaces.
	
	PARAMETERS: Source string from which spaces will
	  be removed;
	
	RETURNS: Source string with whitespaces removed.
	*************************************************/ 
	 var objRegExp = /^(\s*)$/;
	
		//check for all spaces
		if(objRegExp.test(strValue)) {
		   strValue = strValue.replace(objRegExp, '');
		   if( strValue.length == 0)
			  return strValue;
		}
		
	   //check for leading & trailing spaces
	   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
	   if(objRegExp.test(strValue)) {
		   //remove leading and trailing whitespace characters
		   strValue = strValue.replace(objRegExp, '$2');
		}
	  return strValue;
	
	}

	function validateNotEmpty( form_field,strmsg ) {
	/************************************************
	DESCRIPTION: Validates that a string is not all
	  blank (whitespace) characters.
		
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	   var strTemp = form_field.value;
	   strTemp = trimAll(strTemp);
	   if(strTemp.length > 0){
		 return true;
	   }  
	   else
	   {
			alert(strmsg);		
			form_field.focus();
			return false;
		}	
	}

function validDateMDY(formField, message)
	{
		var result = true;
		if (!validateNotEmpty(formField, message))
			result = false;
		if (result)
		{
			var elems = formField.value.split('/');
			result = (elems.length == 3); // should be three components
			if (result)
			{
				var day = parseInt(elems[1]);
				var month = parseInt(elems[0]);
				var year = parseInt(elems[2]);
				//alert(month + "/" + day + "/" + year);
				result = !isNaN(month) && (month > 0) && (month < 13) && !isNaN(day) && (day > 0) && (day < 32) && !isNaN(year) && (elems[2].length == 4);
			}
			if (!result)
			{
				//alert(month + "/" + day + "/" + year);
				alert(message);
				formField.focus();
			}
		}
		return result;
	}
/*---------------------------------------------------*/
function $(id){return document.getElementById(id);}
/*---------------------------------------------------*/
sfHover = function() {
	var sfEls = parent.frames("content").document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" over";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" over\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
/*---------------------------------------------------*/
filterHover = function(){
	try{
		$("closeNow").onclick = function(){
			if(this.className == "") {
				this.className+=" over";
				$("anc_close").style.display = "block";
			} else {
				$('closeNow').className = ''; 
				$("anc_close").style.display = 'none';
			}
		}
	}
	catch(ex){alert(ex);};
}//end of function
//if (window.attachEvent) window.attachEvent("onload", filterHover);
/*---------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------*/
function hideSpan(id, bttn, statusFalse, statusTrue){
bttn.src = (bttn.src.toString().indexOf(statusFalse)>-1)?statusTrue:statusFalse;
var sp = document.getElementById(id);
sp.checked = !sp.checked
}//end of function

	if(window.attachEvent){
		window.attachEvent("onload", filterHover);
	} else {
		if(document.addEventListener){
			document.addEventListener("load", filterHover, false);	
		};
		if(window.addEventListener){
			window.addEventListener("load", filterHover, false);	
		};
	};
