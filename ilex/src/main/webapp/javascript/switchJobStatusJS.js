function validateGo() {
	if(trim(document.forms[0].invoiceNumber.value) == '') {
		alert('Please enter an Invoice Number.');
		document.forms[0].invoiceNumber.focus();
		return false;
	}	
}

function switchStatusForJob(jobId) {
	document.forms[0].action = "SwitchJobStatusAction.do?switchJob=switchJob&jobId="+jobId;
	document.forms[0].submit();			
}