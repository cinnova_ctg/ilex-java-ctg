<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id="dcExportStatusList" name="dcExportStatusList" scope="request"/>
<%
String id = "";
String type = ""; 
String rdofilter = "";

if( request.getAttribute( "id" ) != null )
	id = ""+request.getAttribute( "id" );

if( request.getAttribute( "type" ) != null )
	type = ""+request.getAttribute( "type" );

int joblist_size =( int ) Integer.parseInt( request.getAttribute( "joblistlength" ).toString()); 

String module = "";
if(session.getAttribute("tRModule") != null) {
	module = session.getAttribute("tRModule").toString();
}

%>
<head>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/jquery-1.11.0.min.js"></script>
	<script language = "JavaScript" src = "javascript/UtilityFunction.js"></script>
	<style>
	.pcstdtext{
font-family:Verdana, Arial, Helvetica, sans-serif;  padding-left:2;font-size:11px;
font-weight:normal; 
color:#000000;  
}
	</style>
</head>

<html:form action="InvoiceJobDetail" > 
<body id = "pbody"  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');load();" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr> 
    <td  width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr> 
          <% if(type.equals("Appendix")) { %>
          <td width="155" class="toprow1"><a href="AppendixHeader.do?function=view&appendixid=<bean:write name="InvoiceJobDetailForm" property="id" />&viewjobtype=<bean:write name="InvoiceJobDetailForm" property="viewjobtype"/>&ownerId=<bean:write name="InvoiceJobDetailForm" property="ownerId"/>" onmouseout="MM_swapImgRestore();" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">Appendix DashBoard</a></td>
          <% } %>
          <td width="700" class="toprow1">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<html:hidden property="function_type"/>
<html:hidden property="function"/>
<html:hidden property="id"/>
<html:hidden property="type"/>
<html:hidden property = "nettype" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "invoice_Flag"/>
<html:hidden property = "toggle"/>
<html:hidden property = "problem_summary"/>
<table  border = "0" cellspacing = "1" cellpadding = "1" >
 <tr>
  <td><img src = "images/spacer.gif" width = "2" height = "0"></td>
  <td>
  	<table border = "0" cellspacing = "1" cellpadding = "1" width="1000">
    	<logic:present name = "retval" scope = "request">
	    	<tr>
				<td  colspan = "14" class = "message" height = "30">
		    	 <logic:equal name = "retval" value = "0">
		    		<bean:message bundle = "IM" key = "im.update.success"/>
		    	</logic:equal>
	
		    	<logic:equal name = "retval" value = "-9001">
    				<bean:message bundle = "IM" key = "im.update.failure"/>
				</logic:equal>
				</td>
		      </tr>
  		  </logic:present>
  
	  <logic:present name = "InvoiceJobDetailForm" property="selectmessage">
		 <logic:equal name="InvoiceJobDetailForm" property="selectmessage" value="9999">
	  	 	<tr>
  			 <td class="labeleboldwhite" colspan="1">&nbsp;</td>  
		  	 <td class="message" colspan="14" height="30">
  	 			<bean:message bundle="IM" key="im.selectajob"/>
			 </td>
			</tr>		
		 </logic:equal>
	 </logic:present>
	 			    	
	   		<logic:notEqual name="InvoiceJobDetailForm" property ="invoice_Flag" value="false">
	   		<tr>
	   		<td>&nbsp;</td> 
	   		<td class="labeleboldwhite" height="30"  colspan="21">
	   		<% if(type.equals("All")) { %>
	   			<bean:message bundle = "IM" key = "im.alljobs"/>&nbsp;<bean:write name="InvoiceJobDetailForm" property="appendixName" />
	   		<% }
			   if(type.equals("MSA")) {
	   		%>	
		   		<bean:message bundle = "IM" key = "im.jobsfor"/>&nbsp;<bean:message bundle = "IM" key = "im.msa"/>&nbsp;<bean:write name="InvoiceJobDetailForm" property="titlemsaname" />
		   	<% }
			   if(type.equals("Appendix")) {
	   		%>	
		   		<bean:message bundle = "IM" key = "im.jobsfor"/>&nbsp;<bean:message bundle = "IM" key = "im.appendix"/>&nbsp;<bean:write name="InvoiceJobDetailForm" property="titleappendixname" />&nbsp;->&nbsp;<bean:message bundle = "IM" key = "im.msa"/>&nbsp;<bean:write name="InvoiceJobDetailForm" property="titlemsaname" />
	   		<% } %>
	   		</td>
	  		</tr>
	   		</logic:notEqual>
	   	
	  	<logic:equal name="InvoiceJobDetailForm" property="invoice_Flag" value="false">
	  	<tr>
	  		<td class="labeleboldwhite" height="30"  colspan="21">&nbsp;<bean:message bundle = "IM" key = "im.searchJobs"/></td>
	   	</tr>
	   		</logic:equal>
			<logic:equal name="InvoiceJobDetailForm" property="invoice_Flag" value="false">
			<tr>	
				<td class="labeloboldwhite" colspan="21">
					&nbsp;&nbsp;<bean:message bundle = "IM" key = "im.partnerName"/>&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="partner_name"/>
					&nbsp;&nbsp;<bean:message bundle = "IM" key = "im.po/woNumber"/>&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="powo_number"/>
					&nbsp;&nbsp;<bean:message bundle = "IM" key = "im.exportstatus"/>&nbsp;&nbsp;
								<html:select property="exportStatus" size="1" styleClass="combowhite">
									<html:optionsCollection name = "dcExportStatusList"  property = "exportStatusList"  label = "label"  value = "value" />
								</html:select>
		            &nbsp;&nbsp;<bean:message bundle = "IM" key = "im.fromDate"/>&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="from_date" readonly = "true"/>&nbsp;<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].from_date, document.forms[0].from_date, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
		            &nbsp;&nbsp;<bean:message bundle = "IM" key = "im.toDate"/>&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="to_date" readonly = "true"/>&nbsp;<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].to_date, document.forms[0].to_date, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
		            &nbsp;&nbsp;<bean:message bundle = "IM" key = "im.invoiceNo"/>&nbsp;&nbsp;<html:text  styleClass="textbox" size="10" property ="invoiceno"/>
		        </td>
		      </tr>
		      <tr height="40">
		      	<td colspan="21">&nbsp;
		            <html:button property="search" styleClass="button" onclick = "return searchpartner();"><bean:message bundle = "IM" key = "im.search"/></html:button>
					<html:button property="cancel" styleClass="button" onclick = "return resetting();"><bean:message bundle = "IM" key = "im.cancel"/></html:button>
				</td>
			</tr>
			<TR height="1">
          	<TD height="1">&nbsp;</TD>
			</tr>
			</logic:equal>
				   		  	
			<logic:notEqual name="InvoiceJobDetailForm" property="invoice_Flag" value ="false" >	  	
		 	<tr height="25">
			    <td>&nbsp;</td>
				<td class="labelewhite" colspan = "17">
					<html:radio property="rdofilter" value="F" onclick = "return getFilteredList();"/><bean:message bundle = "IM" key = "im.completed"/>&nbsp;&nbsp;&nbsp;&nbsp;
					<html:radio property="rdofilter" value="T" onclick = "return getFilteredList();"/><bean:message bundle = "IM" key = "im.closednotreconcile"/>&nbsp;&nbsp;&nbsp;&nbsp;
					<html:radio property="rdofilter" value="O" onclick = "return getFilteredList();"/><bean:message bundle = "IM" key = "im.closed"/>&nbsp;&nbsp;&nbsp;&nbsp;
					<html:radio property="rdofilter" value="E" onclick = "return getFilteredList();"/><bean:message bundle = "IM" key = "im.exportedtogp"/>&nbsp;&nbsp;&nbsp;&nbsp;
					<html:radio property="rdofilter" value="A" onclick = "return getFilteredList();"/><bean:message bundle = "IM" key = "im.all"/>
				</td>
				<td colspan = "4"></td>
				</tr>
			
			<TR height="1">
          	<TD height="1">&nbsp;</TD>
			</tr>
			</logic:notEqual>		
			
			<logic:equal name="InvoiceJobDetailForm" property="invoice_Flag" value="false">
	        <html:hidden property="rdofilter"/>
	        </logic:equal>
	
			<logic:notEqual name="InvoiceJobDetailForm" property="invoice_Flag" value="false">
	        <html:hidden property="partner_name"/>
	        <html:hidden property="powo_number"/>
	        <html:hidden property="from_date"/>
	        <html:hidden property="to_date"/>
	        <html:hidden property="invoiceno"/>
	        </logic:notEqual>	                                    
	        
	    <logic:notEqual name="InvoiceJobDetailForm" property="toggle" value="true">
	    
		<tr> 
	    	<td rowspan = "2">&nbsp;</td>
	    	<td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.customername"/></td>
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.projectname"/></td>
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.jobname"/></td>
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.exportstatus"/></td>
		    <td class="tryb" rowspan="2"><bean:message bundle = "IM" key = "im.po"/></td>
		    <td class="tryb" rowspan="2"><bean:message bundle = "IM" key = "im.wo"/></td>
   		    <td class="tryb" rowspan="2"><bean:message bundle = "IM" key = "im.poAuthoirsedTotal"/></td>
		    <td class="tryb" rowspan="2"><bean:message bundle = "IM" key = "im.extendedprice"/></td>
		    <td class="tryb" rowspan="2"><bean:message bundle = "IM" key = "im.status"/></td>
			<td class="tryb" colspan="2"><bean:message bundle = "IM" key = "im.actualdate"/></td>
			<td class="tryb" rowspan="2"><bean:message bundle = "IM" key = "im.submitteddate"/></td>
			<td class="tryb" rowspan="2"><bean:message bundle = "IM" key = "im.invoiceno"/></td>
			<td class="tryb" rowspan="2"><bean:message bundle = "IM" key = "im.invoiceddate"/></td>
			<td class="tryb" rowspan="2" width="100"><bean:message bundle = "IM" key = "im.owner"/></td>
			<td class="tryb" rowspan="2" width="100"><bean:message bundle = "IM" key = "im.customerreference"/></td>
			<td class="tryb" rowspan="2" width="100"><bean:message bundle = "IM" key = "im.receivedDateAndTime"/></td>
			<td class="tryb" rowspan="2" width="100"><bean:message bundle = "IM" key = "im.problemSummary"/></td>
			<td class="tryb" rowspan="2" width="100"><bean:message bundle = "IM" key = "im.sitenumber"/></td>
			<td class="tryb" rowspan="2" width="100"><bean:message bundle = "IM" key = "im.installNotes"/></td>
			<td class="tryb" rowspan="2" width="100"><bean:message bundle = "IM" key = "im.pvsName"/></td>
			<td class="tryb" rowspan="2" width="100"><bean:message bundle = "IM" key = "im.closeddate"/></td>
			<td class="tryb" rowspan="2" width="100">GP Import Status</td>			
	  	</tr>
	  
	  <tr> 
		<td class="tryb"> 
	      <div align="center"><bean:message bundle = "IM" key = "im.start"/></div>
	    </td>
	    
		<td class="tryb"> 
	      <div align="center"><bean:message bundle = "IM" key = "im.complete"/></div>
	    </td>
	  </tr>
	  
   
   
 
   
      <% if(joblist_size > 0) { %>	
      
	<logic:present name="joblist">
	<%int i=1;
	String bgcolor="";
	String bgcolortext=""; 
	String hypertext=""; 
	String hypertextcenter=""; 
	%>
	
		<logic:iterate id="jlist" name="joblist" >
		<%if((i%2)==0)
		{
			bgcolor = "readonlytextnumbereven";
			bgcolortext = "readonlytexteven";
			hypertext = "hypereven"; 
			hypertextcenter = "hyperevencenter";
		
		} 
		else
		{
			bgcolor="readonlytextnumberodd";
			bgcolortext="readonlytextodd";
			hypertext = "hyperodd"; 
			hypertextcenter = "hyperoddcenter"; 
		}
		%>
		
		<bean:define id = "jid" name = "jlist" property = "jobId" />
		<bean:define id = "gpId" name = "jlist" property = "gpImportId" />
		<bean:define id="appendixId" name = "jlist" property = "appendixId"/>
		<bean:define id = "powo" name = "jlist" property = "powo" />
		<bean:define id = "job_status" name = "jlist" property = "job_type" />
		<bean:define id = "ticketId" name = "jlist" property = "ticket_id" type="java.lang.String"/>
		<html:hidden property="hid_jobid" name="jlist" />
	  <tr>		
			
		
		<td  class="labeleboldwhite" height="20">
        <% //if(job_status.equals("O")) { %>			
<%-- 		<!--  	<html:checkbox  property = "check" value = "<%= (String)jid %>" disabled = "true" /> --> --%>
		<%// } else { %>
   			<html:checkbox  property = "check" value = "<%= (String)jid %>" />	
   		<%// } %>	
		</td>
		
		
		<td  align=center class="<%=hypertext %>">
			<bean:write name="jlist" property="msaName"/>
		</td>

		<td  align=center class="<%=hypertext %>">
			&nbsp;<a href="AppendixHeader.do?function=view&appendixid=<bean:write name="jlist" property="appendixId"/>&inv_type=<bean:write name="InvoiceJobDetailForm" property="type" />&inv_id=<bean:write name="InvoiceJobDetailForm" property="id" />&inv_rdofilter=<bean:write name="InvoiceJobDetailForm" property="rdofilter" />&viewjobtype=<bean:write name="InvoiceJobDetailForm" property="viewjobtype"/>&ownerId=<bean:write name="InvoiceJobDetailForm" property="ownerId"/>&invoice_Flag=<bean:write name="InvoiceJobDetailForm" property="invoice_Flag"/>&invoiceno=<bean:write name="InvoiceJobDetailForm" property="invoiceno"/>&partner_name=<bean:write name="InvoiceJobDetailForm" property="partner_name"/>&powo_number=<bean:write name="InvoiceJobDetailForm" property="powo_number"/>&from_date=<bean:write name="InvoiceJobDetailForm" property="from_date"/>&to_date=<bean:write name="InvoiceJobDetailForm" property="to_date"/>"><bean:write name="jlist" property="appendixName"/></a>&nbsp;	
		</td>
		
		<td  align=center class="<%=hypertext %>">
			<a href="javascript:void(0)"  onClick = "if (window.event || document.layers) showWindow(event,<bean:write name="jlist" property="jobId"/>); else showWindow('',<bean:write name="jlist" property="jobId"/>);"><img name = "icon"  src = "images/icon.gif" width="13" height="13" style="vertical-align: middle;"  border = "0" /></a>
			&nbsp;<a href="JobDashboardAction.do?jobid=<bean:write name="jlist" property="jobId"/>&inv_type=<bean:write name="InvoiceJobDetailForm" property="type" />&inv_id=<bean:write name="InvoiceJobDetailForm" property="id" />&inv_rdofilter=<bean:write name="InvoiceJobDetailForm" property="rdofilter" />&viewjobtype=<bean:write name="InvoiceJobDetailForm" property="viewjobtype"/>&ownerId=<bean:write name="InvoiceJobDetailForm" property="ownerId"/>&invoice_Flag=<bean:write name="InvoiceJobDetailForm" property="invoice_Flag"/>&invoiceno=<bean:write name="InvoiceJobDetailForm" property="invoiceno"/>&partner_name=<bean:write name="InvoiceJobDetailForm" property="partner_name"/>&powo_number=<bean:write name="InvoiceJobDetailForm" property="powo_number"/>&from_date=<bean:write name="InvoiceJobDetailForm" property="from_date"/>&to_date=<bean:write name="InvoiceJobDetailForm" property="to_date"/>"><bean:write name="jlist" property="jobName"/></a>
			<logic:equal name="jlist" property="out_of_scope_activity_flag" value = "Y">
				&nbsp;&nbsp;<img name = "outscopeactivityflag"  src = "images/oos1.gif" width = "14" height = "11" border = "0" alt = '<bean:message bundle = "IM" key = "im.outscopeflagpresent"/>'/>
			</logic:equal>
			
			<logic:notEqual name="jlist" property="out_of_scope_activity_flag" value = "Y">
			</logic:notEqual>

		</td>
		
		<td  align=center class="<%=hypertextcenter %>">
			<bean:write name="jlist" property="exportStatus"/>
		</td>
		
		<td  align=center class="<%=hypertextcenter %>">
			<%=powo %>
		</td>
		
		<td  align=center class="<%=hypertextcenter %>">
			<%=powo %>
		</td>
		
		<td  align=center class="<%=bgcolor %>">
			<bean:write name="jlist" property="poAuthorizedTotal"/>
		</td>
		
		<td  align=center class="<%=bgcolor %>">
			<bean:write name="jlist" property="extendedprice"/>
		</td>
		
		<td  align=center class="<%=bgcolortext %>">
			<bean:write name="jlist" property="status"/>
		</td>
		
		
		<td  align=center class="<%=bgcolortext %>">
			<bean:write name="jlist" property="actualstartdate"/>
		</td>
		
		<td  align=center class="<%=bgcolortext %>">
			<bean:write name="jlist" property="actualcompletedate"/>
		</td>
		
		<td  align=center class="<%=bgcolortext %>">
			<bean:write name="jlist" property="submitteddate"/>
		</td>
		
		<td  align=center class="<%=bgcolortext %>">
			<html:text name="jlist" property="invoice_no" size = "10" styleClass = "textbox"></html:text>
		</td>
		
		<td  align=center class="<%=bgcolortext %>">
			<html:text name="jlist" property="associated_date" size = "10" styleClass = "textbox" readonly="true" onclick = "javascript:return popUpCalendar(this,this,'mm/dd/yyyy');"></html:text>
		</td>
		
		<td  align=center class="<%=hypertext %>">
			<bean:write name="jlist" property="createdby"/>
		</td>
		
		
		
		<td  align=center class="<%=hypertext %>">
			<!--<html:text name="jlist" property="customerref" size = "10" styleClass = "textbox" readonly="true"></html:text>-->
			<bean:write name="jlist" property="customerref" />
			<html:hidden name="jlist" property="customerref"/></td>
			
		<!-- Start :Added By Amit For Received Date & Time,Site No.-->
		<td  align=center class="<%=hypertext %>">
			<bean:write name="jlist" property="request_received_date"/>
			<bean:write name="jlist" property="request_received_time"/>
		</td>
			
				<logic:equal name="jlist" property="ticket_id" value="0">
 				<td class="<%=hypertextcenter%>"></td>
				</logic:equal>
		
 		<logic:notEqual name="jlist" property="ticket_id" value="0">
		<%if(!ticketId.equals("")) { %>	
		 <td class="<%=hypertextcenter%>"><a href="JobDashboardAction.do?function=view&ticket_id=<bean:write name="jlist" property="ticket_id"/>&jobid=<bean:write name="jlist" property="jobId"/>&appendixid=<bean:write name="jlist" property="appendixId"/>&ticket_type=existingticket&fromAccountingDB=accountingDB&viewjobtype=<bean:write name = "InvoiceJobDetailForm" property = "viewjobtype" />&ownerId=<bean:write name = "InvoiceJobDetailForm" property = "ownerId" />&invoice_Flag=<bean:write name="InvoiceJobDetailForm" property="invoice_Flag"/>&invoiceno=<bean:write name="InvoiceJobDetailForm" property="invoiceno"/>&partner_name=<bean:write name="InvoiceJobDetailForm" property="partner_name"/>&powo_number=<bean:write name="InvoiceJobDetailForm" property="powo_number"/>&from_date=<bean:write name="InvoiceJobDetailForm" property="from_date"/>&to_date=<bean:write name="InvoiceJobDetailForm" property="to_date"/>">[<bean:message bundle="IM" key="im.problemSummaryView"/>]</a></td>		
		<% }else{ %>
 		<td class="<%=hypertext%>"><bean:message bundle="IM" key="im.probelmSummaryResult"/></td> 
		<%} %>
		</logic:notEqual>
		 
		 <td  align=center class="<%=hypertext %>">
			<bean:write name="jlist" property="siteNo"/>
		</td>
		<td class="<%=hypertextcenter%>" vAlign=top>
		<a  href="ViewInstallationNotes.do?function=view&from=accountingDB&appendixid=<bean:write name="jlist" property="appendixId"/>&fromAccountingDB=accountingDB&jobid=<bean:write name="jlist" property="jobId"/>&viewjobtype=<bean:write name="InvoiceJobDetailForm" property="viewjobtype" />&ownerId=<bean:write name="InvoiceJobDetailForm" property="ownerId" />" >[View]</a> 
		</td>
		<td  align=center class="<%=hypertext %>">
			<bean:write name="jlist" property="partner_name"/>
		</td>
		
		<td  align=center class="<%=bgcolortext %>">
			<bean:write name="jlist" property="closed_date"/>
		</td>
		
		
		<td  align=center class="<%=bgcolortext %>">
			<logic:notEqual name="jlist" property="gpImportId" value="0">
				<logic:notEqual name="jlist" property="gpImportStatus" value="3">
					<html:checkbox  property = "gpImportId" styleId='<%="gpImportIdss"+i %>' onclick='<%="updateGPStatus("+gpId+", "+i+");" %>' />
				</logic:notEqual>
				<logic:equal name="jlist" property="gpImportStatus" value="3">
					<html:checkbox  property = "gpImportId" styleId='<%="gpImportIdss"+i %>' value="" disabled="true" />
				</logic:equal>
			</logic:notEqual>
		</td>
	  </tr>
	  <%i++; %>
		</logic:iterate>
	
	  <tr class = "buttonrow">
	  	<td class="labeleboldwhite" colspan="1">&nbsp;</td> 
	    <td colspan="23" height="20"> 
<!--  <html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "IM" key = "im.sort"/></html:button>  -->		  
<!--  <html:submit property = "view" styleClass = "button" onclick = "return validate('viewIR');"><bean:message bundle = "IM" key = "im.view"/></html:submit>  -->
		
		<html:submit property = "view" styleClass = "button" onclick = "return validate('markClosed');"><bean:message bundle = "IM" key = "im.markclosed"/></html:submit>
		
		<html:button property = "sort" styleId = "sort" styleClass = "button" onclick = "javascript:return sortwindow();"><bean:message bundle = "PRM" key = "prm.sort"/></html:button>
		<logic:equal name = "InvoiceJobDetailForm" property = "nettype" value = "dispatch">
			<html:button property="back" styleClass="button" onclick = "return Backaction();">
				<bean:message bundle="PRM" key="prm.button.back"/>
			</html:button>
		</logic:equal>
	    </td>
	  </tr> 
	  
	  </logic:present>
	<% } else { %>
		<tr>
			<td  colspan = "14" class = "message" height = "30">
			  Jobs not defined
			</td>
		</tr>
	<% } %>
	    
	    </logic:notEqual>
	    <tr>
			<td>
				<div id="ajaxdiv"  style=" position:absolute;"></div>
			</td>
		</tr> 
   </table>
  </td>
 </tr>
</table>
	 <% if(joblist_size > 0) { %>	
		<div class="pcstdtext" style="margin-left: 30px;margin-top: 10px;">
			<span style="float: left;"> Mark Closed With Invoice # <html:text name="InvoiceJobDetailForm" property="bulkInvoiceNo" size = "10" styleClass = "textbox"></html:text></span>
			<span style="float: left;padding-left: 10px;"> and Date: 
					<html:text name="InvoiceJobDetailForm" property="bulkInvoiceDate" size = "10" styleClass = "textbox" readonly="true" onclick = "javascript:return popUpCalendar(this,this,'mm/dd/yyyy');"></html:text>
					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].bulkInvoiceDate, document.forms[0].bulkInvoiceDate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
			 </span>
			<span style="padding-left: 10px;"><html:submit property = "view" styleClass = "button" onclick = "return validateBulkClosed('bulkClosed');">Mark Selected Jobs Closed</html:submit></span>
		</div>
<% } %>
</body>
</html:form>
<script>



function updateGPStatus(gpId, idAtt){

	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    //document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
	    }
	  }
	xmlhttp.open("GET","InvoiceJobDetail.do?getGPImportId="+gpId,true);
	xmlhttp.send();
	var val = "gpImportIdss"+idAtt;
	document.getElementById(val).disabled = "true";
	
	
	
}
var str = '';


function load() {
	document.forms[0].bulkInvoiceDate.value="";
	document.forms[0].bulkInvoiceNo.value="";
}


function validate(type)
{
document.forms[0].function_type.value = type;
var submitflag = 'false';
	if(<%= joblist_size %> != 0)
	{
		if(<%= joblist_size %> == 1) {
			if(document.forms[0].check.checked)	{
				if( document.forms[0].invoice_no.value == "" )
			 		{
			 			alert( "<bean:message bundle = "IM" key = "im.enterinvoiceno"/>" );
			 			document.forms[0].invoice_no.focus();
			 			type = 'invoice_no';
						return false;
			 		}
			 	if( document.forms[0].associated_date.value == "" )
			 		{
			 			alert( "<bean:message bundle = "IM" key = "im.enterinvoicedate"/>" );
			 			document.forms[0].associated_date.focus();
			 			type = 'invoice_date';
						return false;
			 		}
			 	
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++) {
		  		if((document.forms[0].check[i].checked)) {
		  			if( document.forms[0].invoice_no[i].value == "" )
			 		{
			 			alert( "<bean:message bundle = "IM" key = "im.enterinvoiceno"/>" );
			 			document.forms[0].invoice_no[i].focus();
			 			type = 'invoice_no';
						return false;
			 		}
			 		if( document.forms[0].associated_date[i].value == "" )
			 		{
			 			alert( "<bean:message bundle = "IM" key = "im.enterinvoicedate"/>" );
			 			document.forms[0].associated_date[i].focus();
			 			type = 'invoice_date';
						return false;
			 		}
			 	
		  			submitflag = 'true';
		  		}
		  	}
		}
		
		if(submitflag == 'false') {
		  		if(type == 'viewIR') alert( "<bean:message bundle = "IM" key = "im.selectajobview"/>" );
		  		if(type == 'markClosed') alert( "<bean:message bundle = "IM" key = "im.selectajobmark"/>" );
		  		if(type == 'Invoice Request') alert( "<bean:message bundle = "IM" key = "im.selectajobsend"/>" );
				return false;
		  	}
	}
	if(<%= joblist_size %> == 0)
	{
		return false;
	}

if('<%= module%>' != 'PRM' && '<%= module%>' != 'NM') {
	parent.ilexleft.location.reload();
}

document.forms[0].submit(); 
return true;
}

function validateBulkClosed(type){
	document.forms[0].function_type.value = type;

	if(document.forms[0].bulkInvoiceNo.value == ""){
		alert("Please Enter Bulk Invoice #");
		return false;
	}
	if(document.forms[0].bulkInvoiceDate.value == ""){
		alert("Please Enter Bulk Invoice Date");
		return false;
	}
	
	var submitflag = 'false';
	
	if(<%= joblist_size %> == 1) {
		if(document.forms[0].check.checked)	{
			submitflag = 'true';
		}
	}else{
		for( var i = 0; i<document.forms[0].check.length; i++) {
			if((document.forms[0].check[i].checked)) {
								submitflag = 'true';
			}
		}
	}
	
		
	
	if(submitflag == 'false') {
		alert("Please Select At Least One Job");
		return false;
	}
	

		
	
	if(<%= joblist_size %> == 0)
	{
		return false;
	}
	document.forms[0].submit(); 
	return true;
}


function getFilteredList()
{
	resetting();
	document.forms[0].function_type.value = "filteredJob";
	document.forms[0].invoice_Flag.value = "True";
	document.forms[0].submit();
	return true;
}

function resetting()
{
	
document.forms[0].partner_name.value="";
document.forms[0].powo_number.value="";
document.forms[0].from_date.value="";
document.forms[0].to_date.value="";
document.forms[0].invoiceno.value="";
}

function searchpartner()
{
	if(document.forms[0].partner_name.value=="" && document.forms[0].powo_number.value=="" && document.forms[0].from_date.value=="" && document.forms[0].to_date.value=="" && document.forms[0].invoiceno.value=="" && document.forms[0].exportStatus.value=="")
	{
	alert("To search Jobs, at least one parameter is required !");
	return false;
	}
	
	if(Date.parse(document.forms[0].from_date.value)>Date.parse(document.forms[0].to_date.value))
	{
	alert("\"From Date\" should be less than \"To Date\"");
	return false;
	}
	
	document.forms[0].function_type.value = "filteredJob";
	document.forms[0].invoice_Flag.value = "false";
	document.forms[0].toggle.value="false";
	document.forms[0].submit();
	return true;
}

function getchangeFlag()
{
	document.forms[0].function_type.value = "filteredJob";
	//document.forms[0].flag.value = "True";
	document.forms[0].submit();
	return true;
}

function Backaction()
{
	document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype=dispatch&viewjobtype=<bean:write name = "InvoiceJobDetailForm" property = "viewjobtype" />&ownerId=<bean:write name = "InvoiceJobDetailForm" property = "ownerId" />";
	document.forms[0].submit();
	return true;
}

//Start:Added By Amit For sort
function sortwindow()
{

	<% if( type.equals("MSA"))
		type = "Inv_MSA";
		if( type.equals("Appendix"))
		type = "Inv_Appendix";
	%>
	str = 'SortAction.do?appendixid=<bean:write name="InvoiceJobDetailForm" property="id"/>&Type=<%= type%>&rdofilter=<bean:write name="InvoiceJobDetailForm" property="rdofilter"/>&invoice_Flag=<bean:write name="InvoiceJobDetailForm" property="invoice_Flag"/>&partner_name=<bean:write name="InvoiceJobDetailForm" property="partner_name"/>&partner_name=<bean:write name="InvoiceJobDetailForm" property="partner_name"/>&powo_number=<bean:write name="InvoiceJobDetailForm" property="powo_number"/>&from_date=<bean:write name="InvoiceJobDetailForm" property="from_date"/>&to_date=<bean:write name="InvoiceJobDetailForm" property="to_date"/>&invoiceno=<bean:write name="InvoiceJobDetailForm" property="invoiceno"/>&viewjobtype=<bean:write name="InvoiceJobDetailForm" property="viewjobtype"/>&ownerId=<bean:write name="InvoiceJobDetailForm" property="ownerId"/>';
	//p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	return true;
}
//
</script>

</html>

