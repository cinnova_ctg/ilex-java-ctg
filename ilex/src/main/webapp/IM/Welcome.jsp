<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Displaying menutree in the left and homepage(blank) in the right.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>

<title><bean:message bundle = "IM" key = "im.ilex"/></title>
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
</head>

<%
session.setAttribute("tRModule", "IM");
%>

<frameset framespacing="0" border="false" frameborder="0" cols="260,*">
  <frame name="ilexleft" scrolling="yes" src="../AccountingMenu.do"  marginwidth="0" marginheight="0" STYLE="border-right: 1px outset #1D3F71">
<frameset framespacing="0" border="false" frameborder="0" rows="*,20">
	<frame name="ilexmain" id="ilexmain" scrolling="auto"  src="Home.jsp"	 marginwidth="0" marginheight="0">
	<frame name="ilexfooter" scrolling="no"  src="../Footerframe.jsp"
	 marginwidth="0" marginheight="0">

</frameset>
    <noframes>
  <body>
  <b><bean:message bundle="RM" key="rm.nosupport"/></b>
  </body>
  </noframes>
</frameset>
</html:html>




