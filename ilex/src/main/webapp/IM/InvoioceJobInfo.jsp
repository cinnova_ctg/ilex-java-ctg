<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<html>
    <head></head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <a onClick="document.all.ajaxdiv.innerHTML = '';" href="#">
            <img src="images/box_close.gif" alt="" border="0" class="ACTBTN0001">
        </a>
        <table width="100%" border="0" cellspacing="0" cellpadding="1" class="ACTTBL0001">
            <tr>
                <td class="insidebold" colspan="5" nowrap="nowrap">Site Details</td>
            </tr>
            <tr>
                <td  class="inside" colspan="5" nowrap="nowrap">
                    <c:if test="${InvoiceJobDetailForm.siteName ne ''}">
                        <c:out value="${InvoiceJobDetailForm.siteName}"/>/
                    </c:if><c:out value="${InvoiceJobDetailForm.siteNumber}"/>
                </td>
            </tr>
            <tr>
                <td  class="inside" colspan="5" nowrap="nowrap"><c:out value="${InvoiceJobDetailForm.siteAddress}"/></td>
            </tr>
            <tr>
                <td  class="inside" colspan="5" nowrap="nowrap">
                    <c:if test="${InvoiceJobDetailForm.siteState ne '' && InvoiceJobDetailForm.siteCity ne '' }">
                        <c:out value="${InvoiceJobDetailForm.siteCity}"/>,
                    </c:if>
                    <c:if test="${InvoiceJobDetailForm.siteState eq ''}">
                        <c:out value="${InvoiceJobDetailForm.siteCity}"/>
                    </c:if>
                    <c:out value="${InvoiceJobDetailForm.siteState}"/>
                    <c:if test="${InvoiceJobDetailForm.siteCountry ne 'US'}">
                        <c:out value="${InvoiceJobDetailForm.siteCountry}"/>
                    </c:if>
                    <c:out value="${InvoiceJobDetailForm.siteZipCode}"/>
                </td>
            </tr>
            <tr>
                <td class = "inside" colspan = "5" height="4" nowrap="nowrap"></td>
            </tr>
            <tr>
                <td class = "insidebold" colspan = "5">Customer Reference:&nbsp;<font class="inside"><c:out value="${InvoiceJobDetailForm.custref}"/></font></td>
            </tr>
            <tr>
                <td class = "inside" colspan = "5" height="4" nowrap="nowrap"></td>
            </tr>
            <c:if test="${InvoiceJobDetailForm.typeJob eq 'ticket'}">
                <tr>
                    <td class = "insidebold"  style="white-space: normal;" colspan = "5" nowrap="nowrap">Billing Information:&nbsp;<font class="inside"><c:out value="${InvoiceJobDetailForm.billingInfo}"/></font></td>
                </tr>
                <tr>
                    <td class = "inside" colspan = "5" height="4" nowrap="nowrap"></td>
                </tr>
                <tr>
                    <td class = "insidebold" colspan = "5" nowrap="nowrap">Request Type:&nbsp;<font class="inside"><c:out value="${InvoiceJobDetailForm.requestType}"/></font></td>
                </tr>
                <tr>
                    <td class = "inside" colspan = "5" height="4" nowrap="nowrap"></td>
                </tr>
                <tr>
                    <td class="insidebold" colspan="5" nowrap="nowrap">Resources</td>
                </tr>
            </c:if>
            <c:if test="${InvoiceJobDetailForm.typeJob eq 'job'}">
                <tr>
                    <td class="insidebold" colspan="5" nowrap="nowrap">Activities</td>
                </tr>
            </c:if>
            <tr>
                <td colspan = "5">
                    <table border = 0>
                        <tr>
                            <c:if test="${InvoiceJobDetailForm.typeJob eq 'ticket'}">
                                <td class="inside" colspan="1" align="center" nowrap="nowrap">Name</td>
                                <td class="inside" colspan="1" align="center" nowrap="nowrap">CNS Part Number&nbsp;</td>
                            </c:if>
                            <c:if test="${InvoiceJobDetailForm.typeJob ne 'ticket'}">
                                <td class="inside" colspan="2" align="center" nowrap="nowrap">Name</td>
                            </c:if>
                            <td class="inside" colspan="1" align="center" nowrap="nowrap">Type</td>
                            <td class="inside" colspan="1" align="center" nowrap="nowrap">Qty</td>
                            <c:if test="${InvoiceJobDetailForm.typeJob ne 'ticket'}">
                                <td class="inside" colspan="1" align="center" nowrap="nowrap">Invoice Price</td>
                            </c:if>
                            <c:if test="${InvoiceJobDetailForm.typeJob eq 'ticket'}">
                                <td class="inside" colspan="1" align="center" nowrap="nowrap">Price</td>
                            </c:if>
                        </tr>
                        <c:forEach items="${requestScope.resList}" var="resList" varStatus="index">
                            <tr>
                            <c:if test="${InvoiceJobDetailForm.typeJob ne 'ticket'}">
                                <td class="inside" nowrap="nowrap" colspan="2"><c:out value='${resList.rsName}'/>&nbsp;</td>
                            </c:if>
                            <c:if test="${InvoiceJobDetailForm.typeJob eq 'ticket'}">
                                <td class="inside" nowrap="nowrap"><c:out value='${resList.rsName}'/>&nbsp;</td>
                                <td class="inside" colspan="1" align="center" nowrap="nowrap"><c:out value='${resList.cnsPartNo}'/></td>
                            </c:if>
                            <td class="inside" align="center" nowrap="nowrap"><c:out value='${resList.rsType}'/>&nbsp;</td>
                            <td class="inside" align="center" nowrap="nowrap"><fmt:formatNumber value="${resList.rsquantity}" type="currency" pattern="#####0.00"/>&nbsp;</td>
                            <td class="inside" align="right" nowrap="nowrap"><fmt:formatNumber value="${resList.rsPrice}" type="currency" pattern="$#####0.00"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
            <tr>
                <td class = "inside" colspan = "5" height="4" nowrap="nowrap"></td>
            </tr>
            <tr>
                <td class="insidebold" colspan="5" nowrap="nowrap">Dates</td>
            </tr>
            <tr>
                <td colspan=5 nowrap="nowrap">
                    <table border=0 cellspacing="5">
                        <tr>
                            <c:if test="${InvoiceJobDetailForm.typeJob ne 'ticket'}">
                                <td class="insidetitle" colspan="1"  nowrap="nowrap" align="center">Visit</td>
                            </c:if>
                            <c:if test="${InvoiceJobDetailForm.typeJob eq 'ticket'}">
                                <td class="insidetitle" colspan="1"  nowrap="nowrap" align="center">Dispatch</td>
                            </c:if>
                            <td class="insidetitle" colspan="1"  align="center" nowrap="nowrap">Start</td>
                            <td class="insidetitle" colspan="1"  align="center" nowrap="nowrap">End</td>
                            <td class="insidetitle" colspan="1"  nowrap="nowrap">Time Onsite</td>
                        </tr>
                        <c:forEach items="${requestScope.scheduleList}" var="scheduleList" varStatus="index">
                            <tr>
                                <td class="inside"  nowrap="nowrap" align="center"><c:out value='${scheduleList.sequenceNo}'/></td>
                                <td class="inside"  nowrap="nowrap" align="center"><c:out value='${scheduleList.startDate}'/></td>
                                <td class="inside"  nowrap="nowrap" align="center"><c:out value='${scheduleList.startEnd}'/></td>
                                <td class="inside"  nowrap="nowrap" align ="center"><c:out value='${scheduleList.onSite}'/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
            <tr>
                <td class = "inside" colspan = "5" height="4" nowrap="nowrap"></td>
            </tr>
            <tr>
                <td class="insidebold" colspan="5" nowrap="nowrap">Total Time Onsite:&nbsp;<font class="inside"><c:out value="${InvoiceJobDetailForm.timeOnSite}"/></font></td>
            </tr>
            <tr>
                <td class = "inside" colspan = "5" height="4" nowrap="nowrap"></td>
            </tr>
            <c:if test="${InvoiceJobDetailForm.typeJob eq 'ticket'}">
                <tr>
                    <td class="insidebold" colspan="5" nowrap="nowrap">Requestor:&nbsp;<font class="inside"><c:out value="${InvoiceJobDetailForm.requestorName}"/></font></td>
                </tr>
                <tr>
                    <td class = "inside" colspan = "5" height="4"></td>
                </tr>
                <tr>
                    <td class="insidebold" colspan="5">Problem Category:&nbsp;<font class= "inside"><c:out value="${InvoiceJobDetailForm.problemdesc}"/></font></td>
                </tr>
            </c:if>
        </table>
    </body>
</html>
