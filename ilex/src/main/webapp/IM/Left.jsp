<!DOCTYPE HTML>
<%@ page import = "com.mind.common.*" %>
<%@ page import= "com.mind.common.dao.*"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>


<META name="GENERATOR" content="IBM WebSphere Studio">
<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/treeviewscript/jquery-treeview-main-all.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/JLibrary.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/treeviewscript/jquery-treeview-main-all.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/jquery-tab.min.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/jquery.corner.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/jMenu.js?timestamp="+(new Date()*1)></script>
<link href="styles/jquery-tab.css" rel="stylesheet" type="text/css" />
<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
	
<style type="text/css">
form{margin:0px; padding:0px;}
</style>
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">



<TITLE></TITLE>
<script>
function btmSearch()
{ 
	  var url="/Ilex-WS/masterbtm/searchclientlist.html?pageIndex=1&userid=${sessionScope.userid}";
	  alert("URL="+url);
      document.forms[0].action= url;
      if (document.forms[0].clientName.value=='Enter Client Name'){
    	  document.forms[0].clientName.value = '';
    	}
      document.forms[0].submit();
	}
function invSetupSeach(){
	 var url="/Ilex-WS/invoiceSetup/searchclientlist.html?pageIndex=1&userid=${sessionScope.userid}";
	 alert("URL="+url);
     document.forms[0].action= url;
     if (document.forms[0].client.value=='Enter Client Name'){
   	  document.forms[0].client.value = '';
   	}
     document.forms[0].submit();
}

</script>
</HEAD>
<script>	
$(document).ready(function(){
	$("#tabs").tabs();
	$("#tab-1").corner("top");
	$("#tab-2").corner("top");
	$("#tab-3").corner("top");
	//$("#fragment-1").load("AccountingMenu.jsp");
	$("#fragment-1").load("MenuTreeAction.do?menucriteria=PRCM", function(){
		document.getElementById('line0').className = 'm2';
		document.getElementById('line0').style.visibility = 'visible';
			initNavigatorServer('IM');
	});


	
});	
</script>

<title><bean:message bundle="RM" key="rm.hierarchynavigation"/></title>
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 10px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 0px;
    	white-space : nowrap;
    }

    /* Link Styles */
    a  {
    	color : #000000;
    }

    a:link  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:active  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:hover  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:visited  {
    	text-decoration : underline;
    	color : #000000;
    }
    
    .m2 {
        font-family : "Verdana";
        font-size : 10px;
        line-height: 10px;
    }    
    
.jQ-menu ul {
	list-style-type: none;
}

.jQ-menu li {
	text-decoration: none;
}

</style>

<script language="JavaScript" src="javascript/ce_menu.js" type="text/javascript"></script>

<body  style=" background-color: #f4f9fd;">
	<div id="tabs" style=" background-color: #f4f9fd;">
	    <ul style=" background-color: #f4f9fd;">
	    	 <li id="tab-1"><a href="#fragment-1"><span>Invoices</span></a></li>
	        <li id="tab-2" ><a href="#"><span>Clients</span></a></li>
	         <li id="tab-3" ><a href="#fragment-3"><span>Administration</span></a></li>
	       
	    </ul>
	     <div id="fragment-1" style=" background-color: #f4f9fd; border: 0px;">
	     </div>
	     
		  
		  <div id="fragment-3" style=" background-color: #f4f9fd; border: 0px;">
	      <form method="post" name="btmSearchClientDetail" target="ilexmain">
	      <div class="jQ-menu" style="text-align: left;margin: 0px;padding: 0px;"> 
	      <ul style="text-align: left;padding: 0px;margin: 0px;" >
			<li>
				<span class="toggle"><font style="color:#20477d;font-size:11px; font-weight: bold; vertical-align: top;">Bill to Masters</font></span>
				<ul style=" background-color: #f4f9fd;margin-left:8px;">
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink" href="/Ilex-WS/masterbtm/selectclient.html?userid=${sessionScope.userid}" target="ilexmain">Create New Bill To Master</a></font></span></li>
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;">Search:</font></span>
					<div style="padding-top: 5px;"><input type="text" name="clientName" id="clientName" size=30 onblur="javascript: if(this.value=='') this.value='Enter Client Name';" value="Enter Client Name" onfocus="javascript: if(this.value=='Enter Client Name') this.value='';" value="Enter Client Name"/>
					</div>
					<div style="padding-top: 5px;font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 9px;color: #939392">Search to update existing Bill To<br/> Master.</div>
					<div align="left" style="padding-left: 115px;"><input type="button" value="Search"  class="FMBUT0002" onclick="btmSearch();"/></div></li>
				</ul>
			</li>
			
		</ul>
		<div style="height: 20px;">&nbsp;</div>
		  <ul style="text-align: left;padding: 0px;margin: 0px;" >
			<li>
			<span class="toggle"><font style="color:#20477d;font-size:11px; font-weight: bold; vertical-align: top;">Appendix Invoice Setup</font></span>
				<ul style=" background-color: #f4f9fd;margin-left:8px;">
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink" href="/Ilex-WS/invoiceSetup/client.html?userid=${sessionScope.userid}" target="ilexmain">New Appendix Invoice Setup</a></font></span></li>
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;">Search:</span>
					<div style="padding-top: 5px;"><input type="text" name="client" size=30  onblur="javascript: if(this.value=='') this.value='Enter Client Name';" value="Enter Client Name" onfocus="javascript: if(this.value=='Enter Client Name') this.value='';" value="Enter Client Name"/></div>
						<div align="left" style="padding-left: 115px;padding-top:10px;"><input type="button" value="Search"  class="FMBUT0002" onclick="invSetupSeach();"/></div></li>
				</ul>
			</li>
			
		</ul>
		<div style="height: 20px;">&nbsp;</div>
		  <ul style="text-align: left;padding: 0px;margin: 0px;" >
			<li>
			<span class="toggle"><font style="color:#20477d;font-size:11px; font-weight: bold; vertical-align: top;">Exceptions</font></span>
				<ul style=" background-color: #f4f9fd;margin-left:8px;">
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink"  href="#">Invoice</a></font></span></li>
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink"  href="#">Integration</a></font></span></li>
				</ul>
			</li>
			
		</ul>
		<div style="height: 20px;">&nbsp;</div>
		  <ul style="text-align: left;padding: 0px;margin: 0px;" >
			<li>
			<span class="toggle"><font style="color:#20477d;font-size:11px; font-weight: bold; vertical-align: top;">Out of Bounds</font></span>
				<ul style=" background-color: #f4f9fd;margin-left:8px;">
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink"  href="#">Operations</a></font></span></li>
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink"  href="#">Accounting</a></font></span></li>
				</ul>
			</li>
			
		</ul>
		<div style="height: 20px;">&nbsp;</div>
		  <ul style="text-align: left;padding: 0px;margin: 0px;" >
			<li>
			<span class="toggle"><font style="color:#20477d;font-size:11px; font-weight: bold; vertical-align: top;">Reporting</font></span>
				<ul style=" background-color: #f4f9fd;margin-left:8px;">
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink"  href="#">Aging</a></font></span></li>
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink"  href="#">Accrual</a></font></span></li>
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink"  href="#">Account Summary</a></font></span></li>
					<li style="padding-top: 5px;"><span style="font-family: 'Verdana', Arial, Helvetica, 'sans-serif';font-size: 11px;"><font style="vertical-align:top;"><a class="mbtAdminLink"  href="#">Ad  Hoc Report</a></font></span></li>
				</ul>
			</li>
			
		</ul>
		   </div>
		   <div style="height: 20px;">&nbsp;</div>
		    <div><a href='InvoiceJobDetail.do?type=All&id=0&rdofilter=A&invoice_Flag=false&toggle=true' target='ilexmain' target="ilexmain"><font style="color:#20477d;font-size:11px; font-weight: bold; vertical-align: top;">Invoice Search</font></a></div>
	      </div>
			</form>
          </div>
          
	      
    </div>
   


</body>

</html:html>


