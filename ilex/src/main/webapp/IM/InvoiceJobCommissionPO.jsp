<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<LINK href="styles/content.css" rel="stylesheet" type="text/css">
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/popcalendar.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js?timestamp="+(new Date()*1)></script>
<script language="JavaScript" src="javascript/prototype.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/UtilityFunction.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/InvoiceCommission.js?timestamp="+(new Date()*1)></script>

<title>Invoice Commission PO for Job</title>
</head>

<body>
	<html:form action="InvoiceJobCommissionPO">
		<html:hidden property="actualInvoiceNo"/>
		
	   	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
	       	<tr> 
	         		<td class="toprow1"></td>
	       	</tr>
	     </table>
	
		<table border="0" cellspacing="0" cellpadding="0">
			<tr height="10"><td>&nbsp;</td></tr>
			<tr>
				<td width="10" height="0">&nbsp;</td>
				<td>
					<c:if test="${not empty requestScope.invoiceFlagSuccess}">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<c:choose>
									<c:when test="${requestScope.invoiceFlagSuccess ne '0'}">
										<td class = "message">Error occurred while completing Commission PO(s).</td>
									</c:when>
									<c:otherwise>
										<td class = "message">Commission PO(s) completed successfully.</td>
									</c:otherwise>
								</c:choose>
							</tr>
							<tr height="10"><td></td></tr>
						</table>
					</c:if>
					
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="labeleboldwhite"> Commission PO Completion</td>
						</tr>
						<tr height="10"><td></td></tr>
					</table>
					
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="labeloboldwhite">Invoice:&nbsp;&nbsp;</td>
							<td class="labeloboldwhite">
								<html:text property="invoiceNo" styleClass="textbox" size="10" />
							</td>
							<td class="labeloboldwhite">&nbsp;&nbsp;&nbsp;</td>
							<td class="labeloboldwhite">Date:&nbsp;&nbsp;</td>
							<td class="labeloboldwhite">
								<html:text property="paymentReceiveDate" styleClass="textbox" size="10" readonly="true" />
								&nbsp;<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].paymentReceiveDate, document.forms[0].paymentReceiveDate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							</td>
							<td class="labeloboldwhite">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td class="labeloboldwhite">
								<input type="submit" class="button_c" name="go" value="Go" />
							</td>
						</tr>
					</table>
					<c:choose>
						<c:when test="${not empty requestScope.closedJobList}">
							<table border="0" cellspacing="1" cellpadding="1" id="tableMain">
								<tr height="20"><td>&nbsp;</td></tr>
								<tr>
									<td class="tryb" valign="middle">Customer</td>
									<td class="tryb" valign="middle">Appendix Title</td>
									<td class="tryb" valign="middle">Invoice Receipt Date</td>
									<td class="tryb" valign="middle">Job Title</td>
									<td class="tryb" valign="middle">Closed Date</td>
									<td class="tryb" valign="middle">Revenue</td>
								</tr>
								<tbody>
								<c:if test="${requestScope.closedJobListSize > 0}">
									<c:forEach var="list" items="${requestScope.closedJobList}" varStatus="count">
											<tr id="closedJobList">
											 	<td class='hyperwhite' align="left"><c:out value="${list.msaName}" /></td>
												<td class='hyperwhite' align="left"><c:out value="${list.appendixName}" /></td>
												<td class='hyperwhite' align="center"><c:out value="${list.associatedDate}" /></td>
												<td class='hyperwhite' align="left"><c:out value="${list.jobTitle}" /></td>
												<td class='hyperwhite' align="center"><c:out value="${list.closedDate}" /></td>
												<td class='hyperwhite' align="right">
													<fmt:formatNumber value='${list.revenue}' type='currency' pattern='#,###,##0.00'/>
												</td>
												<input type="hidden" name="jobId" value="<c:out value='${list.jobId}'/>" />  
											</tr>
									</c:forEach>
									<tr>
										<td class="labeloboldwhite" colspan="4" valign="middle"></td>
										<td class="hyperevencenter" style="background-color: white;" valign="middle"><b>Total:</b></td>
										<td class="readonlytextnumbereven" style="background-color: white;" valign="middle"><b>
											<fmt:formatNumber value="${InvoiceJobCommissionPOForm.totalRevenue}" type="currency" pattern="#,###,##0.00"/>
										</b></td>
									</tr>

									<tr height="5">
										<td colspan="6">&nbsp;</td>
									</tr>
									<tr>
										<td class="labeloboldwhite">
											<html:submit property="completePOs" styleClass="button_c">Submit</html:submit>
										</td>
										<td class="labeloboldwhite" colspan="5"></td>
									</tr>
								</c:if>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
							<c:if test="${not empty InvoiceJobCommissionPOForm.go || not empty InvoiceJobCommissionPOForm.completePOs}">
								<tr height="5">
									<td colspan="6">&nbsp;</td>
								</tr>
								<tr>
									<td class="message">
									<td class="message" colspan="5">
										There were no records found matching your search criteria.
									</td>
								</tr>
							</c:if>
						</c:otherwise>
				</c:choose>
				</td>
			</tr>
		</table>
	</html:form>
</body>
</html>