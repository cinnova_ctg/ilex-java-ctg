<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
int joblist_size =( int ) Integer.parseInt( request.getAttribute( "joblistlength" ).toString()); 
%>
<head>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language="JavaScript" src="javascript/prototype.js"></script>
	<script language = "JavaScript" src = "javascript/UtilityFunction.js"></script>
</head>
<script>
function changeSearchType(value){
	document.forms[0].powo_number.value = '';
	document.forms[0].partner_name.value = '';
	document.forms[0].invoiceno.value = '';
	document.forms[0].powo_number.value = '';
	document.forms[0].partner_name.value = '';
	document.forms[0].customer_name.value = '';
	document.forms[0].from_date.value = '';
	document.forms[0].to_date.value = '';
		
	if(value == 'po'){
		document.getElementById('hide1').style.display = '';
		document.getElementById('hide2').style.display = '';
		document.getElementById('hide3').style.display = '';
		document.getElementById('hide4').style.display = '';
		document.getElementById('hide5').style.display = 'none';
		document.getElementById('hide6').style.display = 'none';
		document.getElementById('hide7').style.display = 'none';
		document.getElementById('hide8').style.display = 'none';
	}
	if(value == 'invoice'){
		document.getElementById('hide5').style.display = '';
		document.getElementById('hide6').style.display = '';
		document.getElementById('hide7').style.display = '';
		document.getElementById('hide8').style.display = '';
		document.getElementById('hide1').style.display = 'none';
		document.getElementById('hide2').style.display = 'none';
		document.getElementById('hide3').style.display = 'none';
		document.getElementById('hide4').style.display = 'none';
		
	}	
}
</script>

<html:form action="/InvoiceJobSearch"> 
<body id = "pbody" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr> 
    <td  width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr> 
          <td class="toprow1"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<html:hidden property = "toggle"/>
<table  border = "0" cellspacing = "1" cellpadding = "1" >
 <tr>
  <td><img src = "images/spacer.gif" width = "2" height = "0"></td>
  <td>
  	<table border = "0" cellspacing = "1" cellpadding = "1" width="1000">
  	
	  	<tr>
	  		<td class="labeleboldwhite" height="30"  colspan="21">&nbsp;Job Search</td>
	   	</tr>
			<tr>
				<td  colspan="21">
					<table cellpadding="0" cellspacing="5" border="0">
					<tr>
						<td class="labeloboldwhite" colspan="8">
							<html:radio name="InvoiceJobDetailForm" property="searchType" value="Invoice" onclick="changeSearchType('invoice');">Invoice</html:radio>
							<html:radio name="InvoiceJobDetailForm" property="searchType" value="PO" onclick="changeSearchType('po');">PO</html:radio>
						</td>
					</tr>
					<c:if test=""></c:if>
					<tr>	
						<td class="labeloboldwhite" id="hide1" <c:if test="${InvoiceJobDetailForm.searchType eq 'Invoice'}"> style="display: none;"</c:if> nowrap="nowrap">PO Number:</td>
						<td class="labeloboldwhite" id="hide2" <c:if test="${InvoiceJobDetailForm.searchType eq 'Invoice'}"> style="display: none;"</c:if> nowrap="nowrap"><html:text  styleClass="textbox" size="10" property ="powo_number"/></td>
						<td class="labeloboldwhite" id="hide3" <c:if test="${InvoiceJobDetailForm.searchType eq 'Invoice'}"> style="display: none;"</c:if> nowrap="nowrap">&nbsp;or&nbsp;&nbsp;Partner Name:</td>
						<td class="labeloboldwhite" id="hide4" <c:if test="${InvoiceJobDetailForm.searchType eq 'Invoice'}"> style="display: none;"</c:if> nowrap="nowrap"><html:text  styleClass="textbox" size="10" property ="partner_name"/></td>
						<td class="labeloboldwhite" id="hide5" <c:if test="${InvoiceJobDetailForm.searchType eq 'PO'}"> style="display: none;"</c:if> nowrap="nowrap">Invoice:</td>
						<td class="labeloboldwhite" id="hide6" <c:if test="${InvoiceJobDetailForm.searchType eq 'PO'}"> style="display: none;"</c:if> nowrap="nowrap"><html:text  styleClass="textbox" size="10" property ="invoiceno"/></td>
						<td class="labeloboldwhite" id="hide7" <c:if test="${InvoiceJobDetailForm.searchType eq 'PO'}"> style="display: none;"</c:if> nowrap="nowrap">&nbsp;or&nbsp;&nbsp;Customer Name:</td>
						<td class="labeloboldwhite" id="hide8" <c:if test="${InvoiceJobDetailForm.searchType eq 'PO'}"> style="display: none;"</c:if> nowrap="nowrap"><html:text  styleClass="textbox" size="10" property ="customer_name"/></td>
			        	<td class="labeloboldwhite" nowrap="nowrap">&nbsp;From:</td>
						<td class="labeloboldwhite" nowrap="nowrap"><html:text  styleClass="textbox" size="10" property ="from_date" readonly = "true"/>&nbsp;<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].from_date, document.forms[0].from_date, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img></td>
						<td class="labeloboldwhite">&nbsp;To:</td>
				        <td class="labeloboldwhite"><html:text  styleClass="textbox" size="10" property ="to_date" readonly = "true"/>&nbsp;<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].to_date, document.forms[0].to_date, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img></td>
				        <td>
				        <input type="hidden" name="search" value="Go">
				        <html:submit property="search" styleClass="button_c" onclick = "return searchpartner();">Go</html:submit></td>
			        </tr>
			        </table>
		        </td>
		      </tr>
			<TR height="1">
          	<TD height="1">&nbsp;</TD>
			</tr>
		<logic:present name="joblist">
		<tr> 
	    	<td rowspan = "2">&nbsp;</td>
	    	<td class="tryb" rowspan="2" valign="middle">Customer<br>Name</td>
		    <td class="tryb" rowspan="2" valign="middle">Project<br>Name</td>
		    <td class="tryb" rowspan="2" valign="middle">Job<br>Name</td>
   		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.extendedprice"/></td>
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.status"/></td>
		    <td class="tryb" <c:if test="${InvoiceJobDetailForm.searchType eq 'Invoice'}">colspan="5"</c:if><c:if test="${InvoiceJobDetailForm.searchType eq 'PO'}">colspan="4"</c:if>><bean:message bundle = "IM" key = "im.dates"/></td>
		    
		    <c:if test="${InvoiceJobDetailForm.searchType eq 'Invoice'}">
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.invoiceno"/></td>
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.exportstatus"/></td>
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.customerreference"/></td>
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.owner"/></td>
		    </c:if>
		    
		    <c:if test="${InvoiceJobDetailForm.searchType eq 'PO'}">
		    <td class="tryb" rowspan="2" valign="middle">PVS<br>Name</td>
		    <td class="tryb" rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.po"/></td>
		    <td class="tryb" rowspan="2" valign="middle">PO<br>Authorized<br>Total</td>
		    <td class="tryb" rowspan="2" valign="middle">Approved<br>Total</td>
   		    <td class="tryb" rowspan="2" valign="middle">Paid<br>Status</td>
   		    <td class="tryb" rowspan="2" valign="middle">Paid<br>Date</td>
   		    <td class="tryb" rowspan="2" valign="middle">Check<br>Number</td>
			</c:if>
	  	</tr>
	  
	  <tr> 
		<td class="tryb"> 
	      <div align="center"><bean:message bundle = "IM" key = "im.start"/></div>
	    </td>
	    
		<td class="tryb"> 
	      <div align="center"><bean:message bundle = "IM" key = "im.end"/></div>
	    </td>
	    
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "IM" key = "im.complete"/></div>
	    </td>
	    
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "IM" key = "im.closed"/></div>
	    </td>
	    <c:if test="${InvoiceJobDetailForm.searchType eq 'Invoice'}">
	    <td class="tryb"> 
	      <div align="center"><bean:message bundle = "IM" key = "im.invoice"/></div>
	    </td>
	    </c:if>
	  </tr>
   
      <% if(joblist_size > 0) { %>	
      
	
	<%int i=1;
	String bgcolor="";
	String bgcolortext=""; 
	String hypertext=""; 
	String hypertextcenter=""; 
	int count = 0;
	%>
	
		<logic:iterate id="jlist" name="joblist" >
		<%if(++count > 50){break;} %>
		<%if((i%2)==0)
		{
			bgcolor = "readonlytextnumbereven";
			bgcolortext = "readonlytexteven";
			hypertext = "hypereven"; 
			hypertextcenter = "hyperevencenter";
		
		} 
		else
		{
			bgcolor="readonlytextnumberodd";
			bgcolortext="readonlytextodd";
			hypertext = "hyperodd"; 
			hypertextcenter = "hyperoddcenter"; 
		}
		%>
		
		<bean:define id = "jid" name = "jlist" property = "jobId" />
		<bean:define id="appendixId" name = "jlist" property = "appendixId"/>
		<bean:define id = "job_status" name = "jlist" property = "job_type" />
	  <tr>		
	  
		<td  class="labeleboldwhite"></td>
		<td  align="center" class="<%=hypertext %>"><bean:write name="jlist" property="msaName"/></td>
		<td  align=center class="<%=hypertext %>">
			&nbsp;<a href="AppendixHeader.do?function=view&appendixid=<bean:write name="jlist" property="appendixId"/>&inv_type=<bean:write name="InvoiceJobDetailForm" property="type" />&inv_id=<bean:write name="InvoiceJobDetailForm" property="id" />&inv_rdofilter=<bean:write name="InvoiceJobDetailForm" property="rdofilter" />&viewjobtype=<bean:write name="InvoiceJobDetailForm" property="viewjobtype"/>&ownerId=<bean:write name="InvoiceJobDetailForm" property="ownerId"/>&invoice_Flag=<bean:write name="InvoiceJobDetailForm" property="invoice_Flag"/>&invoiceno=<bean:write name="InvoiceJobDetailForm" property="invoiceno"/>&partner_name=<bean:write name="InvoiceJobDetailForm" property="partner_name"/>&powo_number=<bean:write name="InvoiceJobDetailForm" property="powo_number"/>&from_date=<bean:write name="InvoiceJobDetailForm" property="from_date"/>&to_date=<bean:write name="InvoiceJobDetailForm" property="to_date"/>"><bean:write name="jlist" property="appendixName"/></a>&nbsp;	
		</td>
		<td  align=center class="<%=hypertext %>">
			<a href="javascript:void(0)"  onClick = "if (window.event || document.layers) showWindow(event,<bean:write name="jlist" property="jobId"/>); else showWindow('',<bean:write name="jlist" property="jobId"/>);"><img name = "icon"  src = "images/icon.gif" width="13" height="13" style="vertical-align: middle;"  border = "0" /></a>
			&nbsp;<a href="JobDashboardAction.do?jobid=<bean:write name="jlist" property="jobId"/>&ticket_id=<bean:write name="jlist" property="ticket_id" />&appendix_Id=<bean:write name="jlist" property="appendixId" />"><bean:write name="jlist" property="jobName"/></a>
			<logic:equal name="jlist" property="out_of_scope_activity_flag" value = "Y">
				&nbsp;&nbsp;<img name = "outscopeactivityflag"  src = "images/oos1.gif" width = "14" height = "11" border = "0" alt = '<bean:message bundle = "IM" key = "im.outscopeflagpresent"/>'/>
			</logic:equal>
			<logic:notEqual name="jlist" property="out_of_scope_activity_flag" value = "Y">
			</logic:notEqual>
		</td>
		
		<td  align="center" class="<%=bgcolor %>"><bean:write name="jlist" property="extendedprice"/></td>
		<td  align="center" class="<%=bgcolortext %>"><bean:write name="jlist" property="status"/></td>
		<td  align="center" class="<%=bgcolortext %>"><bean:write name="jlist" property="actualstartdate"/></td>
		<td  align="center" class="<%=bgcolortext %>"><bean:write name="jlist" property="actualcompletedate"/></td>
		<td  align="center" class="<%=bgcolortext %>"><bean:write name="jlist" property="submitteddate"/></td>
		<td  align="center" class="<%=bgcolortext %>"><bean:write name="jlist" property="closed_date"/></td>
		
		<c:if test="${InvoiceJobDetailForm.searchType eq 'Invoice'}">
		<td  align="center" class="<%=bgcolortext %>"><bean:write name="jlist" property="associated_date"/></td>
		<td  align="center" class="<%=bgcolortext %>"><bean:write name="jlist" property="invoice_no"/></td>
		<td  align="center" class="<%=hypertextcenter %>"><bean:write name="jlist" property="exportStatus"/></td>		
		<td  align="center" class="<%=hypertext %>"><bean:write name="jlist" property="customerref" />
		<html:hidden name="jlist" property="customerref"/></td>
		<td  align="center" class="<%=hypertext %>"><bean:write name="jlist" property="createdby"/></td>
		</c:if>
		
		<c:if test="${InvoiceJobDetailForm.searchType eq 'PO'}">
		<td  align="center" class="<%=hypertext %>"><bean:write name="jlist" property="partner_name"/></td>
		<td  align="center" class="<%=hypertextcenter %>"><bean:write name="jlist" property="powo"/></td>
		<td  align="center" class="<%=bgcolor %>"><bean:write name="jlist" property="poAuthorizedTotal"/></td>
		<td  align="center" class="<%=hypertextcenter %>"><bean:write name="jlist" property="approved_total"/></td>
		<td  align="center" class="<%=hypertextcenter %>"><bean:write name="jlist" property="paid_status"/></td>
		<td  align="center" class="<%=hypertextcenter %>"><bean:write name="jlist" property="paid_date"/></td>
		<td  align="center" class="<%=hypertextcenter %>"><bean:write name="jlist" property="check_number"/></td>
		</c:if>
		
	  </tr>
	  <%i++; %>
	  </logic:iterate>
	  <%if(count > 50) {%>
	  <tr>
	  	<td class="labeleboldwhite" colspan="1">&nbsp;</td> 
		<td  colspan = "22" class = "tryb" style="text-align: left;">
		  The search results exceed the maximum limit of 50 records. If the record of interest is not listed above, refine the search by entering a more complete name or smaller From/To window.
		</td>
	  </tr>
	  <%}%>	
	  
	<% } else { %>
		<tr>
			<td  colspan = "14" class = "message" height = "30" style="padding-left: 8px;">
				There were no records found matching your search criteria.
			</td>
		</tr>
	<% } %>
	</logic:present>
	<tr>
			<td>
				<div id="ajaxdiv"  style=" position:absolute;"></div>
			</td>
		</tr> 
   </table>
  </td>
 </tr>
</table>
</body>
</html:form>
<script>
function searchpartner()
{
	
	if(document.forms[0].partner_name.value=="" && document.forms[0].powo_number.value=="" && document.forms[0].from_date.value=="" && document.forms[0].to_date.value=="" && document.forms[0].invoiceno.value=="" && document.forms[0].customer_name.value=="")
	{
		alert("To search Jobs, at least one parameter is required !");
		return false;
	}
	
	if(!isBlank(document.forms[0].powo_number.value) || !isBlank(document.forms[0].invoiceno.value)){
		document.forms[0].partner_name.value = '';
		document.forms[0].from_date.value = ''
		document.forms[0].to_date.value = ''
		document.forms[0].customer_name.value = ''
	} else if( !isBlank(document.forms[0].from_date.value) && !isBlank(document.forms[0].to_date.value) ){
		if(document.forms[0].customer_name.value == '' && document.forms[0].partner_name.value == '' ){
			alert("To search Jobs, "+getLable()+", from and to date are required !");
			return false;
		}
	} else if(!isBlank(document.forms[0].customer_name) || !isBlank(document.forms[0].partner_name.value)){
		if(isBlank(document.forms[0].from_date.value) || isBlank(document.forms[0].to_date.value)){
			alert("To search Jobs, "+getLable()+", from and to date are required !");
			return false;
		}
	} else{
		alert("To search Jobs, at least one parameter is required !");
		return false;
	}
	
	
	if(Date.parse(document.forms[0].from_date.value)>Date.parse(document.forms[0].to_date.value))
	{
	alert("\"From Date\" should be less than \"To Date\"");
	return false;
	}
	document.forms[0].action = "InvoiceJobSearch.do?";
	document.forms[0].submit();
	return true;
}

function getLable(){
	if(getRedioValue(document.forms[0].searchType)== 'PO')
		return 'Partner Name';
	else if(getRedioValue(document.forms[0].searchType)== 'Invoice')
		return 'Customer Name';
	else 
		return 'Name';
}
</script>

</html>

