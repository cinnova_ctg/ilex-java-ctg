<div id="menunav">
    <ul>
        <li>
            <a href="#" class="drop"><span>Manage</span></a>
            <ul>
                <li><a href="JobEditAction.do?ref=inscopeview&appendix_Id=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&Job_Id=0">Add Job</a></li>
                <li>
                    <a href="#"><span>Bulk Job</span></a>
                    <ul>
                        <li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
                        <li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= appendixid %>">Large Deployment Add</a></li>
                        <li><a href="ChangeJOSAction.do?ref=bukJobCreationView&appendixid=<%= appendixid %>">Change Job Owner/Scheduler</a></li>
                    </ul>
                </li>
                <% if (contractDocMenu != "") { %>
                <li>
                        <a href="#"><span>Contract Documents</span></a>
                        <ul>
                            <%= contractDocMenu %>
                    </ul>
                    </li>
                <% } %>
                <li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>">Custom Fields</a></li>
                <li><a href="AddCustRef.do?typeid=<%= appendixid %>&type=Appendix&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>">Customer Reference</a></li>
                <li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&fromPage=Appendix">External Sales Agent</a></li>
                <li>
                    <a href="#"><span>Documents</span></a>
                    <ul>
                        <li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=Appendix">Appendix History</a></li>
                        <li>
                            <a href="#"><span>Support Documents</span></a>
                            <ul>
                                <li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&fromPage=Appendix&linkLibName=Appendix&entityType=Appendix Supporting Documents&function=addSupplement">Upload</a></li>
                                <li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&fromPage=Appendix&linkLibName=Appendix&function=supplementHistory">History</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&initialCheck=true&pageType=jobsites">Job Sites</a></li>
                <li>
                    <a href="#"><span>Schedule</span></a>
                    <ul>
                        <li><a href="CostSchedule.do?appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>">View</a></li>
                        <% if (appendixcurrentstatus . equals("F")) { %>
                        <li><a href="javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotupdateprojectschedule"/>')">Update</a></li>
                        <% } else { %>
                        <li><a href="ScheduleUpdate.do?function=update&typeid=<%= appendixid %>&type=P&page=appendixdashboard&pageid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>">Update</a></li>
                        <% } %>
                    </ul>
                </li>
                <li>
                    <a href="#"><span>Comments</span></a>
                    <ul>
                        <li><a href="javascript:viewcomments();">View</a></li>
                        <li><a href="javascript:addcomment();">Add</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span>Partner Information</span></a>
                    <ul>
                        <li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>"><bean:message bundle = "AM" key="am.Partnerinfo.sow"/></a></li>
                        <li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>"><bean:message bundle = "AM" key="am.Partnerinfo.assumptions"/></a></li>
                        <li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>"><bean:message bundle = "AM" key="am.Partnerinfo.Inst_cond"/></a></li>
                    </ul>
                </li>
                <li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&fromPage=Appendix">Team</a></li>
                <%
                if (request . getAttribute("ilex_php_url") != null) {
                    %>
                <li><a href="/content/ilex/#/appendix/<%= appendixid %>/email_subscriptions">Email Subscriptions</a></li>
                    <%
                }
                %>
                <li><a href="OnlineBid.do?appendixId=<%= appendixid %>&fromPage=Appendix">Online Bid</a></li>
                <li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=appendix">MPO</a></li>
                <li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
                <li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
            </ul>
        </li>
        <li>
            <a href="#" class="drop"><span>Reports</span></a>
            <ul>
                <li><a href="javascript:openreportschedulewindow('redline')"><bean:message bundle = "PRM" key = "prm.appendix.redlinereport"/></a></li>
                <li><a href="javascript:openreportschedulewindow('detailsummary')"><bean:message bundle = "PRM" key = "prm.appendix.detailedsummary"/></a></li>
                <li><a href="/content/reports/csv_summary/"><bean:message bundle = "PRM" key = "prm.appendix.csvsummary"/></a></li>
                <li>
                    <a href="#"><span>Status</span></a>
                    <ul>
                        <li><a href="javascript:managestatus( 'view' )"><bean:message bundle = "PRM" key = "prm.appendix.view1"/></a></li>
                        <li><a href="javascript:managestatus( 'update' )"><bean:message bundle = "PRM" key = "prm.appendix.update"/></a></li>
                        <li><a href="javascript:sendstatusreport()"><bean:message bundle = "PRM" key = "prm.appendix.send"/></a></li>
                    </ul>
                </li>
                <li><a href="MSPCCReportAction.do?function=View&appendixid=<%= appendixid %>&viewjobtype=<%= viewjobtype %>&ownerId=<%= ownerid %>&fromPage=Appendix">MSP C/C Report</a></li>
            </ul>
        </li>
        <li>
            <a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%= ownerid %>&msaId=<%= msaid %>&fromPage=viewselector" class="drop">Search</a>
        </li>
        <li>
            <a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name="AppendixHeaderForm" property="appendixid"/>&ownerId=<bean:write name="AppendixHeaderForm" property="ownerId"/>&apiAccess=apiAccess" class="drop">API Access</a>
        </li>
    </ul>
</div>
