 <div id="menunav">
	<ul>
		<%
		if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )) {
			if(msastatus.equals( "Cancelled" )) {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')">Edit</a></li>
				<%
			} else {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')">Edit</a></li>
				<%
			}
		} else {
			%>
			<li><a class="drop" href="MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail">Edit</a></li>	
			<%
		}
		%>
		<li>
			<a href="#" class="drop"><span>Manage</span></a>
			<ul>
				<li>
					<%
					if((String) request.getAttribute("list") != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/></span></a>
						<ul>
						
						
						
							<%= (String)request.getAttribute("list") %>
												
						</ul>
						<%
					} else {
						%>
						<a href="#"><bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/></a>
						<%
					}
					%>
				</li>
				<li>
					<a href="#">View MSA</span></a>
					<ul>
						<li><a href="MSADetailAction.do?MSA_Id=<%= MSA_Id%>&Prjid=<%= MSA_Id %>">Details</a></li>
						<%
						if( msastatus.equals( "Signed" ) || msastatus.equals( "Approved" ) || msastatus.equals( "Pending Customer Review" )) {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/></a></li>
							<li><a href="javascript:view('rtf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewrtf"/></a></li>
						     <%
							if(request.getAttribute("new_addendumlist") != "" ) {
								%>
								<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "msa.detail.menu.viewhtml"/></a></li>
								<%
								/* if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) { */
								
								
									
								
// 								} 
								
					//			else {
									%>
									
									<%
					//			}
							} 
						}
						%> 
					</ul>
				</li>
				<%
				if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )) {
					%>
					<li><a href="javascript:sendemail();"><bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/></a></li>
					
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.sendMSA.nopriviledgestatus"/>');"><bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/></a></li>
					<%
				}
				if( msastatus.equals( "Signed" ) || msastatus.equals( "Inwork" ) ) {
				%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus"/>');"><bean:message bundle = "pm" key = "msa.detail.formaldocdate"/></a></li>
					
					<%
				}
				  else {
					%>
					<li><a href="EditBdmAction.do?msaId=<%= MSA_Id %>&fromType=MSADetail">Edit BDM</a></li>
					
					<%
				} %>
				<% 
				if( msastatus.equals( "Signed" ) || msastatus.equals( "Inwork" )) {
					if(msastatus.equals( "Cancelled" ))	
				{%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus.cancelled"/>');"><bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegeinworkstatus"/></a></li>-->
					
					<%
				} }else {
					%>
					<li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/></a></li>
					
					<%
				}
				
				%>
				<li>
					<a href="#"><span>MSA Comments</span></a>
					<ul>
						<li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "msa.detail.menu.viewall"/></a></li>
						<%
						if(msastatus.equals( "Cancelled" )) {
							%>
							<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/></a></li>
							<%
						}
						%>
					</ul>
				</li>
				<%
				if( msastatus.equals( "Pending Customer Review" )) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%= MSA_Id %>&entityType=MSA"><bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.upload.state"/>');"><bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/></a></li>
					<%
				}
				if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )) {
					if(msastatus.equals( "Cancelled" )) {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>
						<%
					} else {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>
						<%
					}
				} else {
					%>
					<li><a href="javascript:editmsa();"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>	
					<%
				}
				%>
				
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "msa.detail.menu.documents"/></span></a>
					<ul>
						<li><a href="EntityHistory.do?entityId=<%= MSA_Id %>&entityType=MSA&function=viewHistory&msastatus=<%=msastatus%>">MSA  History</a></li>
						<li>
							<a href="#"><span>Support Documents</span></a>
							<ul>
								<li><a href="EntityHistory.do?entityId=<%= MSA_Id %>&msaId=<%= MSA_Id %>&entityType=MSA Supporting Documents&function=addSupplement&linkLibName=MSA">Upload</a></li>
								<li><a href="EntityHistory.do?entityType=MSA Supporting Documents&msaId=<%= MSA_Id %>&linkLibName=MSA&function=supplementHistory">History</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="javascript:del();"><bean:message bundle = "pm" key = "msa.detail.menu.deletemsa"/></a></li>
			</ul>
		</li>
		<li><a class="drop" href="AppendixUpdateAction.do??firstCall=true&MSA_Id=<%= MSA_Id%>">Appendices</a></li>
	</ul>
</div>
	<!-- End of Style -->	