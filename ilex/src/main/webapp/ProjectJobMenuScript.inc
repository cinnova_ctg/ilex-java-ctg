<SCRIPT language = JavaScript1.2>
if (isMenu) {
arMenu1 = new Array(
120,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",

	<%if((String)request.getAttribute( "prjJobStatusList" ) != "" ) {%>
		"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",1,
	<%}else{%>
		"<bean:message bundle = "PRM" key = "prm.changestatus"/>","#",0,
	<%}%>	
	"Documents","#",1,
	"Process Checklist","ChecklistTab.do?job_id=<%=Job_Id%>&page=jobdashboard&viewjobtype=ION&ownerId=1",0,
	"Email Insurance Certificate Request","InsuranceRequest.do?msaId=<%= MSA_Id%>&jobId=<%=Job_Id%>&appendixId=<%=Appendix_Id%>",0,
	"Job Notes","JobNotesAction.do?jobId=<%=Job_Id%>&appendixid=<%=Appendix_Id%>",0
	<%if(request.getAttribute("isTicket")!=null && ((String)request.getAttribute( "isTicket" )).trim().equalsIgnoreCase("Ticket") ) {%>
		,"Depot Requisition","DepotRequisition.do?appendixid=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>",0
		<%if( request.getAttribute("jobStatus")!=null && !((String)request.getAttribute("jobStatus")).trim().equalsIgnoreCase("O") ){%>
		,"Change HelpDesk/ Criticality","TicketChangeCriticality.do?msaId=<%= MSA_Id%>&Job_Id=<%=Job_Id%>&appendixId=<%=Appendix_Id%>",0
		,"Change Request Type","ChangeTicketRequestType.do?msaId=<%= MSA_Id%>&Job_Id=<%=Job_Id%>",0
		<%}%>
		<%if( request.getAttribute("onsite")!=null && ((String)request.getAttribute("onsite")).trim().equalsIgnoreCase("onsite") ){%>
		 ,"Cancel Ticket","javascript:cancelTicket('<%= request.getAttribute("alreadyCancelled")%>');",0
		<%}%>
	<%}%>	
		,"Test Indicator","#",1
)

<%if((String)request.getAttribute( "prjJobStatusList" ) != "" ) {%>
	arMenu1_1=new Array(
		<%= (String) request.getAttribute( "prjJobStatusList" ) %>
	)
<%}%>

arMenu2=new Array(
120,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
"Update","javascript:projectJobManageStatus( 'update' )",0,
"Send","javascript:projectJobSendStatusReport()",0,
"View","javascript:projectJobManageStatus( 'view' )",0
)

arMenu3=new Array(
120,
"","",
"","",
"",""
)


arMenu1_2=new Array(
"Upload","EntityHistory.do?entityId=<%=Job_Id%>&jobId=<%=Job_Id%>&entityType=Deliverables&function=addSupplement&linkLibName=Job",0,
"History","EntityHistory.do?entityType=Deliverables&jobId=<%=Job_Id%>&linkLibName=Job&function=supplementHistory",0
)
<%if(request.getAttribute("isTicket")==null || !((String)request.getAttribute( "isTicket" )).trim().equalsIgnoreCase("Ticket") ) {%>
arMenu1_6=new Array(
	"None","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=None&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Demonstration","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Demonstration&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Internal Test","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Internal Test&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0
)
<%} else if(request.getAttribute("isTicket")!=null && ((String)request.getAttribute( "isTicket" )).trim().equalsIgnoreCase("Ticket")
		&& request.getAttribute("jobStatus")!=null && !((String)request.getAttribute("jobStatus")).trim().equalsIgnoreCase("O") 
		&& request.getAttribute("onsite")!=null && ((String)request.getAttribute("onsite")).trim().equalsIgnoreCase("onsite")) {%>
arMenu1_10=new Array(
	"None","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=None&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Demonstration","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Demonstration&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Internal Test","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Internal Test&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0
)
<%} else if(request.getAttribute("isTicket")!=null && ((String)request.getAttribute( "isTicket" )).trim().equalsIgnoreCase("Ticket")
		&& request.getAttribute("jobStatus")!=null && !((String)request.getAttribute("jobStatus")).trim().equalsIgnoreCase("O") 
		&& request.getAttribute("onsite")==null) {%>
arMenu1_9=new Array(
	"None","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=None&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Demonstration","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Demonstration&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Internal Test","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Internal Test&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0
)
<%} else if(request.getAttribute("isTicket")!=null && ((String)request.getAttribute( "isTicket" )).trim().equalsIgnoreCase("Ticket")
		&& (request.getAttribute("jobStatus")==null || (request.getAttribute("jobStatus")!=null && ((String)request.getAttribute("jobStatus")).trim().equalsIgnoreCase("O")))
		&& request.getAttribute("onsite")==null) {%>
arMenu1_7=new Array(
	"None","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=None&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Demonstration","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Demonstration&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Internal Test","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Internal Test&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0
)
<%} else if(request.getAttribute("isTicket")!=null && ((String)request.getAttribute( "isTicket" )).trim().equalsIgnoreCase("Ticket")
		&& (request.getAttribute("jobStatus")==null || ((String)request.getAttribute("jobStatus")).trim().equalsIgnoreCase("O"))
		&& request.getAttribute("onsite")!=null) {%>
arMenu1_8=new Array(
	"None","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=None&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Demonstration","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Demonstration&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0,
	"Internal Test","JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Internal Test&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>",0
)
<%}%>

document.write("<SCRIPT LANGUAGE = 'JavaScript1.2' SRC = 'javascript/hierMenus.js'><\/SCRIPT>");
}
</SCRIPT>