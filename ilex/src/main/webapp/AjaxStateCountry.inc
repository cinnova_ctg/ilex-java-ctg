<script>
function getAjaxStateCountryMatch(){

        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.forms[0].country.value = response;
               }
        }
     
 	
        ajaxRequest.open("POST", "StateCountry.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("stateid="+document.forms[0].cmboxState.value); 
}

function ChangeCountry(){
	if(document.forms[0].stateSelect.value == 0){
		document.forms[0].country.value = ' '; // - for default combo value
	}
	else{
		var arrayVar = new Array();
		arrayVar = (document.forms[0].stateSelect.value).toString().split(",");
		document.forms[0].state.value = arrayVar[0];
		document.forms[0].country.value = arrayVar[1];
	}
}
</script>
