<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE HTML>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project Manager view selector</title>
</head>
<% 
/* Variables of View Selector */
	
	String prjType = "";
	String pageName = "";
	int addStatusRow = -1;
	int addOwnerRow = -1;
	boolean addStatusSpace = false;
	boolean addOwnerSpace = false;
	String checkowner = null;
	String checkstatus = null;
	int rowHeight = 120;
	if(request.getParameter("projectType") != null){ prjType = request.getParameter("projectType"); }
	if(request.getParameter("pageName") != null){ pageName = request.getParameter("pageName"); }
	String formBean = request.getParameter("bean");
%>
<body>
<!-- For Assign Team: Start -->
<%@ include file = "/NMenu.inc" %> 					<!-- Include file for Menu CSS -->
<%@ include  file="/AjaxOwners.inc" %>  			<!-- Include file for Ajax call -->
<%@ include  file="/DashboardStatusScript.inc" %> 	<!-- Include file for JavaScript function -->

<%if(prjType.equals("Appendix")) { // - For Appendix: Start%>
<!-- Include file get parameter and Script function for Appendix Dashboard Menu -->
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %> 	

<!-- Hidden element for menu: Start -->
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property = "msaId" />
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property="fromPage"/>
<!-- Hidden element for menu: End -->

<!-- Table for Menu and View Selector: Start -->
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
<tr>
	<!-- Top Menu: Start -->
	<td valign="top" width = "100%">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		<tr>
			<td valign="top" width = "100%">
				<%if(prjType.equals("Appendix")) {%>
				<table cellpadding="0" cellspacing="0" border="0" width = "100%">
					<tr> 
						<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
						<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
						<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid %>" style="width: 120px"><center>Search</center></a></td>
						<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					</tr>
					<tr>
			        	<td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		  	<span id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
											 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixid }'/>"><c:out value='${requestScope.appendixName }'/></a>
											 <a ><span class="breadCrumb1">MSP C/C Report</span></a>
									</span>		
				        </td>
				    </tr>
					<%if(pageName.equals("AssignTeam.jsp")) {%>
						<tr>
							<td colspan ="4" ><h2><bean:message bundle="PRM" key="prm.assign.team.appendix"/>&nbsp;<bean:write name ="<%=formBean%>" property="appendixName" /></h2></td>
						</tr>
					<%} %>
				</table>
				<%} %>
			</td>
		</tr>
	</table>
	</td>
	<!-- Top Menu: End -->
	
	<!-- Top sky blue: Start -->
	<td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<!-- Top sky blue: End -->
	
	<!-- View Selector: Start -->
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="155" height="39" class="headerrow" colspan="2">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="150">
						<table width="100%" cellpadding="0" cellspacing="3" border="0">
							<tr>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" 
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');" /></a>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td height="21" background="images/content_head_04.jpg" width="140">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="70%" colspan="2">&nbsp;</td>
						<td nowrap="nowrap" colspan="2">
							<div id="featured1">
								<div id="featured">
									<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img	src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();" /></a><a href="javascript:void(0)"
										class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();" /></a>
									</li>
								</div>
							</div>
							<span>
							<div id="filter" class="divstyle">
							<table width="250" cellpadding="0" class="divtable">
								<tr>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Status View</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText">
											<logic:present name="jobStatusList" scope="request">
												<logic:iterate id="list" name="jobStatusList">
													<bean:define id="statusName" name="list" property="statusdesc" type="java.lang.String" />
													<bean:define id="statusId" name="list" property="statusid" type="java.lang.String" />
													<%checkstatus = "javascript: checkingStatus('" + statusId + "');";
													if ((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
														addStatusRow++; addStatusSpace = true;
													}%>
													<%if (addStatusSpace) {%>
													&nbsp;&nbsp;&nbsp;&nbsp;
													<%}%>
													<html:multibox property="jobSelectedStatus" onclick="<%=checkstatus%>">
														<bean:write name="list" property="statusid" />
													</html:multibox>
													<bean:write name="list" property="statusdesc" />
													<br> 
													<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present> <img src="images/hrfilterLine.gif" width="100px" class="imagefilter" />
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="week">This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="month">This Month</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>"	property="jobMonthWeekCheck" value="clear">Clear</html:radio></td></tr>
											</table>
											</td>
										</tr>
									</table>
									</td>
									<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter" /></td>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table> 
									</td>
								</tr>
							</table>
							</div>
							</span>		
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</td>
	<!-- View Selector: End -->
</tr>
</table>
<!-- Table for Menu and View Selector: End -->

<!-- Inclue file for Menu Script -->
<%@ include  file="/AppedixDashboardMenuScript.inc" %> <!-- Include file for appendix menu script -->
<%}	//- For Appendix: End%>

<%if(prjType.equals("NetMedX")) { //- For NetMedX: Start%>
<!-- Include file get parameter and Script function for Appendix Dashboard Menu -->
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %> 	

<!-- Hidden element for menu: Start -->
<html:hidden property="viewjobtype" />
<html:hidden property="ownerId" />
<html:hidden property = "msaId" />
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property="fromPage"/> 
<!-- Hidden element for menu: End -->

<!-- Table for Menu and View Selector: Start -->
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
<tr>
	<!-- Top Menu: Start -->
	<td valign="top" width = "100%">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		<tr>
			<td valign="top" width = "100%">
				<table cellpadding="0" cellspacing="0" border="0" width = "100%">
					<tr>
						<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
						<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
						<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>" style="width: 120px"><center>Search</center></a></td>
						<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					</tr>
					<tr>
						<td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%"><div id="breadCrumb">&nbsp;</div></td>
					</tr>
					<%if(pageName.equals("AssignTeam.jsp")) {%>
						<tr>
							<td colspan ="4" ><h2><bean:message bundle="PRM" key="prm.assign.team.netmedex"/>&nbsp;<bean:write name ="<%=formBean%>" property="appendixName" /></h2></td>
						</tr>
					<%} %>
				</table>
			</td>
		</tr>
	</table>
	</td>
	<!-- Top Menu: End -->
	
	<!-- Top sky blue: Start -->
	<td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<!-- Top sky blue: End -->
	
	<!-- View Selector: Start -->
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="155" height="39" class="headerrow" colspan="2">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="150">
						<table width="100%" cellpadding="0" cellspacing="3" border="0">
							<tr>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)"
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');" /></a>
								</td>
								<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" 
									onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');" /></a>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td height="21" background="images/content_head_04.jpg" width="140">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="70%" colspan="2">&nbsp;</td>
						<td nowrap="nowrap" colspan="2">
							<div id="featured1">
								<div id="featured">
									<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img	src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();" /></a><a href="javascript:void(0)"
										class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();" /></a>
									</li>
								</div>
							</div>
							<span>
							<div id="filter" class="divstyle">
							<table width="250" cellpadding="0" class="divtable">
								<tr>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Status View</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText">
											<logic:present name="jobStatusList" scope="request">
												<logic:iterate id="list" name="jobStatusList">
													<bean:define id="statusName" name="list" property="statusdesc" type="java.lang.String" />
													<bean:define id="statusId" name="list" property="statusid" type="java.lang.String" />
													<%checkstatus = "javascript: checkingStatus('" + statusId + "');";
													if ((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
														addStatusRow++; addStatusSpace = true;
													}%>
													<%if (addStatusSpace) {%>
													&nbsp;&nbsp;&nbsp;&nbsp;
													<%}%>
													<html:multibox property="jobSelectedStatus" onclick="<%=checkstatus%>">
														<bean:write name="list" property="statusid" />
													</html:multibox>
													<bean:write name="list" property="statusdesc" />
													<br> 
													<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present> <img src="images/hrfilterLine.gif" width="100px" class="imagefilter" />
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="week">This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="month">This Month</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>"	property="jobMonthWeekCheck" value="clear">Clear</html:radio></td></tr>
											</table>
											</td>
										</tr>
									</table>
									</td>
									<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter" /></td>
									<td width="50%" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table> 
									</td>
								</tr>
							</table>
							</div>
							</span>		
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</td>
	<!-- View Selector: End -->
</tr>
</table>
<!-- Table for Menu and View Selector: End -->

<!-- Inclue file for Menu Script -->
<%@ include  file="/NetMedXDashboardMenuScript.inc" %> <!-- Include file for appendix menu script -->
<%} //- For NetMedX: End%>
<!-- For Assign Team: End -->


</body>
</html>