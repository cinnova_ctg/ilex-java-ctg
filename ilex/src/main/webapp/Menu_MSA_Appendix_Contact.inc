<div id="menunav">
	<ul>
	<%
		if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )) {
			if(appendixStatus.equals( "Cancelled" )) {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>
				<%
			} else {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>
				<%
			}
		} else {
			%>
			<li><a class="drop" href="AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>	
			<%
		}
		%>
		<li>
			<a href="#" class="drop"><span>Manage</span></a>
			<ul>
				<li>
					<%
					if((String) request.getAttribute("new_list") != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/></span></a>
						<ul>
							<%= (String) request.getAttribute("new_list") %>
						</ul>
						<%
					} else {
						%>
						<a href="#"><bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/></a>
						<%
					}
					%>
				</li>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.viewappendix"/></span></a>
					<ul>
						<li><a href="AppendixDetailAction.do?MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>">Details</a></li>
						<%
						if( appendixStatus.equals( "Signed" ) || appendixStatus.equals( "Approved" ) || appendixStatus.equals( "Pending Customer Review" )) {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/></a></li>
							<li><a href="javascript:view('rtf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewrtf"/></a></li>
							<%
							if(request.getAttribute("new_addendumlist") != "" ) {
								%>
								<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
								<%
								if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) {
									%>
									<li>
										<a href="#"><span>History</span></a>
										<ul>
											<%= (String) request.getAttribute("new_addendumlist") %>
										</ul>
									</li>
									<li><a href="javascript:view('excel');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/></a></li>
									<%
								} else {
									%>
									<li>
										<a href="#"><span>History</span></a>
										<ul>
											<%= (String) request.getAttribute("new_addendumlist") %>
										</ul>
									</li>
									<%
								}
							} else {
								if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) {
									%>
									<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
									<li><a href="javascript:view('excel');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/></a></li>
									<%
								} else {
									%>
									<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
									<%
								}
							}
						}
						%>
					</ul>
				</li>
				<%
				if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )) {
					%>
					<li><a href="javascript:sendemail();"><bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.sendAppendix.noprivilegestatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/></a></li>
					<%
				}
				if( appendixStatus.equals( "Signed" ) || appendixStatus.equals( "Inwork" ) || appendixStatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:setendcustomer();"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				}
					
					
				if(appendixStatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:addcontact();"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				}
				%>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.comments"/></span></a>
					<ul>
						<li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "appendix.detail.menu.viewall"/></a></li>
						<%
						if(appendixStatus.equals( "Cancelled" )) {
							%>
							<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						}
						%>
					</ul>
				</li>
				<%
				if( appendixStatus.equals( "Pending Customer Review" )) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&entityType=Appendix"><bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.upload.state"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/></a></li>
					<%
				}
				if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )) {
					if(appendixStatus.equals( "Cancelled" )) {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>
						<%
					} else {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>
						<%
					}
				} else {
					%>
					<li><a href="javascript:editappendix();"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>	
					<%
				}
				%>
				<li><a href="ESAEditAction.do?ref=view&appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>">External Sales Agent</a></li>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.menu.documents"/></span></a>
					<ul>
						<li><a href="EntityHistory.do?entityId=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&entityType=Appendix&function=viewHistory">Appendix History</a></li>
						<li>
							<a href="#"><span>Support Documents</span></a>
							<ul>
								<li><a href="EntityHistory.do?entityId=<%= Appendix_Id %>&appendixId=<%= Appendix_Id %>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Appendix">Upload</a></li>
								<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= Appendix_Id %>&linkLibName=Appendix&function=supplementHistory">History</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="javascript:del();"><bean:message bundle = "pm" key = "appendix.detail.menu.deleteappendix"/></a></li>
			</ul>
		</li>
		<li><a class="drop" href="JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>"><bean:message bundle = "pm" key = "appendix.detail.jobs"/></a></li>
	</ul>
</div>