
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<HEAD>

<%@ page 
language="java"

contentType="text/html; charset=ISO-8859-1"

pageEncoding="ISO-8859-1"

%>


<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"
	type="text/css">
<TITLE></TITLE>


<link rel="stylesheet" href="styles/style.css" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td ><img src="images/spacer.gif" width="1" height="1"></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
          <td  width=150 align=left class="toprow1"><a href="#"    onMouseOut="MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu3',event)" class="drop">&nbsp;&nbsp;&nbsp;&nbsp;Add First Level Category</a></td>
          <td width = "650" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
      </table>
    </td>
  </tr>
</table> 

<br>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="2" cellpadding="2" width="400"> 
  <tr> 
    <td colspan="4" class="label4Bold" height="30"><bean:message bundle="CM" key="cm.manageorganizations"/></td>
  </tr> 

  <tr><td colspan="4" class="BlackLine"  height="5"></td></tr>
    <td  width="35%" class="labelobold" height="20" colspan=2><bean:message bundle="CM" key="cm.viewcustomer"/></td>
   

 
   <tr> 
    <td  class="labeleBold"><bean:message bundle="CM" key="cm.customername"/></td>
    <td  class="labele" align="left">
	<html:text styleClass="textbox"  size="14" property="customername" value=""/>
	</td>
  </tr>
  
 
  <tr> 
    <td colspan="2" class="ButtonRow"> 
      <html:submit property="add"><bean:message bundle="CM" key="cm.topdisplay.add"/></html:submit>
    </td>
  </tr>
   <tr  align="right"> 
    <td colspan="30" height="37"><br>
      <a href="#" class="pagelink">Logout </a>|<a href="#" class="pagelink"> Help</a> 
    </td>
  </tr>
</table>
 </td>
 </tr> 
</table>

</BODY>
</html:html>
