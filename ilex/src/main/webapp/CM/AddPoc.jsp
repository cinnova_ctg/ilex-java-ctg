<!DOCTYPE HTML>
<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<bean:define id="dcLonDirec" name="dcLonDirec" scope="request"/>
<bean:define id="dcLatDirec" name="dcLatDirec" scope="request"/>
<bean:define id="dcRegionList" name="dcRegionList" scope="request"/>
<bean:define id="dcCategoryList" name="dcCategoryList" scope="request"/>
<bean:define id="dcCountryList" name="dcCountryList" scope="request"/>
<bean:define id="dcTimeZoneList" name="dcTimeZoneList" scope="request"/>
<bean:define id="dcClassification" name="dcClassification" scope="request"/>
<%
int addTop = 0;

int top = 0;
int minTop =0;
String org_name="";
String table_label="";
String type_name="";
String menu_head="";
String menu_label="";
String menu_label2="";
String js_blank_msg="cm.entertopcustomer";
String add_division="cm.addPOC";
String manage_org="";
String mfg_label="";
String select_label="";
String division_location="cm.divison/location";
String label1 = "";
String label2 = "";
String label3 = "";
String label4 = "";

int comboflag=0;
int flag=0;

char type=((String)request.getParameter("org_type")).trim().charAt(0);
String org_type=request.getParameter("org_type").trim();
if(type == 'P'){
	minTop = -26; 
}
else
	minTop = -26;

switch(type){
case 'C':
{
	org_name="cm.customername";
	break;
}
case 'N':
{

	org_name="cm.CNSname";
	break;

}
case 'P':
{
	
	org_name="cm.PVSname";
	break;

}
default:
{

}

}
%>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"
	type="text/css">
		<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<title></title>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
<jsp:include page="/common/IncludeGoogleKey.jsp"/>
<script type="text/javascript">

  function load()
   {
  
   	if(typeof GBrowserIsCompatible == 'undefined') {
			//alert('Need Internet Connection to calculate Lat/Long.');
   	}
   	
   	  if(typeof GBrowserIsCompatible == 'undefined'){
   	  }else {
	  if (GBrowserIsCompatible())
	  {
		    geocoder = new GClientGeocoder();
	  }
	  
	 }
	
    }

    function showZip() 
    {
    
		{

   			var zip = document.forms[0].zip.value;
   	
      		if (geocoder) {
        	geocoder.getLatLng(
          	zip ,
              function(point) 
              {
                  if (point)
                  { 
                  	
					var i=point.lng();
					if(eval(i<0))
					document.forms[0].lon_direction.value = "S";
					else
					document.forms[0].lon_direction.value = "N";						
					var hh="" + i + "";
					arrr=hh.split(".");
					document.forms[0].lon_deg.value=arrr[0];
					var deg =arrr[0];
					var min = arrr[1]*60;
					var minstr = "" +min +"";
					document.forms[0].lon_min.value=minstr.substring(0,2);

					
					var j =point.lat();
					if(eval(j<0))
					document.forms[0].lat_direction.value = "W";
					else
					document.forms[0].lat_direction.value = "E";
					var gg="" +j + ""; 
					arr=gg.split(".");
					document.forms[0].lat_deg.value=arr[0];
					var deglat = arr[0];
					var minlat = arr[1]*60;
					var minlatstr = "" +minlat +"";
					document.forms[0].lat_min.value=minlatstr.substring(0,2);
					
				 }
				  else
				  {
				  
				  // If lat lon not calculated on zip / address
                  document.forms[0].latlonFound.value = "NA";	
                  	
				  document.forms[0].lon_deg.value="";
				  document.forms[0].lon_min.value="";
				  document.forms[0].lat_deg.value="";
				  document.forms[0].lat_min.value="";
				  }
			  }
        );
      }
    
    }
}

    
    function showAddress(type) {

	//alert(type);
	//alert(document.forms[0].latlonFound.value);
	
	if((type == 'zip') && (document.forms[0].latlonFound.value == 'true')) {
		//alert('in if')
		return false;
	}
	
    var address = document.forms[0].firstaddressline.value;

    if(typeof GBrowserIsCompatible == 'undefined'){
   	}
   	else{
    
      if (geocoder) {
        geocoder.getLatLng(
          address,
              function(point) 
              {
                  if (point)
                  { 
					// If lat lon calculated on address
                  	document.forms[0].latlonFound.value = "true";
                  
					var i=point.lng();
					if(eval(i<0))
					document.forms[0].lon_direction.value = "S";
					else
					document.forms[0].lon_direction.value = "N";						
					var hh="" + i + "";
					arrr=hh.split(".");
					document.forms[0].lon_deg.value=arrr[0];
					
					var deg =arrr[0];
					var min = arrr[1]*60;
					var minstr = "" +min +"";
					document.forms[0].lon_min.value=minstr.substring(0,2);

					
					var j =point.lat();
					if(eval(j<0))
					document.forms[0].lat_direction.value = "W";
					else
					document.forms[0].lat_direction.value = "E";
					var gg="" +j + ""; 
					arr=gg.split(".");
					document.forms[0].lat_deg.value=arr[0];
					
					var deglat = arr[0];
					var minlat = arr[1]*60;
					var minlatstr = "" +minlat +"";
					document.forms[0].lat_min.value=minlatstr.substring(0,2);
				 }
				  else
				  {
				 	// If lat lon calculated on zip
                  	document.forms[0].latlonFound.value = "false";	
				 	showZip();	
				  }
			  }
        );
        
    }
    }
}
    </script>

<%@ include  file="/Menu.inc" %>
<%@ include  file="/AjaxStateCountry.inc" %>

<link rel="stylesheet" href="styles/style.css" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="load();" bgcolor="#FFFFFF" >

<div id="menunav">
    <ul>
  		<li>
			<c:choose>
				<c:when test="${AddPocForm.lo_pc_id ne '0' && AddPocForm.org_type eq 'N' && AddPocForm.empStatus eq '0' && AddPocForm.org_name eq 'Corporate'}">
					<a href="javascript:alert('Addition of POC is not allowed from Terminated section.');" onmouseout="MM_swapImgRestore(); "  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">
				</c:when>
				<c:otherwise>
					<a href="AddPoc.do?function=add&org_type=<%=type%>&org_discipline_id=<bean:write name="AddPocForm" property="org_discipline_id"/>&division_id=<bean:write name="AddPocForm" property="division_id"/>"  onmouseout="MM_swapImgRestore(); "  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">
				</c:otherwise>
			</c:choose>
			<bean:message bundle="CM" key="cm.addPOC"/></a>
  		</li>
  		<li><a href="javascript:del();" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="cm.pocdelete"/></a></li>
	</ul>
</div>

<div id="top1" style="display: block;">
	<html:form action="AddPoc" >
		<table>
			<html:hidden property="lo_pc_ad_id" />
			<html:hidden property="lo_pc_id" />
			<html:hidden property="refresh" />
			<html:hidden property="refreshState" />
			<html:hidden property="refreshType" />
			<html:hidden property="function" />
			<html:hidden property="latlonFound" />
			<html:hidden property="org_type" value='<%=request.getParameter("org_type")%>' />
			<html:hidden property="org_discipline_id" value='<%=request.getParameter("org_discipline_id")%>' />
			<html:hidden property="division_id" value='<%=request.getParameter("division_id")%>' />
			<html:hidden property = "lat_deg" />
			<html:hidden property = "lat_min" />
			<html:hidden property = "lat_direction" />
			<html:hidden property = "lon_deg" />
			<html:hidden property = "lon_min" />
			<html:hidden property = "lon_direction" />
			<html:hidden property = "state" />
		</table>
		<div id="top" style="block: display">
		 	<table  width="603" border="0" cellspacing="1" cellpadding="1" class="ilex_cb_table"> 
				<logic:present name = "AddPocForm" property="deletemessage">
					<logic:equal name="AddPocForm" property="deletemessage" value="-9001">
				  		<tr>
				  			<td colspan="2" class="message">		
				  				<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>
				  			</td>
				  		</tr>
					</logic:equal>
					<logic:equal name="AddPocForm" property="deletemessage" value="-9002">
						<tr>
				  			<td  colspan="2" class="message" height="10">
				  				<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>
				  			</td>
				  		</tr>
					</logic:equal>
					<logic:equal name="AddPocForm" property="deletemessage" value="-1">
				  		<tr>
				  			<td  colspan="2" class="message" height="10">
				  				<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>
				  			</td>
				  		</tr>
					</logic:equal>
					<logic:equal name="AddPocForm" property="deletemessage" value="0">
				  		<tr>
				  			<td  colspan="2" class="message" height="10">
			  					<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.deletedsuccess"/>
				  			</td>
				  		</tr>
					</logic:equal>	
				</logic:present>
				<logic:present name="AddPocForm" property="addmessage">
					<logic:equal name="AddPocForm" property="addmessage" value="-9002">
						<tr>
							<td  colspan="2" class="message" height="10">		
								<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.insertfailed"/>
							</td>
						</tr>
					</logic:equal>
					<logic:equal name="AddPocForm" property="addmessage" value="-9003">
						<tr>
							<td  colspan="2" class="message" height="10">		
								<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>
							</td>
						</tr>
					</logic:equal>
					<logic:equal name="AddPocForm" property="addmessage" value="-1">
						<tr>
							<td  colspan="2" class="message" height="10">		
								<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.insertfailed"/>
							</td>
						</tr>
					</logic:equal>
				</logic:present>
				<logic:present name="AddPocForm" property="updatemessage">
					<logic:equal name="AddPocForm" property="updatemessage" value="-9005">
						<tr>
							<td colspan="2" class="message" height="10" >
								<bean:message bundle="CM" key="cm.updateerror"/>  
							</td>
						</tr>
					</logic:equal>
					<logic:equal name="AddPocForm" property="updatemessage" value="-9001">
						<tr>
							<td colspan="2" class="message" height="10" >
								<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>  
							</td>
						</tr>
					</logic:equal>
					<logic:equal name="AddPocForm" property="updatemessage" value="-1">
						<tr>
							<td colspan="2" class="message" height="10" >
								<bean:message bundle="CM" key="cm.updateerror"/>
							</td>
						</tr>
					</logic:equal>	
				</logic:present>
				<tr class="heading"> 
					<!--<bean:write name="AddPocForm" property="org_name"/>/<bean:write name="AddPocForm" property="division_name"/>-->
					<logic:equal name = "AddPocForm" property="lo_pc_id" value="0">
						<td colspan="2" class="labellobold"><bean:message bundle="CM" key="cm.addnewpoc"/></td>
					</logic:equal>
					<logic:notEqual name = "AddPocForm" property="lo_pc_id" value="0">
						<td colspan="2" class="labellobold"><bean:message bundle="CM" key="cm.updatepoc"/></td>
					</logic:notEqual>
				</tr>  
				<tr> 
					<td width="260" class="labelobold"><bean:message bundle="CM" key="<%=org_name%>"/></td>
					<td width="320" class="labelo"><bean:write name="AddPocForm" property="org_name"/></td>
				</tr>
				<tr> 
					<td class="labelebold"><bean:message bundle="CM" key="cm.divison/location"/></td>
					<td class="labele"><bean:write name="AddPocForm" property="division_name"/></td>
				</tr>
				<tr> 
					<td class="labelobold"><font class="red">* </font><bean:message bundle="CM" key="cm.firstname"/></td>
					<td class="labelo"><html:text styleClass="txtCtrlWide textbox" maxlength="50" property="firstname" /></td>
				</tr>
				<tr> 
					<td class="labelebold"><font class="red">* </font><bean:message bundle="CM" key="cm.lastname"/></td>
					<td class="labele"><html:text styleClass="txtCtrlWide textbox" maxlength="50" property="lastname" /></td>
				</tr>
				<c:choose>
		    		<c:when test="${AddPocForm.org_type eq 'N'}">
						 <tr> 
							<td class = "labelobold"><font class="red">* </font><bean:message bundle="CM" key="cm.department"/></td>
							<td class = "labelo">
							    <html:select  name = "AddPocForm" property="classificationId" size="1" styleClass="txtCtrlWide comboo" onchange="getRankList1();">
									<html:optionsCollection name = "dcClassification"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>   
							</td>
						</tr>
						<tr>
							<td class="labelebold"><font class="red">* </font><bean:message bundle="CM" key="cm.rank"/></td>
						    <td class = "labele">
								<html:select  name = "AddPocForm" property="rank" styleId="rank" size="1" styleClass="txtCtrlWide comboo">
									<html:option value="0">--Select--</html:option>
									<logic:iterate name="rankList" id="rankListItem">
										<html:option value="${rankListItem}">${rankListItem}</html:option>
									</logic:iterate>
								</html:select>
							</td>
						</tr>
					    <tr>
					    	<td class="labelobold"><font class="red">* </font><bean:message bundle="CM" key="cm.invoice.approval"/></td>
							<td class="labelo">
								<span class="checkboxspaninline" style="text-align: left;">
									<html:radio name="AddPocForm" property="invoiceApproval" value = "Approval Required"></html:radio>
									<span class="radiolabelspan"><bean:message bundle="CM" key="cm.invoice.appreq"/></span>
								</span>
								<span class="checkboxspaninline" style="text-align: left;">
									<html:radio name="AddPocForm" property="invoiceApproval" value="Approval Authority"></html:radio>
									<span class="radiolabelspan"><bean:message bundle="CM" key="cm.invoice.apprauth"/></span>
								</span>
							</td>
						</tr>
						<tr>
							<td class="labelebold"><font class="red">* </font><bean:message bundle="CM" key="cm.title"/></td>
							<td class="labele">
								<html:select property="title" size="1" styleClass="txtCtrlWide comboo">
									<html:optionsCollection name = "dcCNSPOCTitles"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>
							</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td class="labelobold"><bean:message bundle="CM" key="cm.title"/></td>
							<td class="labelo">
								<html:text  styleClass="txtCtrlWide textbox" maxlength="50" property="title" />
							</td>
						</tr>
					</c:otherwise>
				</c:choose>
		
				<%
				if(type == 'P' || type=='N') {
					label1 = "labelebold";
					label2 = "labele";
					label3 = "labelobold";
					label4 = "labelo";
				} else{
					label1 = "labelobold";
					label2 = "labelo";
					label3 = "labelebold";
					label4 = "labele";
				}
				%>
				
				<!-- new field -->
				<logic:match name = "AddPocForm" property ="org_type" value="P">
					<tr>
						<td class="<%=label1%>"><bean:message bundle = "CM" key = "cm.pvsContact" /></td>
						<td class="<%=label2%>">
							<logic:equal name="AddPocForm" property="pvsContact" value="P">Yes</logic:equal>
							<logic:equal name="AddPocForm" property="pvsContact" value="S">No</logic:equal>
						</td>
					</tr>
					<html:hidden property = "pvsContact" />
				</logic:match>
				<logic:notMatch name = "AddPocForm" property ="org_type" value="P">
					<html:hidden property="pvsContact" />
				</logic:notMatch>
				<logic:match name = "AddPocForm" property ="org_type" value="P">
					<html:hidden property = "ckboxUseDivison" />
				</logic:match>
				<!-- end -->
				
				<%
				if(org_type != null && !org_type.equals("P")) {
					%>
					<tr>
						<td class="<%=label3%>" colspan="2" style="padding: 5px;">
							<span class="checkboxspan" style="text-align: left;">
								<html:checkbox property="ckboxUseDivison"></html:checkbox>
								<span class="checkboxlabelspan"><bean:message bundle="CM" key="cm.usedivison/locationaddress"/></span>
							</span>
						</td>
					</tr>
					<%
				} else {
					label1 = "labelobold";
					label2 = "labelo";
					label3 = "labelebold";
					label4 = "labele";
				}
				%>
			</table>
		</div>
		
		<%
		if(org_type != null && !org_type.equals("P")) {
			String aDisplay = (String)request.getAttribute("a");
			if (aDisplay == "hidden") {
				aDisplay = "none";
			} else {
				aDisplay = "block";
			}
			%>
			<div id="a" style="display: <%=aDisplay %>;">
				<table width="603" border="0" cellspacing="1" cellpadding="1" class="ilex_cb_table"> 
					<tr> 
						<td width="260" class="<%=label1%>" height="25"><font class="red">* </font><bean:message bundle="CM" key="cm.firstaddressline"/></td>
						<td width="320" class="<%=label2%>">
							<html:text  styleClass="txtCtrlWide textbox" maxlength="50" property="firstaddressline" onblur="showAddress('address');" />
						</td>
					</tr>
		 				<tr>
		 					<td class="<%=label3%>"><bean:message bundle="CM" key="cm.secondaddressline"/></td>
		 					<td class="<%=label4%>">
		 						<html:text styleClass="txtCtrlWide textbox" maxlength="50" property="secondaddressline" />
						</td>
					</tr>
					<tr>
						<td class="<%=label1%>"><font class="red">* </font><bean:message bundle="CM" key="cm.city"/></td>
					    <td class="<%=label2%>">
					    	<html:text styleClass="txtCtrlWide textbox" maxlength="50" property="city" />
				    	</td>
			    	</tr>
			    	<tr>
			    		<td class="<%=label3%>"><bean:message bundle = "CM" key = "cm.state/province"/></td>
			    		<td class="<%=label4%>">
			    			<logic:present name="initialStateCategory">
			    				<html:select property = "stateSelect"  styleClass="txtCtrlWide comboo" onchange = "ChangeCountry();">
			    					<logic:iterate id="initialStateCategory" name="initialStateCategory" >
			    						<bean:define id="statecatname" name="initialStateCategory" property="siteCountryIdName" />
			    						<%
			    						if(!statecatname.equals("notShow")) {
			    							%>
											<optgroup class=labelebold label="<%=statecatname%>">
											<%
										}
										%>
										<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
										<%
										if(!statecatname.equals("notShow")) {
											%>
											</optgroup>
											<%
										}
										%>
									</logic:iterate>
								</html:select>
							</logic:present>
						</td>
					</tr>
					<tr>
						<td class="<%=label1%>"><font class="red">* </font><bean:message bundle="CM" key="cm.zip/postalcode"/></td>
					    <td class="<%=label2%>">
					    	<html:text styleClass="txtCtrlWide textbox" maxlength="50" property="zip" onblur="showAddress('zip');" />
				    	</td>
			    	</tr>
			    	<tr>
			    		<td class="<%=label3%>"><font class="red">* </font><bean:message bundle="CM" key="cm.country"/></td>
			    		<td class="<%=label4%>">
				        	<html:select property="country" size="1" styleClass="txtCtrlWide comboo" onchange = "getRefresh('country');">
				        		<html:optionsCollection name = "dcCountryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
			        		</html:select>
		        		</td>
		       		</tr>
		      			<c:choose>
		      				<c:when test="${AddPocForm.org_type  ne 'N'}">
		      					<tr>
		      						<td class="<%=label1%>">Time Zone</td>
		      						<td class="<%=label2%>">
		      						<html:select  name = "AddPocForm" property="timeZoneId" size="1" styleClass="txtCtrlWide comboe">
		      							<html:optionsCollection name = "dcTimeZoneList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
									</html:select>
								</td>
							</tr>
						</c:when>
					</c:choose>
				</table>
			</div>
			<%
		}
		String bDisplay = (String)request.getAttribute("b");
		if (bDisplay == "hidden") {
			bDisplay = "none";
		} else {
			bDisplay = "block";
		}
		%>
		<div id="b" style="display: <%=bDisplay %>;">
			<table width="603" border="0" cellspacing="1" cellpadding="1" class="ilex_cb_table"> 
			 	<tr> 
					<td width="260" class="<%=label1%>"><bean:message bundle="CM" key="cm.firstaddressline"/></td>
					<td width="320" class="<%=label2%>"><% if (((String[])request.getAttribute("division_address"))[1]!=null)out.println(((String[])request.getAttribute("division_address"))[1]); else out.println(""); %>	</td>
				</tr>
				<tr> 
					<td class="<%=label3%>"><bean:message bundle="CM" key="cm.secondaddressline"/></td>
					<td class="<%=label4%>"><% if (((String[])request.getAttribute("division_address"))[2]!=null)out.println(((String[])request.getAttribute("division_address"))[2]); else out.println(""); %>	</td>
				</tr>
				<tr> 
					<td class="<%=label1%>"><bean:message bundle="CM" key="cm.city"/></td>
					<td class="<%=label2%>"><% if (((String[])request.getAttribute("division_address"))[3]!=null)out.println(((String[])request.getAttribute("division_address"))[3]); else out.println(""); %>	</td>
				</tr>
				<tr>
					<td class="<%=label3%>"><bean:message bundle="CM" key="cm.state/province"/></td>
					<td class="<%=label4%>"><% if (((String[])request.getAttribute("division_address"))[4]!=null)out.println(((String[])request.getAttribute("division_address"))[4]); else out.println(""); %></td> 
			 	</tr>
				<tr> 
					<td class="<%=label1%>"><bean:message bundle="CM" key="cm.zip/postalcode"/></td>
					<td class="<%=label2%>"><% if (((String[])request.getAttribute("division_address"))[5]!=null)out.println(((String[])request.getAttribute("division_address"))[5]); else out.println(""); %></td>
				</tr>
				<tr> 
					<td class="<%=label3%>"><bean:message bundle="CM" key="cm.country"/></td>
					<td class="<%=label4%>"><% if (((String[])request.getAttribute("division_address"))[6]!=null)out.println(((String[])request.getAttribute("division_address"))[6]); else out.println(""); %></td>
				</tr>  
				<c:choose>
					<c:when test="${AddPocForm.org_type  ne 'N'}">
						<tr> 
							<td class="<%=label1%>">Time Zone</td>
							<td class="<%=label2%>"><% if (((String[])request.getAttribute("division_address"))[9]!=null)out.println(((String[])request.getAttribute("division_address"))[9]); else out.println(""); %></td>
						</tr>
					</c:when>
				</c:choose>
			</table>
		</div>
		
		<div id="bottom" style="display: block;">
			<%
			if(type=='N') {
				label1 = "labelobold";
				label2 = "labelo";
				label3 = "labelebold";
				label4 = "labele";
			}
			%>
			<table width="603" border="0" cellspacing="1" cellpadding="1" class="ilex_cb_table"> 
				<tr> 
					<td width="260" class="<%=label3%>"><font class="red">* </font><bean:message bundle="CM" key="cm.phone1"/></td>
					<td width="320" class="<%=label4%>">
						<html:text styleClass="txtCtrlWide textbox" maxlength="50" property="phone1" />
					</td>
				</tr>
				<tr> 
					<td class="<%=label1%>"><bean:message bundle="CM" key="cm.phone2"/></td>
					<td class="<%=label2%>">
			    		<html:text styleClass="txtCtrlWide textbox" maxlength="50" property="phone2" />
			   		</td>
				</tr>
				<tr> 
					<td class="<%=label3%>"><bean:message bundle="CM" key="cm.fax"/></td>
					<td class="<%=label4%>">
						<html:text  styleClass="txtCtrlWide textbox" maxlength="50" property="fax" />
					</td>
				</tr>
				<tr> 
					<td class="<%=label1%>"><bean:message bundle="CM" key="cm.email1"/><font class="red"> *</font></td>
					<td class="<%=label2%>">
						<html:text styleClass="txtCtrlWide textbox" maxlength="100" property="email1" onblur="return setuservalue();"/>
					</td>
				</tr>
				<tr>
					<td class="<%=label3%>"><bean:message bundle="CM" key="cm.email2"/></td>
					<td class="<%=label4%>">
						<html:text styleClass="txtCtrlWide textbox" maxlength="100" property="email2" />
					</td>
				</tr>
				<tr>
					<td class="<%=label1%>"><bean:message bundle="CM" key="cm.mobile"/></td>
					<td class="<%=label2%>">
						<html:text styleClass="txtCtrlWide textbox" maxlength="50" property="mobile" />
					</td>
				</tr>
				<%
				if(org_type != null && !org_type.equals("P")) {
					%> 
					<tr>
						<!--  Start: Added By Atul 16/01/2007 -->
						<td class="<%=label3%>"><bean:message bundle="CM" key="cm.username"/></td>
						<!--  End: Added By Atul 16/01/2007 -->
						<td class="<%=label4%>">
							<html:text styleClass="txtCtrlWide textbox" maxlength="50" property="user_name"  readonly="true"/>
						</td>
					</tr>
					<tr>
						<!--  Start: Added By Atul 16/01/2007 -->
						<td class="<%=label1%>"><bean:message bundle="CM" key="cm.password"/></td>
						<td class="<%=label2%>">
							<html:text styleClass="txtCtrlWide textbox" maxlength="20" property="password" />
						</td>
						<!--  End: Added By Atul 16/01/2007 -->
					</tr>
					<tr>
						<td class="<%=label3%>">Employment Status</td>
						<td class="<%=label4%>">
							<c:choose>
								<c:when test="${AddPocForm.lo_pc_id ne '0' && AddPocForm.empStatus eq '0'}">
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "1" disabled="true"></html:radio>
										<span class="radiolabelspan">Active</span>
									</span>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "0" disabled="true"></html:radio>
										<span class="radiolabelspan">Terminated</span>
									</span>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "2" disabled="true"></html:radio>
										<span class="radiolabelspan">Test</span>
									</span>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "3" disabled="true"></html:radio>
										<span class="radiolabelspan">Temp</span>
									</span>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "4" disabled="true"></html:radio>
										<span class="radiolabelspan">LOA</span>
									</span>
								</c:when>
								<c:otherwise>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "1"></html:radio>
										<span class="radiolabelspan">Active</span>
									</span>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "0"></html:radio>
										<span class="radiolabelspan">Terminated</span>
									</span>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "2"></html:radio>
										<span class="radiolabelspan">Test</span>
									</span>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "3"></html:radio>
										<span class="radiolabelspan">Temp</span>
									</span>
									<span class="checkboxspaninline" style="text-align: left;">
										<html:radio name="AddPocForm" property="empStatus" value = "4"></html:radio>
										<span class="radiolabelspan">LOA</span>
									</span>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<%
					if(type=='N') {
						label1 = "labelebold";
						label2 = "labele";
						label3 = "labelobold";
						label4 = "labelo";
					}
					%>
					<c:if test="${AddPocForm.org_type  ne 'N'}">
						<tr>
							<td class="<%=label1%>">Classification</td>
							<td class="<%=label2%>">
								<html:select  name = "AddPocForm" property="classificationId" size="1" styleClass="txtCtrlWide comboo">
									<html:optionsCollection name = "dcClassification"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>
							</td>
						</tr>
					</c:if>
					<tr>
						<td class="<%=label3%>" colspan="2" style="padding: 5px;">
						<span class="checkboxspan" style="text-align: left;">
							<html:checkbox property="role_assign"></html:checkbox>
							<span class="checkboxlabelspan"><bean:message bundle="CM" key="cm.assignrole"/></span>
						</span>
					</tr>
					<%
				} else {
					%>
					<html:hidden property="user_name" />
					<html:hidden property="password" />
					<%
				}
				%>
			</table>
		</div>
		<%
		if(org_type != null && !org_type.equals("P")) {
			String cDisplay = (String)request.getAttribute("c");
			if (cDisplay == "hidden") {
				cDisplay = "none";
			} else {
				cDisplay = "block";
			}
			%>
			<div id="c" style="display: <%=cDisplay %>;">
				<table border = "0" cellspacing = "1" cellpadding = "1" style="table-layout: fixed; width: 603px;" class="ilex_cb_table"> 
					<%
					int i = 0; 
					String label = "";
					String label5 = "";
					if(type == 'P') {
						label5 = "labelleboldblue";
					} else {
						label5 = "labellobold";
					}
					
					boolean showC2 = false;
					boolean showC3 = false;
					boolean showC4 = false;
					boolean showC5 = false;
					boolean showC6 = false;
					boolean showC7 = false;
					
					int cols = 0;
					%>
					<logic:present name = "RoleList" scope="request"> 
						<logic:iterate id = "rl" name = "RoleList">
							<%
							if (i==0) {
								%>
		 						<tr>
		 	 	    				<logic:notEqual name = "rl" property = "c2" value="">
		 	 	    					<%
		 	 	    					showC2 = true;
		 	 	    					cols += 4;
		 	 	    					%>	 					
		 	 							<td colspan = "4" class = "<%=label5%> ">
		 	 								<bean:write name = "rl" property = "c2"/>
		 								</td>
									</logic:notEqual>
									<logic:notEqual name = "rl" property = "c3" value="">
		 	 	    					<%
		 	 	    					showC3 = true;
		 	 	    					cols += 4;
		 	 	    					%>	 	
										<td colspan = "4" class = "<%=label5%>">
											<bean:write name = "rl" property = "c3"/>
										</td>
									</logic:notEqual>
									<logic:notEqual name = "rl" property = "c4" value="">
		 	 	    					<%
		 	 	    					showC4 = true;
		 	 	    					cols += 4;
		 	 	    					%>	 	
										<td colspan = "4" class = "<%=label5%> ">
											<bean:write name = "rl" property = "c4"/>
										</td>
									</logic:notEqual>
									<logic:notEqual name = "rl" property = "c5" value="">
		 	 	    					<%
		 	 	    					showC5 = true;
		 	 	    					cols += 4;
		 	 	    					%>	 	
										<td colspan = "4" class = "<%=label5%> ">
											<bean:write name = "rl" property = "c5"/>
										</td>
									</logic:notEqual>
									<logic:notEqual name = "rl" property = "c6" value="">
		 	 	    					<%
		 	 	    					showC6 = true;
		 	 	    					cols += 4;
		 	 	    					%>	 	
										<td colspan = "4" class = "<%=label5%> ">
											<bean:write name = "rl" property = "c6"/>
										</td>
									</logic:notEqual>
									<logic:notEqual name = "rl" property = "c7" value="">
		 	 	    					<%
		 	 	    					showC7 = true;
		 	 	    					cols += 4;
		 	 	    					%>	 	
										<td colspan = "4" class = "<%=label5%> ">
											<bean:write name = "rl" property = "c7"/>
										</td>
									</logic:notEqual>
								</tr>
			 	 				<%
		 	 				} else {
		 	 					if(type == 'P') {
									if(i%2==0) label  = "labele";
									if(i%2==1) label  = "labelo";
								} else {
									if(i%2==0) label  = "labelo";
									if(i%2==1) label  = "labele";
								}
								%>
		 	 					<tr>
		 	 						<%
		 							if(showC2) {
		 	 							%>
			 	 						<logic:notEqual name = "rl" property = "c2" value="">
			 	 							<td colspan = "3" class = "<%= label %>">
			 	 								<bean:write name = "rl" property = "c2"/>
											</td>
										</logic:notEqual>
										<logic:notEqual name = "rl" property = "c2id" value="">
											<td colspan = "1" class = "<%= label %>">
												<html:multibox property = "check">
													<bean:write name = "rl" property = "c2id"/>
												</html:multibox>
											</td>
										</logic:notEqual>
										<logic:equal name="rl" property = "c2" value=""> 
											<td colspan = "4" class = "<%= label %>">&nbsp;</td>  
										</logic:equal>
										<%
									}
		 	 						if(showC3) {
		 	 							%>
										<logic:notEqual name = "rl" property = "c3" value="">
											<td colspan = "3" class = "<%= label %>">
												<bean:write name = "rl" property = "c3"/>
											</td>
										</logic:notEqual>
										<logic:notEqual name = "rl" property = "c3id" value="">
											<td colspan = "1" class = "<%= label %>">
												<html:multibox property = "check">
													<bean:write name = "rl" property = "c3id"/>
												</html:multibox>
											</td>
										</logic:notEqual>
										<logic:equal name="rl" property = "c3" value=""> 
											<td colspan = "4" class = "<%= label %>">&nbsp;</td>  
										</logic:equal>
										<%
									}
		 	 						if(showC4) {
		 	 							%>
										<logic:notEqual name = "rl" property = "c4" value="">
											<td colspan = "3" class = "<%= label %>">
												<bean:write name = "rl" property = "c4"/>
											</td>
										</logic:notEqual>
										<logic:notEqual name = "rl" property = "c4id" value="">
											<td colspan = "1" class = "<%= label %>">
												<html:multibox property = "check">
													<bean:write name = "rl" property = "c4id"/>
												</html:multibox>
											</td>
										</logic:notEqual>
										<logic:equal name="rl" property = "c4" value=""> 
											<td colspan = "4" class = "<%= label %>">&nbsp;</td>  
										</logic:equal>
										<%
									}
		 	 						if(showC5) {
		 	 							%>
										<logic:notEqual name = "rl" property = "c5" value="">
											<td colspan = "3" class = "<%= label %>">
												<bean:write name = "rl" property = "c5"/>
											</td>
										</logic:notEqual>
										<logic:notEqual name = "rl" property = "c5id" value="">
											<td colspan = "1" class = "<%= label %>">
												<html:multibox property = "check">
													<bean:write name = "rl" property = "c5id"/>
												</html:multibox>
											</td>
										</logic:notEqual>
										<logic:equal name="rl" property = "c5" value=""> 
											<td colspan = "4" class = "<%= label %>">&nbsp;</td>  
										</logic:equal>
										<%
									}
		 	 						if(showC6) {
		 	 							%>
										<logic:notEqual name = "rl" property = "c6" value="">
											<td colspan = "3" class = "<%= label %>">
												<bean:write name = "rl" property = "c6"/>
											</td>
										</logic:notEqual>
										<logic:notEqual name = "rl" property = "c6id" value="">
											<td colspan = "1" class = "<%= label %>">
												<html:multibox property = "check">
													<bean:write name = "rl" property = "c6id"/>
												</html:multibox>
											</td>
										</logic:notEqual>
										<logic:equal name="rl" property = "c6" value=""> 
											<td colspan = "4" class = "<%= label %>">&nbsp;</td>  
										</logic:equal>
										<%
									}
		 	 						if(showC7) {
		 	 							%>
										<logic:notEqual name = "rl" property = "c7" value="">
											<td colspan = "3" class = "<%= label %>">
												<bean:write name = "rl" property = "c7"/>
											</td>
										</logic:notEqual>
										<logic:notEqual name = "rl" property = "c7id" value="">
											<td colspan = "1" class = "<%= label %>">
												<html:multibox property = "check">
													<bean:write name = "rl" property = "c7id"/>
												</html:multibox>
											</td>
										</logic:notEqual>
										<logic:equal name="rl" property = "c7" value=""> 
											<td colspan = "4" class = "<%= label %>">&nbsp;</td>  
										</logic:equal>
										<%
									}
									%>
								</tr>
			 	 				<%
		 	 				} 
		 	 				i++; 
		 	 				%>
						</logic:iterate>
					</logic:present>
					<tr> 
						<td colspan="<%= cols %>" class="ButtonRow" style="background-color: #cecec7; height: 26px;">
							<div class="submitUnit">
								<html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="CM" key="cm.save"/></html:submit>
								<html:reset property="reset" styleClass="button"><bean:message bundle="CM" key="cm.reset"/></html:reset>
							</div>
						</td>
					</tr>
				</table>
			</div>
		<%
		}
		String dDisplay = (String)request.getAttribute("d");
		if (dDisplay == "hidden") {
			dDisplay = "none";
		} else {
			dDisplay = "block";
		}
		%>
		<div id="d" style="display: <%=dDisplay %>;">
			<table border = "0" cellspacing = "1" cellpadding = "1" width="600" class="ilex_cb_table">
				<tr>
					<td colspan="2" class="ButtonRow" style="background-color: #cecec7; height: 26px;">
						<div class="submitUnit">
							<html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="CM" key="cm.save"/></html:submit>
							<html:reset property="reset" styleClass="button"><bean:message bundle="CM" key="cm.reset"/></html:reset>
						</div>
					</td>
				</tr>
			</table>
		</div>
		  
		<logic:present name = "refreshtree" scope = "request">
			<script>
				parent.ilexleft.location.reload();
			</script>
		</logic:present>
	</html:form>
</div>
</BODY>
<script>

function setSearch()
{
	trimFields();
	if(isBlank(document.forms[0].type.value)&& isBlank(document.forms[0].division.value)&& isBlank(document.forms[0].poc.value)){
		if(! chkBlank(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;
	}
	if(! CheckQuotes(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;	
	if(! CheckQuotes(document.forms[0].division,"<bean:message bundle="CM" key="cm.searchdivision"/>"))return false;
	if(! CheckQuotes(document.forms[0].poc,"<bean:message bundle="CM" key="cm.searchpoc"/>"))return false;
	document.forms[0].action = "Search.do?org=<%=type%>&type="+document.forms[0].type.value+"&division="+document.forms[0].division.value;				
	document.forms[0].submit();								
	return true;	
}

function setValue()
{	
	document.forms[0].refresh.value = "true";
	document.forms[0].submit();
	return true;
}
function setuservalue()
{	
	    var email1=document.forms[0].email1.value;
		var index=email1.indexOf("@");
		var localPart=email1.slice(0,index);
	document.forms[0].user_name.value =localPart;
	return true;
}


function validate()
{
	trimFields();
	if(isBlank(document.forms[0].firstname.value))
	{	
		alert("<bean:message bundle="CM" key="cm.firstnameblank"/>");
		document.forms[0].firstname.focus();		
		return false;
	}
	


	if(isBlank(document.forms[0].lastname.value))
	{
		alert("<bean:message bundle="CM" key="cm.lastnameblank"/>");		
		document.forms[0].lastname.focus();
		return false;
	}
	if(document.forms[0].org_type.value=='N'){
	if(! chkCombo(document.forms[0].classificationId,"<bean:message bundle="CM" key="cm.department"/>"))return false;		
	if(! chkCombo(document.forms[0].rank,"<bean:message bundle="CM" key="cm.rank"/>"))return false;	
	if(! chkCombo(document.forms[0].title,"<bean:message bundle="CM" key="cm.title"/>"))return false;	
	}
	
	if(document.forms[0].ckboxUseDivison.value != "false"){
	if(!document.forms[0].ckboxUseDivison.checked)
	{
		if(isBlank(document.forms[0].firstaddressline.value))
		{
			errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
			warningEmpty(document.forms[0].firstaddressline,"<bean:message bundle="CM" key="cm.firstaddressline"/>",errormsg);		
			return false;
		}
		else
		{
			if(! CheckQuotes(document.forms[0].firstaddressline,"<bean:message bundle="CM" key="cm.firstaddressline"/>"))
			return false;
		}
		
		if(isBlank(document.forms[0].city.value))
		{
			errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
			warningEmpty(document.forms[0].city,"<bean:message bundle="CM" key="cm.city"/>",errormsg);		
			return false;
		}
		else
		{
			if(! CheckQuotes(document.forms[0].city,"<bean:message bundle="CM" key="cm.city"/>"))
			return false;
		}
		
		if(!isAlphabetic(trimBetweenTrail(document.forms[0].city.value)))
		{		
			alert("<bean:message bundle="CM" key="cm.cityalpha"/>");
			document.forms[0].city.focus();		
			return false;
		}	
		
		if(! chkCombo(document.forms[0].stateSelect,"<bean:message bundle="CM" key="cm.state/province"/>"))return false;						
		
		if(isBlank(document.forms[0].zip.value) && document.forms[0].country.value == 'US')
		{
			errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
			warningEmpty(document.forms[0].zip,"<bean:message bundle="CM" key="cm.zip/postal"/>",errormsg);		
			return false;
		}
		else
		{
			if(! CheckQuotes(document.forms[0].zip,"<bean:message bundle="CM" key="cm.zip/postal"/>"))
			return false;
		}

		if(isBlank(document.forms[0].country.value))
		{
			errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
			warningEmpty(document.forms[0].country,"<bean:message bundle="CM" key="cm.country"/>",errormsg);		
			return false;
		}	
		else
		{
			if(! CheckQuotes(document.forms[0].country,"<bean:message bundle="CM" key="cm.country"/>"))
			return false;
		}
		
	
	
		if(!isAlphabetic(trimBetweenTrail(document.forms[0].country.value)))
		{		
			alert("<bean:message bundle="CM" key="cm.countryalpha"/>");
			document.forms[0].country.focus()			
			return false;
		}	
	}
}	
	if(isBlank(document.forms[0].phone1.value))
	{
		alert("<bean:message bundle="CM" key="cm.phone1blank"/>");		
		document.forms[0].phone1.focus();
		return false;
	}
	//else
	//{
	//	if(! CheckQuotes(document.forms[0].phone1,"<bean:message bundle="CM" key="cm.phone1"/>"))
	//	return false;
	//}
	
	//if(! CheckQuotes(document.forms[0].phone2,"<bean:message bundle="CM" key="cm.phone2"/>"))return false;
	if(! CheckQuotes(document.forms[0].mobile,"<bean:message bundle="CM" key="cm.mobile"/>"))return false;
	
	if(isBlank(document.forms[0].email1.value))
	{
		alert("<bean:message bundle="CM" key="cm.email1blank"/>");		
		document.forms[0].email1.focus();
		return false;
	}	
	else
	{
		if(! CheckQuotes(document.forms[0].email1,"<bean:message bundle="CM" key="cm.email1"/>"))
		return false;
	}

	//if(!isInteger(removeHiffen(document.forms[0].fax.value))&&(!isBlank(document.forms[0].fax.value)))
	//{
		//alert("<bean:message bundle="CM" key="cm.faxinteger"/>");		
		//document.forms[0].fax.focus();
		//return false;
	//}		
	if(!isEmail(document.forms[0].email1.value) || (!checkEmailSpaces(document.forms[0].email1.value)))
	{
		alert("<bean:message bundle="CM" key="cm.email1invalid"/>");		
		document.forms[0].email1.focus();
		return false;
	}
	if(!isBlank(document.forms[0].email2.value) && ((!checkEmailSpaces(document.forms[0].email2.value)) ||(!isEmail(document.forms[0].email2.value))) )
	{		alert("<bean:message bundle="CM" key="cm.email2invalid"/>");		
			document.forms[0].email2.focus();
			return false;
	} 
	
	if(! CheckQuotes(document.forms[0].email2,"<bean:message bundle="CM" key="cm.email2"/>"))
	return false;
	
	if(! CheckQuotes(document.forms[0].user_name,"<bean:message bundle="CM" key="cm.username"/>"))
		return false;
	
	if(! CheckQuotes(document.forms[0].password,"<bean:message bundle="CM" key="cm.password"/>"))
		return false;

	document.forms[0].empStatus[0].disabled = false;
	document.forms[0].empStatus[1].disabled = false;
	
	return true;// CheckQuotes ();
	
}

</script>
<script>

function trimBetweenTrail(str){
nstr="";
for (var i=0;i<str.length;i++){
	if(str.substring(i,i+1)!=" ")
	nstr=nstr+str.substring(i,i+1);
	}
	return nstr;

}

function removeHiffen(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!="-")	nstr=nstr+str.substring(i,i+1);
	}// END OF FOR
	return nstr;
}

function trimFields()
{
	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script language="JavaScript" src="javascript/jquery-1.11.0.min.js"></script>
<script>
$(document).ready(function() {
	$('input[name="role_assign"]').click(function() {
	    if($(this).is(':checked')) {
			$('#d').hide();
			$('#c').show();
			console.log('showing role assign div');
		} else{
			$('#c').hide();
			$('#d').show();
			console.log('hiding role assign div');
		}
	});

	$('input[name="ckboxUseDivison"]').click(function() {
	    if($(this).is(':checked')) {
			$('#a').hide();
			$('#b').show();
			console.log('showing division selection div');
		} else {
			$('#b').hide();
			$('#a').show();
			console.log('hiding division selection div');
		}
	});
});
</script>

<script>



function MM_showHideLayers() { //v3.0

  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }

}


function del() 
{
		if(!(isBlank(trim('<bean:write name="AddPocForm" property="firstname"/>')) && isBlank(trim('<bean:write name="AddPocForm" property="lastname"/>'))) )
		{
			if(document.forms[0].pvsContact.value=='P') {
					alert("<bean:message bundle="CM" key="cm.pvsContact.primarydelete"/>"); 
			} else {
					var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
					var convel = confirm( msg );
					if(convel)
					{
						document.forms[0].refresh.value = "false";
						document.forms[0].action = "AddPoc.do?function=delete&org_type=<%=type%>&org_discipline_id=<bean:write name="AddPocForm" property="org_discipline_id"/>&division_id=<bean:write name="AddPocForm" property="division_id"/>&lo_pc_id=<bean:write name="AddPocForm" property="lo_pc_id"/>";
						document.forms[0].submit();	
						return true;
					}
			}
		}
		else
		{
			alert( "<bean:message bundle="RM" key="rm.selectdeletion"/>" );
		}

}
function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}

	return true;
}
function checkEmailSpaces(obj) {
	if(!isEmpty(obj)) {
		if(isWhitespace(obj)) {
			return false;
		}
	}
	return true;
}
function chkCombo(obj,label){
	if(obj.value==0)	{	
		alert("Please select "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function getRefresh(type)
{
	document.forms[0].refreshState.value = "true";
    document.forms[0].refreshType.value = type;
	document.forms[0].submit();
	return true;
}
function getRankList1()
{
	var departmentId=document.forms[0].classificationId.value;
	$.ajax({
		  url: "AddPoc.do?function=getRankList&id="+departmentId,
		  context: document.body,
		  success: function(data){
			  var x = eval('(' + data + ')');
			  var options = '';
			  options= '<option value="0">--Select--</option>';
			  for (var i = 0; i <x.length; i++) {
					options += '<option value="' + x[i] + '">' + x[i] + '</option>';
				}
				$("#rank").html(options);
				
		  }
		});
	
	
	}

</script>
</html:html>

