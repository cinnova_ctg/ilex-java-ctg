<!DOCTYPE HTML>
<%@ page import = "com.mind.common.*" %>
<%@ page import = "com.mind.common.dao.*" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<bean:define id = "currState" name = "currState" scope = "request"/>
<bean:define id = "expansionState" name = "expansionState" scope = "request"/>
<bean:define id = "element" name = "element" scope = "request"/>
<bean:define id = "controller" name = "controller" scope = "request"/>
<html:html>

<%
	boolean temp = false;
%>
<head>


<%@ include file = "../Header.inc" %>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<title>Hierarchy Navigation</title>
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 10px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 2px;
    	white-space : nowrap;
    }

    /* Link Styles */
    a  {
    	color : #000000;
    }

    a:link  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:active  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:hover  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:visited  {
    	text-decoration : underline;
    	color : #000000;
    }
    
    .m2 {
        font-family : "Verdana";
        font-size : 10px;
        line-height: 10px;
    }
    
        
</style>

<script>
function searchfunction()
{
	trimFields();
	if(document.forms[0].searchtext.value == '')
	{		
		alert("Please enter value in search field.");
		document.forms[0].searchtext.focus()			
		return false;
	}
	
	var searchcheck1 = '';
	var searchcheck2 = '';

	var str = 'true';
	var str1 = 'null';
	if( document.forms[0].searchradio[0].checked == true )
		document.cookie = "searchcheck1cm="+str+";";
	else
		document.cookie = "searchcheck1cm="+str1+";";
	
	if( document.forms[0].searchradio[1].checked == true )
		document.cookie = "searchcheck2cm="+str+";";
	else
		document.cookie = "searchcheck2cm="+str1+";";
	
	document.cookie = "searchcm="+document.forms[0].searchtext.value+";";
	
	if( document.forms[0].searchradio[0].checked == true )
		searchcheck1 = document.forms[0].searchradio[0].value;
	
	
	if( document.forms[0].searchradio[1].checked == true )
	{
		searchcheck2 = document.forms[0].searchradio[1].value;
	}
	
	if( (!document.forms[0].searchradio[0].checked ) && (!document.forms[0].searchradio[1].checked ) && ( document.forms[0].searchtext.value == '' ) )
	{
		
		var str = '';
		str = 'C';
		document.cookie = "searchcheck1cm="+str+";";
	
		str = 'P';
		document.cookie = "searchcheck2cm="+str+";";
	
		str = '';
		document.cookie = "searchcm="+str+";";
		
		
		searchcheck1 = 'C';
		searchcheck2 = 'P';
		
	}
		document.forms[0].searchcheck1.value = searchcheck1;	
		document.forms[0].searchcheck2.value = searchcheck2;	
		document.forms[0].action = 'MenuTreeAction.do?menucriteria=CM&searchcheck1='+searchcheck1+'&searchcheck2='+searchcheck2+'&searchtext='+document.forms[0].searchtext.value;
	
	return true;
}


function setdefault()
{
	
	var tcookie = document.cookie;
	var st1 = tcookie.indexOf ("searchcheck1cm=");
	var cookievalue1;
	if( st1 != -1 )
	{
		st1 = st1+14;
		st1 = tcookie.indexOf ( "=" , st1);
      	var en = tcookie.indexOf ( ";" , st1);
      	if (en > st1)
        {
          cookievalue1 = tcookie.substring(st1+1, en);
        }
        
        if( cookievalue1 == 'null' )
        	cookievalue1 = '';
        
       if( cookievalue1 == 'true' )
       		document.forms[0].searchradio[0].checked = 'true';
        
	}
	else
		document.forms[0].searchradio[0].checked = 'true';
	
	
	var st2 = tcookie.indexOf ( "searchcheck2cm=" )
	var cookievalue2;
	
	if( st2 != -1 )
	{
		st2 = st2+14;
		st2 = tcookie.indexOf ( "=" , st2);
      	var en = tcookie.indexOf ( ";" , st2);
      	if (en > st2)
        {
          cookievalue2 = tcookie.substring(st2+1, en);
        }
        
        if( cookievalue2 == 'null' )
        	cookievalue2 = '';
        	
       
       if( cookievalue2 == 'true' )
       		document.forms[0].searchradio[1].checked = 'true';   
	}
	
	
	
	var st3 = tcookie.indexOf ( "searchcm=" )
	var cookievalue;
	if( st3 != -1 )
	{
		st3 = st3+8;
		st3 = tcookie.indexOf ( "=" , st3);
      	var en = tcookie.indexOf ( ";" , st3);
      	if (en > st3)
        {
          cookievalue3 = tcookie.substring(st3+1, en);
        }
        
        if( cookievalue3 == 'null' )
        	cookievalue3 = '';
        	
        document.forms[0].searchtext.value = cookievalue3;
        
	}
	else
		document.forms[0].searchtext.value = '';
	
	document.forms[0].searchtext.focus();
	
	
	var p = false;
	
	var tableimg = document.getElementsByTagName('img');
	
	if( document.forms[0].searchtext.value != '' )
	{
		for (var k = 0; k < tableimg.length; k++) 
		{
			if( tableimg[k].src.indexOf( "images/rm_materials.jpg" )!= -1 || tableimg[k].src.indexOf( "images/red_folder.gif" )!= -1   ) 
			{
				p = true;
				break;
			}
		}
	}
	else
	{
		p = true;
	}
	
	
	if( !p )
	{
		
		document.getElementById('content3').style.visibility = 'visible';
		document.getElementById('content3').style.display = 'block';
	}
	else
	{
		document.getElementById('content3').style.visibility = 'hidden';
		document.getElementById('content3').style.display = 'none';
	}
	
	
	return true;
	
	
		
}


function showall()
{
	var str = '';
	str = 'C';
	document.cookie = "searchcheck1cm="+str+";";
	
	str = 'P';
	document.cookie = "searchcheck2cm="+str+";";
	
	str = '';
	document.cookie = "searchcm="+str+";";
	
	document.forms[0].searchcheck1.value = 'C';	
	document.forms[0].searchcheck2.value = 'P';
	document.forms[0].searchtext.value = '';
	document.forms[0].action = 'MenuTreeAction.do?menucriteria=CM&searchcheck1=C&searchcheck2=P&searchtext=';
	document.forms[0].submit();
	return true;
}

function hideall()
{

	var str = 'null';
	
	document.cookie = "searchcheck1cm="+str+";";

	document.cookie = "searchcheck2cm="+str+";";
	
	str = '';
	document.cookie = "searchcm="+str+";";
	
	document.forms[0].searchcheck1.value = '';	
	document.forms[0].searchcheck2.value = '';
	document.forms[0].searchtext.value = '';
	document.forms[0].action = 'MenuTreeAction.do?menucriteria=CM&searchcheck1=&searchcheck2=&searchtext=';
	document.forms[0].submit();
	return true;
}
</script>


<script language="JavaScript" src="javascript/ce_menu.js" type="text/javascript"></script>

</head>

<body text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/sidebg.jpg" onload="<% if( session.getAttribute( "firsttimeCM" ) != null ) { session.setAttribute( "firsttimeCM" , null ); }else{ %>setdefault();<%} %>MM_preloadImages('images/allthree.gif','images/rm_materials.jpg','images/allblue.gif','images/twoblue.gif','images/blue.gif','images/rm_mfg.gif','images/allyellow.gif','images/twoyellow.gif','images/yellow.gif','images/red_folder.gif','images/allred.gif','images/twored.gif','images/red.gif')">



<%
MenuElement me = ((MenuTree)(element)).getRootElement();
MenuController mc =((MenuTree)(element)).getMenuController();

response.getWriter().print("<div style='visibility:hidden;' id='content1'><table><form method=gett><input type='hidden' name='menucriteria' value='CM'><input type='hidden' name='searchcheck1' value=''><input type='hidden' name='searchcheck2' value=''><tr><td class = 'm2'><b>Name:</b>&nbsp;<input type = 'text' name = 'searchtext' size = '16' class = 'textbox' /></td><td><input name='go' type='submit' value = 'Search' class = 'button' onClick = 'return searchfunction();' /></td></tr></div>"); 
response.getWriter().print("<div style='visibility:hidden;' id='content2'><tr><td colspan = '2' class = 'm2'><b>Search Type:</b>&nbsp;&nbsp;&nbsp;<input  type = 'radio' name = 'searchradio' value = 'C'  />&nbsp;Customer&nbsp;&nbsp;<input  type = 'radio' name = 'searchradio' value = 'P' />&nbsp;PVS</tr></form></table></div>"); 

response.getWriter().print("<div style='visibility:hidden;display:none;' id='content3'><table><tr><td colspan = '2'><img name = 'Image1' src='images/2.gif' border='0' width='11' height='10'/>&nbsp;<span class = 'm2'>No records match the search criteria.</span></td></tr></table></div>"); 


response.getWriter().print("<div style='visibility:hidden;' id='content4'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class = 'm2' href='javascript:hideall();'>Clear Search</a></div>"); 

me.drawDirect(0,mc,response.getWriter());

%>
<script language="JavaScript">

document.getElementById('content1').style.visibility = 'visible';
document.getElementById('content2').style.visibility = 'visible';
document.getElementById('content4').style.visibility = 'visible';


document.getElementById('line0').className = 'm2';
document.getElementById('line0').style.visibility = 'visible';

currState="<%=((MenuTree)(element)).getCurrState()%>";
expansionState="<%=expansionState%>";
initNavigatorServerCheckCookie('CM');

</script>
<script>
function trimFields() {
	var field=document.forms[0];
	for(var i=0; i<field.length; i++) {
		if(field[i].type=='text') {
			var temp1=field[i].value;
			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}
</script>


</body>

</html:html>