<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%
String menu_label="";
String menu_label2="";
int comboflag=0;
int flag=0;
char type=((String)request.getAttribute("org")).trim().charAt(0);
switch(type){
case 'C':
{
	menu_label="cm.addnewtopcustomer";
	menu_label2="cm.deletenewtopcustomer";
	break;
}
case 'N':
{
	menu_label="cm.addnewtopCNS";
	menu_label2="cm.deletenewtopCNS";
	break;
}
case 'P':
{
	//menu_label="cm.adddivision"; change made by Seema-15th nov 2006
	menu_label="cm.addnewtopPVS";
	menu_label2="cm.deletenewtopPVS";
	
	break;
}
	default:
	{
		menu_label="rm.maddmanufacturers";
		menu_label2="rm.mdeletemanufacturers";
	}
}
%>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>

<title></title>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>

<%@ include  file="/Menu.inc" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table border="0" cellspacing="1" cellpadding="0">
        <tr align="center">
           <logic:match name = "SearchForm" property ="org" value="C">  
           		<td width=200 align=left class="toprow1"><a href="AddTopLevel.do?function=add&org_type=<%=type%>&type=type" onmouseout="MM_swapImgRestore();" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/></a></td>
           </logic:match>
		
		   <!--  Changes mage by Seema-15th Nov 2006     -->
           <logic:match name = "SearchForm" property ="org" value="P">  
           		<!-- <td width=200 align=left class="toprow1"><a href="AddTopLevel.do?function=add&org_type=<%=type%>&type=type" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/></a></td>-->
                     
	           <logic:present name = "divPage" scope = "request">
	           		<!-- <td   width=200 align=left class="toprow1"><a href="AddDivison.do?function=add&type=type&org_type=<%=type%>&org_discipline_id=<%=((Integer)request.getAttribute("customer_id")).intValue()%>"     onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/></a></td> -->
	           		<%--<td   width=200 align=left class="toprow1"><a href="AddDivison.do?function=add&type=type&org_type=<%=type%>&org_discipline_id=<%=((Integer)request.getAttribute("customer_id")).intValue()%>"     onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="cm.add"/> <bean:write name="SearchForm" property="searchtype"/></a></td>--%>
	           		<td   width=200 align=left class="toprow1"><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=<bean:write name="SearchForm" property="searchtype"/>"  onmouseout="MM_swapImgRestore();"     onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="cm.add"/> <bean:write name="SearchForm" property="searchtype"/></a></td>
	           </logic:present>
	           <logic:notPresent name = "divPage" scope = "request">
	           		<!-- <td width=200 align=left class="toprow1"><a href="AddTopLevel.do?function=add&org_type=<%=type%>&type=type" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/></a></td>  -->
	           		<%--<td width=200 align=left class="toprow1"><a href="AddDivison.do?function=add&type=type&org_type=<%=type%>&org_discipline_id=<bean:write name="SearchForm" property="certifiedId"/>" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">Add Certified Partners</a></td>
	           		<td width=200 align=left class="toprow1"><a href="AddDivison.do?function=add&type=type&org_type=<%=type%>&org_discipline_id=<bean:write name="SearchForm" property="minutemanId"/>" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">Add Minuteman Partners</a></td>--%>
	           		<td width=200 align=left class="toprow1"><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">Add Certified Partners</a></td>
	           		<td width=200 align=left class="toprow1"><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop">Add Minuteman Partners</a></td>
	           </logic:notPresent>
           
           </logic:match>
        
          <logic:match name = "SearchForm" property ="org" value="N">
          		<td width=200 align=left class="toprow1"><a href="AddDivison.do?function=add&org_type=<%=type%>&org_discipline_id=<bean:write name="SearchForm" property="org_discipline_id"/>&type=type"     onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/></a></td>
          </logic:match> 
          <td width = "400" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
         </tr>
      </table>
    </td>
  </tr>
</table>
</head>
<html:form action="Search" >
<html:hidden property="org"/>
<html:hidden property="searchtype"/>
<html:hidden property="org_discipline_id"/>
<table border="0" cellspacing="1" cellpadding="1" >
<tr>
<td  width="1" height="0"></td>
  <td>
  <table border="0" cellspacing="1" cellpadding="1" width=100% > 
 	 <tr>
	  	<td class="labeleboldwhite">
	  		Search <bean:write name="SearchForm" property="searchtype"/>
	  	</td>
  </tr>
  
  	<logic:match name = "SearchForm" property ="org" value="P">
  	<tr>
		<td class="dblabel" colspan="1">
			<!--<bean:message bundle="CM" key="cm.search1.pvsname"/>:&nbsp;&nbsp;&nbsp;<html:text styleClass="textbox" size="20"  maxlength="50" property="division" />-->
			<bean:message bundle="CM" key="cm.search1.name"/>:&nbsp;&nbsp;&nbsp;<html:text styleClass="textbox" size="20"  maxlength="50" property="division" />
			<html:hidden property="type"/>
		</td>
		<td class="labeleboldwhite">&nbsp;<html:button property="search" styleClass="button" onclick="return setSearch();"><bean:message bundle="CM" key="cm.search"/></html:button></td>
	</tr>	
  	</logic:match>

  	<logic:notMatch name = "SearchForm" property ="org" value="P">
  	<tr class="labelobold">
  	  	<td colspan="1"><bean:write name="SearchForm" property="searchtype"/></td>
		<td colspan="1"><bean:message bundle="CM" key="cm.searchdivision"/></td>
	</tr>
	<tr>
		<td><html:text styleClass="textbox" size="20"  maxlength="50" property="type" /></td>	
		<td><html:text  styleClass="textbox" size="20"  maxlength="50" property="division" /></td>
		<td class="labeleboldwhite">&nbsp;<html:button property="search" styleClass="button" onclick="return setSearch();"><bean:message bundle="CM" key="cm.search"/></html:button></td>
	</tr>
  	</logic:notMatch>
  </table>
  </td>
</tr>
</table>


<% int i=0;String cls=""; %>
<table border="0" cellspacing="1" cellpadding="1" >
<tr>
<td width="1" height="0"></td>
  <td>
  <table border="0" cellspacing="1" cellpadding="1"> 
<!-------------------------------------  Search Result------------------------------->  
  <logic:notPresent name="viewPocResult" scope="request">
  
  <logic:notPresent name="searchResult" scope="request">
  	<logic:notPresent name="firsttime" scope="request">
	  <tr>
  			<td class="message" height="10"><bean:message bundle="CM" key="cm.search1.nomatchingresults"/></td>
	  </tr>
  </logic:notPresent>
  </logic:notPresent>

    <logic:present name="searchResult" scope="request">
	  <tr>
	  	<td class="message" colspan="5"><bean:message bundle="CM" key="cm.search1.searchresults"/></td>
	  </tr>
  
  	  <tr class="tryb">
		<logic:match name = "SearchForm" property ="org" value="P">
			<th><bean:message bundle="CM" key="cm.search1.pvsname"/></th>
		</logic:match>
	  	<logic:notMatch name = "SearchForm" property ="org" value="P">
		  	<th><bean:message bundle="CM" key="cm.search1.organization"/></th>
		  	<th><bean:message bundle="CM" key="cm.search1.division"/></th>
		</logic:notMatch> 	
	  	
	  	<th><bean:message bundle="CM" key="cm.search1.city"/></th>
	  	<th><bean:message bundle="CM" key="cm.search1.state"/></th>
	  	<th><bean:message bundle="CM" key="cm.search1.personofcontacts"/></th>
	  </tr>
  
  <logic:iterate name="searchResult" id="search">
  <%if (i%2==0)cls="labelo" ;
    if (i%2==1)cls="labele" ;
    i++;
   %>

	  <tr class="<%=cls%>">	  
	 	<logic:notMatch name = "SearchForm" property ="org" value="P">
	  		<td><a href=AddTopLevel.do?function=add&type=type&element=<bean:write name="search" property="lo_ot_id"/>&org_type=<bean:write name="search" property="lo_ot_type"/>><bean:write name="search" property="lo_ot_name"/></a></td>
	  	</logic:notMatch>
	  	<td><a href=AddDivison.do?function=add&type=type&org_discipline_id=<bean:write name="search" property="lo_ot_id"/>&element=<bean:write name="search" property="lo_om_id"/>&org_type=<bean:write name="search" property="lo_ot_type"/>><bean:write name="search" property="lo_om_division"/></a>
	  	
	  	<logic:equal name="search" property="incidentType" value="Y" >
		 <img src="images/2.gif" border="0" height="10" width = "11">
		 </logic:equal>
		 <logic:equal name="search" property="status" value="R" >
		  <img src="images/1.gif" border="0" height="10" width = "11">
		 </logic:equal>
	  	
	  	</td>
	  	<td><bean:write name="search" property="lo_ad_city"/></td>
	  	<td><bean:write name="search" property="lo_ad_state"/></td>
	
		<logic:notEqual name="search" property="lo_om_id" value="0">		
		  	<logic:match name="search" property="contact" value="Add">
		  		<td><a href='AddPoc.do?function=add&org_discipline_id=<bean:write name="search" property="lo_ot_id"/>&division_id=<bean:write name="search" property="lo_om_id"/>&lo_pc_id=<bean:write name="search" property="lo_pc_id"/>&org_type=<bean:write name="search" property="lo_ot_type"/>'><bean:write name="search" property="contact"/></a></td>
			</logic:match>	
	  		<logic:match name="search" property="contact" value="View">
		  		<td><a href='Search.do?division_id=<bean:write name="search" property="lo_om_id"/>&org_discipline_id=<bean:write name="search" property="lo_ot_id"/>&search_type=<bean:write name="SearchForm" property="searchtype"/>+POC&org_type=<bean:write name="search" property="lo_ot_type"/>'><bean:write name="search" property="contact"/></a></td>
			</logic:match>
		</logic:notEqual>	  

		<logic:equal name="search" property="lo_om_id" value="0">		
		<td>-</td>
		</logic:equal>
	
	  </tr>
	</logic:iterate>
  </logic:present>
  
</logic:notPresent>

<!-------------------------------------  VIEW POC------------------------------->
  <logic:present name="viewPocResult" scope="request">

  <% i=0; cls="";%>
    <logic:present name="searchPocResult" scope="request">
  <tr>
  <td class="message" colspan="5"><bean:message bundle="CM" key="cm.search1.pocsearchresults"/></td>
  </tr>
  
  	  <tr class="tryb">
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.personofcontacts"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.address1"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.address2"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.city"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.state"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.zipcode"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.country"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.email"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.phone"/>&nbsp;</th>
	  	<th>&nbsp;<bean:message bundle="CM" key="cm.search1.fax"/>&nbsp;</th>
	  </tr>
  
  <logic:iterate name="searchPocResult" id="search">
  <%if (i%2==0)cls="labelo" ;
    if (i%2==1)cls="labele" ;
    i++;
   %>

	  <tr>	  
	    <td class="<%=cls%>">&nbsp;<a href=AddPoc.do?function=update&org_discipline_id=<bean:write name="SearchForm" property="org_discipline_id"/>&division_id=<bean:write name="SearchForm" property="division_id"/>&lo_pc_id=<bean:write name="search" property="lo_pc_id"/>&org_type=<bean:write name="SearchForm" property="org"/>><bean:write name="search" property="contact"/></a>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_address1"/>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_address2"/>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_city"/>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_state"/>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_zip_code"/>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_country"/>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_email1"/>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_phone"/>&nbsp;</td>
	  	<td class="<%=cls%>">&nbsp;<bean:write name="search" property="lo_pc_fax"/>&nbsp;</td>	  	
	  	
	  </tr>
	  
	</logic:iterate>
  </logic:present>
</logic:present>
  
  </table>
  </td>
</tr>
</table>
</html:form>
</html:html>
<script>
function setSearch()
{

	document.forms[0].type.value=trim(document.forms[0].type.value);
	document.forms[0].division.value=trim(document.forms[0].division.value);
//	document.forms[0].poc.value=trim(document.forms[0].poc.value);	
	if(isBlank(document.forms[0].type.value)&& isBlank(document.forms[0].division.value)){
		alert("<bean:message bundle="CM" key="cm.search1.entersomevalueforsearch"/>");
		return false;
	}
//	if(! chkBlank(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;
	if(! CheckQuotes(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;	
//	if(! chkBlank(document.forms[0].division,"<bean:message bundle="CM" key="cm.searchdivision"/>"))return false;		
	if(! CheckQuotes(document.forms[0].division,"<bean:message bundle="CM" key="cm.searchdivision"/>"))return false;
//	if(! CheckQuotes(document.forms[0].poc,"<bean:message bundle="CM" key="cm.searchpoc"/>"))return false;
	document.forms[0].action = "Search.do?type="+document.forms[0].type.value+"&division="+document.forms[0].division.value;				
	document.forms[0].submit();								
	return true;	
}


function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("<bean:message bundle="CM" key="cm.search1.pleaseenter"/>"+label);	
		obj.focus();
		return false;
	}
	return true;
}
function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}
</script>