<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>

<!-- Include Jquery -->
<script src="javascript/jquery-1.11.0.min.js"></script>
<script>

$( document ).ready(function() {
        var user_data = getCookie('cns-user_data');
        var token = getCookie('cns-token');
        
        var parsedUserData = JSON.parse(user_data);
        
        $.ajax({
            method: "GET",
            url: "https://api.contingent.com:3001/ilex/customer/workCenter",
            headers: {"x-access-token": token, "x-access-user-id": parsedUserData.id},
            cache: true,
            dataType: "json"         
        }).done(function (res) {
            if (res.status === 'success') {
                
                //Populate the dropdown with the results of the source query.
                var options = $("#sourceBox");
                
                $.each(res.data, function() {
                    options.append($("<option />").val(this.work_center_id).text(this.work_center_name));
                });          
                
                //Set to Contingent as default.
                $('[name="source"]').val("1");
                
            } else {
                //API Error
                window.location('/Ilex/Login.do?logout=true');
            }
        });
        
        $("#sourceBox").change(function() {
            $('[name="source"]').val($(this).val());
        });
        
});



</script>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %> <%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %> <%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id = "dccm" name = "dccm" scope = "request"/>
<%
String table_label="";
String menu_head="";
String menu_label="";
String menu_label2="";
String js_blank_msg="";
String add_division="cm.adddivision";
String manage_org="";
String mfg_label="";
String select_label="";
String type_name="";
int comboflag=0;
int flag=0;

char type=((String)request.getParameter("org_type")).trim().charAt(0);

switch(type){
case 'C':
{
	if(((Integer)request.getAttribute("element1")).intValue()==0)
		table_label="cm.addnewtopcustomer";
	else
		table_label="cm.updatenewtopcustomer";		

	menu_head="cm.customermenuhead";		
	type_name="cm.customername";
		
	menu_label="cm.addnewtopcustomer";
	menu_label2="cm.deletenewtopcustomer";
    js_blank_msg="cm.entertopcustomer";
    manage_org="cm.managecustomer";
	select_label="cm.custcategory";
	mfg_label="cm.custsecondcategory";
	break;
}
case 'N':
{
	if(((Integer)request.getAttribute("element1")).intValue()==0)
		table_label="cm.addnewtopCNS";
	else
		table_label="cm.updatenewtopCNS";
	menu_head="cm.CNSmenuhead";
	type_name="cm.CNSname";	
	menu_label="cm.addnewtopCNS";
	menu_label2="cm.deletenewtopCNS";
	js_blank_msg="cm.entertopCNS";
	manage_org="cm.manageCNS";
	select_label="cm.cnscategory";
	mfg_label="cm.cnssecondcategory";
	
	break;
}
case 'P':
{
	if(((Integer)request.getAttribute("element1")).intValue()==0)
		table_label="cm.addnewtopPVS";
	else
		table_label="cm.updatenewtopPVS";
	menu_head="cm.PVSmenuhead";
	type_name="cm.PVSname";	
	menu_label="cm.addnewtopPVS";
	menu_label2="cm.deletenewtopPVS";
	js_blank_msg="cm.entertopPVS";
	manage_org="cm.managePVS";
	select_label="cm.PVScategory";
	mfg_label="cm.PVSsecondcategory";
	
	break;
}
case 4:
{
	menu_label="rm.taddmanufacturers";
	menu_label2="rm.tdeletemanufacturers";
	js_blank_msg="";

	select_label="rm.tecategory";
	mfg_label="rm.tsecondcategory";
	
	break;
}
	default:
	{
		menu_label="rm.maddmanufacturers";
		menu_label2="rm.mdeletemanufacturers";

		select_label="rm.mecategory";
		mfg_label="rm.msecondcategory";
	}
}
%>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"
	type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>

<title></title>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>

<%@ include  file="/Menu.inc" %>




<link rel="stylesheet" href="styles/style.css" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');mkReadOnly();" bgcolor="#FFFFFF">
<logic:present name = "AddTopLevelForm" property="deletemessage">

	<logic:equal name="AddTopLevelForm" property="deletemessage" value="0">
	</logic:equal>
	
	<logic:equal name="AddTopLevelForm" property="deletemessage" value="-9001">
	</logic:equal>

	<logic:equal name="AddTopLevelForm" property="deletemessage" value="-9002">
	</logic:equal>
	
</logic:present>

<logic:present name="AddTopLevelForm" property="addmessage">

	<logic:equal name="AddTopLevelForm" property="addmessage" value="0">
	<% flag=1; %>
	</logic:equal>
	
	<logic:equal name="AddTopLevelForm" property="addmessage" value="-9002">
	</logic:equal>
	
	<logic:equal name="AddTopLevelForm" property="addmessage" value="-9003">
	</logic:equal>
	
</logic:present>

<logic:present name="AddTopLevelForm" property="updatemessage">

	<logic:equal name="AddTopLevelForm" property="updatemessage" value="0">
	<% flag=1; %>
	</logic:equal>	
	
	<logic:equal name="AddTopLevelForm" property="updatemessage" value="-9001">
	<% flag=1; %>
	</logic:equal>
	
	<logic:equal name="AddTopLevelForm" property="updatemessage" value="-9003">
	<% flag=1; %>
	</logic:equal>	
	
</logic:present>


<%if (flag!=1){ %>  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center">
          <td id="pop2"  width=200 align=left class="toprow1"><a href="AddTopLevel.do?function=add&org_type=<%=type%>&type=type"     onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/></a></td>
          <%//if(((Integer)request.getAttribute("element1")).intValue()!=0){    %>
                     <td   width=200 align=left class="toprow1"><a href="javascript:del();" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label2 %>"/></a></td>
					<td width = "400" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <% //}else
          if (false){ %>
		  <td width = "600" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		  <% } %>
          </tr>
      </table>
    </td>
  </tr>
</table>
<% } else {%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center">
          <td   width=200 align=left class="toprow1"><a href="AddDivison.do?function=add&org_type=<%=type%>&org_discipline_id=<%= request.getAttribute("element1") %>&type=type"     onmouseout="MM_swapImgRestore();"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=add_division %>"/></a></td>
                    <!--  <td width=200 align=left><a href="AddTopLevel.do?function=delete&org_type=<%=type%>&element=<%= request.getAttribute("element1") %>&type=type"     onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=manage_org %>"/>/></a></td>-->
                     <td id="pop2" width=200 align=left class="toprow1"><a href="#" onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="drop"><bean:message bundle="CM" key="<%=manage_org  %>" /></a></td>
					 <td width = "400" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
      </table>
    </td>
  </tr>
</table>
<% } %>
<SCRIPT language=JavaScript1.2>


if (isMenu) {

arMenu1=new Array(
140,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
"<bean:message bundle="CM" key="cm.add"/>","AddTopLevel.do?function=add&org_type=<%=type%>&type=type",0,
"<bean:message bundle="CM" key="cm.update"/>","AddTopLevel.do?function=add&org_type=<%=type%>&element=<%= request.getAttribute("element1")%>&type=type",0
)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

</script>
<TABLE>
<html:form action="AddTopLevel" >
<html:hidden  property="refresh" />
<html:hidden property="function" />
<html:hidden property="org_type" value='<%=request.getParameter("org_type")%>' />

<table border="0" cellspacing="1" cellpadding="1" >
<tr>
<td  width="1" height="0"></td>
  <td>
  <table border="0" cellspacing="1" cellpadding="1" width=100% > 

  <logic:present name = "AddTopLevelForm" property="deletemessage">

	<logic:equal name="AddTopLevelForm" property="deletemessage" value="0">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.deletedsuccess"/>  </td></tr> 
		
	</logic:equal>
	
	<logic:equal name="AddTopLevelForm" property="deletemessage" value="-9001">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>  </td></tr> 
		
	</logic:equal>

	<logic:equal name="AddTopLevelForm" property="deletemessage" value="-9002">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>  </td></tr> 
	</logic:equal>	
	
</logic:present>

<logic:present name="AddTopLevelForm" property="addmessage">

	<logic:equal name="AddTopLevelForm" property="addmessage" value="0">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.addedsuccessfully"/>  </td></tr> 
	</logic:equal>

	<logic:equal name="AddTopLevelForm" property="addmessage" value="-9002">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.insertfailed"/>  </td></tr> 
	</logic:equal>
	
	<logic:equal name="AddTopLevelForm" property="addmessage" value="-9003">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>  </td></tr> 
	</logic:equal>
	
</logic:present>

<logic:present name="AddTopLevelForm" property="updatemessage">

	<logic:equal name="AddTopLevelForm" property="updatemessage" value="0">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.updatesuccessfully"/>  </td></tr> 
	</logic:equal>	
	
	<logic:equal name="AddTopLevelForm" property="updatemessage" value="-9001">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.updatefailed"/>  </td></tr> 
	</logic:equal>
	
	<logic:equal name="AddTopLevelForm" property="updatemessage" value="-9003">
  <tr><td  colspan="2" class="message" height="30" >	<bean:message bundle="CM" key="<%=mfg_label %>"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>  </td></tr> 
	</logic:equal>		
</logic:present>   

  <tr> 
    <td colspan="2" class="labeleboldwhite" height="30" ><bean:message bundle="CM" key="<%=table_label%>"/></td>
  </tr> 

   <tr>
    <td  class="labelobold" height="25" ><bean:message bundle="CM" key="cm.organizationcategory"/></td>
    <td  class="labelo" width="200"><bean:write name = "AddTopLevelForm" property = "orgcatg"/></td>
   </tr>
      <%if (flag!=1) { %>  
  <tr> 
    <td  class="labelebold" height="25" ><bean:message bundle="CM" key="<%=select_label %>"/></td>
    <td  align=left class="labele">
   
	<html:select name="AddTopLevelForm" property="element" size="1" styleClass="comboe" onchange = "return setValue();">
	<html:optionsCollection name = "dccm"  property = "firstlevelcatglist"  label = "label"  value = "value" />
	</html:select></td>
  </tr>
 <% } %>
  
    <%if (flag!=1) { %>  
    <tr>
        <td><b style="font-size:13px">Select Work Center</b></td>
        <td>
		    <html:select name="AddTopLevelForm" property="workCenterId" size="1" styleClass="comboe">
				<html:optionsCollection name = "dccm"  property = "workCenterList"  label = "label"  value = "value" />
			</html:select>
        </td>
    </tr>
     <% } %>
 
    <html:hidden property="source" name="AddTopLevelForm"/>
    
   <tr> 
       <%if (flag!=1){ %>     
            <td  class="labelobold"  ><bean:message bundle="CM" key="<%=type_name%>"/><font class="red"> *</font></td>
		    <td  class="labelo" align="left">    
			<html:text  styleClass="textbox" size="14" name = "AddTopLevelForm" maxlength="50" property="custname" />
	   <% } else{ %>
     		<td  class="labelebold" ><bean:message bundle="CM" key="<%=type_name%>"/></td>				
		    <td  class="labele" align="left" >     
     		<bean:write name="AddTopLevelForm" property="custname" />
		<% }%>
	</td>
  </tr>  
  <%if (flag!=1){ %> 
    <tr> 
    <td colspan="2" class="buttonrow"> 
     <html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="CM" key="cm.save"/></html:submit>
     <html:reset property="reset" styleClass="button"><bean:message bundle="CM" key="cm.reset"/></html:reset>
  </tr>
  <% } %>
  
<jsp:include page = '../Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'cmAddTopLevel'/>
</jsp:include>


	</table></td></tr>
	</table>
<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>

<table>
<tr>
	<td ><html:text name = "AddTopLevelForm" styleClass="textboxdummy" size="1" readonly="true" property="textboxdummy" /></td>
</tr>
</table>

</html:form>
</TABLE>

</BODY>
<script>

function setValue()
{
	document.forms[0].refresh.value = "true";
	document.forms[0].submit();
	return true;
}


function validate() {
	trimFields();
	if(isBlank(document.forms[0].custname.value)) {
		errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";
		warningEmpty(document.forms[0].custname,"<bean:message bundle="CM" key="<%=type_name%>"/>",errormsg);
		return false;
	} else {
		if(! CheckQuotes(document.forms[0].custname,"<bean:message bundle="CM" key="<%=type_name%>"/>"))
		return false;
	}
	if(!chkNetMedX(document.forms[0].custname.value)) return false;
	return true; //CheckQuotes ();
}
</script>
<script>
function trimFields() {
	var field=document.forms[0];
	for(var i=0; i<field.length; i++) {
		if(field[i].type=='text') {
			var temp1=field[i].value;
			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>
function CheckQuotes () {
	var field=document.forms[0];
	for(i=0; i<field.length; i++) {
		if(field[i].type=='text') {
			if((field[i].value.indexOf("'")>=0)||(field[i].value.indexOf("\"")>=0)||(field[i].value.indexOf("&")>=0)) {
				errormsg="<bean:message bundle="CM" key="cm.warnmsgchar"/>";
				alert(errormsg);
				field[i].focus();
				return false;
			}
		}
	}
return true;
}	

function del() {
		if(<%= (((Integer)request.getAttribute("element1")).intValue()!=0) %>) {
			if(chkNetMedX(document.forms[0].custname.value)) {
			var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
			var convel = confirm( msg );
			if( convel ) {
				document.forms[0].refresh.value = "false";
				document.forms[0].action = "AddTopLevel.do?function=delete&org_type=<%=type%>&element=<%= request.getAttribute("element1")%>&refresh=false&type=type";				
				document.forms[0].submit();								
				return true;	
		 }  }
		}
		else {
			alert( "<bean:message bundle="RM" key="rm.selectdeletion"/>" );
		}
}
function CheckQuotes(obj,label) {
	if(invalidChar(obj.value,label)) {
		obj.focus();
		return false;
	}
	return true;
}
function chkNetMedX(cname){
	if (cname=='NetMedX'){
		alert("<bean:message bundle="CM" key="cm.netmedxisenvironmentvariable"/>");
		return false;
	}
	else
		return true;
}

function mkReadOnly(){
	if(document.forms[0].custname)
	if (document.forms[0].custname.value=='NetMedX'){
		document.forms[0].custname.readOnly=true;
		document.forms[0].custname.className ="readonlytext";
	}
}
</script>
</html:html>