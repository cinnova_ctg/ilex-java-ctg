<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<bean:define id = "dccm" name = "dccm" scope = "request"/>
<bean:define id="dcLonDirec" name="dcLonDirec" scope="request"/>
<bean:define id="dcLatDirec" name="dcLatDirec" scope="request"/>
<bean:define id="dcCountryList" name="dcCountryList" scope="request"/>
<bean:define id="dcTimeZoneList" name="dcTimeZoneList" scope="request"/>


<%
String  labelbold ="";
String  label = "";
String table_label="";
String type_name="";
String menu_head="";
String menu_label="";
String menu_label2="";
String js_blank_msg="cm.entertopcustomer";
String add_division="cm.addPOC";
String manage_org="";
String mfg_label="";
String select_label="";
String division_location="cm.divison/location";
String last_first_name = "cm.lastfirstname";
String conMinuteman ="";
String element1=request.getAttribute("element1")+"";


if(request.getAttribute("conMinuteman") != null)
	conMinuteman = request.getAttribute("conMinuteman")+"";

int comboflag=0;
int flag=0;
char type=((String)request.getParameter("org_type")).trim().charAt(0);
if(((Integer)request.getAttribute("element1")).intValue()==0){
	table_label="cm.adddivison/location";
	if(type =='P') {
		table_label = "cm.add";
	}
}
else{
	table_label="cm.updatedivison/location";
		if(type =='P') {
			table_label = "cm.update";
		}
	}
switch(type){
case 'C':
{
	type_name="cm.custsecondcategory";
	
	menu_head="cm.customermenuhead";		
	menu_label="cm.adddivision";
	menu_label2="cm.deletedivision";
    manage_org="cm.managecustomer";
	select_label="cm.custcategory";
	mfg_label="cm.custsecondcategory";
	break;
}
case 'N':
{
	type_name="cm.cnssecondcategory";
	menu_head="cm.CNSmenuhead";
	menu_label="cm.adddivision";
	menu_label2="cm.deletedivision";
	manage_org="cm.manageCNS";
	select_label="cm.cnscategory";
	mfg_label="cm.cnssecondcategory";

	break;
}
case 'P':
{
	type_name="cm.PVSsecondcategory";
	menu_head="cm.PVSmenuhead";
	menu_label="cm.adddivision";
	menu_label2="cm.deletedivision";
	manage_org="cm.managePVS";
	select_label="cm.PVScategory";
	mfg_label="cm.PVSsecondcategory";

	if(type =='P')
	{
		menu_label2 = "cm.delete";
		menu_label = "cm.add";
	}
	
	break;
}
case 4:
{
	menu_label="rm.taddmanufacturers";
	menu_label2="rm.tdeletemanufacturers";

	select_label="rm.tecategory";
	mfg_label="rm.tsecondcategory";
	
	break;
}
	default:
	{
		menu_label="rm.maddmanufacturers";
		menu_label2="rm.mdeletemanufacturers";

		select_label="rm.mecategory";
		mfg_label="rm.msecondcategory";
	}
}
%>
<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<%@ include  file="/AjaxStateCountry.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"
	type="text/css">
		<script language = "JavaScript" src = "javascript/JLibrary.js"></script>

<title></title>
<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
    </style>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
<jsp:include page="/common/IncludeGoogleKey.jsp"/>


<script type="text/javascript">
  
  function load()
   {
   
   	if(typeof GBrowserIsCompatible == 'undefined') {
		//	alert('Need Internet Connection to calculate Lat/Long.');
   	}
   	
   	  if(typeof GBrowserIsCompatible == 'undefined'){
   	  }else {
	  if (GBrowserIsCompatible())
	  {
		    geocoder = new GClientGeocoder();
	  }
	  
	 }
	  
    }
    
    function showZip() 
    {
    
		{

   			var zip = document.forms[0].zip.value;
   	
      		if (geocoder) {
        	geocoder.getLatLng(
          	zip ,
              function(point) 
              {
                  if (point)
                  { 
                  	
					var i=point.lng();
					if(eval(i<0))
					document.forms[0].lon_direction.value = "S";
					else
					document.forms[0].lon_direction.value = "N";						
					var hh="" + i + "";
					arrr=hh.split(".");
					document.forms[0].lon_deg.value=arrr[0];
					var deg =arrr[0];
					var min = arrr[1]*60;
					var minstr = "" +min +"";
					document.forms[0].lon_min.value=minstr.substring(0,2);

					
					var j =point.lat();
					if(eval(j<0))
					document.forms[0].lat_direction.value = "W";
					else
					document.forms[0].lat_direction.value = "E";
					var gg="" +j + ""; 
					arr=gg.split(".");
					document.forms[0].lat_deg.value=arr[0];
					var deglat = arr[0];
					var minlat = arr[1]*60;
					var minlatstr = "" +minlat +"";
					document.forms[0].lat_min.value=minlatstr.substring(0,2);
					
				 }
				  else
				  {
				  // If lat lon not calculated on zip / address
                  document.forms[0].latlonFound.value = "NA";	
				  
				  document.forms[0].lon_deg.value="";
				  document.forms[0].lon_min.value="";
				  document.forms[0].lat_deg.value="";
				  document.forms[0].lat_min.value="";
				  }
			  }
        );
      }
    
    }
}
    
    function showAddress(type) {
      
	if((type == 'zip') && (document.forms[0].latlonFound.value == 'true')) {
		//alert('in if')
		return false;
	}
    
      var address = document.forms[0].firstaddressline.value;
    //  document.forms[0].city.focus();
    	
    if(typeof GBrowserIsCompatible == 'undefined'){
   	}
   	else{
    
      if (geocoder) {
        geocoder.getLatLng(
          address,
              function(point) 
              {
                  if (point)
                  { 
                  //alert('in point ');
	                // If lat lon calculated on address
	                document.forms[0].latlonFound.value = "true";
					
					var i=point.lng();
					if(eval(i<0))
					document.forms[0].lon_direction.value = "S";
					else
					document.forms[0].lon_direction.value = "N";						
					var hh="" + i + "";
					arrr=hh.split(".");
					document.forms[0].lon_deg.value=arrr[0];
					
					var deg =arrr[0];
					var min = arrr[1]*60;
					var minstr = "" +min +"";
					document.forms[0].lon_min.value=minstr.substring(0,2);

					
					var j =point.lat();
					if(eval(j<0))
					document.forms[0].lat_direction.value = "W";
					else
					document.forms[0].lat_direction.value = "E";
					var gg="" +j + ""; 
					arr=gg.split(".");
					document.forms[0].lat_deg.value=arr[0];
					
					var deglat = arr[0];
					var minlat = arr[1]*60;
					var minlatstr = "" +minlat +"";
					document.forms[0].lat_min.value=minlatstr.substring(0,2);
				 }
				  else
				  {
				  // If lat lon calculated on zip
                  	document.forms[0].latlonFound.value = "false";
				 	showZip();	
				  }
			  }
        );
        
    }
    }
}
</script>

<%@ include  file="/Menu.inc" %>

<link rel="stylesheet" href="styles/style.css" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="load();MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" bgcolor="#FFFFFF">


<logic:present name = "AddDivisonForm" property="deletemessage">

	<logic:equal name="AddDivisonForm" property="deletemessage" value="0">
	</logic:equal>	
	
	<logic:equal name="AddDivisonForm" property="deletemessage" value="-9001">
	</logic:equal>
	
	<logic:equal name="AddDivisonForm" property="deletemessage" value="-9002">
	</logic:equal>
		
</logic:present>

<logic:present name="AddDivisonForm" property="addmessage">

	<logic:equal name="AddDivisonForm" property="addmessage" value="0">
	<% flag=1; %>
	</logic:equal>
	
	<logic:equal name="AddDivisonForm" property="addmessage" value="-9002">
	</logic:equal>

	<logic:equal name="AddDivisonForm" property="addmessage" value="-9003">
	</logic:equal>
	
</logic:present>

<logic:present name="AddDivisonForm" property="updatemessage">
	<logic:equal name="AddDivisonForm" property="updatemessage" value="0">
	<% flag=1; %>
	</logic:equal>	
	
	<logic:equal name="AddDivisonForm" property="updatemessage" value="-9001">
	<% flag=1; %>
	</logic:equal>
	
	<logic:equal name="AddDivisonForm" property="updatemessage" value="-9003">
	<% flag=1; %>
	</logic:equal>	
	
</logic:present>

 
<%if (flag!=1){ %>  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center">        
        <logic:match name = "AddDivisonForm" property ="org_type" value="P">
          <%--<td id="pop2" colspan=3 width=200 class="toprow1"><a href="AddDivison.do?function=add&type=type&org_type=<%=type%>&org_discipline_id=<%=((Integer)request.getAttribute("customer_id")).intValue()%>"     onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/> <bean:write name = "AddDivisonForm" property = "custname"/></a></td>--%>
          <td id="pop2" colspan=3 width=200 class="toprow1"><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=<bean:write name = "AddDivisonForm" property = "custname"/>"  onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/> <bean:write name = "AddDivisonForm" property = "custname"/></a></td>        
		 </logic:match>
		 <logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
          <td id="pop2" colspan=3 width=200 class="toprow1"><a href="AddDivison.do?function=add&type=type&org_type=<%=type%>&org_discipline_id=<%=((Integer)request.getAttribute("customer_id")).intValue()%>"     onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label %>"/></a></td>        
		 </logic:notMatch>
		 
		  
          <%//if(((Integer)request.getAttribute("element1")).intValue()!=0){    %>        	
  		 <logic:match name = "AddDivisonForm" property ="org_type" value="P">
  		 	<logic:present name = "AddDivisonForm" property ="element" >
  		 		<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
  		 		<td width=200 align=left class="toprow1">
  		 		 <a href="javascript:del();" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label2 %>"/> <bean:write name = "AddDivisonForm" property = "custname"/></a></td>
  		 		 <%}%>
          		 <!-- <td width = "400" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp; -->
          		 <td width = "150" align=left class="toprow1"><a href="AddPoc.do?function=add&org_type=<%=type%>&org_discipline_id=<%=((Integer)request.getAttribute("customer_id")).intValue()%>&division_id=<%= request.getAttribute("element1")%>" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="cm.addPOC"/></a></td>			
          		 <td width = "400" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp; 
          	</logic:present>
  		 </logic:match>
	  		 
  		 <logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
  		 	<td width = "150" align=left class="toprow1">
  		 	<a href="javascript:del();" onmouseout="MM_swapImgRestore();"  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=menu_label2 %>"/></a></td>
          	<td width = "400" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
  		 </logic:notMatch>
  		 
  		  </td>
          <% //} else
          if(false){%>
          <td width = "600" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		 <%} %>
           </tr>
      </table>
    </td>
  </tr>
</table>
<% } else {%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center">
          <td  colspan=3 width=150 align=left class="toprow1"><a href="AddPoc.do?function=add&type=type&org_type=<%=type%>&org_discipline_id=<%=((Integer)request.getAttribute("customer_id")).intValue()%>&division_id=<%= request.getAttribute("element1") %>"     onmouseout="MM_swapImgRestore();"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="CM" key="<%=add_division %>"/></a></td>                   
          <td  id="pop2" width="300" align=left class="toprow1"><a href="#" onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"  onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="drop"><bean:message bundle="CM" key="<%=manage_org %>" />&nbsp;<bean:message bundle="CM" key="<%=division_location %>" /></a></td>
		  <td width = "350" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<% } %>


<SCRIPT language=JavaScript1.2>
<!--

if (isMenu) {
arMenu1=new Array(
80,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
"<bean:message bundle="CM" key="cm.add"/>","AddDivison.do?function=add&org_type=<%=type%>&org_discipline_id=<%= String.valueOf(((Integer)request.getAttribute("customer_id")).intValue())%>",0,
"<bean:message bundle="CM" key="cm.update"/>","AddDivison.do?function=add&org_type=<%=type%>&org_discipline_id=<%= String.valueOf(((Integer)request.getAttribute("customer_id")).intValue())%>&element=<%= String.valueOf(((Integer)request.getAttribute("element1")).intValue())%>",0
)

arMenu3=new Array()
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}
//-->
</script>

<TABLE>
<html:form  action="AddDivison" >

<table border="0" cellspacing="1" cellpadding="1" >
<tr>
<td  width="1" height="0"></td>
<td>
<html:hidden property="refresh" />
<html:hidden property="function" />
<html:hidden property="org_type" />
<html:hidden property="countrychange" />
<html:hidden property="org_discipline_id" />
<html:hidden property="latlonFound" />
<html:hidden property = "lat_deg" />
<html:hidden property = "lat_min" />
<html:hidden property = "lat_direction" />
<html:hidden property = "lon_deg" />
<html:hidden property = "lon_min" />
<html:hidden property = "lon_direction" />
<html:hidden property = "accept_address" />

<html:hidden property = "latitude" />
<html:hidden property = "longitude" />
<html:hidden property = "latLongAccuracy" />
<html:hidden property="state" />

<!-- Comment Update PArt-->
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
 <tr>
  <td width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" >

  <logic:present name = "AddDivisonForm" property="deletemessage">
	<logic:equal name="AddDivisonForm" property="deletemessage" value="0">
	  		<tr><td  colspan="2" class="message" height="30" >
				<logic:equal name="AddDivisonForm" property="org_type" value="P">	
		  			<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.deletedsuccess"/>  
		  		</logic:equal>
		  		<logic:notEqual name="AddDivisonForm" property="org_type" value="P">	
		  			<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.deletedsuccess"/>  
		  		</logic:notEqual>	
	  		</td></tr>
	</logic:equal>
	
	<logic:equal name="AddDivisonForm" property="deletemessage" value="-9001">
  			<tr><td  colspan="2" class="message" height="30" >		
	  			<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  			<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>  
		  		</logic:equal>
		  		<logic:notEqual name="AddDivisonForm" property="org_type" value="P">	
		  			<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>
		  		</logic:notEqual>	
  			</td></tr>
	</logic:equal>

	<logic:equal name="AddDivisonForm" property="deletemessage" value="-9002">
  			<tr><td  colspan="2" class="message" height="30" >		
	  			<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  		<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>  
		  		</logic:equal>
		  		<logic:notEqual name="AddDivisonForm" property="org_type" value="P">	
		  			<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/>  
		  		</logic:notEqual>	
  			</td></tr>
	</logic:equal>
	<logic:equal name="AddDivisonForm" property="deletemessage" value="-9003">
  			<tr><td  colspan="2" class="message" height="30" >		
	  			<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  		<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed1"/>  
		  		</logic:equal>
  			</td></tr>
	</logic:equal>
	<logic:equal name="AddDivisonForm" property="deletemessage" value="-9990">
  			<tr><td  colspan="2" class="message" height="30" >		
	  			<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  		<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed2"/>  
		  		</logic:equal>
  			</td></tr>
	</logic:equal>	
	
</logic:present>

<logic:present name="AddDivisonForm" property="addmessage">
	<logic:equal name="AddDivisonForm" property="addmessage" value="0">
  			<tr><td  colspan="2" class="message" height="30" >		
  				<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  		<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.addedsuccessfully"/>  
		  		</logic:equal>
		  		<logic:notEqual name="AddDivisonForm" property="org_type" value="P">
  					<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.addedsuccessfully"/>	  
  				</logic:notEqual>	
  			</td></tr>
	</logic:equal>

	<logic:equal name="AddDivisonForm" property="addmessage" value="-9002">
  		<tr><td  colspan="2" class="message" height="30" >		
  			<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  	<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.insertfailed"/>  
		  	</logic:equal>
		  	<logic:notEqual name="AddDivisonForm" property="org_type" value="P">
  				<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.insertfailed"/>  
  			</logic:notEqual>	
  		</td></tr>
	</logic:equal>
	
	<logic:equal name="AddDivisonForm" property="addmessage" value="-9003">
  		<tr><td  colspan="2" class="message" height="30" >		
  			<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  	<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>  
		  	</logic:equal>
		  	<logic:notEqual name="AddDivisonForm" property="org_type" value="P">
	  			<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>  
  			</logic:notEqual>
  		</td></tr>
	</logic:equal>
	
</logic:present>

<logic:present name="AddDivisonForm" property="updatemessage">
	<logic:equal name="AddDivisonForm" property="updatemessage" value="0">
		  <tr><td  colspan="2" class="message" height="30" >		
		  	<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  	<% if(conMinuteman.equals("")) { %>
				  	<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.updatesuccessfully"/>
			  	<% } else { %>
					<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;Converted Successfully			  		
			  	<% } %>
		  	</logic:equal>
		  	<logic:notEqual name="AddDivisonForm" property="org_type" value="P">
		  		<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.updatesuccessfully"/>  
		  	</logic:notEqual>	
		  </td></tr>
	</logic:equal>	

	<logic:equal name="AddDivisonForm" property="updatemessage" value="-9001">
  			<tr><td  colspan="2" class="message" height="30" >		
  			<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  	<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.updatefailed"/>
		  	</logic:equal>
		  	<logic:notEqual name="AddDivisonForm" property="org_type" value="P">
  				<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.updatefailed"/>  
  			</logic:notEqual>	
  			</td></tr>
	</logic:equal>

	<logic:equal name="AddDivisonForm" property="updatemessage" value="-9003">
  			<tr><td  colspan="2" class="message" height="30" >		
			<logic:equal name="AddDivisonForm" property="org_type" value="P">	
			  	<bean:write name = "AddDivisonForm" property = "custname"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>  
		  	</logic:equal>
		  	<logic:notEqual name="AddDivisonForm" property="org_type" value="P">
  				<bean:message bundle="CM" key="cm.divison/location"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>  
  			</logic:notEqual>	
  			</td></tr>
	</logic:equal>
	
	<logic:equal name="AddDivisonForm" property="updatemessage" value="-9010">
  			<tr><td  colspan="2" class="message" height="30" >		
			  	Minuteman Partner <bean:message bundle="CM" key="cm.alreadyexists"/>  
  			</td></tr>
	</logic:equal>
		
</logic:present> 
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
 <tr>
  <td width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" >      
  	   <%
  	   
  	   if (flag!=1){ %> 
  	    
        <logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
	       <tr> 
	        <td colspan="4" class="labeleboldwhite" height="30" ><bean:message bundle="CM" key="<%=table_label%>"/>&nbsp;<bean:message bundle="CM" key="cm.under"/><bean:message bundle="CM" key="<%= mfg_label%>"/><%=  " : " %><bean:write name = "AddDivisonForm" property = "custname"/></td>
	       </tr> 
       </logic:notMatch>
       <logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
	       <tr>
	        <td  class="labelebold" height="25"><bean:message bundle="CM" key="<%=type_name%>"/>&nbsp;<bean:message bundle="CM" key="cm.name"/></td>
	        <td  class="labele"><bean:write name = "AddDivisonForm" property = "custname"/> </td>
	       </tr>
  		</logic:notMatch>
  	
  		<logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
  	
	       <tr> 
	        <td  class="labeloBold" height="25"><bean:message bundle="CM" key="cm.selectExisting"/><bean:message bundle="CM" key="cm.divison/location"/></td>
	        <td  class="labelo"> 
	    	<html:select name="AddDivisonForm" property="element" size="1" styleClass="comboo" onchange = "return setValue();">
		 		<html:optionsCollection name = "dccm"  property = "secondlevelcatglist"  label = "label"  value = "value" />
	    	</html:select>&nbsp;&nbsp;&nbsp;&nbsp;  <bean:message bundle="CM" key="cm.or"/></td>	
	       </tr>
		</logic:notMatch>
	
		<logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
			<tr> 
	        <td  class="labeleBold" height="25"><bean:message bundle="CM" key="cm.divison/location"/><font class="red"> *</font></td>
	        <td  class="labele">
		    <html:text  styleClass="textbox" size="20" maxlength="50" property="divison_name" /></td>
	       </tr>
		</logic:notMatch>		
	  	  	<logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
			<html:hidden property="cmboxCoreCompetency" />
 		</logic:notMatch>
   <logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
       <tr> 
        <td  class="labeloBold" height="25"><bean:message bundle="CM" key="cm.firstaddressline" /><font class="red"> *</font></td>
        <td  class="labelo">
	     <html:text  styleClass="textbox" size="25" maxlength="100" property="firstaddressline" onblur="showAddress();"/></td> 
       </tr>

       <tr> 
        <td  class="labeleBold" height="25"><bean:message bundle="CM" key="cm.secondaddressline"/></td>
        <td  class="labele">
	    <html:text styleClass="textbox"  size="25" maxlength="100" property="secondaddressline"/></td>
       </tr>

       <tr> 
        <td  class="labeloBold" height="25"><bean:message bundle="CM" key="cm.city"/><font class="red"> *</font></td>
        <td  class="labelo">
	    <html:text  styleClass="textbox" size="20" maxlength="50" property="city" /></td>
       </tr>
       <tr>
			<td class = "labeleBold" height="25"><bean:message bundle = "CM" key = "cm.state/province"/><font class="red"> *</font></td>
			    <td  class="labele">
			    	<logic:present name="initialStateCategory">
						<html:select property = "stateSelect"  styleClass="comboe" onchange = "return ChangeCountry();">
							<logic:iterate id="initialStateCategory" name="initialStateCategory" >
								<bean:define id="statecatname" name="initialStateCategory" property="siteCountryIdName" />		   
								<%  if(!statecatname.equals("notShow")) { %>
								<optgroup class=labelebold label="<%=statecatname%>">
								<% } %>
									<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
								<%  if(!statecatname.equals("notShow")) { %>	
								</optgroup> 
								<% } %>
							</logic:iterate>
						</html:select>
					</logic:present>	
				 </td>
			</tr>
       
       <tr> 
        <td  class="labeloBold" height="25"><bean:message bundle="CM" key="cm.zip/postal"/><font class="red"> *</font></td>
        <td  class="labelo">
	     <html:text styleClass="textbox"  size="10" maxlength="50" property="zip" onblur="showAddress();"/></td>
	   </tr>
  
       <tr> 
        <td  class="labeleBold" height="25"><bean:message bundle="CM" key="cm.country"/><font class="red"> *</font></td>
        <td  class="labele">
	   		  <html:select property="country" size="1" styleClass="comboe"> 
				<html:optionsCollection name = "dcCountryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
				</html:select>
	    </td>
       </tr>
        
       <tr> 
        <td class = "labelobold">Time Zone</td>
					    <td class = "labelo">
				      		 <html:select  name = "AddDivisonForm" property="timezone" size="1" styleClass="comboo">
								<html:optionsCollection name = "dcTimeZoneList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>   
					    </td>
       
       </tr>
   
       
       <tr> 
        <td  class="labeleBold" height="25"><bean:message bundle="CM" key="cm.divison.url"/></font></td>
        <td  class="labele">
	     <html:text styleClass="textbox"  size="20"  maxlength="50"  property="website" />
	    </td>
       </tr>
  

       <tr> 
        <td colspan="2" class="ButtonRow"> 
        <html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="CM" key="cm.save"/></html:submit>
        <html:reset property="reset" styleClass="button"><bean:message bundle="CM" key="cm.reset"/></html:reset>
        </logic:notMatch>
     	</td>
       </tr> 
    
        
   	  <%} else { 
		  
		  if(type == 'P'){
			  labelbold = "labeleBold";
			  label = "labele";}
	    else
			{ 	labelbold = "labeloBold";
	   			label = "labelo";
			}  %>  
       <tr> 
        	<td colspan="4" class="labeleboldwhite" height="30" >
			  	<logic:notEqual name="AddDivisonForm" property="org_type" value="P">
               		<bean:message bundle="CM" key="cm.updatedivison/location"/>&nbsp;<bean:message bundle="CM" key="cm.under"/>&nbsp;<bean:message bundle="CM" key="<%= mfg_label%>"/><%=  " : " %><bean:write name = "AddDivisonForm" property = "custname"/>
               	</logic:notEqual>	
        	</td>
       </tr> 
       
        <logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">
	       <tr> 
	        <td  class="labelebold" height="25"><bean:message bundle="CM" key="<%=type_name%>"/>&nbsp;<bean:message bundle="CM" key="cm.name"/></td>
	        <td  class="labele"><bean:write name = "AddDivisonForm" property = "custname"/></td>
	       </tr>  
	    </logic:notMatch>
	    
	    
		<logic:notMatch name = "AddDivisonForm" property ="org_type" value="P">  
	       <tr> 
	        <td  class="labeleBold" height="25"><bean:message bundle="CM" key="cm.firstaddressline"/></td>
	        <td  class="labele">
		    <bean:write name="AddDivisonForm" property="firstaddressline" /></td>
	       </tr>
	       <tr> 
	        <td  class="labeloBold" height="25"><bean:message bundle="CM" key="cm.secondaddressline"/></td>
	        <td  class="labelo">
		    <bean:write name="AddDivisonForm" property="secondaddressline"/></td>
	       </tr>
	       <tr> 
	        <td  class="labeleBold" height="25"><bean:message bundle="CM" key="cm.city"/></td>
	        <td  class="labele">
		    <bean:write name="AddDivisonForm" property="city" /></td>
	       </tr>
	       <tr> 
	        <td  class="labeloBold" height="25"><bean:message bundle="CM" key="cm.state/province"/></td>
	        <td class="labelo" >
		    <bean:write name="AddDivisonForm" property="state" /></td>				
		    </td>
	       </tr>
	       <tr> 
	        <td  class="labeleBold" height="25"><bean:message bundle="CM" key="cm.zip/postal"/></td>
	        <td  class="labele">
		    <bean:write name="AddDivisonForm" property="zip" /></td>
	       </tr>
	       <tr> 
	        <td  class="labeloBold" height="25"><bean:message bundle="CM" key="cm.country"/></td>
	        <td  class="labelo">
		     <bean:write name="AddDivisonForm" property="country" />
		    </td>
	       </tr>
	       <tr> 
		        <td class = "labelebold" height="25">Time Zone</td>
			    <td class = "labele">
		      		  <bean:write name="AddDivisonForm" property="timeZoneName" />
			    </td>
	       </tr>
	       <tr> 
	        <td  class="labeloBold" height="25"><bean:message bundle="CM" key="cm.divison.url"/></td>
	        <td  class="labelo">
		     <bean:write name="AddDivisonForm" property="website" />
		    </td>
	       </tr>
       </logic:notMatch>
   	  <% } %>
   	    

   	  
	</table></td></tr>
	</table>
	


<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>

</html:form>
</TABLE>

</BODY>

<script>

function setSearch()
{
	trimFields();
	if(isBlank(document.forms[0].type.value)&& isBlank(document.forms[0].division.value)&& isBlank(document.forms[0].poc.value)){
		if(! chkBlank(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;
	}
//	if(! chkBlank(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;
	if(! CheckQuotes(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;	
//	if(! chkBlank(document.forms[0].division,"<bean:message bundle="CM" key="cm.searchdivision"/>"))return false;		
	if(! CheckQuotes(document.forms[0].division,"<bean:message bundle="CM" key="cm.searchdivision"/>"))return false;
	if(! CheckQuotes(document.forms[0].poc,"<bean:message bundle="CM" key="cm.searchpoc"/>"))return false;
	document.forms[0].action = "Search.do?org=<%=type%>&type="+document.forms[0].type.value+"&division="+document.forms[0].division.value;				
	document.forms[0].submit();								
	return true;	
}

function setValue()
{	
	document.forms[0].refresh.value = "true";
	document.forms[0].submit();
	return true;
}

function validate()
{

	trimFields();
	
	if(isBlank(document.forms[0].divison_name.value))
	{
		if (document.forms[0].custname != null && document.forms[0].custname.value == "Minuteman Partners"){
			
			errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
			warningEmpty(document.forms[0].divison_name,"<bean:message bundle="CM" key="<%=last_first_name%>"/>",errormsg);		
			return false;
		}
		else{
			errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
			warningEmpty(document.forms[0].divison_name,"<bean:message bundle="CM" key="<%=division_location%>"/>",errormsg);		
			return false;
		}
	}
	else
	{
		if(! CheckQuotes(document.forms[0].divison_name,"<bean:message bundle="CM" key="<%=division_location%>"/>"))
		return false;
	}
	
	if(document.forms[0].org_type.value=='P')
	{
		if(! chkCombo(document.forms[0].cmboxCoreCompetency,"<bean:message bundle="CM" key="cm.coreCompetency"/>"))
			return false;
	}		
	
	if(isBlank(document.forms[0].firstaddressline.value))
	{
		errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
		warningEmpty(document.forms[0].firstaddressline,"<bean:message bundle="CM" key="cm.firstaddressline"/>",errormsg);		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].firstaddressline,"<bean:message bundle="CM" key="cm.firstaddressline"/>"))
		return false;
	}
		
	if(isBlank(document.forms[0].city.value))
	{
		errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
		warningEmpty(document.forms[0].city,"<bean:message bundle="CM" key="cm.city"/>",errormsg);		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].city,"<bean:message bundle="CM" key="cm.city"/>"))
		return false;
	}
	
	if(!isAlphabetic(trimBetweenTrail(document.forms[0].city.value)))
	{		
		alert("<bean:message bundle="CM" key="cm.cityalpha"/>");
		document.forms[0].city.focus();		
		return false;
	}	

	if(! chkCombo(document.forms[0].state,"<bean:message bundle="CM" key="cm.state/province"/>"))return false;						

	if(isBlank(document.forms[0].zip.value) && document.forms[0].country.value == 'US')
	{
		errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
		warningEmpty(document.forms[0].zip,"<bean:message bundle="CM" key="cm.zip/postal"/>",errormsg);		
		return false;
	}
	else
	{
		if(! CheckQuotes(document.forms[0].zip,"<bean:message bundle="CM" key="cm.zip/postal"/>"))
		return false;
	}
	//if(! chkInteger(document.getElementById('zip'),"<bean:message bundle="CM" key="cm.zip/postal"/>"))return false;		
	
	if(! chkCombo(document.forms[0].country,"<bean:message bundle="CM" key="cm.country"/>"))return false;		


//	if(isBlank(document.forms[0].country.value))
//	{
//		errormsg="<bean:message bundle="CM" key="cm.warnmsg"/>";		
//		warningEmpty(document.forms[0].country,"<bean:message bundle="CM" key="cm.country"/>",errormsg);		
//		return false;
//	}	
//	else
//	{
//		if(! CheckQuotes(document.forms[0].country,"<bean:message bundle="CM" key="cm.country"/>"))
//		return false;
//	}
	
	if(!isAlphabetic(trimBetweenTrail(document.forms[0].country.value)))
	{		
		alert("<bean:message bundle="CM" key="cm.countryalpha"/>");
		document.forms[0].country.focus()			
		return false;
	}		

	return true; //CheckQuotes ();
}
</script>
<script>
function trimBetweenTrail(str){
nstr="";
for (var i=0;i<str.length;i++){
	if(str.substring(i,i+1)!=" ")
	nstr=nstr+str.substring(i,i+1);
	}
	return nstr;

}

function removeHiffen(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!="-")	nstr=nstr+str.substring(i,i+1);
	}// END OF FOR
	return nstr;
}

function trimFields()
{

	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>


function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}

function del() 
{
		
		if(<%= (((Integer)request.getAttribute("element1")).intValue()!=0) %>)
		{
			var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
			var convel = confirm( msg );
			if( convel )
			{
				document.forms[0].refresh.value = "false";
				document.forms[0].action = "AddDivison.do?function=delete&type=type&org_type=<%=type%>&org_discipline_id=<%= String.valueOf(((Integer)request.getAttribute("customer_id")).intValue())%>&element=<%= String.valueOf(((Integer)request.getAttribute("element1")).intValue())%>&refresh=false";				
				document.forms[0].submit();								
				return true;	
			}
			
		}
		else
		{
			alert( "<bean:message bundle="RM" key="rm.selectdeletion"/>" );
			
		}
}
	function chkCombo(obj,label){
	if(obj.value==0 || obj.value == null)	{	
		alert("<bean:message bundle="CM" key="cm.pleaseselect"/>"+label);	
		obj.focus();
		return false;
	}
	return true;
}

function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("<bean:message bundle="CM" key="cm.pleaseenter"/>"+label);	
		obj.focus();
		return false;
	}
	return true;
}

function chkInteger(obj,label){
	if(!isInteger(removeHiffen(obj.value)))	{	
		alert("Only numeric values are allowed in "+label);	
		obj.focus();
		return false;
	}
	return true;
}


</script>
</html:html>
