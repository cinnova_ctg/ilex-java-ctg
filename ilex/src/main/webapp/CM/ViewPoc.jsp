<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String table_label="";
String menu_head="";
String menu_label="";
String menu_label2="";
String js_blank_msg="";
String add_division="cm.adddivision";
String manage_org="";
String mfg_label="";
String select_label="";
String type_name="";
int comboflag=0;
int flag=0;
String label1 = "";
String label2 = "";
String label3 = "";
String label4 = "";
String orgName = "";
int loPcId = 0;
String empStatus = "";


if(request.getAttribute("loPcId") != null) {
	loPcId = Integer.parseInt(request.getAttribute("loPcId").toString());
}
if(request.getAttribute("orgName") != null) {
	orgName = request.getAttribute("orgName").toString();
}
if(request.getAttribute("empStatus") != null) {
	empStatus = request.getAttribute("empStatus").toString();
}
char type=((String)request.getAttribute("org_type")).trim().charAt(0);
String org_type=request.getParameter("org_type").trim();

switch(type){
case 'C':
{
	
	menu_head="cm.customermenuhead";		
	type_name="cm.customername";
		
	menu_label="cm.addnewtopcustomer";
	menu_label2="cm.deletenewtopcustomer";
    js_blank_msg="cm.entertopcustomer";
    manage_org="cm.managecustomer";
	select_label="cm.custcategory";
	mfg_label="cm.custsecondcategory";
	break;
}
case 'N':
{

	menu_head="cm.CNSmenuhead";
	type_name="cm.CNSname";	
	menu_label="cm.addnewtopCNS";
	menu_label2="cm.deletenewtopCNS";
	js_blank_msg="cm.entertopCNS";
	manage_org="cm.manageCNS";
	select_label="cm.cnscategory";
	mfg_label="cm.cnssecondcategory";
	
	break;
}
case 'P':
{

	menu_head="cm.PVSmenuhead";
	type_name="cm.PVSname";	
	menu_label="cm.addnewtopPVS";
	menu_label2="cm.deletenewtopPVS";
	js_blank_msg="cm.entertopPVS";
	manage_org="cm.managePVS";
	select_label="cm.PVScategory";
	mfg_label="cm.PVSsecondcategory";
	
	break;
}

	default:
	{
		
		menu_label="rm.maddmanufacturers";
		menu_label2="rm.mdeletemanufacturers";


		select_label="rm.mecategory";
		mfg_label="rm.msecondcategory";
		
	}

}
%>
<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<!-- <LINK href="styles/style.css" rel="stylesheet"
	type="text/css"> -->
<link href="css/ilex_top_menu.css" type="text/css" rel="stylesheet">
<link href="css/top_menu.css" type="text/css" rel="stylesheet">
<link href="css/adminmain.css" type="text/css" rel="stylesheet">
<link href="css/css.css" type="text/css" rel="stylesheet">
<link href="css/medius_header.css" type="text/css" rel="stylesheet">	
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<title></title>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
<%@ include  file="/Menu.inc" %>
 <script>
            WebFont.load({
                custom: {
                    families: ['CalibriRegular'],
                    urls: ['css/ilex_top_menu.css']
                }
            });
        </script>

<link rel="stylesheet" href="styles/style.css" type="text/css">
</head>

<html:form action="AddPoc" >
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" bgcolor="#FFFFFF">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
       <table width="775" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
          
        <!-- <td bgcolor="#DADACB" width=120 align=left><a href="#"    onMouseOut="MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu3',event)" class="drop">Add POC</a></td> -->
        
		  <td id="pop2" width="280" align="left" class="TopRow1"><html:link href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" onmouseover="MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" styleClass="drop">&nbsp;&nbsp;&nbsp;&nbsp;Manage POC</html:link></td>
		  <td width = "520" class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>
<SCRIPT language=JavaScript1.2>
<!--

if (isMenu) {


arMenu1=new Array(
140,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",

 <% 
 if(loPcId != 0 && org_type.trim().equalsIgnoreCase("N") && empStatus.trim().equalsIgnoreCase("0") && orgName.trim().equalsIgnoreCase("Corporate") ) {%>
		"<bean:message bundle="CM" key="cm.add"/>","javascript:alert('Addition of POC is not allowed from Terminated section.');",0,
  <%} else {%>
		"<bean:message bundle="CM" key="cm.add"/>","AddPoc.do?function=add&org_type=<bean:write name="AddPocForm" property="org_type"/>&org_discipline_id=<bean:write name="AddPocForm" property="org_discipline_id"/>&division_id=<bean:write name="AddPocForm" property="division_id"/>",0,
  <%}%>
"<bean:message bundle="CM" key="cm.update"/>","AddPoc.do?function=update&org_type=<bean:write name="AddPocForm" property="org_type"/>&org_discipline_id=<bean:write name="AddPocForm" property="org_discipline_id"/>&division_id=<bean:write name="AddPocForm" property="division_id"/>&lo_pc_id=<bean:write name="AddPocForm" property="lo_pc_id"/>",0
)
//"<bean:message bundle="CM" key="cm.delete"/>","AddPoc.do?function=delete&org_type=<bean:write name="AddPocForm" property="org_type"/>&org_discipline_id=<bean:write name="AddPocForm" property="org_discipline_id"/>&division_id=<bean:write name="AddPocForm" property="division_id"/>&lo_pc_id=<bean:write name="AddPocForm" property="lo_pc_id"/>",0
arMenu3=new Array()


  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}
//-->
</script>


<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="1" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" width="600" > 
	  
  <logic:present name = "AddPocForm" property="deletemessage">

	<logic:equal name="AddPocForm" property="deletemessage" value="0">	
  <tr><td  colspan="2" class="message" height="10" >		<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.deletedsuccess"/></td></tr>
	</logic:equal>
	
	<logic:equal name="AddPocForm" property="deletemessage" value="-9001">	
  <tr><td  colspan="2" class="message" height="10" >		<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/></td></tr>
	</logic:equal>

	<logic:equal name="AddPocForm" property="deletemessage" value="-9002">	
  <tr><td  colspan="2" class="message" height="10" >		<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.deletefailed"/></td></tr>
	</logic:equal>	
	
</logic:present>

<logic:present name="AddPocForm" property="addmessage">

	<logic:equal name="AddPocForm" property="addmessage" value="0">	
  <tr><td  colspan="2" class="message" height="10" >		<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.addedsuccessfully"/></td></tr>
	</logic:equal>

	<logic:equal name="AddPocForm" property="addmessage" value="-9002">	
  <tr><td  colspan="2" class="message" height="10" >		<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.insertfailed"/></td></tr>
	</logic:equal>
	
	<logic:equal name="AddPocForm" property="addmessage" value="-9003">	
  <tr><td  colspan="2" class="message" height="10" >		<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/></td></tr>
	</logic:equal>
	
</logic:present>

<logic:present name="AddPocForm" property="updatemessage">

	<logic:equal name="AddPocForm" property="updatemessage" value="0">  </td></tr>
  		<tr>
  			<td colspan="2" class="message" height="10" >
  				<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.updatesuccessfully"/>
  			</td>
  		</tr>
	</logic:equal>	

	<logic:equal name="AddPocForm" property="updatemessage" value="-9005">
  		<tr>
  			<td colspan="2" class="message" height="10" >
  				<bean:message bundle="CM" key="cm.updateerror"/>  
  			</td>
  		</tr>
	</logic:equal>

	<logic:equal name="AddPocForm" property="updatemessage" value="-9001">
  		<tr>
  			<td colspan="2" class="message" height="10" >
	  			<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.alreadyexists"/>  
  			</td>
  		</tr>
	</logic:equal>
	
	<logic:equal name="AddPocForm" property="updatemessage" value="-1">  </td></tr>
  		<tr>
  			<td colspan="2" class="message" height="10" >
  				<bean:message bundle="CM" key="cm.poc"/>&nbsp;<bean:message bundle="CM" key="cm.updatesuccessfully"/>
  			</td>
  		</tr>
	</logic:equal>	
		
</logic:present>


  
  <logic:present name = "chkrole" scope = "request">
	  <logic:equal name = "chkrole" value = "1">
    	<tr><td  colspan = "2" class = "message" height = "30">
	    		<bean:message bundle = "CM" key = "cm.already.role"/>
		</td></tr>
	</logic:equal>  
	<logic:equal name = "chkrole" value = "2">
    	<tr><td  colspan = "2" class = "message" height = "30">
	    		<bean:message bundle = "CM" key = "cm.assigned.role"/>
		</td></tr>
	</logic:equal>  
  </logic:present>
  

  <tr> 
    <td colspan="2" class="labeleboldwhite" height="30"  width=45%><bean:message bundle="CM" key="cm.viewpoc"/>&nbsp;<bean:write name="AddPocForm" property="org_name"/>/<bean:write name="AddPocForm" property="division_name"/></td>
  </tr> 
    <td  class="labelobold" height="25" width=45%><bean:message bundle="CM" key="<%=type_name%>"/></td>
    <td  class="labelo"><bean:write name="AddPocForm" property="org_name"/></td>
  </tr>
  
  <tr> 
    <td  class="labelebold" height="25"><bean:message bundle="CM" key="cm.divison/location"/></td>
    <td  class="labele"><bean:write name="AddPocForm" property="division_name"/></td>
   </tr>
  
   <tr> 
    <td  class="labelobold" height="25"><bean:message bundle="CM" key="cm.firstname"/></td>
    <td  class="labelo"><bean:write name="AddPocForm" property="firstname"/></td>
   </tr>

   <tr> 
    <td  class="labelebold" height="25"><bean:message bundle="CM" key="cm.lastname"/></td>
    <td  class="labele"><bean:write name="AddPocForm" property="lastname"/></td>
   </tr>
   
   <c:choose>
    		<c:when test="${AddPocForm.org_type eq 'N'}">
			    <tr> 
				<td class = "labelobold" height="25"><bean:message bundle="CM" key="cm.department"/></td>
				<td class = "labelo">
				    <html:select  name = "AddPocForm" property="classificationId" size="1" styleClass="comboo" disabled="true">
					<html:optionsCollection name = "dcClassification"  property = "firstlevelcatglist"  label = "label"  value = "value" />
					</html:select>   
				</td>
			  </tr>   
			 <tr> 
			    <td  class="labelebold" height="25"><bean:message bundle="CM" key="cm.rank"/></td>
			    <td  class="labele"><bean:write name="AddPocForm" property="rank"/></td>
	  		</tr>
				  <tr>
				 <td   width = "300" class="labelobold" height="25"><bean:message bundle="CM" key="cm.invoice.approval"/></td>
				 <td   width = "300" class="labelo"><html:radio name="AddPocForm" property="invoiceApproval" value = "Approval Required" disabled="true"><bean:message bundle="CM" key="cm.invoice.appreq"/></html:radio>
				 		&nbsp;&nbsp;&nbsp;&nbsp;<html:radio name="AddPocForm" property="invoiceApproval" value="Approval Authority" disabled="true"><bean:message bundle="CM" key="cm.invoice.apprauth"/></html:radio></td>
			 </tr>
			    <tr> 
				    <td  class="labelebold" height="25"><bean:message bundle="CM" key="cm.title"/></td>
				    <td  class="labele"><bean:write name="AddPocForm" property="title"/></td>
				  </tr>
            </c:when>
            <c:otherwise>
              <tr> 
				    <td  class="labelobold" height="25"><bean:message bundle="CM" key="cm.title"/></td>
				    <td  class="labelo"><bean:write name="AddPocForm" property="title"/></td>
				  </tr>
            </c:otherwise>
    </c:choose>    	
 
<% if(type == 'P')
{
	label1 = "labelebold";
	label2 = "labele";
	label3 = "labelobold";
	label4 = "labelo";
}
else
{
	label1 = "labelobold";
	label2 = "labelo";
	label3 = "labelebold";
	label4 = "labele";
}
%>
	<logic:match name = "AddPocForm" property ="org_type" value="P">
	<tr>
	 <td   width = "300" class="<%=label1%>" height="25"><bean:message bundle = "CM" key = "cm.pvsContact" /></td>
	 <td   width = "300" class="<%=label2%>">
	 		<logic:equal name="AddPocForm" property="pvsContact" value="P">Yes</logic:equal>
		    <logic:equal name="AddPocForm" property="pvsContact" value="S">NO</logic:equal>
	</td>
	</tr>
	</logic:match>
	
	<c:choose>
    		<c:when test="${AddPocForm.org_type eq 'N'}">
    		<% label1 = "labelebold";
	label2 = "labele";
	label3 = "labelobold";
	label4 = "labelo";%>
	</c:when>
	</c:choose>
 <tr> 
    <td  class="<%=label3%>" height="25"><bean:message bundle="CM" key="cm.firstaddressline"/></td>
    <td  class="<%=label4%>"><bean:write name="AddPocForm" property="firstaddressline"/></td>
  </tr>

    <tr> 
    <td  class="<%=label1%>" height="25"><bean:message bundle="CM" key="cm.secondaddressline"/></td>
    <td  class="<%=label2%>"><bean:write name="AddPocForm" property="secondaddressline"/></td>
  </tr>

   <tr> 
    <td  class="<%=label3%>" height="25"><bean:message bundle="CM" key="cm.city"/></td>
    <td  class="<%=label4%>"><bean:write name="AddPocForm" property="city"/></td>
  </tr>

  <tr> 
    <td  class="<%=label1%>" height="25"><bean:message bundle="CM" key="cm.state/province"/></td>
    <td  class="<%=label2%>"><bean:write name="AddPocForm" property="state"/></td>
  </tr>

  <tr> 
    <td  class="<%=label3%>" height="25"><bean:message bundle="CM" key="cm.zip/postalcode"/></td>
    <td  class="<%=label4%>"><bean:write name="AddPocForm" property="zip"/></td>
  </tr>

  <tr> 
    <td  class="<%=label1%>" height="25"><bean:message bundle="CM" key="cm.country"/></td>
    <td  class="<%=label2%>"><bean:write name="AddPocForm" property="country"/></td>
  </tr>
 <c:choose>
    		<c:when test="${AddPocForm.org_type ne 'N'}">
   <tr> 
    <td class = "<%=label3%>" height="25">Time Zone</td>
	    <td class = "<%=label4%>"><bean:write name="AddPocForm" property="timeZoneName"/></td>
   </tr>
   </c:when>
   </c:choose>

<c:choose>
    		<c:when test="${AddPocForm.org_type eq 'N'}">
    		<%label1 = "labelobold";
    		label2 = "labelo";
    		label3 = "labelebold";
    		label4 = "labele"; %>
    		</c:when>
    		
 </c:choose>
   <tr> 
    <td  class="<%=label1%>" height="25"><bean:message bundle="CM" key="cm.phone1"/></td>
    <td  class="<%=label2%>"><bean:write name="AddPocForm" property="phone1"/></td>
  </tr>

  <tr> 
    <td  class="<%=label3%>" height="25"><bean:message bundle="CM" key="cm.phone2"/></td>
    <td  class="<%=label4%>"><bean:write name="AddPocForm" property="phone2"/></td>
  </tr>

<tr> 
    <td  class="<%=label1%>" height="25"><bean:message bundle="CM" key="cm.fax"/></td>
    <td  class="<%=label2%>"><bean:write name="AddPocForm" property="fax"/></td>
  </tr>

<tr> 
    <td  class="<%=label3%>" height="25"><bean:message bundle="CM" key="cm.email1"/></td>
    <td  class="<%=label4%>"><bean:write name="AddPocForm" property="email1"/></td>
  </tr>

 <tr> 
    <td  class="<%=label1%>" height="25"><bean:message bundle="CM" key="cm.email2"/></td>
    <td  class="<%=label2%>"><bean:write name="AddPocForm" property="email2"/></td>
  </tr>
  

<tr> 
    <td  class="<%=label3%>" height="25"><bean:message bundle="CM" key="cm.mobile"/></td>
    <td  class="<%=label4%>"><bean:write name="AddPocForm" property="mobile"/></td>
  </tr>
<%if(org_type != null && !org_type.equals("P")) {%>
 <tr> 
    <td  class="<%=label1%>" height="25"><bean:message bundle="CM" key="cm.username"/></td>
    <td  class="<%=label2%>"><bean:write name="AddPocForm" property="user_name"/></td>
  </tr>
  

<tr> 
    <td  class="<%=label3%>" height="25"><bean:message bundle="CM" key="cm.password"/></td>
    <td  class="<%=label4%>"><bean:write name="AddPocForm" property="password"/></td>
  </tr>
<tr>
	 <td   width = "300" class="<%=label1%>" height="25">Employment Status</td>
	 <td   width = "300" class="<%=label2%>"><html:radio name="AddPocForm" property="empStatus" value = "1" disabled="true">Active</html:radio>
	 		&nbsp;&nbsp;&nbsp;&nbsp;<html:radio name="AddPocForm" property="empStatus" value="0" disabled="true">Terminated</html:radio>
	 		&nbsp;&nbsp;&nbsp;&nbsp;<html:radio name="AddPocForm" property="empStatus" value="2" disabled="true">Test</html:radio>
	 		&nbsp;&nbsp;&nbsp;&nbsp;<html:radio name="AddPocForm" property="empStatus" value="3" disabled="true">Temp</html:radio>
	 		&nbsp;&nbsp;&nbsp;&nbsp;<html:radio name="AddPocForm" property="empStatus" value="4" disabled="true">LOA</html:radio>
	 		</td>
 </tr>
 <c:choose>
    		<c:when test="${AddPocForm.org_type ne'N'}">
			 <tr> 
				<td class = "<%=label3%>" height="25">Classification</td>
				<td class = "<%=label4%>">
				    <html:select  name = "AddPocForm" property="classificationId" size="1" styleClass="comboo" disabled="true">
					<html:optionsCollection name = "dcClassification"  property = "firstlevelcatglist"  label = "label"  value = "value" />
					</html:select>   
				</td>
			</tr>   
			</c:when>
</c:choose>
<%} %>
<logic:present name = "roleview" scope = "request">
<c:choose>
    		<c:when test="${AddPocForm.org_type eq'N'}">
				<tr> 
				    <td  class="<%=label3%>" height="25"><bean:message bundle="CM" key="cm.roleassign"/></td>
				    <td  class="<%=label4%>" height="25"><%=(String)request.getAttribute("roleview")%></td>
 				</tr>   
 				</c:when>
 			<c:otherwise>
 			  <tr> 
				    <td  class="<%=label1%>" height="25"><bean:message bundle="CM" key="cm.roleassign"/></td>
				    <td  class="<%=label2%>" height="25"><%=(String)request.getAttribute("roleview")%></td>
 				</tr>   
 			</c:otherwise>	 
 </c:choose> 
 
</logic:present>    

<jsp:include page = '../Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'cmViewPoc'/>
</jsp:include>
</table>
 </td>
 </tr> 
</table>
<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present> 
</BODY>
</html:form>
</html:html>

<script>

function setSearch()
{
	document.forms[0].type.value=trim(document.forms[0].type.value);
	document.forms[0].division.value=trim(document.forms[0].division.value);
	document.forms[0].poc.value=trim(document.forms[0].poc.value);		
	if(isBlank(document.forms[0].type.value)&& isBlank(document.forms[0].division.value)&& isBlank(document.forms[0].poc.value)){
		if(! chkBlank(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;
	}
//	if(! chkBlank(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;
	if(! CheckQuotes(document.forms[0].type,"<bean:message bundle="CM" key="cm.searchtype"/>"))return false;	
//	if(! chkBlank(document.forms[0].division,"<bean:message bundle="CM" key="cm.searchdivision"/>"))return false;		
	if(! CheckQuotes(document.forms[0].division,"<bean:message bundle="CM" key="cm.searchdivision"/>"))return false;
	if(! CheckQuotes(document.forms[0].poc,"<bean:message bundle="CM" key="cm.searchpoc"/>"))return false;
	var org_type="<bean:write name="AddPocForm" property="org_type"/>";
	document.forms[0].action = "Search.do?org="+org_type+"&type="+document.forms[0].type.value+"&division="+document.forms[0].division.value;
	document.forms[0].submit();								
	return true;	
}

function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		obj.focus();
		return false;
	}
	return true;
}



function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}
</script>
