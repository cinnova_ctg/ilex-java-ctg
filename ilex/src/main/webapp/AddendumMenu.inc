<%
	String Job_Id = "";
	String addendumStatus ="";
	String job_Status ="";
	String jobViewtype ="";
	
	if(request.getAttribute("Job_Id")!=null)
		Job_Id= request.getAttribute("Job_Id").toString();
		
	if(request.getAttribute("job_Status")!=null)
		job_Status= request.getAttribute("job_Status").toString();
		
	if(request.getAttribute("jobViewtype")!=null)
		jobViewtype= request.getAttribute("jobViewtype").toString();	
	
%>

<script>
function addendumSendEmail() {
	document.forms[0].action = "EmailAction.do?Type=PRMAppendix&Appendix_Id=<%=Appendix_Id%>&jobId=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;	
}	

function addendumManageStatus(v) {
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_job&id=<%=Job_Id%>";
	document.forms[0].submit();
	return true;	
}

function addendumViewContractDocument(id, docType) {
	if(docType=='html')   document.forms[0].target = "_blank";
	else   document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+id+"&View="+docType+"&Type=Appendix&from_type=PRM&Id=<%=Appendix_Id%>";
	document.forms[0].submit();
}

function addendumSendStatusReport() {
	document.forms[0].action = "EmailAction.do?Type=prm_job&Type1=prm_job&&id=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;
}

function addendumViewComments() {
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}

function addendumAddComment() {
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Appendix_Id=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}


</script>