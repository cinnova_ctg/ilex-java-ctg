<%
String Activity_Id = "";
String checknetmedx = "";
String fromflag = "";
String jobid = "";
String activityTypeForJob = "";
String refForJob = "";
String jobtype = "";
String activityStatus = "";
String activityType = "";
String Activitylibrary_Id = "";
String chkaddendumresource = "";

if( request.getAttribute( "Activity_Id" ) != null )
		Activity_Id = ( String ) request.getAttribute( "Activity_Id" );

if( request.getAttribute( "checknetmedx" ) != null )
		checknetmedx = ( String ) request.getAttribute( "checknetmedx" );

if( request.getAttribute( "jobtype" ) != null )
		jobtype = ( String ) request.getAttribute( "jobtype" );

if( request.getAttribute( "activityStatus" ) != null )
		activityStatus = ( String ) request.getAttribute( "activityStatus" );
		
if( request.getAttribute( "activityType" ) != null )
		activityType = ( String ) request.getAttribute( "activityType" );

if( request.getAttribute( "Activitylibrary_Id" ) != null )
		Activitylibrary_Id = ( String ) request.getAttribute( "Activitylibrary_Id" );

if( request.getAttribute( "chkaddendumresource" ) != null )
		chkaddendumresource = ( String ) request.getAttribute( "chkaddendumresource" );

if( request.getAttribute( "from" ) != null )
	{
		if(request.getAttribute( "jobid" )!=null){
			jobid = request.getAttribute( "jobid" ).toString();
		}
		from = request.getAttribute( "from" ).toString();
		fromflag =  request.getAttribute( "from" ).toString();
	}


if( request.getAttribute( "activityTypeForJob" ) != null ){
	activityTypeForJob = request.getAttribute( "activityTypeForJob" ).toString();

	if(activityTypeForJob.equals("detailactivity")||activityTypeForJob.equals("View")) 
		 refForJob = "detailjob";
	else
		 refForJob = "inscopejob";
}
%>

<script>
function activityDel() 
{
		<%if(activityStatus.equals("Approved")){%>
			alert( "<bean:message bundle = "pm" key = "activity.detail.approvedactivity"/>" );
			return false;
		<%}else{%>
		
			var convel = confirm( "<bean:message bundle = "pm" key = "activity.detail.deleteactivity"/>" );
			if( convel )
			{
				document.forms[0].action = "DeleteAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Activity_Id=<%= Activity_Id%>&Job_Id=<%= Job_Id %>&Type=Activity";
				document.forms[0].submit();
				return true;	
			}
		<%}%>
}
var str = '';

function activityAddComment()
{
	<%if( request.getAttribute( "from" ) != null ){
		fromflag = request.getAttribute( "from" ).toString(); %>
	
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Activity&Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>';
	
	<%}else{%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Activity&Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>';
	<%}%>

	document.forms[0].submit();
	return true;
}

function activityViewComment()
{
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Activity&Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>';
	document.forms[0].submit();
	return true;	
}
function viewResources(choice)
{
	<%if(checknetmedx.equals("1")) {%>
						  		document.forms[0].action = 'ResourceListAction.do?resourceListType='+choice+'&ref=View&type=PM&MSA_Id=<%=MSA_Id %>&Activity_Id=<%= Activity_Id %>&Activitylibrary_Id=<%= Activity_Id %>' ;
							 	<%}
						  		else
								{
									if(  request.getAttribute( "from" ) != null )
									{%>
										document.forms[0].action ='ResourceListAction.do?resourceListType='+choice+'&from=<%= from %>&jobid=<%= jobid %>&addendum_id=<%=addendum_id%>&ref=<%=chkaddendumresource%>&Activity_Id=<%= Activity_Id %>'; 
									<%}else{%>
										document.forms[0].action ='ResourceListAction.do?resourceListType='+choice+'&addendum_id=<%=addendum_id%>&ref=<%=chkaddendumresource%>&Activity_Id=<%= Activity_Id %>'; 
		<%}} %>
	document.forms[0].submit();
	return true;

}




</script>