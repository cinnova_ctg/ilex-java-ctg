<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<%
String tableData = "";
String  pageTitle = ""; 
String reportTitle= "";

tableData = request.getAttribute("tableData") == null ?"":request.getAttribute("tableData").toString();
pageTitle = request.getAttribute("pageTitle") == null ?"":request.getAttribute("pageTitle").toString();
if(pageTitle.equals("ISP vs Circuit Type")) {
	reportTitle = "Type";
} else if(pageTitle.equals("ISP vs Circuit IP")){
	reportTitle = "IP";
}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ISP Uptime versus</title>
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
</head>
<%
String backgroundclass="";
boolean csschooser = false;
int rowCount = 0;
%>
<body>


<html:form action="MSPISPUpTime.do">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<!-- Menu -->
			<jsp:include page="/MSP/MspTopMenu.jsp"/>
		</td>
	</tr>
	<tr>
		<!-- Bread Crumb -->
		<td background="images/content_head_04.jpg" height="21">
			<div id="breadCrumb">
			 	<a href="#" class="bgNone">MSP</a>
			 	<a>Reports</a>
			 	<a><span class="breadCrumb1"><%=pageTitle %></span></a>
			</div>
	    </td>
	</tr>
</table>
<%@include file="/MSP/MspTopMenuScript.inc" %>
<table border="0" cellpadding="0" cellspacing="0" style="padding-top: 0px;">
<tr>
	<td style="width: 10px;"></td>
	<td style="padding-top: 0px;">
		<table cellpadding="0" cellspacing="1" border="0" width="720">
			<tr>
				<td height="25"><h1 style="font-size: 14px; white-space: nowrap;">ISP Uptime % versus Circuit <%=reportTitle %></h1></td>
			</tr>
			<tr>
				<td><table style="border:2px solid #e8e8e5;"><tr><td><%=tableData%></td></tr></table></td>
			</tr>
		</table>
	</td>
</tr>
</table>

</html:form>
</body>
</html>

