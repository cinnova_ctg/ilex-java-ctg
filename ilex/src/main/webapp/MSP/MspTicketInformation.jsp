
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<link href="styles/base/ui.all.css" rel="stylesheet" type="text/css" />

<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/Core.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/jquery-ui-1.7.3.custom.min.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/jquery-ui-timepicker.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/AddJobInstallationNotes.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "MSP/MspJavaScript/MspManageOutage.js?timestamp="+(new Date()*1)></script>
  </script>
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/style_common.css" rel="stylesheet"	type="text/css">

<style type="text/css">
#slider{
	height: 4px; 
	border-color: #E0E0DC #E6E6E3 #8E8E8C #808080; 
	border-style: solid; 
	border-top-width: 2px; 
	border-right-width: 0px; 
	border-bottom-width: 0px; 
	border-left-width: 1px
}
.calImageStyle{
background-color: transparent;
border: none;
padding: 1px;
margin-top: 1px;
vertical-align: middle;
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; }
.ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }
}

</style>
<script>
$(document).ready(function(){
	$('#changeOwner').click(function() {
		openChangesTicketOwnerPopup();
	}); 
	
	
	$('#createTicketEffortStart').datetimepicker({
		dateFormat :"mm-dd-yy",
		showOn : "both",
		buttonImage :'${pageContext.request.contextPath}/images/calendar.gif'
		
	}).next(".ui-datepicker-trigger").addClass(
			"calImageStyle");
	$('#createTicketEffortStop').datetimepicker({
		dateFormat :"mm-dd-yy",
		showOn : "both",
		buttonImage :'${pageContext.request.contextPath}/images/calendar.gif'
		
	}).next(".ui-datepicker-trigger").addClass(
			"calImageStyle");
	
});
function openChangesTicketOwnerPopup() {
	blockUnblockTicketOwner();
	var uri="MspChangeTicketOwnerAction.do?ticket_id=<bean:write name='ManageOutageForm' property='ticketId'/>&jobid=<bean:write name='ManageOutageForm' property='jobId'/>&appendix_Id=<bean:write name='ManageOutageForm' property='appendixId'/>&timeStamp="+(new Date()*1)
	var uri=encodeURI(uri);
	$("#ticketOwnerItemValue").load(uri);
	$("#ticketOwnerItem").text('Change Ticket Owner for <bean:write name="ManageOutageForm" property="ticketNumber"/>, <bean:write name="ManageOutageForm" property="siteNumber"/>');
}
function blockUnblockTicketOwner() {
	
	$.blockUI(
		{ 
			message: $('#ticketOwnerModal'),
			css: { 
				width: '400px',
				height:'300px',
				top:'120px', 
			    left:'400px', 
			    position: 'fixed',
				color:'lightblue',
				'backgroundColor':'#fff'

				},
			overlayCSS:  { 
		        backgroundColor: '#f0f0f0', 
		        opacity:         0.3 
		    },
		    centerX: false,  
		    centerY: false,
		    fadeIn:  0,
		    fadeOut: 0
		}
		);
	
}
function closeChangeTicketOwnerPopup() {
    $.unblockUI(); 
	$("#ticketOwnerItemValue").empty().html('<img src="./images/waiting_tree.gif" />');
	document.getElementById("ticketOwnerItem").innerHTML = '';
   // return false; 
}
function  calculateRecordEffort(){
	var startDate = new Date($("#createTicketEffortStart").val());
	var stopDate = new Date($("#createTicketEffortStop").val());
	var dif = stopDate.getTime() - startDate.getTime();
	var differenceInMinutes = dif / (1000 * 60);
	if (!isNaN(differenceInMinutes)) {
		if (parseFloat(differenceInMinutes) >= 0) {
			$("#estimatedEffort").val(differenceInMinutes);
		}
	}
}


</script>
<title>Ticket Information</title>
</head>
<body>
<html:form action="/ManageOutage.do?">
<html:hidden property="jobId"/>
<html:hidden name="ManageOutageForm" property="installNotesPresent"/>
<html:hidden name="ManageOutageForm" property="NActiveMonitorStateChangeID"/>
<html:hidden name="ManageOutageForm" property="wugInstance"/>
<html:hidden name="ManageOutageForm" property="ticketId"/>
<html:hidden name="ManageOutageForm" property="escalationLevel"/>
<html:hidden name="ManageOutageForm" property="currentStatus"/>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<td>
		<h1 style="font-size: 11px;">Ticket Information and Status</h1>
	</td>
</tr>
<tr>
	<td colspan="10">
		<table  class="Ntextoleftalignnowrappaleyellowborder2" cellspacing="8" cellpadding="0" border="0"> 
			<tr>
			<td  class="dbvaluesmallFont">Ticket Number:</td>
			<td>
				<table border="0">
					<tr>
						<td class="dbvaluesmallFont"><bean:write name="ManageOutageForm" property="ticketNumber"/>&nbsp;&nbsp;&nbsp;&nbsp;(<a href="JobDashboardAction.do?jobid=<bean:write name='ManageOutageForm' property='jobId'/>&ticket_id=<bean:write name='ManageOutageForm' property='ticketId'/>&appendix_Id=<bean:write name='ManageOutageForm' property='appendixId'/>" styleClass="a">View Ticket Dashboard</a>)</td>
						<td class="buttonrow" align="right" style="background: #FAF8CC;" >
<%--						&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="changeOwner" id="changeOwner" class="button" value="Change Owner" />--%>
						
						</td>
					</tr>
				
				</table>
				</td>
			</tr>
		
		<tr>
			<td class="dbvaluesmallFont">Problem Category:</td>
			<td>
				<html:select name="ManageOutageForm" property="problemCategory" styleClass="select">
					<html:optionsCollection name="ManageOutageForm"  property="problemCategoryList"  label = "label"  value = "value" />
				</html:select>
			</td>
		</tr>
		
		<tr>
			<td class="dbvaluesmallFont">Problem Description:</td>
			<td><html:textarea name="ManageOutageForm" property="probleDescription" rows="4" cols="55" styleClass="textarea"/></td>
		</tr>
		
		<tr>
			<td class="dbvaluesmallFont">Next Action:</td>
			<td>
				<html:select name="ManageOutageForm" property="nextAction" styleClass="select">
					<html:optionsCollection name="ManageOutageForm" property="nextActionList"  label = "label"  value = "value" />
				</html:select>
				<input type="button" name="nextActionListUpdateBtn" class="button" value="Update" />
				<bean:write name="ManageOutageForm" property="nextActionMsg"/>
			</td>
		</tr>
		
		<tr>
			<td colspan="2" class="dbvaluesmallFont">Installation Notes:</td>
		
		</tr>
		<tr>
			<td colspan="2"><html:textarea name="ManageOutageForm" property="installationNotes" rows="6" cols="80" styleClass="textarea"/></td>
		</tr>
		
		<tr>
			<td class="dbvaluesmallFont">Recent Notes(<a id="installNotesCountDiv" href="ViewInstallationNotes.do?function=view&from=jobdashboard&from=Jobdashboard&jobid=<bean:write name='ManageOutageForm' property='jobId'/>&appendixid=<bean:write name='ManageOutageForm' property='appendixId'/>" ><bean:write name='ManageOutageForm' property='installNotesCount'/></a>)</td>
			<td class="dbvaluesmallFont" align="right"><html:button property="addNotes" styleClass="addButton_c">Add</html:button></td>
		</tr>
		<tr>
		<td colspan="2">
		<table cellpadding="0" cellspacing="0" border="0">
		<tr valign="bottom" >						
			<td width="420">
				<table width="100%" border="1" style="border-width:1px;border-collapse: collapse;" bordercolor="#D9DFEF"class="Ntextoleftalignnowrapblue" cellspacing="0" cellpadding="0">
					<tr >						
						<td width="32%"  valign="top" align="center" id="latestInstallNotesDate" class="dbvaluesmallFont" >
							<bean:write name="ManageOutageForm" property="latestInstallNotesDate"/>
						</td>								
						<td width="68%"  valign="top" align="left" id="latestInstallNotes"  class="dbvaluesmallFont" style="white-space: normal;">
							<c:out value='${ManageOutageForm.latestInstallNotes}' escapeXml="false"/>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr valign="top" style="padding-top: 4px;">	
			<td width="420">										
				<table width="100%" border="1" style="border-width:1px;border-collapse: collapse;" bordercolor="#D9DFEF" bgcolor="white"  class="Ntexteleftalignnowrapnoback"   cellspacing="0" cellpadding="0">
					<tr>						
						<td width="32%" align="center" valign="top" id="secondLatestInstallNotesDate" class="dbvaluesmallFont" >
							<bean:write name="ManageOutageForm" property="secondLatestInstallNotesDate"/>
						</td>								
						<td width="68%" align="left" valign="top" id="secondLatestInstallNotes" class="dbvaluesmallFont" style="white-space: normal;">
							<c:out value='${ManageOutageForm.secondLatestInstallNotes}' escapeXml="false"/>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
		</td>
		</tr>		
		</table>
	</td>
</tr>

<tr style="padding-top: 5px;">
	<td>
		<table cellpadding="0" cellspacing="0" border="0">
	 	<tr>
	 	<td>
		<h1 style="font-size: 11px;">Resolution and Actions</h1>
		</td>
		<c:if test="${not empty requestScope.updateResActionsFlag}">
	 	<c:if test="${requestScope.updateResActionsFlag eq '1'}">
 		<td id="hideMessage" style="color: #b3b2b0;font-size: 11px;font-weight: normal; white-space: nowrap; padding: 12px 0px 2px 0px; margin: 0px 0px 0px 0px;">
			&nbsp;- Resolution information was updated.
		</td>
		</c:if>
		<c:if test="${requestScope.updateResActionsFlag eq '0'}">
		<td id="hideMessage" style="color: red;font-size: 11px;font-weight: normal; white-space: nowrap; padding: 12px 0px 2px 0px; margin: 0px 0px 0px 0px;">
			&nbsp;- Update action failed.
		</td>
		</c:if>
		</c:if>
		</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table class="Ntextoleftalignnowrappaleyellowborder2" cellspacing="8" cellpadding="0" border="0" width="100%" id="updateResolution"> 
		<tr class="Ntextoleftalignnowrappaleyellowborder2"> 
			<td class="Ntextoleftalignnowrappaleyellowborder2">&nbsp;</td>
			<td class="buttonrow" align="right" style="background: #FAF8CC; padding-right: 8px;" >
				<input type="button" name="updateResolutionSection" class="button" value="Update" />
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont"><b>Record Effort:</b></td>
			<td></td>
		</tr>
		<tr>
			<td style="text-align: left;" colspan="2"><span style="padding-left:150px;" class="dbvaluesmallFont"><b>Start<b></b></span><span style="margin-left: 100px;"  class="dbvaluesmallFont"><b>Stop</b></span></td>
		</tr>
		<tr>
			<td colspan="2">
				<div  style="margin-right: 10px;float: left;">
					<html:text  styleId="estimatedEffort" name="ManageOutageForm" property="estimatedEffort"  style="width:30px;"/>&nbsp;Mins
				</div>
				<div style="margin-right: 20px;float: left;">
					<b>or</b>
				</div>
					<div  style="width: 150px;float: left;" ><input type="text" id="createTicketEffortStart" onchange="calculateRecordEffort();" style="width: 120px;"></div>
				<div><input type="text" id="createTicketEffortStop" onchange="calculateRecordEffort();" style="width: 120px;"></div>
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont" colspan="2"><input type="checkbox" name="overrideEfforts"/>Override Efforts</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont">Backup Circuit Status:</td>
			<td class="dbvaluesmallFont">
			<html:radio name="ManageOutageForm" property="backupCircuitStatus" value="1" >Up</html:radio>
			<html:radio name="ManageOutageForm" property="backupCircuitStatus" value="0">Down</html:radio>
			<html:radio name="ManageOutageForm" property="backupCircuitStatus" value="99">Not Applicable</html:radio>
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont">Issue Owner:</td>
			<td class="dbvaluesmallFont">
				<html:select name="ManageOutageForm" property="issueOwner" styleClass="select">
					<html:optionsCollection name="ManageOutageForm"  property="issueOwnerList"  label = "label"  value = "value" />
				</html:select>
			</td>
		</tr>
		
		<tr>
			<td class="dbvaluesmallFont">Resolution:</td>
			<td class="dbvaluesmallFont">
				<html:select name="ManageOutageForm" property="resolution" styleClass="select">
					<html:optionsCollection name="ManageOutageForm"  property="resolutionList"  label = "label"  value = "value" />
				</html:select>
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont">Root Cause:</td>
			<td class="dbvaluesmallFont">
				<html:select name="ManageOutageForm" property="rootCause" styleClass="select">
					<html:optionsCollection name="ManageOutageForm"  property="rootCauseList"  label = "label"  value = "value" />
				</html:select>
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont"></td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%">
				<tr>
					<td>
						<c:choose>
							<c:when test="${ManageOutageForm.disableButton eq true}">
								<html:button property="helpDesk" styleClass="addButton_c" style="width: 142px;" disabled="true">Resolved by Help Desk</html:button>
							</c:when>
							<c:otherwise>
								<html:button property="helpDesk" styleClass="addButton_c" style="width: 142px;">Resolved by Help Desk</html:button>
							</c:otherwise>
						</c:choose>
					</td>
					<td align="right">
						<c:choose>
							<c:when test="${ManageOutageForm.disableButton eq true}">
								<html:button property="mspDispatch" styleClass="addButton_c" style="width: 148px;" disabled="true">Escalate to MSP Dispatch</html:button>
							</c:when>
							<c:otherwise>
								<html:button property="mspDispatch" styleClass="addButton_c" style="width: 148px;">Escalate to MSP Dispatch</html:button>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

</html:form>
<div id="ticketOwnerModal"
	style="display: none; cursor: default; border: 1px; border-color: red">
<div style="border: 2px; background-color: blue">
<table   border='0' cellspacing="0" cellpadding="0"
	class="PUSSH0001" width="100%">
	<tr>
		<td align="left" id="ticketOwnerItem" 
			></td>
		<td align="right"><img height="15" width="15" alt="Close"
			src="images/delete.gif" id="closeShipping"
			onclick="closeChangeTicketOwnerPopup();"></td>
	</tr>
</table>
</div>
<div id="ticketOwnerItemValue" class="modalValue" align="center" style="padding- top: 4px; overflow: visible; height: 300px; overflow-x: auto; overflow-y: auto;"><img src="./images/waiting_tree.gif" /></div>

</div>
</body>
</html>