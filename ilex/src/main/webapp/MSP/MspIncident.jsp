<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%
String backgroundclass="";
boolean csschooser = false;
String manageDay = "";
String multiDays = "";
%>
</head>
<body>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>
		<h1 style="font-size: 14px;">Incident - Help Desk</h1>
	</td>
</tr>
<tr style="padding-top: 2px;">
<td>
<table border="0" cellspacing="0" cellpadding="2" class="BlueTab">
  <tr>
    <td>
       <table border="0" cellpadding="0" cellspacing="1">
       <tr>
           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;Status&nbsp;&nbsp;&nbsp;</b></td>
           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;Next Action&nbsp;&nbsp;&nbsp;</b></td>
           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
           <td class="texthdNoBorder"><b>&nbsp;Duration&nbsp;</b></td>
           <td class="texthdNoBorder"><b>Up/Down</b></td>
            <td class="texthdNoBorder"><b>Last Action</b></td>
          <td class="texthdNoBorder" style="width: 70px;"><b>Ticket #</b></td>
        </tr>
        <logic:iterate name="MspIncidentForm" id="incidentList" property="incidentList">  
            <%
            if(csschooser == true){
            	csschooser = false;
				backgroundclass = "Ntexteleftalignnowrap";
			} else{
				csschooser = true;	
				backgroundclass = "Ntextoleftalignnowrap";
			}
            %>
         <bean:define id="eventDay" name="incidentList" property="eventDay" type = "java.lang.String" />
         <bean:define id="multipleDays" name="incidentList" property="multipleDays" type = "java.lang.String" />
         <% multiDays = multipleDays; %>
         <%if(!manageDay.equals(eventDay) && multiDays.equals("1")){ %>
         <tr>
          	<td colspan="7" class="texthdNoBorder" style="text-align: left;"><bean:write name="incidentList" property="eventDay"/></td>
         </tr>
            <%} manageDay = eventDay;%>
       	 <tr>
         	<td class="<%=backgroundclass %>"><a href="ManageOutage.do?&NActiveMonitorStateChangeID=<bean:write name='incidentList' property='NActiveMonitorStateChangeID'/>&wugInstance=<bean:write name='incidentList' property='wugInstance'/>&escalationLevel=<bean:write name='MspIncidentForm' property='escalationLevel'/>"><bean:write name="incidentList" property="displayName"/></a></td>
         	<td class="<%=backgroundclass %>" align="center"><bean:write name="incidentList" property="currentStatus"/></td>
         	<td class="<%=backgroundclass %>" align="center"><bean:write name="incidentList" property="nextAction"/></td>
          	<td class="<%=backgroundclass %>" align="center"><bean:write name="incidentList" property="eventTime"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="incidentList" property="outageDuration"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="incidentList" property="eventCount"/></td>
	        <td class="<%=backgroundclass %>"><bean:write name="incidentList" property="lastAction"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="incidentList" property="ticketReference"/></td>
         </tr>     
        </logic:iterate>
        </table>
     </td>
  </tr>
  <tr><td><font style="line-height: 4px;">&nbsp;</font></td></tr>
</table>
</td>
</tr>
</table>
</body>
</html>