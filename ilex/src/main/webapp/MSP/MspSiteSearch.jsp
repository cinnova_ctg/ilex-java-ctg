<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>MSP Site Search</title>
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language="JavaScript" src="MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>	
	<script language="JavaScript" src="MSP/MspJavaScript/SiteSearch.js?timestamp="+(new Date()*1)></script>	
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/style_common.css" rel="stylesheet" type="text/css" />
	<link href="styles/dropdownStyle.css" rel="stylesheet" type="text/css" />
</head>
<%
String backgroundclass="";
boolean csschooser = false;
int rowCount = 0;
%>
<script>
function openSiteViewPopup(site_id,site_number) {
	var site_id = escape(encodeURI(site_id));
	var site_number=escape(encodeURI(site_number));
	blockUnblockUI();
	$("#siteViewItemValue").empty().html('<img src="./images/waiting_tree.gif" />');
	$("#siteViewBox").load("SiteDetail.do?hmode=sitedetail&siteId="+site_id+"&siteNumber="+site_number+"&timeStamp="+(new Date()*1));
	$("#siteViewBox").text('');
}
function openSiteTicketViewPopup(customer_id,site_id,site_number) {
	var customer_id = escape(encodeURI(customer_id));
	var site_number=escape(encodeURI(site_number));
	var site_id=escape(encodeURI(site_id));
	blockUnblockUI1();
	$("#siteTicketItemValue").empty().html('<img src="./images/waiting_tree.gif" />');
	$("#siteTicketItemValue").load("SearchHistory.do?hmode=searchSiteTicket&siteId="+site_id+"&siteNumber="+site_number+"&customerId="+customer_id+"&timeStamp="+(new Date()*1));
	$("#siteViewTicketBox").text('');
}
function closesiteTicketViewBoxPopup() {
    $.unblockUI(); 
    $("#siteTicketItemValue").empty().html('<img src="./images/waiting_tree.gif" />');
    document.getElementById("siteticketItem").innerHTML = '';	
   // return false; 
}
function blockUnblockUI() {
	$.blockUI(
		{ 
			message: $('#siteViewBox'),
			css: { 
				width: 'auto',
				height:'auto',
				color:'lightblue' 
					
				},
			overlayCSS:  { 
		        backgroundColor: '#f0f0f0', 
		        opacity:         0.3 
		    },
		    centerX: true,  
		    centerY: true,
		    fadeIn:  0,
		    fadeOut: 0
		}
		);
}
function blockUnblockUI1() {
	$.blockUI(
		{ 
			message: $('#siteTicketViewBox'),
			css: { 
				width: 'auto',
				height:'auto',
				color:'lightblue' 
					
				},
			overlayCSS:  { 
		        backgroundColor: '#f0f0f0', 
		        opacity:         0.3 
		    },
		    centerX: true,  
		    centerY: true,
		    fadeIn:  0,
		    fadeOut: 0
		}
		);
}
</script>
<body>
<c:choose>
	<c:when test="${fromHD}">
		<jsp:include page="/MSP/hdMspBreadCrumb.jsp"/>
	</c:when>
<c:otherwise>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<!-- Menu -->
			<jsp:include page="/MSP/MspTopMenu.jsp"/>
		</td>
	</tr>
	<tr>
		<!-- Bread Crumb -->
		<td background="images/content_head_04.jpg" height="21">
			<div id="breadCrumb">
			 	<a href="#" class="bgNone">MSP</a>
			 	<a>Site</a>
			 	<a><span class="breadCrumb1">Search for Site</span></a>
			</div>
	    </td>
	</tr>
</table>
<%@include file="/MSP/MspTopMenuScript.inc" %>
</c:otherwise>
</c:choose>

<html:form action="MspSiteSearch.do">
	<c:choose>
		<c:when test="${fromHD}">
			<html:hidden property="fromHD" value="true"></html:hidden>
		</c:when>
	</c:choose>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td style="width: 10px;"></td>
<td style="padding-top: 5px;">
	<table cellpadding="0" cellspacing="4" border="0">
	<tr>
		<td><font size="2">Customer:</font></td>
		<td>
			<html:select name="MspSiteSearchForm" property="customer" styleClass="select">
				<html:optionsCollection name="MspSiteSearchForm" property="customerList"/>
			</html:select>
		</td>
		<td width="30px"></td>
		<td ><font size="2">Site Number:</font></td>
		<td class="labelNoColorBold">
			<html:text name="MspSiteSearchForm" property="siteNumber" styleClass="text" size="30"/>
		</td>	
	</tr>
	<tr>
		<td><font size="2">Address:</font></td>
		<td class="labelNoColorBold">
		<html:text name="MspSiteSearchForm" property="address" styleClass="text" size="30"/>
		</td>
		<td width="30px"></td>
		<td><font size="2">DSL Number </font></td>
		<td class="labelNoColorBold">
		<html:text name="MspSiteSearchForm" property="siteInternetLineNumber" styleClass="text" size="30"/>
		</td>
		<td width="30px"></td>
	</tr>
	<tr>
		<td><font size="2">City:</font></td>
		<td class="labelNoColorBold">
		<html:text name="MspSiteSearchForm" property="city" styleClass="text" size="30"/>
		</td>
		<td width="30px"></td>
		<td><font size="2">Phone Number:</font></td>
		<td class="labelNoColorBold">
		<html:text name="MspSiteSearchForm" property="sitePhoneNumber" styleClass="text" size="30"/>
		</td>
	</tr>
	<tr>
	<td><font size="2">Zip Code:</font></td>
		<td class="labelNoColorBold">
			<html:text name="MspSiteSearchForm" property="zipcode" styleClass="text" size="20"/>
		</td>
		<td width="30px"></td>
		<td><font size="2">Account Number:</font></td>
		<td class="labelNoColorBold">
		<html:text name="MspSiteSearchForm" property="accountNumber" styleClass="text" size="30"/>
		</td>
	</tr>
	<tr>
	<td class="labelNoColorBold"></td>
	<td>
		<table>
			<tr>
				<td class="labelNoColorBold"><html:button property="search" styleClass="button_c">&nbsp;Search&nbsp;</html:button></td>
				<td class="labelNoColorBold"><html:reset property="cancel" styleClass="button_c">&nbsp;Cancel&nbsp;</html:reset></td>
			</tr>
		</table>
	</td>
		
	</tr>
	</table>
</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="padding-left: 10px;">
<tr>
	<td>&nbsp;</td>
</tr>
<tr style="padding-top: 2px;">
<td>
<table border="0" cellspacing="0" cellpadding="2" class="BlueTab">
  <tr>
    <td>
       <table border="0" cellpadding="0" cellspacing="1">
       <tr>
           <td class="testhdNoBorderLeft"><b>Site Number</b></td>
           <td class="testhdNoBorderLeft" ><b>Address</b></td>
           <td class="testhdNoBorderLeft" ><b>City</b></td>
           <td class="texthdNoBorder"><b>State</b></td>
           <td class="texthdNoBorder" style="width: 70px;"><b>Actions</b></td>
        </tr>
        <%
        String flag=(String)session.getAttribute("flag");        
        %>
        <logic:iterate name="MspSiteSearchForm" id="searchList" property="searchList">  
        
            <%
            ++rowCount;
			if(rowCount > 50){break;}
            if(csschooser == true){
            	csschooser = false;
				backgroundclass = "Ntexteleftalignnowrap";
			} else{
				csschooser = true;	
				backgroundclass = "Ntextoleftalignnowrap";
			}
            %>
       	 <tr>
            <td class="<%=backgroundclass %>" align="left"><bean:write name="searchList" property="site_number"/></td>
          	<td class="<%=backgroundclass %>" align="left"><bean:write name="searchList" property="site_address"/></td>
	        <td class="<%=backgroundclass %>" align="left"><bean:write name="searchList" property="site_city"/></td>
	        <td class="<%=backgroundclass %>" align="center"><bean:write name="searchList" property="site_state"/></td>
        	<td class="<%=backgroundclass %>" align="left"><a href="javascript:openSiteViewPopup('<bean:write name="searchList" property="site_id"></bean:write>','<bean:write name="searchList" property="site_number"></bean:write>')">View Details</a>  |  <a href="javascript:openSiteTicketViewPopup('<bean:write name="searchList" property="site_customer"></bean:write>','<bean:write name="searchList" property="site_id"></bean:write>','<bean:write name="searchList" property="site_number"></bean:write>')">Find Tickets</a></td>
        	
     
         </tr>
        </logic:iterate>
        <%if(rowCount == 0 && flag=="1") {%>     
         <tr>
         	<td colspan="5" class="labelNoColor">
         <font color="gray"> The search criteria did not match any Sites. Verify the criteria used and correct if necessary</font>	
         	</td>
         </tr>
         <%}%>
         <%
         if(rowCount >50)
         {%>
          <tr >
         	<td nowrap="nowrap" colspan="5" class="labelNoColor" style="padding-top: 10px"><font color="gray">
         	The search results are restrict to 50 matches. If you do not see the desired site, narrow the results by adjusting your search criteria.</font></td>
         </tr>
         
        	<% 
         } 
         %>
         
        </table>
     </td>
  </tr>
  <tr><td><font style="line-height: 4px;">&nbsp;</font></td></tr>
</table>
</td>
</tr>
</table>
</html:form>
<%-- Following code will open Site Detail Modal. --%>
<div id="siteViewBox"
	style="display: none; cursor: default; border: 1px; border-color: red">
<div style="border: 2px; background-color: blue; width :400px">
<table border='0' cellspacing="0" cellpadding="0"
	class="Ntextoleftalignnowrappaleyellowborder2" width="100%" >
	
</table>
</div>
<div id="siteViewItemValue" class="modalValue" style="padding- top: 20px; overflow: visible; height: 250px; overflow-x: auto; overflow-y: auto;"><img src="./images/waiting_tree.gif" /></div>

</div>

<div id="siteTicketViewBox"
	style="display: none; cursor: default; border: 1px; border-color: red">
<div style="border: 2px; background-color: blue; width :400px">
<table border='0' cellspacing="0" cellpadding="0"
	class="Ntextoleftalignnowrappaleyellowborder2" width="100%" >
	<tr>
	<td align="left" id="siteticketItem" style= "padding-left:8px;  padding-top: 1px">
	<h1 style="font-size: 11px; padding-top: 2px;">Site Tickets Information</h1>
	</td>
	<td align="right"><img height="15" width="15" alt="Close"
			src="images/delete.gif" id="closeShipping"
			onclick="closesiteTicketViewBoxPopup();"></td>
			</tr>
</table>
</div>
<div id="siteTicketItemValue" class="modalValue"
	style="padding-top: 4px; overflow: visible; height: 250px; overflow-x: auto; overflow-y: auto;"><img
	src="./images/waiting_tree.gif" /></div>
</div>
</body>
</html>
