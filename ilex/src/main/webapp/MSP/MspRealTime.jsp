
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JQueryMain.js"></script>
<%
String backgroundclass="";
boolean csschooser = false;
String manageDay = "";
int count = 0;
String rowsWarning = "";
String multiDays = "";

%>
</head>
<body>
<script>
realTimePageFlag = <bean:write name="RealTimeStateForm" property="pageFlag"/>;
</script>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
 <tr>
	 <td>
	 	<table cellpadding="0" cellspacing="0" border="0">
	 	<tr>
	 	<td>
		 	<h1 style="font-size: 14px; white-space: nowrap;">Initial Assessment - Managed Sites</h1>
	 	</td>
	 	<td id="hideMessage" style="color: #b3b2b0;font-size: 11px;font-weight: normal; white-space: nowrap; padding: 12px 0px 2px 0px; margin: 0px 0px 0px 0px;">
	 		<c:if test="${not empty sessionScope.mspMessage}">
				<c:out value="${sessionScope.mspMessage}"/>
			</c:if>
		</td>
		</tr>
		</table>
	 </td>
 </tr>
 <tr style="padding-top: 2px;">
    <td>
    <table border="0" cellspacing="2" cellpadding="0" class="BlueTab">
      <tr>
        <td>
        <table border="0" cellpadding="0" cellspacing="1">
          <tr>
              <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;</b></td>
              <td class="texthdNoBorder"><b>&nbsp;&nbsp;Status&nbsp;&nbsp;</b></td>
              <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
              <td class="texthdNoBorder"><b>Duration</b></td>
              <td class="texthdNoBorder"><b>Up/Down</b></td>
              <td class="texthdNoBorder"><b>Action</b></td>
              </tr>
              
              	<tr>
									<td class="texthdNoBorder" colspan="6"  style="text-align: right;">&nbsp;
											
									</td>
					</tr>
            <logic:iterate name="RealTimeStateForm" id="realTimeState" property="realTimeState">  
            <%
            ++count;
            if(csschooser == true){
            	csschooser = false;
				backgroundclass = "Ntexteleftalignnowrap";
			} else{
				csschooser = true;	
				backgroundclass = "Ntextoleftalignnowrap";
			}
            %>
            <bean:define id="eventDay" name="realTimeState" property="eventDay" type = "java.lang.String" />
            <bean:define id="rowsNotDisplayed" name="realTimeState" property="rowsNotDisplayed" type = "java.lang.String" />
            <bean:define id="multipleDays" name="realTimeState" property="multipleDays" type = "java.lang.String" />
            <bean:define id="previousNoAction" name="realTimeState" property="previousNoAction" type = "java.lang.Integer" />
            <% multiDays = multipleDays; %>
            <%if(!manageDay.equals(eventDay) && multiDays.equals("1")){ %>
            <tr>
            	<td colspan="6" class="texthdNoBorder" style="text-align: left;"><bean:write name="realTimeState" property="eventDay"/></td>
            </tr>
            <%} manageDay = eventDay;%>
            <tr>
              <td>
              <table border="0" cellspacing="0" cellpadding="0" width="100%">
                  <tr>
                    <td class="<%=backgroundclass %>"><bean:write name="realTimeState" property="displayName"/></td>
                    <td class="<%=backgroundclass %>" align="right">
                    <%if(previousNoAction.intValue() == 0) {%>
				  	<%}%>
				  	<%if(previousNoAction.intValue() == 1) {%>
				  		<img src="images/cau-yellow.gif" alt=""/>
				  	<%}%>
				  	<%if(previousNoAction.intValue() == 2) {%>
						<img src="images/cau.gif" alt=""/>				  		
				  	<%}%>
				  	<%if(previousNoAction.intValue() > 2) {%>
				  		<img src="images/cau-red.gif" alt=""/>
				  	<%}%>
                    </td>
                  </tr>
                </table></td>
              <td class="<%=backgroundclass %>" align="center"><bean:write name="realTimeState" property="currentStatus"/></td>
              <td class="<%=backgroundclass %>" align="center"><bean:write name="realTimeState" property="eventTime"/></td>
              <td class="<%=backgroundclass %>" align="right"><bean:write name="realTimeState" property="outageDuration"/></td>
              <td class="<%=backgroundclass %>" align="right"><bean:write name="realTimeState" property="eventCount"/></td>
              <td class="<%=backgroundclass %>" style="padding-right: 0px;" align="center">
              	<a href="#" class='RealTimeClickable' onclick="escalateConfirmation('./RealTimeState.do?hmode=escalate&nActiveMonitorStateChangeID=<bean:write name="realTimeState" property="NActiveMonitorStateChangeID"/>&wugInstance=<bean:write name="realTimeState" property="wugInstance"/>','realTimeState','./RealTimeState.do?');">Escalate</a>&nbsp;&nbsp;|
			     <%if(previousNoAction.intValue() > 2) {%>
			  		<a class='RealTimeClickable'>&nbsp;No Action&nbsp;&nbsp;</a>
			  	<%} else{%>         	
			  		<logic:equal name="realTimeState" property="currentStatus" value="Down">
	              		<a class='RealTimeClickable'>&nbsp;No Action&nbsp;&nbsp;</a>
	              	</logic:equal>
	              	<logic:notEqual name="realTimeState" property="currentStatus" value="Down">
	              	&nbsp;<a href="#" id="deleteAction" class='RealTimeClickable' onclick="ajaxCall('./RealTimeState.do?hmode=noAction&nActiveMonitorStateChangeID=<bean:write name="realTimeState" property="NActiveMonitorStateChangeID"/>&wugInstance=<bean:write name="realTimeState" property="wugInstance"/>','realTimeState','./RealTimeState.do?');disableLink();">No Action</a>&nbsp;&nbsp;
	              	</logic:notEqual>
			  	<%} %>
              </td>
              </tr>
              <% rowsWarning = rowsNotDisplayed;%>
              </logic:iterate>
             <%if(rowsWarning.equals("1")){%>
             <tr>
              	<td colspan="6">
	              	<table cellpadding="0" cellspacing="0" border="0" style="padding-top: 10px;">
	              	<tr valign="top">
				  	<td><img src="images/cau.gif" alt="" style="vertical-align: top;" hspace="5"/></td>
	    			<td class="labelNoColor">There are additional sites requiring attention. Only the most recent 25 sites are<br> display under this section.</td>
	  			  	</tr>
	  			  	</table>
  			  	</td>
  			 </tr>
  			 <%} else{%>
  			 <tr><td colspan="6"><font style="line-height: 4px;">&nbsp;</font></td></tr>
  			 <%} %>
          </table>
      </tr>
  </table>
</td>
</tr>
</table>
</body>
</html>