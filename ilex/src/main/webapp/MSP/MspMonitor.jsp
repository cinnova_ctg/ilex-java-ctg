<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="style/style.css" type="text/css" rel="stylesheet" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td id="TopLinkNavi">
    <div class="TopLinkNavig">Monitor</div><div class="TopLinkNavig">Sites</div><div class="TopLinkNavig">History</div><div class="TopLinkNavig">Reports</div><div class="TopLinkNavig">Exceptions</div>
    
    </td>
  </tr>
  <tr>
    <td id="TopLinkSubNavi"><div class="TopLinkSubNavig">MSP</div><div class="TopLinkSubNavig2ndLevel">Monitor</div><div class="TopLinkSubNavig3rdLevel">Current Status</div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><h2 style="font-size: 14px;padding-left: 0px;padding-bottom: 0px;">Monitoring Summary</h2>
    <br />
<br />
<table width="80%" border="0" cellspacing="0" cellpadding="0" id="YellowTab">
  <tr>
    <td><br />
      <br />
      <table width="99%" border="0" align="center" cellpadding="1" cellspacing="1">
        <tr>
          <td width="9%">&nbsp;</td>
          <td width="13%" class="GrayBgText">Active</td>
          <td width="13%" class="GrayBgText">Up</td>
          <td width="13%" class="GrayBgText">State Change</td>
          <td width="13%" class="GrayBgText">Help Desk</td>
          <td width="13%" class="GrayBgText">MSP Dispatch</td>
          <td width="13%" class="GrayBgText">Watch</td>
          <td width="13%" class="GrayBgText">Bounces</td>
          </tr>
        <tr>
          <td class="BlackTabText">Site</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="images/cau.gif" alt="" width="18" height="15" hspace="10" /></td>
                <td width="100%" class="WhiteBgText">XXX</td>
              </tr>
            </table></td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
        </tr>
        <tr>
          <td class="BlackTabText">Devices</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          <td class="WhiteBgText">XXX</td>
          </tr>
      </table></td>
  </tr>
</table>
<br />
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h1>Real-Time State Changes	- Initial Assessment</h1></td>
    <td><h1>Incident - Help Desk Action In work</h1></td>
  </tr>
  <tr>
    <td width="50%"><table width="96%" border="0" cellspacing="0" cellpadding="0" class="BlueTab">
      <tr>
        <td height="185"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
          <tr>
              <td width="13%" class="GrayBgText">Site</td>
              <td width="13%" class="GrayBgText">Status</td>
              <td width="10%" class="GrayBgText">Start</td>
              <td width="11%" class="GrayBgText">Duration</td>
              <td width="13%" class="GrayBgText">Up/Down</td>
              <td width="18%" class="GrayBgText">Action</td>
              </tr>
            <tr>
              <td bgcolor="#FFFFFF"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="1900%" class="WhiteBgText1">Site</td>
                    <td><img src="images/trang.gif" width="25" height="21" alt="" /></td>
                  </tr>
                </table></td>
              <td class="WhiteBgText2">Up</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText3"><a href="#">Escalate</a> | <a href="#">No Action</a></td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Down</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText3"><a href="#">Escalate</a> | <a href="#">No Action</a></td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Down</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText3"><a href="#">Escalate</a> | <a href="#">No Action</a></td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Down</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText3"><a href="#">Escalate</a> | <a href="#">No Action</a></td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Down</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText3"><a href="#">Escalate</a> | <a href="#">No Action</a></td>
              </tr>
          </table>
        <br />
        <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
    <td><img src="images/cau.gif" width="18" height="15" alt="" /></td>
    <td class="BodyText" width="100%">There are additional site requiring action. Note, only the most recent 25 are displayed at one time. If there was a network outrage, consider the &quot;Mass no Action&quot; Function.</td>
  </tr>
</table>
        <br /></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="20%" class="WhiteBgText1">Last Update</td>
            <td width="20%" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="100%" class="WhiteBgText">HH:MM</td>
                <td class="WhiteBgText"><img src="images/cau.gif" alt="" width="18" height="15" hspace="5" /></td>
              </tr>
            </table></td>
            <td width="15%" class="WhiteBgText">Event Count</td>
            <td width="10%" class="WhiteBgText1">XXX</td>
            <td width="15%" class="WhiteBgText">Site Errors</td>
            <td width="7%" class="WhiteBgText2">XXX</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td width="50%" valign="top"><table width="95%" border="0" cellspacing="0" cellpadding="0" class="BlueTab">
      <tr>
        <td><table width="100%" border="0" cellpadding="2" cellspacing="1">
          <tr>
              <td width="13%" class="GrayBgText">Site</td>
              <td width="13%" class="GrayBgText">Status</td>
              <td width="10%" class="GrayBgText">Start</td>
              <td width="11%" class="GrayBgText">Duration</td>
              <td width="13%" class="GrayBgText">Up/Down</td>
              <td width="18%" class="GrayBgText">Ticket#</td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Up</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgTicket">00001234</td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Down</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgTicket">00001234</td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Down</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgTicket">00001234</td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Down</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
           <td class="WhiteBgTicket">00001234</td>
              </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Up</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgTicket">00001234</td>
            </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Up</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgTicket">00001234</td>
            </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Up</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgTicket">00001234</td>
            </tr>
            <tr>
              <td class="WhiteBgText1">Site</td>
              <td class="WhiteBgText2">Up</td>
              <td class="WhiteBgText2">HH:MM</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgText">XXX</td>
              <td class="WhiteBgTicket">00001234</td>
            </tr>
            </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<br />
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h1>Bounces within Last 24 Hours</h1></td>
    <td><h1>Event - MSP Dispatch in Work</h1></td>
  </tr>
  <tr>
    <td width="50%"><table width="97%" border="0" cellspacing="0" cellpadding="0" class="BlueTab">
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
              <tr>
                <td width="13%" class="GrayBgText">Site</td>
                <td width="13%" class="GrayBgText">Status</td>
                <td width="10%" class="GrayBgText">Start</td>
              </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
                </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
                </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
                </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
                </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
                </tr>
            </table></td>
            <td><table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
              <tr>
                <td width="13%" class="GrayBgText">Site</td>
                <td width="13%" class="GrayBgText">Status</td>
                <td width="10%" class="GrayBgText">Start</td>
              </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
              </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
              </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
              </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
              </tr>
              <tr>
                <td class="WhiteBgText1">Site</td>
                <td class="WhiteBgText2">HH:MM</td>
                <td class="WhiteBgText">XXX</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td width="50%"><table width="95%" border="0" cellspacing="0" cellpadding="0" class="BlueTab">
      <tr>
        <td><table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
          <tr>
            <td width="13%" class="GrayBgText">Site</td>
            <td width="13%" class="GrayBgText">Status</td>
            <td width="10%" class="GrayBgText">Start</td>
            <td width="11%" class="GrayBgText">Duration</td>
            <td width="13%" class="GrayBgText">Up/Down</td>
            <td width="18%" class="GrayBgText">Ticket#</td>
          </tr>
          <tr>
            <td class="WhiteBgText1">Site</td>
            <td class="WhiteBgText2">Up</td>
            <td class="WhiteBgText2">HH:MM</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgTicket">00001234</td>
          </tr>
          <tr>
            <td class="WhiteBgText1">Site</td>
            <td class="WhiteBgText2">Down</td>
            <td class="WhiteBgText2">HH:MM</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgTicket">00001234</td>
          </tr>
          <tr>
            <td class="WhiteBgText1">Site</td>
            <td class="WhiteBgText2">Down</td>
            <td class="WhiteBgText2">HH:MM</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgTicket">00001234</td>
          </tr>
          <tr>
            <td class="WhiteBgText1">Site</td>
            <td class="WhiteBgText2">Down</td>
            <td class="WhiteBgText2">HH:MM</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgTicket">00001234</td>
          </tr>
          <tr>
            <td class="WhiteBgText1">Site</td>
            <td class="WhiteBgText2">Down</td>
            <td class="WhiteBgText2">HH:MM</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgText">XXX</td>
            <td class="WhiteBgTicket">00001234</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table></td>
  </tr>
</table>
</body>
</html>
