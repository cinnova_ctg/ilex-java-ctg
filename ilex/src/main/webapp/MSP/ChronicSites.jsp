<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>

<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />

<title>Chronic Sites Report</title>

<script type="text/javascript">
	$(document).ready(function() {
	
		$("input[name='view']").click(function(){ 
			if($("select[name='customerName']").val() == '0' || $("select[name='customerName']").val() == '') {
				alert('Select Customer for search.');
				return false;
			}
			
			$("form[name='ChronicSitesForm']").attr('action' , 'ChronicSites.do?hmode=view');
			$("form[name='ChronicSitesForm']").submit();
		});
		
		$("#tableData tbody #rowData:even").addClass("Ntexteleftalignnowrap");
		$("#tableData tbody #rowData:odd").addClass("Ntextoleftalignnowrap");
	});
</script>
</head>
<body>

<html:form action="ChronicSites.do">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<!-- Menu -->
			<jsp:include page="/MSP/MspTopMenu.jsp"/>
		</td>
	</tr>
	<tr>
		<!-- Bread Crumb -->
		<td background="images/content_head_04.jpg" height="21">
			<div id="breadCrumb">
			 	<a href="#" class="bgNone">MSP</a>
			 	<a>Reports</a>
			 	<a>Problem Sites</a>
			 	<a><span class="breadCrumb1">Dashboard</span></a>
			</div>
	    </td>
	</tr>
</table>
<%@include file="/MSP/MspTopMenuScript.inc" %>

<table cellpadding="0" cellspacing="0" border="0">
	<tr valign="top">
		<td width="10"></td>
		<td style="padding-top: 10px;">
			<table cellpadding="0" cellspacing="4" border="0">
				<tr valign="top">
					<td class="labelNoColorBold" style="width: 80px;vertical-align: top; padding-top: 3px;">Customer<font class="red">*</font>:</td>
					<td>
						<html:select name="ChronicSitesForm" property="customerName" styleClass="select" style="vertical-align: top;">
							<html:optionsCollection name="ChronicSitesForm" property="customerList"/>
						</html:select>
					</td>
					<td class="labelNoColorBold">&nbsp;&nbsp;</td>
					<td class="labelNoColorBold">
						<html:button property="view" styleClass="button_c" style="margin-top: 0px;">Go</html:button>
					</td>	
					<td class="labelNoColorBold">
						<html:reset property="cancel" styleClass="button_c" style="margin-top: 0px;">Reset</html:reset>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>	

<c:if test="${not empty requestScope.viewResult and requestScope.viewResult eq 'view'}">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td style="width: 10px;"></td>
			<td style="padding-top: 0px;">
				<table cellpadding="0" cellspacing="4" border="0">
					<tr>
						<td height="5"></td>
					</tr>
					<tr style="padding-top: 2px;">
						<td>
							<table border="0" cellspacing="0" cellpadding="2" class="BlueTab">
							  <tr>
							    <td>
							       <table border="0" cellpadding="0" cellspacing="1" id="tableData">
							       		<tr>
								           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Uptime %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								           <td class="texthdNoBorder"><b>&nbsp;Credits&nbsp;</b></td>
								        </tr>
								        <c:if test="${not empty ChronicSitesForm.chronicSiteList}">
								        	<c:forEach var="list" items="${ChronicSitesForm.chronicSiteList}" varStatus="count">
								        		<tr id="rowData">
								        			<td class="labelNoColor"><c:out value="${list.site}" /></td>
								        			<td class="labelNoColor" align="right"><c:out value="${list.upTime}" /></td>
								        			<td class="labelNoColor" align="right"><c:out value="${list.credits}" /></td>
								        		</tr>
								        	</c:forEach>
								        </c:if>
							       </table>
							     </td>
							   </tr>
							 </table>
						</td>
					</tr>		       
				</table>
			</td>	
		</tr>
	</table>
</c:if>		
</html:form>
</body>
</html>