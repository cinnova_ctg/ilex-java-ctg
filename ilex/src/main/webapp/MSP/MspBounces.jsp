<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%--
<html>
<%
	String backgroundclass="";
	boolean csschooser = false;
	int count = 1;
	int rowCount=0;
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td>
		<h1 style="font-size: 14px;">Bounces within Last 24 Hours</h1>
	</td>
  </tr>
  <tr style="padding-top: 2px;">
    <td><table border="0" cellspacing="2" cellpadding="0" class="BlueTab">
      <tr>
        <td valign="top"><table border="0" align="center" cellpadding="0" cellspacing="1">
          <tr>
             <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
	         <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;</b></td>
	         <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Count&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          </tr>
          <bean:define id="listSize" name="MspBouncesForm" property="listSize" type = "java.lang.Integer"/>
          <logic:iterate id="bouncesList" name="MspBouncesForm" property="bouncesList">
          <%rowCount=listSize.intValue()/2; 
          	if( (listSize.intValue()%2) > 0 ) rowCount = rowCount+1;
          	
	          	if(csschooser == true){
	             	csschooser = false;
	 				backgroundclass = "Ntexteleftalignnowrap";
	 			} else{
	 				csschooser = true;	
	 				backgroundclass = "Ntextoleftalignnowrap";
	 			}
          %>
          <%if(count <= rowCount ) {%>
          	<tr>
            <td class="<%=backgroundclass %>"><bean:write name="bouncesList" property="site"/></td>
            <td class="<%=backgroundclass %>" align="center"><bean:write name="bouncesList" property="date"/></td>
            <td class="<%=backgroundclass %>" align="right"><bean:write name="bouncesList" property="count"/></td>
            </tr>
           <%}count++;%>
          </logic:iterate>
          <%count=1;csschooser=false;%>
        </table></td>
        <td valign="top"><table border="0" align="center" cellpadding="0" cellspacing="1">
          <tr>
            <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
	         <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;</b></td>
	         <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Count&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          </tr>
          <logic:iterate id="bouncesList" name="MspBouncesForm" property="bouncesList">
          <%if( count > rowCount ) {
        	  
	        	if(csschooser == true){
	               	csschooser = false;
	   				backgroundclass = "Ntexteleftalignnowrap";
	   			} else{
	   				csschooser = true;	
	   				backgroundclass = "Ntextoleftalignnowrap";
	   			}  
          %>
          	<tr>
            <td class="<%=backgroundclass %>"><bean:write name="bouncesList" property="site"/></td>
            <td class="<%=backgroundclass %>" align="center"><bean:write name="bouncesList" property="date"/></td>
            <td class="<%=backgroundclass %>" align="right"><bean:write name="bouncesList" property="count"/></td>
            </tr>
           <%}count++;%>
          </logic:iterate>
        </table></td>
      </tr>
      <tr><td><font style="line-height: 4px;">&nbsp;</font></td></tr>
    </table></td>
  </tr>
</table>
</body>
</html> --%>