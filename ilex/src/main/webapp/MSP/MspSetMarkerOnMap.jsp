<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
$(document).ready(function(){
	setMarker(map, markers);
})
function setMarker(map, markers){
	var bounds = map.getBounds();
	var southWest = bounds.getSouthWest();
	var northEast = bounds.getNorthEast();
	var lngSpan = northEast.lng() - southWest.lng();
	var latSpan = northEast.lat() - southWest.lat();
	for (var i = 0; i < 10; i++) {
	    var point = new GLatLng(southWest.lat() + latSpan * Math.random(),
	        southWest.lng() + lngSpan * Math.random());
	    marker = new GMarker(point,baseIcon);
	    map.addOverlay(marker);
	    markers[i] = marker;
	    //map.removeOverlay(markers[i]);
	}
	setBubbol(markers);
	
}
function setBubbol(markers){
	$(markers).each(function(i,marker){
    	GEvent.addListener(marker, "click", function(){
    	marker.openInfoWindowHtml($("#message").html());
    	//displayPoint(marker, i, map);
	    });
    
	});
</script>
</head>
<body>
XXXXXX
</body>
</html>