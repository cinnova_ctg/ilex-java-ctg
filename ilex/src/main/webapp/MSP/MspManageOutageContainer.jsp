<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>


<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />

<title>Manage Outage</title>
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<!-- Menu -->
				<jsp:include page="/MSP/MspTopMenu.jsp"/>
			</td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="breadCrumb">
				 	<a href="#" class="bgNone">MSP</a>
				 	<a>Monitor</a>
				 	<a href="MspMonitor.do?">Current Status</a>
				 	<a><span class="breadCrumb1">Manage Outage</span></a>
				</div>
		    </td>
		</tr>
</table>
<%@include file="/MSP/MspTopMenuScript.inc" %>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
	<td style="padding-left: 10px;" valign="top">
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<jsp:include page="/MSP/MspDeviceAttribute.jsp"/>
			</td>
		</tr>
		</table>
	</td>
	<td style="padding-left: 30px;" valign="top">
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<jsp:include page="/MSP/MspTicketInformation.jsp"/>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

</body>
</html>