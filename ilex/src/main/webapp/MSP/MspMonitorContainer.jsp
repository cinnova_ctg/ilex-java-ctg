
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>MSP Monitor</title>
	
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language="JavaScript" src="javascript/popcalendar.js"></script>
	<script language="JavaScript" src="MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>	
	<script language="JavaScript" src="MSP/MspJavaScript/MspMonitor.js?timestamp="+(new Date()*1)></script>
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<!-- Menu -->
				<jsp:include page="/MSP/MspTopMenu.jsp"/>
			</td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="breadCrumb">
				 	<a href="#" class="bgNone">MSP</a>
				 	<a>Monitor</a>
				 	<a><span class="breadCrumb1">Current Status</span></a>
				</div>
		    </td>
		</tr>
	</table>
	<%@include file="/MSP/MspTopMenuScript.inc" %>
	<!-- Page Sections -->
	<table cellpadding="0" cellspacing="0" border="0">
	<tr valign="top">
		<td style="padding-left: 10px;" colspan="3">
			<div id="monitoringSummary">
				<img src='./images/waiting_tree.gif'>
			</div>
		</td>
	</tr>
	<tr valign="top">
		<td style="padding-left: 10px; padding-top: 10px;">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<div id="realTimeState">
						<img src='./images/waiting_tree.gif'>
					</div>
				</td>
			</tr>
			<tr>
				<td>
				<div id="mspRealTimeFooter">
				  	<img src='./images/waiting_tree.gif'>
				  </div>
				</td>
			</tr>
			<%--Changes Start By : Yogendra Pratap Singh --%>
<!-- 			 start-Link for unmanaged sites Initial assessment -->
			<tr>
				<td>
				<a href="./RealTimeState.do?hmode=unManagedSitesLink"><h1 style="font-size: 14px; white-space: nowrap;">Initial Assessment - Unmanaged Sites</h1>
				</a>
				</td>
			</tr>
<!-- 			end -->
			<%--<tr>
				<td>
					<div id="bounces">
						<img src='./images/waiting_tree.gif'>
					</div>
				</td>
			</tr>--%>
			<%-- Changes End By : Yogendra Pratap Singh--%>
			</table>
		</td>
		<td style="padding-left: 20px; padding-top: 10px;">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<div id="incident">
						<img src='./images/waiting_tree.gif'>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="customerReportedEvent">
						<img src='./images/waiting_tree.gif'>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="event">
						<img src='./images/waiting_tree.gif'>
					</div>
				</td>
			</tr>
			</table>
		</td>
		<td style="padding-left: 10px;"></td>
	</tr>
	</table>
</body>
</html>
