<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%
String backgroundclass="";
boolean csschooser = false;
String createMsg = null;
if(session.getAttribute("ticketCreateMessage") != null){
	createMsg = (String)session.getAttribute("ticketCreateMessage");
	session.removeAttribute("ticketCreateMessage");
}
%>
</head>
<body>

<table border="0" cellspacing="0" cellpadding="0" >
 <tr>
	<td>
	<table cellpadding="0" cellspacing="0" border="0">
 	<tr>
	 	<td><h1 style="font-size: 14px;">Event - Customer/Manual</h1></td>
		<td id="ticketCreateMessage" style="color: #b3b2b0;font-size: 11px;font-weight: normal; white-space: nowrap; padding: 12px 0px 2px 0px; margin: 0px 0px 0px 0px;">&nbsp;&nbsp;
		<%if(createMsg != null){ %>
			<%=createMsg%>
		<%}%>
		</td>
	</tr>
	</table>
	</td>	
 </tr>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="2" class="BlueTab">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
         <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
         <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;</b></td>
         <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Created&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
         <td class="texthdNoBorder"><b>&nbsp;Owner&nbsp;</b></td>
        <td class="texthdNoBorder" style="width: 70px;"><b>Ticket #</b></td>
      </tr>
      <logic:iterate name="MspCustomerReportedEventForm" id="mspNetmedxTicketList" property="mspNetmedxTicketList">  
            <%
            if(csschooser == true){
            	csschooser = false;
				backgroundclass = "Ntexteleftalignnowrap";
			} else{
				csschooser = true;	
				backgroundclass = "Ntextoleftalignnowrap";
			}
            %>
       	 <tr>
         	<td class="<%=backgroundclass %>"><bean:write name="mspNetmedxTicketList" property="customerAppendixName"/></td>
         	<td class="<%=backgroundclass %>"><a href="ManageMspNetmedxTicket.do?ticketId=<bean:write name="mspNetmedxTicketList" property="ticketId"/>&jobId=<bean:write name="mspNetmedxTicketList" property="jobId"/>" ><bean:write name="mspNetmedxTicketList" property="siteName"/></a></td>
          	<td class="<%=backgroundclass %>" align="center"><bean:write name="mspNetmedxTicketList" property="createdDate"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="mspNetmedxTicketList" property="ownerName"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="mspNetmedxTicketList" property="ticketNumber"/></td>
         </tr>     
        </logic:iterate>
    </table></td>
  </tr>
  <tr><td><font style="line-height: 4px;">&nbsp;</font></td></tr>
</table>
</td>
</tr>
</table>
</body>
</html>