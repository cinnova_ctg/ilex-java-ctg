<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/date-picker.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>

<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />

<title>Count of Tickets/Day</title>

<script type="text/javascript">
	$(document).ready(function() {
		$("input[name='search']").click(function(){ 
			if($("input[name='fromDate']").val() == ''){
				alert('Select From date for search.');
				return false;
			}
			if($("input[name='toDate']").val() == ''){
				alert('Select To date for search.');
				return false;
			}
			if(!compDate_mdy( $("input[name='fromDate']").val(),$("input[name='toDate']").val() )){
				alert('To date should be greater than From date.');
				return false;
			}	
			$("form[name='MspReportForm']").attr('action' , 'MspTicketProcessed.do?hmode=search');
			$("form[name='MspReportForm']").submit();
		});
		
		/* Add alternative row colors to list of records. */
		$("#tableData tbody #rowData:even").addClass("Ntexteleftalignnowrap");
		$("#tableData tbody #rowData:odd").addClass("Ntextoleftalignnowrap");
	})
</script>
</head>

<body>
<html:form action="/MspTicketProcessed">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<!-- Menu -->
			<jsp:include page="/MSP/MspTopMenu.jsp"/>
		</td>
	</tr>
	<tr>
		<!-- Bread Crumb -->
		<td background="images/content_head_04.jpg" height="21">
			<div id="breadCrumb">
			 	<a href="#" class="bgNone">MSP</a>
			 	<a>Reports</a>
			 	<a><span class="breadCrumb1">Count of Tickets/Day</span></a>
			</div>
	    </td>
	</tr>
</table>
<%@include file="/MSP/MspTopMenuScript.inc" %>

<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td style="width: 10px;"></td>
		<td style="padding-top: 10px;">
			<table cellpadding="0" cellspacing="4" border="0">
				<tr>
					<td class="labelNoColorBold" style="padding-top: 2px;">From<font class="red">*</font>:</td>
					<td>
						<table border=0><tr>
							<td>
								<html:text name="MspReportForm" property="fromDate" styleClass="text" size="10"/>
								<img style="vertical-align: top;" src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" 
									onclick = "return popUpCalendar( document.forms[0].fromDate , document.forms[0].fromDate , 'mm/dd/yyyy' )" 
									onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							</td>
							<td class="labelNoColorBold">
								&nbsp;&nbsp;&nbsp;&nbsp;To<font class="red" style="vertical-align: middle">*</font>:&nbsp;&nbsp;
							</td>
							<td>
								<html:text name="MspReportForm" property="toDate" styleClass="text" size="10"/>
								<img style="vertical-align: top;" src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" 
									onclick = "return popUpCalendar( document.forms[0].toDate , document.forms[0].toDate , 'mm/dd/yyyy' )" 
									onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							</td>
							<td class="labelNoColorBold" style="padding-left: 10px;">
								<html:button property="search" styleClass="button_c1">Go</html:button>
							</td>
							<td class="labelNoColorBold"><html:reset property="cancel" styleClass="button_c1">Reset</html:reset></td>
						</tr></table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>		

<c:if test="${not empty requestScope.searchFlag and requestScope.searchFlag eq 'search'}">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td style="width: 10px;"></td>
			<td style="padding-top: 0px;">
				<table cellpadding="0" cellspacing="4" border="0">
					<tr>
						<td height="5"></td>
					</tr>
					<tr>
						<td class="labelNoColorBold">Total Tickets:&nbsp;&nbsp;<c:out value="${MspReportForm.total}"/></td>
					</tr>
					<tr style="padding-top: 2px;">
						<td>
							<table border="0" cellspacing="0" cellpadding="2" class="BlueTab">
								<tr>
								    <td>
								       <table border="0" cellpadding="0" cellspacing="1" id="tableData">
									       	<tr>
									           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
									           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Count&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
									        </tr>
									        <c:if test="${not empty MspReportForm.searchList}">
									        	<c:forEach var="list" items="${MspReportForm.searchList}" varStatus="count">
									        		<tr id="rowData">
									        			<td align="center" class="labelNoColor"><c:out value="${list.eventDay}" /></td>
									        			<td align="center" class="labelNoColor"><c:out value="${list.eventCount}" /></td>
									        		</tr>
									        	</c:forEach>
									        </c:if>
								       </table>
								     </td>
							     </tr>  
							     <tr><td><font style="line-height: 4px;">&nbsp;</font></td></tr>
							</table>
						</td>
					</tr>		
				</table>	
			</td>
		</tr>
	</table>
</c:if>			
</html:form>
</body>
</html>