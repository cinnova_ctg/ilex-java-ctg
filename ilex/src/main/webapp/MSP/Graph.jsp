<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>


<html>
<%
String strXMLSystem="";
String strXMLBounces="";
String strXMLCount="";
	strXMLSystem = request.getAttribute("SLAGraph").toString();
	strXMLBounces = request.getAttribute("BouncesGraph").toString();
	strXMLCount= request.getAttribute("CountsGraph").toString();

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="MSP/MspJavaScript/FusionCharts.js"></script>


<title>Insert title here</title>
</head>
<script>
function changeGraph(divID) {
	
	//document.getElementById(divID).style.dispaly="block";
	//document.getElementById(divID).style.visible="visible";
	if(divID == "SystemPerformances"){
		document.getElementById("SystemPerformances").style.display="";
		document.getElementById("Bounces").style.display="none";
		document.getElementById("Count").style.display="none";
		document.getElementById("graphTitle").innerHTML = "<h1 style='font-size: 14px; white-space: nowrap;vertical-align: top;'>System Performance - Average All Sites</h1>";
	}
	else if(divID == "Bounces"){
		document.getElementById("SystemPerformances").style.display="none";
		document.getElementById("Bounces").style.display="";
		document.getElementById("Count").style.display="none";
		document.getElementById("graphTitle").innerHTML = "<h1 style='font-size: 14px; white-space: nowrap;vertical-align: top;'>Count of Site Bounces</h1>";
	}
	else if(divID == "Count"){
		document.getElementById("SystemPerformances").style.display="none";
		document.getElementById("Bounces").style.display="none";
		document.getElementById("Count").style.display="";
		document.getElementById("graphTitle").innerHTML = "<h1 style='font-size: 14px; white-space: nowrap;vertical-align: top;'>Sites Monitored</h1>";
	}

}
</script>
<%@ include file="/MSP/FusionCharts.jsp" %>
<body>
	<table   style="width: 720px; height: 300px; border:2px solid #e8e8e5;" >
		<tr>
			<td>
				<table  border="0" cellpadding="0" cellspacing="0" width ="100%">
					<tr>
						<td  id ="graphTitle" style="vertical-align: top;">
							<h1 style="font-size: 14px; white-space: nowrap;vertical-align: top;">System Performance - Average All Sites</h1>
						</td>
						<td align="right"  style="padding-top: 5px; padding-right: 5px;">
							<table bgcolor="#f9f9f9">
								<tr>
									<td class="smallTabletextnopadding"><a href="#" onclick="changeGraph('SystemPerformances');">System Performance</a></td>
								</tr>
								<tr>
									<td class="smallTabletextnopadding"><a href="#" onclick="changeGraph('Count');">Count of Sites</a></td>
								</tr>
								<tr>
									<td class="smallTabletextnopadding"><a href="#" onclick="changeGraph('Bounces');">Site Bounces</a></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div id = "SystemPerformances">
								<%
									//String strXml=(String)request.getAttribute("strAccountXml");
									String chartCodeSystem=createChart("MSP/FCF_MSLine.swf", "", strXMLSystem, "1", 750, 300, false,false);
								%>
								<%=chartCodeSystem%>
							</div>
							<div id = "Bounces" style="display: none;">
								<%
									//String strXml=(String)request.getAttribute("strAccountXml");
									String chartCodeBounces=createChart("MSP/FCF_MSLine.swf", "", strXMLBounces, "2", 750, 300, false,false);
								%>
								<%=chartCodeBounces%>
							</div>
							<div id = "Count" style="display: none;">
								<%
									//String strXml=(String)request.getAttribute("strAccountXml");
									String chartCodeCount=createChart("MSP/FCF_MSLine.swf", "", strXMLCount, "3", 750, 300, false,false);
								%>
								<%=chartCodeCount%>
							</div>
						</td>
						<td>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
