<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<title>Site/Device Attribute</title>
</head>
<script>
function closesiteViewBoxPopup() {
    $.unblockUI(); 
    $("#siteViewItemValue").empty().html('<img src="./images/waiting_tree.gif" />');
   // return false; 
}
</script>
<body>
<html:form action="SiteDetail.do">
<table cellpadding="0" cellspacing="0" border="0" width="400px" align="right" >
	<tr>
		<td>
			<table  border='0' cellspacing="0" cellpadding="0" class="Ntextoleftalignnowrappaleyellowborder2" width="100%">
				<tr>
					<td>
						<table>
							<tr>
								<td style="padding-left:8px; padding-top: 1px"><h1 style="font-size: 11px; padding-top: 2px;">Site/Device Information</h1></td>
				 		    </tr>
					        <tr>
							    <td style="padding-left:8px" align="left" class="dbvaluesmallFont" style="width: 200px; white-space: nowrap;">Site: <bean:write name="SiteDetailForm" property="siteNumber"/></td>
				            </tr>
					   </table>
					</td>
			    </tr>
			</table>
		</td>
			<td align="right">
				<table  border='0' cellspacing="0" cellpadding="0" class="Ntextoleftalignnowrappaleyellowborder2" width="100%">
					<tr>
						<td style="padding-top: 4px;">
							<img height="15" width="15" alt="Close" src="images/delete.gif" id="closeShipping" onclick="closesiteViewBoxPopup();">
						</td>
					</tr>
					<tr>
						<td height="22px"></td>
					</tr>
			   </table>
		   </td>
	</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-left: 10px"><b>Device Attributes:</b></td>
</tr>

<tr>
	<td style="padding-top: 10px; padding-left: 10px; padding-right: 10px">
	<table cellpadding="0" cellspacing="1" border="0" class="BlueTab" style="border:0px; ">
		<tr>
			<td class="texthdNoBorder" width="100">Attribute</td>
			<td class="texthdNoBorder" width="100">Value</td>
		</tr>
		<tr>
			<td class="texteWhiteBackground">Circuit Type</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="circuitType"/></td>
		</tr>
		<tr>
			<td class="texteWhiteBackground">Modem Type</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="modemType"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Internet Service Line #</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="internetServiceLineNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">ISP</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="isp"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">ISP Support #</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="ispSupportNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Circuit IP</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="circuitIP"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Site Contact #</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="siteContactNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Location</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="location"/></td>
		</tr>
		<tr>
			<td class="texteWhiteBackground">Account</td>
			<td class="texteWhiteBackground"><bean:write name="SiteDetailForm" property="accountNumber"/></td>
		</tr>
	</table>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-top: 10px; padding-left: 10px"><b>Site Details</b></td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-top: 10px; padding-left: 10px">Address:&nbsp; <bean:write name="SiteDetailForm" property="siteAddress"/></td>
</tr>

<tr>
	<td style="padding-left: 10px">
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="dbvaluesmallFont" nowrap="nowrap">City:&nbsp; <bean:write name="SiteDetailForm" property="siteCity"/></td>
			<td align="center" class="dbvaluesmallFont" nowrap="nowrap" style="width: 150px;">State:&nbsp;<bean:write name="SiteDetailForm" property="siteState"/></td>
			<td align="right" class="dbvaluesmallFont" nowrap="nowrap">Zip Code:&nbsp;<bean:write name="SiteDetailForm" property="siteZip"/></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-bottom: 10px; padding-left: 10px">Time Zone:&nbsp; <bean:write name="SiteDetailForm" property="siteTimeZone"/></td>
	<td></td>
</tr>
</table>
</html:form>
</body>
</html>