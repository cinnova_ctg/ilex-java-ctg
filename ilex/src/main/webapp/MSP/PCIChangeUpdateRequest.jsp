<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
	<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
		<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/jquery.readonly.js"></script>
	<script language="JavaScript" src="javascript/popcalendar.js"></script>
</head>
	<script>
	$(document).ready(function() {
		var count=1;
		var newRow;
		$("input[name='addWorkFlow']").click(function() {
			++count;
			if(count%2!=0){
			newRow = 
				'<tr>'+
			'<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  ><input type="text" class="PCTXT0001"/></td>'+
			'<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  >'+
				'<select>'+
				'<option>In Work</option>'+
				'<option>Complete</option>'+
				'<option>Hold</option>'+
				'<option>Not Required</option>'+
				'</select>'+
			'</td>'+
		    '<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" ><input type="text" size="10" class="PCTXT0001" onclick = "check1(this)"/></td>'+
		    '<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001"  ><input type="text" size="10"  class="PCTXT0001" onclick = "check1(this)"/></td>'+
		    '<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  ><input type="text" class="PCTXT0001"/></td>'+
			'<td ><input type="button" class="FMBUT0001 WASTD0002" value="Save" style="margin-top: 0px;"></input></td>'+
			'<td ><input type="button" class="FMBUT0001 WASTD0002" value="Delete" style="margin-top: 0px;" onclick="deleteRow(this);"></input></td>'+
'</tr>'
			}
			else{
				newRow = 
					'<tr>'+
				'<td  class="PCSTDEVEN0001 PCSTD0003 TBTXT0001"  ><input type="text" class="PCTXT0001"/></td>'+
				'<td  class="PCSTDEVEN0001 PCSTD0003 TBTXT0001"  >'+
					'<select>'+
					'<option>In Work</option>'+
					'<option>Complete</option>'+
					'<option>Hold</option>'+
					'<option>Not Required</option>'+
					'</select>'+
				'</td>'+
			    '<td  class="PCSTDEVEN0001 PCSTD0003 TBDAT0001" ><input type="text" class="PCTXT0001" size="10" onclick = "check1(this)"/></td>'+
			    '<td  class="PCSTDEVEN0001 PCSTD0003 TBDAT0001"  ><input type="text" class="PCTXT0001" size="10" onclick = "check1(this)"/></td>'+
			    '<td  class="PCSTDEVEN0001 PCSTD0003 TBTXT0001"  ><input type="text" class="PCTXT0001"/></td>'+
				'<td ><input type="button" class="FMBUT0001 WASTD0002" value="Save" style="margin-top: 0px;"></input></td>'+
				'<td ><input type="button" class="FMBUT0001 WASTD0002" value="Delete" style="margin-top: 0px;" onclick="deleteRow(this);"></input></td>'+
	'</tr>'
				
			}
			$("#appendWorkFlow tr:last").after(newRow)
			});
		//To disable the Form
		$("#saveRequest").click(function() {
			//alert('Hi');
		if($("#status").val()=="Closed"){
			//alert('Hi Inside');
			$("#PCI").attr("disabled", true);
			//$("#PCI").readonly(true);
			
		}	
		
		});
		
	});
	function deleteRow(obj){
		if(confirm('Please verify request to delete Action.')) {
			$(obj).parent().parent().remove();
		}
	}
	
	</script>
<script type="text/javascript">
function check1(obj)
{
	
	return popUpCalendar(obj,obj,'mm/dd/yyyy');
	//alert("this is in the check");
}

</script>	
	<body id="body1" >
<html:form action="PCIChangeControlAction.do" styleId="PCIChangeControlForm">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
			<td>
				<!-- Menu -->
				<jsp:include page="/MSP/MspTopMenu.jsp"/>
			</td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="BCNAV0001">
				 	<a class="BCNAV0002" >MSP</a>
				 	<a >PCI Change Control</a>
				 	<a >Change Request</a>
				 	<a><span class="BCPAG0001">Search</span></a>
				</div>
		    </td>
		</tr>
	</table>
<div>

<div>
<table border="0" class="WASTD0001" id="PCI">
<tr>
	<td class="PCSSH0001" height="20" colspan="1">
	<div>Change Request Details</div></td>
	<td align="right"><input type="button" class="FMBUT0001" value="Update Change Request" id="saveRequest"></input>
	<input type="button" class="FMBUT0001" value="Delete Change Request" id="saveRequest"></input></td>	
 </tr> 
<tr>
<td class="PCLAB0001" height="20"  colspan="21">
	<div align="center">Requestor</div></td>
</tr>
<tr>
	<td class = "PCLAB0001">Status:</td>
	<td>
		<select id="status">
			<option value="Open">Open</option>
			<option value="Closed">Closed</option>
			<option value="Canceled">Cancelled</option>
		</select>
	</td>
</tr>
<tr>
<td class = "PCLAB0001">Customer:</td>
	<td>
		<select>
		<option>A & E Electric</option>
		<option>AA - Hungsco</option>
		<option>ACMI Corporation</option>
		</select>
	</td>
</tr>

<tr>
	<td class = "PCLAB0001">Site Identifier:</td>
	<td> <table>
			<tr>
				<td  class="PCSTD0002">BN-0001, San Jose, CA</td>
				<td><select>
						<option>LA-0001, Los Angles, LA</option>
						<option>TS-0001, San Jose, CA</option>
						<option>AN-0001, Test, TX</option>
					</select>
				</td>
			</tr>
		</table> 
		
	</td>
</tr>

<tr>
	<td class = "PCLAB0001">Description of Change:</td>
	<td>
	<TEXTAREA NAME="SpecialRequest" 
	   ROWS="4" COLS="62">
	</TEXTAREA>
	</td>
</tr>

<tr>
	<td class = "PCLAB0001">Reason for Changes:</td>
	<td>
	<TEXTAREA NAME="SpecialRequest" id="SpecialRequest"
	   ROWS="4" COLS="62" >
	</TEXTAREA>
	</td>
</tr>

<tr>
	<td class = "PCLAB0001">Resource Impact:</td>
	<td>
	<input type="text" height="200px" >
	</td>
</tr>

<tr>
			<td ><input type="button" class="FMBUT0001" value="Save" id="saveRequest"></input>
							&nbsp;&nbsp;  <input type="reset" class="FMBUT0001" value="Reset" ></input></td>
</tr>
</table>
</div>

<div>
<table border="0"  class="WASTD0002" cellspacing="0" cellpadding="0" id ="appendWorkFlow">
<tr >
<td class="PCSSH0001" colspan="4">
	<div>Actions</div></td>
	<td colspan="1" align="right"><div><input name="addWorkFlow"   type="button" class="FMBUT0001" value="Add" style="margin-bottom: 20px;"></input></div></td>
</tr>

<tr>
			<td  class="PCSTD0004 PCSTD0003" rowspan="2" >Action</td>
			<td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Status</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Planned</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Actual</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Notes</td>
			<td align="right"></td>
</tr>
<tr>
</tr>
<tr>
			<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  ><input type="text" class="PCTXT0001"/></td>
			<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  >
				<select>
				<option>In Work</option>
				<option>Complete</option>
				<option>Hold</option>
				<option>Not Required</option>
				</select>
			</td>
		    <td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" ><input type="text" class="PCTXT0001" size="10" onclick = "check1(this)"/></td>
		    <td  class="PCSTDODD0001 PCSTD0003 TBDAT0001"  ><input type="text" class="PCTXT0001" size="10" onclick = "check1(this)"/></td>
		    <td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  ><input type="text" class="PCTXT0001"/></td>
			<td ><input type="button" class="FMBUT0001 WASTD0002" value="Save" style="margin-top: 0px;"></input></td>
			<td ><input type="button" class="FMBUT0001 WASTD0002" value="Delete" style="margin-top: 0px;" onclick="deleteRow(this);"></input></td>
</tr>
</table>
</div>
<div>
<table border="0" class="WASTD0001" cellspacing="0" cellpadding="0">
<tr >
<td class="PCSSH0001" colspan="5">
	<div>Final Disposition</div></td>

</tr>

<tr >
<td height="20px"></td>
</tr>
<tr>
			<td  class="PCSTD0004 PCSTD0003" rowspan="2" colspan="1" >Name/Title</td>
			<td  class="PCSTD0004 PCSTD0003"  rowspan="2" colspan="1" >Disposition</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" colspan="1" >Date</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2"  colspan="2">Comments</td>
</tr>
<tr>
</tr>
<tr>
			<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" colspan="1" ><input type="text" class="PCTXT0001" size="30"/></td>
			<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" colspan="1" >
			<select>
				<option>Select</option>
				<option>Approved</option>
				<option>Dispproved</option>
			</select>
			</td>
		    <td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" colspan="1"><input type="text" class="PCTXT0001" size="10" onclick = "check1(this)"/></td>
		    <td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" colspan="2" ><input type="text" size="35" class="PCTXT0001"/></td>
</tr>
<tr>
			<td ><input type="button" class="FMBUT0001" value="Save"></input>
							&nbsp;&nbsp;  <input type="reset" class="FMBUT0001" value="Reset" ></input></td>
</tr>
</table>
</div>

</div>
</html:form>
</body>

</html>

