/*
* JavaScript file of MspSearchHistory.jsp
*/

$(document).ready(function() {
	/* Vailidations*/
	$("input[name='search']").click(function(){ 
		
		if($("select[name='customer']").val() == '0'){
			alert('Select customer for search.');
			return false;
		}	
		if(jQuery.trim($("input[name='site']").val()) == ''){
			alert('Select site for search.');
			return false;
		}
		if(!($("input[name='from']").val()=='') && !($("input[name='to']").val()=='')){
			if(!compDate_mdy( $("input[name='from']").val(),$("input[name='to']").val() )){
			alert('To date should be greater than from date.');
			return false;
		}	
		}
		$("form[name='MspSearchHistoryForm']").attr('action' , 'SearchHistory.do?hmode=search');
		$("form[name='MspSearchHistoryForm']").submit();
	});
})