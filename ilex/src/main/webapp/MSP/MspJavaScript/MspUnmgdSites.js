
//JavaScript for Initial Assessment - Unmanaged Sites 																		 2.3.01 change
var refreshTimer = 45000;// - 45 Seconds		-Unmanaged - Initial Assessment
var intervalId;
$(document).ready(function() {
	$("#unManagedRealTimeState").load("./RealTimeState.do?hmode=unManagedInitialAssessment"+addtimestamp2());
	intervalId = setInterval(function(){
	$("#unManagedRealTimeState").load("./RealTimeState.do?hmode=unManagedInitialAssessment"+addtimestamp2());}, 
	refreshTimer
);
});//- End $(document).ready(function()

function unmanagedEscalateConfirmation(ajaxUrl,elementId,loadFrom){
	if (confirm('Confirm escalation for this site by clicking Ok.  Otherwise, click Cancel.')) {
			unmanagedDisableLink();
            ajaxCallUnmgdSite(ajaxUrl,elementId,loadFrom);
        }
}
function unmanagedDisableLink(){
	$("a.unmanagedRealTimeClickable").removeAttr('onclick');
	$("a.unmanagedRealTimeClickable").removeAttr('href');
	$("a.unmanagedRealTimeClickable").css({color:'#b3b2b0', cursor:'none'});
}
