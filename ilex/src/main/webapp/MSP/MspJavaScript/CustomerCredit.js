/*
* JavaScript file for CustomerCredits Report.
*/

$(document).ready(function() {
	/* Vailidations*/
	$("input[name='search']").click(function(){ 

		if($("select[name='customerId']").val() == '0' || $("select[name='customerId']").val() == '') {
			alert('Select Customer for search.');
			return false;
		}
		if($("input[name='fromDate']").val() == ''){
			alert('Select From date for search.');
			return false;
		}
		if($("input[name='toDate']").val() == ''){
			alert('Select To date for search.');
			return false;
		}
		if(!compDate_mdy( $("input[name='fromDate']").val(),$("input[name='toDate']").val() )){
			alert('To date should be greater than From date.');
			return false;
		}	
		$("form[name='MspReportForm']").attr('action' , 'CustomerCredits.do?hmode=search');
		$("form[name='MspReportForm']").submit();
	});
	
	/* Add alternative row colors to list of records. */
	$("#tableData tbody #rowData:even").addClass("Ntexteleftalignnowrap");
	$("#tableData tbody #rowData:odd").addClass("Ntextoleftalignnowrap");
})