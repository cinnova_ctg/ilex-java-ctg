/*
 * Author: Vijay Kumar Singh
 * Description: This js file is used under ManageMspNetMedXTicket.jsp page.
 * This js is used for call slider, validation of form and submit form
 *  
 * */
var messageFadeOutTime = 10000;

$(document).ready(function() {
	$("#slider").slider({
		value:$("input[name='estimatedEffort']").val()/5,
		min: 0,
		max: 12,
		step: 1,
		slide: function(event, ui) {
			$("input[name='estimatedEffort']").val(ui.value*5);
		}
	});
	$("input[name='estimatedEffort']").val($("#slider").slider("value")*5);
	
	$("input[name='addNotes']").click(function(){ 
		if(jQuery.trim($("textarea[name='installationNotes']").val()) == ''){
			alert('Installation note should be entered');
			return false;
		}
		$("input[name='installNotesPresent']").val(true);
		addCommentsForInstallationNotes($("input[name='jobId']").val())
	});
	
	$("input[name='closeTicket']").click(function(){ 
		if( $("input[name='installNotesPresent']").val() == 'false' ){
			alert("Atleast one installation note is required.");
			$("#installationNotes").focus();
			return false;
		}			
		if( jQuery.trim($("input[name='estimatedEffort']").val()) == '' || jQuery.trim($("input[name='estimatedEffort']").val()) == '0' ){
			alert("Estimated effort should be selected.");
			$("#slider").focus();
			
			return false;
		}
		setTicketToClose();		
	});
	
	$("input[name='updateEffort']").click(function() {
		var url = "ManageMspNetmedxTicket.do?hmode=updateEffort&jobId="+$("input[name='jobId']").val()+"&estimatedEffort="+$("input[name='estimatedEffort']").val()+"&ticketId="+$("input[name='ticketId']").val();
		jsonAjaxCall(url);//- JQueryAjaxUtility.js
	});
});

function onSuccessJsonAjaxCall(data){
	$.each(data, function(i, j){
		setMessage(j.updateflag,j.estimatedEffort);
	});	
}

/*
 * Set slider new value and show updated message.
 * 
*/
function setMessage(flag,effortVal){
	$("#hideMessage").text("");
	$("#hideMessage").fadeIn("fast");
	$("#slider" ).slider( "option", "value",(effortVal/5) );
	if(flag > -1){
		$("#hideMessage").html("&nbsp;- Estimated Effort was updated.");
	}else{
		$("#hideMessage").html("&nbsp;- Update action failed.");
	}	
	$("#hideMessage").animate({opacity: 1.0}, messageFadeOutTime).fadeOut();
}

function setTicketToClose(){
	if (confirm('Do you want to close this ticket?')) {
            $("form[name='ManageMspNetmedxTicketForm']").attr('action', 'ManageMspNetmedxTicket.do?hmode=closeTicket'+addtimestamp2());
			$("form[name='ManageMspNetmedxTicketForm']").submit();
        }
}


	

