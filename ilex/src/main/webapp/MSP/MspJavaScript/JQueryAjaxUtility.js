function ajaxCall(ajaxUrl,elementId,loadFrom){
/**
.@param 	
		ajaxUrl 	- URL - to validate
		elementId	- DIV id. 
		loadFrom	- URL to fill DIV.
		 
	Note. Create these function to use this ajaxCall. 
	1. onAjaxError(elementId)
	2. onAjaxComplete(elementId)
	3. onAjaxError(elementId)
	4. onAjaxBeforeSend(elementId)
	5. onAjaxSuccess(elementId)
*/
/* Call ajax from get refreshed data from server.*/
	$.ajax({
  	type: "POST",
   	url: ajaxUrl + addtimestamp2(),
   	error: function(){ onAjaxError(elementId) },
   	beforeSend: function(){ onAjaxBeforeSend(elementId); },
   	complete: function(){ onAjaxComplete(elementId); },
   	success: function(msg){
			    	if(msg != 0){
			    		$("#"+elementId).load(loadFrom + addtimestamp2() );
			    		onAjaxSuccess(elementId);
			    	}
			   	}
 });
}// End ajaxCall()

//added for initial assessment - unmanaged sites 
function ajaxCallUnmgdSite(ajaxUrl,elementId,loadFrom){
	/**
	.@param 	
			ajaxUrl 	- URL - to validate
			elementId	- DIV id. 
			loadFrom	- URL to fill DIV.
		
	*/
	
		$.ajax({
	  	type: "POST",
	   	url: ajaxUrl + addtimestamp2(),
	    success: function(msg){
				    	if(msg != 0){
				    		$("#"+elementId).load(loadFrom + addtimestamp2() );
				    		
				    	}
				   	}
	 });
	}// End ajaxCallUnmgdSite()


function ajaxRequest(url){
/**
.@param 
	url - URL
.@deprecated 
	NOTE. create a function onSuccessRequest(msg) to get Ajax return. 
*/
/*Call ajex for update */
	$.ajax({
  	type: "POST",
   	url: url + addtimestamp2(),
   	success: function(msg){onSuccessRequest(msg)}
 });
}//End - ajaxForNoAction()

function jsonAjaxCall(url){
/**
.@param 
	url - URL
.@deprecated 
	NOTE. create a function onSuccessJsonAjaxCall(data) to get Ajax return. 
*/
	$.ajax({
	    type: "post",
	    dataType: "json",
	    url: url + addtimestamp2(),
	    async: false,
	    timeout: 10000,
	    error: function() {},
	    success: function(data) {onSuccessJsonAjaxCall(data)}
	});
}// End - jsonAjaxCall()


function addtimestamp1(){
/*  - Add this as a parameter into ajex url.
	  to Cache-Control.
	- When no parameter is defined into ajurl.*/
	
	return 'timestamp='+ (new Date()*1);
}//End -addtimestamp1()

function addtimestamp2(){
/*  - Add this as a parameter into ajex url.
	  to Cache-Control.
	- When parameter is defined into ajex url.*/
	
	return '&timestamp='+ (new Date()*1);
}//-addtimestamp2()