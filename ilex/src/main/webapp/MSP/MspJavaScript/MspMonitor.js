/*Refresh different page section on different time interval.*/
var refreshTimer1 = 60000; // - 60 Seconds		-Monitoring Summary
var refreshTimer2 = 10000; // - 10 Seconds		-Real-Time State Changes - Initial Assessment
var refreshTimer3 = 60000; // - 60 Seconds		-Incident - Help Desk Action In Work
var refreshTimer4 = 60000; // - 60 Seconds		-Event - MSP Dispatch In Work
var refreshTimer5 = 60000; // - 60 Seconds		-Customer Reported Event - MSP NetMedX Ticket

var intervalId1;
var intervalId2;
var intervalId3;
var intervalId4;
var intervalId5;
var intervalId6;


/*For Monitoring Summary 5 refresh cycles */
var refreshCycles0 = false;
var refreshCycles1 = false;
var refreshCycles2 = false;
var refreshCounter0 = 0;
var refreshCounter1 = 0;
var refreshCounter2 = 0;

var realTimePageFlag = 0;
var realTimeMsgfadeOut = 1000;
var customerRptfadeOut = 10000;

var refreshIncidentDiv = false;	// -Incident - Help Desk Action In Work

$(document).ready(function() {
	/* First show all page sectiones on page load.*/
	$("#monitoringSummary").load("./MspMonitoringSummary.do?"+addtimestamp1());
	$("#realTimeState").load("./RealTimeState.do?"+addtimestamp1());
	$("#incident").load("./MspIncident.do?"+addtimestamp1());
	$("#escalation").load("./MspMonitor.do?"+addtimestamp1());
	//$("#bounces").load("./MspBounces.do?"+addtimestamp1());
	$("#event").load("./MspEvent.do?"+addtimestamp1());
	$("#mspRealTimeFooter").load("./RealTimeState.do?hmode=realTimeFooter"+addtimestamp2());
	$("#customerReportedEvent").load("./MspCustomerReportedEvent.do?"+addtimestamp1());
	
	/*Refresh Monitoring Summary*/
	intervalId1 = setInterval(function(){
					ajaxCall("./MspMonitoringSummary.do?hmode=monitoringSummary","monitoringSummary","./MspMonitoringSummary.do?");}, 
					refreshTimer1
				);
	//Change By Yogendra
	/*Refresh Real-Time State Changes - Initial Assessment*/
	
	//End Changes By Yogendra
					
	/*Refresh Real-Time State Changes - Initial Assessment*/
	intervalId2 = setInterval(function(){
					ajaxCall("./RealTimeState.do?hmode=escalationLevel&pageFlag="+realTimePageFlag+"","realTimeState","./RealTimeState.do?");}, 
					refreshTimer2
				);
	intervalId5 = setInterval(function(){
					$("#mspRealTimeFooter").load("./RealTimeState.do?hmode=realTimeFooter"+addtimestamp2());}, 
					refreshTimer2
				);
				
	/*Refresh Incident - Help Desk Action In Work*/
	intervalId3 = setInterval(function(){
					ajaxCall("./MspIncident.do?hmode=incident","incident","./MspIncident.do?");}, 
					refreshTimer3
				);
				
	/*Refresh Event - MSP Dispatch In Work*/
	intervalId4 = setInterval(function(){
					ajaxCall("./MspEvent.do?hmode=event","event","./MspEvent.do?");}, 
					refreshTimer4
				);
	/*Refresh Customer Reported Event - MSP NetMedX Ticket*/
	intervalId6 = setInterval(function(){
		ajaxCall("./MspCustomerReportedEvent.do?","customerReportedEvent","./MspCustomerReportedEvent.do?intervalflag=true");}, 
		refreshTimer5
	);
				
});//- End $(document).ready(function()

function onAjaxBeforeSend(elementId){
	if(elementId == 'realTimeState'){
		refreshIncidentDiv = true;
		clearInterval(intervalId2);
		clearInterval(intervalId3);		
		clearInterval(intervalId5);
	}
}//End

function onAjaxComplete(elementId){
	if(elementId == 'realTimeState'){
		refreshIncidentDiv = true;
		resetTimer(elementId);
	}
	refreshIncidentDiv = false;
	
}//End

function onAjaxSuccess(elementId){
	if(refreshIncidentDiv){
		refreshIncidentDiv = false;
		$("#incident").load("./MspIncident.do?"+addtimestamp1());
	}
}//End

function onAjaxError(elementId){
	if(elementId == 'realTimeState'){
		resetTimer(elementId);
	}
}

function resetTimer(){
	/*Reset Real-Time State Changes - Initial Assessment Refresh-Timer*/
	clearInterval(intervalId2);
	intervalId2 = setInterval(function(){
					ajaxCall("./RealTimeState.do?hmode=escalationLevel&pageFlag="+realTimePageFlag+"","realTimeState","./RealTimeState.do?");}, 
					refreshTimer2
				);
	
	$("#hideMessage").animate({opacity: 1.0}, realTimeMsgfadeOut).fadeOut();
	$("#ticketCreateMessage").animate({opacity: 1.0}, customerRptfadeOut).fadeOut();

	clearInterval(intervalId5);			
	intervalId5 = setInterval(function(){
				$("#mspRealTimeFooter").load("./RealTimeState.do?hmode=realTimeFooter"+addtimestamp2());}, 
				refreshTimer2
			);
		
	/*Reset Incident - Help Desk Action In Work Refresh-Timer*/
	clearInterval(intervalId3);
	intervalId3 = setInterval(function(){
				ajaxCall("./MspIncident.do?hmode=incident","incident","./MspIncident.do?");}, 
				refreshTimer3
			);
}

function escalateConfirmation(ajaxUrl,elementId,loadFrom){
	if (confirm('Confirm escalation for this site by clicking Ok.  Otherwise, click Cancel.')) {
			disableLink();
            ajaxCall(ajaxUrl,elementId,loadFrom);
        }
}


function disableLink(){
	$("a.RealTimeClickable").removeAttr('onclick');
	$("a.RealTimeClickable").removeAttr('href');
	$("a.RealTimeClickable").css({color:'#b3b2b0', cursor:'none'});
}

