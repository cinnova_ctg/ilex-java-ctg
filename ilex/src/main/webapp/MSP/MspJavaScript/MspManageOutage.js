$(document).ready(function() {

	if(jQuery.trim($("#siteCurrentStatus").text()) == 'UP'){$("#siteCurrentStatus").css({'color':'green'});}
	else{$("#siteCurrentStatus").css({'color':'red'});}
	
	$("input[name='addNotes']").click(function(){ 
		if(jQuery.trim($("textarea[name='installationNotes']").val()) == ''){
			alert('Installation note should be entered');
			return false;
		}
		
		if( jQuery.trim($("input[name='backupCircuitStatus']:radio:checked").val()) == '' ){
			alert("You must select a Backup Circuit Status before taking this action.");
				return false;
			}
		
		$("input[name='installNotesPresent']").val(true);
		addCommentsForInstallationNotesandBackupStatus($("input[name='jobId']").val());
	});
	
	function addCommentsForInstallationNotesandBackupStatus(jobId)
	{
		if(jobId != null && jobId!= ''){
			//var installationNotes = escape(encodeURI(document.all.installationNotes.value));
			var installationNotes = escape(encodeURI($("textarea[name='installationNotes']").val()));
			var ajaxRequest = getAjaxRequestObjectInstallNotes();	
			ajaxRequest.onreadystatechange = function(){
	           if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){ 
		           var xmlDoc=ajaxRequest.responseXML;
		           var id;
				   id = xmlDoc.getElementsByTagName("installNotesInfo")[0];	
				   var installNotesCount=id.getElementsByTagName("installNotesCount")[0].firstChild.nodeValue;
				   var latestInstallNotesDate=id.getElementsByTagName("latestInstallNotesDate")[0].firstChild.nodeValue;					 
				   var latestInstallNotes=id.getElementsByTagName("latestInstallNotes")[0].firstChild.nodeValue;
				   
		           var secondLatestInstallNotesDate=id.getElementsByTagName("secondLatestInstallNotesDate")[0].firstChild.nodeValue;					 
				   var secondLatestInstallNotes=id.getElementsByTagName("secondLatestInstallNotes")[0].firstChild.nodeValue;
				   $("#installNotesCountDiv").html("");
				   $("#installNotesCountDiv").html(installNotesCount);
				   
		          
		           document.getElementById("latestInstallNotesDate").innerHTML = "";
		           document.getElementById("latestInstallNotes").innerHTML = "";
		           document.getElementById("secondLatestInstallNotesDate").innerHTML = "";
		           document.getElementById("secondLatestInstallNotes").innerHTML = "";
		           
		           document.getElementById("latestInstallNotesDate").innerHTML = latestInstallNotesDate;
		           document.getElementById("latestInstallNotes").innerHTML = removedLastSpecialChar(latestInstallNotes);
		           document.getElementById("secondLatestInstallNotesDate").innerHTML = secondLatestInstallNotesDate;
		           document.getElementById("secondLatestInstallNotes").innerHTML = removedLastSpecialChar(secondLatestInstallNotes);
		         //  document.all.installationNotes.value = "";
		         $("textarea[name='installationNotes']").val("");
	          }// End of if
		    };
		    var url ="ManageOutage.do";
		    var params = "hmode=addInstallationNotesAndUpdateBackupCircuitStatus&"+$("form[name='ManageOutageForm']").serialize();
			ajaxRequest.open("POST", url, true);
			ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			ajaxRequest.send(params); 
		}// End of If
	} 
	
	
	
	$("input[name='helpDesk']").click(function(){ 
	
		if( jQuery.trim($("#siteCurrentStatus").text()) == 'DOWN' ){
			alert("The site is currently DOWN so the ticket cannot be closed.");
			return false;
		}

		if( $("input[name='installNotesPresent']").val() == 'false' ){
			alert("Atleast one installation note is required.");
			$("#installationNotes").focus();
			return false;
		}
			
		if( jQuery.trim($("input[name='estimatedEffort']").val()) == '' || jQuery.trim($("input[name='estimatedEffort']").val()) == '0' ){
			alert("Estimated effort should be selected.");
			//$("#slider").focus();
			
			return false;
		}
		
		if( jQuery.trim($("input[name='backupCircuitStatus']:radio:checked").val()) == '' ){
			alert("Backup Circuit Status should be Checked.");
			return false;
		}
		
		if( jQuery.trim($("select[name='issueOwner']").val()) == '0' ){
			alert("Issue owner should be selected.");
			$("select[name='issueOwner']").focus();
			return false;
		}
		if( jQuery.trim($("select[name='resolution']").val()) == '0' ){
			alert("Resolution should be selected.");
			$("select[name='resolution']").focus();
			return false;
		}
		if( jQuery.trim($("select[name='rootCause']").val()) == '0' ){
			alert("Root cause should be selected.");
			$("select[name='rootCause']").focus();
			return false;
		}
		
		
		resolvedByHelpDeskConfirmation();		
	});
	
	$("input[name='mspDispatch']").click(function(){
	
		if( $("input[name='installNotesPresent']").val() == 'false' ){
			alert("Atleast one installation note is required.");
			$("#installationNotes").focus();
			return false;
		}
		if( jQuery.trim($("input[name='estimatedEffort']").val()) == '' || jQuery.trim($("input[name='estimatedEffort']").val()) == '0' ){
			alert("Estimated effort should be selected.");
			//$("#slider").focus();
			return false;
		}
		escalateToMspDispatchConfirmation();
	});
	
	$("input[name='nextActionListUpdateBtn']").click(function(){
		var d = document.getElementsByName('nextAction')[0];
		$("form[name='ManageOutageForm']").attr('action' , 'ManageOutage.do?hmode=updateNextActionIdActions&nextActionId'+d.value+addtimestamp2());
		$("form[name='ManageOutageForm']").submit();
	});

	$("input[name='updateResolutionSection']").click(function() {
		
		if( jQuery.trim($("input[name='backupCircuitStatus']:radio:checked").val()) == '' ){
			alert("You must select a Backup Circuit Status before taking this action.");
			return false;
		}
	if(!$("input[name='overrideEfforts']").is(":checked")){
		var effortValue=$("input[name='estimatedEffort']").val();
		if (effortValue == "") {
			alert("Please Enter the Efforts");
			return false;
		}
		if (isNaN(effortValue)) {
			alert("Please Enter valid Efforts");
			return false;

		}
		if ((parseInt(effortValue) <= 0) || (parseInt(effortValue) > 1441)) {
			alert("Efforts should be greater than 0 and less than 1441");
			return false;

		}
		}
	
		$("form[name='ManageOutageForm']").attr('action' , 'ManageOutage.do?hmode=updateResolutionAndActions'+addtimestamp2());
		$("form[name='ManageOutageForm']").submit();
		//ajaxCall("./ManageOutage.do?hmode=updateResolutionAndActions&NActiveMonitorStateChangeID="+$("input[name='NActiveMonitorStateChangeID']").val()+"&issueOwner="+$("input[name='issueOwner']").val()+"&rootCause="+$("input[name='rootCause']").val()+"&resolution="+$("input[name='resolution']").val()+"&efforts="+$("input[name='estimatedEffort']").val(),"updateResolution","./ManageOutage.do?");
	});

//	$("#slider").slider({
//		value:$("input[name='estimatedEffort']").val()/5,
//		min: 0,
//		max: 12,
//		step: 1,
//		slide: function(event, ui) {
//			$("input[name='estimatedEffort']").val(ui.value*5);
//		}
//	});
//	$("input[name='estimatedEffort']").val($("#slider").slider("value")*5);

});
function resolvedByHelpDeskConfirmation(){
	if (confirm('Do you want to close this ticket?')) {
            $("form[name='ManageOutageForm']").attr('action' , 'ManageOutage.do?hmode=resolvedByHelpDesk'+addtimestamp2());
			$("form[name='ManageOutageForm']").submit();
        }
}

function escalateToMspDispatchConfirmation() {
	$.ajax({
		type: "POST",
		url: "ManageOutage.do?hmode=countDispatches&jobId="+$("input[name='jobId']").val(),
		success: function(countDispatch){
			if(countDispatch > 1) {
				alert('Cannot Escalate Ticket to MSP Dispatch due to multiple dispatches.');
			} else {
				if (confirm('Do you want to Escalate this ticket to MSP Dispatch?')) {
					$("form[name='ManageOutageForm']").attr('action', 'ManageOutage.do?hmode=escalateToMSPDispatch'+addtimestamp2());
					$("form[name='ManageOutageForm']").submit();
			    }	
			}
		}
	});
}

	

