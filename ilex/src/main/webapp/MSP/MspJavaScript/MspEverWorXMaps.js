/*Initializing  Global var*/
var refreshCounter0 = 0;
var refreshCounter1 = 0;
var refreshCycles0 = false;
var refreshCycles1 = false;
var refreshTimer1 = 60000; // - 60 Sec
var refreshTimer2 = 1560000; // - 29 Min
var refreshTimer3 = 1860000; // - 31 Min
var refreshTimer4 = 1980000; // 33 Min
var refreshTimer5 = 30000; // 30 Sec
var timeOut1 = 2000; // - 4 Sec
var timeOut2 = 8000; // - 8 Sec 
var timeOut3 = 12000; // - 10.2 Sec
var intervalId1;
var intervalId2;
var intervalId3;
var intervalId4;
var intervalId5;

var maps = [];
var allMarkers = [];
var mapZoomLevel = 4;
var centerLat = 38.81362;
var centerLong = -94.554209;
var gicons = [];
var bubbolText = [];
var siteUp = 1;
var siteBouncing = 2;
var siteDown = 3;
var currentSite = 1;
var animationIds = [];
gicons[siteUp] =  "images/green_marker.png";
gicons[siteBouncing] =  "images/yellow_marker.png";
gicons[siteDown] =  "images/red_marker.png";
bubbolText[siteUp] =  "Up";
bubbolText[siteBouncing] =  "Bouncing";
bubbolText[siteDown] =  "Down";

/**
	Initialize GMap. Its should call on page load.
	Its insitialise map, add controls, set default center point and set default zoom level.
	@param	elementId - Div Id where map should display on page.
	@index	index - Index of map[].
*/
function initialiseMap(elementId,index){
	maps[index] = new GMap2(document.getElementById(elementId));
	maps[index].addControl(new GLargeMapControl());
	maps[index].addControl(new GMapTypeControl());
}

/**
	Add controls, set default center point and set default zoom level on given map.
	@index	index - Index of map[].
*/
function setMapAttribute(index){
    var burnsvilleMN = new GLatLng(centerLat,centerLong);
    maps[index].setCenter(burnsvilleMN, 8);
    maps[index].setZoom(mapZoomLevel);
}
/**
	This function is used to set GMarkers with info bubbol on GMap image.
*/
function setMarker(data,index){
	removeMarker(index);
	setMapAttribute(index); // - set default center point and zoom level.
	var divObject = $("#hideMessage").clone();
	var animationId;
	var markers = [];
	$.each(data, function(i, j){
		animationId = divObject.animate({opacity: 0.0}, 50).fadeOut(function(){
			if(j.Latitude != '' && j.Longitude != ''){
			    var point = new GLatLng(j.Latitude, j.Longitude);
				var marker = new GMarker(point,setBaseIcon(j.Status));
				setBubbol(marker,j.Name,j.City,j.State,j.Status);
			    maps[index].addOverlay(marker);
			    markers.push(marker); 
		    }
		});
    });
    allMarkers[index] = markers;
    animationIds[index] = animationId;
}

/**
	This function is used to set bubbol information into Gmarker.
*/
function setBubbol(marker,displayName,city,state,status){
	var htmlString = setShadowData(displayName,city,state,status);
	   	GEvent.addListener(marker, "click", function(){
	   	setShadowData(displayName,city,state,status);
	   	marker.openInfoWindowHtml(htmlString);
    });
}

function setShadowData(displayName,city,state,status){
	$("#shadowDisplayName").text(displayName);
	$("#shadowCity").text(city);
	$("#shadowState").text(state);
	$("#shadowStatus").text(bubbolText[status]);
	return $("#shadowMessage").html();
}

function removeMarker(index){
	if(animationIds[index] != null){
		animationIds[index].remove();
	}
	$(allMarkers[index]).each(function(i,marker){ maps[index].removeOverlay(marker); })
	allMarkers[index] = [];
}

function setBaseIcon(status){
	var baseIcon = new GIcon();
	baseIcon.image = gicons[status];
	baseIcon.shadow="images/shadow_marker.png";
	baseIcon.iconSize = new GSize(20, 20);
	baseIcon.shadowSize = new GSize(37, 20);
	baseIcon.iconAnchor = new GPoint(9, 20);
	baseIcon.infoWindowAnchor = new GPoint(9, 2);
	baseIcon.infoShadowAnchor = new GPoint(18, 25);
	return baseIcon;
}

function getNextStatus(){
	if(currentSite == siteUp){
		currentSite = siteBouncing;
	} else if(currentSite == siteBouncing){
		currentSite = siteDown;
	} else if(currentSite == siteDown){
		currentSite = siteUp;		
	}
	return currentSite;
}

function setAjaxMarker(index){
	$.ajax({
	    type: "post",
	    dataType: "json",
	    url: "EverWorx.do?hmode=getMarkers&status="+index+"&timestamp="+(new Date()*1),
	    async: false,
	    timeout: 10000,
	    error: function() {},
	    success: function(data) {setMarker(data,index)}
	});
}

function showDiv(){
	showMaps(getNextStatus());
}
function stopInterval(siteType){
	clearInterval(intervalId5);	
	showMaps(siteType)
}

function hideMaps()
{
	$("#mapBouncing").fadeOut();		
	$("#mapDown").fadeOut();	
	$("#bouncingButton").fadeOut();			
	$("#downButton").fadeOut();		
}

function showMaps(siteType){
	if(siteType == 1){
		$("#mapBouncing").hide();		
		$("#mapDown").hide();
		$("#bouncingButton").hide();	
		$("#downButton").hide();
		$("#mapUp").show();
		$("#upButton").show();
	} else if(siteType == 2){
		$("#mapUp").hide();
		$("#mapDown").hide();
		$("#upButton").hide();
		$("#downButton").hide();	
		$("#mapBouncing").show();				
		$("#bouncingButton").show();		
	} else if(siteType == 3){
		$("#mapUp").hide();		
		$("#mapBouncing").hide();
		$("#upButton").hide();	
		$("#bouncingButton").hide();	
		$("#mapDown").show();				
		$("#downButton").show();		
	}
	
}
