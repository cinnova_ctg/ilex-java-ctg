
function getAjaxRequestObjectInstallNotes(){
	var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }	
	return ajaxRequest;        
}

function addTicketResolutionDetails(NActiveMonitorStateChangeID, issueOwner, rootCause, resolution) {
	var ajaxRequest = getAjaxRequestObject();
	ajaxRequest.onreadystatechange = function(){
		 if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){ 
		 	
		 }
	}
	
	var url ="ManageOutage.do";
    var params = "hmode=updateResolutionAndActions&NActiveMonitorStateChangeID="+NActiveMonitorStateChangeID+"&issueOwner="+issueOwner+"&rootCause="+rootCause+"&resolution="+resolution;
	ajaxRequest.open("POST", url, true);
	ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajaxRequest.send(params); 	
}