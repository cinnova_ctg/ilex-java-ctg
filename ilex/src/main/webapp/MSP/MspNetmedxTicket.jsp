<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>MSP Search</title>
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language="JavaScript" src="MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>	
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">
$(document).ready(function() {
	/*For ticket serch*/
	$("input[name='search']").click(function(){
		if($("select[name='customer']").val() == '0'){
			alert('Please select customer for search.');
			$("select[name='customer']").focus();
			return false;
		}
		if(jQuery.trim($("input[name='siteSearch']").val()) == ''){
			alert('Please enter site name for search.');
			$("input[name='siteSearch']").val('');
			$("input[name='siteSearch']").focus();
			return false;
		}
		$("form[name='MspNetmedxTicketForm']").attr('action' , 'MspNetmedxTicket.do?hmode=siteSearch');
		$("form[name='MspNetmedxTicketForm']").submit();
	});	
	
	/*For create ticket */
	$("input[name='createTicket']").click(function(){
		if($("select[name='siteName']").val() == '0'){
			alert('Please select Site for create ticket.');
			$("select[name='siteName']").focus();
			return false;
		}
		if($("select[name='problemCategory']").val() == '0'){
			alert('Please select Problem Category for create ticket.');
			$("select[name='problemCategory']").focus();
			return false;
		}
		if(jQuery.trim($("#problemDescription").val()) == ''){
			alert('Please enter Problem Description for create ticket.');
			$("#problemDescription").focus();
			return false;
		}
		$("form[name='MspNetmedxTicketForm']").attr('action' , 'MspNetmedxTicket.do?hmode=createTicket');
		$("form[name='MspNetmedxTicketForm']").submit();
	});	
});			
</script>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<!-- Menu -->
			<jsp:include page="/MSP/MspTopMenu.jsp"/>
		</td>
	</tr>
	<tr>
		<!-- Bread Crumb -->
		<td background="images/content_head_04.jpg" height="21">
			<div id="breadCrumb">
			 	<a href="#" class="bgNone">MSP</a>
			 	<a>Monitor</a>
			 	<a>Administrator</a>
			 	<a><span class="breadCrumb1">Create MSP-NetMedX Ticket</span></a>
			</div>
	    </td>
	</tr>
</table>
<%@include file="/MSP/MspTopMenuScript.inc" %>

<html:form action="MspNetmedxTicket.do" >
<table cellpadding="0" cellspacing="0" border="0"> 
<tr>
<td style="padding-right: 10px;"></td>
<td>
<table cellpadding="0" cellspacing="2" border="0" width="500">
<c:if test="${not empty requestScope.ticketCreateMessage}">
<tr>
	<td class="message">
		<c:out value="${requestScope.ticketCreateMessage}"/>
	</td>
</tr>
</c:if>
<tr>
	<td style=""><h1 style="font-size: 14px;">Create Ticket</h1></td>
</tr>
<tr>
	<td>
	<table cellpadding="4" cellspacing="0" border="0" class="Ntextoleftalignnowrappaleyellowborder2" >
	<tr height="5"></tr>
	<tr>
		<td class="dbvaluesmallFont">Select Customer<font class="red">*</font>:</td>
		<td style="width: 350px;">
			<html:select name="MspNetmedxTicketForm" property="customer" styleClass="select">
				<html:optionsCollection name="MspNetmedxTicketForm" property="customerList"/>
			</html:select>
		</td>
	</tr>
	<tr>
		<td class="dbvaluesmallFont" style="padding-bottom: 15px;">Search for Site<font class="red">*</font>:</td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" >
			<tr>
				<td valign="baseline">
					<html:text name="MspNetmedxTicketForm" property="siteSearch" styleClass="text" size="40"/>
				</td>
			</tr>	
			<tr>	
				<td class = "dbvaluesmallFontBold">
					<font style="color: #b3b2b0;font-weight: normal;"> (Enter the WUG display name or just site number digits)</font>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="dbvaluesmallFont"></td>
		<td>
			<html:button property="search" styleClass="button_c" style="margin-top: 0px;">Search</html:button>
		</td>
	</tr>
	
	<c:if test="${not empty requestScope.search}">
	<tr valign="top">
		<td style="padding-right: 5px;" colspan="2">
			<hr size="1"></hr>
		</td>		
	</tr>
	<tr>
		<td class="labelNoColorBold" colspan="2">Sites Found:</td>
	</tr>
	<c:if test="${not empty MspNetmedxTicketForm.siteList}">
	<tr>
		<td class="dbvaluesmallFont">Site<font class="red">*</font>:</td>
		<td>
			<html:select name="MspNetmedxTicketForm" property="siteName" styleClass="select">
				<html:optionsCollection name="MspNetmedxTicketForm" property="siteList"/>
			</html:select>
		</td>
	</tr>
	<tr>
		<td class="dbvaluesmallFont">Problem Category<font class="red">*</font>:</td>
		<td>
			<html:select name="MspNetmedxTicketForm" property="problemCategory" styleClass="select">
				<html:optionsCollection name="MspNetmedxTicketForm" property="problemCategoryList"/>
			</html:select>
		</td>
	</tr>
	<tr>
		<td class="dbvaluesmallFont" valign="top">Problem Description<font class="red">*</font>:</td>
		<td>
			<html:textarea name="MspNetmedxTicketForm" styleId="problemDescription" property="problemDescription" cols="60" rows="5" styleClass="textarea"/>
		</td>
	</tr>
	<tr>
		<td class="dbvaluesmallFont"></td>
		<td>
			<html:button property="createTicket" styleClass="button_c">Create Ticket</html:button>
		</td>
	</tr>
	</c:if>
	<c:if test="${empty MspNetmedxTicketForm.siteList}">
	<tr>
		<td class="dbvaluesmallFont" colspan="2" style="padding-left: 15px;">
		<font style="color: red;">
		There are no sites matching the criteria you entered. Be sure to enter all or<br> 
		part of the display name used in WUG or just enter the actual digits for<br> 
		the site number.<br><br>
		</font>
		</td>
	</tr>
	</c:if>
	</c:if>
	</table>
	</td>
</tr>
</table>
</td>
</tr>
</table>
</html:form>
</body>
</html>