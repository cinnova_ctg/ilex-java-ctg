<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "MSP/MspJavaScript/MspEverWorXMaps.js?timestamp="+(new Date()*1)></script>
	<jsp:include page="/common/IncludeGoogleKey.jsp"/>
	<style media="screen" type="text/css">
		 .map{ width:950px; height:550px; }
	</style>
	
<script type="text/javascript">
$(document).ready(function(){
	hideMaps();
	$("#monitoringSummary").load("./MspMonitoringSummary.do?fromPage=everWorx&timestamp="+(new Date()*1));
	intervalId1 = setInterval(function(){$("#monitoringSummary").load("./MspMonitoringSummary.do?fromPage=everWorx&timestamp="+(new Date()*1));}, refreshTimer1);
		
	$("#mapUp").load(initialiseMap("mapUp",siteUp));
	intervalId2 = setInterval(function(){setAjaxMarker( siteUp );}, refreshTimer2);
	
	$("#mapBouncing").load(initialiseMap("mapBouncing",siteBouncing));
	intervalId3 = setInterval(function(){setAjaxMarker( siteBouncing );}, refreshTimer3);
	
	$("#mapDown").load(initialiseMap("mapDown",siteDown));
	intervalId4 = setInterval(function(){setAjaxMarker( siteDown );}, refreshTimer4);
	showMaps(siteUp);	
	setTimeout('setAjaxMarker(siteUp)',timeOut1);
	setTimeout('setAjaxMarker(siteBouncing)',timeOut2);
	setTimeout('setAjaxMarker(siteDown)',timeOut3);
	intervalId5 = setInterval(function(){showDiv();}, refreshTimer5);
});
</script>
</head>

<body onunload="GUnload();">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<!-- Menu -->
			<jsp:include page="/MSP/MspTopMenu.jsp"/>
		</td>
	</tr>
	<tr>
		<!-- Bread Crumb -->
		<td background="images/content_head_04.jpg" height="21">
			<div id="breadCrumb">
			 	<a href="#" class="bgNone">MSP</a>
			 	<a href="MspMonitor.do?">Monitor</a>
			 	<a><span class="breadCrumb1">EverWorX Locations</span></a>
			</div>
	    </td>
	</tr>
</table>
<%@include file="/MSP/MspTopMenuScript.inc" %>
<table cellpadding="0" cellspacing="0" border="0" style="padding-top: 10px;">
<tr>
	<td>&nbsp;&nbsp;<td>
	<table cellpadding="0" cellspacing="0" border="0">
	<tr class="Ntextoleftalignnowrappaleyellowborder2">
		<td >
			<div id="monitoringSummary">
				<img src='./images/waiting_tree.gif'>
			</div>
		</td>
		<td align="right" style="padding: 6px 6px 6px 6px;">
				<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
						<table bgcolor="#cccfcc" style="border:1px;" cellpadding="2" cellspacing="0"> 
						<tr>
							<td>
								<table class="BlueTab" cellpadding="0" cellspacing="0" border="0" style="padding-left: 4px;">
								<tr>
									<td class="dbvaluesmallFont">Up:</td>
									<td class="dbvaluesmallFont"><a href="#"><img src="images/green_legend.png" border="0" onclick="stopInterval(siteUp);"/></a>&nbsp;</td>
								</tr>
								<tr>
									<td class="dbvaluesmallFont">Bouncing:&nbsp;</td>
									<td class="dbvaluesmallFont"><a href="#"><img src="images/yellow_legend.png" border="0" onclick="stopInterval(siteBouncing);"/></a>&nbsp;</td>
								</tr>
								<tr>
									<td class="dbvaluesmallFont">Down:</td>
									<td class="dbvaluesmallFont"><a href="#"><img src="images/red_legend.png" border="0" onclick="stopInterval(siteDown);"/></a>&nbsp;</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
	</tr>
	<tr>
		<td style="border-top: inset 1px;" colspan="3">
			 <table cellspacing="0" cellpadding="0" class="BlueTab">
			 <tr>
			 	<td colspan="2">
				 	<table cellpadding="0" cellspacing="0" border="0" style="padding: 3px 3px 3px 3px;">
				 	<tr>
						 <td>
							<div id="mapUp" class="map" ><img src='./images/waiting_tree.gif'></div>
							<div id="mapBouncing" class="map"><img src='./images/waiting_tree.gif'></div>
							<div id="mapDown" class="map"><img src='./images/waiting_tree.gif'></div>
						 </td>
					</tr>
					</table>	
				<td>
			 </tr>
			 <tr>
			 	<td class="dbvaluesmallFont">
			 		&nbsp;&nbsp;<img src="images/green_legend.png" border="0"/>&nbsp;&nbsp;Up
			 		&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/yellow_legend.png" border="0"/>&nbsp;&nbsp;Bounced > 5 Within Last 24 Hours
			 		&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/red_legend.png" border="0"/>&nbsp;&nbsp;Down, Initial Assessment, Help Desk or MSP Dispatch 
			 	</td>
	 			<td class="dbvaluesmallFont" align="right" >
					<div id="upButton"><a href="#" onclick="setAjaxMarker(siteUp);" style="text-decoration: none;">Refresh&nbsp;</a></div>
					<div id="bouncingButton"><a href="#" onclick="setAjaxMarker(siteBouncing);">Refresh</a></div>
					<div id="downButton"><a href="#" onclick="setAjaxMarker(siteDown);">Refresh</a></div>
				</td>
	 	
			 </tr>
			 </table>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<div id="shadowMessage" style="display:none;">
<table>
<tr>
<td style=" padding: 4px 4px 4px 4px;">
    <table cellpadding="0" cellspacing="0" border="0">
    <tr>
    	<td class="dbvaluesmallFont">Display Name:&nbsp;</td>
    	<td class="dbvaluesmallFont" id="shadowDisplayName"></td>
    </tr>
    <tr>
    	<td class="dbvaluesmallFont">City:&nbsp;</td>
    	<td class="dbvaluesmallFont" id="shadowCity"></td>
    </tr>
    
    <tr>
    	<td class="dbvaluesmallFont">State:&nbsp;</td>
    	<td class="dbvaluesmallFont" id="shadowState"></td>
    </tr>
    
    <tr>
    	<td class="dbvaluesmallFont">Status:&nbsp;</td>
    	<td class="dbvaluesmallFont" id="shadowStatus"></td>
    </tr>
    </table>
</td> 
</tr>
</table>
</div>
<div id="hideMessage"></div>
</body>
</html>