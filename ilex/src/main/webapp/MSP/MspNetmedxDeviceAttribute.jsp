<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<title>Site/Device Attribute</title>
</head>
<body>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
	<td><h1 style="font-size: 11px;">Site/Device Information</h1></td>
</tr>
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="dbvaluesmallFont">Customer:&nbsp;</td>
			<td class="dbvaluesmallFont"><bean:write name="ManageMspNetmedxTicketForm" property="customerName"/></td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont">Site:&nbsp;</td>
			<td class="dbvaluesmallFont"><bean:write name="ManageMspNetmedxTicketForm" property="site_name"/></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-top: 5px;"><b>Device Attributes:</b></td>
</tr>

<tr>
	<td style="padding-top: 10px;">
	<table cellpadding="0" cellspacing="1" border="0" class="BlueTab" style="border:0px;">
		<tr>
			<td class="texthdNoBorder" width="100">Attribute</td>
			<td class="texthdNoBorder" width="100">Value</td>
		</tr>
		<tr>
			<td class="texteWhiteBackground">Circuit Type</td>
			<td class="texteWhiteBackground"><bean:write name="ManageMspNetmedxTicketForm" property="circuitType"/></td>
		</tr>
		<tr>
			<td class="texteWhiteBackground">Modem Type</td>
			<td class="texteWhiteBackground"><bean:write name="ManageMspNetmedxTicketForm" property="modemType"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Internet Service Line #</td>
			<td class="texteWhiteBackground"><bean:write name="ManageMspNetmedxTicketForm" property="internetServiceLineNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">ISP</td>
			<td class="texteWhiteBackground"><bean:write name="ManageMspNetmedxTicketForm" property="isp"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">ISP Support #</td>
			<td class="texteWhiteBackground"><bean:write name="ManageMspNetmedxTicketForm" property="ispSupportNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Circuit IP</td>
			<td class="texteWhiteBackground"><bean:write name="ManageMspNetmedxTicketForm" property="circuitIP"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Site Contact #</td>
			<td class="texteWhiteBackground"><bean:write name="ManageMspNetmedxTicketForm" property="siteContactNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Location</td>
			<td class="texteWhiteBackground"><bean:write name="ManageMspNetmedxTicketForm" property="location"/></td>
		</tr>
		
	</table>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-top: 10px;"><b>Site Details</b></td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-top: 10px;">Address:&nbsp;<bean:write name="ManageMspNetmedxTicketForm" property="site_address"/></td>
</tr>

<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="dbvaluesmallFont" nowrap="nowrap">City:&nbsp;<bean:write name="ManageMspNetmedxTicketForm" property="site_city"/></td>
			<td align="center" class="dbvaluesmallFont" nowrap="nowrap" style="width: 150px;">State:&nbsp;<bean:write name="ManageMspNetmedxTicketForm" property="site_state"/></td>
			<td align="right" class="dbvaluesmallFont" nowrap="nowrap">Zip Code:&nbsp;<bean:write name="ManageMspNetmedxTicketForm" property="site_zip"/></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont">Time Zone:&nbsp;<bean:write name="ManageMspNetmedxTicketForm" property="site_time_zone"/></td>
</tr>
</table>
</body>
</html>