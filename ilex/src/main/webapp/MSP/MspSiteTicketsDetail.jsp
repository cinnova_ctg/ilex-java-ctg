<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language="JavaScript" src="MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>	
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
</head>
<script>
function closesiteTicketViewBoxPopup() {
    $.unblockUI(); 
    $("#siteTicketItemValue").empty().html('<img src="./images/waiting_tree.gif" />');
   // return false; 
}
</script>

<%
String backgroundclass="";
boolean csschooser = false;
int rowCount = 0;
%>
<body>
<html:form action="SearchHistory.do">
<%
System.out.println("this come in the called jsp");
%>
<table cellpadding="0" cellspacing="0" border="0" width="400px" align="right"  >
<!--<tr>
		<td>
			<table border='0' cellspacing="0" cellpadding="0" class="Ntextoleftalignnowrappaleyellowborder2" width="100%" >
				<tr>
					<td align="left" style= "padding-left:8px; padding-top: 1px"><h1 style="font-size: 11px; padding-top: 2px;">Site Tickets Information</h1></td>
					<td align="right"><img height="15" width="15" alt="Close" src="images/delete.gif" id="closeShipping" onclick="closesiteTableViewBoxPopup();"></td>
				</tr>
			</table>
		</td>
</tr>
	-->
  <tr>
    <td>
      <table cellpadding="0" cellspacing="1" border="0" class="BlueTab" width="400px">
       <tr>
           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
           <td class="texthdNoBorder"><b>&nbsp;Duration&nbsp;</b></td>
           <%
           System.out.println("this come in the called jsp1");
           %>
           <td class="texthdNoBorder"><b>Up/Down</b></td>
          <td class="texthdNoBorder" style="width: 70px;"><b>Ticket #</b></td>
        </tr>
        <logic:iterate name="MspSearchHistoryForm" id="searchList" property="searchList">  
            <%
            ++rowCount;
			if(rowCount > 15){break;}
            if(csschooser == true){
            	csschooser = false;
				backgroundclass = "Ntexteleftalignnowrap";
			} else{
				csschooser = true;	
				backgroundclass = "Ntextoleftalignnowrap";
			}
            %>
       	 <tr>
         	<td class="<%=backgroundclass %>"><a href="ManageOutage.do?&wugInstance=<bean:write name='searchList' property='wugInstance'/>&NActiveMonitorStateChangeID=<bean:write name='searchList' property='NActiveMonitorStateChangeID'/>&escalationLevel=<bean:write name='searchList' property='escalationLevel'/>&fromPage=searchPage"><bean:write name="searchList" property="displayName"/></a></td>
          	<td class="<%=backgroundclass %>" align="center"><bean:write name="searchList" property="eventDay"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="searchList" property="outageDuration"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="searchList" property="currentStatus"/></td>
	        <td class="<%=backgroundclass %>" align="right"><a href="JobDashboardAction.do?jobid=<bean:write name='searchList' property='jobId'/>&ticket_id=<bean:write name='searchList' property='ticketId'/>&appendix_Id="><bean:write name="searchList" property="ticketReference"/></a></td>
         </tr>
        </logic:iterate>
        <%if(rowCount > 15) {%>     
         <tr>
         	<td nowrap="nowrap" colspan="5" class="labelNoColor" style="padding-top: 10px"><font color="gray">
         	The search results are restrict to 15 matches. If you do not see the desired ticket, narrow the results by adjusting your search criteria.</font></td>
         </tr>
         <%}%>
         <tr><td><font style="line-height: 5px;">&nbsp;</font></td></tr>
        </table>
        </td>
        </tr>
</table>
</html:form>
</body>
</html>