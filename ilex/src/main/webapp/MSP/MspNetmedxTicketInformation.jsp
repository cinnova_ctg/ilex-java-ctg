<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	
	<link href="styles/jquery-ui-slider.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/Core.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/Slider.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/AddJobInstallationNotes.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "MSP/MspJavaScript/ManageMspNetmedxTicket.js?timestamp="+(new Date()*1)></script>
	<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
	<style type="text/css">
	#slider{
		height: 4px; 
		border-color: #E0E0DC #E6E6E3 #8E8E8C #808080; 
		border-style: solid; 
		border-top-width: 2px; 
		border-right-width: 0px; 
		border-bottom-width: 0px; 
		border-left-width: 1px
	}
	</style>
	<title>Ticket Information</title>
</head>
<body>
<html:form action="/ManageMspNetmedxTicket.do?">
<html:hidden name="ManageMspNetmedxTicketForm" property="jobId"/>
<html:hidden name="ManageMspNetmedxTicketForm" property="ticketId"/>
<html:hidden name="ManageMspNetmedxTicketForm" property="estimatedEffort"/>
<html:hidden name="ManageMspNetmedxTicketForm" property="installNotesPresent"/>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<td>
		<h1 style="font-size: 11px;">Ticket Information and Status</h1>
	</td>
</tr>
<tr>
	<td>
		<table class="Ntextoleftalignnowrappaleyellowborder2" cellspacing="8" cellpadding="0" border="0"> 
		<tr>
			<td class="dbvaluesmallFont">Ticket Number:</td>
			<td class="dbvaluesmallFont"><bean:write name="ManageMspNetmedxTicketForm" property="ticketNumber"/>&nbsp;&nbsp;&nbsp;&nbsp;(<a href="JobDashboardAction.do?jobid=<bean:write name="ManageMspNetmedxTicketForm" property="jobId"/>&ticket_id=<bean:write name="ManageMspNetmedxTicketForm" property="ticketId"/>&appendix_Id=<bean:write name="ManageMspNetmedxTicketForm" property="appendixId"/>" styleClass="a">View Ticket Dashboard</a>)</td>
		</tr>
		
		<tr>
			<td class="dbvaluesmallFont" valign="top">Problem Category:</td>
			<td>
				<html:select name="ManageMspNetmedxTicketForm" property="problemCategory" styleClass="select" disabled="true">
					<html:optionsCollection name="ManageMspNetmedxTicketForm"  property="problemCategoryList"  label = "label"  value = "value" />
				</html:select>
			</td>
		</tr>
		
		<tr>
			<td class="dbvaluesmallFont" valign="top">Problem Description:</td>
			<td><html:textarea name="ManageMspNetmedxTicketForm" property="probleDescription" rows="4" cols="55" styleClass="textarea" readonly="true"/></td>
		</tr>
		
		<tr>
			<td  colspan="2" class="dbvaluesmallFont">Installation Notes:</td>
		</tr>
		<tr>
			<td colspan="2" ><html:textarea name="ManageMspNetmedxTicketForm" property="installationNotes" rows="6" cols="80" styleClass="textarea"/></td>
		</tr>
		
		<tr>
			<td class="dbvaluesmallFont">Recent Notes(<a id="installNotesCountDiv" href="ViewInstallationNotes.do?function=view&from=jobdashboard&from=Jobdashboard&jobid=<bean:write name='ManageMspNetmedxTicketForm' property='jobId'/>&appendixid=<bean:write name='ManageMspNetmedxTicketForm' property='appendixId'/>" ><bean:write name='ManageMspNetmedxTicketForm' property='installNotesCount'/></a>)</td>
			<td class="dbvaluesmallFont" align="right"><html:button property="addNotes" styleClass="addButton_c">Add</html:button></td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellpadding="0" cellspacing="0" border="0">
				<tr valign="bottom" >						
					<td width="420">
						<table width="100%" border="1" style="border-width:1px;border-collapse: collapse;" bordercolor="#D9DFEF"class="Ntextoleftalignnowrapblue" cellspacing="0" cellpadding="0">
							<tr >						
								<td width="32%"  valign="top" align="center" id="latestInstallNotesDate" class="dbvaluesmallFont" >
									<bean:write name="ManageMspNetmedxTicketForm" property="latestInstallNotesDate"/>
								</td>								
								<td width="68%"  valign="top" align="left" id="latestInstallNotes"  class="dbvaluesmallFont" style="white-space: normal;">
									<c:out value='${ManageMspNetmedxTicketForm.latestInstallNotes}' escapeXml="false"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr valign="top" style="padding-top: 4px;">	
					<td width="420">										
						<table width="100%" border="1" style="border-width:1px;border-collapse: collapse;" bordercolor="#D9DFEF" bgcolor="white"  class="Ntexteleftalignnowrapnoback"   cellspacing="0" cellpadding="0">
							<tr>						
								<td width="32%" align="center" valign="top" id="secondLatestInstallNotesDate" class="dbvaluesmallFont" >
									<bean:write name="ManageMspNetmedxTicketForm" property="secondLatestInstallNotesDate"/>
								</td>								
								<td width="68%" align="left" valign="top" id="secondLatestInstallNotes" class="dbvaluesmallFont" style="white-space: normal;">
									<c:out value='${ManageMspNetmedxTicketForm.secondLatestInstallNotes}' escapeXml="false"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>		
		</table>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td style="white-space: nowrap;">
				<h1 style="font-size: 11px;">Complete and Close</h1>
			</td>
			<td id="hideMessage" style="color: #b3b2b0;font-size: 11px;font-weight: normal; white-space: nowrap; padding: 12px 0px 2px 0px; margin: 0px 0px 0px 0px;">
					&nbsp;
				</td>
		</tr>
		</table>
		<table class="Ntextoleftalignnowrappaleyellowborder2" cellspacing="8" cellpadding="0" border="0" width="100%" id="updateResolution">
		<tr class="Ntextoleftalignnowrappaleyellowborder2"> 
			<td class="Ntextoleftalignnowrappaleyellowborder2">&nbsp;</td>
			<td class="buttonrow" align="right" style="background: #FAF8CC; padding-right: 8px;" >
				<input type="button" name="updateEffort" class="button" value="Update" />
			</td>
		</tr> 
		<tr>
			<td class="dbvaluesmallFont" valign="bottom">Estimated Effort:</td>
			<td>
			<table cellpadding="0" cellspacing="0" style="width: 300px;">
			<tr>
			    <td class="sliderFont" style="0px" align="right">0</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">5</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">10</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">15</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">20</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">25</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">30</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">35</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">40</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">45</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">50</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">55</td>
				<td class="sliderFont" style="width: 8.33%;" align="right">60</td>
				<td></td>
			</tr>	
			<tr>
				<td class="sliderFont"></td>
				<td colspan="12" valign="top" style="padding-top: 5px;">
					<div id="slider"></div>
				</td>
				<td class="sliderFont">
					&nbsp;&nbsp;(Minutes)
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="dbvaluesmallFont"><html:button property="closeTicket" styleClass="addButton_c">Close Ticket</html:button></td>
		</tr>
		</table>
	</td>
</tr>
</table>
</html:form>
</body>
</html>