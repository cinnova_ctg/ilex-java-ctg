<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
     <tr>
       <td width="40%" class="dbvaluesmallFont">Last Update:&nbsp;&nbsp;<bean:write name="realTimeStateInfo" property="runTimeDisplay"/>
       <logic:equal name="realTimeStateInfo" property="runTimeAlert" value="1"><img src="images/cau.gif" alt="" hspace="5" style="vertical-align: bottom;"/></logic:equal>
       </td>
       <td  width="40%" class="dbvaluesmallFont" align="left">Event Count:&nbsp;&nbsp;<bean:write name="realTimeStateInfo" property="eventCount"/></td>
       <%--Code Commented On 20 December 2010 : For Removing the Site Error :XXX section --%>
       <%--<td class="dbvaluesmallFont" align="right">Site Errors:&nbsp;&nbsp;XXX</td>--%>
       <td width="20%" class="dbvaluesmallFont" align="right">&nbsp;</td>
     </tr>
   </table>
</body>
</html>