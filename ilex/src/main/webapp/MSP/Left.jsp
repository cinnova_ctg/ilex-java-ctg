<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<html:html>
<head>
	<link href="styles/jquery-tab.css" rel="stylesheet" type="text/css" />
		<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/treeviewscript/jquery-treeview-main-all.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/jquery-tab.min.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/jquery.corner.js?timestamp="+(new Date()*1)></script>
	<script language = "JavaScript" src = "javascript/jquery.blockUI.js?timestamp="+(new Date()*1)></script>
	<style type="text/css">
		body {font-family : "Arial"; font-size : 10px; cursor : auto; color : #000000; padding : 0px 0px 0px 10px; white-space : nowrap;}
		a {text-decoration : none;	color : #000000;}
		a:hover {text-decoration: underline;color : #000000;}
	</style>
	<title>Monitoring Section Left Tree Menu</title>
<script>	
$(document).ready(function(){
	/*For top round corner of left tree tab.*/
	$("#tab-1").corner("top");
	$("#tab-2").corner("top");
	$("#tab-3").corner("top");

	/*Overwride ilextbottom from properties.*/
	$('input:button',window.parent.ilexfooter.document.forms[0]).click(function(){
		if ($(this).val() == '<'){
			window.top.frames['ilexbottom'].document.body.cols="280,*";
		}
	});
	
	
	$("#tabs>ul li a span").click(function(){
		if($(this).parent().parent().attr("id")=="tab-3"){
			window.top.frames['ilexbottom'].document.body.cols="280,*";
		}else{
			window.top.frames['ilexbottom'].document.body.cols="210,*";
		}
	});
	
	$("#tabs>ul li a").click(function(){
		if($(this).parent().attr("id")=="tab-3"){
			window.top.frames['ilexbottom'].document.body.cols="280,*";
		}else{
			window.top.frames['ilexbottom'].document.body.cols="210,*";
		}
	});
	if(${param.selectedtab}==2){
		window.top.frames['ilexbottom'].document.body.cols="280,*";
	}
$("#tabs").tabs('option', 'selected', ${param.selectedtab});
	
});	
function openHelpDeskWindow(){
	alert("Hello");
	window.top.frames['ilexbottom'].location="/Ilex-WS/hdMainTable/tableMainIndex.html?userid=12345";
}



</script>
</head>
<body bgcolor="#f4f9fd" style="padding-left: 0px;">
	<div id="tabs" style=" background-color: #f4f9fd;">
    <ul style=" background-color: #f4f9fd;">
    		<li id="tab-1"><a href="#fragment-1" onclick="openHelpDeskWindow();"><span>Help Desk</span></a></li>
        	<li id="tab-2" ><a href="#fragment-2"><span>PCI</span></a></li>
        	<li id="tab-3" ><a href="#fragment-3"><span>Admin</span></a></li>
    </ul>
  
	    <div id="fragment-2" style=" background-color: #f4f9fd; border: 0px;">
		<ul id="pci" class="treeview-default" style="margin: 0px; padding-left: 0px; ">
			<li style="padding-left: 0px;"><span><font style=" font-weight: bold; vertical-align: top;"><img alt="" src="images/msp_image.jpeg"/>Change Request</font></span>
				<ul style=" background-color: #f4f9fd;">
					<li style=" background-color: #f4f9fd; " ><span><font style="vertical-align:top;"><a href="PCIControlSearchAction.do" target="ilexmain">Search</a></font></span></li>
					<li style=" background-color: #f4f9fd;"><span><font style="vertical-align:top;"><a href="PCIChangeControlAction.do?" target="ilexmain">Create New Request</a></font></span></li>
				</ul>
			</li>
			<li style="padding-left: 0px;"><span><font style="vertical-align:top; font-weight: bold; "><img alt="" src="images/msp_image.jpeg"/>Policies and Procedures</font></span>
				<ul style=" background-color: #f4f9fd;">
					<li style=" background-color: #f4f9fd;"><span><font style="vertsical-align:top;"><a href="#">Policy 1</a></font></span></li>
					<li style=" background-color: #f4f9fd;" ><span><font style="vertical-align:top;"><a href="#">Policy 2</a></font></span></li>
				</ul>
			</li>
		</ul>
		</div>
		
		
		
	    <div id="fragment-1" style=" background-color: #f4f9fd; border: 0px;">
		</div>
		
	<div id="fragment-3" style=" background-color: #f4f9fd; border: 0px;">
		<ul id="admin" class="treeview-default" style="margin: 0px; padding-left: 0px; ">
			<li style="padding-left: 0px;"><span><font style=" font-weight: bold; vertical-align: top;"><img alt="" src="images/msp_image.jpeg"/>Help Desk</font></span>
				<ul style=" background-color: #f4f9fd;">
					<li style=" background-color: #f4f9fd; " ><span><font style="vertical-align:top;"><a href="/Ilex-WS/hdTicketMonitoring/getAcquiredTickets.html?userid=${sessionScope.userid}" target="ilexmain">HD Ticket Monitoring</a></font></span></li>
				</ul>
			</li>
			<li><div style="border-top: 1px dotted grey;width: 180px;">&nbsp;</div></li>
			<li style="padding-left: 0px;"><span><font style="vertical-align:top; font-weight: bold; "><img alt="" src="images/msp_image.jpeg"/>Monitoring Setup/Support</font></span>
				<ul style=" background-color: #f4f9fd;">
					<li style=" background-color: #f4f9fd;"><span><font style="vertsical-align:top;"><a href="/Ilex-WS/hdGroupMgmt/setupNewCustomer.html?userid=${sessionScope.userid}" target="ilexmain">Setup New Customer</a></font></span></li>
					<li style=" background-color: #f4f9fd;" ><span><font style="vertical-align:top;"><a href="/Ilex-WS/hdGroupMgmt/viewCustomers.html?customerType=MonitoringCustomers&pageIndex=1&userid=${sessionScope.userid}" target="ilexmain" >View Monitoring Customers</a></font></span></li>
					<li style=" background-color: #f4f9fd;" ><span><font style="vertical-align:top;"><a href="/Ilex-WS/hdGroupMgmt/viewCustomers.html?customerType=AlertXCustomers&pageIndex=1&userid=${sessionScope.userid}" target="ilexmain" >View AlertX Customers</a></font></span></li>
				</ul>
			</li>
			<li style="padding-left: 0px;margin-top: 0px;padding-top: 0px;"><span><font style="vertical-align:top; font-weight: bold; "><img alt="" src="images/msp_image.jpeg"/>Monitoring Requirements Management</font></span>
				<ul style=" background-color: #f4f9fd;">
					<li style=" background-color: #f4f9fd;"><span><font style="vertsical-align:top;"><a href="/Ilex-WS/masterRequirement/viewMasterRequirement.html?pageIndex=1&userid=${sessionScope.userid}" target="ilexmain">Master Requirements</a></font></span></li>
					<li style=" background-color: #f4f9fd;" ><span><font style="vertical-align:top;"><a href="/Ilex-WS/customerRequirement/viewCustomers.html?pageIndex=1&userid=${sessionScope.userid}" target="ilexmain" >Customer/Project Requirements</a></font></span></li>
				</ul>
			</li>
		</ul>
	</div>  
		
		
		
	</div>
<script>
/*For tab*/
$("#tabs").tabs();


/*For Treemenu*/
$("#pci").treeview({
	collapsed: true,
	control: "#treecontrol"
});

$("#helpDesk").treeview({
	collapsed: false,
	control: "#treecontrol"
});
$("#admin").treeview({
	collapsed: false,
	control: "#treecontrol"
});
// $("#monitoring").treeview({
// 	collapsed: false,
// 	control: "#treecontrol"
// });
</script>
</body>
</html:html>

 