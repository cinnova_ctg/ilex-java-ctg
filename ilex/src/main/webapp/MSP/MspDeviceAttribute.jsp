<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<title>Site/Device Attribute</title>
</head>
<body>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td><h1 style="font-size: 11px;">Site/Device Information</h1></td>
			<td align="right" valign="bottom">
				<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="dbvaluesmallFont"><b>Current Status:</b>&nbsp;</td>
					<td class="dbvaluesmallFont" id="siteCurrentStatus"><b><bean:write name="ManageOutageForm" property="currentStatus"/></b></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td align="left" class="dbvaluesmallFont" style="width: 200px; white-space: nowrap;">Site:&nbsp;<bean:write name="ManageOutageForm" property="siteNumber"/></td>
			<td align="right" class="dbvaluesmallFont" >Escalation Level:&nbsp;<bean:write name="ManageOutageForm" property="escalationLabel"/></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-top: 20px;"><b>Updated By:</b>&nbsp;<bean:write name="ManageOutageForm" property="lastAction"/></td>
</tr>
<tr>
	<td class="dbvaluesmallFont"><b>Device Attributes:</b></td>
</tr>

<tr>
	<td style="padding-top: 10px;">
	<table cellpadding="0" cellspacing="1" border="0" class="BlueTab" style="border:0px;">
		<tr>
			<td class="texthdNoBorder" width="100">Attribute</td>
			<td class="texthdNoBorder" width="100">Value</td>
		</tr>
		<tr>
			<td class="texteWhiteBackground">Circuit Type</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="circuitType"/></td>
		</tr>
		<tr>
			<td class="texteWhiteBackground">Modem Type</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="modemType"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Internet Service Line #</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="internetServiceLineNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">ISP</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="isp"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">ISP Support #</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="ispSupportNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Circuit IP</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="circuitIP"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Site Contact #</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="siteContactNo"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Location</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="location"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">VOIP</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="VOIP"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Account</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="accountNew"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Access Code</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="accessCode"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Help Desk</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="helpDeskNew"/></td>
		</tr>
		
		<tr>
			<td class="texteWhiteBackground">Fax #</td>
			<td class="texteWhiteBackground"><bean:write name="ManageOutageForm" property="fax"/></td>
		</tr>
		
	</table>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-top: 10px;"><b>Site Details</b></td>
</tr>
<tr>
	<td class="dbvaluesmallFont" style="padding-top: 10px;">Address:&nbsp;<bean:write name="ManageOutageForm" property="siteAddress"/></td>
</tr>

<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="dbvaluesmallFont" nowrap="nowrap">City:&nbsp;<bean:write name="ManageOutageForm" property="siteCity"/></td>
			<td align="center" class="dbvaluesmallFont" nowrap="nowrap" style="width: 150px;">State:&nbsp;<bean:write name="ManageOutageForm" property="siteState"/></td>
			<td align="right" class="dbvaluesmallFont" nowrap="nowrap">Zip Code:&nbsp;<bean:write name="ManageOutageForm" property="siteZip"/></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="dbvaluesmallFont">Time Zone:&nbsp;<bean:write name="ManageOutageForm" property="siteTimeZone"/></td>
</tr>
</table>
</br>

</body>
</html>