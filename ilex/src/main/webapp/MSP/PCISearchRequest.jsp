<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html:html>
<head>

<link rel="stylesheet" href="styles/style_common.css" type="text/css">
<link rel="stylesheet" href="styles/style.css" type="text/css">

<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language="JavaScript" src="javascript/JQueryMain.js?timestamp="
	+(newDate()*1)></script>
<script language="JavaScript"
	src="MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp=" +(newDate()*1)></script>
<script language="JavaScript" src="javascript/jquery.readonly.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>


<script>
	$(document).ready(function() {
		$("#PCIContent").hide();
		//Select list(Site Identifier) prepration for the perticular customer selection
		$('#customer').change(function() {
			$(function() {
				$("select#customer").change(function() {
					$.getJSON("PCIControlSearchAction.do?hmode=populateSiteIdentifierForSearch&customer="+ document.forms[0].orgTopId.value,{customer : $(this).val(),ajax : 'true'},
						function(j) {
							var options = '';
							for ( var i = 0; i < j.length; i++) {
								options += '<option value="' + j[i].value + '">'+ j[i].label+ '</option>';
							}
							$("select#siteIdentifier").html(options);
						});
				});
			});
		});

	//Code for searching the data according to the data provided from the frontend
	$("#searchButton").click(function() {
		//Validation for the Date Fileds
			if(trim($("input[name='toDate']").val()) != '' && trim($("input[name='fromDate']").val()) != ''){
				if(!compDate_mdy( $("input[name='fromDate']").val(),$("input[name='toDate']").val() )){
					alert('To date should be greater than From date.');
					return false;
				}	
			}

		//Wait Message
		$("#searchData").empty().html('<center><img src="./images/waiting_tree.gif" /></center><br><center>Please Wait...</center></br>');
		//Ajax Json Call
		$.getJSON("PCIControlSearchAction.do?hmode=pciControlSearch&customer="+ document.forms[0].orgTopId.value+
				"&selectedChangeRequestStatus="+document.forms[0].selectedChangeRequestStatus.value+
				"&fromDate="+document.forms[0].fromDate.value+
				"&selectedSiteID="+document.forms[0].selectedSiteID.value+
				"&toDate="+document.forms[0].toDate.value,{customer : $(this).val(),ajax : 'true'},function(j) {
			var tableData = '';
			var changeDiscription='';
			//prepare the table header
			tableData += '<table border="0" cellpadding="0" cellspacing="1" class="TBSTD0001">';
			tableData += '<thead>';
			tableData += '<tr><th>Customer</th>';
			tableData += '<th>Status</th>';
			tableData += '<th>Site</th>';
			tableData += '<th>Change Description</th></tr>';
			tableData += '</thead>';
			//get the data and iterate it according to the fields
			if(j.length!=0){
				for ( var i = 0; i < j.length; i++) {
					if (i % 2 == 0)
						className = 'PCSTDEVEN0001 PCSTDODD0001 PCSTD0003';
					else
						className = 'PCSTDODD0001 PCSTDODD0001 PCSTD0003';
					tableData += '<tr>';
					tableData += '<td class="'+className+'">'+ j[i].customer + '</td>';
					tableData += '<td class="'+className+'">'+ j[i].status + '</td>';
					tableData += '<td class="'+className+'">'+ j[i].site + '</td>';
					changeDiscription=j[i].changeDiscription.substr(0,50);
					tableData += '<td class="'+className+'"><a href="PCIChangeControlAction.do?hmode=pciViewChangeRequest&changeRequestID='+j[i].changeRequestID+'">'+ changeDiscription + '</a></td>';
					tableData += '<tr>';
				}
			}else{
				tableData += '<td><center><font class=MSSUC0001>No change request found.</font></center></td>';
			}
			tableData += '</table>';
			$("#PCIContent").show();
			$("div#searchData").html(tableData);
		});
	});
});

	function selectDate(obj) {
		return popUpCalendar(obj, obj, 'mm/dd/yyyy');
	}
//
--></script>
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td><!-- Menu --> <jsp:include page="/MSP/MspTopMenu.jsp" /></td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
			<div id="BCNAV0001"><a class="BCNAV0002">MSP</a> <a>PCI
			Change Control</a> <a>Change Request</a> <a><span class="BCPAG0001">Search</span></a>
			</div>
			</td>
		</tr>
	</table>
	<%@include file="/MSP/MspTopMenuScript.inc" %>
<html:form action="PCIControlSearchAction.do" styleId="PCIControlSearchForm">
	
	<table border="0" cellpadding="0" cellspacing="0" class="WASTD0001">
		<tr>
			<td>
			<div>
			<div>
			<table>
				<tr>
					<td class="PCSSH0001" height="20" colspan="5">Search for Change Request</td>
				</tr>
				<TR><TD height="10px;"></TD></TR>
				<tr>
					<td class="PCLAB0001">Customer:</td>
					<td>
						<html:select property="orgTopId" styleId="customer"	size="1" styleClass="PCCOMBO001">
							<html:optionsCollection name="PCIControlSearchForm"	property="customerList" value="value" label="label" />
						</html:select>
					</td>
					<td class="PCLAB0001">
					<div align="right">Date</div>
					</td>
				</tr>
				<tr>
					<td class="PCLAB0001">Status:</td>
					<td>
						<html:select property="selectedChangeRequestStatus"	size="1" styleClass="PCCOMBO001">
							<html:optionsCollection name="PCIControlSearchForm" property="selectedChangeRequestStatusList" value="value" label="label" />
						</html:select></td>
					<td />
					<td class="PCLAB0001">From:</td>
					<td class="PCSTD0003 "><html:text styleClass="PCTXT0001"	property="fromDate" size="10" onclick="selectDate(this)" readonly="readonly"></html:text></td>
				</tr>
				<tr>
					<td class="PCLAB0001">Site Identifier:</td>
					<td>
						<html:select styleId="siteIdentifier" property="selectedSiteID" size="1" styleClass="PCCOMBO001">
							<html:optionsCollection name="PCIControlSearchForm" property="siteIdentifierList" value="value" label="label" />
						</html:select>
					</td>
					<td />
					<td class="PCLAB0001">To:</td>
					<td class="PCSTD0003 "><html:text styleClass="PCTXT0001" property="toDate" size="10" onclick="selectDate(this)" readonly="readonly"></html:text></td>
				</tr>
				<tr>
					<td><html:button styleClass="FMBUT0001" styleId="searchButton" property="Search" value="Search" /></td>
				</tr>
			</table>
			</div>
			</div>
			</td>
		</tr>

		<tr>
			<td>
			<div id="PCIContent">
			<table class="PCSSH0002">
				<tr>
					<td class="PCSSH0001">Search Result</td>
				</tr>
			</table>
				<div id="searchData"></div>
			</div>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>