<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%
String backgroundclass="";
boolean csschooser = false;
boolean refreshTimer = true;
int i=0;
%>
<script language = "JavaScript" src = "javascript/JQueryMain.js"></script>
</head>
<body>
<table cellpadding="0" cellspacing="0" border="0">
<%--
<tr>
	<td><h1 style="font-size: 14px;">Monitoring Summary</h1></td>
</tr>
--%>
<tr style="padding-top: 2px;">
	 <td>
		<table cellpadding="0" cellspacing="5" class="Ntextoleftalignnowrappaleyellowborder2">
		   <tr>
			   <td>
				   <table cellpadding="0" cellspacing="1" border="0">
				   	<tr>
				     <td>&nbsp;</td>
				     <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
				     <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Down&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
				     <td class="texthdNoBorder"><b>Initial Assessment</b></td>
				     <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;Help Desk&nbsp;&nbsp;&nbsp;</b></td>
				     <td class="texthdNoBorder"><b>MSP Dispatch</b></td>
				     <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer/Manual&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
				     <%-- <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bounces&nbsp;&nbsp;&nbsp;&nbsp;</b></td>--%>
				     </tr>
				     <logic:iterate id="mspMonitoringSummary" name="MspMonitoringSummaryForm" property="monitoringSummaryList">
				     <bean:define id="displaySymbol" name="mspMonitoringSummary" property="displaySymbol" type = "java.lang.Integer" />
				     <%
			            if(csschooser == true){
			            	csschooser = false;
							backgroundclass = "Ntexteleftalignnowrap";
						} else{
							csschooser = true;	
							backgroundclass = "Ntextoleftalignnowrap";
						}
		             %>
				     <tr>
				       <td class = "dbvaluesmallFont"><bean:write name="mspMonitoringSummary" property="line"/>&nbsp;</td>
				       <td class="<%=backgroundclass %>" align="right"><bean:write name="mspMonitoringSummary" property="active"/></td>
				       <td class="<%=backgroundclass %>" align="right"><bean:write name="mspMonitoringSummary" property="down"/></td>
				       <td>
					       <table cellpadding="0" cellspacing="0" border="0" width="100%">
					       <tr>
						       <td class="<%=backgroundclass %>" align="left" id="displayImage<%=i%>">
						       <logic:equal name="mspMonitoringSummary" property="displaySymbol" value="1">
						       	<img src="images/cau.gif" alt="" border="0" style="vertical-align: bottom;"/>
						       	</logic:equal>
						       </td>
						       <td class="<%=backgroundclass %>" align="right"><bean:write name="mspMonitoringSummary" property="initialAssessment"/></td>
						       </tr>
						       <script type="text/javascript">
						        var displaySymbol<%=i%> = <bean:write name="mspMonitoringSummary" property="displaySymbol"/>
						        
						        if(refreshCounter<%=i%> > 4){
						        	refreshCycles<%=i%> = false;
						        	refreshCounter<%=i%> = 0;
						        }
						        
						        if(displaySymbol<%=i%> == 1 && refreshCounter<%=i%> == 0){
						        	refreshCycles<%=i%> = true;
						        	refreshCounter<%=i%> = 0;
						        }
						        
						        if(refreshCycles<%=i%>){
   									$("#displayImage<%=i%>").html("<img src='images/cau.gif' alt='' border='0' style='vertical-align: bottom;'/>");
   									refreshCounter<%=i%> = refreshCounter<%=i%>+1;
						        }
							   </script>	
					       </table>
				       <td class="<%=backgroundclass %>" align="right"><bean:write name="mspMonitoringSummary" property="helpDesk"/></td>
				       <td class="<%=backgroundclass %>" align="right"><bean:write name="mspMonitoringSummary" property="mspDispatch"/></td>
				       <td class="<%=backgroundclass %>" align="right"><bean:write name="mspMonitoringSummary" property="watch"/></td>
				       <%-- <td class="<%=backgroundclass %>" align="right"><bean:write name="mspMonitoringSummary" property="bounces"/></td>--%>
				     </tr>
				     <%i++; %>
				     </logic:iterate>
				</table>
			</td>
			
		</tr>
		</table>
	</td>
</tr>
</table>
</body>
</html>