<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<%
String slaTable = "";
String countTable = "";
String costTable = "";

slaTable = request.getAttribute("SLATable") == null ?"":request.getAttribute("SLATable").toString();
countTable = request.getAttribute("CountTable") == null ?"":request.getAttribute("CountTable").toString();
costTable = request.getAttribute("CostTable") == null ?"":request.getAttribute("CostTable").toString();
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>System Performance</title>
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	
<script type="text/javascript">
	$(document).ready(function() {
		changeCustomerName();
	 });
	 
	function updateMap() {
		document.forms[0].submit();
	}
	function changeCustomerName() {
		//alert(document.forms[0].customer.value)
		//document.getElementById("customername").innerText= document.forms[0].customer.value;
		var objectValue = document.forms[0].customer;
		var text = objectValue.options[objectValue.selectedIndex].text;
		if(objectValue) {
			document.getElementById("customername").innerHTML = "<h1 style='font-size: 14px; white-space: nowrap;'>"+text+"</h1>";
		}
	}
	
</script>
</head>
<%
String backgroundclass="";
boolean csschooser = false;
int rowCount = 0;
%>
<body>


<html:form action="SystemPerformance.do">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<!-- Menu -->
			<jsp:include page="/MSP/MspTopMenu.jsp"/>
		</td>
	</tr>
	<tr>
		<!-- Bread Crumb -->
		<td background="images/content_head_04.jpg" height="21">
			<div id="breadCrumb">
			 	<a href="#" class="bgNone">MSP</a>
			 	<a>Reports</a>
			 	<a>System Performance</a>
			 	<a><span class="breadCrumb1">Dashboard</span></a>
			</div>
	    </td>
	</tr>
</table>
<%@include file="/MSP/MspTopMenuScript.inc" %>
<table border="0" cellpadding="0" cellspacing="0" style="padding-top: 0px;">
<tr >
	<td style="padding-left: 10px;"></td>
	<td>
		<table width="720">
			<tr>
				<td id="customername"><h1 style="font-size: 14px; white-space: nowrap;">All Customers</h1></td>
				<td align="right">
					<html:select name="SystemPerformanceFrom" property="customer" styleClass="select" onchange="updateMap();">
						<html:optionsCollection name="SystemPerformanceFrom" property="customerList"/>
					</html:select>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td style="width: 10px;"></td>
	<td style="padding-top: 0px; " >
		<jsp:include page="/MSP/Graph.jsp"/>
	</td>
</tr>
<tr>
	<td style="width: 10px;"></td>
	<td style="padding-top: 0px;">
		<table cellpadding="0" cellspacing="1" border="0" width="720">
			<tr>
				<td height="25"><h1 style="font-size: 14px; white-space: nowrap;">Count of Sites under Contract</h1></td>
				<td style="padding-left: 10px;"><h1 style="font-size: 14px; white-space: nowrap;">Average Site Performance</h1></td>
				<td style="padding-left: 10px;"><h1 style="font-size: 14px; white-space: nowrap;">Average Cost</h1></td>
			</tr>
			<tr>
			
				<td><table style="border:2px solid #e8e8e5;"><tr><td><%=countTable%></td></tr></table></td>
				<td style="padding-left: 10px;"><table style="border:2px solid #e8e8e5;"><tr><td><%=slaTable%></td></tr></table></td>
				<td style="padding-left: 10px;"><table style="border:2px solid #e8e8e5;"><tr><td><table border="0" cellpadding="0" cellspacing="1"> 
<tr class="tr1">
	<td class="td_h1">&nbsp;</td>
	<td class="td_h1">DHCP</td>
	<td class="td_h1">PPPoE</td>
	<td class="td_h1">Static</td>
	<td class="td_h1">Overall</td>
</tr>
<tr class="tr1">
	<td class="td_h2">ADSL</td>
	<td class="td_d0"></td>
	<td class="td_d0"></td>
	<td class="td_d0"></td>
	<td class="td_ds"></td>
</tr>
<tr class="tr1">
	<td class="td_h2">Cable</td>
	<td class="td_d1"></td>
	<td class="td_d1">&nbsp;</td>
	<td class="td_d1"></td>
	<td class="td_ds"></td>
</tr>
<tr class="tr1">
	<td class="td_h2">EVDO</td>
	<td class="td_d0">&nbsp;</td>
	<td class="td_d0">&nbsp;</td>
	<td class="td_d0"></td>
	<td class="td_ds"></td>
</tr>
<tr class="tr1">
	<td class="td_h2">FIOS</td>
	<td class="td_d1"></td>
	<td class="td_d1">&nbsp;</td>
	<td class="td_d1"></td>
	<td class="td_ds"></td>
</tr>
<tr class="tr1">
	<td class="td_h2">FW</td>
	<td class="td_d0"></td>
	<td class="td_d0"></td>
	<td class="td_d0"></td>
	<td class="td_ds"></td>
</tr>
<tr class="tr1">
	<td class="td_h2">T1</td>
	<td class="td_d1">&nbsp;</td>
	<td class="td_d1">&nbsp;</td>
	<td class="td_d1"></td>
	<td class="td_ds"></td>
</tr>
<tr class="tr1">
	<td class="td_h2">Overall</td>
	<td class="td_ds"></td>
	<td class="td_ds"></td>
	<td class="td_ds"></td>
	<td class="td_ds"></td>
</tr>
</table></td></tr></table></td>
			</tr>
		</table>
	</td>
</tr>
</table>

</html:form>
</body>
</html>

