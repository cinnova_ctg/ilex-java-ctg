<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>MSP Search</title>
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src="javascript/date-picker.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language="JavaScript" src="javascript/popcalendar.js"></script>
	<script language="JavaScript" src="MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>	
	<script language="JavaScript" src="MSP/MspJavaScript/SearchHistory.js?timestamp="+(new Date()*1)></script>	
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/style_common.css" rel="stylesheet" type="text/css" />
	<link href="styles/dropdownStyle.css" rel="stylesheet" type="text/css" />
</head>
<%
String backgroundclass="";
boolean csschooser = false;
int rowCount = 0;
%>
<body>
<c:choose>
	<c:when test="${fromHD}">
		<jsp:include page="/MSP/hdMspBreadCrumb.jsp"/>
	</c:when>
	<c:otherwise>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td>
					<!-- Menu -->
					<jsp:include page="/MSP/MspTopMenu.jsp"/>
				</td>
			</tr>
			<tr>
				<!-- Bread Crumb -->
				<td background="images/content_head_04.jpg" height="21">
					<div id="breadCrumb">
					 	<a href="#" class="bgNone">MSP</a>
					 	<a>History</a>
					 	<a><span class="breadCrumb1">Search for Closed Ticket</span></a>
					</div>
			    </td>
			</tr>
		</table>
		<%@include file="/MSP/MspTopMenuScript.inc" %>
	</c:otherwise>
</c:choose>
<html:form action="SearchHistory.do">
	<c:choose>
		<c:when test="${fromHD}">
			<html:hidden property="fromHD" value="true"></html:hidden>
		</c:when>
	</c:choose>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td style="width: 10px;"></td>
<td style="padding-top: 5px;">
	<table cellpadding="0" cellspacing="4" border="0">
	<tr>
		<td class="labelNoColorBold" style="width: 80px;">Customer:</td>
		<td>
			<html:select name="MspSearchHistoryForm" property="customer" styleClass="select">
				<html:optionsCollection name="MspSearchHistoryForm" property="customerList"/>
			</html:select>
		</td>
	</tr>
	<tr>
		<td class="labelNoColorBold">Site:</td>
		<td class="labelNoColorBold">
			<html:text name="MspSearchHistoryForm" property="site" styleClass="text" size="30"/>
		</td>
	</tr>
	<tr>
		<td class="labelNoColorBold">From:</td>
		<td class="labelNoColorBold">
			<html:text name="MspSearchHistoryForm" property="from" styleClass="text" size="10"/>
			<img style="vertical-align: top;" src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].from , document.forms[0].from , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
			<font style="width: 60px;">&nbsp;</font>
			To:&nbsp;&nbsp;
			<html:text name="MspSearchHistoryForm" property="to" styleClass="text" size="10"/>
			<img style="vertical-align: top;" src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].to , document.forms[0].to , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
		</td>
	</tr>
	<tr>
		<td class="labelNoColorBold"><html:button property="search" styleClass="button_c">Search</html:button></td>
		<td class="labelNoColorBold"><html:reset property="cancel" styleClass="button_c">Cancel</html:reset></td>
	</tr>
	</table>
</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="padding-left: 10px;">
<tr>
	<td>&nbsp;</td>
</tr>
<tr style="padding-top: 2px;">
<td>
<table border="0" cellspacing="0" cellpadding="2" class="BlueTab">
  <tr>
    <td>
       <table border="0" cellpadding="0" cellspacing="1">
       <tr>
           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
           <td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
           <td class="texthdNoBorder"><b>&nbsp;Duration&nbsp;</b></td>
           <td class="texthdNoBorder"><b>Up/Down</b></td>
          <td class="texthdNoBorder" style="width: 70px;"><b>Ticket #</b></td>
        </tr>
        <logic:iterate name="MspSearchHistoryForm" id="searchList" property="searchList">  
            <%
            ++rowCount;
			if(rowCount > 50){break;}
            if(csschooser == true){
            	csschooser = false;
				backgroundclass = "Ntexteleftalignnowrap";
			} else{
				csschooser = true;	
				backgroundclass = "Ntextoleftalignnowrap";
			}
            %>
       	 <tr>
         	<td class="<%=backgroundclass %>"><a href="ManageOutage.do?&wugInstance=<bean:write name='searchList' property='wugInstance'/>&NActiveMonitorStateChangeID=<bean:write name='searchList' property='NActiveMonitorStateChangeID'/>&escalationLevel=<bean:write name='searchList' property='escalationLevel'/>&fromPage=searchPage"><bean:write name="searchList" property="displayName"/></a></td>
          	<td class="<%=backgroundclass %>" align="center"><bean:write name="searchList" property="eventDay"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="searchList" property="outageDuration"/></td>
	        <td class="<%=backgroundclass %>" align="right"><bean:write name="searchList" property="currentStatus"/></td>
	        <td class="<%=backgroundclass %>" align="right"><a href="JobDashboardAction.do?jobid=<bean:write name='searchList' property='jobId'/>&ticket_id=<bean:write name='searchList' property='ticketId'/>&appendix_Id="><bean:write name="searchList" property="ticketReference"/></a></td>
         </tr>
        </logic:iterate>
        <%if(rowCount > 50) {%>     
         <tr>
         	<td nowrap="nowrap" colspan="5" class="labelNoColor" style="padding-top: 10px"><font color="gray">
         	The search results are restrict to 50 matches. If you do not see the desired ticket, narrow the results by adjusting your search criteria.</font></td>
         </tr>
         <%}%>
        </table>
     </td>
  </tr>
  <tr><td><font style="line-height: 4px;">&nbsp;</font></td></tr>
</table>
</td>
</tr>
</table>
</html:form>
</body>
</html>