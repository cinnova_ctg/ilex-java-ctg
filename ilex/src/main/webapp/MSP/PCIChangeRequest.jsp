<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
	<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
	<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
	<script language="JavaScript" src="javascript/popcalendar.js"></script>
	<script language="JavaScript" src="MSP/MspJavaScript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>	
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
</head>

<bean:define id="selectedSiteIdentifierName" type="java.lang.String" name="PCIChangeControlForm" property="selectedSiteIdentifierName"></bean:define>

	
	<script>

	$(document).ready(function() {
		//View Mode
	<% if(request.getAttribute("mode").equals("view")){%>

	var selectedSiteIdentifierName='<%=selectedSiteIdentifierName%>'+'&nbsp;&nbsp;&nbsp;';
	$("#siteIdentifierName").html(selectedSiteIdentifierName);
	//All the Change Request Closed with Final Disposition Status Disapproved or Approved Can't be Edited
		if($("#finalDispositionStatusSelected").val()=='FD_APP' || 
				$("#finalDispositionStatusSelected").val()=='FD_DAPP'){
			enableDisable();
		}
		//Other Can Be Edited
	else{
		enableDisable();
		$("#updateRequest").attr("disabled",false);
		$("#deleteRequest").attr("disabled",false);
		$("#actionMessageDiv").hide();
	}
	
	//Update Mode
	<%}else if(request.getAttribute("mode").equals("Update")){%>
			var selectedSiteIdentifierName='<%=selectedSiteIdentifierName%>'+'  ';
			$("#siteIdentifierName").html(selectedSiteIdentifierName);
		<%if("9002".equals(request.getAttribute("errorMessage"))){%>
			$("#messageDiv").html("All Actions must be Completed");
		<%}else if("0".equals(request.getAttribute("errorMessage"))){%>
			$("#messageDiv").html("Successfully Updated");
			enableDisable();
			$("#updateRequest").attr("disabled",false);
			$("#deleteRequest").attr("disabled",false);
		<%}%>
			document.PCIChangeControlForm.currentMode.value='U';
			//enableUpdate();
			//$("#modeStatus").hide();
			$("#updateRequest").attr("disabled",true);
			$("#deleteRequest").attr("disabled",true);
			//Create Mode
	<%}else{%>
		document.PCIChangeControlForm.currentMode.value='A';
		//To disable the Site Identifier Fields Disable
		$("#siteIdentifier").attr("disabled", true);
		
		//To disable the Action Fields Disable
		$("#actionName").attr("disabled", true);
		$("#status").attr("disabled", true);
		$("#plannedDate").attr("disabled", true);
		$("#actualDate").attr("disabled", true);
		$("#notes").attr("disabled", true);
		$("#addAction").attr("disabled", true);
		$("#saveAction1").attr("disabled", true);
		$("#deleteAction1").attr("disabled", true);
		
		//To disable the Final Disposition Fields Disable	
		$("#finalDispositionName").attr("disabled", true);
		$("#finalDispositionStatusSelected").attr("disabled", true);
		$("#finalDispositionDate").attr("disabled", true);
		$("#finalDispositionComments").attr("disabled", true);
		$("#saveFinalDisposition").attr("disabled", true);
		$("#resetFinalDisposition").attr("disabled", true);
		
		//To disable the Change Request Status Fields Disable
		$("#selectedChangeRequestStatus").attr("disabled", true);
		
		<%}%>
		
		//Dynamic Changing the Site Identifier Based on the Customer Selected
		$('#customer').change(function() {
			 $(function() {
	                $("select#customer").change(function() {
	                    $.getJSON("PCIChangeControlAction.do?hmode=populateSiteIdentifier&customer="+document.forms[0].orgTopId.value,{customer: $(this).val(), ajax: 'true'}, function(j){
	                    var options = '';
	                        for (var i = 0; i < j.length; i++) {
	                            options += '<option value="' + j[i].value + '">' + j[i].label + '</option>';
	                        }
	                        $("select#siteIdentifier").attr("disabled", false);
	                        $("select#siteIdentifier").html(options);
	                    });
	                });
	            });
			});
		
		//Code when user clicked to submit button Of Change Request
		$("#submitChangeRequest").click(function(){ 
			//Validations For Successfully Submitting the data 
			if($('#customer').val()==0){
				alert("Please Select the Customer");
				$('#customer').focus();
				return false;
				}
			if($('#siteIdentifier').val()==0){
				alert("Please Select one Site Identifier");
				$('#siteIdentifier').focus();
				return false;
			}
			if($.trim($('#descriptionOfChange').val())==''){
				alert("Description of Change Cannot be Blank");
				$('#descriptionOfChange').focus();
				return false;
			}
			if($.trim($('#reasonForChanges').val())==''){
				alert("Reason For Changes Cannot be Blank");
				$('#reasonForChanges').focus();
				return false;
			}
			
			//As the User Can't Select Closed Status for Chnage Request in Update Mode
			if($('#selectedChangeRequestStatus').val()=='CR_CL'){
				$("#messageDiv").hide();
				alert("Status Closed Can't Be Selected");
				$('#selectedChangeRequestStatus').focus();
				return false;
			}
			//Ajax Call to save the New PCI Change Request Created
			var mode=document.PCIChangeControlForm.currentMode.value;
			document.PCIChangeControlForm.selectedSiteIdentifierName.value=$('#siteIdentifier option:selected').text();
			//'A' represents the Add/Created mode
			if(mode=='A'){
				$.ajax({
					async:'false',
					type: 'POST',
					url: 'PCIChangeControlAction.do?hmode=submitUpdateChangeRequest&changeRequestMode=A',
					data: $("#PCIChangeControlForm").serialize(),
					success: function(j){
						var array=	j.split("\",\"");
						var array1=array[0].split("[\"");
						var array2=array[1].split("\"]");
						$("#messageDiv").show();
						$("#messageDiv").html("PCI Change Request ID #<font color='green'> "+array1[1]+" </font>was Saved");
						$("#siteIdentifierName").html(array2[0]);
						enableDisable();
					}
					});	
			} 
			//'U' rerpresents the Updated/Modify Mode
			else if(mode=='U'){
				$.ajax({
					async:'false',
					type: 'POST',
					url: "PCIChangeControlAction.do?hmode=submitUpdateChangeRequest&changeRequestMode=U",
					data: $("#PCIChangeControlForm").serialize(),
					success: function(j){
						var array=	j.split("\",\"");
						var array1=array[0].split("[\"");
						var array2=array[1].split("\"]");
						$("#actionMessageDiv").hide();
						$("#messageDiv").show();
						$("#messageDiv").html("PCI Change Request ID #<font color='green'> "+array1[1]+" </font>was Updated");
						$("#siteIdentifierName").html(array2[0]);
					}
					});	
			}
			});
		
		//For Setting the Update Mode  and enable the Form Element.
		$("#updateRequest").click(function(){
			document.PCIChangeControlForm.currentMode.value='U';
			enableUpdate();
			$("#updateRequest").attr("disabled",true);
			$("#deleteRequest").attr("disabled",true);
		});
		
		//'D'For Setting the Delete Mode.
		$("#deleteRequest").click(function(){
			document.PCIChangeControlForm.currentMode.value='D';
			enableUpdate();// Enable the disabled form
			if(confirm('Please verify request to delete Change Request.')) {
			$.ajax({
				async:'false',
				type: 'POST',
				url: "PCIChangeControlAction.do?hmode=submitUpdateChangeRequest&changeRequestMode=D",
				data: $("#PCIChangeControlForm").serialize(),
				success: function(j){
					var array=	j.split("\",\"");
					var array1=array[0].split("[\"");
					var array2=array[1].split("\"]");
					$("#actionMessageDiv").hide();
					$("#messageDiv").show();
					$("#messageDiv").html("PCI Change Request ID #<font color='green'> "+array1[1]+" </font>was Deleted");
					$("#siteIdentifierName").html(array2[0]);
					enableDisable();
				}
			});	
			}
		});
	});
	
	//Save/Update Actions Method
function saveAction(actionID,obj){
	var crActionObj = new Object();
	var actionName;
	var status ;
	var plannedDate ;
	var actualDate ;
	var notes ;
	//For Hiding the success and failure message div	
	$('#messageDiv').hide();
	
	//Ajax Call To Save the Actions
	
	//get the perticular rows data on witch user click.
	var row=$(obj).parent().parent();
	$(row).each(function() {    
		actionName = $(this).find("#actionName").val(); 
		status = $(this).find("select#status").val();		
		plannedDate = $(this).find("#plannedDate").val();
		actualDate = $(this).find("#actualDate").val();
		notes = $(this).find("#notes").val();
	}); 
	//Validation For Action Save/Update
	if($.trim(actionName)==''){
		alert("Please Enter the Action Name");
		$(row).find("#actionName").focus();
		return false;
	}
	if(actionName.length>50){
		alert("Maximum Limit for Action Name is 50 character");
		$(row).find("#actionName").focus();
		return false;
	}
	if(plannedDate==''){
		alert("Please Select the Action Planned Date");
		$(row).find("#plannedDate").focus();
		return false;
	}
	//Remove validation from Actions-notes filed
	/*if($.trim(notes)==''){
		alert("Please Enter the Action notes");
		$(row).find("#notes").focus();
		return false;
	}*/
	if(notes.length>100){
		alert("Maximum Limit for Action Notes is 100 character");
		$(row).find("#notes").focus();
		return false;
	}

	//Ajax JSON Call TO Save The Actions
	$.getJSON("PCIChangeControlAction.do?hmode=saveUpdateAction&actionName="+actionName+
			"&status="+status+
			"&plannedDate="+plannedDate+
			"&actualDate="+actualDate+
			"&notes="+notes+
			"&actionID="+actionID+
			"&changeRequestID="+document.forms[0].changeRequestID.value,
			{changeRequestID : $(this).val(),ajax : 'true'},function(j) {
				//To show the Please wait... Message
				$("#actionDiv").empty().html('<center><img src="./images/waiting_tree.gif" /></center><br><center>Please Wait...</center></br>');
				// Now get the JSON Data and prepare the view for actions on screen.
				count=j.length;
				var tableData = '';
				tableData+='<table border="0"  class="WASTD0001" cellspacing="0" cellpadding="0" id ="appendWorkFlow">';
				tableData+='<tr ><td class="PCSSH0001" colspan="4"><div>Actions</div></td><td colspan="1" align="right"><div><input name="addWorkFlow"   type="button" class="FMBUT0001" onclick="addNewRow();" value="Add" style="margin-bottom: 20px;"/></div></td></tr>';
				tableData+='<tr><td class="MSSUC0001 WASTD0001" colspan="6" ><div id="actionMessageDiv"></div></td></tr>';
				tableData+='<tr><td height="10px"></td></tr><tr>';
				tableData+='<td  class="PCSTD0004 PCSTD0003" rowspan="2" >Action</td>';
				tableData+='<td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Status</td>';
				tableData+='<td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Planned</td>';
				tableData+='<td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Actual</td>';
				tableData+='    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Notes</td>';
				tableData+='<td align="right"></td>';
				tableData+='</tr>';	
				tableData+='<tr></tr>';
			for ( var i = 0; i < j.length; i++) {
				var selectedValue='';
				if ((i+1) % 2 == 0)
					className = 'PCSTDEVEN0001 PCSTDODD0001 PCSTD0003';
				else
					className = 'PCSTDODD0001 PCSTDODD0001 PCSTD0003';
				
				selectedValue=j[i].status;
				//alert(selectedValue);
				tableData += '<tr>';
				tableData += '<td  class="'+className+'"  ><input type="text" id="actionName" class="text" value="'+ j[i].actionName + '"/></td>';
				tableData +='<td class="'+className+'">';
				if(selectedValue=='CA_I')
					{
					tableData +='<select class="Ncomboe" id="status" >';
					tableData +='<option selected value="CA_I" >Inwork</option>';
					tableData +='<option value="CA_O">Complete</option>';
					tableData +='<option value="CA_H">Hold</option>';
					tableData +='<option value="CA_C">Cancelled</option>';
					tableData +='<option value="CA_NR">Not Required</option>';
					}
				else if(selectedValue=='CA_O')
					{
					tableData +='<select class="Ncomboe" id="status" >';
					tableData +='<option value="CA_I" >Inwork</option>';
					tableData +='<option selected value="CA_O">Complete</option>';
					tableData +='<option value="CA_H">Hold</option>';
					tableData +='<option value="CA_C">Cancelled</option>';
					tableData +='<option value="CA_NR">Not Required</option>';
					}
				else if(selectedValue=='CA_H'){
					tableData +='<select class="Ncomboe" id="status" >';
					tableData +='<option value="CA_I" >Inwork</option>';
					tableData +='<option value="CA_O">Complete</option>';
					tableData +='<option selected value="CA_H">Hold</option>';
					tableData +='<option value="CA_C">Cancelled</option>';
					tableData +='<option value="CA_NR">Not Required</option>';
				}
				else if(selectedValue=='CA_C')
				{
					tableData +='<select class="Ncomboe" id="status" >';
					tableData +='<option value="CA_I" >Inwork</option>';
					tableData +='<option value="CA_O">Complete</option>';
					tableData +='<option value="CA_H">Hold</option>';
					tableData +='<option selected value="CA_C">Cancelled</option>';
					tableData +='<option value="CA_NR">Not Required</option>';
				}
			else if(selectedValue=='CA_NR'){
				tableData +='<select class="Ncomboe" id="status" >';
				tableData +='<option value="CA_I" >Inwork</option>';
				tableData +='<option value="CA_O">Complete</option>';
				tableData +='<option value="CA_H">Hold</option>';
				tableData +='<option value="CA_C">Cancelled</option>';
				tableData +='<option selected value="CA_NR">Not Required</option>';
			}
				tableData +='</select></td>'; 
				
				tableData +='<td class="'+className+'"><input type="text" id="plannedDate" class="PCTXT0001" name="plannedDate" size = "10"  onclick = "check1(this);" value="'+ j[i].plannedDate + '" /></td>'
				tableData +='<td class="'+className+'"><input type="text" id="actualDate" class="PCTXT0001" name="actualDate" size = "10" onclick = "check1(this);" value="'+ j[i].actualDate +'"   /></td>'
				tableData +='<td class="'+className+'"><input type="text" id="notes" class="text" name="notes" value="'+ j[i].notes + '" /></td>'
				tableData +='<td ><input type="button" class="FMBUT0001 WASTD0002" value="Save" style="margin-top: 0px;" onclick=saveAction("'+j[i].actionID+'",this)></input></td>';
				tableData +='<td ><input type="button" class="FMBUT0001 WASTD0002" value="Delete" style="margin-top: 0px;" onclick=deleteRow("'+j[i].actionID+'",this);></input></td>'
				tableData += '</tr>';
			}
			tableData+='</table>';
			$('#messageDiv').hide();
			$("#actionDiv").show();
			$("div#actionDiv").html(tableData);
			$('#actionMessageDiv').html('<font align="left">Action Performed</font>');
		
	});
	//JSON Call Ends Here
}
	
//Delete Actions Method
function deleteRow(actionID,obj){
	$('#messageDiv').hide();
	if(actionID=='0'){
		$(obj).parent().parent().remove();
		--count;
		return true;
	}
	//get the perticular rows data on witch user click.
	var row=$(obj).parent().parent();
	var actionName;
	var status ;
	var plannedDate ;
	var actualDate ;
	var notes ;

	$(row).each(function() {    
		actionName = $(this).find("#actionName").val(); 
		 status = $(this).find("select#status").val();
		 plannedDate = $(this).find("#plannedDate").val();
		 actualDate = $(this).find("#actualDate").val();
		 notes = $(this).find("#notes").val();
	});
	//confirmation before removing the data.
	if(confirm('Please verify request to delete Action.')) {
		$.ajax({
			async:'false',
			type: 'POST',
			url: "PCIChangeControlAction.do?hmode=deleteAction&actionName="+actionName+"&status="+status+"&plannedDate="+plannedDate+"&actualDate="+actualDate+"&notes="+notes+"&actionID="+actionID+"&changeRequestID="+document.forms[0].changeRequestID.value,
			data: $("#PCIChangeControlForm").serialize(),
			success: function(j){
				$(obj).parent().parent().remove();
				--count;
			},
			error: function(xhr, textStatus, errorThrown){
				if(xhr.status==500){
					$('#actionMessageDiv').show();
					$('#actionMessageDiv').html("Only Action associated to Change Request Can't Be Deleted");
				}
				
			}  
		});
	}
}
</script>
<script type="text/javascript">
var count=1;
function saveFinalDis(){
	$('#messageDiv').hide();
	//Validations For Saving the Final Disposition Form
	if($('#finalDispositionName').val()==0){
	alert("Please Select the Final Disposition Name");
	$('#finalDispositionName').focus();
	return false;
	}
	
	if($('#finalDispositionStatusSelected').val()=='FD_DE'){
	alert("Please Select the Final Disposition Status");
	$('#finalDispositionStatusSelected').focus();
	return false;
	}
	
	if($('#finalDispositionDate').val()==0){
	alert("Please Select the Final Disposition Date");
	$('#finalDispositionDate').focus();
	return false;
	}
	//Validation Removed for final disposition Comment
	/*if($('#finalDispositionComments').val()==0){
	alert("Please Select the Final Disposition Comments");
	$('#finalDispositionComments').focus();
	return false;
	}*/
	
	var url = "PCIChangeControlAction.do?hmode=submitFinalDisposition";
	document.forms[0].action = url;
	document.forms[0].submit();

}
// To set the limit of text in the field.
function limitText(limitField, limitNum, fieldName) {
	if (limitField.value.length > limitNum) {
		alert("Maximum Limit for "+fieldName+" :"+limitNum+" is achived");
		limitField.value = limitField.value.substring(0, limitNum);
	} 
}
// To add a new row for the actions.
function addNewRow(){
	$('#messageDiv').hide();
	$('#actionMessageDiv').empty();
	var newRow;
		++count;
		if(count%2!=0){
		newRow = 
				'<tr>'+
				'<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  ><input type="text" id="actionName" class="text"/></td>'+
				'<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  >'+
					'<select class="Ncomboe" id="status">'+
					'<option value="CA_I">Inwork</option>'+
					'<option value="CA_O" >Complete</option>'+
					'<option value="CA_H">Hold</option>'+
					'<option value="CA_NR">Not Required</option>'+
					'<option value="CA_C">Cancelled</option>'+
					'</select>'+
				'</td>'+
			    '<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" ><input type="text" id="plannedDate" class="PCTXT0001" size="10" onclick = "check1(this)"/></td>'+
			    '<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001"  ><input type="text" id="actualDate" class="PCTXT0001" size="10" onclick = "check1(this)"/></td>'+
			    '<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001"  ><input type="text" id="notes" class="text"/></td>'+
				'<td ><input type="button" class="FMBUT0001 WASTD0002" value="Save" style="margin-top: 0px;" onclick=saveAction("notSaved",this)></input></td>'+
				'<td ><input type="button" class="FMBUT0001 WASTD0002" value="Delete" style="margin-top: 0px;" onclick="deleteRow(0,this);"></input></td>'+
				'</tr>'
		}
		else{
			newRow = 
				'<tr>'+
				'<td  class="PCSTDEVEN0001 PCSTD0003 TBTXT0001"  ><input type="text" id="actionName" class="text"/></td>'+
				'<td  class="PCSTDEVEN0001 PCSTD0003 TBTXT0001"  >'+
					'<select class="Ncomboe" id="status">'+
					'<option value="CA_I">Inwork</option>'+
					'<option value="CA_O" >Complete</option>'+
					'<option value="CA_H">Hold</option>'+
					'<option value="CA_NR">Not Required</option>'+
					'<option value="CA_C">Cancelled</option>'+
					'</select>'+
				'</td>'+
			    '<td  class="PCSTDEVEN0001 PCSTD0003 TBDAT0001" ><input type="text" id="plannedDate" size="10" class="PCTXT0001" onclick = "check1(this)"/></td>'+
			    '<td  class="PCSTDEVEN0001 PCSTD0003 TBDAT0001"  ><input type="text" id="actualDate" size="10"  class="PCTXT0001" onclick = "check1(this)"/></td>'+
			    '<td  class="PCSTDEVEN0001 PCSTD0003 TBTXT0001"  ><input type="text" id="notes" class="text"/></td>'+
				'<td ><input type="button" class="FMBUT0001 WASTD0002" value="Save" style="margin-top: 0px;" onclick=saveAction("notSaved",this)></input></td>'+
				'<td ><input type="button" class="FMBUT0001 WASTD0002" value="Delete" style="margin-top: 0px;" onclick="deleteRow(0,this);"></input></td>'
				'</tr>'
			
		}
	$("#appendWorkFlow tr:last").after(newRow)
}
function check1(obj){
	return popUpCalendar(obj,obj,'mm/dd/yyyy');
}
function enableDisable(){
	var f = document.forms[0];
	var len=f.elements.length;
	for(x=0;x<len;x++) {
		f.elements[x].disabled=true;
	}
}
function enableUpdate(){
	var f = document.forms[0];
	var len=f.elements.length;
	for(x=0;x<len;x++) {
		f.elements[x].disabled=false;
	}
}	

</script>	
<body >
<html:form  action="PCIChangeControlAction.do" styleId="PCIChangeControlForm">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<!-- Menu -->
				<jsp:include page="/MSP/MspTopMenu.jsp"/>
			</td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="BCNAV0001">
				 	<a href="MspMonitor.do" class="BCNAV0002">MSP</a>
				 	<a >PCI Change Control</a>
				 	<a href="PCIChangeControlAction.do">Change Request</a>
				 	<a><span class="BCPAG0001">Change Request Details</span></a>
				</div>
		    </td>
		</tr>
	</table>
	<%@include file="/MSP/MspTopMenuScript.inc" %>
	<div id="messageDiv" class="MSSUC0001 WASTD0001"></div>
<div id="changeRequestDiv" style="margin-top: 0px;">
<table border="0" class="WASTD0001" >
<tr>
	<td colspan="1"><div class="PCSSH0001">Change Request Details</div></td>
	<td ></td>
	<% if(request.getAttribute("mode").equals("view") || request.getAttribute("mode").equals("Update")){%>
		&nbsp;&nbsp;&nbsp;<td><div id="modeStatus"><html:button property="type" styleClass="FMBUT0001" style="margin-top:0px; " value="Update Change Request" styleId="updateRequest"></html:button>
     &nbsp;&nbsp; <html:button  property="type"   styleClass="FMBUT0001" value="Delete Change Request"  style="margin-top:0px; " styleId="deleteRequest"></html:button>    
	</div></td> 
      <%}%>
</tr>
</table>
<table border="0" class="WASTD0001" id="PCI">
<html:hidden property="requestorName"/>
<html:hidden property="currentMode"/>
<html:hidden property="changeRequestID"/>
<html:hidden property="selectedSiteIdentifierName"/>

<tr>
	<td class = "PCLAB0001">Status:</td>
	<td>
	<bean:define id="selectedChangeRequestStatus" name="PCIChangeControlForm" property="selectedChangeRequestStatus" type="java.lang.String" ></bean:define>
		<html:select styleId="selectedChangeRequestStatus" property = "selectedChangeRequestStatus" value="<%=selectedChangeRequestStatus %>" size = "1" styleClass = "Ncomboe">
		   		<html:optionsCollection name = "PCIChangeControlForm" property = "selectedChangeRequestStatusList" value = "value" label = "label"/> 
		</html:select>
		
	</td>
	<td><a  class="PCLAB0001"><bean:write name="PCIChangeControlForm"  property="requestorName"/></a></td>
</tr>
<tr>
<td class = "PCLAB0001">Customer:</td>

	<td>
	<bean:define id="orgTopId" name="PCIChangeControlForm" property="orgTopId" type="java.lang.String" ></bean:define>
		<html:select property = "orgTopId" styleId="customer"  size = "1" value="<%=orgTopId %>" styleClass = "Ncomboe">
		   		<html:optionsCollection name = "PCIChangeControlForm" property = "customerList" value = "value" label = "label"/> 
		</html:select>
	</td>
	<td></td>
</tr>

<tr>
	<td class = "PCLAB0001">Site Identifier:</td>
	<% if(request.getAttribute("mode").equals("view") || request.getAttribute("mode").equals("Update")){%>
	<td><div id="siteIdentifierName" class="PCLAB0001" style=" border-style: none; vertical-align: center; " ></div></td>
	<%}%>
	<td colspan="2">
		<html:select styleId="siteIdentifier" style="vertical-align: top; " property = "selectedSiteID"  size = "1" styleClass = "Ncomboe">
		   		<html:optionsCollection name = "PCIChangeControlForm" property = "siteIdentifierList" value = "value" label = "label"/> 
		</html:select>
	</td>
</tr>

<tr>
	<td class = "PCLAB0001">Description of Change:</td>
	<td colspan="2">
	<html:textarea styleId="descriptionOfChange" property="descriptionOfChange" rows="4"  cols="51" onkeydown="limitText(this.form.descriptionOfChange,2000,'Description of Change');"  onkeyup="limitText(this.form.descriptionOfChange,2000,'Description of Change');"></html:textarea>
	</td>
</tr>

<tr>
	<td class = "PCLAB0001">Reason for Changes:</td>
	<td colspan="2">
	<html:textarea styleId="reasonForChanges" property="reasonForChanges" rows="4" cols="51" onkeydown="limitText(this.form.reasonForChanges,2000,'Reason for Changes');"  onkeyup="limitText(this.form.reasonForChanges,2000,'Reason for Changes');"></html:textarea>
	</td>
</tr>

<tr>
	<td class = "PCLAB0001">Resource Impact:</td>
	<td colspan="2">
	<html:textarea styleId="resourceImpact" property="resourceImpact" rows="4" cols="51" onkeydown="limitText(this.form.resourceImpact,100,'Resource Impact');"  onkeyup="limitText(this.form.resourceImpact,100,'Resource Impact');"></html:textarea>
	</td>
</tr>

<tr>
	<td>
		<html:button property="type" styleId="submitChangeRequest" styleClass ="FMBUT0001">Submit</html:button>
		&nbsp;&nbsp;<html:reset property="type"  styleClass ="FMBUT0001">Reset</html:reset>
	</td>
</tr>
</table>
</div>

<div id="actionDiv">

<table border="0"  class="WASTD0001" cellspacing="0" cellpadding="0" id ="appendWorkFlow">
<tr >
<td class="PCSSH0001" colspan="4">
	<div>Actions</div></td>
	<td colspan="1" align="right"><div><input name="addWorkFlow" id="addAction"   type="button" class="FMBUT0001" value="Add" onclick="addNewRow();" style="margin-bottom: 20px;"></input></div></td>
</tr>
<tr><td class="MSSUC0001 WASTD0001" colspan="6"><div id="actionMessageDiv"></div></td></tr>
<tr>
			<td  class="PCSTD0004 PCSTD0003" rowspan="2" >Action</td>
			<td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Status</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Planned</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Actual</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Notes</td>
			<td align="right"></td>
</tr>
<%int i=0;
String bgcolor="";
%>
<logic:notEmpty  name="PCIChangeControlForm" property="changeRequestActionsList">	
<logic:iterate id="changeRequestActionsList" name="PCIChangeControlForm" property="changeRequestActionsList">
<%++i; %>
<%if((i%2)==0){
bgcolor = "PCSTDEVEN0001";
} 
else{
bgcolor="PCSTDODD0001";
}
%>
<tr>
</tr>
<tr>
			<td  class="<%=bgcolor %> PCSTD0003 TBTXT0001"  ><html:text  styleId="actionName" name="changeRequestActionsList" styleClass="text" property="actionName" ></html:text></td>
			<td  class="<%=bgcolor %> PCSTD0003 TBTXT0001"  >
				<bean:define id="status" name="changeRequestActionsList" property="status" type="java.lang.String" ></bean:define>
				<html:select styleId="status" property = "status" value="<%=status %>"  size = "1" styleClass = "Ncomboe">
		   		<html:optionsCollection name = "PCIChangeControlForm" property = "changeRequestActionStatusList" value = "value" label = "label"/> 
				</html:select>
			</td>
		    <td  class="<%=bgcolor %> PCSTD0003 TBDAT0001" ><html:text name="changeRequestActionsList" styleId="plannedDate" styleClass="PCTXT0001" property="plannedDate" size = "10"  onclick = "check1(this)" ></html:text></td>
		    <td  class="<%=bgcolor %> PCSTD0003 TBDAT0001"  ><html:text name="changeRequestActionsList" styleId="actualDate" styleClass="PCTXT0001" property="actualDate" size = "10"  onclick = "check1(this)" ></html:text></td>
		    <td  class="<%=bgcolor %> PCSTD0003 TBTXT0001"  ><html:text name="changeRequestActionsList" styleId="notes" styleClass="text" property="notes" ></html:text></td>
			<bean:define id="actionID" name="changeRequestActionsList" property="actionID" type="java.lang.String" ></bean:define>
			<td ><input type="button" id="saveAction1" class="FMBUT0001 WASTD0002" value="Save" style="margin-top: 0px;" onclick="saveAction(<%=actionID%>,this)"></input></td>
			<td ><input type="button" id="deleteAction1" class="FMBUT0001 WASTD0002" value="Delete" style="margin-top: 0px;" onclick="deleteRow(<%=actionID%>,this);"></input></td>
</tr>
</logic:iterate>
</logic:notEmpty>
<script type="text/javascript">
count='<%=i%>';
</script>
</table>
</div>
<div id="finalDispositionDiv">
<table border="0" class="WASTD0001" cellspacing="0" cellpadding="0">
<tr >
<td class="PCSSH0001" colspan="5">
	<div>Final Disposition</div></td>

</tr>
<tr >
<td height="20px"></td>
</tr>
<tr>
			<td  class="PCSTD0004 PCSTD0003" rowspan="2" colspan="1" >Name/Title</td>
			<td  class="PCSTD0004 PCSTD0003"  rowspan="2" colspan="1" >Disposition</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" colspan="1" >Date</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2"  colspan="2">Comments</td>
</tr>
<tr>
</tr>
<tr>
			<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" colspan="1" ><html:text name="PCIChangeControlForm" styleClass="text" styleId="finalDispositionName"  property="finalDispositionName" onkeydown="limitText(this.form.finalDispositionName,50,'Final Disposition Name/Title');"  onkeyup="limitText(this.form.finalDispositionName,50,'Final Disposition Name/Title');"></html:text></td>
			<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" colspan="1" >
			<bean:define id="finalDispositionStatusSelected" name="PCIChangeControlForm" property="finalDispositionStatusSelected" type="java.lang.String" ></bean:define>
				<html:select styleId="finalDispositionStatusSelected" property = "finalDispositionStatusSelected" value="<%=finalDispositionStatusSelected %>" size = "1" styleClass = "Ncomboe">
		   		<html:optionsCollection name = "PCIChangeControlForm" property = "finalDispositionStatusList" value = "value" label = "label"/> 
				</html:select>
			</td>
		    <td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" colspan="1"><html:text styleId="finalDispositionDate" name="PCIChangeControlForm" styleClass="PCTXT0001" property="finalDispositionDate" size = "10"  onclick = "check1(this)" ></html:text></td>
		    <td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" colspan="2" ><html:text  styleId="finalDispositionComments" name="PCIChangeControlForm" styleClass="text" property="finalDispositionComments" size="35" onkeydown="limitText(this.form.finalDispositionComments,100,'Final Disposition Comments');"  onkeyup="limitText(this.form.finalDispositionComments,100,'Final Disposition Comments');" ></html:text></td>
</tr>
<tr>
			<td ><input type="button" class="FMBUT0001" value="Save" id="saveFinalDisposition" onclick="saveFinalDis()"></input>
							&nbsp;&nbsp;  <input type="reset" id="resetFinalDisposition" class="FMBUT0001" value="Reset" ></input></td>
</tr>
</table>
</div>

</div>
</html:form>
</body>

</html>

