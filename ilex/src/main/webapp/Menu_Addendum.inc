<div id="menunav">
    <ul>
  		<li>
        	<a href="#" class="drop"><span>Job</span></a>
  			<ul>
				<%
				if((String)request.getAttribute( "addendumStatusListNew" ) != "" ) {
					%>
     				<li>
	     				<a href="#"><span><bean:message bundle = "PRM" key = "prm.changestatus"/></span></a>
	     				<ul>
							<%= (String) request.getAttribute( "addendumStatusListNew" ) %>
						</ul>
					</li>
					<%
				} else {
					%>
     				<li><a href="#"><bean:message bundle = "PRM" key = "prm.changestatus"/></a></li>
					<%
				}
				%>
 				<li>
 					<a href="#"><span>Addendum</span></a>
					<ul>
						<%
						if(!job_Status.equals("Draft") && !job_Status.equals("Review") && !job_Status.equals("Cancelled")) {
							%>
     						<li><a href="javascript:addendumSendEmail();">Send</a></li>
							<%
						} else {
							%>
     						<li><a href="javascript:alert('<bean:message bundle = "PRM" key = "prm.addendum.sendemail.noprivilegestatus"/>');">Send</a></li>
							<%
						}
						%>
     					<li>
     						<a href="#"><span>View</span></a>
     						<ul>
								<%
								if(!job_Status.equals("Draft") && !job_Status.equals("Review") && !job_Status.equals("Cancelled")) {
									%>
									<li><a href="javascript:addendumViewContractDocument(<%=Job_Id%>,'pdf');">PDF</a></li>
									<%
								} else {
									if(jobViewtype.equals("All")) {
										%>
										<li><a href="javascript:addendumViewContractDocument(<%=Job_Id%>, 'pdf');">PDF</a></li>
										<li><a href="javascript:addendumViewContractDocument(<%=Job_Id%>, 'rtf');">RTF</a></li>
										<li><a href="javascript:addendumViewContractDocument(<%=Job_Id%>, 'html');">HTML</a></li>
										<%
									} else {
										%>
										<li><a href="javascript:addendumViewContractDocument(<%=Job_Id%>, 'pdf');">PDF</a></li>
										<li><a href="javascript:addendumViewContractDocument(<%=Job_Id%>, 'excel');">EXCEL</a></li>
										<%
									}
								}
								%>	
     						</ul>
     					</li>
     					<li><a href="ManageUpliftAction.do?ref=addendumUplift&Job_Id=<%=Job_Id%>&Appendix_Id=<%=Appendix_Id%>">Manage Uplifts</a></li>
					</ul>
				</li>
 				<li><a href="javascript:alert('<bean:message bundle = "PRM" key = "prm.CannotupdateJobschedulefordefault"/>')"><bean:message bundle = "PRM" key = "prm.updateschedule"/></a></li>
				<li>
 					<a href="#"><span>Documents</span></a>
					<ul>
	 					<li><a href="EntityHistory.do?entityId=<%=Job_Id%>&entityType=Addendum&function=viewHistory&appendixid=<%=Appendix_Id%>">Addendum History</a></li>
	 					<li>
		 					<a href="#"><span>Support Documents</span></a>
		 					<ul>
			 					<li><a href="EntityHistory.do?entityId=<%=Job_Id%>&jobId=<%=Job_Id%>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Addendum">Upload</a>
			 					<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&jobId=<%=Job_Id%>&linkLibName=Addendum&function=supplementHistory">History</a>
							</ul>
						</li>
					</ul>
				</li>
				<%
				if(job_Status.equals("Pending Customer Review")) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%=Job_Id%>&entityType=Addendum&appendixid=<%=Appendix_Id%>">Upload File</a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "PRM" key = "addendum.detail.upload.state"/>')">Upload File</a></li>
					<%
				}
				%>
  			</ul>
  		</li>
  		<li>
        	<a href="#" class="drop"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:addendumManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:addendumSendStatusReport()">Send</a></li>
      			<li><a href="javascript:addendumManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>
	