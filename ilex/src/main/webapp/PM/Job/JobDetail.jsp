<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying  job detail.
*
-->
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<bean:define id = "job_type" name = "JobDetailForm" property = "job_type" /> 
<bean:define id = "appendixtype" name = "JobDetailForm" property = "appendixtype" />
<bean:define id = "chkadd" name = "JobDetailForm" property = "chkaddendum" />
<%
	String Appendix_Id ="";
	String Job_Id = "";
	String chkaddendum ="";
	String from = "PM";
	int esaCount = 0;
	
	if( request.getAttribute( "Appendix_Id" ) != null )
		Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" );
	
	if( request.getAttribute( "Job_Id" ) != null )
	{
		Job_Id = ( String ) request.getAttribute( "Job_Id" );
	}
	
	if(request.getAttribute("esaCount") != null) {
		esaCount = Integer.parseInt(request.getAttribute("esaCount").toString());
	}
%>

<html:html>
<head>
	<title><bean:message bundle = "pm" key = "job.detail.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/content.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<script>


var str = '';

function addcomment()
{
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<bean:write name = "JobDetailForm" property = "addendum_id" />&ref=<bean:write name = "JobDetailForm" property = "chkaddendum" />&Type=Job&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>';
	document.forms[0].submit();
	return true;	 
	 
}

function viewcomments()
{
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<bean:write name = "JobDetailForm" property = "addendum_id" />&ref=<bean:write name = "JobDetailForm" property = "chkaddendum" />&Type=Job&Id=<%= Job_Id %>';
	document.forms[0].submit();
	return true;	 
}

function del() 
{
	
		if( document.forms[0].job_type.value == 'Default' || document.forms[0].status.value == 'Approved' )
		{
			if( document.forms[0].job_type.value == 'Default' )
			{
				alert( "<bean:message bundle = "pm" key = "job.detail.defaultjob"/>" );
				return false;
			}
			
			if( document.forms[0].status.value == 'Approved' )
			{
				alert( "<bean:message bundle = "pm" key = "job.detail.approvedjob"/>" );
				return false;
			}
		}
		else
		{
			var convel = confirm( "<bean:message bundle = "pm" key = "job.detail.delete.confirm"/>" );
			if( convel )
			{
				document.forms[0].action ="DeleteAction.do?addendum_id=<bean:write name = "JobDetailForm" property = "addendum_id" />&ref=<bean:write name = "JobDetailForm" property = "chkaddendum" />&Type=Job&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id%>";
				document.forms[0].submit();
				return true;	
			}
		}
}

function upload()
{	
	if( document.forms[0].job_type.value == 'Default'){
		document.forms[0].action = "EntityHistory.do?entityId=<%= Job_Id %>&jobId=<%= Job_Id %>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Job";		
	}else{
		document.forms[0].action = "EntityHistory.do?entityId=<%= Job_Id %>&jobId=<%= Job_Id %>&entityType=Deliverables&function=addSupplement&linkLibName=Job";
	}
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	if( document.forms[0].job_type.value == 'Default'){
		document.forms[0].action = "EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%=Appendix_Id%>&linkLibName=Job&function=supplementHistory&jobId=<%= Job_Id %>";		
	}else{
		document.forms[0].action = "EntityHistory.do?entityType=Deliverables&appendixId=<%=Appendix_Id%>&linkLibName=Job&function=supplementHistory&jobId=<%= Job_Id %>";
	}
	document.forms[0].submit();
	return true;
}


</script>

<%@ include file = "/NMenu.inc" %> 

<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');leftAdjLayers();">
<html:form action = "/JobDetailAction">
<html:hidden property = "job_type" />
<html:hidden property = "status" />
<html:hidden property = "addendum_id" />

<div id="menunav">
    <ul>
        <logic:equal name ="JobDetailForm" property ="status" value ="Approved">
            <li><a href="javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" class="drop"><bean:message bundle = "pm" key = "job.detail.edit"/></a></li>
        </logic:equal>
        <logic:notEqual name ="JobDetailForm" property ="status" value ="Approved">
            <%
            if (job_type.equals("Default") || job_type.equals("Addendum") || appendixtype.equals("NetMedX")) {
                %>
                <li><a href="javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" class="drop"><bean:message bundle = "pm" key = "job.detail.edit"/></a></li>
                <%
            } else {
                %>
                <li><a href="JobEditAction.do?Job_Id=<bean:write name="JobDetailForm" property ="job_Id"/>&appendix_Id=<bean:write name = "JobDetailForm" property = "appendix_Id"/>&ref=<bean:write name = "JobDetailForm" property = "chkaddendum" />" class="drop"><bean:message bundle = "pm" key = "job.detail.edit"/></a></li>
                <%
            }
            %>
        </logic:notEqual>
        <li>
            <a href="#" class="drop"><span>Manage</span></a>
            <ul>
                <%
                if ((String)request.getAttribute("menu_list") != "") {
                    %>
                    <li><a href="#"><bean:message bundle = "pm" key = "job.detail.menu.changestatus.new"/></a></li>
                    <ul>
                        <%= (String) request.getAttribute("menu_list") %>
                    </ul>
                    <%
                } else {
                    %>
                    <li><a href="#"><bean:message bundle = "pm" key = "job.detail.menu.changestatus.new"/></a></li>
                    <%
                }
                %>
                <li><a href="JobDetailAction.do?addendum_id =<bean:write name = 'JobDetailForm' property = 'addendum_id' />&ref=<bean:write name = "JobDetailForm" property = "chkaddendum" />&Job_Id=<%= Job_Id%>"><bean:message bundle = "pm" key = "job.detail.menu.viewdetails"/></a></li>
                <logic:notEqual name = "JobDetailForm" property = "job_type" value = "Default">
                    <li><a href="SiteAction.do?addendum_id=<bean:write name = "JobDetailForm" property = "addendum_id" />&ref=Add&ref1=<bean:write name = "JobDetailForm" property = "chkaddendum" />&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id%>&fromProposalManag=yes"><bean:message bundle = "pm" key = "job.detail.menu.managesiteinfo"/></a></li>
                </logic:notEqual>
                <logic:equal name = "JobDetailForm" property = "job_type" value = "Default">
                    <li><a href="javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.managenosite"/>');"><bean:message bundle = "pm" key = "job.detail.menu.managesiteinfo"/></a></li>
                </logic:equal>
                <li>
                    <a href="dddddddddd"><bean:message bundle = "pm" key = "job.detail.menu.comments"/></a>
                    <ul>
                        <li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "job.detail.menu.viewall"/></a></li>
                        <li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "job.detail.menu.addcomment"/></a></li>
                    </ul>
                </li>
                <li>
                    <a href="dddddddddd"><bean:message bundle = "pm" key = "job.detail.menu.documents"/></a>
                    <ul>
                        <li><a href="javascript:viewdocuments();"><bean:message bundle = "pm" key = "job.detail.menu.view"/></a></li>
                        <li><a href="javascript:upload();"><bean:message bundle = "pm" key = "job.detail.menu.upload"/></a></li>
                    </ul>
                </li>
                <%
                if(job_type.equals("Default")) {
                    %>
                    <li>
                        <a href="dddddddddd"><bean:message bundle = "AM" key = "am.summary.Partnerinfo"/></a>
                        <ul>
                            <li><a href="PartnerSOWAssumptionList.do?&fromId=<%= Job_Id %>&viewType=sow&fromType=pm_act"><bean:message bundle = "AM" key="am.Partnerinfo.sow"/></a></li>
                            <li><a href="PartnerSOWAssumptionList.do?&fromId=<%= Job_Id %>&viewType=assumption&fromType=pm_act"><bean:message bundle = "AM" key="am.Partnerinfo.assumptions"/></a></li>
                        </ul>
                    </li>
                    <%
                }
                %>
                <li><a href="javascript:del();"><bean:message bundle = "pm" key = "job.detail.menu.deletejob"/></a></li>
            </ul>
        </li>
        <li>
            <a href="#" class="drop"><span><bean:message bundle = "pm" key = "job.detail.activities"/></span></a>
            <ul>
                <li><a href="ActivityUpdateAction.do?addendum_id=<bean:write name = 'JobDetailForm' property = 'addendum_id' />&ref=<bean:write name = 'JobDetailForm' property = 'chkaddendumactivity' />&Job_Id=<%= Job_Id %>"><bean:message bundle = "pm" key="job.detail.manage"/></a></li>
                <%
                if (job_type.equals("Addendum") || job_type.equals("Newjob") || job_type.equals("inscopejob")) { 
                    if (job_type.equals("Addendum") || job_type.equals("Newjob")) {
                        if (job_type.equals("Addendum")) {
                            if (chkadd.equals("detailjob")) {
                                from = "PM";
                            } else {
                                from = "PRM";
                            }
                        } else {
                            from = "PM";
                        }
                        %>
                        <li><a href="CopyAddendumActivityAction.do?from=<%= from %>&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>"><bean:message bundle = "pm" key = "activity.detail.copyactivities"/></a></li>
			<%
                    } else{
                        %> 
                        <li><a href="CopyAddendumActivityAction.do?from=PRM&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>"><bean:message bundle = "pm" key = "activity.detail.copyactivities"/></a></li>
			<%
                    }
                } else {
                    %>
                    <li><a href="CopyActivityAction.do?ref=view&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>"><bean:message bundle = "pm" key = "activity.detail.copyactivities"/></a></li>
                    <%
                }
                
                if (esaCount <= 0) {
                    %>
                    <li><a href="javascript:alert('No ESA is defined for this appendix.');">Commissions</a></li>
                    <%
                } else {
                    %>
                    <li><a href="ESAActivityCommissionAction.do?ref=unspecified&jobRef=detailjob&appendixId=<%= Appendix_Id %>&jobId=<%= Job_Id %>">Commissions</a></li>
                    <%
                }
                %>
            </ul>
        </li>
    </ul>
</div>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
                <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        
			        <tr><td background="images/content_head_04.jpg" height="21" width ="100%" colspan="23">
					         <div id="breadCrumb">
						          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
						    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "JobDetailForm" property = "msa_id"/>&Appendix_Id=<bean:write name = "JobDetailForm" property = "appendix_Id"/>"><bean:write name = "JobDetailForm" property = "msaname"/></a>
						    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "JobDetailForm" property = "appendix_Id"/>&Name=<bean:write name = "JobDetailForm" property = "appendixname"/>&ref=<bean:write name = "JobDetailForm" property = "chkaddendum" />"><bean:write name = "JobDetailForm" property = "appendixname"/></a>
							</div>
					    </td>
	 				</tr>
	 				<tr>    
						<td colspan = "5" height = "30">
							<h2>
							 	<bean:message bundle = "pm" key = "job.detail.label.new"/>
								<bean:write name = "JobDetailForm" property = "name"/>
							</h2>
						</td>
					</tr> 
	 				
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
<table>
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr><td width="2" height="0"></td>
  			 <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "100%"> 
					
					<logic:present name = "uploadflag" scope = "request">
						<tr> 
							<td colspan = "4" class = "message" height = "30"> 
						    	<logic:equal name = "uploadflag" value = "0">	
									<bean:message bundle = "pm" key = "job.detail.upload.success"/>		
					    		</logic:equal>
		    		
					    		<logic:notEqual name = "uploadflag" value = "0">
									<bean:message bundle = "pm" key = "job.detail.upload.failure"/>
					    		</logic:notEqual>
					    		<tr> 
							</td>
						</tr>
		    		</logic:present>
		    		
		    		<logic:present name = "changestatusflag" scope = "request">
	    				<tr> 
							<td colspan = "4" class = "message" height = "30">
						    	<logic:equal name = "changestatusflag" value = "0">
									<bean:message bundle = "pm" key = "job.detail.changestatus.success"/>	
					    		</logic:equal>
		    		
					    		<logic:equal name = "changestatusflag" value = "-9001">
									<bean:message bundle = "pm" key = "job.detail.changestatus.failure1"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9002">
									<bean:message bundle = "pm" key = "job.detail.changestatus.failure2"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9003">
									<bean:message bundle = "pm" key = "job.detail.changestatus.failure3"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9004">
									<bean:message bundle = "pm" key = "job.detail.changestatus.failure4"/>	
					    		</logic:equal>
					    	</td>
				    	</tr>
    				</logic:present>
					</table>
					
			<table width="450">	
					<tr><td colspan = "2" class = "formCaption" ><bean:message bundle = "pm" key = "job.detail.general"/></td></tr>

					 <tr>
					    <td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.status"/></td>
					    <td class = "colLight"><bean:write name = "JobDetailForm" property = "status"/></td>
  					</tr>

					
					<tr>
						<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.localityupliftfactor"/></td>
  						<td class = "colLight" ><bean:write name = "JobDetailForm" property = "locality_uplift"/></td>
  					</tr>
					
					<tr>
    					<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.union/nonunion"/></td>
					    <td class = "colLight"><bean:write name = "JobDetailForm" property = "union"/></td>
  					</tr>
  					
  					<tr>
    					<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.unionupliftfactor"/></td>
  						<td class = "colLight" ><bean:write name = "JobDetailForm" property = "union_uplift_factor"/></td>
  					</tr>
  					
  					<tr>
    					<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.lastmodified"/></td>
    					<td class = "colLight"><bean:write name = "JobDetailForm" property = "lastjobmodifiedby" />&nbsp;<bean:write name = "JobDetailForm" property = "lastjobmodifieddate" /> </td>
  					</tr>					

					<tr><td colspan = "2" class = "formCaption" ><bean:message bundle = "pm" key = "job.detail.estimatedcost"/></td></tr>
					
					<tr><td  class = "colDark"><bean:message bundle = "pm" key = "job.detail.materialscost"/></td>
					    <td  class = "colLight"><bean:write name = "JobDetailForm" property = "estimated_materialcost"/></td>
					</tr>
					
					<tr><td class = "colDark"><bean:message bundle = "pm" key = "job.detail.cnslaborcost"/></td>
					    <td class = "colLight"><bean:write name = "JobDetailForm" property = "estimated_cnsfieldlaborcost"/></td>
					</tr>
					
					<tr><td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.contractlaborcost"/></td>
						<td class = "colLight"><bean:write name = "JobDetailForm" property = "estimated_contractfieldlaborcost"/></td>
					</tr>
					
					<tr><td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.estimatedfreightcost"/></td>
						<td class = "colLight"><bean:write name = "JobDetailForm" property = "estimated_freightcost"/></td>
					</tr>
					
					<tr><td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.estimatedtravelcost"/></td>
						<td class = "colLight"><bean:write name = "JobDetailForm" property = "estimated_travelcost"/></td>
					</tr>
					
					<tr><td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.totalcost"/></td>
						<td class = "colLight"><bean:write name = "JobDetailForm" property = "estimated_totalcost"/></td>
					</tr>
					
					<tr><td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.extendedprice"/></td>
						<td class = "colLight"><bean:write name = "JobDetailForm" property = "extendedprice"/></td>
					</tr>
					
					<tr><td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.proformamargin"/></td>
    					<td class = "colLight"><bean:write name = "JobDetailForm" property = "proformamargin"/></td>
					</tr>
					
					<logic:notEqual name = "JobDetailForm" property = "job_type" value = "Default">
					
					<tr><td colspan = "2" class = "formCaption"><bean:message bundle = "pm" key = "job.detail.contactinformation"/></td></tr>
					
					<tr><td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.cnsPOC"/></td>
						<td class = "colLight" ><bean:write name = "JobDetailForm" property = "CNS_POC"/></td>
					</tr>
					
					<tr><td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.installationPOC"/></td>
						<td class = "colLight" ><bean:write name = "JobDetailForm" property = "installation_POC"/></td>
					</tr>
					
					<tr><td class = "colDark"><bean:message bundle = "pm" key = "job.detail.sitePOC"/></td>
						<td class = "colLight"><bean:write name = "JobDetailForm" property = "site_POC"/></td>
					</tr>
					
					<tr><td colspan = "2" class = "formCaption" height = "30" ><bean:message bundle = "pm" key = "job.detail.siteinformation"/></td></tr>
					
					<tr>
						<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.sitenumber"/></td>
						<td class = "colLight"><bean:write name = "JobDetailForm" property = "site_no"/></td>
					</tr>
					
					<tr>	
						<td class = "colDark"><bean:message bundle = "pm" key = "job.detail.sitename"/></td>
						<td class = "colLight"><bean:write name = "JobDetailForm" property = "site_name"/></td>
	   				</tr>
	   				
	   				<tr>
						<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.address"/></td>
					    <td class = "colLight"><bean:write name = "JobDetailForm" property = "site_address"/></td>
					</tr>
					
					<tr>    
					    <td class = "colDark"><bean:message bundle = "pm" key = "job.detail.city"/></td>
					    <td class = "colLight"><bean:write name = "JobDetailForm" property = "site_city"/></td>
	  				</tr>
	   				
	   				<tr> 
						<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.state"/></td>
					    <td class = "colLight"><bean:write name = "JobDetailForm" property = "site_state"/></td>
					</tr>
					
					<tr>    
					    <td class = "colDark"><bean:message bundle = "pm" key = "job.detail.zipcode"/></td>
					    <td class = "colLight"><bean:write name = "JobDetailForm" property = "site_zipcode"/></td>
  					</tr>
	   				
	   				<tr><td colspan = "2" class = "formCaption" ><bean:message bundle = "pm" key = "job.detail.scheduleinformation"/></td></tr>
	   				
	   				<tr>
						<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.estimatedstartdate"/></td>
					    <td class = "colLight" ><bean:write name = "JobDetailForm" property = "estimated_start_schedule"/></td>
	   				</tr>
	  					
  					<tr>
						<td class = "colDark" ><bean:message bundle = "pm" key = "job.detail.estimatedenddate"/></td>
					    <td class = "colLight"><bean:write name = "JobDetailForm" property = "estimated_complete_schedule"/></td>
					</tr>

					</logic:notEqual>

					<tr><td colspan = "2" class = "formCaption" height = "30" ><bean:message bundle = "pm" key = "job.detail.comments"/></td></tr>
					
					<logic:empty name ="JobDetailForm" property ="lastchangedate">
					<tr>
						<td class = "colLight" colspan="2"><font class="Nmessage">&nbsp;&nbsp;No Comments.</font></td>	
					</tr>	
					</logic:empty>
					
					<logic:notEmpty name ="JobDetailForm" property ="lastchangedate">
						<tr> 
						    <td class = "colLight"><bean:write name = "JobDetailForm" property = "lastchangedate"/></td>
						    <td class = "colLight"><bean:write name = "JobDetailForm" property = "lastcomment"/></td>
					    </tr>
   					</logic:notEmpty>
				    
			</table>
					<jsp:include page = '/Footer.jsp'>
		      			<jsp:param name = 'colspan' value = '37'/>
		      			<jsp:param name = 'helpid' value = 'jobdetailpage'/>
		 			</jsp:include>	
			</td>
		</tr>
	</table>
</html:form>
</table>
</td></tr>
</table>
</body>
<script>
function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;
}
</script>
</html:html>



