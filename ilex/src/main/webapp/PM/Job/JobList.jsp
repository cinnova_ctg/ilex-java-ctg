<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating and  updating  MSAs.
*
-->
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id = "appendixtype" name = "JobListForm" property = "appendixtype" />
<bean:define id = "codes" name = "codeslist" scope = "request"/>
<% 
int job_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 
%><head>

	<title><bean:message bundle = "pm" key = "msa.tabular.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<script language = "JavaScript" src = "javascript/functions.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>  
	
</head>
<html:html>

<%
	String backgroundclass = null;
	String backgroundhyperlinkclass = null;
	String backgroundclasstoleftalign=null;
	boolean csschooser = true;
	
	int i = 0;
%>

<body class="body" onLoad ="leftAdjLayers();">
<html:form action = "/JobUpdateAction">
<html:hidden property = "msaname" />
<html:hidden property = "MSA_Id" />
<html:hidden property = "appendix_Id" />
<html:hidden property = "ref" /> 
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "chkaddendum" />
 <logic:notEqual name ="JobListForm" property="chkaddendum" value="inscopejob">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr><td class="headerrow" height="19" width = "100%" >&nbsp;</td></tr>
			        <tr><td background="images/content_head_04.jpg" height="21" width = "100%" >
				          	<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle ="pm" key ="common.msa.list"/></a>
				          						 <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "JobListForm" property = "MSA_Id"/>&firstCall=true"><bean:write name = "JobListForm" property = "msaname"/></a>
				          						 
				            </div>
			          	</td>
	 				</tr>
	 				<tr>
			          <td colspan="6"><h2><a class="head" href="AppendixDetailAction.do?MSA_Id=<bean:write name = "JobListForm" property = "MSA_Id"/>&Appendix_Id=<bean:write name = "JobListForm" property = "appendix_Id"/>"><bean:write name ="JobListForm" property = "appendixname"/></a>:&nbsp;<bean:message bundle = "pm" key ="job.tabular.heading"/></h2></td>
			        </tr>
	 				
	 				
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:notEqual>

<logic:equal name ="JobListForm" property="chkaddendum" value="inscopejob">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr><td class="headerrow" height="19">&nbsp;</td></tr>
						<tr><td background="images/content_head_04.jpg" height="21">&nbsp;</td></tr>
						<tr><td><h2>
					 	<a class = "head" href ="AppendixHeader.do?function=view&appendixid=<bean:write name = "JobListForm" property = "appendix_Id"/>&viewjobtype=<bean:write name = "JobListForm" property = "viewjobtype"/>&ownerId=<bean:write name = "JobListForm" property = "ownerId"/>"><bean:write name ="JobListForm" property = "appendixname"/></a>:<bean:message bundle = "pm" key ="job.tabular.heading"/>  
					        
						</h2></td></tr>
						
						
	</table>
</logic:equal>

<table width ="100%" border="0" cellspacing="1" cellpadding="1" >
 <tr>
  <td colspan="3"><table border = "0" cellspacing = "1" cellpadding = "1" >
			<logic:present name = "editJobFlag" scope = "request">	
			   <tr><td>&nbsp;</td>
			  	 <td colspan = "2" class = "message" height = "30">
			     	<logic:equal name = "editJobFlag" value = "0">
			     		<logic:equal name = "editAction" value = "A">
					 		<script>
								parent.ilexleft.location.reload();
							</script>
					 		Job added successfully
					 	</logic:equal>
					
					 	<logic:notEqual name = "editAction" value = "A">
					     	Job updated successfully
					 	</logic:notEqual>
				 	</logic:equal>
				
				 	<logic:notEqual name = "editJobFlag" value = "0">
					 	<logic:equal name = "editAction" value = "A">
					 		Job add failure
					 	</logic:equal>
					
					 	<logic:notEqual name = "editAction" value = "A">
					     	Job update failure
					 	</logic:notEqual>
					</logic:notEqual>
			    </td>
			 </tr>
			</logic:present> 
			
			<logic:present name = "deleteflag" scope = "request">
					    	<tr>
								<td>&nbsp;</td>
		    					<td colspan = "5" class = "message" height = "30">
						    		<logic:equal name = "deleteflag" value = "0">
						    			<bean:message bundle = "pm" key = "job.tabular.delete.success"/>
							    			<script>
												parent.ilexleft.location.reload();
											</script>
						    		</logic:equal>
							    	<logic:equal name = "deleteflag" value = "-9001">
					    				<bean:message bundle = "pm" key = "job.tabular.delete.failure1"/>	
							    	</logic:equal>
							    	
							    	<logic:equal name = "deleteflag" value = "-9002">
					    				<bean:message bundle = "pm" key = "job.tabular.delete.failure2"/>	
							    	</logic:equal>
							    	
							    	<logic:equal name = "deleteflag" value = "-9003">
					    				<bean:message bundle = "pm" key = "job.tabular.delete.failure3"/>	
							    	</logic:equal>
							    	
							    	<logic:equal name = "deleteflag" value = "-9004">
					    				<bean:message bundle = "pm" key = "job.tabular.delete.failure4"/>	
							    	</logic:equal>
							    	
							    	<logic:equal name = "deleteflag" value = "-9005">
					    				<bean:message bundle = "pm" key = "job.tabular.delete.failure5"/>	
							    	</logic:equal>
							    </td>
						    </tr>
			</logic:present>
  				<tr> 
  					<td rowspan = "2">&nbsp; </td>
	    			<td class = "Ntryb" rowspan = "2" colspan ="2">
	    			 <table width="100%"border="0">
					    <tr>
					    	<td class = "Ntryb" align="center"  width="100%"><bean:message bundle = "pm" key = "job.tabular.name"/></td>
					    	<td class = "Ntryb" align="left">
					    		<table width="100%" border="0"><tr><td  align="right"><html:button styleClass = "Nbutton" property ="Add" onclick ="return addJob();">Add</html:button></td></tr></table>
					    	</td>
					    </tr>
					 </table>
	    			</td>
				    <td class = "Ntryb" colspan = "3"><bean:message bundle = "pm" key = "job.tabular.siteinformation"/></td>
				    <td class = "Ntryb" colspan = "6"><bean:message bundle = "pm" key = "job.tabular.estimatedcost"/></td>
				    <td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "job.tabular.extendedprice"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "job.tabular.proformamargin.new"/></td>
					<td class = "Ntryb" colspan = "2"><bean:message bundle = "pm" key = "job.tabular.estimatedschedule"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "job.tabular.status"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "appendix.tabular.customerreference"/></td>
				</tr>
				<tr> 
					<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.sitename"/></div></td>
				    <td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.locality_uplift"/></div></td>
				    <td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.union"/></div></td>
					<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.estimatedmaterialscost"/></div></td>
					<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.estimatedCNSlaborcost"/></div></td>
					<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.estimatedcontractlaborcost"/></div></td>
					<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.estimatedfreightcost"/></div></td>
					<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.estimatedtravelcost"/></div></td>
					<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.estimatedtotalcost"/></div></td>
				    <td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.estimatedschedulestart"/></div></td>
				    <td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "job.tabular.estimatedschedulecomplete"/></div></td>
				  </tr>
  <% if( job_size > 0 ){ %>  			  
				  
 <logic:present name = "codes" property = "joblist"> 
 		<logic:iterate id = "ms" name = "codes" property = "joblist">
 		<bean:define id = "job_type" name = "ms" property = "jobtype" type = "java.lang.String"/>
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass = "Ntexto";
				backgroundclasstoleftalign ="Ntextoleftalignnowrap";
				backgroundhyperlinkclass = "Nhyperodd";
				csschooser = false;
			}
	
			else
			{
				csschooser = true;	
				backgroundclasstoleftalign = "Ntexteleftalignnowrap";
				backgroundhyperlinkclass = "Nhypereven";
				backgroundclass = "Ntexte";
			}
		%>
		<tr>		
			<td class = "Nlabeleboldwhite">&nbsp;</td>
			<td class = "<%= backgroundhyperlinkclass %>"><bean:write name = "ms" property = "name" /></td>
			
			<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX" ) )
				{%>
				<td class ="<%= backgroundhyperlinkclass %>" >[<a href = "JobDetailAction.do?addendum_id=<bean:write name = "JobListForm" property = "addendum_id" />&ref=<bean:write name = "JobListForm" property = "chkaddendum" />&Job_Id=<bean:write name = "ms" property = "job_Id" />">Detail</a>]</td>
					<html:hidden name = "ms" property = "name" />
				<% }
				else  
				{%>
					<logic:equal name = "ms" property = "status" value = "Approved">
							<td class ="<%= backgroundhyperlinkclass %>">[<a href = "JobDetailAction.do?addendum_id=<bean:write name = "JobListForm" property = "addendum_id" />&ref=<bean:write name = "JobListForm" property = "chkaddendum" />&Job_Id=<bean:write name = "ms" property = "job_Id" />">Detail</a>&nbsp;|&nbsp;Edit]</td> 
					</logic:equal>
					<logic:notEqual name = "ms" property = "status" value = "Approved">
						<td class ="<%= backgroundhyperlinkclass %>">[<a href = "JobDetailAction.do?addendum_id=<bean:write name = "JobListForm" property = "addendum_id" />&ref=<bean:write name = "JobListForm" property = "chkaddendum" />&Job_Id=<bean:write name = "ms" property = "job_Id" />">Detail</a>&nbsp;|&nbsp;<a href ="JobEditAction.do?Job_Id=<bean:write name = "ms" property = "job_Id" />&appendix_Id=<bean:write name = "JobListForm" property = "appendix_Id" />&ref=<bean:write name = "JobListForm" property = "chkaddendum" />">Edit</a>]</td> 
					</logic:notEqual>
					
				<% } %>	
		<!-- 	<td class ="<%= backgroundhyperlinkclass %>">[<a href = "JobDetailAction.do?addendum_id=<bean:write name = "JobListForm" property = "addendum_id" />&ref=<bean:write name = "JobListForm" property = "chkaddendum" />&Job_Id=<bean:write name = "ms" property = "job_Id" />">Detail</a>&nbsp;|&nbsp;<a href ="JobEditAction.do?Job_Id=<bean:write name = "ms" property = "job_Id" />&appendix_Id=<bean:write name = "JobListForm" property = "appendix_Id" />">Edit</a>]</td>   -->
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "site_name"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "locality_uplift"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "union"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimated_materialcost"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimated_cnsfieldlaborcost"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimated_contractfieldlaborcost"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimated_freightcost"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimated_travelcost"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimated_totalcost"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "extendedprice"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "proformamargin"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimated_start_schedule"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimated_complete_schedule"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "status"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "custref"/></td>
  		</tr>
	   <% i++;  %>
	  </logic:iterate>
  </logic:present>
  
	<tr><td >&nbsp;</td>
		  			<td colspan = "17" class = "Nbuttonrow">
						<html:button property = "sort" styleClass = "Nbutton" onclick = "return sortwindow();"><bean:message bundle = "pm" key = "job.tabular.sort"/></html:button>
					</td>
				</tr>
<% } else { %>
				<tr>
						<td width ="2">&nbsp;</td>
		    			<td  colspan = "8" class = "message" height = "30">
		    				<bean:message bundle = "pm" key ="job.not.present"/>
		    			</td>
		    	</tr>
		
<% } %>
			</table>
		</td>
	</tr>
<jsp:include page = '/Footer.jsp'>
		      			<jsp:param name = 'colspan' value = '37'/>
		      			<jsp:param name = 'helpid' value = 'jobtabularpage'/>
		 			</jsp:include>	
</table>
</html:form>

<script>

function ShowDiv()
{
if(document.forms[0].chkaddendum.value=="inscopejob"){}
else{


leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}
}
function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
if(document.forms[0].chkaddendum.value=="inscopejob"){}
else{
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
}}
function addJob()
{
document.forms[0].action = "JobEditAction.do?Job_Id=0&appendix_Id=<bean:write name = 'JobListForm' property = 'appendix_Id'/>&ref=<bean:write name = 'JobListForm' property = 'chkaddendum' />";
document.forms[0].submit();
return true;
}

var str = '';
function sortwindow()
{
		str = 'SortAction.do?addendum_id=<bean:write name = "JobListForm" property = "addendum_id"/>&Type=Job&ref=<bean:write name = "JobListForm" property = "chkaddendum"/>&Appendix_Id=<bean:write name = "JobListForm" property = "appendix_Id"/>&viewjobtype=<bean:write name ="JobListForm" property="viewjobtype"/>';	
		p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
		//	p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); 
}
</script>
</body>
</html:html>
  
