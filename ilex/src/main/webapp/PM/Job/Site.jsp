<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<bean:define id="dcStateCM" name="dcStateCM"  scope="request"/>
<bean:define id="dcLonDirec" name="dcLonDirec" scope="request"/>
<bean:define id="dcLatDirec" name="dcLatDirec" scope="request"/>
<bean:define id="dcGroupList" name="dcGroupList" scope="request"/>
<bean:define id="dcEndCustomerList" name="dcEndCustomerList" scope="request"/>
<bean:define id="dcTimeZoneList" name="dcTimeZoneList" scope="request"/>
<bean:define id="dcRegionList" name="dcRegionList" scope="request"/>
<bean:define id="dcCategoryList" name="dcCategoryList" scope="request"/>
<bean:define id="dcSiteStatus" name="dcSiteStatus" scope="request"/>


<%
	String fromType = "no";
	String Appendix_Id = "";
	String ref1 = "";
	if( request.getAttribute( "fromType" ) != null )
		fromType = ( String ) request.getAttribute( "fromType" );
	
	if(request.getAttribute("Appendix_Id")!=null)
		Appendix_Id= request.getAttribute("Appendix_Id").toString();	
	if(request.getParameter("ref1") !=null) {
		ref1=( String ) request.getParameter( "ref1" );
		
	}
	
%>

<html:html>
<head>
	<title><bean:message bundle = "pm" key = "site.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel="stylesheet" href="styles/style.css" type="text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<%@ include  file="/NMenu.inc" %>
</head>
	<%@ include  file="/JobMenu.inc" %>
	<%@ include  file="/AjaxStateCountry.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF" onload = "disableItems();leftAdjLayers();">
<html:form action = "/SiteAction">
<html:hidden property = "id1" />
<html:hidden property = "id2" />
<html:hidden property = "ref1" />
<html:hidden property = "nettype" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "addendum_id" />
<html:hidden property = "refersh" />
<html:hidden property = "submitPage" />
<html:hidden property = "sitelistid" />
<html:hidden property ="site_Id"/>
<html:hidden property ="fromType"/>
<html:hidden property ="msaid"/>
<html:hidden property ="msaname"/>
<html:hidden property ="state"/>
<html:hidden property ="date"/>
<logic:equal name ="SiteForm" property ="fromType" value ="yes">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
    				<tr>
			    	     <%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
						   	<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>  
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
					</tr>
					
					<tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "SiteForm" property = "msaid"/>&Appendix_Id=<bean:write name = "SiteForm" property = "id1"/>"><bean:write name = "SiteForm" property = "msaname"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "SiteForm" property = "id1"/>&Name=<bean:write name = "SiteForm" property = "appendixname"/>&ref=<%=chkaddendum%>"><bean:write name = "SiteForm" property = "appendixname"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
	 					<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;Job&nbsp;<bean:message bundle = "pm" key = "site.labelInformation"/>:&nbsp;<bean:write name="SiteForm" property="jobName" /></h1></td>
	 				</tr>
	      		</table>
   				</td>
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" >
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>

<logic:empty name ="SiteForm" property ="fromType">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr>
				          <td background="images/content_head_04.jpg" height="21" colspan="23">
				          	<div id="breadCrumb">&nbsp;</div>
				          </td>
				</tr>
	</table>
</logic:empty>

<logic:equal name ="SiteForm" property ="fromType" value ="yes">
		<%@ include  file="/JobMenuScript.inc" %>
</logic:equal>                                           
	
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>  <td width="2" height="0"></td>
  			 <td>
  			 	<logic:empty name="SiteForm" property ="fromType">
		  			 	<table border = "0" cellspacing = "2" cellpadding = "2" width = "500"> 
							<tr>    
								<td colspan = "10"><h1>
								<bean:message bundle = "pm" key = "site.labelInformation"/>&nbsp;
								<bean:write name="SiteForm" property="appendixname" />&nbsp;:&nbsp;<bean:write name="SiteForm" property="jobName" /></h1></td>
							</tr> 
						</table>					
				</logic:empty>	
				
				 <table border="0" cellspacing="1" cellpadding="1" >	
				 
					<tr><td colspan = "6" class = "formCaption"><bean:message bundle = "pm" key = "site.general"/></td></tr>
					    					 
					<tr>
					 <td  class = "colDark"><bean:message bundle = "pm" key = "site.labelName"/></td>
					 <td  class = "colLight" colspan="5"><html:text property = "site_name" styleClass = "text" size="15"/>&nbsp;<html:button property = "sitesearch" styleClass = "button_c" onclick = "return opensearchsitewindow();"><bean:message bundle = "pm" key = "site.select"/></html:button></td>
					</tr>
					<tr> 
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.siteNumber"/></td>
					    <td class = "colLight" colspan="5" ><html:text property = "site_number" styleClass = "textbox"/></td>
					</tr>
					<tr>    
     				    <td class = "colDark" ><bean:message bundle = "pm" key = "site.status"/></td>
					    <td class ="colLight" colspan="5" >	
							<html:select property="sitestatus" size="1" styleClass="select">
									<html:optionsCollection name = "dcSiteStatus"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					    </td>
  					</tr>
					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.address"/></td>
					    <td class = "colLight" colspan="5" ><html:text property = "site_address"  styleClass = "text" size="40" maxlength="128"/></td>
					</tr>
					
					<tr>    
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.city"/></td>
					    <td class = "colLight" colspan="5"><html:text property = "site_city" styleClass = "text" size="15"/></td>
					</tr>
					
					<tr>    
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.state"/></td>
					    <td class = "colLight" colspan="5">
					    	<logic:present name="initialStateCategory">
								<html:select property = "stateSelect"  styleClass="select" onchange="ChangeCountry();">
									<logic:iterate id="initialStateCategory" name="initialStateCategory" >
										<bean:define id="statecatname" name="initialStateCategory" property="siteCountryIdName" />		   
										<optgroup class=select label="<%=statecatname%>">
											<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
										</optgroup> 
									</logic:iterate>
								</html:select>
							</logic:present>	
					    </td>
  					</tr>
  					
  					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.zipcode"/></td>
					    <td class = "colLight" colspan="5" ><html:text property = "site_zipcode" styleClass = "text" size="10"/></td>
					</tr>    
					    
					 <tr>   
					    <td class = "colDark""><bean:message bundle = "pm" key = "site.country"/></td>
					    <td class = "colLight" colspan="5" >
							<html:select  property="country" size="1" styleClass="select">
								<html:optionsCollection name = "dcCountryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					    </td>
					 </tr>  
					 
					 <tr>   
					    <td class = "colDark">Category</td>
						<td class = "colLight" colspan="5" >
					    	<html:select  property="sitecategory" size="1" styleClass="select">
								<html:optionsCollection name = "dcCategoryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>   
					    </td> 
					 </tr>
					 
					 <tr>
					    <td class = "colDark">Region</td>
						<td class = "colLight" colspan="5" >
							<html:select  property="siteregion" size="1" styleClass="select">
								<html:optionsCollection name = "dcRegionList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>  
					    </td> 
  					</tr>
  					
  					
  					<tr>
  						<td class = "colDark"><bean:message bundle = "pm" key = "site.latitude"/></td>
					    <td class = "colLight" colspan="5" >
						    <html:text  property = "sitelatdeg" styleClass = "text" size="3" maxlength="4"/>&nbsp;&deg;
						    &nbsp;<html:text   property = "sitelatmin" styleClass = "text" size="2" maxlength="2"/>&nbsp;" 
							      <html:select property="sitelatdirection" size="1" styleClass="select">
									<html:optionsCollection name = "dcLatDirec"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>	
					    </td>
					</tr>
					
					<tr>    
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.longitude"/></td>
					    <td class = "colLight" colspan="5" >
						    	  <html:text   property = "sitelondeg" styleClass = "text" size="3" maxlength="4"/>&nbsp;&deg;
						    &nbsp;<html:text   property = "sitelonmin" styleClass = "text" size="2" maxlength="2"/>&nbsp;" 
							      <html:select property="sitelondirection" size="1" styleClass="select">
									<html:optionsCollection name = "dcLonDirec"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>
					    </td>
					</tr>
					
					
					<tr>  
					   
					    <td class = "colDark">Time Zone</td>
					    <td class = "colLight" colspan="5" >
				      		 <html:select property="sitetimezone" size="1" styleClass="select">
								<html:optionsCollection name = "dcTimeZoneList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>   
					    </td>
					</tr>
  					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.localityuplift"/></td>
					    <td class = "colLight" colspan="5"><html:text property = "locality_uplift" styleClass = "text" size="10"/></td>
					</tr>
					
					<tr>    
						<td class = "colDark"><bean:message bundle = "pm" key = "site.union"/></td>
					    <td class = "colLight" colspan="5" >
					    <html:radio property="union_site" value="Y">Yes</html:radio>&nbsp;&nbsp;&nbsp;
					    <html:radio property="union_site" value="N">No</html:radio>
					    </td>
					</tr>
					
					<tr>    
					  	 <td class = "colDark"><bean:message bundle = "pm" key = "site.group"/></td>
					     <td class = "colLight" colspan="5" >
					     	<html:select  property="sitegroup" size="1" styleClass="select">
					     		<html:optionsCollection name = "dcGroupList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					     </td>
					</tr>     
					     
					 <tr>    
					     <td class = "colDark"><bean:message bundle = "pm" key = "site.end.customer"/></td>
					     <td class = "colLight" colspan="5" >
					    	<html:select  property="siteendcustomer" size="1" styleClass="select"> 
					    		<html:optionsCollection name = "dcEndCustomerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
							</html:select>
					     </td>
  					</tr>
  					
  					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.worklocation"/></td>
					    <td class = "colLight" colspan="5" ><html:text property = "site_worklocation" styleClass = "text" size="85" maxlength="128" /></td>
  					</tr>
  					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.siteDirections"/></td>
					    <td class = "colLight" colspan="5" ><html:text property = "site_direction" styleClass = "text" size="85" /></td>
  					</tr>
  					
  					<tr><td colspan = "6" class = "formCaption"><bean:message bundle = "pm" key = "site.pointOfContact"/>&nbsp;-&nbsp;<bean:message bundle = "pm" key = "site.pointOfContactPrimary"/></td></tr>    
										
					<tr>					
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
					    <td class = "colLight" colspan="5" ><html:text property = "primary_Name" styleClass = "text" size="15" /></td>	    		
					</tr>
					<tr>					
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
						<td class = "colLight" colspan="5" ><html:text property = "site_phone" styleClass = "text" size="20" /></td>	    		
					</tr>
					
					<tr>	
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
					    <td class = "colLight" colspan="5" ><html:text property = "primary_email" styleClass = "text" size="30" /></td>	    		
  					</tr>
  					
				    </tr><td colspan = "6" class = "formCaption"><bean:message bundle = "pm" key = "site.pointOfContact"/>&nbsp;-&nbsp;<bean:message bundle = "pm" key = "site.pointOfContactSecondary"/></td>
						 
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
					 	<td  class = "colLight" colspan="5" ><html:text property = "secondary_Name" styleClass = "text" size="15" /></td>	
					</tr>
					
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
					 	<td class = "colLight" colspan="5" ><html:text property = "secondary_phone" styleClass = "text" size="20" /></td>	    		
					</tr>
					
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
					 	 <td  class = "colLight" colspan="5" ><html:text property = "secondary_email" styleClass = "text" size="30" /></td>	    		
					</tr>
					
					
					
					
					
					
					
					
					
					
					
					</tr><td colspan = "6" class = "formCaption"><bean:message bundle = "pm" key = "site.pointOfContact"/>&nbsp;-&nbsp;<bean:message bundle = "pm" key = "site.clientHD"/></td>
						 
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
					 	<td  class = "colLight" colspan="5" ><html:text property = "clientHD_Name" styleClass = "text" size="15" /></td>	
					</tr>
					
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
					 	<td class = "colLight" colspan="5" ><html:text property = "clientHD_phone" styleClass = "text" size="20" /></td>	    		
					</tr>
					
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
					 	 <td  class = "colLight" colspan="5" ><html:text property = "clientHD_email" styleClass = "text" size="30" /></td>	    		
					</tr>
					
					
					
					
					
					
					
					
					
					
  					<tr>
					    <td class = "colDark"><bean:message bundle = "pm" key = "site.specialConditions"/></td>
					    <td class = "colLight" colspan="5"><html:textarea property = "site_notes" styleClass = "textarea" cols="70" rows="5" readonly = "true"/></td>
						
  					</tr>
  					
  					  					
  					<logic:equal name = "SiteForm" property = "ref" value = "Update">
	  					<tr>
			  				<td colspan = "6" class = "colLight" align ="center">
						  		<html:hidden property = "site_Id" />
						  		<html:submit property = "ref" styleClass = "button_c" onclick = "return validate();"><bean:message bundle = "pm" key = "site.update"/></html:submit>	
						  		
								<%if(fromType.equalsIgnoreCase("yes") || ref1.equals("fromDailyReportPage")){ %>
								 <html:button property="back" styleClass="button_c" onclick = "javascript:BackByHistory();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
								<%}else{ %>								
								 <html:button property="back" styleClass="button_c" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
								 <%} %>
								 <html:submit property="history" styleClass="button_c" onclick = "return GoToSiteHistory();"><bean:message bundle="PRM" key="prm.button.history"/></html:submit>
							</td>
						</tr>
  					</logic:equal>
  					
  					
  					<logic:equal name = "SiteForm" property = "ref" value = "Add">
	  					<tr>
			  				<td colspan = "8" class = "colLight" align ="center">
						  		<html:submit property = "ref" styleClass = "button_c" onclick = "return validate();"><bean:message bundle = "pm" key = "site.add"/></html:submit>
								<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "site.cancel"/></html:reset>
								
								<%if(fromType.equalsIgnoreCase("yes") || ref1.equals("fromDailyReportPage")){ %>
								<html:button property="back" styleClass="button_c" onclick = "javascript:history.go(-1);"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
								<%}else{ %>								
								<html:button property="back" styleClass="button_c" onclick = "return Backaction();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
								<%} %>
								
							</td>
						</tr>
  					</logic:equal>
  					
  					<jsp:include page = '/Footer.jsp'>
		      			<jsp:param name = 'colspan' value = '37'/>
		      			<jsp:param name = 'helpid' value = 'sitepage'/>
		 			</jsp:include>	
					
				</table>
			</td>
		</tr>
</table>
</html:form>


<script>
function trimFields() 
{
	
	var field=document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if( field[i].type == 'text' ) 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}


function BackByHistory()
{
	javascript:history.go(-1);
}

function validate()
{
	//alert('In Validate Method');
	document.forms[0].submitPage.value = 'submitPage';	
	trimFields();
	
	if( document.forms[0].locality_uplift.value != "" )
	{
		if( !isFloat( document.forms[0].locality_uplift.value ) )
		{
			alert( "<bean:message bundle = "pm" key = "site.enterlocalityuplift"/>" );
			document.forms[0].locality_uplift.value = "";
			document.forms[0].locality_uplift.focus();
			return false;
		}
	}
	
	else
	{
		alert( "<bean:message bundle = "pm" key = "site.noenterlocalityuplift"/>" );
		document.forms[0].locality_uplift.focus();
		return false;
	}
	
	if( document.forms[0].union_site.value == '0' )
	{
		alert( "<bean:message bundle = "pm" key = "site.selectunionsite"/>" );
		document.forms[0].union_site.focus();
		return false;
	}
	if(document.forms[0].sitelatmin.value!="")
		{
			var re2digit=/^\d+$/ 
			if (document.forms[0].sitelatmin.value.search(re2digit)==-1) 
			{
			alert("Please Enter Numeric Value for Latitude Minute");
			document.forms[0].sitelatmin.value = "";
			document.forms[0].sitelatmin.focus();
			return false;
			}
		}
		
		
		if(document.forms[0].sitelonmin.value!="")
		{
			var re2digit=/^\d+$/ 
			if (document.forms[0].sitelonmin.value.search(re2digit)==-1) 
			{
			alert("Please Enter Numeric Value for Longitude Minute");
			document.forms[0].sitelonmin.value = "";
			document.forms[0].sitelonmin.focus();
			return false;
			}
		}
	 	if(document.forms[0].sitelatdeg.value != "") {
			var strInt = "^[0-9-]{" + 0 + "," + "}(\\.[0-9]{0," + "})?$";
			var reg = new RegExp(strInt);
			var val=document.forms[0].sitelatdeg.value;
			if (val.match(reg) != null) 
			{}
			else
			{
				alert("Please enter a numeric value in lattitude degree." );
	 			document.forms[0].sitelatdeg.value = "";
	 			document.forms[0].sitelatdeg.focus();
	 			return false;
			}
		}
	 	
	 	if(document.forms[0].sitelondeg.value != "") {
			var strInt = "^[0-9-]{" + 0 + "," + "}(\\.[0-9]{0," + "})?$";
			var reg = new RegExp(strInt);
			var val=document.forms[0].sitelondeg.value;
			if (val.match(reg) != null) 
			{}
			else
			{
				alert("Please enter a numeric value in longitude degree." );
	 			document.forms[0].sitelondeg.value = "";
	 			document.forms[0].sitelondeg.focus();
	 			return false;
			}
		}
	 	
	 	
	
	document.forms[0].site_name.disabled = false;
	document.forms[0].site_number.disabled = false;
	document.forms[0].site_address.disabled = false;
	document.forms[0].site_city.disabled = false;
	document.forms[0].stateSelect.disabled = false;
	document.forms[0].site_zipcode.disabled = false;
	document.forms[0].country.disabled = false;
	document.forms[0].sitecategory.disabled = false;
	document.forms[0].siteregion.disabled = false;
	document.forms[0].sitelatdeg.disabled = false;
	document.forms[0].sitelatmin.disabled = false;
	document.forms[0].sitelatdirection.disabled = false;
	document.forms[0].sitelondeg.disabled = false;
	document.forms[0].sitelonmin.disabled = false;
	document.forms[0].sitelondirection.disabled = false;
	document.forms[0].sitetimezone.disabled = false;
	document.forms[0].sitegroup.disabled = false;
	document.forms[0].siteendcustomer.disabled = false;
	document.forms[0].sitestatus.disabled = false;
	
	
	
	return true;
	
}

function Backaction()
{
	var appendixid=document.forms[0].id1.value;
	
	if( document.forms[0].ref1.value == 'fromjobdashboard' )
	{
		document.forms[0].action = "JobDashboardAction.do?jobid=<bean:write name = "SiteForm" property = "id2" />" ;
	}
	else
	{	
		if(document.forms[0].nettype.value == 'dispatch')
			document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype=dispatch&viewjobtype=<bean:write name = "SiteForm" property = "viewjobtype" />&ownerId=<bean:write name = "SiteForm" property = "ownerId" />";
		else	
			document.forms[0].action = "AppendixHeader.do?appendixid="+appendixid+"&function=view&viewjobtype=<bean:write name = "SiteForm" property = "viewjobtype" />&ownerId=<bean:write name = "SiteForm" property = "ownerId" />";
	}

	document.forms[0].submit();
	return true;
}

// For Site History
function GoToSiteHistory()
{
 var appendixid=document.forms[0].id1.value;

 document.forms[0].action = "SiteHistoryAction.do?siteid=<bean:write name = "SiteForm" property = "site_Id" />&jobid=<bean:write name = "SiteForm" property = "id2" />&page=siteeditpage&from=site&viewjobtype=<bean:write name = "SiteForm" property = "viewjobtype" />&ref1="+document.forms[0].ref1.value+"&Appendix_Id="+appendixid;

 
}


function opensearchsitewindow()
{
	var appendixid=document.forms[0].id1.value;
	/*if( document.forms[0].type.value == 0 )
	{
		alert( 'Please select type' );
		document.forms[0].type.focus();
		return false;
	}*/
	
//	str = "SiteSearch.do?ref=search&jobid=<bean:write name = "SiteForm" property = "id2" />&siteid=<bean:write name = "SiteForm" property = "site_Id" />&reftype=<bean:write name = "SiteForm" property = "ref" />&appendixid="+appendixid;
	str = "SiteSearch.do?ref=search&jobid=<bean:write name = "SiteForm" property = "id2" />&siteid=<bean:write name = "SiteForm" property = "site_Id" />&reftype=<bean:write name = "SiteForm" property = "ref" />&appendixid="+appendixid+"&fromType="+document.forms[0].fromType.value;

	
	suppstrwin = window.open( str , '' , 'left = 200 , top = 200 , width = 870 , height = 400 , resizable = yes , scrollbars = yes' ); 
	suppstrwin.focus();

	return true;
}
function changeCountry()
{
	
	document.forms[0].refersh.value="true";
	document.forms[0].action = "SiteAction.do?ref=Add&Job_Id=<bean:write name = "SiteForm" property = "id2" />&Appendix_Id=<bean:write name="SiteForm" property = "id1"/>";
	document.forms[0].submit();
	return true;
}
function disableItems()
{
	
	document.forms[0].site_name.disabled = true;
	document.forms[0].site_number.disabled = true;
	document.forms[0].site_address.disabled = true;
	document.forms[0].site_city.disabled = true;
	document.forms[0].stateSelect.disabled = true;
	document.forms[0].site_zipcode.disabled = true;
	document.forms[0].country.disabled = true;
	document.forms[0].sitecategory.disabled = true;
	document.forms[0].siteregion.disabled = true;
	document.forms[0].sitelatdeg.disabled = true;
	document.forms[0].sitelatmin.disabled = true;
	document.forms[0].sitelatdirection.disabled = true;
	document.forms[0].sitelondeg.disabled = true;
	document.forms[0].sitelonmin.disabled = true;
	document.forms[0].sitelondirection.disabled = true;
	document.forms[0].sitetimezone.disabled = true;
	document.forms[0].sitegroup.disabled = true;
	document.forms[0].siteendcustomer.disabled = true;
	document.forms[0].sitestatus.disabled = true;
}

</script>
<script>
function ShowDiv()
{
if(document.forms[0].fromType.value =='yes')
{
	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";
}
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
if(document.forms[0].fromType.value =='yes')
{

		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;

}
}
</script>
</body>
</html:html>

