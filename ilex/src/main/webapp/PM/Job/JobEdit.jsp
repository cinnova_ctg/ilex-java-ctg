<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String Appendix_Id ="";

	if(request.getAttribute("Appendix_Id")!=null)
		Appendix_Id= request.getAttribute("Appendix_Id").toString();
%>

<html:html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<link rel = "stylesheet" href="styles/content.css" type="text/css">
<title>Job Edit Page</title>
<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
</style>
<link rel="stylesheet" href="styles/jquery.ui.smoothness.css" type="text/css">
<script type='text/javascript' src='javascript/jquery-1.11.0.min.js'></script>
<script type='text/javascript' src='javascript/jquery-ui.min.js'></script>

<script>
    $(document).ready(function() {
        var api_url = '<%= (String) request.getAttribute("api_url") %>';
        var api_user = '<%= (String) request.getAttribute("api_user") %>';
        var api_pass = '<%= (String) request.getAttribute("api_pass") %>';
        
        var appendix_id = '<c:out value='${JobEditForm.appendixId}'/>';
        
        var emailRecipients = {
            "Requestor": {},
            "Appendix Contact": {}
        };
        
        $.ajax({
            type: "GET",
            url: api_url + "ilex/poc/appendix/" + appendix_id + "/requestor/list",
            dataType: "json",
            headers: {
                "Authorization": "Basic " + btoa(api_user + ":" + api_pass)
            },
            success: function(res) {
                if (res.status === 'success') {
                    var output = '<option value="-1">-- Select --</option>';
                    $.each(res.data, function(i, field){
                        output += '<option value="' + field.id + '~' + field.email + '~' + field.name + '"';
                        
                        if (field.name === requestor) {
                            output+= ' selected';
                        }
                        
                        output += '>' + field.name + '</option>';
                    });
                    $('#requestor').html(output);
                } else {
                }
            },
            failure: function(errMsg) {
            }
        });
        
        $.ajax({
            type: "GET",
            url: api_url + "ilex/appendix/contact/" + appendix_id + "/list",
            dataType: "json",
            headers: {
                "Authorization": "Basic " + btoa(api_user + ":" + api_pass)
            },
            success: function(res) {
                if (res.status === 'success') {
                    var output = '<option value="-1">-- Select --</option>';
                    $.each(res.data, function(i, field){
                        output += '<option value="' + field.id + '~' + field.email + '~' + field.name + '">' + field.name + ': ' + field.email + '</option>';
                    });
                    $('#requestrec').html(output);
                } else {
                }
            },
            failure: function(errMsg) {
            }
        });
        
        $('#requestor').change(function() {
            parseAddEmail($(this).val(), 'Requestor');
        });
        
        $('#requestrec').change(function() {
            parseAddEmail($(this).val(), 'Appendix Contact');
        });
        
        function parseAddEmail(val, type) {
            var vals = val.split("~");
            
            if (vals.length == 3) {
                var id = vals[0];
                var email = vals[1];
                var name = vals[2];
        
                addEmail(email, id, name, type);
            }
        }
        
        function addEmail(email, id, name, type) {
            if (validateEmail(email)) {
                if (typeof emailRecipients[type] !== 'undefined') {
                    if (emailRecipients[type].hasOwnProperty(id) === false) {
                        emailRecipients[type][id] = {
                            id: id,
                            email: email,
                            name: name,
                            type: type
                        };

                        renderEmailRecipients();
                    }
                }
            }
        }
        
        function renderEmailRecipients() {
            var output = '<div style="padding-top: 1px;">None</div>';
            var hasOutput = false;
            
            for (var type in emailRecipients) {
                var csl = '';
                var list = emailRecipients[type];
                for (var id in list) {
                    if (!hasOutput) {
                        output = '';
                        hasOutput = true;
                    }
                    
                    var recipient = list[id];
                    if (csl !== '') {
                        csl += ',';
                    }
                    
                    csl += recipient.id;
                    
                    output += '<div>' + recipient.name + ': ' + recipient.email + '&nbsp;&nbsp;'
                        + '<input class="button_c1 delete_recipient" type="button" data-id="' + id + '" data-type="' + type + '" value="Delete" <c:if test="${JobSetUpForm.isPOAssigned eq 'Y'}">disabled ="disabled"</c:if>>'
                        + '</div>';
                }
                    
                if (type === 'Requestor') {
                    $('#requestorIds').val(csl);
                } else {
                    $('#additionalRecipientIds').val(csl);
                }
            }
            
            $('#email_recipients').html(output);
        }
        
        function validateEmail(email) { 
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        
        $(document).on('click', '.delete_recipient', function() {
            var id = $(this).data('id');
            var type = $(this).data('type');
            
            if (typeof id === 'undefined' || typeof type === 'undefined') {
                return;
            }
            
            deleteRecipient(id, type);
        });
        
        function deleteRecipient(id, type) {
            delete emailRecipients[type][id];
            
            renderEmailRecipients();
        }
        
        renderEmailRecipients();
        $('#requestor').html('<option value="-1">Loading...</option>');
        $('#requestrec').html('<option value="-1">Loading...</option>');
       
        $('[name="estimatedStartSchedule"]').datepicker();
        $('[name="estimatedCompleteSchedule"]').datepicker();
    });
</script>

<script>
    function openaddcontactwindow() {
        url = "AddAppendixContact.do?reftype=&appendixid=<c:out value='${JobSetUpForm.appendixId}'/>&isTicket=y&fromType=ticketDetail";
        window.open( url , 'myWindow' , 'left = 200 , top = 200 , width = 870 , height = 400 , resizable = yes , scrollbars = yes' ); 
        return true;
    }
</script>

<link rel="stylesheet" href="styles/style.css" type="text/css">
<title>Insert title here</title>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());
%>

</head>
<%@ include  file="/JobMenu.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF" onload="leftAdjLayers();">
<html:form action = "/JobEditAction">
<html:hidden property="jobId"/>
<html:hidden property="appendixId"/>
<html:hidden property="actionAddUpdate"/>
<html:hidden property="jobType"/>
<html:hidden property="status"/>
<html:hidden property="ownerId"/>
<html:hidden property="viewjobtype"/>
<html:hidden property="ref"/>
<html:hidden property="msaId"/>
<html:hidden property="jobOwnerOtherCheck"/> 
      
<input type="hidden" id="requestorIds" name="requestorIds" value="<c:out value='${JobSetUpForm.requestorIds}'/>" />
<input type="hidden" id="additionalRecipientIds" name="additionalRecipientIds" value="<c:out value='${JobSetUpForm.additionalRecipientIds}'/>" />

<logic:notEqual name="JobEditForm" property="jobType" value="inscopejob">

	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
				 <tr>
				 	<logic:equal name="JobEditForm" property="jobId" value="0">
			          	<td class = "Ntoprow1" width="450">&nbsp;</td>
			        </logic:equal>
				 	<logic:notEqual name="JobEditForm" property="jobId" value="0">
						<%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
					</logic:notEqual>	
					</tr>
			        <tr>
			        	<logic:equal name="JobEditForm" property="jobId" value="0">
							<td background="images/content_head_04.jpg" height="21" width ="100%">
								<div id="breadCrumb">
						          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
						    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "JobEditForm" property = "msaId"/>&Appendix_Id=<bean:write name = "JobEditForm" property = "appendixId"/>"><bean:write name = "JobEditForm" property = "msaName"/></a>
						    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "JobEditForm" property = "appendixId"/>&Name=<bean:write name = "JobEditForm" property = "appendixName"/>&ref=<%=chkaddendum%>"><bean:write name = "JobEditForm" property = "appendixName"/></a>
								</div>
							</td>
			        	</logic:equal>
			        	<logic:notEqual name="JobEditForm" property="jobId" value="0">
			        		<td background="images/content_head_04.jpg" height="21" colspan="7">
					        	<div id="breadCrumb">
							          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
							    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "JobEditForm" property = "msaId"/>&Appendix_Id=<bean:write name = "JobEditForm" property = "appendixId"/>"><bean:write name = "JobEditForm" property = "msaName"/></a>
							    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "JobEditForm" property = "appendixId"/>&Name=<bean:write name = "JobEditForm" property = "appendixName"/>&ref=<%=chkaddendum%>"><bean:write name = "JobEditForm" property = "appendixName"/></a>
								</div>
							</td>
						</logic:notEqual>    
						    
	 				</tr>
	 				<logic:present name="editJobFlag" scope = "request">
	 					<logic:equal name="editJobFlag" value="-9003">
	 						<tr valign="middle"><td colspan = "6" class="message" height="30" style="padding-left: 13px;">
								The job name entered is already assigned to another job in this appendix.
							</td></tr>
	 					</logic:equal>
	 				</logic:present>
	 				<tr>    
							<td colspan = "6" width="500"><h2>
							 	<logic:equal name="JobEditForm" property="jobId" value="0">
			          			 Add Job
			        			</logic:equal>
			        			<logic:notEqual name="JobEditForm" property="jobId" value="0">
			        			&nbsp;<bean:message bundle = "pm" key ="job.edit.new.label"/>&nbsp;<bean:write name = "JobEditForm" property = "jobName"/>
			        			</logic:notEqual>
			        		</h2>	
						</td>
					</tr> 
	 				
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" >
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
				</table>
			</td>
		</tr>
	</table>
</logic:notEqual>
<logic:equal name="JobEditForm" property="jobType" value="inscopejob">	
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AppendixDashboardMenuScript.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
			      </tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <logic:present name="editJobFlag" scope = "request">
	 					<logic:equal name="editJobFlag" value="-9003">
	 						<tr valign="middle"><td colspan = "1" class="message" height="30" style="padding-left: 13px;">
								The job name entered is already assigned to another job in this appendix.
							</td></tr>
	 					</logic:equal>
	 				</logic:present>
			        <tr>    
						<td colspan = "1"><h2>
						 	<logic:equal name="JobEditForm" property="jobId" value="0">
		          			 Add Job
		        			</logic:equal>
		        			<logic:notEqual name="JobEditForm" property="jobId" value="0">
		        			&nbsp;<bean:message bundle = "pm" key ="job.edit.new.label"/>&nbsp;<bean:write name = "JobEditForm" property = "jobName"/>
		        			</logic:notEqual>
		        			</h2>	
						</td>
					</tr> 
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="JobEditForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="JobEditForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="JobEditForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					          <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
</logic:equal>

<logic:notEqual name="JobEditForm" property="jobType" value="inscopejob">	

		<logic:notEqual name="JobEditForm" property="jobId" value="0">	
			<%@ include  file="/JobMenuScript.inc" %>	
		</logic:notEqual>

</logic:notEqual>
                
	<table width ="100%" border="0" cellspacing="1" cellpadding="1" >
			<tr>
					  <td width="2" height="0"></td>
					  <td>
					  <table border="0" cellspacing="1" cellpadding="1" width="450">
					  		 <tr> 
								<td colspan = "2" class = "formCaption" ><bean:message bundle = "pm" key ="job.detail.general"/></td>
	  						 </tr> 	
	  						  <tr>
		  						<td  class="colDark">Job Name</td>
								<td  class="colLight">
									<html:text size = "20" maxlength = "" name = "JobEditForm" property ="jobName" styleClass = "text" />
								</td>
							 </tr>
                                                        <tr>
                                                            <td  class="colDark">Requestor
							</td>
							<td class="colLight">
                                                            <select id="requestor" name="requestor" class="select"></select>
                                                        </td>
                                                        <tr>
                                                            <td  class="colDark">Additional Recipient
							</td>
							<td class="colLight">
                                                            <select style="width: 180px;" id="requestrec" name="requestrec" class="select"></select>&nbsp;&nbsp;<a target="_blank" class="button_c1" href="/content/ilex/#/appendix/<c:out value='${JobEditForm.appendixId}'/>/contacts">Manage</a>
                                                        </td>	
                                                    </tr>
                                                    <tr>
                                                        <td class="colDark">Email Recipients</td>
                                                        <td class="colLight">
                                                            <div id="email_recipients"></div>
                                                        </td>
                                                    </tr>

						<tr> 
								<td colspan = "2" class = "formCaption" height = "25"><bean:message bundle = "pm" key ="job.detail.siteinformation"/></td>
	  						 </tr> 	
	  						  <tr> 
							    <td  class="colDark"><bean:message bundle = "pm" key = "job.tabular.sitename"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="siteName"/></td>
							  </tr>
	  						 <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key = "job.tabular.locality_uplift"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="localityUplift"/></td>
							  </tr>
	  						 <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key = "job.tabular.union"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="union"/></td>
							  </tr>
							  <tr> 
								<td colspan = "2" class = "formCaption" height = "25"><bean:message bundle = "pm" key ="job.tabular.estimatedcost"/></td>
	  						 </tr> 	
	  						 <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.estimatedmaterialscost"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="estimatedMaterialCost"/></td>
							  </tr>
							  <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.estimatedCNSlaborcost"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="estimatedCnsFieldLaborCost"/></td>
							  </tr>
							  <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.estimatedcontractlaborcost"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="estimatedContractFieldLaborCost"/></td>
							  </tr>
							  <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.estimatedfreightcost"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="estimatedFreightCost"/></td>
							  </tr>
							  <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.estimatedtravelcost"/></td>
							    <td  class="colLight"><bean:write name ="JobEditForm" property ="estimatedTravelCost"/></td>
							  </tr>
	  						 <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.estimatedtotalcost"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="estimatedTotalCost"/></td>
							  </tr>
	  						 <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.extendedprice"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="extendedPrice"/></td>
							  </tr>
	  						 <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.proformamargin"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="proformaMargin"/></td>
							 </tr>
							 <tr> 
								<td colspan = "2" class = "formCaption" height = "25"><bean:message bundle = "pm" key ="job.tabular.estimatedschedule"/></td>
	  						 </tr> 
	  						  <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.estimatedschedulestart"/></td>
							    <td   class="colLight">
								    <html:text  name  ="JobEditForm" styleClass = "text" size = "10" property = "estimatedStartSchedule" readonly = "true"/>
<!-- 							    </td> -->
							    
<!-- 							    <td nowrap="nowrap">  -->
								    <html:select styleId="startdatehh" property = "startdatehh" styleClass='comboo'>  
								    <option value='00'>00</option><% String k=null; int j=0; for(j=1;j<=12;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%>
									</html:select>
									<html:select styleId="startdatemm" property = "startdatemm" styleClass='comboo'>  
									<option value='00'>00</option><% String k=null; int j=0; for(j=0;j<=59;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%>
									</html:select>
									<html:select styleId="startoptionname" property = "startoptionname" styleClass='comboo'>
									<option value=AM>AM</option><option value=PM>PM</option>
									</html:select>
			  					</td>
							    
							    
							  </tr>
	  						 <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.estimatedschedulecomplete"/></td>
							    <td   class="colLight">
								     <html:text  name  ="JobEditForm" styleClass = "text" size = "10" property = "estimatedCompleteSchedule" readonly = "true"/>
		   							
		   							<html:select styleId="completedatehh" property = "completedatehh" styleClass='comboo'>  
								    <option value='00'>00</option><% String k=null; int j=0; for(j=1;j<=12;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%>
									</html:select>
									<html:select styleId="completedatemm" property = "completedatemm" styleClass='comboo'>  
									<option value='00'>00</option><% String k=null; int j=0; for(j=0;j<=59;j++) { if(j<10) k="0"+j; else k=""+j; %> <option value=<%=k%>><%=k%></option><%}%>
									</html:select>
									<html:select styleId="completeoptionname" property = "completeoptionname" styleClass='comboo'>
									<option value=AM>AM</option><option value=PM>PM</option>
									</html:select>
									
							    </td>
							 </tr>
	  						  <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="job.tabular.status"/></td>
							    <td   class="colLight"><bean:write name ="JobEditForm" property ="status"/></td>
							 </tr>
	  						 <tr> 
							    <td   class="colDark"><bean:message bundle = "pm" key ="appendix.tabular.customerreference"/></td>
							    <td   class="colLight">
							    <html:text  styleClass = "text" maxlength = "50" size = "10" name ="JobEditForm" property ="custRef"/></td>
							 </tr>
							 <tr>
							 	<td class = "colLight" colspan="2" align="center">
									<html:submit property = "save" styleClass = "button_c" onclick = "return validate();">Submit</html:submit>&nbsp;
									<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>&nbsp;
									<html:button property = "back" styleClass = "button_c" onclick = "Backaction();">Back</html:button>
								</td>
							<tr>	
					  </table>
			</tr>			
	</table>
</html:form>
</body>
<script>

function trimFields() 
{
	var field=document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text') 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}

function Backaction()
{
	history.go(-1);
}
function validate()
{	
	trimFields();
	var val = true;
	if( document.forms[0].jobName.value == "" )
		{
			alert( "<bean:message bundle = "pm" key = "job.tabular.jobenter"/>" );
			document.forms[0].jobName.focus();
			return false;
		}
	//if( document.forms[0].requestorEmail.value == "" )
	//{
		//alert( "Please Add Email" );
		//document.forms[0].requestorEmail.focus();
		//val = emailvalidation();
	//}
	
	
	
	if(document.forms[0].estimatedStartSchedule.value!="" && document.forms[0].estimatedCompleteSchedule.value!="") 
		{
			if (!compDate_mdy(document.forms[0].estimatedStartSchedule.value, document.forms[0].estimatedCompleteSchedule.value)) 
			{
				document.forms[0].estimatedCompleteSchedule.focus();
				alert("<bean:message bundle = "pm" key = "job.tabular.datevalidation"/>");
				return false;
			} 
		}	
		
		
		
 	return true;
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="JobEditAction.do?showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="JobEditAction.do?resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].appendixId.value;
 	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}

</script>

</html:html>