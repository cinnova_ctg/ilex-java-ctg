<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying  MSA detail.
*
-->

<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
 <bean:define id = "msastatus" name = "MSADetailForm" property = "status"/> 
<bean:define id = "uploadstatus" name = "MSADetailForm" property = "file_upload_check" type = "java.lang.String"/>  
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>

<%
int i = 0;
String editMSAFlag = null;
String editAction = null;
int addRow = -1;
boolean addSpace = false;
int rowHeight = 65;
int ownerListSize = 0;
String checkowner = null;
int j=2;

if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());


if(request.getAttribute("editAction") != null)
	editAction = request.getAttribute("editAction")+"";

if(request.getAttribute("editMSAFlag") != null)
	editMSAFlag = request.getAttribute("editMSAFlag")+"";



String MSA_Id = "";
	if( request.getParameter( "MSA_Id" ) != null )
	MSA_Id = request.getParameter( "MSA_Id" );

else if( request.getAttribute( "MSA_Id" ) != null)
	MSA_Id = (String) request.getAttribute( "MSA_Id" );
	
	

	
rowHeight += ownerListSize*18;



%>
<html:html>
<head>
	<title><bean:message bundle = "pm" key = "msa.detail.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>    
  	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/functions.js"></script>
	
</head>
<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<bean:write name = "MSADetailForm" property = "status" />";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>
<%@ include file = "/NMenu.inc" %>  

<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
</head>

<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');leftAdjLayers();OnLoad();">
<html:form action = "/MSADetailAction">
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr> 
		       <%--   <%if( !msastatus.equals( "Draft" )  && !msastatus.equals( "Review" )){%>
			        <%if(msastatus.equals( "Cancelled" )){%>
			        	<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
			        <%}else{ %>
						<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
			        <%}%>
				<%}else{%>
               		 <td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
				<%}%>
					<td id = "pop2"  width = "120"  class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
					<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
					<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td>  --%>
					
	 	<%--  <%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>		  --%>
		
	<!--  Menu New Style -->
  <div id="menunav">
	<ul>
		<%
		if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )) {
			if(msastatus.equals( "Cancelled" )) {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')">Edit</a></li>
				<%
			} else {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')">Edit</a></li>
				<%
			}
		} else {
			%>
			<li><a class="drop" href="javascript:editmsa();">Edit</a></li>	
			<%
		}
		%>
		<li>
			<a href="#" class="drop"><span>Manage</span></a>
			<ul>
				<li>
					<%
					if((String) request.getAttribute("list") != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/></span></a>
						<ul>
						
						
						
							<%= (String)request.getAttribute("list") %>
												
						</ul>
						<%
					} else {
						%>
						<a href="#"><bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/></a>
						<%
					}
					%>
				</li>
				<li>
					<a href="#">View MSA</span></a>
					<ul>
						<li><a href="MSADetailAction.do?MSA_Id=<%= MSA_Id %>&Prjid=<%= MSA_Id %>">Details</a></li>
						<%
						if( msastatus.equals( "Signed" ) || msastatus.equals( "Approved" ) || msastatus.equals( "Pending Customer Review" )) {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/></a></li>
							<li><a href="javascript:view('rtf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewrtf"/></a></li>
						     <%
							if(request.getAttribute("new_addendumlist") != "" ) {
								%>
								<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "msa.detail.menu.viewhtml"/></a></li>
								<%
								/* if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) { */
								
								
									
								
// 								} 
								
					//			else {
									%>
									
									<%
					//			}
							} 
						}
						%> 
					</ul>
				</li>
				<%
				if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )) {
					%>
					<li><a href="javascript:sendemail();"><bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/></a></li>
					
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.sendMSA.nopriviledgestatus"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/></a></li>
					<%
				}
				if( msastatus.equals( "Signed" ) || msastatus.equals( "Inwork" ) ) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.formaldocdate"/></a></li>
					
					<%
				}  else {
					%>
					<li><a href="EditBdmAction.do?msaId=<%= MSA_Id %>&fromType=MSADetail">Edit BDM</a></li>
					
					<%
				} %>
				<% 
				if( msastatus.equals( "Signed" ) || msastatus.equals( "Inwork" )) {
					%>
					<%-- <li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/></a></li> --%>
					
					<%
				} else {
					%>
					 <li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/></a></li>
					
					<%
				}
				if(msastatus.equals( "Cancelled" )) {
					%> 
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "msa.detail.menu.projectmanager"/></a></li> 
 					<% 
 				} 
				%> 
				<li>
					<a href="#"><span>MSA Comments</span></a>
					<ul>
						<li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "msa.detail.menu.viewall"/></a></li>
						<%
						if(msastatus.equals( "Cancelled" )) {
							%>
							<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/></a></li>
							<%
						}
						%>
					</ul>
				</li>
				<%
				if( msastatus.equals( "Pending Customer Review" )) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%= MSA_Id %>&entityType=MSA"><bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.upload.state"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/></a></li>
					<%
				}
				if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )) {
					if(msastatus.equals( "Cancelled" )) {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>
						<%
					} else {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>
						<%
					}
				} else {
					%>
					<li><a href="javascript:editmsa();"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>	
					<%
				}
				%>
				
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "msa.detail.menu.documents"/></span></a>
					<ul>
						<li><a href="EntityHistory.do?entityId=<%= MSA_Id %>&entityType=MSA&function=viewHistory&msastatus=<%=msastatus%>">MSA  History</a></li>
						<li>
							<a href="#"><span>Support Documents</span></a>
							<ul>
								<li><a href="EntityHistory.do?entityId=<%= MSA_Id %>&msaId=<%= MSA_Id %>&entityType=MSA Supporting Documents&function=addSupplement&linkLibName=MSA">Upload</a></li>
								<li><a href="EntityHistory.do?entityType=MSA Supporting Documents&msaId=<%= MSA_Id %>&linkLibName=MSA&function=supplementHistory">History</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="javascript:del();"><bean:message bundle = "pm" key = "msa.detail.menu.deletemsa"/></a></li>
			</ul>
		</li>
		<li><a class="drop" href="AppendixUpdateAction.do??firstCall=true&MSA_Id=<%= MSA_Id%>">Appendices</a></li>
	</ul>
</div>
	<!-- End of Style -->	
			
        		</tr>
		        <tr>
			          <td background="images/content_head_04.jpg" height="21" colspan="7" width="100%">
			          		<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
			          </td>
		        </tr>
		        <tr>    
					 <td colspan = "7" height="1"><h2><bean:message bundle = "pm" key = "msa.detail.new.label"/>&nbsp;<bean:write name = "MSADetailForm" property = "name"/></h2></td>
				</tr> 
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" colspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
	                			<div id="featured">
										<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
								</div>								
	                </div>		
				<span>
				<div id="filter" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="statuslist" scope="request">
									    	<logic:iterate id = "list" name = "statuslist"> 
										 		<html:multibox property = "selectedStatus"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											</logic:iterate>
										</logic:present>
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="MSADetailForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="MSADetailForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="MSADetailForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist">
							    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<% 
										checkowner = "javascript: checking('"+ownerId+"');";
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                         
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
                              
                            </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
<%-- <SCRIPT language = JavaScript1.2>
var msaSend = '<bean:message bundle = "pm" key = "msa.detail.sendMSA.nopriviledgestatus"/>';
var msaCRS = '<bean:message bundle = "pm" key = "msa.detail.upload.state"/>';
<!--
if ( isMenu ) {
arMenu1 = new Array()
arMenu3 = new Array() 
arMenu2 = new Array(
130,
findPosX( 'pop2' ),findPosY( 'pop2' ),
"","",
"","",
"","",
	<%if( ( String )request.getAttribute( "list" ) != "" ){%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/>","#",1,
	<%}else{%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/>","#",0,
	<%}%>
		"<bean:message bundle = "pm" key = "msa.detail.Viewmsa"/>","#",1,
		
	<%if( !msastatus.equals( "Draft" )  && !msastatus.equals( "Review" ) && !msastatus.equals( "Cancelled" )){%>
   		 "<bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/>","javascript:sendemail();",0,
   		 "Edit BDM","EditBdmAction.do?msaId=<%= MSA_Id %>&fromType=MSADetail",0,
   		 "<bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus"/>')",0,
    <%} else {%>
    	"<bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/>","javascript:alert(msaSend);",0,
    	<%if(msastatus.equals( "Cancelled" )){%>
	    	"Edit BDM","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.noprivilegetransstatus.bdm.cancelled"/>')",0,
	    	"<bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus.cancelled"/>')",0,
    	<%}else{%>
	    	"Edit BDM","EditBdmAction.do?msaId=<%= MSA_Id %>&fromType=MSADetail",0,
	    	"<bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/>","javascript:setformaldocdate();",0,
    	<%}%>
	<% } %>
	
	 "<bean:message bundle = "pm" key = "msa.detail.Comments"/>","#",1,
	<% if( msastatus.equals( "Pending Customer Review" )){%>
	      "<bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/>","EntityHistory.do?function=modifyDocument&entityId=<%= MSA_Id %>&entityType=MSA",0,
	<%}else{%>
		  "<bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/>","javascript:alert(msaCRS)",0,	
	<%}%>
		
	<%if( !msastatus.equals( "Draft" )  && !msastatus.equals( "Review" )){%>
		<%if(msastatus.equals( "Cancelled" )){%>
			"<bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')",0,
		<%}else{%>
			"<bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')",0,		
		<%}%>
		
	<%}else{%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/>","javascript:editmsa();",0,
	<%}%>
	"Documents","#",1,	
	"<bean:message bundle = "pm" key = "msa.detail.menu.deletemsa"/>","javascript:del();",0
)

	<% if( ( String )request.getAttribute( "list" ) != "" ) {%>
	arMenu2_1=new Array(
	<%=( String ) request.getAttribute( "list" )%>
	)
	<%}%>

	arMenu2_6=new Array(
	"<bean:message bundle = "pm" key = "msa.detail.menu.viewall"/>","javascript:viewcomments();",0,
	<%if(msastatus.equals( "Cancelled" )){%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/>","javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>');",0
	<%}else{%>
		"<bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/>","javascript:addcomment();",0
	<%}%>
	)

	arMenu2_2=new Array(
	<%if( !msastatus.equals( "Draft" )  && !msastatus.equals( "Review" ) && !msastatus.equals("Cancelled")){%>
		"Details","MSADetailAction.do?MSA_Id=<%= MSA_Id %>&Prjid=<%= MSA_Id %>",0,
		"<bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/>","javascript:view('pdf');",0	
	<%}else{%>
		"Details","MSADetailAction.do?MSA_Id=<%= MSA_Id %>&Prjid=<%= MSA_Id %>",0,
		"<bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/>","javascript:view('pdf');",0,
		"<bean:message bundle = "pm" key = "msa.detail.menu.viewrtf"/>","javascript:view('rtf');",0,
		"<bean:message bundle = "pm" key = "msa.detail.menu.viewhtml"/>","javascript:view('html');",0
	<%}%>
	)
	
	arMenu2_7 = new Array(
	 "<bean:message bundle = "pm" key = "msa.detail.menu.upload"/>","javascript:upload();",0
	)

	arMenu2_9 = new Array(
		"MSA History","EntityHistory.do?entityId=<%= MSA_Id %>&entityType=MSA&function=viewHistory&msastatus=<%=msastatus%>",0,
		"Support Documents","",1
	)

	arMenu2_9_2 = new Array(
	"Upload","EntityHistory.do?entityId=<%= MSA_Id %>&msaId=<%= MSA_Id %>&entityType=MSA Supporting Documents&function=addSupplement&linkLibName=MSA",0,
	"History","EntityHistory.do?entityType=MSA Supporting Documents&msaId=<%= MSA_Id %>&linkLibName=MSA&function=supplementHistory",0
	)

}
document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
//-->
</script> --%>
<table>

	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		
		<tr>
  			<td width="2" height="0"></td>
  			 <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "450"> 
  			 				<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							<logic:present name = "deleteflag" scope = "request">
					    		<tr> 
								<td colspan = "4" class = "message" height = "30">
					    			<logic:equal name = "deleteflag" value = "0">
					    				<bean:message bundle = "pm" key = "msa.tabular.delete.success"/>		
					    				<script>
											parent.ilexleft.location.reload();
										</script>	
									</logic:equal>
					
									<logic:equal name = "deleteflag" value = "-9001">
										<bean:message bundle = "pm" key = "msa.tabular.delete.failure1"/>	
									</logic:equal>
					
									<logic:equal name = "deleteflag" value = "-9002">
										<bean:message bundle = "pm" key = "msa.tabular.delete.failure2"/>	
									</logic:equal>
								</td>
								</tr>
							</logic:present>
							 <logic:present name = "editMSAFlag" scope = "request">	
							   <tr>
							  	 <td colspan = "4" class = "message" height = "30">
							     	<logic:equal name = "editMSAFlag" value = "0">
									 	<logic:notEqual name = "editAction" value = "A">
									     	MSA updated successfully
									 	</logic:notEqual>
								 	</logic:equal>
								
								 	<logic:notEqual name = "editMSAFlag" value = "0">
									 	<logic:equal name = "editAction" value = "A">
									 		MSA add failure
									 	</logic:equal>
									
									 	<logic:notEqual name = "editAction" value = "A">
									     	MSA update failure
									 	</logic:notEqual>
									</logic:notEqual>
							    </td>
							 </tr>
							</logic:present> 
							<logic:present name = "addcommentflag" scope = "request">
							<tr> 
							<td colspan = "4" class = "message" height = "30">
						    	<logic:equal name = "addcommentflag" value = "0">
									<bean:message bundle = "pm" key = "msa.detail.addcomment.success"/>	
					    		</logic:equal>
		    		
					    		<logic:notEqual name = "addcommentflag" value = "0">
									<bean:message bundle = "pm" key = "msa.detail.addcomment.failure"/>
					    		</logic:notEqual>
					    		</td>
					    		</tr>
		    				</logic:present>
    				
		    				<logic:present name = "changestatusflag" scope = "request">
		    				<tr> 
							<td colspan = "4" class = "message" height = "30">
						    	<logic:equal name = "changestatusflag" value = "0">
									<bean:message bundle = "pm" key = "msa.detail.changestatus.success"/>
					    		</logic:equal>
    		
					    		<logic:notEqual name = "changestatusflag" value = "0">
									<bean:message bundle = "pm" key = "msa.detail.changestatus.failure"/>
					    		</logic:notEqual>
					    		</td>
					    		</tr>
		    				</logic:present>
		    				
		    				<logic:present name = "uploadflag" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
								    	<logic:equal name = "uploadflag" value = "0">
											<bean:message bundle = "pm" key = "msa.detail.upload.success"/>
							    		</logic:equal>
		    		
							    		<logic:notEqual name = "uploadflag" value = "0">
											<bean:message bundle = "pm" key = "msa.detail.upload.failure"/>
							    		</logic:notEqual>
							    	</td>
						    	</tr>
		    				</logic:present>
		    				<logic:present name = "docMException" scope = "request">
			    				<tr> 
										<td colspan = "4" class = "message" height = "30">
											<bean:message bundle = "pm" key = "error.nodocument.docm"/>
										</td>
								</tr>		
		    				</logic:present>
		    				
		    				<logic:present name = "emailflag" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
								    	<logic:equal name = "emailflag" value = "0">
											<bean:message bundle = "pm" key = "msa.detail.mailsent"/>
							    		</logic:equal>
		    		
							    		<logic:notEqual name = "emailflag" value = "0">
											<bean:message bundle = "pm" key = "msa.detail.mailsentfailure"/>
							    		</logic:notEqual>
							    	</td>
						    	</tr>
		    				</logic:present>
		    				<logic:present name = "documentInsertionInDocM" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.documentinsertion"/>
									</td>
								</tr>
							</logic:present>
							<logic:present name = "EntityDetailsNotFoundException" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.entitydetails"/>
									</td>
								</tr>
							</logic:present>
							<logic:present name = "ControlledTypeException" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.controlledtype"/>
									</td>
								</tr>
							</logic:present>
							<logic:present name = "DocMException" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.docmexception"/>
									</td>
								</tr>
							</logic:present>
							<logic:present name = "CouldNotAddToRepositoryException" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.couldnotaddtorepository"/>
									</td>
								</tr>
							</logic:present>
							<logic:present name = "CouldNotReplaceException" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.couldnotreplace"/>
									</td>
								</tr>
							</logic:present>
							<logic:present name = "CouldNotCheckDocumentException" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.couldcotcheckdocument"/>
									</td>
								</tr>
							</logic:present>
							<logic:present name = "DocumentIdNotUpdatedException" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.couldupdatedocumentid"/>
									</td>
								</tr>
							</logic:present>
							<logic:present name = "Exception" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
										<bean:message bundle = "pm" key = "exception.general"/>
									</td>
								</tr>
							</logic:present>			
 		</table> 
 		<TABLE width="450">				
  					<!-- general:start -->
  					<tr> 
    					<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "msa.detail.general"/></td>
    				</tr>
    				<tr>
	    				<td class = "colDark" height = "20"><bean:message bundle = "pm" key = "msa.detail.type"/></td>
	    				<td  class = "colLight"><bean:write name = "MSADetailForm" property = "msatype"/></td>
	    			</tr>
    				<tr>
    					<td  class = "colDark" height = "20"><bean:message bundle = "pm" key = "msa.detail.salestype"/></td>
					    <td  class = "colLight"><bean:write name = "MSADetailForm" property = "salestype"/></td>
    				</tr>
    				<tr>
    					<td class = "colDark" height = "20">
							<bean:message bundle = "pm" key = "msa.detail.status"/>&nbsp;
								<logic:equal name = "MSADetailForm" property = "file_upload_check" value = "U">
									<img name = "checkuploaded"  src = "images/Upload1.gif" width = "14" height = "11" border = "0" alt = '<bean:message bundle = "pm" key = "msa.detail.upload.message"/>'/>
								</logic:equal>
						</td>
    					<td class = "colLight"><bean:write name = "MSADetailForm" property = "status" /></td>
    				</tr>
    				<tr> 
    					<td class = "colDark" height = "20"><bean:message bundle = "pm" key = "msa.detail.Formaldocumentdate"/></td>
    					<td   class = "colLight"><bean:write name = "MSADetailForm" property = "formaldocdate" /></td>
					</tr>
					
					<tr> 
    					<td class = "colDark" height = "20"><bean:message bundle = "pm" key = "msa.detail.unionupliftfactor"/></td>
    					<td  class = "colLight"><bean:write name = "MSADetailForm" property = "unionupliftfactor" /></td>
					</tr>
					
					<tr> 
    					<td class = "colDark" height = "20"><bean:message bundle = "pm" key = "msa.detail.lastmodified"/></td>
    					<td class = "colLight"><bean:write name = "MSADetailForm" property = "lastmsamodifiedby" />  <bean:write name = "MSADetailForm" property = "lastmsamodifieddate" /> </td>
					</tr>
					<!-- general:End -->
					
    				<!-- ScheduleInformation:start -->
    				<tr>
						<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "msa.detail.ScheduleInformation"/></td>
					</tr> 
					<tr> 
						<td   class = "colDark"><bean:message bundle = "pm" key = "msa.detail.PlannedScheduleRequired"/></td>
    					<td   class = "colLight"><bean:write name = "MSADetailForm" property = "reqplasch"/></td>
					</tr>
					<tr>
					    
					    <td  class = "colDark"><bean:message bundle = "pm" key = "msa.detail.PlannedScheduleEffective"/></td>
					    <td  class = "colLight"><bean:write name = "MSADetailForm" property = "effplasch" /></td>
  					</tr>
					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "msa.detail.plannedscheduledays"/></td>
    					<td class = "colLight"><bean:write name = "MSADetailForm" property = "schdays" /></td>
					</tr>
   					<!-- ScheduleInformation:End -->
 
					<!-- reviews:start -->
					<tr> 
    					<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "msa.detail.reviews"/></td>
    				</tr>
    				<tr>
    					<td class = "colDark" height = "20"><bean:message bundle = "pm" key = "msa.detail.draftreview"/> </td>
    					<td class = "colLight"><bean:write name = "MSADetailForm" property = "draftreviews" /></td>
    				</tr>
    				<tr>
    					<td class = "colDark" height = "20"><bean:message bundle = "pm" key = "msa.detail.businessreview"/></td>
    					<td class = "colLight"><bean:write name = "MSADetailForm" property = "businessreviews" /></td>
    				</tr>
    				<!-- reviews:End -->
    				
    				<!-- Payment:start -->
    				<tr>
						<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "msa.detail.Payment"/></td>
					</tr>
					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "msa.detail.PaymentTerms"/></td>
    					<td class = "colLight"><bean:write name = "MSADetailForm" property = "payterms" /></td>
  					</tr>
  					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "msa.detail.PaymentQualifier"/></td>
    					<td class = "colLight"><bean:write name = "MSADetailForm" property = "payqualifier" /></td>
					</tr>
  					<!-- Payment:End -->

					<!-- Contacts:Start -->
					<tr> 
						<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "msa.detail.Contacts"/></td>
					</tr>
					<tr>
						<td class = "colDark" height = "20" width = "34%"><bean:message bundle = "pm" key = "msa.detail.salespoc"/></td>
						<td class = "colLight" height = "20"><bean:write name = "MSADetailForm" property = "salesPOC" /></td>
					</tr>
					<tr>
						<td class = "colDark" width = "33%"><bean:message bundle = "pm" key = "msa.detail.customerPOCbusiness"/></td>
						<td class = "colLight"><bean:write name = "MSADetailForm" property = "custPocbusiness" /></td>
					</tr>
					<tr>
						<td class = "colDark" width = "33%"><bean:message bundle = "pm" key = "msa.detail.customerPOCcontract"/></td>
						<td class = "colLight"><bean:write name = "MSADetailForm" property = "custPoccontract" /></td>	
					</tr>
					<!-- Contacts:End -->
					<!-- Comment:Strat -->
					<tr> 
						<td colspan = "2" class = "formCaption" height = "30">
						<bean:message bundle = "pm" key = "msa.detail.Comments"/></b></td>
					</tr> 
					 <logic:notEqual name = "MSADetailForm" property = "lastcomment" value = "">
					<tr> 
					    <td class = "colDark">
					    	<bean:write name = "MSADetailForm" property = "lastchangedate" /></td>
					    <td class = "colLight">
					    	<bean:write name = "MSADetailForm" property = "lastcomment"/>
					    </td>
				    </tr>
				   </logic:notEqual>
				    <logic:equal name = "MSADetailForm" property = "lastcomment" value = "">
					<tr> 
					    <td class = "colLight" colspan="2"><font class="Nmessage">&nbsp;&nbsp;No Comments.</font>
					    </td>
				    </tr>
				   </logic:equal>
					<!-- Comment:End -->
				    
				    <!-- Upload file: Start -->
				    <logic:equal name = "MSADetailForm" property = "file_upload_check" value = "U">
					<tr>
						<td class = "formCaption" colspan = "2">
							<bean:message bundle = "pm" key = "common.fileupload.history"/>
						</td>
					</tr>
					<logic:iterate id = "ms" name = "uploadeddocs" scope = "request">
							<tr>
								<td class = "colDark"><bean:message bundle = "pm" key = "common.fileupload.uploaddate"/></td>
							    <td class = "colLight">
									<bean:write name = "ms" property = "file_uploaded_date"/>		
								</td>
							</tr>
							<tr>
								<td class = "colDark"><bean:message bundle = "pm" key = "common.fileupload.filename"/></td>
								<td class = "colLight"> 
									<a href = "ViewuploadedocumentAction.do?ref=downloaddocument&file_id=<bean:write name = "ms" property = "file_id" />"><bean:write name = "ms" property = "file_name" /></a>
								</td>
							</tr>
							<tr>	
								<td class = "colDark"><bean:message bundle = "pm" key = "common.fileupload.uploadremarks"/></td>
								<td class = "colLight">
									<bean:write name = "ms" property = "file_remarks"  />
								</td>
							</tr>	
							<tr>
								<td class = "colDark"><bean:message bundle = "pm" key = "common.fileupload.uploadby"/></td>
								<td class = "colLight">
									<bean:write name = "ms" property = "file_uploaded_by"/>		
								</td>
		  					</tr>
		   				  </logic:iterate>	
		  		</logic:equal>	
				    <!-- Upload file: End -->
				</TABLE>
					<jsp:include page = '/Footer.jsp'>
		      			<jsp:param name = 'colspan' value = '37'/>
		      			<jsp:param name = 'helpid' value = 'msadetailpage'/>
		 			</jsp:include>	
			</td>
		</tr>
	</table>
</html:form>
</table>
</td></tr>
</table>
</body>
<script>
function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filter").style.visibility="visible";
<%}%>

}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
		
}
function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="MSADetailAction.do?MSA_Id=<bean:write name = 'MSADetailForm' property = 'msaId' />&Prjid=<bean:write name = 'MSADetailForm' property = 'msaId'/>&Name=<bean:write name = 'MSADetailForm' property = 'name'/>&clickShow=true";
	document.forms[0].submit();
	return true;
}

function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="MSADetailAction.do?MSA_Id=<bean:write name = 'MSADetailForm' property = 'msaId' />&Prjid=<bean:write name = 'MSADetailForm' property = 'msaId'/>&Name=<bean:write name = 'MSADetailForm' property = 'name'/>&firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				document.forms[0].selectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				if(document.forms[0].selectedOwners[j].checked) {
				 	document.forms[0].selectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{
	for(i=0;i<document.forms[0].selectedOwners.length;i++)
	{
			if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
			{
				document.forms[0].otherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="MSADetailAction.do?MSA_Id=<bean:write name = 'MSADetailForm' property = 'msaId' />&Prjid=<bean:write name = 'MSADetailForm' property = 'msaId'/>&Name=<bean:write name = 'MSADetailForm' property = 'name'/>&opendiv=0";
 				document.forms[0].submit();
			}
	}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="MSADetailAction.do?MSA_Id=<bean:write name = 'MSADetailForm' property = 'msaId' />&Prjid=<bean:write name = 'MSADetailForm' property = 'msaId'/>&Name=<bean:write name = 'MSADetailForm' property = 'name'/>&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="MSADetailAction.do?MSA_Id=<bean:write name = 'MSADetailForm' property = 'msaId' />&Prjid=<bean:write name = 'MSADetailForm' property = 'msaId'/>&Name=<bean:write name = 'MSADetailForm' property = 'name'/>&week=0&clickShow=true";
document.forms[0].submit();
return true;
}






</script>
</html:html>

