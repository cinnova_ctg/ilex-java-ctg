<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<bean:define id = "codes" name = "codeslist" scope = "request"/>

<%
int allCommentSize = 0;
int addRow = -1;
boolean addSpace = false;
int rowHeight = 65;
int ownerListSize = 0;
int j=2;
String checkowner=null;


if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());


if(request.getAttribute("allCommentSize") != null)
	allCommentSize = Integer.parseInt(request.getAttribute("allCommentSize")+"");
String MSA_Id ="";
String msastatus="";
String uploadstatus ="";
String status ="";

if(request.getAttribute("status")!=null){
	status = request.getAttribute("status").toString();
	msastatus = request.getAttribute("status").toString();
}
if(request.getAttribute("MSA_Id")!=null)
	MSA_Id= request.getAttribute("MSA_Id").toString();

if(request.getAttribute("uploadstatus")!=null)
	uploadstatus= request.getAttribute("uploadstatus").toString();
rowHeight += ownerListSize*18;

%>
<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%=status%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>
<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">

<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<title>MSA Edit Page</title>
<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
</style>

<script language="JavaScript" src="javascript/date-picker.js"></script>

<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>

	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
    <script language = "JavaScript" src = "javascript/popcalendar.js"></script>
    <script language = "JavaScript" src = "javascript/functions.js"></script>
	<%@ include  file="/NMenu.inc" %>
	<link rel="stylesheet" href="styles/style.css" type="text/css">
</head>
<%-- <%@ include  file="/MSAMenu.inc" %> --%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF"  onLoad ="leftAdjLayers();OnLoad();">
<html:form action = "/MSAEditAction">
<%-- <html:hidden property="msaId" /> --%>
<html:hidden property="actionAddUpdate" />
<html:hidden property="fromType"/>
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
							<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>


<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
		        	 <logic:equal name="MSAEditForm" property="msaId" value="0">
		        	 		<td class = "Ntoprow1" width="100%" colspan="6">&nbsp;</td>
		        	</logic:equal>
		        	<logic:notEqual name="MSAEditForm" property="msaId" value="0">
		        	
          			<%-- 	<%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )){%>
							<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
						<%}else{%>
	                 			 <td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
						<%}%>
							<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="8" width="450" >&nbsp;</td> --%>
					<%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>			
							</logic:notEqual>
				</tr>
		        <tr>
				          
				          <logic:equal name="MSAEditForm" property="msaId" value="0">
				          <td background="images/content_head_04.jpg" height="21" colspan="2">
							<div id="breadCrumb"><a href="MSAUpdateAction.do" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><span class="currentSelected"></span></div>
						 </td>	
						 <td background="images/content_head_04.jpg" height="21" colspan="3">
							
							</td>
						</logic:equal>
						<logic:notEqual name="MSAEditForm" property="msaId" value="0">
							 <td background="images/content_head_04.jpg" height="21" colspan="3">
							<div id="breadCrumb"><a href="MSAUpdateAction.do" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
							</td>
							<td background="images/content_head_04.jpg" height="21" colspan="8">
							&nbsp;
							</td>
						</logic:notEqual>
				          
 				 </tr>
 				 <tr>
 				 	<td colspan = "6"><h2>
					<logic:equal name="MSAEditForm" property="msaId" value="0">MSA Add
					</logic:equal>
					<logic:notEqual name="MSAEditForm" property="msaId" value="0">
						<bean:message bundle = "pm" key = "msa.edit.new.label"/><bean:write name = "MSAEditForm" property = "msaName"/>
					</logic:notEqual></h2>
					</td>
 				 </tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" colspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
	                			<div id="featured">
										<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
								</div>								
	                </div>		
				<span>
				<div id="filter" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="statuslist" scope="request">
									    	<logic:iterate id = "list" name = "statuslist"> 
										 		<html:multibox property = "selectedStatus"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											</logic:iterate>
										</logic:present>
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="MSAEditForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="MSAEditForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="MSAEditForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist">
							    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<% 
										checkowner = "javascript: checking('"+ownerId+"');";
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>">
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                         
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
                              
                            </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
<TABLE>
<logic:notEqual name="MSAEditForm" property="msaId" value="0">
<%@ include  file="/MSAMenuScript.inc" %>
</logic:notEqual>
<table border="0" cellspacing="1" cellpadding="1" >
<tr>
<td width="1" height="0"></td>
							
<td>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
 <tr>
  <td width="2" height="0"></td>
  <td>
  	<table border="0" cellspacing="1" cellpadding="1" width="500">
	  <tr> 
		<td colspan = "6" class = "formCaption" height = "25" ><bean:message bundle = "pm" key = "msa.detail.general"/></td>
	  </tr> 
	  <tr height="20"> 
	    <td colspan = "3" width="" class="colDark">Name<font class="red"> *</td>
	    <td colspan = "3" width="325" class="colLight">
		    <logic:equal name="MSAEditForm" property="msaId" value="0">
				<html:select property = "orgTopId" size = "1" styleClass = "Ncomboe">
			   		<html:optionsCollection name = "codes" property = "customername" value = "value" label = "label"/> 
			    </html:select>
			</logic:equal>
			<logic:notEqual name="MSAEditForm" property="msaId" value="0">
				<bean:write name="MSAEditForm" property="msaName" />
				<html:hidden property="orgTopId" />
			</logic:notEqual>
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "3" width="" class="colDark">MSA Type</td>
	    <td colspan = "3" width="325" class="colLight">
			<html:radio property="msaType" value="H"><font class="radioText">Hardware Only</font></html:radio>&nbsp;
			<html:radio property="msaType" value="S"><font class="radioText">Standard</font></html:radio>
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "3" width="" class="colDark">Sales Type</td>
	    <td colspan = "3" width="325" class="colLight">
			<html:radio property="salesType" value="D"><font class="radioText">Direct</font></html:radio>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<html:radio property="salesType" value="C"><font class="radioText">Channel</font></html:radio>
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "3" width="" class="colDark">BDM</td>
	    <td colspan = "3" width="325" class="colLight">
			 <html:select property = "salesPOC" size = "1" styleClass = "select">
			   		<html:optionsCollection name = "codes" property = "salesPOCcode" value = "value" label = "label"/> 
			 </html:select>
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "3" width="" class="colDark">Union Uplift Factor</td>
	    <td colspan = "3" width="325" class="colLight">
	    	 <html:text styleClass="text" size="5" maxlength="10" property="unionUpliftFactor"/>
		</td>
	  </tr>
	  <tr> 
		<td colspan = "6" class = "formCaption" height = "25">Payment Terms</td>
	  </tr> 
	  <tr> 
	    <td colspan = "3" width="" class="colDark">Term</td>
	    <td colspan = "3" width="325" class="colLight">
		    <html:select property = "payTerms" styleClass = "select">
				<html:optionsCollection name = "codes" property = "paytermscode" value = "value" label = "label"/> 
			</html:select>
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "3" width="" class="colDark">Qualifier</td>
		<td colspan = "3" width="325" class="colLight">
		    <html:select property = "payQualifier" styleClass = "select">
				<html:optionsCollection name = "codes" property = "payqualifiercode" value = "value" label = "label"/> 
			</html:select>
		</td>
     </tr>
      <tr> 
		<td colspan = "6" class = "formCaption" height = "25">Schedule Information</td>
	  </tr> 
	  <tr> 
	    <td colspan = "3" width="" class="colDark">Required By Date</td>
	    <td colspan = "3" width="325" class="colLight">
		    <html:text  styleClass = "text" size = "10" property = "reqPlannedSch" readonly = "true"/>
	   		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].reqPlannedSch , document.forms[0].reqPlannedSch , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "3" width="" class="colDark">Planned Effective Date</td>
		<td colspan = "3" width="325" class="colLight">
		    <html:text  styleClass = "text" size = "10" property = "effPlannedSch" readonly = "true"/>
	   		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].effPlannedSch , document.forms[0].effPlannedSch , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
		</td>
     </tr>
      <tr> 
	    <td colspan = "3" width="" class="colDark">Days Valid For</td>
		<td colspan = "3" width="325" class="colLight">
		    <html:text styleClass="text" size="5" maxlength="10" property="schDays"/>
		</td>
     </tr>
	  <tr> 
		<td colspan = "6" class = "formCaption" height = "25">Reviews</td>
	  </tr> 
	   
	  <tr> 
	    <td colspan = "3" width="" class="colDark">Draft</td>
	    <td colspan = "3" width="325" class="colLight">
			<html:radio property="draftReviews" value="S"><font class="radioText">Standard</font></html:radio>&nbsp;&nbsp;&nbsp;
			<html:radio property="draftReviews" value="E"><font class="radioText">Expedite</font></html:radio>		
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "3" width="" class="colDark">Business</td>
		<td colspan = "3" width="325" class="colLight">
		    <html:radio property="businessReviews" value="S"><font class="radioText">Standard</font></html:radio>&nbsp;&nbsp;&nbsp;
			<html:radio property="businessReviews" value="E"><font class="radioText">Expedite</font></html:radio>
		</td>
     </tr>
	  <tr> 
		<td colspan = "6" class = "formCaption" height = "25">Customer Contacts</td>
	  </tr> 
	  <tr height = "20"> 
	    <td colspan = "3" width="" class="colDark">Business</td>
	    <td colspan = "3" width="325" class="colLight">
			<logic:equal name="MSAEditForm" property="msaId" value="0">
				Update Only
				<html:hidden property="custPOCBusiness" />
			</logic:equal>
			<logic:notEqual name="MSAEditForm" property="msaId" value="0">
				<html:select property = "custPOCBusiness" styleClass = "select">
					<html:optionsCollection name = "codes" property = "pocList" value = "value" label = "label"/> 
				</html:select>	
			</logic:notEqual>	
		</td>
	  </tr>
	  <tr height = "20">  
	    <td colspan = "3" width="" class="colDark">Contracts</td>
		<td colspan = "3" width="325" class="colLight">
			<logic:equal name="MSAEditForm" property="msaId" value="0">
				Update Only
				<html:hidden property="custPOCContract" />
			</logic:equal>
			<logic:notEqual name="MSAEditForm" property="msaId" value="0">
			    <html:select property = "custPOCContract" styleClass = "select">
					<html:optionsCollection name = "codes" property = "pocList" value = "value" label = "label"/> 
				</html:select>
			</logic:notEqual>
		</td>
     </tr>
     <tr> 
		<td colspan = "6" class = "formCaption" height = "25">Comments</td>
	 </tr>
	  
	  <%
	  if(allCommentSize > 0) {
   	  %>	
			
		<tr>
			<td class = "colDark"><bean:message bundle = "pm" key = "common.viewcomment.commentdate"/></td>
			<td class = "colDark"><bean:message bundle = "pm" key = "common.viewcomment.commentby"/></td>
			<td colspan = "4" class = "colDark"><bean:message bundle = "pm" key = "common.viewcomment.comments"/></td>
		</tr> 
		<logic:iterate id = "comment" name = "allComment" scope = "request">
			<tr height="20">
				<td  class = "colLight"><bean:write name = "comment" property = "commentdate"/></td>	
				<td  class = "colLight"><bean:write name = "comment" property = "commentby"/></td>				  
				<td  colspan = "4" class = "colLight"><bean:write name = "comment" property = "comment"/></td>
	 		</tr>
	 	</logic:iterate>
	  <%
		  }
	  else {
	  %> 	
	  		 <tr> 
			    <td  class="colLight" colspan="6"><font class="Nmessage">&nbsp;No Comments.</font></td>
			 </tr>
	  <%} %>
	 <tr> 
	    <td colspan = "3" class="colDark">Created</td>
		<td colspan = "3" class="colLight"><bean:write name="MSAEditForm" property="createInfo" /></td>
	 </tr>
	 <tr>
		<td colspan = "3" class="colDark">Updated</td>
		<td colspan = "3" class="colLight"><bean:write name="MSAEditForm" property="changeInfo" /></td>
     </tr>       
   	<td class = "colLight" colspan="6" align="center">
		<html:submit property = "save" styleClass = "button_c" onclick = "return validate();">Submit</html:submit>&nbsp;
		<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>&nbsp;
		<html:button property = "back" styleClass = "button_c" onclick = "Backaction();">Back</html:button>
	</td>
	</table></td></tr>
	</table>
</html:form>
</TABLE>

</BODY>

<script>

function trimFields() 
{
	var field=document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text') 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}

function Backaction()
{
	history.go(-1);
}

function validate()
{	
	trimFields();

 		if( document.forms[0].orgTopId.value == "0" )
 		{
 			alert( "<bean:message bundle = "pm" key = "msa.tabular.msaname"/>" );
 			document.forms[0].orgTopId.focus();
 			return false;
 		}
 		
 		if( document.forms[0].schDays.value == "" )
  		{
  			document.forms[0].schDays.value = "0";
  		}
 		
 		if( !isInteger( document.forms[0].schDays.value ) )
 		{
 			alert( "<bean:message bundle = "pm" key = "msa.tabular.numericenter"/>" );  
 			document.forms[0].schDays.value = "";
 			document.forms[0].schDays.focus();
 			return false;
 		}
 
 		
 		if( document.forms[0].unionUpliftFactor.value == "" )
	  	{
	  		alert( "<bean:message bundle = "pm" key = "msa.tabular.unionupliftfactorenter"/>" );
	  		document.forms[0].unionUpliftFactor.focus();
	  		return false;
	  	}
	 
		if( !isFloat( document.forms[0].unionUpliftFactor.value ) )
		{
		 	alert( "<bean:message bundle = "pm" key = "msa.tabular.validunionupliftfactor"/>" );
		 	document.forms[0].unionUpliftFactor.value = "";
		 	document.forms[0].unionUpliftFactor.focus();
			return false;
		}
		
		if(document.forms[0].reqPlannedSch.value!="" && document.forms[0].effPlannedSch.value!="") 
		{
			if (!compDate_mdy(document.forms[0].reqPlannedSch.value, document.forms[0].effPlannedSch.value)) 
			{
				document.forms[0].effPlannedSch.focus();
				alert("<bean:message bundle = "pm" key = "msa.tabular.datevalidation"/>");
				return false;
			} 
		}
 	return true;
}
function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filter").style.visibility="visible";
<%}%>

}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;
		
}
function show()
{
	
	document.forms[0].go.value="true";
	document.forms[0].action = "MSAEditAction.do?clickShow=true&msaId=<bean:write name='MSAEditForm' property ='msaId'/>";
	document.forms[0].submit();
	return true;
}

function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="MSAEditAction.do?msaId=<bean:write name = 'MSAEditForm' property = 'msaId' />&firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{
	if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				document.forms[0].selectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				if(document.forms[0].selectedOwners[j].checked) {
				 	document.forms[0].selectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else {
	
	for(i=0;i<document.forms[0].selectedOwners.length;i++)
	{
			if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
			{
				document.forms[0].otherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="MSAEditAction.do?msaId=<bean:write name = 'MSAEditForm' property = 'msaId' />&opendiv=0";
 				document.forms[0].submit();
			}
	}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="MSAEditAction.do?msaId=<bean:write name = 'MSAEditForm' property = 'msaId' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="MSAEditAction.do?msaId=<bean:write name = 'MSAEditForm' property = 'msaId' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
</script>
</html:html>
