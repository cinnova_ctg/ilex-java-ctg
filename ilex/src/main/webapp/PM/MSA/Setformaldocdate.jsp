<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>



<%
int addRow = -1;
boolean addSpace = false;
int rowHeight = 65;
int ownerListSize = 0;
int j=2;
String checkowner=null;

if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString()); 



String MSA_Id ="";
String msastatus="";
String uploadstatus ="";
String status ="";

if(request.getAttribute("status")!=null){
	status = request.getAttribute("status").toString();
	msastatus = request.getAttribute("status").toString();
}
if(request.getAttribute("MSA_Id")!=null)
	MSA_Id= request.getAttribute("MSA_Id").toString();

if(request.getAttribute("uploadstatus")!=null)
	uploadstatus= request.getAttribute("uploadstatus").toString();
	
	

%>
<script>
function validate()
{
	document.forms[0].authenticate.value = "success";
	if(document.forms[0].formaldocdate.value == "")
	{
		alert( "<bean:message bundle = "pm" key = "msa.formaldocumentpage.selectdate"/>" );
		return false;
	}
	return true;
}
function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
	/*  */
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%=status%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
}		
</script>

<html:html>
<head>
	<%@ include  file="/Header.inc" %>
	<title>SET FORMAL DOCUMENT DATE</title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language="JavaScript" src="javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css"> 
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<%@ include  file="/NMenu.inc" %>



</head>
<%-- <%@ include  file="/MSAMenu.inc" %> --%>
<%@ include  file="/AppendixMenu.inc" %>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad ="leftAdjLayers();OnLoad();">
<html:form action = "/FormalDocDateAction" target = "ilexmain">
<html:hidden property = "id" />
<html:hidden property = "authenticate" />
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<html:hidden property="msaId"/> 
<html:hidden property="appendixId"/>
<html:hidden property = "type" />
<html:hidden property = "name" />
<html:hidden property="appendixOtherCheck"/> 

<logic:equal name = "FormalDocDateForm" property = "type" value = "MSA">								
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        	 <%--  <%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )){%>
								<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
						  <%}else{%>
	                 			 <td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
							<%}%>
							<td id = "pop2"  width = "120"  class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
						<%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>		
					</tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
					    </td>
	 				</tr>
	 				<tr> 
    					<td colspan = "7"><h2>MSA&nbsp;<bean:message bundle = "pm" key = "msa.formaldocumentpage.new.header"/> <bean:write name = "FormalDocDateForm" property = "name"/></h2></td>
 		  			</tr> 
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "selectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="FormalDocDateForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="FormalDocDateForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="FormalDocDateForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "FormalDocDateForm" property = "type" value = "Appendix">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			      <%--  <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>	
					<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td> --%>
					<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
					<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>	
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="FormalDocDateForm" property ="msaId"/>&firstCall=true"><bean:write name  ="FormalDocDateForm" property ="msaName"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7"><h2>Appendix&nbsp;<bean:message bundle = "pm" key = "msa.formaldocumentpage.new.header"/> <bean:write name = "FormalDocDateForm" property = "name"/></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="FormalDocDateForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="FormalDocDateForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="FormalDocDateForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>">
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "FormalDocDateForm" property = "type" value = "MSA">
<%@ include  file="/MSAMenuScript.inc" %>
</logic:equal>
<logic:equal name = "FormalDocDateForm" property = "type" value = "Appendix">
<%@ include  file="/AppendixMenuScript.inc" %>
</logic:equal>

<table width = "100%" border = "0" cellspacing="1" cellpadding = "1" align = "center" valign  = "top">
	  	 <tr>
  			<td width = "2" height = "0"></td>
  			 <td>
  			 <logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present> 	
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
  			 <table border = "0" cellspacing = "1" cellpadding = "1" width = "500"> 
 		  <tr>
 		  	<td class = "colDark"><bean:message bundle = "pm" key = "msa.formaldocumentpage.date"/></td> 
 		  	<td class = "colLight">
 		  		<html:text property = "formaldocdate" size = "10" readonly = "true" styleClass = "text" />
 		  		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].formaldocdate , document.forms[0].formaldocdate , 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
 		  	</td>
 		  </tr>
 		  
 		  <tr>
 		  	<td colspan = "2" class = "colLight" align="center"><html:submit property = "setdate" styleClass = "button_c" onclick = "javascript:window.close();return validate();"><bean:message bundle = "pm" key = "msa.formaldocumentpage.set"/></html:submit>
 		  </tr>
 		 </table>
			</td>
		</tr>
	</table>
</html:form>
</table>
</body>
<script>
function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
	document.getElementById("filter").style.visibility="visible";
<%}%>

}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
		
}
function show()
{
	document.forms[0].go.value="true";
	if(document.forms[0].type.value=='MSA')
			document.forms[0].action = "FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&Type=MSA&clickShow=true";
	
	if(document.forms[0].type.value=='Appendix')
			document.forms[0].action = "FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&Appendix_Id=<bean:write name = 'FormalDocDateForm' property = 'appendixId' />&Type=Appendix&clickShow=true";
			
	document.forms[0].submit();
	return true;
}

function initialState()
{
	document.forms[0].go.value="";
	if(document.forms[0].type.value=='MSA')	
		document.forms[0].action="FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&Type=MSA&firstCall=true&home=home&clickShow=true";
	
	if(document.forms[0].type.value=='Appendix')	
		document.forms[0].action="FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&Appendix_Id=<bean:write name = 'FormalDocDateForm' property = 'appendixId' />&Type=Appendix&firstCall=true&home=home&clickShow=true";
		
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{

	if(document.forms[0].type.value=='MSA'){
		if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
		{
			if(checkboxvalue == '%'){
				for(j=2;j<document.forms[0].selectedOwners.length;j++)
				{
					document.forms[0].selectedOwners[j].checked=false;
				}
			}
			else
			{
				for(j=2;j<document.forms[0].selectedOwners.length;j++)
				{
					if(document.forms[0].selectedOwners[j].checked) {
					 	document.forms[0].selectedOwners[1].checked = false;
					 	break;
					}
				}		
			}		
		}
		else
		{
				for(i=0;i<document.forms[0].selectedOwners.length;i++)
				{
						if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
						{
							document.forms[0].otherCheck.value = 'otherOwnerSelected';
							document.forms[0].action="FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&opendiv=0&Type=MSA";
			 				document.forms[0].submit();
						}
				}
			}
		}	
	
	if(document.forms[0].type.value=='Appendix'){
		if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
		{
			if(checkboxvalue == '%'){
				for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
				{
					document.forms[0].appendixSelectedOwners[j].checked=false;
				}
			}
			else
			{
				for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
				{
					if(document.forms[0].appendixSelectedOwners[j].checked) {
					 	document.forms[0].appendixSelectedOwners[1].checked = false;
					 	break;
					}
				}		
			}		
		}
		else
		{
			for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
			{
					if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
					{
						document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&opendiv=0&Appendix_Id=<bean:write name = 'FormalDocDateForm' property = 'appendixId' />&Type=Appendix";
		 				document.forms[0].submit();
		
					}
			}
	}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&month=0&clickShow=true&Type=MSA";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&week=0&clickShow=true&Type=MSA";
document.forms[0].submit();
return true;
}

function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&Appendix_Id=<bean:write name = 'FormalDocDateForm' property = 'appendixId' />&month=0&clickShow=true&Type=Appendix";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="FormalDocDateAction.do?Id=<bean:write name = 'FormalDocDateForm' property = 'id' />&Appendix_Id=<bean:write name = 'FormalDocDateForm' property = 'appendixId' />&week=0&clickShow=true&Type=Appendix";
document.forms[0].submit();
return true;
}












</script>
</html:html>