<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating and  updating  MSAs.
*
-->
<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<bean:define id = "codes" name = "codeslist" scope = "request"/>
<% 
int addRow = -1;
int j=2;
boolean addSpace = false;
int rowHeight = 65;
int msa_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 

int ownerListSize = 0;

if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());
	
String editMSAFlag = null;
String editAction = null;

if(request.getAttribute("editAction") != null)
	editAction = request.getAttribute("editAction")+"";

if(request.getAttribute("editMSAFlag") != null)
	editMSAFlag = request.getAttribute("editMSAFlag")+"";

rowHeight += ownerListSize*18;
String checkowner = null;
%><head>

	<title><bean:message bundle = "pm" key = "msa.tabular.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 

	<script language = "JavaScript" src = "javascript/functions.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>  
	
</head>
<html:html>

<%
	String backgroundclass = null;
	String backgroundhyperlinkclass = null;
	String backgroundclasstoleftalign=null;
	boolean csschooser = true;
	
	int i = 0;
%>

<body class="body" onLoad="leftAdjLayers();OnLoad();">
<html:form action = "/MSAUpdateAction">
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td  id="pop1" width="100%" class="headerrow" height="19" >&nbsp;</td></tr>
		         <tr><td background="images/content_head_04.jpg" height="21" width ="100%">&nbsp;</td></tr>
		        <tr><td colspan="6"><h2><bean:message bundle = "pm" key = "msa.tabular.header"/></h2></td></tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" colspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                      <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
	                			<div id="featured">
										<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
								</div>								
	                </div>		
				<span>
				<div id="filter" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
							    		<logic:present name ="statuslist" scope="request">
									    	<logic:iterate id = "list" name = "statuslist"> 
										 		<html:multibox property = "selectedStatus"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											</logic:iterate>
										</logic:present>
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="MSAListForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="MSAListForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="MSAListForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist">
							    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<% 

										checkowner = "javascript: checking('"+ownerId+"');";
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                         
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
                              
                            </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
 <table> 
 <tr>
  <td colspan="3"><table border = "0" cellspacing = "1" cellpadding = "1" >
  
  		<logic:present name = "clickShow" scope = "request">
	  		<logic:notEqual name = "clickShow" value = "">
				<script>
					parent.ilexleft.location.reload();
				</script>	
			</logic:notEqual>
		</logic:present>
		
		<logic:present name = "specialCase" scope = "request">
	  		<logic:notEqual name = "specialCase" value = "">
				<script>
					parent.ilexleft.location.reload();
				</script>	
			</logic:notEqual>
		</logic:present>

  			<logic:present name = "deleteflag" scope = "request">
	    		<tr> 
				<td>&nbsp;</td>
				<td colspan = "5" class = "message" height = "30">
	    			<logic:equal name = "deleteflag" value = "0">
	    				<bean:message bundle = "pm" key = "msa.tabular.delete.success"/>		
	    				<script>
							parent.ilexleft.location.reload();
						</script>	
					</logic:equal>
	
					<logic:equal name = "deleteflag" value = "-9001">
						<bean:message bundle = "pm" key = "msa.tabular.delete.failure1"/>	
					</logic:equal>
	
					<logic:equal name = "deleteflag" value = "-9002">
						<bean:message bundle = "pm" key = "msa.tabular.delete.failure2"/>	
					</logic:equal>
				</td>
				</tr>
	
</logic:present>
	 <logic:present name = "editMSAFlag" scope = "request">	
	   <tr><td>&nbsp;</td>
	  	 <td colspan = "2" class = "message" height = "30">
	     	<logic:equal name = "editMSAFlag" value = "0">
	     		<logic:equal name = "editAction" value = "A">
			 		<script>
						parent.ilexleft.location.reload();
					</script>
			 		MSA added successfully
			 	</logic:equal>
			
			 	<logic:notEqual name = "editAction" value = "A">
			     	MSA updated successfully
			 	</logic:notEqual>
		 	</logic:equal>
		
		 	<logic:notEqual name = "editMSAFlag" value = "0">
			 	<logic:equal name = "editAction" value = "A">
			 		MSA add failure
			 	</logic:equal>
			
			 	<logic:notEqual name = "editAction" value = "A">
			     	MSA update failure
			 	</logic:notEqual>
			</logic:notEqual>
	    </td>
	 </tr>
	</logic:present> 
    <c:if test="${! empty  requestScope.sourceNotValid}">
		<tr><td>&nbsp;</td>
			<TD colspan = "2" class="message" height=5 />
				<c:out value="${requestScope.sourceNotValid}"/>
			</TD>
		</tr>
	</c:if>
  <% if( msa_size == 0 ){ %>  
  <logic:present name ="roleExists" scope ="request">
  	<logic:equal name = "roleExists" value = "Y" >
   
    <tr> 
    <td rowspan = "2">&nbsp;</td>
    <td class = "Ntryb" rowspan = "2" colspan="2">
    <table width="100%"border="0">
    <tr>
    	<td class = "Ntryb" align="center"  width="100%">
    		<bean:message bundle = "pm" key = "msa.tabular.name"/></b>
    	</td>
    	<td class = "Ntryb" align="left">
    		<table width="100%"border="0"><tr><td  align="right"><html:button styleClass = "Nbutton" property ="Add" onclick ="return addMSA();">Add</html:button></td></tr></table>
    	</td>
    </tr>
    </table>
    </td>
	<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.type"/></td>
	<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.salestype"/></td>
    <td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.bdm"/></td>  
    <td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.unionupliftfactor"/></td>
    <td class = "Ntryb" colspan = "2"><bean:message bundle = "pm" key = "msa.tabular.payment"/></td>
    <td class = "Ntryb" colspan = "2"><bean:message bundle = "pm" key = "msa.tabular.plannedimplementation"/></td>
	<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.status"/></td>
  </tr>
   <tr> 
    <td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "msa.tabular.payment.term"/></div></td>
    <td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "msa.tabular.payment.qualifier"/></div></td>
    <td class = "Ntryb" title = "<bean:message bundle = "pm" key = "msa.tabular.requiredtooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "msa.tabular.plannedscheduled.delievery"/></div></td>
    <td class = "Ntryb" title = "<bean:message bundle = "pm" key = "msa.tabular.effectivetooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "msa.tabular.plannedscheduled.effective"/></div></td>
   </tr>
   	</logic:equal>	
   </logic:present>
  <%}%>
    
   <% if( msa_size > 0 ){ %>  
  <tr> 
    <td rowspan = "2">&nbsp;</td>
    <td class = "Ntryb" rowspan = "2" colspan="2">
    <table width="100%"border="0">
    <tr>
    	<td class = "Ntryb" align="center"  width="100%">
    		<bean:message bundle = "pm" key = "msa.tabular.name"/></b>
    	</td>
    	<td class = "Ntryb" align="left">
    		<table width="100%"border="0"><tr><td  align="right"><html:button styleClass = "Nbutton" property ="Add" onclick ="return addMSA();">Add</html:button></td></tr></table>
    	</td>
    </tr>
    </table>
    </td>
	<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.type"/></td>
	<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.salestype"/></td>
    <td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.bdm"/></td>  
    <td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.unionupliftfactor"/></td>
    <td class = "Ntryb" colspan = "2"><bean:message bundle = "pm" key = "msa.tabular.payment"/></td>
    <td class = "Ntryb" colspan = "2"><bean:message bundle = "pm" key = "msa.tabular.plannedimplementation"/></td>
	<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "msa.tabular.status"/></td>
  </tr>
   <tr> 
    <td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "msa.tabular.payment.term"/></div></td>
    <td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "msa.tabular.payment.qualifier"/></div></td>
    <td class = "Ntryb" title = "<bean:message bundle = "pm" key = "msa.tabular.requiredtooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "msa.tabular.plannedscheduled.delievery"/></div></td>
    <td class = "Ntryb" title = "<bean:message bundle = "pm" key = "msa.tabular.effectivetooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "msa.tabular.plannedscheduled.effective"/></div></td>
   </tr>
 <logic:present name = "codes" property = "msalist" > 
 	<logic:iterate id = "ms" name = "codes" property = "msalist">
 	
	 	<bean:define id = "msaStatus" name = "ms" property = "status" type = "java.lang.String"/>
		<bean:define id = "uploadStatus" name = "ms" property = "file_upload_check" type = "java.lang.String"/>
		
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass = "Ntexto";
				backgroundclasstoleftalign ="Ntextoleftalignnowrap";
				backgroundhyperlinkclass = "Nhyperodd";
				csschooser = false;
			}
	
			else
			{
				csschooser = true;	
				backgroundclass = "Ntexte";
				backgroundclasstoleftalign = "Ntexteleftalignnowrap";
				backgroundhyperlinkclass = "Nhypereven";
			}
		%>
		<tr>		
			<td class = "Nlabeleboldwhite">&nbsp;</td>
			<html:hidden  name = "ms" property = "msa_Id"/>
			
			<td class = "<%= backgroundhyperlinkclass %>"> 
			<bean:write name = "ms" property = "name" />
			</td>
			<td class = "<%= backgroundhyperlinkclass %>" align="right"> 
			<%if( !msaStatus.equals( "Draft" ) && !msaStatus.equals( "Review" )){%>
				[<a href = "MSADetailAction.do?MSA_Id=<bean:write name = "ms" property = "msa_Id" />&Prjid=<bean:write name = "ms" property = "projectid"/>&Orgid=<bean:write name = "ms" property = "orgid"/>&Name=<bean:write name = "ms" property = "name"/>">Detail</a>&nbsp;|&nbsp;Edit]
			<% } else { %>
				[<a href = "MSADetailAction.do?MSA_Id=<bean:write name = "ms" property = "msa_Id" />&Prjid=<bean:write name = "ms" property = "projectid"/>&Orgid=<bean:write name = "ms" property = "orgid"/>&Name=<bean:write name = "ms" property = "name"/>">Detail</a>&nbsp;|&nbsp;<a href ="MSAEditAction.do?msaId=<bean:write name = "ms" property = "msa_Id" />">Edit</a>]					
			<% } %>
			</td>
			<html:hidden name = "ms" property = "projectid"/>
			<td  class = "<%= backgroundclasstoleftalign %>" ><bean:write name = "ms" property = "msatype" /></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "salestype" /></td>
	 		<td class = "<%= backgroundclasstoleftalign %>"><bean:write name = "ms" property = "salesPOC" /></td>     
			<td class = "<%= backgroundclass %>" align="center" ><bean:write name = "ms" property = "unionupliftfactor" /></td>
			<td class = "<%= backgroundclass %>" ><bean:write name = "ms" property = "payterms" /></td>
		    <td class = "<%= backgroundclasstoleftalign %>" ><bean:write name = "ms" property = "payqualifier" /></td>
			<td class = "<%= backgroundclass %>" ><bean:write name = "ms" property = "reqplasch" /></td>
			<td class = "<%= backgroundclass %>" ><bean:write name = "ms" property = "effplasch" /></td>
			<td class = "<%= backgroundclass %>" ><bean:write name = "ms" property = "status"/></td>
  		</tr>
  		
	   <% i++;  %>
	  </logic:iterate>
  </logic:present>
  
	<tr> 
    	<td ><img src = "images/spacer.gif" width = "1" height = "1"></td>
		<td colspan = "12" class = "Nbuttonrow">
			<html:button property = "sort" styleClass = "Nbutton" onclick = "return sortwindow();"><bean:message bundle = "pm" key = "msa.tabular.sort"/></html:button>
		</td>
	</tr>

<% } else { %>
				<tr>
						<td width ="2">&nbsp;</td>
		    			<td  colspan = "8" class = "message" height = "30">
		    				<bean:message bundle = "pm" key = "msa.not.present"/>
		    			</td>
		    	</tr>
<% } %>
 </table>
</td>
</tr>
<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'msatabularpage'/>
</jsp:include>	
</table>
</html:form>
</body>

<script>
function addMSA()
{
document.forms[0].go.value="";
document.forms[0].action="MSAEditAction.do?msaId=0";
document.forms[0].submit();
return true;
}

function month_MSA()
{
document.forms[0].action="MSAUpdateAction.do?month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].action="MSAUpdateAction.do?week=0&clickShow=true";
document.forms[0].submit();
return true;
}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function checking(checkboxvalue)
{
	if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				document.forms[0].selectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				if(document.forms[0].selectedOwners[j].checked) {
				 	document.forms[0].selectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{
		for(i=0;i<document.forms[0].selectedOwners.length;i++)
		{
			if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
			{
				document.forms[0].otherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="MSAUpdateAction.do?opendiv=0";
 				document.forms[0].submit();
			}
		}
	}
}

function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filter").style.visibility="visible";
<%}%>

}
function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;
		
}

function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="MSAUpdateAction.do?clickShow=true";
	document.forms[0].submit();
	return true;
}

function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="MSAUpdateAction.do?firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}	

var str = '';
function sortwindow()
{
	str = 'SortAction.do?Type=MSA';
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */

}
</script>
</html:html>
  
