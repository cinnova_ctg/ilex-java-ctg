<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<bean:define id = "codes" name = "codeslist" scope = "request"/>

<%
int allCommentSize = 0;
int addRow = -1;
boolean addSpace = false;
int rowHeight = 65;
int ownerListSize = 0;
int j=2;
String checkowner=null;


if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());


if(request.getAttribute("allCommentSize") != null)
	allCommentSize = Integer.parseInt(request.getAttribute("allCommentSize")+"");

rowHeight += ownerListSize*18;

%>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">

<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<title>MSA Edit Page</title>
<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
</style>

<script language="JavaScript" src="javascript/date-picker.js"></script>

<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>

	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
    <script language = "JavaScript" src = "javascript/popcalendar.js"></script>
    <script language = "JavaScript" src = "javascript/functions.js"></script>
	<%@ include  file="/NMenu.inc" %>
	<link rel="stylesheet" href="styles/style.css" type="text/css">
</head>
<%@ include  file="/MSAMenu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF"  onLoad ="leftAdjLayers();OnLoad();">
<html:form action = "/EditBdmAction">
<html:hidden property="msaId" />
<html:hidden property="actionAddUpdate" />
<html:hidden property="fromType"/>
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
							<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>


<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
		        	 <logic:equal name="MSAEditForm" property="msaId" value="0">
		        	 		<td class = "Ntoprow1" width="100%" colspan="6">&nbsp;</td>
		        	</logic:equal>
		        	<logic:notEqual name="MSAEditForm" property="msaId" value="0">
		 
		 <!--  New Style Menu  -->
<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>		 
	
		 
<div id="menunav">
	<ul>	 
<% if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) || uploadstatus.equals( "U" ) )  
							{boolean check = false;
								if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) ) 
								{check = true;
									if( msastatus.equals( "Signed" ) )
									{
								%>
									<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')">Edit</a></li>
								<%}else{
								%>
									<%-- <li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')">Edit</a></li> --%>
								<%}}if( uploadstatus.equals( "U" ) &&  !check )
								{%>
									<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegeupload"/>')"><bean:message bundle = "pm" key = "msa.detail.edit"/></a></li>
								<%}
							} 
							else 
							{ %>
	                 			 <li><a class="drop" href="MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail"><bean:message bundle = "pm" key = "msa.detail.edit"/>Edit</a></li>
							<% 
							} %>
							<%-- <td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="8" width="450" >&nbsp;</td> --%>
							
	
		
		<li>
			<a href="#" class="drop"><span>Manage</span></a>
			<ul>
				<li>
					<%
					if((String) request.getAttribute("list") != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/></span></a>
						<ul>
						
						
						
							<%= (String)request.getAttribute("list") %>
												
						</ul>
						<%
					} else {
						%>
						<a href="#"><bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/></a>
						<%
					}
					%>
				</li>
				<li>
					<a href="#">View MSA</span></a>
					<ul>
						<li><a href="MSADetailAction.do?MSA_Id=<%= MSA_Id %>&Prjid=<%= MSA_Id %>">Details</a></li>
						<%
						if( msastatus.equals( "Signed" ) || msastatus.equals( "Approved" ) || msastatus.equals( "Pending Customer Review" )) {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/></a></li>
							<li><a href="javascript:view('rtf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewrtf"/></a></li>
						     <%
							if(request.getAttribute("new_addendumlist") != "" ) {
								%>
								<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "msa.detail.menu.viewhtml"/></a></li>
								<%
								/* if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) { */
								
								
									
								
// 								} 
								
					//			else {
									%>
									
									<%
					//			}
							} 
						}
						%> 
					</ul>
				</li>
				<%
				if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )) {
					%>
					<li><a href="javascript:sendemail();"><bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/></a></li>
					
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.sendMSA.nopriviledgestatus"/>');"><bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/></a></li>
					<%
				}
				if( msastatus.equals( "Signed" ) || msastatus.equals( "Inwork" ) || msastatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.formaldocdate"/></a></li>
					
					<%
				}  else {
					%>
					<li><a href="EditBdmAction.do?msaId=<%= MSA_Id %>&fromType=MSADetail">Edit BDM</a></li>
					
					<%
				} %>
				<% 
				if( msastatus.equals( "Signed" ) || msastatus.equals( "Inwork" ) || msastatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus.cancelled"/></a></li>
					
					<%
				} else {
					%>
					<li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/></a></li>
					
					<%
				}
				if(msastatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				} 
				%>
				<li>
					<a href="#"><span>MSA Comments</span></a>
					<ul>
						<li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "msa.detail.menu.viewall"/></a></li>
						<%
						if(msastatus.equals( "Cancelled" )) {
							%>
							<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/></a></li>
							<%
						}
						%>
					</ul>
				</li>
				<%
				if( msastatus.equals( "Pending Customer Review" )) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%= MSA_Id %>&entityType=MSA"><bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.upload.state"/>');"><bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/></a></li>
					<%
				}
				if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )) {
					if(msastatus.equals( "Cancelled" )) {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>
						<%
					} else {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>
						<%
					}
				} else {
					%>
					<li><a href="javascript:editmsa();"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>	
					<%
				}
				%>
				
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "msa.detail.menu.documents"/></span></a>
					<ul>
						<li><a href="EntityHistory.do?entityId=<%= MSA_Id %>&entityType=MSA&function=viewHistory&msastatus=<%=msastatus%>">MSA  History</a></li>
						<li>
							<a href="#"><span>Support Documents</span></a>
							<ul>
								<li><a href="EntityHistory.do?entityId=<%= MSA_Id %>&msaId=<%= MSA_Id %>&entityType=MSA Supporting Documents&function=addSupplement&linkLibName=MSA">Upload</a></li>
								<li><a href="EntityHistory.do?entityType=MSA Supporting Documents&msaId=<%= MSA_Id %>&linkLibName=MSA&function=supplementHistory">History</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="javascript:del();"><bean:message bundle = "pm" key = "msa.detail.menu.deletemsa"/></a></li>
			</ul>
		</li>
		<li><a class="drop" href="AppendixUpdateAction.do??firstCall=true&MSA_Id=<%= MSA_Id%>">Appendices</a></li>
	</ul>
</div>
	<!-- End of Style -->	
							
							</logic:notEqual>
				</tr>
		        <tr>
				          
				          <logic:equal name="MSAEditForm" property="msaId" value="0">
				          <td background="images/content_head_04.jpg" height="21" colspan="2">
							<div id="breadCrumb"><a href="MSAUpdateAction.do" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><span class="currentSelected"></span></div>
						 </td>	
						 <td background="images/content_head_04.jpg" height="21" colspan="3">
							
							</td>
						</logic:equal>
						<logic:notEqual name="MSAEditForm" property="msaId" value="0">
							 <td background="images/content_head_04.jpg" height="21" colspan="3">
							<div id="breadCrumb"><a href="MSAUpdateAction.do" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
							</td>
							<td background="images/content_head_04.jpg" height="21" colspan="8">
							&nbsp;
							</td>
						</logic:notEqual>
				          
 				 </tr>
 				 <tr>
 				 	<td colspan = "6"><h2>
					<logic:equal name="MSAEditForm" property="msaId" value="0">MSA Add
					</logic:equal>
					<logic:notEqual name="MSAEditForm" property="msaId" value="0">
						MSA:&nbsp;<bean:write name = "MSAEditForm" property = "msaName"/>&nbsp;-&nbsp;Reassign BDM
					</logic:notEqual></h2>
					</td>
 				 </tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" colspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
	                			<div id="featured">
										<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
								</div>								
	                </div>		
				<span>
				<div id="filter" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="statuslist" scope="request">
									    	<logic:iterate id = "list" name = "statuslist"> 
										 		<html:multibox property = "selectedStatus"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											</logic:iterate>
										</logic:present>
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="MSAEditForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="MSAEditForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="MSAEditForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist">
							    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<% 
										checkowner = "javascript: checking('"+ownerId+"');";
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>">
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                         
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
                              
                            </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
<TABLE>
<logic:notEqual name="MSAEditForm" property="msaId" value="0">
<%@ include  file="/MSAMenuScript.inc" %>
</logic:notEqual>
<table border="0" cellspacing="1" cellpadding="1" >
<tr>
<td width="1" height="0"></td>
							
<td>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
 <tr>
  <td>
  	<table border="0" cellspacing="1" cellpadding="1" width="500">
	   
	  <tr>
	    				<td class = "colDark" height = "20">Current BDM:</td>
	    				<td  class = "colLight" height = "20"><bean:write name = "MSAEditForm" property = "curBdm"/></td>
	   </tr>
	  <tr> 
	    <td   class="colDark" height = "20">New BDM:</td>
	    <td   class="colLight" height = "20">
			 <html:select property = "salesPOC" size = "1" styleClass = "select">
			   		<html:optionsCollection name = "codes" property = "salesPOCcode" value = "value" label = "label"/> 
			 </html:select>
		</td>
	  </tr>
  	<td class = "colLight" colspan="6" align="center">
		<html:submit property = "save" styleClass = "button_c" onclick = "return validate();">Save</html:submit>&nbsp;
		<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>&nbsp;
	</td>
	</table></td></tr>
	</table>
</html:form>
</TABLE>

</BODY>

<script>

function validate()
{
return true;
}
function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filter").style.visibility="visible";
<%}%>

}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;
		
}
function show()
{
	
	document.forms[0].go.value="true";
	document.forms[0].action = "MSAEditAction.do?clickShow=true&msaId=<bean:write name='MSAEditForm' property ='msaId'/>";
	document.forms[0].submit();
	return true;
}

function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="MSAEditAction.do?msaId=<bean:write name = 'MSAEditForm' property = 'msaId' />&firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{
	if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				document.forms[0].selectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				if(document.forms[0].selectedOwners[j].checked) {
				 	document.forms[0].selectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else {
	
	for(i=0;i<document.forms[0].selectedOwners.length;i++)
	{
			if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
			{
				document.forms[0].otherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="MSAEditAction.do?msaId=<bean:write name = 'MSAEditForm' property = 'msaId' />&opendiv=0";
 				document.forms[0].submit();
			}
	}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="MSAEditAction.do?msaId=<bean:write name = 'MSAEditForm' property = 'msaId' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="MSAEditAction.do?msaId=<bean:write name = 'MSAEditForm' property = 'msaId' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
</script>


</html:html>
