<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>
<%@ page import=" com.mind.common.IlexAction" %>
<%@ page import=" com.mind.xml.EditXmldao" %>
<%@ page import="com.mind.ilex.xml.doc.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.mind.common.LabelValue" %>

<%List<com.mind.ilex.xml.doc.Section> sect = new ArrayList();
List paragraphtitle = new java.util.ArrayList();
List paragraphtitleLabel = new java.util.ArrayList();
String temp_title = "";
String temp_number = "";
String sectionid = "";
String prevsectionid = "";	
com.mind.ilex.xml.doc.Section section=null;
						  		
								%>
<%
int addRow = -1;

boolean addSpace = false;
int rowHeight = 65;
int ownerListSize = 0;

String  checkowner=null;
String file_name =(String) request.getAttribute("file_name");
IlexAction act=new IlexAction(); 
String xmlFile = EditXmldao.getXmlFile(file_name,act.getDataSource(request, "ilexnewDB"));

if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());
rowHeight += ownerListSize*18;

String MSA_Id ="";
String msastatus="";
String uploadstatus ="";
String status ="";

if(request.getAttribute("status")!=null){
	status = request.getAttribute("status").toString();
	msastatus = request.getAttribute("status").toString();
}
if(request.getAttribute("MSA_Id")!=null)
	MSA_Id= request.getAttribute("MSA_Id").toString();

if(request.getAttribute("uploadstatus")!=null)
	uploadstatus= request.getAttribute("uploadstatus").toString();




int list_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString());
int i = 0;

String Type =(String) request.getAttribute("Type");
String EditType =(String) request.getAttribute("EditType");
String key_value = "";
String show_type = "";

/* set page header */
if(Type.equals("MSA")) key_value = "msa.editxml.title";
if(Type.equals("NetMedX") || Type.equals("NewNetMedX")) {
	key_value = "netmedx.editxml.title";
	if(EditType.equals("PM")) show_type = "Appendix";
	else show_type = Type;
}
else show_type = Type;
if(Type.equals("Appendix")) key_value = "appendix.editxml.title";
if(Type.equals("Hardware")) key_value = "appendix.editxml.title";
%>
<html:html>
<head>
	<title><bean:message bundle = "pm" key = "<%= key_value%>"/></title>
	<%@ include file = "/Header.inc" %>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css" />
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css">  
	<script language = "JavaScript" src = "javascript/functions.js"></script>
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script> 
	<script language = "JavaScript" src = "javascript/JQueryMain.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.inithide').hide();
		$('.paragraph1').show();
		$('#Label1').show();
		
		
		});
	function getData(name,label){
		 var str = "";
		$('.inithide').hide();
		$('.paragraph'+name).show();
		$('#Label'+name).show();
	}
	
	</script> 
	<script type="text/javascript" language="javascript">
		
	</script>
<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>		 
		
	
	<%@ include file = "/NMenu.inc" %>  
</head>
<%-- <%@ include  file="/MSAMenu.inc" %>   --%>
<%@ include  file="/AppendixMenu.inc" %>  
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad ="leftAdjLayers();OnLoad();">
<nested:form action = "/EditXmlAction" method="post">
<html:hidden  property="refresh"/>
<html:hidden property ="id"/>
<html:hidden  property="xmlfile"/>
<html:hidden  property="typeid"/>
<html:hidden  property="type"/>
<html:hidden  property="edittype"/>
<html:hidden property="MSA_Id"/> 
<html:hidden property="msaname"/>
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<html:hidden property="appendixOtherCheck"/>
<html:hidden property="appendixId"/> 
 
<logic:equal name ="EditXmlForm" property ="edittype" value ="PM">

	<logic:equal name ="EditXmlForm" property ="type" value ="MSA">
			<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        	  <%-- <%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )){%>
								<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
							<%}else{%>
	                 			 <td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
							<%}%>
							<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
						 
					  <%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>	 
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href= "MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
						<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;MSA&nbsp;<bean:message bundle = "pm" key = "msa.editxml.new.edit"/><bean:write name ="EditXmlForm" property="name"/></h1></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "selectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>">  
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>

<logic:equal name ="EditXmlForm" property ="type" value ="Hardware">
<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>


<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			       <%-- <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>	
	
					<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td> --%>
					<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %> 
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "EditXmlForm" property = "MSA_Id"/>&firstCall=true"><bean:write name = "EditXmlForm" property = "msaname"/></a></div>				       
					    </td>
	 				</tr>
	 				<tr>
						<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;Appendix&nbsp;<bean:message bundle = "pm" key = "msa.editxml.new.edit"/>&nbsp;<bean:write name ="EditXmlForm" property="name"/></h1></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>

</logic:equal>

<logic:equal name ="EditXmlForm" property ="type" value ="Appendix">
<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        <%-- <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>	
	
					<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td> --%>
					
							<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>
					
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "EditXmlForm" property = "MSA_Id"/>&firstCall=true"><bean:write name = "EditXmlForm" property = "msaname"/></a></div>				       
					    </td>
	 				</tr>
	 				<tr>
						<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;Appendix&nbsp;<bean:message bundle = "pm" key = "msa.editxml.new.edit"/>&nbsp;<bean:write name ="EditXmlForm" property="name"/></h1></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>


</logic:equal>
<logic:equal name ="EditXmlForm" property ="type" value ="NetMedX">
<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			       <%--  <tr>
			        	<%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>
	
					<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td> --%>
			        <script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
			        <%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>	
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "EditXmlForm" property = "MSA_Id"/>&firstCall=true"><bean:write name = "EditXmlForm" property = "msaname"/></a></div>				       
					    </td>
	 				</tr>
	 				<tr>
						<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;NetMedX&nbsp;<bean:message bundle = "pm" key = "msa.editxml.new.edit"/>&nbsp;<bean:write name ="EditXmlForm" property="name"/></h1></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="EditXmlForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
</logic:equal>
						<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							
<logic:equal name ="EditXmlForm" property ="edittype" value ="PM">
	<logic:equal name ="EditXmlForm" property ="type" value ="MSA">							
			<%@ include  file="/MSAMenuScript.inc" %>
	</logic:equal>
	<logic:equal name ="EditXmlForm" property ="type" value ="Appendix">							
			<%@ include  file="/AppendixMenuScript.inc" %>
	</logic:equal>
	<logic:equal name ="EditXmlForm" property ="type" value ="Hardware">							
			<%@ include  file="/AppendixMenuScript.inc" %>
	</logic:equal>
	<logic:equal name ="EditXmlForm" property ="type" value ="NetMedX">
			<%@ include  file="/AppendixMenuScript.inc" %>
	</logic:equal>
</logic:equal>							
							
							
							
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			<td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "75%"> 	
					<logic:present name = "retval" scope = "request">
			    	<logic:equal name = "retval" value = "1">
			    		<tr>
			    			<td  colspan = "3" class = "message" height = "30">
			    				<bean:message bundle = "pm" key = "msa.editxml.success"/>
			    			</td>
			    		</tr>
			    	</logic:equal>
			    	
			    	<logic:equal name = "retval" value = "2">
			    		<tr>
			    			<td  colspan = "3" class = "message" height = "30">
			    				<bean:message bundle = "pm" key = "netmedx.editxml.success"/>
			    			</td>
			    		</tr>
			    	</logic:equal>
			    	
			    	<logic:equal name = "retval" value = "3">
			    		<tr>
			    			<td  colspan = "3" class = "message" height = "30">
			    				<bean:message bundle = "pm" key = "appendix.editxml.success"/>
			    			</td>
			    		</tr>
			    	</logic:equal>
			    	
			    	<logic:equal name = "retval" value = "4">
			    		<tr>
			    			<td  colspan = "3" class = "message" height = "30">
			    				<bean:message bundle = "pm" key = "hardware.editxml.success"/>
			    			</td>
			    		</tr>
			    	</logic:equal>
			    </logic:present>
									
					 
	
					<tr>
						<td>
							<table>
							<tr>
							<td  class = "colDark"><bean:message bundle = "pm" key = "msa.editxml.selectnewpara"/></td>
					    <td class = "colLight">
						
						  	<nested:nest property="document">
						  	<nested:nest property="sections">
 							<nested:iterate property="section" id="sectionList">
						  		<%
									section = (com.mind.ilex.xml.doc.Section) sectionList;
									sectionid = section.getParentid().toString();
									if (!prevsectionid.equals(sectionid)) {
										sect.add(section);
									}
									prevsectionid = sectionid;
								%>
								</nested:iterate>
								</nested:nest>
								<%
						  		for (int i3 = 0; i3 < sect.size(); i3++) {
									Section sec = (com.mind.ilex.xml.doc.Section) sect.get(i3);
											
									if ((!sec.getTitle().equals(""))
											|| (!sec.getNumber().equals(""))) {
										temp_title = EditXmldao.removeParameter(sec.getTitle().getContent());
										temp_number = EditXmldao.removeParameter(sec.getNumber().getContent());
										paragraphtitle.add(new LabelValue(
												temp_number + " " + temp_title, sec.getParentid()
														.toString()));
										
									}
								}
						  		%>
						  		 <select onchange="getData(this.value,this.text);">
						  		 <% 
						  		 for(int i1=0;i1<paragraphtitle.size();i1++) 
 										{ 
 										%>   
 										<option value="<%=((LabelValue)paragraphtitle.get(i1)).getValue() %>" > 
 											<%=((LabelValue)paragraphtitle.get(i1)).getLabel()%>
 										</option> 
 										<%}
										%>   
							</select>	  	
					    </td>
					    </tr>
							</table>
					
						</td>
					    
 					</tr>
 					 <tr> 
					     
					     <td colspan="2"class="formCaption">
					     	<nested:nest property="sections">
	 							<nested:iterate property="section" id="sectionList">
							  		<%
										section = (com.mind.ilex.xml.doc.Section) sectionList;
										sectionid = section.getParentid().toString();
										if (!prevsectionid.equals(sectionid)) {
											sect.add(section);
										}
										prevsectionid = sectionid;
									%>
									</nested:iterate>
								</nested:nest>
								<%
						  		for (int i3 = 0; i3 < sect.size(); i3++) {
									Section sec = (com.mind.ilex.xml.doc.Section) sect.get(i3);
											
									if ((!sec.getTitle().equals(""))
											|| (!sec.getNumber().equals(""))) {
										temp_title = sec.getTitle().getContent();
										temp_number = sec.getNumber().getContent();
										paragraphtitleLabel.add(new LabelValue(
												temp_number + " " + temp_title, sec.getParentid()
														.toString()));
										
									}
								}
						  		%>
						  		 <% 
						  		 for(int ilabel=0;ilabel<paragraphtitleLabel.size();ilabel++) 
 										{ 
 										%>   
					     					<div  class="inithide" id="Label<%=((LabelValue)paragraphtitleLabel.get(ilabel)).getValue() %>"> 
					     					<bean:message bundle = "pm" key = "msa.editxml.editpara"/>
										    			<%=((LabelValue)paragraphtitleLabel.get(ilabel)).getLabel()%>
										     </div>
 										<%}
										%>   
					     
					     
						 
						 </td>						
					  </tr>
 					
					
 					<% 	 i = 0; 
					int j = 0;
 					String label = "";
					String labelbold = "";
 					prevsectionid = "";
 					%>	
 						<tr ><td>	
 					<nested:nest property="sections">
		 					<nested:iterate property="section" >
										<div class="inithide paragraph<nested:write property='parentid'/>">
										<table>
													<% 
													i++;  
								 					if(i%2==0) { label  = "colLight"; labelbold  = "colDark"; } 
													if(i%2==1) { label  = "colLight"; labelbold  = "colDark"; } 
													%> 
													<tr>
													    <td class = "<%= labelbold %>" height="20"><bean:message bundle = "pm" key = "msa.editxml.number"/></td>
													    <td class = "<%= label %>" height="20">
								  				      		<nested:text property = "number.content" styleClass = "text"></nested:text>
													    </td>
								 					</tr>
													
													<tr>
														<td class = "<%= labelbold %>" height="20"><bean:message bundle = "pm" key = "msa.editxml.title"/></td>
														<td class = "<%= label %>" height="20">
															<nested:text property = "title.content" size="70" styleClass = "text">
								  				      		</nested:text>
														</td>
												    </tr>
									
													<% 
								 					i++;  
													if(i%2==0) { label  = "colLight"; labelbold  = "colDark"; } 
													if(i%2==1) { label  = "colLight"; labelbold  = "colDark"; } 
													%> 
																    
												    <tr>
													    <td class = "<%= labelbold %>" height="20"><bean:message bundle = "pm" key = "msa.editxml.content"/></td>
													    <td class = "<%= label %>" height="20">
															<nested:textarea property = "content.content" cols="70" rows="8" styleClass = "textarea">
								  				      		</nested:textarea>
								 						</td>
													</tr> 
						 					</table>
						 					</div>		
												
										
											 					
		 					
		 					</nested:iterate>
 					</nested:nest>
 					</nested:nest>
 							</td></tr>

 					
				    
					  <tr> 
					     <td class="colLight" align="center" colspan="2"> 
					     <html:submit property="save" styleClass="button_c" onclick = "return SaveXml();"><bean:message bundle="pm" key="msa.editxml.update"/></html:submit> 
						 <html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "msa.editxml.cancel"/></html:reset>
					     <html:button property="view" styleClass="button_c" onclick = "return View('pdf');"><bean:message bundle="pm" key="msa.editxml.view"/></html:button>
					     </td>
					  </tr>
					 
					  <jsp:include page = '/Footer.jsp'>
							<jsp:param name = 'colspan' value = '2'/>
							<jsp:param name = 'helpid' value = 'pmeditmsa'/>
					  </jsp:include>
 					
 				</table>
			</td>
		</tr>
</table>
</nested:form>
<script>

		
		
		
		
		
function View(v)
{
    window.location.href = "ViewAction.do?View="+v+"&Type=<bean:write name ='EditXmlForm' property ='type'/>&Id=<bean:write name ='EditXmlForm' property ='id'/>&from_type=<bean:write name ='EditXmlForm' property ='edittype'/>";
	return false;	
}

function SaveXml(){
document.forms[0].target = "_self";
document.forms[0].action = "EditXmlAction.do";
return true;
}




</script>
<script>
function OnLoad()
{

	<%if(request.getAttribute("opendiv") != null){%>
		document.getElementById("filter").style.visibility="visible";
	<%}%>

}
function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
	if(document.forms[0].edittype.value!="PMT"){
			var layers, leftAdj=0;
			leftAdj =document.body.clientWidth;
			leftAdj = leftAdj - 280;
			document.getElementById("filter").style.left=leftAdj;
	}		
}
function show()
{
	
	document.forms[0].go.value="true";
	
	if(document.forms[0].type.value=='MSA')
		document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&clickShow=true";
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&clickShow=true";
		
		
	document.forms[0].submit();
	return true;
}

function initialState()
{
 document.forms[0].go.value="";
	if(document.forms[0].type.value=='MSA') {
		document.forms[0].action="EditXmlAction.do?Type=MSA&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&firstCall=true&home=home&clickShow=true";
		}
		
	if(document.forms[0].type.value=='Appendix' || document.forms[0].type.value=='NetMedX' || document.forms[0].type.value=='Hardware')
		document.forms[0].action="EditXmlAction.do?Type=Appendix&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&firstCall=true&home=home&clickShow=true";
		
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{
	if(document.forms[0].type.value=='MSA'){
			if(document.forms[0].otherCheck.value == 'otherOwnerSelected') {
			
			if(checkboxvalue == '%'){
				for(j=2;j<document.forms[0].selectedOwners.length;j++)
				{
					document.forms[0].selectedOwners[j].checked=false;
				}
			}
			else
			{
				for(j=2;j<document.forms[0].selectedOwners.length;j++)
				{
					if(document.forms[0].selectedOwners[j].checked) {
					 	document.forms[0].selectedOwners[1].checked = false;
					 	break;
					}
				}		
			}		
		}
		else
		{
	
			for(i=0;i<document.forms[0].selectedOwners.length;i++)
			{
					if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
					{
						document.forms[0].otherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="EditXmlAction.do?Type=MSA&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&opendiv=0";
		 				document.forms[0].submit();
					}
			}
		}
	}
	if(document.forms[0].type.value=='Appendix' || document.forms[0].type.value=='NetMedX' || document.forms[0].type.value=='Hardware'){
				if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected') {
				if(checkboxvalue == '%') {
				for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
				{
					document.forms[0].appendixSelectedOwners[j].checked=false;
				}
			}
			else
			{
				for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
				{
				
				
					if(document.forms[0].appendixSelectedOwners[j].checked) {
						
					 	document.forms[0].appendixSelectedOwners[1].checked = false;
					 	break;
					}
				}		
			}		
		}
		else
		{
			for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
			{
					if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
					{
						document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="EditXmlAction.do?Type=Appendix&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&opendiv=0";
		 				document.forms[0].submit();
		
					}
			}
			}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="EditXmlAction.do?Type=MSA&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="EditXmlAction.do?Type=Appendix&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
//alert('in month_Appendix()');
document.forms[0].go.value="true";
document.forms[0].action="EditXmlAction.do?Type=Appendix&EditType=PM&Id=<bean:write name = 'EditXmlForm' property = 'id' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
</script>
</body>
</html:html>