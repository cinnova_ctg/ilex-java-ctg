<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
 
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 

<%
String Job_Id = "";
String MSA_Id = "";
String from  = "";
String addendum_id = "";
String Appendix_Id = "";
String chkaddendum = "";

if( request.getAttribute( "Job_Id" ) != null )
	Job_Id = ( String ) request.getAttribute( "Job_Id" ).toString();  

if( request.getAttribute( "MSA_Id" ) != null )
	MSA_Id = ( String ) request.getAttribute( "MSA_Id" ).toString();  

if( request.getAttribute( "from" ) != null )
	from = ( String ) request.getAttribute( "from" ).toString();  

if( request.getAttribute( "addendum_id" ) != null )
	addendum_id = ( String ) request.getAttribute( "addendum_id" ).toString();  

if( request.getAttribute( "Appendix_Id" ) != null )
	Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" ).toString();  

if( request.getAttribute( "Appendix_Id" ) != null )
	Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" ).toString();  

if( request.getAttribute( "chkaddendum" ) != null )
	chkaddendum = request.getAttribute( "chkaddendum" ).toString();  

%>

 

<bean:define id = "codes" name = "codeslist" scope = "request"/>
<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<link rel = "stylesheet" href="styles/content.css" type="text/css">
<link rel = "stylesheet" href="styles/base/ui.core.css" type="text/css">
<link rel = "stylesheet" href="styles/base/ui.datepicker.css" type="text/css">
<link rel = "stylesheet" href="styles/base/ui.theme.css" type="text/css">
<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<title>Appendix Edit Page</title>
<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
.calImageStyle{
background-color: transparent;
border: none;
padding: 0px;
margin: 0px;
vertical-align: middle;
}
</style>

<!-- <script language="JavaScript" src="javascript/date-picker.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script> -->
<script language="JavaScript" src="javascript/jquery.ui.core.js"></script>
<script language="JavaScript" src="javascript/jquery.ui.datepicker.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
<link rel="stylesheet" href="styles/style.css" type="text/css">
	<script>
		var tempprevquantity = '';
		var tempprevactivitytype = '';
	</script>
<script>
	<% String tempmaterialsCost="javascript:document.forms[0].quantityNo.value"; %>
</script>
<%@ include  file="/NMenu.inc" %>
<%@ include  file="/ActivityMenu.inc" %>
<script>
$(document).ready(function(){
	$(  "#estimatedStartSchedule").datepicker({
		buttonImage: 'images/calendar.gif',
			showOn : "both",
				yearRange: "-10:+5" ,
				disabled: true,
				changeMonth: true,
				changeYear: true
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
	
	$(  "#estimatedCompleteSchedule").datepicker({
		buttonImage: 'images/calendar.gif',
			showOn : "both",
				yearRange: "-10:+5" ,
				disabled: true,
				changeMonth: true,
				changeYear: true
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
	$('#ui-datepicker-div').css('display','none');
});
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF" onload ="leftAdjLayers();"> 

<html:form action="/ActivityEditAction">
<html:hidden property = "jobId" /> 
<html:hidden property = "activity_Id" />
<html:hidden property = "addendum_id" />
<html:hidden property = "activity_lib_Id" />
<html:hidden property = "activity_cost_type" />
<html:hidden property = "ref" />
<html:hidden property = "chkaddendum" />

<logic:notEqual name = "ActivityEditForm" property = "chkaddendum" value = "inscopedetailactivity">

<table>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			       <tr> 
			    		<logic:equal name="ActivityEditForm" property="activity_Id" value="0">
			          		<td class = "Ntoprow1" width="450">&nbsp;</td>
			        	</logic:equal>	
			    		<logic:notEqual name="ActivityEditForm" property="activity_Id" value="0">
			  
					        	<%if(  jobtype.equals( "Default" ) ||  jobtype.equals( "Addendum" ) ) { %>
									<%if(activityStatus.equals("Approved")){%>
										<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditapprovedactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
									<%}%>
									<%if(activityStatus.equals("Draft")){%>
				        				<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
									<%}%>
					        	<%}else{%>
									<%if(activityType.equals("Overhead")){%>
								  		<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditoverheadactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
							        	<%}else{%>
									   	<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
							        	<%}%>
					        	<%}%>
					        	<td id = "pop2" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><CENTER>Manage</CENTER></a></td>
					        	<td id = "pop3" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><CENTER>Resources</CENTER></a></td>
								<td id = "pop4" width = "840" class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td> 	
					</logic:notEqual>						
						
	        	</tr>
			          <tr>
			       		 <logic:notEqual name="ActivityEditForm" property="activity_Id" value="0">
					          <td background="images/content_head_04.jpg" height="21" colspan="23">
					          	<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
					          	<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "ActivityEditForm" property = "msaId" />">
					          				<bean:write name = "ActivityEditForm" property = "msaName"/></a>
					          				<A href="JobUpdateAction.do?ref=<%=refForJob %>&Appendix_Id=<bean:write name = "ActivityEditForm" property = "appendixid" />">
					          				<bean:write name = "ActivityEditForm" property = "appendixName"/></A>
					          				<A href="ActivityUpdateAction.do?ref=<bean:write name = "ActivityEditForm" property = "chkaddendum"/>&Job_Id=<bean:write name = "ActivityEditForm" property = "jobId" />">
					          				<bean:write name = "ActivityEditForm" property = "jobName"/></A>
					          	</div>
					          </td>
					     </logic:notEqual>     
					     <logic:equal name="ActivityEditForm" property="activity_Id" value="0">
				     		<td background="images/content_head_04.jpg" height="21" width ="100%">
					          	<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
					          	<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "ActivityEditForm" property = "msaId" />">
					          				<bean:write name = "ActivityEditForm" property = "msaName"/></a>
					          				<A href="JobUpdateAction.do?ref=<%=refForJob %>&Appendix_Id=<bean:write name = "ActivityEditForm" property = "appendixid" />">
					          				<bean:write name = "ActivityEditForm" property = "appendixName"/></A>
					          				<A href="ActivityUpdateAction.do?ref=<bean:write name = "ActivityEditForm" property = "chkaddendum"/>&Job_Id=<bean:write name = "ActivityEditForm" property = "jobId" />">
					          				<bean:write name = "ActivityEditForm" property = "jobName"/></A>
					          	</div>
				          </td>
				          
					     </logic:equal>
  						</tr>
  						
	 					<tr><td colspan = "4"><h2>
	 					
	 					<logic:equal name ="ActivityEditForm" property = "ref" value = "detailactivity">
		 					 <logic:equal name="ActivityEditForm" property="activity_Id" value="0">
								<bean:message bundle = "pm" key ="activity.detail.title"/>&nbsp;<bean:message bundle = "pm" key ="activity.add.head"/> 
							</logic:equal>
							
							 <logic:notEqual name="ActivityEditForm" property="activity_Id" value="0">
								 <bean:message bundle = "pm" key ="activity.edit.new.label"/>&nbsp;<bean:write name="ActivityEditForm"  property="activityName" />
							 </logic:notEqual>
						</logic:equal>
						
						<logic:notEqual name ="ActivityEditForm" property = "ref" value = "detailactivity">
							<A class = "head" href = "JobDashboardAction.do?jobid=<bean:write name = "ActivityEditForm" property = "jobId" />"><bean:write name = "ActivityEditForm" property = "jobName"/></A>
						 </logic:notEqual>
	 					</h2></td></tr> 
	 				
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>

</logic:notEqual>

<logic:equal name = "ActivityEditForm" property = "chkaddendum" value ="inscopedetailactivity">
	<table border="0" cellspacing="1" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19">&nbsp;</td></tr>
					<tr><td background="images/content_head_04.jpg" height="21" width="100%">
						<div id="breadCrumb">
							<a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${ActivityEditForm.msaId }'/>&app=null"   class="bgNone"><c:out value='${ActivityEditForm.msaName }'/></a>
							<a href="AppendixHeader.do?function=view&fromPage=appenndix&viewjobtype=ION&appendixid=<c:out value='${ActivityEditForm.appendixid }'/>"><c:out value='${ActivityEditForm.appendixName }'/></a>
						    <a href="JobDashboardAction.do?appendix_Id=<c:out value='${ActivityEditForm.appendixid }'/>&Job_Id=<c:out value='${ActivityEditForm.jobId }'/>"><c:out value='${ActivityEditForm.jobName }'/></a>
						    <c:out value='${ActivityEditForm.activityName }'/>
				  			<a><span class="breadCrumb1">Update Activity</a>
				  		</td>
				  	</tr>
				  	<tr><td>&nbsp;</td></tr>	
	</table>
</logic:equal>



<logic:notEqual name = "ActivityEditForm"  property = "chkaddendum" value ="inscopedetailactivity">
	 <logic:notEqual name="ActivityEditForm" property="activity_Id" value="0">
				<%@include  file = "/ActivityMenuScript.inc"%>
	</logic:notEqual>
</logic:notEqual>



<TABLE>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
 <tr>
  <td width="2" height="0"></td>
  <td>
  	<table border="0" cellspacing="1" cellpadding="1" width="450">
	  <tr> 
		<td colspan = "3" class = "formCaption"><bean:message bundle = "pm" key = "msa.detail.general"/></td>
	  </tr> 
	   
	  <tr> 
	    <td colspan = "1" class="colDark">Name<font class="red"> *</td>
	    <td colspan = "2" class="colLight">
	    <logic:equal name="ActivityEditForm" property="activity_Id" value="0">
	    	<html:text name="ActivityEditForm" size = "50" maxlength = "100" property = "activityName" styleClass = "text" />
		</logic:equal>	
		<logic:notEqual name="ActivityEditForm" property="activity_Id" value="0">
			<bean:write name="ActivityEditForm"  property="activityName" />
			<html:hidden property = "activityName" />
		</logic:notEqual>
		</td>
	  </tr>


	  <tr> 
	    <td colspan = "1" class="colDark">Activity Type<font class="red"> *</td>
	    <td colspan = "2" class="colLight">
	    
		    <logic:equal name="ActivityEditForm" property="activity_Id" value="0">
	    	   	<html:select name="ActivityEditForm" property = "activityType" size = "1" styleClass = "select">
				 	<html:optionsCollection name = "codes" property = "activitytypecode" value = "value" label = "label"/> 
				</html:select>
	    	</logic:equal>	
			
			<logic:notEqual name="ActivityEditForm" property="activity_Id" value="0">
				<logic:equal name="ActivityEditForm" property="jobType" value="Default">
				
					<logic:notEqual name="ActivityEditForm" property="newStatus" value="Approved">
						<html:select name="ActivityEditForm" property = "activityType" size = "1" styleClass = "select">
					 		<html:optionsCollection name = "codes" property = "activitytypecode" value = "value" label = "label"/> 
						</html:select>
					</logic:notEqual>
				
					<logic:equal name="ActivityEditForm" property="newStatus" value="Approved">	
							<bean:write name="ActivityEditForm" property ="activityTypeDetail"/>
							<html:hidden property = "activityType" />
					</logic:equal>
				
				</logic:equal>
				
				<logic:notEqual name="ActivityEditForm" property="jobType" value="Default">
					
					<logic:equal name="ActivityEditForm" property="newStatus" value="Approved">	
							<bean:write name="ActivityEditForm" property ="activityTypeDetail"/>
							<html:hidden property = "activityType" />
					</logic:equal>
					
					<logic:notEqual name="ActivityEditForm" property="newStatus" value="Approved">
						<html:select name="ActivityEditForm" property = "activityType" size = "1" styleClass = "select">
					 		<html:optionsCollection name = "codes" property = "activitytypecode" value = "value" label = "label"/> 
						</html:select>
					</logic:notEqual>
				
				</logic:notEqual>
			
			</logic:notEqual>
		</td>
	  </tr>
	  
	  <tr> 
	    <td colspan = "1" class="colDark">Quantity</td>
	    <td colspan = "2" class="colLight">
	    <logic:equal name="ActivityEditForm" property="jobType" value="Default">
			<html:text name="ActivityEditForm" size = "10" maxlength = "" property = "quantity" styleClass = "Nreadonlytextnumbereven" onfocus = "assignquantity(document.forms[0].quantity.value);" onblur="return multiplyquantity(tempprevquantity);" />
		</logic:equal>
		 <logic:notEqual name="ActivityEditForm" property="jobType" value="Default">
			<html:text name="ActivityEditForm" size = "10" maxlength = "" property = "quantity" styleClass = "text"	onfocus = "assigntempquantity(document.forms[0].quantity.value);" onblur="return multiplyquantity(tempprevquantity);" />
		</logic:notEqual>
		</td>
	  </tr>
	  <tr> 
		<td colspan = "3" class = "formCaption">Estimated Cost</td>
	  </tr> 
	  <tr> 
	  	<td colspan = "1"  class="colDark">Materials</td>
	    <td colspan = "2"  class="colLight">
	    <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "estimatedMaterialcost" styleClass = "Nreadonlytextnumbereven"/>
	  </tr>
	  <tr> 
	  	<td colspan = "1" class="colDark">Corporate Labor</td>
	    <td colspan = "2" class="colLight">
	    <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "estimatedCnsfieldhqlaborcost" styleClass = "Nreadonlytextnumbereven" />
	  </tr>
	  
	  <html:hidden property = "estimatedCnsfieldlaborcost" />
	  <html:hidden property = "estimatedCnshqlaborcost" />
	  
	  <tr> 
	    <td colspan = "1" class="colDark">Contract Labor</td>
	    <td colspan = "2" class="colLight">
	    <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "estimatedContractfieldlaborcost" styleClass = "Nreadonlytextnumbereven" />
	    	
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "1" class="colDark">Freight</td>
	    <td colspan = "2" class="colLight">
	     <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "estimatedFreightcost" styleClass = "Nreadonlytextnumbereven" />
	    	
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "1" class="colDark">Travel</td>
	    <td colspan = "2" class="colLight">
	    <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "estimatedTravelcost" styleClass = "Nreadonlytextnumbereven" />
	    	
		</td>
		</tr>
		<tr> 
	    <td colspan = "1" class="colDark">Total</td>
	    <td colspan = "2" class="colLight">
	     <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "estimatedTotalcost" styleClass = "Nreadonlytextnumbereven" />
	    	
		</td>
	  </tr>
	  <tr> 
		<td colspan = "3" class = "formCaption" height = "25">Cost-Price</td>
	  </tr> 
	  <tr> 
	    <td colspan = "1" class="colDark">Extended Subtotal</td>
	    <td colspan = "2" class="colLight">
	      <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "extendedprice" styleClass = "Nreadonlytextnumbereven" />
		</td>
	  </tr>
	  <tr>
	    <td colspan = "1" class="colDark">Overhead</td>
	    <td colspan = "2" class="colLight">
	     <html:text  name = "ActivityEditForm" size = "10"  readonly = "true" property = "overheadcost" styleClass = "Nreadonlytextnumbereven" />
	    	
		</td>
	  </tr>
	  <tr>
	    <td colspan = "1" class="colDark">Extended Price</td>
	    <td colspan = "2" class="colLight">
	      <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "listprice" styleClass = "Nreadonlytextnumbereven" />
		</td>
	  </tr>
	  <tr>
	    <td colspan = "1" class="colDark">Pro Forma Margin</td>
	    <td colspan = "2" class="colLight">
	    <html:text name = "ActivityEditForm" size = "10"  readonly = "true" property = "proformamargin" styleClass = "Nreadonlytextnumbereven" />
	    	
		</td>
	  </tr>
      <tr> 
		<td colspan = "3" class = "formCaption" height = "25">Estimated Schedule</td>
	  </tr> 
	   
	  <tr> 
	    <td colspan = "1" class="colDark">Start</td>
	    <td colspan = "2" class="colLight">
	    <logic:equal name="ActivityEditForm" property="jobType" value="Default">
	    	<bean:write name ="ActivityEditForm" property ="estimatedStartSchedule"/>
	    	<html:hidden property = "estimatedStartSchedule" />
	    </logic:equal>
	    <logic:notEqual name="ActivityEditForm" property="jobType" value="Default">
	      	<logic:notEqual name="ActivityEditForm" property="newStatus" value="Approved">
				<html:text name="ActivityEditForm" size = "10" maxlength = "" property = "estimatedStartSchedule" styleClass = "text"  styleId="estimatedStartSchedule"/> 
			</logic:notEqual>
	    	<logic:equal name="ActivityEditForm" property="newStatus" value="Approved">
	    		<bean:write name ="ActivityEditForm" property ="estimatedStartSchedule"/>
	    		<html:hidden property = "estimatedStartSchedule" />
	    	</logic:equal>
	   </logic:notEqual>
		</td>
	  </tr>
	  <tr> 
	    <td colspan = "1" class="colDark">Complete</td>
		<td colspan = "2" class="colLight">
		
		 <logic:equal name="ActivityEditForm" property="jobType" value="Default">
		 	<bean:write name ="ActivityEditForm" property ="estimatedCompleteSchedule"/>
		 	<html:hidden property = "estimatedCompleteSchedule" />
		 </logic:equal>
		 
		 <logic:notEqual name="ActivityEditForm" property="jobType" value="Default">
			<logic:notEqual name="ActivityEditForm" property="newStatus" value="Approved">
				<html:text name="ActivityEditForm" size = "10" maxlength = "" property = "estimatedCompleteSchedule" styleClass = "text"  styleId="estimatedCompleteSchedule"/> 
		   	</logic:notEqual>
		   	
		   	<logic:equal name="ActivityEditForm" property="newStatus" value="Approved">
		    		<bean:write name ="ActivityEditForm" property ="estimatedCompleteSchedule"/>
		    		<html:hidden property = "estimatedCompleteSchedule" />
		   	</logic:equal>
	   	</logic:notEqual>
	   	</td>
     </tr>
     
     <tr> 
	    <td colspan = "1" class="colDark">Status</td>
		<td colspan = "2" class="colLight">
			<bean:write name="ActivityEditForm" property="newStatus"/>
			<html:hidden property = "newStatus" />
		</td>
     </tr>
      <tr> 
		<td colspan = "3" class = "formCaption">Comments</td>
	  </tr>
	  
	  <%
	  boolean csschooser = true;
	  String cssclass = "";
	  String commentbyclass = "";
	
   	  %>	
		<logic:notEqual name="ActivityEditForm" property="comment" value="">
		<tr>
			<td class = "formCaption"><bean:message bundle = "pm" key = "common.viewcomment.commentdate"/></td>
			<td class = "formCaption"><bean:message bundle = "pm" key = "common.viewcomment.commentby"/></td>
			<td class = "formCaption"><bean:message bundle = "pm" key = "common.viewcomment.comments"/></td>
		</tr> 
		
		<tr>
			<td  class = "colLight"><bean:write name = "ActivityEditForm" property = "commentdate"/></td>	
			<td  class = "colLight"><bean:write name = "ActivityEditForm" property = "commentby"/></td>				  
			<td  class = "colLight"><bean:write name = "ActivityEditForm" property = "comment"/></td>
 		</tr>
 		</logic:notEqual>
 		<logic:equal name="ActivityEditForm" property="comment" value="">
  		<tr> 
			<td  class="colLight" colspan="3"><font class="Nmessage">&nbsp;&nbsp;No Comments.</font></td>
		</tr>
			 </logic:equal>
	<%		
	  if( csschooser == true )
		{
			cssclass = "colDark";
			commentbyclass = "colLight";
			csschooser = false;
		}
		else
		{
			cssclass = "colDark";
			commentbyclass = "colLight";
			csschooser = true; 
		}
	%> 
	  
	 <tr> 
	    <td colspan = "1" class="<%= cssclass %>">Created</td>
		<td colspan = "2" class="<%= commentbyclass %>"><bean:write name="ActivityEditForm" property="creatBy" />&nbsp;<bean:write name="ActivityEditForm" property="creatDate" /></td>
	 </tr>
	 
	<%		
	  if( csschooser == true )
		{
			cssclass = "colDark";
			commentbyclass = "colLight";
			csschooser = false;
		}
		else
		{
			cssclass = "colDark";
			commentbyclass = "colLight";
			csschooser = true; 
		}
	%>
	
	 <tr>
		<td colspan = "1" class="<%= cssclass %>">Updated</td>
		<td colspan = "2" class="<%= commentbyclass %>"><bean:write name="ActivityEditForm" property="lastactivitymodifiedby" />&nbsp;<bean:write name="ActivityEditForm" property="lastactivitymodifieddate" /></td>
     </tr>       
   	<td class = "colLight" colspan="3" align="center">
		<html:submit property = "save" styleClass = "button_c" onclick = "return validate();">Submit</html:submit>&nbsp;
		<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>&nbsp;
		<html:button property = "back" styleClass = "button_c" onclick = "Backaction();">Back</html:button>
	</td>
	</table></td></tr>
	</table>
</html:form>
</TABLE>

</BODY>

<script>
function assigntempquantity(tempqty)
{
	if (tempqty == '' || tempqty == 0 )
		{
			tempprevquantity = tempprevquantity;
		}
	else
		{
			tempprevquantity = tempqty;
		}
}

function multiplyquantity( prevval )
{
		if( document.forms[0].quantity.value == "")
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.blankquantity"/>" );
			document.forms[0].quantity.focus();
			return false;
		}
		
		if( isFloat( document.forms[0].quantity.value ) )
		{
			if( !( document.forms[0].quantity.value > 0 ) )
			{
				alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterzero"/>" );
				document.forms[0].quantity.value = '';
				document.forms[0].quantity.focus();
				return false;
			}
			if(document.forms[0].estimatedCnshqlaborcost.value==""){document.forms[0].estimatedCnshqlaborcost.value=0;}
			if(document.forms[0].estimatedCnsfieldlaborcost.value==""){document.forms[0].estimatedCnsfieldlaborcost.value=0;}
		
			document.forms[0].estimatedMaterialcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedMaterialcost.value ) ) / parseFloat( prevval ) );
			document.forms[0].estimatedCnsfieldlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedCnsfieldlaborcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimatedCnshqlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedCnshqlaborcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimatedCnsfieldhqlaborcost.value = round_func( parseFloat( document.forms[0].estimatedCnsfieldlaborcost.value ) + parseFloat( document.forms[0].estimatedCnshqlaborcost.value ) );
			
			document.forms[0].estimatedContractfieldlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedContractfieldlaborcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimatedFreightcost.value = round_func( ( parseFloat(  document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedFreightcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimatedTravelcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedTravelcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimatedTotalcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedTotalcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].extendedprice.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].extendedprice.value ) ) / parseFloat( prevval ) );		
		}
		else
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitynumeric"/>" );
			document.forms[0].quantity.value = '';
			document.forms[0].quantity.focus();
			return false;
		}
}

function Backaction()
{
	history.go(-1);
}

function validate()
{	
		if( document.forms[0].activityName.value == "" || document.forms[0].activityName.value > -1){
 			alert( "<bean:message bundle = "pm" key = "activity.tabular.newnameenter"/>");
 			document.forms[0].activityName.value = ''
 			document.forms[0].activityName.focus();
 			return false;
 		}
 		
 		if(!document.forms[0].activity_Id.value=="0"){
	 		if( document.forms[0].activityType.value == "0" ){
					alert( "<bean:message bundle = "pm" key = "activity.tabular.selectactivitytype"/>" );
					document.forms[0].activityType.focus();
					return false;
			}
		}
		
		document.forms[0].action="ActivityEditAction.do?Job_Id=<bean:write name="ActivityEditForm" property="jobId" />&ref=<bean:write name="ActivityEditForm" property="ref" />&Activity_Id=<bean:write name="ActivityEditForm" property="activity_Id" />&from=<bean:write name="ActivityEditForm" property="from" />";
 	return true;  
 	
}

</script>
<script>
function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;

}
function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}


function leftAdjLayers(){
if(document.forms[0].chkaddendum.value!='inscopedetailactivity'){
			var layers, leftAdj=0;
			leftAdj =document.body.clientWidth;
	 		leftAdj = leftAdj - 280;
			document.getElementById("filter").style.left=leftAdj;
	}		
}

</script>
</html:html>
