<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying  Activity detail.
*
-->
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:define id = "jobtype" name = "ActivityDetailForm" property = "job_type" />
<bean:define id ="activityTypeForJob" name="ActivityDetailForm" property="chkaddendum"/>


<%
int sow_size = 0;
int assumption_size = 0;
int uplift_size = 0;
int uplift_column_size = 0;
//Added By Amit for checking null of sowsize and assumptionsize
if(request.getAttribute( "sowsize" )!=null) {
	 sow_size =( int ) Integer.parseInt( request.getAttribute( "sowsize" ).toString()); 
}
if(request.getAttribute("assumptionsize" )!=null) {
	 assumption_size =( int ) Integer.parseInt( request.getAttribute( "assumptionsize" ).toString()); 
}
if(request.getAttribute( "uplift_size" )!=null) {
	uplift_size =( int ) Integer.parseInt( request.getAttribute( "uplift_size" ).toString()); 
}

uplift_column_size = uplift_size+1;

	String Activity_Id = "";
	String Job_Id = "";
	String MSA_Name = "";
	String MSA_Id = "";
	String checknetmedx = "";
	String jobid = "";
	String fromflag = "";
	String from = "";
	int i = 0;
	String bgcolor = null;
	String bgcolorbold = null;
	String refForJob = null;
	if( request.getAttribute( "Activity_Id" ) != null )
		Activity_Id = ( String ) request.getAttribute( "Activity_Id" );
	if( request.getAttribute( "Job_Id" ) != null )
		Job_Id = ( String ) request.getAttribute( "Job_Id" );
	if( request.getAttribute( "MSA_Name" ) != null )
		MSA_Name = ( String ) request.getAttribute( "MSA_Name" );
	if( request.getAttribute( "MSA_Id" ) != null )
		MSA_Id = ( String ) request.getAttribute( "MSA_Id" );

	if( request.getAttribute( "checknetmedx" ) != null )
		checknetmedx = ( String ) request.getAttribute( "checknetmedx" );

	if( request.getAttribute( "from" ) != null )
	{
		//Added By Amit a Check
		if(request.getAttribute( "jobid" )!=null){
		jobid = request.getAttribute( "jobid" ).toString();}
		from = request.getAttribute( "from" ).toString();
	}
%>

<%if(activityTypeForJob.equals("detailactivity")||activityTypeForJob.equals("View")) 
	 refForJob = "detailjob";
else
	 refForJob = "inscopejob";
%>	


<html:html>
<head>
	<title><bean:message bundle = "pm" key = "activity.detail.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<%@ include file="/NMenu.inc" %>
<script>
function del() 
{
		if( document.forms[0].status.value == 'Approved' )
		{
			alert( "<bean:message bundle = "pm" key = "activity.detail.approvedactivity"/>" );
			return false;
		}
		else
		{
			var convel = confirm( "<bean:message bundle = "pm" key = "activity.detail.deleteactivity"/>" );
			if( convel )
			{
				document.forms[0].action = "DeleteAction.do?addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&Activity_Id=<%= Activity_Id%>&Job_Id=<%= Job_Id %>&Type=Activity";
				document.forms[0].submit();
				return true;	
			}
		}
}


var str = '';

function addcomment()
{
	//str = 'MenuFunctionAddCommentAction.do?ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&Type=Activity&Activity_Id=<%= Activity_Id %>';
	//p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:320px' ); 
	
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&Type=Activity&Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>';
	<%}
	else
	{%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&Type=Activity&Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>';
	<%}%>
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	//str = 'MenuFunctionViewCommentAction.do?Type=Activity&Id=<%= Activity_Id %>';
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' ); 
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&Type=Activity&Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>';
	document.forms[0].submit();
	return true;	
}

function viewresources(choice)
{
	<%if(checknetmedx.equals("1")) {%>
						  		document.forms[0].action = 'ResourceListAction.do?resourceListType='+choice+'&ref=View&type=PM&MSA_Id=<%=MSA_Id %>&Activity_Id=<%= Activity_Id %>&Activitylibrary_Id=<%= Activity_Id %>&jobType=<bean:write name = "ActivityDetailForm" property = "job_type" />' ;
							 	<%}
						  		else
								{
									if(  request.getAttribute( "from" ) != null )
									{%>
										document.forms[0].action ='ResourceListAction.do?resourceListType='+choice+'&from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendumresource" />&Activity_Id=<%= Activity_Id %>&jobType=<bean:write name = "ActivityDetailForm" property = "job_type" />'; 
									<%}else{%>
										document.forms[0].action ='ResourceListAction.do?resourceListType='+choice+'&addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendumresource" />&Activity_Id=<%= Activity_Id %>&jobType=<bean:write name = "ActivityDetailForm" property = "job_type" />'; 
		<%}} %>
	document.forms[0].submit();
	return true;

}

</script>

<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');leftAdjLayers();"> 
<html:form action = "/ActivityDetailAction">
<html:hidden property = "job_type" />
<html:hidden property = "status" />
<html:hidden property = "chkaddendum" />
<html:hidden property = "addendum_id" />


<table>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr> 
			        		<%if(from == null || from.equalsIgnoreCase("")){%>
					        	<%if(  jobtype.equals( "Default" ) ||  jobtype.equals( "Addendum" ) ) { %>
					        		<logic:equal name ="ActivityDetailForm" property ="status" value ="Approved">
					        				<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditapprovedactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					        		</logic:equal>
					        		<logic:equal name ="ActivityDetailForm" property ="status" value ="Draft">
					        				<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum"/>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					        		</logic:equal>
					        	<%}else{%>
						        	<logic:equal name="ActivityDetailForm" property="type" value="Overhead">
						        		<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditoverheadactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
						        	</logic:equal>
						        	<logic:notEqual name="ActivityDetailForm" property="type" value="Overhead">
						        		<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum"/>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
						        	</logic:notEqual>
					        	<%}%>
				        	<td id = "pop2" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><CENTER>Manage</CENTER></a></td>
				        	<td id = "pop3" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><CENTER>Resources</CENTER></a></td>
							<td id = "pop4" width = "840" class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td> 	
						<%}else{%>	
							<td  class = "Ntoprow1" >&nbsp;</td>
						<%}%>
	        	</tr>
			          <tr>
						          <!-- BreadCrumb goes into this td -->
				          <td background="images/content_head_04.jpg" height="21" colspan="23">
				          <%if(request.getAttribute( "from" ) != null && request.getAttribute( "from" ) != ""){ %>
					          	<div id="breadCrumb">
					          			<a class ="bgNone" href = "SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<bean:write name = "ActivityDetailForm" property = "msa_id" />&app=null"><bean:write name = "ActivityDetailForm" property = "msaname"/></a>
					          			<a href="AppendixHeader.do?function=view&fromPage=appendix&viewjobtype=ION&&msaId=<bean:write name = "ActivityDetailForm" property = "msa_id" />&appendixid=<bean:write name = "ActivityDetailForm" property = "appendix_id" />"><bean:write name = "ActivityDetailForm" property = "appendixname"/></a>
					          			<a href="JobDashboardAction.do?function=view&jobid=<bean:write name = "ActivityDetailForm" property = "job_Id" />&appendix_id=<bean:write name = "ActivityDetailForm" property = "appendix_id" />"><bean:write name = "ActivityDetailForm" property = "jobname"/></a>
					          			<a><span class ="breadCrumb1">Activity Summary</span></a>
								</div>
					        <%}else{ %>
								
								<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
						          	<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "ActivityDetailForm" property = "msa_id" />">
					  					<bean:write name = "ActivityDetailForm" property = "msaname"/></a>
					  				<A href="JobUpdateAction.do?ref=<%=refForJob %>&Appendix_Id=<bean:write name = "ActivityDetailForm" property = "appendix_id" />">
					  					<bean:write name = "ActivityDetailForm" property = "appendixname"/></A>
					  				<A href="ActivityUpdateAction.do?&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&job_Id=<bean:write name = "ActivityDetailForm" property = "job_Id" />">
					  					<bean:write name = "ActivityDetailForm" property = "jobname"/></A>
					          </div>
							<%} %>
  							</td>
						          <!-- End of BreadCrumb-->
					  </tr>
					 <%if(request.getAttribute( "from" ) != null && request.getAttribute( "from" ) != ""){ %>
					 <%}else{%>
	 					<tr> <td colspan = "4"><h2><bean:message bundle = "pm" key = "activity.detail.new.label"/><bean:write name = "ActivityDetailForm" property = "name"/></h2></td></tr> 
	 				 <%}%>	
	 				
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" >
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
<%if(from == null || from.equalsIgnoreCase("")){%>
<SCRIPT language=JavaScript1.2>
<!--
if ( isMenu ) {
arMenu1 = new Array(
122,
findPosX( 'pop2' ),findPosY( 'pop2' ),
"","",
"","",
"","",
"<bean:message bundle = "pm" key = "activity.detail.menu.changestatus.new"/>","#",1,
"<bean:message bundle = "pm" key = "activity.detail.menu.view.activity"/>","#",1,
"<bean:message bundle = "pm" key = "activity.detail.menu.comments"/>","#",1,
"<bean:message bundle = "pm" key = "activity.detail.menu.deleteactivity"/>","javascript:del();",0
)

<% if( ( String )request.getAttribute( "list" ) != "" ) {%>
arMenu1_1=new Array(
<%= ( String ) request.getAttribute( "list" ) %>
)
<%}%>

arMenu1_2=new Array(
"<bean:message bundle = "pm" key = "activity.detail"/>","ActivityDetailAction.do?from=<%=fromflag%>&jobid=<%= jobid %>addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&Activity_Id=<%= Activity_Id %>",0,
"<bean:message bundle = "AM" key = "am.detail.menu.managesow.new"/>","ManageSOWAction.do?ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&function=View&Type=Activity&Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>",0,
"<bean:message bundle = "AM" key = "am.detail.menu.manageassumption.new"/>","ManageAssumptionAction.do?ref=<bean:write name = "ActivityDetailForm" property = "chkaddendum" />&addendum_id=<bean:write name = "ActivityDetailForm" property = "addendum_id" />&function=View&Type=Activity&Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>",0
)

arMenu1_3=new Array(
"<bean:message bundle = "pm" key = "activity.detail.menu.viewall"/>","javascript:viewcomments();",0,
"<bean:message bundle = "pm" key = "activity.detail.menu.addcomment"/>","javascript:addcomment();",0
)

arMenu2=new Array(
122,
findPosX( 'pop3' ),findPosY( 'pop3' ),
"","",
"","",
"","",
"<bean:message bundle = "pm" key = "activity.detail.resources.all"/>","javascript:viewresources('A');",0,
"<bean:message bundle = "pm" key = "activity.detail.resources.labour"/>","javascript:viewresources('L');",0,
"<bean:message bundle = "pm" key = "activity.detail.resources.materials"/>","javascript:viewresources('M');",0,
"<bean:message bundle = "pm" key = "activity.detail.resources.travel"/>","javascript:viewresources('T');",0,
"<bean:message bundle = "pm" key = "activity.detail.resources.freight"/>","javascript:viewresources('F');",0
)




document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

//-->
</script>

<%}%>
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr><td width="2" height="0"></td>
  			 <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "100%"> 
					
					<logic:present name = "changestatusflag" scope = "request">
	    				<tr> 
							<td colspan = "4" class = "message" height = "30" >
						    	<logic:equal name = "changestatusflag" value = "0">
									<bean:message bundle = "pm" key = "activity.detail.changestatus.success"/>	
					    		</logic:equal>
		    		
					    		<logic:equal name = "changestatusflag" value = "-9001">
									<bean:message bundle = "pm" key = "activity.detail.changestatus.failure1"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9002">
									<bean:message bundle = "pm" key = "activity.detail.changestatus.failure2"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9003">
									<bean:message bundle = "pm" key = "activity.detail.changestatus.failure3"/>	
					    		</logic:equal>
					    	</td>
				    	</tr>
    				</logic:present>
					
				</table>
					
					<table border = "0" cellspacing = "1" cellpadding = "1" width = 450>  
  					<!-- General:start -->
  					<tr> 
    					<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "activity.detail.general"/></td>
    				</tr>
    				 <%if(request.getAttribute( "from" ) != null && request.getAttribute( "from" ) != ""){ %>
    				<tr>
    					<td class = "colDark">Activity Name</td>
    					<td class = "colLight" width="350"><bean:write name = "ActivityDetailForm" property = "name"/></td>
    				</tr>	
    				<%}%>
    				<tr>
    					<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.type"/></td>
    					<td class = "colLight" width="350"><bean:write name = "ActivityDetailForm" property = "type"/></td>
    				</tr>
    				<tr>
    					<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.quantity"/></td>
    					<td class = "colLight" ><bean:write name = "ActivityDetailForm" property = "quantity"/></td>
    				</tr>
    				<tr>
    					<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.status"/></td>
    					<td class = "colLight" ><bean:write name = "ActivityDetailForm" property = "status"/></td>
    				</tr>
    				<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.lastmodified"/></td>
						<td class = "colLight" ><bean:write name = "ActivityDetailForm" property = "lastactivitymodifiedby" />&nbsp;<bean:write name = "ActivityDetailForm" property = "lastactivitymodifieddate" /> </td>
						
    				</tr>
    				<!-- General:end -->
    				<!-- Price&cost:start -->
    				<tr>
						<td colspan = "2" class = "formCaption"><bean:message bundle = "pm" key = "activity.detail.costprice"/></td>
						<%--
						<logic:notEqual name = "ActivityDetailForm" property = "chkaddendum" value = "detailactivity">
						<% if(uplift_size > 0) { %>
						<td colspan="4" rowspan="<%= uplift_column_size%>">
						<table border = "0" cellspacing = "1" cellpadding = "1" 
							<tr>				
								<td class = "labellobold" height = "30" width = 14%>Activity Uplift Factors</td>
								<td class = "labelloboldmiddle" height = "30" width = 4%>Value</td>
								<td class = "labelloboldmiddle" height = "30" width = 4%>Cost</td>
								<td width = 46%></td>
							</tr>
							<logic:present name = "upliftFactorList" scope = "request"> 
							 	<logic:iterate id = "ufl" name = "upliftFactorList">
							 	<% if(i%2 == 0) { bgcolorbold = "labelobold"; bgcolor = "readonlytextnumberodd"; }
							 		else { bgcolorbold = "labelebold"; bgcolor = "readonlytextnumbereven"; }%>
							 	<tr>				
									<td class = "<%= bgcolorbold%>" height = "20"><bean:write name="ufl" property="upliftFactorName"/></td>
									<td class = "<%= bgcolor%>" height = "20"><bean:write name="ufl" property="upliftFactorValue"/></td>
									<td class = "<%= bgcolor%>" height = "20"><bean:write name="ufl" property="upliftFactorCost"/></td>
									<td width = 44%></td>
								</tr>
								<% i++; %>
							 	</logic:iterate>
							 </logic:present>	
						</table>
						</td>
						<% } else { %>
					<!-- 	<td colspan="2"width = 68%></td>	-->				 
						<% } %>
						</logic:notEqual>
					--%>
					</tr>  
					<!-- Price&cost:start -->
					<tr> 
    					<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.estimatedmaterialcost"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "estimated_materialcost"/></td>
  					</tr>
  					
  					<tr> 
    					<td class = "colDark" ><bean:message bundle = "pm" key = "activity.detail.estimatedcnslabourcost"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "estimated_cnslaborcost"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark" ><bean:message bundle = "pm" key = "activity.detail.estimatedcontractlabourcost"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "estimated_contractlaborcost"/></td>
    				</tr>
    				<tr>
    				
  						<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.estimatedfreightcost"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "estimated_freightcost"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark" ><bean:message bundle = "pm" key = "activity.detail.estimatedtravelcost"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "estimated_travelcost"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.estimatedtotalcost"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "estimated_totalcost"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.extendedprice"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "extendedprice"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.estimatedunitcost"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;" ><bean:write name = "ActivityDetailForm" property = "estimated_unitcost"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.unitprice"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "estimated_unitprice"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark" ><bean:message bundle = "pm" key = "activity.detail.overheadcost"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;" ><bean:write name = "ActivityDetailForm" property = "overheadcost"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark" >Commission Cost</td>
    					<td class = "colLight" style="text-align:left;width: 5em;" ><bean:write name = "ActivityDetailForm" property = "commissionCost"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark" >Commission Revenue</td>
    					<td class = "colLight" style="text-align:left;width: 5em;" ><bean:write name = "ActivityDetailForm" property = "commissionRevenue"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark" ><bean:message bundle = "pm" key = "activity.detail.listprice"/></td>
    					<td class = "colLight" style="text-align:left;width: 5em;"><bean:write name = "ActivityDetailForm" property = "listprice"/></td>
    				</tr>
    				<tr>
  						<td class = "colDark"><bean:message bundle = "pm" key = "activity.detail.proformamargin"/></td>
						<td class = "colLight"><bean:write name = "ActivityDetailForm" property = "proformamargin"/></td>
					</tr>
					
					<%if(request.getAttribute( "from" ) != null && request.getAttribute( "from" ) != ""){ %>
					<%}else{%>
						<!-- comment:start -->			
  					<tr> 
						<td colspan = "2" class = "formCaption">
							<bean:message bundle = "pm" key = "activity.detail.comments"/>
						</td>	
						
  					</tr>
  					<tr>
  						<logic:present name="ActivityDetailForm" property="lastcomment"> 
						    <td class = "colDark" ><bean:write name = "ActivityDetailForm" property = "lastchangeby" />
						    	<bean:write name = "ActivityDetailForm" property = "lastchangedate" />
						    </td>
						    <td class = "colLight" >
						    	<bean:write name = "ActivityDetailForm" property = "lastcomment"/>
						    </td>
					    </logic:present>
					    <logic:notPresent name="ActivityDetailForm" property="lastcomment"> 
					    	 <td class = "colLight" colspan="2"><font class="Nmessage">&nbsp;&nbsp;No Comments.</font>
					    	 </td>
					    </logic:notPresent>
				    </tr>
  					
  					<!-- comment:End --> 
					<%}%>
					
					<% int k = 1; %>
					
					<% if( ( sow_size > 0 ) || ( assumption_size > 0 ) ) {%>
					<!-- Sow:start -->
				    <tr>
						<td class = "formCaption" colspan = "2" ><bean:message bundle = "AM" key = "am.detail.sow"/></td>
					</tr>
					<c:choose>
							<c:when test  ="${empty requestScope.sowlist}">
								<td class = "dbvalueDisabled" colspan = "2">&nbsp;&nbsp;Not Defined</td>  
							</c:when>
							<c:when test  ="${not empty requestScope.sowlist}">
								<tr>
									<td class = "colLightRap" colspan = "2">	
										<logic:iterate id = "ms" name = "sowlist">	
											 <bean:write name = "ms" property = "sow" /> <br><br>
											<% k++; %>
										</logic:iterate>
									</td>
								</tr>	
							</c:when>
					</c:choose>		
					<!-- Sow:End -->
					<!-- Assumption:start -->
					<tr>
						<td class = "formCaption" colspan = "2"><bean:message bundle = "AM" key = "am.detail.assumption"/></td>
					</tr>
					<c:choose>
							<c:when test  ="${empty requestScope.assumptionlist}">
								<td class = "dbvalueDisabled" colspan = "2">&nbsp;&nbsp;Not Defined</td>  
							</c:when>
							<c:when test  ="${not empty requestScope.assumptionlist}">
								<tr>
									<% k = 1; %>
									
									<td class = "colLightRap" colspan = "2">	
										<logic:iterate id = "ms" name = "assumptionlist" >	
											 <bean:write name = "ms" property = "assumption" /> <br><br>
											<% k++; %>
										</logic:iterate>
									</td>
								</tr>
							</c:when>
					</c:choose>
					
					<!-- Assumption:End -->
					<%} %>
					
					<% if( request.getAttribute( "from" ) != null && !request.getAttribute("from").toString().equals("")){ %>
					<%}else{ 
					%>
					<tr>
							 <td class = "colLight" align="center" colspan="2">
							 	<html:button property = "back"  styleClass="button_c" onclick = "return BackByHistory();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
							</td>
						</tr>
					<%} %>
					<jsp:include page = '/Footer.jsp'>
		      			<jsp:param name = 'colspan' value = '37'/>
		      			<jsp:param name = 'helpid' value = 'appendixdetailpage'/>
		 			</jsp:include>	
	 			</table>  
			</td>
		</tr>
	</table>
					
</html:form>
</table>
</body>
<script>
function Backactionjob()
{
	<% if(  request.getAttribute( "jobid" ) != null ){%>
		document.forms[0].action = "JobDashboardAction.do?jobid=<%= request.getAttribute( "jobid" ).toString() %>" ;
	<%} %>
	document.forms[0].submit();
	return true;
}

function BackByHistory()
{
	javascript:history.go(-1);
}
</script>

<script>
function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;

}
function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;
}

</script>

</html:html>
  					







