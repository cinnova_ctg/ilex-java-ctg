<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating and  updating  Activities.
*
-->
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;
int jobOwnerListSize = 0;
String testAppendix = "";
if(request.getAttribute("testAppendix") != null) 
	testAppendix = request.getAttribute("testAppendix").toString(); 

if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());



%>




<%@ page import="java.util.*" %>
<%@ page import="com.mind.dao.PRM.*" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% 
	int activity_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 
	String appendixname = "";
	String fromflag = "";  // This flag will be used only when copy activity from job dashboard through New Activity link
	String prevdivcheckid = "";
	String Appendix_Id ="";
	int div=1;
	

	if(request.getAttribute("Appendix_Id")!=null)
		Appendix_Id=request.getAttribute("Appendix_Id").toString();	
	
	if( request.getAttribute( "appendixname" )!= null ) {
		appendixname=request.getAttribute("appendixname").toString();
	}

	if( request.getAttribute( "from" ) != null ) {
		request.setAttribute( "from" , request.getAttribute( "from" ).toString() );
	}
%>
<style type="text/css">
/* styles of a menudiv visible */
.#menunav {
    visibility: visible;
}
</style>
<html:html>

<head>
	<title><bean:message bundle = "pm" key = "activity.tabular.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<link rel = "stylesheet" href="docm/styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language="JavaScript" src="javascript/ilexGUI.js"></script>
	<script>
		var tempprevquantity = '';
		var tempprevactivitytype = '';
	</script>
	<script>
	function fun_HideShowBlock(index,isClicked)
		{
			var len=document.all.tabsize.value;			
	        for(var id=0;id<len;id++){
	        	var element=document.getElementById("tab_"+id);
	        	if(element.style.display=="block"){
					element.style.display="none";
	        		}
	        }
			element.style.display="block"; 
			buttonPressed(index);
		}
		function buttonPressed(idx)
			{					
				for(var i=0;i<document.all.TOP_BUTTON.length/3;i++) {
					document.all.TOP_BUTTON[3*i+1].style.fontSize='10';
					//document.all.TOP_BUTTON[3*i+1].width='150';
					document.all.TOP_BUTTON[3*i+1].width=document.all.TOP_BUTTON[3*i+1].innerHTML.length*8;
					document.all.TOP_BUTTON[3*i+1].style.fontFamily='Verdana|Arial|Helvetica|sans-serif';
					if(!document.all.TOP_TAB.length){
						document.all.TOP_TAB.style.width='5';
					}else{
						document.all.TOP_TAB[i].style.width='5';
					}
	
					if(i==idx)
					{
						/*if(idx==0){
							document.all.TOP_BUTTON[3*i].style.display='block';
							document.all.TOP_BUTTON[3*i+1].style.display='block';
							document.all.TOP_BUTTON[3*i+2].style.display='block';
						}*/
						document.all.TOP_BUTTON[3*i].innerHTML='<img src="images/left-cor-light.gif" width="5" height="18" border="0">';
						document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#D9DFEF';
						document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="images/right-cor-light.gif" width="5" height="18"  border="0">';
						document.all.TOP_BUTTON[3*i+1].style.borderBottom = '0px #D9DFEF solid';
						document.all.TOP_BUTTON[3*i+1].style.fontWeight='bold';
					}
					else//Restore Remaining
					{
						/*if(idx==0){
							document.all.TOP_BUTTON[3*i].style.display='none';
							document.all.TOP_BUTTON[3*i+1].style.display='none';
							document.all.TOP_BUTTON[3*i+2].style.display='none';
						 }else{*/
							document.all.TOP_BUTTON[3*i].innerHTML='<img src="images/left-cor.gif" width="5" height="18"  border="0">';
							document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#B5C8D9';
							document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="images/right-cor.gif" width="5" height="18"  border="0">';
						    document.all.TOP_BUTTON[3*i+1].style.borderBottom = '2px #B5C8D9 solid';
							document.all.TOP_BUTTON[3*i+1].style.fontWeight='normal';
						//}
					}
					viewChange('tab_'+idx  );
				}
			}
	
	</script>
<!--  div visible show  -->	
	<script type="text/javascript">

/*  $(document).ready(function()
         {
	
	 $('#menunav').show();
	 $('#menunav').css('visibility', 'visible');
	 
       
             });         

 $( window ).load(function() {
		
	 $('#menunav').show();
	 $('#menunav').css('visibility', 'visible');
	});
  */

</script>
<!--  End of visisble -->	
<%@ include file = "/NMenu.inc" %> 
</head>
<%@ include  file="/JobMenu.inc" %>
<%
	String backgroundclass = null;
	String backgroundhyperlinkclass = null;
	String comboclass = null;
	boolean csschooser = true;
	String readonlyclass = null;
	String readonlynumberclass = null;
	String displaycombo = null;
	String valstr = null;
	String datecheck = null;
	String schdayscheck = null;
	String multiplyqty = null;
	int i = 0;
	int myId= 0-1;
	
	
%>
<html:form action = "/CopyAddendumActivityAction">
<body onload = "<c:if test='${requestScope.Size gt 0}'>fun_HideShowBlock('0','0');</c:if> return setoverheadcheck(); leftAdjLayers(); ">
<html:hidden property = "job_Id" />
<html:hidden property = "fromflag" />
<html:hidden property = "job_type" />
<html:hidden property = "tabId" />

<logic:equal name ="ActivityListForm" property ="fromflag" value="PM">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
			 <tr>
	    			<td valign="top" width = "100%">
	    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
	    				<tr>
				    	     <%if(jobStatus.equals("Approved")){%>
							
								<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
							<%}else{%>
							
											<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
												<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
											<%}else{ %>
												<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
											<%}%>
							<%}%>
							
							   	<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
								<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>  
								<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
						</tr>
						
						<tr>
					        <td background="images/content_head_04.jpg" height="21" colspan="7">
					          <div id="breadCrumb">
						        		  <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "ActivityListForm" property = "msa_id"/>&Appendix_Id=<bean:write name = "ActivityListForm" property = "appendixid"/>"><bean:write name = "ActivityListForm" property = "msaname"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "ActivityListForm" property = "appendixid"/>&Name=<bean:write name = "ActivityListForm" property = "appendixname"/>&ref=<%=chkaddendum%>"><bean:write name = "ActivityListForm" property = "appendixname"/></a>
							</div>	    	  
							</td>
						</tr>
		 				<tr>
		 					<td colspan="7"><h2>
			 					<logic:equal name = "ActivityListForm" property = "job_type" value = "Addendum">
			 						<logic:equal name = "ActivityListForm" property = "fromflag" value = "PM">
						    			<bean:message bundle = "pm" key ="activity.copyactivity.job"/>&nbsp;<bean:message bundle = "pm" key ="activity.tabular.copyactivities"/>&nbsp;<bean:write name = "ActivityListForm" property = "jobName" />
						    		</logic:equal>
			 					</logic:equal>
			 					
			 					<logic:notEqual name = "ActivityListForm" property = "job_type" value = "Addendum">
						    		<logic:equal name = "ActivityListForm" property = "fromflag" value = "PM">
						    			<bean:message bundle = "pm" key ="activity.copyactivity.job"/>&nbsp;<bean:message bundle = "pm" key ="activity.tabular.copyactivities"/>&nbsp;<bean:write name = "ActivityListForm" property = "jobName" />
						    		</logic:equal>
						    	</logic:notEqual>	
		 					</h2></td>
		 					
		 				</tr>
		      		</table>
	   				</td>
				    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
					<td valign="top" width="155">
					
	    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
	        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              						<tr><td width="150">
	              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
	              							    <tr>
								               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
								                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
								               </tr>
	                  						</table>
	                  						</td>
	              						</tr>
	            					</table>
	            				</td>
	            		 	</tr>
	        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
	          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              						<tr><td nowrap="nowrap" colspan="2">
		                						<div id="featured1">
		                							<div id="featured">
														<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
													</div>								
		                						</div>		
												<span>
													<div id="filter">
	        											<table width="250" cellpadding="0" class="divnoview">
	                    									<tr><td width="50%" valign="top">
	                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
	                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
	                        										</table>
	                        									</td>
						                        			</tr>
	  													</table>											
													</div>
												</span>
											</td>
										</tr>
	            					</table>
	        					</td>
	    				 </tr>
	</table>
	</td>
	</tr>
	</table>
</logic:equal>
<logic:present name = "addendumflag" scope = "request">

<logic:notEqual name ="ActivityListForm" property ="fromflag" value="PM">

<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<!--  Menu Change 2014 -->
	<div id="menunav" style="visisbilty:visible;">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li><a href="JobEditAction.do?ref=inscopeview&amp;appendix_Id=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;Job_Id=0">Add Job</a></li>
        		<li>
        			<a href="#"><span>Bulk Job</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=<%= appendixid %>">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=<%= appendixid %>">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li>
				
	        		<% if( contractDocMenu != "" ) { %>
	        		<li>
	        			<a href="#"><span>Contract Documents</span></a>
	        			<ul>
							<%= contractDocMenu %>
	        			</ul>
	        		</li>
				<% } %>
				
        		<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Custom Fields</a></li>
        		<li><a href="AddCustRef.do?typeid=<%= appendixid %>&amp;type=Appendix&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Customer Reference</a></li>
        		<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">External Sales Agent</a></li>
        		<li>
        			<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;initialCheck=true&amp;pageType=jobsites">Job Sites</a></li>
        		<li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=<%= appendixid %>&amp;type=P&amp;page=appendixdashboard&amp;pageid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Update</a></li>
						
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Special Instructions/Conditions</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">Team</a></li>
        		<li><a href="OnlineBid.do?appendixId=<%= appendixid %>&amp;fromPage=Appendix">Online Bid</a></li>
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
        		<li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li>
				<li><a href="MSPCCReportAction.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">MSP C/C Report</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid%>&msaId=<%=msaid%>&fromPage=viewselector">Search</center></a></td>
						
  		</li>
  	
	</ul>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	     <table cellpadding="0" cellspacing="0" border="0" width = "100%">
			<tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		
								 	
				        </td>
			       
						<tr>
							
						</tr>
					
			       </tr>
			       
			       
		</table>		
   </td>
      
    <td width="35" valign="top"><img width="35" height="60" alt="" src="images/content_head_02.jpg" id="content_head_02"></td>
	<td width="155" valign="top">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr>
		          <td width="155" height="39" colspan="2" class="headerrow">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tbody><tr>
			                <td width="150">
			                	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				                   	<tbody><tr>
							            <td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('II');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" title="Scheduled" id="Image10" alt="Month" src="http://localhost:8080/Ilex/images/Icon_Scheduled.gif"></a></td>
	  									<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IO');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" title="Scheduled OverDue" id="Image10" alt="Month" src="images/Icon_Schedule_Overdued.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('ION');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" title="OnSite" id="Image10" alt="Month" src="images/Icon_Onsite.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IOF');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" title="Offsite" id="Image10" alt="Month" src="images/Icon_Ofsite.gif"></a></td>
				                    </tr>
			                  	</tbody>
			                  	</table>
			                </td>
			              </tr>
			          </tbody></table>
		          </td>
		        </tr>
        		<tr>
          		<td width="140" height="21" background="images/content_head_04.jpg">
          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              		<tbody><tr>
              		<td width="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li id="closeNow" name="closeNow"><a onclick="ShowDiv();" title="Open View" href="javascript:void(0)">My Views <img border="0" onclick="ShowDiv();" title="Open View" src="images/showFilter.gif"></a><a class="imgstyle" href="javascript:void(0)"><img border="0" onclick="hideDiv();" title="Close View" src="images/offFilter2.gif"></a></li>
									</div>								
	                			</div>		
						<span>
								<div class="divstyle" id="filter" style="">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tbody><tr>
		                      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		                          <tbody><tr>
                           	 <td class="filtersCaption" colspan="3">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										
									    	 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('AI');" value="AI" name="jobSelectedStatus">   
											 	Inwork<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IS');" value="IS" name="jobSelectedStatus">   
											 	To Be Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('II');" checked="checked" value="II" name="jobSelectedStatus">   
											 	Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IO');" checked="checked" value="IO" name="jobSelectedStatus">   
											 	Scheduled Overdue<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('ION');" checked="checked" value="ION" name="jobSelectedStatus">   
											 	Onsite<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IOF');" checked="checked" value="IOF" name="jobSelectedStatus">   
											 	Offsite<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('F');" value="F" name="jobSelectedStatus">   
											 	Complete<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('O');" value="O" name="jobSelectedStatus">   
											 	Closed<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('H');" value="H" name="jobSelectedStatus">   
											 	Hold<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('C');" value="C" name="jobSelectedStatus">   
											 	Cancelled<br> 
											 	
											
												
										<img width="100px" class="imagefilter" src="images/hrfilterLine.gif">
										<table cellpadding="0">
											<tbody><tr><td class="tabNormalText"><input type="radio" value="week" name="jobMonthWeekCheck" onclick="checkCustomSelection()">This Week</td></tr>
											<tr><td class="tabNormalText"><input type="radio"  checked="checked" value="month" name="jobMonthWeekCheck"  onclick="checkCustomSelection()">This Month</td></tr>	
											<tr><td class="tabNormalText"><input type="radio"  value="custom" name="jobMonthWeekCheck"  onclick="checkCustomSelection()">Custom</td></tr>	
											<tr id="drange" ><td class="tabNormalText" >
											 
												<table style="border: 0px">
													<tr>
													  <td  class="tabNormalText">From Date:</td>
													  <td  class="tabNormalText">
													
								<input class="textbox" type="text"  value="<%=new java.text.SimpleDateFormat("MM-dd-yyyy").format(new Date()) %>" readonly="true" name="fromDate" id="fromDate" size="10">
								
								<img style="cursor:pointer" onclick="javascript:NewCssCal('fromDate')" src="images/calendar.gif">
								
								</td>
													</tr>
													<tr>
													  <td  class="tabNormalText">To Date</td>
													 <td  class="tabNormalText">
								<input class="textbox" type="text"  value="<%=new java.text.SimpleDateFormat("MM-dd-yyyy").format(new Date()) %>" readonly="true" name="toDate" id="toDate" size="10">
								
								<img style="cursor:pointer" onclick="javascript:NewCssCal('toDate')" src="images/calendar.gif"></td>
													</tr>
												</table> 
												
											</td></tr>
											
											
											 
										</tbody></table>	
									</td>
	    					</tr>
                        </tbody></table></td>
                      <td><img width="1px" height="120" class="imagefilter" src="images/filterLine.gif"></td>
                      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td class="filtersCaption" colspan="3">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id="OwnerSpanId"> 
					   			
							    	
							    		
							    		
								    		
							    			 
							    		<input type="checkbox" onclick="javascript: checkingOwner('7');" checked="checked" value="7" name="jobSelectedOwners">   
										  Me
										  <br> 
									
									</span>
								
								
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img height="34" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="changeFilter();" class="divbutton" src="images/showBtn.gif"><br></a></td></tr>
                              <tr><td colspan="3"><img width="2" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="resetFilter();" class="divbutton" src="images/reset.gif"><br></a></td></tr>                              
                              <tr><td colspan="3"><img height="8" src="images/spacer.gif"></td></tr>
                              </tbody></table></td>
                        </tr>
  			</tbody></table>											
		</div>
	</span>
				</td>
	
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      </td>
 </tr>
 </tbody>
 </table>

















































<!--  End of Menu  -->

	<%-- <table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
		    <td valign="top" width = "100%">
				<table cellpadding="0" cellspacing="0" border="0" width = "100%">
					<tr>
					    <td class="top"> 
					      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					        <tr> 
					          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage1</center></a></td>
					          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
					     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid%>&msaId=<%=msaid%>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
					          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
					        </tr>
					      </table>
					    </td>
					</tr>
					<tr>
						<!-- BreadCrumb goes into this td -->
						<td background="images/content_head_04.jpg" height="21" colspan="23" width ="100%"></td>
					</tr>
		      	</table>
		    </td>
		    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
		    <td valign="top" width="155">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="155" height="39"  class="headerrow" conspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="150">
										<table width="100%" cellpadding="0" cellspacing="3" border="0">
											<tr>
												<td><img src="images/Icon_Scheduled.gif" width="31" title="Scheduled"/></td>
	  											<td><img src="images/Icon_Schedule_Overdued.gif" width="31" title="Scheduled OverDue"/></td>
												<td><img src="images/Icon_Onsite.gif" width="31" title="OnSite"/></td>
												<td><img src="images/Icon_Ofsite.gif" width="31" title="Offsite"/></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height="21" background="images/content_head_04.jpg" width="140">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="myView" colspan="2" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;My Views&nbsp;<img src="images/showFilter.gif" title="Open View" border="0"/><img src="images/offFilter2.gif" border="0" title="Close View"/></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table> --%>
<%-- <%@ include  file="/AppedixDashboardMenuScript.inc" %> --%>
</logic:notEqual>
 </logic:present>

<logic:notPresent name = "addendumflag" scope = "request">
<logic:notEqual name = "ActivityListForm" property = "fromflag" value = "PM">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		<tr>
	    	<td> 
		    	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0" height = "18">
		        	<tr align = "center"> 		    		
						<td class = "Ntoprow1" width = "100%">&nbsp;</td>		
		        	</tr>
		        	<tr>
			        	<td background="images/content_head_04.jpg" height="21" colspan="7">
					        <span id="breadCrumb">
					         	<a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
								<a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
								<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
								<a><span class="breadCrumb1">Copy Activities</a>
							</span>
						</td>
					</tr>
		      	</table>
	    	</td>
		</tr>
	</table>
</logic:notEqual>
</logic:notPresent>

<logic:equal name ="ActivityListForm" property ="fromflag" value="PM">	
 	<%@ include  file="/JobMenuScript.inc" %>
</logic:equal> 

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
	<tr>
		<td>
			<table border = "0" cellspacing = "1" cellpadding = "1" width="1200">
				<logic:present name = "addendumflag" scope = "request">
					<tr>
						<td>&nbsp;&nbsp;</td>
						<td colspan = "15" class = "message" height = "15">
						    	<logic:equal name = "addendumflag" value = "0">Addendum Created Successfully<script>parent.ilexleft.location.reload();</script></logic:equal>
					    </td>
				    </tr>
				    <logic:equal name = "ActivityListForm" property = "job_type" value = "Addendum">
				    		<logic:notEqual name = "ActivityListForm" property = "fromflag" value = "PM">
				    		<tr>
				    		<td>&nbsp;&nbsp;</td>
								<td colspan = "15" height = "30">
								<h1>
				    				<bean:message bundle = "pm" key = "activity.tabular.selectactivities"/>&nbsp;<a href = "JobDashboardAction.do?jobid=<bean:write name = "ActivityListForm" property = "job_Id" />"><bean:write name = "ActivityListForm" property = "jobName" /></a>&nbsp;<bean:message bundle = "pm" key = "activity.tabular.selectaddendumjob"/>&nbsp;<bean:message bundle = "pm" key = "activity.tabular.selectaddendumunder"/>&nbsp;<a href = "AppendixHeader.do?function=view&appendixid=<bean:write name = "ActivityListForm" property = "appendixid" />"><bean:write name = "ActivityListForm" property = "appendixname" /></a>&nbsp;<bean:message bundle = "pm" key = "activity.tabular.appendix"/>
				    			</h1>
				    		</td>
				    		</tr>	
				    		</logic:notEqual>
				    	</logic:equal>
				   </table>
			  </logic:present>
			  <logic:notPresent name = "addendumflag" scope = "request">
			  <tr>
			      <td height="6"></td>
				  <td colspan = "15"></td>
		      </tr>	
			  </logic:notPresent>
		</td>
	</tr>
	<logic:empty name="codes" scope = "request">
		  <tr>
		  	<td class='message' height='10' colspan='10'>
		  		&nbsp;&nbsp;&nbsp;&nbsp;No Activities.
		  	</td>
		  </tr>
	  </logic:empty> 	    
	<logic:present name = "codes" scope = "request">
	<tr>
		<td style="padding-left: 10px;">
			 <table border="0" align="left" cellpadding="0" cellspacing="0" >
				 <tr>
								 	
					<% 
					ArrayList list= (ArrayList) request.getAttribute("addendumjobnamelist");
					
					 		String[] tabNames = new String[list.size()];
					 		%>
					 		<input type = hidden name='tabsize' value='<%=list.size()%>'/>
					 		<%
					 		for(int j=0;j<list.size(); j++){
					 			tabNames[j] = ((AddendumJobName)list.get(j)).getAddendumjobname();
					 		}
				 	for(int tabIndex=0;tabIndex<tabNames.length;tabIndex++) {
				 	%>
				      <td id="TOP_TAB" width="10" class='leftRightBorder'>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td id="TOP_BUTTON" style="cursor:hand;"
										onClick="fun_HideShowBlock('<%=tabIndex%>','<%=tabIndex%>');" align="center" 
										nowrap ><img src="images/left-cor.gif"  width="5" height="18" border="0" ></td>
									<td id="TOP_BUTTON" style="cursor:hand;" 
										onClick="fun_HideShowBlock('<%=tabIndex%>','<%=tabIndex%>');" align="center"  class='drop' 
										nowrap bgcolor='#B5C8D9'><%=tabNames[tabIndex]%></td>
									<td id="TOP_BUTTON" style="cursor:hand;" 
										onClick="fun_HideShowBlock('<%=tabIndex%>','<%=tabIndex%>');" align="center" 
										nowrap><img src="images/right-cor.gif"   width="5" height="18" border="0"></td>
								</tr>
							</table>
						</td>
					<%
				 	}
					%>
					</tr>
				</table>
			</td>
		</tr>
				<logic:notPresent name = "codes" scope = "request">
					 <tr>
						<td>&nbsp;</td>
						<td colspan = "5" class = "message" height = "30">
	 						<bean:message bundle = "pm" key = "job.copy.activities.notpresent"/>
	 					</td>
	 				</tr>
 				</logic:notPresent>
			</logic:present>
		<%i=0;%>


		
	
 	
	<logic:present name = "codes" scope = "request">
 	<logic:iterate id = "ms" name = "codes">
	
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass ="Ntexto";
				backgroundhyperlinkclass = "Nhyperodd";
				comboclass = "combowhite";
				readonlyclass = "Nreadonlytextodd";
				readonlynumberclass = "Nreadonlytextnumberodd";
				csschooser = false;
			}
	
			else
			{
				csschooser = true;	
				comboclass = "combowhite";
				readonlyclass = "Nreadonlytexteven";
				backgroundhyperlinkclass = "Nhypereven";
				readonlynumberclass = "Nreadonlytextnumbereven";
				backgroundclass = "Ntexte";
			}
		%>
		<bean:define id = "val" name = "ms" property = "activity_Id" type = "java.lang.String"/>
		<bean:define id = "divcheckid" name = "ms" property = "temp_jobid" type = "java.lang.String"/>
		
		<% 
			if( !prevdivcheckid.equals( divcheckid ) ) { %>
		<% 		if( prevdivcheckid != "" ){
			%>
				<tr><td  class = "colLight">&nbsp;</td>
			<td colspan = "1" class = "colLight">
  				<html:submit property = "save" styleClass = "button_c" onclick = "return validateAll();"><bean:message bundle = "AM" key = "am.material.submit"/></html:submit>
				<% if(request.getAttribute("from") != null)	{%>
					<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();">
						<bean:message bundle="PRM" key="prm.button.back"/>
					</html:button>
				<%}%>
				<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "job.tabular.cancel"/></html:reset>
			</td>
			<td colspan = "13" class = "colLight">
				
			</td>
			
			
		</tr>
  	</table></td></tr> 
  			<%} if (div == 1){%>
						<logic:present name = "addendumflag" scope = "request">
							<tr><td><div id = "tab_<%=++myId%>" style="visibility:visible;POSITION:absolute;top:140;left:11; ">
						</logic:present>
						<logic:notPresent name = "addendumflag" scope = "request">
							<logic:equal name ="ActivityListForm" property ="fromflag" value="PM">
							<tr><td><div id = "tab_<%=++myId%>" style="visibility:visible;POSITION:absolute;top:100;left:11; ">
							</logic:equal>
							<logic:notEqual name ="ActivityListForm" property ="fromflag" value="PM">
							<tr><td><div id = "tab_<%=++myId%>" style="visibility:visible;POSITION:absolute;top:69;left:11; ">
							</logic:notEqual>
						</logic:notPresent>						
				<%}else{ %>
					<logic:present name = "addendumflag" scope = "request">
							<tr><td><div id = "tab_<%=++myId%>" style="visibility:visible;POSITION:absolute;top:140;left:11; ">
						</logic:present>
						<logic:notPresent name = "addendumflag" scope = "request">
							<logic:equal name ="ActivityListForm" property ="fromflag" value="PM">
							<tr><td><div id = "tab_<%=++myId%>" style="visibility:visible;POSITION:absolute;top:100;left:11; ">
							</logic:equal>
							<logic:notEqual name ="ActivityListForm" property ="fromflag" value="PM">
							<tr><td><div id = "tab_<%=++myId%>" style="visibility:visible;POSITION:absolute;top:69;left:11; ">
							</logic:notEqual>
						</logic:notPresent>	
				<%} %>
				<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" cellspacing = "1" cellpadding = "1" >
				
					 <logic:notEqual name = "ActivityListForm" property = "fromflag" value = "PM">  
						<!-- <tr><td>&nbsp; </td></tr>
						<tr><td>&nbsp; </td><td colspan = "12"><h1>
								<!-- <b><bean:write name = "ms" property = "job_name" />&nbsp;Activities 
								</b>
								</h1>
							</td>
						</tr> -->
					</logic:notEqual>
					
					<tr>  
				    <td class = "Ntryb" rowspan = "2">&nbsp; </td>
				    <td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.name"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.type"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.quantity"/></td>
					<td class = "Ntryb" colspan = "6"><bean:message bundle = "pm" key = "activity.tabular.estimatedcost"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.extendedprice"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.overhead"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.listprice"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.proformamargin"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.status"/></td>
				
				    </tr>  
				    
				<tr>
				
    				<td class = "Ntryb"> 
      					<bean:message bundle = "pm" key = "activity.tabular.materials"/>
    				</td>
    				
    				<td class = "Ntryb" align = "center"> 
      					<bean:message bundle = "pm" key = "activity.tabular.cnslabor"/>
    				</td>
    
    				<td class = "Ntryb"> 
      					<bean:message bundle = "pm" key = "activity.tabular.contractlabor"/>
    				</td>
    				
    				<td class = "Ntryb"> 
      					<bean:message bundle = "pm" key = "activity.tabular.freight"/>
    				</td>
    
    				<td class = "Ntryb"> 
      					<bean:message bundle = "pm" key = "activity.tabular.travel"/>
    				</td>
	  
	  				<td class = "Ntryb"> 
      					<bean:message bundle = "pm" key = "activity.tabular.estimatedtotal"/>
    				</td>
				</tr>
				
			<% div++; } %>
		
		
		<tr>		
			
			<td class = "<%= backgroundclass %>"> 
				<html:checkbox property = "check" value = "<%= val %>" />
    		</td>
    		
    		<html:hidden name = "ms" property = "activity_Id"/>
			<html:hidden name = "ms" property = "flag" />
			<html:hidden name = "ms" property = "bidFlag" />
			<bean:define id="estimatedMaterialCost" name="ms" property="estimated_materialcost"></bean:define>
			<bean:define id="estimatedCnsFieldCost" name="ms" property="estimated_cnshqlaborcost"></bean:define>
			<bean:define id="estimatedContractCost" name="ms" property="estimated_contractfieldlaborcost"></bean:define>
			<bean:define id="estimatedContractFieldLaborCost" name="ms" property="estimated_cnsfieldlaborcost"></bean:define>
			<bean:define id="estimatedFreightCost" name="ms" property="estimated_freightcost"></bean:define>
			<bean:define id="estimatedTravelCost" name="ms" property="estimated_travelcost"></bean:define>
			<bean:define id="estimatedTotalCost" name="ms" property="estimated_totalcost"></bean:define>
			<bean:define id="extendedPrice" name="ms" property="extendedprice"></bean:define>
			
			<%  
				if( activity_size == 1)
				{
					valstr = "javascript: document.forms[0].check.checked = true;updateactivitytype( this , '' );"; 
					displaycombo = "javascript: displaycombo( this , '' );";
					schdayscheck = "javascript: document.forms[0].check.checked = true;tempprevquantity = document.forms[0].quantity.value;";
					multiplyqty = "javascript: validateNumbers(this);return multiplyquantity("+estimatedMaterialCost+","
					+estimatedContractFieldLaborCost+","
					+estimatedCnsFieldCost+","
					+estimatedContractCost+","
					+estimatedFreightCost+","
					+estimatedTravelCost+","
					+estimatedTotalCost+","
					+extendedPrice+","
					+" null );";
				}
				else if( activity_size > 1 )
				{
					valstr = "javascript:document.forms[0].check['"+i+"'].checked = true;updateactivitytype( this ,'"+i+"' );"; 
					schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;tempprevquantity = document.forms[0].quantity['"+i+"'].value;";
					displaycombo = "javascript: displaycombo( this , '"+i+"' );";
					multiplyqty = "javascript: validateNumbers(this);return multiplyquantity("+estimatedMaterialCost+","
								+estimatedContractFieldLaborCost+","
								+estimatedCnsFieldCost+","
								+estimatedContractCost+","
								+estimatedFreightCost+","
								+estimatedTravelCost+","
								+estimatedTotalCost+","
								+extendedPrice+","
								+" '"+i+"');";	
				}	
			%>
			
			<td class = "<%= backgroundhyperlinkclass %>" id="ActivityName<%=i%>">  
				<bean:write name = "ms" property = "name" />
			</td>
			<html:hidden name = "ms" property = "name" />
			
			<td class = "<%= backgroundclass %>">  
				
				<logic:equal name = "ms" property = "activitytype" value = "Overhead">
					<html:text styleId = '<%= "activitytype"+i%>' name = "ms" size = "8"  readonly = "true" property = "activitytype"  styleClass = "<%= readonlyclass %>" />	
					<html:hidden name = "ms" property = "activitytypecombo" />
				</logic:equal>
				
				<logic:notEqual name = "ms" property = "activitytype" value = "Overhead">
					<logic:notEqual name = "ActivityListForm" property = "job_type" value = "Default">
				  		<html:select styleId = '<%= "activitytypecombo"+i%>' name = "ms" property = "activitytypecombo" size = "1" styleClass = "<%= comboclass %>" onchange = "<%= valstr %>">
							<html:optionsCollection  property = "activity" value = "value" label = "label"/> 
						</html:select>
						<html:hidden styleId = '<%= "activitytype"+i%>' name = "ms" property = "activitytype" />
					</logic:notEqual>
					
					<logic:equal name = "ActivityListForm" property = "job_type" value = "Default">
						 <html:text styleId = '<%= "activitytype"+i%>' name = "ms" size = "8"  readonly = "true" property = "activitytype"  styleClass = "<%= readonlyclass %>" />	
						 <html:hidden name = "ms" property = "activitytypecombo" />
			   		</logic:equal>
			   		
				</logic:notEqual>
			</td>
			
			
			<td class = "<%= backgroundclass %>">  
				<logic:equal name = "ActivityListForm" property = "job_type" value = "Default">
					<html:text styleId = '<%= "quantity"+i%>' name = "ms" size = "4"  property = "quantity" styleClass = "text" onkeypress="return onlyNumbers(event);" onfocus = "<%= schdayscheck %>" onblur = "<%= multiplyqty %>" />
				</logic:equal>
				
				<logic:notEqual name = "ActivityListForm" property = "job_type" value = "Default">
					<logic:equal name = "ms" property = "activitytype" value = "Overhead">
						<html:text styleId = '<%= "quantity"+i%>' name = "ms" size = "4"  property = "quantity" styleClass = "<%= readonlynumberclass %>" readonly = "true" />	
					</logic:equal>
					
					<logic:notEqual name = "ms" property = "activitytype" value = "Overhead">
						<html:text styleId = '<%= "quantity"+i%>' name = "ms" size = "4"  property = "quantity" styleClass = "Ntextbox" onkeypress="return onlyNumbers(event);" onfocus = "<%= schdayscheck %>" onblur = "<%= multiplyqty %>" />
					</logic:notEqual>
				</logic:notEqual>
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_materialcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_materialcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>" style="padding-right: 4px;padding-left: 40px;">  
				<html:text styleId = '<%= "estimated_cnsfieldhqlaborcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_cnsfieldhqlaborcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<html:hidden name = "ms" property = "estimated_cnsfieldlaborcost" />
			<html:hidden name = "ms" property = "estimated_cnshqlaborcost" />
			
			
			<td class = "<%= backgroundclass %>" style="padding-right: 4px;padding-left: 40px;">  
				<html:text styleId = '<%= "estimated_contractfieldlaborcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_contractfieldlaborcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_freightcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_freightcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_travelcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_travelcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_totalcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_totalcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "extendedprice"+i%>' name = "ms" size = "10"  readonly = "true" property = "extendedprice" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "overheadcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "overheadcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "listprice"+i%>' name = "ms" size = "10"  readonly = "true" property = "listprice" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "proformamargin"+i%>' name = "ms" size = "5"  readonly = "true" property = "proformamargin" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">
				<bean:write name = "ms" property = "status"/>
			</td>
		</tr>
  		
  		<% 
			prevdivcheckid = divcheckid;
			i++;  
		%>
	   
	  </logic:iterate>

		<tr>
		    <td  class = "colLight">&nbsp;</td>
			<td colspan = "1" class = "colLight">
  				<html:submit property = "save" styleClass = "button_c" onclick="return validateAll();"><bean:message bundle = "AM" key = "am.material.submit"/></html:submit>
				 <% 
				if( request.getAttribute( "from" ) != null )
				{%>
					
						<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					
				<%} 
				%> 
				<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "job.tabular.cancel"/></html:reset>
			
			</td>
			<td  colspan = "13"  class = "colLight">			
			</td>
		
			
			
		</tr>
		</table>
		</div>
	 
	  
  
	
		</logic:present> 
	</table>
	
	 
		<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'copyactivitypage'/>
		 </jsp:include>	
		 
</html:form>
<script>



function updateactivitytype (elem , i)
{
	var textid = elem.id;
	var comboid = document.getElementById ( elem.id );
	if ( i!= null)
	{	
		textid = textid.substring ( 0,textid.indexOf ( 'combo' ) ) + textid.substring ( textid.indexOf ( 'combo' ) + 5 , textid.length );
		document.getElementById ( textid ).value = comboid.options[comboid.selectedIndex].text;
		//comboid.className = 'combooddhidden';
	}
	else
	{
		textid = textid.substring (0, textid.indexOf ( 'combo' ) );
		document.getElementById ( textid ).value = comboid.options[comboid.selectedIndex].text;
		//comboid.className = 'combooddhidden';
	}
	return;
}

function setoverheadcheck()
{	
	if( <%= activity_size %> == 1 ) {
		if( document.forms[0].activitytype.value == 'Overhead' ) {
			document.forms[0].check.checked = true;
			document.forms[0].check.disabled = true;

		}
	}
	
<%-- 	if( <%= activity_size %> == 1 ) { --%>
// 		if( document.getElementById("ActivityName1").innerHTML == 'Freight' ) {
// 			document.forms[0].check.checked = true;
//  			document.forms[0].check.disabled = true;

// 		}
// 	}
	
	if( <%= activity_size %> > 1 ) {
		for( var i = 0; i < document.forms[0].check.length;i++ ) {
			if( document.forms[0].activitytype[i].value == 'Overhead' ) {
				document.forms[0].check[i].checked = true;
				document.forms[0].check[i].disabled = true;
			}
			//alret("--");
// 			if( (document.getElementById("ActivityName"+i).innerHTML).trim() == 'Freight' ) {
// 				document.forms[0].check[i].checked = true;
//  				document.forms[0].check[i].disabled = true;
// 			}
		}
	}
	
	
		defaultAddendumdisplay();
		return true;
	

}

function enableOverhead(){
	if( <%= activity_size %> == 1 ) {
		if( document.forms[0].activitytype.value == 'Overhead' ) {
			document.forms[0].check.disabled = false;
		}
	}
	
<%-- 	if( <%= activity_size %> == 1 ) { --%>
// 		if( document.getElementById("ActivityName1").innerHTML == 'Freight' ) {
// 			document.forms[0].check.checked = true;
//  			document.forms[0].check.disabled = false;

// 		}
// 	}
	
	if( <%= activity_size %> > 1 ) {
		for( var i = 0; i < document.forms[0].check.length;i++ ) {
			if( document.forms[0].activitytype[i].value == 'Overhead' ) {
				document.forms[0].check[i].disabled = false;
			}
			
// 			if( (document.getElementById("ActivityName"+i).innerHTML).trim() == 'Freight' ) {
// 				document.forms[0].check[i].checked = true;
//  				document.forms[0].check[i].disabled = false;
// 			}
		}
	}
}

function defaultAddendumdisplay() { //v3.0
	
	
		var tableRow = document.getElementsByTagName('div');
		if(document.forms[0].fromflag.value == 'PRM'){
			
			if (tableRow.length==0) 
			{ 
				return; 
			}
			for (var k = tableRow.length-1; k >-1; k--) 
			{
				viewChange(tableRow[k].getAttributeNode( 'id' ).value );
			}
		
			viewChange('tab_0' );
		}
	return true;
}
	
	
	
	function viewChange() { //v3.0
	var tableRow = document.getElementsByTagName( 'div' );
	for (var k = 0; k < tableRow.length; k++) 
	{
	 	if(tableRow[k].getAttributeNode('id').value != '')
	 		MM_showHideLayers(tableRow[k].getAttributeNode( 'id' ).value , '' , 'hide' );
	}	
	
	var args = viewChange.arguments;
	
	MM_showHideLayers( args[0] , '' , 'show' );
	
	 $('#menunav').show();
	 $('#menunav').css('visibility', 'visible');
	 
	return true;
}

 function MM_showHideLayers() { //v3.0

  var i,p,v,obj,args=MM_showHideLayers.arguments;
 
  for (i=0; i<(args.length-2); i+=3)
   	if ((obj=MM_findObj(args[i]))!=null) 
  	{ 
  		v=args[i+2];
	    if (obj.style) 
	    { 
	    	obj=obj.style; 
	    	v=(v=='show')?'visible':
	    		(v='hide')?'hidden':v;
	    }
    	obj.visibility=v;
    }
  
}

function validateAll() {
	var submitflag = 'false';
	if( <%= activity_size %> != 0 ) {
		if( <%= activity_size %> == 1 ) {
			if( !document.forms[0].check.checked ) {
					alert("Please select an activity to add");
					return false;
			 }

			 if(document.forms[0].activitytype.value == '-Select-') {
			 	alert('Please select activity type');
			 	return false;
			 }
		}
  	 else {
			for( var i = 0; i<document.forms[0].check.length; i++ ) {
		  		if( document.forms[0].check[i].checked ) {
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' ) {
					alert( "Please select an activity to add" );
					return false;
				}
			for( var i = 0; i<document.forms[0].check.length; i++ ) {
			if(document.forms[0].check[i].checked && document.forms[0].activitytype[i].value == '-Select-'){
	  				alert('Please select activity type');
	  				document.forms[0].activitytype[i].focus();
	  				return false;
		  		}	
			}	
		}
	}
enableOverhead();	
return true;		  	
}

function validate()
{
	if( <%= activity_size %> != 0 ) {
		var submitflag = 'false';
		if( <%= activity_size %> == 1 ) {
			if( !document.forms[0].check.checked ) {
					alert("Please select an activity to add");
					return false;
			 }
			}
  	 else {
			for( var i = 0; i<document.forms[0].check.length; i++ ) {
		  		if(document.forms[0].check[i].checked ) {
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' ) {
					alert( "Please select an activity to add" );
					return false;
				}
			for( var i = 0; i<document.forms[0].check.length; i++ ) {
			if(document.forms[0].check[i].checked && document.forms[0].activitytype[i].value == '-Select-'){
	  				alert('Please select activity type');
	  				document.forms[0].activitytype[i].focus();
	  				return false;
		  		}	
			}				
				
		  	}
		  }	
	return true;
}


function callcopy( prevactivitytype , index )
{
	if( <%= activity_size %> > 1 )
	{
		var overheadindex = '';
		var sumreqextendedprice = "0.0000";
		
		if( prevactivitytype == 'Required' )
		{
			document.forms[0].overheadcost[index].value = parseFloat( '0.0000' );
			document.forms[0].listprice[index].value = round_func( parseFloat( document.forms[0].extendedprice[index].value ) + parseFloat( document.forms[0].overheadcost[index].value ) );
			
			if(  parseFloat( document.forms[0].listprice[index].value ) > parseFloat( '0.0000' ) )
			{
				document.forms[0].proformamargin[index].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[index].value ) / parseFloat( document.forms[0].listprice[index].value ) ) );
			}
		}
		
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytype[i].value == 'Overhead' )
			{
				overheadindex = i;
			}
		}
		
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytype[i].value == "Required" )
			{
				sumreqextendedprice = round_func( parseFloat( sumreqextendedprice ) + parseFloat( document.forms[0].extendedprice[i].value ) );	
			}
		}
		
		if( overheadindex != '' )
		{
			if( parseFloat( sumreqextendedprice ) > 0 )
			{
				for( var i = 0; i < document.forms[0].check.length; i++ )
				{
					if( document.forms[0].activitytype[i].value == "Required" )
					{
						document.forms[0].overheadcost[i].value = round_func( ( parseFloat( document.forms[0].extendedprice[i].value ) * parseFloat( document.forms[0].extendedprice[overheadindex].value ) ) / parseFloat( sumreqextendedprice ) ); 	
						document.forms[0].check[i].checked = true;
						document.forms[0].listprice[i].value = round_func( parseFloat( document.forms[0].extendedprice[i].value ) + parseFloat( document.forms[0].overheadcost[i].value ) );
	
						if(  parseFloat( document.forms[0].listprice[i].value ) >  parseFloat( "0.0000" ) )
						{
							document.forms[0].proformamargin[i].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[i].value ) / parseFloat( document.forms[0].listprice[i].value ) ) );
						}
					}
				}		
			}
		}
			
	}
	
}


function multiplyquantity(materialCost, 
		cnsFieldCost, cnshqlLaborCost, contractLabor,
		freightCost, travelCost, totalCost, extendedCost,
		index )
{
	if( index != null )
	{
		if( document.forms[0].quantity[index].value == "" )
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.blankquantity"/>" );
			document.forms[0].quantity[index].focus();
			return false;
		}
		if( isFloat( document.forms[0].quantity[index].value ) )
		{
			if( !(document.forms[0].quantity[index].value > 0 ) )
			{
				alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
				document.forms[0].quantity[index].value = '';
				document.forms[0].quantity[index].focus();
				return false;
			}
			
			document.forms[0].estimated_materialcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(materialCost)));
			
			document.forms[0].estimated_cnsfieldlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( cnsFieldCost)));		
			
			document.forms[0].estimated_cnshqlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(cnshqlLaborCost)));		
			document.forms[0].estimated_cnsfieldhqlaborcost[index].value = round_func( parseFloat( document.forms[0].estimated_cnsfieldlaborcost[index].value ) + parseFloat( document.forms[0].estimated_cnshqlaborcost[index].value ) );
			
			document.forms[0].estimated_contractfieldlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(contractLabor)));		
			document.forms[0].estimated_freightcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(freightCost)));		
			document.forms[0].estimated_travelcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(travelCost)));		
			document.forms[0].estimated_totalcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(totalCost)));			
			document.forms[0].extendedprice[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( extendedCost )));			
			if( document.forms[0].activitytype[index].value == "Optional" ) 
			{
				document.forms[0].listprice[index].value = round_func( parseFloat( document.forms[0].extendedprice[index].value ) + parseFloat( document.forms[0].overheadcost[index].value ) );
				if(  parseFloat( document.forms[0].listprice[index].value ) > 0 )
				{
					document.forms[0].proformamargin[index].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[index].value ) / parseFloat( document.forms[0].listprice[index].value ) ) );
				}
			}
			else
			{
				
				calnew( index );
			}
		}
		
		else
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
			document.forms[0].quantity[index].value = '';
			document.forms[0].quantity[index].focus();
			return false;
		}
	}
	
	else
	{
		if( document.forms[0].quantity.value == "" )
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.blankquantity"/>" );
			document.forms[0].quantity.focus();
			return false;
		}
		
		if( isFloat( document.forms[0].quantity.value ) )
		{
			if( !(document.forms[0].quantity.value > 0 ) )
			{
				alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
				document.forms[0].quantity.value = '';
				document.forms[0].quantity.focus();
				return false;
			}
			
			
			
			document.forms[0].estimated_materialcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(materialCost)) );
			
			document.forms[0].estimated_cnsfieldlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( cnsFieldCost)));				
			document.forms[0].estimated_cnshqlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(cnshqlLaborCost)));			
			document.forms[0].estimated_cnsfieldhqlaborcost.value = round_func( parseFloat( document.forms[0].estimated_cnsfieldlaborcost.value ) + parseFloat( document.forms[0].estimated_cnshqlaborcost.value ) );
			
			
			document.forms[0].estimated_contractfieldlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( contractLabor) ) );		
			document.forms[0].estimated_freightcost.value = round_func( ( parseFloat(  document.forms[0].quantity.value ) * parseFloat(freightCost)));		
			document.forms[0].estimated_travelcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(travelCost)));			
			document.forms[0].estimated_totalcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(totalCost)));				
			document.forms[0].extendedprice.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( extendedCost )));			
			calnew( index );
		}
		else
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
			document.forms[0].quantity.value = '';
			document.forms[0].quantity.focus();
			return false;
		}
	}	
}



function calnew( index )
{	
	var overheadindex = '';
	for( var i = 0; i < document.forms[0].check.length; i++ )
	{
		if( document.forms[0].activitytype[i].value == 'Overhead' )
			overheadindex = i;
	}
	
	
	if( overheadindex == '' )
		return false;
			
	if( parseFloat( document.forms[0].extendedprice[overheadindex].value ) > 0 ) 
	{	
		var tempreqextendedprice = "0.0";
		
		
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytype[i].value == "Required" )
			{
				if( document.forms[0].extendedprice[i].value == '0.0000' )
				{
					
					document.forms[0].overheadcost[i].value = '0.0000';
					document.forms[0].listprice[i].value = '0.0000';
					document.forms[0].proformamargin[i].value = '0.0000';
				}
				
				tempreqextendedprice = round_func( parseFloat( tempreqextendedprice ) + parseFloat( document.forms[0].extendedprice[i].value ) );	
			}
		}
		
		if( parseFloat( tempreqextendedprice ) > 0 )
		{	
			for( var i = 0; i < document.forms[0].check.length; i++ )
			{
				if( document.forms[0].activitytype[i].value == "Required" )
				{
					document.forms[0].overheadcost[i].value = round_func( ( parseFloat( document.forms[0].extendedprice[i].value ) * parseFloat( document.forms[0].extendedprice[overheadindex].value ) ) / parseFloat( tempreqextendedprice ) ); 	
					//document.forms[0].check[i].checked = true;
					document.forms[0].listprice[i].value = round_func( parseFloat( document.forms[0].extendedprice[i].value ) + parseFloat( document.forms[0].overheadcost[i].value ) );
					//alert(document.forms[0].listprice[i].value);
					if(  parseFloat( document.forms[0].listprice[i].value ) > "0.0" )
					{
						document.forms[0].proformamargin[i].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[i].value ) / parseFloat( document.forms[0].listprice[i].value ) ) );
					}
				}
			}		
		}
	}	
	else
	{
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytype[i].value != "Overhead" ) 
			{
				//document.forms[0].check[i].checked = true;
				document.forms[0].listprice[i].value = round_func( parseFloat( document.forms[0].extendedprice[i].value ) + parseFloat( document.forms[0].overheadcost[i].value ) );

				if(  parseFloat( document.forms[0].listprice[i].value ) > parseFloat( "0.0" ) )
				{
					document.forms[0].proformamargin[i].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[i].value ) / parseFloat( document.forms[0].listprice[i].value ) ) );
				}
			}
		}		
	}
}



function Backactionjob()
{
	document.forms[0].action = "JobDashboardAction.do?jobid=<bean:write name = "ActivityListForm" property = "job_Id" />" ;
	document.forms[0].submit();
	return true;
}

</script>
<script>
function ShowDiv()
{
if(document.forms[0].fromflag.value=='PM'){
	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";
}
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
if(document.forms[0].fromflag.value=='PM'){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;

}
}

function onlyNumbers(e)
{
	var keynum; var keychar; var numcheck;
	
	if(window.event){ // IE
		keynum = e.keyCode;
	}
	keychar = String.fromCharCode(keynum);
	numcheck = /\d/;
	return numcheck.test(keychar);
}
function validateNumbers(obj)
{
	if(parseInt(obj.value) != obj.value);{
		obj.value=parseInt(obj.value);
	}
	if(!isNaN(obj.value)){
		return true;
	}
	else{
		obj.value = '';
	}
}

</script>



</body>

</html:html>

