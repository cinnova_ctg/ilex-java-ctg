<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating and  updating  Activities.
*
-->
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<% 
	int activity_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 
	String appendixname = "";
	String fromflag = ""; // This flag will be used only when copy activity from job dashboard through New Activity link      
	String Appendix_Id ="";
                         
	if( request.getAttribute( "appendixname" )!= null )
		appendixname= request.getAttribute("appendixname").toString();
	
	if( request.getAttribute( "from" ) != null )
		request.setAttribute( "from" , request.getAttribute( "from" ).toString() );

	if(request.getAttribute("Appendix_Id")!=null)
		Appendix_Id= request.getAttribute("Appendix_Id").toString();
%>

<html:html>

<head>

	<title><bean:message bundle = "pm" key = "activity.tabular.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language="JavaScript" src="javascript/ilexGUI.js"></script>
	<script>
		var tempprevquantity = '';
		var tempprevactivitytype = '';
	</script>

<%@ include file = "/NMenu.inc" %> 
</head>
<%@ include  file="/JobMenu.inc" %>

<%
	String backgroundclass = null;
	String backgroundhyperlinkclass = null;
	String comboclass = null;
	boolean csschooser = true;
	String readonlyclass = null;
	String readonlynumberclass = null;
	String displaycombo = null;
	String valstr = null;
	String datecheck = null;
	String schdayscheck = null;
	String multiplyqty = null;
	int i = 0;
	
%>
<body onload = "return setoverheadcheck();leftAdjLayers();">
<html:form action = "/CopyActivityAction">

<html:hidden property = "job_Id" />
<html:hidden property = "ref" /> 
<html:hidden property = "activity_cost_type" />
<html:hidden property = "job_type" />
<html:hidden property = "fromflag" />

<div id="menunav">
    <ul>
        <%
        int esaCount = 0;
	if(request.getAttribute("esaCount") != null) {
            esaCount = Integer.parseInt(request.getAttribute("esaCount").toString());
	}

        if(jobStatus.equals("Approved")){
            %>
            <li><a href="javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" class="drop"><bean:message bundle = "pm" key = "job.detail.edit"/></a></li>
            <%
        } else {
            if(job_type.equals("Default") || job_type.equals("Addendum") || appendixtype.equals("NetMedX")) {
                %>
                <li><a href="javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" class="drop"><bean:message bundle = "pm" key = "job.detail.edit"/></a></li>
                <%
            } else {
                %>
                <li><a href="JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" class="drop"><bean:message bundle = "pm" key = "job.detail.edit"/></a></li>
                <%
            }
        }
        %>
        <li>
            <a href="#" class="drop"><span>Manage</span></a>
            <ul>
                <%
                if((String) request.getAttribute("jobStatusList") != "") {
                    %>
                    <li>
                        <a href="#"><bean:message bundle = "pm" key = "job.detail.menu.changestatus.new"/></a>
                        <%= (String) request.getAttribute("jobStatusList") %>
                    </li>
                    <%
                } else {
                    %>
                    <li><a href="#"><bean:message bundle = "pm" key = "job.detail.menu.changestatus.new"/></a></li>
                    <%
                }
                %>
                <li><a href="JobDetailAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Job_Id=<%= Job_Id%>"><bean:message bundle = "pm" key = "job.detail.menu.viewdetails"/></a></li>
                <%
                if(job_type.equals("Default")) {
                    %>
                    <li><a href="javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.managenosite"/>');"><bean:message bundle = "pm" key = "job.detail.menu.managesiteinfo"/></a></li>
                    <%
                } else {
                    %>
                    <li><a href="SiteAction.do?addendum_id=<%=addendum_id%>&ref=Add&ref1=<%=chkaddendum%>&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id%>&fromProposalManag=yes"><bean:message bundle = "pm" key = "job.detail.menu.managesiteinfo"/></a></li>
                    <%
                }
                %>
                <li>
                    <a href="#"><bean:message bundle = "pm" key = "job.detail.menu.comments"/></a>
                    <ul>
                        <li><a href="javascript:jobViewComments();"><bean:message bundle = "pm" key = "job.detail.menu.viewall"/></a></li>
                        <li><a href="javascript:jobAddComment();"><bean:message bundle = "pm" key = "job.detail.menu.addcomment"/></a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><bean:message bundle = "pm" key = "job.detail.menu.documents"/></a>
                    <ul>
                        <li><a href="javascript:jobViewDocuments();"><bean:message bundle = "pm" key = "job.detail.menu.view"/></a></li>
                        <li><a href="javascript:jobUpload();"><bean:message bundle = "pm" key = "job.detail.menu.upload"/></a></li>
                    </ul>
                </li>
                <%
                if (job_type.equals("Default")) {
                    %>
                    <li>
                        <a href="#"><bean:message bundle = "AM" key = "am.summary.Partnerinfo"/></a>
                        <ul>
                            <li><a href="PartnerSOWAssumptionList.do?&fromId=<%= Job_Id %>&viewType=sow&fromType=pm_act"><bean:message bundle = "AM" key="am.Partnerinfo.sow"/></a></li>
                            <li><a href="PartnerSOWAssumptionList.do?&fromId=<%= Job_Id %>&viewType=assumption&fromType=pm_act"><bean:message bundle = "AM" key="am.Partnerinfo.assumptions"/></a></li>
                        </ul>
                    </li>
                    <%
                }
                %>
                <li><a href="javascript:jobDel();"><bean:message bundle = "pm" key = "job.detail.menu.deletejob"/></a></li>
            </ul>
        </li>
        <li>
            <a href="#" class="drop"><span><bean:message bundle = "pm" key = "job.detail.activities"/></span></a>
            <ul>
                <li><a href="ActivityUpdateAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendumactivity%>&Job_Id=<%= Job_Id %>"><bean:message bundle = "pm" key="job.detail.manage"/></a></li>
                <%
                if (job_type.equals("Addendum") || job_type.equals("Newjob") || job_type.equals("inscopejob")) { 
                    if (job_type.equals("Addendum") || job_type.equals("Newjob")) {
                        if (job_type.equals("Addendum")) {
                            if (chkadd.equals("detailjob")) {
                                from = "PM";
                            } else {
                                from = "PRM";
                            }
                        } else {
                            from = "PM";
                        }
                        %>
                        <li><a href="CopyAddendumActivityAction.do?from=<%= from %>&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>"><bean:message bundle = "pm" key = "activity.detail.copyactivities"/></a></li>
			<%
                    } else {
                        %>
                        <li><a href="CopyAddendumActivityAction.do?from=PRM&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>"><bean:message bundle = "pm" key = "activity.detail.copyactivities"/></a></li>
			<%
                    }
                } else {
                    %>
                    <li><a href="CopyActivityAction.do?ref=view&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>"><bean:message bundle = "pm" key = "activity.detail.copyactivities"/></a></li>
                    <%
                }
                 if (Integer.parseInt(  (String)request.getAttribute("esaCount") ) <= 0) {
		
                    %>
                    <li><a href="javascript:alert('No ESA is defined for this appendix.');">Commissions</a></li>
                    <%
                } else {
                    %>
                    <li><a href="ESAActivityCommissionAction.do?ref=unspecified&jobRef=detailjob&appendixId=<%= Appendix_Id %>&jobId=<%= Job_Id %>">Commissions</a></li>
                    <%
                }
                %>
            </ul>
        </li>
    </ul>
</div>
            
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
    <tr>
        <td valign="top" width = "100%">
        <table cellpadding="0" cellspacing="0" border="0" width = "100%">
					
					<tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "ActivityListForm" property = "msa_id"/>&Appendix_Id=<bean:write name = "ActivityListForm" property = "appendixId"/>"><bean:write name = "ActivityListForm" property = "msaname"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "ActivityListForm" property = "appendixId"/>&Name=<bean:write name = "ActivityListForm" property = "appendixname"/>&ref=<%=chkaddendum%>"><bean:write name = "ActivityListForm" property = "appendixname"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
	 					<logic:present name = "copylibactivities" scope = "request">
	 						<logic:equal name = "copylibactivities" value = "true">
	 							<td colspan="7"><h2>Job&nbsp;<bean:message bundle = "pm" key ="activity.tabular.new.selectdefaultactivity"/></td>
	 						</logic:equal>
	 					</logic:present>
	 				</tr>
	      		</table>
   				</td>
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>	

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
	<tr>
		<td>
			<table border = "0" cellspacing = "1" cellpadding = "1">
				
				  		<logic:present name = "copydefaultjobactivity" scope = "request">
				  		<tr>
						<td>&nbsp;</td>
						
						<td colspan = "6" height = "30"><h1>
					    	<logic:equal name = "copydefaultjobactivity" value = "true">
					    		<bean:message bundle = "pm" key = "activity.tabular.newjobactivity"/>&nbsp;<bean:message bundle = "pm" key = "activity.tabular.forJob"/>:&nbsp;<bean:write name = "ActivityListForm" property = "jobName" /> 
					    	</logic:equal>
					    	</h1>
					    	</td>
					    	</tr>
						</logic:present>
						
						
						 <logic:notPresent name = "codes" scope = "request">
						 <tr>
						<td>&nbsp;</td>
						<td colspan = "5" class = "message" height = "30">
		 					<bean:message bundle = "pm" key = "job.copy.activities.notpresent"/>
		 					</td>
		 					</tr>
		 				</logic:notPresent>
				
				
				<tr>  
				    <td rowspan = "2">&nbsp; </td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.name"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.type"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.quantity"/></td>
					<td class = "Ntryb" colspan = "6"><bean:message bundle = "pm" key = "activity.tabular.estimatedcost"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.extendedprice"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.overhead"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.listprice"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.proformamargin"/></td>
					<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.status"/></td>
				
				</tr>  
				    
				<tr> 
    				<td class = "Ntryb"> 
      					<div align = "center"><bean:message bundle = "pm" key = "activity.tabular.materials"/></div>
    				</td>
    				
    				<td class = "Ntryb"> 
      					<div align = "center"><bean:message bundle = "pm" key = "activity.tabular.cnslabor"/></div>
    				</td>
    
    				<td class = "Ntryb"> 
      					<div align = "center"><bean:message bundle = "pm" key = "activity.tabular.contractlabor"/></div>
    				</td>
    				
    				<td class = "Ntryb"> 
      					<div align = "center"><bean:message bundle = "pm" key = "activity.tabular.freight"/></div>
    				</td>
    
    				<td class = "Ntryb"> 
      					<div align = "center"><bean:message bundle = "pm" key = "activity.tabular.travel"/></div>
    				</td>
	  
	  				<td class = "Ntryb"> 
      					<div align = "center"><bean:message bundle = "pm" key = "activity.tabular.estimatedtotal"/></div>
    				</td>
				</tr>



					
 	<logic:present name = "codes" scope = "request"> 

 	<logic:iterate id = "ms" name = "codes">
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass ="Ntexto";
				backgroundhyperlinkclass = "Nhyperodd";
				comboclass = "combooddhidden";
				readonlyclass = "Nreadonlytextodd";
				readonlynumberclass = "Nreadonlytextnumberodd";
				csschooser = false;
			}
	
			else
			{
				csschooser = true;	
				comboclass = "comboevenhidden";
				readonlyclass = "Nreadonlytexteven";
				backgroundhyperlinkclass = "Nhypereven";
				readonlynumberclass = "Nreadonlytextnumbereven";
				backgroundclass = "Ntexte";
			}
		%>
		<bean:define id = "val" name = "ms" property = "activity_Id" type = "java.lang.String"/>
		<tr>		
			
			<td class = "Nlabeleboldwhite"> 
				<html:checkbox property = "check" value = "<%= val %>" />
    		</td>
    		
    		<html:hidden name = "ms" property = "activity_Id"/>
			<html:hidden name = "ms" property = "activity_lib_Id"/>
			
			<%  
			if( activity_size == 1)
			{
				valstr = "javascript: document.forms[0].check.checked = true;tempprevactivitytype = document.forms[0].activitytype.value;updatetext( this , '' );callcopy( tempprevactivitytype , null );"; 
				displaycombo = "javascript: displaycombo( this , '' );";
				schdayscheck = "javascript: document.forms[0].check.checked = true;tempprevquantity = document.forms[0].quantity.value;";
				multiplyqty = "javascript: return multiplyquantity( tempprevquantity , null );";
			}
			else if( activity_size > 1 )
			{
				valstr = "javascript: document.forms[0].check['"+i+"'].checked = true;tempprevactivitytype = document.forms[0].activitytype['"+i+"'].value;updatetext( this ,'"+i+"' );callcopy( tempprevactivitytype , '"+i+"' );"; 
				schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;tempprevquantity = document.forms[0].quantity['"+i+"'].value;";
				displaycombo = "javascript: displaycombo( this , '"+i+"' );";
				multiplyqty = "javascript: return multiplyquantity( tempprevquantity , '"+i+"' );";	
			}	
			%>
			
			<td class = "<%= backgroundhyperlinkclass %>"> 
				<bean:write name = "ms" property = "name" />
			</td>
			<html:hidden name = "ms" property = "name" />
			
			<td class = "<%= backgroundclass %>">  
				
				<logic:equal name = "ms" property = "activitytype" value = "Overhead">
					<html:text styleId = '<%= "activitytype"+i%>' name = "ms" size = "22"  readonly = "true" property = "activitytype"  styleClass = "<%= readonlyclass %>" />	
					
					<html:hidden name = "ms" property = "activitytypecombo" />
				</logic:equal>
				
				<logic:notEqual name = "ms" property = "activitytype" value = "Overhead">

					<logic:notEqual name = "ActivityListForm" property = "job_type" value = "Default">

						<html:text styleId = '<%= "activitytype"+i%>' name = "ms" size = "22"  readonly = "true" property = "activitytype"  styleClass = "text" onfocus = "<%= displaycombo %>"/>	
						<html:select styleId = '<%= "activitytypecombo"+i%>' name = "ms" property = "activitytypecombo" size = "1" styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"  onchange = "<%= valstr %>">
				   			<html:optionsCollection  property = "activity" value = "value" label = "label"/> 
			   			</html:select> 
					</logic:notEqual>
					
					<logic:equal name = "ActivityListForm" property = "job_type" value = "Default">

						 <html:text styleId = '<%= "activitytype"+i%>' name = "ms" size = "22"  readonly = "true" property = "activitytype"  styleClass = "<%= readonlyclass %>" />	
						 <html:hidden name = "ms" property = "activitytypecombo" />
						 
			   		</logic:equal>
				</logic:notEqual>
			</td>
			
			
			<td class = "<%= backgroundclass %>">  
				<logic:equal name = "ActivityListForm" property = "job_type" value = "Default">

				<!-- Commented By Amit to make Qty editable for default job in Proposal Manager-->			
				<%--<html:text styleId = '<= "quantity"+i>' name = "ms" size = "4"  property = "quantity" styleClass = "<= readonlyclass >" readonly = "true"/>	--%>
				
				<html:text styleId = '<%= "quantity"+i%>' name = "ms" size = "4"  property = "quantity" styleClass = "textbox" onfocus = "<%= schdayscheck %>" onblur = "<%= multiplyqty %>" />
					
					
				</logic:equal>
				
				<logic:notEqual name = "ActivityListForm" property = "job_type" value = "Default">


					<logic:equal name = "ms" property = "activitytype" value = "Overhead">
						<html:text styleId = '<%= "quantity"+i%>' name = "ms" size = "4"  property = "quantity" styleClass = "<%= readonlynumberclass %>" readonly = "true" />	

					</logic:equal>
					
					<logic:notEqual name = "ms" property = "activitytype" value = "Overhead">

						<html:text styleId = '<%= "quantity"+i%>' name = "ms" size = "4"  property = "quantity" styleClass = "textbox" onfocus = "<%= schdayscheck %>" onblur = "<%= multiplyqty %>" />
					</logic:notEqual>
				</logic:notEqual>
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_materialcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_materialcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_cnsfieldhqlaborcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_cnsfieldhqlaborcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<html:hidden name = "ms" property = "estimated_cnsfieldlaborcost" />
			<html:hidden name = "ms" property = "estimated_cnshqlaborcost" />
			
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_contractfieldlaborcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_contractfieldlaborcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_freightcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_freightcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_travelcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_travelcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "estimated_totalcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_totalcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "extendedprice"+i%>' name = "ms" size = "10"  readonly = "true" property = "extendedprice" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "overheadcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "overheadcost" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "listprice"+i%>' name = "ms" size = "10"  readonly = "true" property = "listprice" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = '<%= "proformamargin"+i%>' name = "ms" size = "7"  readonly = "true" property = "proformamargin" styleClass = "<%= readonlynumberclass %>" />
			</td>
			
			<td class = "<%= backgroundclass %>">
				<bean:write name = "ms" property = "status"/>
			</td>			
  		</tr>
  		
	   <% i++;  %>
	   
	  </logic:iterate>
	  
	 
	  
  
		<tr> 
			<td><img src = "images/spacer.gif" width = "1" height = "1"></td>
		</tr>

		<tr><td>&nbsp;</td>
			<td colspan = "13" class = "colLight">
  				<html:submit property = "save" styleClass = "button_c" onclick = "return validate();"><bean:message bundle = "AM" key = "am.material.submit"/></html:submit>
				<% 
				if( request.getAttribute( "from" ) != null )
				{%>
					
						<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					
				<%}
			%>
			
			
			</td>
			<td class = "colLight">
				<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "job.tabular.cancel"/></html:reset>
			</td>
			
			
		</tr>
		</logic:present> 
	</table>
	</td>
	</tr>
	 
		<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'copyactivitypage'/>
		 </jsp:include>	
	

</table>

</html:form>
<script>
function setoverheadcheck()
{	
	if( <%= activity_size %> == 1 )
	{
		if( document.forms[0].activitytype.value == 'Overhead' )
		{
			document.forms[0].check.checked = true;
			document.forms[0].check.disabled = true;
		}
	}
	
	if( <%= activity_size %> > 1 )
	{
		for( var i = 0; i < document.forms[0].check.length;i++ )
		{
			if( document.forms[0].activitytype[i].value == 'Overhead' )
			{
				document.forms[0].check[i].checked = true;
				document.forms[0].check[i].disabled = true;
			}
		}
	}
	return true;
}


function enableOverhead(){
		if( <%= activity_size %> == 1 ) {
		if( document.forms[0].activitytype.value == 'Overhead' ) {
			document.forms[0].check.disabled = false;
		}
	}
	
	if( <%= activity_size %> > 1 ) {
		for( var i = 0; i < document.forms[0].check.length;i++ ) {
			if( document.forms[0].activitytype[i].value == 'Overhead' ) {
				document.forms[0].check[i].disabled = false;
			}
		}
	}
}


function validate()
{
	if( <%= activity_size %> > 1 )
	{
		for( var i = 0; i < document.forms[0].check.length;i++ )
		{
			/*if( document.forms[0].activitytype[i].value == 'Required' )
			{
				if( !document.forms[0].check[i].checked )
				{
					alert( 'Please select all required activitiies' );
					return false;
				}
			}*/
			
			if( document.forms[0].activitytype[i].value == 'Overhead' )
			{
				if( !document.forms[0].check[i].checked )
				{
					alert( "<bean:message bundle = "pm" key = "activity.tabular.selectoverheadactivity"/>" );
					return false;
				}
			}
			
		}
	}
	else
	{
		if(!document.forms[0].check.checked)
		{
			alert('Please select an activity to copy');
			return false;
		}
	}
	
	document.forms[0].ref.value = "Submit";
	
	enableOverhead();
	return true;
}


function callcopy( prevactivitytype , index )
{
	if( <%= activity_size %> > 1 )
	{
		var overheadindex = '';
		var sumreqextendedprice = "0.0000";
		
		if( prevactivitytype == 'Required' )
		{
			document.forms[0].overheadcost[index].value = parseFloat( '0.0000' );
			document.forms[0].listprice[index].value = round_func( parseFloat( document.forms[0].extendedprice[index].value ) + parseFloat( document.forms[0].overheadcost[index].value ) );
			
			if(  parseFloat( document.forms[0].listprice[index].value ) > parseFloat( '0.0000' ) )
			{
				document.forms[0].proformamargin[index].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[index].value ) / parseFloat( document.forms[0].listprice[index].value ) ) );
			}
		}
		
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytype[i].value == 'Overhead' )
			{
				overheadindex = i;
			}
		}
		
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytype[i].value == "Required" )
			{
				sumreqextendedprice = round_func( parseFloat( sumreqextendedprice ) + parseFloat( document.forms[0].extendedprice[i].value ) );	
			}
		}
		
		if( overheadindex != '' )
		{
			if( parseFloat( sumreqextendedprice ) > 0 )
			{
				for( var i = 0; i < document.forms[0].check.length; i++ )
				{
					if( document.forms[0].activitytype[i].value == "Required" )
					{
						document.forms[0].overheadcost[i].value = round_func( ( parseFloat( document.forms[0].extendedprice[i].value ) * parseFloat( document.forms[0].extendedprice[overheadindex].value ) ) / parseFloat( sumreqextendedprice ) ); 	
						document.forms[0].check[i].checked = true;
						document.forms[0].listprice[i].value = round_func( parseFloat( document.forms[0].extendedprice[i].value ) + parseFloat( document.forms[0].overheadcost[i].value ) );
	
						if(  parseFloat( document.forms[0].listprice[i].value ) >  parseFloat( "0.0000" ) )
						{
							document.forms[0].proformamargin[i].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[i].value ) / parseFloat( document.forms[0].listprice[i].value ) ) );
						}
					}
				}		
			}
		}
			
	}
	
}


function multiplyquantity( prevval , index )
{
	if( index != null )
	{
		if( document.forms[0].quantity[index].value == "" )
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
			document.forms[0].quantity[index].focus();
			return false;
		}
		
		if( isFloat( document.forms[0].quantity[index].value ) )
		{
			if( !(document.forms[0].quantity[index].value > 0 ) )
			{
				alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
				document.forms[0].quantity[index].value = '';
				document.forms[0].quantity[index].focus();
				return false;
			}
			
			document.forms[0].estimated_materialcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( document.forms[0].estimated_materialcost[index].value ) ) / parseFloat( prevval ) );
			
			document.forms[0].estimated_cnsfieldlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( document.forms[0].estimated_cnsfieldlaborcost[index].value ) ) / parseFloat( prevval ) );		
			
			document.forms[0].estimated_cnshqlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( document.forms[0].estimated_cnshqlaborcost[index].value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_cnsfieldhqlaborcost[index].value = round_func( parseFloat( document.forms[0].estimated_cnsfieldlaborcost[index].value ) + parseFloat( document.forms[0].estimated_cnshqlaborcost[index].value ) );
			
			document.forms[0].estimated_contractfieldlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( document.forms[0].estimated_contractfieldlaborcost[index].value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_freightcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( document.forms[0].estimated_freightcost[index].value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_travelcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( document.forms[0].estimated_travelcost[index].value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_totalcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( document.forms[0].estimated_totalcost[index].value ) ) / parseFloat( prevval ) );			
			document.forms[0].extendedprice[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat( document.forms[0].extendedprice[index].value ) ) / parseFloat( prevval ) );			
			if( document.forms[0].activitytype[index].value == "Optional" ) 
			{
				document.forms[0].listprice[index].value = round_func( parseFloat( document.forms[0].extendedprice[index].value ) + parseFloat( document.forms[0].overheadcost[index].value ) );
				if(  parseFloat( document.forms[0].listprice[index].value ) > 0 )
				{
					document.forms[0].proformamargin[index].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[index].value ) / parseFloat( document.forms[0].listprice[index].value ) ) );
				}
			}
			else
			{
				
				calnew( index );
			}
		}
		
		else
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
			document.forms[0].quantity[index].value = '';
			document.forms[0].quantity[index].focus();
			return false;
		}
	}
	
	else
	{
		if( document.forms[0].quantity.value == "" )
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
			document.forms[0].quantity.focus();
			return false;
		}
		
		if( isFloat( document.forms[0].quantity.value ) )
		{
			if( !(document.forms[0].quantity.value > 0 ) )
			{
				alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
				document.forms[0].quantity.value = '';
				document.forms[0].quantity.focus();
				return false;
			}
			
			
			
			document.forms[0].estimated_materialcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimated_materialcost.value ) ) / parseFloat( prevval ) );
			
			document.forms[0].estimated_cnsfieldlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimated_cnsfieldlaborcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_cnshqlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimated_cnshqlaborcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_cnsfieldhqlaborcost.value = round_func( parseFloat( document.forms[0].estimated_cnsfieldlaborcost.value ) + parseFloat( document.forms[0].estimated_cnshqlaborcost.value ) );
			
			
			document.forms[0].estimated_contractfieldlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimated_contractfieldlaborcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_freightcost.value = round_func( ( parseFloat(  document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimated_freightcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_travelcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimated_travelcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].estimated_totalcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimated_totalcost.value ) ) / parseFloat( prevval ) );		
			document.forms[0].extendedprice.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].extendedprice.value ) ) / parseFloat( prevval ) );		
			
			calnew( index );
		}
		else
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterthan"/>" );
			document.forms[0].quantity.value = '';
			document.forms[0].quantity.focus();
			return false;
		}
	}	
}



function calnew( index )
{	
	var overheadindex = '';
	for( var i = 0; i < document.forms[0].check.length; i++ )
	{
		if( document.forms[0].activitytype[i].value == 'Overhead' )
			overheadindex = i;
	}
	
	
	if( overheadindex == '' )
		return false;
			
	if( parseFloat( document.forms[0].extendedprice[overheadindex].value ) > 0 ) 
	{	
		var tempreqextendedprice = "0.0";
		
		
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytype[i].value == "Required" )
			{
				if( document.forms[0].extendedprice[i].value == '0.0000' )
				{
					
					document.forms[0].overheadcost[i].value = '0.0000';
					document.forms[0].listprice[i].value = '0.0000';
					document.forms[0].proformamargin[i].value = '0.0000';
				}
				
				tempreqextendedprice = round_func( parseFloat( tempreqextendedprice ) + parseFloat( document.forms[0].extendedprice[i].value ) );	
			}
		}
		
		if( parseFloat( tempreqextendedprice ) > 0 )
		{	
			for( var i = 0; i < document.forms[0].check.length; i++ )
			{
				if( document.forms[0].activitytype[i].value == "Required" )
				{
					document.forms[0].overheadcost[i].value = round_func( ( parseFloat( document.forms[0].extendedprice[i].value ) * parseFloat( document.forms[0].extendedprice[overheadindex].value ) ) / parseFloat( tempreqextendedprice ) ); 	
					document.forms[0].check[i].checked = true;
					document.forms[0].listprice[i].value = round_func( parseFloat( document.forms[0].extendedprice[i].value ) + parseFloat( document.forms[0].overheadcost[i].value ) );
					//alert(document.forms[0].listprice[i].value);
					if(  parseFloat( document.forms[0].listprice[i].value ) > "0.0" )
					{
						document.forms[0].proformamargin[i].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[i].value ) / parseFloat( document.forms[0].listprice[i].value ) ) );
					}
				}
			}		
		}
	}	
	else
	{
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytype[i].value != "Overhead" ) 
			{
				document.forms[0].check[i].checked = true;
				document.forms[0].listprice[i].value = round_func( parseFloat( document.forms[0].extendedprice[i].value ) + parseFloat( document.forms[0].overheadcost[i].value ) );

				if(  parseFloat( document.forms[0].listprice[i].value ) > parseFloat( "0.0" ) )
				{
					document.forms[0].proformamargin[i].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[i].value ) / parseFloat( document.forms[0].listprice[i].value ) ) );
				}
			}
		}		
	}
}



function Backactionjob()
{
	document.forms[0].action = "JobDashboardAction.do?jobid=<bean:write name = "ActivityListForm" property = "job_Id" />" ;
	document.forms[0].submit();
	return true;
}

</script>
<script>
function ShowDiv()
{

	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";

return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){

		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;


}
</script>


</body>

</html:html>
