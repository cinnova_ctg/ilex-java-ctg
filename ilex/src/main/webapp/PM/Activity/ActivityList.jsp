<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating and  updating  Activities.
*
-->
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:define id = "jobtype" name = "ActivityListForm" property = "job_type" />
<bean:define id = "refForJob" name = "ActivityListForm" property = "ref" />
<% 
	
	String job_Id = ( String ) request.getAttribute( "Job_Id" );

	if(refForJob.equals("detailactivity")||refForJob.equals("View")) 
			 refForJob = "detailjob";
		else
			 refForJob = "inscopejob";
		 
	int activity_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 

	String module = "";
	if(session.getAttribute("tRModule") != null) {
		module = session.getAttribute("tRModule").toString();
	}
%>
<html:html>
<head>
	<title><bean:message bundle = "pm" key = "activity.tabular.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<%@ include file = "/NMenu.inc" %> 
	<%@ include  file="/MSAMenu.inc" %>
	<%@ include  file="/AppendixMenu.inc" %>
	<%@ include  file="/AddendumMenu.inc" %>

	<%@ include file="/DefaultJobMenu.inc" %>
	<%@ include  file="/ProjectJobMenu.inc" %>
</head>
<%
	String backgroundclass = null;
	String backgroundhyperlinkclass = null;
	String comboclass = null;
	String readonlyclass = null;
	String readonlynumberclass = null;
	boolean csschooser = true;
	String displaycombo = null;
	String valstr = null;
	String datecheck = null;
	String schdayscheck = null;
	String multiplyqty = null;
	int i = 0;

%>
<script>
function onlyNumbers(e)
{
	var keynum; var keychar; var numcheck;
	
	if(window.event){ // IE
		keynum = e.keyCode;
	}
	keychar = String.fromCharCode(keynum);
	numcheck = /\d/;
	return numcheck.test(keychar);
}
function validateNumbers(obj)
{
	if(parseInt(obj.value) != obj.value);{
		obj.value=parseInt(obj.value);
	}
	if(!isNaN(obj.value)){
		return true;
	}
	else{
		obj.value = '';
	}
}

</script>
<body onLoad ="leftAdjLayers();"> 
<html:form action = "/ActivityUpdateAction">
<html:hidden property = "job_Id" />
<html:hidden property = "ref" /> 
<html:hidden property = "from" /> 
<html:hidden property = "activity_cost_type" />
<html:hidden property = "job_type" />
<html:hidden property = "addendum_id" />
<html:hidden property = "chkaddendum" />


<!--<logic:equal name ="ActivityListForm" property ="ref" value ="inscopedetailactivity">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr><td class="headerrow" height="19" width ="100%">&nbsp;</td></tr>
							<tr><td background="images/content_head_04.jpg" height="21" width ="100%">&nbsp;</td></tr>
							<tr><td><h2>
										<A class = "head" href = "JobDashboardAction.do?jobid=<bean:write name = "ActivityListForm" property = "job_Id" />"><bean:write name = "ActivityListForm" property = "jobName"/></A><bean:message bundle = "pm" key = "activity.tabular.new.label"/>
							</h2></td></tr>
							
							
	</table>
</logic:equal>-->
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr><td class="headerrow" height="19" width = "100%" >&nbsp;</td></tr>
			          <tr>
				         <!-- BreadCrumb goes into this td -->
				         <td background="images/content_head_04.jpg" height="21" width = "100%" >
				         <%if(request.getAttribute( "from" ) != null && !request.getAttribute( "from").equals("")){ %>
		          			<div id="breadCrumb">
		          					<a class ="bgNone" href = "SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<bean:write name = "ActivityListForm" property = "msa_id" />&app=null"><bean:write name = "ActivityListForm" property = "msaname"/></a>
				          			<a href="AppendixHeader.do?function=view&fromPage=appendix&viewjobtype=ION&&msaId=<bean:write name = "ActivityListForm" property = "msa_id" />&appendixid=<bean:write name = "ActivityListForm" property = "appendixid" />">
				          				<bean:write name = "ActivityListForm" property = "appendixname"/>
				          			</a>
				          			<a href="JobDashboardAction.do?function=view&jobid=<bean:write name = "ActivityListForm" property = "job_Id" />&appendixid=<bean:write name = "ActivityListForm" property = "appendixid" />">
				          				<bean:write name = "ActivityListForm" property = "jobName"/>
				          			</a>
				          			<a><span class ="breadCrumb1">Manage Activities</span></a>
							</div>
						<%} else{ %>
							<div id="breadCrumb">
		          				<a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
		          				<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "ActivityListForm" property = "msa_id" />">
		          				<bean:write name = "ActivityListForm" property = "msaname"/></a>
		          				<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "ActivityListForm" property = "appendixid" />">
		          				<bean:write name = "ActivityListForm" property = "appendixname"/>
							</div>
						<%}%>
				        </td>
				        <!-- End of BreadCrumb-->
					  </tr>
					  
					  <%if(request.getAttribute( "from" ) != null && !request.getAttribute( "from").equals("")){ %>
					  <%}else{%>
					  	 <tr>
						   	<td colspan="5">
						   		<h2>
						   			<A class="head" href="JobDetailAction.do?ref=detailjob&Job_Id=<bean:write name = "ActivityListForm" property = "job_Id" />"><bean:write name = "ActivityListForm" property = "jobName" /></A>
						   			<bean:message bundle = "pm" key = "activity.tabular.new.label"/>
						   		</h2>
						   	</td>
					   	</tr>	
					  <%}%>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" >
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>

<c:set var = "gridControl" value = "${ActivityListForm.activityGridControl}"/>

			<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
					<tr>
						<td>
							<table border = "0" cellspacing = "1" cellpadding = "1">
								<!-- Start :Added By Amit -->
						  		<logic:present name = "changestatusflag" scope = "request">
							  		<tr><td>&nbsp;</td>
								    	<td colspan = "5" class = "message" height = "30">
									  		<logic:equal name = "changestatusflag" value = "0"><bean:message bundle = "AM" key = "am.tabular.resourcesapprovedsuccessfully"/></logic:equal>
											<logic:equal name = "changestatusflag" value = "-9001"><bean:message bundle = "pm" key = "activity.detail.changestatus.failure1"/></logic:equal>
										</td>
									</tr>
						  		</logic:present>
						  		<logic:present name = "changestatusdraftflag" scope = "request">
							  		<tr><td>&nbsp;</td>
								    	<td colspan = "5" class = "message" height = "30">
									  		<logic:equal name = "changestatusdraftflag" value = "0"><bean:message bundle = "pm" key = "activity.detail.changestatus.success"/></logic:equal>
								    		<logic:equal name = "changestatusdraftflag" value = "-9001"><bean:message bundle = "pm" key = "activity.detail.changestatus.failure1"/></logic:equal>
								    		<logic:equal name = "changestatusdraftflag" value = "-9002"><bean:message bundle = "pm" key = "activity.detail.changestatus.failure2"/></logic:equal>
								    		<logic:equal name = "changestatusdraftflag" value = "-9003"><bean:message bundle = "pm" key = "activity.detail.changestatus.failure3"/></logic:equal>
										</td>
									</tr>
						  		</logic:present>
						  		
						  		
						  		
						  		<!--  End :Added By Amit -->
								
								  		<logic:present name = "copyactivityflag" scope = "request">
								  		<tr>
											<td>&nbsp;</td>
											<td colspan = "5" class = "message" height = "30">
										    	<logic:equal name = "copyactivityflag" value = "0">
										    	<bean:message bundle = "pm" key = "activity.tabular.copyactivity"/>
										    		<script>
										    			<% if(!module.equals("PRM") && !module.equals("NM")) {	// If module is not Project Manager and NetMedx Manager. %>
															parent.ilexleft.location.reload();
														<% } %>
													</script>
										    	</logic:equal>
										    </td>
									    </tr>
										</logic:present>
										<logic:present name = "addflag" scope = "request">
										<tr>
											<td>&nbsp;</td>
											<td colspan = "5" class = "message" height = "30">
									    		<logic:equal name = "addflag" value = "0">
									    			<bean:message bundle = "pm" key = "activity.tabular.add.success"/>
										    		<script>
										    			<% if(!module.equals("PRM") && !module.equals("NM")) {	// If module is not Project Manager and NetMedx Manager. %>
															parent.ilexleft.location.reload();
														<% } %>
													</script>
									    		</logic:equal>
							    			
							    				<logic:equal name = "addflag" value = "-90018"><bean:message bundle = "pm" key = "activity.tabular.add.failure1"/></logic:equal>
							    				<logic:equal name = "addflag" value = "-90020"><bean:message bundle = "pm" key = "activity.tabular.add.failure2"/></logic:equal>
							    				<logic:equal name = "addflag" value = "-9002"><bean:message bundle = "pm" key = "activity.tabular.add.failure3"/></logic:equal>
							    				<logic:equal name = "addflag" value = "-9003"><bean:message bundle = "pm" key = "activity.tabular.add.failure4"/></logic:equal>
							    				<logic:equal name = "addflag" value = "-9005"><bean:message bundle = "pm" key = "activity.tabular.add.failure5"/></logic:equal>
							    				<logic:equal name = "addflag" value = "-9006"><bean:message bundle = "pm" key = "activity.tabular.add.failure6"/></logic:equal>
							    				<logic:equal name = "addflag" value = "-9007"><bean:message bundle = "pm" key = "activity.tabular.add.failure7"/></logic:equal>
							    				<logic:equal name = "addflag" value = "-9008"><bean:message bundle = "pm" key = "activity.tabular.add.failure8"/></logic:equal>
							    				<logic:equal name = "addflag" value = "-9099"><bean:message bundle = "pm" key = "activity.tabular.add.failure9"/></logic:equal>
							    			</td>
							    		</tr>
							    	</logic:present>
							    	<logic:present name = "updateflag" scope = "request">
										<tr>
											<td>&nbsp;</td>
											<td colspan = "5" class = "message" height = "30">
								    		<logic:equal name = "updateflag" value = "0">
								    			<bean:message bundle = "pm" key = "activity.tabular.update.success"/>
								    		</logic:equal>
								    		</td>
							    		</tr>
							    	</logic:present>
							    	<logic:present name = "deleteflag" scope = "request">
										<tr>
											<td>&nbsp;</td>
											<td colspan = "10" class = "message" height = "30">
								    		<logic:equal name = "deleteflag" value = "0">
								    			<bean:message bundle = "pm" key = "activity.tabular.delete.success"/>
									    		<script>
									    			<% if(!module.equals("PRM") && !module.equals("NM")) {	// If module is not Project Manager and NetMedx Manager. %>
														parent.ilexleft.location.reload();
													<% } %>
												</script>
								    		</logic:equal>
								    		<logic:equal name = "deleteflag" value = "-9001"><bean:message bundle = "pm" key = "activity.tabular.delete.failure9"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-9002"><bean:message bundle = "pm" key = "activity.tabular.delete.failure8"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-9003"><bean:message bundle = "pm" key = "activity.tabular.delete.failure1"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-9004"><bean:message bundle = "pm" key = "activity.tabular.delete.failure2"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-9005"><bean:message bundle = "pm" key = "activity.tabular.delete.failure3"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-9006"><bean:message bundle = "pm" key = "activity.tabular.delete.failure4"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-9007"><bean:message bundle = "pm" key = "activity.tabular.delete.failure5"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-9008"><bean:message bundle = "pm" key = "activity.tabular.delete.failure6"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-99999"><bean:message bundle = "pm" key = "activity.tabular.delete.failure7"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "-90090"><bean:message bundle = "pm" key = "activity.tabular.delete.failure10"/></logic:equal>
								    		<logic:equal name ="deleteflag"  value ="9600"><bean:message  bundle ="pm" key="activity.tabular.delete.failure11"/></logic:equal>
								    		<logic:equal name = "deleteflag" value = "547">Action could not be performed possibly due to reference of this activity from somewhere.</logic:equal>
								    		<c:if test = "${not empty requestScope.activityDeletionFailed}">
								    			<br>Following activities are referred in MPO.Deletetion Failed<br>	
												<c:forEach var = "act" items = "${requestScope.activityDeletionFailed}" varStatus ="status">
													<c:out value = "${act}"/><br>
												</c:forEach>								    		
								    		</c:if>	
								    		</td>
							    		</tr>
							    	</logic:present>
								<tr>  
								    <td rowspan = "2">&nbsp; </td>
								    <td rowspan = "2" class = "Ntryb">&nbsp; </td>
									<td class = "Ntryb" rowspan = "2" colspan="2">
										<TABLE border="0" width="100%">
											<tr>
												 <logic:equal name="ActivityListForm" property="job_type" value="Default">
													<td class = "Ntryb"><bean:message bundle = "pm" key = "activity.tabular.name"/></td>
													<td align="right" style="white-space: nowrap;"><html:button styleClass = "Nbutton" property ="Add" onclick ="return addActivity();" disabled="true">Add</html:button>&nbsp;
																	  <html:button property = "sort" styleClass = "Nbutton" onclick = "return sortwindow();"><bean:message bundle = "pm" key = "job.tabular.sort"/></html:button>	
													</td>
												</logic:equal>
												 <logic:notEqual name="ActivityListForm" property="job_type" value="Default">
													 <td class = "Ntryb"><bean:message bundle = "pm" key = "activity.tabular.name"/></td>
													 <td align="right" style="white-space: nowrap;">
													 		<c:choose>
													 			<c:when test = "${gridControl.activityAddFlag eq 'Y'}"><html:button styleClass = "Nbutton" property ="Add" onclick ="return addActivity();">Add</html:button>&nbsp;</c:when>
													 			<c:when test = "${gridControl.activityAddFlag ne 'Y'}"><html:button styleClass = "Nbutton" property ="Add" onclick ="return addActivity();" disabled="true">Add</html:button>&nbsp;</c:when>	
													 		</c:choose>	
													 		<html:button property = "sort" styleClass = "Nbutton" onclick = "return sortwindow();"><bean:message bundle = "pm" key = "job.tabular.sort"/></html:button>		
													 </td>
												 </logic:notEqual>
											</tr>
										</TABLE>
									</td>
									<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.type"/></td>
									<%if(request.getAttribute( "from" ) != null && !request.getAttribute( "from").equals("")){ %>
										<td class = "Ntryb" rowspan = "2">Bid Flag</td>
									<%} %>
									<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.quantity"/></td>
									<td class = "Ntryb" colspan = "6"><bean:message bundle = "pm" key = "activity.tabular.estimatedcost"/></td>
									<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.extendedprice"/></td>
									<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.overhead"/></td>
									<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.listprice"/></td>
									<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.proformamargin"/></td>
									<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "activity.tabular.status"/></td>
								</tr>  
								<tr> 
				    				<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "activity.tabular.materials"/></div></td>
				    				<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "activity.tabular.cnslabor"/></div></td>
				    				<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "activity.tabular.contractlabor"/></div></td>
				    				<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "activity.tabular.freight"/></div></td>
				    				<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "activity.tabular.travel"/></div></td>
					  				<td class = "Ntryb"><div align = "center"><bean:message bundle = "pm" key = "activity.tabular.estimatedtotal"/></div></td>
								</tr>
  <% if( activity_size > 0 ){ %> 			
				
				<logic:present name = "codes" scope = "request"> 
				 	<logic:iterate id = "ms" name = "codes">
						<%	
							if ( csschooser == true ) 
							{
								backgroundclass =" Ntexto";
								backgroundhyperlinkclass = "Nhyperodd";
								comboclass = "select";
								readonlyclass = "Nreadonlytextodd";
								readonlynumberclass = "Nreadonlytextnumberodd";
								csschooser = false;
							}
					
							else
							{
								csschooser = true;	
								comboclass = "select";
								readonlyclass = "Nreadonlytexteven";
								backgroundhyperlinkclass = "Nhypereven";
								readonlynumberclass = "Nreadonlytextnumbereven";
								backgroundclass = "Ntexte";
							}
						%>
						<bean:define id = "val" name = "ms" property = "activity_Id" type = "java.lang.String"/>
						<tr>	
        					<td class = "Nlabeleboldwhite">&nbsp;</td>
							<td class = "<%= backgroundclass %>"> 
								<logic:equal name = "ms" property = "activitytype" value = "Overhead">
									<html:checkbox property = "check" value = "<%= val %>" disabled = "true"/>
								</logic:equal>
				
								<logic:notEqual name = "ms" property = "activitytype" value = "Overhead">
									<% if(  jobtype.equals( "Default" ) ||  jobtype.equals( "Addendum" ) ) { 
									//if(  jobtype.equals( "Default" )) { 
									%>
										<html:checkbox property = "check" value = "<%= val %>" />
									<%}else { %>
									<html:checkbox property = "check" value = "<%= val %>" />
									<%} %>
								</logic:notEqual>
    						</td>	
				    		<html:hidden name = "ms" property = "activity_Id"/>
							<bean:define id="estimatedMaterialCost" name="ms" property="estimated_materialcost"></bean:define>
							<bean:define id="estimatedCnsFieldCost" name="ms" property="estimated_cnshqlaborcost"></bean:define>
							<bean:define id="estimatedContractCost" name="ms" property="estimated_contractfieldlaborcost"></bean:define>
							<bean:define id="estimatedContractFieldLaborCost" name="ms" property="estimated_cnsfieldlaborcost"></bean:define>
							<bean:define id="estimatedFreightCost" name="ms" property="estimated_freightcost"></bean:define>
							<bean:define id="estimatedTravelCost" name="ms" property="estimated_travelcost"></bean:define>
							<bean:define id="estimatedTotalCost" name="ms" property="estimated_totalcost"></bean:define>
							<bean:define id="extendedPrice" name="ms" property="extendedprice"></bean:define>
							<bean:define id="prevQty" name="ms" property="quantity"></bean:define>
							<%  
								if( activity_size == 1 )
								{
									valstr = "javascript: document.forms[0].check.checked = true; return setlistpricezero(null);"; 
									displaycombo = "javascript: displaycombo( this , '' );";
									schdayscheck = "javascript: document.forms[0].check.checked = true;tempprevquantity = document.forms[0].quantity.value;";
									multiplyqty = "javascript: validateNumbers(this);return multiplyquantity("+estimatedMaterialCost+","
									+estimatedContractFieldLaborCost+","
									+estimatedCnsFieldCost+","
									+estimatedContractCost+","
									+estimatedFreightCost+","
									+estimatedTravelCost+","
									+estimatedTotalCost+","
									+extendedPrice+","
									+prevQty+","
									+"  null );";
									datecheck = "javascript:document.forms[0].check.checked=true; return popUpCalendar(this,this,'mm/dd/yyyy');";
								}
								else if( activity_size > 1 )
								{
									valstr = "javascript: document.forms[0].check['"+i+"'].checked = true; return setlistpricezero('"+i+"');"; 
									//schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true; tempprevquantity = document.forms[0].quantity['"+i+"'].value;";
									schdayscheck = "javascript: tempprevquantity = document.forms[0].quantity['"+i+"'].value;";
									displaycombo = "javascript: displaycombo( this , '"+i+"' );";
									multiplyqty = "javascript: validateNumbers(this);return multiplyquantity("+estimatedMaterialCost+","
									+estimatedContractFieldLaborCost+","
									+estimatedCnsFieldCost+","
									+estimatedContractCost+","
									+estimatedFreightCost+","
									+estimatedTravelCost+","
									+estimatedTotalCost+","
									+extendedPrice+","
									+prevQty+","
									+"'"+i+"' );";	
									datecheck = "javascript:document.forms[0].check["+i+"].checked=true; return popUpCalendar(this,this,'mm/dd/yyyy');";	
								}	
							%>
							<html:hidden name = "ms" property = "name" />
							<c:choose>
								<c:when test="${not empty requestScope.from && requestScope.from eq 'jobdashboard'}">
									<td colspan ="2" class = "<%= backgroundclass %>" align="left"> 
										<bean:write name = "ms" property = "name" />
									</td>
								</c:when>
								<c:when test="${requestScope.from ne 'jobdashboard'}">
									<td  class = "<%= backgroundclass %>" align="left"> 
										<bean:write name = "ms" property = "name" />
									</td>
									<td class = "<%= backgroundclass %>" align="right">
										<%if(request.getAttribute("checknetmedx").equals("1")) {%>
											[<a href = "ResourceListAction.do?ref=View&type=PM&from=<bean:write name = "ActivityListForm" property = "from" />&resourceListType=A&Activitylibrary_Id=<bean:write name = "ms" property = "activity_Id" />&Activity_Id=<bean:write name = "ms" property = "activity_Id" />">Details</a>&nbsp;|&nbsp;<a href = "ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "ms" property = "activity_Id" />&ref=<bean:write name = "ActivityListForm" property = "chkaddendum" />&from=<bean:write name = "ActivityListForm" property = "from" />&jobType=<%= jobtype %>" />Resources</a>]
										<%}	else{ %>
											<logic:equal name ="ActivityListForm" property ="chkaddendum" value ="detailactivity">
											[<a href = "ActivityDetailAction.do?ref=<bean:write name = "ActivityListForm" property = "chkaddendum" />&type=PM&from=<bean:write name = "ActivityListForm" property = "from" />&addendum_id=<bean:write name = "ActivityListForm" property = "addendum_id" />&Activitylibrary_Id=<bean:write name = "ms" property = "activity_Id" />&Activity_Id=<bean:write name = "ms" property = "activity_Id" />">Details</a>&nbsp;|&nbsp;<a href = "ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "ms" property = "activity_Id" />&ref=View&from=<bean:write name = "ActivityListForm" property = "from" />&jobType=<%= jobtype %>"" />Resources</a>]
											</logic:equal>
											<logic:notEqual name ="ActivityListForm" property ="chkaddendum" value ="detailactivity">
											[<a href = "ActivityDetailAction.do?ref=<bean:write name = "ActivityListForm" property = "chkaddendum" />&type=PM&from=<bean:write name = "ActivityListForm" property = "from" />&addendum_id=<bean:write name = "ActivityListForm" property = "addendum_id" />&Activitylibrary_Id=<bean:write name = "ms" property = "activity_Id" />&Activity_Id=<bean:write name = "ms" property = "activity_Id" />">Details</a>&nbsp;|&nbsp;<a href = "ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "ms" property = "activity_Id" />&ref=<bean:write name = "ActivityListForm" property = "chkaddendum" />&from=<bean:write name = "ActivityListForm" property = "from" />&jobType=<%= jobtype %>"" />Resources</a>]
											</logic:notEqual>
										<%} %>
									</td>
								</c:when>
							</c:choose>
							
							<td class = "<%= backgroundclass %>">  
								
								<logic:equal name = "ms" property = "activitytype" value = "Overhead">
									<bean:write name="ms" property="activitytype"/>
									<html:hidden name = "ms" property = "activitytypecombo" />
								</logic:equal>
								
								<logic:notEqual name = "ms" property = "activitytype" value = "Overhead">
									<logic:equal name = "ms" property = "status" value = "Approved">
										<bean:write name="ms" property="activitytype"/>
										<html:hidden name = "ms" property = "activitytypecombo" />
									</logic:equal>
										
									<logic:notEqual name = "ms" property = "status" value = "Approved">
										<c:choose>
											<c:when test = "${gridControl.activityTypeFlag eq 'Y'}">
												<html:select styleId = '<%= "activitytypecombo"+i%>' name = "ms" property = "activitytypecombo" size = "1" styleClass = "<%= comboclass %>" onblur = ""  onchange = "<%= valstr %>">
									   				<html:optionsCollection  property = "activity" value = "value" label = "label"/> 
										   		</html:select> 	
											</c:when>
											<c:when test = "${gridControl.activityTypeFlag ne 'Y'}">
												<bean:write name="ms" property="activitytype"/>
												<html:hidden name = "ms" property = "activitytypecombo" />	
											</c:when>
										</c:choose>
									</logic:notEqual>
								</logic:notEqual>
							</td>
							<!-- for Bid Flag: Start -->
							<!-- Changed on 08/06/1008(mm/dd/yyyy) -->
							<%if(request.getAttribute( "from" ) != null && !request.getAttribute( "from").equals("")){ %>
							<td class = "<%= backgroundclass %>" >
							
								<logic:equal name = "ms" property = "status" value = "Approved">
									<c:if test = "${ms.bidFlag eq '0'}">No</c:if>
									<c:if test = "${ms.bidFlag eq '1'}">Yes</c:if>
								</logic:equal>
								
								<logic:notEqual name = "ms" property = "status" value = "Approved">
									<html:multibox property = "bidFlag" onclick = "<%= valstr %>">
										<bean:write name = "ms" property = "activity_Id" />
									</html:multibox>
								</logic:notEqual>
								
								<!-- For Not Approve job: End -->
							</td>
							<%}%>
							<!-- for Bid Flag: End -->
							
							<td class = "<%= backgroundclass %>">  
								<% if(jobtype.equals( "Default" )) { %>
									<html:text style="text-align:right;" styleId = '<%= "quantity"+i %>'  name = "ms" size = "4"   property = "quantity" onkeypress="return onlyNumbers(event);" onblur="validateNumbers(this);" styleClass = "<%= readonlynumberclass %>" readonly = "true" />	
								<% } else { %>
									<logic:notEqual name = "ms" property = "activitytype" value = "Overhead">
										<logic:equal name = "ms" property = "status" value = "Approved">
											<html:text style="text-align:right;" styleId = '<%= "quantity"+i %>' name = "ms" size = "4"  property = "quantity" onkeypress="return onlyNumbers(event);" onblur="validateNumbers(this);" styleClass = "<%= readonlynumberclass %>" readonly = "true" />	
										</logic:equal>
									
										<logic:notEqual name = "ms" property = "status" value = "Approved">
											<c:choose>
												<c:when test = "${gridControl.activityQuantityFlag eq 'Y'}">
													<html:text  style="text-align:right;"  styleId = '<%= "quantity"+i%>' name = "ms" size = "4"  property = "quantity" onkeypress="return onlyNumbers(event);" styleClass = "text" onchange="<%= valstr %>" onfocus = "<%= schdayscheck %>" onblur = "<%= multiplyqty %>" />
												</c:when>
												<c:when test = "${gridControl.activityQuantityFlag ne 'Y'}">
													<html:text style="text-align:right;" styleId = '<%= "quantity"+i %>' name = "ms" size = "4"  property = "quantity" onkeypress="return onlyNumbers(event);" onblur="validateNumbers(this);" styleClass = "<%= readonlynumberclass %>" readonly = "true" />	
												</c:when>
											</c:choose>	
										</logic:notEqual>
									</logic:notEqual>
								
									<logic:equal name = "ms" property = "activitytype" value = "Overhead">	
										<html:text style="text-align:right;" styleId = '<%= "quantity"+i%>' name = "ms" size = "4"  property = "quantity" onkeypress="return onlyNumbers(event);" onblur="validateNumbers(this);" styleClass = "<%= readonlynumberclass %>"  />	
									</logic:equal>
								<% } %>
							</td>
							<td class = "<%= backgroundclass %>">  
								<html:text style="text-align:right;" styleId = '<%= "estimated_materialcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_materialcost" styleClass = "<%= readonlynumberclass %>" />
							</td>
							<td class = "<%= backgroundclass %>" style="padding-right: 4px;padding-left: 40px;">  
								<html:text style="text-align:right;" styleId = '<%= "estimated_cnsfieldhqlaborcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_cnsfieldhqlaborcost" styleClass = "<%= readonlynumberclass %>" />
							</td>
								
							<html:hidden name = "ms" property = "estimated_cnsfieldlaborcost" />
							<html:hidden name = "ms" property = "estimated_cnshqlaborcost" />
							
							<td class = "<%= backgroundclass %>" style="padding-right: 4px;padding-left: 40px;">  
								<html:text style="text-align:right;" styleId = '<%= "estimated_contractfieldlaborcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_contractfieldlaborcost" styleClass = "<%= readonlynumberclass %>" />
							</td>
							<td class = "<%= backgroundclass %>">  
								<html:text style="text-align:right;" styleId = '<%= "estimated_freightcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_freightcost" styleClass = "<%= readonlynumberclass %>" />
							</td>
							
							<td class = "<%= backgroundclass %>">  
								<html:text style="text-align:right;" styleId = '<%= "estimated_travelcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_travelcost" styleClass = "<%= readonlynumberclass %>" />
							</td>
							
							<td class = "<%= backgroundclass %>">  
								<html:text style="text-align:right;" styleId = '<%= "estimated_totalcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "estimated_totalcost" styleClass = "<%= readonlynumberclass %>" />
							</td>
							
							<td class = "<%= backgroundclass %>">  
								<html:text style="text-align:right;" styleId = '<%= "extendedprice"+i%>' name = "ms" size = "10"  readonly = "true" property = "extendedprice" styleClass = "<%= readonlynumberclass %>" />
							</td>
							
							<td class = "<%= backgroundclass %>">  
								<html:text style="text-align:right;" styleId = '<%= "overheadcost"+i%>' name = "ms" size = "10"  readonly = "true" property = "overheadcost" styleClass = "<%= readonlynumberclass %>" />
							</td>
							
							<td class = "<%= backgroundclass %>">  
								<html:text style="text-align:right;" styleId = '<%= "listprice"+i%>' name = "ms" size = "10"  readonly = "true" property = "listprice" styleClass = "<%= readonlynumberclass %>" />
							</td>
							
							<td class = "<%= backgroundclass %>">  
								<html:text style="text-align:right;" styleId = '<%= "proformamargin"+i%>' name = "ms" size = "5"  readonly = "true" property = "proformamargin" styleClass = "<%= readonlynumberclass %>" />
							</td>
							
								<html:hidden styleId = '<%= "estimated_start_schedule"+i%>' name = "ms" property = "estimated_start_schedule"/>
								<html:hidden styleId = '<%= "estimated_complete_schedule"+i %>' name = "ms" property = "estimated_complete_schedule"/>
										
							<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "status"/></td>
										
				  		</tr>
				  		<html:hidden name = "ms"  property = "estimated_complete_schedule" />
				  		<html:hidden name = "ms"  property = "estimated_start_schedule" />
				  		
					   <%i++;  %>
					  </logic:iterate>
				  </logic:present>
						<tr><td>&nbsp;</td>
							<td colspan = "17" class = "Nbuttonrow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  				<html:submit property = "save" styleClass = "button_c" onclick = "return validate();"><bean:message bundle = "AM" key = "am.material.submit"/></html:submit>
				  				
				  				<c:choose>
								 	<c:when test  = "${gridControl.activityStatusFlag eq 'Y'}">
										<html:submit property = "approve" styleClass = "button_c" onclick = "return massApprove('a');"><bean:message bundle = "AM" key = "am.tabular.massApprove"/></html:submit>								 		
										<html:submit property = "draft" styleClass = "button_c"  onclick = "return massApprove('d');"><bean:message bundle = "AM" key = "am.tabular.massDraft"/></html:submit>	
								 	</c:when>
								 	<c:when test  = "${gridControl.activityStatusFlag ne 'Y'}">
								 		<html:submit property = "approve" styleClass = "button_c" disabled = "true"  onclick = "return massApprove('a');"><bean:message bundle = "AM" key = "am.tabular.massApprove"/></html:submit>								 		
										<html:submit property = "draft" styleClass = "button_c"  disabled = "true" onclick = "return massApprove('d');"><bean:message bundle = "AM" key = "am.tabular.massDraft"/></html:submit>	
								 	</c:when>
								</c:choose>
								<c:if test="${gridControl.activityDeleteFlag eq 'Y'}">
									<html:submit property = "delete" styleClass = "button_c" onclick = "return del();"><bean:message bundle = "AM" key = "am.tabular.massDelete"/></html:submit>
								</c:if>
								<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "job.tabular.cancel"/></html:reset>
							</td>
						
						</tr>
<% } else { %>
				<tr>
						<td width ="2">&nbsp;</td>
		    			<td  colspan = "8" class = "message" height = "30">
		    				<bean:message bundle = "pm" key ="activity.not.present"/>
		    			</td>
		    	</tr>
		
<% } %>					
					</table>
					
					</td>
					</tr>
					 
						<jsp:include page = '/Footer.jsp'>
						      <jsp:param name = 'colspan' value = '37'/>
						      <jsp:param name = 'helpid' value = 'activitytabularpage'/>
						 </jsp:include>	
				
			</table>
		</td>
	</tr>
</table>
</html:form>
<script>
function sortwindow()
{
	str = 'SortAction.do?Type=Activity&ref=<bean:write name = "ActivityListForm" property = "ref"/>&Job_Id=<%= job_Id %>&from=<bean:write name = "ActivityListForm" property = "from" />';
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
	
    if (window.showModelessDialog) {        // Internet Explorer
    	p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' )
    } 
    else {//other browsers
        window.open (str, "popupwindow","width=400, height=350, alwaysRaised=yes,top=250,left=450");
    }
	
}

function validate()
{
	var submitflag = 'false';
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( !document.forms[0].check.checked )
			{
				if( document.forms[0].job_type.value != 'Default' )
		  		{}
				else
				{
					alert( "<bean:message bundle = "pm" key = "activity.tabular.selectrow"/>" );
					return false;
				}
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' )
		  	{
					alert( "<bean:message bundle = "pm" key = "activity.tabular.selectrow"/>" );
					return false;
		  	}
		}
	}
	else
	{
		
	}
	
	
	if( <%= activity_size %> != 0 )
	{	
		if( <%= activity_size %> == 1 )
		{
			if( document.forms[0].check.checked ) 
	  		{
	  			
	  			if( document.forms[0].activitytypecombo.value == "0" )
				{
					alert( "<bean:message bundle = "pm" key = "activity.tabular.selectactivitytype"/>" );
					return false;
				}
		
				
			/*	if(document.forms[0].estimated_start_schedule.value!="" && document.forms[0].estimated_complete_schedule.value!="") 
				{
					if (!compDate_mdy(document.forms[0].estimated_start_schedule.value, document.forms[0].estimated_complete_schedule.value)) 
					{
						document.forms[0].estimated_complete_schedule.focus();
						alert("<bean:message bundle = "pm" key = "activity.tabular.datevalidation"/>");
						return false;
					} 
				}   */
				
				
				if( document.forms[0].quantity.value == "" )
				{
					alert( "<bean:message bundle = "pm" key = "activity.tabular.blankquantity"/>" );
					document.forms[0].quantity.focus();
					return false;
				}
		
				else
				{
					if( !isFloat( document.forms[0].quantity.value ) )
					{
						alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitynumeric"/>" );
						document.forms[0].quantity.value = "";
						document.forms[0].quantity.focus();
						return false;
					}
					
					else
					{
						if( document.forms[0].quantity.value == parseFloat( "0.0" ) )
						{
							alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterzero"/>" );
							document.forms[0].quantity.value = "";
							document.forms[0].quantity.focus();
							return false;	
						} 
					}
				}
				
	 		}
		}
		
		else
		{
			for( var i = 0; i < document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			if( document.forms[0].activitytypecombo[i].value == "0" )
					{
						alert( "<bean:message bundle = "pm" key = "activity.tabular.selectactivitytype"/>" );
						return false;
					}
		
				/*  	if( document.forms[0].estimated_start_schedule[i].value!="" && document.forms[0].estimated_complete_schedule[i].value!="") 
					{
						if (!compDate_mdy(document.forms[0].estimated_start_schedule[i].value, document.forms[0].estimated_complete_schedule[i].value)) 
						{
							document.forms[0].estimated_complete_schedule[i].focus();
							alert("<bean:message bundle = "pm" key = "activity.tabular.datevalidation"/>");
							return false;
						} 
					}  */
				
					if( document.forms[0].quantity[i].value == "" )
					{
						alert( "<bean:message bundle = "pm" key = "activity.tabular.blankquantity"/>" );
						document.forms[0].quantity[i].focus();
						return false;
					}
		
					else
					{
						if( !isFloat( document.forms[0].quantity[i].value ) )
						{
							alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitynumeric"/>" );
							document.forms[0].quantity[i].value = "";
							document.forms[0].quantity[i].focus();
							return false;
						}
						
						else
						{
							if( document.forms[0].quantity[i].value == parseFloat( "0.0" ) )
							{
								alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterzero"/>" );
								document.forms[0].quantity[i].value = "";
								document.forms[0].quantity[i].focus();
								return false;	
							} 
						}
					}
				}
		 	}
		 }
 	}
	
	document.forms[0].ref.value = "Submit";
	return true;
}


function addActivity()
{
document.forms[0].action = "ActivityEditAction.do?Job_Id=<bean:write name = "ActivityListForm" property = "job_Id" />&ref=<bean:write name = "ActivityListForm" property = "chkaddendum" />&Activity_Id=0&from=<bean:write name = "ActivityListForm" property = "from" />";
document.forms[0].submit();
return true;
}

</script>

<script>
function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;

}
function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
if(document.forms[0].ref.value!='inscopedetailactivity'){
	var layers, leftAdj=0;
	leftAdj =document.body.clientWidth;
	leftAdj = leftAdj - 280;
	document.getElementById("filter").style.left=leftAdj;
}
}

function massApprove(x)
{
	var status = x;
	var submitflag = 'false';
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) )
			{
				if(status == 'a'){
					alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassapprove"/>" );
				}
				if(status == 'd'){
					alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdraft"/>" );
				}
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
			
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' )
		  	{
				if(status == 'a'){
					alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassapprove"/>" );
				}
				if(status == 'd'){
					alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdraft"/>" );
				}
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassapprove"/>" );
	}
	
	
	if(submitflag =='true')
	{
		document.forms[0].action = "ActivityUpdateAction.do?ref=<bean:write name = "ActivityListForm" property = "chkaddendum" />&Job_Id=<bean:write name = "ActivityListForm" property = "job_Id" />";

		return true;
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.material.selectresourceformassapprove"/>" );
		return false;
	}
}

function del()
{
	var submitflag = 'false';
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdelete"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false'  )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdelete"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdelete"/>" );
	}
	

	if(submitflag =='true')
	{
		
		var convel = confirm( "<bean:message bundle = "AM" key = "am.tabuler.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "ActivityUpdateAction.do?ref=massDeleteActivity&Job_Id=<bean:write name = "ActivityListForm" property = "job_Id" />&addendum_id=<bean:write name = "ActivityListForm" property = "addendum_id" />&type=<bean:write name = "ActivityListForm" property = "chkaddendum" />";

			return true;	
		}
		else
		{
			return false;	
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdelete"/>" );
		return false;
	}
}

function setlistpricezero(index)
{
	if(index != null) {
		if( document.forms[0].activitytypecombo[index].value == "E" || document.forms[0].activitytypecombo[index].value == "P" ) 
		{
			document.forms[0].listprice[index].value = '0.00';
			document.forms[0].proformamargin[index].value = '0.00';
		} else {
			document.forms[0].listprice[index].value = round_func( parseFloat( document.forms[0].extendedprice[index].value ) + parseFloat( document.forms[0].overheadcost[index].value ) );
			if(  parseFloat( document.forms[0].listprice[index].value ) > 0 )
				{
					document.forms[0].proformamargin[index].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[index].value ) / parseFloat( document.forms[0].listprice[index].value ) ) );
				}
		}
	} else {
		if( document.forms[0].activitytypecombo.value == "E" || document.forms[0].activitytypecombo.value == "P" ) 
		{
			document.forms[0].listprice.value = '0.00';
			document.forms[0].proformamargin.value = '0.00';
		} else {
			document.forms[0].listprice.value = round_func( parseFloat( document.forms[0].extendedprice.value ) + parseFloat( document.forms[0].overheadcost.value ) );
			if(  parseFloat( document.forms[0].listprice.value ) > 0 )
				{
					document.forms[0].proformamargin.value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost.value ) / parseFloat( document.forms[0].listprice.value ) ) );
				}
		}
	}
}

function multiplyquantity(materialCost, 
		cnsFieldCost, cnshqlLaborCost, contractLabor,
		freightCost, travelCost, totalCost, extendedCost,
		prevQty, index )
{
	if( index != null )
	{
		if( document.forms[0].quantity[index].value == "" )
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.blankquantity"/>" );
			document.forms[0].quantity[index].focus();
			return false;
		}
		if( isFloat( document.forms[0].quantity[index].value ) )
		{
			if( !(document.forms[0].quantity[index].value > 0 ) )
			{
				alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterzero"/>" );
				document.forms[0].quantity[index].value = '';
				document.forms[0].quantity[index].focus();
				return false;
			}
			
		//	document.forms[0].estimated_materialcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(materialCost)));
			document.forms[0].estimated_materialcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(parseFloat(materialCost)/prevQty)));
			
			document.forms[0].estimated_cnsfieldlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(parseFloat( cnsFieldCost)/prevQty)));		
			
			document.forms[0].estimated_cnshqlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(parseFloat(cnshqlLaborCost)/prevQty)));		
			document.forms[0].estimated_cnsfieldhqlaborcost[index].value = round_func( parseFloat( document.forms[0].estimated_cnsfieldlaborcost[index].value ) + parseFloat( document.forms[0].estimated_cnshqlaborcost[index].value ) );
			
			document.forms[0].estimated_contractfieldlaborcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(parseFloat(contractLabor)/prevQty)));		
			document.forms[0].estimated_freightcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(parseFloat(freightCost)/prevQty)));		
			document.forms[0].estimated_travelcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(parseFloat(travelCost)/prevQty)));		
			document.forms[0].estimated_totalcost[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(parseFloat(totalCost)/prevQty)));			
			document.forms[0].extendedprice[index].value = round_func( ( parseFloat( document.forms[0].quantity[index].value ) * parseFloat(parseFloat( extendedCost )/prevQty)));			
			if( document.forms[0].activitytypecombo[index].value == "O" || document.forms[0].activitytypecombo[index].value == "P" ) 
			{
			if(document.forms[0].activitytypecombo[index].value == "P"){
				document.forms[0].listprice[index].value = '0.00';
			}else {
				document.forms[0].listprice[index].value = round_func( parseFloat( document.forms[0].extendedprice[index].value ) + parseFloat( document.forms[0].overheadcost[index].value ) );
			}
				if(  parseFloat( document.forms[0].listprice[index].value ) > 0 )
				{
					document.forms[0].proformamargin[index].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[index].value ) / parseFloat( document.forms[0].listprice[index].value ) ) );
				}
			}
			else
			{	
				calnew( index );
			}
		}
		
		else
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitynumeric"/>" );
			document.forms[0].quantity[index].value = '';
			document.forms[0].quantity[index].focus();
			return false;
		}
	}
	
	else
	{
		if( document.forms[0].quantity.value == "" )
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitynumeric"/>" );
			document.forms[0].quantity.focus();
			return false;
		}
		
		if( isFloat( document.forms[0].quantity.value ) )
		{
			if( !( document.forms[0].quantity.value > 0 ) )
			{
				alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitygreaterzero"/>" );
				document.forms[0].quantity.value = '';
				document.forms[0].quantity.focus();
				return false;
			}
			
			
			
			document.forms[0].estimated_materialcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(parseFloat(materialCost)/prevQty)) );
			
			document.forms[0].estimated_cnsfieldlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(parseFloat( cnsFieldCost)/prevQty)));				
			document.forms[0].estimated_cnshqlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(parseFloat(cnshqlLaborCost)/prevQty)));			
			document.forms[0].estimated_cnsfieldhqlaborcost.value = round_func( parseFloat( document.forms[0].estimated_cnsfieldlaborcost.value ) + parseFloat( document.forms[0].estimated_cnshqlaborcost.value ) );
			
			
			document.forms[0].estimated_contractfieldlaborcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(parseFloat(contractLabor)/prevQty) ) );		
			document.forms[0].estimated_freightcost.value = round_func( ( parseFloat(  document.forms[0].quantity.value ) * parseFloat(parseFloat(freightCost)/prevQty)));		
			document.forms[0].estimated_travelcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(parseFloat(travelCost)/prevQty)));			
			document.forms[0].estimated_totalcost.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(parseFloat(totalCost)/prevQty)));				
			document.forms[0].extendedprice.value = round_func( ( parseFloat( document.forms[0].quantity.value ) * parseFloat(parseFloat( extendedCost )/prevQty)));			
			calnew( index );
		}
		else
		{
			alert( "<bean:message bundle = "pm" key = "activity.tabular.quantitynumeric"/>" );
			document.forms[0].quantity.value = '';
			document.forms[0].quantity.focus();
			return false;
		}
	}	
}

function calnew( index )
{	
	var overheadindex = '';
	for( var i = 0; i < document.forms[0].check.length; i++ )
	{
		if( document.forms[0].activitytypecombo[i].value == 'V' )
			overheadindex = i;
	}
	
	if( overheadindex == '' ) {
		setlistpricezero( index );
		return false;
	}

			
	if( parseFloat( document.forms[0].extendedprice[overheadindex].value ) > 0 ) 
	{	
		var tempreqextendedprice = "0.0";
		
		
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytypecombo[i].value == "R" || document.forms[0].activitytypecombo[i].value == "E" )
			{
				if( document.forms[0].extendedprice[i].value == '0.00' )
				{
					document.forms[0].overheadcost[i].value = '0.00';
					document.forms[0].listprice[i].value = '0.00';
					document.forms[0].proformamargin[i].value = '0.00';
				}
				
				tempreqextendedprice = round_func( parseFloat( tempreqextendedprice ) + parseFloat( document.forms[0].extendedprice[i].value ) );	
			}
		}
		
		if( parseFloat( tempreqextendedprice ) > 0 )
		{	
			for( var i = 0; i < document.forms[0].check.length; i++ )
			{
				if( document.forms[0].activitytypecombo[i].value == "R" || document.forms[0].activitytypecombo[i].value == "E")
				{
					document.forms[0].overheadcost[i].value = round_func( ( parseFloat( document.forms[0].extendedprice[i].value ) * parseFloat( document.forms[0].extendedprice[overheadindex].value ) ) / parseFloat( tempreqextendedprice ) ); 	
					
					//document.forms[0].check[i].checked = true;
					
					if(document.forms[0].activitytypecombo[i].value == "E"){
						document.forms[0].listprice[i].value = '0.00';
					} else {
						document.forms[0].listprice[i].value = round_func( parseFloat( document.forms[0].extendedprice[i].value ) + parseFloat( document.forms[0].overheadcost[i].value ) );
					}
					if(  parseFloat( document.forms[0].listprice[i].value ) > parseFloat( "0.0" ) )
					{
						document.forms[0].proformamargin[i].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[i].value ) / parseFloat( document.forms[0].listprice[i].value ) ) );
					}
				}
			}		
		}
	}	
	else
	{
		for( var i = 0; i < document.forms[0].check.length; i++ )
		{
			if( document.forms[0].activitytypecombo[i].value != "V" ) 
			{
				//document.forms[0].check[i].checked = true;
				
				if(document.forms[0].activitytypecombo[i].value == "E" || document.forms[0].activitytypecombo[i].value == "P"){
					document.forms[0].listprice[i].value = '0.00';
				} else {
					document.forms[0].listprice[i].value = round_func( parseFloat( document.forms[0].extendedprice[i].value ) + parseFloat( document.forms[0].overheadcost[i].value ) );
				}
				
				if(  parseFloat( document.forms[0].listprice[i].value ) > parseFloat( "0.0" ) )
				{
					document.forms[0].proformamargin[i].value = round_func( 1 - parseFloat( parseFloat( document.forms[0].estimated_totalcost[i].value ) / parseFloat( document.forms[0].listprice[i].value ) ) );
				}
			}
		}		
	}
}

/* For Bid Flag: Start */
function CheckAllBid(){
/*
	- This Function set value Bid Flag value of all activity in current activity list.
*/
	if( <%= activity_size %> > 1 ){
		if( document.forms[0].bidFlag.length > 0){
			/* Set value for all activities in list */
			for( var i = 0; i < document.forms[0].bidFlag.length; i++ )
			{
				if(document.forms[0].bidFlag[i].checked)
					document.forms[0].bidFlag[i].value = '1'; // - set checked value to 1
				else
					document.forms[0].bidFlag[i].value = '0'; // - set un checked value to 0
			}
			CheckAllCheckBox(); // - For checked all activity Bid Flag check box.
		}
	}
	else{
		if(document.forms[0].bidFlag.checked){
			document.forms[0].bidFlag.value = '1'; // - set checked value to 1
		}else{
			document.forms[0].bidFlag.value = '0'; // - set un checked value to 0
		}
		document.forms[0].bidFlag.checked = true;
	}
}

function CheckAllCheckBox(){
/*
	- This Function checked Bid Flag check box of all activities of current activity list.
*/
	if( <%= activity_size %> > 1 ){
		for( var i = 0; i < document.forms[0].bidFlag.length; i++ )
		{
			document.forms[0].bidFlag[i].checked = true;
		}
	}
}
/* For Bid Flag: End */
</script>
<%--
 <c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
	<c:if test = "${requestScope.jobType eq 'Addendum'}">
	
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
		<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/DefaultJobMenuScript.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
</c:if>
--%>
</html:html>
