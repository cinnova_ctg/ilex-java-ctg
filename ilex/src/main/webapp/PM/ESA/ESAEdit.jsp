<!DOCTYPE HTML>
<%@ include file = "/Header.inc" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
	 <title>ESAEdit Page</title>
	<script language = "JavaScript" src = "javascript/UtilityFunction.js"></script> 
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js"></script>
	<script language = "JavaScript" src = "javascript/POActivityResource/GeneralResource.js"></script>	
	
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	
<style type="text/css">
	.agentSelect{font-size: 11px; border: 1px solid #b9cada;}
</style>

<script>
	function setFormValues() {
		for(var i=0; i<3; i++) {
			if(document.getElementsByName('status'+eval(i))[0].checked) {
				document.forms[0].status[i].value = document.getElementsByName('status'+eval(i))[0].value;
			}
			else if(document.getElementsByName('status'+eval(i))[1].checked) {
				document.forms[0].status[i].value = document.getElementsByName('status'+eval(i))[1].value;
			}
			
			if(document.getElementsByName('commissionType'+eval(i))[0].checked) {
				document.forms[0].commissionType[i].value = document.getElementsByName('commissionType'+eval(i))[0].value;
			} else if(document.getElementsByName('commissionType'+eval(i))[1].checked) {
				document.forms[0].commissionType[i].value = document.getElementsByName('commissionType'+eval(i))[1].value;
			} else 
				document.forms[0].commissionType[i].value = '';
				
			if(!document.forms[0].agent[i].disabled && document.forms[0].agent[i].value != '0') {
				document.forms[0].commission[i].value = trimUpto4Decimal(document.forms[0].commission[i].value);
				
				if(!isFloat(document.forms[0].commission[i].value)) {
					alert('Commission Rate for External Sales Agent '+(i+1)+' must be a float value.');
					document.forms[0].commission[i].focus();
					return false;
				} else {
					if(document.forms[0].commission[i].value == '' || round_funcfor4digits(eval(document.forms[0].commission[i].value)) < 0.0000
						|| round_funcfor4digits(eval(document.forms[0].commission[i].value)) >1.00) {
						alert('Commission Rate for External Sales Agent '+(i+1)+' must be >= 0.0 and < 1.00.');
						document.forms[0].commission[i].focus();
						return false;
					}
				}
				if(document.forms[0].commissionType[i].value == '')	{
					alert('Please enter Commission Type for External Sales Agent '+(i+1)+'.');
					document.forms[0].commissionType[i].focus();
					return false;
				}
				if(document.forms[0].payementTerms[i].value == '0')	{
					alert('Please enter Payment Terms for External Sales Agent '+(i+1)+'.');
					document.forms[0].payementTerms[i].focus();
					return false;
				}
				if(document.forms[0].effectiveDate[i].value == '')	{
					alert('Please enter Effective Date for External Sales Agent '+(i+1)+'.');
					document.forms[0].effectiveDate[i].focus();
					return false;
				} else {
					if(!compareDateWithCurrentDate(document.forms[0].effectiveDate[i].value)) {
						alert('Effective Date for External Sales Agent'+(i+1)+' must not be greater than current date.');
						return false;
					}
				}
				if(document.forms[0].status[i].value == '')			{
					alert('Please enter Agent Status for External Sales Agent '+(i+1)+'.');
					document.forms[0].status[i].focus();
					return false;
				}
			}
		}
		
		for(var i=0; i<3; i++) {
			document.forms[0].agent[i].disabled = false;
			document.forms[0].commission[i].disabled = false;
			document.forms[0].payementTerms[i].disabled = false;
			document.forms[0].effectiveDate[i].disabled = false;
			document.getElementsByName('status'+eval(i))[0].disabled = false;
			document.getElementsByName('status'+eval(i))[1].disabled = false;
			document.getElementsByName('commissionType'+eval(i))[0].disabled = false;
			document.getElementsByName('commissionType'+eval(i))[1].disabled = false;
		}
		return true;
	}
	
function trimUpto4Decimal(val) {
	if(val.length > 6 ) {
		val = val.substring(0,6);
	}
	return val;
}	
	
function defaultPayementTerms(){
	var objAgent = document.forms[0].agent;
	var objPayementTerms = document.forms[0].payementTerms;
	for(var i=0; i<objAgent.length; i++){
		if(objAgent[i].value == '0')
			objPayementTerms[i].value = getSelectedOption(objPayementTerms[i],'Quarterly 90 Days Arrears/Paid Invoices');
	}
}


function compareDateWithCurrentDate(val) {
	var today = new Date();
	var currentDate;
	currentDate = today.getMonth()+1 + "/" + today.getDate() + "/" + today.getFullYear();
	
	if(!compDate_mdy(val, currentDate)) {
		return false;
	} else {
		return true;
	}
}
</script>

</head>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "defaultPayementTerms(); <c:if test = "${empty requestScope.fromPage}">
																							leftAdjLayers();OnLoad();
																						</c:if>editCombo();" >

<html:form action="/ESAEditAction.do?ref=view" >
<c:if test="${not empty requestScope.fromPage}">
		<jsp:include flush = "true" page = '/PRM/ViewSelectorPRM.jsp'>
			<jsp:param name = 'bean' value = 'ESAEditForm'/>
			<jsp:param name = 'projectType' value = '<%=(String)request.getParameter("fromPage")%>'/>
			<jsp:param name="url" value="ESAEditAction.do?"/>
			<jsp:param name = 'pageName' value = 'ESAEdit.jsp'/>
		</jsp:include>
	</c:if>
	<c:if test = "${empty requestScope.fromPage}">
		<jsp:include flush="true" page="/PM/PMViewSelector.jsp">
			<jsp:param name="bean" value="ESAEditForm"/>
			<jsp:param name="url" value="ESAEditAction.do?"/>
			<jsp:param name="fromPage" value="ESAEdit.jsp"/>
		</jsp:include>
	</c:if>
	
	<html:hidden name="ESAEditForm" property="appendixId"/>
	<html:hidden name="ESAEditForm" property="msaId"/>
	
	
	
	 <table border="0"  cellpadding="0" cellspacing="0">
	  <c:if test="${empty requestScope.esaSaveStatus}">
		  <tr>
		 	<td height="10">
		 	</td>
		 </tr>
	</c:if>
	 
	 <c:if test="${not empty requestScope.esaSaveStatus}">
	 	<tr><td style="padding-left: 10px;" height="10">
			<table border="0" cellpadding="0" cellspacing="0">
				<c:if test="${requestScope.esaSaveStatus eq '0'}">
					<tr>
						<td class="message">
							ESA Details Updated Successfully.
						</td>
					</tr>
				</c:if>
				<c:if test="${requestScope.esaSaveStatus ne '0'}">
					<tr>
						<td class="message">
							Failed to Update ESA Details.
						</td>
					</tr>
				</c:if>
			</table>
		</td></tr>	
		<tr>
		 	<td height="5">
		 	</td>
		 </tr>
	</c:if>

	 <tr>
		 <td style="padding-left: 10px;">
		 	<table border="0" cellpadding="0" cellspacing="1" width="720">
				<c:forEach var="esaList" items="${ESAEditForm.esaList}" varStatus="count">
					<c:set var="agentCount"  value="${count.index+1}"/>
					 <tr>
					 	<td colspan=2 class = "formCaption">External Sales Agent <c:out value="${agentCount}" /> </td>
					 </tr>
					 <input type="hidden" name="esaId" value="<c:out value="${esaList.esaId}" />" />
					 <input type="hidden" name="esaSeqNo" value="<c:out value="${agentCount}" />" />
						
					 <tr>
					 	<td class = "colDarkSmall">Agent:</td>
					 	<td class = "colLight">
					 		<select class="agentSelect" id="agent" name ="agent" <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> onchange="editCombo(document.forms[0].agent,<c:out value='${count.index}'/>);">
					 			<c:forEach items="${ESAEditForm.esaAgentList}" var="esaAgentList" varStatus="innerIndex">
									<option value="<c:out value='${esaAgentList.value}'/>" <c:if test="${ESAEditForm.agent[count.index] eq esaAgentList.value}">selected="selected"</c:if>>
									<c:out value='${esaAgentList.label}' /></option>
								</c:forEach>
					 		</select>
					 	</td>
					 </tr>
					<tr style="display: none;">
						<td class = "colDarkSmall">Agent:</td>
					 	<td class = "colLight">
					 		<select class="select" id="agenthidden<c:out value='${count.index}'/>" name ="agenthidden" style="visibility: hidden;">
					 			<c:forEach items="${ESAEditForm.esaAgentList}" var="esaAgentList" varStatus="innerIndex">
									<option value="<c:out value='${esaAgentList.value}'/>" <c:if test="${ESAEditForm.agent[count.index] eq esaAgentList.value}">selected="selected"</c:if>>
									<c:out value='${esaAgentList.label}' /></option>
								</c:forEach>
					 		</select>
					 	</td>
					</tr>
					<tr>
					 	<td class = "colDarkSmall">Commission:</td>
					 	<td class = "colLight">
						<%-- 	<input type="text" class="textbox" name = "commission" styleClass = "text"  value="<fmt:formatNumber value="${esaList.commission}" type="currency" pattern="#####0.0000"/>"  <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> /> --%>
				 		<input type="text" class="textbox" name = "commission" styleClass = "text"  value="<c:out value="${esaList.commission}"/>" <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> />  
					 	</td>
					 </tr>
					 
					 <tr>
					 	<td class = "colDarkSmall">Commission Type:</td>
					 	<td class = "colLight">
					 		<input type="radio" class="redio" name="commissionType<c:out value="${count.index}"/>" value="Add" <c:if test="${ESAEditForm.commissionType[count.index] eq 'Add'}">checked="checked"</c:if> <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> />&nbsp;Add&nbsp;&nbsp;
					 		<input type="radio" class="redio" name="commissionType<c:out value="${count.index}"/>" value="Blend" <c:if test="${ESAEditForm.commissionType[count.index] eq 'Blend'}">checked="checked"</c:if> <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> />&nbsp;Blend
					 		<input type="hidden" name="commissionType"/>
					 	</td>
					 </tr>
				 
					 <tr>
					 	<td class = "colDarkSmall">Payment Terms:</td>
					 	<td class = "colLight">
					 		<select class="select" id="payementTerms<c:out value='${count.index}'/>" name ="payementTerms" <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> >
					 			<c:forEach items="${ESAEditForm.esaPaymentList}" var="esaPaymentList" varStatus="innerIndex">
									<option value="<c:out value='${esaPaymentList.value}'/>" <c:if test="${ESAEditForm.payementTerms[count.index] eq esaPaymentList.value}">selected="selected"</c:if>>
									<c:out value='${esaPaymentList.label}' /></option>
								</c:forEach>
					 		</select>
					 	</td>
					 </tr>
				 
				 <tr>
				 	<td class = "colDarkSmall">Effective Date:</td>
				 	<td class = "colLight">
				 		<input type="text" class="textbox" size="10" id="effectiveDate<c:out value='${agentCount}'/>" name="effectiveDate" value="<c:out value='${esaList.effectiveDate}'/>" readonly="true" <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> />
						<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> onclick = "return popUpCalendar(document.getElementById('effectiveDate<c:out value="${agentCount}"/>'), document.getElementById('effectiveDate<c:out value="${agentCount}"/>'), 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;" />
				 	</td>
				 </tr>
				 
				  <tr>
				 	<td class = "colDarkSmall">Agent Status:</td>
				 	<td class = "colLight">
				 		<input type="radio" class="redio" name="status<c:out value="${count.index}"/>" value="1" <c:if test="${ESAEditForm.status[count.index] eq '1'}">checked="checked"</c:if> <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> />&nbsp;Active&nbsp;&nbsp;
				 		<input type="radio" class="redio" name="status<c:out value="${count.index}"/>" value="0" <c:if test="${ESAEditForm.status[count.index] eq '0'}">checked="checked"</c:if> <c:if test="${ESAEditForm.status[count.index] eq '0'}">disabled="true"</c:if> />&nbsp;Deactivated
				 		<input type="hidden" name="status"/>
				 	</td>
				 </tr>
				 <tr>
				 	<td height="5">
				 	</td>
				 </tr>
				 
			 </c:forEach>


			 <tr>
			 	<td colspan = 2>
			 		<c:choose>
			 			<c:when test="${not empty requestScope.appendixStatus && (requestScope.appendixStatus eq 'Approved' || requestScope.appendixStatus eq 'Pending Customer Review' || requestScope.appendixStatus eq 'Signed')}">
			 				<html:submit  property = "save" styleClass = "button_c" disabled="true">Save</html:submit>
			 			</c:when>
			 			<c:otherwise>
			 				<html:submit  property = "save" styleClass = "button_c" onclick="return setFormValues();">Save</html:submit>
			 			</c:otherwise>
			 		</c:choose>
			 		
			 		<html:reset  property = "reset" styleClass = "button_c"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>
			 	</td>
			 </tr>
			 <tr>
			 	<td colspan=2>
			 		<table>
			 			<tr>
							 <td class = "colLight" style="white-space: normal;">
								 Agents are defined as partners under PVS manager with the core competency of "External Sales Agent".
							 </td>
						</tr>
			 			<tr>
							<td height=2>
							</td>
						</tr>
						 <tr>
							 <td class = "colLight" style="white-space: normal;">
								 The agent sequence will align the selected partner with the ESA1, 2, or 3 resource.  In other words, External Sales Agent 1 will be aligned to the ESA 1 resource.  This will help to visually align the commissions to partner on the manage activities page where there is no direct reference to a partner.
							 </td>
						</tr>
						
						<tr>
							<td height=2>
							</td>
						</tr>
						<tr>
							 <td class = "colLight" style="white-space: normal;">
							 	Enter the commission as 0.XXXX. For example, to enter 5%, enter 0.05. To enter 5.5%, use 0.055.
							 </td>
						</tr>
						<tr>
							<td height=2>
							</td>
						</tr>
						<tr>
							 <td class = "colLight" style="white-space: normal;">
							 	Payment terms are defined in the ESA agreement.  Verify the terms before selecting a value since the list shows all terms used for PO and some are not applicable to commission payments.
							 </td>
						</tr>
						<tr>
							<td height=2>
							</td>
						</tr>
						<tr>
							 <td class = "colLight" style="white-space: normal;">
							 	Enter the effective date of the ESA agreement.  This date will be used to determine the revenue basis for eligible jobs.
							 </td>
						</tr>
						<tr>
							<td height=2>
							</td>
						</tr>
						<tr>
							 <td class = "colLight" style="white-space: normal;">
							 	With the exception of the effective date,  all data entered on this form is forward acting and will not impact jobs already created. 
							 </td>
						</tr>	
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</html:form>
</body>
<script>
/* Function for view selector: Start */
function changeFilter()
{
	document.forms[0].action="ESAEditAction.do?ref=view&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="ESAEditAction.do?ref=view&resetList=something";
	document.forms[0].submit();
	return true;

}
/* Function for view selector: End */
</script>

<script>
function editCombo(obj,index){
	var	tempValue = -1;
	$("select.agentSelect").each(function(i){
		if(i != index)
		{
			var selectObj = jQuery(this);
			var selectedValIndex = 0;
			var selectedOption = new Array();
			tempValue = selectObj.selectedValues();
				$("select.agentSelect").each(function(j){
					var selectObj1 = jQuery(this);
					if(selectObj1.selectedValues() != 0 && selectObj1.selectedValues()[0] != tempValue)
					{
						selectedOption[selectedValIndex] = selectObj1.selectedValues();
						selectedValIndex = selectedValIndex + 1;
					}
				});
				$("#agenthidden0").copyOptions(selectObj,'all');
				selectObj.removeOption(selectedOption);
				selectObj.selectOptions(tempValue);
			}
		});
}
</script>
</html>