<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>


<html>
<head>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/summary.css" rel="stylesheet" type="text/css" />

	<title>Commission Management Form</title>
	
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>

<script>
	function changeStatus(obj, cnt, cnt1) {
		if(obj.checked) {
			document.getElementById("statuslabel"+cnt1+cnt).innerText='On';
			document.getElementsByName("statusLabel"+cnt1)[cnt].value = 1;
		} else {
			document.getElementById("statuslabel"+cnt1+cnt).innerText='Off';
			document.getElementsByName("statusLabel"+cnt1)[cnt].value = 1;
		}
		toggleActId(cnt);
	}
	
	function toggleActId(cnt) {
		if(!document.getElementById("actId"+cnt).checked) {
			document.getElementById("actId"+cnt).checked = true;
		} 
	}
	
	function validate() {
		var actLen = document.forms[0].actId.length;
		
		if(!actLen) {
			if(!document.forms[0].activityId.disabled) {
				if(!document.forms[0].actId.checked) {
					alert('Please select activity to update.');
					return false;
				} else {
					document.forms[0].activityId.value = document.forms[0].actId.value;
					document.forms[0].activityQty.value = document.forms[0].actQty.value;
					
					for(var i=0; i<3;i++) {
						if(document.getElementsByName('esaId'+eval(i+1))[0].value != 0)	 {
							document.getElementsByName('actCommType'+eval(i+1))[0].disabled = false;
							document.getElementsByName('commStatus'+eval(i+1))[0].disabled = false;
							document.getElementsByName('actCommissionType'+eval(i+1))[0].value = document.getElementsByName('actCommType'+eval(i+1))[0].value;
							if(document.getElementsByName('commStatus'+eval(i+1))[0].checked)
								document.getElementsByName('commissionStatus'+eval(i+1))[0].value = 1;
							else
								document.getElementsByName('commissionStatus'+eval(i+1))[0].value = 0;					
							document.getElementsByName('commissionRate'+eval(i+1))[0].value = document.getElementsByName('commRate'+eval(i+1))[0].value;
						} 
					}
				}
			} else {
				alert('There must be atleast 1 activity record to update.');
				return false;
			}
		} else {
			var checkedFlag = false;
			for(var i = 0; i<actLen; i++) {
				if(document.forms[0].actId[i].checked)
					checkedFlag = true;
			}
			
			if(!checkedFlag) {
				alert('Please select atleast 1 activity to update.');
				return false;
			} else {
				for(var j = 0; j<actLen; j++) {
					if(document.forms[0].actId[j].checked) {
						document.forms[0].activityId[j].value = document.forms[0].actId[j].value;
						document.forms[0].activityQty[j].value = document.forms[0].actQty[j].value;
						for(var i=0; i<3;i++) {
							if(document.getElementsByName('esaId'+eval(i+1))[j].value != 0)	 {
								document.getElementsByName('actCommType'+eval(i+1))[j].disabled = false;
								document.getElementsByName('commStatus'+eval(i+1))[j].disabled = false;
								document.getElementsByName('actCommissionType'+eval(i+1))[j].value = document.getElementsByName('actCommType'+eval(i+1))[j].value;
								
								if(document.getElementsByName('commStatus'+eval(i+1))[j].checked)
										document.getElementsByName('commissionStatus'+eval(i+1))[j].value = 1;
									else
										document.getElementsByName('commissionStatus'+eval(i+1))[j].value = 0;					
								
								
								document.getElementsByName('commissionRate'+eval(i+1))[j].value = document.getElementsByName('commRate'+eval(i+1))[j].value;
							}
							
						}
					}
				}
			}
		} 
		return true;
	}

	function resetAll() {
		var actLen = document.forms[0].actId.length;
		if(!actLen) {
			for(var j=0;j<3;j++) {
				if(document.getElementsByName('esaId'+eval(j+1))[0].value != 0)	 {
					if(document.getElementsByName("statusLabel"+eval(j+1))[0].value == 1)  {
						document.getElementsByName("statusLabel"+eval(j+1))[0].value = 0;
						if(document.getElementById("statuslabel"+eval(j+1)+0).innerText == 'On') {
							document.getElementById("statuslabel"+eval(j+1)+0).innerText = 'Off';
						} else if(document.getElementById("statuslabel"+eval(j+1)+0).innerText == 'Off') {
							document.getElementById("statuslabel"+eval(j+1)+0).innerText = 'On';
						}
					} 
				}
			}
		} else {
			for(var i=0;i<actLen;i++) {
				for(var j=0;j<3;j++) {
					if(document.getElementsByName('esaId'+eval(j+1))[i].value != 0)	 {
						if(document.getElementsByName("statusLabel"+eval(j+1))[i].value == 1)  {
							document.getElementsByName("statusLabel"+eval(j+1))[i].value = 0;
							if(document.getElementById("statuslabel"+eval(j+1)+i).innerText == 'On') {
								document.getElementById("statuslabel"+eval(j+1)+i).innerText = 'Off';
							} else if(document.getElementById("statuslabel"+eval(j+1)+i).innerText == 'Off') {
								document.getElementById("statuslabel"+eval(j+1)+i).innerText = 'On';
							}
						} 
					} 
				}
			}
		}
	}
	
	function ShowDiv() {
		leftAdjLayers();
		document.getElementById("filter").style.visibility="visible";
		return false;
	}
	
	function hideDiv() {
		document.getElementById("filter").style.visibility="hidden";
		return false;
	}
	
	function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
	}
</script>

</head>
<%
String jobRef = "";
int esaListSize = 0;
int activityListSize = 0;
if(request.getAttribute("esaAgentSize") != null) {
	esaListSize = Integer.parseInt(request.getAttribute("esaAgentSize").toString());
}

if(request.getAttribute("activityListSize") != null) {
	activityListSize = Integer.parseInt(request.getAttribute("activityListSize").toString());
}

%>
<body <c:if test="${ESAActivityCommissionForm.jobRef eq 'detailjob'}" >onLoad ="leftAdjLayers();"</c:if>>
<html:form action="/ESAActivityCommissionAction">
<input type="hidden" name="appendixId" value="<c:out value='${ESAActivityCommissionForm.appendixId}'/>"/>
<input type="hidden" name="jobid" value="<c:out value='${ESAActivityCommissionForm.jobid}'/>"/>
<input type="hidden" name="jobRef" value="<c:out value='${ESAActivityCommissionForm.jobRef}'/>" />


<c:if test="${ESAActivityCommissionForm.jobRef eq 'inscopejob'}" >
	<c:if test="${requestScope.jobType eq 'Default'}" >
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
         	<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>  
         	<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
		 	<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
						 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
						 <a href="JobDashboardAction.do?function=view&jobid=<bean:write name = "ESAActivityCommissionForm" property = "jobid" />&appendix_id=<bean:write name = "ESAActivityCommissionForm" property = "appendixId" />"><c:out value='${requestScope.jobName }'/></a>
						 <a><span class="breadCrumb1">ESA/Activity Commission Management</span></a>
			    </td>
			</tr>
	        <tr><td height="5" >&nbsp;</td></tr>
		</table>
	</c:if>
	
	<c:if test="${requestScope.jobType eq 'Addendum'}" >
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
			<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
			<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
			<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			</tr>
			<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
			         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
						 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
						 <a href="JobDashboardAction.do?function=view&jobid=<bean:write name = "ESAActivityCommissionForm" property = "jobid" />&appendix_id=<bean:write name = "ESAActivityCommissionForm" property = "appendixId" />"><c:out value='${requestScope.jobName }'/></a>
						 <a><span class="breadCrumb1">ESA/Activity Commission Management</span></a>
			    </td>
			</tr>
	        <tr><td height="5" >&nbsp;</td></tr>	
		</table>
	</c:if>
	
	<c:if test="${requestScope.jobType ne 'Addendum' && requestScope.jobType ne 'Default'}">
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
						<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
						<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
						<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
				</tr>
				<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
				         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
										 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
										 <a><span class="breadCrumb1">ESA/Activity Commission Management</a>
				    </td>
				</tr>
		        <tr><td height="5" >&nbsp;</td></tr>	
		</table>
	</c:if>
</c:if>	
<c:if test="${ESAActivityCommissionForm.jobRef eq 'detailjob'}" >	
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr valign="top">
			<td width="100%">	
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19" width = "100%" >&nbsp;</td></tr> 
					<tr><td background="images/content_head_04.jpg" height="21" width = "100%" >
						<div id="breadCrumb">
		          				<a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
		          				<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<c:out value='${requestScope.msaId }'/>">
		          				<c:out value='${requestScope.msaName }'/></a>
		          				<A href="JobUpdateAction.do?ref=<c:out value='${ESAActivityCommissionForm.jobRef}' />&Appendix_Id=<c:out value='${requestScope.appendixId }'/>&Name=<c:out value='${requestScope.appendixName }'/>">
		          				<c:out value='${requestScope.appendixName }'/>
							</div>
					</td></tr>
					<tr>
					   	<td>
					   		<h2>
					   			<A class="head" href="JobDetailAction.do?ref=detailjob&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/>: </A>
					   			ESA/Activity Commission
					   		</h2>
					   	</td>
				   	</tr>		
				</table>
			</td>	
			<td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
			<td valign="top" width="155">
    			<table width="100%" cellpadding="0" cellspacing="0" border="0">
        			<tr><td width="155" height="39"  class="headerrow" colspan="2">
        					<table width="100%" cellpadding="0" cellspacing="0" border="0">
              					<tr><td width="150">
              						<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              						    <tr>
							           		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                  <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							            </tr>
                  					</table>
                  					</td>
              					</tr>
            				</table>
            			</td>
            		</tr>
        			<tr><td height="21" background="images/content_head_04.jpg" width="140">
          					<table width="100%" cellpadding="0" cellspacing="0" border="0">
              					<tr><td nowrap="nowrap" colspan="2">
	                					<div id="featured1">
	                						<div id="featured">
												<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
											</div>								
	                					</div>		
										<span>
											<div id="filter">
        										<table width="250" cellpadding="0" class="divnoview">
                    								<tr><td width="50%" valign="top">
                    										<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          										<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    											</tr>
                        									</table>
                        								</td>
					                       			</tr>
  												</table>											
											</div>
										</span>
									</td>
								</tr>
            				</table>
        				</td>
    				 </tr>
				</table>
			</td> 
		</tr>
	</table>		
</c:if>	

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<c:if test="${empty requestScope.saveStatus}">
			<tr valign="top" height="20">
				<td height="5"></td>
			</tr>
		</c:if>
		<c:if test="${not empty requestScope.saveStatus}">
			<c:if test="${requestScope.saveStatus eq 0}">
				<tr valign="top" height="25"><td style="padding-left: 12px;" class="message">
						ESA/Activity Commission record(s) saved successfully.
				</td></tr>	
			</c:if>
			<c:if test="${requestScope.saveStatus ne 0}">
				<tr valign="top" height="25"><td style="padding-left: 12px;" class="message">
					Failed to update ESA/Activity Commission record(s).
				</td></tr>
			</c:if>
		</c:if>
		
		<tr valign="top">
			<td style="padding-left: 10px;" >
				<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
					<tr>
						<td>
							<table border="0" cellspacing="1" cellpadding="1">
								<tr>
									<td class="Ntryb" <c:if test="${requestScope.esaAgentSize > 0}"> rowspan="2"</c:if> width="15"></td>
									<td class="Ntryb" <c:if test="${requestScope.esaAgentSize > 0}"> rowspan="2"</c:if> width="85">Activities</td>
									<td class="Ntryb" <c:if test="${requestScope.esaAgentSize > 0}"> rowspan="2"</c:if> width="55" align="center">Activity<br>Qty</td>
									<td class="Ntryb" <c:if test="${requestScope.esaAgentSize > 0}"> rowspan="2"</c:if> width="60" align="center">Resource<br>Basis</td>
									<td class="Ntryb" <c:if test="${requestScope.esaAgentSize > 0}"> rowspan="2"</c:if> width="70" align="center">Total Resource Revenue</td>
									<c:if test="${requestScope.esaAgentSize > 0}">
										<c:forEach var="esaAgents" items="${ESAActivityCommissionForm.agents}" varStatus="count">
											<td class="Ntryb" colspan="5" lign="center"><c:out value='${esaAgents.label}'/></td>
											<input type="hidden" name="idESA" value="<c:out value="${esaAgents.value}" />" /> 
										</c:forEach>
									</c:if>
								</tr>
								<c:if test="${requestScope.esaAgentSize > 0}">
									<tr>
										<% for(int i = 0; i< esaListSize; i++) {
										%>
											<td class="Ntryb" width="60">Type</td>
											<td class="Ntryb" align="center" nowrap="nowrap">Status</td>
											<td class="Ntryb" width="45">%</td>
											<td class="Ntryb" align="center" width="70">&nbsp;Cost&nbsp;</td>
											<td class="Ntryb" align="center" width="45">Revenue</td>
																					<%}	%>
										<td>&nbsp;</td>
									</tr>
								</c:if>		
								
								
								<c:forEach var="esaActList" items="${ESAActivityCommissionForm.esaActivityList}" varStatus="count">
									<tr>
										<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="center" valign="middle">
											<input type="checkbox" name="actId" id="actId<c:out value="${count.index}"/>" value="<c:out value="${esaActList.actId}" />" <c:if test="${esaActList.actType eq 'N'}">disabled="disabled"</c:if> />
											<input type="hidden" name="activityId" value="" />
										</td>
										
										<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if>>
											<c:out value="${esaActList.activityName}" />
										</td>
										<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="center">
											<c:out value="${esaActList.actQty}" />
											<input type="hidden" name="actQty" value="<c:out value="${esaActList.actQty}" />" />
										</td>
										<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right" >
											<fmt:formatNumber value="${esaActList.resBasis}" type="currency" pattern='#,###,##0.00'/>
										</td>
										
										<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
											<fmt:formatNumber value="${esaActList.resTotRevenue}" type="currency" pattern='#,###,##0.00'/>
										</td>
									
										<input type="hidden" name="activityQty" value="" />
										<input type="hidden" name="resourceBasis" value="<fmt:formatNumber value="${esaActList.resBasis}" type="currency" pattern='#,###,##0.00'/>" />
										<input type="hidden" name="resRevenue" value="<c:out value="${esaActList.resTotRevenue}" />" />

										<c:choose>
											<c:when test="${not empty esaActList.esaId1 && esaActList.esaId1 ne '0'}">
												
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="center">
													<c:out value="${esaActList.esaTypeLabel1}" />
													<input type="hidden" name="actCommType1" id="actCommType1<c:out value='${count.index}' />" value="<c:out value="${esaActList.esaType1}" />" />
													<input type="hidden" name="actCommissionType1" id="actCommissionType1<c:out value='${count.index}' />" value="" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="center">
													<input type="checkbox" id="commStatus1<c:out value='${count.index}' />" name="commStatus1" value="<c:out value="${esaActList.commissionStatus1}" />" 
														<c:if test="${esaActList.commissionStatus1 eq 1}">
															checked = "checked" 
														</c:if> onclick="javascript:changeStatus(this, '<c:out value='${count.index}' />','1');"
													<c:if test="${esaActList.actType eq 'N'}">disabled="disabled"</c:if> <c:if test="${esaActList.esaActActiveStatus1 eq '0'}">disabled="disabled"</c:if> /> 
													
													<c:if test="${esaActList.commissionStatus1 eq 1}">&nbsp;<span id="statuslabel1<c:out value='${count.index}' />">On</span></c:if>
													<c:if test="${esaActList.commissionStatus1 eq 0}">&nbsp;<span id="statuslabel1<c:out value='${count.index}' />">Off</span></c:if> 
													<input type="hidden" name="statusLabel1" value="0" />
													<input type="hidden" name="commissionStatus1" id="commissionStatus1<c:out value='${count.index}' />" value="" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
													<fmt:formatNumber value="${esaActList.commissionRate1}" type="currency" pattern="#####0.0000"/>
													<input type="hidden" name="commRate1" id="commRate1<c:out value='${count.index}' />" value="<fmt:formatNumber value="${esaActList.commissionRate1}" type="currency" pattern="#####0.0000"/>" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
													<fmt:formatNumber value="${esaActList.esaCost1}" type="currency" pattern='#,###,##0.00'/>
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
													<fmt:formatNumber value="${esaActList.esaRevenue1}" type="currency" pattern='#,###,##0.00'/>
												</td>
												<input type="hidden" name="esaActCost1" value="<fmt:formatNumber value="${esaActList.esaCost1}" type="currency" pattern='#,###,##0.00'/>" /> 
												<input type="hidden" name="esaActRevenue1" value="<fmt:formatNumber value="${esaActList.esaRevenue1}" type="currency" pattern='#,###,##0.00'/>" /> 
												<input type="hidden" name="commissionRate1" value="" />
												<input type="hidden" name="esaActActiveStatus1" value="<c:out value='${esaActList.esaActActiveStatus1}' />" />
											</c:when>
										</c:choose>
										<input type="hidden" name="esaId1" value="<c:out value="${esaActList.esaId1}" />" /> 
										
										<c:choose>
											<c:when test="${not empty esaActList.esaId2 && esaActList.esaId2 ne '0'}">
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="center">
													<c:out value="${esaActList.esaTypeLabel2}" />
													<input type="hidden" name="actCommType2" id="actCommType2<c:out value='${count.index}' />" value="<c:out value="${esaActList.esaType2}" />" />
													<input type="hidden" id="actCommissionType2<c:out value='${count.index}' />" name="actCommissionType2" value="<c:out value='${esaActList.esaType2}' />" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="center">
													<input type="checkbox" id="commStatus2<c:out value='${count.index}' />" name="commStatus2" value="<c:out value="${esaActList.commissionStatus2}" />" 
														<c:if test="${esaActList.commissionStatus2 eq 1}">
															checked = "checked" 
														</c:if> onclick="javascript:changeStatus(this, '<c:out value='${count.index}' />', '2');"
													<c:if test="${esaActList.actType eq 'N'}">disabled="disabled"</c:if> <c:if test="${esaActList.esaActActiveStatus2 eq '0'}">disabled="disabled"</c:if> /> 
													<c:if test="${esaActList.commissionStatus2 eq 1}">&nbsp;<span id="statuslabel2<c:out value='${count.index}' />">On</span></c:if>
													<c:if test="${esaActList.commissionStatus2 eq 0}">&nbsp;<span id="statuslabel2<c:out value='${count.index}' />">Off</span></c:if> 
													<input type="hidden" name="statusLabel2" value="0" />
													<input type="hidden" id="commissionStatus2<c:out value='${count.index}' />" name="commissionStatus2" value="<c:out value='${esaActList.actId}' />" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
													<fmt:formatNumber value="${esaActList.commissionRate2}" type="currency" pattern="#####0.0000"/>
													<input type="hidden" name="commRate2" id="commRate2<c:out value='${count.index}' />" value="<fmt:formatNumber value="${esaActList.commissionRate2}" type="currency" pattern="#####0.0000"/>" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
													<fmt:formatNumber value="${esaActList.esaCost2}" type="currency" pattern='#,###,##0.00'/>
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
													<fmt:formatNumber value="${esaActList.esaRevenue2}" type="currency" pattern='#,###,##0.00'/>
												</td>
												<input type="hidden" name="esaActCost2" value="<fmt:formatNumber value="${esaActList.esaCost2}" type="currency" pattern='#,###,##0.00'/>" /> 
												<input type="hidden" name="esaActRevenue2" value="<fmt:formatNumber value="${esaActList.esaRevenue2}" type="currency" pattern='#,###,##0.00'/>" /> 
												<input type="hidden" name="commissionRate2" value="" />
												<input type="hidden" name="esaActActiveStatus2" value="<c:out value='${esaActList.esaActActiveStatus2}' />" />
											</c:when>
										</c:choose>
										<input type="hidden" name="esaId2" value="<c:out value="${esaActList.esaId2}" />" /> 
										
										<c:choose>
											<c:when test="${not empty esaActList.esaId3 && esaActList.esaId3 ne '0'}">
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="center">
													<c:out value="${esaActList.esaTypeLabel3}" />
													<input type="hidden" name="actCommType3" id="actCommType3<c:out value='${count.index}' />" value="<c:out value="${esaActList.esaType3}" />" />
													<input type="hidden" id="actCommissionType3<c:out value='${count.index}' />" name="actCommissionType3" value="<c:out value='${esaActList.esaType3}' />" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="center">
													<input type="checkbox" id="commStatus3<c:out value='${count.index}' />" name="commStatus3" value="<c:out value="${esaActList.commissionStatus3}" />" 
														<c:if test="${esaActList.commissionStatus3 eq 1}">
															checked = "checked" 
														</c:if> onclick="javascript:changeStatus(this, '<c:out value='${count.index}' />', '3');"
													<c:if test="${esaActList.actType eq 'N'}">disabled="disabled"</c:if> <c:if test="${esaActList.esaActActiveStatus3 eq '0'}">disabled="disabled"</c:if> /> 
													<c:if test="${esaActList.commissionStatus3 eq 1}">&nbsp;<span id="statuslabel3<c:out value='${count.index}' />">On</span></c:if>
													<c:if test="${esaActList.commissionStatus3 eq 0}">&nbsp;<span id="statuslabel3<c:out value='${count.index}' />">Off</span></c:if> 
													<input type="hidden" name="statusLabel3" value="0" />
													<input type="hidden" id="commissionStatus3<c:out value='${count.index}' />" name="commissionStatus3" value="<c:out value='${esaActList.actId}' />" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
														<fmt:formatNumber value="${esaActList.commissionRate3}" type="currency" pattern="#####0.0000"/>
														<input type="hidden" name="commRate3" id="commRate3<c:out value='${count.index}' />" value="<fmt:formatNumber value="${esaActList.commissionRate3}" type="currency" pattern="#####0.0000"/>" />
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
														<fmt:formatNumber value="${esaActList.esaCost3}" type="currency" pattern='#,###,##0.00'/>
												</td>
												<td <c:if test="${(count.index % 2)eq '0'}">class='Ntexto'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexte'</c:if> align="right">
														<fmt:formatNumber value="${esaActList.esaRevenue3}" type="currency" pattern='#,###,##0.00'/>
												</td>
												<input type="hidden" name="esaActCost3" value="<fmt:formatNumber value="${esaActList.esaCost3}" type="currency" pattern='#,###,##0.00'/>" /> 
												<input type="hidden" name="esaActRevenue3" value="<fmt:formatNumber value="${esaActList.esaRevenue3}" type="currency" pattern='#,###,##0.00'/>" /> 
												<input type="hidden" name="commissionRate3" value="" />
												<input type="hidden" name="esaActActiveStatus3" value="<c:out value='${esaActList.esaActActiveStatus3}' />" />
											</c:when>
										</c:choose>
										
										<input type="hidden" name="esaId3" value="<c:out value="${esaActList.esaId3}" />" /> 
										<td>&nbsp;</td>	
									</tr>		
								</c:forEach>
								
								<% int buttonRowColspan = 5;
									if(esaListSize > 0) {
										buttonRowColspan = buttonRowColspan+esaListSize*5;
									}
								%>
								<tr class = "Nbuttonrow">
									<td colspan="<%=buttonRowColspan %>" style="padding-left: 30px;;">
										<% if(esaListSize>0 && activityListSize>0) { %>									
												<html:submit  property = "save" styleClass = "button_c" onclick="return validate();">Save</html:submit>
										<%} else { %>
												<html:submit  property = "save" styleClass = "button_c" disabled="true" onclick="return validate();">Save</html:submit>
										<%} %>
			 							<input type="reset" name="reset" class="button_c" value="Cancel" onclick="resetAll();" /> 
			 						<%--	<html:reset property = "reset" styleClass = "button_c" onclick="resetAll();return false;"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>  --%>
			 						</td>
								</tr>
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<c:if test="${ESAActivityCommissionForm.jobRef eq 'inscopejob'}" >	
	<c:if test = "${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
	
	<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/DefaultJobMenuScript.inc" %>
	</c:if>
	
	<c:if test = "${requestScope.jobType eq 'Job' || requestScope.jobType eq 'Ticket' }">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if> 
</c:if>	

</html:form>
</body>
</html>