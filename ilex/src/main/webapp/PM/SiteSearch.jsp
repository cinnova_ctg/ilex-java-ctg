<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html:html>
<% 
	String setvalue = "";
	String selectedradio = "";
	int k=0;
	int size=0;
	String ref1 = "";

	if(request.getAttribute( "size" ).toString()!=null)
	{
		size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
	}

	String Appendix_Id = "";
	
	if( request.getAttribute( "Appendix_Id" ) != null )
		Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" );
	

%>
<HEAD>
<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<title></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<%@ include  file="/NMenu.inc" %>
</head>

<body> 
<html:form action="SiteSearch">
<%@ include  file="/JobMenu.inc" %>
<html:hidden property="ref"/>
<html:hidden property="msa_id"/>
<html:hidden property="jobid"/>
<html:hidden property="appendixid"/>
<html:hidden property = "reftype"/>
<html:hidden property = "lo_ot_id" />
<html:hidden property="siteid"/> 
<html:hidden property ="fromType"/>
<input type="hidden" name="isTicket" value="<c:out value='${param.isTicket }'/>" />

<logic:equal name ="SiteSearchForm" property ="fromType" value ="yes">
<logic:empty name ="SiteSearchForm" property ="siteid">
<div id="menunav">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
    				<tr>
			    	     <%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
						   	<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>  
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
					</tr>
					
					<tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "SiteSearchForm" property = "msa_id"/>&Appendix_Id=<bean:write name = "SiteSearchForm" property = "appendixid"/>"><bean:write name = "SiteSearchForm" property = "msa_name"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "SiteSearchForm" property = "appendixid"/>&Name=<bean:write name = "SiteSearchForm" property = "appendixname"/>&ref=<%=chkaddendum%>"><bean:write name = "SiteSearchForm" property = "appendixname"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
	 					<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;Job&nbsp;<bean:message bundle = "PRM" key = "prm.sitesearch.searchcriteria"/>:&nbsp;<bean:write name ="SiteSearchForm" property ="jobName"/></h1></td>
	 				</tr>
	      		</table>
   				</td>
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</div>
</logic:empty>
</logic:equal>	

<logic:equal name = "SiteSearchForm" property = "fromType" value="yes">
	<logic:notEmpty name ="SiteSearchForm" property ="siteid">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
					
					<tr>
	 					<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;Job&nbsp;<bean:message bundle = "PRM" key = "prm.sitesearch.searchcriteria"/>:&nbsp;<bean:write name ="SiteSearchForm" property ="jobName"/></h1></td>
	 				</tr>
					
		</table>
	</logic:notEmpty>
</logic:equal>

<logic:notEqual name = "SiteSearchForm" property = "fromType" value="yes">

	<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr>	<%-- Added by Pankaj--%>
				          <td background="images/content_head_04.jpg" height="21">
				          <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${SiteSearchForm.msaId }'/>&app=null"   class="bgNone"><c:out value='${SiteSearchForm.msaName }'/></a>
								 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${SiteSearchForm.appendixId }'/>"><c:out value='${SiteSearchForm.appendixName }'/></a>
								 <a><c:out value='${SiteSearchForm.jobName }'/></a>
								 <a><span class="breadCrumb1">Site Search</a></td>
				</tr>
				 <tr>
   						 <td colspan="4" height="30" >
						    <h1 style="padding-left: 6px;">
							    <bean:message bundle = "PRM" key = "prm.sitesearch.searchcriteria"/>:&nbsp;<bean:write name="SiteSearchForm" property="msa_name"/>
						    </h1>
					    </td>
			 	 </tr> 
	</table>
</logic:notEqual>
<table>

<logic:equal name ="SiteSearchForm" property ="fromType" value ="yes">
		<logic:empty name ="SiteSearchForm" property ="siteid">
			<%@ include  file="/JobMenuScript.inc" %>
		</logic:empty>
</logic:equal>                                           

<table  border="0" cellspacing="1" cellpadding="1" >
  <tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  
  <logic:present name = "retVal" scope = "request">
	<logic:equal name = "retVal" value = "0">
	  	<tr>
	    	<td colspan="4"class="message" height="30" >
	    		Site added successfully.
	    	</td>
		</tr>
	 </logic:equal>
	 
	 <logic:equal name = "retVal" value = "-90001">
	  	<tr>
	    	<td colspan="4"class="message" height="30" >
	    		Site already exists, Insert fail.
	    	</td>
		</tr>
	 </logic:equal>
  </logic:present>
  
  <tr> 
    <td  class = "colDark"  ><bean:message bundle="PRM" key="prm.sitesearch.sitename" /></td>
	<td  class = "colLight" ><html:text  styleClass="text" size="20" name="SiteSearchForm" property="site_name" maxlength="50"/></td>
	<td  class = "colDark"  ><bean:message bundle="PRM" key="prm.sitesearch.sitenumber" /></td>
	<td  class = "colLight" ><html:text  styleClass="text" size="20" name="SiteSearchForm" property="site_number" maxlength="50"/></td>
    
    <td class="colDark"  ><bean:message bundle="pm" key="pmt.allsites.sitecitysearch"/></td>
	<td class="colLight" ><html:text name="SiteSearchForm" property="site_city" styleClass="text" size="20"></html:text></td>
	
	<td  class="colDark" ><bean:message bundle="pm" key="pmt.allsites.brand" /></td>
	<td  class="colLight">
		<html:select name="SiteSearchForm" property="site_brand" size="1" styleClass="select">
			 <html:optionsCollection name = "dcGroupList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
		</html:select>
	</td>
	</tr>
	<tr>
	<td  class="colDark"  ><bean:message bundle="pm" key="pmt.allsites.endcustomer" /></td>
	<td  class="colLight">
		<html:select name="SiteSearchForm" property="site_end_customer" size="1" styleClass="select">
			 <html:optionsCollection name = "dcEndCustomerList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
		</html:select>
	</td>
    <td  class="colDark" ><bean:message bundle="pm" key="pmt.allsites.statesearch" /></td>
	<td  class="colLight" colspan="5">
		<logic:present name="initialStateCategory">
			<html:select property = "site_state"  styleClass="select">
				<logic:iterate id="initialStateCategory" name="initialStateCategory" >
					<bean:define id="statecatname" name="initialStateCategory" property="siteCountryIdName" />	
					<%  if(!statecatname.equals("notShow")) { %>	   
					<optgroup class=select label="<%=statecatname%>">
					<% } %>
						<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
						<%  if(!statecatname.equals("notShow")) { %>
					</optgroup> 
					<% } %>
				</logic:iterate>
			</html:select>
		</logic:present>	
	</td>
 </tr>
  
  <tr> 
	<td  class="colLight" colspan="8" align="center">
		<html:submit property="search" styleClass="button_c" onclick="return searchpartner();">
			<bean:message bundle="PRM" key="prm.sitesearch.search" />
		</html:submit>
		<html:reset property="reset" styleClass="button_c">
			<bean:message bundle="PRM" key="prm.sitesearch.reset" />
		</html:reset>
		<logic:equal name ="SiteSearchForm" property ="siteid" value ="">
			<html:button property ="add" styleClass="button_c" onclick ="return addnewsite();">Add New Site</html:button>
		</logic:equal>
	</td>
  </tr>
  
  </table>
 </td>
 </tr>
</table>

<table  border="0" cellspacing="0" cellpadding="0"  style="padding-left: 4px;">
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1"> 

<logic:present name="sitesearchlist" scope="request">
 <tr>
    <td  width="100%" colspan="7"><h1><bean:message bundle="PRM" key="prm.sitesearch.basicsearchresults" /></h1></td>
  </tr>
 
  <tr> 
    <td  class="Ntryb" ></td>
    <td class="Ntryb"  ><bean:message bundle="PRM" key="prm.sitesearch.sitename" /></td>
    <td class="Ntryb"  ><bean:message bundle="PRM" key="prm.sitesearch.sitenumber" /></td>
    <td class="Ntryb" ><bean:message bundle="PRM" key="prm.sitesearch.address" /></td>
	<td class="Ntryb" ><bean:message bundle="PRM" key="prm.sitesearch.city" /></td>
    <td class="Ntryb" ><bean:message bundle="PRM" key="prm.sitesearch.state" /></td>
	<td class="Ntryb" ><bean:message bundle="PRM" key="prm.sitesearch.zipcode" /></td>
    <td class="Ntryb" ><bean:message bundle="pm" key="pmt.allsites.brand" /></td>
    <td class="Ntryb" ><bean:message bundle="pm" key="pmt.allsites.endcustomer" /></td>
  </tr>
  
  <%int i=1;
  	int j = 0;
	int selectedradiobutton = 0;
	String bgcolor=""; %>
	
	<logic:iterate id="sslist" name="sitesearchlist">
		<bean:define id="slid" name="sslist" property="sitelistid" />
		<bean:define id="sname" name="sslist" property="sitenumber" type = "java.lang.String"/>
 	<%
 	String siteNameVal = sname;
 	if((i%2)==0) 		
		{
			bgcolor="Ntexto"; 
		
		} 
	  else
		{
			bgcolor="Ntexte";
		}

		
		if( size == 1 ) 
		{ 
			setvalue = "javascript:assignsite( '',"+slid+",'"+siteNameVal.replace("'", "\\'")+"' );";
		}
		else
		{
			setvalue = "javascript:assignsite( '"+j+"',"+slid+",'"+siteNameVal.replace("'","\\'")+"');";
		}%>
		
		<html:hidden name = "sslist" property = "sitelistid" />
		<html:hidden name = "sslist" property = "sitelocalityfactor" />
		<html:hidden name = "sslist" property = "unionsite" />
		<html:hidden name = "sslist" property = "sitedesignator" />
		<html:hidden name = "sslist" property = "siteworklocation" />
		<html:hidden name = "sslist" property = "sitecountry" />
		<html:hidden name = "sslist" property = "sitesecphone" />
		<html:hidden name = "sslist" property = "sitephone" />
		<html:hidden name = "sslist" property = "sitedirection" />
		<html:hidden name = "sslist" property = "sitepripoc" />
		<html:hidden name = "sslist" property = "sitesecpoc" />
		<html:hidden name = "sslist" property = "sitepriemail" />
		<html:hidden name = "sslist" property = "sitesecemail" />
		<html:hidden name = "sslist" property = "sitenotes" />
		<html:hidden name = "sslist" property = "installerpoc" />
		<html:hidden name = "sslist" property = "sitepoc" />
		<html:hidden name = "sslist" property ="sitelatdeg"/>
		<html:hidden name = "sslist" property ="sitelatmin"/>
		<html:hidden name = "sslist" property ="sitelatdirection"/>
		<html:hidden name = "sslist" property ="sitelondeg"/>
		<html:hidden name = "sslist" property ="sitelonmin"/>
		<html:hidden name = "sslist" property ="sitelondirection"/>	
		<html:hidden name = "sslist" property ="sitestatus"/>	
		<html:hidden name = "sslist" property ="sitecategory"/>	
		<html:hidden name = "sslist" property ="siteregion"/>	
		<html:hidden name = "sslist" property ="sitetimezone"/>	
	
<logic:present name = "retVal" scope = "request">	
	<logic:notEqual name = "latestSiteId" value = "0" >
	<% if ( j == 0)  { %>
 		<tr><td></td>
 		<td colspan="6"><h1>New Site</h1></td></tr>
 	<% } if ( j == 1)  { %>
 		<tr>
 		<td></td>
 		<td colspan="6"><h1>Search Result</h1></td></tr>
 	<% } %>
	</logic:notEqual>	
</logic:present>	
	
	  <tr>
	    <td  class="<%=bgcolor %>"  height="20"><html:radio   property = "selectedsite"  value = "<%= ( String ) slid %>" onclick = "<%= setvalue %>"/></td>  
			
			
			
	  <!--   <td  class="<%=bgcolor %>" height="20"><%=i %></td>  -->
		
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitename"/>
			 <html:hidden name = "sslist" property = "sitename" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitenumber"/>
			 <html:hidden name = "sslist" property = "sitenumber" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="siteaddress"/>
			 <html:hidden name = "sslist" property = "siteaddress" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitecity"/>
			 <html:hidden name = "sslist" property = "sitecity" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitestate"/>
			 <html:hidden name = "sslist" property = "sitestatedesc" />
			 <html:hidden name = "sslist" property = "sitestate" />
		 </td>
		 <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitezipcode"/>
			 <html:hidden name = "sslist" property = "sitezipcode" /> 
		 </td>
		  <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="sitebrand"/>
			  <html:hidden name = "sslist" property = "sitebrand" /> 
		 </td>
		  <td  class="<%=bgcolor %>" height="20">
			 <bean:write name="sslist" property="siteendcustomer"/>
			 <html:hidden name = "sslist" property = "siteendcustomer" /> 
		 </td>
		
		
	  </tr> 
	  
	    <%i++;j++; %>
	</logic:iterate>
	
	<%if(i==1)
	{ %>
		<tr>
			<td>&nbsp;</td>
			<td  class="message" height="30" colspan="7"><bean:message bundle="PRM" key="prm.sitesearch.norecord" /></td>
		</tr>
	<%} %>

</logic:present>  

  </table>
 </td>
 </tr>
</table> 
</html:form>
</table>
</BODY>

<script>
function searchpartner()
{
	document.forms[0].ref.value = "search";
	return true;	
}



function unassignpartner()
{
	document.forms[0].ref.value = "unassign";
	document.forms[0].submit();
	return true;	
}

function assignsite(temp,site_list_id, sitenumber)
{	
	var siteId = document.forms[0].siteid.value;		
	var address = '';	
	document.forms[0].target = "ilexmain";
	var url = "JobDashboardAction.do?function=view&add=add&type=9&isClicked=7&Job_Id="+document.forms[0].jobid.value+"&sitelistid="+site_list_id+"&siteId="+siteId; 
	var isTicket = document.forms[0].isTicket.value; 
	if(isTicket == 'y'){
		window.opener.document.all.site_id.value = siteId;
		window.opener.document.all.sitelistid.value =	site_list_id;
		
		window.opener.document.all.site.value = sitenumber;
		window.close();
		return true;
	}else{
            window.opener.location.href = url;
		window.close();
		return true;
	}	
}

function addnewsite()
{

document.forms[0].action="SiteManage.do?method=Add&appendixid="+document.forms[0].appendixid.value+"&msaid="+document.forms[0].msa_id.value+"&jobid="+document.forms[0].jobid.value+"&fromType="+document.forms[0].fromType.value;
document.forms[0].submit();
return true;
}
function breadCrum(stage)
{
document.forms[0].target = "ilexmain";
	if(stage=='1')
		document.forms[0].action = "MSAUpdateAction.do?firstCall=true";
	if(stage=='2')
		document.forms[0].action = "MSADetailAction.do?MSA_Id=<bean:write name = "SiteSearchForm" property = "msa_id"/>";
	if(stage=='3')
		document.forms[0].action = "AppendixDetailAction.do?MSA_Id=<bean:write name = "SiteSearchForm" property = "msa_id"/>&Appendix_Id =<bean:write name ="SiteSearchForm" property ="appendixid"/>";	
document.forms[0].submit();
window.close();
return true;
}
</script>
<script>
function ShowDiv()
{

if(document.forms[0].fromType.value =='yes'&& document.forms[0].siteid.value=='')
{
	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";
}
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
if(document.forms[0].fromType.value =='yes'&& document.forms[0].siteid.value=='')
{

		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;

}
}
</script>
</html:html>
