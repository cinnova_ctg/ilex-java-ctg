<!DOCTYPE HTML>
<%@ page import = "com.mind.common.*" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<bean:define id = "menustring" name = "menustring" scope = "request"/>
<bean:define id = "currState" name = "currState" scope = "request"/>
<bean:define id = "expansionState" name = "expansionState" scope = "request"/>

<bean:define id = "element" name = "element" scope = "request"/>
<bean:define id = "controller" name = "controller" scope = "request"/>
<html:html>


<head>
<%@ include file = "../Header.inc" %>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
<title>Hierarchy Navigation</title>
<script type="text/javascript">
var tabIds = new Array(
			"A-D",
			"E-J",
			"K-O",
			"P-S",
			"T-Z",
			"Options");
function fun_HideShowBlock(index,isLeftTreeRefresh, tabName, isMainFrameRefreshed)
{
	if(tabName == '') {
		//replace the tabId with index
		tabId=index;
		for(i=0;i<tabIds.length;i++) {
			if((tabIds[i])==tabId) {
				index = i;
				break;
			}
		}
		tabName = tabIds[index];
	}
	if(tabName != 'Options') {
		tabRefresh(index,isLeftTreeRefresh, isMainFrameRefreshed);
	} else {
		document.getElementById("options").style.display = "block";
		document.getElementById("lefttree").style.display = "none";
		buttonPressed(index);
	}
}

function tabRefresh(index, isLeftTreeRefresh, isMainFrameRefreshed)
{
	var selectedTab = '';
	var selectedIndex = 0;

	if(index != -1) {
		selectedIndex = index;
		selectedTab = tabIds[index];
	} else {
		selectedTab = $('td.topTabSelected').html();
		for(i=0;i<tabIds.length;i++) {
			if((tabIds[i])==selectedTab) {
				selectedIndex = i;
				break;
			}
		}
	}
	
	if(isLeftTreeRefresh != 'N') {
			var url;
			document.forms[0].filter.value=selectedTab;
			document.forms[0].isClicked.value=selectedIndex; 
			document.forms[0].isRefreshed.value=isMainFrameRefreshed; 
			url = "MenuTreeAction.do?menucriteria=PM"; //&filter="+tabId;
			
			document.forms[0].action=url;
			document.forms[0].submit();
			return true;
		}
		else {
			buttonPressed(index);
			if(isMainFrameRefreshed == 'Y') {
				updateMainFrame(selectedTab,isMainFrameRefreshed);
			}
		}
	
}
function updateMainFrame(tabName, isMainFrameRefreshed) {
		if(tabName == 0) {
		} else {
			document.forms[0].filter.value = tabName;
		}
		parent.ilexmain.location="MSAUpdateAction.do?filter="+tabName;
		return true;
}


function buttonPressed(idx)
{
	for(var i=0;i<document.all.TOP_BUTTON.length/3;i++) {
		document.all.TOP_BUTTON[3*i+1].width='150';
		document.all.TOP_BUTTON[3*i+1].width=document.all.TOP_BUTTON[3*i+1].innerHTML.length*8;
		document.all.TOP_TAB[i].style.width='5';
		
		if(i==idx)
		{
			document.all.TOP_BUTTON[3*i].innerHTML='<img src="images/left-cor-light.gif" width="4" height="18" border="0">';
			document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="images/right-cor-light.gif" width="4" height="18"  border="0">';
			document.all.TOP_BUTTON[3*i+1].className = 'topTabSelected';
			
		}
		else//Restore Remaining
		{
				document.all.TOP_BUTTON[3*i].innerHTML='<img src="images/left-cor.gif" width="4" height="18"  border="0">';
				document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="images/right-cor.gif" width="4" height="18"  border="0">';
				document.all.TOP_BUTTON[3*i+1].className = 'topTab';
			//}
		}
	}
}
</script>
</head>
<body  onload = "MM_preloadImages('images/activitymanager_small.gif','images/rm_materials.jpg','images/t_msa.gif','images/rm_mfg.gif','images/t_activity.gif'); fun_HideShowBlock(<%=request.getAttribute("isClicked") %>,'N','','<%=request.getAttribute("isRefreshed") %>')"  text = "#000000" leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" bgcolor="#F4F9FD">
<form method="get">
<input type='hidden' name='menucriteria' value='PM' />
<input type='hidden' name='filter' value='' />
<input type='hidden' name='isClicked' value='0' /> 
<input type='hidden' name='isRefreshed' value='N' />  
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding-top: 6px;">
<table id ="topTable" border="0" cellspacing="0" cellpadding="0">
<tr>
						<% 
				 		
					 		String[][] tabNames = {
						 			{"A-D","A-D"},
						 			{"E-J","E-J"},
						 			{"K-O","K-O"},
						 			{"P-S","P-S"},
						 			{"T-Z","T-Z"},
						 			{"Options","Options"}
					 			};
						%>
						<div class="topTabDiv">
						<% for(int tabIndex=0;tabIndex<tabNames.length;tabIndex++) {%>
					      <td id="TOP_TAB" class='leftRightBorderMenu'>
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td id="TOP_BUTTON" style="cursor:hand;"
											onClick="fun_HideShowBlock('<%=tabIndex%>','Y','<%=tabNames[tabIndex][1]%>','Y');" align="center"
											nowrap ><img src="images/left-cor.gif" width="5" height="18" ></td>
										<td id="TOP_BUTTON" class="topTab"
											onClick="fun_HideShowBlock('<%=tabIndex%>','Y','<%=tabNames[tabIndex][1]%>','Y');" align="center"  
											nowrap><%=tabNames[tabIndex][0]%></td>
										<td id="TOP_BUTTON" style="cursor:hand;" 
											onClick="fun_HideShowBlock('<%=tabIndex%>','Y','<%=tabNames[tabIndex][1]%>','Y');" align="center" 
											nowrap><img src="images/right-cor.gif" width="5" height="18"></td>
									</tr>
								</table>
							</td>
					<%
				 	}
					%>
					</div>
</tr>
</table>
</td>
</tr>
<tr>
		<td>
		<table id="lefttree" border="0" cellspacing="0" cellpadding="0" style="display: block;">
		<tr>
			<td  nowrap="nowrap" style="padding-left: 3px; padding-top: 3px;">
				<jsp:include page="LeftContent.jsp"/>
			</td>
		</tr>
		</table>
		</td>
</tr>
<tr>
	<td style="padding-left: 5px; padding-top: 5px;">
		<table id= "options" border="0" cellspacing="0" cellpadding="0" style="display: none;">
			<tr>
				<td><font size="1px;" color="BLACK" style="font-weight: normal;">There are no Options defined at this time.</font></td>
			</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>

</html:html>