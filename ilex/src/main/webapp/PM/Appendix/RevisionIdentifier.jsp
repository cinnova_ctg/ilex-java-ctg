<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%
String MSA_Id = "";
int allCommentSize = 0;

int addRow = -1;
boolean addSpace = false;
int rowHeight = 65;	
int ownerListSize = 0;
if( request.getAttribute( "MSA_Id" ) != null)
	MSA_Id = (String) request.getAttribute( "MSA_Id" );

if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());

rowHeight += ownerListSize*18;

%>

<html:html>
<head>
	<title>REVISION IDENTIFIER</title>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css"> 
	
</head>


<%@ include file = "/NMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>

<body onLoad = "leftAdjLayers();OnLoad();">
<html:form action = "/AppendixRevisionIdentifierAction">
<html:hidden property="id"/>
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<html:hidden property="msaId"/> 
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property = "ref" />


<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        <tr>
			         <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>	
					<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			        
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="AppendixRevisionIdentifierForm" property ="msaId"/>&firstCall=true"><bean:write name = "AppendixRevisionIdentifierForm" property = "msaName"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7">
	 						<h2>Appendix&nbsp;<bean:message bundle = "pm" key = "appendix.revisionidentifier.header"/>:&nbsp;<bean:write name = "AppendixRevisionIdentifierForm" property = "name" /></h2>
			 		   </td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="AppendixRevisionIdentifierForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="AppendixRevisionIdentifierForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<% 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "checking();"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
<%@ include  file="/AppendixMenuScript.inc" %>
							<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
<table width = "100%" border = "0" cellspacing="1" cellpadding = "1" align = "center" valign  = "top">
	  
	  	 <tr>
  			<td width = "2" height="0"></td>
  			 <td>
  			 <table border = "0" cellspacing = "1" cellpadding = "1" width = ""> 
 		  <tr>
 		  	<td class = "Nlabelobold"><bean:message bundle = "pm" key = "appendix.revisionidentifier.majorrevision"/></td> 
 		  	<td class = "Nlabelo">
				<html:radio property = "revisionidentifier" value = "1" />
			</td>
		  </tr>
			
		  <tr>
 		  	<td class = "Nlabelobold"><bean:message bundle = "pm" key = "appendix.revisionidentifier.schedulerevision"/></td> 
 		  	<td class = "Nlabelo">
				<html:radio property = "revisionidentifier" value = "2" />
			</td>
		  </tr>
		  
		  <tr>
 		  	<td class = "Nlabelobold"><bean:message bundle = "pm" key = "appendix.revisionidentifier.administrativerevision"/></td> 
 		  	<td class = "Nlabelo">
				<html:radio property = "revisionidentifier" value = "3" />
			</td>
 		  <tr>
 		  	<td colspan = "2" class = "colLight"><html:submit property = "save" styleClass = "button_c" onclick = "return validate();"><bean:message bundle = "pm" key = "appendix.revisionidentifier.save"/></html:submit>
 		  </tr>
 		 </table>
			</td>
		</tr>
	</table>
</table>
</html:form>
</body>

<script>
function validate()
{
	if( document.forms[0].revisionidentifier.value == "" )
	{
		alert( "<bean:message bundle = "pm" key = "appendix.revisionidentifier.select"/>" );
		
	}
	return true;
}
function initialState()
{
	document.forms[0].action ="AppendixRevisionIdentifierAction.do?Appendix_Id=<bean:write name ='AppendixRevisionIdentifierForm' property ='id'/>&firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}

function month_Appendix()
{
document.forms[0].action="AppendixRevisionIdentifierAction.do?Appendix_Id=<bean:write name ='AppendixRevisionIdentifierForm' property ='id'/>&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
document.forms[0].action="AppendixRevisionIdentifierAction.do?Appendix_Id=<bean:write name ='AppendixRevisionIdentifierForm' property ='id'/>&week=0&clickShow=true";
document.forms[0].submit();
return true;
}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}
function checking()
{

	for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
	{
			if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
			{
				document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="AppendixRevisionIdentifierAction.do?Appendix_Id=<bean:write name ='AppendixRevisionIdentifierForm' property ='id'/>&opendiv=0";
 				document.forms[0].submit();

			}
	}
}

function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
	document.getElementById("filter").style.visibility="visible";
<%}%>
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
		
}

function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AppendixRevisionIdentifierAction.do?Appendix_Id=<bean:write name ='AppendixRevisionIdentifierForm' property ='id'/>&clickShow=true";
	document.forms[0].submit();
	return true;
}

</script>

</html:html>