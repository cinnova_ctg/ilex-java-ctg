<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
 
<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<html:html>
<head>
<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<title></title>

<%@ include  file="/Menu.inc" %>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td ><img src="images/spacer.gif" width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table  border="0" cellspacing="1" cellpadding="0" height="18">
        <tr> 
      </table>
    </td>
  </tr>
 </table>
 
 <%int i=0; String label=""; %>
 <br>
 <html:form action="InworkState"> 
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="1" height="0"></td>
      <td colspan="9">
      <table table border = "0" cellspacing = "1" cellpadding = "1">
		
				<logic:equal name = "inworkflag" value = "0" scope = "request">
					<script>
						parent.ilexleft.location.reload();
					</script>
				</logic:equal>
		
		<tr><td>&nbsp; </td>
			<td class = "labeleboldwhite" colspan ="3" height ="30"><bean:message bundle="pm" key="pm.inwork.changestatusinwork"/></td>
			<td class = "labeleboldwhite" colspan ="5" height ="30">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
	  	<tr align="center">
		  	<td colspan="1" class="labeleboldwhite">&nbsp;</td>
	  		<td colspan="4" class="tryb"><bean:message bundle="pm" key="pm.inwork.head1"/></td>
  			<td colspan="4" class="tryb"><bean:message bundle="pm" key="pm.inwork.head2"/></td>
		</tr>
    
      	<logic:present name="inworkrows" scope="request">
      	<logic:iterate id="list" name="inworkrows">
      	<% if ((i++%2)==0)
	      		label="labelo";
  		else
	  			label="labele";
	  	%>
	  	<tr>
		  	<td colspan="1" class="labeleboldwhite"><html:multibox property ="check"><bean:write name ="list" property ="lx_pr_id"/></html:multibox></td>
	  		<td colspan="4" class="<%=label%>" align="center"><bean:write  name="list" property="lo_ot_name"/></td>
	  		<td colspan="4" class="<%=label%>" align="center"><bean:write  name="list" property="lx_pr_title"/></td>
		</tr>
		</logic:iterate>
		</logic:present>

		 <tr>
		 <td colspan="1"></td>
		    <td colspan="8" class="buttonrow">
		      <html:submit property="inwork" styleClass="button"><bean:message bundle="pm" key="pm.inwork.inwork"/></html:submit>
		      <html:reset property="reset" styleClass="button"> <bean:message bundle="pm" key="pm.inwork.reset"/></html:reset>
		    </td>
		</tr>
		
		 
	  </table>
		</td>
	</tr>
</table>
</html:form>
</body>
</html:html>