<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<%
String MSA_Id = "";
String checkowner=null;
int allCommentSize = 0;
String status="";
int addRow = -1;
boolean addSpace = false;
int rowHeight = 65;	
int ownerListSize = 0;
if( request.getAttribute( "MSA_Id" ) != null)
	MSA_Id = (String) request.getAttribute( "MSA_Id" );

if(request.getAttribute("allCommentSize") != null)
	allCommentSize = Integer.parseInt(request.getAttribute("allCommentSize")+"");

if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());

rowHeight += ownerListSize*18;

String msastatus="";

/* String status =""; */



String Appendix_Id ="";
String appendixStatus ="";
String latestaddendumid ="";
String appendixType = "";

if(request.getAttribute("Appendix_Id")!=null)
	Appendix_Id= request.getAttribute("Appendix_Id").toString();
	
if(request.getAttribute("appendixStatus")!=null)
	appendixStatus= request.getAttribute("appendixStatus").toString();
	
if(request.getAttribute("latestaddendumid")!=null)
	latestaddendumid= request.getAttribute("latestaddendumid").toString();	
	
if( request.getAttribute( "appendixType" ) != null)
	appendixType = (String) request.getAttribute( "appendixType" );


%>
<bean:define id = "codes" name = "codeslist" scope = "request"/>
<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<link rel = "stylesheet" href="styles/content.css" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<title>Appendix Edit Page</title>
<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
</style>

<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>

<%@ include  file="/NMenu.inc" %>

<link rel="stylesheet" href="styles/style.css" type="text/css">
</head>
<%-- <%@ include  file="/AppendixMenu.inc" %> --%>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF"  onLoad = "leftAdjLayers();OnLoad();disableInableTravelAuthorize();">
<html:form action="/AppendixEditAction">
<html:hidden property="msaId" />
<html:hidden property="appendixId" />
<html:hidden property="actionUpdateEdit" />
<html:hidden property="fromType"/>
<html:hidden property="msaName"/>
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
        <tr>
          <logic:equal name="AppendixEditForm" property="appendixId" value="0">
        
          	<td class = "Ntoprow1" width="450">&nbsp;</td>
          </logic:equal>
          <logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
         			<%-- <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>	
					<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>  --%>
			<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>	
				<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>	 
				</logic:notEqual>
        </tr>
         <tr>
         		<logic:equal name="AppendixEditForm" property="appendixId" value="0">
					<td background="images/content_head_04.jpg" height="21" width ="100%">
          				<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "AppendixEditForm" property = "msaId"/>"><bean:write name = "AppendixEditForm" property = "msaName"/></a></div>
       			    </td>
         		</logic:equal>
         		<logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
		          <td background="images/content_head_04.jpg" height="21" colspan="7">
          				<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "AppendixEditForm" property = "msaId"/>"><bean:write name = "AppendixEditForm" property = "msaName"/></a></div>
       			  </td>
       			 </logic:notEqual>
        </tr>
   		  <tr>
					<td colspan = "6" width="500"><h2>
					<logic:equal name="AppendixEditForm" property="appendixId" value="0">
						Add Appendix
					</logic:equal>
					<logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
						&nbsp;<bean:message bundle = "pm" key ="appendix.edit.new.label"/>&nbsp;<bean:write name = "AppendixEditForm" property = "appendixName"/>
					</logic:notEqual></h2>
					</td>
	     </tr>
      </table></td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
    <td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" conspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                      <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
	                			<div id="featured">
										<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
								</div>								
	                </div>		
				<span>
				<div id="filterAppendix" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
							    		<logic:present name ="statuslist" scope="request">
									    	<logic:iterate id = "list" name = "statuslist"> 
										 		<html:multibox property = "appendixSelectedStatus"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											</logic:iterate>
										</logic:present>
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="AppendixEditForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="AppendixEditForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="AppendixEditForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
	    
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist"> 
										<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
										<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<%
										checkowner = "javascript: checking('"+ownerId+"');"; 
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 							    	
							    	
								 		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
									  <bean:write name ="olist" property = "ownerName"/>
									  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                         
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
                              
                            </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
</table>

<logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
	<%@ include  file="/AppendixMenuScript.inc" %>
</logic:notEqual>


  							<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>	
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
 <tr>
  <td width="2" height="0">&nbsp;</td>
  <td>
  	<table border="0" cellspacing="1" cellpadding="1" width="550">
	  <tr> 
		<td colspan = "3" class = "formCaption" height = "25"><bean:message bundle = "pm" key = "msa.detail.general"/></td>
	  </tr> 
	  <tr> 
	    <td   class="colDark">Name<font class="red"> *</td>
	    <td  colspan = "2" class="colLight">
		    <logic:equal name="AppendixEditForm" property="appendixId" value="0">
				<html:text size = "70" maxlength = "100" property = "appendixName" styleClass = "text" />
			</logic:equal>
			<logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
				<bean:write name="AppendixEditForm" property="appendixName" />
				<html:hidden property="appendixName" />
			</logic:notEqual>
		</td>
	  </tr>

	  <tr> 
	    <td   class="colDark">Appendix Type</td>
	    <td   colspan = "2" class="colLight">
		 <html:select property = "appendixType" size = "1" styleClass = "select" onchange = "javascript: disableInableTravelAuthorizeoncheck(this);">
			<html:optionsCollection name = "codes" property = "appendixtypecode" value = "value" label = "label" /> 
		 </html:select>
		</td>
	 </tr>
	 
	 
	  <tr>
	  	<td class="coldark">MSP </td>
		<td colspan ="2" class ="colLight">
			<html:radio name="AppendixEditForm" property="msp" value="Y"><font class="radioText" >Yes</font></html:radio>&nbsp;&nbsp;&nbsp;
			<html:radio name="AppendixEditForm" property= "msp" value="N" ><font class="radioText">No</font></html:radio>
		</td>	  
	  </tr>
	<tr> 
	    <td   class="colDark">Appendix Type Breakout<font class="red"> *</td>
	    <td   colspan = "2" class="colLight">
			 <html:select property = "appendixTypeBreakout" size = "1" styleClass = "select">
			   		<html:optionsCollection name = "codes" property = "appendixTypeBreakoutList" value = "value" label = "label"/> 
			 </html:select>
		</td>
	  </tr>

	  <tr> 
	    <td   class="colDark">CNS POC</td>
	    <td   colspan = "2" class="colLight">
		<html:select property = "cnsPOC" size = "1" styleClass = "select">
			<html:optionsCollection name = "codes" property = "salesPOCcode" value = "value" label = "label"/> 
		 </html:select>		</td>
	  </tr>
	  
	  <tr> 
	    <td   class="colDark">Customer Division<font class="red"> *</td>
	    <td   colspan = "2" class="colLight">
			 <html:select property = "customerDivision" size = "1" styleClass = "select">
			   		<html:optionsCollection name = "codes" property = "customerdivision" value = "value" label = "label"/> 
			 </html:select>
		</td>
	  </tr>
	  
	  <tr> 
	    <td   class="colDark">Number Sites</td>
	    <td   colspan = "2" class="colLight">
			<html:text size = "4" maxlength = "5" property = "siteNo" styleClass = "text" />
		</td>
	  </tr>
	  <!-- Added Travel Authorized for(Intransit Resource) Start-->
	  <tr>
	  	<td   class="colDark">Travel Authorized</td>
	  	<td colspan = "2" class="colLight">
	  		<html:radio name="AppendixEditForm" property="travelAuthorized" value="Y"><font class="radioText" >Yes</font></html:radio>&nbsp;&nbsp;
	  		<html:radio name="AppendixEditForm" property="travelAuthorized" value="N"><font class="radioText" >No</font></html:radio>
	  	</td>
	  </tr>
	  	  <tr>
	  	<td class="coldark">Test Appendix</td>
		<td colspan ="2" class ="colLight">
			<html:radio name="AppendixEditForm" property="testAppendix" value="1"><font class="radioText" >Yes</font></html:radio>&nbsp;&nbsp;&nbsp;
			<html:radio name="AppendixEditForm" property= "testAppendix" value="0" ><font class="radioText">No</font></html:radio>
		</td>	  
	  </tr>
	  
	  <!-- Added Travel Authorized for(Intransit Resource) End-->
	  <tr> 
		<td colspan = "3" class = "formCaption" >Uplift Factors</td>
	  </tr> 
	  	  
	  <tr> 
	    <td   class="colDark">Union</td>
	    <td   colspan = "2" class="colLight">
	    	 <html:text styleClass="text" size="5" maxlength="5" property="unionUpliftFactor"/>
		</td>
	  </tr>
	  
	  <tr> 
	    <td   class="colDark">Expedite 24</td>
	    <td   colspan = "2" class="colLight">
	    	 <html:text styleClass="text" size="5" maxlength="5" property="expedite24UpliftFactor"/>
		</td>
	  </tr>
	  
	  <tr> 
	    <td   class="colDark">Expedite 48</td>
	    <td   colspan = "2" class="colLight">
	    	 <html:text styleClass="text" size="5" maxlength="5" property="expedite48UpliftFactor"/>
		</td>
	  </tr>
	  
	  <tr> 
	    <td   class="colDark">After Hours</td>
	    <td   colspan = "2" class="colLight">
	    	 <html:text styleClass="text" size="5" maxlength="5" property="afterhoursUpliftFactor"/>
		</td>
	  </tr>
	  
	  <tr> 
	    <td   class="colDark">Weekend/Holiday</td>
	    <td   colspan = "2" class="colLight">
	    	 <html:text styleClass="text" size="5" maxlength="5" property="whUpliftFactor"/>
		</td>
	  </tr>

	  <tr> 
		<td colspan = "3" class = "formCaption" >Cost-Price</td>
	  </tr> 
	   
	  <tr> 
	    <td   class="colDark">Estimated Cost($)</td>
	    <td   colspan = "2" class="colLight">
			<bean:write name="AppendixEditForm" property="estimatedCost" />
		</td>
	  </tr>
	  
	  <tr>
	    <td   class="colDark">Extended Price($)</td>
	    <td   colspan = "2" class="colLight">
			<bean:write name="AppendixEditForm" property="extendedPrice" />
		</td>
	  </tr>
	  
	  <tr>
	    <td   class="colDark">Pro Forma Margin</td>
	    <td   colspan = "2" class="colLight">
			<bean:write name="AppendixEditForm" property="proformaMargin" />
		</td>
	  </tr>

      <tr> 
		<td colspan = "3" class = "formCaption" >Schedule Information</td>
	  </tr> 
	   
	  <tr> 
	    <td   class="colDark">Required Planned Schedule</td>
	    <td   colspan = "2" class="colLight">
		    <html:text  styleClass = "text" size = "10" property = "reqPlannedSch" readonly = "true"/>
	   		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].reqPlannedSch , document.forms[0].reqPlannedSch , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
		</td>
	  </tr>

	  <tr> 
	    <td   class="colDark">Effective Planned Schedule</td>
		<td   colspan = "2" class="colLight">
		    <html:text  styleClass = "text" size = "10" property = "effPlannedSch" readonly = "true"/>
	   		<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].effPlannedSch , document.forms[0].effPlannedSch , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
		</td>
     </tr>
     
      <tr> 
	    <td   class="colDark">Days Valid For</td>
		<td   colspan = "2" class="colLight">
		    <html:text styleClass="text" size="5" maxlength="10" property="schDays"/>
		</td>
     </tr>

	  <tr> 
		<td colspan = "3" class = "formCaption" >Customer POC</td>
	  </tr> 
	   
	  <tr> 
	    <td   class="colDark">Business</td>
	    <td   colspan = "2" class="colLight">
			<logic:equal name="AppendixEditForm" property="appendixId" value="0">
				Update Only
				<html:hidden property="custPocBusiness" />
			</logic:equal>
			<logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
				<html:select property = "custPocBusiness" styleClass = "select">
					<html:optionsCollection name = "codes" property = "custPOCbusinesscode" value = "value" label = "label"/> 
				</html:select>	
			</logic:notEqual>	
		</td>
	  </tr>

	  <tr>  
	    <td   class="colDark">Contracts</td>
		<td   colspan = "2" class="colLight">
			<logic:equal name="AppendixEditForm" property="appendixId" value="0">
				Update Only
				<html:hidden property="custPocContract" />
			</logic:equal>
			<logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
			    <html:select property = "custPocContract" styleClass = "select">
					<html:optionsCollection name = "codes" property = "custPOCcontractcode" value = "value" label = "label"/> 
				</html:select>
			</logic:notEqual>
		</td>
     </tr>
     
     <tr>  
	    <td   class="colDark">Billing</td>
		<td   colspan = "2" class="colLight">
			<logic:equal name="AppendixEditForm" property="appendixId" value="0">
				Update Only
				<html:hidden property="custPocBilling" />
			</logic:equal>
			<logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
			    <html:select property = "custPocBilling" styleClass = "select">
					<html:optionsCollection name = "codes" property = "custPOCbillingcode" value = "value" label = "label"/> 
				</html:select>
			</logic:notEqual>
		</td>
     </tr>
     
      <tr> 
		<td colspan = "3" class = "formCaption" >Reviews</td>
	  </tr> 
	   
	  <tr> 
	    <td   class="colDark">Draft</td>
	    <td   colspan = "2" class="colLight">
			<html:radio property="draftReviews" value="S"><font class="radioText">Standard</font></html:radio>&nbsp;&nbsp;&nbsp;
			<html:radio property="draftReviews" value="E"><font class="radioText">Expedite</font></html:radio>		
		</td>
	  </tr>

<!-- comment the bussinesreview
	  <tr> 
	    <td   class="colDark">Business</td>
		<td   colspan = "2" class="colLight">
		    <html:radio property="businessReviews" value="S"><font class="radioText">Standard</font></html:radio>&nbsp;&nbsp;&nbsp;
			<html:radio property="businessReviews" value="E"><font class="radioText">Expedite</font></html:radio>
		</td>
     </tr>
 -->
     
     <tr> 
	    <td   class="colDark">Status</td>
		  <td   colspan = "2" class="colLight">
		    <logic:equal name="AppendixEditForm" property="appendixId" value="0">
				<bean:message bundle = "pm" key = "appendix.tabular.draft"/>
			</logic:equal>
			<logic:notEqual name="AppendixEditForm" property="appendixId" value="0">
				<bean:write name="AppendixEditForm" property="status" />
			</logic:notEqual>
		</td>
	  </tr>
     
      <tr> 
		<td colspan = "3" class = "formCaption" >Comments</td>
	  </tr>
	  
	  <%
	  boolean csschooser = true;
	  String cssclass = "";
	  String commentbyclass = "";
	  if(allCommentSize > 0) {
   	  %>	
			
		<tr>
			<td class = "formCaption"><bean:message bundle = "pm" key = "common.viewcomment.commentdate"/></td>
			<td class = "formCaption"><bean:message bundle = "pm" key = "common.viewcomment.commentby"/></td>
			<td class = "formCaption"><bean:message bundle = "pm" key = "common.viewcomment.comments"/></td>
		</tr> 
			
			<logic:iterate id = "comment" name = "allComment" scope = "request">
			  
			<%		
				if( csschooser == true )
					{
						cssclass = "Nlabelotop";
						commentbyclass = "Nlabelotopnowrap";
						csschooser = false;
					}
					else
					{
						cssclass = "Nlabeletop";
						commentbyclass = "Nlabeletopnowrap";
						csschooser = true; 
					}
			%>
			
			<tr>
				<td  class = "<%= cssclass %>"><bean:write name = "comment" property = "commentdate"/></td>	
				<td  class = "<%= commentbyclass %>"><bean:write name = "comment" property = "commentby"/></td>				  
				<td  class = "<%= cssclass %>"><bean:write name = "comment" property = "comment"/></td>
	 		</tr>
	 	</logic:iterate>
	  <% }
	  else {
	  %> 	
	  		 <tr> 
			    <td  class="colLight" colspan="6"><FONT class="Nmessage">&nbsp;No Comments.</FONT></td>
			 </tr>
	  <% } %>
	  
	<%		
	  if( csschooser == true )
		{
			cssclass = "colDark";
			commentbyclass = "colLight";
			csschooser = false;
		}
		else
		{
			cssclass = "colDark";
			commentbyclass = "colLight";
			csschooser = true; 
		}
	%> 
	  
	 <tr> 
	    <td   class="<%= cssclass %>">Created</td>
		<td   colspan = "2" class="<%= commentbyclass %>"><bean:write name="AppendixEditForm" property="createInfo" /></td>
	 </tr>
	 
	<%		
	  if( csschooser == true )
		{
			cssclass = "colDark";
			commentbyclass = "colLight";
			csschooser = false;
		}
		else
		{
			cssclass = "colDark";
			commentbyclass = "colLight";
			csschooser = true; 
		}
	%>
	
	 <tr>
		<td   class="<%= cssclass %>">Updated</td>
		<td   colspan = "2" class="<%= commentbyclass %>"><bean:write name="AppendixEditForm" property="changeInfo" /></td>
     </tr>       
   	<td class = "colLight" colspan="3" align="center">
		<html:submit property = "save" styleClass = "button_c" onclick = "return validate();">Submit</html:submit>&nbsp;
		<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>&nbsp;
		<html:button property = "back" styleClass = "button_c" onclick = "Backaction();">Back</html:button>
	</td>
	</table></td></tr>
</table>
</html:form>


</BODY>

<script>
function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="AppendixEditAction.do?msaId=<bean:write name ='AppendixEditForm' property ='msaId'/>&appendixId=<bean:write name ='AppendixEditForm' property ='appendixId'/>&firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}

function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="AppendixEditAction.do?msaId=<bean:write name ='AppendixEditForm' property ='msaId'/>&appendixId=<bean:write name ='AppendixEditForm' property ='appendixId'/>&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
//alert('in month_Appendix()');
document.forms[0].go.value="true";
document.forms[0].action="AppendixEditAction.do?msaId=<bean:write name ='AppendixEditForm' property ='msaId'/>&appendixId=<bean:write name ='AppendixEditForm' property ='appendixId'/>&week=0&clickShow=true";
document.forms[0].submit();
return true;
}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filterAppendix").style.visibility="visible";
return false;
}

function hideDiv()
{
//alert('in month_Appendix()');
document.getElementById("filterAppendix").style.visibility="hidden";

return false;
}
function checking(checkboxvalue)
{
if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				document.forms[0].appendixSelectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				if(document.forms[0].appendixSelectedOwners[j].checked) {
				 	document.forms[0].appendixSelectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{
	for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
	{
			if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
			{
				document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="AppendixEditAction.do?msaId=<bean:write name ='AppendixEditForm' property ='msaId'/>&appendixId=<bean:write name ='AppendixEditForm' property ='appendixId'/>&opendiv=0";
 				document.forms[0].submit();

			}
	}
	}
}

function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filterAppendix").style.visibility="visible";
<%}%>
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filterAppendix").style.left=leftAdj;
		
}

function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AppendixEditAction.do?msaId=<bean:write name ='AppendixEditForm' property ='msaId'/>&appendixId=<bean:write name ='AppendixEditForm' property ='appendixId'/>&clickShow=true";
	document.forms[0].submit();
	return true;
}

function trimFields() 
{
	var field=document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text') 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}

function Backaction()
{
	history.go(-1);
}

function validate()
{	
	trimFields();
	
		if( document.forms[0].appendixName.value == "" )
 		{
 			alert( "<bean:message bundle = "pm" key = "appendix.tabular.newnameenter"/>");
 			document.forms[0].appendixName.focus();
 			return false;
 		}
		
		if( document.forms[0].appendixTypeBreakout.value == "0" )
 		{
			alert( "<bean:message bundle = "pm" key = "appendix.tabular.typebreakout"/>");
 			document.forms[0].appendixTypeBreakout.focus();
 			return false;
 		}
 		
 		if( document.forms[0].customerDivision.value == "0" )
		  		{
		  			alert( "<bean:message bundle = "pm" key = "appendix.tabular.validcustomerdivision"/>" );
		  			document.forms[0].customerDivision.focus();
		  			return false;
		  		}
		  		
		 if( document.forms[0].siteNo.value == "" )
 		{
 			document.forms[0].siteNo.value =0;
 			//return true;
 		}
 		if( !isInteger( document.forms[0].siteNo.value ) )
			 	{
			 		alert( "Site no. should be a numeric value" );  
			 		document.forms[0].siteNo.value = "";
			 		document.forms[0].siteNo.focus();
			 		return false;
			 	}

 		
 		if( document.forms[0].schDays.value == "" )
  		{
  			document.forms[0].schDays.value = "0";
  		}
 		
 		if( !isInteger( document.forms[0].schDays.value ) )
 		{
 			alert( "<bean:message bundle = "pm" key = "msa.tabular.numericenter"/>" );  
 			document.forms[0].schDays.value = "";
 			document.forms[0].schDays.focus();
 			return false;
 		}
 
 		
 		if( document.forms[0].unionUpliftFactor.value == "" )
	  	{
	  		alert( "<bean:message bundle = "pm" key = "msa.tabular.unionupliftfactorenter"/>" );
	  		document.forms[0].unionUpliftFactor.focus();
	  		return false;
	  	}
	 
		if( !isFloat( document.forms[0].unionUpliftFactor.value ) )
		{
		 	alert( "<bean:message bundle = "pm" key = "msa.tabular.validunionupliftfactor"/>" );
		 	document.forms[0].unionUpliftFactor.value = "";
		 	document.forms[0].unionUpliftFactor.focus();
			return false;
		}
		
		if(document.forms[0].reqPlannedSch.value!="" && document.forms[0].effPlannedSch.value!="") 
		{
			if (!compDate_mdy(document.forms[0].reqPlannedSch.value, document.forms[0].effPlannedSch.value)) 
			{
				document.forms[0].effPlannedSch.focus();
				alert("<bean:message bundle = "pm" key = "msa.tabular.datevalidation"/>");
				return false;
			} 
		}
		document.forms[0].travelAuthorized[0].disabled= false;
		document.forms[0].travelAuthorized[1].disabled= false;
 	return true;
}
</script>
<script>
function disableInableTravelAuthorize(field) 
{
	if(document.forms[0].appendixType.value == '3A' || document.forms[0].appendixType.value =='3B' || document.forms[0].appendixType.value =='3C' || document.forms[0].appendixType.value =='3D') {
		document.forms[0].travelAuthorized[0].disabled=false;
		document.forms[0].travelAuthorized[1].disabled=false;
	} else {
		//document.forms[0].travelAuthorized.value=disabled;
		document.forms[0].travelAuthorized[0].checked = false;
		document.forms[0].travelAuthorized[1].checked = true;
		document.forms[0].travelAuthorized[0].disabled=true;
		document.forms[0].travelAuthorized[1].disabled=true;
	}
}
function disableInableTravelAuthorizeoncheck(field) 
{
	if(document.forms[0].appendixType.value == '3A' || document.forms[0].appendixType.value =='3B' || document.forms[0].appendixType.value =='3C' || document.forms[0].appendixType.value =='3D') {
		document.forms[0].travelAuthorized[0].checked = true;
		document.forms[0].travelAuthorized[1].checked = false;	
		document.forms[0].travelAuthorized[0].disabled=false;
		document.forms[0].travelAuthorized[1].disabled=false;
	} else {
		//document.forms[0].travelAuthorized.value=disabled;
		document.forms[0].travelAuthorized[1].checked = false;
		document.forms[0].travelAuthorized[1].checked = true;
		document.forms[0].travelAuthorized[0].disabled=true;
		document.forms[0].travelAuthorized[1].disabled=true;
	}
}
</script>
</html:html>
