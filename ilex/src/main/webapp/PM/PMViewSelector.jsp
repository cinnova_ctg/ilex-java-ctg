<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	String formBean = request.getParameter("bean");
	String url = request.getParameter("url");
	String fromPage = request.getParameter("fromPage");

	String MSA_Id = "";
	String checkowner=null;
	int allCommentSize = 0;
	
	int addRow = -1;
	boolean addSpace = false;
	int rowHeight = 65;	
	int ownerListSize = 0;
	if( request.getAttribute( "MSA_Id" ) != null)
		MSA_Id = (String) request.getAttribute( "MSA_Id" );
	
	if(request.getAttribute("allCommentSize") != null)
		allCommentSize = Integer.parseInt(request.getAttribute("allCommentSize")+"");
	
	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());
	
	rowHeight += ownerListSize*18;
	
String msastatus="";
String Appendix_Id="";
String status="";
String latestaddendumid ="";
String appendixType = "";
String appendixStatus="";

	
if(request.getAttribute("appendixStatus")!=null)
	appendixStatus= request.getAttribute("appendixStatus").toString();
	
if(request.getAttribute("latestaddendumid")!=null)
	latestaddendumid= request.getAttribute("latestaddendumid").toString();	
	
if( request.getAttribute( "appendixType" ) != null)
	appendixType = (String) request.getAttribute( "appendixType" );
	/* String status =""; */

	if(request.getAttribute("status")!=null){
		status = request.getAttribute("status").toString();
		msastatus = request.getAttribute("status").toString();
	}
	
	if( request.getParameter( "MSA_Id" ) != null )
		MSA_Id = request.getParameter( "MSA_Id" );

	else if( request.getAttribute( "MSA_Id" ) != null)
		MSA_Id = (String) request.getAttribute( "MSA_Id" );

	if( request.getParameter( "Appendix_Id" ) != null )
		Appendix_Id = request.getParameter( "Appendix_Id" );

	else if( request.getAttribute( "Appendix_Id" ) != null)
		Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" );

	if( request.getAttribute( "appendixType" ) != null)
		appendixType = (String) request.getAttribute( "appendixType" );
	
%>
	<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>PM View Selector</title>
	<link rel="stylesheet" href="styles/style.css" type="text/css">
	<%@ include  file="/NMenu.inc" %>
	<%-- <%@ include  file="/AppendixMenu.inc" %> --%>
</head>
<body>
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
        <tr>
       		<%-- <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
			<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
			<%}else{%>	
			<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
			<%}%>	
			<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
			<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
			<td id ="pop4" width ="840" class ="Ntoprow1">&nbsp;</td>  --%>
	
<%-- 		<%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>		  --%>
	
		<div id="menunav">
	<ul>
		<%
		if( !status.equals( "Draft" ) && !status.equals( "Review" )) {
			if(status.equals( "Cancelled" )) {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>
				<%
			} else {
				%>
				<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>
				<%
			}
		} else {
			%>
			<li><a class="drop" href="AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail"><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></a></li>	
			<%
		}
		%>
		<li>
			<a href="#" class="drop"><span>Manage</span></a>
			<ul>
				<li>
					<%
					if((String) request.getAttribute("new_list") != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/></span></a>
						<ul>
							<%= (String) request.getAttribute("new_list") %>
						</ul>
						<%
					} else {
						%>
						<a href="#"><bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/></a>
						<%
					}
					%>
				</li>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.viewappendix"/></span></a>
					<ul>
						<li><a href="AppendixDetailAction.do?MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>">Details</a></li>
						<%
						if( status.equals( "Signed" ) || status.equals( "Approved" ) || status.equals( "Pending Customer Review" )) {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/></a></li>
							<li><a href="javascript:view('rtf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewrtf"/></a></li>
							<%
							if(request.getAttribute("new_addendumlist") != "" ) {
								%>
								<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
								<%
								if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) {
									%>
									<li>
										<a href="#"><span>History</span></a>
										<ul>
											<%= (String) request.getAttribute("new_addendumlist") %>
										</ul>
									</li>
									<li><a href="javascript:view('excel');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/></a></li>
									<%
								} else {
									%>
									<li>
										<a href="#"><span>History</span></a>
										<ul>
											<%= (String) request.getAttribute("new_addendumlist") %>
										</ul>
									</li>
									<%
								}
							} else {
								if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) {
									%>
									<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
									<li><a href="javascript:view('excel');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/></a></li>
									<%
								} else {
									%>
									<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
									<%
								}
							}
						}
						%>
					</ul>
				</li>
				<%
				if( !status.equals( "Draft" ) && !status.equals( "Cancelled" ) && !status.equals( "Review" )) {
					%>
					<li><a href="javascript:sendemail();"><bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert(appendixSend)"><bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/></a></li>
					<%
				}
				if( status.equals( "Signed" ) || status.equals( "Inwork" ) || status.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:setendcustomer();"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				}
// 				if( status.equals( "Signed" ) || status.equals( "Inwork" ) || status.equals( "Cancelled" )) {
					%>
					<%-- <li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li> --%>
					<%
// 				} else {
					%>
					<%-- <li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:setendcustomer();"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li> --%>
					<%
// 				}
				if(status.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:addcontact();"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				}
				%>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.comments"/></span></a>
					<ul>
						<li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "appendix.detail.menu.viewall"/></a></li>
						<%
						if(status.equals( "Cancelled" )) {
							%>
							<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						}
						%>
					</ul>
				</li>
				<%
				if( status.equals( "Pending Customer Review" )) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&entityType=Appendix"><bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert(appendixCRS)"><bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/></a></li>
					<%
				}
				if( !status.equals( "Draft" ) && !status.equals( "Review" )) {
					if(status.equals( "Cancelled" )) {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>
						<%
					} else {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>
						<%
					}
				} else {
					%>
					<li><a href="javascript:editappendix();"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>	
					<%
				}
				%>
				<li><a href="ESAEditAction.do?ref=view&appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>">External Sales Agent</a></li>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.menu.documents"/></span></a>
					<ul>
						<li><a href="EntityHistory.do?entityId=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&entityType=Appendix&function=viewHistory">Appendix History</a></li>
						<li>
							<a href="#"><span>Support Documents</span></a>
							<ul>
								<li><a href="EntityHistory.do?entityId=<%= Appendix_Id %>&appendixId=<%= Appendix_Id %>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Appendix">Upload</a></li>
								<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= Appendix_Id %>&linkLibName=Appendix&function=supplementHistory">History</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="javascript:del();"><bean:message bundle = "pm" key = "appendix.detail.menu.deleteappendix"/></a></li>
			</ul>
		</li>
		<li><a class="drop" href="JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>"><bean:message bundle = "pm" key = "appendix.detail.jobs"/></a></li>
	</ul>
</div>
        </tr>
        <tr>
			<td background="images/content_head_04.jpg" height="21" width ="100%" colspan="5">
				<%if(fromPage.equalsIgnoreCase("ESAEdit.jsp")){ %>
	       		<div id="breadCrumb">
	       			<a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
	       			<a href="AppendixUpdateAction.do?firstCall=true&MSA_Id=<c:out value='${requestScope.msaId }'/>"><c:out value='${requestScope.msaName }'/></a>
	       			<a><span class="breadCrumb1">External Sales Agent Setup</span></a>
	       		</div>
	       		<%} %>
	    	</td>
	    </tr>
      </table>
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
    <td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" conspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                      <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
               			<div id="featured">
								<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
						</div>								
	                </div>		
				<span>
				<div id="filterAppendix" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
                          	  <td class="tabNormalText">
						    		<logic:present name ="statuslist" scope="request">
								    	<logic:iterate id = "list" name = "statuslist"> 
									 		<html:multibox property = "appendixSelectedStatus"> 
												<bean:write name ="list" property = "statusid"/>
									 		</html:multibox>   
										 	<bean:write name ="list" property = "statusdesc"/><br> 
										</logic:iterate>
									</logic:present>
									<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
									<table cellpadding="0">
										<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
										<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
										<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
									</table>	
								</td>
	    					</tr>
                        </table>
                      </td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist"> 
										<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
										<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<%
										checkowner = "javascript: checking('"+ownerId+"');"; 
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 							    	
							    	
								 		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
									  <bean:write name ="olist" property = "ownerName"/>
									  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                          <tr>
                            <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                          </tr>
                          <tr>
                            <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                              </a></td>
                          </tr>
                          <tr>
                            <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                          </tr>
                          <tr>
                            <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                              </a></td>
                          </tr>
                          <tr>
                            <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                          </tr>
                              
                            </table></td>
                        </tr>
  					</table>											
				</div>
				</span>
			</td>
		 </tr>
      	</table></td>
       </tr>
    </table>
   </td>
 </tr>
</table>

<logic:notEqual name="<%=formBean%>" property="appendixId" value="0">
	<%@ include  file="/AppendixMenuScript.inc" %>
</logic:notEqual>
</body>
</html>
<script>
function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="<%=url%>ref=view&appendixId=<bean:write name = '<%=formBean%>' property = 'appendixId' />&Type=Appendix&firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}

function checking(checkboxvalue)
{
if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				document.forms[0].appendixSelectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				if(document.forms[0].appendixSelectedOwners[j].checked) {
				 	document.forms[0].appendixSelectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{

	for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
	{
			if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
			{
				document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="<%=url%>ref=view&appendixId=<bean:write name = '<%=formBean%>' property = 'appendixId'/>&msaId=<bean:write name = "<%=formBean%>" property = "msaId" />&type=Appendix&viewjobtype=ION&Type=Appendix&opendiv=0";
 				document.forms[0].submit();

			}
	}
	}
}

function OnLoad()
{
	<%if(request.getAttribute("opendiv") != null){%>
	document.getElementById("filterAppendix").style.visibility="visible";
	<%}%>
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filterAppendix").style.left=leftAdj;
		
}

function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="<%=url%>ref=view&appendixId=<bean:write name = '<%=formBean%>' property = 'appendixId'/>&type=Appendix&viewjobtype=ION&Type=Appendix&clickShow=true&go=go";
	document.forms[0].submit();
	return true;
}
function ShowDiv()
{
	leftAdjLayers();
	document.getElementById("filterAppendix").style.visibility="visible";
	return false;
}

function hideDiv()
{
	document.getElementById("filterAppendix").style.visibility="hidden";
	return false;
}
function month_Appendix()
{
	document.forms[0].go.value="true";
	document.forms[0].action="<%=url%>ref=view&appendixId=<bean:write name = '<%=formBean%>' property = 'appendixId' />&viewjobtype=ION&Type=Appendix&month=0&clickShow=true&Type=Appendix";
	document.forms[0].submit();
	return true;
}
function week_Appendix()
{
	document.forms[0].go.value="true";
	document.forms[0].action="<%=url%>ref=view&appendixId=<bean:write name = '<%=formBean%>' property = 'appendixId' />&week=0&clickShow=true&Type=Appendix";
	document.forms[0].submit();
	return true;
}
</script>

