<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on sites.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<title></title>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>

<%@ include  file="/Menu.inc" %>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table>
<html:form action="MarginComputationAction">
<html:hidden property = "position" />
<html:hidden property = "size" />
<html:hidden property = "materialResourceSize" />
<html:hidden property = "travelResourceSize" />
<html:hidden property = "freightResourceSize" />
<html:hidden property = "laborResourceSize" />

<table  border="0" cellspacing="1" cellpadding="1" >
  <tr>
  <td width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1"> 
	  <tr>
	    <td colspan="2" class="labeleboldwhite" height="20">Reverse Margin Computation</td>
	  </tr>
	  
	  <tr>
	  	<td class="dblabel" height="20" colspan="2"><bean:write name="MarginComputationForm" property="resourceName"/></td>
	  </tr> 
  	  <tr>
	  	<td class="colDark" height="20" width="150">Estimated Unit Cost:</td>
  		<td class="colLight" height="20">
		  	<html:text styleClass="Nreadonlytextnumbereven" size="12" name="MarginComputationForm" property="estUnitCost" readonly = "true"/>
		</td> 	
	  </tr>
	  <tr>
	  	<td class="colDark" height="20">Target Unit Price:</td>
	  	<td class="colLight" height="20">
	  		<html:text styleClass="text" size="12" maxlength = "9" name="MarginComputationForm" property="targetUnitPrice"/>
	  	</td>
	  </tr>
	  <tr>
	  	<td class="colDark" height="20">New Margin:</td>
	  	<td class="colLight" height="20">
	  		<html:text  styleClass="text" size="9"  maxlength = "9" name="MarginComputationForm" property="newMargin" readonly = "true"/>
	  	</td>
	  </tr>
	  <tr><td height="20" colspan="2" class = "colDark">&nbsp;</td></tr>
	  <tr> 
		<td height="20" colspan="2">
			<html:button styleClass = "button_c" property="computemargin" onclick="marginComputation();">Compute Margin</html:button>
			&nbsp;&nbsp;
			<html:button styleClass = "button_c" property="setmargin" onclick="setMargin();">Set Margin</html:button>
		</td>
      </tr>
  </table>
 </td>
 </tr>
</table>
</html:form>
</table>
</BODY>

<script>
function marginComputation() {
var rlength = 7;

if( document.forms[0].targetUnitPrice.value == ''){
		alert('Please Enter Numeric Value > 0');
		document.forms[0].targetUnitPrice.focus();
		return false;
}
else { 
	if( !isFloat( document.forms[0].targetUnitPrice.value ) )
	{
		alert('Please Enter Numeric Value > 0');
		document.forms[0].targetUnitPrice.value = "";
		document.forms[0].targetUnitPrice.focus();
		return false;
	}
	
	else { 
		if(document.forms[0].targetUnitPrice.value == 0) {
		alert('Please Enter Numeric Value > 0');
		document.forms[0].targetUnitPrice.focus();
		return false;
		}
	}	
}
if(document.forms[0].targetUnitPrice.value == 0)
{
	alert( 'Please Enter Numeric Value > 0' );
	document.forms[0].targetUnitPrice.focus();
	return false;	
}

var margin = 1 - (document.forms[0].estUnitCost.value/document.forms[0].targetUnitPrice.value);
var rnumber = Math.round(margin*Math.pow(10,rlength))/Math.pow(10,rlength);

document.forms[0].newMargin.value = rnumber;
}


function setMargin() {

if(document.forms[0].newMargin.value < 0 || document.forms[0].newMargin.value > 1 || document.forms[0].newMargin.value == '')
{
	alert( 'Margin should be between 0 and 1' );
	document.forms[0].targetUnitPrice.focus();
	return false;
}
if(document.forms[0].size.value > 0){
	if(document.forms[0].size.value == 1) {
		window.opener.document.forms[0].proformamargin.value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].check.checked = true;
		window.opener.calarr(null);
	}	
	else {
		window.opener.document.forms[0].proformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;	
		window.opener.document.forms[0].check[document.forms[0].position.value].checked = true;
		window.opener.calarr(document.forms[0].position.value);
	}
}

if(document.forms[0].materialResourceSize.value > 0){
	if(document.forms[0].materialResourceSize.value == 1) {
		window.opener.document.forms[0].materialProformamargin.value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].materialProformamargin.focus();
		window.opener.document.forms[0].materialCheck.checked = true;
		window.opener.materialcalarr(null);
	}	
	else {
		//window.opener.document.forms[0].proformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].materialProformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;	
		window.opener.document.forms[0].materialProformamargin[document.forms[0].position.value].focus();
		window.opener.document.forms[0].materialCheck[document.forms[0].position.value].checked = true;
		//window.opener.calarr(document.forms[0].position.value);
	}	
}

if(document.forms[0].laborResourceSize.value > 0){
	if(document.forms[0].laborResourceSize.value == 1) {
		window.opener.document.forms[0].laborProformamargin.value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].laborProformamargin.focus();
		window.opener.document.forms[0].laborCheck.checked = true;
		window.opener.laborcalarr(null);
	}	
	else {
		//window.opener.document.forms[0].proformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].laborProformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;	
		window.opener.document.forms[0].laborProformamargin[document.forms[0].position.value].focus();
		window.opener.document.forms[0].laborCheck[document.forms[0].position.value].checked = true;
		//window.opener.calarr(document.forms[0].position.value);
	}	
}

if(document.forms[0].freightResourceSize.value > 0){
	if(document.forms[0].freightResourceSize.value == 1) {
		window.opener.document.forms[0].freightProformamargin.value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].freightProformamargin.focus();
		window.opener.document.forms[0].freightCheck.checked = true;
		window.opener.freightcalarr(null);
	}	
	else {
		//window.opener.document.forms[0].proformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].freightProformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;	
		window.opener.document.forms[0].freightProformamargin[document.forms[0].position.value].focus();
		window.opener.document.forms[0].freightCheck[document.forms[0].position.value].checked = true;
		//window.opener.calarr(document.forms[0].position.value);
	}	
}
if(document.forms[0].travelResourceSize.value > 0){
	if(document.forms[0].travelResourceSize.value == 1) {
		window.opener.document.forms[0].travelProformamargin.value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].travelProformamargin.focus();
		window.opener.document.forms[0].travelCheck.checked = true;
		window.opener.travelcalarr(null);
	}	
	else {
		//window.opener.document.forms[0].proformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;
		window.opener.document.forms[0].travelProformamargin[document.forms[0].position.value].value = document.forms[0].newMargin.value;	
		window.opener.document.forms[0].travelProformamargin[document.forms[0].position.value].focus();
		window.opener.document.forms[0].travelCheck[document.forms[0].position.value].checked = true;
		//window.opener.calarr(document.forms[0].position.value);
	}	
}

window.close();
}


</script>
</html:html>
