<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%
	String temp_str = null;
	String Id = ( String ) request.getAttribute( "Activity_Id" );
	int travel_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString());
%>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.travel.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>
<bean:define id = "checkfortext" name = "refresh" scope = "request"/>
<%@ include file="/Menu.inc" %>

<%
	String backgroundclass = "texto";
	String backgroundhyperlinkclass = null;
	String displaycombo = null;
	String readonlyclass = null;
	String readonlynumberclass = null;
	String backnew = "";
	int i = 0;
	String comboclass = null;
	boolean csschooser = true;
	String schdayscheck = null;
	String valstr = null;
	String arrcal = null;

	String from = "";
	String jobid = "";

	if( request.getAttribute( "from" ) != null )
	{
		from = request.getAttribute( "from" ).toString();
		if(request.getAttribute( "jobid" ) != null )
		jobid = request.getAttribute( "jobid" ).toString();
	}
	
%>



<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages( 'images/sales1b.gif' , 'images/services1b.gif' , 'images/about1b.gif' , 'images/cust-serv1b.gif' , 'images/offers1b.gif')">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  <tr> 
    <td><img src = "images/spacer.gif" width = "1" height = "1"></td>
  </tr>
</table>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  <tr>
    <td class = "toprow"> 
      <table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
        <tr align = "center"> 
           <% if( request.getAttribute( "from" ) != null ){ %>
          		<td width = "140" class = "toprow1"><a href = "MaterialUpdateAction.do?from=<%= from %>&fromPage=jobDBAddResrs&jobid=<%= jobid %>&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Material</a></td>
		        <td width = "150" class = "toprow1"><a href = "LaborUpdateAction.do?from=<%= from %>&fromPage=jobDBAddResrs&jobid=<%= jobid %>&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Labor</a></td>
		        <td width = "120" class = "toprow1"><a href = "FreightUpdateAction.do?from=<%= from %>&fromPage=jobDBAddResrs&jobid=<%= jobid %>&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Freight</a></td>
          		<td class = "toprow1" width = "400"><a href = "ActivityDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "backtoactivity" />&Activity_Id=<%= Id %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "drop"><bean:message bundle = "pm" key = "resource.tabular.back"/></a></td> 		
          		<!--  <td class = "toprow1" width = "400">&nbsp;</td>	-->
          
          <%}else{ %>
		          <td width = "140" class = "toprow1"><a href = "MaterialUpdateAction.do?addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Material</a></td>
		          <td width = "150" class = "toprow1"><a href = "LaborUpdateAction.do?addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Labor</a></td>
		          <td width = "120" class = "toprow1"><a href = "FreightUpdateAction.do?addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Freight</a></td>
				  <td class = "toprow1" width = "400"><a href = "ActivityDetailAction.do?addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "backtoactivity" />&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id" />" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "drop"><bean:message bundle = "pm" key = "resource.tabular.back"/></a></td>		
		         
           <%} %>
         
        </tr>
      </table>
    </td>
  </tr>
</table>


<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
 <tr>
 	
  	<td>
  		<table border = "0" cellspacing = "1" cellpadding = "1" width = "1500">
  		
		  		<logic:present name = "addflag" scope = "request">
		  			<tr>
  				<td>&nbsp;</td>
	    		<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "addflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.travel.add.success"/>		
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.travel.add.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.travel.add.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9003">
			    		<bean:message bundle = "AM" key = "am.travel.add.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.travel.add.failure4"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9005">
			    		<bean:message bundle = "AM" key = "am.travel.add.failure5"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9006">
			    		<bean:message bundle = "AM" key = "am.travel.add.failure6"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
				</logic:present>
		
				<logic:present name = "updateflag" scope = "request">
				<tr>
  				<td>&nbsp;</td>
	    		<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "updateflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.travel.update.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9007">
			    		<bean:message bundle = "AM" key = "am.travel.update.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "updateflag" value = "-9008">
			    		<bean:message bundle = "AM" key = "am.travel.update.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "updateflag" value = "-9009">
			    		<bean:message bundle = "AM" key = "am.travel.update.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9010">
			    		<bean:message bundle = "AM" key = "am.travel.update.failure4"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
				</logic:present>
  		
		  		<logic:present name = "deleteflag" scope = "request">
		  		<tr>
  				<td>&nbsp;</td>
	    		<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "deleteflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.travel.delete.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.travel.delete.failure1"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.travel.delete.failure2"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.travel.delete.failure3"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
			    </logic:present>
		  
  		
			<tr> 
				<td>&nbsp; </td>
	    		<td class = "labeleboldhierrarchy" colspan = "13" height = "30">
	    			<bean:message bundle = "pm" key = "pm.travel.header"/>&nbsp;  
	   				<A href = "MSADetailAction.do?MSA_Id=<bean:write name = "TravelResourceForm" property = "msa_id" />"><bean:write name ="TravelResourceForm" property = "msaname"/></A>
					<bean:message bundle = "pm" key = "appendix.detail.arrow"/>&nbsp;
	   				<logic:equal name = "TravelResourceForm" property = "chkaddendum" value = "View">
						<A href = "AppendixDetailAction.do?Appendix_Id=<bean:write name = "TravelResourceForm" property = "appendix_id" />"><bean:write name ="TravelResourceForm" property = "appendixname"/></A>
						<bean:message bundle = "pm" key = "activity.tabular.job"/>&nbsp;
						<A href="JobDetailAction.do?ref=detailjob&Job_Id=<bean:write name = "TravelResourceForm" property = "jobid" />"><bean:write name ="TravelResourceForm" property = "jobname"/></A>
						<bean:message bundle = "pm" key = "pm.material.activity"/>&nbsp;
						<A href = "ActivityDetailAction.do?addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id"/>&ref=<bean:write name = "TravelResourceForm" property = "backtoactivity" />&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id" />"><bean:write name ="activityname" scope = "request"/> </A>
						
					</logic:equal>
	   				<logic:notEqual name = "TravelResourceForm" property = "chkaddendum" value = "View">
						<A href = "AppendixHeader.do?function=view&appendixid=<bean:write name = "TravelResourceForm" property = "appendix_id" />"><bean:write name ="TravelResourceForm" property = "appendixname"/></A>
						<bean:message bundle = "pm" key = "activity.tabular.job"/>&nbsp;
						<A href="JobDashboardAction.do?jobid=<bean:write name = "TravelResourceForm" property = "jobid" />"><bean:write name ="TravelResourceForm" property = "jobname"/></A>
						<bean:message bundle = "pm" key = "pm.material.activity"/>&nbsp;
		    			<bean:write name ="activityname" scope = "request"/> 
					</logic:notEqual>
	    			
	    			
	    		</td>
			</tr>
  
	  		<tr> 
	    		<td rowspan = "2">&nbsp; </td>
	    		<td class = "tryb" rowspan = "2">
	    			<bean:message bundle = "AM" key = "am.travel.name"/>
	    		</td>
	   
				<td class = "tryb" rowspan = "2">
					<bean:message bundle = "AM" key = "am.travel.type"/>
				</td>
				
				<td class = "tryb" rowspan = "2">
					<bean:message bundle = "AM" key = "am.travel.cnspartnumber"/>
				</td>
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.travel.quantity"/>
			    </td>
			  
			    <td class = "tryb" colspan = "2">
			    	<bean:message bundle = "AM" key = "am.travel.estimatedcost"/>
			    </td>
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.travel.proformamargin"/>
			    </td>
			    <td class = "tryb" colspan = "2">
			    	<bean:message bundle = "AM" key = "am.travel.price"/>
			    </td>
			    
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.travel.status"/>
			    </td>
			       <td width = 50%></td>
			</tr>
  
	  		<tr> 
	    		<td class = "tryb"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.travel.estimatedunitcost"/>
	      			</div>
	    		</td>
	    
	    		<td class = "tryb"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.travel.estimatedtotalcost"/>
	      			</div>
	    		</td>
	    
	    
			    <td class = "tryb"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.travel.priceunit"/>
			      </div>
			    </td>
	    
			    <td class = "tryb"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.travel.priceextended"/>
			      </div>
			    </td>
			</tr>
			
<html:form action = "/TravelUpdateAction">
<html:hidden  property = "fromPage" />
	<logic:present name = "travelresourcelist" scope = "request"> 
 		<logic:iterate id = "ms" name = "travelresourcelist">
			<bean:define id = "resourceId" name = "ms" property = "travelid" type = "java.lang.String"/>
			<bean:define id = "estUnitCost" name = "ms" property = "estimatedunitcost" type = "java.lang.String"/>
		<%	
			temp_str = "marginComputation('"+resourceId+"', '"+estUnitCost+"', "+i+", "+travel_size+");"; 	
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				comboclass = "combooddhidden";
				csschooser = false;
				readonlyclass = "readonlytextodd";
				readonlynumberclass = "readonlytextnumberodd";
				backgroundhyperlinkclass = "hyperodd";
			}
	
			else
			{
				csschooser = true;	
				comboclass = "comboevenhidden";
				backgroundclass = "texte";
				readonlyclass = "readonlytexteven";
				readonlynumberclass = "readonlytextnumbereven";
				backgroundhyperlinkclass = "hypereven";
			}
		%>
			<bean:define id = "val" name = "ms" property = "travelid" type = "java.lang.String"/>
			<bean:define id = "flag" name = "ms" property = "flag" type = "java.lang.String"/>
			<html:hidden name="ms" property="flag_travelid"/>
			<tr>		
				
				<td class = "labeleboldwhite"> 
					<logic:equal name = "ms" property = "status" value = "Approved">
						<%--<html:checkbox property = "check" value = "<%= flag+val %>" disabled = "true"/>	--%>
						<html:multibox property = "check" disabled = "true"><bean:write  name="ms" property = "flag_travelid"/></html:multibox>	
					</logic:equal>
				
					<logic:notEqual name = "ms" property = "status" value = "Approved">
						<%--<html:checkbox property = "check" value = "<%= flag+val %>" /> --%>
						<html:multibox property = "check" ><bean:write  name="ms" property = "flag_travelid"/></html:multibox>
					</logic:notEqual>
	    		</td>
				
				<html:hidden name = "ms" property = "travelid"/>
				<html:hidden name = "ms" property = "travelcostlibid" />
				
				<%  
					if( travel_size == 1 )
					{
						valstr = "javascript: document.forms[0].check.checked = true;updatetext(this,'');"; 
						displaycombo = "javascript: displaycombo(this,'')";
						schdayscheck = "javascript: document.forms[0].check.checked = true;";
						arrcal = "javascript: calarr( null );";		
					}
					else if( travel_size > 1 )
					{
						valstr = "javascript: document.forms[0].check['"+i+"'].checked = true;updatetext( this ,'"+i+"' );"; 
						schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;";
						displaycombo = "javascript: displaycombo( this , '"+i+"' )";
						arrcal = "javascript: calarr('"+i+"');";
					}
				%>
				
				<!-- for multiple resource select start -->
				<html:hidden name="ms" property="flag"/>
				<logic:equal name="ms" property="flag" value="T">
				<td class = "<%= backgroundhyperlinkclass %>"> 
					<bean:write name = "ms" property = "travelname" />
				</td>
				</logic:equal>
				<logic:equal name="ms" property="flag" value="P">
				<td class = "<%= backgroundhyperlinkclass %>"> 
					<% if( request.getAttribute( "from" ) != null )
					{%>
						<a href = "TravelDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendumdetail" />&Travel_Id=<bean:write name = "ms" property = "travelid"/>&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "travelname" /></a>
					<%}else{ %>
						<a href = "TravelDetailAction.do?addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendumdetail" />&Travel_Id=<bean:write name = "ms" property = "travelid"/>&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "travelname" /></a>
					<%} %>
				</td>
				</logic:equal>
				<!-- for multiple resource select end -->
				<!--  <td class = "<%= backgroundhyperlinkclass %>"> 
					<% if( request.getAttribute( "from" ) != null )
					{%>
						<a href = "TravelDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendumdetail" />&Travel_Id=<bean:write name = "ms" property = "travelid"/>&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "travelname" /></a>
					<%}else{ %>
						<a href = "TravelDetailAction.do?addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "chkaddendumdetail" />&Travel_Id=<bean:write name = "ms" property = "travelid"/>&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "travelname" /></a>
					<%} %>
				
				</td>-->
				
				<html:hidden name = "ms" property = "travelname" />
				
				<td class = "<%= backgroundhyperlinkclass %>">
					<bean:write name = "ms" property = "traveltype" />
					<html:hidden styleId = "traveltype" name = "ms" property = "traveltype" />
				</td>
				
				<td class = "<%= backgroundhyperlinkclass %>">
					<bean:write name = "ms" property = "cnspartnumber" />
					<html:hidden styleId = "cnspartnumber" name = "ms"  property = "cnspartnumber" />
				</td>
			   
				<logic:equal name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text  name = "ms" property = "quantity" size = "6" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
					</td>
				</logic:equal>		
				
				<logic:notEqual name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "quantity"+i%>" name = "ms"  size = "6" maxlength = "6" property = "quantity" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
					</td>
				</logic:notEqual>
				
				<html:hidden name = "ms" property = "prevquantity" />
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "estimatedunitcost" name = "ms" size = "6"  property = "estimatedunitcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td>
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "estimatedtotalcost" name = "ms" size = "10" maxlength = "9"  property = "estimatedtotalcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td> 
			   
				<logic:equal name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
					</td>
					
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "priceunit" name = "ms" size = "10" maxlength = "9"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" disabled="true"><bean:message bundle = "AM" key = "am.common.set"/></html:button>
			   		</td>
				</logic:equal>
				
				<logic:notEqual name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
				    </td> 

	 				<td class = "<%= backgroundclass %>">
						<html:text styleId = "priceunit" name = "ms" size = "10" maxlength = "9"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" onclick = "<%= temp_str%>"><bean:message bundle = "AM" key = "am.common.set"/></html:button>
			   		</td>
				</logic:notEqual>
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "priceextended" name = "ms" size = "10" maxlength = "9"  property = "priceextended" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "ms" property = "status"/>		
				</td>
				
				<html:hidden name = "ms" property = "status"/>	
				
				<html:hidden name = "ms" property = "sellablequantity" />
	   			<html:hidden name = "ms" property = "minimumquantity" /> 
	   			
	   				
	  		</tr>
  		
	   <% i++;  %>
		</logic:iterate>
	</logic:present>
	
				<%	
				  	if( travel_size != 0 )
				  	{
				  		if( travel_size % 2 == 1 )
				  		{
							backnew = "textenewelement";
				  			backgroundclass = "texte";
				  			i = 2;
							displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
							comboclass = "comboevenhidden";
							readonlyclass = "readonlytexteven";
							readonlynumberclass = "readonlytextnumbereven";
						}
				  		else
				  		{
							backnew = "textonewelement";
				  			backgroundclass = "texto";
				  			i = 1;
							displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
							comboclass = "combooddhidden";
							readonlyclass = "readonlytextodd";
							readonlynumberclass = "readonlytextnumberodd";
				  		}
				  	}
				  	else
					{
						backnew = "textonewelement";
						backgroundclass = "texto";
						i = 1;
						displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
						comboclass = "combooddhidden";
						readonlyclass = "readonlytextodd";
						readonlynumberclass = "readonlytextnumberodd";
					}
	   			%>
	   		
	   			<html:hidden property = "activity_Id" />
	   			<html:hidden property = "ref" /> 
	   			<html:hidden property = "newsellablequantity" />
	   			<html:hidden property = "newminimumquantity" />
	   			<html:hidden property = "backtoactivity" />
	   			<html:hidden property = "chkaddendum" />
	   			<html:hidden property = "chkaddendumdetail" />
	   			<html:hidden property = "addendum_id" />
	   			<html:hidden property = "fromflag" />
	   			<html:hidden property = "dashboardid" />
	   			
	   		<tr> 
		    	<td class = "labeleboldwhite"> 
		    		<logic:equal name = "act_status"  value = "Approved" scope = "request">
		    			<html:checkbox property = "newtravelid" disabled = "true"/>
		    		</logic:equal>
		    		
		    		<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
		    			<html:checkbox property = "newtravelid" />
		    		</logic:notEqual>
		   		</td>
		   		
		   		<td class = "<%= backnew %>">
		   	 		<logic:equal name = "act_status"  value = "Approved" scope = "request">
		   	 			<html:text property = "newtravelname" size = "20" styleClass = "<%= readonlyclass %>" readonly = "true" />
			   			<html:hidden property = "newtravelnamecombo" />
		   	 		</logic:equal>
		   	 		
		   	 		<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
		   	 			<html:text styleId = "newtravelname" property = "newtravelname" size = "20" styleClass = "textbox" onclick ="javascript:opensearchwindow();" readonly="true" onmouseover = "highlightTextField( this );" onmouseout = "unHighlightTextField( this );" />
			   			<html:select styleId = "newtravelnamecombo" property = "newtravelnamecombo" size = "1" styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"    onchange = "document.forms[0].newtravelid.checked = true; updatetext( this , null ); return refresh();">
					    	<html:optionsCollection  property = "travellist" value = "value" label = "label"/> 
				   		</html:select>
		   	 		</logic:notEqual>
		   		</td>
		   		
		   		<td class = "<%= backgroundclass %>">
		   	 		<bean:write name = "TravelResourceForm" property = "newtraveltype" />
		   	 		<html:hidden styleId = "newtraveltype" property = "newtraveltype" />
		   		</td>
		   		
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "TravelResourceForm" property = "newcnspartnumber" />
					<html:hidden styleId = "newcnspartnumber"  property = "newcnspartnumber" />
				</td>
				
				<td class = "<%= backgroundclass %>">
					<logic:equal name = "act_status"  value = "Approved" scope = "request">
						<html:text size = "4" maxlength = "6" property = "newquantity" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
					</logic:equal>
					
					<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
						<% if( checkfortext.equals( "true" ) ) { %>	
							<html:text size = "4" maxlength = "6" property = "newquantity" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
						<%}else { %>
							<html:text styleId = "newquantity"  size = "4" maxlength = "6" property = "newquantity" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "document.forms[0].newtravelid.checked = true;" onblur = "calnew();" />
						<%} %>
					</logic:notEqual>
				</td>
				
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "newestimatedunitcost"  size = "6"  property = "newestimatedunitcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td>
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "newestimatedtotalcost" size = "10" maxlength = "9"  property = "newestimatedtotalcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td> 
			   
				<td class = "<%= backgroundclass %>">
					<logic:equal name = "act_status"  value = "Approved" scope = "request">
						<html:text size = "6" maxlength = "5" property = "newproformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
			    	</logic:equal>
			    	
			    	<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
			    		<% if( checkfortext.equals( "true" ) ) { %>	
			    			<html:text size = "6" maxlength = "5" property = "newproformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
			    		<%}else { %>
			    			<html:text styleId = "newproformamargin" size = "6" maxlength = "5" property = "newproformamargin" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "document.forms[0].newtravelid.checked = true;" onblur = "calnew();"/>
			    		<%} %>
			    	</logic:notEqual>
			    </td> 
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "newpriceunit" size = "10" maxlength = "9"  property = "newpriceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "newpriceextended"  size = "10" maxlength = "9"  property = "newpriceextended" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "TravelResourceForm" property = "newstatus"/>		
				</td>        	
	  		</tr>
	  		
	  		
	  		<tr>	<td >&nbsp; </td>
			  	<td colspan = "9" class = "buttonrow">
			  		<html:submit property = "save" styleClass = "button" onclick = "return validate();"><bean:message bundle = "AM" key = "am.travel.submit"/></html:submit>
					<html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "AM" key = "am.travel.sort"/></html:button>
					<!-- Start :Added By Amit for Mass delete and Approve ,on 14.08.2006-->
					<logic:equal name = "TravelResourceForm" property = "chkaddendum" value = "View">
					<html:button property = "massDelete" styleClass = "button" onclick = "return del();"><bean:message bundle = "AM" key = "am.material.massDelete"/></html:button>

					<html:button property = "approve" styleClass = "button" onclick = "return massApprove();"><bean:message bundle = "AM" key = "am.material.massApprove"/></html:button>
					</logic:equal>
					<!-- End :Added By Amit for Mass delete and Approve,on 14.08.2006-->
					<% if( request.getAttribute( "from" ) != null ){ 
					 %>
						<logic:present name="TravelResourceForm" property="fromPage">
						<html:button property = "back"  styleClass="button" onclick = "return BackTrack();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
						</logic:present>
					<%	}else{  
					%> 
						<!--<html:button property = "back"  styleClass="button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>-->
					<%} 
					%>
				</td>
				<td class = "buttonrow">
					<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "AM" key = "am.travel.cancel"/></html:reset>
				</td>
			</tr>    
		</table>
	</td>
</tr>
</table>
  
</html:form>

<script>
function marginComputation(resourceId, estUnitCost, position, size)
{
	str = "MarginComputationAction.do?resourceId="+resourceId+"&estUnitCost="+estUnitCost+"&position="+position+"&size="+size;	
	suppstrwin = window.open(str, '', 'left = 300 , top = 250 , width = 320, height = 240 , resizable = yes , scrollbars = yes')
	suppstrwin.focus();
}

function opensearchwindow()
{
	str = "OpenSearchWindowTempAction.do?ref=<bean:write name = "TravelResourceForm" property = "ref" />&type=Travel&from=pm&activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id" />&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />";

	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}

function sortwindow()
{
	str = 'SortAction.do?addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&Type=pm_resT&Activitylibrary_Id=<%= Id %>&ref=<bean:write name = "TravelResourceForm" property = "chkaddendum" />';
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	//p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); 
}

function validate()
{
	
	
	var submitflag = 'false';
	if( <%= travel_size %> != 0 )
	{
		if( <%= travel_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newtravelid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.travel.selectresource"/>" );
				return false;
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newtravelid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.travel.selectresource"/>" );
				return false;
		  	}
		}
	}
	
	else
	{
		if( !document.forms[0].newtravelid.checked )
		{
			alert( "<bean:message bundle = "AM" key = "am.travel.newtravel"/>" );
			return false;
		}
	}
	
	if( <%= travel_size %> != 0 )
	{
		if( <%= travel_size %> == 1 )
		{
			if( document.forms[0].check.checked ) 
	  		{
	  			if( document.forms[0].quantity.value == "" )
	  			{
	  				document.forms[0].quantity.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].quantity.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
		 			document.forms[0].quantity.value = "";
		 			document.forms[0].quantity.focus();
					return false;
		 		}
		 		
		 		if( document.forms[0].proformamargin.value == "" )
	  			{
	  				document.forms[0].proformamargin.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].proformamargin.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
		 			document.forms[0].proformamargin.value = "";
		 			document.forms[0].proformamargin.focus();
					return false;
		 		}	
	  		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
			  		if( document.forms[0].quantity[i].value == "" )
		  			{
		  				document.forms[0].quantity[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].quantity[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
			 			document.forms[0].quantity[i].value = "";
			 			document.forms[0].quantity[i].focus();
						return false;
			 		}
			 		
			 		if( document.forms[0].proformamargin[i].value == "" )
		  			{
		  				document.forms[0].proformamargin[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].proformamargin[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
			 			document.forms[0].proformamargin[i].value = "";
			 			document.forms[0].proformamargin[i].focus();
						return false;
			 		}	
		  		}
		  	}
		}
	}
	
	
	if( document.forms[0].newtravelid.checked ) 
  	{
	  	if( document.forms[0].newtravelnamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.travel.travelenter"/>" );
 			document.forms[0].newtravelname.focus();
 			return false;
 		}
	  	
	  	
	  	if( document.forms[0].newquantity.value == "" )
  		{
  			alert( "<bean:message bundle = "AM" key = "am.material.quantitymandatory"/>" );
  			document.forms[0].newquantity.focus();
  			return false;
  		}
 
	  	if( !isFloat( document.forms[0].newquantity.value ) )
	 	{
	 		alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
	 		document.forms[0].newquantity.value = "";
	 		document.forms[0].newquantity.focus();
			return false;
	 	}
	 		
	 	if( document.forms[0].newproformamargin.value == "" )
  		{
  			document.forms[0].newproformamargin.value = "0.0";
  		}
 
	  	if( !isFloat( document.forms[0].newproformamargin.value ) )
	 	{
	 		alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
	 		document.forms[0].newproformamargin.value = "";
	 		document.forms[0].newproformamargin.focus();
			return false;
	 	}	
  	}
	document.forms[0].ref.value = "Submit";
	return true;
}

function refresh()
{
	document.forms[0].ref.value = "Refresh";
	document.forms[0].submit();
	return true;
}

function calarr( i )
{
	if( <%= travel_size %> == 1 )
	{
		if( ( document.forms[0].quantity.value == "" ) || ( document.forms[0].quantity.value == "null" ) )
		{ 
			document.forms[0].quantity.value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantity.value ) ) 
		{ 
			if( parseFloat( document.forms[0].quantity.value ) <  parseFloat( document.forms[0].minimumquantity.value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].quantity.value = document.forms[0].minimumquantity.value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].quantity.value )  ) /  parseFloat( document.forms[0].sellablequantity.value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].quantity.value ) - (divisor * parseFloat( document.forms[0].sellablequantity.value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].quantity.value = (parseFloat( document.forms[0].sellablequantity.value )*(divisor + 1)).toFixed(4) ; 
			}
			
			/*var remainder = ( parseFloat( document.forms[0].quantity.value ) - parseFloat( document.forms[0].minimumquantity.value ) ) %  parseFloat( document.forms[0].sellablequantity.value );
			
			if(  remainder == 0 )
			{
			}
			else
			{
				var temp = ( parseFloat( document.forms[0].quantity.value ) - parseFloat( document.forms[0].minimumquantity.value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].sellablequantity.value );
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].quantity.value = parseFloat( document.forms[0].minimumquantity.value ) + ( parseFloat( document.forms[0].sellablequantity.value ) * ( parseFloat( temp ) + 1 ) ); 
			}*/
			
			document.forms[0].estimatedtotalcost.value = parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedunitcost.value );
			document.forms[0].estimatedtotalcost.value = round_func( document.forms[0].estimatedtotalcost.value );
		
			if( ( document.forms[0].proformamargin.value == "" ) || ( document.forms[0].proformamargin.value == "null" ) || ( document.forms[0].proformamargin.value >= "1" ) )
			{ 
				document.forms[0].proformamargin.value = "0.0";
			}
		
			if( isFloat( document.forms[0].proformamargin.value ) ) 
			{ 
				document.forms[0].priceunit.value = parseFloat( document.forms[0].estimatedunitcost.value ) / ( 1 - parseFloat( document.forms[0].proformamargin.value ) );
				document.forms[0].priceunit.value = round_func( document.forms[0].priceunit.value );
			}
			
			if( isFloat( document.forms[0].quantity.value ) ) 
			{ 
				document.forms[0].priceextended.value = parseFloat( document.forms[0].priceunit.value ) * parseFloat( document.forms[0].quantity.value );
				document.forms[0].priceextended.value = round_func( document.forms[0].priceextended.value );
			}
		}
	}
	
	
	else
	{
		if( ( document.forms[0].quantity[i].value == "" ) || ( document.forms[0].quantity[i].value == "null" ) )
		{ 
			document.forms[0].quantity[i].value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantity[i].value ) ) 
		{ 
			if( parseFloat( document.forms[0].quantity[i].value ) <  parseFloat( document.forms[0].minimumquantity[i].value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].quantity[i].value = document.forms[0].minimumquantity[i].value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].quantity[i].value )  ) /  parseFloat( document.forms[0].sellablequantity[i].value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].quantity[i].value ) - (divisor * parseFloat( document.forms[0].sellablequantity[i].value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].quantity[i].value = (parseFloat( document.forms[0].sellablequantity[i].value )*(divisor + 1)).toFixed(4) ; 
			}
			
			/*var remainder = ( parseFloat( document.forms[0].quantity[i].value ) - parseFloat( document.forms[0].minimumquantity[i].value ) ) %  parseFloat( document.forms[0].sellablequantity[i].value );
			
			if(  remainder == 0 )
			{
			}
			else
			{
				var temp = ( parseFloat( document.forms[0].quantity[i].value ) - parseFloat( document.forms[0].minimumquantity[i].value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].sellablequantity[i].value );
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].quantity[i].value = parseFloat( document.forms[0].minimumquantity[i].value ) + ( parseFloat( document.forms[0].sellablequantity[i].value ) * ( parseFloat( temp ) + 1 ) ); 
			}*/
			
			document.forms[0].estimatedtotalcost[i].value = parseFloat( document.forms[0].quantity[i].value ) * parseFloat( document.forms[0].estimatedunitcost[i].value );
			document.forms[0].estimatedtotalcost[i].value = round_func( document.forms[0].estimatedtotalcost[i].value );
		
			if( ( document.forms[0].proformamargin[i].value == "" ) || ( document.forms[0].proformamargin[i].value == "null" ) || ( document.forms[0].proformamargin[i].value >= "1" ) )
			{ 
				document.forms[0].proformamargin[i].value = "0.0";
			}
		
			if( isFloat( document.forms[0].proformamargin[i].value ) ) 
			{ 
				document.forms[0].priceunit[i].value = parseFloat( document.forms[0].estimatedunitcost[i].value ) / ( 1 - parseFloat( document.forms[0].proformamargin[i].value ) );
				document.forms[0].priceunit[i].value = round_func( document.forms[0].priceunit[i].value );
			}
			
			if( isFloat( document.forms[0].quantity[i].value ) ) 
			{ 
				document.forms[0].priceextended[i].value = parseFloat( document.forms[0].priceunit[i].value ) * parseFloat( document.forms[0].quantity[i].value );
				document.forms[0].priceextended[i].value = round_func( document.forms[0].priceextended[i].value );
			}
		}
	}
}

function calnew()
{
	if( document.forms[0].newtravelnamecombo.value == '0' )
	{
		alert( "<bean:message bundle = "AM" key = "am.travel.travelenter"/>" ); 
		return false;
	}
	
	if( ( document.forms[0].newquantity.value == "" ) || ( document.forms[0].newquantity.value == "null" ) )
	{ 
		document.forms[0].newquantity.value = "0.0";
	}
	
	if( isFloat( document.forms[0].newquantity.value ) ) 
	{ 
		if( parseFloat( document.forms[0].newquantity.value ) <  parseFloat( document.forms[0].newminimumquantity.value ) )
		{
			alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
			document.forms[0].newquantity.value = document.forms[0].newminimumquantity.value;
		}
		
		var divisor =  ( parseFloat( document.forms[0].newquantity.value )  ) /  parseFloat( document.forms[0].newsellablequantity.value );
		divisor = parseInt(divisor);
		var remainder = parseFloat( document.forms[0].newquantity.value ) - (divisor * parseFloat( document.forms[0].newsellablequantity.value ) );
		var zero=0;
		if(  remainder.toFixed(4) == zero.toFixed(4) )
		{
		}
		else
		{
			alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
			document.forms[0].newquantity.value = parseFloat( document.forms[0].newsellablequantity.value ) * (divisor + 1); 
		}
		
		/*var remainder = ( parseFloat( document.forms[0].newquantity.value ) - parseFloat( document.forms[0].newminimumquantity.value ) ) %  parseFloat( document.forms[0].newsellablequantity.value );
		
		if(  remainder == 0 )
		{
		}
		else
		{
			var temp = ( parseFloat( document.forms[0].newquantity.value ) - parseFloat( document.forms[0].newminimumquantity.value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].newsellablequantity.value );
			alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
			document.forms[0].newquantity.value = parseFloat( document.forms[0].newminimumquantity.value ) + ( parseFloat( document.forms[0].newsellablequantity.value ) * ( parseFloat( temp ) + 1 ) ); 
		}*/
		
		document.forms[0].newestimatedtotalcost.value = parseFloat( document.forms[0].newquantity.value ) * parseFloat( document.forms[0].newestimatedunitcost.value );
		document.forms[0].newestimatedtotalcost.value = round_func( document.forms[0].newestimatedtotalcost.value );
	
		if( ( document.forms[0].newproformamargin.value == "" ) || ( document.forms[0].newproformamargin.value == "null" ) || ( document.forms[0].newproformamargin.value >= "1" ) )
		{ 
			document.forms[0].newproformamargin.value = "0.0";
		}
	
		if( isFloat( document.forms[0].newproformamargin.value ) ) 
		{ 
			document.forms[0].newpriceunit.value = parseFloat( document.forms[0].newestimatedunitcost.value ) / ( 1 - parseFloat( document.forms[0].newproformamargin.value ) );
			document.forms[0].newpriceunit.value = round_func( document.forms[0].newpriceunit.value );
		}
		
		if( isFloat( document.forms[0].newquantity.value ) ) 
		{ 
			document.forms[0].newpriceextended.value = parseFloat( document.forms[0].newpriceunit.value ) * parseFloat( document.forms[0].newquantity.value );
			document.forms[0].newpriceextended.value = round_func( document.forms[0].newpriceextended.value );
		}
	}
}

function Backactionjob()
{
	<% if(  request.getAttribute( "jobid" ) != null ){%>
		document.forms[0].action = "ActivityDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id" />&ref=<bean:write name = "TravelResourceForm" property = "backtoactivity" />&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id" />";
	<%} %>
	document.forms[0].submit();
	return true;
}

function BackTrack()
{

	<% if(  request.getAttribute( "jobid" ) != null ){%>
		document.forms[0].action = "JobDashboardAction.do?from=<%= from %>&jobid=<%= jobid %>&appendix_id=<bean:write name = "TravelResourceForm" property = "appendix_id" />&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id"/>&ref=<bean:write name = "TravelResourceForm" property = "backtoactivity" />&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id" />"
	<%} %>
	document.forms[0].submit();
	return true;
}

//Start:Added By Amit For Mass Approve
function massApprove()
{
	var submitflag = 'false';
	if( <%= travel_size %> != 0 )
	{
		if( <%= travel_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newtravelid.checked ) )
			{
				
				alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassapprove"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
			
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newtravelid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassapprove"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassapprove"/>" );
	}
	
	if( document.forms[0].newtravelid.checked ) 
  	{
	  	
	  	if( document.forms[0].newtravelnamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassapprove"/>" );
 			document.forms[0].newtravelname.focus();
 			return false;
 		}
	
  	}
  	else
  	{
  	}

	if(submitflag =='true')
	{

		document.forms[0].action = "TravelUpdateAction.do?ref=massApprove&Activitylibrary_Id=<%= Id %>&jobid=<%= jobid %>";			

		document.forms[0].submit();
		return true;
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassapprove"/>" );
		return false;
	}
}
//End :For Mass Approve

//Start :Added by Amit For Mass Delete


function del()
{

	
	var submitflag = 'false';
	if( <%= travel_size %> != 0 )
	{
		if( <%= travel_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newtravelid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassdelete"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newtravelid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassdelete"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassdelete"/>" );
	}
	
	if( document.forms[0].newtravelid.checked ) 
  	{
	  	
	  	if( document.forms[0].newtravelnamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.travel.travelenter"/>" );
 			document.forms[0].newtravelname.focus();
 			return false;
 		}
	
  	}
  	else
  	{
  	}

	if(submitflag =='true')
	{
	
		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "TravelUpdateAction.do?type=<bean:write name = "TravelResourceForm" property = "chkaddendum"/>&addendum_id=<bean:write name = "TravelResourceForm" property = "addendum_id"/>&ref=massDelete&Activity_Id=<bean:write name = "TravelResourceForm" property = "activity_Id"/>";

			document.forms[0].submit();
			return true;	
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.travel.selectresourceformassdelete"/>" );
		return false;
	}
	
}

//End : For Mass delete

</script>

</body>
</html:html>