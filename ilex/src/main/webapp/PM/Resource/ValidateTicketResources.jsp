<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script>
function ValidateTicketResources(index){
	/**
		1. Pick Default min and max resource value.
	 	2. Check min and max resource with changed resource.
		3. Set alert message.
	*/
	var isTransit;
	var qty;
	if(index == null){ // - When Single Resource Present.
		isTransit = document.forms[0].laborChecktransit.checked;
		qty = document.forms[0].laborQuantityhours.value;
	}// -End of If
	else{ // - When mulipule Resource Present.
		isTransit = document.forms[0].laborChecktransit[index].checked;
		qty = document.forms[0].laborQuantityhours[index].value;
	} // -End of Else
	
	/* Set Default values for Vailidation */
	var minLaborQty='<fmt:formatNumber value="${ResourceListForm.ticketMinLaborHour}" type="currency" pattern="######0.0000"/>';
	var minTravelQty='<fmt:formatNumber value="${ResourceListForm.ticketMinTravelHour}" type="currency" pattern="######0.0000"/>';
	var requestType='<c:out value="${ResourceListForm.ticketRequestType}"/>';
	
	var travelCheck=true;
	var laborCheck=true;
	var str = "";
	
	/* Validate In-Transit Resource with min Qty. */
	if(isTransit){
		if(requestType == 'NetMedX - Standard'){
			if(parseFloat(qty) < parseFloat(minTravelQty) || parseFloat(qty) > 1 ){
				travelCheck = false;
				if(parseFloat(qty) < parseFloat(minTravelQty)){str = "less";}
				else if(parseFloat(qty) > 1){str = "greater";}
			}
		}else{
			if(parseFloat(qty) != parseFloat(minTravelQty)){
				travelCheck = false;
				if(parseFloat(qty) < parseFloat(minTravelQty)){str = "less";}
				else if(parseFloat(qty) > 1){str = "greater";}
			}
		}
	}
	/* Validate Labor Resource with Max Qty. */
	else{
		if(parseFloat(qty) < parseFloat(minLaborQty) ){
			laborCheck = false;
		}
	}
	
	/* Alert Messages */
	if(!travelCheck){
		alert("Quantity of In-Transit resource of "+requestType+" ticket can not be "+str+" than 1.0000.");
		if(index == null){
			document.forms[0].laborQuantityhours.value = round_funcfor4digits(minTravelQty);
		}else{
			document.forms[0].laborQuantityhours[index].value = round_funcfor4digits(minTravelQty);
		}
		
	} // - End of if(!travelCheck)
	if(!laborCheck){
		alert("Quantity of Labor resource of "+requestType+" ticket can not be less than min value "+minLaborQty+".");
		if(index == null){
			document.forms[0].laborQuantityhours.value = round_funcfor4digits(minLaborQty);
		}else{
			document.forms[0].laborQuantityhours[index].value = round_funcfor4digits(minLaborQty);
		}
	} // -End of if(!laborCheck)
	
}// - End of ValidateTicketResources() function

function round_funcfor4digits(num)
	{
		if(isNaN(num)) num = 0;
		num = parseFloat(num);
		num = num;
		num = num.toFixed(4);
		return num;
	}
function ValidateTransitResource(index)
{
/**
		1. Check Transit Resource value shou;d not be greater than 1.
	*/
	var isTransit;
	var qty;
	if(index == null){ // - When Single Resource Present.
		isTransit = document.forms[0].laborChecktransit.checked;
		qty = document.forms[0].laborQuantityhours.value;
	}// -End of If
	else{ // - When mulipule Resource Present.
		isTransit = document.forms[0].laborChecktransit[index].checked;
		qty = document.forms[0].laborQuantityhours[index].value;
	} // -End of Else
	
	if(isTransit  && parseFloat(qty) > 1) {
		alert("Quantity of In-Transit resource of ticket can not be greater than 1.0000.");
		if(index == null){
			document.forms[0].laborQuantityhours.value = round_funcfor4digits(0.0000);
		}else{
			document.forms[0].laborQuantityhours[index].value = round_funcfor4digits(0.0000);
		}
	}
}
</script>