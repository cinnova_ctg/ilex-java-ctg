<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%
	String from = "";
	String jobid = "";
	//Separate Code Needed in the Individual jsp for a material level   
	String fromflag ="";
	String chkaddendum ="";
	String addendum_id ="";
	String Activity_Id ="";
	String refForJob ="";

	if( request.getAttribute( "Activity_Id" ) != null )
		Activity_Id = ( String )request.getAttribute( "Activity_Id" );
	 
	if( request.getAttribute( "chkaddendum" ) != null )
		chkaddendum = ( String )request.getAttribute( "chkaddendum" );	

	if( request.getAttribute( "addendum_id" ) != null )
		addendum_id = ( String )request.getAttribute( "addendum_id" );
	//End
	if( request.getAttribute( "from" ) != null )
	{
		if(request.getAttribute( "jobid" )!=null){
			jobid = request.getAttribute( "jobid" ).toString();
		}
		from = request.getAttribute( "from" ).toString();
		fromflag =  request.getAttribute( "from" ).toString();
	}

%>

<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.material.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	

<%@ include file="/NMenu.inc" %>
<%@ include file="/CommonResources.inc" %>	
<%@ include file="/MaterialMenu.inc" %>	
	
</head>





<body onload="leftAdjLayers();">
<html:form action = "/MaterialEditAction.do">

<html:hidden name="MaterialEditForm" property="materialid"/>
<html:hidden name="MaterialEditForm" property="materialcostlibid"/>
<html:hidden name="MaterialEditForm" property="prevquantity"/>
<html:hidden name="MaterialEditForm" property="addendum_id"/>
<html:hidden  name="MaterialEditForm" property = "activity_Id" />
<html:hidden  name="MaterialEditForm" property = "flag" />
<html:hidden  name="MaterialEditForm" property = "fromflag" />
<html:hidden  name="MaterialEditForm" property = "dashboardid" />
<html:hidden  name="MaterialEditForm" property = "ref" />

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
				 <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "MaterialEditAction.do?&ref=<%=chkaddendum%>&Material_Id=<%=Material_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MaterialEditForm" property = "msaId" />">
			          		<bean:write name = "MaterialEditForm" property = "msaName"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MaterialEditForm" property = "appendixId" />">
			   				<bean:write name = "MaterialEditForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MaterialEditForm" property = "jobId" />">
			       			<bean:write name = "MaterialEditForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=A&Activity_Id=<bean:write name = "MaterialEditForm" property = "activity_Id" />">
			       			<bean:write name = "MaterialEditForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
						    
	 				</tr>
	 				<tr>
					   <td colspan="7"><h2><bean:message bundle = "pm" key = "pm.material.edit.label"/><bean:write name = "MaterialEditForm" property = "materialName" /></h2></td>  	    
				    </tr>
	 				
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" >
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
<%@ include file="/MaterialMenuScript.inc" %>	
	<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
		 <tr>
	  		<td width="2" height="0"></td>
		  	 <td>
	  	    	<table border="0" cellspacing="1" cellpadding="1" width="450">
			  		<tr> 
						<td colspan = "3" class = "formCaption"><bean:message bundle = "pm" key = "msa.detail.general"/></td>
		  	    	</tr> 
		  	    	<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.tabular.name"/></td>
					    <td colspan = "2" class="colLight">
						    <bean:write name="MaterialEditForm" property = "materialName" />
						</td>
	  				</tr>
	  				<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.material.type"/></td>
					    <td colspan = "2" class="colLight">
						    <bean:write name="MaterialEditForm" property = "materialType" />
						</td>
	  				</tr>
	  				<tr> 
					    <td colspan = "1" class="colDark">CNS Part Number</td>
					    <td colspan = "2" class="colLight">
					    	<html:text name = "MaterialEditForm" size = "10"  readonly = "true" property = "cnspartnumber" styleClass = "Nreadonlytextnumbereven" />
						</td>
	  				</tr>
	  				<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.material.quantity"/><font class="red"> *</td>
					    <td colspan = "2" class="colLight">
						    <html:text name="MaterialEditForm" size = "5" maxlength = "50" property = "quantity" styleClass = "text" onchange="calarr();"/>
						    <html:hidden name = "MaterialEditForm" property = "minimumquantity" />   
						    <html:hidden name = "MaterialEditForm" property = "sellablequantity" />   
						</td>
	  				</tr>
	  				<tr> 
						<td colspan = "3" class = "formCaption"><bean:message bundle = "AM" key = "am.material.estimatedcost"/></td>
	  				</tr> 
	  				<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.material.estimatedunitcost"/></td>
					    <td colspan = "2" class="colLight">
					    	<html:text name = "MaterialEditForm" size = "10"  readonly = "true" property = "estimatedunitcost" styleClass = "Nreadonlytextnumbereven" />
						</td>
	  				</tr>
	  				</tr> 
	  				<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.material.estimatedtotalcost"/></td>
					    <td colspan = "2" class="colLight">
					    	<html:text name = "MaterialEditForm" size = "10"  readonly = "true" property = "estimatedtotalcost" styleClass = "Nreadonlytextnumbereven" />
						</td>
	  				</tr>
	  				<tr> 
						<td colspan = "3" class = "formCaption" height = "25">Cost-Price</td>
					</tr> 
					<tr> 
					    <td colspan = "1" class="colDark">Pro Forma Margin</td>
					    <td colspan = "2" class="colLight">
					    	<logic:equal name="MaterialEditForm" property="status" value="Draft">
					    		<html:text name="MaterialEditForm" size = "10" maxlength = "50" property = "proformamargin" styleClass = "text" onfocus="calarr();" onchange="calarr();" onblur="calarr();"/>
					    	</logic:equal>
					    	<logic:notEqual name="MaterialEditForm" property="status" value="Draft">
					    		<html:text name = "MaterialEditForm" size = "10"  readonly = "true" property = "proformamargin" styleClass = "Nreadonlytextnumbereven" />
					    	</logic:notEqual>
						</td>
	  				</tr>
					<tr> 
					    <td colspan = "1" class="colDark">
						    <bean:message bundle = "AM" key = "am.material.estimatedunitcost"/>
					    </td>
					    <td colspan = "2" class="colLight">
					    	<html:text name = "MaterialEditForm" size = "10"  readonly = "true" property = "priceunit" styleClass = "Nreadonlytextnumbereven" />
					    	<html:button property = "back" styleClass = "buttonverticalalign" onclick = "marginComputation();">Set</html:button>
						</td>
	  				</tr>
	  				</tr> 
	  				<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.material.priceextended"/></td>
					    <td colspan = "2" class="colLight">
						    <html:text name = "MaterialEditForm" size = "10"  readonly = "true" property = "priceextended" styleClass = "Nreadonlytextnumbereven" />
						</td>
	  				</tr>
	  				<tr> 
						<td colspan = "3" class = "formCaption" height = "25"><bean:message bundle = "AM" key = "am.material.manufacturer"/></td>
					</tr>
					<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.material.manufacturername"/></td>
					    <td colspan = "2" class="colLight">
					   		<bean:write name="MaterialEditForm" property = "manufacturername" />
						</td>
	  				</tr>
	  				<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.material.manufacturerpartnumber"/></td>
					    <td colspan = "2" class="colLight">
					    	<bean:write name="MaterialEditForm" property = "manufacturerpartnumber" />
						</td>
	  				</tr>
					<tr> 
					    <td colspan = "1" class="colDark"><bean:message bundle = "AM" key = "am.material.status"/></td>
					    <td colspan = "2" class="colLight">
					    	<bean:write name="MaterialEditForm" property = "status" />
						</td>
	  				</tr>
	  				<tr> 
						<td colspan = "3" class = "formCaption">Comments</td>
	 				</tr>
	 				<tr> 
						<td  class="colLight" colspan="3"><font class="Nmessage">&nbsp;&nbsp;No Comments.</font></td>
				    </tr>
				    <tr> 
	    				<td colspan = "1" class="colDark">Created</td>
						<td colspan = "2" class="colLight">
							<bean:write name="MaterialEditForm" property = "createdBy" />&nbsp;<bean:write name="MaterialEditForm" property = "createdDate" />
						</td>
				    </tr>
					<tr>
  						<td colspan = "1" class="colDark">Updated</td>
						<td colspan = "2" class="colLight">
							<bean:write name="MaterialEditForm" property = "changedBy" />&nbsp;<bean:write name="MaterialEditForm" property = "changedDate" />
						</td>
				   </tr>       
				   <tr>
					   <td class = "colLight" colspan="3" align="center">
							<html:submit property = "save" styleClass = "button_c" onclick = "return validate();">Submit</html:submit>&nbsp;
							<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>&nbsp;
							<html:button property = "back" styleClass = "button_c" onclick = "Backaction();">Back</html:button>
					   </td>
				   </tr>
	  				
				</table>
			 </td>
		</tr>
	</table>
				
</html:form>
</body>
</html:html>

<script>
function validate(){

		if( document.forms[0].quantity.value == "" )
		{
		  	alert( "<bean:message bundle = "AM" key = "am.material.quantitymandatory"/>" );
	  		document.forms[0].quantity.focus();
	  		return false;
		}
		
		if(document.forms[0].quantity.value == 0 ){
			calarr();
		}
		 
  		if( !isFloat( document.forms[0].quantity.value ) )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
 			document.forms[0].quantity.value = "";
 			document.forms[0].quantity.focus();
			return false;
 		}
 		
 		if( document.forms[0].proformamargin.value == "" )
  		{
  			document.forms[0].proformamargin.value = "0.0";
  		}
 
	  	if( !isFloat( document.forms[0].proformamargin.value ) )
	 	{
	 		alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
	 		document.forms[0].proformamargin.value = "";
	 		document.forms[0].proformamargin.focus();
			return false;
	 	}	
	document.forms[0].action="MaterialEditAction.do?Activity_Id=<bean:write name="MaterialEditForm" property="activity_Id"/>&resourceListType=M&ref=<bean:write name="MaterialEditForm" property="chkaddendum"/>&from=<bean:write name="MaterialEditForm" property="fromflag"/>&jobid=<bean:write name="MaterialEditForm" property="dashboardid"/>";
	return true;
}
function marginComputation()
{
	str = "MarginComputationAction.do?resourceId=<bean:write name="MaterialEditForm" property = "materialid" />&estUnitCost=<bean:write name="MaterialEditForm" property = "estimatedunitcost" />&position=1&size=0";	
	str = str.replace("#", "*!*");
	suppstrwin = window.open(str, '', 'left = 700 , top = 250 , width = 320, height = 240 , resizable = yes , scrollbars = yes')
	suppstrwin.focus();
}
function Backaction(){
	history.go(-1);
}

function calarr()
{
		if( ( document.forms[0].quantity.value == "" ) || ( document.forms[0].quantity.value == "null" ) )
		{ 	
			document.forms[0].quantity.value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantity.value ) ) 
		{ 
			if( parseFloat( document.forms[0].quantity.value ) <  parseFloat( document.forms[0].minimumquantity.value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].quantity.value = document.forms[0].minimumquantity.value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].quantity.value )  ) /  parseFloat( document.forms[0].sellablequantity.value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].quantity.value ) - (divisor * parseFloat( document.forms[0].sellablequantity.value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].quantity.value = (parseFloat( document.forms[0].sellablequantity.value )*(divisor + 1)).toFixed(4) ; 
			}
			document.forms[0].estimatedtotalcost.value = parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedunitcost.value );
			document.forms[0].estimatedtotalcost.value = round_func( document.forms[0].estimatedtotalcost.value );
			
		
			if( ( document.forms[0].proformamargin.value == "" ) || ( document.forms[0].proformamargin.value == "null" ) || ( document.forms[0].proformamargin.value >= "1" ) )
			{ 
				document.forms[0].proformamargin.value = "0.0";
			}
		
			if( isFloat( document.forms[0].proformamargin.value ) ) 
			{ 
				//alert(document.forms[0].proformamargin.value);
				document.forms[0].priceunit.value = parseFloat( document.forms[0].estimatedunitcost.value ) / ( 1 - parseFloat( document.forms[0].proformamargin.value ) );
				document.forms[0].priceunit.value = round_func( document.forms[0].priceunit.value );
			}
			
			if( isFloat( document.forms[0].quantity.value ) ) 
			{ 
				document.forms[0].priceextended.value = parseFloat( document.forms[0].priceunit.value ) * parseFloat( document.forms[0].quantity.value );
				document.forms[0].priceextended.value = round_func( document.forms[0].priceextended.value );
			}
		}
	
}
</script>
<script>
function ShowDiv()
{
	leftAdjLayers();
	document.getElementById("filter").style.visibility="visible";
	return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
}
</script>


