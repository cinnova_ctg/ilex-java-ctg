<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%
	String temp_str = null;
	String Id = ( String ) request.getAttribute( "Activity_Id" );
	int labor_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString());
%>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.labor.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>
<bean:define id = "checkfortext" name = "refresh" scope = "request"/>
<%@ include file = "/Menu.inc" %>

<%
	String backgroundclass = "texto";
	String backgroundhyperlinkclass = null;
	String displaycombo = null;
	String readonlyclass = null;
	String readonlynumberclass = null;
	String backnew = "";
	int i = 0;
	String comboclass = null;
	boolean csschooser = true;
	String schdayscheck = null;
	String valstr = null;
	String arrcal = null;
	String transarr = null;
	String newtransarr = null;

	String arrnetmedxcal = null;

	String from = "";
	String jobid = "";
	int colspan = 9;

	if( request.getAttribute( "from" ) != null )
	{
		from = request.getAttribute( "from" ).toString();
		if(request.getAttribute( "jobid" ) != null )
		jobid = request.getAttribute( "jobid" ).toString();
	}
	
%>




<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages( 'images/sales1b.gif' , 'images/services1b.gif' , 'images/about1b.gif' , 'images/cust-serv1b.gif' , 'images/offers1b.gif')">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  <tr> 
    <td><img src = "images/spacer.gif" width = "1" height = "1"></td>
  </tr>
</table>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  <tr>
    <td class = "toprow"> 
      <table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
        <tr align = "center"> 
           <% if( request.getAttribute( "from" ) != null ){ %>
          			<td width = "140" class = "toprow1"><a href = "MaterialUpdateAction.do?from=<%= from %>&fromPage=jobDBAddResrs&jobid=<%= jobid %>&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Material</a></td>
          			<td width = "150" class = "toprow1"><a href = "FreightUpdateAction.do?from=<%= from %>&fromPage=jobDBAddResrs&jobid=<%= jobid %>&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop" class = "toprow1">Freight</a></td>
          			<td width = "120" class = "toprow1"><a href = "TravelUpdateAction.do?from=<%= from %>&fromPage=jobDBAddResrs&jobid=<%= jobid %>&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Travel</a></td>
          			<td class = "toprow1" width = "400"><a href = "ActivityDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "backtoactivity" />&Activity_Id=<%= Id %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "drop"><bean:message bundle = "pm" key = "resource.tabular.back"/></a></td>		
          			<!--  <td class = "toprow1" width = "400">&nbsp;</td>	-->	
        
          	
          	<%}else{ %>
          			<td width = "140" class = "toprow1"><a href = "MaterialUpdateAction.do?addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Material</a></td>
          			<td width = "150" class = "toprow1"><a href = "FreightUpdateAction.do?addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop" class = "toprow1">Freight</a></td>
          			<td width = "120" class = "toprow1"><a href = "TravelUpdateAction.do?addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendum" />&Activity_Id=<%= Id %>" class = "drop">Travel</a></td>
          			<td class = "toprow1" width = "400"><a href = "ActivityDetailAction.do?addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "backtoactivity" />&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id" />" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "drop"><bean:message bundle = "pm" key = "resource.tabular.back"/></a></td>		
        
          	 <%} %>
          
          
        </tr>
      </table>
    </td>
  </tr>
</table>


<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
 <tr>
  	<td>
  		<table border = "0" cellspacing = "1" cellpadding = "1" width = "1500">
  		
		  		<logic:present name = "addflag" scope = "request">
		  		<tr><td>&nbsp;</td>
	    		<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "addflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.labor.add.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9003">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure4"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9005">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure5"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9006">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure6"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
				</logic:present>
		
				<logic:present name = "updateflag" scope = "request">
				<tr><td>&nbsp;</td>
	    		<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "updateflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.labor.update.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9007">
			    		<bean:message bundle = "AM" key = "am.labor.update.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "updateflag" value = "-9008">
			    		<bean:message bundle = "AM" key = "am.labor.update.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "updateflag" value = "-9009">
			    		<bean:message bundle = "AM" key = "am.labor.update.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9010">
			    		<bean:message bundle = "AM" key = "am.labor.update.failure4"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
				</logic:present>
				
				<logic:present name = "deleteflag" scope = "request">
				<tr><td>&nbsp;</td>
	    		<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "deleteflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.labor.delete.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.labor.delete.failure1"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.labor.delete.failure2"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.labor.delete.failure3"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
			    </logic:present>
			 	
			<tr> 
				<td>&nbsp; </td>
	    		<td class = "labeleboldhierrarchy" colspan = "13" height = "30">
	    			<bean:message bundle = "pm" key = "pm.labor.header"/>&nbsp;  
	   				<A href = "MSADetailAction.do?MSA_Id=<bean:write name = "LaborResourceForm" property = "msa_id" />"><bean:write name ="LaborResourceForm" property = "msaname"/></A>
					<bean:message bundle = "pm" key = "appendix.detail.arrow"/>&nbsp;
	   				<logic:equal name = "LaborResourceForm" property = "chkaddendum" value = "View">
						<A href = "AppendixDetailAction.do?Appendix_Id=<bean:write name = "LaborResourceForm" property = "appendix_id" />"><bean:write name ="LaborResourceForm" property = "appendixname"/></A>
						<bean:message bundle = "pm" key = "activity.tabular.job"/>&nbsp;
						<A href="JobDetailAction.do?ref=detailjob&Job_Id=<bean:write name = "LaborResourceForm" property = "jobid" />"><bean:write name ="LaborResourceForm" property = "jobname"/></A>
						<bean:message bundle = "pm" key = "pm.material.activity"/>&nbsp;
						<A href = "ActivityDetailAction.do?ref=<bean:write name = "LaborResourceForm" property = "backtoactivity" />&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id" />"><bean:write name ="activityname" scope = "request"/></A> 
					</logic:equal>
	   				<logic:notEqual name = "LaborResourceForm" property = "chkaddendum" value = "View">
						<A href = "AppendixHeader.do?function=view&appendixid=<bean:write name = "LaborResourceForm" property = "appendix_id" />"><bean:write name ="LaborResourceForm" property = "appendixname"/></A>
						<bean:message bundle = "pm" key = "activity.tabular.job"/>&nbsp;
						<A href="JobDashboardAction.do?jobid=<bean:write name = "LaborResourceForm" property = "jobid" />"><bean:write name ="LaborResourceForm" property = "jobname"/></A>
						<bean:message bundle = "pm" key = "pm.material.activity"/>&nbsp;
						<bean:write name ="activityname" scope = "request"/> 
					</logic:notEqual>
	    		</td>
			</tr>
  
	  		<tr> 
	    		<td rowspan = "2">&nbsp; </td>
	    		<td class = "tryb" rowspan = "2">
	    			<bean:message bundle = "AM" key = "am.labor.name"/>
	    		</td>
	   
				<td class = "tryb" rowspan = "2">
					<bean:message bundle = "AM" key = "am.labor.type"/>
				</td>
				
				<td class = "tryb" rowspan = "2">
					<bean:message bundle = "AM" key = "am.labor.cnspartnumber"/>
				</td>
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.hours"/>
			    </td>
			    
			    <td class = "tryb" colspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.estimatedcost"/>
			    </td>
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.proformamargin"/>
			    </td>
			    <td class = "tryb" colspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.price"/>
			    </td>
			    
			    <logic:present name="check_netmedx_appendix" >
			    <logic:equal name="check_netmedx_appendix" value="Y">
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.intransit"/>
			    </td>
			    </logic:equal>
			    </logic:present>
			    			    
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.status"/>
			    </td>
			      <td width = 50%></td>
			    
			</tr>
  
	  		<tr> 
	    		<td class = "tryb"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.labor.hourlybase"/>
	      			</div>
	    		</td>
	    
	    		<td class = "tryb"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.labor.estimatedtotalcost"/>
	      			</div>
	    		</td>
	    
	    
			    <td class = "tryb"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.labor.priceunit"/>
			      </div>
			    </td>
	    
			    <td class = "tryb"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.labor.priceextended"/>
			      </div>
			    </td>
			</tr>
			
<html:form action = "/LaborUpdateAction">
<html:hidden  property = "fromPage" />
<html:hidden  property = "checknetmedx" />
	<logic:present name = "laborresourcelist" scope = "request"> 
 		<logic:iterate id = "ms" name = "laborresourcelist">
			<bean:define id = "resourceId" name = "ms" property = "laborid" type = "java.lang.String"/>
			<bean:define id = "estUnitCost" name = "ms" property = "estimatedhourlybasecost" type = "java.lang.String"/>
		<%	
			temp_str = "marginComputation('"+resourceId+"', '"+estUnitCost+"', "+i+", "+labor_size+");"; 	
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				backgroundhyperlinkclass = "hyperodd";
				comboclass = "combooddhidden";
				readonlyclass = "readonlytextodd";
				csschooser = false;
				readonlynumberclass = "readonlytextnumberodd";
			}
	
			else
			{
				csschooser = true;	
				comboclass = "comboevenhidden";
				readonlyclass = "readonlytexteven";
				backgroundclass = "texte";
				backgroundhyperlinkclass = "hypereven";
				readonlynumberclass = "readonlytextnumbereven";
			}
		%>
			<bean:define id = "val" name = "ms" property = "laborid" type = "java.lang.String"/>
			<bean:define id = "flag" name = "ms" property = "flag" type = "java.lang.String"/>
			<tr>		
				
				<td class = "labeleboldwhite"> 
					<logic:equal name = "ms" property = "status" value = "Approved">
						<%--<html:checkbox property = "check" value = "<%= flag+val %>" disabled = "true"/>	--%>
						<html:multibox property = "check" disabled = "true"><bean:write  name="ms" property = "flag_laborid"/></html:multibox>
					</logic:equal>
				
					<logic:notEqual name = "ms" property = "status" value = "Approved">
						<%--<html:checkbox property = "check" value = "<%= flag+val %>" />	--%>
						<html:multibox property = "check" ><bean:write  name="ms" property = "flag_laborid"/></html:multibox>
					</logic:notEqual>	
	    		</td>
				
				<html:hidden name = "ms" property = "laborid" />
				<html:hidden name = "ms" property = "laborcostlibid" />
				
				<%  
					if( labor_size == 1 )
					{
						valstr = "javascript: document.forms[0].check.checked = true;updatetext(this,'');"; 
						displaycombo = "javascript: displaycombo(this,'')";
						schdayscheck = "javascript: document.forms[0].check.checked = true;";
						arrcal = "javascript: calarr( null );";	
						transarr="javascript: return caltransarr( null );";	
						arrnetmedxcal = "javascript: calnetmedxarr( null );";	
					}
					else if( labor_size > 1 )
					{
						valstr = "javascript: document.forms[0].check['"+i+"'].checked = true;updatetext( this ,'"+i+"' );"; 
						schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;";
						displaycombo = "javascript: displaycombo( this , '"+i+"' )";
						arrcal = "javascript: calarr('"+i+"');";
						transarr = "javascript: return caltransarr('"+i+"');";
						arrnetmedxcal = "javascript: calnetmedxarr( '"+i+"' );";	
						//transarr = "javascript: abc();";
					}
				%>
				
				<!-- for multiple resource select start -->
				<html:hidden name = "ms" property = "flag_laborid"/>
				<html:hidden name = "ms" property = "flag"/>
				<logic:equal name = "ms" property = "flag" value = "T">
				<td class = "<%= backgroundhyperlinkclass %>"> 
					<bean:write name = "ms" property = "laborname" />
				</td>
				</logic:equal>
				<logic:equal name = "ms" property = "flag" value = "P">
				<td class = "<%= backgroundhyperlinkclass %>"> 
					<% if( request.getAttribute( "from" ) != null )
					{%>
						<a href = "LaborDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendumdetail" />&Labor_Id=<bean:write name = "ms" property = "laborid"/>&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "laborname" /></a>
					<%}else{ %>
						<a href = "LaborDetailAction.do?addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendumdetail" />&Labor_Id=<bean:write name = "ms" property = "laborid"/>&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "laborname" /></a>
					<%} %>
				</td>
				</logic:equal>
				<!-- for multiple resource select end -->
				<!--  <td class = "<%= backgroundhyperlinkclass %>"> 
					<% if( request.getAttribute( "from" ) != null )
					{%>
						<a href = "LaborDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendumdetail" />&Labor_Id=<bean:write name = "ms" property = "laborid"/>&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "laborname" /></a>
					<%}else{ %>
						<a href = "LaborDetailAction.do?addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendumdetail" />&Labor_Id=<bean:write name = "ms" property = "laborid"/>&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "laborname" /></a>
					<%} %>
				</td>-->
				
				<html:hidden name = "ms" property = "laborname" />
				
				<td class = "<%= backgroundhyperlinkclass %>">
					<bean:write name = "ms" property = "labortype" />
					<html:hidden styleId = "labortype" name = "ms" property = "labortype"  />
				</td>
				
				<td class = "<%= backgroundhyperlinkclass %>">
					<bean:write name = "ms" property = "cnspartnumber" />
					<html:hidden styleId = "cnspartnumber" name = "ms"  property = "cnspartnumber" />
				</td>
			   
				<logic:equal name = "check_netmedx_appendix" value = "Y">
					<logic:equal name = "ms" property = "status" value = "Approved">
						<td class = "<%= backgroundclass %>">
							<html:text  name = "ms" property = "quantityhours" size = "6" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
						</td>
					</logic:equal>		
					
					<logic:notEqual name = "ms" property = "status" value = "Approved">
						<td class = "<%= backgroundclass %>">
							<html:text styleId = "<%= "quantityhours"+i%>" name = "ms"  size = "6" maxlength = "6" property = "quantityhours" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
						</td>
					</logic:notEqual>
				</logic:equal>
				
				<logic:notEqual name = "check_netmedx_appendix" value = "Y">
					<logic:equal name = "ms" property = "status" value = "Approved">
						<td class = "<%= backgroundclass %>">
							<html:text  name = "ms" property = "quantityhours" size = "6" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
						</td>
					</logic:equal>		
					
					<logic:notEqual name = "ms" property = "status" value = "Approved">
						<td class = "<%= backgroundclass %>">
							<html:text styleId = "<%= "quantityhours"+i%>" name = "ms"  size = "6" maxlength = "6" property = "quantityhours" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
						</td>
					</logic:notEqual>
				</logic:notEqual>
				
				<html:hidden name = "ms" property = "prevquantityhours" />
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "estimatedhourlybasecost" name = "ms" size = "6"  property = "estimatedhourlybasecost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td>
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "estimatedtotalcost" name = "ms" size = "10"  property = "estimatedtotalcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td> 
			   
				<logic:equal name = "check_netmedx_appendix" value = "Y">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
					</td>	
					
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "priceunit" name = "ms" size = "8"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" disabled="true"><bean:message bundle = "AM" key = "am.common.set"/></html:button>
					</td>
				</logic:equal>
				
				<logic:notEqual name = "check_netmedx_appendix" value = "Y">
					<logic:notEqual name = "ms" property = "status" value = "Approved">
						<td class = "<%= backgroundclass %>">
							<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
					    </td> 
					    
					    <td class = "<%= backgroundclass %>">
							<html:text styleId = "priceunit" name = "ms" size = "8"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" onclick = "<%= temp_str%>"><bean:message bundle = "AM" key = "am.common.set"/></html:button>
					    </td>
					</logic:notEqual> 
					
					<logic:equal name = "ms" property = "status" value = "Approved">
						<td class = "<%= backgroundclass %>">
							<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
						</td>

					     <td class = "<%= backgroundclass %>">
							<html:text styleId = "priceunit" name = "ms" size = "8"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" disabled="true">Set</html:button>
					    </td>
					</logic:equal>
				</logic:notEqual>
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "priceextended" name = "ms" size = "8"  property = "priceextended" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			    
			    <logic:present name="check_netmedx_appendix" >
			    <logic:equal name="check_netmedx_appendix" value="Y">
			    
			     <td class = "<%= backgroundclass %>">
					<html:multibox property="checktransit" onclick = "<%= transarr %>" ><bean:write  name="ms" property = "laborid"/></html:multibox>	
				</td>
				</logic:equal>
				</logic:present>
			   
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "ms" property = "status"/>		
				</td>
				<html:hidden name = "ms" property = "status"/>
				<html:hidden name = "ms" property = "transit"/>
				
				<html:hidden name = "ms" property = "sellablequantity" />
	   			<html:hidden name = "ms" property = "minimumquantity" />        	
	  		</tr>
  		
	   <% i++;  %>
		</logic:iterate>
	</logic:present>
	
				<%	
				  	if( labor_size != 0 )
				  	{
				  		if( labor_size % 2 == 1 )
				  		{
							backnew = "textenewelement";
				  			backgroundclass = "texte";
				  			i = 2;
							displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
							comboclass = "comboevenhidden";
							readonlyclass = "readonlytexteven";
							readonlynumberclass = "readonlytextnumbereven";
						}
				  		else
				  		{
							backnew = "textonewelement";
				  			backgroundclass = "texto";
				  			i = 1;
							displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
							comboclass = "combooddhidden";
							readonlyclass = "readonlytextodd";
							readonlynumberclass = "readonlytextnumberodd";
				  		}
				  	}
				  	else
					{
						backnew = "textonewelement";
						backgroundclass = "texto";
						i = 1;
						displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
						comboclass = "combooddhidden";
						readonlyclass = "readonlytextodd";
						readonlynumberclass = "readonlytextnumberodd";
					}
	   			%>
	   		
	   			<html:hidden property = "activity_Id" />
	   			<html:hidden property = "ref" /> 
	   			<html:hidden property = "newsellablequantity" />
	   			<html:hidden property = "newminimumquantity" />
	   			<html:hidden property = "backtoactivity" />
	   			<html:hidden property = "chkaddendum" />
	   			<html:hidden property = "chkaddendumdetail" />
	   			<html:hidden property = "addendum_id" />
	   			<html:hidden property = "fromflag" />
	   			<html:hidden property = "dashboardid" />
	   			<html:hidden property = "newtransit" />
	   			
	   			<% newtransarr="javascript: return caltransnew();";	 %>
	   			
	   		<tr> 
		    	<td class = "labeleboldwhite"> 
		    		<logic:equal name = "act_status"  value = "Approved" scope = "request">
		    			<html:checkbox property = "newlaborid" disabled = "true"/>
		    		</logic:equal>
		    		<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
		    			<html:checkbox property = "newlaborid" />
		    		</logic:notEqual>
		   		</td>
		   		
		   		<td class = "<%= backnew %>">
		   	 		<logic:equal name = "act_status"  value = "Approved" scope = "request">
		   	 			<html:text property = "newlaborname" size = "20" styleClass = "<%= readonlyclass %>" readonly = "true" />
			   			<html:hidden property = "newlabornamecombo" />
		   	 		</logic:equal>
		   	 		
		   	 		<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
			   	 		<html:text styleId = "newlaborname" property = "newlaborname" size = "20" styleClass = "textbox" onclick ="javascript:opensearchwindow();" readonly="true" onmouseover = "highlightTextField( this );" onmouseout = "unHighlightTextField( this );" />
			   			<html:select styleId = "newlabornamecombo" property = "newlabornamecombo" size = "1" styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"    onchange = "document.forms[0].newlaborid.checked = true; updatetext( this , null ); return refresh();">
					    	<html:optionsCollection  property = "laborlist" value = "value" label = "label"/> 
				   		</html:select>
		   	 		</logic:notEqual>
		   		</td>
		   		
		   		<td class = "<%= backgroundclass %>">
		   	 		<bean:write  name = "LaborResourceForm" property = "newlabortype" />
		   	 		<html:hidden styleId = "newlabortype" property = "newlabortype" />
		   		</td>
		   		
			    <td class = "<%= backgroundclass %>">
					<bean:write  name = "LaborResourceForm" property = "newcnspartnumber" />
					<html:hidden styleId = "newcnspartnumber"  property = "newcnspartnumber" />
				</td>
				
				<td class = "<%= backgroundclass %>">
					<logic:equal name = "act_status"  value = "Approved" scope = "request">
						<html:text size = "6" maxlength = "6" property = "newquantityhours" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
					</logic:equal>
					
					<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
						<% if( checkfortext.equals( "true" ) ) { %>	
							<html:text size = "6" maxlength = "6" property = "newquantityhours" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
						<%}else { %>
						<html:text styleId = "newquantityhours"  size = "6" maxlength = "6" property = "newquantityhours" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "document.forms[0].newlaborid.checked = true;" onblur = "calnew();" />
						<%} %>
					</logic:notEqual>
				</td>
				
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "newestimatedhourlybasecost"  size = "6"  property = "newestimatedhourlybasecost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td>
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "newestimatedtotalcost" size = "10" maxlength = "9"  property = "newestimatedtotalcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td> 
			   
				<td class = "<%= backgroundclass %>">
					<logic:equal name = "act_status"  value = "Approved" scope = "request">
						<html:text size = "6" maxlength = "5" property = "newproformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
					</logic:equal>
					
					<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
						<% if( checkfortext.equals( "true" ) ) { %>	
							<html:text size = "6" maxlength = "5" property = "newproformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
			   			<%}else { %>
			   				<html:text styleId = "newproformamargin" size = "6" maxlength = "5" property = "newproformamargin" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "document.forms[0].newlaborid.checked = true;" onblur = "calnew();"/>
			   			<%} %>
			   		</logic:notEqual>
			    </td> 
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "newpriceunit" size = "10" maxlength = "9"  property = "newpriceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "newpriceextended"  size = "10" maxlength = "9"  property = "newpriceextended" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			    
			    <logic:present name="check_netmedx_appendix" >
			    <logic:equal name="check_netmedx_appendix" value="Y">
			    
			    <bean:define id="newtransit" name="LaborResourceForm" property="newtransit" type = "java.lang.String"/>
			     <td class = "<%= backgroundclass %>">
					<html:checkbox property = "newchecktransit" value = "<%= newtransit %>" onclick = "<%= newtransarr %>"/>		
				</td>
				</logic:equal>
				</logic:present>
			   
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "LaborResourceForm" property = "newstatus"/>		
				</td>        	
	  		</tr>
	  		
	  		
	  		<tr>	
	  			<td >&nbsp; </td>
	  			<logic:present name="check_netmedx_appendix" >
			    <logic:equal name="check_netmedx_appendix" value="Y">
				<% colspan=10;%>
				</logic:equal>
				</logic:present>
			  	<td colspan="<%=colspan%>" class = "buttonrow">
			  		<html:submit property = "save" styleClass = "button" onclick = "return validate();"><bean:message bundle = "AM" key = "am.labor.submit"/></html:submit>
					<html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "AM" key = "am.labor.sort"/></html:button>
					
					<!-- Start :Added By Amit for Mass delete and Approve ,on 14.08.2006-->
					<logic:equal name = "LaborResourceForm" property = "chkaddendum" value = "View">
					<html:button property = "massDelete" styleClass = "button" onclick = "return del();"><bean:message bundle = "AM" key = "am.material.massDelete"/></html:button>

					<html:button property = "approve" styleClass = "button" onclick = "return massApprove();"><bean:message bundle = "AM" key = "am.material.massApprove"/></html:button>
					</logic:equal>
					<!-- End :Added By Amit for Mass delete and Approve,on 14.08.2006-->
					<% if( request.getAttribute( "from" ) != null ){ 
						if( request.getAttribute( "from" ).equals( "jobdashboard" ) )
						{
					%> 

						<html:button property = "back"  styleClass="button" onclick = "return BackTrack();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>

					<!--<html:button property = "back"  styleClass="button" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>-->
						
					<% }if( request.getAttribute( "from" ).equals( "jobdashboard_resource" ) )
					{ %>
					<!-- 	<html:button property = "back"  styleClass="button" onclick = "return Backactionjobdash();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>  -->
					<%}%>
					
					<% }%>
					 
				
				</td>
				
				<td class = "buttonrow" >
					<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "AM" key = "am.labor.cancel"/></html:reset>
				</td>
			</tr>
	  		
	  		
			    
		</table>
	</td>
</tr>
</table>
  
</html:form>


<script>
function marginComputation(resourceId, estUnitCost, position, size)
{
	str = "MarginComputationAction.do?resourceId="+resourceId+"&estUnitCost="+estUnitCost+"&position="+position+"&size="+size;	
	suppstrwin = window.open(str, '', 'left = 300 , top = 250 , width = 320, height = 240 , resizable = yes , scrollbars = yes')
	suppstrwin.focus();
}

function opensearchwindow()
{
	str = "OpenSearchWindowTempAction.do?ref=<bean:write name = "LaborResourceForm" property = "ref" />&type=Labor&from=pm&activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id" />&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id" />&checknetmedx="+document.forms[0].checknetmedx.value;

	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}

function sortwindow()
{
	str = 'SortAction.do?addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id" />&Type=pm_resL&Activitylibrary_Id=<%= Id %>&ref=<bean:write name = "LaborResourceForm" property = "chkaddendum" />';
	
	window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
}

function validate()
{
		
	var submitflag = 'false';
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newlaborid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.labor.selectresource"/>" );
				return false;
			}
		}
		else
		{
			
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	
		  	if( submitflag == 'false' && !( document.forms[0].newlaborid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.labor.selectresource"/>" );
				return false;
		  	}
		}
	}
	
	else
	{
		if( !document.forms[0].newlaborid.checked )
		{
			alert( "<bean:message bundle = "AM" key = "am.labor.newlabor"/>" );
			return false;
		}
	}
	
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			if( document.forms[0].check.checked ) 
	  		{
	  			if( document.forms[0].quantityhours.value == "" )
	  			{
	  				document.forms[0].quantityhours[i].value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].quantityhours.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
		 			document.forms[0].quantityhours.value = "";
		 			document.forms[0].quantityhours.focus();
					return false;
		 		}
		 		
		 		if( document.forms[0].proformamargin.value == "" )
	  			{
	  				document.forms[0].proformamargin.value = "0.0";
	  			}
	 
		  		if(document.forms[0].checknetmedx.value!=null && document.forms[0].checknetmedx.value=="Y")
				{
			  		
			 	}
			 	else
			 	{
			 		if( !isFloat( document.forms[0].proformamargin.value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
			 			document.forms[0].proformamargin.value = "";
			 			document.forms[0].proformamargin.focus();
						return false;
			 		}
			 	}	
	  		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
			  		if( document.forms[0].quantityhours[i].value == "" )
		  			{
		  				document.forms[0].quantityhours[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].quantityhours[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
			 			document.forms[0].quantityhours[i].value = "";
			 			document.forms[0].quantityhours[i].focus();
						return false;
			 		}
			 		
			 		if( document.forms[0].proformamargin[i].value == "" )
		  			{
		  				document.forms[0].proformamargin[i].value = "0.0";
		  			}
		 
			  		
			  		if(document.forms[0].checknetmedx.value!=null && document.forms[0].checknetmedx.value=="Y")
					{
			  		
			 		}
			 		else
			 		{
				  		if( !isFloat( document.forms[0].proformamargin[i].value ) )
				 		{
				 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
				 			document.forms[0].proformamargin[i].value = "";
				 			document.forms[0].proformamargin[i].focus();
							return false;
				 		}
				 	}	
		  		}
		  	}
		}
	}
	
	
	if( document.forms[0].newlaborid.checked ) 
	{
		/*if( document.forms[0].newlabornamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.laborenter"/>" );
 			document.forms[0].newlaborname.focus();
 			return false;
 		}*/
 		
 		if( document.forms[0].newlaborname.value == "" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.laborenter"/>" );
 			//document.forms[0].newlaborname.focus();
 			return false;
 		}
		
		
		
		
		if( document.forms[0].newquantityhours.value == "" )
		{
		  	alert( "<bean:message bundle = "AM" key = "am.material.quantitymandatory"/>" );
	  		document.forms[0].newquantityhours.focus();
	  		return false;
		}
		 
  		if( !isFloat( document.forms[0].newquantityhours.value ) )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
 			document.forms[0].newquantityhours.value = "";
 			document.forms[0].newquantityhours.focus();
			return false;
 		}
			 		
	 	if( document.forms[0].newproformamargin.value == "" )
  		{
  			document.forms[0].newproformamargin.value = "0.0";
  		}
 
	  	if( !isFloat( document.forms[0].newproformamargin.value ) )
	 	{
	 		alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
	 		document.forms[0].newproformamargin.value = "";
	 		document.forms[0].newproformamargin[i].focus();
			return false;
	 	}	
	}
	
	
	
	document.forms[0].ref.value = "Submit";
	return true;
}

function refresh()
{
	document.forms[0].ref.value = "Refresh";
	document.forms[0].submit();
	return true;
}



function calarr( i )
{
	if(document.forms[0].checknetmedx.value!=null && document.forms[0].checknetmedx.value=="Y")
	{
		if( <%= labor_size %> == 1 )
		{
			if( ( document.forms[0].quantityhours.value == "" ) || ( document.forms[0].quantityhours.value == "null" ) )
			{ 
				document.forms[0].quantityhours.value = "0.0";
			}
			
			if( isFloat( document.forms[0].quantityhours.value ) ) 
			{ 
							
				document.forms[0].estimatedtotalcost.value = parseFloat( document.forms[0].quantityhours.value ) * parseFloat( document.forms[0].estimatedhourlybasecost.value );
				document.forms[0].estimatedtotalcost.value = round_func( document.forms[0].estimatedtotalcost.value );
			
				if( ( document.forms[0].proformamargin.value == "" ) || ( document.forms[0].proformamargin.value == "null" ) || ( document.forms[0].proformamargin.value >= "1" ) )
				{ 
					document.forms[0].proformamargin.value = "0.0";
				}
			
				if( isFloat( document.forms[0].proformamargin.value ) ) 
				{ 
					document.forms[0].priceunit.value = round_func( document.forms[0].priceunit.value );
				}
				
				if( isFloat( document.forms[0].quantityhours.value ) ) 
				{ 
					document.forms[0].priceextended.value = parseFloat( document.forms[0].priceunit.value ) * parseFloat( document.forms[0].quantityhours.value );
					document.forms[0].priceextended.value = round_func( document.forms[0].priceextended.value );
				}
			}
		}
		
		else
		{
			if( ( document.forms[0].quantityhours[i].value == "" ) || ( document.forms[0].quantityhours[i].value == "null" ) )
			{ 
				document.forms[0].quantityhours[i].value = "0.0";
			}
			
			if( isFloat( document.forms[0].quantityhours[i].value ) ) 
			{ 
				document.forms[0].estimatedtotalcost[i].value = parseFloat( document.forms[0].quantityhours[i].value ) * parseFloat( document.forms[0].estimatedhourlybasecost[i].value );
				document.forms[0].estimatedtotalcost[i].value = round_func( document.forms[0].estimatedtotalcost[i].value );
				
				if( ( document.forms[0].proformamargin[i].value == "" ) || ( document.forms[0].proformamargin[i].value == "null" ) || ( document.forms[0].proformamargin[i].value >= "1" ) )
				{ 
					document.forms[0].proformamargin[i].value = "0.0";
				}
			
				if( isFloat( document.forms[0].proformamargin[i].value ) ) 
				{ 
					document.forms[0].priceunit[i].value = round_func( document.forms[0].priceunit[i].value );
				}
				
				if( isFloat( document.forms[0].quantityhours[i].value ) ) 
				{ 
					document.forms[0].priceextended[i].value = parseFloat( document.forms[0].priceunit[i].value ) * parseFloat( document.forms[0].quantityhours[i].value );
					document.forms[0].priceextended[i].value = round_func( document.forms[0].priceextended[i].value );
				}
			}
		}
	}
	else
	{
	
		if( <%= labor_size %> == 1 )
		{
			if( ( document.forms[0].quantityhours.value == "" ) || ( document.forms[0].quantityhours.value == "null" ) )
			{ 
				document.forms[0].quantityhours.value = "0.0";
			}
			
			if( isFloat( document.forms[0].quantityhours.value ) ) 
			{ 
				if( parseFloat( document.forms[0].quantityhours.value ) <  parseFloat( document.forms[0].minimumquantity.value ) )
				{
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
					document.forms[0].quantityhours.value = document.forms[0].minimumquantity.value;
				}
				
				var divisor =  ( parseFloat( document.forms[0].quantityhours.value )  ) /  parseFloat( document.forms[0].sellablequantity.value );
				divisor = parseInt(divisor.toFixed(4));
				var remainder = parseFloat( document.forms[0].quantityhours.value ) - (divisor * parseFloat( document.forms[0].sellablequantity.value ) );
				var zero=0;
				
				if(  parseInt(remainder.toFixed(4)*10000) == zero )
				{
				}
				else
				{
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
					document.forms[0].quantityhours.value = (parseFloat( document.forms[0].sellablequantity.value )*(divisor + 1)).toFixed(4) ; 
				}
				
				document.forms[0].estimatedtotalcost.value = parseFloat( document.forms[0].quantityhours.value ) * parseFloat( document.forms[0].estimatedhourlybasecost.value );
				document.forms[0].estimatedtotalcost.value = round_func( document.forms[0].estimatedtotalcost.value );
			
				if( ( document.forms[0].proformamargin.value == "" ) || ( document.forms[0].proformamargin.value == "null" ) || ( document.forms[0].proformamargin.value >= "1" ) )
				{ 
					document.forms[0].proformamargin.value = "0.0";
				}
			
				if( isFloat( document.forms[0].proformamargin.value ) ) 
				{ 
					document.forms[0].priceunit.value = parseFloat( document.forms[0].estimatedhourlybasecost.value ) / ( 1 - parseFloat( document.forms[0].proformamargin.value ) );
					document.forms[0].priceunit.value = round_func( document.forms[0].priceunit.value );
				}
				
				if( isFloat( document.forms[0].quantityhours.value ) ) 
				{ 
					document.forms[0].priceextended.value = parseFloat( document.forms[0].priceunit.value ) * parseFloat( document.forms[0].quantityhours.value );
					document.forms[0].priceextended.value = round_func( document.forms[0].priceextended.value );
				}
			}
		}
		
		else
		{
			if( ( document.forms[0].quantityhours[i].value == "" ) || ( document.forms[0].quantityhours[i].value == "null" ) )
			{ 
				document.forms[0].quantityhours[i].value = "0.0";
			}
			
			if( isFloat( document.forms[0].quantityhours[i].value ) ) 
			{ 
				if( parseFloat( document.forms[0].quantityhours[i].value ) <  parseFloat( document.forms[0].minimumquantity[i].value ) )
				{
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
					document.forms[0].quantityhours[i].value = document.forms[0].minimumquantity[i].value;
				}
				
				var divisor =  ( parseFloat( document.forms[0].quantityhours[i].value )  ) /  parseFloat( document.forms[0].sellablequantity[i].value );
				divisor = parseInt(divisor.toFixed(4));
				var remainder = parseFloat( document.forms[0].quantityhours[i].value ) - (divisor * parseFloat( document.forms[0].sellablequantity[i].value ) );
				var zero=0;
				if(  parseInt(remainder.toFixed(4)*10000) == zero )
				{
				}
				else
				{
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
					document.forms[0].quantityhours[i].value = (parseFloat( document.forms[0].sellablequantity[i].value )*(divisor + 1)).toFixed(4) ; 
				}
				
				/*var remainder = ( parseFloat( document.forms[0].quantityhours[i].value ) - parseFloat( document.forms[0].minimumquantity[i].value ) ) %  parseFloat( document.forms[0].sellablequantity[i].value );
				
				if(  remainder == 0 )
				{
				}
				else
				{
					var temp = ( parseFloat( document.forms[0].quantityhours[i].value ) - parseFloat( document.forms[0].minimumquantity[i].value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].sellablequantity[i].value );
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
					document.forms[0].quantityhours[i].value = parseFloat( document.forms[0].minimumquantity[i].value ) + ( parseFloat( document.forms[0].sellablequantity[i].value ) * ( parseFloat( temp ) + 1 ) ); 
				}*/
				
				document.forms[0].estimatedtotalcost[i].value = parseFloat( document.forms[0].quantityhours[i].value ) * parseFloat( document.forms[0].estimatedhourlybasecost[i].value );
				document.forms[0].estimatedtotalcost[i].value = round_func( document.forms[0].estimatedtotalcost[i].value );
				
				if( ( document.forms[0].proformamargin[i].value == "" ) || ( document.forms[0].proformamargin[i].value == "null" ) || ( document.forms[0].proformamargin[i].value >= "1" ) )
				{ 
					document.forms[0].proformamargin[i].value = "0.0";
				}
			
				if( isFloat( document.forms[0].proformamargin[i].value ) ) 
				{ 
					document.forms[0].priceunit[i].value = parseFloat( document.forms[0].estimatedhourlybasecost[i].value ) / ( 1 - parseFloat( document.forms[0].proformamargin[i].value ) );
					document.forms[0].priceunit[i].value = round_func( document.forms[0].priceunit[i].value );
				}
				
				if( isFloat( document.forms[0].quantityhours[i].value ) ) 
				{ 
					document.forms[0].priceextended[i].value = parseFloat( document.forms[0].priceunit[i].value ) * parseFloat( document.forms[0].quantityhours[i].value );
					document.forms[0].priceextended[i].value = round_func( document.forms[0].priceextended[i].value );
				}
			}
		}
	}
	
}
function caltransnew()
{
		
		if(document.forms[0].newtransit.value=='N')
		{
			document.forms[0].newtransit.value='Y';
		}
		else
		{
			document.forms[0].newtransit.value='N';
		}
	
}


function caltransarr( i )
{
	
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			
		}
		else
		{
		  for( var j = 0; j < <%= labor_size %>; j++ )
		     {
	  			if(i!=j)
	  			{
			  					  		
			  		if(document.forms[0].transit[i].value == 'N') {
				  		if((document.forms[0].laborname[i].value == document.forms[0].laborname[j].value) && (document.forms[0].transit[j].value == 'Y')) 
				  		{
				  		    alert( "<bean:message bundle = "AM" key = "am.labor.sameresourcecannot"/>" );
				  			return false;	
				  		}
				  		
			  		}
			  		
			  		if(document.forms[0].transit[i].value == 'Y') {
				  		if((document.forms[0].laborname[i].value == document.forms[0].laborname[j].value) && (document.forms[0].transit[j].value == 'N')) 
				  		{
				  		    alert( "<bean:message bundle = "AM" key = "am.labor.sameresourcecannot"/>" );
				  			return false;	
				  		}
			  		}
			  	}
		  	}
		  
		}
	}
	
	
	
	if( i == null)
	{
		
		
		if(document.forms[0].transit.value=='N')
		{
			document.forms[0].transit.value='Y';
		}
		else
		{
			document.forms[0].transit.value='N';
		}
	}
	
	else
	{
		
		if(document.forms[0].transit[i].value=='N')
		{
			
			document.forms[0].transit[i].value='Y';
			
		}
		else
		{
			
			document.forms[0].transit[i].value='N';
			
		}	
	}
	
}





function calnew()
{
	alert("inside calnew");
	if( document.forms[0].newlabornamecombo.value == '0' )
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.laborenter"/>" ); 
		return false;
	}
	
	alert("document.forms[0].newquantityhours.value"+document.forms[0].newquantityhours.value);
	if( ( document.forms[0].newquantityhours.value == "" ) || ( document.forms[0].newquantityhours.value == "null" ) )
	{ 
		document.forms[0].newquantityhours.value = "0.0";
	}
	
	if( isFloat( document.forms[0].newquantityhours.value ) ) 
	{ 
		if( parseFloat( document.forms[0].newquantityhours.value ) <  parseFloat( document.forms[0].newminimumquantity.value ) )
		{
			alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
			document.forms[0].newquantityhours.value = document.forms[0].newminimumquantity.value;
		}
		
		
		var divisor =  ( parseFloat( document.forms[0].newquantityhours.value )  ) /  parseFloat( document.forms[0].newsellablequantity.value );
		divisor = parseInt(divisor);
		var remainder = parseFloat( document.forms[0].newquantityhours.value ) - (divisor * parseFloat( document.forms[0].newsellablequantity.value ) );
		if( document.forms[0].newsellablequantity.value < 1 )
		{	
			remainder=0;
		}
				
		var zero=0;
		if(  remainder.toFixed(4) == zero.toFixed(4) )
		{
		}
		else
		{
			alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
			document.forms[0].newquantityhours.value = parseFloat( document.forms[0].newsellablequantity.value ) * (divisor + 1); 
		}
	
		
		/*var remainder = 0;
		
		if( document.forms[0].newsellablequantity.value < 1 )
		{	
		}
		else
		{
			remainder = ( parseFloat( document.forms[0].newquantityhours.value ) - parseFloat( document.forms[0].newminimumquantity.value ) ) %  parseFloat( document.forms[0].newsellablequantity.value );
		}
		
		if(  remainder == 0 )
		{
		}
		else
		{
			var temp = ( parseFloat( document.forms[0].newquantityhours.value ) - parseFloat( document.forms[0].newminimumquantity.value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].newsellablequantity.value );
			alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
			document.forms[0].newquantityhours.value = parseFloat( document.forms[0].newminimumquantity.value ) + ( parseFloat( document.forms[0].newsellablequantity.value ) * ( parseFloat( temp ) + 1 ) ); 
		}*/
		
		document.forms[0].newestimatedtotalcost.value = parseFloat( document.forms[0].newquantityhours.value ) * parseFloat( document.forms[0].newestimatedhourlybasecost.value );
		document.forms[0].newestimatedtotalcost.value = round_func( document.forms[0].newestimatedtotalcost.value );
		alert("document.forms[0].newestimatedtotalcost.value"+document.forms[0].newestimatedtotalcost.value);
		
		if( ( document.forms[0].newproformamargin.value == "" ) || ( document.forms[0].newproformamargin.value == "null" ) || ( document.forms[0].newproformamargin.value >= "1" ) )
		{ 
			document.forms[0].newproformamargin.value = "0.0";
		}
	
		if( isFloat( document.forms[0].newproformamargin.value ) ) 
		{ 
			document.forms[0].newpriceunit.value = parseFloat( document.forms[0].newestimatedhourlybasecost.value ) / ( 1 - parseFloat( document.forms[0].newproformamargin.value ) );
			document.forms[0].newpriceunit.value = round_func( document.forms[0].newpriceunit.value );
		}
		
		if( isFloat( document.forms[0].newquantityhours.value ) ) 
		{ 
			document.forms[0].newpriceextended.value = parseFloat( document.forms[0].newpriceunit.value ) * parseFloat( document.forms[0].newquantityhours.value );
			document.forms[0].newpriceextended.value = round_func( document.forms[0].newpriceextended.value );
		}
	}
}




</script>

</body>

<script>
function Backactionjob()
{
	<% if(  request.getAttribute( "jobid" ) != null ){%>
		document.forms[0].action = "ActivityDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "backtoactivity" />&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id" />";
	<%} %>
	document.forms[0].submit();
	return true;
}

function Backactionjobdash()
{
	<% if(  request.getAttribute( "jobid" ) != null ){%>
		document.forms[0].action = "JobDashboardAction.do?jobid=<%= jobid %>";
	<%} %>
	document.forms[0].submit();
	return true;
}

function BackTrack()
{

	<% if(  request.getAttribute( "jobid" ) != null ){%>
		document.forms[0].action = "JobDashboardAction.do?from=<%= from %>&jobid=<%= jobid %>&appendix_id=<bean:write name = "LaborResourceForm" property = "appendix_id" />&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=<bean:write name = "LaborResourceForm" property = "backtoactivity" />&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id" />"
	<%} %>
	document.forms[0].submit();
	return true;
}


function calnetmedxarr( i )
{
	if( <%= labor_size %> == 1 )
	{
		if( ( document.forms[0].quantityhours.value == "" ) || ( document.forms[0].quantityhours.value == "null" ) )
		{ 
			document.forms[0].quantityhours.value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantityhours.value ) ) 
		{ 
			document.forms[0].estimatedtotalcost.value = parseFloat( document.forms[0].quantityhours.value ) * parseFloat( document.forms[0].estimatedhourlybasecost.value );
			document.forms[0].estimatedtotalcost.value = round_func( document.forms[0].estimatedtotalcost.value );
			
			document.forms[0].priceextended.value = parseFloat( document.forms[0].priceunit.value ) * parseFloat( document.forms[0].quantityhours.value );
			document.forms[0].priceextended.value = round_func( document.forms[0].priceextended.value );
			
			document.forms[0].proformamargin.value = 1 - round_func( parseFloat( document.forms[0].estimatedhourlybasecost.value ) / parseFloat( document.forms[0].priceunit.value ) );
			
		}
	}
	
	else
	{
		if( ( document.forms[0].quantityhours[i].value == "" ) || ( document.forms[0].quantityhours[i].value == "null" ) )
		{ 
			document.forms[0].quantityhours[i].value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantityhours[i].value ) ) 
		{ 
			document.forms[0].estimatedtotalcost[i].value = parseFloat( document.forms[0].quantityhours[i].value ) * parseFloat( document.forms[0].estimatedhourlybasecost[i].value );
			document.forms[0].estimatedtotalcost[i].value = round_func( document.forms[0].estimatedtotalcost[i].value );
			
			document.forms[0].priceextended[i].value = parseFloat( document.forms[0].priceunit[i].value ) * parseFloat( document.forms[0].quantityhours[i].value );
			document.forms[0].priceextended[i].value = round_func( document.forms[0].priceextended[i].value );
			
			document.forms[0].proformamargin[i].value = 1 - round_func( parseFloat( document.forms[0].estimatedhourlybasecost[i].value ) / parseFloat( document.forms[0].priceunit[i].value ) );
			
		}
	}
	
}


//Start:Added By Amit For Mass Approve
function massApprove()
{
	var submitflag = 'false';
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newlaborid.checked ) )
			{
				
				alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
			
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newlaborid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
	}
	
	if( document.forms[0].newlaborid.checked ) 
  	{
	  	
	  	if( document.forms[0].newlabornamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
 			document.forms[0].newlaborname.focus();
 			return false;
 		}
	
  	}
  	else
  	{
  	}

	if(submitflag =='true')
	{

		document.forms[0].action = "LaborUpdateAction.do?ref=massApprove&Activitylibrary_Id=<%= Id %>&jobid=<%= jobid %>";			

		document.forms[0].submit();
		return true;
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
		return false;
	}
}
//End :For Mass Approve

//Start :Added by Amit For Mass Delete


function del()
{

	
	var submitflag = 'false';
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newmaterialid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newlaborid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
	}
	
	if( document.forms[0].newlaborid.checked ) 
  	{
	  	
	  	if( document.forms[0].newlabornamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.laborenter"/>" );
 			document.forms[0].newlaborname.focus();
 			return false;
 		}
	
  	}
  	else
  	{
  	}

	if(submitflag =='true')
	{
	
		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "LaborUpdateAction.do?type=<bean:write name = "LaborResourceForm" property = "chkaddendum"/>&addendum_id=<bean:write name = "LaborResourceForm" property = "addendum_id"/>&ref=massDelete&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id"/>";

			document.forms[0].submit();
			return true;	
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
		return false;
	}
	
}

//End : For Mass delete

</script>



</html:html>