<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<% int sow_size =( int ) Integer.parseInt( request.getAttribute( "sowsize" ).toString()); 
	int assumption_size =( int ) Integer.parseInt( request.getAttribute( "assumptionsize" ).toString()); 

%> 


<%
	String Travel_Id = "";
	String Activity_Id = "";
	String jobid = "";
	String fromflag = "";
	String from = "";
	String refForJob = "";

	if( request.getAttribute( "Travel_Id" ) != null )
	{
		Travel_Id = ( String ) request.getAttribute( "Travel_Id" );
	}

	if( request.getAttribute( "Activity_Id" ) != null )
	{	
		Activity_Id = ( String )request.getAttribute( "Activity_Id" );
	
	}

	if( request.getAttribute( "from" ) != null )
	{
		jobid = request.getAttribute( "jobid" ).toString();
		from = request.getAttribute( "from" ).toString();
		System.out.print("from"+ from);
	}

%>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.travel.detail.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/content.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>


<%@ include file = "/NMenu.inc" %> 
<%@ include file = "/CommonResources.inc" %>
</head>


<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')">
<html:form action = "/TravelDetailAction">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  		<tr>
    	<td valign="top" width = "100%">
    		<table cellpadding="0" cellspacing="0" border="0" width = "100%">
    		<c:if test="${requestScope.from ne 'jobdashboard'}">
	        	<tr>
		          <!-- Sub Navigation goes into this td -->
		          	  <%if( from != null && from != ""){%>
		          	
			          <%}else{ %>
			          <!-- from project manager:start -->
						          <td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "TravelResourceDetailForm" property = "activity_Id" />&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>All</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=L&Activity_Id=<bean:write name = "TravelResourceDetailForm" property = "activity_Id" />&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Labor</center></a></td>
 								  <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=M&Activity_Id=<bean:write name = "TravelResourceDetailForm" property = "activity_Id" />&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Material</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=T&Activity_Id=<bean:write name = "TravelResourceDetailForm" property = "activity_Id" />&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Travel</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=F&Activity_Id=<bean:write name = "TravelResourceDetailForm" property = "activity_Id" />&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Freight</center></a></td>
					          <!-- from project manager:start -->
			          <%} %>
			          <td  id="pop5" width="600" class="headerrow" height="19">&nbsp;</td>
			      <!-- End of Sub Navigation -->
		        </tr>
	        	
				<tr>
					<td background="images/content_head_04.jpg" height="21" colspan="23">
					<%if( from != null && from != ""){%>
		             	<div id="breadCrumb"><A class ="bgNone" href="AppendixHeader.do?function=view&fromPage=appendix&viewjobtype=ION&&msaId=<bean:write name = "TravelResourceDetailForm" property = "msa_id" />&appendixid=<bean:write name = "TravelResourceDetailForm" property = "appendix_id" />"><bean:write name = "TravelResourceDetailForm" property = "appendixname"/></A>
		          				<a href="JobDashboardAction.do?function=view&jobid=<bean:write name = "TravelResourceDetailForm" property = "job_id" />&appendixid=<bean:write name = "TravelResourceDetailForm" property = "appendix_id" />"><bean:write name = "TravelResourceDetailForm" property = "jobname"/></a>
		          		</div>
				   <%}else{%>
				      	<div id="breadCrumb">
				          	<a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
				          	<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "TravelResourceDetailForm" property = "msa_id" />">
			  					<bean:write name = "TravelResourceDetailForm" property = "msaname"/></a>
					  		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "TravelResourceDetailForm" property = "appendix_id" />">
					  			<bean:write name = "TravelResourceDetailForm" property = "appendixname"/></A>
					  		<A href="ActivityUpdateAction.do?&ref=<%=refForActivity%>&job_Id=<bean:write name = "TravelResourceDetailForm" property = "job_id" />">				          	
					  			<bean:write name = "TravelResourceDetailForm" property = "jobname"/></A>
					  		<A href="ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "TravelResourceDetailForm" property = "activity_Id" />&ref=<%=refForResource%>&addendum_id=<bean:write name = "TravelResourceDetailForm" property = "addendum_id"/>">
								<bean:write name = "TravelResourceDetailForm" property = "activityname"/></A>
						</div>
					<%} %>
				     </td>
				</tr>
			</c:if>
			<c:if test="${requestScope.from eq 'jobdashboard'}">
				<tr>
					
					<td class ="Ntoprow1" colspan="7">&nbsp;</td>
				</tr>
				<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
				         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
							 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
							 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
							  <a><bean:write name = "TravelResourceDetailForm" property = "activityname"/></a>
							  <A href="ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "TravelResourceDetailForm" property = "activity_Id" />&ref=<%=refForResource%>&addendum_id=<bean:write name = "TravelResourceDetailForm" property = "addendum_id"/>&from=<%=from%>">Resource List</a>
								 <a><span class="breadCrumb1">Details</a>
				    </td>
				</tr>
				<tr>    
					<td colspan = "5" width = "800">&nbsp;</td>
				</tr> 
			</c:if>
				<c:if test="${requestScope.from ne 'jobdashboard'}">
					<tr>    
						<td colspan = "5" height = "30" width = "800">
							<h2><bean:message bundle = "pm" key = "pm.travel.detail.label"/><bean:write name = "TravelResourceDetailForm" property = "travelname"/></h2>
						</td>
					</tr> 
				</c:if> 
			</table>
		</td>
		<c:if test="${requestScope.from ne 'jobdashboard'}">
		<td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
		<td valign="top" width="155">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
    				<tr><td width="155" height="39"  class="headerrow" colspan="2">
    						<table width="100%" cellpadding="0" cellspacing="0" border="0">
          						<tr><td width="150">
          							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
          							    <tr>
						               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
						                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
						               </tr>
              						</table>
              						</td>
          						</tr>
        					</table>
        				</td>
        		 	</tr>
    				<tr><td height="21" background="images/content_head_04.jpg" width="140">
      						<table width="100%" cellpadding="0" cellspacing="0" border="0">
          						<tr><td nowrap="nowrap" colspan="2">
                						<div id="featured1">
                							<div id="featured">
												<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
											</div>								
                						</div>		
										<span>
											<div id="filter">
    											<table width="250" cellpadding="0" class="divnoview">
                									<tr><td width="50%" valign="top">
                											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                      											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
    			</table>
    		</td>
    		</c:if>
    	</tr>
    </table>
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr><td width="2" height="0"></td>
  			
  			 <td>
					
			<table  border = "0" cellspacing = "1" cellpadding = "1" width="450">						
					<tr>
						<td colspan = "2" class = "formCaption" ><bean:message bundle = "AM" key = "am.travel.detail.general"/></td>
  					</tr>
  					<c:if test="${requestScope.from eq 'jobdashboard'}">
					 <tr>
					    <td class = "colDark">Resource Name</td>
    					<td class = "colLight" width="350"><bean:write name = "TravelResourceDetailForm" property = "travelname"/></td>
  					 </tr>
  					</c:if>
  					<tr>
    					<td class = "colDark"><bean:message bundle = "AM" key = "am.travel.detail.type"/></td>
    					<td class = "colLight" width="350"><bean:write name = "TravelResourceDetailForm" property = "traveltype"/></td>
  					</tr>
  					<tr>
    					<td class = "colDark" ><bean:message bundle = "AM" key = "am.travel.detail.quantity"/></td>
    					<td class = "colLight"><bean:write name = "TravelResourceDetailForm" property = "quantity"/></td>
  					</tr>
  					<tr>
    					<td class = "colDark"><bean:message bundle = "AM" key = "am.travel.detail.status"/></td>
    					<td class = "colLight"><bean:write name = "TravelResourceDetailForm" property = "status"/></td>
  					</tr>
  					<tr>
    					<td class = "colDark" ><bean:message bundle = "AM" key = "am.travel.detail.minimumquantity"/></td>
						<td class = "colLight"><bean:write name = "TravelResourceDetailForm" property = "minimumquantity"/></td>
  					</tr>
  					<tr>
    					<td class = "colDark" ><bean:message bundle = "AM" key = "am.travel.detail.minimumsellablequantity"/></td>
						<td class = "colLight"><bean:write name = "TravelResourceDetailForm" property = "sellablequantity"/></td>
  					</tr>
  					<tr> 
    					<td class = "colDark" ><bean:message bundle = "AM" key = "am.travel.detail.cnspartnumber1"/></td>
						<td class = "colLight"><bean:write name = "TravelResourceDetailForm" property = "cnspartnumber"/></td>
  					</tr>
  					<tr> 
						<td colspan = "2" class = "formCaption" ><bean:message bundle = "AM" key = "am.travel.detail.costprice"/></td>
					</tr> 
					<tr> 
						<td class = "colDark" ><bean:message bundle = "AM" key = "am.travel.detail.estimatedunitcost"/></td>
    					<td class = "colLight" style="text-align:right;width: 5em;"><bean:write name = "TravelResourceDetailForm" property = "estimatedunitcost"/></td>
  					</tr>
  					
  					<tr> 
						<td class = "colDark"><bean:message bundle = "AM" key = "am.travel.detail.estimatedtotalcost"/></td>
    					<td class = "colLight" style="text-align:right;width: 5em;"><bean:write name = "TravelResourceDetailForm" property = "estimatedtotalcost"/></td>
  					</tr>
  					
  					<tr> 
						<td class = "colDark"><bean:message bundle = "AM" key = "am.travel.detail.unitprice"/></td>
    					<td class = "colLight" style="text-align:right;width: 5em;"><bean:write name = "TravelResourceDetailForm" property = "priceunit"/></td>
  					</tr>
  					
  					
  					<tr> 
						<td class = "colDark"><bean:message bundle = "AM" key = "am.travel.detail.extendedprice"/></td>
    					<td class = "colLight" style="text-align:right;width: 5em;"><bean:write name = "TravelResourceDetailForm" property = "priceextended"/></td>
  					</tr>
  					
  					<tr> 
						<td class = "colDark" ><bean:message bundle = "AM" key = "am.travel.detail.proformamargin"/></td>
						<td class = "colLight"><bean:write name = "TravelResourceDetailForm" property = "proformamargin"/></td>
  					</tr>
   				
					<tr><td colspan = "2" class = "formCaption" height = "30" ><bean:message bundle = "AM" key = "am.detail.sow"/></td></tr>
					<% int k = 1; %>
					<% if ( sow_size > 0 ) {%>
						<tr> 
							<td class = "colLightRap" colspan = "2" >	
								<logic:iterate id = "ms" name = "sowlist">	
									<bean:write name = "ms" property = "sow" /> <br><br>
									<%
									if(sow_size>k) {
										k++;
										%><br><br><%
									}
									%>
								</logic:iterate>
							</td>
						</tr>	
					<%} else {%>
						<tr> 
							<td class = "dbvalueDisabled" colspan = "2">&nbsp;&nbsp;Not Defined</td>  
						</tr>	
					<%} %>


					<tr><td colspan = "2" class = "formCaption" height = "30" ><bean:message bundle = "AM" key = "am.detail.assumption"/></td></tr>
					<% k = 1; %>
					<% if( assumption_size > 0 ) {%>
						<tr> 
							<td class = "colLightRap" colspan = "2" >	
								<logic:iterate id = "ms" name = "assumptionlist">	
									<bean:write name = "ms" property = "assumption" />
									<%
									if(assumption_size>k) {
										k++;
										%><br><br><%
									}
									%>
								</logic:iterate>
							</td>
						</tr>	
					<%} else {%>
						<tr> 
							<td class = "dbvalueDisabled" colspan = "2">&nbsp;&nbsp;Not Defined</td>  
						</tr>	
					<%} %>
  					
  					<html:hidden property = "travelid" />
  					<html:hidden property = "activity_Id" />
  					<html:hidden property = "chkaddendum" />
  					<html:hidden property = "addendum_id" />
  					<html:hidden property = "chkdetailactivity" />
  				
				</table>
					<jsp:include page = '/Footer.jsp'>
		      			<jsp:param name = 'colspan' value = '37'/>
		      			<jsp:param name = 'helpid' value = 'traveldetailpage'/>
		 			</jsp:include>	
				</table>
			</td>
		</tr>
	</table>
</html:form>

</body>

<script>
function Backactionjob()
{
	<% if(  request.getAttribute( "jobid" ) != null ){
		if( from.equals( "jobdashboard_resource" ) )
		{%>
			document.forms[0].action = "JobDashboardAction.do?jobid=<%= jobid %>";
	 <%	}else {%>
	
		document.forms[0].action = "ActivityDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&Activity_Id=<bean:write name = "TravelResourceDetailForm" property = "activity_Id" />&ref=<bean:write name = "TravelResourceDetailForm" property = "chkdetailactivity"/>&addendum_id=<bean:write name = "TravelResourceDetailForm" property = "addendum_id"/>";
	<%} }%>
	document.forms[0].submit();
	return true;
}
</script>
<script>
function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
 		leftAdj = leftAdj - 260;
		document.getElementById("filter").style.left=leftAdj;
}
</script>



</html:html>
  					







