<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">		
<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
-->
<%@page import="java.util.ArrayList"%>
<%@ page import="java.sql.*" %> <%@ page import="javax.sql.DataSource" %>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %> 
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %> 
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--Start Added by Vishal 21/12/2006  --%>


<bean:define id="dcCountryList" name="dcCountryList" scope="request"/>
<bean:define id ="dcStateCM" name = "dcStateCM" scope = "request"/>
<bean:define id ="dcStateCM" name = "dcStateCM" scope = "request"/>

<%--End  --%>
<bean:define id="dcWlCompany" name="dcWlCompany" scope="request"/>
<bean:define id="dcSDCompany" name="dcSDCompany" scope="request"/>
<bean:define id="dcITCompany" name="dcITCompany" scope="request"/>
<bean:define id="dcTACompany" name="dcTACompany" scope="request"/>
<bean:define id="dcResourceLevel" name="dcResourceLevel" scope="request"/>
<bean:define id="dcHighestCriticalityAvailable" name="dcHighestCriticalityAvailable" scope="request"/>
<bean:define id="dcCertifications" name="dcCertifications"  scope="request"/>
<bean:define id="dcPartnerstatus" name="dcPartnerstatus"  scope="request"/>

<%
	Connection conn = ((DataSource) request.getAttribute("SQL"))
			.getConnection();
	Statement stmt = conn.createStatement();
	String temp_str = null;
	String topPartnerName = "";
	int k = 0;
	int CompetencySize = 0;
	int keywordRowSize = 3;
	String noLatLong = "";
	String acceptAddress = "";
	String action = "";
	String partnerName = "";

	if (request.getAttribute("CompetencySize") != null)
		CompetencySize = Integer.parseInt(request
				.getAttribute("CompetencySize") + "");
	if (request.getAttribute("topPartnerName") != null)
		topPartnerName = request.getAttribute("topPartnerName")
				.toString();
	
	if (request.getAttribute("noLatLong") != null)
		noLatLong = request.getAttribute("noLatLong").toString();
	if (request.getAttribute("acceptAddress") != null)
		acceptAddress = request.getAttribute("acceptAddress")
				.toString();
	if (request.getAttribute("action") != null)
		action = request.getAttribute("action").toString();
	if (request.getAttribute("partnerName") != null)
		partnerName = request.getAttribute("partnerName").toString();

	String labelBold = "";
	String labeleBold = "";
	String labele = "";
	String labelCombo = "";
	String labelo = "";
	String labelered = "";
	String labelored = "";
	int topDiv = 125;

	int tech_info_size = 0;
	temp_str = "DeleteTechnician(0,0,document.getElementById('dynaTech'),0)";

	if (request.getAttribute("tech_info_size") != null)
		tech_info_size = (int) Integer.parseInt(request.getAttribute(
				"tech_info_size").toString());

	if (request.getAttribute("topPartnerName") != null
			&& request.getAttribute("topPartnerName").equals(
					"Minuteman Partner")) {
		labelBold = "labelobold";
		labeleBold = "labelebold";
		labele = "labelo";
		labelo = "labele";
		labelCombo = "comboo";
		labelered = "labelored";
		labelored = "labelered";
	} else {
		labelBold = "labelebold";
		labeleBold = "labelobold";
		labele = "labele";
		labelo = "labelo";
		labelCombo = "comboe";
		labelered = "labelered";
		labelored = "labelored";
	}
%>				
<html:html>
<HEAD>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv = "Content-Type" content = "text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=9"/>
<%@ include  file="/SendEmail.inc" %>
<%@ include  file="/AjaxStateCountry.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>

<script language="JavaScript" src="javascript/JQueryMain.js"></script>
<title></title>
<style type="text/css">
tbody div{
    overflow:scroll;
    height:270px;
}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="moreTech(document.getElementById('dynaTech'),1);" >
	<div id="menunav">
	    <ul>
	    	<li>
	    		<a href="#" class="drop"><span><bean:message bundle="PVS" key="pvs.manageprofile" /></span></a>
	  			<ul>
	  				<li><a href="Partner_Edit.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=Update">Update</a></li>
	  				<li>
			            <a href="#"><span>Incident Reportsview</span></a>
			            <ul>
			      			<li><a href="AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=view&page=PVS">View</a></li>
			      			<li><a href="AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=add&page=PVS">Post</a></li>
		      			</ul>
		        	</li>
	  				<li><a href="javascript:jobHistory();">Job History</a></li>
	  				<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
						<c:if test="${requestScope.topPartnerName ne 'Minuteman Partner'}">
	  						<li><a href="Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="Partner_EditForm" property="pid"/>&convert=M">Convert To Minuteman</a></li>
						</c:if>
						<c:if test="${requestScope.topPartnerName ne 'Standard Partner'}">
	  						<li><a href="Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="Partner_EditForm" property="pid"/>&convert=S">Convert To Standard</a></li>
						</c:if>
					</c:if>
					<%
					if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
						%>
	  					<li><a href="javascript:del();">Delete</a></li>
						<%
					}
					%>
					<c:if test="${sessionScope.RdmRds eq 'N'}">
	  					<li><a href="javascript:gettingAjaxDataForEmail('<bean:write name="Partner_EditForm" property="pid"/>');">Request Restriction</a></li>
					</c:if>
					<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
						<c:if test="${sessionScope.partnerRegStatus eq 'R'}">
	  						<li><a href="javascript:reSendUnamePwd('<bean:write name="Partner_EditForm" property="pid"/>');">Send Username / Password</a></li>
						</c:if>
						<c:if test="${sessionScope.partnerRegStatus eq 'N'}">
	  						<li><a href="javascript:reSendRegEmail('<bean:write name="Partner_EditForm" property="pid"/>');">Re-send Registration Email</a></li>
						</c:if>
					</c:if>
	  			</ul>
	    	</li>
	  		<li>
	        	<a href="#" class="drop"><span><bean:message bundle="PVS" key="pvs.managepartner" /></span></a>
	  			<ul>
	  				<li>
			            <a href="#"><span>Minuteman</span></a>
			            <ul>
			      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners">Add</a></li>
			      			<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=update">Update</a></li>
			      			<%
			      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
			      				%>
			      				<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=delete">Delete</a></li>	      				
								<%
							}
							%>
		      			</ul>
		        	</li>
	  				<li>
			            <a href="#"><span>Standard Partner</span></a>
			            <ul>
			      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners">Add</a></li>
			      			<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=update">Update</a></li>
			      			<%
			      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
			      				%>
			      				<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=delete">Delete</a></li>	      				
								<%
							}
							%>
		      			</ul>
		        	</li>
	 				<li><a href="PVSSearch.do?orgTopName=all&action=newadded&enablesort=true">New Partners</a></li>
	 				<li><a href="PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true">Recent Updates</a></li>
	 				<li><a href="PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true">Annual Review Due</a></li>
	  			</ul>
	  		</li>
	  		<li><a href="Partner_Search.do?ref=search&type=v" class="drop"><bean:message bundle="PVS" key="pvs.partnersearch" /></a></li>
	  		<li><a href="MCSA_View.do" class="drop"><bean:message bundle="PVS" key="pvs.managemcsa" /></a></li>
		</ul>
	</div>

<logic:present name="deletemsg" scope="request">
<table>
 <tr>
  	<td class="message" height="30">
  		<logic:equal name="deletemsg" value="-9001">
	  		<bean:message bundle="PVS" key="pvs.partneredit.deletefailed"/>
  		</logic:equal>
  		<logic:equal name="deletemsg" value="-9003">
	  		<bean:message bundle="PVS" key="pvs.po.deletefailed"/>
  		</logic:equal>
  	</td>
  </tr>
</table>
</logic:present>
<logic:present name="deletemsgmysql" scope="request">
<table>
 <tr>
  	<td class="message" height="30"><bean:message bundle="PVS" key="pvs.partneredit.deletefailedmysql"/></td>
  </tr>
</table>
</logic:present>  

<logic:present name="retvalue" scope="request">
	<table>		 
		<logic:present name="emailSent" scope="request">	
			<logic:equal name="emailSent" value="emailSent">
				<tr><td class="message" height="25">Email was successfully sent to the Partner. </td></tr>
				<%
					topDiv = topDiv + 25;
				%> 
			</logic:equal> 
		</logic:present>
		<logic:present name="emailError" scope="request">	
			<logic:equal name="emailError" value="emailError">
				<tr><td class="message" height="25">Email could not be sent, please verify the email address. </td></tr>
				<%
					topDiv = topDiv + 25;
				%> 
			</logic:equal> 
		</logic:present>	 
		<logic:notEqual name="view" value="view">
			<logic:equal name="retvalue" value="0">
				<%
					if (action.equalsIgnoreCase("Add")) {
				%>
						<tr><td class="message" height="25"><%=topPartnerName%> Added Successfully</td></tr>
				<%
					topDiv = topDiv + 25;
									} else if (action.equalsIgnoreCase("Update")) {
				%>
						<tr><td class="message" height="25"><%=topPartnerName%> Updated Successfully</td></tr>
				<%
					topDiv = topDiv + 25;
									} else {
									}
				%>
			</logic:equal>
		</logic:notEqual>			
		
		<logic:present name="retMySqlValue" scope="request">
			<logic:equal name="retMySqlValue" value="9600">
					<tr><td class="messagewithwrap" height="25"><bean:message bundle="PVS" key="pvs.error.partner.connectingtoweb" /></td></tr>
					<%
						topDiv = topDiv + 25;
					%>
			</logic:equal>
			<logic:equal name="retMySqlValue" value="9601">
					<tr><td class="messagewithwrap" height="25"><bean:message bundle="PVS" key="pvs.error.savingwebpartner" /></td></tr>
					<%
						topDiv = topDiv + 25;
					%>
			</logic:equal>
		</logic:present>
		<logic:present name="convert" scope="request">
			<logic:equal name="retMySqlValue" value="1">
				<%
					if (topPartnerName == "Minuteman Partner") {
				%>
					<tr><td class="messagewithwrap" height="25">Standard Partner Converted Successfully.</td></tr>
					<%
						topDiv = topDiv + 25;
					%>
				<%
					} else {
				%>
					<tr><td class="messagewithwrap" height="25">Minuteman Partner Converted Successfully.</td></tr>
					<%
						topDiv = topDiv + 25;
					%>
				<%
					}
				%>	
			</logic:equal>
		</logic:present>
		
</table>
</logic:present>

<%
	int i = 0;
		String label = "";
%>
<BR>
<b>
 <a href="#"  onclick="viewChange('a')" class="tabtext"><bean:message bundle="PVS" key="pvs.partneredit.general"/></a>
 <a href="#"  onclick="viewChange('b')" class="tabtext"><bean:message bundle="PVS" key="pvs.partneredit.contactinfo"/></a>
 <a href="#"  onclick="viewChange('d')" class="tabtext"><bean:message bundle="PVS" key="pvs.partneredit.technicianinfo"/></a>
 <%if (Integer.parseInt(request.getAttribute(
			"isHRUser").toString()) == 1) { %>
 <a href="#"  onclick="viewChange('e')" class="tabtext"><bean:message bundle="PVS" key="pvs.partneredit.toollist"/></a>
 <% } %>
 <a href="#"  onclick="viewChange('g')" class="tabtext"><bean:message bundle="PVS" key="pvs.partneredit.certification"/></a>
 <a href="#"  onclick="viewChange('h');disableCombo();" class="tabtext"><bean:message bundle="PVS" key="pvs.partneredit.adminside"/></a>
 <a href="#"  onclick="viewChange('j')" class="tabtext"><bean:message bundle="PVS" key="pvs.partneredit.upload"/></a>
<BR>
<table width="990" border="0" cellspacing="0" cellpadding="0">
  <tr>  
	  <td  width="6" height="0"></td>
	  <td colspan="9"class="labeleboldwhite" height="30" ><%=topPartnerName%>&nbsp;View</td>
   </tr>
  <tr>
   	 <td  width="6" height="0"></td>
	 <td class="labeleboldwhite" colspan="1" width="125" height="30"><bean:message bundle = "PVS" key = "pvs.editpartnerdetails" />&nbsp;:</td>
	 <td id="pdrow2" class="labelenoboldwhite" width="710" style="padding-bottom: 18px;" height="30"><%=partnerName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: blue;"><bean:write  name="Partner_EditForm" property="thumbsUpCount" /></span>&nbsp;<img  src="images/thumbs60x30.png">&nbsp;<span style="color: blue;"><bean:write name="Partner_EditForm" property="thumbsDownCount"/></span>
	  <logic:notEqual name="Partner_EditForm" property="onTimePercentage" value="" > 
										 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/hoursglass30x30.png" border="0">&nbsp;
											<span style="color:#1f497d;text-align: left;"><bean:write name="Partner_EditForm" property="onTimePercentage"/></span>		
	</logic:notEqual>
	</td>
	 <td id="pdrow3" class="labeleboldwhite" width="50" height="30"><bean:message bundle="PVS" key="pvs.partneredit.adpo" /></td>
	 <td id="pdrow4" class="labelenoboldwhite" width="70" height="30"><bean:write name="Partner_EditForm" property="adpo" /></td>
	 <td id="pdrow5" colspan="5" width="35"></td>
  </tr>
</table>
  
<html:form action="Partner_Edit" enctype ="multipart/form-data">
<html:hidden property="pid" />
<html:hidden property="address_id" />
<html:hidden property="fromtype" />
<html:hidden property="jobid" />
<html:hidden property="pri_id"/>
<html:hidden property="sec_id"/> 
<html:hidden property="orgid"/>
<html:hidden property ="refersh"/>
<html:hidden property="orgTopId"/>
<html:hidden property="negotiatedRate"/>
<!-- to be deleted -->
<html:hidden property="lat_min"/>
<html:hidden property="lon_min"/>
<html:hidden property="lat_degree"/>
<html:hidden property="lon_degree"/>
<html:hidden property="lat_direction"/>
<html:hidden property="lon_direction"/>
<html:hidden property="resourceDeveloper" />
<html:hidden property="topPartnerName" /> 

<div id="a" style="visibility: visible;  POSITION: absolute;top:<%=topDiv%>;left:2;">
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<!-- start -->
<tr>
  <td  width="1" height="0"></td>
      <td><table border="0" cellspacing="1" cellpadding="1" width="900"> 
     
			<logic:present name = "topPartnerType" scope = "request">
				<logic:notEqual name = "topPartnerName" value="Minuteman Partner">
						<tr height="20">
							<td class="labelobold" colspan = "2"><bean:message bundle="PVS" key="pvs.partnerName"/>:</td>
							<td colspan="6" class="labelo">								
									<bean:write name="Partner_EditForm" property="partnerCandidate"/>
							
							</td>						
							<td colspan="2" class="labelobold"><bean:message bundle="PVS" key="pvs.partnerincorptype"/>:</td>
							<td colspan="2" class="labelo" nowrap>
									 <bean:write name="Partner_EditForm" property="companyType"/></td>
						 	<html:hidden property="partnerLastName" value = "" />
							<html:hidden property="partnerFirstName" value = "" />    
						</tr>

						<tr height="20">
							<td colspan="2" class="labelebold"><bean:message bundle="PVS" key="pvs.partnertaxid"/>:</td>
							<td colspan="2" class="labele" nowrap>
								<logic:equal name="Partner_EditForm" property="resourceDeveloper" value = "Y">
									<bean:write name="Partner_EditForm" property="taxId"/>
								</logic:equal>
								
								<logic:notEqual name="Partner_EditForm" property="resourceDeveloper" value = "Y">
									<bean:message bundle="PVS" key="pvs.seeresourcedevmanager"/>&nbsp;
								</logic:notEqual>
							</td>
							<td colspan="4" class="labelebold"></td>
					
							<td colspan="2" class="labelebold"><bean:message bundle="PVS" key="pvs.partnerincorpdate"/>:</td>
							<td colspan="2" class="labele" nowrap>
    									<bean:write name="Partner_EditForm" property="dateInc"/>
    						</td>
    						
    					</tr>
    						
				</logic:notEqual>

				<logic:equal name = "topPartnerName" value="Minuteman Partner">
						<tr height="20">
							<html:hidden property="partnerCandidate" value = "" />
							<td colspan="2" class="labelobold"><bean:message bundle="PVS" key="pvs.minutemanlastname"/>:</td>
							<td colspan="2" class="labelo">
									<bean:write name="Partner_EditForm" property="partnerLastName"/>								
							</td>
		
							<td colspan="2" class="labelobold"><bean:message bundle="PVS" key="pvs.minutemanfirstname"/>:</td>
							<td colspan="2" class="labelo">								
									<bean:write name="Partner_EditForm" property="partnerFirstName"/>								
							</td>
					
							<td  colspan="2" class="labelobold">
							<%
								if (Integer.parseInt(request.getAttribute(
														"isHRUser").toString()) == 1) {
							%>
							<bean:message bundle="PVS" key="pvs.ssnm"/>:
							<%
								}
							%>
							</td>
							<td  colspan="2" class="labelo" nowrap>
							<%
								if (Integer.parseInt(request.getAttribute(
														"isHRUser").toString()) == 1) {
							%>
								<logic:equal name="Partner_EditForm" property="resourceDeveloper" value = "Y">
									<bean:write name="Partner_EditForm" property="taxId"/>
								</logic:equal>
								
								<logic:notEqual name="Partner_EditForm" property="resourceDeveloper" value = "Y">
									<bean:message bundle="PVS" key="pvs.seeresourcedevmanager"/>&nbsp;
								</logic:notEqual>
							<%
								}
							%>
							</td>
						</tr height="20">
						
							<html:hidden property="companyType" value = "1099" />
							<html:hidden property="dateInc" value = "" />
				</logic:equal>
			</logic:present>
				
				<logic:present name = "acceptAddress" scope = "request">
					<logic:equal name = "acceptAddress" value="1">
						<tr height="20">
								<td class="labelodark" colspan="12">									
										<html:checkbox property="acceptAddress" value="1" disabled="true" />&nbsp;<bean:message bundle="PVS" key="pvs.acceptaddress" />
																		
								</td>			
						</tr>
					</logic:equal>
				</logic:present>						
			<%
										if (request.getAttribute("LatLongNotFound") != null
														&& request.getAttribute("LatLongNotFound").equals(
																"LatLongNotFound")) {
									%>
			<tr><td class = "<%=labelo%>" colspan="13"><font style="color: blue;"><bean:message bundle="PVS" key="pvs.nolatlongfound" /></font></td></tr>	
			<%
					}
				%>
			<tr height="20">
				<td  class="<%=labeleBold%>"  colspan="2"><bean:message bundle="PVS" key="pvs.address"/>:</td>
				<td  class="<%=labelo%>"  colspan="10">					
						<bean:write name="Partner_EditForm" property="address1"/>&nbsp;
					
				</td>
			</tr>
			<tr height="20">
				<td  class="<%=labelBold%>"  colspan="2"></td>
				<td  class="<%=labele%>"  colspan="10">					
						<bean:write name="Partner_EditForm" property="address2"/>&nbsp;
					
				</td>
			</tr> 
			
			     <tr height="20">
                        <!--Adding another field "Vendor site code" -->
                        <td  class="<%=labeleBold%>"  colspan="2"><bean:message bundle="PVS" key="pvs.vendor.sitecode.id"/>:</td>
                    <td  class="<%=labelo%>"  colspan="6">
                    <bean:write name="Partner_EditForm" property="vendorSiteCode"/>

                    </td>
                    <!-- Adding another field "Comcast Vendor Id" -->
                    <td  class="<%=labeleBold%>"  colspan="1"><bean:message bundle="PVS" key="pvs.comcast.vendor.id"/>:</td>
                    <td  class="<%=labelo%>" colspan="3">
                    <bean:write name="Partner_EditForm" property="comcastVendorID"/>
                    </td>
                    </tr>

			<tr height="20">
				<td  class="<%=labeleBold%>"  colspan="2"><bean:message bundle="PVS" key="pvs.city"/>:</td>
				<td  class="<%=labelo%>"  colspan="6">
						<bean:write name="Partner_EditForm" property="city"/>
					
				</td>
<!-- 				Adding another field "GP Vendor Id" -->
				<td  class="<%=labeleBold%>"  colspan="1"><bean:message bundle="PVS" key="pvs.vendor.id"/>:</td>
				<td  class="<%=labelo%>" colspan="3">
						<bean:write name="Partner_EditForm" property="gpVendorId"/>
					
				</td>
				
			</tr>
			<tr height="20">
				<td class = "<%=labelBold%>" colspan="2" ><bean:message bundle = "PVS" key="pvs.stateprovince"/>:</td>
				<td class = "<%=labele%>" colspan="2" >
					<logic:present name="initialStateCategory">
							<bean:write name="Partner_EditForm" property="state"/>												
					</logic:present>	
				</td>
				<td  class="<%=labelBold%>" colspan="2" ><bean:message bundle="PVS" key="pvs.country"/>:</td>
			    <td  class="<%=labele%>" colspan="2" >
			     	  <bean:write name="Partner_EditForm" property="country"/>				 
	    		</td>	    		 
				<td  class="<%=labelBold%>"  colspan="1" ><bean:message bundle="PVS" key="pvs.zip"/>:</td>
				<td  class="<%=labele%>"  colspan="1" >
						<bean:write name="Partner_EditForm" property="zip"/>					
				</td>
				<td  class="<%=labele%>" colspan="2" >&nbsp;</td>				
			</tr>
		 
			<tr height="20">
				<td  class="<%=labeleBold%>"  colspan="2"><bean:message bundle="PVS" key="pvs.mainphone"/>:</td>
				<td  class="<%=labelo%>"  colspan="2"><bean:write name="Partner_EditForm" property="mainPhone"/></td>
				<td  class="<%=labeleBold%>"  colspan="2"><bean:message bundle="PVS" key="pvs.mainfax"/>:</td>
				<td  class="<%=labelo%>"  colspan="2"><bean:write name="Partner_EditForm" property="mainFax"/></td>
				<td  class="<%=labeleBold%>"  colspan="1"><bean:message bundle="PVS" key="pvs.companyurl"/>:</td>
				<td  class="<%=labelo%>"  colspan="3"><bean:write name="Partner_EditForm" property="companyURL"/></td>
			</tr>

			<tr height="30">
				<td class="labellobold" colspan="12">Capabilities</td>
			</tr>
	
			<tr height="20">
				<td  class="<%=labelBold%>"  colspan="6" ><bean:message bundle="PVS" key="pvs.corecompetency"/>:</td>
				<td  class="<%=labelBold%>"  colspan="6" ><bean:message bundle="PVS" key="pvs.partnerEdit.keyWord"/>:</td>				
			</tr>

			<tr height="20">
				<td class="<%=labelo%>" colspan="6">
						<logic:present name="dcCoreCompetancy" scope ="request">
							
							<table border="0" width="99%">
							<logic:iterate id="Competency" name="dcCoreCompetancy">
							<%
								if (k % 3 == 0) {
							%>
								<tr>
							<%
								}
							%>
								<td class="<%=labelo%>" width="33%" nowrap="nowrap">
									<html:multibox property = "cmboxCoreCompetency" disabled="true"> 
										<bean:write name ="Competency" property = "competancyId"/>
									</html:multibox>   
								 	<bean:write name ="Competency" property = "competancyName"/>
								 	<logic:notEmpty name="Competency" property="score" > - <bean:write name="Competency" property="score" />%</logic:notEmpty>
								 </td>	
							<%
									if (k % 3 != 0 && k % 3 != 1) {
								%>
								</tr>
							 <%
							 	}
							 					k++;
							 %>
							</logic:iterate>
							</table>
						</logic:present>
				</td>
				<%
					k = ((k / 4) + (k % 2));
							keywordRowSize = keywordRowSize + k;
							String rowNum = String.valueOf(keywordRowSize);
				%>
<!-- 				check for row no height -->
				 <% 
				 int row = Integer.parseInt(rowNum);
				 if( row < 10){
				
					rowNum="10";
				
				} %> 
				<td class = "<%=labelo%>" colspan = "6"><html:textarea property = "keyWord" styleClass = "textbox" cols="70" rows="<%=rowNum%>" readonly="true"/></td>
			</tr>
			<tr height="30">
				<td class="labellobold" colspan="2">Contacts</td>
				<td class="labelloboldcenter" colspan="4"><bean:message bundle="PVS" key="pvs.partnerprimary"/>:</td>
				<td class="labelloboldcenter" colspan="6"><bean:message bundle="PVS" key="pvs.partnersecondary"/>:</td>
			</tr> 
			
			<tr height="20">
				<td class="<%=labelBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerfirstname"/>:</td>
				<td class="<%=labele%>" colspan="4"><bean:write name="Partner_EditForm" property="primaryFirstName"/></td>
				<td class="<%=labelBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerfirstname"/>:</td>
				<td class="<%=labele%>" colspan="4"><bean:write name="Partner_EditForm" property="secondaryFirstName"/></td>
			</tr>
			
			<tr height="20">
				<td class="<%=labeleBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerlastname"/>:</td>
				<td class="<%=labelo%>" colspan="4"><bean:write name="Partner_EditForm" property="primaryLastName"/></td>
				<td class="<%=labeleBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerlastname"/>:</td>
				<td class="<%=labelo%>" colspan="4"><bean:write name="Partner_EditForm" property="secondaryLastName"/></td>
			</tr>
			
			<tr height="20">
				<td class="<%=labelBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partneremail"/>:</td>
				<td class="<%=labele%>" colspan="4"><bean:write name="Partner_EditForm" property="prmEmail"/></td>
				<td class="<%=labelBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partneremail"/>:</td>
				<td class="<%=labele%>" colspan="4"><bean:write name="Partner_EditForm" property="secEmail"/></td>
			</tr>
			
			<tr height="20">
				<td class="<%=labeleBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerphone"/>:</td>
				<td class="<%=labelo%>" colspan="4"><bean:write name="Partner_EditForm" property="prmPhone"/></td>
				<td class="<%=labeleBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerphone"/>:</td>
				<td class="<%=labelo%>" colspan="4"><bean:write name="Partner_EditForm" property="secPhone"/></td>
			</tr>
			
			<tr height="20">
				<td class="<%=labelBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partnercell"/>:</td>
				<td class="<%=labele%>" colspan="4"><bean:write name="Partner_EditForm" property="prmMobilePhone"/></td>
				<td class="<%=labelBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.partnercell"/>:</td>
				<td class="<%=labele%>" colspan="4"><bean:write name="Partner_EditForm" property="secMobilePhone"/></td>
			</tr>
			
			<tr height="20">
				<td class="<%=labeleBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.webusername"/>:</td>
				<td class="<%=labelo%>" colspan="10">
						<bean:write name="Partner_EditForm" property="webUserName"/>
				</td>
			</tr>
			
			<tr height="20">
				<td class="<%=labelBold%>" colspan="2"><bean:message bundle="PVS" key="pvs.webpassword"/>:</td>
				<td class="<%=labele%>" colspan="10">
						<bean:write name="Partner_EditForm" property="webPassword"/>					
				</td>
			</tr>

			<logic:notEqual name="Partner_EditForm" property="daysRange"
				value="">

				<tr height="30">
				<td class="labellobold" colspan="12">Availability Schedule</td>
				</tr>
				<tr height="20">
					<td class="<%=labelBold%>" colspan="2">Day Range</td>
					<td class="<%=labele%>" colspan="10"><bean:write
							name="Partner_EditForm" property="daysRange" /></td>
				</tr>
				<tr height="20">
					<td class="<%=labeleBold%>" colspan="2">Time Range</td>
					<td class="<%=labelo%>" colspan="10"><bean:write
							name="Partner_EditForm" property="timeRange" /></td>
				</tr>

			</logic:notEqual>
									
			<tr height="30">
				<td class="labellobold" colspan="2">Recommended Clients</td>
				<td class="labelloboldcenter" colspan="4">Restricted Clients</td>
				<td class="labelloboldcenter" colspan="6">History</td>
			</tr> 	
								
								</tr>
								<tr>
									<td colspan="12" class="labelebold">
										<html:select styleId="recommendedPartnerList"
											name="Partner_EditForm" property="selectedPartnersFormRecommendedArr" size='15'
											style="width: 150px;float:left;margin-right:70px">
											<html:optionsCollection name="Partner_EditForm"
												property="selectedPartnersFormRecommendedCol" label="label"
												value="value" />
										</html:select> 

										<html:select styleId="restrictedPartnerList"
											name="Partner_EditForm" property="selectedPartnersForRestrictedArr" size='15'
											style="width: 150px;float:left;">
											<html:optionsCollection name="Partner_EditForm"
												property="selectedPartnersForRestrictedCol" label="label"
												value="value" />
										</html:select>
 <div class="scrollit">
<table align="center">
												
<td class="labellobold" colspan="2">Name</td>
<td class="labellobold" colspan="2">Type</td>
<td class="labellobold" colspan="2">Date</td>
<td class="labellobold" colspan="2">User Name</td>
<td class="labellobold" colspan="4">Action</td>		

 <% 
		ArrayList<String>  list_log=(ArrayList<String>)request.getAttribute("History");
 		String[] stockArr = new String[list_log.size()];
		stockArr = list_log.toArray(stockArr);
		String[] logarray;
		for (String s : stockArr){
			logarray = s.split("\\s*@\\s*"); %>

<tr height="20">
	<td  class="<%=labele%>"  colspan="2"><%=logarray[0] %></td>
	<td  class="<%=labele%>" style="white-space: nowrap;" colspan="2"><%=logarray[1] %></td>
	<td  class="<%=labele%>" style="text-align: left;" colspan="2"><%=logarray[2] %></td>
	<td  class="<%=labele%>" style="text-align: left;" colspan="2"><%=logarray[3] %></td>
	<td class="<%=labele%>" style="text-align: left;" colspan="4"><%=logarray[4] %></td>

</tr>

 <% } %> 			
										
</table>
</div>	
									
								</tr>
			
</td>

			</table></td>
</tr>
</table>
</div>

<div id="b" style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">

<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr >
  <td  width="1" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" width="600">   
 
<tr> 
	  <td colspan="9" class="labellobold" height="30" ><bean:message bundle="PVS" key="pvs.afterhours"/></td>
</tr>

<tr height="20">
	<td   class="tryB"  colspan="1">&nbsp; </td>
 	<td   class="tryB"  colspan="3"><bean:message bundle="PVS" key="pvs.afterhoursphone"/></td>
	<td   class="tryB"  colspan="5"><bean:message bundle="PVS" key="pvs.afterhouremail"/></td>
</tr>
<logic:present name="hr_email" scope="request">
<logic:iterate id="hr" name="hr_email">
<%
	if ((i++ % 2) == 0)
						label = "labelo";
					else
						label = "labele";
%>
<tr class="<%=label%>" height="20">
	<td colspan="1"><%=i%></td>
	<td colspan="3"><bean:write name="hr" property="afterHoursPhone"/></td>
	<td colspan="5"><bean:write name="hr" property="afterHoursEmail"/></td>
</tr>
</logic:iterate>
</logic:present>	

</table>
 </td>
 </tr>
	<tr height="20">
		<td colspan="2">&nbsp;</td>
	</tr>
	
	<tr >
		<td  width="1" height="0"></td>
		<td><table width="600" border="0" cellspacing="1" cellpadding="1">
			<tr height="20">
  				<td colspan="2">
  					<table id="dynatable" border="0" cellspacing="1" cellpadding="1" width="600">     
 					 <tr > 
  						  <td colspan="6" class="labellobold" height="30" ><bean:message bundle="PVS" key="pvs.physicalfieldoffice"/></td>
 					 </tr> 
 					 <tr height="20">
						<td class="tryB"  colspan="1"><bean:message bundle="PVS" key="pvs.address1"/></td>
 						<td class="tryB"  colspan="1"><bean:message bundle="PVS" key="pvs.address2"/></td>
						<td class="tryB"  colspan="1"><bean:message bundle="PVS" key="pvs.state"/></td>
						<td class="tryB"  colspan="1"><bean:message bundle="PVS" key="pvs.zipcode"/></td>
						<td class="tryB"  colspan="1"><bean:message bundle="PVS" key="pvs.phone"/></td>
						<td class="tryB"  colspan="1"><bean:message bundle="PVS" key="pvs.fax"/></td>
 					 </tr>
						<%
							i = 10;
									label = "";
						%>
						<logic:present name="phyAddress" scope="request">
						<logic:iterate id="address" name="phyAddress">
						<%
							if ((i % 2) == 0)
												label = "labelo";
											else
												label = "labele";
						%>
   					 <tr class="<%=label%>" height="20">
						<td colspan="1"><bean:write name="address" property="phyFieldAddress1"/> </td>
						<td colspan="1"><bean:write name="address" property="phyFieldAddress2"/> </td>
						<td colspan="1"><bean:write name="address" property="phyFieldState"/> </td>
						<td colspan="1"><bean:write name="address" property="phyFieldZip"/> </td>
						<td colspan="1"><bean:write name="address" property="phyFieldPhone"/> </td>
						<td colspan="1"><bean:write name="address" property="phyFieldFax"/> </td>
					</tr>
						<%
							i++;
						%>
						</logic:iterate>
						</logic:present>
 					</table>
 				</td>
 			</tr> 		
 			
			</table>
		</td>
	</tr>

</table>
</div>

<div id="d" style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
<table border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="1" height="0"></td>
  <td>
  <table id="dynaTech" border="0" cellspacing="1" cellpadding="1" width="600">
	<logic:present name="tech_info" scope="request">
	<logic:iterate id="info" name="tech_info">
 	<bean:define id="techid" name="info" property="tech_id" />
	<tr height="30"> 
    	<td colspan="6" class="labellobold" height="30"><bean:message bundle="PVS" key="pvs.technician/engineers"/></td>
	</tr>
	<tr >
		<TD class="labelebold" colspan="3"></TD>
		<td class="labele" colspan="3"><html:hidden styleId='<%="tech_id"+i%>' name="info" property="tech_id"/></td>
	</tr>
	<tr height="20">
		<td  class="labelobold"  colspan="3"><bean:message bundle="PVS" key="pvs.tech/engineername"/></td>
		<td  class="labelo"  colspan="3"><bean:write name="info" property="engName"/></td>
	</tr>
	<tr height="20">
		<td  class="labelebold"  colspan="3"><bean:message bundle="PVS" key="pvs.resourcelevel"/></td>
		<td  class="labele"  colspan="3"><bean:write name="info" property="resourceLevelName"/>
	    </td>
	</tr>

	<tr height="20">
		<td  class="labelobold"  colspan="3"><bean:message bundle="PVS" key="pvs.location"/></td>
		<td  class="labelo"  colspan="3"><bean:write name="info" property="engZip"/></td>
	</tr>
	<tr height="20">
		<td  class="labelebold"  colspan="3"><bean:message bundle="PVS" key="pvs.equippedwith"/></td>
		<td class="labele" colspan="3">
			<logic:present name="equipmentList" scope="request">
			<logic:iterate id="EL" name="equipmentList">
			  <html:multibox styleId='<%="chkboxEquipped"+i%>' name="info" property="chkboxEquipped" disabled="true">
				<bean:write name="EL" property="label"/> </html:multibox><bean:write name="EL" property="value"/> <br>
			</logic:iterate>
			</logic:present>
	<html:hidden property="chkboxEquipped" value=","/>
	</td>
	</tr>

	<tr height="20">
		<td  class="labelobold"  colspan="3"><bean:message bundle="PVS" key="pvs.union/nonunion"/></td>
		<td class="labelo"  colspan="3">
			<logic:equal name="info" property="rdoUnion" value = "1">
						<bean:message bundle="PVS" key="pvs.partneredit.nonunion"/>
			</logic:equal>
			
			<logic:equal name="info" property="rdoUnion" value = "2">
						<bean:message bundle="PVS" key="pvs.partneredit.union"/>
			</logic:equal>		
		</td>
	</tr>

	<tr height="20">
		<td  class="labelebold"  colspan="3"><bean:message bundle="PVS" key="pvs.highestcriticalityavailable"/></td>
		<td  class="labele"  colspan="3">
				<bean:write name="info" property="highCriticalityName"/>
	    </td>
	</tr>
	<tr height="20">
		<td  class="labelobold"  colspan="3"><bean:message bundle="PVS" key="pvs.certificationsandskill"/></td>
		<td  class="labelo"  colspan="3">
			<logic:notEqual name="info" property="certificationName" value="~">
				<bean:write name="info" property="certificationName" />
			</logic:notEqual>

	    	<input type="hidden" id="<%="cmboxCertifications"
															+ i%>" name="cmboxCertifications" value="~"/>
	     </td>
	</tr>
	<tr>
		<td class="labelebold" colspan="3">Security Certification:</td>
		<td class="labele" colspan="3">
			<table>
			<tr>
				<td class="labele">
					Drug Screening Certification:
				</td>
				<c:set var="date1" value="${info.checkBox1Date}" />
				<c:if test="${date1=='01/01/1900'}">
					<c:set var="date1" value="" />
				</c:if>
				<td>
					<html:checkbox name ="info" property="checkBox1" disabled="true" value="1"></html:checkbox>
				</td>
				<td class="labele">
					<html:text name="info" property="checkBox1Date" size="10" styleClass="textbox" readonly="true" value="${date1}"></html:text>
				</td>
			</tr>
			<tr>
				<td class="labele">
					Criminal Background Certification:
				</td>
				<c:set var="date2" value="${info.checkBox2Date}" />
				<c:if test="${date2=='01/01/1900'}">
					<c:set var="date2" value="" />
				</c:if>
				<td>
					<html:checkbox name ="info" property="checkBox2" disabled="true" value="1"></html:checkbox>
				</td>
				<td class="labele">
					<html:text name="info" property="checkBox2Date" size="10" styleClass="textbox" readonly="true" value="${date2}"></html:text>
				</td>
			</tr>
			<tr>
				<td class="labele">
					Harassment Free Work<br>Envoirnment Certification:
				</td>
				<c:set var="date3" value="${info.checkBox3Date}" />
				<c:if test="${date3=='01/01/1900'}">
					<c:set var="date3" value="" />
				</c:if>
				<td>
					<html:checkbox name ="info" property="checkBox3" disabled="true" value="1"></html:checkbox>
				</td>
				<td class="labele">
					<html:text name="info" property="checkBox3Date" size="10" styleClass="textbox" readonly="true" value="${date3}"></html:text>
				</td>
			</tr>
			</table>
			
		</td>
	</tr>

	<%
		i++;
												temp_str = "DeleteTechnician("
														+ techid
														+ ", "
														+ tech_info_size
														+ ", document.getElementById('dynaTech'),0)";
	%>
	</logic:iterate>
	</logic:present>	

</table>
 </td>
 </tr> 

<jsp:include page = '/Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'pvsPartner_Edit'/>
</jsp:include>
</table>
</div>

<div id="e" style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr height="20">
  <td  width="1" height="0"></td>
  <td><table id="tableToollist" border="0" cellspacing="1" cellpadding="1" width="600"> 

  <tr> 
    <td colspan="9"class="labellobold" height="20" ><b><bean:message bundle="PVS" key="pvs.toollist"/></b><br><bean:message bundle="PVS" key="pvs.pleaseenterareazip"/></td>
  </tr> 
  
<tr height="20">
	<td  class="tryB"  colspan="1" >&nbsp;</td>
 	<td  class="tryB"  colspan="4" nowrap><bean:message bundle="PVS" key="pvs.zip"/></td>
	<td  class="tryB"  colspan="4"><bean:message bundle="PVS" key="pvs.toolname"/></td>
</tr>
<%
	i = 0;
									label = "";
%>
<logic:present name="toolList" scope="request">
<logic:iterate id="TL" name="toolList">
<%
	if ((i++ % 2) == 0)
												label = "labelo";
											else
												label = "labele";
%>
<tr height="20">
	<td class=<%=label%> colspan="1"><html:multibox styleId='<%="chkboxTools"+(i-1)%>' property="chkboxTools" disabled="true">
	<bean:write name="TL" property="chkboxTools" /> </html:multibox></td>
	<td  class=<%=label%> colspan="4"><bean:write name="TL" property="toolZips"/></td>
	<td  class=<%=label%> colspan="4" ><img style='margin-left:<bean:write name="TL" property="width" />px' alt="arr" src="images/arr_black.png"> <bean:write name="TL" property="label"/></img> 
	</td></tr>
</logic:iterate>
</logic:present>

<jsp:include page = '/Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'pvsPartner_Edit'/>
</jsp:include>
 </table>
 </td>
 </tr> 
</table>
</div>

<div id="g" style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="1" height="0"></td>
  <td>
  <table border="0" cellspacing="1" cellpadding="1" width="600"> 
  <tr height="20"> 
    <td colspan="9"class="labellobold" height="20"><bean:message bundle="PVS" key="pvs.companylevelwireless"/></td>
  </tr> 
  
<tr height="20">
	<td  class="tryB" height="20" colspan="3" width=200>&nbsp; </td>
 	<td  class="tryB" height="20" colspan="3" width=250><bean:message bundle="PVS" key="pvs.validfrom"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
	<td  class="tryB" height="20" colspan="3" width=250><bean:message bundle="PVS" key="pvs.through"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
</tr>
<tr><td colSpan='9'>
	<table id="dynatableWL"  width="100%" border="0" cellspacing="1" cellpadding="1" >
	<%
		i = 0;
												label = "";
												String labelbold = "", combo = "";
	%>
	<logic:present name="WLCompnay" scope="request">
	<logic:iterate id="WL" name="WLCompnay">
	<%
		if ((i % 2) == 0) {
															labelbold = "labelobold";
															label = "labelo";
															combo = "comboo";
														} else {
															labelbold = "labelebold";
															label = "labele";
															combo = "comboe";
														}
	%>
		<tr height="20">
			<td  class="<%=label%>" colspan="3" width="220"><bean:write name="WL" property="wlcompanyName" /></td>
			<td   class="<%=label%>" colspan="3" width="272"><bean:write name="WL" property="validFromWC" /></td>
		    <td   class="<%=label%>" colspan="3" width="208"><bean:write name="WL" property="validToWC" /></td>    
		</tr>
		<%
			i++;
		%>
		</logic:iterate>
		</logic:present>
	</table>
	<td>
</tr>	

  <tr height="20"> 
    <td colspan="9"class="labellobold" height="20"><bean:message bundle="PVS" key="pvs.companylevelstructured"/></td>
  </tr> 
  
<tr height="20">
	<td  class="tryB" height="20" colspan="3">&nbsp; </td>
 	<td  class="tryB" height="20" colspan="3"><bean:message bundle="PVS" key="pvs.validfrom"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
	<td  class="tryB" height="20" colspan="3"><bean:message bundle="PVS" key="pvs.through"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
</tr>

<tr ><td colSpan='9'>
	<table id="dynatableSD"  width="100%" border="0" cellspacing="1" cellpadding="1" >
	<%
		i = 0;
												label = "";
												labelbold = "";
												combo = "";
	%>
	<logic:present name="SDCompnay" scope="request">
	<logic:iterate id="SD" name="SDCompnay">
	<%
		if ((i % 2) == 0) {
															labelbold = "labelobold";
															label = "labelo";
															combo = "comboo";
														} else {
															labelbold = "labelebold";
															label = "labele";
															combo = "comboe";
														}
	%>
		<tr height="20">
			<td  class="<%=label%>" colspan="3" width="220"><bean:write name="SD" property="sdcompanyName" /></td>
			<td   class="<%=label%>" colspan="3" width="272"><bean:write name="SD" property="validFromSD" /></td>
		    <td   class="<%=label%>" colspan="3" width="208"><bean:write name="SD" property="validToSD" /></td>    
		</tr>
		<%
			i++;
		%>
		</logic:iterate>
		</logic:present>
	</table>
	<td>
</tr>

  <tr height="20"> 
    <td colspan="9"class="labellobold" height="20" ><bean:message bundle="PVS" key="pvs.companylevelintegrated"/></td>
  </tr> 
  
<tr height="20">
	<td  class="tryB" height="20" colspan="3">&nbsp; </td>
 	<td  class="tryB" height="20" colspan="3" ><bean:message bundle="PVS" key="pvs.validfrom"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
	<td  class="tryB" height="20" colspan="3"><bean:message bundle="PVS" key="pvs.through"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
</tr>

<tr><td colSpan='9'>
	<table id="dynatableIT"  width="100%" border="0" cellspacing="1" cellpadding="1" >
	<%
		i = 0;
												label = "";
												labelbold = "";
												combo = "";
	%>
	<logic:present name="ITCompnay" scope="request">
	<logic:iterate id="IT" name="ITCompnay">
	<%
		if ((i % 2) == 0) {
															labelbold = "labelobold";
															label = "labelo";
															combo = "comboo";
														} else {
															labelbold = "labelebold";
															label = "labele";
															combo = "comboe";
														}
	%>
		<tr height="20">
			<td  class="<%=label%>" colspan="3" width="220"><bean:write name="IT" property="itcompanyName" /></td>
			<td   class="<%=label%>" colspan="3" width="272"><bean:write name="IT" property="validFromIT" /></td>
		    <td   class="<%=label%>" colspan="3" width="208"><bean:write name="IT" property="validToIT" /></td>    
		</tr>
		<%
			i++;
		%>
		</logic:iterate>
		</logic:present>	
	</table>
	<td>
</tr>	 

  <tr height="20"> 
    <td colspan="9"class="labellobold" height="20"><bean:message bundle="PVS" key="pvs.companylevelindustry"/></td>
  </tr> 
  
<tr height="20">
	<td   class="tryB" height="20" colspan="3">&nbsp; </td>
 	<td   class="tryB" height="20" colspan="3"><bean:message bundle="PVS" key="pvs.validfrom"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
	<td   class="tryB" height="20" colspan="3"><bean:message bundle="PVS" key="pvs.through"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
</tr>

<tr><td colSpan='9'>
	<table id="dynatableTA"  width="100%" border="0" cellspacing="1" cellpadding="1" >
	<%
		i = 0;
												label = "";
												labelbold = "";
												combo = "";
	%>
	<logic:present name="TACompnay" scope="request">
	<logic:iterate id="TA" name="TACompnay">
	<%
		if ((i % 2) == 0) {
															labelbold = "labelobold";
															label = "labelo";
															combo = "comboo";
														} else {
															labelbold = "labelebold";
															label = "labele";
															combo = "comboe";
														}
	%>
		<tr height="20">
			<td  class="<%=label%>" colspan="3" width="220"><bean:write name="TA" property="tacompanyName" /></td>
			<td   class="<%=label%>" colspan="3" width="272"><bean:write name="TA" property="validFromTA" /></td>
		    <td   class="<%=label%>" colspan="3" width="208"><bean:write name="TA" property="validToTA" /></td>    
		</tr>
		<%
			i++;
		%>
		</logic:iterate>
		</logic:present>	
	</table>
	<td>
</tr>

<jsp:include page = '/Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'pvsPartner_Edit'/>
</jsp:include>

</table>
 </td>
 </tr> 
</table>
</div>
<div id="h" style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
	<table width="600" border="0" cellspacing="1" cellpadding="1" align="center" >		
		<tr height="20">
			<td colspan="9">
				<table width="600" border="0" cellspacing="1">
				<tr> 
				    <td colspan="2" class="labellobold" height="30"><bean:message bundle="PVS" key="pvs.adminsideitems"/></td>
				</tr>
				<tr height="20">
					<td   class="labelobold" width="300">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.overallstatus"/></td>
					<td   class="labelo" width="300">				       		 
				       		 <logic:equal name="Partner_EditForm" property="cmboxStatus" value = "A">
				       		 	<bean:message  bundle="PVS" key="pvs.partner.status.a"/>
				       		 </logic:equal>
				       		 <logic:equal name="Partner_EditForm" property="cmboxStatus" value = "D">
				       		 	<bean:message  bundle="PVS" key="pvs.partner.status.d"/>
				       		 </logic:equal>
				       		 <logic:equal name="Partner_EditForm" property="cmboxStatus" value = "P">
				       		 	<bean:message  bundle="PVS" key="pvs.partner.status.p"/>
				       		 </logic:equal>
				       		 <logic:equal name="Partner_EditForm" property="cmboxStatus" value = "R">
				       		 	<bean:message  bundle="PVS" key="pvs.partner.status.r"/>
				       		 </logic:equal>
				       		 <logic:equal name="Partner_EditForm" property="cmboxStatus" value = "O">
				       		 	<bean:message  bundle="PVS" key="pvs.partner.status.o"/>
				       		 </logic:equal>
					</td>
				
				<tr height="20">
					<td  class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.leadminuteman"/></td>
					<td  class="labele">
					<html:radio name="Partner_EditForm" property="leadMinuteman" value = "true" disabled="true"></html:radio>Yes
					<html:radio name="Partner_EditForm" property="leadMinuteman" value = "false" disabled="true"></html:radio>No
					
					</td>
					
				<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;Speedpay Payment Term Accepted</td>
							<% if (topPartnerName.equals("Minuteman Partner")){%>		
								<td class="labelo">&nbsp;&nbsp;N/A</td>
								<%}
								else { 
								
								%>
								
								<td class="labelo"><html:radio name="Partner_EditForm"
											property="speedPaymentTerms" value="true" disabled="true"></html:radio>Yes <html:radio
											name="Partner_EditForm" property="speedPaymentTerms"
											value="false" disabled="true"></html:radio>No</td>
								<%}%>			
				</tr>	
					
				</tr>
					<tr height="20">
					<td  class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.externalsalesagent"/></td>
					<td  class="labele">
					<html:radio name="Partner_EditForm" property="externalSalesAgent" value = "true" disabled="true"></html:radio>Yes
					<html:radio name="Partner_EditForm" property="externalSalesAgent" value = "false" disabled="true"></html:radio>No
					
					</td>
				</tr>
				<%--End  --%>
				<tr height="20">
					<td  class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.minutemanPayrollId"/></td>
					<td  class="labelo"><bean:write name="Partner_EditForm" property="minutemanPayrollId" /></td>
				</tr>
				<tr height="20">
					<td  class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.dateadded"/></td>
					<td  class="labele"><bean:write name="Partner_EditForm" property="partnerCreateDate" /></td>
				</tr>
				<tr height="20">
					<td  class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.partnersource"/></td>
					<td  class="labelo"><bean:write name="Partner_EditForm" property="partnerSource" /></td>
				</tr>
				<tr height="20">
					<td  class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.partnerupdate"/></td>
					<td  class="labele" ><bean:write name="Partner_EditForm" property="partnerUpdateDate" /></td>
				</tr>
				
				<tr height="20">
					<td  class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.partnerupdateby"/></td>
					<td  class="labelo" ><bean:write name="Partner_EditForm" property="partnerUpdateBy" /></td>
				</tr>
				
				<tr height="20">
					<td  class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.partnerwebupdate"/></td>
					<td  class="labele" ><bean:write name="Partner_EditForm" property="partnerWebUpdateDate" /></td>
				</tr>
				
				<tr>
					<td  class="labellobold"  colspan="2" height="30"><bean:message  bundle="PVS" key="pvs.partneredit.partnerregstatus"/></td>
				</tr>
				
				<tr height="20">
					<td  class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.regstatus"/></td>
					
					<logic:equal name="Partner_EditForm" property="registered" value="NOT REGISTERED">
						<td  class="labelored"><b><bean:write name="Partner_EditForm" property="registered" /></b></td>
					</logic:equal>
					
					<logic:notEqual name="Partner_EditForm" property="registered" value="NOT REGISTERED">
						<td  class="labelo"><bean:write name="Partner_EditForm" property="registered" /></td>
					</logic:notEqual>
				</tr>
				
				<tr height="20">
					<td  class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message  bundle="PVS" key="pvs.partneredit.regdate"/></td>
					<td  class="labele"><bean:write name="Partner_EditForm" property="regDate" /></td>
				</tr>
					          
				</tr>
				<tr>
					<td  class="labelobold"  colspan="2" height="20">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.mcsa"/></td>
				</tr>
				<tr height="20">
					<td class="labelebold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.mcsa.version"/></td>
					<td class="labele">
						<logic:notEqual name="Partner_EditForm" property="mcsaVersion" value="0">
							<bean:write name="Partner_EditForm" property="mcsaVersion" />
						</logic:notEqual>
					</td>
				</tr>
				<tr height="20">
					<td  class="labelobold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.mcsa.signedby"/></td>
					<td  class="labelo"><bean:write name="Partner_EditForm" property="signedBy" /></td>
				</tr>
				
				<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.regrenewaldate"/></td>
					<td   class="labele"><bean:write name="Partner_EditForm" property="registrationRenewalDate" /></td>
				</tr>
				<tr>
					<td  class="labellobold"  colspan="2" height="30"><bean:message bundle="PVS" key="pvs.partneredit.incidentsummary"/></td>
				</tr>
				
				<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.incident.flag"/></td>
					<td   class="labele">			
							<logic:equal name="Partner_EditForm" property="incidentReport" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>
						    </logic:equal>
						    <logic:equal name="Partner_EditForm" property="incidentReport" value="N">
						    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>
						    </logic:equal>					    
				    </td>
				</tr>
				<tr height="20">
					<td   class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.incident.report"/></td>
					<td   class="labelo" ><bean:write name="Partner_EditForm" property="incidentReportFiled" /></td>
				</tr>
				<tr>
					<td  class="labellobold"  colspan="2" height="30"><bean:message bundle="PVS" key="pvs.partneredit.requireforms"/></td>
				</tr>
				
				<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.w-9"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="rdoW9" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="rdoW9" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					
				    <html:checkbox name="Partner_EditForm" property="w9Uploaded" value="Y"  disabled="true"  /><bean:message bundle="PVS" key="pvs.partneredit.w9uploaded"   />
				    </td>
				</tr>
				
			<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.insurance.label"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="isInsuranceSubmitted" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="isInsuranceSubmitted" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					
				    <html:checkbox name="Partner_EditForm" property="isInsuranceUploaded" value="Y" disabled="true"/><bean:message bundle="PVS" key="pvs.partneredit.w9uploaded" />
				    </td>
				</tr>
				
				<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.resume.label"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="isResumeSubmitted" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="isResumeSubmitted" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					
				    <html:checkbox name="Partner_EditForm" property="isResumeUploaded" value="Y" disabled="true"/><bean:message bundle="PVS" key="pvs.partneredit.resume.uploaded" />
				    </td>
				</tr>
			
				<!--  modifcation 13320 -->

			<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.Background.label"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="backgroundCheckSubmitted" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="backgroundCheckSubmitted" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					
				    <html:checkbox name="Partner_EditForm" property="backgroundCheckUploaded" value="Y" disabled="true"/><bean:message bundle="PVS" key="pvs.partneredit.Background.Uploaded" />
				    </td>
				</tr>
				
		<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.Drug.label"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="drugScreenCompletionSubmitted" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="drugScreenCompletionSubmitted" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					
				    <html:checkbox name="Partner_EditForm" property="drugScreenCompletionUploaded" value="Y" disabled="true"/><bean:message bundle="PVS" key="pvs.partneredit.Drug.Uploaded" />
				    </td>
				</tr>

				<!--  End  of Modifcation 13320 -->
				<tr>
					<td  class="labellobold"  colspan="2" height="30"><bean:message bundle="PVS" key="pvs.partneredit.otherpartnerinfo"/></td>
				</tr>
				<tr height="20">
					<td   class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.monthlynewsletter"/></td>
					<td   class="labelo" >
						<logic:equal name="Partner_EditForm" property="monthlyNewsletter" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
					    </logic:equal>
					    <logic:equal name="Partner_EditForm" property="monthlyNewsletter" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
					    </logic:equal>
				    </td>
				</tr>
				
			<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.Photoid" /></td>
									<td class="labelo">
									<logic:equal name="Partner_EditForm" property="photoIdentificationID" value="Y">
									<bean:message
											bundle="PVS" key="pvs.partneredit.Printed" />
											&nbsp;&nbsp;<a href="javascript:void(0)" onClick="if (window.event || document.layers) showWindow(event,'<bean:write name="Partner_EditForm" property="pid"/>'); 
												else showWindow('','<bean:write name="Partner_EditForm" property="pid"/>');"><img src="images/details_with_badge.png" border="0" align="absmiddle" 
												style="width: 12px;padding-right: 2px;" />REVIEW</a>
									 </logic:equal>		
									<logic:equal name="Partner_EditForm" property="photoIdentificationID" value="N">		
											 <bean:message
											bundle="PVS" key="pvs.partneredit.None" />
											
											&nbsp;&nbsp;<a href="javascript:void(0)" onClick="if (window.event || document.layers) showWindow(event,'<bean:write name="Partner_EditForm" property="pid"/>'); 
												else showWindow('','<bean:write name="Partner_EditForm" property="pid"/>');"><img src="images/details.png" border="0" align="absmiddle" 
												style="width: 12px;padding-right: 2px;" />REVIEW</a>
									 </logic:equal>	

									 &nbsp;&nbsp;&nbsp;<a href="javascript:deleteImage('<bean:write name="Partner_EditForm" property="pid"/>')">DELETE</a>&nbsp;&nbsp;&nbsp;

									<a href="javascript:uploadImage('<bean:write name="Partner_EditForm" property="pid"/>','<%=session.getAttribute("useremail")%>')">UPLOAD</a>
		</tr>					
			
			</table>
		</td>
	</tr>

<jsp:include page = '/Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'pvsPartner_Edit'/>
</jsp:include>
  </tr>
  
</table>
 </td>
 </tr> 
</table>
</div>
<div id="i" style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="1" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" width="600"> 
<tr> 
    <td colspan="9" class="labellobold" height="30"><bean:message bundle="PVS" key="pvs.changepassword"/></td>
</tr>
  
<tr height="20">
	<td   class="labelobold"  colspan="4"><bean:message bundle="PVS" key="pvs.newpassword"/></td>
	<td   class="labelo"  colspan="5"><html:password  styleClass="textbox" size="20" property="newPassword" value=""/></td>
</tr>
<tr height="20">
	<td  class="labelebold"  colspan="4"><bean:message bundle="PVS" key="pvs.confirmpassword"/></td>
	<td   class="labele"  colspan="5"><html:password  styleClass="textbox" size="20" property="confirmPassword" value=""/></td>
</tr>
<tr height="20"> 
    <td colspan="9" class="buttonrow"> 
      <html:submit property="save" styleClass="button" onclick="return validateAll();"><bean:message bundle="PVS" key="pvs.partneredit.save"/></html:submit>
      <html:reset property="reset" styleClass="button"><bean:message bundle="PVS" key="pvs.partneredit.reset"/></html:reset>
    </td>
</tr>
<jsp:include page = '/Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'pvsPartner_Edit'/>
</jsp:include>
</table>
 </td>
 </tr> 
</table>
</div>
<div id="j" style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">

<!--  if user having the CPD or Accounting right then user will be able to view this option. -->
											<logic:equal
											name="Partner_EditForm" property="accountingCPDRole"
											value="Y">
											
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr height="20">
  <td  width="1" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" width="600"> 
  <tr> 
    <td colspan="19"class="labellobold" height="30" ><bean:message bundle="PVS" key="pvs.uploadedfiles"/></td>
  </tr> 
<tr height="20">
	<td   class="tryB"  colspan="2"><bean:message bundle="PVS" key="pvs.dateposted"/></td>
 	<td   class="tryB"  colspan="3"><bean:message bundle="PVS" key="pvs.displayname"/></td>
	<td   class="tryB"  colspan="2"><bean:message bundle="PVS" key="pvs.originalname"/></td>
	<td   class="tryB"  colspan="2"><bean:message bundle="PVS" key="pvs.download"/></td>
		<td class="tryB" height="20" colspan="2">Status</td>
</tr>
<%
	boolean csschooser = true;
																String backgroundclass = "", remarkclass = "";
%>
<logic:present  name="Partner_EditForm" property="uploadedfileslist">
	<logic:iterate id="list" name="Partner_EditForm" property="uploadedfileslist"  >
	<%
		if (csschooser == true) {
																				backgroundclass = "texto";
																				csschooser = false;
																				remarkclass = "labelotop";
																			} else {
																				csschooser = true;
																				backgroundclass = "texte";
																				remarkclass = "labeletop";
																			}
	%>
 <html:hidden name="list" property="file_id" /> 
<tr height="20">
	<td  class="<%=backgroundclass%>"  colspan="2"><bean:write  name="list" property="file_uploaded_date" /></td>
	<td  class="<%=remarkclass%>" style="white-space: nowrap;" colspan="3"><bean:write  name="list" property="file_remarks"  /></td>
	<td  class="<%=backgroundclass%>" style="text-align: left;" colspan="2"><bean:write  name="list" property="file_name"  /></td>
	<td  class="<%=backgroundclass%>"  colspan="2"><a href = "ViewuploadedocumentAction.do?ref=downloaddocument&file_id=<bean:write name = "list" property = "file_id" />"><bean:write name = "list" property = "file_name" /></a></td>
	<td class="<%=backgroundclass%>" style="text-align: left;" colspan="2"><bean:write name="list"	property="file_status" /></td>

</tr>
</logic:iterate>
</logic:present>

<jsp:include page = '/Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'pvsPartner_Edit'/>
</jsp:include>

</table></td></tr></table></logic:equal>

</div>
  
<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>  

</html:form>

<div id="ajaxdiv" style="position: absolute;"></div>

</BODY>
</html:html>
<script>

function getshowtab() {
	var args = getshowtab.arguments;
		if(args[0]!= null) {
				if(args[0]=='a') {
					document.getElementById('pdrow2').style.width = 710;
					document.getElementById('pdrow3').style.width = 50;
					document.getElementById('pdrow4').style.width = 70;
					document.getElementById('pdrow5').style.width = 35;
				}else {
					document.getElementById('pdrow2').style.width = 350;
					document.getElementById('pdrow3').style.width = 50;
					document.getElementById('pdrow4').style.width = 70;
					document.getElementById('pdrow5').style.width = 395;
				}
		}
}

function viewChange() { //v3.0
	 MM_showHideLayers('a','','hide');
 	 MM_showHideLayers('b','','hide');
 	 MM_showHideLayers('d','','hide');
	 MM_showHideLayers('e','','hide');
	 MM_showHideLayers('f','','hide');	 
	 MM_showHideLayers('g','','hide');
	 MM_showHideLayers('h','','hide');	 
	 MM_showHideLayers('i','','hide');	 	 
	 MM_showHideLayers('j','','hide');	  	 
	 var args=viewChange.arguments;
	 MM_showHideLayers(args[0],'','show');
	 getshowtab(args[0]);
	 return true;
}
function MM_showHideLayers() { //v3.0

  var i,p,v,obj,args=MM_showHideLayers.arguments;
  
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v;}
    obj.visibility=v;}
}
function moreAttach(){
	var arg= moreAttach.arguments;
	var i = dynatable.rows.length-2 ;

	if (arg[0]==1){
		if (i>=1){
		 return;
		 }		
	}
	if (arg[0]!=1)
	if(!validateDivC())	return false;
	
	i = dynatable.rows.length-2 ;
    var cls='';
	if(dynatable.rows.length%2==0)	cls= 'labelo';
	if(dynatable.rows.length%2==1)	cls= 'labele';
	
	var oRow=dynatable.insertRow();
	var oCell=oRow.insertCell(0);
	alert(1);
	oCell.innerHTML="<input type='text' id='phyFieldAddress1"+i+"' name='phyFieldAddress1' size=20 maxlength=100 class='textbox' >";
	oCell.className=cls;

	oCell=oRow.insertCell(1);
	oCell.innerHTML="<input type='text' id='phyFieldAddress2"+i+"' name='phyFieldAddress2' size=20 maxlength=100 class='textbox'>";
	oCell.className=cls;

	oCell=oRow.insertCell(2);
	oCell.innerHTML="<input type='text' id='phyFieldState"+i+"' name='phyFieldState' size=15 maxlength=100 class='textbox'>";
	oCell.className=cls;

	oCell=oRow.insertCell(3);
	oCell.innerHTML="<input type='text' id='phyFieldZip"+i+"' name='phyFieldZip' size=10 maxlength=100 class='textbox' >";
	oCell.className=cls;

	oCell=oRow.insertCell(4);
	oCell.innerHTML="<input type='text' id='phyFieldPhone"+i+"' name='phyFieldPhone' size=10 maxlength=100 class='textbox' >";
	oCell.className=cls;

	oCell=oRow.insertCell(5);
	oCell.innerHTML="<input type='text' id='phyFieldFax"+i+"' name='phyFieldFax' size=10 maxlength=100 class='textbox'>";
	oCell.className=cls;
}	// END OF FUNCTION moreAttach()


</script>

<script>

function moreAttachCompany(){
	var arg=moreAttachCompany.arguments;
	var i = arg[0].rows.length ;
	if(! validateCompany(arg[0],arg[1],arg[2],arg[3],arg[4],'',5))return false;	
	var oRow=arg[0].insertRow();
	if(arg[0].rows.length%2==0){
    	c1class="labelebold";
    	c2class="labele";
    	combo="comboe";
	}
	else{
    	c1class="labelobold";
    	c2class="labelo";
    	combo="comboo";    	
	}

	var oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className=c1class;
	if(arg[2]=='w')
	oCell.innerHTML="<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+combo+"' onchange=\"return validateCompany(document.getElementById('dynatableWL'),'wlcompany','w' ,'validFromWC','validToWC' );\"><option value='0'>---Select---</option><%String sql = "select * from cp_company where cp_comp_id in(Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in(select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='W'  ))order by cp_comp_name";
																int countwl = 0;
																ResultSet rs1 = stmt
																		.executeQuery(sql);
																while (rs1
																		.next()) {
																	countwl++;%> <option value='<%=rs1
																			.getString("cp_comp_id")%>'><%=rs1
																			.getString("cp_comp_name")%></option>	<%}%> </select>";	
	if(arg[2]=='sd')
	oCell.innerHTML="<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+combo+"' onchange=\"return validateCompany(document.getElementById('dynatableSD'),'sdcompany','sd','validFromSD','validToSD' );\"><option value='0'>---Select---</option><%sql = "select * from cp_company where cp_comp_id in(Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in(select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='SD' ))order by cp_comp_name";
																countwl = 0;
																rs1 = stmt
																		.executeQuery(sql);
																while (rs1
																		.next()) {
																	countwl++;%> <option value='<%=rs1
																			.getString("cp_comp_id")%>'><%=rs1
																			.getString("cp_comp_name")%></option>	<%}%> </select>";
	if(arg[2]=='it')
	oCell.innerHTML="<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+combo+"' onchange=\"return validateCompany(document.getElementById('dynatableIT'),'itcompany','it','validFromIT','validToIT' );\"><option value='0'>---Select---</option><%sql = "select * from cp_company where cp_comp_id in(Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in(select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='IT' ))order by cp_comp_name";
																countwl = 0;
																rs1 = stmt
																		.executeQuery(sql);
																while (rs1
																		.next()) {
																	countwl++;%> <option value='<%=rs1
																			.getString("cp_comp_id")%>'><%=rs1
																			.getString("cp_comp_name")%></option>	<%}%> </select>";
	if(arg[2]=='ta')
	oCell.innerHTML="<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+combo+"' onchange=\"return validateCompany(document.getElementById('dynatableta'),'tacompany','ta','validFromTA','validToTA' );\"><option value='0'>---Select---</option><%sql = "select * from cp_company where cp_comp_id in(Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in(select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='ITA'))order by cp_comp_name";
																countwl = 0;
																rs1 = stmt
																		.executeQuery(sql);
																while (rs1
																		.next()) {
																	countwl++;%> <option value='<%=rs1
																			.getString("cp_comp_id")%>'><%=rs1
																			.getString("cp_comp_name")%></option>	<%}%> </select>";

	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className=c2class;
	oCell.innerHTML="<input type='text' id='"+arg[3]+i+"' name='"+arg[3]+"' size='10' value='' readonly='true' class='textbox' /><img src ='images/calendar.gif' width = 19 height = 17 border = 0 align ='center' onclick =\"return popUpCalendar(document.forms[0]."+arg[3]+i+",document.forms[0]."+arg[3]+i+", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";

	oCell=oRow.insertCell(2);
	oCell.colSpan=3;	
	oCell.className=c2class;	
	oCell.innerHTML="<input type='text' id='"+arg[4]+i+"' name='"+arg[4]+"' size='10' value='' readonly='true' class='textbox' /><img src ='images/calendar.gif' width = 19 height = 17 border = 0 align ='center' onclick =\"return popUpCalendar(document.forms[0]."+arg[4]+i+",document.forms[0]."+arg[4]+i+", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";	
}// END OF FUNCTION moreAttachCompany()

function validateCompany(){    
	viewChange('g');
	var arg= validateCompany.arguments;
	
	var arg5='';
	if(arg[2]=='w')
		arg5="<bean:message bundle="PVS" key="pvs.companylevelwireless"/>";
	if(arg[2]=='sd')
		arg5="<bean:message bundle="PVS" key="pvs.companylevelstructured"/>";
	if(arg[2]=='it')
		arg5="<bean:message bundle="PVS" key="pvs.companylevelintegrated"/>";	
	if(arg[2]=='ta')
		arg5="<bean:message bundle="PVS" key="pvs.companylevelindustry"/>";

	if (arg[6]==5){
 		for (var k=0;k<arg[0].rows.length;k++){
 			if(! chkCombo(document.getElementById(arg[1]+k),arg5))return false; 		
 			if(! chkBlank(document.getElementById(arg[3]+k),'date from in ' + arg5))return false; 		

 			if(! chkBlank(document.getElementById(arg[4]+k),'date to in ' + arg5))return false; 		
			
 			var vf=document.getElementById(arg[3]+k).value.toString();
 			var vt=document.getElementById(arg[4]+k).value.toString();
			var diffrence=new Date(parseInt(vt.substring(6)),parseInt(vt.substring(0,2))-1,parseInt(vt.substring(3,5)))-new Date(parseInt(vf.substring(6)),parseInt(vf.substring(0,2))-1,parseInt(vf.substring(3,5)));
			
			if (diffrence<0){
				 alert("Date of through is less than valid from in " +arg5);
		 		return false;
 			}// END OF IF 					 		
 			
 		}// END OF FOR
	 }// END OF IF

	 for (var i=0;i<arg[0].rows.length;i++){
	   for (var j=i+1;j<arg[0].rows.length;j++){
			var ob1=(document.getElementById(arg[1]+i));
			var ob2=(document.getElementById(arg[1]+j));
			if(  ob1.options[ob1.selectedIndex].text == ob2.options[ob2.selectedIndex].text  ){
			    alert("You have select duplicate items in "+arg5);
			    ob2.focus();
			    return false;
			}// END OF IF
		}// END OF FOR j
	  }// END OF FOR i
  
	return true;
}// END OF FUNCTION validateCompany()

</script>
<script>
function moreTech(){

	var arg = moreTech.arguments;
	var i = arg[0].rows.length;

	if (arg[1]==1) {
		if (i>=1) return;
	}	
	
	if (arg[1]!=1)
	{	
		var j=(arg[0].rows.length/9);
		for(var k=0;k<j;k++)
		{			
	 		if((document.getElementById('engName'+k).value=="") && j==k+1)
	 		{  
	 			alert('Technician name is required');
	 			document.getElementById('engName' +k).focus();
	 	  		return false;
	  		}
	 			
	 	}
    	if(!validateDivD()) return false;
   	}
	
	var rid=(arg[0].rows.length/9);
		
	var oTable2=arg[0];
	oTable2.width="600";
	oTable2.cellspacing="1";
	oTable2.id="dynaTech";	
		
	var oRow=oTable2.insertRow();
	
	var oCell=oRow.insertCell(0);
	oCell.colSpan=6;
	oCell.height=30;
	oCell.className="labellobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.technician/engineers"/>";
	
	//Seema-19/12/2006
	oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.className="labele";
	oCell.innerHTML="<input type='hidden' name='tech_id' value='0' id='tech_id"+ rid+"'>"; 
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.tech/engineername"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelo";
	oCell.innerHTML="";
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelebold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.resourcelevel"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labele";
	oCell.innerHTML="";
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.location"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelo";
	oCell.innerHTML="";
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelebold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.equippedwith"/>";
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labele";
	oCell.innerHTML="<%sql = "SELECT * from cc_equipment_detail ";
																countwl = 0;
																rs1 = stmt
																		.executeQuery(sql);
																while (rs1
																		.next()) {
																	countwl++;%> <input type='checkbox' name='chkboxEquipped' value='<%=rs1
																			.getString("cc_ed_name")%>'  id='chkboxEquipped"+ rid+"' disabled><%=rs1
																			.getString("cc_ed_name")%> <br><%}%>	<input type='hidden' name='chkboxEquipped' value=',' >";
	
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.union/nonunion"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelo";
	oCell.innerHTML="</td>";
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelebold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.highestcriticalityavailable"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labele";
	oCell.innerHTML="";
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.certificationsandskill"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelo";
	oCell.innerHTML="";

	oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labelebold";
	oCell.innerHTML="Security Certification:";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.height=20;
	oCell.className="labele";
	oCell.innerHTML="";
	
	oCell=oRow.insertCell(2);
	oCell.innerHTML="<input type='hidden' id='cmboxCertifications"+ rid+"' name='cmboxCertifications' value='~'>"; 
	
}

</script>

<script>
function DeleteTechnicianJava(techId){

document.forms[0].action = "Partner_Edit.do?tech_id="+ techId+"&delete=true";
document.forms[0].submit();
return true;
}// END OF FUNCTION deleteSchedule()

function DeleteTechnician() {
var techId = DeleteTechnician.arguments[0];

var techSize = DeleteTechnician.arguments[1];

	if(techId == 0) 
		DeleteTechRowJS(DeleteTechnician.arguments[2], DeleteTechnician.arguments[3]);
	else {
			
			if(DeleteTechnician.arguments[2].rows.length/9 == techSize)
			{	
				DeleteTechnicianJava(techId);
				if(DeleteTechnician.arguments[2].rows.length/9 >1)
				DeleteTechRowJS(DeleteTechnician.arguments[2], DeleteTechnician.arguments[3]);	
			}
			else
				DeleteTechRowJS(DeleteTechnician.arguments[2], DeleteTechnician.arguments[3]);	
	}
}


function DeleteTechRowJS(){

var object = DeleteTechRowJS.arguments[0];
var num = DeleteTechRowJS.arguments[1];
if( object.rows.length>num)
  {
   for(i=0;i<9;i++)
	object.deleteRow();
	
  } 		
}// END OF FUNCTION DeleteROW()
</script>

<script>
function DeleteRow(){
var object= DeleteRow.arguments[0];
var num= DeleteRow.arguments[1];
if( object.rows.length>num)
object.deleteRow();
}// END OF FUNCTION DeleteROW()
</script>
<script>

function validateAll(){

document.forms[0].refersh.value = "";
	trimFields();	
	
	if(! validateDivA()) return false;
	if(! validateDivB()) return false;
	if(! validateDivD()) return false;	
	if(! validateDivE()) return false;			
	if(! validateDivG()) return false;		
	if(! validateDivH()) return false;			
	enableFields();
	return true;
}// END OF FUNCTION validateAll()

</script>
<script>
function enableFields() {
	document.forms[0].partnerCandidate.disabled = false;
	document.forms[0].partnerLastName.disabled = false;
	document.forms[0].partnerFirstName.disabled = false;
	document.forms[0].taxId.disabled = false;
	document.forms[0].companyType.disabled = false;
	document.forms[0].dateInc.disabled = false;
	document.forms[0].acceptAddress.disabled = false;
	document.forms[0].address1.disabled = false;
	document.forms[0].address2.disabled = false;
	document.forms[0].city.disabled = false;
	document.forms[0].selectState.disabled = false;
	document.forms[0].country.disabled = false;
	document.forms[0].zip.disabled = false;
	document.forms[0].webUserName.disabled = false;
	document.forms[0].webPassword.disabled = false;
	document.forms[0].cmboxStatus.disabled = false;
	document.forms[0].incidentReport[0].disabled = false;
	document.forms[0].incidentReport[1].disabled = false;
}

//****************************** DIV 'a' ******************************************
function validateDivA(){

	 viewChange('a');
	
	if('<%=topPartnerName%>'=="Minuteman Partner") {
		if(! chkBlank(document.forms[0].partnerLastName,"<bean:message bundle="PVS" key="pvs.minutemanlastname"/>")) return false;
		if(! chkBlank(document.forms[0].partnerFirstName,"<bean:message bundle="PVS" key="pvs.minutemanfirstname"/>")) return false;
	} else {
		if(! chkBlank(document.forms[0].partnerCandidate,"<bean:message bundle="PVS" key="pvs.partnercandidate"/>")) return false;
	}
	
	if(! chkBlank(document.forms[0].address1,"<bean:message bundle="PVS" key="pvs.address1"/>"))	return false;
	if(! chkBlank(document.forms[0].city,"<bean:message bundle="PVS" key="pvs.city"/>"))	return false;	
	if(! chkCombo(document.forms[0].selectState,"<bean:message bundle="PVS" key="pvs.state"/>"))return false;
	if(! chkCombo(document.forms[0].country,"<bean:message bundle="PVS" key="pvs.country"/>"))return false;
	
	if(document.forms[0].country.value=='US' || document.forms[0].country.value=='CA') {
		if(! chkBlank(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>"))	return false;
		if(! chkInteger(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>")) return false;
	}else {
		if(! chkInteger(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>")) return false;
	}
	
	if(! chkBlank(document.forms[0].mainPhone,"<bean:message bundle="PVS" key="pvs.mainphone"/>"))return false;
	
	var submitflag = 'false';
	if(<%=CompetencySize%> != 0) {
		if( <%=CompetencySize%> == 1 )
		{
			if( !( document.forms[0].cmboxCoreCompetency.checked ))
			{
				alert('Please Check Core Competencies');
				return false;
			}
			else
			{
				submitflag = 'true';
			}
		} else {
			
			for( var i = 0; i<document.forms[0].cmboxCoreCompetency.length; i++ )
		  	{
		  		if( document.forms[0].cmboxCoreCompetency[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	
		  	if( submitflag == 'false')
		  	{	
		  		alert('Please Check Core Competencies');
				return false;
		  	}
		}
	}
	
	if(! chkBlank(document.forms[0].primaryFirstName,"Primary First Name"))	return false;
	if(! chkBlank(document.forms[0].primaryLastName,"Primary Last Name"))	return false;
	if(! chkBlank(document.forms[0].prmEmail,"Primary Email"))	return false;
	if(! CheckQuotes(document.forms[0].prmEmail,"Primary Email")) return false;
	
	if(document.forms[0].prmEmail.value=='Not Provided') {
	}else {
		if(! chkEmail(document.forms[0].prmEmail,"Primary Email")) return false;	
	}
	
	if(! chkBlank(document.forms[0].prmPhone,"Primary Phone"))	return false;
	if(! chkBlank(document.forms[0].prmMobilePhone,"Primary Cell"))	return false;
		
	return true;
	
}

</script>
<script>

//****************************** DIV 'b' ******************************************

function validateDivB(){

	viewChange('b');
	var i=0;
	
	while(document.getElementById('afterHoursPhone'+i)){
	
		if(! CheckQuotes(document.getElementById('afterHoursPhone'+i),"<bean:message bundle="PVS" key="pvs.afterhoursphone"/>"))return false;	

		if(! CheckQuotes(document.getElementById('afterHoursEmail'+i),"<bean:message bundle="PVS" key="pvs.afterhouremail"/>"))return false;
		i++;		
	}

	if(! validateDivC()) return false;
	return true;
	
}

</script>

<script>

//****************************** DIV 'c' ******************************************

function validateDivC(){
	var i = dynatable.rows.length-2 ;
		for (var j=0;j<i;j++) {
			if(isBlank(document.getElementById('phyFieldAddress1'+j).value) && isBlank(document.getElementById('phyFieldState'+j).value) && isBlank(document.getElementById('phyFieldZip'+j).value) && isBlank(document.getElementById('phyFieldPhone'+j).value) && isBlank(document.getElementById('phyFieldFax'+j).value) && i==j+1) {
     				dynatable.deleteRow();
	      			return true;
			}
		if(! CheckQuotes(document.getElementById('phyFieldAddress1'+j),"<bean:message bundle="PVS" key="pvs.address1"/>"))return false;
		if(! CheckQuotes(document.getElementById('phyFieldAddress2'+j),"<bean:message bundle="PVS" key="pvs.address2"/>"))return false;
		if(! CheckQuotes(document.getElementById('phyFieldState'+j),"<bean:message bundle="PVS" key="pvs.state"/>"))return false;
		if(! CheckQuotes(document.getElementById('phyFieldZip'+j),"<bean:message bundle="PVS" key="pvs.zipcode"/>"))return false;
		if(! CheckQuotes(document.getElementById('phyFieldPhone'+j),"<bean:message bundle="PVS" key="pvs.phone"/>"))return false;
		if(! CheckQuotes(document.getElementById('phyFieldFax'+j),"<bean:message bundle="PVS" key="pvs.fax"/>"))return false;
		if(! chkInteger(document.getElementById('phyFieldFax'+j),"<bean:message bundle="PVS" key="pvs.fax"/>"))return false;		
		}//END OF FOR	
	return true;
}

//****************************** DIV 'd' ******************************************
function validateDivD()
{ 
	viewChange('d');		
	if (document.getElementById('dynaTech').rows.length==0) return true;
	var i = document.getElementById('dynaTech').rows.length/9 ;
	var h = 0;
	while(h<i)
	{	
		
				if((document.getElementById('tech_id'+h).value!=0) && (document.getElementById('engName'+ h).value==""))
	 				{  
	 				   alert('Technician name is required');
	 				   document.getElementById('engName' + h).focus();
	 	  	   		   return false;
	  				}
	 			
	 			if(! CheckQuotes(document.getElementById('engName'+ h),"<bean:message bundle="PVS" key="pvs.tech/engineername"/>"))return false;	
				if(! CheckQuotes(document.getElementById('engZip'+ h),"<bean:message bundle="PVS" key="pvs.location"/>"))return false;	
		
	 		
			h=h+1;
	}
	return true;
}

</script>


<script>

//****************************** DIV 'e' ******************************************
function validateDivE(){	
	viewChange('e');			
	var i=0;
	while(document.getElementById('chkboxTools'+i)){
		if ((document.getElementById('chkboxTools'+i).checked==true))
			if(! chkBlank(document.getElementById('toolZips'+i),"<bean:message bundle="PVS" key="pvs.zip"/>"))return false;
			
		if ((document.getElementById('chkboxTools'+i).checked==false)){
			if(!isBlank(document.getElementById('toolZips'+i).value)){
				chkCheck(document.getElementById('chkboxTools'+i),document.getElementById('toolZips'+i),"<bean:message bundle="PVS" key="pvs.zip"/>");
				return false;
			}
		}
	i++;		
	}
	return true;
}   // END OF FUNCTION 

//****************************** DIV 'g' ******************************************
function validateDivG(){	
	viewChange('e');			
	if(! validateCompany(document.getElementById('dynatableWL'),'wlcompany','w' ,'validFromWC','validToWC','',5))return false;	
	if(! validateCompany(document.getElementById('dynatableSD'),'sdcompany','sd','validFromSD','validToSD','',5))return false;	
	if(! validateCompany(document.getElementById('dynatableIT'),'itcompany','it','validFromIT','validToIT','',5))return false;	
	if(! validateCompany(document.getElementById('dynatableTA'),'tacompany','ta','validFromTA','validToTA','',5))return false;	
	return true;
}   // END OF FUNCTION 

//****************************** DIV 'h' ******************************************
function validateDivH(){	
	viewChange('h');
	return true;
}   // END OF FUNCTION 
//****************************** DIV 'i' ******************************************

function validateDivI(){	
	viewChange('i');	
	if(! chkBlank(document.forms[0].newPassword,"<bean:message bundle="PVS" key="pvs.newpassword"/>"))return false;
	if(! CheckQuotes(document.forms[0].newPassword,"<bean:message bundle="PVS" key="pvs.newpassword"/>"))return false;
	if(! CheckQuotes(document.forms[0].newPassword,"<bean:message bundle="PVS" key="pvs.newpassword"/>"))return false;
	if(!(document.forms[0].newPassword.value==document.forms[0].confirmPassword.value)){
		alert("<bean:message bundle="PVS" key="pvs.confirmpassword"/> and <bean:message bundle="PVS" key="pvs.newpassword"/> are different");
		document.forms[0].confirmPassword.focus();
		return false;
	}// END OF IF			
	return true;
}

function trimFields() {
	var field=document.forms[0];
	for(var i=0; i<field.length; i++) {
		if(field[i].type=='text') {
			var temp1=field[i].value;
			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--) {
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }
		field[i].value=temp2;
		}
	}
return true;
}

function trimBetweenString(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!=" ")	nstr=nstr+str.substring(i,i+1);
	}// END OF FOR
	return nstr;
}

function removeHiffen(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!="-")	nstr=nstr+str.substring(i,i+1);
	}// END OF FOR
	return nstr;
}

function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkInteger(obj,label){
	if(!isInteger(removeHiffen(obj.value)))	{	
		alert("Only numeric values are allowed in "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkAlphabetic(obj,label){
	if(!isAlphabetic(trimBetweenString(obj.value)))	{	
		alert("Only alphabets are allowed in "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkCombo(obj,label){
	
	if(obj.value==0 || obj.value == null )	{	
		alert("Please Select "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function chkEmail(obj,label){
	if(!isEmail(obj.value))	{	
		alert("Invalid "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function setUnameEmail(obj,label) {
	if(document.forms[0].prmEmail.value=='Not Provided'){
	}else {
		var val = chkEmail(obj,label);
			if(val==true) {
				document.forms[0].webUserName.value = document.forms[0].prmEmail.value;
				document.forms[0].webPassword.value = document.forms[0].zip.value;
				return true;
			}
	}
}

function setNotProvided(obj) {
		obj.value = 'Not Provided';
		document.forms[0].webUserName.value = "";
		document.forms[0].webPassword.value = "";
}

function setUserPassword() {
	if(document.forms[0].webUserName.value=="")
		document.forms[0].webPassword.value = "";
	else 
		document.forms[0].webPassword.value = document.forms[0].zip.value;
}	


function chkRadio(obj,label){
	if(obj.value==""){	
		alert("Please Select "+label);	
		return false;
	}
	return true;
}

function chkCheck(obj,obj2,label){
		alert("Please check the check box as text field contains some entry");	
		obj2.focus();
		return false;
}

function chkFloat(obj,label){
	if(!isFloat(obj.value))	{
		alert("Please enter decimal value in "+label);	
		obj.focus();
		return false;
		}
	return true;
}
function CheckQuotes(obj,label){
	if(invalidChar(obj.value,label)) {		
		obj.focus();
		return false;
	}
	return true;
}

function Backaction() {
	if(document.forms[0].fromtype.value="powo") {
          document.forms[0].action = "POWOAction.do?jobid="+document.forms[0].jobid.value;
		  document.forms[0].submit();
		  return true;
	 } 
}
//Start :Added By Amit,This will disable MCSA combo if Radio button Y is clicked
function disableMCSACombo() {
	document.forms[0].mcsaVersion.disabled = true;
	document.forms[0].mcsaVersion.value = '0';
}

function enableMCSACombo() {
	document.forms[0].mcsaVersion.disabled = false;
}

//This method will be called on Admin side

function disableCombo()
{ 
}
//End :Added By Amit

function deletetechnicianinfo(){
	var techid = deletetechnicianinfo.arguments[0];
	document.forms[0].action = "Partner_Edit.do?tech_id="+ techid+"&delete=true";
	document.forms[0].submit();
	return true;
}// END OF FUNCTION deleteSchedule()

function del() 
{
		var convel = confirm( "Are you sure you want to delete the Partner?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?fromId=<bean:write name = "Partner_EditForm" property = "pid"/>&func=delete";
			document.forms[0].submit();
			return true;	
		}
} 
function jobHistory() 
{		
	document.forms[0].action = "JobInformationperPartner.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&from=Partner_Edit";
	document.forms[0].submit();
	return true;	
} 

function changeRegionCategory()
{
	document.forms[0].refersh.value="true";
	document.forms[0].action = "Partner_Edit.do?pid=<bean:write name = "Partner_EditForm" property = "pid"/>&check=country"
	document.forms[0].submit();
	return true;
}

function showWindow(e,pId) {
	var div=document.all.ajaxdiv;
	
	str = 'PartnerDetail.do?partnerId='+pId;
	$('#ajaxdiv').load(str);

	var _x;
	var _y;
	
	var paddingTop = $(document).scrollTop();
	

	var browserName = navigator.appName;
	
	if (browserName != "Microsoft Internet Explorer") {
		
		_x = globalMouseX;
		_y = globalMouseY + paddingTop;
		
	}
	
	if (browserName == "Microsoft Internet Explorer") {
		
		_x = e.clientX ;
		_y = e.clientY + paddingTop;
	}
	
	$("#ajaxdiv").css("left",_x + 8*1 + "px").css("top", _y + "px");
	
}



	function uploadImage(partnerId,updatedby){
		var ilex_url = document.URL;
		var lz_url = "";
		
		var startURL = ilex_url.substr(0, ilex_url.indexOf('.')).replace('ilex','pcc').replace('http:','https:');
		lz_url = startURL+".contingent.com/index.php?cmp=upload_badge_doc";
				
	  	var url= lz_url+"&cp_partner_id="+partnerId+"&updated_by="+updatedby;
	  	window.open(url,"_blank","directories=no, status=no,width=1100, height=600,top=50,left=20");
	}		
	
	function deleteImage(partnerId){
		var url="DeleteImage.do?partnerId="+partnerId;
		
		
		$.get(url,function(data, status){
			alert(data);
		});
		
	}

</script>
<%
	rs1.close();
	stmt.close();
	conn.close();
%>