<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying partner summary.
*
-->
<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>


<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>
<title></title>
</head>
<body>
<div id="menunav">
    <ul>
  		<li>
        	<a href="#" class="drop"><span><bean:message bundle="PVS" key="pvs.managepartner" /></span></a>
  			<ul>
  				<li>
		            <a href="#"><span>Minuteman</span></a>
		            <ul>
		      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners">Add</a></li>
		      			<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=update">Update</a></li>
		      			<%
		      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
		      				%>
		      				<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=delete">Delete</a></li>	      				
							<%
						}
						%>
	      			</ul>
	        	</li>
  				<li>
		            <a href="#"><span>Standard Partner</span></a>
		            <ul>
		      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners">Add</a></li>
		      			<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=update">Update</a></li>
		      			<%
		      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
		      				%>
		      				<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=delete">Delete</a></li>	      				
							<%
						}
						%>
	      			</ul>
	        	</li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=newadded&enablesort=true">New Partners</a></li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true">Recent Updates</a></li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true">Annual Review Due</a></li>
  			</ul>
  		</li>
  		<li><a href="Partner_Search.do?ref=search&type=v" class="drop"><bean:message bundle="PVS" key="pvs.partnersearch" /></a></li>
  		<li><a href="MCSA_View.do" class="drop"><bean:message bundle="PVS" key="pvs.managemcsa" /></a></li>
	</ul>
</div>
<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>  
<table>
	<tr>
		<td>&nbsp;</td>
		<logic:present name="deletemsg" scope="request">
			<td class="message" height="30"><bean:message bundle="PVS" key="pvs.partneredit.deletefailed"/></td>
		</logic:present>
		<logic:present name="partnerDeleted" scope="request">
			<td class="message" height="30">Partner Deleted Successfully.</td>
		</logic:present>
		<logic:present name="deletemsgmysql" scope="request">
			<td class="message" height="30"><bean:message bundle="PVS" key="pvs.partneredit.deletefailedmysql"/></td>
		</logic:present> 
	</tr>
</table>
</body>
</html:html>
