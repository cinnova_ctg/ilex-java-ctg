<!-- <!DOCTYPE HTML> -->
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id="dcPartnerType" name="dcPartnerType" scope="request" />
<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<title></title>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<style type="text/css">textarea {behavior: url(maxlength.htc)}</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<html:form action="PVSSearch" >
<div id="menunav">
    <ul>
  		<li>
        	<a href="#" class="drop"><span><bean:message bundle="PVS" key="pvs.managepartner" /></span></a>
  			<ul>
  				<li>
		            <a href="#"><span>Minuteman</span></a>
		            <ul>
		      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners">Add</a></li>
		      			<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=update">Update</a></li>
		      			<%
		      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
		      				%>
		      				<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=delete">Delete</a></li>	      				
							<%
						}
						%>
	      			</ul>
	        	</li>
  				<li>
		            <a href="#"><span>Standard Partner</span></a>
		            <ul>
		      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners">Add</a></li>
		      			<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=update">Update</a></li>
		      			<%
		      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
		      				%>
		      				<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=delete">Delete</a></li>	      				
							<%
						}
						%>
	      			</ul>
	        	</li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=newadded&enablesort=true">New Partners</a></li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true">Recent Updates</a></li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true">Annual Review Due</a></li>
  			</ul>
  		</li>
  		<li><a href="Partner_Search.do?ref=search&type=v" class="drop"><bean:message bundle="PVS" key="pvs.partnersearch" /></a></li>
  		<li><a href="MCSA_View.do" class="drop"><bean:message bundle="PVS" key="pvs.managemcsa" /></a></li>
	</ul>
</div>


<html:hidden property="act"/>
<html:hidden property="orgTopName"/>
<bean:define id  = "checkAct" name="PVSSearchForm" property="act"/>
<logic:present name="deletemsg" scope="request">
<table>
<tr>
	<td class="message" height="30"><bean:message bundle="PVS" key="pvs.partneredit.deletefailed"/></td>
</tr>
</table>
</logic:present>
<logic:present name="partnerDeleted" scope="request">
<table>
<tr>
	<td class="message" height="30">Partner Deleted Successfully.</td>
</tr>
</table>
</logic:present>
<logic:present name="deletemsgmysql" scope="request">
<table>
<tr>
	<td class="message" height="30"><bean:message bundle="PVS" key="pvs.partneredit.deletefailedmysql"/></td>
</tr>
</table>	
</logic:present> 
<table border="0" cellspacing="1" cellpadding="1" width=100%>
	<tr>
	  <td  width="1" height="0"></td>
	  <td>
		  <table border="0" cellspacing="1" cellpadding="1" width=100% > 
		 	<tr>
		 		<logic:equal name="PVSSearchForm" property="act" value="delete">
		 			<logic:equal name="PVSSearchForm" property="orgTopName" value="Minuteman Partners">
				  		<td class="labeleboldwhite" colspan="2">Search Minuteman Partners</td>
				  	</logic:equal>
			  	</logic:equal>
			  	
				<logic:equal name="PVSSearchForm" property="act" value="delete">
		 			<logic:equal name="PVSSearchForm" property="orgTopName" value="Certified Partners">
				  		<td class="labeleboldwhite" colspan="2">Search Certified Partners</td>
				  	</logic:equal>
			  	</logic:equal>			  	
			  	
			  	<logic:equal name="PVSSearchForm" property="act" value="update">
			  		<logic:equal name="PVSSearchForm" property="orgTopName" value="Minuteman Partners">
				  		<td class="labeleboldwhite" colspan="2">Search Minuteman Partners</td>
				  	</logic:equal>
			  	</logic:equal>
			  	
			  	<logic:equal name="PVSSearchForm" property="act" value="update">
			  		<logic:equal name="PVSSearchForm" property="orgTopName" value="Certified Partners">
				  		<td class="labeleboldwhite" colspan="2">Search Certified Partners</td>
				  	</logic:equal>
			  	</logic:equal>
			  	
			  	<logic:equal name="PVSSearchForm" property="act" value="resentupdated">
			  		<td class="labeleboldwhite" colspan="2">Updated Partners</td>
			  	</logic:equal>
			  	<logic:equal name="PVSSearchForm" property="act" value="newadded">
			  		<td class="labeleboldwhite" colspan="2">New Partners</td>
			  	</logic:equal>
			  		<logic:equal name="PVSSearchForm" property="act" value="annualreport">
			  		<td class="labeleboldwhite" colspan="2"><bean:message bundle="PVS" key="pvs.annual.due"/></td>
			  	</logic:equal>
		  	</tr>
		  	<tr>
		  		<logic:equal name="PVSSearchForm" property="act" value="delete">
					<td>
						<html:text  styleClass="text" size="40"  maxlength="100" property="division" />
					</td>
				</logic:equal>
				<logic:equal name="PVSSearchForm" property="act" value="update">
					<td width="50">
						<html:text  styleClass="text" size="40"  maxlength="100" property="division" />
					</td>
				</logic:equal>
				
				 <%if(checkAct.equals("newadded") || checkAct.equals("resentupdated") || checkAct.equals("annualreport")){%>
				<td>
					<table border="0" cellspacing="1" cellpadding="1" width="550">
						<tr>
							<td class="dblabelsmall">Start Date:
								<html:text  name  ="PVSSearchForm" styleClass = "text" size = "10" property = "startdate" readonly = "true"/>
			 					<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].startdate , document.forms[0].startdate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
			 				</td>
			 				<td class="dblabelsmall">End Date:
			 					<html:text  name  ="PVSSearchForm" styleClass = "text" size = "10" property = "enddate" readonly = "true"/>
								<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].enddate , document.forms[0].enddate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
							</td>
							<td class="dblabelsmall"><bean:message bundle="PVS" key="pvs..annual.due.partnertype"/>
								<html:select  name = "PVSSearchForm" property = "partnerType" size="1" styleClass="combowhite">
									<html:optionsCollection name = "dcPartnerType"  property = "firstlevelcatglist"  label = "label"  value = "value" />
								</html:select>
							</td>	
						</tr>
					</table>
				</td>
				<%}%>
				<td class="labeleboldwhite" width="75%">&nbsp;
					<html:button property="search" styleClass="button" onclick="return setSearch();"><bean:message bundle="CM" key="cm.search"/></html:button>&nbsp;
					<html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "pm" key = "msa.tabular.sort"/></html:button>
				</td>
			</tr>
		  </table>
	  </td>
	</tr>
</table>

<% int i=0;String cls=""; %>
<table border="0" cellspacing="1" cellpadding="1" width="100%">
	<tr>
	  <td width="1" height="0"></td>
	  <td>
		  <table border="0" cellspacing="1" cellpadding="1"> 
			  <logic:notPresent name="searchPvsResult" scope="request">
			   <%if(checkAct.equals("annualreport")){%>
			   	 <tr>
			  			<td class="message" height="10"><bean:message bundle="CM" key="cm.search1.nomatchingresults"/></td>
				 </tr>
			   <%}else{%>
			  	<logic:present name="firsttime" scope="request">
				  <tr>
			  			<td class="message" height="10"><bean:message bundle="CM" key="cm.search1.nomatchingresults"/></td>
				  </tr>
			  </logic:present>
			  <%}%>
			  </logic:notPresent>
			    <logic:present name="searchPvsResult" scope="request">
				  <!--<tr>
				  	<td class="message" colspan="5"><bean:message bundle="CM" key="cm.search1.searchresults"/></td>
				  </tr> -->
				  
			  	  <tr>
			  	  <%if(checkAct.equals("newadded") || checkAct.equals("resentupdated")){%>
			  	  	  <th class="Ntryb">Date</th>	
			  	  <%}else if(checkAct.equals("annualreport")) {%>
			  	  		 <th class="Ntryb"><bean:message bundle="PVS" key="pvs.review.date"/></th>	
			  	  <%}%>
					<th class="Ntryb">Partner Name</th>
					<%if(checkAct.equals("newadded") || checkAct.equals("resentupdated")||checkAct.equals("annualreport")){%>
						<th class="Ntryb">Type</th>
					<%}%>
					<th class="Ntryb">Address Line 1</th>
					<th class="Ntryb">Address Line 2</th>
				  	<th class="Ntryb"><bean:message bundle="CM" key="cm.search1.city"/></th>
				  	<th class="Ntryb"><bean:message bundle="CM" key="cm.search1.state"/></th>
				  	<th class="Ntryb">Country</th>
				  	<th class="Ntryb">Zip Code</th>
				  	<td class="Ntryb"></td>
				  </tr>
			  
			  <logic:iterate name="searchPvsResult" id="search">
			  <bean:define id ="addValid" name = "search" property = "addressValidator" type="java.lang.String"/>
			  <%if (i%2==0)cls="Ntexto" ;
			    if (i%2==1)cls="Ntexte" ;
			    i++;
			   %>
				  <tr>	
				  	<%if(checkAct.equals("annualreport")){%>
				  		<td class="<%=cls%>"><bean:write name="search" property="regRenewalDate"/></td>
				  	<%}else if(checkAct.equals("newadded") ||checkAct.equals("resentupdated")){%>
				  		<td class="<%=cls%>"><bean:write name="search" property="cp_pt_date"/></td>
				  	<%}%>
				  	<td class="<%=cls%>">
				  	<%if(checkAct.equals("newadded") || checkAct.equals("resentupdated") || checkAct.equals("annualreport")){%>
				  	<a href="Partner_Edit.do?function=Update&pid=<bean:write name="search" property="cp_partner_id"/>&formPVSSearch=fromPVSSearch"><bean:write name="search" property="lo_om_division"/></a>
				  	<%}else{%>
				  		<bean:write name="search" property="lo_om_division"/>
				  	<%}%>
				  	
				  		<logic:equal name="search" property="cp_pd_incident_type" value="Y" >
						 <img src="images/2.gif" border="0" height="10" width = "11">
						 </logic:equal>
						 <logic:equal name="search" property="cp_pd_status" value="R" >
						  <img src="images/1.gif" border="0" height="10" width = "11">
						 </logic:equal>
				  	</td>
				  	
				  		<%if(checkAct.equals("newadded") || checkAct.equals("resentupdated") || checkAct.equals("annualreport")){%>
				  				<td class="<%=cls%>"><bean:write name="search" property="lo_ot_name"/></td>
				  		<%}%>
				  		
				  	<%if(addValid.equals("InValid") && ( checkAct.equals("newadded") || checkAct.equals("resentupdated")  || checkAct.equals("annualreport"))){ %>
				  		<td class="<%=cls%>"><font color ="red"><bean:write name="search" property="lo_ad_address1"/></font></td>
					  	<td class="<%=cls%>"><font color ="red"><bean:write name="search" property="lo_ad_address2"/></font></td>
					  	<td class="<%=cls%>"><font color ="red"><bean:write name="search" property="lo_ad_city"/></font></td>
					  	<td class="<%=cls%>"><font color ="red"><bean:write name="search" property="lo_ad_state"/></font></td>
						<td class="<%=cls%>"><font color ="red"><bean:write name="search" property="lo_ad_country"/></font></td>
					  	<td class="<%=cls%>"><font color ="red"><bean:write name="search" property="lo_ad_zip_code"/></font></td>
				  	<%}else{%>
					  	<td class="<%=cls%>"><bean:write name="search" property="lo_ad_address1"/></td>
					  	<td class="<%=cls%>"><bean:write name="search" property="lo_ad_address2"/></td>
					  	<td class="<%=cls%>"><bean:write name="search" property="lo_ad_city"/></td>
					  	<td class="<%=cls%>"><bean:write name="search" property="lo_ad_state"/></td>
						<td class="<%=cls%>"><bean:write name="search" property="lo_ad_country"/></td>
					  	<td class="<%=cls%>"><bean:write name="search" property="lo_ad_zip_code"/></td>
				  	<%}%>
				  	
				  	<logic:equal name="PVSSearchForm" property="act" value="update">
				  	<td class="<%=cls%>" width="5">[<a href="Partner_Edit.do?function=Update&pid=<bean:write name="search" property="cp_partner_id"/>">Edit</a>]</td>
				  	</logic:equal>
				  	<logic:equal name="PVSSearchForm" property="act" value="delete">
				  	<td class="<%=cls%>" width="5">[<a href=# onclick="return del(<bean:write name="search" property="cp_partner_id"/>);" >Delete</a>]</td>
				  	</logic:equal>
				  	<%if(checkAct.equals("newadded") || checkAct.equals("resentupdated")||checkAct.equals("annualreport")){%>
				  		<td class="<%=cls%>" width="5">[<a href="Partner_Edit.do?function=Update&pid=<bean:write name="search" property="cp_partner_id"/>&formPVSSearch=fromPVSSearch">View</a>]</td>
				  	<%}%>
				  </tr>
				</logic:iterate>
			  </logic:present>
		  </table>
	  </td>
	</tr>
</table>
<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>  
</html:form>
</body>
</html:html>
<script>

function setSearch()
{
	if(document.forms[0].act.value == "update" || document.forms[0].act.value == "delete"){
		document.forms[0].division.value=trim(document.forms[0].division.value);
		if(isBlank(document.forms[0].division.value)){
			alert("<bean:message bundle="CM" key="cm.search1.entersomevalueforsearch"/>");
			document.forms[0].division.focus();
			return false;
		}
	
	document.forms[0].action = "PVSSearch.do?division="+document.forms[0].division.value+"&firsttime=firsttime";
	}
	
	if(document.forms[0].act.value  == "resentupdated" || document.forms[0].act.value  == "newadded" || document.forms[0].act.value  == "annualreport"){
		if(document.forms[0].startdate.value  == ""){
			alert('Select Start Date for Search');
			return false;
		}
		if(document.forms[0].enddate.value  == ""){
			alert('Select End Date for Search');
			return false;
		}
		if (!compDate_mdy(document.forms[0].startdate.value, document.forms[0].enddate.value)) 
			{
				alert("End Date Should be greater than Start Date");
				return false;
			} 
	document.forms[0].action = "PVSSearch.do?firsttime=firsttime";
	}
	document.forms[0].submit();								
	return true;	
}

function del(pid) 
{
		//alert(pid);
		var convel = confirm( "Are you sure you want to delete the Partner?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?pid="+pid+"&func=delete&frompage=pvssearch";
			document.forms[0].submit();
			return true;	
		}
} 

var str = '';
function sortwindow()
{
	var orgTopName = '<bean:write name="PVSSearchForm" property="orgTopName"/>';
	var action = '<bean:write name="PVSSearchForm" property="act"/>';
	var startdate = '<bean:write name="PVSSearchForm" property="startdate"/>';
	var enddate = '<bean:write name="PVSSearchForm" property="enddate"/>';
	var division = '<bean:write name="PVSSearchForm" property="division"/>';
	var partnerType = '<bean:write name="PVSSearchForm" property = "partnerType"/>';
	var type =''; 	
	
	if(startdate=='')type = 'PVSSearch';
	else type = 'PVSSearchDate';
		
	if(partnerType == '')partnerType = '0';
		str = 'SortAction.do?Type='+type+'&division='+division+'&orgTopName='+orgTopName+'&action='+action+'&startdate='+startdate+'&enddate='+enddate+'&partnerType='+partnerType;
		p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
		/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
	
}
</script>