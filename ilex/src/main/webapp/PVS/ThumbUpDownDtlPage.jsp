<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<html>
<head>
<title><bean:message bundle="PVS" key="pvs.partner.detail" /></title>
<style type="text/css">
.partnerName {
	float: left;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	word-wrap: break-word;
}

#pictureFrame img {
	border: 5px solid white;
	border-radius: 5px;
}
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<a onClick="document.all.ajaxdiv.innerHTML = '';" href="#"> <img
		src="images/box_close.gif" alt="" border="0" class="ACTBTN0001">
	</a>
	<table width="750px" border="0" cellspacing="0" cellpadding="1"
		class="ACTTBL0001">
		<tr>
			<td class="inside" width="300px"><b>Comment</b></td>
			<td class="inside" width="150px"><b>Job Title</b></td>
			<td class="inside" width="150px"><b>Updated By</b></td>
			<td class="inside" width="150px"><b>Date</b></td>
		</tr>
		<logic:iterate id="upDownList"  property="thumbUpDownList" name="PartnerDetailForm" indexId="count">
			<% String[] ar = (String[]) upDownList; %>
			<tr>					
					<td class="inside"><%= ar[0] %></td>
					<td class="inside"><%= ar[1] %></td>
					<td class="inside"><%= ar[3] %></td>
					<td class="inside"><%= ar[2] %></td>
			</tr>
		</logic:iterate>
		

	</table>
</body>
</html>
