<!DOCTYPE HTML>

<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on partners.
*
-->
<%@page import="java.util.Iterator,java.util.Set,java.util.Map,java.util.LinkedHashMap"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<bean:define id="dcpvs" name="dcpvs" scope="request" />
<bean:define id="dcPartnerType" name="dcPartnerType" scope="request" />
<bean:define id="dcRadiusRange" name="dcRadiusRange" scope="request" />
<bean:define id="dcCoreCompetancy" name="dcCoreCompetancy"
	scope="request" />
<bean:define id="dcTechCertifications" name="dcTechCertifications"
	scope="request" />
<jsp:include page="/common/IncludeGoogleKey.jsp" />

<html:html>
<HEAD>
<%@ include file="/Header.inc"%>


<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/summary.css" rel="stylesheet" type="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<title></title>
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language="JavaScript" src="javascript/JQueryMain.js"></script>
<script language="JavaScript" src="javascript/JQueryAjaxUtility.js?timestamp="+(new Date()*1)></script>

<script type="text/javascript">

/*Global variables*/
var geocoder; 
var location1; 
var location2;
var gDir;
var siteLat=0.0;
var siteLong=0.0;
var jobAddress;

function initialize() {
	/*Initialize GClientGeocoder*/
	geocoder = new GClientGeocoder();
}//- End initialize()

var flagVal = true;
function assignPartnerWithDirction(pid, address, city, state, country, partnerLat, partnerLong){
	/*This function call getDirectionWithLatLong() with partner id, partner latitude, partner longitude.*/	
	if($("input[name='selectedpartner']").val()== pid) {
		/*Check with existing assigned partner*/
		alert('PO cannot be reassigned to same partner.');
		return false;
	}
	
	/*
	This code will chech that if selected partner has a restriction list and containg the customer whoes PO is this then, it will show an alert and will force user to select 
	another partner for the current PO.
	*/
	
	$.ajax({
	        type: "GET",
	        url: "Partner_Search.do?ref=isPartnerRestricted&partnerId="+pid+"&jobId= "+<%=request.getParameter("typeid")%>,
	        async: false,
	        success: function (result) {
				flagVal = result;
	        }
	});
	
	 if (flagVal== 'false'){
     	alert("This partner is resticted by selected customer, can not be assigned to PO.");
 		return false;
     }
	

	
	var partnerAddress = address+","+city+","+state+","+country;
	getDirectionWithLatLong(pid,partnerLat,partnerLong); //- Assign partner
} //- End assignPartnerWithDirction()

/*
 * The following function is not used due to recreation of latitude and longitude of partner/site's.
 	Latitude and longirude of Partner and Site has been also created at server side so no need to create it here.
 */
function getDirectionWithAddress(pid,address1,address2,partnerLat,partnerLong) {
	/*In this function 
	  1. First check with geocoder.getLocations(address) to find accuracy of address.
	     If accuracy is not good then call server to assign partner with no 'Distance' and 'Duration'.
	  2. Call google.maps.Directions() to find Distance and Duration on the basis of given two addresses.
	  3. Call server to assign partner with 'Distance' and 'Duration'.
	*/
	geocoder.getLocations(address1, function (response) {
		if (!response || response.Status.code != 200){//- Address1 is not accurate.
			getDirectionWithLatLong(pid,partnerLat,partnerLong); // - Assign partner
		} else {
			location1 = {lat: response.Placemark[0].Point.coordinates[1], lon: response.Placemark[0].Point.coordinates[0], address: response.Placemark[0].address};
			geocoder.getLocations(address2, function (response) {//- Address2 is not accurate.
				if (!response || response.Status.code != 200){
					getDirectionWithLatLong(pid,partnerLat,partnerLong); //- Assign partner
				} else {
					location2 = {lat: response.Placemark[0].Point.coordinates[1], lon: response.Placemark[0].Point.coordinates[0], address: response.Placemark[0].address};
					gDir = new google.maps.Directions();
					gDir.load('from: ' + location1.address + ' to: ' + location2.address);
					
					
					getDirectionWithLatLong(pid,partnerLat,partnerLong); //- Assign partner
				}
			});
		}
	});
} //- End getDirectionWithAddress()

function getDirectionWithLatLong(pid, partnerLat, partnerLong){
	/*
	 This function
	 1. Fisrt validate all latitude and longitude.  
	 2. If not correct, Call google.maps.Directions() to find Distance and Duration 
	    on the basis given two pair of latitude and longitude. Call server to assign partner 
	    with found distance and duration.
	 3. Otherwise, Call server to assign partner with not distance and duration.   
	*/
	
	return assignpartner(pid, partnerLat, partnerLong, siteLat , siteLong);//- Assign Partner
}//- End getDirectionWithLatLong()

/*On page load set these default valuse and initialize GClientGeocoder()*/
 $(document).ready(function(){
	siteLat = $("input[name='siteLatitude']").val();
	siteLong = $("input[name='siteLongitude']").val();
	var siteAddress = $("input[name='siteAddress']").val();
	var siteCity = $("input[name='siteCity']").val();
	var siteState = $("input[name='siteState']").val();
	var siteCountry = $("input[name='siteCountry']").val();
	var zipCode= document.getElementById("zipCodeField").value;
	jobAddress = siteAddress+','+siteCity+','+siteState+','+siteCountry;
	initialize(); // - initialize GClientGeocoder()
	if(zipCode==''){
	if(siteCountry!=''){
		$('#IntCountry').val(siteCountry);
	 $.getJSON("Partner_Search.do?ref=findCity&IntCountry="+siteCountry,{ajax: 'true'}, function(j){
         var options = '';
             for (var i = 0; i < j.length; i++) {
            	 if(siteCity!="" && j[i].value==siteCity){
            		  options += '<option value="' + j[i].value + '" selected>' + j[i].label + '</option>';	 
            	 }else{
                 options += '<option value="' + j[i].value + '">' + j[i].label + '</option>';
            	 }
              
             }   
             $("select#cityNameOptions").html(options);
        });
	
	}
	}
	
}); 
</script>

<script>
//Dynamic Changing the Site Identifier Based on the Country Selected
$(document).ready(function(){		
	
	$('#IntCountry').change(function() {
		$("input[name='siteCountry']").val($('#IntCountry').val());
		document.getElementById("zipCodeField").value="";	
	
                $.getJSON("Partner_Search.do?ref=findCity&IntCountry="+$('#IntCountry').val(),{ajax: 'true'}, function(j){
                var options = '';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].value + '">' + j[i].label + '</option>';
                     
                    }  
                    $("select#cityNameOptions").html(options);
                   
        });
	});
	
	//if ZIP Code is provided the no need to provide COUNTRY and city....
	$('#zipCodeField').change(function(){
		 var options = '';
		
		 options = '<option value=0> --Select--</option>';
		 document.getElementById("IntCountry").options[0].selected=true;
		 $("input[name='siteCountry']").val(document.getElementById("IntCountry").options[0].value)
		 $("select#cityNameOptions").html(options);
		document.getElementById("cityNameOptions").options[0].selected=true;
	});
		
	
	
});

function clearRecommendedList(){
	var abc = document.getElementById("selectedRecommendedPartnerList");
	for (i=0;i<abc.options.length;i++) {
		abc.options[i].selected = false;
 	}
}

function clearAllSelectLists(){
	var abc = document.getElementById("coreCompetencyId");
	for (i=0;i<abc.options.length;i++) {
		abc.options[i].selected = false;
 	}
	
	abc = document.getElementById("techniciansId");
	for (i=0;i<abc.options.length;i++) {
		abc.options[i].selected = false;
 	}
	
	abc = document.getElementById("toolKitArrId");
	for (i=0;i<abc.options.length;i++) {
		abc.options[i].selected = false;
 	}
}

function clearRestrictedList(){
	var abc = document.getElementById("selectedRestrictedPartnerList");
	for (i=0;i<abc.options.length;i++) {
		abc.options[i].selected = false;
 	}
}
</script>


</head>
<%
		String heading = "";
		String fromtype = "";
		String oldPartnerType = "";
		String zipCodeValue = "";

		if (request.getParameter("type").equalsIgnoreCase("v")) {
			heading = "pvs.certifiedpartnersummaryview";
		} else {
			heading = "pvs.assignpartner";
		}

		if (request.getAttribute("fromtype") != null) {
			fromtype = "" + request.getAttribute("fromtype");
		}

		if (request.getAttribute("zipCodeValue") != null) {
			zipCodeValue = "" + request.getAttribute("zipCodeValue");
		}

		String backgroundclass = null;
		String backgroundhyperlinkclass = null;
		String backgroundclasstoleftalign = null;
		boolean csschooser = true;
%>

<body id="pbody" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%
	if (request.getParameter("type").equalsIgnoreCase("v")) {
%>
<div id="menunav">
    <ul>
  		<li>
        	<a href="#" class="drop"><span><bean:message bundle="PVS" key="pvs.managepartner" /></span></a>
  			<ul>
  				<li>
		            <a href="#"><span>Minuteman</span></a>
		            <ul>
		      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners">Add</a></li>
		      			<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=update">Update</a></li>
		      			<%
		      				if (session.getAttribute("RDM") != null
		      								&& session.getAttribute("RDM").equals("Y")) {
		      			%>
		      				<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=delete">Delete</a></li>	      				
							<%
	      												}
	      											%>
	      			</ul>
	        	</li>
  				<li>
		            <a href="#"><span>Standard Partner</span></a>
		            <ul>
		      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners">Add</a></li>
		      			<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=update">Update</a></li>
		      			<%
		      				if (session.getAttribute("RDM") != null
		      								&& session.getAttribute("RDM").equals("Y")) {
		      			%>
		      				<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=delete">Delete</a></li>	      				
							<%
	      												}
	      											%>
	      			</ul>
	        	</li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=newadded&enablesort=true">New Partners</a></li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true">Recent Updates</a></li>
 				<li><a href="PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true">Annual Review Due</a></li>
  			</ul>
  		</li>
  		<li><a href="Partner_Search.do?ref=search&type=v" class="drop"><bean:message bundle="PVS" key="pvs.partnersearch" /></a></li>
  		<li><a href="MCSA_View.do" class="drop"><bean:message bundle="PVS" key="pvs.managemcsa" /></a></li>
	</ul>
</div>
<%
	}
%>

<%
	if (request.getParameter("type").equals("J")
				|| request.getParameter("type").equals("AJ")
				|| request.getParameter("type").equals("CJ")
				|| request.getParameter("type").equals("IJ")
				|| request.getParameter("type").equals("A")
				|| request.getParameter("type").equals("AA")
				|| request.getParameter("type").equals("CA")
				|| request.getParameter("type").equals("IA")) {
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="toprow">
		<table width="800" border="0" cellspacing="1" cellpadding="0"
			height="18">
			<tr align="left">
				<td width="200" class="toprow1">
				<%
					if (request.getParameter("type").equals("J")
									|| request.getParameter("type").equals("AJ")
									|| request.getParameter("type").equals("CJ")
									|| request.getParameter("type").equals("IJ")) {
				%> <a
					href="JobHeader.do?function=view&jobid=<%=request.getParameter("typeid")%>"
					onmouseout="MM_swapImgRestore();"
					onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);"
					class="drop"><bean:message bundle="PVS"
					key="pvs.backtojobdashboard" /> <%
 	}
 %> <%
 	if (request.getParameter("type").equals("A")
 					|| request.getParameter("type").equals("AA")
 					|| request.getParameter("type").equals("CA")
 					|| request.getParameter("type").equals("IA")) {
 %> <a
					href="ActivityHeader.do?function=view&activityid=<%=request.getParameter("typeid")%>"
					onmouseout="MM_swapImgRestore();"
					onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);"
					class="drop"><bean:message bundle="PVS"
					key="pvs.backtoactivitydashboard" /> <%
 	}
 %> </a></td>
				<td width=600 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%
	}
%>



<table>
	<html:form action="Partner_Search">
		<html:hidden property="ref" />
		<html:hidden property="fromprm" />
		<html:hidden property="type" />
		<html:hidden property="typeid" />
		<html:hidden property="assigncheck" />
		<html:hidden property="formtype" />
		<html:hidden property="viewjobtype" />
		<html:hidden property="ownerId" />
		<html:hidden property="powoId" />
		<html:hidden property="search" />
		<html:hidden property="selectedpartner" />
		<html:hidden property="currentPartner" />
		<html:hidden property="hiddensave" />
		<html:hidden property="tabId" />
		<html:hidden property="isClicked" />
		<html:hidden property="reAssign" />
		<html:hidden property="appendixId" />
		<html:hidden property="assign" />

		<html:hidden property="travelDistance" />
		<html:hidden property="travelDuration" />
		<html:hidden property="siteLatitude" />
		<html:hidden property="siteLongitude" />
		<html:hidden property="siteAddress" />
		<html:hidden property="siteCity" />
		<html:hidden property="siteState" />
		<html:hidden property="siteCountry" />
		
		<logic:present name="partnerDeleted" scope="request">
			<script>
				parent.ilexleft.location.reload();
			</script>
		</logic:present>

		<div class="labeleboldwhite" height="30" style="width: 800px;">
			<bean:message bundle="PVS" key="<%=heading %>" />
			<%
				if (fromtype.equals("powo")) {
			%>
 				&nbsp;<bean:message bundle="PVS" key="pvs.forJob" />:&nbsp;<bean:write name="Partner_SearchForm" property="jobName" />,&nbsp;<bean:message bundle="PVS" key="pvs.po" />:&nbsp;<bean:write name="Partner_SearchForm" property="powoId" />
 				<%
 					}
 				%>
		</div>
		<div class="partnersearchtables">
			<div class="labeleboldrightborder partnersearchtable partnersearchtable1">
	            <dl class="labelebold ctrlUnit">
	            	<dt><bean:message bundle="PVS" key="pvs.name" /></dt>
					<dd><html:text styleClass="txtCtrl" size="20" name="Partner_SearchForm" property="name" maxlength="50" /></dd>
	            </dl>
	            <dl class="labelebold ctrlUnit">
	            	<dt><bean:message bundle="PVS" key="pvs.keyword" /></dt>
            		<dd><html:text styleClass="txtCtrl" size="20" name="Partner_SearchForm" property="searchTerm" /></dd>
	            </dl>
	            <dl class="labelebold ctrlUnit">
            		<dt><bean:message bundle="PVS" key="pvs.partnertype" /></dt>
	            	<dd>
	            		<%
	            			LinkedHashMap partnerTypeList = (LinkedHashMap) request
	            							.getAttribute("partnerTypeList");
	            					Set s = partnerTypeList.entrySet();
	            					Iterator it = s.iterator();
	            					while (it.hasNext()) {
	            						// key=value separator this by Map.Entry to get key and value
	            						Map.Entry m = (Map.Entry) it.next();
	            						// getKey is used to get key of Hashtable
	            						String key = (String) m.getKey();
	            						// getValue is used to get value of key in Hashtable
	            						String value = (String) m.getValue();
	            		%>
							<span class="checkboxspan"><html:multibox property="checkedPartnerName" name="Partner_SearchForm" value="<%=key.toString() %>" >
							</html:multibox><span class="checkboxlabelspan"><%=value%></span></span>
							<%
								}
							%>
	            	</dd>
	            </dl>
	            <dl class="labelebold ctrlUnit">
	            	<dt><bean:message bundle="PVS" key="pvs.badgeId" /></dt>
	            	<dd>
						<span class="checkboxspan"><html:radio property="cpdBadgeId" value="A" /><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.search.badge.all" /></span></span>
						<span class="checkboxspan"><html:radio property="cpdBadgeId" value="P" /><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.search.badge.printed" /></span></span>
	                	<span class="checkboxspan"><html:radio property="cpdBadgeId" value="N" /><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.search.badge.none" /></span></span>	                	
	              	</dd>
	            </dl>
	            
	            <dl class="labelebold ctrlUnit" title="Background Check Document Received">
	             <dt><bean:message bundle="PVS" key="pvs.BackgroundCheck" /></dt>
	                <dd><html:checkbox name="Partner_SearchForm" property="cbox3" value="Y" /></dd>
	            </dl>
	             <dl class="labelebold ctrlUnit" title="Drug Screen Document Received">
	             <dt><bean:message bundle="PVS" key="pvs.DrugScreen" /></dt>
	                <dd><html:checkbox name="Partner_SearchForm" property="cboxDrug" value="Y" /></dd>
	            </dl>         
	            
	            
	            
	            
	            
	            
	            
	            
	            <dl class="labelebold ctrlUnit">
	            	<dt><bean:message bundle="PVS" key="pvs.mcsaVersion1" /></dt>
	            	<dd>
	            		<html:select property="mcsaVersion" size="1" styleClass="comboe txtCtrl">
							<html:optionsCollection name="mcsaVersionCombo" property="mcsaVersion" label="label" value="value" />
						</html:select>
	              	</dd>
	            </dl>
			</div>
			<div class="labeloboldrightbottomborder partnersearchtable partnersearchtable2">
				<%--Changes Start by Yogendra For Adding label On partner Search Screen --%>
				<dl class="labelobold ctrlUnit">
					<dt><bean:message bundle="PVS" key="pvs.USAndCanada" />:</dt><dd style="height: 15px;"></dd>
				</dl>
				<%--Changes End by Yogendra For Adding label On partner Search Screen --%>
				<dl class="ctrlUnit" style="height: 15px;">
					<dt class="labelobold"><bean:message bundle="PVS" key="pvs.siteZipCode" /></dt>
	                <dd class="ddshort"><html:text styleClass="UpperCasetextbox txtCtrlShort" size="8" name="Partner_SearchForm" styleId="zipCodeField" property="zipcode" /></dd>
					<dt class="labelo dtinline"><bean:message bundle="PVS" key="pvs.radius" /></dt>
	                <dd>
	               		<html:select property="radius" size="1" styleClass="comboo txtCtrlShort">
	               			<html:optionsCollection name="dcRadiusRange" property="firstlevelcatglist" label="label" value="value" />
		                </html:select>
	                </dd>
				</dl>
	            <div class="labelo checkboxspanning">
		            <span class="checkboxspaninline"><html:checkbox property="mainOffice" value="Y"></html:checkbox><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.mainOffice" /></span></span>
		            <span class="checkboxspaninline"><html:checkbox property="fieldOffice" value="Y"></html:checkbox><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.fieldOffices" /></span></span>
		            <span class="checkboxspaninline"><html:checkbox property="techniciansZip" value="Y"></html:checkbox><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.technicians" /></span></span>
	            </div>
	            <%--Changes Start by Yogendra For Adding International Section On partner Search Screen --%>
	            <dl class="labelobold ctrlUnit">
					<dt>International:</dt><dd style="height: 15px;"></dd>
				</dl>
	            <dl class="ctrlUnit">
					<dt class="labelo">Country</dt>
	                <dd>
		                <html:select property="selectedCountryName" styleId="IntCountry" size="1" styleClass="comboo txtCtrlWide">
							<html:optionsCollection name="Partner_SearchForm"	property="countryNameValueList" label="label" value="value" />
						</html:select>
	                </dd>
					<dt class="labelo">City</dt>
	                <dd>
						<html:select property="selectedCityName" styleId="cityNameOptions" size="1" styleClass="comboo txtCtrlWide">
							<html:optionsCollection name="Partner_SearchForm" property="cityNameValueList" label="label" value="value" />
						</html:select>
	                </dd>
				</dl>
				<%--Changes End by Yogendra For Adding International Section On partner Search Screen --%>
	            <hr>
	            <dl class="ctrlUnit">
					<dt><bean:message bundle="PVS" key="pvs.search.quality.rating" /></dt>
					<dd>
	                	<span class="checkboxspan"><html:radio property="partnerRating" value="N" /><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.search.quality.none" /></span></span>
	                	<span class="checkboxspan"><html:radio property="partnerRating" value="4" /><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.search.quality.four" /></span></span>
	                	<span class="checkboxspan"><html:radio property="partnerRating" value="3" /><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.search.quality.three" /></span></span>
	                	<span class="checkboxspan"><html:radio property="partnerRating" value="A" /><span class="checkboxlabelspan"><bean:message bundle="PVS" key="pvs.search.quality.all" /></span></span>
	                </dd>
				</dl>
			</div>
			<div class="labeleboldwithoutrightborder partnersearchtable partnersearchtable3">
				<dl class="ctrlUnit">
					<dt class="labelebold"><bean:message bundle="PVS" key="pvs.partneredit.technicianinfo" /></dt>
					<dd>
						<html:select name="Partner_SearchForm" size="4" property="technicians" styleId="techniciansId" styleClass="comboe txtCtrlWide">
							<html:option value="">--Select--</html:option>
							<html:optionsCollection name="dcTechCertifications" property="firstlevelcatglist" label="label" value="value" />
						</html:select>
						<span class="checkboxspan"><html:checkbox property="securityCertiftn" value="Y"></html:checkbox><span class="checkboxlabelspan">Security Certifications</span></span>
					</dd>
				</dl>
				<dl class="ctrlUnit">
					<dt class="labelebold"><bean:message bundle="PVS" key="pvs.certifications" /></dt>
					<dd>
						<logic:present name="initialStateCategory">
							<html:select name="Partner_SearchForm" property="certifications" styleClass="comboe txtCtrlWide">
								<logic:iterate id="initialStateCategory" name="initialStateCategory">
									<bean:define id="catname" name="initialStateCategory" property="certificationType" />
									<%
										if (!catname.equals("notShow")) {
									%>
										<optgroup class=select label="<%=catname%>">
										<%
											}
										%>
									<html:optionsCollection name="initialStateCategory" property="tempList" value="value" label="label" />
									<%
										if (!catname.equals("notShow")) {
									%>
										</optgroup>
										<%
											}
										%>
								</logic:iterate>
							</html:select>
						</logic:present>
					</dd>
				</dl>
				<dl class="ctrlUnit">
					<dt class="labelebold"><bean:message bundle="PVS" key="pvs.corecompetency" /></dt>
                	<dd>
                		<html:select name="Partner_SearchForm" size="4" property="coreCompetency" styleId="coreCompetencyId" styleClass="comboe testtest txtCtrlWide">
							<html:option value="">--Select--</html:option>
							<html:optionsCollection name="dcCoreCompetancy" property="firstlevelcatglist" label="label" value="value" />
						</html:select>
                	</dd>
				</dl>
				<dl class="ctrlUnit">
					<dt class="labelebold"><bean:message bundle="PVS" key="pvs.toolKits" /></dt>
               		<dd>
               			<html:select name="Partner_SearchForm" property="toolKitArr" size="4" styleId="toolKitArrId" styleClass="comboe txtCtrlWide" multiple="true">
							<html:option value="">--Select--</html:option>
							<html:optionsCollection name="dcToolKit" property="toolKitlist" label="label" value="value" />
						</html:select>
               		</dd>
				</dl>
			</div>
            <div class="labeloboldrightbottomborder partnersearchtable partnersearchtable4">
				<dl class="ctrlUnit inline">
					<dt class="labelobold splitlabelheader">Client Recommendations</dt>
					<dd>
						<html:select name="Partner_SearchForm" property="recommendedArr" size="15" styleClass="comboo txtCtrl" multiple="true" styleId="selectedRecommendedPartnerList" style="width: 160px;">
							<html:optionsCollection name="recommendedComboCM" property="recommendedlist" label="label" value="value" />
						</html:select>
					</dd>
				</dl>
			</div>
            <div class="labeloboldrightbottomborder partnersearchtable partnersearchtable5">
				<dl class="ctrlUnit inline">
					<dt class="labelobold splitlabelheader">Client Restriction</dt>
					<dd>
						<html:select styleId="selectedRestrictedPartnerList" name="Partner_SearchForm" property="restrictedArr" size="15"  style="width: 160px;" styleClass="comboo txtCtrl" multiple="true">
							<html:optionsCollection name="restrictedComboCM" property="restrictedlist" label="label" value="value" />
						</html:select>
                	</dd>
				</dl>
            </div>
		</div>
		<div class="partnersearchtables">
            <div class="partnersearchbuttons partnersearchtable1">
            	<div class="submitUnit">
            		<html:button property="search" styleClass="button" onclick="return validate();">
            			<bean:message bundle="PVS" key="pvs.search" />
           			</html:button>
           			<html:reset property="reset" styleClass="button">
						<bean:message bundle="PVS" key="pvs.reset" />
					</html:reset>
					<%
						if (fromtype.equals("powo")) {
					%>
						<html:button property="back" styleClass="button" onclick="return Backaction();">
							<bean:message bundle="PVS" key="pvs.back" />
						</html:button>
						<%
							}
						%>
            	</div>
            </div>
            <div class="partnersearchbuttons partnersearchtable2"></div>
            <div class="partnersearchbuttons partnersearchtable3">
            	<dl class="submitUnit ctrlUnit">
            		<dt>
            		
            			<%
            		            				if (!fromtype.equals("powo")) {
            		            			%>
            			<html:select property="cmboxStatus" size="1"
							styleClass="comboo">
							<html:option value="ALL">ALL</html:option>
							<html:optionsCollection name="dcPartnerstatus"
								property="partnerstatus" label="label" value="value" />
						</html:select>
            			<%
            				} else {
            			%>
	               				<html:hidden property="cmboxStatus" value="Special Restriction" />
	               			<%
	               				}
	               			%>
						<html:button property="clear3" styleClass="button" onclick="clearAllSelectLists();">Clear</html:button>
           			</dt>
                	<dd>
                		<span class="checkboxspaninline">
	                		<span class="checkboxlabelspan" style="font-size: 13px;"><bean:message bundle="PVS" key="pvs.exclude" />:</span>
	                		<html:checkbox name="Partner_SearchForm" property="cbox1" value="Y" />
	                		<span class="checkboxlabelspan">
	                			<a style="text-decoration: none;" href="#" target="ilexmain" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/2.gif',1)">
	                				<img name="Image1" src="images/2.gif" border="0" alt="<bean:message bundle="PVS" key="pvs.partnerswithincidents"/>">
	               				</a>
	               			</span>
	               		</span>

							<%
								if (!fromtype.equals("powo")) {
							%>
							<span class="checkboxspaninline">
	                		<html:checkbox name="Partner_SearchForm" property="cbox2" value="Y" />
	                			<a href="#" target="ilexmain" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/1.gif',1)">
									<img name="Image2" src="images/1.gif" border="0" alt="<bean:message bundle="PVS" key="pvs.partnerswhosemcsaterminated"/>">
								</a>
	               			</span>
	               			<%
	               				} else {
	               			%>
	               				<html:hidden name="Partner_SearchForm" property="cbox2" value="Y" />
	               			<%
	               				}
	               			%>
	               		</span>
	               		
                	</dd>
            	</dl>
            </div>
            <div class="partnersearchbuttons partnersearchtable4">
            	<div class="submitUnit center">
            		<html:button property="clear1" styleClass="button" onclick="clearRecommendedList();">Clear</html:button>
            	</div>
            </div>
            <div class="partnersearchbuttons partnersearchtable5">
            	<div class="submitUnit center">
            		<html:button property="clear2" styleClass="button" onclick="clearRestrictedList()">Clear</html:button>
            	</div>
            </div>
		</div>

		<table border="0" cellspacing="1" cellpadding="2">
			<tr>
				<td></td>
			</tr>
		</table>


		<!-- V stands for control from PVS Manager  -->
		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td style="padding-left: 6px">
				<table border="0" cellspacing="1" cellpadding="0">
					<logic:present name="partnersearchlist" scope="request">
						<%
							int colspan = 0;
										if (!zipCodeValue.equals("")) {
											colspan = 16;
										} else {
											colspan = 14;
										}
						%>
						<tr>
							<%
								if (fromtype.equals("powo")) {
							%>
							<td class="colLight" align="right" colspan="16"><html:button
								property="gMap" styleClass="buttonSearch" onclick="showGMap();">
								<bean:message bundle="PVS" key="pvs.search.standardMap" />
							</html:button> <html:button property="vendexMap" styleClass="buttonSearch"
								onclick="showGMapforvendex();">
								<bean:message bundle="PVS" key="pvs.search.vendexMap" />
							</html:button></td>

							<%
								} else {
							%>
							<td class="colLight" align="right" colspan="<%=colspan%>">
							 <c:if test="${Partner_SearchForm.csvExportStatus  eq '1'}">
							 <html:button
								property="exportToExcel" styleClass="button" onclick="exportExcel();">
								CSV Export
							</html:button>
							 </c:if>
							<html:button
								property="gMap" styleClass="button" onclick="showGMap();">
								<bean:message bundle="PVS" key="pvs.search.mapview" />
							</html:button></td>
							<%
								}
							%>
						</tr>
					</logic:present>
					<logic:notPresent name="partnersearchlist" scope="request">
						<tr>
							<td>&nbsp;</td>
						</tr>
					</logic:notPresent>
					<logic:present name="partnersearchlist" scope="request">
						<tr>
 							<td class="Ntrybgray" rowspan="2" >Status</td> 
							<td class="Ntrybgray" rowspan="2"  nowrap="nowrap" style="width: 100px;">Partner Name</td>
							<td class="Ntrybgray" rowspan="2"  nowrap="nowrap" style="width: 75px;" title="Jobs ever rated">User</br> Rating</td>
							<td class="Ntrybgray" rowspan="2" title="Total number of completed jobs by partner" >Jobs</br> Ordered</td>
							<td class="Ntrybgray" rowspan="2" >% </br>Completed</td>
							<td class="Ntrybgray" rowspan="2" >%</br>On Time</td>
							<td class="Ntrybgray" rowspan="2" >Last used<sup>1</sup></td>
							<td class="Ntrybgray" rowspan="2" >Distance</td>
							<td class="Ntrybgray" rowspan="2" >City</td>
							<td class="Ntrybgray" rowspan="2" >State</td>
							

							<td class="Ntrybgray" rowspan="2">Zip Code</td>
							<td class="Ntrybgray" rowspan="2">Cell Phone</td>

							<%
								if (fromtype.equals("powo")
													|| (request.getParameter("type")
															.equalsIgnoreCase("v") && !zipCodeValue
															.equals(""))) {
							%>
							<td class="Ntrybgray" rowspan="2"><bean:message bundle="PVS"
								key="pvs.search.bid" /></td>
							<td class="Ntrybgray" rowspan="2"><bean:message bundle="PVS"
								key="pvs.search.vendexMatches" /></td>
							<%
								}
							%>


							<td class="Ntrybgray" rowspan="2"><bean:message bundle="PVS"
								key="pvs.search.adpo" /></td>
							
						</tr>
				<tr> 

					</tr> 

						<%
 							int i = 1;
 						%>
								<tr>
									<td class="Ntrybgray" style="text-align: left;height: 19px;" colspan="16">
									Minuteman</td>
								</tr>
								
								
								<logic:iterate id="pslist" name="partnersearchlist">
								<logic:equal property="partnerType" value="Minuteman" name="pslist">
								
								
								
								
							<bean:define id="PartType" name="pslist" property="partnerType"
								type="java.lang.String" />
							<bean:define id="pID" name="pslist" property="pid" />
							<bean:define id="P" name="pslist" property="pid" />
							<bean:define id="badgePrinted" name="pslist" property="badgePrinted" />
							<bean:define id="status" name="pslist" property="status" />
							
							<bean:define id="color" name="pslist" property="icontype"
								type="java.lang.String" />
							<bean:define id="partnerAddress1" name="pslist"
								property="partnerAddress1" type="java.lang.String" />
							<bean:define id="city" name="pslist" property="city"
								type="java.lang.String" />
							<bean:define id="state" name="pslist" property="state"
								type="java.lang.String" />
							<bean:define id="country" name="pslist" property="country"
								type="java.lang.String" />
								<bean:define id="leadMinuteman" name="pslist" property="leadMinuteman"
								type="java.lang.Boolean" />
							<%
								if (partnerAddress1.indexOf("'") > -1) {
														partnerAddress1 = partnerAddress1
																.replaceAll("'", " ");
													}
													if (city.indexOf("'") > -1) {
														city = city.replaceAll("'", " ");
													}
													if (state.indexOf("'") > -1) {
														state = state.replaceAll("'", " ");
													}
													if (country.indexOf("'") > -1) {
														country = country.replaceAll("'", " ");
													}
							%>
							<%
								if (csschooser == true) {

														backgroundclass = "Ntexto";
														backgroundclasstoleftalign = "Ntextoleftalignnowrap";
														backgroundhyperlinkclass = "Nhyperodd";
														csschooser = false;
													} else {
														csschooser = true;
														backgroundclass = "Ntexte";
														backgroundclasstoleftalign = "Ntexteleftalignnowrap";
														backgroundhyperlinkclass = "Nhypereven";
													}

													if (color.endsWith("red")) {
														backgroundclass = "Ntextpred";
													} else if (color.endsWith("green")) {
														backgroundclass = "Ntextpgreen";
													} else if (color.endsWith("yellow")) {
														backgroundclass = "Ntextpyellow";
													} else if (color.endsWith("orange")) {
														backgroundclass = "Ntextporange";
													}
													if (leadMinuteman == true) {
														backgroundclass = "NtextLeadMinuteMan";
													}
							%>
							

							<tr>
							<td  class="<%=backgroundclass%>" noWrap="noWrap" height="10">
							
							<logic:equal name="pslist" property="incTypeFlag" value="Y">
												<img src="images/2.png" border="0">											
											</logic:equal>
											<logic:equal
												name="pslist" property="status" value="Dormant">
												<img src="images/dormant.png" border="0">
											</logic:equal>
											<%
												if (leadMinuteman == true) {
											%>
												<img src="images/3.png" border="0"/>
											<%
												}
											%>
												 <logic:equal name="pslist" property="deliverableSource" value="Mobile App">
												<img src="images/iphone.png" border="0">
											</logic:equal>
							 <logic:equal name="pslist" property="barredflag" value="Y">
													<img src="images/1.gif" border="0">
												</logic:equal>
							
							</td>
											<td class="<%=backgroundclass%>" noWrap="noWrap" height="10" title="<bean:write name="pslist" property="partnername" />"
												style=""> 
												<a href="javascript:void(0)"
												onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); 
												else showWindow('',<%=(String) pID%>);"><img src="<%if (!status.equals("Incomplete")) {
									if (badgePrinted.equals("true")) {%>images/details_with_badge.png<%} else {%>images/details.png<%}
								} else {%>images/details_yellow.png<%}%>" border="0" align="absmiddle" 
												style="width: 12px;padding-right: 2px;" /></a>
												
												
											<a href="Partner_Edit.do?pid=<bean:write name="pslist" 
											property="pid"/>&function=add&fromtype=<%=fromtype%>&jobid=<%=request.getParameter("typeid")%>&showViewPage=y"><bean:write name="pslist" property="partnernameTemp" /></a>
											</td>

											<td class="<%=backgroundclass%>" noWrap="noWrap"><logic:notEqual
													name="pslist" property="thumbsUpCount" value="">
													<div
														style="width: 19px; text-align: right;float: left;">
														<bean:write name="pslist" property="thumbsUpCount" />
													</div>

													<div style="width: 16px; padding-right: 2px; float: left;">
														<a href="javascript:void(0)"
															onClick="showUpDownComments(event,'up',<%=(String) pID%>)"><img
															src="images/up.png"></a>
													</div>
												</logic:notEqual> <logic:notEqual name="pslist" property="thumbsDownCount"
													value="">
													<div
														style="float: left; width: 19px; text-align: right;">
														<bean:write name="pslist" property="thumbsDownCount" />
													</div>
													<div style="float: left;width: 16px;">
														<a href="javascript:void(0)"
															onClick="showUpDownComments(event,'down',<%=(String) pID%>)"><img src="images/down.png"></a>
													</div></logic:notEqual>
												</td>

											<td class="<%=backgroundclass%>" align="center">
										<bean:write 
	 									name="pslist" property="contractedCount" />
</td>
										<td class="<%=backgroundclass%>" align="center"><bean:write 
									name="pslist" property="compeletedCount" /></td>
										
										
										
										
										
										
										<td class="<%=backgroundclass%>" noWrap="noWrap" style="text-align: right;">
										 <logic:notEqual name="pslist" property="onTimePercentage" value="" > 
										 	
											&nbsp;&nbsp;<bean:write name="pslist" property="onTimePercentage"/>
											
											
										</logic:notEqual> 
										 <logic:equal name="pslist" property="onTimePercentage" value="" >
										 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</logic:equal> 
										
										
										<%
 																															if (fromtype.equals("powo")) {
 																														%>
										<a
											href="javascript:void(0)"
											onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); else showWindow('',<%=(String) pID%>);">
										<bean:message bundle="PVS" key="pvs.search.details" />
											</a>[<a href="#"
											onclick="return assignPartnerWithDirction(<%=(String) pID%>,'<%=partnerAddress1%>','<%=city%>','<%=state%>','<%=country%>',<bean:write name="pslist" property="partnerLatitude"/>,<bean:write name="pslist" property="partnerLongitude"/>);"><bean:message
											bundle="PVS" key="pvs.search.standardPO" /></a>]
											[<a href="#"><bean:message	bundle="PVS" key="pvs.search.multiplePO" /></a>]
										<%
												} else {
											%>
										<a
											href="javascript:void(0)"
											onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); else showWindow('',<%=(String) pID%>);">
											</a>
										<%
											}
										%>
								</td>

								<bean:define id="updateDated" name="pslist" property="updateDate" type="java.lang.String" />
								<bean:define id="createDated" name="pslist" property="createDate" type="java.lang.String" />
								<bean:define id="lastDated" name="pslist" property="partnerLastUsed" type="java.lang.String" />
								
								<td class="<%=backgroundclass%>" align="center">
								<%
									if ("".equalsIgnoreCase(lastDated.trim())) {
															if (!"".equalsIgnoreCase(updateDated.trim())) {
								%>
									<b><bean:write	name="pslist" property="updateDate" /></b>
									<%
										} else {
									%>
									<b><bean:write	name="pslist" property="createDate" /></b>
								<%
									}
														} else {
								%>
									<bean:write	name="pslist" property="partnerLastUsed" />
								<%
									}
								%>
								</td>
								
								<%--Changes end for Showing update or Created date when LastDate is not present --%>
								<td class="<%=backgroundclass%>" align="right"><bean:write
									name="pslist" property="distance" /></td>
								
								<td title="<bean:write name="pslist" property="city" />" class="<%=backgroundclass%>"><bean:write name="pslist" property="cityTemp" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="state" /></td>
									<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="zipcode" /></td>
									<td class="<%=backgroundclass%>" title="Call: <bean:write name="pslist" property="partnerPriCellPhone" />">
									<a href="tel: <bean:write name="pslist" property="partnerPriCellPhonePlain" />" >
									<img src="images/call.png" alt="Call" border="0" width="12" height="12" /></a>
									&nbsp;<bean:write name="pslist" property="partnerPriCellPhoneTemp" />
									</td>

								<%
									if (fromtype.equals("powo")
																|| (request.getParameter("type")
																		.equalsIgnoreCase("v") && !zipCodeValue
																		.equals(""))) {
								%>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="bid" /></td>
								<td class="<%=backgroundclass%>" align="right"><a
									href="javascript:showGMapVendexMaches(<bean:write name="pslist" property="pid"/>);"><bean:write
									name="pslist" property="vendexMatches" /></a></td>
								<%
									}
								%>

								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesProfessional" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesKnowledgeable" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesEquipped" /></td>
								<td class="<%=backgroundclass%>" align="right"><bean:write
									name="pslist" property="partnerAdpo" /></td>
											 			
							</tr>
							
							</logic:equal>
						</logic:iterate>
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						<tr>
									<td class="Ntrybgray" style="text-align: left;height: 19px;" colspan="16">
									SpeedPay</td>
								</tr>
							
							
							<logic:iterate id="pslist" name="partnersearchlist">
								<logic:equal property="partnerType" value="Certified Partners - Speedpay Users" name="pslist">
								
								
								
								
							<bean:define id="PartType" name="pslist" property="partnerType"
								type="java.lang.String" />
							<bean:define id="pID" name="pslist" property="pid" />
							<bean:define id="P" name="pslist" property="pid" />
							<bean:define id="badgePrinted" name="pslist" property="badgePrinted" />
							<bean:define id="status" name="pslist" property="status" />
							<bean:define id="color" name="pslist" property="icontype"
								type="java.lang.String" />
							<bean:define id="partnerAddress1" name="pslist"
								property="partnerAddress1" type="java.lang.String" />
							<bean:define id="city" name="pslist" property="city"
								type="java.lang.String" />
							<bean:define id="state" name="pslist" property="state"
								type="java.lang.String" />
							<bean:define id="country" name="pslist" property="country"
								type="java.lang.String" />
								<bean:define id="leadMinuteman" name="pslist" property="leadMinuteman"
								type="java.lang.Boolean" />
							<%
								if (partnerAddress1.indexOf("'") > -1) {
														partnerAddress1 = partnerAddress1
																.replaceAll("'", " ");
													}
													if (city.indexOf("'") > -1) {
														city = city.replaceAll("'", " ");
													}
													if (state.indexOf("'") > -1) {
														state = state.replaceAll("'", " ");
													}
													if (country.indexOf("'") > -1) {
														country = country.replaceAll("'", " ");
													}
							%>
							<%
								if (csschooser == true) {

														backgroundclass = "Ntexto";
														backgroundclasstoleftalign = "Ntextoleftalignnowrap";
														backgroundhyperlinkclass = "Nhyperodd";
														csschooser = false;
													} else {
														csschooser = true;
														backgroundclass = "Ntexte";
														backgroundclasstoleftalign = "Ntexteleftalignnowrap";
														backgroundhyperlinkclass = "Nhypereven";
													}

													if (color.endsWith("red")) {
														backgroundclass = "Ntextpred";
													} else if (color.endsWith("green")) {
														backgroundclass = "Ntextpgreen";
													} else if (color.endsWith("yellow")) {
														backgroundclass = "Ntextpyellow";
													} else if (color.endsWith("orange")) {
														backgroundclass = "Ntextporange";
													}
													if (leadMinuteman == true) {
														backgroundclass = "NtextLeadMinuteMan";
													}
							%>
		
							<tr>
							<td  class="<%=backgroundclass%>" noWrap="noWrap" height="10">
							
							<logic:equal name="pslist" property="incTypeFlag" value="Y">
												<img src="images/2.png" border="0">											
											</logic:equal>
											<logic:equal
												name="pslist" property="status" value="Dormant">
												<img src="images/dormant.png" border="0">
											</logic:equal>
											<%
												if (leadMinuteman == true) {
											%>
												<img src="images/3.png" border="0"/>
											<%
												}
											%>
												 <logic:equal name="pslist" property="deliverableSource" value="Mobile App">
												<img src="images/iphone.png" border="0">
											</logic:equal>
							<logic:equal name="pslist" property="barredflag" value="Y">
												<img src="images/1.gif" border="0">
											</logic:equal>							
							
							</td>
								<td class="<%=backgroundclass%>" noWrap="noWrap" height="10" title="<bean:write name="pslist" property="partnername" />"
												style=""> 
												<a href="javascript:void(0)"
												onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); 
												else showWindow('',<%=(String) pID%>);"><img src="<%if (!status.equals("Incomplete")) {
									if (badgePrinted.equals("true")) {%>images/details_with_badge.png<%} else {%> images/details.png<%}
								} else {%>images/details_yellow.png<%}%>" border="0" align="absmiddle" 
												style="width: 12px;padding-right: 2px;" /></a>
											<a href="Partner_Edit.do?pid=<bean:write name="pslist" 
											property="pid"/>&function=add&fromtype=<%=fromtype%>&jobid=<%=request.getParameter("typeid")%>&showViewPage=y"><bean:write name="pslist" property="partnernameTemp" /></a>
											</td>
									

											
											
											

<td class="<%=backgroundclass%>" noWrap="noWrap"><logic:notEqual
													name="pslist" property="thumbsUpCount" value="">
													<div
														style="width: 19px; text-align: right;float: left;">
														<bean:write name="pslist" property="thumbsUpCount" />
													</div>

													<div style="width: 16px; padding-right: 2px; float: left;">
														<a href="javascript:void(0)"
															onClick="showUpDownComments(event,'up',<%=(String) pID%>)"><img
															src="images/up.png"></a>
													</div>
												</logic:notEqual> <logic:notEqual name="pslist" property="thumbsDownCount"
													value="">
													<div
														style="float: left; width: 19px; text-align: right;">
														<bean:write name="pslist" property="thumbsDownCount" />
													</div>
													<div style="float: left;width: 16px;">
														<a href="javascript:void(0)"
															onClick="showUpDownComments(event,'down',<%=(String) pID%>)"><img src="images/down.png"></a>
													</div></logic:notEqual>
												</td>
										
										<td class="<%=backgroundclass%>" align="center">
											<a href="javascript:void(0)" onmouseout="closePopupWorkNature();" onmouseover="if (window.event || document.layers) showSpeedPayWorkDistributionPopup(event,<%=(String) pID%>); else showSpeedPayWorkDistributionPopup('',<%=(String) pID%>);">
												<bean:write name="pslist" property="contractedCount" /> 									
		 									</a>
											</td>
										<td class="<%=backgroundclass%>" align="center"><bean:write 
									name="pslist" property="compeletedCount" /></td>
										
										
										
										
										
										
										<td class="<%=backgroundclass%>" noWrap="noWrap" style="text-align: right;">
										 <logic:notEqual name="pslist" property="onTimePercentage" value="" > 
										 	
											&nbsp;&nbsp;<bean:write name="pslist" property="onTimePercentage"/>
											
											
										</logic:notEqual> 
										 <logic:equal name="pslist" property="onTimePercentage" value="" >
										 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</logic:equal> 
										
										
										<%
 																															if (fromtype.equals("powo")) {
 																														%>
										<a
											href="javascript:void(0)"
											onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); else showWindow('',<%=(String) pID%>);">
										<bean:message bundle="PVS" key="pvs.search.details" />
											</a>[<a href="#"
											onclick="return assignPartnerWithDirction(<%=(String) pID%>,'<%=partnerAddress1%>','<%=city%>','<%=state%>','<%=country%>',<bean:write name="pslist" property="partnerLatitude"/>,<bean:write name="pslist" property="partnerLongitude"/>);"><bean:message
											bundle="PVS" key="pvs.search.standardPO" /></a>][<a href="#"><bean:message
											bundle="PVS" key="pvs.search.multiplePO" /></a>]
										<%
												} else {
											%>
										<a
											href="javascript:void(0)"
											onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); else showWindow('',<%=(String) pID%>);">
											</a>
										<%
											}
										%>
								</td>

								<bean:define id="updateDated" name="pslist" property="updateDate" type="java.lang.String" />
								<bean:define id="createDated" name="pslist" property="createDate" type="java.lang.String" />
								<bean:define id="lastDated" name="pslist" property="partnerLastUsed" type="java.lang.String" />
								
								<td class="<%=backgroundclass%>" align="center">
								<%
									if ("".equalsIgnoreCase(lastDated.trim())) {
															if (!"".equalsIgnoreCase(updateDated.trim())) {
								%>
									<b><bean:write	name="pslist" property="updateDate" /></b>
									<%
										} else {
									%>
									<b><bean:write	name="pslist" property="createDate" /></b>
								<%
									}
														} else {
								%>
									<bean:write	name="pslist" property="partnerLastUsed" />
								<%
									}
								%>
								</td>
								
								<%--Changes end for Showing update or Created date when LastDate is not present --%>
								<td class="<%=backgroundclass%>" align="right"><bean:write
									name="pslist" property="distance" /></td>
								
								<td title="<bean:write name="pslist" property="city" />" class="<%=backgroundclass%>"><bean:write name="pslist" property="cityTemp" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="state" /></td>
									<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="zipcode" /></td>
									<td class="<%=backgroundclass%>" title="Call: <bean:write name="pslist" property="partnerPriCellPhone" />">
									<a href="tel: <bean:write name="pslist" property="partnerPriCellPhonePlain" />" >
									<img src="images/call.png" alt="Call" border="0" width="12" height="12" /></a>
									&nbsp;<bean:write name="pslist" property="partnerPriCellPhoneTemp" />
									</td>

								<%
									if (fromtype.equals("powo")
																|| (request.getParameter("type")
																		.equalsIgnoreCase("v") && !zipCodeValue
																		.equals(""))) {
								%>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="bid" /></td>
								<td class="<%=backgroundclass%>" align="right"><a
									href="javascript:showGMapVendexMaches(<bean:write name="pslist" property="pid"/>);"><bean:write
									name="pslist" property="vendexMatches" /></a></td>
								<%
									}
								%>

								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesProfessional" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesKnowledgeable" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesEquipped" /></td>
								<td class="<%=backgroundclass%>" align="right"><bean:write
									name="pslist" property="partnerAdpo" /></td>
							</tr>
							
							</logic:equal>
						</logic:iterate>	
								
								
						
						
						
						
						
						
						
						
						
						
						
						
						<tr>
									<td class="Ntrybgray" style="text-align: left;height: 19px;" colspan="16">
									Certified</td>
								</tr>
							
							
							<logic:iterate id="pslist" name="partnersearchlist">
								<logic:equal property="partnerType" value="Certified" name="pslist">
								
								
								
								
							<bean:define id="PartType" name="pslist" property="partnerType"
								type="java.lang.String" />
							<bean:define id="pID" name="pslist" property="pid" />
							<bean:define id="P" name="pslist" property="pid" />
							<bean:define id="badgePrinted" name="pslist" property="badgePrinted" />
							<bean:define id="status" name="pslist" property="status" />
							<bean:define id="color" name="pslist" property="icontype"
								type="java.lang.String" />
							<bean:define id="partnerAddress1" name="pslist"
								property="partnerAddress1" type="java.lang.String" />
							<bean:define id="city" name="pslist" property="city"
								type="java.lang.String" />
							<bean:define id="state" name="pslist" property="state"
								type="java.lang.String" />
							<bean:define id="country" name="pslist" property="country"
								type="java.lang.String" />
								<bean:define id="leadMinuteman" name="pslist" property="leadMinuteman"
								type="java.lang.Boolean" />
							<%
								if (partnerAddress1.indexOf("'") > -1) {
														partnerAddress1 = partnerAddress1
																.replaceAll("'", " ");
													}
													if (city.indexOf("'") > -1) {
														city = city.replaceAll("'", " ");
													}
													if (state.indexOf("'") > -1) {
														state = state.replaceAll("'", " ");
													}
													if (country.indexOf("'") > -1) {
														country = country.replaceAll("'", " ");
													}
							%>
							<%
								if (csschooser == true) {

														backgroundclass = "Ntexto";
														backgroundclasstoleftalign = "Ntextoleftalignnowrap";
														backgroundhyperlinkclass = "Nhyperodd";
														csschooser = false;
													} else {
														csschooser = true;
														backgroundclass = "Ntexte";
														backgroundclasstoleftalign = "Ntexteleftalignnowrap";
														backgroundhyperlinkclass = "Nhypereven";
													}

													if (color.endsWith("red")) {
														backgroundclass = "Ntextpred";
													} else if (color.endsWith("green")) {
														backgroundclass = "Ntextpgreen";
													} else if (color.endsWith("yellow")) {
														backgroundclass = "Ntextpyellow";
													} else if (color.endsWith("orange")) {
														backgroundclass = "Ntextporange";
													}
													if (leadMinuteman == true) {
														backgroundclass = "NtextLeadMinuteMan";
													}
							%>

							<tr>
							<td  class="<%=backgroundclass%>" noWrap="noWrap" height="10">
							
							<logic:equal name="pslist" property="incTypeFlag" value="Y">
												<img src="images/2.png" border="0">											
											</logic:equal>
											<logic:equal
												name="pslist" property="status" value="Dormant">
												<img src="images/dormant.png" border="0">
											</logic:equal>
											<%
												if (leadMinuteman == true) {
											%>
												<img src="images/3.png" border="0"/>
											<%
												}
											%>
												 <logic:equal name="pslist" property="deliverableSource" value="Mobile App">
												<img src="images/iphone.png" border="0">
											</logic:equal>
							 <logic:equal name="pslist" property="barredflag" value="Y">
												<img src="images/1.gif" border="0">
											</logic:equal>				
							
							</td>
								<td class="<%=backgroundclass%>" noWrap="noWrap" height="10" title="<bean:write name="pslist" property="partnername" />"
												style=""> 
												<a href="javascript:void(0)"
												onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); 
												else showWindow('',<%=(String) pID%>);"><img src="<%if (!status.equals("Incomplete")) {
									if (badgePrinted.equals("true")) {%>images/details_with_badge.png<%} else {%> images/details.png<%}
								} else {%>images/details_yellow.png<%}%>" border="0" align="absmiddle" 
												style="width: 12px;padding-right: 2px;" /></a>
											<a href="Partner_Edit.do?pid=<bean:write name="pslist" 
											property="pid"/>&function=add&fromtype=<%=fromtype%>&jobid=<%=request.getParameter("typeid")%>&showViewPage=y"><bean:write name="pslist" property="partnernameTemp" /></a>
											</td>
									

											
											
											

<td class="<%=backgroundclass%>" noWrap="noWrap"><logic:notEqual
													name="pslist" property="thumbsUpCount" value="">
													<div
														style="width: 19px; text-align: right;float: left;">
														<bean:write name="pslist" property="thumbsUpCount" />
													</div>

													<div style="width: 16px; padding-right: 2px; float: left;">
														<a href="javascript:void(0)"
															onClick="showUpDownComments(event,'up',<%=(String) pID%>)"><img
															src="images/up.png"></a>
													</div>
												</logic:notEqual> <logic:notEqual name="pslist" property="thumbsDownCount"
													value="">
													<div
														style="float: left; width: 19px; text-align: right;">
														<bean:write name="pslist" property="thumbsDownCount" />
													</div>
													<div style="float: left;width: 16px;">
														<a href="javascript:void(0)"
															onClick="showUpDownComments(event,'down',<%=(String) pID%>)"><img src="images/down.png"></a>
													</div></logic:notEqual>
												</td>
										
										<td class="<%=backgroundclass%>" align="center">
										<bean:write 
	 									name="pslist" property="contractedCount" />
</td>
										<td class="<%=backgroundclass%>" align="center"><bean:write 
									name="pslist" property="compeletedCount" /></td>
										
										
										
										
										
										
										<td class="<%=backgroundclass%>" noWrap="noWrap" style="text-align: right;">
										 <logic:notEqual name="pslist" property="onTimePercentage" value="" > 
										 	
											&nbsp;&nbsp;<bean:write name="pslist" property="onTimePercentage"/>
											
											
										</logic:notEqual> 
										 <logic:equal name="pslist" property="onTimePercentage" value="" >
										 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</logic:equal> 
										
										
										<%
 																															if (fromtype.equals("powo")) {
 																														%>
										<a
											href="javascript:void(0)"
											onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); else showWindow('',<%=(String) pID%>);">
										<bean:message bundle="PVS" key="pvs.search.details" />
											</a>[<a href="#"
											onclick="return assignPartnerWithDirction(<%=(String) pID%>,'<%=partnerAddress1%>','<%=city%>','<%=state%>','<%=country%>',<bean:write name="pslist" property="partnerLatitude"/>,<bean:write name="pslist" property="partnerLongitude"/>);"><bean:message
											bundle="PVS" key="pvs.search.standardPO" /></a>][<a href="#"><bean:message
											bundle="PVS" key="pvs.search.multiplePO" /></a>]
										<%
												} else {
											%>
										<a
											href="javascript:void(0)"
											onClick="if (window.event || document.layers) showWindow(event,<%=(String) pID%>); else showWindow('',<%=(String) pID%>);">
											</a>
										<%
											}
										%>
								</td>

								<bean:define id="updateDated" name="pslist" property="updateDate" type="java.lang.String" />
								<bean:define id="createDated" name="pslist" property="createDate" type="java.lang.String" />
								<bean:define id="lastDated" name="pslist" property="partnerLastUsed" type="java.lang.String" />
								
								<td class="<%=backgroundclass%>" align="center">
								<%
									if ("".equalsIgnoreCase(lastDated.trim())) {
															if (!"".equalsIgnoreCase(updateDated.trim())) {
								%>
									<b><bean:write	name="pslist" property="updateDate" /></b>
									<%
										} else {
									%>
									<b><bean:write	name="pslist" property="createDate" /></b>
								<%
									}
														} else {
								%>
									<bean:write	name="pslist" property="partnerLastUsed" />
								<%
									}
								%>
								</td>
								
								<%--Changes end for Showing update or Created date when LastDate is not present --%>
								<td class="<%=backgroundclass%>" align="right"><bean:write
									name="pslist" property="distance" /></td>
								
								<td title="<bean:write name="pslist" property="city" />" class="<%=backgroundclass%>"><bean:write name="pslist" property="cityTemp" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="state" /></td>
									<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="zipcode" /></td>
									<td class="<%=backgroundclass%>" title="Call: <bean:write name="pslist" property="partnerPriCellPhone" />">
									<a href="tel: <bean:write name="pslist" property="partnerPriCellPhonePlain" />" >
									<img src="images/call.png" alt="Call" border="0" width="12" height="12" /></a>
									&nbsp;<bean:write name="pslist" property="partnerPriCellPhoneTemp" />
									</td>

								<%
									if (fromtype.equals("powo")
																|| (request.getParameter("type")
																		.equalsIgnoreCase("v") && !zipCodeValue
																		.equals(""))) {
								%>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="bid" /></td>
								<td class="<%=backgroundclass%>" align="right"><a
									href="javascript:showGMapVendexMaches(<bean:write name="pslist" property="pid"/>);"><bean:write
									name="pslist" property="vendexMatches" /></a></td>
								<%
									}
								%>

								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesProfessional" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesKnowledgeable" /></td>
								<td class="<%=backgroundclass%>"><bean:write
									name="pslist" property="quesEquipped" /></td>
								<td class="<%=backgroundclass%>" align="right"><bean:write
									name="pslist" property="partnerAdpo" /></td>
											 			
							</tr>
							
							</logic:equal>
						</logic:iterate>	
						
						
						
						
						
						
						
						
						
						
						
						
						
						<tr>
							<td class="buttonrow" height="20" colspan="18"><html:button
								property="search" styleClass="button"
								onclick="return sortwindow();">Sort</html:button></td>
							<!-- <TD class="labele" height="20" colspan="2"></TD> -->
						</tr>
						<tr height="10px"><td></td></tr>
						<tr>
							<td colspan="18" class="Ntrybgray" style=" background: white;"><sup>1</sup> When the date in the "Last Date" column is displayed in bold text, this indicate the partner has not been used. The date displayed is when the partner has added to the PVS database. </td>
						</tr>
						<tr>
							<td>
							<div id="ajaxdiv" style="position: absolute;"></div>
							</td>
						</tr>
					</logic:present>
				</table>
				</td>
			</tr>
			
		</table>

		<table border="0" cellspacing="1" cellpadding="1">
			<tr>
				<td width="2" height="0"></td>
				<td>
				<table border="0" cellspacing="1" cellpadding="1">


					<%
						if (request.getParameter("type").equalsIgnoreCase("v")
										|| fromtype.equals("powo")) {
					%>
					<logic:present name="Partner_SearchForm" property="assignmessage">
						<logic:equal name="Partner_SearchForm" property="assignmessage"
							value="-9001">
							<table>
								<tr>
									<td width="100%" class="message" height="30"><bean:message
										bundle="PVS" key="pvs.errorassigningpartner" /></td>
								</tr>
							</table>
						</logic:equal>
						<logic:equal name="Partner_SearchForm" property="assignmessage"
							value="-9002">
							<table>
								<tr>
									<td width="100%" class="message" height="30"><bean:message
										bundle="PVS" key="pvs.alreadyassigned" /></td>
								</tr>
							</table>
						</logic:equal>
						<logic:equal name="Partner_SearchForm" property="assignmessage"
							value="0">
							<table>
								<tr>
									<td width="100%" class="message" height="30"><bean:message
										bundle="PVS" key="pvs.successfullyassigned" /></td>
								</tr>
							</table>
						</logic:equal>
						<logic:equal name="Partner_SearchForm" property="assignmessage"
							value="-9100">
							<table>
								<tr>
									<td width="100%" class="message" height="30"><bean:message
										bundle="PVS" key="pvs.selectforassignment" /></td>
								</tr>
							</table>
						</logic:equal>
					</logic:present>
					<%
						}
					%>
					<logic:present name="Partner_SearchForm" property="norecordmessage">
						<logic:equal name="Partner_SearchForm" property="norecordmessage"
							value="9999">
							<table>
								<tr>
									<td width="100%" class="message" height="30"><bean:message
										bundle="PVS" key="pvs.norecord" /></td>
								</tr>
							</table>
						</logic:equal>
					</logic:present>

					</tr>
				</table>
				</td>
			</tr>
		</table>
	</html:form>



</table>
</BODY>

<script>


// for managing x , y coordinates to show popup
var globalMouseX;
var globalMouseY;

$(document).mousemove(function( event ) {
	globalMouseX = event.pageX;
	globalMouseY = event.clientY;
	
	});
	
function showUpDownComments(e,upOrDown,pId) {
	var div=document.all.ajaxdiv;
	str = 'PartnerUpDownComments.do?partnerId='+pId+'&upDownFlagVal='+upOrDown;
	$('#ajaxdiv').load(str);

	var _x;
	var _y;
	
	var paddingTop = $(document).scrollTop();

	var browserName = navigator.appName;
	
	if (browserName != "Microsoft Internet Explorer") {
		
		_x = globalMouseX;
		_y = globalMouseY + paddingTop;
		
	}
	if (browserName == "Microsoft Internet Explorer") {
		
		_x = e.clientX ;
		_y = e.clientY + paddingTop;
	}
	
	$("#ajaxdiv").css("left",_x + 8*1 + "px").css("top", _y + "px");//.css("position","fixed");
	
	
}


function showWindow(e,pId) {
	var div=document.all.ajaxdiv;
	str = 'PartnerDetail.do?partnerId='+pId;
	$('#ajaxdiv').load(str);

	var _x;
	var _y;
	
	var paddingTop = $(document).scrollTop();

	var browserName = navigator.appName;
	
	if (browserName != "Microsoft Internet Explorer") {
		
		_x = globalMouseX;
		_y = globalMouseY + paddingTop;
		
	}
	if (browserName == "Microsoft Internet Explorer") {
		
		_x = e.clientX ;
		_y = e.clientY + paddingTop;
	}
	
	$("#ajaxdiv").css("left",_x + 8*1 + "px").css("top", _y + "px");//.css("position","fixed");
	
	
}

function showSpeedPayWorkDistributionPopup(e,pId)
{
		
	
	var div=document.all.ajaxdiv;
    var x=y=0;
    
	str = 'PartnerTypeDetail.do?partnerId='+pId;
	$('#ajaxdiv').load(str);

	var _x;
	var _y;
	
	var paddingTop = $(document).scrollTop();

	var browserName = navigator.appName;
	
	if (browserName != "Microsoft Internet Explorer") {
		
		_x = globalMouseX;
		_y = globalMouseY + paddingTop;
		
	}
	if (browserName == "Microsoft Internet Explorer") {
		
		_x = e.clientX ;
		_y = e.clientY + paddingTop;
	}
	
	$("#ajaxdiv").css("left",_x + 8*1 + "px").css("top", _y + "px");//.css("position","fixed");
	
	
	
}

function showGMap() {
		 document.forms[0].action = "PartnerGMap.do?jobId=<%=request.getParameter("typeid")%>";
		 document.forms[0].submit();
		 return true;
}

function exportExcel(){
	document.forms[0].ref.value = "exportToExcel";
	document.forms[0].search.value = "search";
	 document.forms[0].action = "Partner_Search.do";
	 document.forms[0].submit();
}
function chkInteger(obj,label){
	if(!isInteger(obj.value))	{	
		alert("<bean:message bundle="PVS" key="pvs.search.numeric.check"/>"+label);	
		obj.value ="";
		obj.focus();
		return false;
	}
	return true;
}

function validate()
{
	
	if(!(document.forms[0].checkedPartnerName[0].checked || document.forms[0].checkedPartnerName[1].checked || document.forms[0].checkedPartnerName[2].checked))
	{
		alert("Please Check Atleast One Partner Type");
	return false;
	}
	trimFields();
	var zipCodeFieldChecked = true;

		if (document.forms[0].zipcode.value!="")
		{
			if(document.forms[0].mainOffice.checked || document.forms[0].fieldOffice.checked || document.forms[0].techniciansZip.checked){
					zipCodeFieldChecked = true;
				}
			else
				{
					alert("<bean:message bundle="PVS" key="pvs.search.zip.option.message"/>");
					zipCodeFieldChecked = false;
				}
		}
	if(zipCodeFieldChecked == true){
			searchpartner();
	}		
}
function searchpartner()
{
	document.forms[0].ref.value = "search";
	document.forms[0].search.value = "search";
	document.forms[0].submit();
	return true;	
}

function assignpartner(pid, partnerLatitude, partnerLongitude, siteLatitude, siteLongitude)
{

	document.forms[0].selectedpartner.value = pid;
	document.forms[0].currentPartner.value = pid;
	document.forms[0].isClicked.value="1";
	document.forms[0].ref.value = "assign";
	document.forms[0].hiddensave.value="save";
	document.forms[0].action = "Partner_Search.do?partnerLatitude="+partnerLatitude+"&partnerLongitude="+partnerLongitude; 
	document.forms[0].submit();
	return true;
}


function Backaction()
{
	<%if (fromtype.equals("powo")) {%> 
   	          document.forms[0].action = "JobDashboardAction.do?type=<%=request.getParameter("tabId")%>&Job_Id=<%=request.getParameter("typeid")%>";
			  document.forms[0].submit();
			  return true;
	         
  <%}%>
}

function sortwindow()
{	
	str = './SortAction.do?type=partnerSearch&ref=Search&name=<bean:write name="Partner_SearchForm" property="name"/>&keyword=<bean:write name="Partner_SearchForm" property="searchTerm"/>&partnersearchType=<bean:write name="Partner_SearchForm" property="partnerTypeName"/>&mvsaVers=<bean:write name="Partner_SearchForm" property="mcsaVersion"/>&zip=<bean:write name="Partner_SearchForm" property="zipcode"/>&radius=<bean:write name="Partner_SearchForm" property="radius"/>&mainoffice=<bean:write name="Partner_SearchForm" property="mainOffice"/>&fieldOffice=<bean:write name="Partner_SearchForm" property="fieldOffice"/>&techhi=<bean:write name="Partner_SearchForm" property="techniciansZip"/>&rating=<bean:write name="Partner_SearchForm" property="partnerRating"/>&technician=<bean:write name="Partner_SearchForm" property="technicians"/>&certification=<bean:write name="Partner_SearchForm" property="certifications"/>&corecomp=<bean:write name="Partner_SearchForm" property="coreCompetency"/>&chekBox1=<bean:write name="Partner_SearchForm" property="cbox1"/>&chekBox2=<bean:write name="Partner_SearchForm" property="cbox2"/>&jobId=<bean:write name="Partner_SearchForm" property="typeid"/>&selectedPartnersList=<bean:write name="Partner_SearchForm" property="selectedPartnersList"/>&coreCompetencyList=<bean:write name="Partner_SearchForm" property="coreCompetencyList"/>&techniciansList='+ encodeURIComponent('<bean:write name="Partner_SearchForm" property="techniciansList"/>')+'&securityCertiftn=<bean:write name="Partner_SearchForm" property="securityCertiftn"/>&selectedCountryName=<bean:write name="Partner_SearchForm" property="selectedCountryName"/>&selectedCityName=<bean:write name="Partner_SearchForm" property="selectedCityName"/>';
	   if (window.showModelessDialog) {        // Internet Explorer
		   p = showModelessDialog(str, window, 'status:false;dialogWidth:400px; dialogHeight:350px' );
	    } 
	    else {//other browsers
	        window.open (str, "popupwindow","width=400, height=350, alwaysRaised=yes,top=250,left=450");
	    }
	return true;
}
function showGMapforvendex() {
		document.forms[0].action = "PartnerGMap.do?maptype=jobtojob&jobId=<%=request.getParameter("typeid")%>";
		document.forms[0].submit();
		return true;
}
function showGMapVendexMaches(pId) {
		document.forms[0].action = "PartnerGMap.do?jobId=<%=request.getParameter("typeid")%>&radius=<bean:write name="Partner_SearchForm" property="radius"/>&partnerId="+pId;
		document.forms[0].submit();
		return true;
}


// for the popup javascript

function changePicture(indexPic)
{
	
	$(".partnerFaces img").css("display","none");
	
	$(".partnerName a").css("text-decoration","none");
	
	document.getElementById("pictureFrame").innerHTML = "";
	
	$( ".partnerFaces" ).each(function( index ) {
		
		if(index == indexPic)
		{
			var object = $(this);
			document.getElementById("pictureFrame").innerHTML = object.html();
			$("#pictureFrame img").css("display","block").css("border","5px solid white").css("border-radius","5px");
		}
		
	});

	
	$(".partnerName a").each(function( index ) {
		
		if(index == indexPic)
		{
			$(this).css("text-decoration","underline");		
		}
		
	});
	
	$(".partnerName a").css("cursor","pointer");
	
}

function closePopup()
{
	document.all.ajaxdiv.innerHTML='';
	
}

function closePopupWorkNature()
{
	
	document.all.ajaxdiv.innerHTML='';
}

</script>
</html:html>
