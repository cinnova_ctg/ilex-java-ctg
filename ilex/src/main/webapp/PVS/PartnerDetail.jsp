<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<html>
<head>
    <title><bean:message bundle="PVS" key="pvs.partner.detail" /></title>
    <style type="text/css">
        .partnerName {

                float: left;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
                word-wrap: break-word;


        }
        #pictureFrame img 
        {
                border: 5px solid white;
                border-radius: 5px;
        }
    </style>

    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <a onClick="document.all.ajaxdiv.innerHTML = '';" href="#">
            <img src="images/box_close.gif" alt="" border="0" class="ACTBTN0001">
        </a>
	<table width="100%" border="0" cellspacing="0" cellpadding="1" class="ACTTBL0001">
            <logic:present scope="request" name="showPartnerType">
                <tr>
                    <td class="inside" colspan="3">Partner Type:&nbsp;<bean:write name="PartnerDetailForm" property="partnerType" />
                    </td>
                </tr>
            </logic:present>
            <tr>
                <td class="inside">
                    <bean:write name="PartnerDetailForm" property="partnerAddress1" />
                    <logic:notEqual name="PartnerDetailForm" property="partnerAddress2" value="">,&nbsp;<bean:write name="PartnerDetailForm" property="partnerAddress2" /></logic:notEqual>
                </td>
		<td></td>
            </tr>
            <tr>
                <td class="inside" colspan="3">
                    <bean:write name="PartnerDetailForm" property="partnerCity" />,&nbsp;
                    <bean:write name="PartnerDetailForm" property="partnerState" />&nbsp;
                    <bean:write name="PartnerDetailForm" property="partnerZipCode" />&nbsp;
                </td>
            </tr>
            <tr>
                <td class="inside" colspan="3"><bean:write name="PartnerDetailForm" property="partnerPhone" /></td>
            </tr>
            <tr>
                <td class="inside" colspan="3" height="4"></td>
            </tr>
            <tr>
                <td class="inside" colspan="3">Primary Contact</td>
            </tr>
            <tr>
                <td style="padding-left: 12px">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="inside" colspan="3"><bean:write name="PartnerDetailForm" property="partnerPriName" /></td>
                        </tr>
                        <tr>
                            <td class="inside" colspan="3"><bean:write name="PartnerDetailForm" property="partnerPriPhone" /></td>
                        </tr>
                        <logic:present scope="request" name="showPartnerType">
                            <logic:notEqual name="PartnerDetailForm" property="partnerCellPhone" value="">
                                <tr>
                                    <td class="inside" colspan="3"><bean:write name="PartnerDetailForm" property="partnerCellPhone" />&nbsp;(Cell)</td>
                                </tr>
                            </logic:notEqual>
                        </logic:present>
                        <tr>
                            <td class="inside" colspan="3"><bean:write name="PartnerDetailForm" property="partnerPriCellPhone" /></td>
                        </tr>
                        <tr>
                            <td class="inside" colspan="3"><bean:write name="PartnerDetailForm" property="partnerPriEmail" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <logic:notEqual name="PartnerDetailForm" property="daysRange" value="">
                <tr>
                    <td class="inside" colspan="3">Availability Schedule</td>
                </tr>
                <tr>
                    <td style="padding-left: 12px">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="inside" colspan="3">Days: <bean:write name="PartnerDetailForm" property="daysRange" /></td>
                            </tr>
                            <tr>
                                <td class="inside" colspan="3">Time: <bean:write name="PartnerDetailForm" property="timeRange" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </logic:notEqual>
            <tr>
                <td colspan="3">
                    <div width="50%" style="float: left;">
                        <table>
                            <tr>
                                <td>
                                    <div id="pictureFrame" style="display: block;">
                                        <img width="130px" src="<bean:write name="PartnerDetailForm" property="imgPath" />" alt="<bean:write name="PartnerDetailForm" property="partnerName" />" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="width: 100px;" class="partnerName">
                        <table>
                            <logic:iterate id="dataController" property="imagesList" name="PartnerDetailForm" indexId="counter"  >
                                <tr>
                                    <td>
                                        <a title="<bean:write name="dataController" property="label" />" onclick="changePicture('<%=  counter  %>');return false;" class="inside" style="text-decoration: <% if(counter == 0){ %>underline;<% } %>"> <bean:write name="dataController" property="label" /> </a>
                                    </td>
                                </tr>
                            </logic:iterate>
                        </table>
                    </div>
                    <script type="text/javascript">
                        $(".partnerName a").css("cursor","pointer");
                    </script>
                    <div width="0" style="display: none;">
                        <table>
                            <logic:iterate id="data" property="imagesList" name="PartnerDetailForm">
                                <tr>
                                    <td colspan="2" class="partnerFaces inside" style="display: none;">
                                        <img  width="130px" src="<bean:write name="data" property="value" />" alt="<bean:write name="PartnerDetailForm" property="partnerName" />" />
                                    </td>
                                </tr>
                            </logic:iterate>
                        </table>
                    </div>
                </td>
            </tr>		
        </table>
    </body>
</html>
