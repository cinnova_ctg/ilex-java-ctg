<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
function validateForm() {
	var file = document.getElementById("certificateFile");
	if(file.value==""){
		alert("Please select a file to upload");
		return false;
	}
}
</script>
</head>
<body>
	<h2>Upload Security Certificate</h2>
   <html:form action="uploadCertificate" enctype="multipart/form-data" onsubmit="return validateForm()">
   	<html:file name="Security_CertificateForm" property="certicateFile" styleId="certificateFile" />
   	<input type="submit" property="save" value="UPLOAD"/>
   </html:form>
</body>
</html>