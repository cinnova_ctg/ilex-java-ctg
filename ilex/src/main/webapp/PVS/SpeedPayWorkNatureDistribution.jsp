<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>



<html>
<head>

<title>Speedpay Work Details</title>


<style type="text/css">

.roundedContainer 
{
	border: 2px solid black; 
	border-radius: 10px;
	box-shadow : 0 1px 1px #CCCCCC;
}

.roundedContainer th:first-child {
    -moz-border-radius: 10px 10px 0 0;
    -webkit-border-radius: 10px 10px 0 0;
    border-radius: 10px 10px 0 0;
}

.roundedContainer tr:last-child td:first-child {
    -moz-border-radius: 0 0 10px 10px ;
    -webkit-border-radius: 0 0 10px 10px;
    border-radius: 0 0 10px 10px;
}

/* .roundedContainer tr:last-child td:second-child { */
/*     -moz-border-radius: 0 0 10px 10px ; */
/*     -webkit-border-radius: 0 0 10px 10px; */
/*     border-radius: 0 0 10px 10px; */
/* } */

.noRounds{
 -moz-border-radius: 0 0 0 0 !important;
    -webkit-border-radius: 0 0 0 0 !important;
    border-radius: 0 0 0 0 !important;

}

</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<TABLE WIDTH=250 BORDER=0 CELLPADDING=0 CELLSPACING=0  class='roundedContainer'>
	
	<thead>
		<tr>
			
			<th id="thPartnerdetail" style="background-color: #D3D3D3;padding: 5px;" align="right">
				<a onClick="closePopupWorkNature();" href="#">
					<img SRC="images/testimg.png" ALT="" WIDTH=12 HEIGHT=12 border="0" title="close window">
				</a>
			</th>
		</tr>
		</thead>
		
		<tbody>
			
			<tr>
				
				<td valign="top" bgcolor="#E6E6E6">
					<table width="100%"
						border="0" cellspacing="1" cellpadding="4">
							<tr>
								<td width="150" class="Ntrybgray">
									<b>Type</b>
								</td>
	           					<td class="Ntrybgray" style="background-color: transparent;"> 
	           						<b>Count</b>
           						</td>
							</tr>
					
							<logic:iterate id="data" property="partenersDistributionList" name="PartnerTypeDetailForm"  >
									<tr>
										<td width="150" class="Ntrybgray noRounds">
											<bean:write name="data" property="label" />
										</td>
			           					<td class="Ntrybgray noRounds" style="background-color: transparent;"> 
			           						<bean:write name="data" property="value" />
		           						</td>
									</tr>
							</logic:iterate>
					
					</table>
				</td>
			</tr>
			
			<tr>
				
				<td valign="top" bgcolor="#E6E6E6">
					<table width="100%"
						border="0" cellspacing="1" cellpadding="4">
							<tr>
								<td width="150" class="Ntrybgray" style="border-radius: 0 0  0px  0px !important; -webkit-border-radius: 0 0  0px  0px !important;-moz-border-radius: 0 0  0px  0px !important;">
									Last Speedpay Date
								</td>
	           					<td class="Ntrybgray" style="background-color: transparent;"> 
	           						<bean:write name="PartnerTypeDetailForm" property="lastSpeedpayDate" />
           						</td>
							</tr>					
					</table>
				</td>
			</tr>
			<tr>
				
				<td valign="top" bgcolor="#E6E6E6">
					<table width="100%"
						border="0" cellspacing="1" cellpadding="4">							
							<tr>
								<td class="Ntrybgray" style="border-radius: 0 0 10px 10px !important; -webkit-border-radius: 0 0 10px 10px !important;-moz-border-radius: 0 0 10px 10px !important;">
									Count of POs by types, a job may have 1 or more POs, <br />so the sum(of POs) must be greater than or equal to <u>Jobs Ordered</u>
								</td>
							</tr>
					
					</table>
				</td>
			</tr>
		
		</tbody>
	</TABLE>

</body>
</html>
