<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For Displaying menutree in the left .
*
-->
<!DOCTYPE HTML>
<%@ page import = "com.mind.common.*" %>
<%@ page import="com.mind.common.dao.*" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id = "menustring" name = "menustring" scope = "request"/>
<bean:define id = "currState" name = "currState" scope = "request"/>
<bean:define id = "expansionState" name = "expansionState" scope = "request"/>

<bean:define id = "element" name = "element" scope = "request"/>
<bean:define id = "controller" name = "controller" scope = "request"/>

<%
	String firsttime = "";
	String searchcriteria = ""; 

	if( request.getAttribute( "firsttime" ) != null )
		firsttime = request.getAttribute( "firsttime" ).toString();

	if( request.getAttribute( "searchcriteria" ) != null )
		searchcriteria = request.getAttribute( "searchcriteria" ).toString();

	if( searchcriteria.equals( "null" ) )
		searchcriteria = "";
	
%>
<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>
<TITLE></TITLE>
</HEAD>

<title><bean:message bundle="RM" key="rm.hierarchynavigation"/></title>
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 10px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 0px;
    	white-space : nowrap;
    }

    /* Link Styles */
    a  {
    	color : #000000;
    }

    a:link  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:active  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:hover  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:visited  {
    	text-decoration : underline;
    	color : #000000;
    }
    
    .m2 {
        font-family : "Verdana";
        font-size : 10px;
        line-height: 10px;
    }    
</style>

<script>
function searchfunction()
{
// 	setCookie("searchpvs", document.forms[0].searchtext.value);
	document.forms[0].action = 'MenuTreeAction.do?menucriteria=PVS&firsttime=<%= firsttime %>';
	return true;
}

function showall()
{
// 	setCookie("searchpvs", "");
	document.forms[0].firsttime.value = 'false';
	document.forms[0].searchtext.value = '';
	document.forms[0].action = 'MenuTreeAction.do?menucriteria=PVS&firsttime=false&searchtext=';
	document.forms[0].submit();
	return true;
}

function hideall()
{
// 	setCookie("searchpvs", "null");
	document.forms[0].firsttime.value = 'false';
	document.forms[0].searchtext.value = '      ';	
	document.forms[0].action = 'MenuTreeAction.do?menucriteria=PVS&firsttime=false&searchtext=null';
	document.forms[0].submit();
	return true;
}

function setdefault()
{
	var p = false;
	setCookie("searchpvs", "null");
	document.forms[0].searchtext.value = getCookie("searchpvs");
	document.forms[0].searchtext.focus();
	
	var tableimg = document.getElementsByTagName('img');
	
	if( document.forms[0].searchtext.value != '' )
	{
		for (var k = 0; k < tableimg.length; k++) 
		{
			if( tableimg[k].src.indexOf( "images/twored.gif" )!= -1 ) 
			{
				p = true;
				break;
			}
		}
	}
	else
	{
		p = true;
	}
	
	
	if( !p)
	{
		document.getElementById('content3').style.visibility = 'visible';
		document.getElementById('content3').style.display = 'block';
	}
	else
	{
		document.getElementById('content3').style.visibility = 'hidden';
		document.getElementById('content3').style.display = 'none';
	}
	
	if( document.forms[0].searchtext.value == "null" )
		document.forms[0].searchtext.value = "";
	return true;
}
</script>

 
<script language="JavaScript" src="javascript/ce_menu.js" type="text/javascript"></script>
<body onload="setdefault();MM_preloadImages('images/red_folder.gif','images/twored.gif');"  text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/sidebg.jpg">
<%
MenuElement me = ((MenuTree)(element)).getRootElement();
MenuController mc =((MenuTree)(element)).getMenuController();

response.getWriter().print("<div><div style='visibility:hidden;' id='content1'><table><form method=get><input type='hidden' name='menucriteria' value='PVS'><input type='hidden' name='firsttime' value=''><tr><td class = 'm2'><b>Name:</b>&nbsp;<input type = 'text' name = 'searchtext' size = '16' class = 'textbox' /></td><td><input name='go' type='submit' value = 'Search' class = 'button' onClick = 'return searchfunction();' /></td></tr></form></table></div>"); 

response.getWriter().print("<div style='visibility:hidden;display:none;' id='content3'><table><tr><td colspan = '2'><img name = 'Image1' src='images/2.gif' border='0' width='11' height='10'/>&nbsp;<span class = 'm2'>No records match the search criteria.</span></td></tr></table></div>"); 
response.getWriter().print("<div style='visibility:hidden;' id='content2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:hideall();'>Clear Search</a></div></div>"); 
me.drawDirect(0,mc,response.getWriter());
%>
<script language="JavaScript">

document.getElementById('content1').style.visibility = 'visible';
document.getElementById('content2').style.visibility = 'visible';
document.getElementById('line0').className = 'm2';
document.getElementById('line0').style.visibility = 'visible';


currState="<%=((MenuTree)(element)).getCurrState()%>";
expansionState="<%=expansionState%>";



initNavigatorServer('PV');
//setdefault();
</script>
</body>

</html:html>


