<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %> <%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %> <%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<title>PartnerEditGeneralTab</title>
<bean:define id="dcCountryList" name="dcCountryList" scope="request"/>
<bean:define id ="dcStateCM" name = "dcStateCM" scope = "request"/>
<%
String temp_str = null;
String topPartnerName = "";
int k=0;
int CompetencySize = 0;
int keywordRowSize = 3;
String noLatLong = "";
String acceptAddress = "";

if(request.getAttribute("CompetencySize") != null)  CompetencySize = Integer.parseInt(request.getAttribute("CompetencySize")+"");
if(request.getAttribute("topPartnerName") != null) topPartnerName = request.getAttribute("topPartnerName").toString();
if(request.getAttribute("noLatLong") != null) noLatLong = request.getAttribute("noLatLong").toString();
if(request.getAttribute("acceptAddress") != null) acceptAddress = request.getAttribute("acceptAddress").toString();

String labelBold ="";
String labeleBold= "";
String labele = "";
String labelCombo= "";
String labelo = "";
String labelered = "";
String labelored = "";

if(request.getAttribute("topPartnerName")!=null && request.getAttribute("topPartnerName").equals("Minuteman Partner")){
	labelBold = "labelobold";
	labeleBold = "labelebold";
	labele = "labelo";
	labelo = "labele";
	labelCombo = "comboo";
	labelered = "labelored";
	labelored = "labelered";
}
else{
	labelBold = "labelobold";
	labeleBold = "labelebold";
	labele = "labelo";
	labelo = "labele";
	labelCombo = "comboe";
	labelered = "labelered";
	labelored = "labelored";
}
%>

<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>
<%@ include  file="/AjaxStateCountry.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>

<title></title>

<%@ include  file="/NMenu.inc" %>
</HEAD>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width = "100%"> 
      <table  border="0" cellspacing="0" cellpadding="0" height="18"  width = "100%">
        <tr >
    	 <logic:present name="retvalue" scope="request">
  		 <logic:equal name="retvalue" value="0"> 
      		<td id="pop2" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu2',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.manageprofile"/></center></a></td>
      	 </logic:equal>
      	 </logic:present>	
         <td id="pop1" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managepartner"/></center></a></td>                    
         <td  width="120"  class="Ntoprow1" align="center"><a href="Partner_Search.do?ref=search&type=v"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partnersearch"/></center></a></td>		 
		 <td width="120"  class="Ntoprow1" align="center"><a  href="MCSA_View.do"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partneredit.managemcsa"/></center></a></td>
         <td  class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table> 
    </td>
  </tr>
</table> --%>

 <logic:present name="retvalue" scope="request">
  		 <logic:equal name="retvalue" value="0"> 
      		<%-- <td id="pop2" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu2',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.manageprofile"/></center></a></td> --%>
      	 <div id="menunav">
	    <ul>
	    	<li>
	    		<a class="drop" href="#"><span>Manage Profile</span></a>
	  			<ul>
	  				<li><a href="Partner_Edit.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=Update">Update</a></li>
	  				<li>
			            <a href="#"><span>Incident Reports</span></a>
			            <ul>
			      			<li><a href="AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&amp;function=view&amp;page=PVS">View</a></li>
			      			<li><a href="AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&amp;function=add&amp;page=PVS">Post</a></li>
		      			</ul>
		        	</li>
	  				<li><a href="javascript:jobHistory();">Job History</a></li>
	  				 
	  				 
	  				<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%> 
					<li><a href="javascript:del();",0>Delete</a></li>
				 	<%}%> 
	  				 
	  				 
	  				 <!--  condition change for some user -->
	  				<%--  <c:if test="${sessionScope.RdmRds eq 'Y'}"> 
					<c:if test="${requestScope.topPartnerName ne 'Minuteman Partners'}"> 	
					<li><a href="Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="AddIncidentForm" property="pid"/>&convert=M",0>Convert To Standard</a></li>
					</c:if>
					<c:if test="${requestScope.topPartnerName ne 'Certified Partners'}"> 	
					<li><a href="Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="AddIncidentForm" property="pid"/>&convert=S",0>Convert To Standard</a></li>
					</c:if>
					</c:if>
					 <%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%> 
					<li><a href="javascript:del();",0>Delete</a></li>
				 	<%}%> 
						<c:if test="${sessionScope.RdmRds eq 'N'}"> 
						<li><a href="javascript:gettingAjaxDataForEmail('<bean:write
						name="AddIncidentForm" property="pid" />');",0></a>Request Restriction</a></li>
					</c:if>
						<c:if test="${sessionScope.RdmRds eq 'Y'}">
							<c:if test="${sessionScope.partnerRegStatus eq 'R'}"> 
							<li><a href="javascript:reSendUnamePwd('<bean:write
									name="AddIncidentForm" property="pid" />');",0>Send Username/Password</a></li>
							</c:if>
							<c:if test="${sessionScope.partnerRegStatus eq 'N'}">
							<li><a href="javascript:reSendRegEmail('<bean:write
									name="AddIncidentForm" property="pid" />');",0>Re-send Registration Email</a></li>
							</c:if>
						</c:if> --%>

	  			</ul>
	    	</li>
	  		
		</ul>
	</div>

      	 
      	 
      	 
 </logic:equal>
</logic:present>
<div id="menunav">
	    <ul>

	  		<li>
	        	<a class="drop" href="#"><span>Manage Partners</span></a>
	  			<ul>
	  				<li>
			            <a href="#"><span>Minuteman</span></a>
			            <ul>
			      			<li><a href="Partner_Edit.do?function=Add&amp;pid=0&amp;orgTopName=Minuteman Partners">Add</a></li>
			      			<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&amp;action=update">Update</a></li>
			      			<%
			      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
			      				%>
			      				<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=delete">Delete</a></li>	      				
								<%
							}
							%>
		      			</ul>
		        	</li>
	  				<li>
			            <a href="#"><span>Standard Partner</span></a>
			            <ul>
			      			<li><a href="Partner_Edit.do?function=Add&amp;pid=0&amp;orgTopName=Certified Partners">Add</a></li>
			      			<li><a href="PVSSearch.do?orgTopName=Certified Partners&amp;action=update">Update</a></li>
			      			<!--  add condition  -->
			      			
			      			<%
			      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
			      				%>
			      				<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=delete">Delete</a></li>	      				
								<%
							}
							%>
			      			
			      			
		      			</ul>
		        	</li>
	 				<li><a href="PVSSearch.do?orgTopName=all&amp;action=newadded&amp;enablesort=true">New Partners</a></li>
	 				<li><a href="PVSSearch.do?orgTopName=all&amp;action=resentupdated&amp;enablesort=true">Recent Updates</a></li>
	 				<li><a href="PVSSearch.do?orgTopName=all&amp;action=annualreport&amp;enablesort=true">Annual Review Due</a></li>
	  			</ul>
	  		</li>
	  		<li><a class="drop" href="Partner_Search.do?ref=search&amp;type=v">Partner Search</a></li>
	  		<li><a class="drop" href="MCSA_View.do">Manage MCSA</a></li>
		</ul>
	</div>



    
<SCRIPT language=JavaScript1.2>
if (isMenu) {
//start
arMenu1 = new Array(
120,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",
"Minuteman","#",1,
"Standard Partner","#",1,
"New Partners","PVSSearch.do?orgTopName=all&action=newadded&enablesort=true",0,
"Recent Updates","PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true",0,
"Annual Review Due","PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true",0
)
arMenu1_1=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners",0,
"Update","PVSSearch.do?orgTopName=Minuteman Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Minuteman Partners&action=delete",0
<%}%>
)
arMenu1_2=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners",0,
"Update","PVSSearch.do?orgTopName=Certified Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Certified Partners&action=delete",0
<%}%>
)
<logic:present name="retvalue" scope="request">
	<logic:equal name="retvalue" value="0"> 
		arMenu2=new Array(
		120,
		findPosX('pop2'),findPosY('pop2'),
		"","",
		"","",
		"","",		
		"Update","Partner_Edit.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=Update",0,
		"Incident Reports","#",1,
		"Job History","javascript:jobHistory();",0
		<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
		,"Delete","javascript:del();",0
		<%}%>
		//"Change Password","passwordchange.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=add&page=PVS",0
		)
		arMenu2_2=new Array(
		"View","AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=view&page=PVS",0,
		"Post","AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=add&page=PVS",0
		)
	</logic:equal>
</logic:present>	
//end

arMenu4=new Array(
120,
340,18,
"","",
"","",
"","",
"","#",0
)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}
</script>

<table width="900" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>&nbsp;</td>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  			<tr> 
  				<logic:notEqual name="Partner_EditForm" property="fromPVSSearch" value="fromPVSSearch">
				<td class="labeleboldwhite" height="30" colspan="2"><%=topPartnerName %>&nbsp;<bean:write name="Partner_EditForm" property="act" /></td>
				</logic:notEqual>
				<logic:equal name="Partner_EditForm" property="fromPVSSearch" value="fromPVSSearch">
					<td class="labeleboldwhite" height="30" colspan="2"><%=topPartnerName %>&nbsp;View</td>
				</logic:equal>
   			</tr>
   			<tr>
			   <td class="labeleboldwhite" height="30" colspan="2"><bean:message bundle = "PVS" key = "pvs.editpartnerdetails" /></td>
	 		</tr>
			<logic:present name="noLatLong">
				<tr>
				<td width="4" height="0"></td> 
				<td class="message" height="30">The address could not be validated. Verify all address fields.</td></tr>
			</logic:present>
			
		<logic:present name="retvalue" scope="request">
			<logic:notEqual name="retvalue" value="-1">
							<logic:equal name="retvalue" value="-90010">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30">Duplicate Partner. See <bean:write name="Partner_EditForm" property="duplicatePartnerName" />,&nbsp;<bean:write name="Partner_EditForm" property="address1" />, <bean:write name="Partner_EditForm" property="city" />, <bean:write name="Partner_EditForm" property="state" />, <bean:write name="Partner_EditForm" property="zip" /> </td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-90011">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.notuniqueusername"/></td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-90001">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-90002">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-90003">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-90004">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-90005">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-90006">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-90007">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
							
							<logic:equal name="retvalue" value="-90008">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
		
							<logic:equal name="retvalue" value="-9001">
								<tr>
								<td width="4" height="0"></td> 
								<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingilexpartner" /></td></tr>
							</logic:equal>
							
							<logic:notEqual name="Partner_EditForm" property="fromPVSSearch" value="fromPVSSearch">
								<logic:equal name="retvalue" value="0">
									<tr>
									<td width="4" height="0"></td> 
									<td class="messagewithwrap" height="30"><%=topPartnerName %> Added successfully</td></tr>
								</logic:equal>
							</logic:notEqual>
			</logic:notEqual>

			<logic:equal name="retvalue" value="-1">
			</logic:equal>
		</logic:present>
		
		<logic:present name="retMySqlValue" scope="request">
			<logic:equal name="retMySqlValue" value="9600">
					<tr>
					<td width="4" height="0"></td> 
					<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.partner.connectingtoweb" /></td></tr>
			</logic:equal>
			<logic:equal name="retMySqlValue" value="9601">
					<tr>
					<td width="4" height="0"></td> 
					<td class="messagewithwrap" height="30"><bean:message bundle="PVS" key="pvs.error.savingwebpartner" /></td></tr>
			</logic:equal>
		</logic:present>

<html:form action="Partner_Edit" enctype ="multipart/form-data">
<html:hidden property="pid" />
<html:hidden property="address_id" />
<html:hidden property="fromtype" />
<html:hidden property="pri_id"/>
<html:hidden property="sec_id"/> 
<html:hidden property="orgid"/>
<html:hidden property ="refersh"/>
<html:hidden property="orgTopId"/>
<html:hidden property="lat_min"/>
<html:hidden property="lon_min"/>
<html:hidden property="lat_degree"/>
<html:hidden property="lon_degree"/>
<html:hidden property="lat_direction"/>
<html:hidden property="lon_direction"/>
<html:hidden property="act"/>
<html:hidden property="latitude" />
<html:hidden property="longitude" />
<html:hidden property="latLongAccuracy" />
<html:hidden property="state" />

<script>
	document.forms[0].act.value='Add';
</script>

<!-- start -->
<tr>
	  <td colspan="3">
      <logic:present name="retvalue" scope="request">
      		<logic:equal name="retvalue" value="0">
      			  <table border="0" cellspacing="1" cellpadding="1" width="800">
      			  
      				<logic:present name = "topPartnerType" scope = "request">
						<logic:notEqual name = "topPartnerName" value="Minuteman Partner">
							<tr height="20">
									<td class="labelebold" colspan = "2" width="200"><bean:message bundle="PVS" key="pvs.partnerName"/>:</td>
									<td colspan="5" class="labele"><bean:write name="Partner_EditForm" property="partnerCandidate"/></td>
									<td colspan="3" class="labelebold" width="150" height="25"><bean:message bundle="PVS" key="pvs.partnerincorptype"/>:</td>
									<td colspan="2" class="labele" width="120"><bean:write name="Partner_EditForm" property="companyType"/></td>
						 			<html:hidden property="partnerLastName" value = "" />
									<html:hidden property="partnerFirstName" value = "" />    
							</tr>
							<tr height="20">
									<td colspan="2" class="labelobold" width="200" height="25"><bean:message bundle="PVS" key="pvs.partnertaxid"/>:</td>
									<td colspan="3" class="labelo" width="210" height="25" nowrap="nowrap">
										<logic:equal name="Partner_EditForm" property="resourceDeveloper" value = "Y">
											<bean:write name="Partner_EditForm" property="taxId"/>
										</logic:equal>
								
										<logic:notEqual name="Partner_EditForm" property="resourceDeveloper" value = "Y">
											<bean:message bundle="PVS" key="pvs.seeresourcedevmanager"/>
										</logic:notEqual>
									</td>
									<td colspan="3" class="labelobold" width="150" height="25"></td>
									<td colspan="2" class="labelo" width="120">
									<%--
									<td colspan="3" class="labelobold" width="150" height="25"><bean:message bundle="PVS" key="pvs.partnerincorptype"/>:</td>
									<td colspan="2" class="labelo" width="120"><bean:write name="Partner_EditForm" property="companyType"/></td>
									--%>
									<td colspan="1" class="labelobold" nowrap="nowrap"><bean:message bundle="PVS" key="pvs.partnerincorpdate"/>:</td>
									<td colspan="1" class="labelo" nowrap="nowrap"><bean:write name="Partner_EditForm" property="dateInc"/></td>
									
    						</tr>
    						
						</logic:notEqual>

						<logic:equal name = "topPartnerName" value="Minuteman Partner">
							<tr height="20">
									<html:hidden property="partnerCandidate" value = "" />
									<td colspan="2" class="labelobold" width="200"><bean:message bundle="PVS" key="pvs.minutemanlastname"/>:</td>
									<td colspan="3" class="labelo" width="200"><bean:write name="Partner_EditForm" property="partnerLastName"/></td>
		
									<td colspan="3" class="labelobold" width="100"><bean:message bundle="PVS" key="pvs.minutemanfirstname"/>:</td>
									<td colspan="2" class="labelo" width="170"><bean:write name="Partner_EditForm" property="partnerFirstName"/></td>
					
									<td  colspan="1" class="labelobold" width="20" bgcolor="red"><bean:message bundle="PVS" key="pvs.ssnm"/>:</td>
									<td  colspan="1" class="labelo" width="100" bgcolor="blue">
										<logic:equal name="Partner_EditForm" property="resourceDeveloper" value = "Y">
											<bean:write name="Partner_EditForm" property="taxId"/>
										</logic:equal>
										<logic:notEqual name="Partner_EditForm" property="resourceDeveloper" value = "Y">
										</logic:notEqual>
									</td>
							</tr>
					
									<html:hidden property="companyType" value = "1099" />
									<html:hidden property="dateInc" value = "" />
						</logic:equal>
					</logic:present>

					<%if(acceptAddress.equals("1")) { %>
						<tr height="20">
									<td class="labelodark" colspan="12" width="800">
										<html:checkbox property="acceptAddress" value="1" disabled="true" />&nbsp;<bean:message bundle="PVS" key="pvs.acceptaddress" />
									</td>
						</tr>
					<%} %>
					<%if(request.getAttribute("LatLongNotFound") != null && request.getAttribute("LatLongNotFound").equals("LatLongNotFound")){%>
						<tr><td class = "<%= labelo %>" colspan="13"><font style="color: blue;"><bean:message bundle="PVS" key="pvs.nolatlongfound" /></font></td></tr>	
					<%} %>
						<tr height="20">
									<td  class="<%= labeleBold %>" colspan="2" width="200"><bean:message bundle="PVS" key="pvs.address"/>:</td>
									<td  class="<%= labelo %>" colspan="10" width="370"><bean:write name="Partner_EditForm" property="address1"/></td>
									<%--<td  class="<%= labelored %>" colspan="2"><bean:message bundle="PVS" key="pvs.address1.message"/></td>--%>
						</tr>	
						<tr height="20">
								<td  class="<%= labelBold %>"  colspan="2" width="200"></td>
								<td  class="<%= labele %>"  colspan="10" width="370"><bean:write name="Partner_EditForm" property="address2"/></td>
								<%--<td  class="<%= labelered %>"  colspan="4"><bean:message bundle="PVS" key="pvs.address2.message"/></td>--%>
						</tr>
						<tr height="20">
								<td  class="<%= labeleBold %>"  colspan="2"><bean:message bundle="PVS" key="pvs.city"/>:</td>
								<td  class="<%= labelo %>"  colspan="10"><bean:write name="Partner_EditForm" property="city"/></td>
						</tr>
						<tr height="20">
								<td class = "<%=labelBold %>" colspan="2" width="200"><bean:message bundle = "PVS" key="pvs.stateprovince"/>:</td>
								<td class = "<%= labele %>" colspan="3" width="200"><bean:write name="Partner_EditForm" property="state"/></td>
				
								<td  class="<%= labelBold %>" colspan="3" width="120"><bean:message bundle="PVS" key="pvs.country"/>:</td>
			   					<td  class="<%= labele %>" colspan="2" width="100"><bean:write name="Partner_EditForm" property="country"/></td>

								<td  class="<%= labelBold %>"  colspan="1" width="60"><bean:message bundle="PVS" key="pvs.zip"/>:</td>
								<td  class="<%= labele %>"  colspan="1" width="50"><bean:write name="Partner_EditForm" property="zip"/></td>

						</tr>
			
						<tr height="20">
								<td  class="<%= labeleBold %>"  colspan="2" width="200"><bean:message bundle="PVS" key="pvs.mainphone"/>:</td>
								<td  class="<%= labelo %>"  colspan="3" width="200" nowrap="nowrap"><bean:write name="Partner_EditForm" property="mainPhone"/></td>
								<td  class="<%= labeleBold %>"  colspan="3" width="120" nowrap="nowrap"><bean:message bundle="PVS" key="pvs.mainfax"/>:</td>
								<td  class="<%= labelo %>"  colspan="2" width=120" nowrap="nowrap"><bean:write name="Partner_EditForm" property="mainFax"/></td>
								<td  class="<%= labeleBold %>"  colspan="1" width="60"><bean:message bundle="PVS" key="pvs.companyurl"/>:</td>
								<td  class="<%= labelo %>"  colspan="1" width="50"><bean:write name="Partner_EditForm" property="companyURL"/></td>
						</tr>

						<tr height="30"><td class="labellobold" colspan="12" width="800">Capabilities</td></tr>
	
						<tr height="20">
								<td  class="<%= labelBold %>"  colspan="8"><bean:message bundle="PVS" key="pvs.corecompetency"/>:</td>
								<td  class="<%= labelBold %>"  colspan="4"><bean:message bundle="PVS" key="pvs.partnerEdit.keyWord"/>:</td>				
						</tr>

						<tr height="20">
								<td class="<%= labelo %>" colspan="8" width="550">
									<logic:present name="dcCoreCompetancy" scope ="request">
										<table border="0" width="99%">
											<logic:iterate id="Competency" name="dcCoreCompetancy">
												<%if(k%3==0) { %>
													<tr>
												<%} %>
													<td class="<%= labelo %>" width="33%" nowrap="nowrap">
														<html:multibox property = "cmboxCoreCompetency" disabled="true"> 
																<bean:write name ="Competency" property = "competancyId"/>
														</html:multibox>   
								 						<bean:write name ="Competency" property = "competancyName"/>
													</td>	
												<% if(k%3!=0 && k%3!=1) {%>
													</tr>
												 <%} k++;%>
											</logic:iterate>
										</table>
									</logic:present>
								</td>
								<% k = ((k/4)+(k%2));
								   keywordRowSize = keywordRowSize + k; 
   								   String RowNum = String.valueOf(keywordRowSize); %>
								<td class = "<%= labelo %>" colspan = "4"><html:textarea property = "keyWord" styleClass = "textbox" cols="70" rows="<%=RowNum%>" readonly="true"/></td>
						</tr>
				
						<tr height="30">
								<td class="labellobold" colspan="2">Contacts</td>
								<td class="labelloboldcenter" colspan="6"><bean:message bundle="PVS" key="pvs.partnerprimary"/>:</td>
								<td class="labelloboldcenter" colspan="4"><bean:message bundle="PVS" key="pvs.partnersecondary"/>:</td>
						</tr> 
			
						<tr height="20">
								<td class="<%= labelBold %>" colspan="2" width="200"><bean:message bundle="PVS" key="pvs.partnerfirstname"/>:</td>
								<td class="<%= labele %>" colspan="6" width ="350"><bean:write name="Partner_EditForm" property="primaryFirstName"/></td>
								<td class="<%= labelBold %>" colspan="2" width="90"><bean:message bundle="PVS" key="pvs.partnerfirstname"/>:</td>
								<td class="<%= labele %>" colspan="2" width="160"><bean:write name="Partner_EditForm" property="secondaryFirstName"/></td>
						</tr>
			
						<tr height="20">
								<td class="<%= labeleBold %>" colspan="2" width="200"><bean:message bundle="PVS" key="pvs.partnerlastname"/>:</td>
								<td class="<%= labelo %>" colspan="6" width="350"><bean:write name="Partner_EditForm" property="primaryLastName"/></td>
								<td class="<%= labeleBold %>" colspan="2" width="90"><bean:message bundle="PVS" key="pvs.partnerlastname"/>:</td>
								<td class="<%= labelo %>" colspan="2" width="160"><bean:write name="Partner_EditForm" property="secondaryLastName"/></td>
						</tr>
			
						<tr height="20">
								<td class="<%= labelBold %>" colspan="2" width="200"><bean:message bundle="PVS" key="pvs.partneremail"/>:</td>
								<td class="<%= labele %>" colspan="6" width="350"><bean:write name="Partner_EditForm" property="prmEmail"/></td>
								<td class="<%= labelBold %>" colspan="2" width="90"><bean:message bundle="PVS" key="pvs.partneremail"/>:</td>
								<td class="<%= labele %>" colspan="2" width="160"><bean:write name="Partner_EditForm" property="secEmail"/></td>
						</tr>
			
						<tr height="20">
								<td class="<%= labeleBold %>" colspan="2" width="200"><bean:message bundle="PVS" key="pvs.partnerphone"/>:</td>
								<td class="<%= labelo %>" colspan="6" width="350"><bean:write name="Partner_EditForm" property="prmPhone"/></td>
								<td class="<%= labeleBold %>" colspan="2" width="90"><bean:message bundle="PVS" key="pvs.partnerphone"/>:</td>
								<td class="<%= labelo %>" colspan="2" width="160"><bean:write name="Partner_EditForm" property="secPhone"/></td>
						</tr>
			
						<tr height="20">
								<td class="<%= labelBold %>" colspan="2" width="200"><bean:message bundle="PVS" key="pvs.partnercell"/>:</td>
								<td class="<%= labele %>" colspan="6" width="350"><bean:write name="Partner_EditForm" property="prmMobilePhone"/></td>
								<td class="<%= labelBold %>" colspan="2" width="90"><bean:message bundle="PVS" key="pvs.partnercell"/>:</td>
								<td class="<%= labele %>" colspan="2" width="160"><bean:write name="Partner_EditForm" property="secMobilePhone"/></td>
						</tr>
			
						<tr height="20">
								<td class="<%= labeleBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.webusername"/>:</td>
								<td class="<%= labelo %>" colspan="10"><bean:write name="Partner_EditForm" property="webUserName"/></td>
						</tr>
			
						<tr height="20">
								<td class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.webpassword"/>:</td>
								<td class="<%= labele %>" colspan="10"><bean:write name="Partner_EditForm" property="webPassword"/></td>
						</tr>
			
      				</table>
      		</logic:equal>
      		
      		<logic:notEqual name="retvalue" value="0">
      			<table border="0" cellspacing="1" cellpadding="1" width="900">
					<logic:present name = "topPartnerType" scope = "request">
						<logic:notEqual name = "topPartnerName" value="Minuteman Partner">
							<tr>
								<td class="labelebold" colspan = "2"><bean:message bundle="PVS" key="pvs.partnerName"/><font class="red">*</font>:</td>
								<td colspan="6" class="labele"><html:text  styleClass="textbox" size="55" property="partnerCandidate" tabindex="1"/></td>
								<td colspan="2" class="labelebold"><bean:message bundle="PVS" key="pvs.partnerincorptype"/>:</td>
								<td colspan="2" class="labele">
										 <html:select property="companyType" size="1" styleClass="comboe" tabindex="2">
													<html:optionsCollection name = "dcCorporationType"  property = "firstlevelcatglist"  label = "label"  value = "value" />
				  						 </html:select>
				  			    </td>	
								<script>
									document.forms[0].partnerCandidate.focus();
								</script>
								
							 	<html:hidden property="partnerLastName" value = "" />
								<html:hidden property="partnerFirstName" value = "" />    

							</tr>
							<tr>
								<td colspan="2" class="labelobold"><bean:message bundle="PVS" key="pvs.partnertaxid"/>:</td>
								<td colspan="2" class="labelo"><html:text  styleClass="textbox" size="20" property="taxId" tabindex="3" /></td>
								<td colspan="4" class="labelobold"></td>					
								<td colspan="2" class="labelobold"><bean:message bundle="PVS" key="pvs.partnerincorpdate"/>:</td>
								<td colspan="2" class="readonlytextodd"><html:text  styleClass="textbox" size="10" property="dateInc" readonly = "true" tabindex="4"/>
    							<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].dateInc, document.forms[0].dateInc, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
    				
    						</tr>
    						
    						<tr>
    							<td colspan="2" class="labele"></td>
    							<td colspan="10" class="labelered" height="19"><bean:message bundle="PVS" key="pvs.partnertaxidmessage" /></td>
    						</tr>
						</logic:notEqual>

						<logic:equal name = "topPartnerName" value="Minuteman Partner">
							<tr>
								<html:hidden property="partnerCandidate" value = "" />
								<td colspan="2" class="labelobold"><bean:message bundle="PVS" key="pvs.minutemanlastname"/><font class="red">*</font>:</td>
								<td colspan="2" class="labelo"><html:text style="" styleClass="textbox" size="17" property="partnerLastName" tabindex="1" onblur="javascript: document.forms[0].primaryLastName.value = document.forms[0].partnerLastName.value;"/></td>
								
		
								<td colspan="2" class="labelobold"><bean:message bundle="PVS" key="pvs.minutemanfirstname"/><font class="red">*</font>:</td>
								<td colspan="2" class="labelo"><html:text style="" styleClass="textbox" size="17" property="partnerFirstName" tabindex="2" onblur="javascript: document.forms[0].primaryFirstName.value = document.forms[0].partnerFirstName.value;"/></td>
					
								<td  colspan="2" class="labelobold"><bean:message bundle="PVS" key="pvs.ssnm"/>:</td>
								<td  colspan="2" class="labelo"><html:text  styleClass="textbox" size="15" property="taxId" tabindex="3"/></td>
							</tr>
					<script>
						document.forms[0].partnerLastName.focus();
					</script>
								<html:hidden property="companyType" />
								<html:hidden property="dateInc" />
						</logic:equal>
					</logic:present>

				<% if(noLatLong.equals("noLatLong") || acceptAddress.equals("1")) { %>
				<tr>
						<td class="labelodark" colspan="12">
							<html:checkbox property="acceptAddress" value="1" />&nbsp;<bean:message bundle="PVS" key="pvs.acceptaddress" />
						</td>			
				</tr>
				<% } else {
					}
				%>
									
				<tr>
						<td  class="<%= labeleBold %>"  colspan="2"><bean:message bundle="PVS" key="pvs.address"/><font class="red">*</font>:</td>
						<td  class="<%= labelo %>"  colspan="3"><html:text  styleClass="textbox" size="45" property="address1" tabindex="5" />&nbsp;</td>
						<td  class="<%= labelored %>"  colspan="7"><bean:message bundle="PVS" key="pvs.address1.message"/></td>
				</tr>
				<tr>
						<td  class="<%= labelBold %>"  colspan="2"></td>
						<td  class="<%= labele %>"  colspan="3"><html:text  styleClass="textbox" size="45" property="address2" tabindex="6"/>&nbsp;</td>
						<td  class="<%= labelered %>"  colspan="7"><bean:message bundle="PVS" key="pvs.address2.message"/></td>
				</tr>
				<tr>
						<td  class="<%= labeleBold %>"  colspan="2"><bean:message bundle="PVS" key="pvs.city"/><font class="red">*</font>:</td>
						<td  class="<%= labelo %>"  colspan="10"><html:text  styleClass="textbox" size="35" property="city" tabindex="7" /></td>
				</tr>
				<tr>
						<td class = "<%=labelBold %>" colspan="2"><bean:message bundle = "PVS" key="pvs.stateprovince"/><font class="red">*</font>:</td>
						<td class = "<%= labele %>" colspan="2">
							<logic:present name="initialStateCategory">
								<html:select property = "stateSelect"  styleClass="<%= labelCombo %>" tabindex="8" onchange = "ChangeCountry();">
									<logic:iterate id="initialStateCategory" name="initialStateCategory" >
										<bean:define id="statecatname" name="initialStateCategory" property="siteCountryIdName" />		   
										<%  if(!statecatname.equals("notShow")) { %>
												<optgroup class=labelebold label="<%=statecatname%>">
										<% } %>
											<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
										<%  if(!statecatname.equals("notShow")) { %>	
											</optgroup> 
										<% } %>
									</logic:iterate>
								</html:select>
							</logic:present>	
						</td>
				
						<td  class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.country"/><font class="red">*</font>:</td>
			   			<td  class="<%= labele %>" colspan="2">
			     			  <html:select property="country" size="1" styleClass="<%= labelCombo %>" tabindex="9">
									<html:optionsCollection name = "dcCountryList"  property = "firstlevelcatglist"  label = "label"  value = "value" />
				  			  </html:select>
	    				</td>

						<td  class="<%= labelBold %>"  colspan="1"><bean:message bundle="PVS" key="pvs.zip" />:</td>
						<td  class="<%= labele %>"  colspan="3"><html:text  styleClass="textbox" size="15" property="zip" tabindex="10" onblur="javascript:  convertToUppercase(); setUserPassword();"/></td>
				</tr>
			
		 
				<tr>
						<td  class="<%= labeleBold %>"  colspan="2"><bean:message bundle="PVS" key="pvs.mainphone" /><font class="red">*</font>:</td>
						<td  class="<%= labelo %>"  colspan="2"><html:text  styleClass="textbox" size="27" property="mainPhone" tabindex="11" onblur="javascript: document.forms[0].prmPhone.value = document.forms[0].mainPhone.value;"/></td>
						<td  class="<%= labeleBold %>"  colspan="2"><bean:message bundle="PVS" key="pvs.mainfax" />:</td>
						<td  class="<%= labelo %>"  colspan="2"><html:text  styleClass="textbox" size="20" property="mainFax" tabindex="12"/></td>
						<td  class="<%= labeleBold %>"  colspan="1"><bean:message bundle="PVS" key="pvs.companyurl" />:</td>
						<td  class="<%= labelo %>"  colspan="3"><html:text  styleClass="textbox" size="24" property="companyURL" tabindex="13"/></td>
				</tr>

				<tr height="30"><td class="labellobold" colspan="12">Capabilities</td></tr>
	
				<tr height="20">
						<td  class="<%= labelBold %>"  colspan="6"><bean:message bundle="PVS" key="pvs.corecompetency"/><font class="red">*</font>:</td>
						<td  class="<%= labelBold %>"  colspan="6"><bean:message bundle="PVS" key="pvs.partnerEdit.keyWord" />:</td>				
				</tr>

				<tr height="20">
						<td class="<%= labelo %>" colspan="8" width="550">
									<logic:present name="dcCoreCompetancy" scope ="request">
										<table border="0" width="99%">
											<logic:iterate id="Competency" name="dcCoreCompetancy">
												<%if(k%3==0) { %>
													<tr>
												<%} %>
													<td class="<%= labelo %>" width="33%" nowrap="nowrap">
														<html:multibox property = "cmboxCoreCompetency"> 
																<bean:write name ="Competency" property = "competancyId"/>
														</html:multibox>   
								 						<bean:write name ="Competency" property = "competancyName"/>
													</td>	
												<% if(k%3!=0 && k%3!=1) {%>
													</tr>
												 <%} k++;%>
											</logic:iterate>
										</table>
									</logic:present>
								</td>
						<% k = ((k/4)+(k%2));
						   keywordRowSize = keywordRowSize + k; 
   						   String RowNum = String.valueOf(keywordRowSize); %>
						<td class = "<%= labelo %>" colspan = "6"><html:textarea property = "keyWord" styleClass = "textbox" cols="70" rows="<%=RowNum%>" tabindex="15"/></td>
				</tr>
				
				<tr height="30">
						<td class="labellobold" colspan="2">Contacts</td>
						<td class="labelloboldcenter" colspan="4"><bean:message bundle="PVS" key="pvs.partnerprimary"/>:</td>
						<td class="labelloboldcenter" colspan="6"><bean:message bundle="PVS" key="pvs.partnersecondary"/>:</td>
				</tr> 
			
				<tr height="20">
						<td class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerfirstname"/><font class="red">*</font>:</td>
						<td class="<%= labele %>" colspan="4"><html:text styleClass="textbox" size="20" property="primaryFirstName" tabindex="16"/></td>
						<td class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerfirstname"/>:</td>
						<td class="<%= labele %>" colspan="4"><html:text styleClass="textbox" size="20" property="secondaryFirstName" tabindex="23"/></td>
				</tr>
			
				<tr height="20">
						<td class="<%= labeleBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerlastname"/><font class="red">*</font>:</td>
						<td class="<%= labelo %>" colspan="4"><html:text styleClass="textbox" size="20" property="primaryLastName" tabindex="17"/></td>
						<td class="<%= labeleBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerlastname"/>:</td>
						<td class="<%= labelo %>" colspan="4"><html:text styleClass="textbox" size="20" property="secondaryLastName" tabindex="24"/></td>
				</tr>
			
				<tr height="20">
						<td class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partneremail"/><font class="red">*</font>:</td>
						<td class="<%= labele %>" colspan="4"><html:text styleClass="textbox" size="27" property="prmEmail" tabindex="18" onblur="javascript: setUnameEmail(document.forms[0].prmEmail,'Primary Email'); ;"/> (or <a href="javascript: document.forms[0].prmEmail.focus();" tabindex="19" onclick="javascript: setNotProvided(document.forms[0].prmEmail);"> Not Provided</a>)</td>
						<td class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partneremail"/>:</td>
						<td class="<%= labele %>" colspan="4"><html:text styleClass="textbox" size="27" property="secEmail" tabindex="25" onblur="javascript: chkEmail(document.forms[0].secEmail,'Secondary Email');"/></td>
				</tr>
			
				<tr height="20">
						<td class="<%= labeleBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerphone"/><font class="red">*</font>:</td>
						<td class="<%= labelo %>" colspan="4"><html:text styleClass="textbox" size="27" property="prmPhone" tabindex="20"/></td>
						<td class="<%= labeleBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partnerphone"/>:</td>
						<td class="<%= labelo %>" colspan="4"><html:text styleClass="textbox" size="20" property="secPhone" tabindex="26"/></td>
				</tr>
			
				<tr height="20">
						<td class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partnercell"/><font class="red">*</font>:</td>
						<td class="<%= labele %>" colspan="4"><html:text styleClass="textbox" size="27" property="prmMobilePhone" tabindex="21"/> (or <a href="javascript: document.forms[0].prmMobilePhone.focus();" tabindex="22" onclick="javascript: document.forms[0].prmMobilePhone.value='Not Provided';"> Not Provided</a>)</td>
						<td class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.partnercell"/>:</td>
						<td class="<%= labele %>" colspan="4"><html:text styleClass="textbox" size="20" property="secMobilePhone" tabindex="27"/></td>
				</tr>
			
				<tr>
						<td class="<%= labeleBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.webusername"/>:</td>
						<td class="<%= labelo %>" colspan="10"><html:text styleClass="textbox" size="30" property="webUserName" tabindex="28" onblur="javascript: setUserPassword();"/></td>
				</tr>
			
				<tr>
						<td class="<%= labelBold %>" colspan="2"><bean:message bundle="PVS" key="pvs.webpassword"/>:</td>
						<td class="<%= labele %>" colspan="10"><html:text styleClass="textbox" size="30" property="webPassword" tabindex="29"/></td>
				</tr>
			
				<tr> 
    					<td colspan="12" class="buttonrow"> 
      						<html:submit property="save" styleClass="button" tabindex="30" onclick="return validateDivA();"><bean:message bundle="PVS" key="pvs.partneredit.save"/></html:submit>
      						<html:submit property="saveContinue" styleClass="button" tabindex="31" onclick="return validateDivA();"><bean:message bundle="PVS" key="pvs.partneredit.saveandcontinue"/></html:submit>
     				   <!-- 		<html:reset property="reset" styleClass="button"><bean:message bundle="PVS" key="pvs.partneredit.reset"/></html:reset>   -->
     				   		<html:button property="reset" styleClass="button" tabindex="32" onclick="return getresetelements();"><bean:message bundle="PVS" key="pvs.partneredit.reset"/></html:button>
			   			</td>
				</tr>
			</table>
   		</logic:notEqual>
   </logic:present>
	</td>
</tr>

	</td>
</tr>
</table>

<logic:present name = "refreshtree" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present> 

</html:form>
</body>
</html:html>
<script>

function trimFields() {
	var field=document.forms[0];
	for(var i=0; i<field.length; i++) {
		if(field[i].type=='text') {
			var temp1=field[i].value;
			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--) {
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }
		field[i].value=temp2;
		}
	}
return true;
}

function validateDivA(){

document.forms[0].refersh.value = "";
 trimFields();
	if('<%=topPartnerName%>'=="Minuteman Partner") {
		if(! chkBlank(document.forms[0].partnerLastName,"<bean:message bundle="PVS" key="pvs.minutemanlastname"/>")) return false;
		if(! chkBlank(document.forms[0].partnerFirstName,"<bean:message bundle="PVS" key="pvs.minutemanfirstname"/>")) return false;
	} else {
		if(! chkBlank(document.forms[0].partnerCandidate,"Partner Name")) return false;
	}
	
	if(! chkBlank(document.forms[0].address1,"Address"))	return false;
	if(! chkBlank(document.forms[0].city,"<bean:message bundle="PVS" key="pvs.city"/>"))	return false;	
	if(! chkCombo(document.forms[0].stateSelect,"<bean:message bundle="PVS" key="pvs.state"/>"))return false;
	if(! chkCombo(document.forms[0].country,"<bean:message bundle="PVS" key="pvs.country"/>"))return false;

	if(document.forms[0].country.value=='US' ) {
			if(! chkBlank(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>"))	return false;
		//	if(! chkInteger(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>")) return false;
		}else {
			if(document.forms[0].country.value=='CA') {
				if(! chkBlank(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>"))	return false;
				if(! checkCanadianZipcode(document.forms[0].zip)) {
					alert('Canadian postal codes contain six characters in the format X9X 9X9, where X is a letter and 9 is a digit, with a space separating the third and fourth characters. For example: K1A 0B1. Please correct your input.');
					document.forms[0].zip.focus();
					return false;
				}
			}
		//	if(! chkInteger(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>")) return false;
		}
		
	
	if(! chkBlank(document.forms[0].mainPhone,"<bean:message bundle="PVS" key="pvs.mainphone"/>"))return false;
	//if(! chkInteger(document.forms[0].mainFax,"<bean:message bundle="PVS" key="pvs.mainfax"/>"))return false;		
	
	
	var submitflag = 'false';
	/*if(<%= CompetencySize %> != 0) {
		if( <%= CompetencySize %> == 1 )
		{
			if( !( document.forms[0].cmboxCoreCompetency.checked ))
			{
				alert('Please Check Core Competencies');
				return false;
			}
			else
			{
				submitflag = 'true';
			}
		} else {
			
			for( var i = 0; i<document.forms[0].cmboxCoreCompetency.length; i++ )
		  	{
		  		if( document.forms[0].cmboxCoreCompetency[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	
		  	if( submitflag == 'false')
		  	{	
		  		alert('Please Check Core Competencies');
				return false;
		  	}
		}
	}*/
	
	if(! chkBlank(document.forms[0].primaryFirstName,"Primary First Name"))	return false;
	if(! chkBlank(document.forms[0].primaryLastName,"Primary Last Name"))	return false;
	if(! chkBlank(document.forms[0].prmEmail,"Primary Email"))	return false;
	if(! CheckQuotes(document.forms[0].prmEmail,"<bean:message bundle="PVS" key="pvs.partneremail"/>")) return false;
	
	if(document.forms[0].prmEmail.value=='Not Provided') {
	//	alert('Not Provided');
	}else {
		//alert('do not check valid email');
		if(! chkEmail(document.forms[0].prmEmail,"Primary Email")) return false;	
	}
	
	if(! chkBlank(document.forms[0].prmPhone,"Primary Phone"))	return false;
	if(! chkBlank(document.forms[0].prmMobilePhone,"Primary Cell"))	return false;
	
	return true;
}

function removeHiffen(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!="-")	nstr=nstr+str.substring(i,i+1);
	}// END OF FOR
	return nstr;
}

function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkInteger(obj,label){
	if(!isInteger(removeHiffen(obj.value)))	{	
		alert("Only numeric values are allowed in "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function chkCombo(obj,label){
	
	if(obj.value==0 || obj.value == null )	{	
		alert("Please Select "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function chkEmail(obj,label){
	if(!isEmail(obj.value))	{	
		alert("Invalid "+label);	
		obj.focus();
		return false;
	}
	if(!isEmpty(obj.value)) {
		if(isWhitespace(obj.value)) {
			alert("Invalid "+label);
			obj.focus();
			return false;
		}
	}
	return true;
}
function setUnameEmail(obj,label) {
	if(document.forms[0].prmEmail.value=='Not Provided'){
	}else {
		var val = chkEmail(obj,label);
			if(val==true) {
				if(document.forms[0].webUserName.value=="") {
					document.forms[0].webUserName.value = document.forms[0].prmEmail.value;
					if(document.forms[0].webPassword.value=="") {
						document.forms[0].webPassword.value = document.forms[0].zip.value;
					}
				} else {
					if(document.forms[0].webPassword.value=="") {
						document.forms[0].webPassword.value = document.forms[0].zip.value;
					}
				}
				return true;
			}
	}
}

function setNotProvided(obj) {
		obj.value = 'Not Provided';
		document.forms[0].webUserName.value = "";
		document.forms[0].webPassword.value = "";
}

function setUserPassword() {
	if(document.forms[0].webUserName.value=="")
		document.forms[0].webPassword.value = "";
	else {
		if(document.forms[0].webPassword.value=="")
			document.forms[0].webPassword.value = document.forms[0].zip.value;
	}
}	

function CheckQuotes (obj,label){
	if(invalidChar(obj.value,label)) {		
		obj.focus();
		return false;
	}
	return true;
}

function getresetelements() {

document.forms[0].partnerCandidate.value="";
document.forms[0].partnerLastName.value="";
document.forms[0].partnerFirstName.value="";
document.forms[0].taxId.value="";
document.forms[0].companyType.value=" ";
document.forms[0].dateInc.value="";
document.forms[0].address1.value="";
document.forms[0].address2.value="";
document.forms[0].city.value="";
document.forms[0].stateSelect.value="0";
document.forms[0].country.value="0";
document.forms[0].zip.value="";
document.forms[0].mainPhone.value="";
document.forms[0].mainFax.value="";
document.forms[0].companyURL.value="";
document.forms[0].keyWord.value="";
document.forms[0].primaryFirstName.value="";
document.forms[0].secondaryFirstName.value="";
document.forms[0].primaryLastName.value="";
document.forms[0].secondaryLastName.value="";
document.forms[0].prmEmail.value="";
document.forms[0].secEmail.value="";
document.forms[0].prmPhone.value="";
document.forms[0].secPhone.value="";
document.forms[0].prmMobilePhone.value="";
document.forms[0].secMobilePhone.value="";
document.forms[0].webUserName.value="";
document.forms[0].webPassword.value="";
//start

	if(<%= CompetencySize %> != 0) {
		if( <%= CompetencySize %> == 1 )
		{
			if( document.forms[0].cmboxCoreCompetency.checked )
			{
				document.forms[0].cmboxCoreCompetency.checked = false;
			}
		} else {
			for( var i = 0; i<document.forms[0].cmboxCoreCompetency.length; i++ )
		  	{
		  		if( document.forms[0].cmboxCoreCompetency[i].checked ) 
		  		{
		  			document.forms[0].cmboxCoreCompetency[i].checked = false;
		  		}
		  	}
		}
	}
//end

return true;
}

function del() 
{
		var convel = confirm( "Are you sure you want to delete the Partner?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?fromId=<bean:write name = "Partner_EditForm" property = "pid"/>&func=delete";
			document.forms[0].submit();
			return true;	
		}
}
function jobHistory() 
{		
	document.forms[0].action = "JobInformationperPartner.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&from=Partner_Edit";
	document.forms[0].submit();
	return true;	
} 
function convertToUppercase() {
	var str=document.forms[0].zip.value
	if(document.forms[0].country.value=='CA') {
		if(str.length == 7) {
		document.forms[0].zip.value=str.toUpperCase();
		}
	}
}
</script>