<!DOCTYPE HTML>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <LINK href="styles/style.css" rel="stylesheet"	type="text/css">
    <title><bean:message bundle="PVS" key="pvs.partner.location.gmap"/></title>
 	 <jsp:include page="/common/IncludeGoogleKey.jsp"/>
    <script type="text/javascript">
    //<![CDATA[
    var gicons = [];
    var baseIcon = new GIcon();

  //  gicons["Cred"] = new GIcon(G_DEFAULT_ICON , "images/red1_p.png");
   // gicons["Mred"] = new GIcon(G_DEFAULT_ICON, "images/red1_m.png");
   // gicons["Sred"] = new GIcon(G_DEFAULT_ICON, "images/red1_s.png");
    
   // gicons["Cgreen"] = new GIcon(G_DEFAULT_ICON, "images/green1_p.png");
   // gicons["Mgreen"] = new GIcon(G_DEFAULT_ICON, "images/green1_p.png");
   // gicons["Sgreen"] = new GIcon(G_DEFAULT_ICON, "images/green1_s.png");
    
   // gicons["Cyellow"] = new GIcon(G_DEFAULT_ICON, "images/yellow1_p.png");
   // gicons["Myellow"] = new GIcon(G_DEFAULT_ICON, "images/yellow1_m.png");
   // gicons["Syellow"] = new GIcon(G_DEFAULT_ICON, "images/yellow1_s.png");
    
   // gicons["Corange"] = new GIcon(G_DEFAULT_ICON, "images/ORANGE_P.gif");
   // gicons["Morange"] = new GIcon(G_DEFAULT_ICON, "images/ORANGE_M.gif");
   // gicons["Sorange"] = new GIcon(G_DEFAULT_ICON, "images/ORANGE_S.gif");
   
    gicons["Cred"] =  "images/red1_p.png";
    gicons["Mred"] =  "images/red1_m.png";
    gicons["Sred"] =  "images/red1_s.png";
    
    gicons["Cgreen"] =  "images/green1_p.png";
    gicons["Mgreen"] =  "images/green1_m.png";
    gicons["Sgreen"] =  "images/green1_s.png";
    
    gicons["Cyellow"] =  "images/yellow1_p.png";
    gicons["Myellow"] =  "images/yellow1_m.png";
    gicons["Syellow"] =  "images/yellow1_s.png";
    
    gicons["Corange"] =  "images/orange1_p.png";
    gicons["Morange"] =  "images/orange1_m.png";
    gicons["Sorange"] =  "images/orange1_s.png";
    
    gicons["Black"] =  "images/black_circle.gif";
    gicons["Blue"] =  "images/blue_img.png";
    gicons["Purple"] =  "images/Puple_img.png";
   
    //gicons["Black"] = new GIcon(G_DEFAULT_ICON, "images/black_circle.gif");
    
    function load() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        	map.addControl(new GLargeMapControl());
	    	map.addControl(new GMapTypeControl());
        //map.setCenter(new GLatLng(33.802017, -84.463458), 13);
        //map.setZoom(3);
        var xmlDoc = GXml.parse(<%=request.getAttribute("partnerXML").toString()%>);  
        var partners = xmlDoc.documentElement.getElementsByTagName("partner");
        var xmlDoc1 = GXml.parse(<%=request.getAttribute("siteXML").toString()%>);
        var site = xmlDoc1.documentElement.getElementsByTagName("site");
        
        var xmlDoc2 = GXml.parse(<%=request.getAttribute("checkXML").toString()%>);
        var check = xmlDoc2.documentElement.getElementsByTagName("check");
        var zoom=0;
        
        if(check[0].getAttribute("Radius") != '' ) {
        	if(check[0].getAttribute("Radius") == 25)
        	 zoom=9;
        	else if(check[0].getAttribute("Radius") == 10 )
        	 zoom=10;
        	else if(check[0].getAttribute("Radius") == 50) {
        	 zoom=8;
        	 }
        	else if(check[0].getAttribute("Radius") == 100)
        	 zoom=7;
        	else if(check[0].getAttribute("Radius") == 200)
        	 zoom=6;
        	else if(check[0].getAttribute("Radius") == 500)
        	 zoom=5;
        }
        var checkpartner = true;
     <%   if( request.getAttribute("centerPartner")!= null ) { %>
			map.setCenter(new GLatLng(partners[0].getAttribute("lat"),partners[0].getAttribute("lon")), 13);
        	map.setZoom(zoom); 
        	checkpartner=false;
       <%} %>
       	if(checkpartner==false) {
       	//Do nothing
       	}else if(checkpartner==true && site.length > 0 && zoom != 0)  {
        	map.setCenter(new GLatLng(check[0].getAttribute("checkLAt"),check[0].getAttribute("checkLong")), 13);
        	map.setZoom(zoom);
        }
        else if(check[0].getAttribute("checkLAt")!= 'null' && check[0].getAttribute("checkLong") != 'null' && zoom != 0 ) {
        	map.setCenter(new GLatLng(partners[0].getAttribute("lat"),partners[0].getAttribute("lon")), 13);
        	map.setZoom(zoom);
        } else {
        	map.setCenter(new GLatLng(36.81362, -96.554209), 13);
        	map.setZoom(3);
        }
         
        
        
        for (var i = 0; i < site.length; i++) {
	            var lat = parseFloat(site[i].getAttribute("lat"));
	            var lon = parseFloat(site[i].getAttribute("lon"));
				var siteNumber = site[i].getAttribute("siteNumber");
				var siteAddress = site[i].getAttribute("siteAddress");
				var siteCity = site[i].getAttribute("siteCity");
				var siteState = site[i].getAttribute("siteState");
				var siteZipCode = site[i].getAttribute("siteZipCode");
				var icontype = site[i].getAttribute("icontype");
				var msaName = site[i].getAttribute("msaName");
				var appendixName = site[i].getAttribute("appendixName");
				
	            var point = new GLatLng(lat,lon);
	            //map.setCenter(new GLatLng(lat,lon), 13);
	            var dispStr="<table border=0 Cellspacing=0 Cellpading=0>";
	            if(msaName != '') {
			            	dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" + msaName + "</td></tr>" ;					
			     }
			    if(appendixName != '') {
			            dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" + appendixName + "</td></tr>" ;					
			     }
	            
	            if(siteNumber != '') {
	            	dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" + siteNumber + "</td></tr>" ;					
	            }
	            if(siteAddress != '') {
					dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" +siteAddress+ "</td></tr>" ;					            
	            }
	            if(siteCity != '' || siteState != '' || siteZipCode || '') {
	            	dispStr=dispStr+"<tr><td style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'>" + siteCity + " " + siteState + " " + siteZipCode+ "</td></tr>" ;					
	            }
	            dispStr=dispStr+"</table>";
	            var marker = createMarker(point,dispStr,icontype);
	            map.addOverlay(marker);
          }
          var str="";
          var styleStr="style = 'font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;color:#000000;text-decoration:none;font-weight: normal;border-right-width: 0px;border-right-style: none;'";
          
		for (var i = 0; i < partners.length; i++) {
				var lat = parseFloat(partners[i].getAttribute("lat"));
	            var lon = parseFloat(partners[i].getAttribute("lon"));
				var partnerName = partners[i].getAttribute("partnerName");
				var partnerType = partners[i].getAttribute("partnerType");
				var partnerAddress1 = partners[i].getAttribute("partnerAddress1");
				var partnerAddress2 = partners[i].getAttribute("partnerAddress2");
				var partnerCity = partners[i].getAttribute("partnerCity");
				var partnerState = partners[i].getAttribute("partnerState");
				var partnerZipCode = partners[i].getAttribute("partnerZipCode");
				var partnerPhone = partners[i].getAttribute("partnerPhone");
				var partnerPriName = partners[i].getAttribute("partnerPriName");
				var partnerPriPhone = partners[i].getAttribute("partnerPriPhone");
				var partnerPriCell = partners[i].getAttribute("partnerPriCell");
				var partnerPriEmail = partners[i].getAttribute("partnerPriEmail");
				var partnerDistance = partners[i].getAttribute("partnerDistance");
				var icontype = partners[i].getAttribute("icontype");
				var partnerId = partners[i].getAttribute("partnerId");
				
				var point = new GLatLng(lat,lon);
					//alert("NAME "+ partnerName +" "+lat +" "+lon);
				 str="<table border=0 cellpadding=0 cellspacing=0 ><tr><td "+styleStr+">" +partnerName+ "</td></tr>" +
										""+"<tr><td "+styleStr+">Partner Type:&nbsp;" +partnerType+ "</td></tr>";
	            //var displayString = partnerName  + "<br>" 
 				if(partnerAddress1 == '' && partnerAddress2 == '' ) {
 				    //displayString = displayString;
 				    str=str;
 				    
           		} else {
           			//displayString = displayString +  partnerAddress1 + "" + partnerAddress2 + "<br>" ;
           			str=str+"<tr><td "+styleStr+">" +partnerAddress1+ "&nbsp;" +partnerAddress2+ "</td></tr>";
           		}	
	       		if(partnerCity  != '' || partnerState != '' || partnerZipCode != '') {
           			//displayString=displayString + partnerCity + ", " + partnerState + " " + partnerZipCode + "<br>" ;
           			str=str+"<tr><td "+styleStr+">" + partnerCity + ", " + partnerState + " " + partnerZipCode +  "</td></tr>";
           		}
          		if(partnerPhone != '') {
           			//displayString=displayString  + partnerPhone + "<br>"  ;
           			str=str+"<tr><td "+styleStr+">"+ partnerPhone +  "</td>";
           		}
				//displayString=displayString +"<br>" + "Primary Contact" + "<br>" ;
				str=str+"</tr><tr height=5><td></td></tr><tr><td "+styleStr+"></td></tr><tr><td " +styleStr+">Primary Contact</td></tr><tr><td style='padding-left:12px'><table border=0 cellspacing=0 cellpadding=0>";
						
				
	           if(partnerPriName != '') {
           			//displayString=displayString + "&nbsp;&nbsp;&nbsp;" + partnerPriName ;
           			str=str+"<tr><td "+styleStr+">" + partnerPriName+ "</td></tr>";
           		}
           							
           		if(partnerPriPhone == '-' || partnerPriPhone == '') {
           			//displayString=displayString ;
           			str=str;
           		}
           		else {
           			//displayString=displayString + "<br>" + "&nbsp;&nbsp;&nbsp;" + partnerPriPhone ;
           			str=str+"<tr><td "+styleStr+">" + partnerPriPhone+ "</td></tr>";
           		}
      			if(partnerPriCell != '' ) {
           			//displayString=displayString + "<br>" + "&nbsp;&nbsp;&nbsp;" + partnerPriCell ;
           			str=str+"<tr><td "+styleStr+">"+ partnerPriCell+ " (Cell)</td></tr>";
           		}
           		if(partnerPriEmail != '') {
           			//displayString=displayString + "<br>" + "&nbsp;&nbsp;&nbsp;" + partnerPriEmail + "<br>";
           			str=str+"<tr><td "+styleStr+">" + partnerPriEmail+ "</td></tr>";
           			
           		}
            	//displayString=displayString +"<br>" + "Distance: "  + partnerDistance ;
					str=str+"</table></td></tr><tr height=5><td></td></tr><tr><td "+styleStr+">Distance:&nbsp;" + partnerDistance+ "</td></tr>";
					<%if(request.getAttribute("poXml") != null ) { %>
						var xmlDocpo = GXml.parse(<%=request.getAttribute("poXml").toString()%>); 
					        var po = xmlDocpo.documentElement.getElementsByTagName("po");
					        if(po.length > 0) {
					        	str = str+"<tr><td "+styleStr+">Assign to PO: ";
					        	for( var j =0; j< po.length ;j++) {
					        		str = str+"<a href = \"#\" onclick=\"return assignpartner(" +partnerId+","+po[j].getAttribute("poId")+","+po[j].getAttribute("jobId")+");\">" + po[j].getAttribute("poId")+ "</a>";
					        		if(j!=(po.length)-1)
					        			str = str + ", ";
					        	}
					        	str = str + "</td></tr>";
				        	}
				      <% }%>
				        str = str + "</table>";
            	
	            var marker = createMarker(point,str,icontype);
	            map.addOverlay(marker);
         }
         //alert(str);
         //document.write(str);
        }
    }
     function createMarker(point,str,icontype) {
     			//G_DEFAULT_ICON="images/star_shadow.png";
     			if(icontype=="Black" ||icontype=="Blue" || icontype=="Purple") {
     				baseIcon.shadow="http://www.google.com/mapfiles/shadow50.png";
     				baseIcon.iconSize = new GSize(20, 20);
     			 	baseIcon.shadowSize = new GSize(37, 20);
     				baseIcon.iconAnchor = new GPoint(9, 20);
     				baseIcon.infoWindowAnchor = new GPoint(9, 2);
     				baseIcon.infoShadowAnchor = new GPoint(18, 25);
     			}
     			else {
     				baseIcon.shadow= "images/star_shadow.png";
     				baseIcon.iconSize = new GSize(20, 34);
     				baseIcon.shadowSize = new GSize(37, 34);
     				baseIcon.iconAnchor = new GPoint(9, 34);
     				baseIcon.infoWindowAnchor = new GPoint(9, 2);
     				baseIcon.infoShadowAnchor = new GPoint(18, 25);
     			}
     			
     			baseIcon.image=gicons[icontype];
     			//var icon = new GIcon(G_DEFAULT_ICON,gicons[icontype]);
     			//icon.image =   ;
     			//icon.shadow = "images/star_shadow.png";
     			
     			var marker = new GMarker(point,baseIcon);
     			//var str="<table width='215'><tr><td style = 'font-size=12px;'>" +partnerName+ "<a target='_blank' href='http://www.lcls.org/'>Lewis & Clark Library System</a></td></tr><tr><td><img src='http://www.lcls.org/images/galleries/tour/01-BuildingFromLot.JPG' border='0' width='195px' height='95' /></td></tr><tr><td>425 Goshen Road<br />Edwardsville,IL 62025<br />618-656-3216</td></tr></table><br /><a target='_blank' href='http://maps.google.com/maps?q=425 Goshen Road%20Edwardsville,%20IL'>Directions</a>";
		        GEvent.addListener(marker, "click", function() {
          		marker.openInfoWindowHtml(str);
        		});
        		return marker;
		      }
    //]]>
    </script>
    <script>
    	function assignpartner(pid,powoId,jobId)
			{
				document.forms[0].action = 'Partner_Search.do?ref=assign&tabId=3&hiddensave=save&isClicked=1&formtype=powo&typeid='+jobId+'&currentPartner='+pid+'&powoId='+powoId;
				document.forms[0].submit();
				return true;				
			}
    </script>

  </head>
    <body onload="load()" onunload="GUnload()">
    <form action="JobDashboardAction" method="post">
	<table width = "900px" cellpadding="0" cellspacing="0">
		<tr><td>
			<tr><td style="padding-top: 4px; width: 950px; height: 4px"></td></tr>
			<tr>
			<logic:present name="jobTitle" scope="request">
				<logic:present name="vendexjobName" scope="request">
					<td class="labeleboldwhite" style=" padding-left: 6px;">Assign Partner for Job: <%=request.getAttribute("jobTitle") %> (Partner to all Jobs View)</td>
				</logic:present>
			</logic:present>
			<logic:present name="vendexMapView" scope="request">
				<td class="labeleboldwhite" style=" padding-left: 6px;">Assign Partner for Job: <%=request.getAttribute("jobTitle") %> (All Partners/All Jobs View)</td>
			</logic:present>
			<logic:present name="partnerName" scope="request">
				<td class="labeleboldwhite" style=" padding-left: 6px;">PVS Search: <%=request.getAttribute("partnerName") %> (Partner to all Jobs View)</td>
			</logic:present>
			<logic:present name="standard" scope="request">
				<td class="labeleboldwhite" style=" padding-left: 6px;">Assign Partner for Job: <%=request.getAttribute("jobTitle") %> (Job to Partner View)</td>
			</logic:present>
			<logic:present name="MapView" scope="request">
				<td class="labeleboldwhite" style=" padding-left: 6px;"></td>
			</logic:present>
			
			<td align = "right">
			<input type="button" style="width:100;font-family : verdana; padding-left:2;font-size : 10px; font-style : normal; font-weight : normal; color : #000000; letter-spacing : normal; word-spacing : normal; border : 1px #333333 solid; background : #D0D3D5;" name="search123" value="Search Results" onclick="javascript:history.go(-1);" ></td>
			</tr>
			<tr><td style="padding-top: 4px; padding-top: 4px; width: 900px; height: 4px">
		</td></tr>
	</table>
	<table>
	<tr>
	<td  style= "padding-left: 6px;">		 	   	
  	 <div id="map" style="width: 950px; height: 500px"></div> 
  	 </td>
  	  </tr>
  	 </table>
  	 <table border="0" cellpadding="0" cellspacing="0" width=950>
  	 	<tr>
  	 		<td colspan=2 class = labellegend style= "padding-left: 6px;">Legend:</td>
  	 	</tr>
  	 	<tr >
  	 		<td colspan=2></td>
  	 	</tr>
  	 	<tr>	
  	 		<td class = labellegend style= "padding-left: 18px;">Rating: 0.0 -<IMG SRC="images/red1_no_title.png" WIDTH=15 HEIGHT=15 ALT="">- 3.5&nbsp;&nbsp;&nbsp;3.51 -<IMG SRC="images/yellow1_no_title.png" WIDTH=15 HEIGHT=15 ALT="">- 4.5&nbsp;&nbsp;&nbsp;4.51 -<IMG SRC="images/green1_no_title.png" WIDTH=15 HEIGHT=15 ALT="">- 5.0</td>
  	 		<td class = labellegend style= "padding-left: 18px;">Restricted Partner: <IMG SRC="images/orange1_no-title.png" WIDTH=15 HEIGHT=15 ALT=""></td>  	 		
  	 		<td class = labellegend  style= "padding-left: 18px;">Customer Site:</td>
	  	 	<td align="left"  style="padding-top: 3px"><IMG SRC="images/black_circle.gif" width=15 height=15 ALT=""></td>
	  	 	<td class = labellegend  style= "padding-left: 18px;">Additonal Sites:</td>
	  	 	<td align="left"  style="padding-top: 3px"><IMG SRC="images/blue_img.png" width=15 height=15 ALT="">&nbsp;</IMG></td>
	  	 	<td class = labellegend>(Current Project)</td>
	  	 	<td align="left"  style="padding-top: 3px"><IMG SRC="images/Puple_img.png" width=15 height=15 ALT="">&nbsp;</IMG></td>
	  	 	<td class = labellegend>(Other)</td>
  	 	</tr>
  	 	<tr>
  	 		<td  class = labellegend style= "padding-left: 18px;">P = Certified Partner,  M = Minuteman,  S = Speedpay</td>
  	 	</tr>
  	 </table>
  	 </body>
  	 </form>

</html>