<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying incident details against a partner.
*
-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	
<bean:define id = "incform" name = "incidentform" scope = "request"/>



<html:html>
<HEAD>


<%@ include  file="/Header.inc" %>
<%@ include  file="/SendEmail.inc" %>
<%@ include  file = "/NMenu.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>

<title></title>


     <!-- include  file="/NMenu.inc" %>-->




</head>
<%
String cpduser = "";
if (request.getAttribute("cpduser") != null)
	cpduser = (String)request
			.getAttribute("cpduser");
String pid1=request.getParameter("pid");

%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<%if(request.getParameter("page").equalsIgnoreCase("PRM"))
{%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table  border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="left"> 
          <td  id="pop1" width="130" class="toprow1"><a href="javascript:history.go(-1);"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.back"/></a></td>
		   <td  id="pop2" width=670 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>	
	
<%}
else
{ %>
	<div id="menunav">
	<ul>
	    	
	    	 <li>
	    		<a class="drop" href="#"><span>Manage Profile</span></a>
	  			<ul>
	  				<li><a href="Partner_Edit.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&amp;function=Update">Update</a></li>
	  				<li>
			            <a href="#"><span>Incident Reports</span></a>
			            <ul>
			      			<li><a href="javascript:del();">Delete</a></li>
			      			<li><a href="AddIncident.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&amp;function=add&amp;page=PVS">Post</a></li>
		      			</ul>
		        	</li>
	  				<li>
	  				<a href="JobInformationperPartner.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&from=Partner_Edit">Job History</a>
	  				</li>
	  				 
					<!--  add condition for some users 	-->





					<c:if test="${sessionScope.RdmRds eq 'Y'}">
						<c:if
							test="${requestScope.topPartnerName ne 'Minuteman Partners'}">
							<li><a
								href="Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="AddIncidentForm" property="pid"/>&convert=M">Convert
									To Minuteman</a></li>
						</c:if>
						<c:if
							test="${requestScope.topPartnerName ne 'Certified Partners'}">
							<li><a
								href="Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="AddIncidentForm" property="pid"/>&convert=S">Convert
									To Standard</a>
						</c:if>
					</c:if>
					
					
				<!--  add condition  delete for specific point -->	
					<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%> 	
					<li><a href="javascript:delP();">Delete</a></li>
					<%}%> 
					

					<c:if test="${sessionScope.RdmRds eq 'N'}"> 
					<li><a href="javascript:gettingAjaxDataForEmail('<bean:write name="AddIncidentForm" property="pid"/>');",0>Request Restriction</a></li>
					</c:if>
					<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
					<c:if test="${sessionScope.partnerRegStatus eq 'R'}"> 
					<li><a href="javascript:reSendUnamePwd('<bean:write name="AddIncidentForm" property="pid"/>');",0>Send Username/Password"</a></li>
					</c:if>
					<c:if test="${sessionScope.partnerRegStatus eq 'N'}">
					<li><a href="javascript:reSendRegEmail('<bean:write name="AddIncidentForm" property="pid"/>');">Re-send Registration Email</a></li>
					</c:if>
					</c:if>
					
								
								
								
								
					
					<%-- <li><a href="javascript:gettingAjaxDataForEmail('<bean:write name="AddIncidentForm" property="pid"/>');">Request Restriction</a></li>
						
							
					<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
					<li><a href="javascript:delPartner();">Delete</a></li>
					<%}%>
					 
						
	  						<li><a href="javascript:reSendUnamePwd('<bean:write name="AddIncidentForm" property="pid"/>');">Send Username / Password</a></li>
						
						
					 --%>
	  			</ul>
	    	</li>
	  		<li>
	        	<a class="drop" href="#"><span>Manage Partners</span></a>
	  			<ul>
	  				<li>
			            <a href="#"><span>Minuteman</span></a>
			            <ul>
			      			<li><a href="Partner_Edit.do?function=Add&amp;pid=<bean:write name="AddIncidentForm" property="pid"/>&amp;orgTopName=Minuteman Partners">Add</a></li>
			      			<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&amp;action=update">Update</a></li>
			      			<!--  add condition specificaly  -->	
			      			<%
			      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
			      				%>
			      				<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=delete">Delete</a></li>	      				
								<%
							}
							%>
		      			</ul>
		        	</li>
	  				<li>
			            <a href="#"><span>Standard Partner</span></a>
			            <ul>
			      			<li><a href="Partner_Edit.do?function=Add&amp;pid=0&amp;orgTopName=Certified Partners">Add</a></li>
			      			<li><a href="PVSSearch.do?orgTopName=Certified Partners&amp;action=update">Update</a></li>
			      			
			      			<!--  add condition -->
			      			<%
			      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
			      				%>
			      				<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=delete">Delete</a></li>	      				
								<%
							}
							%>
			      			
			      			
		      			</ul>
		        	</li>
	 				<li><a href="PVSSearch.do?orgTopName=all&amp;action=newadded&amp;enablesort=true">New Partners</a></li>
	 				<li><a href="PVSSearch.do?orgTopName=all&amp;action=resentupdated&amp;enablesort=true">Recent Updates</a></li>
	 				<li><a href="PVSSearch.do?orgTopName=all&amp;action=annualreport&amp;enablesort=true">Annual Review Due</a></li>
	  			</ul>
	  		</li>
	  		<li><a class="drop" href="Partner_Search.do?ref=search&amp;type=v">Partner Search</a></li>
	  		<li><a class="drop" href="MCSA_View.do">Manage MCSA</a></li>
		</ul>
	</div>





<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width = "100%"> 
      <table  border="0" cellspacing="0" cellpadding="0" height="18"  width = "100%">
        <tr >           
          <td id="pop2" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu2',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.manageprofile"/></center></a></td>
          <td id="pop1" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managepartner"/></center></a></td>                    
          <td  width="120" class="Ntoprow1" align="center"><a href="Partner_Search.do?ref=search&type=v"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partnersearch"/></center></a></td>
		  <td  width="120" class="Ntoprow1" align="center"><a href="MCSA_View.do"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managemcsa"/></center></a></td>
          <td  class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table> --%>
<%} %>

<SCRIPT language=JavaScript1.2>



if (isMenu) {
arMenu1 = new Array(
120,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",
"Minuteman","#",1,
"Standard Partner","#",1,
"New Partners","PVSSearch.do?orgTopName=all&action=newadded&enablesort=true",0,
"Recent Updates","PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true",0,
"Annual Review Due","PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true",0
)
arMenu1_1=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners",0,
"Update","PVSSearch.do?orgTopName=Minuteman Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Minuteman Partners&action=delete",0
<%}%>
)
arMenu1_2=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners",0,
"Update","PVSSearch.do?orgTopName=Certified Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Certified Partners&action=delete",0
<%}%>
)

arMenu2=new Array(
120,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
"Update","Partner_Edit.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&function=Update",0,
"Incident Reports","#",1,
"Job History","javascript:jobHistory();",0

<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
	<c:if test="${requestScope.topPartnerName ne 'Minuteman Partners'}"> 	
		,"Convert To Minuteman","Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="AddIncidentForm" property="pid"/>&convert=M",0
	</c:if>
	<c:if test="${requestScope.topPartnerName ne 'Certified Partners'}"> 	
		,"Convert To Standard","Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="AddIncidentForm" property="pid"/>&convert=S",0
	</c:if>
</c:if>

<%-- <%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","javascript:delPartner();",0
<%}%> --%>
<c:if test="${sessionScope.RdmRds eq 'N'}"> 
	,"Request Restriction","javascript:gettingAjaxDataForEmail('<bean:write name="AddIncidentForm" property="pid"/>');",0
</c:if>
<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
	<c:if test="${sessionScope.partnerRegStatus eq 'R'}"> 
		,"Send Username/Password","javascript:reSendUnamePwd('<bean:write name="AddIncidentForm" property="pid"/>');",0
	</c:if>
	<c:if test="${sessionScope.partnerRegStatus eq 'N'}">
		,"Re-send Registration Email","javascript:reSendRegEmail('<bean:write name="AddIncidentForm" property="pid"/>');",0
	</c:if>
</c:if>

)
arMenu2_2=new Array(
"<bean:message bundle="PVS" key="pvs.delete"/>","javascript:del();",0,
"Post","AddIncident.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&function=add&page=PVS",0
)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}





function updatedetaillist() 
{	
	
	
	var partnerResponse = document.getElementById("responseIdSingle").value.trim();
	/* partnerresponse=encodeURIComponent(partnerresponse); */
	var incidentStatus = document.getElementById("incidentStatus").value;		
	var cpdNotes =document.getElementById("notesIdSingle").value.trim();
	/* cpdNotes=encodeURIComponent(cpdNotes); */
	if(incidentStatus!=2){
		if(partnerResponse.trim() == "")
		{
			alert( "Please Provide Partner Response ");
			return false;
		}else if(cpdNotes.trim() == "") {
			
			alert( "Please Provide CPD Research Notes");
			return false;					
		}			
	}

	document.forms[0].action = "AddIncident.do?did=<%=request.getAttribute("inidvalue")%>&notes=<bean:write name="AddIncidentForm" property="notecpd"/>&pid=<%=pid1%>&action=1&function=save&from=single";
	<%-- //&pid=<%=pid1%>&action=1&function=save&page=<%=request.getParameter("page")%>"; --%>
	
	document.forms[0].submit();
					
	return true;
} 

</script>
<Script>
function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		if(obj.disabled == false)
			obj.focus();
		return false;
	}
	return true;
}
function chkCombo(obj,label){
	
	if(obj.value==0 || obj.value == null )	{	
		alert("Please Select "+label);	
		obj.focus();
		return false;
	}
	return true;
}

/* function textCounter( field, countfield, maxlimit ) {
	  if ( field.value.length > maxlimit )
	  {
	//	field.value = field.value.substring(0, maxlimit );
		  
	    alert( 'This field value can only be 500 characters in length.');
	    
	    return false;
	  }
	  else
		  
	 return true;
} */

/* function textCounter(field, maxlen) {
	if (field.value.length > maxlen + 1)
	//alert('This field value can only be 500 characters in length!');
	if (field.value.length > maxlen)
	field.value = field.value.substring(0, maxlen);
	}  */

</Script>
<Script>
function updatedetail() 
{	
	var isValid = false;
	var str="";
	var lengthOfList = document.forms[0].chkdelete.length;
	var partnerresponse="";
	var cpdtxt="";
	var incidentdrop="";
	var inid="";
//	alert("1");
	if(typeof lengthOfList == 'undefined'){
		
		if(!document.forms[0].chkdelete.checked)
		{	
			   alert("Please Select Incident Report to Update");
			     return false;
		
			
		}else if(document.forms[0].chkdelete.checked){
			
			inid = document.getElementById("chkdelete0").value;
			
			partnerresponse = document.getElementById("response0").value.trim();
			partnerresponse=encodeURIComponent(partnerresponse);
			incidentdrop=document.getElementById("incidentStatus0").value.trim();		
			
			cpdtxt =document.getElementById("notecpd0").value.trim();
			cpdtxt=encodeURIComponent(cpdtxt);
			str += inid+ "~~" + partnerresponse+"~~"+cpdtxt+"~~"+incidentdrop+"|@|";	
			if(incidentdrop!=2){
				if(partnerresponse.trim() == "")
				{
					alert( "Please Provide Partner Response ");
					return false;
				}else if(cpdtxt.trim() == "") {
					
					alert( "Please Provide CPD Research Notes");
					return false;					
				}			
			}
		}
	}else{
	for(  var j = 0; j<lengthOfList; j++)
	{
		if(document.forms[0].chkdelete[j].checked)
		{			
			
			inid = document.forms[0].chkdelete[j].value; 
		
			partnerresponse = document.getElementById("response"+j).value.trim();
			partnerresponse=encodeURIComponent(partnerresponse);
			
			incidentdrop=document.getElementById("incidentStatus"+j).value.trim();		
			
			cpdtxt =document.getElementById("notecpd"+j).value.trim();
			cpdtxt=encodeURIComponent(cpdtxt);
			str += inid+ "~~" + partnerresponse+"~~"+cpdtxt+"~~"+incidentdrop+"|@|";		
			
			isValid=true;			
			if(incidentdrop!=2){
				if(partnerresponse.trim() == "")
				{
					alert( "Please Provide Partner Response ");
					return false;
				}else if(cpdtxt.trim() == "") {
					
					alert( "Please Provide CPD Research Notes");
					return false;					
				}			
			}
			
	   } 
			
   }
}		
	if (isValid==false && typeof lengthOfList != 'undefined'){
	     alert("Please Select Incident Report to Update");
	     return false;
	}
//	alert(str);
	//str=decodeURI(str);
	document.forms[0].action = "AddIncident.do?pid=<%=pid1%>&action=1&str="+str+"&function=save";
 	document.forms[0].submit();					
	return true;


}
</Script>


<table>
	
<logic:present name = "incform" property="addmessage">
	<logic:greaterEqual name="incform" property="addmessage" value="0">
		<tr><td   class="message" height="30" ><bean:message bundle="PVS" key="pvs.addedsuccessfully"/></td>
		</tr>	
	</logic:greaterEqual>
</logic:present>

<logic:present name = "incform" property="deletemessage">
	<logic:equal name="incform" property="deletemessage" value="0">
		<tr><td  class="message" height="30" ><bean:message bundle="PVS" key="pvs.deletedsuccessfully"/></td>
		</tr>
	</logic:equal>
	<logic:equal name="incform" property="deletemessage" value="-9001">
		<tr><td   class="message" height="30" ><bean:message bundle="PVS" key="pvs.deletionfailed"/></td>
		</tr>
	</logic:equal>	
</logic:present>
	
</table>

<table>

<html:form action = "AddIncident" >
<%if(request.getParameter("function").equalsIgnoreCase("add")) 
{%>


<script>
function del() 
{
		
		if(<%= request.getAttribute("inidvalue")!=null %>)
		{
			var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
			var convel = confirm( msg );
			if( convel )
			{
				
				document.forms[0].action = "AddIncident.do?did=<%=request.getAttribute("inidvalue")%>&pid=<%=pid1%>&action=1&function=delete&page=<%=request.getParameter("page")%>";
				
				document.forms[0].submit();
								
				return true;	
			}
			
		}
		else
		{
			
			alert( "<bean:message bundle="RM" key="rm.selectdeletion"/>" );
			
		}
		
	
		
}

</script>
 

<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  
  

  <tr> 
    <td colspan="2" class="labeleboldwhite" height="30" ><b><bean:message bundle="PVS" key="pvs.viewincidentreport"/><bean:write name = "incform" property = "partnername"/></b></td>
  </tr> 
    

<tr>
	<td  colspan="2" class="labellobold" height="20" ><bean:message bundle="PVS" key="pvs.incidentreporthash"/>1</td>
 	
</tr>
<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.incidenttype"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "incidenttype"/></td>
	
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.date"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "date"/></td>
	
</tr>
<tr>
	<td class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.time"/></td>
	<td class="labele" height="20" ><bean:write name = "incform" property = "time"/></td>
	
</tr>
<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.endcustomer"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "endcustomer"/></td>
	
</tr>

<%if(request.getAttribute("topPartnerName")!=null && !request.getAttribute("topPartnerName").equals("Minuteman Partners")){%> 
						
<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.technicianname"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "technicianname"/></td>
</tr>					
						
<%}%> 



<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.incident"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "incidentlist"/></td>
</tr> 


<tr>
	<td  class="labelebold" height="20">Incident Status</td>
	<% if (cpduser.equals("Y")) { %>
	<td>
	<html:select name = "AddIncidentForm" property = "incidentStatus"  styleId="incidentStatus" styleClass = "comboe" >
		<html:option value="1">Pending</html:option>
		<html:option value="2">In Review</html:option>
		<html:option value="3">Complete</html:option>
		<html:option value="4">Denied</html:option>
	
		</html:select>
		</td>
		<% } 
		
		else { %>
	<td   class="labele" height="20"><bean:write name="incform" property="incidentStatus" /></td>
	<% } %>
</tr>

<%-- <tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerresponse"/></td>
	<td   class="labelo" height="20"><bean:write name="incform" property="partner
" /></td>
</tr>  --%> 

<tr>
	<td  class="labelobold" height="20">Incident Severity</td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "incidentSeverity"/></td>
</tr>

<%-- <tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.recommendedaction"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "actionlist"/></td>
</tr> --%>

<logic:notEqual name = "incform" property = "jobname" value = "" >
	<tr>
		<td  class = "labelebold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.msaname"/>
		</td>
		<td  class = "labele" height = "20">
			<bean:write name = "incform" property = "msaname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelobold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.appendixname"/>
		</td>
		<td  class = "labelo" height = "20">
			<bean:write name = "incform" property = "appendixname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelebold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.jobname"/>
		</td>
		<td  class = "labele" height = "20">
			<bean:write name = "incform" property = "jobname"/>
		</td>
	</tr>
</logic:notEqual>

<tr>
	<td  class = "labelebold" height = "20">
		<bean:message bundle = "PVS" key = "pvs.notes"/>
	</td>
	<td  class = "labele" height = "20" style="width:300PX;">
		<bean:write name = "incform" property = "notes"/>
	</td>
</tr>


<% if (cpduser.equals("Y")) { %>
 <tr> 
 	<td  class = "labelobold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.partnerresponse"/></td> 
 	<td   class = "labelo"><html:textarea  styleClass = "textbox" property = "response" styleId="responseIdSingle" cols = "25" rows = "3" >></html:textarea>
 	<!-- <p Style="font-size: 9px;text-align: right;">Max Length: 500 Characters</p> 
 	onkeyup="textCounter(this,500);"
 	
 	-->
 	</td> 
 </tr> 
  <tr> 
 	<td  class = "labelebold" height = "20">CPD Reasearch Notes</td> 
 	<td   class = "labele"><html:textarea  styleClass = "textbox" property = "notecpd" styleId="notesIdSingle" cols = "25" rows = "3" ></html:textarea>
 	<!-- <p Style="font-size: 9px;text-align: right;">Max Length: 500 Characters</p> 
 	onkeyup="textCounter(this,,500);" 
 	-->
 	</td> 
 </tr> 
<tr>
<tr>
	<td  class = "labelobold" height = "20">
		Added By
	</td>
	<td  class = "labelo" height = "20">
		<bean:write name = "incform" property = "addedBy"/>
	</td>
</tr>
<tr>	
	<td  class = "labelebold" height = "20">
		Last updated By
	</td>
	<td  class = "labele" height = "20">
		<bean:write name = "incform" property = "updatedBy"/>
	</td>
	
</tr> 	
 
 <td colspan = "2" class = "buttonrow"> 
      <html:submit property = "update" styleClass = "button"  onclick="return updatedetaillist();" >Update</html:submit>
      
    </td>
   </tr> 
<% }else {%>
	<tr> 
 	<td  class = "labelebold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.partnerresponse"/></td> 
 	<td   class = "labele"><html:textarea  styleClass = "textbox" property = "response" cols = "25" rows = "3" readonly="true" ></html:textarea>
 	
 	</td> 
 </tr> 
  <tr> 
 	<td  class = "labelobold" height = "20">CPD Reasearch Notes</td> 
 	<td   class = "labelo"><html:textarea  styleClass = "textbox" property = "notecpd" cols = "25" rows = "3" readonly="true" ></html:textarea></td> 
 </tr> 
<tr>
	<td  class = "labelobold" height = "20">
		Added By
	</td>
	<td  class = "labelo" height = "20">
		<bean:write name = "incform" property = "addedBy"/>
	</td>
</tr>
<tr>	
	<td  class = "labelebold" height = "20">
		Last updated By
	</td>
	<td  class = "labele" height = "20">
		<bean:write name = "incform" property = "updatedBy"/>
	</td>
	
</tr> 	
<% } %>

<%}else if(request.getParameter("function").equalsIgnoreCase("viewParticularIncident")) 
{%>


<script>
function del() 
{
		
		if(<%= request.getAttribute("inidvalue")!=null %>)
		{
			var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
			var convel = confirm( msg );
			if( convel )
			{
				
				document.forms[0].action = "AddIncident.do?did=<%=request.getAttribute("inidvalue")%>&pid=<%=pid1%>&action=1&function=delete&page=<%=request.getParameter("page")%>";
				
				document.forms[0].submit();
								
				return true;	
			}
			
		}
		else
		{
			
			alert( "<bean:message bundle="RM" key="rm.selectdeletion"/>" );
			
		}
}

</script>
 

<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  
  

  <tr> 
    <td colspan="2" class="labeleboldwhite" height="30" ><b><bean:message bundle="PVS" key="pvs.viewincidentreport"/><bean:write name = "incform" property = "partnername"/></b></td>
  </tr> 
    

<tr>
	<td  colspan="2" class="labellobold" height="20" ><bean:message bundle="PVS" key="pvs.incidentreporthash"/>1</td>
 	
</tr>
<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.incidenttype"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "incidenttype"/></td>
	
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.date"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "date"/></td>
	
</tr>
<tr>
	<td class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.time"/></td>
	<td class="labele" height="20" ><bean:write name = "incform" property = "time"/></td>
	
</tr>
<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.endcustomer"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "endcustomer"/></td>
	
</tr>
<%if(request.getAttribute("topPartnerName")!=null && !request.getAttribute("topPartnerName").equals("Minuteman Partners")){%> 
						
<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.technicianname"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "technicianname"/></td>
</tr>					
						
<%}%> 

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.incident"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "incidentlist"/></td>
</tr>
<tr>
	<td  class="labelebold" height="20">Incident Status</td>
	<% if (cpduser.equals("Y")) { %>
	<td>
	<html:select name = "AddIncidentForm" property = "incidentStatus"  styleId="incidentStatus" styleClass = "comboe" >
		<html:option value="1">Pending</html:option>
		<html:option value="2">In Review</html:option>
		<html:option value="3">Complete</html:option>
		<html:option value="4">Denied</html:option>
	
		</html:select>
		</td>
		<% }  else { %>
	<td   class="labele" height="20"><bean:write name="incform" property="incidentStatus" /></td>
	<% } %>
</tr>

<%-- <tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerresponse"/></td>
	<td   class="labelo" height="20"><bean:write name="incform" property="partnerresponse" /></td>
</tr>   --%>

<tr>
	<td  class="labelebold" height="20">Incident Severity</td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "incidentSeverity"/></td>
</tr>


<%-- <tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerstatus"/></td>
	<td   class="labelo" height="20"><bean:write name="incform" property="partnerstatus" /></td>
</tr>

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerresponse"/></td>
	<td   class="labele" height="20"><bean:write name="incform" property="partnerresponse" /></td>
</tr>  

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.incident"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "incidentlist"/></td>
</tr> --%>

<%-- <tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.recommendedaction"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "actionlist"/></td>
</tr> --%>

<logic:notEqual name = "incform" property = "jobname" value = "" >
	<tr>
		<td  class = "labelobold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.msaname"/>
		</td>
		<td  class = "labelo" height = "20">
			<bean:write name = "incform" property = "msaname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelebold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.appendixname"/>
		</td>
		<td  class = "labele" height = "20">
			<bean:write name = "incform" property = "appendixname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelobold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.jobname"/>
		</td>
		<td  class = "labelo" height = "20">
			<bean:write name = "incform" property = "jobname"/>
		</td>
	</tr>
</logic:notEqual>

<tr>
	<td  class = "labelobold" height = "20">
<%-- 		<bean:message bundle = "PVS" key = "Incident Description"/> --%>Incident Description
	</td>
	<td  class = "labelo" height = "20" style="width:300PX;">
		<bean:write name = "incform" property = "notes"/>
	</td>
</tr>

<% if (cpduser.equals("Y")) { %>
 <tr> 
 	<td  class = "labelebold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.partnerresponse"/></td> 
 	<td   class = "labole"><html:textarea  styleClass = "textbox" property = "response" cols = "25" rows = "3" >
 	<!-- <p Style="font-size: 9px;text-align: right;">Max Length: 500 Characters</p> 
 	onkeyup="textCounter(this,,500);" -->
 	</html:textarea></td> 
 </tr> 
  <tr> 
 	<td  class = "labelobold" height = "20">CPD Reasearch Notes</td> 
 	<td   class = "labelo"><html:textarea  styleClass = "textbox" property = "notecpd" cols = "25" rows = "3"  ></html:textarea></td> 
 <!-- <p Style="font-size: 9px;text-align: right;">Max Length: 500 Characters</p> onkeyup="textCounter(this,500);" -->
 </tr> 
<tr>
<tr>
	<td  class = "labelebold" height = "20">
		Added By
	</td>
	<td  class = "labele" height = "20">
		<bean:write name = "incform" property = "addedBy"/>
	</td>
</tr>
<tr>	
	<td  class = "labeloebold" height = "20">
		Last updated By
	</td>
	<td  class = "labelo" height = "20">
		<bean:write name = "incform" property = "updatedBy"/>
	</td>
	
</tr> 
<tr> 
  <td colspan = "2" class = "buttonrow"> 
      <html:submit property = "update" styleClass = "button" onclick="updatedetailist();" >Update</html:submit>
      
 </td> 
   </tr> 
<% }else {%>
	<tr> 
 	<td  class = "labelebold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.partnerresponse"/></td> 
 	<td   class = "labele"><html:textarea  styleClass = "textbox" property = "response" cols = "25" rows = "3" readonly="true"  ></html:textarea>
 	
 	</td> 
   </tr> 
   <tr> 
 	<td  class = "labelobold" height = "20">CPD Reasearch Notes</td> 
 	<td   class = "labelo"><html:textarea  styleClass = "textbox" property = "notecpd" cols = "25" rows = "3" readonly="true" ></html:textarea></td> 
  </tr> 
   <tr>
	<td  class = "labelebold" height = "20">
		Added By
	</td>
	<td  class = "labele" height = "20">
		<bean:write name = "incform" property = "addedBy"/>
	</td>
  </tr>
<tr>	
	<td  class = "labelobold" height = "20">
		Last updated By
	</td>
	<td  class = "labelo" height = "20">
		<bean:write name = "incform" property = "updatedBy"/>
	</td>
	
</tr> 	
<% 
}
}
else
{%>


<logic:present  name="allreportslist" scope="request">
<% int maxreports =0;
	if( request.getAttribute( "totalreports") != null )
	{
		maxreports = ( int ) Integer.parseInt( request.getAttribute( "totalreports" ).toString()); 
	}%>


<script>
function delP() 
{

	
		var convel = confirm( "Are you sure you want to delete the Partner?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?pid=<bean:write name = "AddIncidentForm" property = "pid"/>&func=delete";
			document.forms[0].submit();
			return true;	
		}
} 	

function del() 
{		
		document.forms[0].temp.value='';
		
		if( <%= maxreports %> != 0 )
		{
			if( <%= maxreports %> == 1 )
			{
				if(document.forms[0].chkdelete.checked )
				{
					document.forms[0].temp.value = document.forms[0].chkdelete.value;
					var msg="<bean:message bundle="PVS" key="pvs.deletemsg"/>"
					var convel = confirm( msg );
					if( convel )
					{
						document.forms[0].action = "AddIncident.do?did="+document.forms[0].temp.value+"&pid=<%=pid1%>&action=2&function=delete&page=<%=request.getParameter("page")%>";
						document.forms[0].submit();			
						return true;	
					}
					
				}
				else
				{
					alert( "<bean:message bundle="PVS" key="pvs.selectdeletion"/>" );
					return false;
				}
			}
			else
			{
				
			
				for(  var j = 0; j<document.forms[0].chkdelete.length; j++)
				{
						
					if(document.forms[0].chkdelete[j].checked)
					{	
						document.forms[0].temp.value += document.forms[0].chkdelete[j].value+',';
					}
						
				}
				
				document.forms[0].temp.value = document.forms[0].temp.value.substring(0,document.forms[0].temp.value.length-1);
				
				if(document.forms[0].temp.value == "")
				{
					alert( "<bean:message bundle="PVS" key="pvs.selectdeletion"/>");
				}
				else
				{
					var msg="<bean:message bundle="PVS" key="pvs.deletemsg"/>"
						var convel = confirm( msg );
						if( convel )
						{
							
							document.forms[0].action = "AddIncident.do?did="+document.forms[0].temp.value+"&pid=<%=pid1%>&action=2&function=delete&page=<%=request.getParameter("page")%>";
							
							document.forms[0].submit();
											
							return true;	
						}
				}
			}
		}
}

<%-- function updatedetaillist() 
{	
	alert("1");
	document.forms[0].action = "AddIncident.do?did=&pid=<%=pid1%>&function=save&page=<%=request.getParameter('page')%>";
	document.forms[0].submit();
	return true;	
} 
   --%>
</script>


</logic:present>


<%-- <td colspan = "2" class = "buttonrow"> 
      <html:submit property = "update" styleClass = "button"  onclick="updatedetaillist();" >Update</html:submit>
      
</td> --%>
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
<tr>
  <td  width = "2" height = "0"></td>
  <td><table border = "0" cellspacing = "1" cellpadding = "1" > 
  <logic:present  name = "allreportslist" scope = "request">
  
  <html:hidden property = "temp"/>
  <html:hidden property = "item"/> 
<% int i=0;%>
<logic:iterate id = "arl" name = "allreportslist">

  <tr> 
    <td class = "labeleboldwhite" height = "30" colspan = "2">
    	<html:multibox    name = "AddIncidentForm" property = "chkdelete"  styleId='<%="chkdelete"+i%>'>
    		<bean:write name = "arl" property = "inid"/>
    	</html:multibox>&nbsp;
   		<bean:message bundle = "PVS" key = "pvs.viewincidentreport"/>
   		<bean:write name = "arl" property = "partnername"/>
   	</td>
  </tr> 
     

<tr>
	<td  colspan="2" class="labellobold" height="20" ><bean:message bundle = "PVS" key = "pvs.incidentreporthash"/><%=i+1 %></td>
</tr>

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.incidenttype"/></td>
	<td  class="labele" height="20"><bean:write name = "arl" property = "incidenttype"/></td>
	
</tr>
<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.date"/></td>
	<td  class="labelo" height="20"><bean:write name = "arl" property = "date"/></td>
	
</tr>

<tr>
	<td class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.time"/></td>
	<td class="labele" height="20" ><bean:write name = "arl" property = "time"/></td>
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.endcustomer"/></td>
	<td  class="labelo" height="20"><bean:write name = "arl" property = "endcustomer"/></td>
</tr>
<%if(request.getAttribute("topPartnerName")!=null && !request.getAttribute("topPartnerName").equals("Minuteman Partners")){%> 
						
	<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.technicianname"/></td>
	<td  class="labele" height="20"><bean:write name = "arl" property = "technicianname"/></td>
</tr>				
						
<%}%> 

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.incident"/></td>
	<td  class="labelo" height="20"><bean:write name = "arl" property = "incidentlist"/></td>
</tr> 

<tr>
	<td  class="labelebold" height="20">Incident Status</td>
	<% if (cpduser.equals("Y")) { %>
	<td>
	<html:select name = "arl" property = "incidentStatus"  styleId='<%="incidentStatus"+i%>' styleClass = "comboe" >
		<html:option value="1">Pending</html:option>
		<html:option value="2">In Review</html:option>
		<html:option value="3">Complete</html:option>
		<html:option value="4">Denied</html:option>
	
		</html:select>
<%-- 		<input type = "hidden" name = "incidentStatusDrop" id="<%="incidentStatusDrop"+i%>" value="" ></input> --%>
		</td>
		<% }  else { %>
	<td   class="labele" height="20"><bean:write name="arl" property="incidentStatus" /></td>
	<% } %>
</tr>

<%-- <tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerresponse"/></td>
	<td   class="labelo" height="20"><bean:write name="incform" property="partnerresponse" /></td>
</tr>  --%> 

<tr>
	<td  class="labelobold" height="20">Incident Severity</td>
	<td  class="labelo" height="20"><bean:write name = "arl" property = "incidentSeverity"/></td>
</tr>
<%-- <tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerstatus"/></td>
	<td   class="labelo" height="20"><bean:write name="arl" property="partnerstatus" /></td>
</tr> 

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerresponse"/></td>
	<td   class="labele" height="20"><bean:write name="arl" property="partnerresponse" /></td>
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.incident"/></td>
	<td  class="labelo" height="20"><bean:write name = "arl" property = "incidentlist"/></td>
</tr>
 --%>
<%-- <tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.recommendedaction"/></td>
	<td  class="labele" height="20"><bean:write name = "arl" property = "actionlist"/></td>
</tr> --%>

<logic:notEqual name = "arl" property = "jobname" value = "" >
	<tr>
		<td  class = "labelebold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.msaname"/>
		</td>
		<td  class = "labele" height = "20">
			<bean:write name = "arl" property = "msaname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelobold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.appendixname"/>
		</td>
		<td  class = "labelo" height = "20">
			<bean:write name = "arl" property = "appendixname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelebold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.jobname"/>
		</td>
		<td  class = "labele" height = "20">
			<bean:write name = "arl" property = "jobname"/>
		</td>
	</tr>
</logic:notEqual>

<tr>
	<td  class = "labelebold" height = "20">
		<bean:message bundle = "PVS" key = "pvs.notes"/>
	</td>
	<td  class = "labele" height = "20" style="width:300PX;" >
		<bean:write name = "arl" property = "notes"/>
	</td>
</tr>

<% if (cpduser.equals("Y")) { %>
 <tr> 
 	<td  class = "labelobold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.partnerresponse"/></td> 
 	<td   class = "labelo">
 	
 	<html:textarea   styleId='<%="response"+i%>' styleClass = "textbox" name = "arl" property = "response" cols = "25" rows = "3" ></html:textarea>
 	<!-- <p Style="font-size: 9px;text-align: right;">Max Length: 500 Characters</p> 
 	onkeyup="textCounter(this,490);"
 	-->
<%--  	<input type = "hidden"  name = "partnerresponse" id = "<%="partnerresponse"+i%>"   value="" ></input>  --%>
 	</td> 
 </tr> 
  <tr> 
 	<td  class = "labelebold" height = "20">CPD Reasearch Notes</td> 
 	<td   class = "labele"><html:textarea  styleId='<%="notecpd"+i%>' styleClass = "textbox" name = "arl"  property = "notecpd" cols = "25" rows = "3" ></html:textarea>
<%--  	<input type = "hidden"  name = "cpdnotes" id = "<%="cpdnotes"+i%>"   value="" ></input>  --%>
 	<!-- <p Style="font-size: 9px;text-align: right;">Max Length: 500 Characters</p> 
 	onkeyup="textCounter(this,500);"
 	-->
 	</td> 
 
 </tr>
 

<tr>
	<td  class = "labelobold" height = "20">
		Added By
	</td>
	<td  class = "labelo" height = "20">
		<bean:write name = "arl" property = "addedBy"/>
	</td>
</tr>
<tr>	
	<td  class = "labelebold" height = "20">
		Last updated By
	</td>
	<td  class = "labele" height = "20">
		<bean:write name = "arl" property = "updatedBy"/>
	</td>
	
</tr> 
<tr>
<%-- <td colspan = "2" class = "buttonrow"> 
      <html:submit property = "update" styleClass = "button"  onclick="updatedetaillist();" >Update</html:submit>
      
</td> --%> 	 
   </tr> 
<% }else {%>
	<tr> 
 	<td  class = "labelobold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.partnerresponse"/></td> 
 	<td   class = "labelo"><html:textarea  styleClass = "textbox" name = "arl" property = "response" cols = "25" rows = "3" readonly="true" ></html:textarea>
 	
 	
 	</td> 
 </tr> 
  <tr> 
 	<td  class = "labelebold" height = "20">CPD Reasearch Notes</td> 
 	<td   class = "labele"><html:textarea  styleClass = "textbox"  name = "arl" property = "notecpd"  cols = "25" rows = "3" readonly="true"  >
 	</html:textarea>
 	
 	</td> 
 </tr> 
<tr>
	<td  class = "labelobold" height = "20">
		Added By
	</td>
	<td  class = "labelo" height = "20">
		<bean:write name = "arl" property = "addedBy"/>
	</td>
</tr>
<tr>	
	<td  class = "labelebold" height = "20">
		Last updated By
	</td>
	<td  class = "labele" height = "20">
		<bean:write name = "arl" property = "updatedBy"/>
	</td>
	
</tr> 	
<% } %>

<tr>
	<td colspan="2" height="20"></td>
</tr>

<%i++; %>
</logic:iterate>

<% 
 int maxreports = ( int ) Integer.parseInt( request.getAttribute( "totalreports" ).toString()); 
 if (cpduser.equals("Y")  &&  (maxreports> 0 ))  { %>
 <tr><td colspan = "2" class = "buttonrow"> 
      <html:submit property = "update" styleClass = "button"  onclick="return updatedetail();" >Update</html:submit>
      
 </td></tr>
 <%} else if(maxreports==0)
		{%>
		
		<tr><td  class="message" height="30" colspan="2"><bean:message bundle="PVS" key="pvs.noreportspresent"/></td></tr>
		<tr><td colspan="2" height="100" ></td></tr>
		
		<%} 
%>	


</logic:present>
<%} %>


	
<jsp:include page = '/Footer.jsp'>
  	<jsp:param name = 'colspan' value = '28'/>
    <jsp:param name = 'helpid' value = 'roleprivilege'/>
</jsp:include>
 </table>
 </td>
 </tr> 
</table>

<logic:present name = "refreshtree" scope = "request">
	<script>
			parent.ilexleft.location.reload();
	</script>
</logic:present>


</html:form>
</table>
</BODY>
<logic:present name="totalreports" scope="request">
  		 <logic:equal name="totalreports" value="0"> 
  		 	<script>
  		 		function del(){
  		 		history.go(0);
  		 		} 
  		 	</script>
  		 </logic:equal>
</logic:present>  

<script>



function CheckQuotes ()
{
	
	var field = document.forms[0];
	for( i = 0; i < field.length; i++ )
	{
		if( field[i].type == 'text' )
		{
			
			if( ( field[i].value.indexOf( "'" ) >= 0 ) || ( field[i].value.indexOf( "\"" ) >= 0 ) || ( field[i].value.indexOf( "&" ) >= 0 ) )
			{

				errormsg = "<bean:message bundle="RM" key = "rm.warnmsgchar"/>";
				alert( errormsg );
				field[i].focus();
				return false;
			}
		}
	}
return true;
}




</script>

</html:html>
