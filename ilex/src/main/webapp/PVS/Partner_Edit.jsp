
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
-->
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.DataSource"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script language="JavaScript" src="javascript/JQueryMain.js?timestamp="+(newDate()*1)></script>
<bean:define id="dcCountryList" name="dcCountryList" scope="request" />
<bean:define id="dcStateCM" name="dcStateCM" scope="request" />

<bean:define id="dcWlCompany" name="dcWlCompany" scope="request" />
<bean:define id="dcSDCompany" name="dcSDCompany" scope="request" />
<bean:define id="dcITCompany" name="dcITCompany" scope="request" />
<bean:define id="dcTACompany" name="dcTACompany" scope="request" />
<bean:define id="dcResourceLevel" name="dcResourceLevel" scope="request" />
<bean:define id="dcHighestCriticalityAvailable"
	name="dcHighestCriticalityAvailable" scope="request" />
<bean:define id="dcCertifications" name="dcCertifications"
	scope="request" />
<bean:define id="dcPartnerstatus" name="dcPartnerstatus" scope="request" />
<script>
$(document).ready(function() {
	
	//Default Select the Other Radio Button on the Upload Tab
	$('input[name=uploadedDocType]:eq(5)').attr('checked', 'checked');
	
	
	//Click Event on the Radio buton on Upload Tab 
	$('#w9Upload').click(function() {
		
		
		document.getElementById('displayID').value="W-9 Upload";
		$('#displayID').attr('readonly', true);
	});
	
	$('#insuranceUpload').click(function() {
	document.getElementById('displayID').value="Proof of Insurance Upload";
	$('#displayID').attr('readonly', true);
	});
	$('#resumeUpload').click(function() {
		document.getElementById('displayID').value="Resume Upload";
		$('#displayID').attr('readonly', true);
		});
	
	$('#backUpload').click(function() {
		document.getElementById('displayID').value='Background Check';
		$('#displayID').attr('readonly', true);
		});
	
	
	
	$('#drugUpload').click(function() {
		
		document.getElementById('displayID').value='Drug Screen';
		$('#displayID').attr('readonly', true);
		
	});
	
	
	
	$('#otherUpload').click(function() {
		document.getElementById('displayID').value='';
		$('#displayID').attr('readonly', false);
		});
	
	
	
	
});


function checkAlphaNumericInput(obj, size){
	var initVal = $(obj).val();
	if(initVal.length > size) {
		$(obj).val(initVal.substring(0,size));
		return false;
	}
		
    outputVal = initVal.replace(/[^0-9a-zA-Z,]/g,"");  
    if (initVal != outputVal) {
        $(obj).val(outputVal);
    }
}
</script>
<%
	Connection conn = ((DataSource) request.getAttribute("SQL"))
			.getConnection();
	Statement stmt = conn.createStatement();
	String temp_str = null;
	String topPartnerName = "";
	int k = 0;
	int CompetencySize = 0;
	int keywordRowSize = 3;
	String noLatLong = "";
	String acceptAddress = "";
	String action = "";
	String partnerName = "";

	if (request.getAttribute("CompetencySize") != null)
		CompetencySize = Integer.parseInt(request
				.getAttribute("CompetencySize") + "");
	if (request.getAttribute("topPartnerName") != null)
		topPartnerName = request.getAttribute("topPartnerName")
				.toString();
	if (request.getAttribute("noLatLong") != null)
		noLatLong = request.getAttribute("noLatLong").toString();
	if (request.getAttribute("acceptAddress") != null)
		acceptAddress = request.getAttribute("acceptAddress")
				.toString();
	if (request.getAttribute("action") != null)
		action = request.getAttribute("action").toString();
	if (request.getAttribute("partnerName") != null)
		partnerName = request.getAttribute("partnerName").toString();

	String labelBold = "";
	String labeleBold = "";
	String labele = "";
	String labelCombo = "";
	String labelo = "";
	String labelered = "";
	String labelored = "";
	int topDiv = 125;

	int tech_info_size = 0;
	temp_str = "DeleteTechnician(0,0,document.getElementById('dynaTech'),0)";

	if (request.getAttribute("tech_info_size") != null)
		tech_info_size = (int) Integer.parseInt(request.getAttribute(
				"tech_info_size").toString());

	if (request.getAttribute("topPartnerName") != null
			&& request.getAttribute("topPartnerName").equals(
					"Minuteman Partner")) {
		labelBold = "labelobold";
		labeleBold = "labelebold";
		labele = "labelo";
		labelo = "labele";
		labelCombo = "comboo";
		labelered = "labelored";
		labelored = "labelered";
	} else {
		labelBold = "labelebold";
		labeleBold = "labelobold";
		labele = "labele";
		labelo = "labelo";
		labelCombo = "comboe";
		labelered = "labelered";
		labelored = "labelored";
	}
%>
<tml:html>
<HEAD>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv = "Content-Type" content = "text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=9"/>



<%@ include file="/SendEmail.inc"%>
<%@ include file="/AjaxStateCountry.inc"%>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<title></title>

</head>
<script>


function gettingAjaxDataForInsert(pid){
		
        var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        var aRecommended = document.getElementById("allRecommendedPartnerList");
    	var sRecommended = document.getElementById("selectedRecommendedPartnerList");

    	var aRestricted= document.getElementById("allRestrictedPartnerList");
    	var sRestricted = document.getElementById("selectedRestrictedPartnerList");
    	var opt,list;
    	list="";
        for (i=0;i<aRecommended.options.length;i++) {
          if (aRecommended.options[i].selected) {
            opt = aRecommended.options[i];
            list=list+opt.value+",";
            sRecommended.options[sRecommended.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
            aRecommended.options[i] = null;
            aRestricted.options[i] = null;
            
            i = i - 1;
          }
        }
        sortAvailable(aRecommended);
    	sortAvailable(sRecommended);
    	sortAvailable(aRestricted);
    	sortAvailable(sRestricted);
       
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		var response = ajaxRequest.responseText;
               		if(response!=""){
                   		         		              		 				
                   		}
               }
        }
	
	
		var  the_data = list;
		
        ajaxRequest.open("POST", "Partner_Edit.do?checkProvidedPid="+the_data+"&Pid="+pid, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("checkProvidedPid="+the_data); 
}
///////////////////////////////////////////////////////////////////
//Restric   case
function gettingAjaxDataForInsertRes(pid){
		
        var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        var opt,list;
    	list="";
    	var aRecommended = document.getElementById("allRecommendedPartnerList");
    	var sRecommended = document.getElementById("selectedRecommendedPartnerList");

    	var aRestricted= document.getElementById("allRestrictedPartnerList");
    	var sRestricted = document.getElementById("selectedRestrictedPartnerList");
        for (i=0;i<aRestricted.options.length;i++) {
          if (aRestricted.options[i].selected) {
            opt = aRestricted.options[i];
            list=list+opt.value+",";
            sRestricted.options[sRestricted.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
            aRestricted.options[i] = null;
            aRecommended.options[i] = null;
            i = i - 1;
          }
        }
        sortAvailable(aRecommended);
    	sortAvailable(sRecommended);
    	sortAvailable(aRestricted);
    	sortAvailable(sRestricted);
       
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		var response = ajaxRequest.responseText;
               		if(response!=""){
                   		}
               }
        }
	
	
		var  the_data = list;
		
        ajaxRequest.open("POST", "Partner_Edit.do?checkProvidedPidRI="+the_data+"&Pid="+pid, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("checkProvidedPidRI="+the_data); 
}
////   Remove case Recommend
function gettingAjaxDataForRemove(pid){
    var ajaxRequest;  // The variable that makes Ajax possible!
    try{
           // Opera 8.0+, Firefox, Safari
           ajaxRequest = new XMLHttpRequest();
    } catch (e){
           // Internet Explorer Browsers
           try{
                   ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
           } catch (e) {
                   try{
                           ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                   } catch (e){
                           // Something went wrong
                           alert("Your browser broke!");
                           return false;
                   }
           }
    }
    
    
    var aRecommended = document.getElementById("allRecommendedPartnerList");
	var sRecommended = document.getElementById("selectedRecommendedPartnerList");

	var aRestricted= document.getElementById("allRestrictedPartnerList");
	var sRestricted = document.getElementById("selectedRestrictedPartnerList");
	 var opt,list;
 	list="";
    for (i=0;i<sRecommended.options.length;i++) {
      if (sRecommended.options[i].selected) {
         opt = sRecommended.options[i];
         list=list+opt.value+",";
        aRecommended.options[aRecommended.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        aRestricted.options[aRestricted.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        sRecommended.options[i] = null;
        i = i - 1;
      }
    }
    sortAvailable(aRecommended);
	sortAvailable(sRecommended);
	sortAvailable(aRestricted);
	sortAvailable(sRestricted);
    
    
    
    ajaxRequest.onreadystatechange = function(){
           if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
           		var response = ajaxRequest.responseText;
           		if(response!=""){
           		}
 			
           }
    }
	var  the_data = list;
	   ajaxRequest.open("POST", "Partner_Edit.do?checkProvidedPidRD="+the_data+"&Pid="+pid, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("checkProvidedPidRD="+the_data); 
}

// Delete Remoce case for Restrict
function gettingAjaxDataForRemoveRes(pid){
    var ajaxRequest;  // The variable that makes Ajax possible!
    try{
           // Opera 8.0+, Firefox, Safari
           ajaxRequest = new XMLHttpRequest();
    } catch (e){
           // Internet Explorer Browsers
           try{
                   ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
           } catch (e) {
                   try{
                           ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                   } catch (e){
                           // Something went wrong
                           alert("Your browser broke!");
                           return false;
                   }
           }
    }
    
    
    var aRecommended = document.getElementById("allRecommendedPartnerList");
	var sRecommended = document.getElementById("selectedRecommendedPartnerList");

	var aRestricted= document.getElementById("allRestrictedPartnerList");
	var sRestricted = document.getElementById("selectedRestrictedPartnerList");
	 var opt,list;
	 	list="";
    for (i=0;i<sRestricted.options.length;i++) {
      if (sRestricted.options[i].selected) {
         opt = sRestricted.options[i];
         list=list+opt.value+",";
        aRestricted.options[aRestricted.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        aRecommended.options[aRecommended.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        sRestricted.options[i] = null;
        i = i - 1;
      }
    }
    sortAvailable(aRecommended);
	sortAvailable(sRecommended);
	sortAvailable(aRestricted);
	sortAvailable(sRestricted);
    
    
    
    ajaxRequest.onreadystatechange = function(){
           if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
           		var response = ajaxRequest.responseText;
           		if(response!=""){
           		}
 			
           }
    }
    var  the_data = list;
	   ajaxRequest.open("POST", "Partner_Edit.do?checkProvidedPidRDD="+the_data+"&Pid="+pid, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("checkProvidedPidRDD="+the_data); 
}

</script>



<script>

//****************************** DIV 'a' ******************************************
function validateDivA(){
	viewChange('a');
		if('<%=topPartnerName%>'=="Minuteman Partner") {
			if(! chkBlank(document.forms[0].partnerLastName,"<bean:message bundle="PVS" key="pvs.minutemanlastname"/>")) return false;
			if(! chkBlank(document.forms[0].partnerFirstName,"<bean:message bundle="PVS" key="pvs.minutemanfirstname"/>")) return false;
		} else {
			if(! chkBlank(document.forms[0].partnerCandidate,"<bean:message bundle="PVS" key="pvs.partnercandidate"/>")) return false;
		}
		
		if(! chkBlank(document.forms[0].address1,"<bean:message bundle="PVS" key="pvs.address1"/>"))	return false;
		if(! chkBlank(document.forms[0].city,"<bean:message bundle="PVS" key="pvs.city"/>"))	return false;	
		if(! chkCombo(document.forms[0].stateSelect,"<bean:message bundle="PVS" key="pvs.state"/>"))return false;
		if(! chkCombo(document.forms[0].country,"<bean:message bundle="PVS" key="pvs.country"/>"))return false;
		
		if(document.forms[0].country.value=='US' ) {
			if(! chkBlank(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>"))	return false;
		}else {
			if(document.forms[0].country.value=='CA') {
				if(! chkBlank(document.forms[0].zip,"<bean:message bundle="PVS" key="pvs.zip"/>"))	return false;
				if(! checkCanadianZipcode(document.forms[0].zip)) {
					alert('Canadian postal codes contain six characters in the format X9X 9X9, where X is a letter and 9 is a digit, with a space separating the third and fourth characters. For example: K1A 0B1. Please correct your input.');
					document.forms[0].zip.focus();
					return false;
				}
			}
		}
		
		if(! chkBlank(document.forms[0].mainPhone,"<bean:message bundle="PVS" key="pvs.mainphone"/>"))return false;
		
	var submitflag = 'false';
	if(<%=CompetencySize%> != 0) {
		if( <%=CompetencySize%> == 1 )
		{
			if( !( document.forms[0].cmboxCoreCompetency.checked ))
			{
				alert('Please Check Core Competencies');
				return false;
			}
			else
			{
				submitflag = 'true';
			}
		} else {
			
			for( var i = 0; i<document.forms[0].cmboxCoreCompetency.length; i++ )
		  	{
		  		if( document.forms[0].cmboxCoreCompetency[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	
		  	if( submitflag == 'false')
		  	{	
		  		alert('Please Check Core Competencies');
				return false;
		  	}
		}
	}
	
	if(! chkBlank(document.forms[0].primaryFirstName,"Primary First Name"))	return false;
	if(! chkBlank(document.forms[0].primaryLastName,"Primary Last Name"))	return false;
	if(! chkBlank(document.forms[0].prmEmail,"Primary Email"))	return false;
	if(! CheckQuotes(document.forms[0].prmEmail,"Primary Email")) return false;
	
	if(document.forms[0].prmEmail.value=='Not Provided') {
	}else {
		if(! chkEmail(document.forms[0].prmEmail,"Primary Email")) return false;	
	}
	
	if(! chkBlank(document.forms[0].prmPhone,"Primary Phone"))	return false;
	if(! chkBlank(document.forms[0].prmMobilePhone,"Primary Cell"))	return false;
		
	
	
	
  	
	


	
	
	
	


	

	enableFields();	
	return true;
}

</script>
<html:html>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"
	onLoad="moreAttach(1);moreTech(document.getElementById('dynaTech'),1);">
	<div id="menunav">
	    <ul>
	    	<li>
	    		<a href="#" class="drop"><span><bean:message bundle="PVS" key="pvs.manageprofile" /></span></a>
	  			<ul>
	  				<li><a href="Partner_Edit.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=Update">Update</a></li>
	  				<li>
			            <a href="#"><span>Incident Reports</span></a>
			            <ul>
			      			<li><a href="AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=view&page=PVS">View</a></li>
			      			<li><a href="AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=add&page=PVS">Post</a></li>
		      			</ul>
		        	</li>
	  				<li><a href="javascript:jobHistory();">Job History</a></li>
	  				<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
						<c:if test="${requestScope.topPartnerName ne 'Minuteman Partner'}">
	  						<li><a href="Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="Partner_EditForm" property="pid"/>&convert=M">Convert To Minuteman</a></li>
						</c:if>
						<c:if test="${requestScope.topPartnerName ne 'Standard Partner'}">
	  						<li><a href="Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="Partner_EditForm" property="pid"/>&convert=S">Convert To Standard</a></li>
						</c:if>
					</c:if>
					<%
					if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
						%>
	  					<li><a href="javascript:del();">Delete</a></li>
						<%
					}
					%>
					<c:if test="${sessionScope.RdmRds eq 'N'}">
	  					<li><a href="javascript:gettingAjaxDataForEmail('<bean:write name="Partner_EditForm" property="pid"/>');">Request Restriction</a></li>
					</c:if>
					<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
						<c:if test="${sessionScope.partnerRegStatus eq 'R'}">
	  						<li><a href="javascript:reSendUnamePwd('<bean:write name="Partner_EditForm" property="pid"/>');">Send Username / Password</a></li>
						</c:if>
						<c:if test="${sessionScope.partnerRegStatus eq 'N'}">
	  						<li><a href="javascript:reSendRegEmail('<bean:write name="Partner_EditForm" property="pid"/>');">Re-send Registration Email</a></li>
						</c:if>
					</c:if>
	  			</ul>
	    	</li>
	  		<li>
	        	<a href="#" class="drop"><span><bean:message bundle="PVS" key="pvs.managepartner" /></span></a>
	  			<ul>
	  				<li>
			            <a href="#"><span>Minuteman</span></a>
			            <ul>
			      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners">Add</a></li>
			      			<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=update">Update</a></li>
			      			<%
			      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
			      				%>
			      				<li><a href="PVSSearch.do?orgTopName=Minuteman Partners&action=delete">Delete</a></li>	      				
								<%
							}
							%>
		      			</ul>
		        	</li>
	  				<li>
			            <a href="#"><span>Standard Partner</span></a>
			            <ul>
			      			<li><a href="Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners">Add</a></li>
			      			<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=update">Update</a></li>
			      			<%
			      			if (session.getAttribute("RDM") != null && session.getAttribute("RDM").equals("Y")) {
			      				%>
			      				<li><a href="PVSSearch.do?orgTopName=Certified Partners&action=delete">Delete</a></li>	      				
								<%
							}
							%>
		      			</ul>
		        	</li>
	 				<li><a href="PVSSearch.do?orgTopName=all&action=newadded&enablesort=true">New Partners</a></li>
	 				<li><a href="PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true">Recent Updates</a></li>
	 				<li><a href="PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true">Annual Review Due</a></li>
	  			</ul>
	  		</li>
	  		<li><a href="Partner_Search.do?ref=search&type=v" class="drop"><bean:message bundle="PVS" key="pvs.partnersearch" /></a></li>
	  		<li><a href="MCSA_View.do" class="drop"><bean:message bundle="PVS" key="pvs.managemcsa" /></a></li>
		</ul>
	</div>
	<logic:present name="deletemsg" scope="request">
		<table>
			<tr>
				<td class="message" height="30"><bean:message bundle="PVS"
						key="pvs.partneredit.deletefailed" /></td>
			</tr>
		</table>
	</logic:present>

	<logic:present name="retvalue" scope="request">
		<table>
			<logic:present name="noLatLong">
				<tr>
					<td class="message" height="25">The address could not be
						validated. Verify all address fields.</td>
				</tr>
				<%
					topDiv = topDiv + 25;
				%>
			</logic:present>

			<logic:notEqual name="retvalue" value="-1">

				<logic:equal name="retvalue" value="-90010">
					<tr>
						<td class="messagewithwrap" height="25">Duplicate Partner.
							See <bean:write name="Partner_EditForm"
								property="duplicatePartnerName" />,&nbsp;<bean:write
								name="Partner_EditForm" property="address1" />, <bean:write
								name="Partner_EditForm" property="city" />, <bean:write
								name="Partner_EditForm" property="state" />, <bean:write
								name="Partner_EditForm" property="zip" />
						</td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90011">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.notuniqueusername" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90001">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90002">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90003">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>\
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90004">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90005">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90006">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90007">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-90008">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="-9001">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingilexpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>

				<logic:equal name="retvalue" value="0">
					<logic:equal name="Partner_EditForm" property="act" value="Add">
						<tr>
							<td class="message" height="25"><%=topPartnerName%> Added
								Successfully</td>
						</tr>
						<%
							topDiv = topDiv + 25;
						%>
					</logic:equal>

					<logic:equal name="Partner_EditForm" property="act" value="Update">
						<tr>
							<td class="message" height="25"><%=topPartnerName%> Updated
								Successfully</td>
						</tr>
						<%
							topDiv = topDiv + 25;
						%>
					</logic:equal>
				</logic:equal>

			</logic:notEqual>

			<logic:present name="retMySqlValue" scope="request">
				<logic:equal name="retMySqlValue" value="9600">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.partner.connectingtoweb" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>
				<logic:equal name="retMySqlValue" value="9601">
					<tr>
						<td class="messagewithwrap" height="25"><bean:message
								bundle="PVS" key="pvs.error.savingwebpartner" /></td>
					</tr>
					<%
						topDiv = topDiv + 25;
					%>
				</logic:equal>
			</logic:present>

		</table>
	</logic:present>



	<%
		int i = 0;
			String label = "";
	%>
	<BR>
	<b> <a href="#" onclick="viewChange('a')" class="tabtext"><bean:message
				bundle="PVS" key="pvs.partneredit.general" /></a> <a href="#"
		onclick="viewChange('b')" class="tabtext"><bean:message
				bundle="PVS" key="pvs.partneredit.contactinfo" /></a>
		<a href="#" onclick="viewChange('d')" class="tabtext"><bean:message
				bundle="PVS" key="pvs.partneredit.technicianinfo" /></a> 
				<%if (Integer.parseInt(request.getAttribute(
			"isHRUser").toString()) == 1) { %>
				<a href="#"
		onclick="viewChange('e')" class="tabtext"><bean:message
				bundle="PVS" key="pvs.partneredit.toollist" /></a>
				<%  } %>
		<a href="#" onclick="viewChange('g')" class="tabtext"><bean:message
				bundle="PVS" key="pvs.partneredit.certification" /></a> <a href="#"
		onclick="viewChange('h');disableCombo();" class="tabtext"><bean:message
				bundle="PVS" key="pvs.partneredit.adminside" /></a> 
		<a href="#" onclick="viewChange('j')" class="tabtext"><bean:message
				bundle="PVS" key="pvs.partneredit.upload" /></a> <BR>
		<table width="990" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="6" height="0"></td>
				<td colspan="9" class="labeleboldwhite" height="30"><%=topPartnerName%>&nbsp;<%=action%></td>
			</tr>
			<tr>
				<td width="6" height="0"></td>
				<td class="labeleboldwhite" colspan="1" width="125" height="30"><bean:message
						bundle="PVS" key="pvs.editpartnerdetails" />&nbsp;:</td>
				<td id="pdrow2" class="labelenoboldwhite" width="710" height="30"><%=partnerName%></td>
				<td id="pdrow3" class="labeleboldwhite" width="50" height="30"><bean:message
						bundle="PVS" key="pvs.partneredit.adpo" /></td>
				<td id="pdrow4" class="labelenoboldwhite" width="70" height="30"><bean:write
						name="Partner_EditForm" property="adpo" /></td>
				<td id="pdrow5" colspan="5" width="35"></td>
			</tr>
		</table> <html:form action="Partner_Edit" enctype="multipart/form-data">
			<html:hidden property="pid" />
			<html:hidden property="address_id" />
			<html:hidden property="fromtype" />
			<html:hidden property="jobid" />
			<html:hidden property="pri_id" />
			<html:hidden property="sec_id" />
			<html:hidden property="orgid" />
			<html:hidden property="refersh" />
			<html:hidden property="orgTopId" />
			<html:hidden property="negotiatedRate" />
			<!-- to be deleted -->
			<html:hidden property="lat_min" />
			<html:hidden property="lon_min" />
			<html:hidden property="lat_degree" />
			<html:hidden property="lon_degree" />
			<html:hidden property="lat_direction" />
			<html:hidden property="lon_direction" />
			<html:hidden property="resourceDeveloper" />
			<html:hidden property="topPartnerName" />
			<html:hidden property="act" />
			<html:hidden property="latitude" />
			<html:hidden property="longitude" />
			<html:hidden property="latLongAccuracy" />
			<html:hidden property="state" />
			<html:hidden property="partnerListCount" styleId="partnerListCount" />
			<script>
	document.forms[0].act.value = 'Update';
</script>


			<div id="a"
				style="visibility: visible;  POSITION: absolute;top:<%=topDiv%>;left:2;">
				<table width="100%" border="0" cellspacing="1" cellpadding="1"
					align="center">
					<!-- start -->
					<tr>
						<td width="1" height="0"></td>
						<td><table border="0" cellspacing="1" cellpadding="1"
								width="900">

								<logic:present name="topPartnerType" scope="request">
									<logic:notEqual name="topPartnerName" value="Minuteman Partner">
										<tr>
											<td class="labelebold" colspan="2"><bean:message
													bundle="PVS" key="pvs.partnerName" /><font class="red">*</font>:</td>
											<td colspan="6" class="labele"><logic:equal
													name="Partner_EditForm" property="resourceDeveloper"
													value="Y">
													<html:text styleClass="textbox" size="55"
														property="partnerCandidate" tabindex="1" />
												</logic:equal> <logic:notEqual name="Partner_EditForm"
													property="resourceDeveloper" value="Y">
													<html:text styleClass="textbox" size="55"
														property="partnerCandidate" tabindex="1" disabled="true" />
												</logic:notEqual></td>
											<td colspan="2" class="labelebold"><bean:message
													bundle="PVS" key="pvs.partnerincorptype" />:</td>
											<td colspan="2" class="labele"><logic:equal
													name="Partner_EditForm" property="resourceDeveloper"
													value="Y">
													<html:select property="companyType" size="1"
														styleClass="comboe" tabindex="2">
														<html:optionsCollection name="dcCorporationType"
															property="firstlevelcatglist" label="label" value="value" />
													</html:select>
												</logic:equal> <logic:notEqual name="Partner_EditForm"
													property="resourceDeveloper" value="Y">
													<html:select property="companyType" size="1"
														styleClass="comboo" tabindex="2" disabled="true">
														<html:optionsCollection name="dcCorporationType"
															property="firstlevelcatglist" label="label" value="value" />
													</html:select>
												</logic:notEqual> <html:hidden property="partnerLastName" value="" /> <html:hidden
													property="partnerFirstName" value="" /></td>
										</tr>

										<tr>
											<td colspan="2" class="labelobold"><bean:message
													bundle="PVS" key="pvs.partnertaxid" />:</td>
											<td colspan="2" class="labelo" nowrap><logic:equal
													name="Partner_EditForm" property="resourceDeveloper"
													value="Y">
													<html:text styleClass="textbox" size="20" property="taxId"
														tabindex="3" />
												</logic:equal> <logic:notEqual name="Partner_EditForm"
													property="resourceDeveloper" value="Y">
													<bean:message bundle="PVS" key="pvs.seeresourcedevmanager" />
													<html:hidden property="taxId" />
												</logic:notEqual></td>
											<td colspan="4" class="labelo" nowrap></td>
											<td colspan="2" class="labelobold"><bean:message
													bundle="PVS" key="pvs.partnerincorpdate" />:</td>
											<td colspan="2" class="readonlytextodd"><html:text
													styleClass="textbox" size="10" property="dateInc"
													readonly="true" tabindex="4" /> <logic:equal
													name="Partner_EditForm" property="resourceDeveloper"
													value="Y">
													<img src="images/calendar.gif" width=19 height=17 border=0
														align="center"
														onclick="return popUpCalendar(document.forms[0].dateInc, document.forms[0].dateInc, 'mm/dd/yyyy')"
														onmouseover="window.status = 'Date Picker';return true;"
														onmouseout="window.status = '';return true;">
												</logic:equal></td>

										</tr>

										<tr>
											<td colspan="2" class="labele"></td>
											<td colspan="10" class="labelered" height="19"><bean:message
													bundle="PVS" key="pvs.partnertaxidmessage" /></td>
										</tr>
									</logic:notEqual>

									<logic:equal name="topPartnerName" value="Minuteman Partner">
										<tr>

											<td colspan="2" class="labelobold"><bean:message
													bundle="PVS" key="pvs.minutemanlastname" /><font
												class="red">*</font>:</td>
											<td colspan="2" class="labelo"><html:hidden
													property="partnerCandidate" value="" /> <logic:equal
													name="Partner_EditForm" property="resourceDeveloper"
													value="Y">
													<html:text style="" styleClass="textbox" size="17"
														property="partnerLastName" tabindex="1" />
												</logic:equal> <logic:notEqual name="Partner_EditForm"
													property="resourceDeveloper" value="Y">
													<html:text style="" styleClass="textbox" size="17"
														property="partnerLastName" tabindex="1" disabled="true" />
												</logic:notEqual></td>

											<td colspan="2" class="labelobold"><bean:message
													bundle="PVS" key="pvs.minutemanfirstname" /><font
												class="red">*</font>:</td>
											<td colspan="2" class="labelo"><logic:equal
													name="Partner_EditForm" property="resourceDeveloper"
													value="Y">
													<html:text style="" styleClass="textbox" size="17"
														property="partnerFirstName" tabindex="2" />
												</logic:equal> <logic:notEqual name="Partner_EditForm"
													property="resourceDeveloper" value="Y">
													<html:text style="" styleClass="textbox" size="17"
														property="partnerFirstName" tabindex="2" disabled="true" />
												</logic:notEqual></td>

											<td colspan="2" class="labelobold">
												<%
													if (Integer.parseInt(request.getAttribute(
																			"isHRUser").toString()) == 1) {
												%> <bean:message bundle="PVS" key="pvs.ssnm" />: <%
 	}
 %>
											</td>
											<td colspan="2" class="labelo" nowrap>
												<%
													if (Integer.parseInt(request.getAttribute(
																			"isHRUser").toString()) == 1) {
												%> <logic:equal name="Partner_EditForm"
													property="resourceDeveloper" value="Y">
													<html:text styleClass="textbox" size="15" property="taxId"
														tabindex="3" />
												</logic:equal> <logic:notEqual name="Partner_EditForm"
													property="resourceDeveloper" value="Y">
													<bean:message bundle="PVS" key="pvs.seeresourcedevmanager" />
													<html:hidden property="taxId" />
												</logic:notEqual> <%
 	} else {
 %> <html:hidden property="taxId" /> <%
 	}
 %> <html:hidden property="companyType" /> <html:hidden
													property="dateInc" />

											</td>
										</tr>
									</logic:equal>
								</logic:present>

								<%
									if (noLatLong.equals("noLatLong")
													|| acceptAddress.equals("1")) {
								%>
								<tr>
									<td class="labelodark" colspan="12"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:checkbox name="Partner_EditForm"
												property="acceptAddress" value="1" />&nbsp;
							</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:checkbox name="Partner_EditForm"
												property="acceptAddress" value="1" disabled="true" />&nbsp;
							</logic:notEqual> <bean:message bundle="PVS" key="pvs.acceptaddress" /></td>
								</tr>
								<%
									} else {
								%>
								<html:hidden property="acceptAddress" />
								<%
									}
								%>
								<%
									if (request.getAttribute("LatLongNotFound") != null
													&& request.getAttribute("LatLongNotFound").equals(
															"LatLongNotFound")) {
								%>
								<tr>
									<td class="<%=labelo%>" colspan="13"><font
										style="color: blue;"><bean:message bundle="PVS"
												key="pvs.nolatlongfound" /></font></td>
								</tr>
								<%
									}
								%>

								<tr>
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.address" /><font class="red">*</font>:</td>
									<td class="<%=labelo%>" colspan="3"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:text styleClass="textbox" size="45" property="address1"
												tabindex="5" />&nbsp;
					</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:text styleClass="textbox" size="45" property="address1"
												tabindex="5" disabled="true" />&nbsp;
					</logic:notEqual></td>
									<td class="<%=labelored%>" colspan="7"><bean:message
											bundle="PVS" key="pvs.address1.message" /></td>
								</tr>
								<tr>
									<td class="<%=labelBold%>" colspan="2"></td>
									<td class="<%=labele%>" colspan="3"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:text styleClass="textbox" size="45" property="address2"
												tabindex="6" />&nbsp;
					</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:text styleClass="textbox" size="45" property="address2"
												tabindex="6" disabled="true" />&nbsp;
					</logic:notEqual></td>
									<td class="<%=labelered%>" colspan="7"><bean:message
											bundle="PVS" key="pvs.address2.message" /></td>
								</tr>
								    <tr>
                                          <td class="<%=labeleBold%>" colspan="2"><bean:message
                                        bundle="PVS" key="pvs.vendor.sitecode.id" />:</td>
                                         <td class="<%=labelo%>" colspan="6"><logic:equal
                                       name="Partner_EditForm" property="resourceDeveloper"
                                           value="Y">
                            <html:text styleClass="textbox" size="35" property="vendorSiteCode"
                                       tabindex="7" />
                        </logic:equal> <logic:notEqual name="Partner_EditForm"
                                                       property="resourceDeveloper" value="Y">
                            <html:text styleClass="textbox" size="35" property="vendorSiteCode"
                                       tabindex="7" disabled="true" />
                        </logic:notEqual></td>


                        <td class="<%=labeleBold%>" colspan="2"><bean:message
                            bundle="PVS" key="pvs.comcast.vendor.id" />:</td>
                        <td class="<%=labelo%>" colspan="2"><logic:equal
                            name="Partner_EditForm" property="resourceDeveloper"
                            value="Y">
                            <html:text styleClass="textbox" size="20" property="comcastVendorID"
                                       tabindex="7" maxlength="15" />
                        </logic:equal> <logic:notEqual name="Partner_EditForm"
                                                       property="resourceDeveloper" value="Y">
                            <html:text styleClass="textbox" size="20" property="comcastVendorID"
                                       tabindex="7" disabled="true" maxlength="15"/>
                        </logic:notEqual></td>
                        </tr>
								
								<tr>
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.city" /><font class="red">*</font>:</td>
									<td class="<%=labelo%>" colspan="6"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:text styleClass="textbox" size="35" property="city"
												tabindex="7" />
										</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:text styleClass="textbox" size="35" property="city"
												tabindex="7" disabled="true" />
										</logic:notEqual></td>
										
										<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.vendor.id" />:</td>
										<td class="<%=labelo%>" colspan="2"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:text styleClass="textbox" size="20" property="gpVendorId"
												tabindex="7" maxlength="15" />
										</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:text styleClass="textbox" size="20" property="gpVendorId"
												tabindex="7" disabled="true" maxlength="15"/>
										</logic:notEqual></td>
										
										
								</tr>
								<tr>
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.stateprovince" /><font class="red">*</font>:</td>
									<td class="<%=labele%>" colspan="2"><logic:present
											name="initialStateCategory">

											<logic:equal name="Partner_EditForm"
												property="resourceDeveloper" value="Y">
												<html:select property="stateSelect"
													styleClass="<%= labelCombo %>" tabindex="8"
													onchange="ChangeCountry();">
													<logic:iterate id="initialStateCategory"
														name="initialStateCategory">
														<bean:define id="statecatname" name="initialStateCategory"
															property="siteCountryIdName" />
														<%
															if (!statecatname.equals("notShow")) {
														%>
														<optgroup class=labelebold label="<%=statecatname%>">
															<%
																}
															%>
															<html:optionsCollection name="initialStateCategory"
																property="tempList" value="value" label="label" />
															<%
																if (!statecatname.equals("notShow")) {
															%>
														</optgroup>
														<%
															}
														%>
													</logic:iterate>
												</html:select>
											</logic:equal>

											<logic:notEqual name="Partner_EditForm"
												property="resourceDeveloper" value="Y">
												<html:select property="stateSelect"
													styleClass="<%= labelCombo %>" tabindex="8" disabled="true">
													<logic:iterate id="initialStateCategory"
														name="initialStateCategory">
														<bean:define id="statecatname" name="initialStateCategory"
															property="siteCountryIdName" />
														<%
															if (!statecatname.equals("notShow")) {
														%>
														<optgroup class=labelebold label="<%=statecatname%>">
															<%
																}
															%>
															<html:optionsCollection name="initialStateCategory"
																property="tempList" value="value" label="label" />
															<%
																if (!statecatname.equals("notShow")) {
															%>
														</optgroup>
														<%
															}
														%>
													</logic:iterate>
												</html:select>
											</logic:notEqual>

										</logic:present></td>
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.country" /><font class="red">*</font>:</td>
									<td class="<%=labele%>" colspan="2"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:select property="country" size="1"
												styleClass="<%= labelCombo %>" tabindex="9">
												<html:optionsCollection name="dcCountryList"
													property="firstlevelcatglist" label="label" value="value" />
											</html:select>
										</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:select property="country" size="1"
												styleClass="<%= labelCombo %>" tabindex="9" disabled="true">
												<html:optionsCollection name="dcCountryList"
													property="firstlevelcatglist" label="label" value="value" />
											</html:select>
										</logic:notEqual></td>

									<td class="<%=labelBold%>" colspan="1"><bean:message
											bundle="PVS" key="pvs.zip" />:</td>
									<td class="<%=labele%>" colspan="3"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:text styleClass="textbox" size="15" property="zip"
												tabindex="10" onblur="javascript: convertToUppercase();" />
										</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:text styleClass="textbox" size="15" property="zip"
												tabindex="10" disabled="true" />
										</logic:notEqual></td>
								</tr>


								<tr>
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.mainphone" /><font class="red">*</font>:</td>
									<td class="<%=labelo%>" colspan="2"><html:text
											styleClass="textbox" size="27" property="mainPhone"
											tabindex="11" /></td>
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.mainfax" />:</td>
									<td class="<%=labelo%>" colspan="2"><html:text
											styleClass="textbox" size="20" property="mainFax"
											tabindex="12" /></td>
									<td class="<%=labeleBold%>" colspan="1"><bean:message
											bundle="PVS" key="pvs.companyurl" />:</td>
									<td class="<%=labelo%>" colspan="3"><html:text
											styleClass="textbox" size="25" property="companyURL"
											tabindex="13" /></td>
								</tr>

								<script>
									if(document.forms[0].resourceDeveloper.value == 'N')
										document.forms[0].mainPhone.focus();
									else {
										if(document.forms[0].topPartnerName.value=='Certified Partners')					
												document.forms[0].partnerCandidate.focus();
										else if(document.forms[0].topPartnerName.value=='Minuteman Partners') 
												document.forms[0].partnerLastName.focus();
										}	
							</script>



								<tr height="30">
									<td class="labellobold" colspan="12">Capabilities</td>
								</tr>

								<tr height="20">
									<td class="<%=labelBold%>" colspan="6"><bean:message
											bundle="PVS" key="pvs.corecompetency" /><font class="red">*</font>:</td>
									<td class="<%=labelBold%>" colspan="6"><bean:message
											bundle="PVS" key="pvs.partnerEdit.keyWord" />:</td>
								</tr>

								<tr height="20">
									<td class="<%=labelo%>" colspan="6"><logic:present
											name="dcCoreCompetancy" scope="request">

											<table border="0" width="99%">
												<logic:iterate id="Competency" name="dcCoreCompetancy">
													<%
														if (k % 3 == 0) {
													%>
													<tr>
														<%
															}
														%>
														<td class="<%=labelo%>" width="33%" nowrap="nowrap">
															
																<html:multibox property="cmboxCoreCompetency"
																	tabindex="14">
																	<bean:write name="Competency" property="competancyId" />
																</html:multibox>

															
															<bean:write name="Competency" property="competancyName" />
															<logic:notEmpty name="Competency" property="score" > - <bean:write name="Competency" property="score" />%</logic:notEmpty>
														</td>
														<%
															if (k % 3 != 0 && k % 3 != 1) {
														%>
													</tr>
													<%
														}
																		k++;
													%>
												</logic:iterate>
											</table>
										</logic:present></td>
									<%
										k = ((k / 4) + (k % 2));
												keywordRowSize = keywordRowSize + k;
												String abc = String.valueOf(keywordRowSize);
									%>
									<td class="<%=labelo%>" colspan="6"><html:textarea
											property="keyWord" styleClass="textbox" cols="70"
											rows="<%=abc%>" tabindex="15" /></td>
								</tr>
								<tr height="30">
									<td class="labellobold" colspan="2">Contacts</td>
									<td class="labelloboldcenter" colspan="4"><bean:message
											bundle="PVS" key="pvs.partnerprimary" />:</td>
									<td class="labelloboldcenter" colspan="6"><bean:message
											bundle="PVS" key="pvs.partnersecondary" />:</td>
								</tr>

								<tr height="20">
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partnerfirstname" /><font class="red">*</font>:</td>
									<td class="<%=labele%>" colspan="4"><html:text
											styleClass="textbox" size="20" property="primaryFirstName"
											tabindex="16" /></td>
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partnerfirstname" />:</td>
									<td class="<%=labele%>" colspan="4"><html:text
											styleClass="textbox" size="20" property="secondaryFirstName"
											tabindex="23" /></td>
								</tr>

								<tr height="20">
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partnerlastname" /><font class="red">*</font>:</td>
									<td class="<%=labelo%>" colspan="4"><html:text
											styleClass="textbox" size="20" property="primaryLastName"
											tabindex="17" /></td>
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partnerlastname" />:</td>
									<td class="<%=labelo%>" colspan="4"><html:text
											styleClass="textbox" size="20" property="secondaryLastName"
											tabindex="24" /></td>
								</tr>

								<tr height="20">
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partneremail" /><font class="red">*</font>:</td>
									<td class="<%=labele%>" colspan="4"><html:text
											styleClass="textbox" size="27" property="prmEmail"
											tabindex="18" /> (or <a
										href="javascript: document.forms[0].prmEmail.focus();"
										tabindex="19"
										onclick="javascript: setNotProvided(document.forms[0].prmEmail);">
											Not Provided</a>)</td>
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partneremail" />:</td>
									<td class="<%=labele%>" colspan="4"><html:text
											styleClass="textbox" size="27" property="secEmail"
											tabindex="25"
											onblur="javascript: chkEmail(document.forms[0].secEmail,'Secondary Email');" /></td>
								</tr>

								<tr height="20">
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partnerphone" /><font class="red">*</font>:</td>
									<td class="<%=labelo%>" colspan="4"><html:text
											styleClass="textbox" size="27" property="prmPhone"
											tabindex="20" /></td>
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partnerphone" />:</td>
									<td class="<%=labelo%>" colspan="4"><html:text
											styleClass="textbox" size="20" property="secPhone"
											tabindex="26" /></td>
								</tr>

								<tr height="20">
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partnercell" /><font class="red">*</font>:</td>
									<td class="<%=labele%>" colspan="4"><html:text
											styleClass="textbox" size="27" property="prmMobilePhone"
											tabindex="21" /> (or <a
										href="javascript: document.forms[0].prmMobilePhone.focus();"
										tabindex="22"
										onclick="javascript: document.forms[0].prmMobilePhone.value='Not Provided';">
											Not Provided</a>)</td>
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.partnercell" />:</td>
									<td class="<%=labele%>" colspan="4"><html:text
											styleClass="textbox" size="20" property="secMobilePhone"
											tabindex="27" /></td>
								</tr>

								<tr>
									<td class="<%=labeleBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.webusername" />:</td>
									<td class="<%=labelo%>" colspan="10"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:text styleClass="textbox" size="30"
												property="webUserName" tabindex="28"
												onblur="javascript: setUserPassword();" />
										</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:text styleClass="textbox" size="30"
												property="webUserName" tabindex="28" disabled="true" />
										</logic:notEqual></td>
								</tr>

								<tr>
									<td class="<%=labelBold%>" colspan="2"><bean:message
											bundle="PVS" key="pvs.webpassword" />:</td>
									<td class="<%=labele%>" colspan="10"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:text styleClass="textbox" size="30"
												property="webPassword" tabindex="29" />
										</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:text styleClass="textbox" size="30"
												property="webPassword" tabindex="29" disabled="true" />
										</logic:notEqual></td>
								</tr>

								<logic:notEqual name="Partner_EditForm" property="daysRange"
									value="">

									<tr height="30">
										<td class="labellobold" colspan="12">Availability
											Schedule</td>
									</tr>
									<tr height="20">
										<td class="<%=labelBold%>" colspan="2">Day Range</td>
										<td class="<%=labele%>" colspan="10"><bean:write
												name="Partner_EditForm" property="daysRange" /></td>
									</tr>
									<tr height="20">
										<td class="<%=labeleBold%>" colspan="2">Time Range</td>
										<td class="<%=labelo%>" colspan="10"><bean:write
												name="Partner_EditForm" property="timeRange" /></td>
									</tr>


								</logic:notEqual>


								<tr>
									<td colspan="12" class="labelo"><span
										style="font-weight: bold; margin-right: 241px;">Client
											Recommendations</span> <span style="font-weight: bold;">Client
											Restrictions</span></td>
								</tr>
								<tr>
									<td colspan="12" class="labelebold">
<script type="text/javascript">


document.onreadystatechange = function () {
	  var state = document.readyState
	      
	if (state == 'complete') { 
		var aRecommended = document.getElementById("allRecommendedPartnerList");
		var sRecommended = document.getElementById("selectedRecommendedPartnerList");

		var aRestricted= document.getElementById("allRestrictedPartnerList");
		var sRestricted = document.getElementById("selectedRestrictedPartnerList");
		
		sortAvailable(aRecommended);
		sortAvailable(sRecommended);
		sortAvailable(aRestricted);
		sortAvailable(sRestricted);
	}
}




function addRecommendedItems() {
	var aRecommended = document.getElementById("allRecommendedPartnerList");
	var sRecommended = document.getElementById("selectedRecommendedPartnerList");

	var aRestricted= document.getElementById("allRestrictedPartnerList");
	var sRestricted = document.getElementById("selectedRestrictedPartnerList");
    for (i=0;i<aRecommended.options.length;i++) {
      if (aRecommended.options[i].selected) {
        var opt = aRecommended.options[i];
        sRecommended.options[sRecommended.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        aRecommended.options[i] = null;
        aRestricted.options[i] = null;
        i = i - 1;
      }
    }
    sortAvailable(aRecommended);
	sortAvailable(sRecommended);
	sortAvailable(aRestricted);
	sortAvailable(sRestricted);
  }
  

  
function removeRecommendedItems() {
	var aRecommended = document.getElementById("allRecommendedPartnerList");
	var sRecommended = document.getElementById("selectedRecommendedPartnerList");

	var aRestricted= document.getElementById("allRestrictedPartnerList");
	var sRestricted = document.getElementById("selectedRestrictedPartnerList");
    
    for (i=0;i<sRecommended.options.length;i++) {
      if (sRecommended.options[i].selected) {
        var opt = sRecommended.options[i];
        aRecommended.options[aRecommended.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        aRestricted.options[aRestricted.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        sRecommended.options[i] = null;
        i = i - 1;
      }
    }
    sortAvailable(aRecommended);
	sortAvailable(sRecommended);
	sortAvailable(aRestricted);
	sortAvailable(sRestricted);
  }
  
function addRestrictedItems() {
	var aRecommended = document.getElementById("allRecommendedPartnerList");
	var sRecommended = document.getElementById("selectedRecommendedPartnerList");

	var aRestricted= document.getElementById("allRestrictedPartnerList");
	var sRestricted = document.getElementById("selectedRestrictedPartnerList");
    for (i=0;i<aRestricted.options.length;i++) {
      if (aRestricted.options[i].selected) {
        var opt = aRestricted.options[i];
        sRestricted.options[sRestricted.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        aRestricted.options[i] = null;
        aRecommended.options[i] = null;
        i = i - 1;
      }
    }
    sortAvailable(aRecommended);
	sortAvailable(sRecommended);
	sortAvailable(aRestricted);
	sortAvailable(sRestricted);
  }
  

  
function removeRestrictedItems() {
	var aRecommended = document.getElementById("allRecommendedPartnerList");
	var sRecommended = document.getElementById("selectedRecommendedPartnerList");

	var aRestricted= document.getElementById("allRestrictedPartnerList");
	var sRestricted = document.getElementById("selectedRestrictedPartnerList");
    
    for (i=0;i<sRestricted.options.length;i++) {
      if (sRestricted.options[i].selected) {
        var opt = sRestricted.options[i];
        aRestricted.options[aRestricted.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        aRecommended.options[aRecommended.options.length] = new Option(htmlDecode(opt.innerHTML), opt.value);
        sRestricted.options[i] = null;
        i = i - 1;
      }
    }
    sortAvailable(aRecommended);
	sortAvailable(sRecommended);
	sortAvailable(aRestricted);
	sortAvailable(sRestricted);
  }
  
function htmlDecode(input){
	  var e = document.createElement('div');
	  e.innerHTML = input;
	  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
	}


    function sortAvailable(ai) {
        var tmp = "";
        for (i=0;i<ai.options.length;i++) {
          if (tmp > "") tmp +="|,|";
          tmp += htmlDecode(ai.options[i].innerHTML) + "|~|" + ai.options[i].value ;
        }
        
        var atmp = tmp.split("|,|")
        atmp = atmp.sort();
        ai.options.length = 0
        for (i=0;i<atmp.length;i++) {
          var opt = atmp[i].split("|~|");
          ai.options[i] = new Option(opt[0],opt[1]);
        }
      }
  

  

 
	    
</script> <html:select styleId="allRecommendedPartnerList" 
											name="Partner_EditForm" property="allPartnersForRecommendedArr" size="15"
											style="width: 150px;float:left;"
											multiple="true">
											<html:optionsCollection name="Partner_EditForm"
												property="allPartnersForRecommendedCol" label="label"
												value="value" />
										</html:select> 
										<ul
											style="float: left; width: 35px; list-style: none; text-decoration: none; padding: 0px; margin: 0px; text-align: center; margin-top: 60px;">
											<li><a href="javascript:gettingAjaxDataForInsert('<bean:write name="Partner_EditForm" property="pid"/>');" title="Move Selected"
												style="text-decoration: none;">&gt;</a></li>
										
											<li><a href="javascript:gettingAjaxDataForRemove('<bean:write name="Partner_EditForm" property="pid"/>');" title="Remove Selected"
												style="text-decoration: none;">&lt;</a></li>
										</ul> <html:select styleId="selectedRecommendedPartnerList"
											name="Partner_EditForm" property="selectedPartnersFormRecommendedArr" size='15'
											style="width: 150px;float:left;" multiple="true">
											<html:optionsCollection name="Partner_EditForm"
												property="selectedPartnersFormRecommendedCol" label="label"
												value="value" />
										</html:select> <html:select styleId="allRestrictedPartnerList"
											name="Partner_EditForm" property="allPartnersForRestrictedArr" size="15"
											style="width: 150px;float:left;margin-left:50px;"
											multiple="true">
											<html:optionsCollection name="Partner_EditForm"
												property="allPartnersForRestrictedCol" label="label"
												value="value" />
										</html:select>

										<ul
											style="float: left; width: 35px; list-style: none; text-decoration: none; padding: 0px; margin: 0px; text-align: center; margin-top: 60px;">
											<li><a href="javascript:gettingAjaxDataForInsertRes('<bean:write name="Partner_EditForm" property="pid"/>');" title="Move Selected"
										
												
												style="text-decoration: none;">&gt;</a></li>
											<li><a href="javascript:gettingAjaxDataForRemoveRes('<bean:write name="Partner_EditForm" property="pid"/>');" title="Remove Selected"
												style="text-decoration: none;">&lt;</a></li>
										</ul> <html:select styleId="selectedRestrictedPartnerList"
											name="Partner_EditForm" property="selectedPartnersForRestrictedArr" size='15'
											style="width: 150px;float:left;" multiple="true">
											<html:optionsCollection name="Partner_EditForm"
												property="selectedPartnersForRestrictedCol" label="label"
												value="value" />
										</html:select></td>
								</tr>

















								<tr>
									<td colspan="12" class="buttonrow"><html:submit
											property="save" styleClass="button" tabindex="30"
											onclick="return validateAll();">
											<bean:message bundle="PVS" key="pvs.partneredit.save" />
										</html:submit> <html:reset property="reset" tabindex="31"
											styleClass="button">
											<bean:message bundle="PVS" key="pvs.partneredit.reset" />
										</html:reset> <logic:present name="Partner_EditForm" property="fromtype">
											<logic:equal name="Partner_EditForm" property="fromtype"
												value="powo">
												<html:button property="back" tabindex="32"
													styleClass="button" onclick="return Backaction();">
													<bean:message bundle="PVS" key="pvs.back" />
												</html:button>
											</logic:equal>
										</logic:present></td>
								</tr>
							</table></td>
					</tr>
				</table>
			</div>

			<div id="b"
				style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
				<table width="100%" border="0" cellspacing="1" cellpadding="1"
					align="center">
					<tr>
						<td width="1" height="0"></td>
						<td><table border="0" cellspacing="1" cellpadding="1"
								width="600">

								<tr>
									<td colspan="9" class="labellobold" height="30"><bean:message
											bundle="PVS" key="pvs.afterhours" /></td>
								</tr>

								<tr>
									<td class="tryB" height="20" colspan="1">&nbsp;</td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.afterhoursphone" /></td>
									<td class="tryB" height="20" colspan="5"><bean:message
											bundle="PVS" key="pvs.afterhouremail" /></td>
								</tr>
								<logic:present name="hr_email" scope="request">
									<logic:iterate id="hr" name="hr_email">
										<%
											if ((i++ % 2) == 0)
																label = "labelo";
															else
																label = "labele";
										%>
										<tr class="<%=label%>">
											<td colspan="1"><%=i%></td>
											<td colspan="3"><html:text styleClass="textbox"
													styleId='<%="afterHoursPhone"+(i-1)%>' size="20" name="hr"
													property="afterHoursPhone" /></td>
											<td colspan="5"><html:text styleClass="textbox"
													styleId='<%="afterHoursEmail"+(i-1)%>' size="30" name="hr"
													property="afterHoursEmail" /></td>
										</tr>
									</logic:iterate>
								</logic:present>

							</table></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>

					<tr>
						<td width="1" height="0"></td>
						<td><table width="600" border="0" cellspacing="1"
								cellpadding="1">
								<tr>
									<td colspan="2">
										<table id="dynatable" border="0" cellspacing="1"
											cellpadding="1" width="600">
											<tr>
												
												<td colspan="6" class="labellobold" height="30"><bean:message
														bundle="PVS" key="pvs.physicalfieldoffice" /></td>
											</tr>
											<tr>
												<td class="tryB" height="20" colspan="1"><bean:message
														bundle="PVS" key="pvs.address1" /></td>
												<td class="tryB" height="20" colspan="1"><bean:message
														bundle="PVS" key="pvs.address2" /></td>
												<td class="tryB" height="20" colspan="1"><bean:message
														bundle="PVS" key="pvs.state" /></td>
												<td class="tryB" height="20" colspan="1"><bean:message
														bundle="PVS" key="pvs.zipcode" /></td>
												<td class="tryB" height="20" colspan="1"><bean:message
														bundle="PVS" key="pvs.phone" /></td>
												<td class="tryB" height="20" colspan="1"><bean:message
														bundle="PVS" key="pvs.fax" /></td>
											</tr>
											<%
												i = 0;
														label = "";
											%>
											<logic:present name="phyAddress" scope="request">
												<logic:iterate id="address" name="phyAddress">
													<%
														if ((i % 2) == 0)
																			label = "labelo";
																		else
																			label = "labele";
													%>
													<tr class="<%=label%>">
														<td colspan="1"><html:text styleClass="textbox"
																styleId='<%="phyFieldAddress1"+i%>' size="20"
																name="address" property="phyFieldAddress1" /></td>
														<td colspan="1"><html:text styleClass="textbox"
																styleId='<%="phyFieldAddress2"+i%>' size="20"
																name="address" property="phyFieldAddress2" /></td>
														<td colspan="1"><html:text styleClass="textbox"
																styleId='<%="phyFieldState"+i%>' size="15"
																name="address" property="phyFieldState" /></td>
														<td colspan="1"><html:text styleClass="textbox"
																styleId='<%="phyFieldZip"+i%>' size="10" name="address"
																property="phyFieldZip" /></td>
														<td colspan="1"><html:text styleClass="textbox"
																styleId='<%="phyFieldPhone"+i%>' size="10"
																name="address" property="phyFieldPhone" /></td>
														<td colspan="1"><html:text styleClass="textbox"
																styleId='<%="phyFieldFax"+i%>' size="10" name="address"
																property="phyFieldFax" /></td>
													</tr>
													<%
														i++;
													%>
												</logic:iterate>
											</logic:present>
										</table>
									</td>
								</tr>

								<tr>
									<td class="ButtonRow"><html:button property="addMore"
											styleClass="button" value="Add " onclick="moreAttach()" /> <html:button
											property="delete" styleClass="button"
											onclick="DeleteRow(document.getElementById('dynatable'),2)">Delete</html:button>
									</td>
								</tr>

								<tr>
									<td colspan="2" class="buttonrow"><html:submit
											property="save" styleClass="button"
											onclick="return validateAll();">
											<bean:message bundle="PVS" key="pvs.partneredit.save" />
										</html:submit> <html:reset property="reset" styleClass="button">
											<bean:message bundle="PVS" key="pvs.partneredit.reset" />
										</html:reset></td>
								</tr>
							</table></td>
					</tr>

				</table>
			</div>


    

			<div id="d"
				style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
				<table border="0" cellspacing="1" cellpadding="1" align="center">
					<tr>
						<td width="1" height="0"></td>
						<td>
							<table id="dynaTech" border="0" cellspacing="1" cellpadding="1"
								width="600">
								<%
									i = 0;
											label = "";
								%>
								<logic:present name="tech_info" scope="request">
									<logic:iterate id="info" name="tech_info">
										<bean:define id="techid" name="info" property="tech_id" />
										<tr>
											<td colspan="6" class="labellobold" height="30"><bean:message
													bundle="PVS" key="pvs.technician/engineers" /></td>
										</tr>
										
										<!--     add checkbox for update & deletion  -->	
										<tr> 
    									<td class = "labelebold" height = "10" colspan = "3">
    									
    									
    									 <input type="checkbox" id='<%="tech_idchk"+i%>'  name="tech_idchk"   class="chkbx"  value="<c:out value='${info.tech_id}'/>"/> 
   		   								</td>
  										</tr> 
										
										<tr>
											<TD class="labelebold" colspan="3"></TD>
											<td class="labele" colspan="3"><html:hidden
													styleId='<%="tech_id"+i%>' name="info" property="tech_id" /></td>
										</tr>
										<tr>
											<td class="labelobold" colspan="3"><bean:message
													bundle="PVS" key="pvs.tech/engineername" /></td>
											<td class="labelo" colspan="3"><html:text
													styleClass="engName textbox" size="20" styleId='<%="engName"+i%>'
													name="info" property="engName" /></td>
										</tr>
										<tr>
											<td class="labelebold" colspan="3"><bean:message
													bundle="PVS" key="pvs.resourcelevel" /></td>
											<td class="labele" colspan="3"><html:select
													styleId='<%="cmboxResourceLevel"+i%>' name="info"
													property="cmboxResourceLevel" size="1" styleClass="comboe">
													<html:optionsCollection name="dcResourceLevel"
														property="firstlevelcatglist" label="label" value="value" />
												</html:select></td>
										</tr>

										<tr>
											<td class="labelobold" colspan="3"><bean:message
													bundle="PVS" key="pvs.location" /></td>
											<td class="labelo" colspan="3"><html:text
													styleClass="textbox" size="20" styleId='<%="engZip"+i%>'
													name="info" property="engZip" /></td>
										</tr>
										<tr>
											<td class="labelebold" colspan="3"><bean:message
													bundle="PVS" key="pvs.equippedwith" /></td>
											<td class="labele" colspan="3"><logic:present
													name="equipmentList" scope="request">
													<logic:iterate id="EL" name="equipmentList">
														<html:multibox styleId='<%="chkboxEquipped"+i%>'
															name="info" property="chkboxEquipped">
															<bean:write name="EL" property="label" />
														</html:multibox>
														<bean:write name="EL" property="value" />
														<br>
													</logic:iterate>
												</logic:present> <html:hidden property="chkboxEquipped" value="," /></td>
										</tr>

										<tr>
											<td class="labelobold" colspan="3"><bean:message
													bundle="PVS" key="pvs.union/nonunion" /></td>
											<td class="labelo" colspan="3"><html:select
													styleId='<%="rdoUnion"+i%>' name="info" property="rdoUnion"
													size="1" styleClass="comboo">
													<html:option value="1">
														<bean:message bundle="PVS" key="pvs.partneredit.nonunion" />
													</html:option>
													<html:option value="2">
														<bean:message bundle="PVS" key="pvs.partneredit.union" />
													</html:option>
												</html:select></td>
										</tr>

										<tr>
											<td class="labelebold" colspan="3"><bean:message
													bundle="PVS" key="pvs.highestcriticalityavailable" /></td>
											<td class="labele" colspan="3"><html:select
													styleId='<%="cmboxHighCriticality"+i%>' name="info"
													property="cmboxHighCriticality" size="1"
													styleClass="comboe">
													<html:optionsCollection
														name="dcHighestCriticalityAvailable"
														property="firstlevelcatglist" label="label" value="value" />
												</html:select></td>
										</tr>
										<tr>
											<td class="labelobold" colspan="3"><bean:message
													bundle="PVS" key="pvs.certificationsandskill" /></td>
											<td class="labelo" colspan="3"><html:select
													styleId='<%="cmboxCertifications"+i%>' name="info"
													property="cmboxCertifications" size="5" styleClass="comboe"
													multiple="true">
													<html:optionsCollection name="dcCertifications"
														property="firstlevelcatglist" label="label" value="value" />
												</html:select> <input type="hidden" id='<%="cmboxCertifications" + i%>'
												name="cmboxCertifications" value="~" /></td>
										</tr>
											<tr>
										 <td class="labelebold" colspan="3">Security Certifications:</td>
										 <td class="labele" colspan="3">
											 <table>
												<tbody>
													<tr>
														<td class="labele">
															Drug Screening Certification:
														</td>
														<td>						
															<c:set var="varUrl1" value="${info.checkBox1}" />
															<c:set var="techName" value="${info.tech_id}" />
															<c:set var="file1" value="File1"/>
															<c:set var="file2" value="File2"/>
															<c:set var="date1" value="${info.checkBox1Date}" />
															<c:if test="${date1=='01/01/1900'}">
																<c:set var="date1" value="" />
															</c:if>
															<c:choose>	
																<c:when test="${varUrl1=='1'}">
																	<input type="checkbox" name="checkBox1" value='<%=""+i %>' checked onChange="removeCheck(this)"/>
																</c:when>	
																<c:otherwise >
																	<input type="checkbox" name="checkBox1" value='<%=""+i %>' onchange="removeCheck(this)" />
																</c:otherwise>
															</c:choose>
															<input type="hidden" name="iteratorval" value='<%=""+i %>' />
														</td>
														<td class="labele">
															<html:text name="info" property="checkBox1Date" size="10"  styleClass="textbox" value="${date1}"  />
														</td>
														<td>
															<a href="javascript:uploadFile('<bean:write name="Partner_EditForm" property="pid"/>','${techName}','DrugScreenCert')">UPLOAD</a>
															&nbsp;&nbsp;<a href="ViewuploadedocumentAction.do?ref=downloadCertificate&partner_id=<bean:write name="Partner_EditForm" property="pid"/>&tech_id=${techName}&fileType=DrugScreenCertificate">REVIEW</a>
														</td>
													</tr>
													<tr>
														<td class="labele">
															Criminal Background Certification:
														</td>
														<td>															
															<c:set var="varUrl2" value="${info.checkBox2}" />															
															<c:set var="date2" value="${info.checkBox2Date}" />
															<c:if test="${date2=='01/01/1900'}">
																<c:set var="date2" value="" />
															</c:if>
															<c:choose>	
																<c:when test="${varUrl2=='1'}">
																	<input type="checkbox" name="checkBox2" value='<%=""+i %>' onchange="removeCheck(this)" checked />
																</c:when>	
																<c:otherwise >
																	<input type="checkbox" name="checkBox2" value='<%=""+i %>' onchange="removeCheck(this)"/>
																</c:otherwise>
															</c:choose>
														</td>
														<td class="labele">
															<html:text name="info" property="checkBox2Date" size="10"  styleClass="textbox" value="${date2}" />
														</td>
														<td>
															<a href="javascript:uploadFile('<bean:write name="Partner_EditForm" property="pid"/>','${techName}','CriminalCertificate')">UPLOAD</a>
															&nbsp;&nbsp;<a href="ViewuploadedocumentAction.do?ref=downloadCertificate&partner_id=<bean:write name="Partner_EditForm" property="pid"/>&tech_id=${techName}&fileType=CriminalCertificate">REVIEW</a>
														</td>
													</tr>
													<tr>
														<td class="labele">
															Harassment Free Work<br>Envoirnment Certification:
														</td>
														<td>						
															<c:set var="varUrl3" value="${info.checkBox3}" />															
															<c:set var="date3" value="${info.checkBox3Date}" />
															<c:if test="${date3=='01/01/1900'}">
																<c:set var="date3" value="" />
															</c:if>
															<c:choose>	
																<c:when test="${varUrl3=='1'}">
																	<input type="checkbox" name="checkBox3"  value='<%=""+i %>' checked="checked" onchange="removeCheck(this)"/>
																</c:when>	
																<c:otherwise >
																	<input type="checkbox" name="checkBox3" value='<%=""+i %>' onchange="removeCheck(this)"/>
																</c:otherwise>
															</c:choose>
														</td>
														<td class="labele">
															<html:text name="info" property="checkBox3Date" size="10"  styleClass="textbox" value="${date3}"/>
														</td>
													</tr>
												</tbody>
											</table>									 
										</td> 
										</tr>

										<%
											i++;
															temp_str = "DeleteTechnician("
																	+ techid
																	+ ", "
																	+ tech_info_size
																	+ ", document.getElementById('dynaTech'),0)";
										%>
									</logic:iterate>
								</logic:present>

							</table>
						</td>
					</tr>
					<tr>
						<td width="1" height="0"></td>
						<td class="ButtonRow" colspan="6"><html:button
								property="addMore" styleClass="button" value="Add "
								onclick="moreTech(document.getElementById('dynaTech'),0)" /> <html:button
								property="deletetech" styleClass="button"
								onclick="<%=temp_str%>">Delete</html:button></td>
					</tr>

					<tr>
						<td width="1" height="0"></td>
						<td colspan="2" class="buttonrow"><html:submit
								property="save" styleClass="button"
								onclick="return validateAll();">
								<bean:message bundle="PVS" key="pvs.partneredit.save" />
							</html:submit> <html:reset property="reset" styleClass="button">
								<bean:message bundle="PVS" key="pvs.partneredit.reset" />
							</html:reset></td>
					</tr>

					<jsp:include page='/Footer.jsp'>
						<jsp:param name='colspan' value='28' />
						<jsp:param name='helpid' value='pvsPartner_Edit' />
					</jsp:include>
				</table>
			</div>

			<div id="e"
				style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
				<table width="100%" border="0" cellspacing="1" cellpadding="1"
					align="center">
					<tr>
						<td width="1" height="0"></td>
						<td><table id="tableToollist" border="0" cellspacing="1"
								cellpadding="1" width="600">

								<tr>
									<td colspan="9" class="labellobold" height="30"><b><bean:message
												bundle="PVS" key="pvs.toollist" /></b><br> <bean:message
											bundle="PVS" key="pvs.pleaseenterareazip" /></td>
								</tr>

								<tr>
									<td class="tryB" height="20" colspan="1">&nbsp;</td>
									<td class="tryB" height="20" colspan="4"><bean:message
											bundle="PVS" key="pvs.zip" /></td>
									<td class="tryB" height="20" colspan="4"><bean:message
											bundle="PVS" key="pvs.toolname" /></td>
								</tr>
								<%
									i = 0;
											label = "";
								%>
								<logic:present name="toolList" scope="request">
									<logic:iterate id="TL" name="toolList">
										<%
											if ((i++ % 2) == 0)
																label = "labelo";
															else
																label = "labele";
										%>
										<tr>
											<td class=<%=label%> colspan="1"><html:multibox
													styleId='<%="chkboxTools"+(i-1)%>' property="chkboxTools">
													<bean:write name="TL" property="chkboxTools" />
												</html:multibox><input type="hidden" name="toolsCheckboxValue"
												value='<bean:write name="TL" property="chkboxTools"/>' /></td>

											<td class=<%=label%> colspan="4"><html:text
													styleClass="textbox" size="20" name="TL"
													property="toolZips" styleId='<%="toolZips"+(i-1)%>' /></td>
											<td class=<%=label%> colspan="4"><html:hidden name="TL"
													property="label" /><img style='margin-left:<bean:write name="TL" property="width" />px' alt="arr" src="images/arr_black.png"> <bean:write name="TL" property="label" /></td>
										</tr>
									</logic:iterate>
								</logic:present>

								<tr>
									<td colspan="9" class="buttonrow"><html:submit
											property="save" styleClass="button"
											onclick="return validateAll();">
											<bean:message bundle="PVS" key="pvs.partneredit.save" />
										</html:submit> <html:reset property="reset" styleClass="button">
											<bean:message bundle="PVS" key="pvs.partneredit.reset" />
										</html:reset></td>
								</tr>
								<jsp:include page='/Footer.jsp'>
									<jsp:param name='colspan' value='28' />
									<jsp:param name='helpid' value='pvsPartner_Edit' />
								</jsp:include>
							</table></td>
					</tr>
				</table>
			</div>






			<div id="g"
				style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
				<table width="100%" border="0" cellspacing="1" cellpadding="1"
					align="center">
					<tr>
						<td width="1" height="0"></td>
						<td>
							<table border="0" cellspacing="1" cellpadding="1" width="600">
								<tr>
									<td colspan="9" class="labellobold" height="30"><bean:message
											bundle="PVS" key="pvs.companylevelwireless" /></td>
								</tr>

								<tr>
									<td class="tryB" height="20" colspan="3" width=200>&nbsp;
									</td>
									<td class="tryB" height="20" colspan="3" width=250><bean:message
											bundle="PVS" key="pvs.validfrom" /><br> <bean:message
											bundle="PVS" key="pvs.mm/dd/yyyy" /></td>
									<td class="tryB" height="20" colspan="3" width=250><bean:message
											bundle="PVS" key="pvs.through" /><br> <bean:message
											bundle="PVS" key="pvs.mm/dd/yyyy" /></td>
								</tr>
								<tr>
									<td colSpan='9'>
										<table id="dynatableWL" width="100%" border="0"
											cellspacing="1" cellpadding="1">
											<%
												i = 0;
																label = "";
																String labelbold = "", combo = "";
											%>
											<logic:present name="WLCompnay" scope="request">
												<logic:iterate id="WL" name="WLCompnay">
													<%
														if ((i % 2) == 0) {
																					labelbold = "labelobold";
																					label = "labelo";
																					combo = "comboo";
																				} else {
																					labelbold = "labelebold";
																					label = "labele";
																					combo = "comboe";
																				}
													%>
													<tr>
														<td class='<%=labelbold%>' colspan="3"><html:select
																styleId='<%="wlcompany"+i%>' name="WL"
																property="wlcompany" size="1" styleClass="<%=combo%>">
																<html:optionsCollection name="dcWlCompany"
																	property="firstlevelcatglist" label="label"
																	value="value" />
															</html:select></td>
														<td class="<%=label%>" colspan="3"><html:text
																styleClass="textbox" size="10" name="WL"
																property="validFromWC" readonly="true"
																styleId='<%="validFromWC"+i%>' /> <img
															src="images/calendar.gif" width=19 height=17 border=0
															align="center"
															onclick="return popUpCalendar(document.forms[0].<%="validFromWC" + i%>, document.forms[0].<%="validFromWC" + i%>, 'mm/dd/yyyy')"
															onmouseover="window.status = 'Date Picker';return true;"
															onmouseout="window.status = '';return true;"></td>
														<td class="<%=label%>" colspan="3"><html:text
																styleClass="textbox" size="10" name="WL"
																property="validToWC" readonly="true"
																styleId='<%="validToWC"+i%>' /> <img
															src="images/calendar.gif" width=19 height=17 border=0
															align="center"
															onclick="return popUpCalendar(document.forms[0].<%="validToWC" + i%>, document.forms[0].<%="validToWC" + i%>, 'mm/dd/yyyy')"
															onmouseover="window.status = 'Date Picker';return true;"
															onmouseout="window.status = '';return true;"></td>
													</tr>
													<%
														i++;
													%>
												</logic:iterate>
											</logic:present>
										</table>
									<td>
								</tr>

								<tr>
									<td class="ButtonRow" colspan="3"><html:button
											property="addMore" styleClass="button" value="Add Company"
											onclick="moreAttachCompany(document.getElementById('dynatableWL'),'wlcompany','w','validFromWC','validToWC' )" />
									</td>
									<td class="ButtonRow" colspan="3"><html:button
											property="addMore1" styleClass="button"
											value="Delete Company"
											onclick="DeleteRow(document.getElementById('dynatableWL'),0)" /></td>
									<td class="ButtonRow" colspan="3">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="9" class="labellobold" height="30"><bean:message
											bundle="PVS" key="pvs.companylevelstructured" /></td>
								</tr>

								<tr>
									<td class="tryB" height="20" colspan="3">&nbsp;</td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.validfrom" /><br> <bean:message
											bundle="PVS" key="pvs.mm/dd/yyyy" /></td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.through" /><br> <bean:message
											bundle="PVS" key="pvs.mm/dd/yyyy" /></td>
								</tr>

								<tr>
									<td colSpan='9'>
										<table id="dynatableSD" width="100%" border="0"
											cellspacing="1" cellpadding="1">
											<%
												i = 0;
																label = "";
																labelbold = "";
																combo = "";
											%>
											<logic:present name="SDCompnay" scope="request">
												<logic:iterate id="SD" name="SDCompnay">
													<%
														if ((i % 2) == 0) {
																					labelbold = "labelobold";
																					label = "labelo";
																					combo = "comboo";
																				} else {
																					labelbold = "labelebold";
																					label = "labele";
																					combo = "comboe";
																				}
													%>
													<tr>
														<td class="<%=labelbold%>" colspan="3"><html:select
																styleId='<%="sdcompany"+i%>' name="SD"
																property="sdcompany" size="1" styleClass="<%=combo%>">
																<html:optionsCollection name="dcSDCompany"
																	property="firstlevelcatglist" label="label"
																	value="value" />
															</html:select></td>
														<td class="<%=label%>" colspan="3"><html:text
																styleClass="textbox" size="10" name="SD"
																property="validFromSD" readonly="true"
																styleId='<%="validFromSD"+i%>' /> <img
															src="images/calendar.gif" width=19 height=17 border=0
															align="center"
															onclick="return popUpCalendar(document.forms[0].<%="validFromSD" + i%>, document.forms[0].<%="validFromSD" + i%>, 'mm/dd/yyyy')"
															onmouseover="window.status = 'Date Picker';return true;"
															onmouseout="window.status = '';return true;"></td>
														<td class="<%=label%>" colspan="3"><html:text
																styleClass="textbox" size="10" name="SD"
																property="validToSD" readonly="true"
																styleId='<%="validToSD"+i%>' /> <img
															src="images/calendar.gif" width=19 height=17 border=0
															align="center"
															onclick="return popUpCalendar(document.forms[0].<%="validToSD" + i%>, document.forms[0].<%="validToSD" + i%>, 'mm/dd/yyyy')"
															onmouseover="window.status = 'Date Picker';return true;"
															onmouseout="window.status = '';return true;"></td>
													</tr>
													<%
														i++;
													%>
												</logic:iterate>
											</logic:present>
										</table>
									<td>
								</tr>

								<tr>
									<td class="ButtonRow" colspan="3"><html:button
											property="addMore" styleClass="button" value="Add Company"
											onclick="moreAttachCompany(document.getElementById('dynatableSD'),'sdcompany','sd','validFromSD','validToSD')" />
									</td>
									<td class="ButtonRow" colspan="3"><html:button
											property="addMore1" styleClass="button"
											value="Delete Company"
											onclick="DeleteRow(document.getElementById('dynatableSD'),0)" /></td>
									<td class="ButtonRow" colspan="3">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="9" class="labellobold" height="30"><bean:message
											bundle="PVS" key="pvs.companylevelintegrated" /></td>
								</tr>

								<tr>
									<td class="tryB" height="20" colspan="3">&nbsp;</td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.validfrom" /><br> <bean:message
											bundle="PVS" key="pvs.mm/dd/yyyy" /></td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.through" /><br> <bean:message
											bundle="PVS" key="pvs.mm/dd/yyyy" /></td>
								</tr>

								<tr>
									<td colSpan='9'>
										<table id="dynatableIT" width="100%" border="0"
											cellspacing="1" cellpadding="1">
											<%
												i = 0;
																label = "";
																labelbold = "";
																combo = "";
											%>
											<logic:present name="ITCompnay" scope="request">
												<logic:iterate id="IT" name="ITCompnay">
													<%
														if ((i % 2) == 0) {
																					labelbold = "labelobold";
																					label = "labelo";
																					combo = "comboo";
																				} else {
																					labelbold = "labelebold";
																					label = "labele";
																					combo = "comboe";
																				}
													%>
													<tr>
														<td class="<%=labelbold%>" colspan="3"><html:select
																styleId='<%="itcompany"+i%>' name="IT"
																property="itcompany" size="1" styleClass="<%=combo%>">
																<html:optionsCollection name="dcITCompany"
																	property="firstlevelcatglist" label="label"
																	value="value" />
															</html:select></td>
														<td class="<%=label%>" colspan="3"><html:text
																styleClass="textbox" size="10" name="IT"
																property="validFromIT" readonly="true"
																styleId='<%="validFromIT"+i%>' /> <img
															src="images/calendar.gif" width=19 height=17 border=0
															align="center"
															onclick="return popUpCalendar(document.forms[0].<%="validFromIT" + i%>, document.forms[0].<%="validFromIT" + i%>, 'mm/dd/yyyy')"
															onmouseover="window.status = 'Date Picker';return true;"
															onmouseout="window.status = '';return true;"></td>
														<td class="<%=label%>" colspan="3"><html:text
																styleClass="textbox" size="10" name="IT"
																property="validToIT" readonly="true"
																styleId='<%="validToIT"+i%>' /> <img
															src="images/calendar.gif" width=19 height=17 border=0
															align="center"
															onclick="return popUpCalendar(document.forms[0].<%="validToIT" + i%>, document.forms[0].<%="validToIT" + i%>, 'mm/dd/yyyy')"
															onmouseover="window.status = 'Date Picker';return true;"
															onmouseout="window.status = '';return true;"></td>
													</tr>
													<%
														i++;
													%>
												</logic:iterate>
											</logic:present>
										</table>
									<td>
								</tr>

								<tr>
									<td class="ButtonRow" colspan="3"><html:button
											property="addMore" styleClass="button" value="Add Company"
											onclick="moreAttachCompany(document.getElementById('dynatableIT'),'itcompany','it','validFromIT','validToIT')" />
									</td>
									<td class="ButtonRow" colspan="3"><html:button
											property="addMore1" styleClass="button"
											value="Delete Company"
											onclick="DeleteRow(document.getElementById('dynatableIT'),0)" /></td>
									<td class="ButtonRow" colspan="3">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="9" class="labellobold" height="30"><bean:message
											bundle="PVS" key="pvs.companylevelindustry" /></td>
								</tr>

								<tr>
									<td class="tryB" height="20" colspan="3">&nbsp;</td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.validfrom" /><br> <bean:message
											bundle="PVS" key="pvs.mm/dd/yyyy" /></td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.through" /><br> <bean:message
											bundle="PVS" key="pvs.mm/dd/yyyy" /></td>
								</tr>

								<tr>
									<td colSpan='9'>
										<table id="dynatableTA" width="100%" border="0"
											cellspacing="1" cellpadding="1">
											<%
												i = 0;
																label = "";
																labelbold = "";
																combo = "";
											%>
											<logic:present name="TACompnay" scope="request">
												<logic:iterate id="TA" name="TACompnay">
													<%
														if ((i % 2) == 0) {
																					labelbold = "labelobold";
																					label = "labelo";
																					combo = "comboo";
																				} else {
																					labelbold = "labelebold";
																					label = "labele";
																					combo = "comboe";
																				}
													%>
													<tr>
														<td class="<%=labelbold%>" colspan="3"><html:select
																styleId='<%="tacompany"+i%>' name="TA"
																property="tacompany" size="1" styleClass="<%=combo%>">
																<html:optionsCollection name="dcTACompany"
																	property="firstlevelcatglist" label="label"
																	value="value" />
															</html:select></td>
														<td class="<%=label%>" colspan="3"><html:text
																styleClass="textbox" size="10" name="TA"
																property="validFromTA" readonly="true"
																styleId='<%="validFromTA"+i%>' /> <img
															src="images/calendar.gif" width=19 height=17 border=0
															align="center"
															onclick="return popUpCalendar(document.forms[0].<%="validFromTA" + i%>, document.forms[0].<%="validFromTA" + i%>, 'mm/dd/yyyy')"
															onmouseover="window.status = 'Date Picker';return true;"
															onmouseout="window.status = '';return true;"></td>
														<td class="<%=label%>" colspan="3"><html:text
																styleClass="textbox" size="10" name="TA"
																property="validToTA" readonly="true"
																styleId='<%="validToTA"+i%>' /> <img
															src="images/calendar.gif" width=19 height=17 border=0
															align="center"
															onclick="return popUpCalendar(document.forms[0].<%="validToTA" + i%>, document.forms[0].<%="validToTA" + i%>, 'mm/dd/yyyy')"
															onmouseover="window.status = 'Date Picker';return true;"
															onmouseout="window.status = '';return true;"></td>
													</tr>
													<%
														i++;
													%>
												</logic:iterate>
											</logic:present>
										</table>
									<td>
								</tr>

								<tr>
									<td class="ButtonRow" colspan="3"><html:button
											property="addMore" styleClass="button" value="Add Company"
											onclick="moreAttachCompany(document.getElementById('dynatableTA'),'tacompany','ta','validFromTA','validToTA')" />
									</td>
									<td class="ButtonRow" colspan="3"><html:button
											property="addMore1" styleClass="button"
											value="Delete Company"
											onclick="DeleteRow(document.getElementById('dynatableTA'),0)" /></td>
									<td class="ButtonRow" colspan="3">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="9" class="buttonrow"><html:submit
											property="save" styleClass="button"
											onclick="return validateAll();">
											<bean:message bundle="PVS" key="pvs.partneredit.save" />
										</html:submit> <html:reset property="reset" styleClass="button">
											<bean:message bundle="PVS" key="pvs.partneredit.reset" />
										</html:reset></td>
								</tr>
								<jsp:include page='/Footer.jsp'>
									<jsp:param name='colspan' value='28' />
									<jsp:param name='helpid' value='pvsPartner_Edit' />
								</jsp:include>

							</table>
						</td>
					</tr>
				</table>
			</div>
			<div id="h"
				style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
				<table width="600" border="0" cellspacing="1" cellpadding="1"
					align="center">
					<tr>
						<td height="0" colspan="9"></td>
					</tr>
					<tr>
						<td width="1" height="0"></td>
						<td colspan="9">
							<table width="600" border="0" cellspacing="1">
								<tr>
									<td colspan="2" class="labellobold" height="30"><bean:message
											bundle="PVS" key="pvs.adminsideitems" /></td>
								</tr>
								<tr height="20">
									<td class="labelobold" width="300">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.overallstatus" /></td>
									<td class="labelo" width="300"><logic:equal
											name="Partner_EditForm" property="resourceDeveloper"
											value="Y">
											<html:select property="cmboxStatus" size="1"
												styleClass="comboo">
												<html:optionsCollection name="dcPartnerstatus"
													property="partnerstatus" label="label" value="value" />
											</html:select>
										</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:select property="cmboxStatus" size="1"
												styleClass="comboo" disabled="true">
												<html:optionsCollection name="dcPartnerstatus"
													property="partnerstatus" label="label" value="value" />
											</html:select>
										</logic:notEqual></td>
								</tr>
								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.leadminuteman" /></td>
									<td class="labele"><html:radio name="Partner_EditForm"
											property="leadMinuteman" value="true"></html:radio>Yes <html:radio
											name="Partner_EditForm" property="leadMinuteman"
											value="false"></html:radio>No</td>
								</tr>
								<!--  add new feature  -->
								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;Speedpay Payment Term Accepted</td>
									<% if (topPartnerName.equals("Minuteman Partner")){%>		
								<td class="labelo">N/A</td>
								<%}
								else { %>
								
								<logic:equal name="Partner_EditForm" property="accountingCPDRole" value="Y">
									<td class="labelo">
									
									<html:radio name="Partner_EditForm"
											property="speedPaymentTerms" value="true" ></html:radio>Yes <html:radio
											name="Partner_EditForm" property="speedPaymentTerms"
											value="false" ></html:radio>No</td>
								</logic:equal>
								<logic:notEqual name="Partner_EditForm" property="accountingCPDRole" value="Y">
								<td class="labelo"><html:radio name="Partner_EditForm"
											property="speedPaymentTerms" value="true"  disabled="true"></html:radio>Yes <html:radio
											name="Partner_EditForm" property="speedPaymentTerms"
											value="false" disabled="true" ></html:radio>No</td>
								</logic:notEqual>				
								<%}%>			
								</tr>
								
								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.externalsalesagent" /></td>
									<td class="labele"><html:radio name="Partner_EditForm"
											property="externalSalesAgent" value="true"></html:radio>Yes <html:radio
											name="Partner_EditForm" property="externalSalesAgent"
											value="false"></html:radio>No</td>
								</tr>
								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.minutemanPayrollId" /></td>
									<td class="labelo"><html:text styleClass="textbox"
											size="10" maxlength="10" property="minutemanPayrollId" /></td>
								</tr>
								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.dateadded" /></td>
									<td class="labele"><bean:write name="Partner_EditForm"
											property="partnerCreateDate" /> <html:hidden
											property="partnerCreateDate" /></td>
								</tr>
								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.partnersource" /></td>
									<td class="labelo"><bean:write name="Partner_EditForm"
											property="partnerSource" /> <html:hidden
											property="partnerSource" /></td>
									</td>
								</tr>
								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.partnerupdate" /></td>
									<td class="labele"><bean:write name="Partner_EditForm"
											property="partnerUpdateDate" /> <html:hidden
											property="partnerUpdateDate" /></td>
								</tr>

								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.partnerupdateby" /></td>
									<td class="labelo"><bean:write name="Partner_EditForm"
											property="partnerUpdateBy" /> <html:hidden
											property="partnerUpdateBy" /></td>
								</tr>

								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.partnerwebupdate" /></td>
									<td class="labele"><bean:write name="Partner_EditForm"
											property="partnerWebUpdateDate" /> <html:hidden
											property="partnerWebUpdateDate" /></td>
								</tr>

								<tr>
									<td class="labellobold" colspan="2" height="30"><bean:message
											bundle="PVS" key="pvs.partneredit.partnerregstatus" /></td>
								</tr>
								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.regstatus" /> <html:hidden
											property="registered" /></td>

									<logic:equal name="Partner_EditForm" property="registered"
										value="NOT REGISTERED">
										<td class="labelored"><b><bean:write
													name="Partner_EditForm" property="registered" /></b></td>
									</logic:equal>

									<logic:notEqual name="Partner_EditForm" property="registered"
										value="NOT REGISTERED">
										<td class="labelo"><bean:write name="Partner_EditForm"
												property="registered" /></td>
									</logic:notEqual>

								</tr>
								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.regdate" /></td>
									<td class="labele"><bean:write name="Partner_EditForm"
											property="regDate" /> <html:hidden property="regDate" /></td>
								</tr>

								<tr>
									<td class="labelobold" colspan="2" height="20">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.mcsa" /></td>
								</tr>
								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.mcsa.version" /></td>
									<td class="labele"><html:select property="mcsaVersion"
											size="1" styleClass="comboe">
											<html:optionsCollection name="mcsaVersionCombo"
												property="mcsaVersion" label="label" value="value" />
										</html:select></td>
								</tr>
								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.mcsa.signedby" /></td>
									<td class="labelo"><bean:write name="Partner_EditForm"
											property="signedBy" /> <html:hidden property="signedBy" /></td>
								</tr>

								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.regrenewaldate" /></td>
									<td class="labele"><html:text styleClass="textbox"
											size="10" property="registrationRenewalDate" readonly="true" />
										<img src="images/calendar.gif" width=19 height=17 border=0
										align="center"
										onclick="return popUpCalendar(document.forms[0].registrationRenewalDate, document.forms[0].registrationRenewalDate, 'mm/dd/yyyy')"
										onmouseover="window.status = 'Date Picker';return true;"
										onmouseout="window.status = '';return true;"></td>
								</tr>
								<tr>
									<td class="labellobold" colspan="2" height="30"><bean:message
											bundle="PVS" key="pvs.partneredit.incidentsummary" /></td>
								</tr>

								<tr height="20">
									<td class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.incident.flag" /></td>
									<td class="labele"><logic:equal name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:radio property="incidentReport" value="Y" />
											<bean:message bundle="PVS" key="pvs.partneredit.yes" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<html:radio
												property="incidentReport" value="N" />
											<bean:message bundle="PVS" key="pvs.partneredit.no" />
										</logic:equal> <logic:notEqual name="Partner_EditForm"
											property="resourceDeveloper" value="Y">
											<html:radio property="incidentReport" value="Y"
												disabled="true" />
											<bean:message bundle="PVS" key="pvs.partneredit.yes" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<html:radio
												property="incidentReport" value="N" disabled="true" />
											<bean:message bundle="PVS" key="pvs.partneredit.no" />
										</logic:notEqual></td>
								</tr>
								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.incident.report" /></td>
									<td class="labelo"><bean:write name="Partner_EditForm"
											property="incidentReportFiled" /> <html:hidden
											property="incidentReportFiled" /></td>
								</tr>
								<tr>
									<td class="labellobold" colspan="2" height="30"><bean:message
											bundle="PVS" key="pvs.partneredit.requireforms" /></td>
								</tr>
								
					<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.w-9"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="rdoW9" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="rdoW9" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>		
				    <html:hidden property="rdoW9" name="Partner_EditForm" />				
				    <html:checkbox name="Partner_EditForm" property="w9Uploaded" styleId="w9UploadedId" value="Y"  disabled="true"  /><bean:message bundle="PVS" key="pvs.partneredit.w9uploaded"   />
				    </td>
				</tr>				
				<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.insurance.label"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="isInsuranceSubmitted" value="Y"  >
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="isInsuranceSubmitted" value="N" >
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					<html:hidden property="isInsuranceSubmitted" name="Partner_EditForm" />
				    <html:checkbox name="Partner_EditForm" property="isInsuranceUploaded" styleId="isInsuranceUploadedId" value="Y" disabled="true"/><bean:message bundle="PVS" key="pvs.partneredit.w9uploaded" />
				    </td>
				</tr>
				
				<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.resume.label"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="isResumeSubmitted" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="isResumeSubmitted" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					<html:hidden property="isResumeSubmitted" name="Partner_EditForm" />
				    <html:checkbox name="Partner_EditForm" property="isResumeUploaded" styleId="isResumeUploadedId" value="Y" disabled="true"/><bean:message bundle="PVS" key="pvs.partneredit.resume.uploaded" />
				    </td>
				</tr>
			
				<!--  modifcation 13320 -->

			<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.Background.label"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="backgroundCheckSubmitted" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="backgroundCheckSubmitted" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					<html:hidden property="backgroundCheckSubmitted" name="Partner_EditForm" />
				    <html:checkbox name="Partner_EditForm" property="backgroundCheckUploaded" styleId="backgroundCheckUploadedId" value="Y" disabled="true"/><bean:message bundle="PVS" key="pvs.partneredit.Background.Uploaded" />
				   	&nbsp;&nbsp;<a href="javascript:uploadFile('<bean:write name="Partner_EditForm" property="pid"/>','${techName}','BackgroundCheck')">UPLOAD</a>
					&nbsp;&nbsp;<a href="ViewuploadedocumentAction.do?ref=downloadPartnerFile&partner_id=<bean:write name="Partner_EditForm" property="pid"/>&fileType=BackgroundCheck">REVIEW</a>
				    </td>
				</tr>
				
		<tr height="20">
					<td   class="labelebold">&nbsp;&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.partneredit.Drug.label"/></td>
					<td   class="labele" >
					<logic:equal name="Partner_EditForm" property="drugScreenCompletionSubmitted" value="Y">
						    	<bean:message bundle="PVS" key="pvs.partneredit.yes"/>&nbsp;
				    </logic:equal>
				    <logic:equal name="Partner_EditForm" property="drugScreenCompletionSubmitted" value="N">
				    	<bean:message bundle="PVS" key="pvs.partneredit.no"/>&nbsp;&nbsp;
				    </logic:equal>						
					<html:hidden property="drugScreenCompletionSubmitted" name="Partner_EditForm" />
				    <html:checkbox name="Partner_EditForm" property="drugScreenCompletionUploaded" styleId="drugScreenCompletionUploadedId" value="Y" disabled="true"/><bean:message bundle="PVS" key="pvs.partneredit.Drug.Uploaded" />
								
				    &nbsp;&nbsp;<a href="javascript:uploadFile('<bean:write name="Partner_EditForm" property="pid"/>','${techName}','DrugScreen')">UPLOAD</a>
 				    &nbsp;&nbsp;<a href="ViewuploadedocumentAction.do?ref=downloadPartnerFile&partner_id=<bean:write name="Partner_EditForm" property="pid"/>&fileType=DrugScreen">REVIEW</a>				    	 
								
											
</td>
								</tr> 
						
								<!--  End of modifcation  -->
								<tr>
									<td class="labellobold" colspan="2" height="30"><bean:message
											bundle="PVS" key="pvs.partneredit.otherpartnerinfo" /></td>
								</tr>
								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.monthlynewsletter" /></td>
									<td class="labelo"><html:radio
											property="monthlyNewsletter" value="Y" /> <bean:message
											bundle="PVS" key="pvs.partneredit.yes" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<html:radio
											property="monthlyNewsletter" value="N" /> <bean:message
											bundle="PVS" key="pvs.partneredit.no" />
								</tr>
								<!--  PHOTO ID  -->
								<tr height="20">
									<td class="labelobold">&nbsp;&nbsp;&nbsp;<bean:message
											bundle="PVS" key="pvs.partneredit.Photoid" /></td>
									<td class="labelo">
									<html:radio
											property="photoIdentificationID" value="N" /> <bean:message
											bundle="PVS" key="pvs.partneredit.None" />
									&nbsp;
									<html:radio
											property="photoIdentificationID" value="Y" /> <bean:message
											bundle="PVS" key="pvs.partneredit.Printed" />
								</tr>
								
						</table>
						</td>
					</tr>

 


 

					<tr>
						<td colspan="10" class="buttonrow"><html:submit
								property="save" styleClass="button"
								onclick="return validateAll();">
								<bean:message bundle="PVS" key="pvs.partneredit.save" />
							</html:submit> <html:reset property="reset" styleClass="button">
								<bean:message bundle="PVS" key="pvs.partneredit.reset" />
							</html:reset></td>
					</tr>
					<jsp:include page='/Footer.jsp'>
						<jsp:param name='colspan' value='28' />
						<jsp:param name='helpid' value='pvsPartner_Edit' />
					</jsp:include>
					</tr>

				</table>
				</td>
				</tr>
				</table>
			</div>
			<div id="i"
				style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
				<table width="100%" border="0" cellspacing="1" cellpadding="1"
					align="center">
					<tr>
						<td width="1" height="0"></td>
						<td><table border="0" cellspacing="1" cellpadding="1"
								width="600">
								<tr>
									<td colspan="9" class="labellobold" height="30"><bean:message
											bundle="PVS" key="pvs.changepassword" /></td>
								</tr>

								<tr>
									<td class="labelobold" colspan="4"><bean:message
											bundle="PVS" key="pvs.newpassword" /></td>
									<td class="labelo" colspan="5"><html:password
											styleClass="textbox" size="20" property="newPassword"
											value="" /></td>
								</tr>
								<tr>
									<td class="labelebold" colspan="4"><bean:message
											bundle="PVS" key="pvs.confirmpassword" /></td>
									<td class="labele" colspan="5"><html:password
											styleClass="textbox" size="20" property="confirmPassword"
											value="" /></td>
								</tr>
								<tr>
									<td colspan="9" class="buttonrow"><html:submit
											property="save" styleClass="button"
											onclick="return validateAll();">
											<bean:message bundle="PVS" key="pvs.partneredit.save" />
										</html:submit> <html:reset property="reset" styleClass="button">
											<bean:message bundle="PVS" key="pvs.partneredit.reset" />
										</html:reset></td>
								</tr>
								<jsp:include page='/Footer.jsp'>
									<jsp:param name='colspan' value='28' />
									<jsp:param name='helpid' value='pvsPartner_Edit' />
								</jsp:include>
							</table></td>
					</tr>
				</table>
			</div>
		
			<div id="j"
				style="visibility: hidden ;  POSITION: absolute;top:<%=topDiv%>;left:2; ">
				
				
				
				<!--  if user having the CPD or Accounting right then user will be able to view this option. -->
											<logic:equal
											name="Partner_EditForm" property="accountingCPDRole"
											value="Y">
											
											
				<table width="100%" border="0" cellspacing="1" cellpadding="1"
					align="center">
					<tr>
						<td width="1" height="0"></td>
						<td><table border="0" cellspacing="1" cellpadding="1"
								width="600">
								<tr>
									<td colspan="19" class="labellobold" height="30"><bean:message
											bundle="PVS" key="pvs.uploadedfiles" /></td>
								</tr>
								<tr>
									<td class="tryB" height="20" colspan="2"><bean:message
											bundle="PVS" key="pvs.dateposted" /></td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.displayname" /></td>
									<td class="tryB" height="20" colspan="2"><bean:message
											bundle="PVS" key="pvs.originalname" /></td>
									<td class="tryB" height="20" colspan="2"><bean:message
											bundle="PVS" key="pvs.download" /></td>
									<td class="tryB" height="20" colspan="2">Status</td>
									<td class="tryB" height="20" colspan="2">Update Status</td>		
											
								</tr>
								<%
									boolean csschooser = true;
																		String backgroundclass = "", remarkclass = "";
								%>
								<logic:present name="Partner_EditForm"
									property="uploadedfileslist">
									<logic:iterate id="list" name="Partner_EditForm"
										property="uploadedfileslist">
										<%
											if (csschooser == true) {
																							backgroundclass = "texto";
																							csschooser = false;
																							remarkclass = "labelotop";
																						} else {
																							csschooser = true;
																							backgroundclass = "texte";
																							remarkclass = "labeletop";
																						}
										%>

										<tr>
											<td class="<%=backgroundclass%>" colspan="2"><html:hidden name="list" property="file_id" /><a href='Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&isDeleteFile=DeleteFile&pid=<bean:write name="Partner_EditForm" property="pid"/>&fileRemarks=<bean:write name="list" property="file_remarks" />&file_id=<bean:write name="list" property="file_id" />' onclick="return confirm('Are you sure, you want to delete this file?');"><img src="images/delete.gif" alt="Delete" height="10" width="10" title="Delete" /></a>&nbsp;&nbsp;<bean:write name="list"	property="file_uploaded_date" /></td>
											<td class="<%=remarkclass%>" style="white-space: nowrap;" colspan="3"><bean:write name="list" property="file_remarks" />&nbsp;</td>
											<td class="<%=backgroundclass%>" style="text-align: left;" colspan="2"><bean:write name="list"	property="file_name" /></td>
											<td class="<%=backgroundclass%>" colspan="2"><a href="ViewuploadedocumentAction.do?ref=downloaddocument&file_id=<bean:write name = "list" property = "file_id" />"><bean:write	name="list" property="file_name" /></a></td>
											<td class="<%=backgroundclass%>" style="text-align: left;" colspan="2"><bean:write name="list"	property="file_status" /></td>
												<td class="<%=backgroundclass%>" style="text-align: left;" colspan="2" >
									         		<span>
										         		[&nbsp;
										         		<a href ='Partner_Edit.do?formPVSSearch=fromPVSSearch&func=Received&pid=<bean:write name="Partner_EditForm" property="pid"/>&fileRemarks=<bean:write name="list" property="file_remarks" />&file_id=<bean:write name="list" property="file_id" />' onclick="javascript:Received();" >Received</a>
										         		&nbsp;|&nbsp;
										         	     <a href = 'Partner_Edit.do?formPVSSearch=fromPVSSearch&func=Approved&pid=<bean:write name="Partner_EditForm" property="pid"/>&fileRemarks=<bean:write name="list" property="file_remarks" />&file_id=<bean:write name="list" property="file_id" />'  onclick="javascript:Approved();">Approved/Current</a>
														&nbsp;|&nbsp;
														<a href = 'Partner_Edit.do?formPVSSearch=fromPVSSearch&func=Denied&pid=<bean:write name="Partner_EditForm" property="pid"/>&fileRemarks=<bean:write name="list" property="file_remarks" />&file_id=<bean:write name="list" property="file_id" />'  onclick="javascript:Denied();">Denied</a>
														&nbsp;|&nbsp;
														<a href = 'Partner_Edit.do?formPVSSearch=fromPVSSearch&func=Archived&pid=<bean:write name="Partner_EditForm" property="pid"/>&fileRemarks=<bean:write name="list" property="file_remarks" />&file_id=<bean:write name="list" property="file_id" />'  onclick="javascript:Archived();">Archived</a>
														&nbsp;]
													
												  </span>
								         		</td>
										</tr>
									</logic:iterate>
								</logic:present>
								<tr>
									<td class="tryB" height="20" colspan="2">&nbsp;</td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.displayname" /></td>
									<td class="tryB" height="20" colspan="3"><bean:message
											bundle="PVS" key="pvs.selectfile" /></td>
									<td class="tryB" height="20" colspan="1">&nbsp;</td>
								</tr>
								<tr>
									<td class="labelobold" colspan="2"><bean:message
											bundle="PVS" key="pvs.uploadfile" /></td>
									<td class="labelo" colspan="3"><html:text
											styleId="displayID" styleClass="textbox" size="20"
											property="displayName" /></td>
									<!--<td  class="labelo"  colspan="3"><html:text  styleClass="textbox" size="20" property="uploadSelectFile" value=""/></td>-->
									<td class="labelo" colspan="4" style="white-space: nowrap;"><html:file
											property="browse" styleClass="button"  styleId="browse" /> &nbsp;&nbsp;

											<html:radio
											styleId="w9Upload" name="Partner_EditForm"
											property="uploadedDocType" value="w9Uploaded" />&nbsp;W-9
										Upload&nbsp; 
										
										<html:radio styleId="insuranceUpload"
											name="Partner_EditForm" property="uploadedDocType"
											value="insuranceDocUploaded" />&nbsp;Proof of Insurance
										Upload&nbsp;&nbsp;&nbsp;<html:radio styleId="resumeUpload"
											name="Partner_EditForm" property="uploadedDocType"
											value="resumeDocUploaded" />&nbsp;Resume Upload&nbsp;
											<html:radio
											styleId="backUpload" name="Partner_EditForm"
											property="uploadedDocType" value="backgroundCheckuploaded" />&nbsp;Background Check&nbsp;&nbsp;
											<html:radio
											styleId="drugUpload" name="Partner_EditForm"
											property="uploadedDocType" value="drugScreenUploaded" />&nbsp;Drug Screen&nbsp;&nbsp;
											
											<html:radio
											styleId="otherUpload" name="Partner_EditForm"
											property="uploadedDocType" value="otherDoc" />&nbsp;Other&nbsp;&nbsp;
										<!--  modifcation 13320   change here -->
											
											
										<!--  End of modifcation -->	
									</td>
								</tr>
								<tr>
									<td colspan="9" class="buttonrow"><html:submit
											property="save" styleClass="button" styleId="button"
											onclick="return validateAll();">
											<bean:message bundle="PVS" key="pvs.partneredit.save" />
										</html:submit> <html:reset property="reset" styleClass="button">
											<bean:message bundle="PVS" key="pvs.partneredit.reset" />
										</html:reset></td>
								</tr>
								<jsp:include page='/Footer.jsp'>
									<jsp:param name='colspan' value='28' />
									<jsp:param name='helpid' value='pvsPartner_Edit' />
								</jsp:include>
							</table></td></tr></table></logic:equal>

							</div> <logic:present name="refreshtree" scope="request">
								<script>
		parent.ilexleft.location.reload();
	</script>
							</logic:present> </html:form>
</BODY>
</html:html>
<script>
function getshowtab() {
	var args = getshowtab.arguments;
		if(args[0]!= null) {
				if(args[0]=='a') {
					document.getElementById('pdrow2').style.width = 710;
					document.getElementById('pdrow3').style.width = 50;
					document.getElementById('pdrow4').style.width = 70;
					document.getElementById('pdrow5').style.width = 35;
				}else {
					document.getElementById('pdrow2').style.width = 350;
					document.getElementById('pdrow3').style.width = 50;
					document.getElementById('pdrow4').style.width = 70;
					document.getElementById('pdrow5').style.width = 395;
				}
		}
}

function viewChange() { //v3.0
	 MM_showHideLayers('a','','hide');
 	 MM_showHideLayers('b','','hide');
 	 MM_showHideLayers('d','','hide');
	 MM_showHideLayers('e','','hide');
	 MM_showHideLayers('f','','hide');	 
	 MM_showHideLayers('g','','hide');
	 MM_showHideLayers('h','','hide');	 
	 MM_showHideLayers('i','','hide');	 	 
	 MM_showHideLayers('j','','hide');	  	 
	 var args=viewChange.arguments;
	 MM_showHideLayers(args[0],'','show');
	 getshowtab(args[0]);
	 return true;
}
function MM_showHideLayers() { //v3.0

  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v;}
    obj.visibility=v;}
}
function moreAttach(){
	var arg= moreAttach.arguments;
	var i = dynatable.rows.length-2 ;

	if (arg[0]==1){
		if (i>=1){
		 return;
		 }		
	}
	if (arg[0]!=1)
	if(!validateDivC())	return false;
		
	i = dynatable.rows.length-2 ;
    var cls='';
	if(dynatable.rows.length%2==0)	cls= 'labelo';
	if(dynatable.rows.length%2==1)	cls= 'labele';

	var oRow=dynatable.insertRow();
	var oCell=oRow.insertCell(0);
	oCell.innerHTML="<input type='text' id='phyFieldAddress1"+i+"' name='phyFieldAddress1' size=20 maxlength=100 class='textbox' >";
	oCell.className=cls;

	oCell=oRow.insertCell(1);
	oCell.innerHTML="<input type='text' id='phyFieldAddress2"+i+"' name='phyFieldAddress2' size=20 maxlength=100 class='textbox'>";
	oCell.className=cls;

	oCell=oRow.insertCell(2);
	oCell.innerHTML="<input type='text' id='phyFieldState"+i+"' name='phyFieldState' size=15 maxlength=100 class='textbox'>";
	oCell.className=cls;

	oCell=oRow.insertCell(3);
	oCell.innerHTML="<input type='text' id='phyFieldZip"+i+"' name='phyFieldZip' size=10 maxlength=100 class='textbox' >";
	oCell.className=cls;

	oCell=oRow.insertCell(4);
	oCell.innerHTML="<input type='text' id='phyFieldPhone"+i+"' name='phyFieldPhone' size=10 maxlength=100 class='textbox' >";
	oCell.className=cls;

	oCell=oRow.insertCell(5);
	oCell.innerHTML="<input type='text' id='phyFieldFax"+i+"' name='phyFieldFax' size=10 maxlength=100 class='textbox'>";
	oCell.className=cls;
}	// END OF FUNCTION moreAttach()











	    	
		
		
				
		
				
</script>

<script>

function moreAttachCompany(){
	var arg=moreAttachCompany.arguments;
	var i = arg[0].rows.length ;
	if(! validateCompany(arg[0],arg[1],arg[2],arg[3],arg[4],'',5))return false;	
	var oRow=arg[0].insertRow();
	if(arg[0].rows.length%2==0){
    	c1class="labelebold";
    	c2class="labele";
    	combo="comboe";
	}
	else{
    	c1class="labelobold";
    	c2class="labelo";
    	combo="comboo";    	
	}

	var oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className=c1class;
	if(arg[2]=='w')
	oCell.innerHTML="<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+combo+"' onchange=\"return validateCompany(document.getElementById('dynatableWL'),'wlcompany','w' ,'validFromWC','validToWC' );\"><option value='0'>---Select---</option><%String sql = "select * from cp_company where cp_comp_id in(Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in(select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='W'  ))order by cp_comp_name";
										int countwl = 0;
										ResultSet rs1 = stmt.executeQuery(sql);
										while (rs1.next()) {
											countwl++;%> <option value='<%=rs1
													.getString("cp_comp_id")%>'><%=rs1
													.getString("cp_comp_name")%></option>	<%}%> </select>";	
	if(arg[2]=='sd')
	oCell.innerHTML="<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+combo+"' onchange=\"return validateCompany(document.getElementById('dynatableSD'),'sdcompany','sd','validFromSD','validToSD' );\"><option value='0'>---Select---</option><%sql = "select * from cp_company where cp_comp_id in(Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in(select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='SD' ))order by cp_comp_name";
										countwl = 0;
										rs1 = stmt.executeQuery(sql);
										while (rs1.next()) {
											countwl++;%> <option value='<%=rs1
													.getString("cp_comp_id")%>'><%=rs1
													.getString("cp_comp_name")%></option>	<%}%> </select>";
	if(arg[2]=='it')
	oCell.innerHTML="<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+combo+"' onchange=\"return validateCompany(document.getElementById('dynatableIT'),'itcompany','it','validFromIT','validToIT' );\"><option value='0'>---Select---</option><%sql = "select * from cp_company where cp_comp_id in(Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in(select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='IT' ))order by cp_comp_name";
										countwl = 0;
										rs1 = stmt.executeQuery(sql);
										while (rs1.next()) {
											countwl++;%> <option value='<%=rs1
													.getString("cp_comp_id")%>'><%=rs1
													.getString("cp_comp_name")%></option>	<%}%> </select>";
	if(arg[2]=='ta')
	oCell.innerHTML="<select id='"+arg[1]+i+"' name='"+arg[1]+"' size='1' Class='"+combo+"' onchange=\"return validateCompany(document.getElementById('dynatableta'),'tacompany','ta','validFromTA','validToTA' );\"><option value='0'>---Select---</option><%sql = "select * from cp_company where cp_comp_id in(Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in(select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='ITA'))order by cp_comp_name";
										countwl = 0;
										rs1 = stmt.executeQuery(sql);
										while (rs1.next()) {
											countwl++;%> <option value='<%=rs1
													.getString("cp_comp_id")%>'><%=rs1
													.getString("cp_comp_name")%></option>	<%}%> </select>";

	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className=c2class;
	oCell.innerHTML="<input type='text' id='"+arg[3]+i+"' name='"+arg[3]+"' size='10' value='' readonly='true' class='textbox' /><img src ='images/calendar.gif' width = 19 height = 17 border = 0 align ='center' onclick =\"return popUpCalendar(document.forms[0]."+arg[3]+i+",document.forms[0]."+arg[3]+i+", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";

	oCell=oRow.insertCell(2);
	oCell.colSpan=3;	
	oCell.className=c2class;	
	oCell.innerHTML="<input type='text' id='"+arg[4]+i+"' name='"+arg[4]+"' size='10' value='' readonly='true' class='textbox' /><img src ='images/calendar.gif' width = 19 height = 17 border = 0 align ='center' onclick =\"return popUpCalendar(document.forms[0]."+arg[4]+i+",document.forms[0]."+arg[4]+i+", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";	
}// END OF FUNCTION moreAttachCompany()

function validateCompany(){    
	viewChange('g');
	var arg= validateCompany.arguments;
	
	var arg5='';
	if(arg[2]=='w')
		arg5="<bean:message bundle="PVS" key="pvs.companylevelwireless"/>";
	if(arg[2]=='sd')
		arg5="<bean:message bundle="PVS" key="pvs.companylevelstructured"/>";
	if(arg[2]=='it')
		arg5="<bean:message bundle="PVS" key="pvs.companylevelintegrated"/>";	
	if(arg[2]=='ta')
		arg5="<bean:message bundle="PVS" key="pvs.companylevelindustry"/>";

	if (arg[6]==5){
 		for (var k=0;k<arg[0].rows.length;k++){
 			if(! chkCombo(document.getElementById(arg[1]+k),arg5))return false; 		
 			if(! chkBlank(document.getElementById(arg[3]+k),'date from in ' + arg5))return false; 		

 			if(! chkBlank(document.getElementById(arg[4]+k),'date to in ' + arg5))return false; 		
			
 			var vf=document.getElementById(arg[3]+k).value.toString();
 			var vt=document.getElementById(arg[4]+k).value.toString();
			var diffrence=new Date(parseInt(vt.substring(6)),parseInt(vt.substring(0,2))-1,parseInt(vt.substring(3,5)))-new Date(parseInt(vf.substring(6)),parseInt(vf.substring(0,2))-1,parseInt(vf.substring(3,5)));
			
			if (diffrence<0){
				 alert("Date of through is less than valid from in " +arg5);
		 		return false;
 			}// END OF IF 					 		
 			
 		}// END OF FOR
	 }// END OF IF

	 for (var i=0;i<arg[0].rows.length;i++){
	   for (var j=i+1;j<arg[0].rows.length;j++){
			var ob1=(document.getElementById(arg[1]+i));
			var ob2=(document.getElementById(arg[1]+j));
			if(  ob1.options[ob1.selectedIndex].text == ob2.options[ob2.selectedIndex].text  ){
			    alert("You have select duplicate items in "+arg5);
			    ob2.focus();
			    return false;
			}// END OF IF
		}// END OF FOR j
	  }// END OF FOR i
  
	return true;
}
</script>
<script>
function moreTech(){

	var arg = moreTech.arguments;
	var i = arg[0].rows.length;

	if (arg[1]==1) {
		if (i>=1) return;
	}	
	

	if (arg[1]!=1 &&  arg[1]!=0)
	{	
		var j=(arg[0].rows.length/9);
		j=Math.floor(j);
		for(var k=0;k<j;k++)
		{			
	 		if((document.getElementById('engName'+k).value=="") && j==k+1)
	 		{  
	 			alert('Technician name is required');
	 			document.getElementById('engName' +k).focus();
	 	  		return false;
	  		}
	 			
	 	}
    	if(!validateDivD()) return false;
   	}
	
	else if(arg[1]==0){
		var j=(arg[0].rows.length/9);
		j=Math.floor(j);
	
		for(var k=0;k<j;k++)
		
		{	
			
			if(document.getElementById('engName'+k) ){
				
	 			if(document.getElementById('engName'+k).value=="")
	 		{  
	 			
	 			alert('Technician name is required');
	 			document.getElementById('engName' +k).focus();
	 	  		return false;
	  		}	
		}
	}
		
   	
}	
	
	var rid=(arg[0].rows.length/9);
	rid = rid;
	rid=Math.floor(rid);	
	var oTable2=arg[0];
	oTable2.width="600";
	oTable2.cellspacing="1";
	oTable2.id="dynaTech";	
		
	var oRow=oTable2.insertRow();
	
	var oCell=oRow.insertCell(0);
	oCell.colSpan=6;
	oCell.height=30;
	oCell.className="labellobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.technician/engineers"/>";
	
	//Seema-19/12/2006
	oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.className="labele";
	oCell.innerHTML="<input type='hidden' name='tech_id' value='0' id='tech_id"+ rid+"'>"; 
    	
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.tech/engineername"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labelo";
	oCell.innerHTML="<input type='text' name='engName' size='20' value='' class='engName textbox' id='engName"+ rid+"'>";
    
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelebold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.resourcelevel"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labele";
	oCell.innerHTML="<select id='cmboxResourceLevel"+ rid+"' name='cmboxResourceLevel' size='1' class='comboe'><option value='0|'>---Select---</option><%sql = "select * from dbo.func_cp_resource_level()";
										countwl = 0;
										rs1 = stmt.executeQuery(sql);
										while (rs1.next()) {
											countwl++;%> <option value='<%=rs1
													.getString("cp_res_id")
													+ "|"
													+ rs1.getString("cp_res_name")%>'><%=rs1
													.getString("cp_res_name")%></option>	<%}%> </select>";
	
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.location"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labelo";
	oCell.innerHTML="<input type='text' name='engZip' size='20' value='' class='textbox' id='engZip"+ rid+"'>";
    
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelebold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.equippedwith"/>";
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labele";
	oCell.innerHTML="<%sql = "SELECT * from cc_equipment_detail ";
										countwl = 0;
										rs1 = stmt.executeQuery(sql);
										while (rs1.next()) {
											countwl++;%> <input type='checkbox' name='chkboxEquipped' value='<%=rs1
													.getString("cc_ed_name")%>'  id='chkboxEquipped"+ rid+"'><%=rs1
													.getString("cc_ed_name")%> <br><%}%>	<input type='hidden' name='chkboxEquipped' value=','>";
	
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.union/nonunion"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labelo";
	oCell.innerHTML="<select name='rdoUnion' size='1' class='comboo' id='rdoUnion1"+ rid+"'><option value='1'>Non-Union</option><option value='2'>Union</option></select></td>";
	
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelebold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.highestcriticalityavailable"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labele";
	oCell.innerHTML="<select id='cmboxHighCriticality"+ rid+"' name='cmboxHighCriticality' size='1' class='comboe'><option value='0'>---Select---</option><%sql = "select *  from cc_criticality_detail order by cc_cd_id ";
										countwl = 0;
										rs1 = stmt.executeQuery(sql);
										while (rs1.next()) {
											countwl++;%> <option value='<%=rs1.getString("cc_cd_id")%>'><%=rs1
													.getString("cc_cd_name")%></option>	<%}%> </select>";
    
    
    
    oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelobold";
	oCell.innerHTML="<bean:message bundle="PVS" key="pvs.certificationsandskill"/>";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labelo";
	oCell.innerHTML="<select id='cmboxCertifications"+ rid+"' name='cmboxCertifications' size='5' class='comboe' multiple='true'><%sql = "select cp_cert_id,cp_cert_comp_tech  from cp_certification order by cp_cert_comp_tech";
										countwl = 0;
										rs1 = stmt.executeQuery(sql);
										while (rs1.next()) {
											countwl++;%> <option value='<%=rs1
													.getString("cp_cert_comp_tech")%>'><%=rs1
													.getString("cp_cert_comp_tech")%></option>	<%}%> </select>";
	
	oCell=oRow.insertCell(2);
	oCell.innerHTML="<input type='hidden' id='cmboxCertifications"+ rid+"' name='cmboxCertifications' value='~'>"; 
	var iteratorval = document.getElementsByName("iteratorval");
	var length = iteratorval.length;
	oRow=oTable2.insertRow();
	oCell=oRow.insertCell(0);
	oCell.colSpan=3;
	oCell.className="labelobold";
	oCell.innerHTML="Security Certifications";	
	oCell=oRow.insertCell(1);
	oCell.colSpan=3;
	oCell.className="labelo";
	oCell.innerHTML="<table><tbody>"+
		" <tr>"+
			"<td class='labele'>"+
				"Drug Screening Certification:"+
			"</td>"+
			"<td>"+						
			   "<input type='checkbox' name='checkBox1' value='"+length+"' onChange='removeCheck(this)'/>"+		
			"</td>"+
			"<td class='labele'>"+
				"<input type='text' name='checkBox1Date' size='10'  class='textbox' value='${date1}'  />"+
			"</td>"+
			"<td>"+
			  "&nbsp;"+
			"</td>"+
		"</tr>"+
		" <tr>"+
			"<td class='labele'>"+
				"Criminal Background Certification:"+
			"</td>"+
			"<td>"+						
		   		"<input type='checkbox' name='checkBox2' value='"+length+"' onChange='removeCheck(this)'/>"+		
			"</td>"+
			"<td class='labele'>"+
				"<input type='text' name='checkBox2Date' size='10'  class='textbox' value='${date2}'  />"+
			"</td>"+
			"<td>"+
		  		"&nbsp;"+
			"</td>"+
		"</tr>"+
		" <tr>"+
			"<td class='labele'>"+
				"Harassment Free Work <br> Envoirnment Certification:"+
			"</td>"+
			"<td>"+						
	   			"<input type='checkbox' name='checkBox3' value='"+length+"' onChange='removeCheck(this)'/>"+		
			"</td>"+
			"<td class='labele'>"+
				"<input type='text' name='checkBox3Date' size='10'  class='textbox' value='${date3}'  />"+
			"</td>"+
			"<td>"+
	  			"&nbsp;"+
			"</td>"+
		"</tr>"+
		"</tbody></table>";
	
	
	
}
</script>

<script>
function DeleteTechnicianJava(techId,str){
document.forms[0].action = "Partner_Edit.do?tech_id="+techId+"&strdel="+str+"&delete=true";
document.forms[0].submit();
return true;
}

function DeleteTechnician() {
var techId = DeleteTechnician.arguments[0];
var techSize = DeleteTechnician.arguments[1];

var str="";

var inid="";
var checkboxes = document.getElementsByName('tech_id').length;
var vals = "";
var count=0;

for (var i=0;i<techSize;i++) {
	
	
	if (document.getElementById('tech_idchk'+i).checked) 
	{

	count++;
	inid = document.getElementById('tech_idchk'+i).value;
		
     str += inid+ "~~";
	
	}
	
}
if(count==0){
	 
	 alert("Please Select Check Box to Delete");
     return false; 
}

	if(techId == 0) 
	 	
		DeleteTechRowJS(DeleteTechnician.arguments[2], DeleteTechnician.arguments[3]);
	else {
			
			if(DeleteTechnician.arguments[2].rows.length/9 == techSize)
			{	
				DeleteTechnicianJava(techId,str);
				if(DeleteTechnician.arguments[2].rows.length/9 >1)
				DeleteTechRowJS(DeleteTechnician.arguments[2], DeleteTechnician.arguments[3]);	
			}
			else
				DeleteTechnicianJava(techId,str);
				DeleteTechRowJS(DeleteTechnician.arguments[2], DeleteTechnician.arguments[3]);	
	}
}


function DeleteTechRowJS(){

var object = DeleteTechRowJS.arguments[0];
var num = DeleteTechRowJS.arguments[1];
if( object.rows.length>num)
  {
   for(i=0;i<9;i++)
	object.deleteRow();

	 if(object.rows.length == 0)
	 	moreTech(document.getElementById('dynaTech'),1);
  }
 
 
}
</script>

<script>
function DeleteRow(){
var object= DeleteRow.arguments[0];
var num= DeleteRow.arguments[1];
if( object.rows.length>num)
object.deleteRow();
}
</script>

<script>

function validateAll(){
document.forms[0].refersh.value = "";
	trimFields();

	var abc = document.getElementById("selectedRecommendedPartnerList");
	if(abc.options.length == 0){
		document.getElementById("partnerListCount").value = "0";
	 }
	 for (i=0;i<abc.options.length;i++) {

			abc.options[i].selected = true;
	        
	 }
	
	  
	 
	 
	 
	 abc = document.getElementById("selectedRestrictedPartnerList");
	 if(abc.options.length == 0){
		 document.getElementById("partnerListCount").value = document.getElementById("partnerListCount").value + "1";
	 }
	 for (i=0;i<abc.options.length;i++) {
			abc.options[i].selected = true;
	 }
	
	 
	 if (document.getElementById("displayID").value != ""  && document.getElementById("browse").value == "") {
   	  
   	  alert("Please Attach file!");
         document.getElementById("browse").focus(); 
         return false;  
   	  
     }	
	  
	 
	
		
	if(! validateDivA()) return false;
	if(! validateDivB()) return false;
	if(! validateDivD()) return false;	
	if(! validateDivE()) return false;			
	if(! validateDivG()) return false;		
	if(! validateDivH()) return false;
	
	
		document.getElementById("w9UploadedId").disabled = false;
		document.getElementById("isInsuranceUploadedId").disabled = false;
		document.getElementById("isResumeUploadedId").disabled = false;
		document.getElementById("backgroundCheckUploadedId").disabled = false;
		document.getElementById("drugScreenCompletionUploadedId").disabled = false;
		
		
	return true;
}

</script>
<script>
function enableFields() {
	document.forms[0].partnerCandidate.disabled = false;
	document.forms[0].partnerLastName.disabled = false;
	document.forms[0].partnerFirstName.disabled = false;
	document.forms[0].taxId.disabled = false;
	document.forms[0].companyType.disabled = false;
	document.forms[0].dateInc.disabled = false;
	document.forms[0].acceptAddress.disabled = false;
	document.forms[0].address1.disabled = false;
	document.forms[0].address2.disabled = false;
	document.forms[0].city.disabled = false;
	document.forms[0].stateSelect.disabled = false;
	document.forms[0].country.disabled = false;
	document.forms[0].zip.disabled = false;
	document.forms[0].webUserName.disabled = false;
	document.forms[0].webPassword.disabled = false;
	document.forms[0].cmboxStatus.disabled = false;
	document.forms[0].incidentReport[0].disabled = false;
	document.forms[0].incidentReport[1].disabled = false;
}

</script>
<script>

//****************************** DIV 'b' ******************************************
function validateDivB(){
	viewChange('b');
	var i=0;
	
	while(document.getElementById('afterHoursPhone'+i)){
	
		if(! CheckQuotes(document.getElementById('afterHoursPhone'+i),"<bean:message bundle="PVS" key="pvs.afterhoursphone"/>"))return false;	

		if(! CheckQuotes(document.getElementById('afterHoursEmail'+i),"<bean:message bundle="PVS" key="pvs.afterhouremail"/>"))return false;
		
		if(! chkEmail(document.forms[0].afterHoursEmail[i],"afterHoursEmail")) {
		return false;	
		}
		i++;		
	}
	if(! validateDivC()) return false;
	return true;
}

</script>

<script>

//****************************** DIV 'c' ******************************************

function validateDivC(){
		
	var i = dynatable.rows.length-2 ;
		for (var j=0;j<i;j++) {
			if(isBlank(document.getElementById('phyFieldAddress1'+j).value) && isBlank(document.getElementById('phyFieldState'+j).value) && isBlank(document.getElementById('phyFieldZip'+j).value) && isBlank(document.getElementById('phyFieldPhone'+j).value) && isBlank(document.getElementById('phyFieldFax'+j).value) && i==j+1) {
     				dynatable.deleteRow();
	      			return true;
			}
		if(! CheckQuotes(document.getElementById('phyFieldAddress1'+j),"<bean:message bundle="PVS" key="pvs.address1"/>"))return false;
		if(! CheckQuotes(document.getElementById('phyFieldAddress2'+j),"<bean:message bundle="PVS" key="pvs.address2"/>"))return false;
		if(! CheckQuotes(document.getElementById('phyFieldState'+j),"<bean:message bundle="PVS" key="pvs.state"/>"))return false;
		if(! CheckQuotes(document.getElementById('phyFieldZip'+j),"<bean:message bundle="PVS" key="pvs.zipcode"/>"))return false;
			
		if(! CheckQuotes(document.getElementById('phyFieldPhone'+j),"<bean:message bundle="PVS" key="pvs.phone"/>"))return false;
		}
	return true;
}

//****************************** DIV 'd' ******************************************
function validateDivD()
{ 
	viewChange('d');		
	if (document.getElementById('dynaTech').rows.length==0) return true;
	var i = document.getElementById('dynaTech').rows.length/9 ;
	
	var h = 0;
	while(h<i)
	{			
				if( (document.getElementById('engName'+ h).value==""))
	 				{  
	 				   alert('Technician name is required');
	 				   document.getElementById('engName' + h).focus();
	 	  	   		   return false;
	  				}
	 			
	 			if(! CheckQuotes(document.getElementById('engName'+ h),"<bean:message bundle="PVS" key="pvs.tech/engineername"/>"))return false;	
				if(! CheckQuotes(document.getElementById('engZip'+ h),"<bean:message bundle="PVS" key="pvs.location"/>"))return false;	
				

		
			h=h+1;
	}
	return true;
}

</script>


<script>

//****************************** DIV 'e' ******************************************
function validateDivE(){	
	viewChange('e');			
	var i=0;
	while(document.getElementById('chkboxTools'+i)){
		if ((document.getElementById('chkboxTools'+i).checked==true))
			if(! chkBlank(document.getElementById('toolZips'+i),"<bean:message bundle="PVS" key="pvs.zip"/>"))return false;
			
		if ((document.getElementById('chkboxTools'+i).checked==false)){
			if(!isBlank(document.getElementById('toolZips'+i).value)){
				chkCheck(document.getElementById('chkboxTools'+i),document.getElementById('toolZips'+i),"<bean:message bundle="PVS" key="pvs.zip"/>");
				return false;
			}
		}
	i++;		
	}
	return true;

		
		}
			

			


//****************************** DIV 'g' ******************************************
function validateDivG(){	
	viewChange('e');			
	if(! validateCompany(document.getElementById('dynatableWL'),'wlcompany','w' ,'validFromWC','validToWC','',5))return false;	
	if(! validateCompany(document.getElementById('dynatableSD'),'sdcompany','sd','validFromSD','validToSD','',5))return false;	
	if(! validateCompany(document.getElementById('dynatableIT'),'itcompany','it','validFromIT','validToIT','',5))return false;	
	if(! validateCompany(document.getElementById('dynatableTA'),'tacompany','ta','validFromTA','validToTA','',5))return false;	
	return true;
}   // END OF FUNCTION 

//****************************** DIV 'h' ******************************************
function validateDivH(){	
	viewChange('h');
	if(document.forms[0].minutemanPayrollId.value!=""){
		if(! chkInteger(document.forms[0].minutemanPayrollId,"<bean:message  bundle="PVS" key="pvs.partneredit.minutemanPayrollId"/>")) return false;
		if(document.forms[0].minutemanPayrollId.value<=0){
			alert("<bean:message  bundle="PVS" key="pvs.partneredit.minutemanPayrollId"/>"+" Should be greater than 0");
			document.forms[0].minutemanPayrollId.focus();
			return false;
		}
	}
	

	
	
	return true;
}   // END OF FUNCTION 
//****************************** DIV 'i' ******************************************

function validateDivI(){	
	viewChange('i');	
	if(! chkBlank(document.forms[0].newPassword,"<bean:message bundle="PVS" key="pvs.newpassword"/>"))return false;
	if(! CheckQuotes(document.forms[0].newPassword,"<bean:message bundle="PVS" key="pvs.newpassword"/>"))return false;
	if(! CheckQuotes(document.forms[0].newPassword,"<bean:message bundle="PVS" key="pvs.newpassword"/>"))return false;
	if(!(document.forms[0].newPassword.value==document.forms[0].confirmPassword.value)){
		alert("<bean:message bundle="PVS" key="pvs.confirmpassword"/> and <bean:message bundle="PVS" key="pvs.newpassword"/> are different");
		document.forms[0].confirmPassword.focus();
		return false;
	}	
	return true;
}

function trimFields() {
	var field=document.forms[0];
	for(var i=0; i<field.length; i++) {
		if(field[i].type=='text') {
			var temp1=field[i].value;
			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--) {
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}
		} else { temp2=temp1; }
		field[i].value=temp2;
		}
	}
return true;
}

function trimBetweenString(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!=" ")	nstr=nstr+str.substring(i,i+1);
	}
	return nstr;
}

function removeHiffen(str){
	nstr="";
	for (var i=0;i<str.length;i++){
		if(str.substring(i,i+1)!="-")	nstr=nstr+str.substring(i,i+1);
	}
	return nstr;
}

function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		if(obj.disabled == false)
			obj.focus();
		return false;
	}
	return true;
}
function chkInteger(obj,label){
	if(!isInteger(removeHiffen(obj.value)))	{	
		alert("Only numeric values are allowed in "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkAlphabetic(obj,label){
	if(!isAlphabetic(trimBetweenString(obj.value)))	{	
		alert("Only alphabets are allowed in "+label);	
		obj.focus();
		return false;
	}
	return true;
}
function chkCombo(obj,label){
	
	if(obj.value==0 || obj.value == null )	{	
		alert("Please Select "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function chkEmail(obj,label){
	if(!isEmail(obj.value))	{	
		alert("Invalid "+label);	
		obj.focus();
		return false;
	}
	if(!isEmpty(obj.value)) {
		if(isWhitespace(obj.value)) {
			alert("Invalid "+label);
			obj.focus();
			return false;
		}
	}
	return true;
}

function setNotProvided(obj) {
		obj.value = 'Not Provided';
}

function setUserPassword() {
	if(document.forms[0].webUserName.value=="")
		document.forms[0].webPassword.value = "";
	
}	


function chkRadio(obj,label){
	if(obj.value==""){	
		alert("Please Select "+label);	
		return false;
	}
	return true;
}

function chkCheck(obj,obj2,label){
		alert("Please check the check box as text field contains some entry");	
		obj2.focus();
		return false;
}

function chkFloat(obj,label){
	if(!isFloat(obj.value))	{
		alert("Please enter decimal value in "+label);	
		obj.focus();
		return false;
		}
	return true;
}
function CheckQuotes(obj,label){
	if(invalidChar(obj.value,label)) {		
		obj.focus();
		return false;
	}
	return true;
}

function Backaction() {
	if(document.forms[0].fromtype.value="powo") {
          document.forms[0].action = "POWOAction.do?jobid="+document.forms[0].jobid.value;
		  document.forms[0].submit();
		  return true;
	 } 
}
//Start :Added By Amit,This will disable MCSA combo if Radio button Y is clicked
function disableMCSACombo() {
	document.forms[0].mcsaVersion.disabled = true;
	document.forms[0].mcsaVersion.value = '0';
}

function enableMCSACombo() {
	document.forms[0].mcsaVersion.disabled = false;
}



	







function deletetechnicianinfo(){
	var techid = deletetechnicianinfo.arguments[0];
	document.forms[0].action = "Partner_Edit.do?tech_id="+ techid+"&delete=true";
	document.forms[0].submit();
	return true;
}// END OF FUNCTION deleteSchedule()

function del() 
{
		var convel = confirm( "Are you sure you want to delete the Partner?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?fromId=<bean:write name = "Partner_EditForm" property = "pid"/>&func=delete";
			document.forms[0].submit();
			return true;	
		}
} 
function jobHistory() 
{		
	document.forms[0].action = "JobInformationperPartner.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&from=Partner_Edit";
	document.forms[0].submit();
	return true;	
} 

function changeRegionCategory()
{
	document.forms[0].refersh.value="true";
	document.forms[0].action = "Partner_Edit.do?pid=<bean:write name = "Partner_EditForm" property = "pid"/>&check=country"
	document.forms[0].submit();
	return true;
}
function convertToUppercase() {
	var str=document.forms[0].zip.value
	if(document.forms[0].country.value=='CA') {
		if(str.length == 7) {
		document.forms[0].zip.value=str.toUpperCase();
		}
	}
}

function Received() 
{
		var convel = confirm( "Are you sure you want to change the status?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?fromId=<bean:write name = "Partner_EditForm" property = "pid"/>&func=Received";
			document.forms[0].submit();
			return true;	
		}
} 

function Approved() 
{
		var convel = confirm( "Are you sure you want to change the status?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?fromId=<bean:write name = "Partner_EditForm" property = "pid"/>&func=Approved";
			document.forms[0].submit();
			return true;	
		}
} 
function Denied() 
{
		var convel = confirm( "Are you sure you want to change the status?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?fromId=<bean:write name = "Partner_EditForm" property = "pid"/>&func=Denied";
			document.forms[0].submit();
			return true;	
		}
} 

function Archived() 
{
		var convel = confirm( "Are you sure you want to change the status?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?fromId=<bean:write name = "Partner_EditForm" property = "pid"/>&func=Archived";
			document.forms[0].submit();
			return true;	
		}
} 
function removeCheck(obj){
	if(obj.checked){
		obj.setAttribute("checked","");
	}else{
		obj.removeAttribute("checked");
	}
}
function uploadFile(partnerId, techId, fileType){
	var url="uploadCertificate.do?partnerId="+partnerId+"&technicianId="+techId+"&uploadCertificate=yes&fileType="+fileType;
  	window.open(url,"_blank","directories=no, status=no,width=400, height=250,top=50,left=20");
}	
</script>
</script>
<%
	rs1.close();
								stmt.close();
								conn.close();
%>