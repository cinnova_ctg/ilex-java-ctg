<div id="menunav">
    <ul>
  		<li>
        	<a href="#" class="drop"><span>Job</span></a>
  			<ul>
				<%
				if((String)request.getAttribute( "prjJobStatusListMenuNew" ) != "" ) {
					%>
					<li>
						<a href="#"><span><bean:message bundle = "PRM" key = "prm.changestatus"/></span></a>
						<ul>
							<%= (String) request.getAttribute( "prjJobStatusListMenuNew" ) %>
						</ul>
					</li>
					<%
				} else {
					%>
					<li><a href="#"><bean:message bundle = "PRM" key = "prm.changestatus"/></a></li>
					<%
				}
				%>
		  		<li>
		        	<a href="#"><span>Documents</span></a>
		        	<ul>
		      			<li><a href="EntityHistory.do?entityId=<%=Job_Id%>&jobId=<%=Job_Id%>&entityType=Deliverables&function=addSupplement&linkLibName=Job">Upload</a></li>
		      			<li><a href="EntityHistory.do?entityType=Deliverables&jobId=<%=Job_Id%>&linkLibName=Job&function=supplementHistory">History</a></li>
		  			</ul>
		  		</li>
      			<li><a href="ChecklistTab.do?job_id=<%=Job_Id%>&page=jobdashboard&viewjobtype=ION&ownerId=1">Process Checklist</a></li>
      			<li><a href="InsuranceRequest.do?msaId=<%= MSA_Id%>&jobId=<%=Job_Id%>&appendixId=<%=Appendix_Id%>">Email Insurance Certificate Request</a></li>
      			<li><a href="JobNotesAction.do?jobId=<%=Job_Id%>&appendixid=<%=Appendix_Id%>">Job Notes</a></li>
      			<%
      			if(request.getAttribute("isTicket")!=null && ((String)request.getAttribute( "isTicket" )).trim().equalsIgnoreCase("Ticket") ) {
      				%>
      				<li><a href="DepotRequisition.do?appendixid=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>">Depot Requisition</a></li>
					<%
					if( request.getAttribute("jobStatus")!=null && !((String)request.getAttribute("jobStatus")).trim().equalsIgnoreCase("O") ) {
						%>
		      			<li><a href="TicketChangeCriticality.do?msaId=<%= MSA_Id%>&Job_Id=<%=Job_Id%>&appendixId=<%=Appendix_Id%>">Change HelpDesk/ Criticality</a></li>
		      			<li><a href="ChangeTicketRequestType.do?msaId=<%= MSA_Id%>&Job_Id=<%=Job_Id%>">Change Request Type</a></li>
						<%
					}
					%>
					<%
					if( request.getAttribute("onsite")!=null && ((String)request.getAttribute("onsite")).trim().equalsIgnoreCase("onsite") ) {
						%>
      					<li><a href="javascript:cancelTicket('<%= request.getAttribute("alreadyCancelled")%>');">Cancel Ticket</a></li>
						<%
					}
				}
				%>
		  		<li>
		        	<a href="#"><span>Test Indicator</span></a>
		        	<ul>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&jobIndicator=None&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>">None</a></li>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Demonstration&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>">Demonstration</a></li>
		      			<li><a href="JobSetUpAction.do?hmode=jobIndicator&jobIndicator=Internal Test&changeTestIndicator=yes&jobid=<%=Job_Id%>&tabId=<c:out value='${JobSetUpForm.tabId}'/>">Internal Test</a></li>
					</ul>
		  		</li>	
  			</ul>
  		</li>
  		<li>
        	<a href="#" class="drop"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:projectJobManageStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:projectJobSendStatusReport()">Send</a></li>
      			<li><a href="javascript:projectJobManageStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>