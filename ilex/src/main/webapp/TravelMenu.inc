<%
	String Travel_Id = "";

	if( request.getAttribute( "Travel_Id" ) != null )
		Travel_Id = ( String )request.getAttribute( "Travel_Id" );
	
%>

<script>

function travelDel() 
{
		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "ResourceListAction.do?type=Delete&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&Travel_Id=<%= Travel_Id %>&Activity_Id=<%= Activity_Id %>&viewT=T";
			document.forms[0].submit();
			return true;	
		}
}

function travelAddComment()
{

	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&addendum_id=<%=addendum_id%>&Type=pm_resT&ref=<%=chkaddendum%>&Resource_Id=<%= Travel_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}
	else
	{%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<%=addendum_id%>&Type=pm_resT&ref=<%=chkaddendum%>&Resource_Id=<%= Travel_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}%>
	
	document.forms[0].submit();
	return true;  
}

function travelManageSow()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'ManageSOWAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resT&Id=<%= Travel_Id %>&Activity_Id=<%= Activity_Id %>';
	
	<%}
	else
	{%>
		document.forms[0].action = 'ManageSOWAction.do?ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resT&Id=<%= Travel_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}%>
	
	document.forms[0].submit();
	return true;
}

function travelManageAssumption()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'ManageAssumptionAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resT&Id=<%= Travel_Id %>&Activity_Id=<%= Activity_Id %>'
	<%}
	else
	{%>
		document.forms[0].action = 'ManageAssumptionAction.do?ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resT&Id=<%= Travel_Id %>&Activity_Id=<%= Activity_Id %>'
	<%}%>
	
	document.forms[0].submit();
	return true;
}

function travelViewComment()
{
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<%=addendum_id%>&Type=pm_resT&ref=<%=chkaddendum%>&Id=<%= Travel_Id %>&Activity_Id=<%= Activity_Id %>';
	document.forms[0].submit();
	return true;
}

function travelViewDetails() {
		document.forms[0].action = 'TravelDetailAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Travel_Id=<%=Travel_Id%>&Activity_Id=<%=Activity_Id%>';
		document.forms[0].submit();
		return true;
}

</script>