
<script>
function projectJobManageStatus(v) {
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_job&id=<%=Job_Id%>";
	document.forms[0].submit();
	return true;	
}

function projectJobSendStatusReport() {
	document.forms[0].action = "EmailAction.do?Type=prm_job&Type1=prm_job&&id=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;
}

function projectJobViewComments() {
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}

function projectJobAddComment() {
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Appendix_Id=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}
function cancelTicket(alreadyCancelled) {
	var already = alreadyCancelled;
	if(already!='alreadyCancelled') {
		var convel = confirm("Are you sure you want to Cancel the Ticket?");
		if( convel )
		{
			document.forms[0].action = "JobDashboardAction.do?function=view&appendix_Id=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>&tabId=3&CancelTicket=CancelTicket";
			document.forms[0].submit();
			return true;	
		}
	} else {
		alert('Operation not allowed because ticket is already cancelled.');
	}
}
</script>