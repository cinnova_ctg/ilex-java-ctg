<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
	

<html:html>
<head>
	<title><bean:message bundle = "um" key = "um.title.unauthorised"/></title>
	<%@ include file = "../../Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css" />
</head>
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			 <td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "90%"> 
  		    		   <tr>
			    			<td  colspan = "3" class = "message" height = "30">
			    				<bean:message bundle = "um" key = "um.nodata"/>
			    			</td>
			    		</tr>
			    		
			    		<tr>
			    			<td  colspan = "3" class = "message" height = "30">
			    				<input type="button" class="Button" value="Back" onclick="history.go(-1);"> 
			    			</td>
			    		</tr>
				 </table>
			  </td>
			 </tr>
</table>
</html:html>

				 	