<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><bean:message key = "report.page.title"/></title>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="docm/javascript/JLibrary.js"></script>
<script language="javascript" src="docm/javascript/ilexGUI.js"></script>



<script language = "JavaScript">

	function chkInteger(obj,label){
		if(!isInteger(obj.value))	{	
			ShowMsg("<bean:message bundle="PVS" key="pvs.search.numeric.check"/>"+label);	
			obj.value ="";
			obj.focus();
			return false;
		}
		return true;
	}
	
function validate(){
	var status  = checkValidate();
	if(status == false)
		return false;
	else	
		return true;
} 	
	
function checkValidate(){
	var d=new Date();
	var check  = true;
		if(document.forms[0].selectedReport.value == ""){
			ShowMsg("<bean:message key ="alert.reporttype.notexists"/>");	
			check = false;
			return check;
		}
		if(document.forms[0].selectedMsa.value == "" || document.forms[0].selectedMsa.value == "0"){
			ShowMsg("<bean:message key ="alert.msa.present"/>");	
			check = false;
			return check;
		}
		if(document.forms[0].selectedYear.value == ""){
			ShowMsg("<bean:message key ="alert.year.empty"/>");	
			check = false;
			return check;
		}else{
				if(eval(document.forms[0].selectedYear.value) < 1970){
					ShowMsg("<bean:message key ="alert.year.not.valid"/>");	
					check = false;
					return check;
				}
				if(document.forms[0].selectedYear.value > d.getFullYear()){
					ShowMsg("<bean:message key ="alert.year.greater"/>");	
					check = false;
					return check;
				}
			}
									
		if(document.forms[0].selectedMonth.value == ""){
			ShowMsg("<bean:message key ="alert.month.empty"/>");	
			check = false;
			return check;
		}
		
		if(document.forms[0].selectedYear.value < d.getFullYear()){}
		else{
			
			if(d.getMonth() == 0){
				if(document.forms[0].selectedMonth.value == 1){
					ShowMsg("<bean:message key ="alert.reporttype.current.month"/>");	
					check = false;
					return check;	
				}else{
					if(document.forms[0].selectedMonth.value > d.getMonth()){
						ShowMsg("<bean:message key ="alert.montthless.currentmonth"/>");	
						check = false;
						return check;
					}	
				}
			}else{	
				if(document.forms[0].selectedMonth.value > d.getMonth()){
					ShowMsg("<bean:message key ="alert.montthless.currentmonth"/>");	
					check = false;
					return check;
				}
			}
		}
	return check;
	}
</script>
<%@ include  file="/NMenu.inc" %>
</head>
<bean:define id="dcMsaList" name="msalist"  scope="request"/>
<bean:define id="dcMonthName" name="monthname" scope="request"/>
<bean:define id="dcReportName" name="reportname" scope="request"/>
<body>
	<html:form action = "ReportGeneration">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr><td background="images/content_head_04.jpg" height="21">&nbsp;</td></tr>
				<tr><td><h2><bean:message  key = "report.page.title"/></h2></td></tr>
				<logic:present name = "Error" scope = "request">
					<tr><td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;<bean:message key ="exception.regenerated.reports.failed"/></td></tr>
				</logic:present>
				<logic:present name = "Success" scope = "request">
					<tr><td colspan = "4" class = "message" height = "30">&nbsp;&nbsp;<bean:message key ="success.regenerated.reports"/></td></tr>
				</logic:present>
				
	</table>			
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr><td width="2" height="0"></td>
			<td><table border="0" cellspacing="1" cellpadding="1" >	
				<tr><td colspan = "2" class = "formCaption"><bean:message key = "general.name"/></td></tr>				
				<tr>
					<td  class = "colDark"><bean:message key="report.type"/></td>
					<td  class = "colLight">
						 <html:select  name = "ReportGenerationForm" property="selectedReport" styleClass="select">
								<html:optionsCollection name = "dcReportName"  property = "reportlist"  label = "label"  value = "value" />
							</html:select>   
					</td>
				</tr>
				<tr><td colspan = "2" class = "formCaption"><bean:message key ="filter.criteria"/></td></tr>				
				<tr>
					<td  class = "colDark"><bean:message  key="msa.name"/></td>
					<td  class = "colLight" colspan = "5">
						 <html:select  name = "ReportGenerationForm" property="selectedMsa" styleClass="select" multiple = "true" size = "5">
								<html:optionsCollection name = "dcMsaList"  property = "msalist"  label = "label"  value = "value" />
							</html:select>   
					</td>
				</tr>
				<tr>
					<td  class = "colDark"><bean:message key="year"/></td>
					<td  class = "colLight">
						<html:text name = "ReportGenerationForm" property = "selectedYear"  size = "2" maxlength = "4" styleClass = "text" onblur = "chkInteger(document.forms[0].selectedYear,'Year');"/>   
					</td>
				</tr>
				<tr>
					<td  class = "colDark"><bean:message key="month"/></td>
					<td  class = "colLight">
						 <html:select  name = "ReportGenerationForm" property="selectedMonth" styleClass="select">
								<html:optionsCollection name = "dcMonthName"  property = "monthnamelist"  label = "label"  value = "value" />
							</html:select>   
					</td>
				</tr>
				<tr>
			  			<td colspan = "2" class = "colLight" align ="center">		
				  			<html:submit property="generateReport" styleClass="button_c" onclick = "return validate();">Re - Generate</html:submit>
			  			</td>
				</table>
			</td>				
		</tr>
	</table>
	
	<table border="0" cellspacing="1" cellpadding="1" align="left">
	<c:forEach var="map" items="${requestScope.MSAmap}" varStatus="status">
			<tr>
				<c:if test="${map[2]eq '1'}">
					<td class="message">&nbsp;&nbsp;<c:out value="${map[0]}" /></td>
					<td class="message">&nbsp;&nbsp;<c:out value="${map[1]}" /></td>
				</c:if>
				<c:if test="${map[2]eq '0'}">
					<td class="message">&nbsp;&nbsp;<i><c:out value="${map[0]}" /></i></td>
					<td class="message">&nbsp;&nbsp;<i><c:out value="${map[1]}" /></i></td>
				</c:if>
			</tr>
		</c:forEach>
	</table>
	</html:form>
</body>
</html>