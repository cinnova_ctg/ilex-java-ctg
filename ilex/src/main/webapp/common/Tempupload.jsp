<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ page language = "java" %>


<%
//	System.out.println("XXX---- inside tempupload ");
	/*by hamid start for multiple resource select start*/
	String MSA_Id = "";
	String Activity_Id = "";
	String addendum_id = "";
	String from = "";
	String  typeId = "";
	String tempdeleteflag = null; //flag to delete data from temp_resource table
	/*by hamid start for multiple resource select end*/
	String ref = "View";
	String path = "";
	String nettype = "";
	
	String id1 = ( String ) request.getAttribute( "Id1" );
	String id2 = ( String ) request.getAttribute( "Id2" );
	String type = ( String ) request.getAttribute( "type" );
	if(request.getAttribute( "typeId" ) != null )
		path = ( String ) request.getAttribute( "path" );
	String viewjobtype ="A" ;
	String appendix_Id="";
	String uploadflag = "";
	String ownerId = "%";
	String job_Id="";
	String jobid="";
	String resourceType="";
	String sowaddflag="";
	String sowupdateflag="";
	String assumptionaddflag="";
	String assumptionupdateflag="";
	String resourceListType="A";
	String appendixId = "";
	String msaId = "";
	String pagedashboard="";
	String qcChangestaus="";	
	
	

	if(request.getAttribute("changestatusflag")!=null){
		qcChangestaus=(String)request.getAttribute("changestatusflag");
	}
	if(request.getAttribute("pagedashboard")!=null){
		pagedashboard=(String)request.getAttribute("pagedashboard");
	}
	if(request.getAttribute( "typeId" ) != null )
		typeId = ( String ) request.getAttribute( "typeId" );
	
	/* by hamid for multiple resource select  start */
	if( request.getAttribute( "from" ) != null )
		from = ( String ) request.getAttribute( "from" );
	
	if( request.getAttribute( "Activity_Id" ) != null )
		Activity_Id = ( String ) request.getAttribute( "Activity_Id" );

	if( request.getAttribute( "MSA_Id" ) != null )
		MSA_Id = ( String ) request.getAttribute( "MSA_Id" );

	if( request.getAttribute( "tempdeleteflag" ) != null )
		tempdeleteflag = ( String ) request.getAttribute( "tempdeleteflag" );

	if( request.getAttribute( "ref" ) != null )
		ref = ( String ) request.getAttribute( "ref" );

	if( request.getAttribute( "addendum_id" ) != null )
		addendum_id = ( String ) request.getAttribute( "addendum_id" );

	/* by hamid for multiple resource select end */
	
	if( request.getAttribute( "appendix_Id" ) != null )
		appendix_Id = ( String ) request.getAttribute( "appendix_Id" );
	
	if( request.getAttribute( "uploadflag" ) != null )
		uploadflag = ( String ) request.getAttribute( "uploadflag" );
	
	String copyactivityflag = "";
	if( request.getAttribute( "copyactivityflag" ) != null )
		copyactivityflag = ( String ) request.getAttribute( "copyactivityflag" );

	if( request.getAttribute( "viewjobtype" ) != null )
		viewjobtype = ( String ) request.getAttribute( "viewjobtype" );

	if( request.getAttribute( "ownerId" ) != null )
		ownerId = ( String ) request.getAttribute( "ownerId" );
	
	if( request.getAttribute( "job_Id" ) != null )
		job_Id = ( String ) request.getAttribute( "job_Id" );

	if( request.getAttribute( "jobid" ) != null )
		jobid = ( String ) request.getAttribute( "jobid" );

	if( request.getAttribute( "resourceType" ) != null )
		resourceType = ( String ) request.getAttribute( "resourceType" );

	if( request.getAttribute( "sowaddflag" ) != null )
		sowaddflag = ( String ) request.getAttribute( "sowaddflag" );
	
	if( request.getAttribute( "sowupdateflag" ) != null )
		sowupdateflag = ( String ) request.getAttribute( "sowupdateflag" );

	if( request.getAttribute( "resourceListType" ) != null )
		resourceListType = ( String ) request.getAttribute( "resourceListType" );

	if( request.getAttribute( "appendixId" ) != null )
		appendixId = ( String ) request.getAttribute( "appendixId" );
	
	if( request.getAttribute( "msaId" ) != null )
		msaId = ( String ) request.getAttribute( "msaId" );

	if( request.getAttribute( "nettype" ) != null )
		nettype = ( String ) request.getAttribute( "nettype" );

	//System.out.println("Path---"+path);
	//System.out.println("type----"+type);

	if( path.equals( "custref" ) )
	{
		if( type.equals( "Appendix" ) ) {
	%>	
	    	<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" />
		    	<jsp:param name = "appendixid" value = "<%= typeId %>" />
		    	<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				<jsp:param name = "ownerId" value = "<%= ownerId %>" />
			</jsp:forward>
	<% } else {
				if (from.equals("AppendixDashboard")) {
			%>
					<jsp:forward page = "/AppendixHeader.do">
						<jsp:param name = "function" value = "view" />
				    	<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				    	<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
						<jsp:param name = "ownerId" value = "<%= ownerId %>" />
					</jsp:forward>
					
			<% }
				else {
			%>
			
				<jsp:forward page = "/JobDashboardAction.do">
					<jsp:param name = "function" value = "view" /> 
					<jsp:param name = "jobid" value = "<%= typeId %>" />
					<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
					<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
					<jsp:param name = "ownerId" value = "<%= ownerId %>" />
				</jsp:forward>
			
			<% }
		}	 
	}
	if( path.equals( "endcust" ) )
	{
		if( type.equals( "Appendix" ) ) {
			%>
			    	<jsp:forward page = "/AppendixHeader.do">
						<jsp:param name = "function" value = "view" />
				    	<jsp:param name = "appendixid" value = "<%= typeId %>" />
				    	<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
						<jsp:param name = "ownerId" value = "<%= ownerId %>" />
					</jsp:forward>
	<%}									
	}



	
	
	if( type.equals( "POWODashBoard" ) )
	{
	%>
			<jsp:forward page = "/POWOAction.do">
				<jsp:param name = "jobid" value = '<%= ( String ) request.getAttribute( "jobid" ) %>' /> 
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
			</jsp:forward>
	<%
		}
	if( type.equals( "Job" ) )
	{
	%>
			<jsp:forward page = "/JobDetailAction.do">
				<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
				<jsp:param name = "Job_Id" value = "<%= id1 %>" />
				<jsp:param name = "uploadflag" value = "<%= uploadflag %>" />
				<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' />
			</jsp:forward>
	<%
		}

	if( type.equals( "PMJobsite" ) )
	{
	%>
			<jsp:forward page = "/JobDetailAction.do">
				<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
				<jsp:param name = "Job_Id" value = "<%= id1 %>" />
				<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' /> 
			</jsp:forward>
	<%
		}

	if( type.equals( "PRMJobsite" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= id2 %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
			</jsp:forward>
	<%
		}

	if( type.equals( "jobcopy" ) )
	{
	%>
			<jsp:forward page = "/ActivityUpdateAction.do">
				<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
				<jsp:param name = "Job_Id" value = "<%= id1 %>" />
				<jsp:param name = "copyactivityflag" value = "<%= copyactivityflag %>" />
			</jsp:forward>
	<%
		}

	if( type.equals( "Appendixdashboard" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= id1 %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				
			</jsp:forward>
	<%
	}

	if( type.equals( "Jobdashboard" ) )
	{
		if(path.equals("appendixdashboard"))
		{%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				
			</jsp:forward>
		
		<%}
		else
		{ %>
			<jsp:forward page = "/JobDashboardAction.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "jobid" value = "<%= id1 %>" />
				
				
			</jsp:forward>
	<%}
	}


	if( type.equals( "PRMJobInstallNotes" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
			</jsp:forward>
	<%
	}


	if( type.equals( "PRMJobCustomerAppendixInfo" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
			</jsp:forward>
	<%
	}


	if( type.equals( "PRMAppendixContact" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				
			</jsp:forward>
	<%
	}

	if( type.equals( "PRMJobProcessChecklist" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				
			</jsp:forward>
	<%
	}

	if( type.equals( "PRMJobScheduleInfo" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
			</jsp:forward>
	<%
	}

	if( type.equals( "PRMChecklist" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				<jsp:param name = "ownerId" value = "<%= ownerId %>" />
			</jsp:forward>
	<%
	}

	if( type.equals( "qchecklist" ) )
	{
	%>
			<jsp:forward page = "/ChecklistTab.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "job_id" value = "<%= job_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				<jsp:param name = "ownerId" value = "<%= ownerId %>" />
			</jsp:forward>
	<%
	}
	
	
	if( type.equals( "netmedxdashboardfromticket" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				
			</jsp:forward>
	<%
	}
////Added By Amit For Bulk Job Creation,After save AppendixHeader.jsp will open	
	if( type.equals( "FormBulkJobCreation" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				<jsp:param name = "bulkuploadflag" value = "<%= copyactivityflag %>" />
			</jsp:forward>
	<%
	}

	/*by hamid for multiple resource select start*/
	
	
		if(type.equals( "Material" ) || type.equals( "Labor" ) || type.equals( "Travel" ) || type.equals( "Freight" )) {
			if(from.equals("am"))
			{%>
			
				<jsp:forward page = "/AMResourceLibraryListAction.do" >
				<jsp:param name = "ref" value = "add" /> 
				<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
				<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
				<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
				<jsp:param name = "editable" value = "editable" />
			</jsp:forward>
			<%} 
		}
		
	
		if( type.equals( "Material" ) )
		{
			if(from.equals("am"))
				{%>
				
					<jsp:forward page = "/MaterialResourceAction.do" >
					<jsp:param name = "ref" value = "View" /> 
					<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "MSA_Id" value = "<%= MSA_Id %>" />
					<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
					
				</jsp:forward>
				<%} else
				{ %>
					<jsp:forward page = "/ResourceListAction.do">
					<jsp:param name = "resourceListType" value = "<%=resourceListType%>" />
					<jsp:param name = "ref" value = "<%= ref %>" /> 
					<jsp:param name = "from" value = "<%= from%>" />
					<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
					<jsp:param name = "addendum_id" value = "<%= addendum_id %>" />
				</jsp:forward>
				<%} %>
					
		<%
		}
		if( type.equals( "Labor" ) )
		{
		
				if(from.equals("am"))
				{%>
					<jsp:forward page = "/LaborResourceAction.do">
					<jsp:param name = "ref" value = "View" /> 
					<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "MSA_Id" value = "<%= MSA_Id %>" />
					<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
					
				</jsp:forward>
				<%} else
				{ %>
					<jsp:forward page = "/ResourceListAction.do">
					<jsp:param name = "resourceListType" value = "<%=resourceListType%>" />
					<jsp:param name = "ref" value = "<%= ref %>" /> 
					<jsp:param name = "from" value = "<%= from%>" />
					<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
					<jsp:param name = "addendum_id" value = "<%= addendum_id %>" />
									
				</jsp:forward>
				<%} %>
					
		<%
		}
		if( type.equals( "Freight" ) )
		{
				if(from.equals("am"))
				{%>
					<jsp:forward page = "/FreightResourceAction.do">
					<jsp:param name = "ref" value = "View" /> 
					<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "MSA_Id" value = "<%= MSA_Id %>" />
					<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
					
				</jsp:forward>
				<%} else
				{ %>
					<jsp:forward page = "/ResourceListAction.do">
					<jsp:param name = "resourceListType" value = "<%=resourceListType%>" />
					<jsp:param name = "ref" value = "<%= ref %>" />
					<jsp:param name = "from" value = "<%= from%>" /> 
					<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
					<jsp:param name = "addendum_id" value = "<%= addendum_id %>" />
									
				</jsp:forward>
				<%} %>
					
		<%
		}
		if( type.equals( "Travel" ) )
		{
				if(from.equals("am"))
				{%>
					<jsp:forward page = "/TravelResourceAction.do">
					<jsp:param name = "ref" value = "View" /> 
					<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "MSA_Id" value = "<%= MSA_Id %>" />
					<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
					
				</jsp:forward>
				<%} else
				{ %>
					<jsp:forward page = "/ResourceListAction.do">
					<jsp:param name = "resourceListType" value = "<%=resourceListType%>" />
					<jsp:param name = "ref" value = "<%= ref %>" /> 
					<jsp:param name = "from" value = "<%= from%>" />
					<jsp:param name = "Activitylibrary_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
					<jsp:param name = "tempdeleteflag" value = "<%= tempdeleteflag %>" />
					<jsp:param name = "addendum_id" value = "<%= addendum_id %>" />
									
				</jsp:forward>
				<%} %>
					
		<%
		}
	/*by hamid for multiple resource select end*/	

	if( type.equals( "PRM_QCChecklist" ) )
	{
	%>
			<jsp:forward page = "/MasterchecklisttoAppendixcopyAction.do">
				<jsp:param name = "appendix_Id" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				<jsp:param name = "ownerId" value = "<%= ownerId %>" />
			</jsp:forward>
	<%
	}

	if( type.equals( "PartnerChecklistAppendixdashboard" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				<jsp:param name = "ownerId" value = "<%= ownerId %>" />
			</jsp:forward>
	<%
	}

	if( type.equals( "sowAssumptiontoResource" ) )
	{
	%>
			<jsp:forward page = "/ResourceListAction.do">
				<jsp:param name = "from" value = "<%= from %>" /> 
				<jsp:param name = "ref" value = "<%= ref %>" /> 
				<jsp:param name = "jobid" value = "<%= job_Id %>" />
				<jsp:param name = "Activity_Id" value = "<%= Activity_Id %>" />
				<jsp:param name = "resourceListType" value = "<%= resourceListType %>" />
				<jsp:param name = "addendum_id" value = "<%= addendum_id %>" />
				<jsp:param name = "sowaddflag" value = "<%= sowaddflag %>" />
				<jsp:param name = "sowupdateflag" value = "<%= sowupdateflag %>" />
				<jsp:param name = "assumptionaddflag" value = "<%= assumptionaddflag %>" />
				<jsp:param name = "assumptionupdateflag" value = "<%= assumptionupdateflag %>" />
				
			</jsp:forward>
	<%
	}

	if( type.equals( "PartnerChecklistJobdashboard" ) )
	{
	%>
			<jsp:forward page = "/JobDashboardAction.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "jobid" value = "<%= job_Id %>" />
				<jsp:param name = "appendixid" value = "<%= appendix_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				<jsp:param name = "ownerId" value = "<%= ownerId %>" />
			</jsp:forward>
	<%
	}

	if( type.equals( "AppendixViewSelector" ) )
	{
	%>
	
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "appendixid" value = "<%= appendixId %>" />
				<jsp:param name = "viewjobtype" value = "%" />
				<jsp:param name = "ownerId" value = "%" />
				<jsp:param name = "fromPage" value = "appendix" />
				<jsp:param name = "msaId" value = "<%= msaId %>" />
				<jsp:param name = "showList" value = "" />
				<jsp:param name = "resetList" value = "" />
				<jsp:param name = "nettype" value = "<%=nettype%>" />
			</jsp:forward>
	<%
	}
    if(type.equals( "jobCompleted")) {
	 %>
	 	<jsp:forward page = "/ChecklistTab.do">
				<jsp:param name = "function" value = "view" /> 
				<jsp:param name = "job_id" value = "<%= job_Id %>" />
				<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
				<jsp:param name = "ownerId" value = "<%= ownerId %>" />
				<jsp:param name = "page" value ="<%= pagedashboard%>"/>
				<jsp:param name = "changestatusflag" value ="<%= qcChangestaus%>"/>
				<jsp:param name = "jobCompleted" value = "jobCompleted"/>
				
		</jsp:forward>
	<%
	}
 %>
	