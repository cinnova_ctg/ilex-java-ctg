<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<html:html>
<% 
	String setvalue = "";
	int size=0;
	if(request.getAttribute( "size" ).toString()!=null)
	{
		size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
	}
%>
<head>
<title><bean:message bundle = "pm" key = "common.searchwindow.search"/></title>
<%@ include file="../Header.inc" %>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">

</head>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" >
<html:form action = "/SearchProblemCategoryAction" >
<html:hidden property="search_type" />
<html:hidden property="search" />
<html:hidden property="lo_ot_id" />
<table  border="0" cellspacing="1" cellpadding="1"  >
  <tr>
  	<td  width="2" height="0"></td>
  	<td><table border="0" cellspacing="1" cellpadding="1" > 
			<tr> 
		  		<td colspan = "6" class = "labeleboldwhite" height = "30">
		  			<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "requestor_search">
			  			<bean:message bundle = "pm" key = "common.rn.searchwindow.searchcriteria"/>
			  		</logic:equal>	
		  			
		  			<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "company_search">
			  			<bean:message bundle = "pm" key = "common.cn.searchwindow.searchcriteria"/>
			  		</logic:equal>
		  			
		  			<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "problem_category">
			  			<bean:message bundle = "pm" key = "common.pc.searchwindow.searchcriteria"/>
		  			</logic:equal>
			  	</td>
			</tr> 
		  	  
		  	<tr>
		  		<td class = "labelebold" width = "100" >
		  			<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "requestor_search">
			  			<bean:message bundle = "pm" key = "common.rn.searchwindow.requestor"/>
			  		</logic:equal>
			  		
			  		<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "company_search">
			  			<bean:message bundle = "pm" key = "common.cn.searchwindow.companyname"/>
		  			</logic:equal>	
		  			
		  			<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "problem_category">
			  			<bean:message bundle = "pm" key = "common.searchwindow.category"/>
		  			</logic:equal>
		  		</td>
		  		
		  		<td class = "labele" >
				 	<html:select property = "problemcategory" size = "1" styleClass = "labele" onchange = "javascript:return refresh();">
						<html:optionsCollection name = "SearchProblemCategoryForm" property = "problemcategorycombo" value = "value" label = "label"/>    
					</html:select>
				</td>
		   	</tr>
			
			<tr>
				<td class = "labelobold" width = "100">
				 	<bean:message bundle = "pm" key = "common.searchwindow.keyword"/>
				</td>

				<td class = "labelobold" >
				 	<html:text property = "keyword" styleClass = "textbox" size="15"/>
				</td>
			</tr>

			<tr>	
				<td class = "labelo"  colspan = "6">
				 	<html:button property = "search" styleClass = "button" onclick = "javascript:return refresh();">
				 		<bean:message bundle = "pm" key = "common.searchwindow.search"/>
				 	</html:button>
				</td>
			</tr>
			
			<tr></tr>
		</table>
	</td>
  </tr>

<table  border="0" cellspacing="1" cellpadding="1"  >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" > 
			<logic:equal name = "SearchProblemCategoryForm" property = "searchclick" value = "true">
			<tr>
    			<td  class="labeleboldwhite" height="30" colspan="7"><bean:message bundle="pm" key="common.searchwindow.basicsearchresults" /></td>
  			</tr>
  			
			<tr>
				<td width="20">&nbsp;</td>
				<td class = "tryb">
					<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "requestor_search">
			  			<bean:message bundle = "pm" key = "common.rn.searchwindow.requestor"/>
			  		</logic:equal>	
			  		
			  		<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "company_search">
			  			<bean:message bundle = "pm" key = "common.cn.searchwindow.companyname"/>
			  		</logic:equal>
			  		
		  			<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "problem_category">
			  			<bean:message bundle = "pm" key = "common.pc.searchwindow.problemcategory"/>
		  			</logic:equal>
				</td>
			</tr>
			
			<%int i=1;
		  	int j = 0;
			String bgcolor = ""; 
			%>
			
			<logic:iterate id = "problemlist" name = "SearchProblemCategoryForm" property = "problemlist">
			
		 	<%if((i%2)==0)
				{
					bgcolor="labele"; 
				} 
			  else
				{
					bgcolor="labelo";
				}
		
				
				if( size == 1 ) 
				{ 
					setvalue = "javascript:assigncategory( '' );";
				}
				else
				{
					setvalue = "javascript:assigncategory( '"+j+"' );";
				}%>
			
			
			
			
				<bean:define id = "problemcategory_id" name = "problemlist" property = "problemcategory_id" type = "java.lang.String"/>
				<tr>
					
					<td >
						<html:radio property = "problemcategory_id" value = "<%= problemcategory_id %>" onclick = "<%= setvalue %>"/>
					</td>
					
					<td  class="<%=bgcolor %>" nowrap>
						<bean:write name = "problemlist" property = "problemcategory_desc" />
						<html:hidden name = "problemlist" property = "problemcategory_desc" />
						<logic:equal name = "SearchProblemCategoryForm" property = "search_type" value = "requestor_search">
							<html:hidden name = "problemlist" property = "poc_email" />
						</logic:equal>	
					</td>
				</tr>

				<%i++;j++; %>
			</logic:iterate>
			<%if(i==1)
			{ %>
				<tr>
					<td>&nbsp;</td>
					<td  class="message" height="30" colspan="7"><bean:message bundle="pm" key="common.searchwindow.norecord" /></td>
				</tr>
			<%} %>
	
			</logic:equal>
 		</table> 
 	  </td>
   </tr>
   
	
  	
  	
</html:form>
</table>

<script>

function refresh()
{
	document.forms[0].search.value = 'Search';
	document.forms[0].submit();
	return true;
}

function assigncategory( temp )
{
	if( temp == '' )
	{
			if(document.forms[0].search_type.value == 'requestor_search') {
				window.opener.document.forms[0].requestor_name.value = document.forms[0].problemcategory_desc.value;
				window.opener.document.forms[0].requestor_email.value = document.forms[0].poc_email.value;
			}
			
			if(document.forms[0].search_type.value == 'company_search') {
				window.opener.document.forms[0].lo_ot_id.value = document.forms[0].problemcategory_id.value;
				window.opener.document.forms[0].company_name.value = document.forms[0].problemcategory_desc.value;
				window.opener.document.forms[0].site_name.value = document.forms[0].problemcategory_desc.value;
				getRefresh();
			}	
			
			if(document.forms[0].search_type.value == 'problem_search') {
				window.opener.document.forms[0].category.value = document.forms[0].problemcategory_id.value;
				window.opener.document.forms[0].category_name.value = document.forms[0].problemcategory_desc.value;
			}	
					
	}
	
	else
	{
		
		if(document.forms[0].search_type.value == 'requestor_search') {
			window.opener.document.forms[0].requestor_name.value = document.forms[0].problemcategory_desc[temp].value;
			window.opener.document.forms[0].requestor_email.value = document.forms[0].poc_email[temp].value;
		}
			
		if(document.forms[0].search_type.value == 'company_search') {
				window.opener.document.forms[0].lo_ot_id.value = document.forms[0].problemcategory_id[temp].value;
				window.opener.document.forms[0].company_name.value = document.forms[0].problemcategory_desc[temp].value;
				window.opener.document.forms[0].site_name.value = document.forms[0].problemcategory_desc[temp].value;
				getRefresh();
		}	
			
		if(document.forms[0].search_type.value == 'problem_search') {
			window.opener.document.forms[0].category.value = document.forms[0].problemcategory_id[temp].value;
			window.opener.document.forms[0].category_name.value = document.forms[0].problemcategory_desc[temp].value;
		}	
			
	}
	window.close();
}

function getRefresh()
{
    window.opener.document.forms[0].refresh.value = "true";
	window.opener.document.forms[0].refreshprodlist.value = "true";
	window.opener.document.forms[0].dialog_number.value = eval(window.opener.document.forms[0].dialog_number.value)+1;
	window.opener.document.forms[0].submit();
	return true;
}
</script>
</body>
</html:html>

