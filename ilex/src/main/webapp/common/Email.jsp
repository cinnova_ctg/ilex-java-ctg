<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import = "java.util.*" %>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />
<bean:size id = "poclist" name = "EmailForm" property = "custPOC" />
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">





<% int  i = 0; 

	boolean flagto = true;
	boolean labelflag = false;
	String valstr = "";
	int j = 0;
	int k=2;
	String checkowner=null;
	String Type=null;
	
	
	int addRow = -1;
	boolean addSpace = false;
	int rowHeight = 120;
	int ownerListSize = 0;

	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());

	if(request.getAttribute("type") != null)
		Type = request.getAttribute("type").toString();

%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkstatus = null;


int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


String MSA_Id ="";
String msastatus="";
String uploadstatus ="";
String status ="";

if(request.getAttribute("status")!=null){
	status = request.getAttribute("status").toString();
	msastatus = request.getAttribute("status").toString();
}
if(request.getAttribute("MSA_Id")!=null)
	MSA_Id= request.getAttribute("MSA_Id").toString();

if(request.getAttribute("uploadstatus")!=null)
	uploadstatus= request.getAttribute("uploadstatus").toString();
	
	



rowHeight += jobOwnerListSize*18;
%>
<% 
	ArrayList outer=(ArrayList)request.getAttribute("docDetailPageList");
	if(outer != null) {
		for(int x=0; x<outer.size(); x++){
			ArrayList inner=(ArrayList)outer.get(x);
			String addDate=(String)inner.get(2);
			String[] b=addDate.split(" ");
			String[] a=b[0].split("-");
			inner.remove(2);
			inner.add(2,a[1]+"/"+a[2]+"/"+a[0]);
		}
	}
%>
	<script>
		
	// function add for Reports 
	 function openreportschedulewindowFromEmail(report_type,append) {
	
	str = "ReportScheduleWindow.do?report_type="+report_type+"&typeid="append+"&fromPage=NetMedX";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 500, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
} 
	
	
	
	</script>


<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>		 
	
	
<bean:define id = "check_for_fileupload" name = "EmailForm" property = "check_for_fileupload" />
<bean:define id = "type" name = "EmailForm" property = "type" />

<html:html>
<head>
	<title><bean:message bundle = "pm" key = "common.Email.Title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css"> 
<%@ include file = "/NMenu.inc" %>  	
</head>
<%-- <%@ include  file="/MSAMenu.inc" %> --%>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>
<%@ include file = "/NMenu.inc" %>
 

<%@ include  file="/ProjectJobMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad ="leftAdjLayers();">

<html:form action = "/EmailAction" target = "ilexmain" enctype = "multipart/form-data">
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "id1" />
<html:hidden property = "id2" />
<html:hidden property = "id3" />
<html:hidden property = "type" />
<html:hidden property = "type1" />
<html:hidden property = "order_type" />
<html:hidden property = "act_id" />
<html:hidden property = "powo_type" />
<html:hidden property = "powo_fromtype" />
<html:hidden property = "invtype1" />
<html:hidden property = "invtype2" />
<html:hidden property = "to" />
<html:hidden property = "cc" />
<html:hidden property = "bcc" />
<html:hidden property = "authenticate" />
<html:hidden property = "check_for_fileupload" />
<html:hidden property = "from" />
<html:hidden property = "po_wo_number" />
<html:hidden property = "filenameX" />
<html:hidden property = "MSA_Id" />
<html:hidden property="otherCheck"/> 
<html:hidden property="go"/>
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property="jobOwnerOtherCheck"/> 
<html:hidden property="addendumFlag"/>
<html:hidden property="tabId"/>
<html:hidden property="isClicked"/>
<html:hidden property="onlyEmail"/>

<logic:equal name = "EmailForm" property = "type" value = "MSA">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        	<%-- <%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )){%>
							<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
						<%}else{%>
	                 			 <td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
							<%}%>
							<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
							
					<%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>		
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7">
	 						<h2>&nbsp;MSA Send Mail:&nbsp;<bean:write name = "EmailForm" property = "name" /></h2>
	 					</td>
	 				</tr>
	      		</table>
   				</td>
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "selectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<% 
																checkowner = "javascript: checking('"+ownerId+"');";
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "selectedOwners" onclick =  "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal> 	

<logic:equal name = "EmailForm" property = "type" value = "Appendix">
<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			       <%--  <tr>
			      <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
					<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>
	
					<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			        
			        </tr> --%>
			   <%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>	     
			        
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?Appendix_Id=<bean:write name = "EmailForm" property = "id2" />&MSA_Id=<bean:write name="EmailForm" property="MSA_Id"/>"><bean:write name="EmailForm" property="msaname"/></a></div>
					    </td>
	 				</tr>
	 				<logic:present name ="docMException" scope="request">
	 				<tr>
	 					<td colspan=7 class = "message" style="padding-left: 10px; padding-top: 5px;">MSA is formally not approved, can not be attached with Appendix.
						</td>
	 				</tr>
	 				</logic:present>
	 				<tr>
	 					<td colspan="7">
	 						<h2>&nbsp;Appendix Send Mail:&nbsp;<bean:write name = "EmailForm" property = "name"/></h2>
			 		   </td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable" border=1>
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<% 
																checkowner = "javascript: checking('"+ownerId+"');";
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>">
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</logic:equal>

<%if(type.equals("prm_appendix") || type.equals("PRMAppendix")){ %>
<logic:notEqual name = "EmailForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AppendixDashboardMenuScript.inc" %>
<%-- <!--  Menu Change  -->
			<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li><a href="JobEditAction.do?ref=inscopeview&amp;appendix_Id=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;Job_Id=0">Add Job</a></li>
        		<li>
        			<a href="#"><span>Bulk Job</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=<%= appendixid %>">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=<%= appendixid %>">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li>
				
	        		<li>
	        			<a href="#"><span>Contract Documents</span></a>
	        			<ul>
							<li><a href="#"><span>Appendix</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('pdf');">PDF</a></li></ul></li></ul></li><li><a href="#"><span>Addendum 01</span></a><ul><li><a href="javascript:alert(canNotSendNonApprovedAddendum);">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('38732', 'pdf');">PDF</a></li><li><a href="javascript:viewContractDocument('38732', 'rtf');">RTF</a></li><li><a href="javascript:viewContractDocument('38732', 'html');">HTML</a></li><li><a href="javascript:viewContractDocument('38732', 'excel');">EXCEL</a></li></ul></li></ul></li><li><a href="#"><span>Addendum 02</span></a><ul><li><a href="javascript:alert(canNotSendNonApprovedAddendum);">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('331185', 'pdf');">PDF</a></li><li><a href="javascript:viewContractDocument('331185', 'rtf');">RTF</a></li><li><a href="javascript:viewContractDocument('331185', 'html');">HTML</a></li><li><a href="javascript:viewContractDocument('331185', 'excel');">EXCEL</a></li></ul></li></ul></li><li><a href="javascript:addaddendum('add', 'add');">New Addendum</a></li>
	        			</ul>
	        		</li>
				
        		<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Custom Fields</a></li>
        		<li><a href="AddCustRef.do?typeid=<%= appendixid %>&amp;type=Appendix&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Customer Reference</a></li>
        		<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">External Sales Agent</a></li>
        		<li>
        			<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;initialCheck=true&amp;pageType=jobsites">Job Sites</a></li>
        		<li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=<%= appendixid %>&amp;type=P&amp;page=appendixdashboard&amp;pageid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Update</a></li>
						
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Special Instructions/Conditions</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">Team</a></li>
        		<li><a href="OnlineBid.do?appendixId=<%= appendixid %>&amp;fromPage=Appendix">Online Bid</a></li>
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
        		<li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li>
				<li><a href="MSPCCReportAction.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">MSP C/C Report</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid %>"><center>Search</center></a></td>
						
  		</li>
  	
	</ul>
</div>
 --%>



<!--  End Menu -->
<%-- <div id="menunav">
  
<ul>
  		<li>
        	<a href="#" class="drop"><span>Manage1</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('115922', 'pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerId%>">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&amp;type=Appendix&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerId%>">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerId%>&amp;fromPage=Appendix">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerId%>&amp;initialCheck=true&amp;pageType=jobsites"> View Job Sites</a></li>
        		<!-- <li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a href="#" class="drop"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>">Search</a>
  		</li>
  		
	</ul>


</div> --%>

<!--  End of Menu -->
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
					<td colspan = "4">
						<h2><bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.statusreport"/>&nbsp;For&nbsp;<bean:message bundle = "pm" key = "common.Email.status1"/>:&nbsp;<bean:write name="EmailForm" property="name"/></h2>
					</td>
				</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id = "OwnerSpanId"> 
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
</logic:notEqual>
<logic:equal name = "EmailForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<div id="menunav">
  
<ul>
  		<li>
        	<a href="#" class="drop"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:viewContractDocument('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerId%>&amp;fromPage=Appendix">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerId%>&amp;initialCheck=true&amp;pageType=jobsites"> View Job Sites</a></li>
        		<!-- <li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         	<li>
        	<a href="#" class="drop"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>">Search</a>
  		</li>
  		
	</ul>


</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
			        
		        </tr>
		   <td colspan = "7" height="1"><h2><bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.statusreport"/>&nbsp;For&nbsp;NetMedX:&nbsp;<bean:write name="EmailForm" property="name"/></h2></td>
	      </tbody></table>
   </td>
      
    <td width="35" valign="top"><img width="35" height="60" alt="" src="images/content_head_02.jpg" id="content_head_02"></td>
	<td width="155" valign="top">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr>
		          <td width="155" height="39" colspan="2" class="headerrow">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tbody><tr>
			                <td width="150">
			                	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				                   	<tbody><tr>
							            <td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('II');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" title="Scheduled" id="Image10" alt="Month" src="http://localhost:8080/Ilex/images/Icon_Scheduled.gif"></a></td>
	  									<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IO');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" title="Scheduled OverDue" id="Image10" alt="Month" src="images/Icon_Schedule_Overdued.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('ION');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" title="OnSite" id="Image10" alt="Month" src="images/Icon_Onsite.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IOF');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" title="Offsite" id="Image10" alt="Month" src="images/Icon_Ofsite.gif"></a></td>
				                    </tr>
			                  	</tbody>
			                  	</table>
			                </td>
			              </tr>
			          </tbody></table>
		          </td>
		        </tr>
        		<tr>
          		<td width="140" height="21" background="images/content_head_04.jpg">
          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              		<tbody><tr>
              		<td width="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li id="closeNow" name="closeNow"><a onclick="ShowDiv();" title="Open View" href="javascript:void(0)">My Views <img border="0" onclick="ShowDiv();" title="Open View" src="images/showFilter.gif"></a><a class="imgstyle" href="javascript:void(0)"><img border="0" onclick="hideDiv();" title="Close View" src="images/offFilter2.gif"></a></li>
									</div>								
	                			</div>		
						<span>
								<div class="divstyle" id="filter" style="">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tbody><tr>
		                      <td width="50%" valign="top">
		                  <!--  add code  -->    
		                <table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
															 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																		<bean:write name ="list" property = "statusid"/>
															 		</html:multibox>   
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table>
									
									
									
									<%-- <table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="3" class="filtersCaption">Status View</td>
										</tr>
										<tr>
											<td></td>
											<td class="tabNormalText">
											<logic:present name="jobStatusList" scope="request">
												<logic:iterate id="list" name="jobStatusList">
													<bean:define id="statusName" name="list" property="statusdesc" type="java.lang.String" />
													<bean:define id="statusId" name="list" property="statusid" type="java.lang.String" />
													<%checkstatus = "javascript: checkingStatus('" + statusId + "');";
													if ((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
														addStatusRow++; addStatusSpace = true;
													}%>
													<%if (addStatusSpace) {%>
													&nbsp;&nbsp;&nbsp;&nbsp;
													<%}%>
													<html:multibox property="jobSelectedStatus" onclick="<%=checkstatus%>">
														<bean:write name="list" property="statusid" />
													</html:multibox>
													<bean:write name="list" property="statusdesc" />
													<br> 
													<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present> 
											
											<img src="images/hrfilterLine.gif" width="100px" class="imagefilter" />
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="week">This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>" property="jobMonthWeekCheck" value="month">This Month</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="<%=formBean%>"	property="jobMonthWeekCheck" value="clear">Clear</html:radio></td></tr>
											</table>
											</td>
										</tr>
									</table>  --%>    
		                      <!--  End code  -->
		                 <!-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                 <tbody><tr>
                          <td class="filtersCaption" colspan="3">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										
									    	 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('AI');" value="AI" name="jobSelectedStatus">   
											 	Inwork<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IS');" value="IS" name="jobSelectedStatus">   
											 	To Be Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('II');" checked="checked" value="II" name="jobSelectedStatus">   
											 	Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IO');" checked="checked" value="IO" name="jobSelectedStatus">   
											 	Scheduled Overdue<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('ION');" checked="checked" value="ION" name="jobSelectedStatus">   
											 	Onsite<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IOF');" checked="checked" value="IOF" name="jobSelectedStatus">   
											 	Offsite<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('F');" value="F" name="jobSelectedStatus">   
											 	Complete<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('O');" value="O" name="jobSelectedStatus">   
											 	Closed<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('H');" value="H" name="jobSelectedStatus">   
											 	Hold<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('C');" value="C" name="jobSelectedStatus">   
											 	Cancelled<br> 
											 	
											
												
										<img width="100px" class="imagefilter" src="images/hrfilterLine.gif">
										<table cellpadding="0">
											<tbody><tr><td class="tabNormalText"><input type="radio" value="week" name="jobMonthWeekCheck">This Week</td></tr>
											<tr><td class="tabNormalText"><input type="radio" value="month" name="jobMonthWeekCheck">This Month</td></tr>	
											<tr><td class="tabNormalText"><input type="radio" checked="checked" value="clear" name="jobMonthWeekCheck">Clear</td></tr>	
										</tbody></table>	
									</td>
	    					</tr>
                        </tbody>
                        </table> -->
                        
                        
                        
                        </td>
                      <td><img width="1px" height="120" class="imagefilter" src="images/filterLine.gif"></td>
                      <td width="50%" valign="top">
                      
                <!--  add code  --> 
                
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												        <span id = "OwnerSpanId"> 
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
								
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
															 			</html:multibox>   
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>	
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                 <%-- <table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table> 
                 
                  --%>
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                <!--  End  -->      
                   <%--    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td class="filtersCaption" colspan="3">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id="OwnerSpanId"> 
					   			
							    	<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
							    		
							    		
								    		
							    			 
							    		<input type="checkbox" onclick="javascript: checkingOwner('<%=ownerId%>');" checked="checked" value="7" name="jobSelectedOwners">   
										  Me
										  <br> 
									
									</span>
								
								
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img height="34" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="changeFilter();" class="divbutton" src="images/showBtn.gif"><br></a></td></tr>
                              <tr><td colspan="3"><img width="2" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="resetFilter();" class="divbutton" src="images/reset.gif"><br></a></td></tr>                              
                              <tr><td colspan="3"><img height="8" src="images/spacer.gif"></td></tr>
                              </tbody></table> --%>
                              
                              
                              </td>
                        </tr>
  			</tbody></table>											
		</div>
	</span>
				</td>
	
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      </td>
 </tr>
 </tbody>
 </table>	
<!-- <table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		   
	      </tbody></table>
   </td>
      
    <td width="35" valign="top"><img width="35" height="60" alt="" src="images/content_head_02.jpg" id="content_head_02"></td>
	<td width="155" valign="top">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr>
		          <td width="155" height="39" colspan="2" class="headerrow">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tbody><tr>
			                <td width="150">
			                	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				                   	<tbody><tr>
							            <td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('II');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" title="Scheduled" id="Image10" alt="Month" src="http://localhost:8080/Ilex/images/Icon_Scheduled.gif"></a></td>
	  									<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IO');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" title="Scheduled OverDue" id="Image10" alt="Month" src="images/Icon_Schedule_Overdued.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('ION');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" title="OnSite" id="Image10" alt="Month" src="images/Icon_Onsite.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IOF');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" title="Offsite" id="Image10" alt="Month" src="images/Icon_Ofsite.gif"></a></td>
				                    </tr>
			                  	</tbody>
			                  	</table>
			                </td>
			              </tr>
			          </tbody></table>
		          </td>
		        </tr>
        		<tr>
          		<td width="140" height="21" background="images/content_head_04.jpg">
          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              		<tbody><tr>
              		<td width="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li id="closeNow" name="closeNow"><a onclick="ShowDiv();" title="Open View" href="javascript:void(0)">My Views <img border="0" onclick="ShowDiv();" title="Open View" src="images/showFilter.gif"></a><a class="imgstyle" href="javascript:void(0)"><img border="0" onclick="hideDiv();" title="Close View" src="images/offFilter2.gif"></a></li>
									</div>								
	                			</div>		
						<span>
								<div class="divstyle" id="filter" style="">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tbody><tr>
		                      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		                          <tbody><tr>
                           	 <td class="filtersCaption" colspan="3">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										
									    	 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('AI');" value="AI" name="jobSelectedStatus">   
											 	Inwork<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IS');" value="IS" name="jobSelectedStatus">   
											 	To Be Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('II');" checked="checked" value="II" name="jobSelectedStatus">   
											 	Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IO');" checked="checked" value="IO" name="jobSelectedStatus">   
											 	Scheduled Overdue<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('ION');" checked="checked" value="ION" name="jobSelectedStatus">   
											 	Onsite<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IOF');" checked="checked" value="IOF" name="jobSelectedStatus">   
											 	Offsite<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('F');" value="F" name="jobSelectedStatus">   
											 	Complete<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('O');" value="O" name="jobSelectedStatus">   
											 	Closed<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('H');" value="H" name="jobSelectedStatus">   
											 	Hold<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('C');" value="C" name="jobSelectedStatus">   
											 	Cancelled<br> 
											 	
											
												
										<img width="100px" class="imagefilter" src="images/hrfilterLine.gif">
										<table cellpadding="0">
											<tbody><tr><td class="tabNormalText"><input type="radio" value="week" name="jobMonthWeekCheck">This Week</td></tr>
											<tr><td class="tabNormalText"><input type="radio" value="month" name="jobMonthWeekCheck">This Month</td></tr>	
											<tr><td class="tabNormalText"><input type="radio" checked="checked" value="clear" name="jobMonthWeekCheck">Clear</td></tr>	
										</tbody></table>	
									</td>
	    					</tr>
                        </tbody></table></td>
                      <td><img width="1px" height="120" class="imagefilter" src="images/filterLine.gif"></td>
                      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td class="filtersCaption" colspan="3">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id="OwnerSpanId"> 
					   			
							    	
							    		
							    		
								    		
							    			 
							    		   
										  Me
										  <br> 
									
									</span>
								
								
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img height="34" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="changeFilter();" class="divbutton" src="images/showBtn.gif"><br></a></td></tr>
                              <tr><td colspan="3"><img width="2" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="resetFilter();" class="divbutton" src="images/reset.gif"><br></a></td></tr>                              
                              <tr><td colspan="3"><img height="8" src="images/spacer.gif"></td></tr>
                              </tbody></table></td>
                        </tr>
  			</tbody></table>											
		</div>
	</span>
				</td>
	
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      </td>
 </tr>
 </tbody>
 </table> -->






<%-- <table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr>
		        	<td colspan = "7" height="1"><h2><bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.statusreport"/>&nbsp;For&nbsp;NetMedX:&nbsp;<bean:write name="EmailForm" property="name"/></h2></td>
		        </tr> 
	      </table>
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        <tr>
	          <td width="155" height="39"  class="headerrow" colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" border="0">
		              <tr>
		                <td width="150">
		                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
			                   	<tr>
						            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
			                    </tr>
		                  	</table>
		                </td>
		              </tr>
		          </table>
	          </td>
	        </tr>
	        <tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>	
	                			<span>
									<div id="filter" class="divstyle">
					        			<table width="250" cellpadding="0" class="divtable">
						                     <tr>
					                            <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
															 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																		<bean:write name ="list" property = "statusid"/>
															 		</html:multibox>   
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="EmailForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table></td>
		                      			    	<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
			                      			    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												        <span id = "OwnerSpanId"> 
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
								
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
															 			</html:multibox>   
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>											
						            </div>
				                </span>
							 </td>
	              		</tr>
            		</table>
            	</td>
        	</tr>
	    </table>
	</td>
 </tr>
</table> --%>
<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
</logic:equal>
<%}%>
<c:if test="${EmailForm.type eq 'prm_job' || EmailForm.type eq 'Send POWO'}">
	<c:if test = "${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/Menu_Addendum.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/Menu_DefaultJob.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/Menu_ProjectJob.inc" %>
	</c:if>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
		         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
					 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
					 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
					 <a ><span class="breadCrumb1">Email</a>
		    </td>
		</tr>
	       <tr><td height="5" >&nbsp;</td></tr>	
	       <c:if test = "${not empty requestScope.documentExists && requestScope.documentExists eq 'notexists'}">
				<tr><td class = "message">&nbsp;&nbsp;Some of the documents attached with this po are not present or has been deleted.</td></tr>
		   </c:if>
	</table>
</c:if>



<logic:notEqual name = "EmailForm"  property = "type" value = "MSA">
	<logic:notEqual name = "EmailForm"  property = "type" value = "Appendix">
		<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 	<tr>
    			<td valign="top" width = "100%">   				
	    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
				       
						<logic:equal name = "EmailForm" property = "type" value = "Invoice Request">
							 <tr>
								<td class="headerrow" height="19" width = "100%"><a href = "InvoiceJobDetail.do?id=<bean:write name = "EmailForm" property = "id2" />&type=<bean:write name = "EmailForm" property = "invtype1" />&rdofilter=<bean:write name = "EmailForm" property = "invtype2" />" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.Email.invoicemanager"/></a></td>		
							</tr>
							<tr>
				    			<td background="images/content_head_04.jpg" height="21" width = "100%">
						         	<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><span class="currentSelected"><bean:message bundle = "pm" key = "common.Email.Header"/><bean:write name="EmailForm" property="name"/>&nbsp;<bean:write name="EmailForm" property="typename"/><bean:write name="EmailForm" property="type"/></span></div>
								</td>
				    		</tr>
						</logic:equal>	
		 				
		 				<logic:equal name ="EmailForm" property ="type1" value ="Invoice Request">
			   			<tr>
			    			<td colspan = "4" class = "Nlabeleboldwhite" height = "30"><h2>  
			    				<bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.sendinvoicerequest"/>
			    			</h2></td>
			   			</tr> 
			   			</logic:equal>
			   			<logic:equal name ="EmailForm" property ="type1" value ="Send POWO">
			   				<logic:equal name ="EmailForm" property ="order_type" value ="po">
						   			<tr>
						    			<td colspan = "4" height = "30"><h2>  
						    				<bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.po"/>
						    			</h2></td>
						   			</tr>
			   				</logic:equal>
			   			</logic:equal>
			   			<logic:equal name ="EmailForm" property ="type1" value ="Send POWO">
			   				<logic:equal name ="EmailForm" property ="order_type" value ="wo">
						   			<tr>
						    			<td colspan = "4" height = "30"><h2>  
						    				<bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.wo"/>
						    				</h2></td>
						   		</tr> 
			   				</logic:equal>
			   			</logic:equal>
			   			<logic:equal name ="EmailForm" property ="type1" value ="Send POWO">
			   				<logic:equal name ="EmailForm" property ="order_type" value ="powo">
						   			<tr>
						    			<td colspan = "4" height = "30"><h2> 
						    				<bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.powo"/>
						    			</h2></td>
						   			</tr> 
							</logic:equal>
						</logic:equal>
		 				<logic:equal name ="EmailForm" property ="type1" value ="Manage Status Report">
					    				<logic:equal name = "EmailForm" property = "type" value = "prm_job">
					    				<tr>
							    			<td colspan = "4" height = "30">
						    				<h2>
						    					<bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.statusreport"/> For 
						    					<bean:message bundle = "pm" key = "common.Email.status2"/>:<bean:write name="EmailForm" property="name"/>
						    				</h2>
						    				</td>
						    			</tr>
					    				</logic:equal>
					    				<logic:equal name = "EmailForm" property = "type" value = "prm_activity">
					    				<tr>
							    			<td colspan = "4" height = "30">
							    				<h2>
						    						<bean:message bundle = "pm" key = "common.Email.Header"/> <bean:message bundle = "pm" key = "common.Email.statusreport"/> For 
						    						<bean:message bundle = "pm" key = "common.Email.status3"/>:<bean:write name="EmailForm" property="name"/>
						    					</h2>
					    					</td>
					    				</tr>
					    				</logic:equal>
					   </logic:equal>	
		      		</table>
   				</td>
		 	</tr>
		</table>
	</logic:notEqual>
</logic:notEqual>

<logic:equal name = "EmailForm" property = "type" value = "MSA">
	<%@ include  file="/MSAMenuScript.inc" %>
</logic:equal>
<logic:equal name = "EmailForm" property = "type" value = "Appendix">
	<%@ include  file="/AppendixMenuScript.inc" %>
</logic:equal>

							<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present> 	
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
<table>
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
		    <td>
		   		<table border = "0" cellspacing = "1" cellpadding = "1" width = "55%"> 
		   		<!-- Start :Added By Amit For Email Alert -->

				<logic:present name = "emailflag" scope = "request">
    				<tr> 

						<td colspan = "4" class = "message" height = "30">
					    		<logic:notEqual name = "emailflag" value = "0">
								<bean:message bundle = "pm" key = "msa.detail.mailsentfailurewarning"/>
				    		</logic:notEqual>
				    	</td>
			    	</tr>
				</logic:present>  
		    				   		
		   		<!-- End :Added By Amit For Email Alert -->
		   			<logic:equal name ="EmailForm" property ="type" value ="webTicket">
			   			<tr> 
			    			<td colspan = "4" class = "Nlabeleboldwhite" height = "30">
			    				<bean:message bundle = "pm" key = "common.Email.Header"/> Email
			    			</td>
			   			</tr> 
		   			</logic:equal>

		     <% boolean p = true; 
		   	String label = "common.Email.CustomerRecipients"; %>
		   	<logic:equal name ="EmailForm" property ="type" value ="Send POWO">
		   	<% label = "common.Email.PartnerRecipients"; %>
		   </logic:equal>
		   
			<logic:notEqual name ="EmailForm" property ="type" value ="PRMAppendix">
			<logic:notEqual name ="EmailForm" property ="type" value ="Appendix"> 
				    <logic:present name = "EmailForm" property = "custPOC">
					    <logic:iterate id = "custpoc" name = "EmailForm" property = "custPOC">
					    <bean:define id = "cpoc" name = "custpoc" property = "name" type = "java.lang.String" />
							<bean:define id = "cpocemail" name = "custpoc" property = "email" type = "java.lang.String" />
				    <% 
						if(   poclist.intValue() > 1 ){
							valstr = "javascript: uncheck( this , '"+i+"' );"; 
						}else{
							valstr = "javascript: uncheck( this , null );"; 
						}
						
					if( !( cpoc.equals( "+" ) ||  cpoc.equals( "-" ) ) )
					{
						if ( p ) {
						%>
					  
					   <tr><td>&nbsp;</td>
			   				<td class = "colDark"><bean:message bundle = "pm" key = "<%= label %>"/>
			   				</td>
			    			<% if ( flagto ){ %>
			    			<td class = "rowDark"  align="center" width="8%"><bean:message bundle = "pm" key = "common.Email.to"/></td>
			    			<td class = "rowDark"  align="center" width="8%"><bean:message bundle = "pm" key = "common.Email.cc"/></td>
			    			<%} else{%>
			    			<td class = "rowDark"  align="center" width="8%"></td>
			    			<td class = "rowDark"  align="center" width="8%"><bean:message bundle = "pm" key = "common.Email.cc"/></td>
			    			<%} %>
			   				<% if( !flagto ){ %>
			   				<td class = "rowDark"  align="center" width="8%"><bean:message bundle = "pm" key = "common.Email.bcc"/></td>
			   				<%} else{%>
			   				<td class = "rowDark"  align="center" width="8%"></td>
			    			<%} %>
			    			<td class = "rowDark" colspan="2" width="60%"></td>
			   			</tr>
							<%
							p = false;
							} 
					}
							%>   
			  	   
						<%	if ( !( cpoc.equals( "+" ) ||  cpoc.equals( "-" ) ) ) {  %>
						    <tr><td>&nbsp;</td>
							    <td class = "colDark" nowrap="nowrap"><bean:write name = "custpoc" property = "name"/></td>
							    	<% if( flagto ) {%>
							    	<td class = "rowLight" align="center"><html:checkbox property = "mailto" value = "<%= cpocemail %>" onclick = "<%= valstr %>"></html:checkbox></td>
									<td class = "rowLight" align="center"><html:checkbox property = "mailcc" value = "<%= cpocemail %>" onclick = "<%= valstr %>"></html:checkbox></td>
									<%}else{ %>
										<td class = "rowLight" align="center"></td>
										<td class = "rowLight" align="center"><html:checkbox property = "mailcc" value = "<%= cpocemail %>" onclick = "<%= valstr %>"></html:checkbox></td>
										<html:hidden property = "mailto" />
									<%} %>
									<% if( !flagto ) {%>
							    	<td class = "rowLight" align="center"><html:checkbox property = "mailbcc" value = "<%= cpocemail %>" onclick = "<%= valstr %>"></html:checkbox></td>
									<%}else{ %>
										<td class = "colLight"></td>
										<html:hidden property = "mailbcc"  />
									<%} %>
									<td class = "colLight" colspan="2" width="60%"></td>
						   <% 
						   }
						   else
						   {
							                            
								p = true;
								if(  cpoc.equals( "-" ) )
								{
									label = "common.Email.cnsrecipients";
									 flagto = false;
								}
								else
								{
									label = "common.Email.CustomerRecipients";     //done for not To for CNS recipients
									 flagto = true;
								}
						  %>
						   <html:hidden property = "mailto" />
						   <html:hidden property = "mailcc" />
						   <html:hidden property = "mailbcc" />
						   <%}
						
							
						   i++; 
  						   %>
							</tr>
					    </logic:iterate>
					    </logic:present>
				</logic:notEqual>	
				</logic:notEqual>
						<!-- by Avinash for Ntextbox Additional To Recipients start -->
						
				<!-- This option for only Appendix Dashboard:start -->
				<%if(Type.equals("Appendix") || Type.equals("PRMAppendix")){ %>
				<%boolean c = true;%>
				 <tr>
				 			<td>&nbsp;</td>
			   				<td class = "colDark"><bean:message bundle = "pm" key = "common.Email.CustomerRecipients"/></td>
			    			<td class = "rowDark"  align="center" width="8%"><bean:message bundle = "pm" key = "common.Email.to"/></td>
			    			<td class = "rowDark"  align="center" width="8%"><bean:message bundle = "pm" key = "common.Email.cc"/></td>
			    			<td class = "rowDark" align="center"></td>
			    			<td class = "rowDark" colspan="2" width="60%"></td>
			   	</tr>
				    <logic:present name = "EmailForm" property = "custPOC">
					    <logic:iterate id = "custpoc" name = "EmailForm" property = "custPOC">
					    <bean:define id = "cpoc" name = "custpoc" property = "name" type = "java.lang.String" />
							<bean:define id = "cpocemail" name = "custpoc" property = "email" type = "java.lang.String" />
				    <% 
						if(   poclist.intValue() > 1 ){
							valstr = "javascript: uncheck( this , '"+i+"' );"; 
						}else{
							valstr = "javascript: uncheck( this , null );"; 
						}
					%>
					
					<%
				   	if( cpoc.equals( "-" ) && i==1){
					   	if(c){
			  	    %>
			  	    <tr><td>&nbsp;</td>
		    			<td class = "colLight" colspan="6">&nbsp;&nbsp;<FONT class="messageResource"><bean:message bundle = "pm" key = "common.Email.nocontactdefine"/></FONT></td>
			   			</tr>
			  	   <%}} %>
			  	   
					<%
					if( (cpoc.equals( "-" ) ) )
					{
						if ( p ) {
						%>
					  
					   <tr><td>&nbsp;</td>
			   				<td class = "colDark"><bean:message bundle = "pm" key = "common.Email.cnsrecipients"/></td>
			    			<td class = "rowDark"  align="center" width="8%"></td>
			    			<td class = "rowDark"  align="center" width="8%"><bean:message bundle = "pm" key = "common.Email.cc"/></td>
			   				<td class = "rowDark"  align="center" width="8%"><bean:message bundle = "pm" key = "common.Email.bcc"/></td>
			    			<td class = "rowDark" colspan="2" width="60%"></td>
			   			</tr>
							<%
							p = false;
							} 
					}
					%>   
						<%	if ( !( cpoc.equals( "+" ) ||  cpoc.equals( "-" ) ) ) {%>
						    <tr><td>&nbsp;</td>
							    <td class = "colDark" nowrap="nowrap"><bean:write name = "custpoc" property = "name"/></td>
							    	<% if( flagto ) {%>
							    	<td class = "rowLight" align="center"><html:checkbox property = "mailto" value = "<%= cpocemail %>" onclick = "<%= valstr %>"></html:checkbox></td>
									<td class = "rowLight" align="center"><html:checkbox property = "mailcc" value = "<%= cpocemail %>" onclick = "<%= valstr %>"></html:checkbox></td>
									<%}else{ %>
										<td class = "rowLight" align="center"></td>
										<td class = "rowLight" align="center"><html:checkbox property = "mailcc" value = "<%= cpocemail %>" onclick = "<%= valstr %>"></html:checkbox></td>
										<html:hidden property = "mailto" />
									<%} %>
									<% if( !flagto ) {%>
							    	<td class = "rowLight" align="center"><html:checkbox property = "mailbcc" value = "<%= cpocemail %>" onclick = "<%= valstr %>"></html:checkbox></td>
									<%}else{ %>
										<td class = "colLight"></td>
										<html:hidden property = "mailbcc"  />
									<%} %>
									<td class = "colLight" colspan="2"></td>
						   <%
						   c = false; 
						   }
						   else
						   {
							                            
								p = true;
								if(  cpoc.equals( "-" ) )
								{
									label = "common.Email.cnsrecipients";
									 flagto = false;
								}
								else
								{
									label = "common.Email.CustomerRecipients";     //done for not To for CNS recipients
									 flagto = true;
								}
						  %>
						   <html:hidden property = "mailto" />
						   <html:hidden property = "mailcc" />
						   <html:hidden property = "mailbcc" />
						   <%}
						
							
						   i++; 
  						   %>
							</tr>
					    </logic:iterate>
					    </logic:present>
					    
						<%} %>		
				<!-- This option for only Appendix Dashboard:end -->
						
						<td>&nbsp;</td>
		   				<td class = "colDark"><bean:message bundle = "pm" key = "common.Email.additionaltorecipients"/>
		   				</td>
		   				
		    			<td class = "colLight"  colspan="5">
		    				 <html:text property = "textmailaddto"  styleClass = "text" size="50"/>
		    			</td>
		    			
		   				</tr>


						<!-- by hamid for Ntextbox cc start -->
						<tr><td>&nbsp;</td>
		   				<td class = "colDark"><bean:message bundle = "pm" key = "common.Email.textcc"/>
		   				</td>
		   				
		    			<td class = "colLight" colspan="5">
		    				 <html:text property = "textmailcc"  styleClass = "text" size="50"/>
		    			</td>
		    			
		   				</tr>
		   				
		   				<!-- by hamid for Ntextbox cc end -->	
						
						<tr><td>&nbsp;</td>
		   				<td class = "colDark"><bean:message bundle = "pm" key = "common.Email.From"/>
		   				</td>
		   				
		    			<td class = "colLight" colspan="5">
		    				 <html:text property = "fromName"  styleClass = "text" size="50"/>
		    			</td>
		   			</tr>	    
					</table>
				</td></tr>
				<tr>
					<td>
					    <table border = "0" cellspacing = "1" cellpadding = "1" width = "55%"> 					  
					
							
						<logic:equal name ="EmailForm" property ="type" value ="webTicket">
							<html:hidden property = "fileformat" />
						</logic:equal>
						<logic:notEqual name ="EmailForm" property ="type" value ="webTicket">
								<tr> <td>&nbsp;</td>
								    <td class = "colDark"><bean:message bundle=  "pm" key = "common.Email.FileFormat"/>
								    </td>
								    <td class = "colLight">
								    <%boolean check = false;%>
									<logic:equal name ="EmailForm" property ="check_for_fileupload" value ="U">
										<%check=true;%>
									</logic:equal>								    
									<logic:equal name ="EmailForm" property ="type1" value ="Invoice Request">
										<%check=true;%>
									</logic:equal>								    
								    <logic:equal name ="EmailForm" property ="type1" value ="Send POWO">
										<%check=true;%>
									</logic:equal>								    
								    <%if(check){%>
										    <html:radio property = "fileformat" value = "PDF" disabled = "true"/>&nbsp;&nbsp;<bean:message bundle = "pm" key = "common.Email.Pdf"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<%-- 		<html:radio property = "fileformat" value = "RTF" disabled = "true"/>&nbsp;&nbsp;<bean:message bundle = "pm" key = "common.Email.Rtf"/>   --%>
								    	<%}else{ %>
								    		<html:radio property = "fileformat" value = "PDF" />&nbsp;&nbsp;<bean:message bundle = "pm" key = "common.Email.Pdf"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<%-- 	<html:radio property = "fileformat" value = "RTF" />&nbsp;&nbsp;<bean:message bundle = "pm" key = "common.Email.Rtf"/>  --%>
								    	<% }%>
								    </td> 
							   </tr>
						  </logic:notEqual>
						   <!-- by hamid for attachment start -->
						   
						   <logic:equal name= "EmailForm" property ="type1" value ="Send POWO">
						   <!-- For PO/WO only come from DocM: Strat -->
						   <jsp:include page = '/common/IncludePOWOEmail.jsp'>
								<jsp:param name = 'onlyEmail' value = '<%=(String)request.getParameter("onlyEmail")%>' />
							</jsp:include>
						   
						   <!-- For PO/WO only come from DocM: End -->
						   </logic:equal>
						   
						  <tr> <td>&nbsp;</td>
							    <td class = "colDark"><bean:message bundle = "pm" key = "common.Email.attachment1"/>
							    </td>
							    <td class = "colLight">
								   <html:file property = "attachfilename" styleClass = "Nbutton" />
							    </td> 	
						   </tr>
						   
						   <!-- Start :Added By Amit -->
						   <tr><td>&nbsp;</td>
						   <td class = "colDark"><bean:message bundle = "pm" key = "common.Email.attachment2"/>
							    </td>
							    <td class = "colLight">
								   <html:file property = "attachfilename1" styleClass = "Nbutton" />
							    </td> 
							</tr> 
							<tr><td>&nbsp;</td>
						   <td class = "colDark"><bean:message bundle = "pm" key = "common.Email.attachment3"/>
							    </td>
							    <td class = "colLight">
								   <html:file property = "attachfilename2" styleClass = "Nbutton" />
							    </td> 
							</tr>  
							<!-- for msa attachment from appendix dashboard:start -->
							<%if(Type.equals("Appendix") || Type.equals("PRMAppendix")){ %>
								<logic:notEqual name ="EmailForm" property ="jobtype" value ="Addendum">  
								 <logic:notEqual name ="EmailForm" property ="fromPage" value ="NetMedX">
									<tr>
										<td>&nbsp;</td>
									    <td class = "colDarkN"><bean:message bundle = "pm" key = "common.Email.includemsa"/></td>
									    <td class = "colLight"><html:checkbox name="EmailForm" property="includeMsa" /></td> 
									</tr>   
								</logic:notEqual>
							  </logic:notEqual>
						 <%} %>
						 <!-- for msa attachment from appendix dashboard:end -->						
							
						   <!-- End : Added by amit -->
						   <!-- by hamid for attachment end -->
					   
						   <tr> <td>&nbsp;</td>
							    <td class = "colDark"><bean:message bundle = "pm" key = "common.Email.subject"/></b>
							    </td>
							    <td class = "colLight">
								    <html:text property = "subject" size = "45" styleClass = "text"/>
							    </td> 	
						   </tr>

					<c:if test="${requestScope.Type eq 'Send POWO'}">
						      <tr> <td>&nbsp;</td>
								    <td class = "colDark">Attached Documents</b>
								    </td>
								    <td class = "colLight" >
									    <table border="0" cellspacing="0" cellpadding="0" width="100%">
										    	<c:forEach items="${requestScope.docDetailPageList}" var="doc" varStatus="index">
												<c:if test="${index.first}">
													<tr id="ListTab" valign="top">
														<td >&nbsp;</td>
											     		<td class = "dbvaluesmallFontBold" align="center">PO</td>
											           	<td class = "dbvaluesmallFontBold" align="center">WO</td>
									         		</tr>
												</c:if>
								        		<%--<input type="hidden" name="document_id" value="<c:out value="${doc[3]}"/>"/>--%>
												<c:if test="${doc[1] eq 'Approved'}">
													<tr >
														<td class = "Nhypereven" id="title" style="padding-left: 0px;"> <c:out value="${doc[0]}"/>, <c:out value="${doc[2]}"/></td>
														<td align="center" class = "Ntexteleftalignnowrap" >
														<input type="checkbox"  class="chkbx"  name="in_wi_po_detail" disabled="disabled" <c:if test="${doc[3] eq '1'}">checked</c:if> /></td>
														<td  align="center" class = "Ntexteleftalignnowrap" >
														<input type="checkbox" class="chkbx" name="in_wi_wo_detail" disabled="disabled" <c:if test="${doc[4] eq '1'}">checked</c:if> /></td>
													</tr>
												</c:if>
									  		</c:forEach>
									  	</table>
								  </td> 	
							   </tr>
						  </c:if>
						  
						   <tr>
						   <td>&nbsp;</td>
						   <td class = "colDark">Content</b></td>
						    	<td class = "colLight" >
						    		<html:textarea property = "content" rows = "12" cols = "70"  styleClass = "textarea"/>
						   		</td>
						   </tr>
						   <tr>
						   <td>&nbsp;</td>
						   <td class = "colDark">Auto Inserted Content</b></td>
						    	<td class = "colLight" >
						    		<html:textarea property = "contentFromDB" rows = "2" cols = "70"  styleClass = "textarea" disabled="true"/>						    		
						   		</td>
						   </tr>
						   <tr>
						   <td>&nbsp;</td>
						   <td class = "colDark">Signatures</b></td>						   
						    	<td class = "colLight" >
						    		<html:textarea property = "contentSign" rows = "7" cols = "70"  styleClass = "textarea" />
						   		</td>
						   </tr>
					   
				   
					    <tr><td>&nbsp;</td>
							<td colspan = "2" class = "colLight" align="center">
								<html:submit property = "send" styleClass = "button_c" onclick = "return validate();"><bean:message bundle = "pm" key = "common.Email.send" /></html:submit>
								<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "pm" key = "common.Email.cancel"/></html:reset>
								<logic:equal name = "EmailForm" property = "type" value = "prm_appendix">
		    						<html:button property="back" styleClass="button_c" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
		    					
		    					</logic:equal>
		    		
					    		<logic:equal name = "EmailForm" property = "type" value = "PRMAppendix">
					    			<html:button property="back" styleClass="button_c" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					    			
					    		</logic:equal>
					    		
					    		<logic:equal name = "EmailForm" property = "type" value = "prm_job">
					    			<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					    			
					    		</logic:equal>
					    		
					    		<logic:equal name = "EmailForm" property = "type" value = "Send POWO">
					    			<html:button property="back" styleClass="button_c" onclick = "return Backactionpowo();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					    			
					    		</logic:equal>
					    		
					    		<logic:equal name = "EmailForm" property = "type" value = "prm_activity">
					    			<html:button property="back" styleClass="button_c" onclick = "return Backactionactivity();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					    			
					    		</logic:equal>
							</td>
					    </tr>
		   		</table>
			</td>
		</tr> 
	</table>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
</html:form>
</body>

<script>

function uncheck( mailarray , index )
{
	if( index != null )
	{
		if( mailarray.name == 'mailto' )
		{
			document.forms[0].mailcc[index].checked = false;
			document.forms[0].mailbcc[index].checked = false;
		}
		
		if( mailarray.name == 'mailcc' )
		{
			document.forms[0].mailto[index].checked = false;
			document.forms[0].mailbcc[index].checked = false;
		}
		
		if( mailarray.name == 'mailbcc' )
		{
			document.forms[0].mailto[index].checked = false;
			document.forms[0].mailcc[index].checked = false;
			
		}
	}
	else
	{
		if( mailarray.name == 'mailto' )
		{
			document.forms[0].mailcc.checked = false;
			document.forms[0].mailbcc.checked = false;
		}
		
		if( mailarray.name == 'mailcc' )
		{
			document.forms[0].mailto.checked = false;
			document.forms[0].mailbcc.checked = false;
		}
		
		if( mailarray.name == 'mailbcc' )
		{
			document.forms[0].mailto.checked = false;
			document.forms[0].mailcc.checked = false;
		}
	}	
}

function Backactionappendix()
{
	
	document.forms[0].action = "AppendixHeader.do?function=view&appendixid=<bean:write name = "EmailForm" property = "id2" />&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
function Backactionjob()
{
	document.forms[0].action = "JobDashboardAction.do?jobid=<bean:write name = "EmailForm" property = "id1" />&isClicked=";
	document.forms[0].submit();
	return true;
}

function Backactionactivity()
{
	document.forms[0].action = "ActivityHeader.do?function=view&activityid=<bean:write name = "EmailForm" property = "id1" />";
	document.forms[0].submit();
	return true;
}

function Backactionpowo()
{
	document.forms[0].action = "POWOAction.do?jobid=<bean:write name = "EmailForm" property = "id2" />";
	document.forms[0].submit();
	return true;
}






function trimFields() 
{
	var field = document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text') 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}

function validate()
{
	trimFields();
	if(!chkEmail())
	{
		return false;
	}
	try{enableCheck();}catch(e){}
	document.forms[0].action="EmailAction.do?Type=<bean:write name = 'EmailForm' property = 'type' />&Type1=<bean:write name = 'EmailForm' property = 'type1' />";
//	document.forms[0].submit();
	//return true;

}

function chkEmail()
{
	email=document.forms[0].textmailcc.value;
	
	if(email!=null)
	{
		if(email!="")
		{
			list=email.split(';');
			for(i=0;i<list.length;i++)
			{ 
				//list[i]=trimBetweenString(trim(list[i]));
				if(!isEmail(list[i]))	
				{	
					alert("<bean:message bundle = "pm" key = "common.Email.invalidemail"/>");	
					document.forms[0].textmailcc.focus();
					return false;
				}
			}
		}
		
	}
	
	return true;
}


</script>
<script>
function OnLoad()
{
	if(document.forms[0].type.value =='MSA' || document.forms[0].type.value =='Appendix'){
	
			<%if(request.getAttribute("opendiv") != null){%>
				document.getElementById("filter").style.visibility="visible";
			<%}%>
	}
}

function show()
{
	
	document.forms[0].go.value="true";
	
	if(document.forms[0].type.value=='MSA')
		document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<bean:write name = 'EmailForm' property = 'id1' />&clickShow=true";
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action = "EmailAction.do?Type=Appendix&MSA_Id=<bean:write name = 'EmailForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'EmailForm' property = 'id2' />&clickShow=true";
		
		
	document.forms[0].submit();
	return true;
}

function initialState()
{
document.forms[0].go.value='';
	if(document.forms[0].type.value=='MSA') {
		document.forms[0].action="EmailAction.do?Type=MSA&MSA_Id=<bean:write name = 'EmailForm' property = 'id1' />&home=home&clickShow=true";
	}
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action="EmailAction.do?Type=Appendix&MSA_Id=<bean:write name = 'EmailForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'EmailForm' property = 'id2' />&firstCall=true&home=home&clickShow=true";
	
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{
	if(document.forms[0].type.value=='MSA')
{
	if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(k=2;k<document.forms[0].selectedOwners.length;k++)
			{
				document.forms[0].selectedOwners[k].checked=false;
			}
		}
		else
		{
			for(k=2;k<document.forms[0].selectedOwners.length;k++)
			{
				if(document.forms[0].selectedOwners[k].checked) {
				 	document.forms[0].selectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{	
			for(i=0;i<document.forms[0].selectedOwners.length;i++)
			{
					if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
					{
						document.forms[0].otherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="EmailAction.do?Type=MSA&MSA_Id=<bean:write name = 'EmailForm' property = 'id1' />&opendiv=0";
		 				document.forms[0].submit();
					}
			}
	}
	}
	if(document.forms[0].type.value=='Appendix'){
			if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
					{
					if(checkboxvalue == '%'){
					for(k=2;k<document.forms[0].appendixSelectedOwners.length;k++)
					{
						document.forms[0].appendixSelectedOwners[k].checked=false;
					}
					}
			else
			{
				for(k=2;k<document.forms[0].appendixSelectedOwners.length;k++)
				{
					if(document.forms[0].appendixSelectedOwners[k].checked) {
					 	document.forms[0].appendixSelectedOwners[1].checked = false;
					 	break;
					}
				}		
			}
			}		
	
	else
	{
			for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
			{
					if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
					{
						document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="EmailAction.do?Type=Appendix&MSA_Id=<bean:write name = 'EmailForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'EmailForm' property = 'id2' />&opendiv=0";
		 				document.forms[0].submit();
		
					}
			}
	}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="EmailAction.do?Type=MSA&MSA_Id=<bean:write name = 'EmailForm' property = 'id1' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<bean:write name = 'EmailForm' property = 'id1' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="EmailAction.do?Type=Appendix&MSA_Id=<bean:write name = 'EmailForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'EmailForm' property = 'id2' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
document.forms[0].go.value="true";
//alert('in month_Appendix()');
document.forms[0].action="EmailAction.do?Type=Appendix&MSA_Id=<bean:write name = 'EmailForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'EmailForm' property = 'id2' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}




</script>
<script>

function changeFilter()
{
	document.forms[0].action="EmailAction.do?Type=<bean:write name = 'EmailForm' property = 'type' />&Type1=<bean:write name = 'EmailForm' property = 'type1' />&MSA_Id=<bean:write name = 'EmailForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'EmailForm' property = 'id2' />&id=<bean:write name = 'EmailForm' property = 'id2' />&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="EmailAction.do?Type=<bean:write name = 'EmailForm' property = 'type' />&Type1=<bean:write name = 'EmailForm' property = 'type1' />&MSA_Id=<bean:write name = 'EmailForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'EmailForm' property = 'id2' />&id=<bean:write name = 'EmailForm' property = 'id2' />&resetList=something&tabId="+document.forms[0].tabId.value;
	document.forms[0].submit();
	return true;

}

</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].id2.value;	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>




</html:html>
   	
   
  
