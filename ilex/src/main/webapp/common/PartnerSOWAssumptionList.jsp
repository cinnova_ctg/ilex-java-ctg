<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%
String jobid = "";

int listSize = 0;
String Appendix_Id ="";

if(request.getAttribute("Appendix_Id")!=null)
	Appendix_Id= request.getAttribute("Appendix_Id").toString();	
if(request.getAttribute("partnerSOWassumptionListSize")!=null) {
	listSize = Integer.parseInt(request.getAttribute("partnerSOWassumptionListSize").toString());
}
%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkowner = null;
String checkstatus = null;
int rowHeight = 120;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


%>
<html:html>
<HEAD>
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<%@ include  file="/NMenu.inc" %>
	<%@ include  file="/JobMenu.inc" %>
	<%@ include  file="/AjaxOwners.inc" %>
	<%@ include  file="/DashboardStatusScript.inc" %>
</HEAD>


<script>
function toggleMenuSection(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
	}
	else {
		thisImage.src = "images/Expand.gif";
	}
}
function toggleDiv( divName ) {
	 var tcookie = document.cookie;
     var st = tcookie.indexOf ( "P1Db" );
     var cookievalue = "";
	  if( st != -1 )
	  {
	      st = st + 3;
	      st = tcookie.indexOf ( "=" , st );
	      en = tcookie.indexOf ( ";" , st );
		  if ( en > st )
	        {
	          cookievalue = tcookie.substring( st+1 , en ); //div's that is reopen
	        }
	 }
	 
	var id = document.getElementById( divName );
	
	if( id.style.display == "none" ) 
		{
				id.style.display = "block";
				//add it to the existing cookie opendiv paraneter
			
				if ( cookievalue.length > 0 )
					cookievalue = cookievalue + ',' + divName;
				else
					cookievalue = divName;
					
				if( cookievalue != "" )
				{ 
					// Save as cookie
					document.cookie = "PDb="+ cookievalue+" ; ";
	            }
		}

	else
		{
			id.style.display = "none";	
			var newcookievalue = '';
			//remove this div from the opendiv cookie
			if ( cookievalue.length > 0 ) {
				var array = cookievalue.split( ',' );
				//if (array.length>1)
				if ( array.length > 0 ) {
					for( i = 0; i < array.length;i++ ) {
						if( array[i] == divName ) { }
						else {
							if( newcookievalue.length > 0 ) {
								newcookievalue = newcookievalue + ',' + array[i];
							} else {
								newcookievalue = array[i];	
							}
						}
					}
				}
				//if(newcookievalue!="")
				//{ // Save as cookie
			 		document.cookie = "PDb="+ newcookievalue+" ; ";
			 	//}
			}
		}
}


function toggleMenuSectionView( unique ) {
	var divid = "div_" + unique;
	thisDiv = document.getElementById( divid );
	try{
	if ( thisDiv == null ) { return ; }
	}catch( e ){
	}
	action = "toggleType = toggleDivView( 'div_" + unique + "' );";
	eval( action );
	action = "thisImage = document.getElementById( 'img_" + unique + "' );";
	eval( action );
	if ( document.getElementById( 'div_' + unique ).offsetHeight > 0 ) {
		thisImage.src = "images/Collapse.gif";
	}
	else {
		thisImage.src = "images/Expand.gif";
	}
}

function toggleDivView(divName) {
	 var tableRow = document.getElementsByTagName( 'tr' );
	if ( tableRow.length == 0 ) { return; }
	for ( var k = 0; k < tableRow.length; k++ ) {
		if ( tableRow[k].getAttributeNode( 'id' ).value == divName ) {
			if ( tableRow[k] ) {
				if ( tableRow[k].style.display == "none" ) {
					tableRow[k].style.display = "block";
				}
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert( errorString );
			}
		}
	
	}
}

// Added Start:By Amit This will be called on each href that opens other form
function createCookie()
{
 var tableRow = document.getElementsByTagName( 'tr' );
  var openDiv = "";
	if ( tableRow.length == 0 ) { return; }
	for ( var k = 0; k < tableRow.length; k++ ) {
		if( ( tableRow[k].getAttributeNode( 'id' ).value != "" )&&( tableRow[k].getAttributeNode( 'id' ).value != "account" ) )
		{
			if( tableRow[k].style.display == "block" ){
			openDiv = openDiv + tableRow[k].getAttributeNode( 'id' ).value + ",";
			}
		}
	}
		if(openDiv!="")
		{ // Save as cookie
		 document.cookie = "PDb="+ openDiv+" ; ";
		 }
}

function createCookie(lastDivForFocus)
{
 		document.cookie = "P1Db="+ lastDivForFocus+" ; ";
}

//following function would be called on OnLoad
function CookieGetOpenDiv()
{
  var tcookie = document.cookie;
  var unique;
  var st = tcookie.indexOf ("PDb");
  var cookievalue ="";
  if(st!= -1)
  {
      st = st+3;
      st = tcookie.indexOf ("=", st);
      en = tcookie.indexOf (";", st);

	  if (en > st)
        {
          cookievalue = tcookie.substring(st+1, en);
        }
       if(cookievalue.length>0) 
       {
		var opendiv = cookievalue.split(',');
		if(opendiv.length>0)
		{
		  for (var j = 0; j < opendiv.length; j++)
		   {
		   		if(eval("document.getElementById('"+opendiv[j]+"')")!=null)	//Added This if condition by amit
		   		{
		  				eval("document.getElementById('"+opendiv[j]+"')").style.display = "block";
						unique = opendiv[j].substring(opendiv[j].indexOf("_")+1,opendiv[j].length);
						action = "thisImage = document.getElementById('img_" + unique +"');";
						eval(action);
					if (document.getElementById('div_' + unique).offsetHeight > 0)
					{
							thisImage.src = "images/Collapse.gif";
		   			}
					else 
					{
							thisImage.src = "images/Expand.gif";
					}
				}
		  }
		}
	}
  }
	  openDiv = "";
	  //Save as cookie
	  document.cookie = "P1Db="+ openDiv+" ; "; 
	  //Start:Added For Focus		
	  var st1 = tcookie.indexOf ("P1Db");
	  var cookievalue1 ="";
	  if(st1!= -1)  {
	      st1 = st1+4;
	      st1 = tcookie.indexOf ("=", st1);
	      en = tcookie.indexOf (";", st1);
		  if (en > st1) {
	          cookievalue1 = tcookie.substring(st1+1, en);
	        }
		}
		 if(cookievalue1!="") {  
		  		if(document.getElementById(cookievalue1)!=null) {
		 	      document.getElementById(cookievalue1).focus();
		    	}
		}
		document.cookie = "P1Db="+ openDiv+" ; ";
}
</script>
<body onLoad = "MM_preloadImages( 'images/Expand.gif' , 'images/Collapse.gif' );leftAdjLayers();">
<html:form action = "PartnerSOWAssumptionList"> 
<html:hidden property = "fromId" />
<html:hidden property = "fromType" />
<html:hidden property = "viewType" />
<html:hidden property = "netType" />
<html:hidden property = "viewJobType" />
<html:hidden property = "ownerId" />
<html:hidden property="jobOwnerOtherCheck"/> 

<bean:define id = "context" name = "PartnerSOWAssumptionForm" property = "context"/>
<logic:equal name ="PartnerSOWAssumptionForm" property="fromType" value="pm_act">
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
    				<tr>
			    	     <%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
						   	<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>  
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
					</tr>
					
					<tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "PartnerSOWAssumptionForm" property = "msaId"/>&Appendix_Id=<bean:write name = "PartnerSOWAssumptionForm" property = "appendixId"/>"><bean:write name = "PartnerSOWAssumptionForm" property = "msaName"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "PartnerSOWAssumptionForm" property = "appendixId"/>&Name=<bean:write name = "PartnerSOWAssumptionForm" property = "appendixName"/>&ref=<%=chkaddendum%>"><bean:write name = "PartnerSOWAssumptionForm" property = "appendixName"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
	 					<td colspan="7"><h1>&nbsp;&nbsp;&nbsp;Job <bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/>:&nbsp;<bean:write name="PartnerSOWAssumptionForm" property ="jobName"/></h1></td>
	 				</tr>
	      		</table>
   				</td>
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/month.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/week.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
					
		</table>
</logic:equal>
<logic:equal name ="PartnerSOWAssumptionForm" property="fromType" value="prm_act">
	<logic:notEqual name = "PartnerSOWAssumptionForm"  property = "fromPage" value = "NetMedX">
	<%@ include  file="/DashboardVariables.inc" %>
	<%@ include  file="/AppedixDashboardMenu.inc" %>
	<%@ include  file="/AppendixDashboardMenuScript.inc" %>
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	
	 <tr>
	    <td valign="top" width = "100%">
		      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		<div id="breadCrumb">&nbsp;</div>
				        </td>
			        <tr>
		 					<td colspan="4"><h2>&nbsp;&nbsp;&nbsp;&nbsp;<%= context%>:&nbsp;Partner&nbsp;<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/></h2></td>
		 				</tr>
		      </table>
	   </td>
	      
	    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
		<td valign="top" width="155">
	    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tr>
			          <td width="155" height="39"  class="headerrow" colspan="2">
				          <table width="100%" cellpadding="0" cellspacing="0" border="0">
				              <tr>
				                <td width="150">
				                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
					                   	<tr>
								            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
		  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
											<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
											<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
					                    </tr>
				                  	</table>
				                </td>
				              </tr>
				          </table>
			          </td>
			        </tr>
	        		<tr>
	          		<td height="21" background="images/content_head_04.jpg" width="140">
	          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
	              		<td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
		                			<div id="featured1">
			                			<div id="featured">
												<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
										</div>								
		                			</div>		
							<span>
									<div id="filter" class="divstyle">
		        			<table width="250" cellpadding="0" class="divtable">
		                    <tr>
			                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
			                          <tr>
	                           	 <td colspan="3" class="filtersCaption">Status View</td>
	                          </tr>
	                          <tr>	
	                          	  <td></td>
		                          	  <td class="tabNormalText">
											<logic:present name ="jobAppStatusList" scope="request">
										    	<logic:iterate id = "list" name = "jobAppStatusList"> 
										    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
										    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
	
													    	<%
												    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
															 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																	addStatusRow++;
																	addStatusSpace = true;
													    		}
													    	%>
													    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
											 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
														<bean:write name ="list" property = "statusid"/>
											 		</html:multibox>   
												 	<bean:write name ="list" property = "statusdesc"/><br> 
												 	<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present>		
											<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
											</table>	
										</td>
		    					</tr>
	                        </table></td>
	                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
	                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
	                          <tr>
	                            <td colspan="3" class="filtersCaption">User View</td>
	                          </tr>
	                          <tr>
						         <td class="tabNormalText">
						          <span id = "OwnerSpanId">
						   			<logic:present name ="jobOwnerList" scope="request">
								    	<logic:iterate id = "olist" name = "jobOwnerList">
								    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
								    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
									    		<% 
		
												checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
												if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
													addOwnerRow++;
													addOwnerSpace = true;
												}	
									    		if(addOwnerRow == 0) { %> 
										    		<br/>
										    		<span class="ownerSpan">
													&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
													<br/>								
												<% } %>
								    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
								    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
												<bean:write name ="olist" property = "ownerId"/>
									 		</html:multibox>   
											  <bean:write name ="olist" property = "ownerName"/>
											  <br/> 
										</logic:iterate>
										</span>
									</logic:present>
									</span>
							    </td>
	                          </tr>
	                         
	                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
	                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
	                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
	                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
	                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
	                              </table></td>
	                        </tr>
	  			</table>											
			</div>
		</span>
					</td>
		
	              </tr>
	            </table></td>
	        </tr>
	      </table>
	      </td>
	 </tr>
	 </table>
	</logic:notEqual>
	
	<logic:equal name = "PartnerSOWAssumptionForm"  property = "fromPage" value = "NetMedX">
	<%@ include  file="/DashboardVariables.inc" %>
	<%@ include  file="/NetMedXDashboardMenu.inc" %>
	<!--  Change Menu  -->
	
	<div id="menunav">
 
<ul>
  		<li>
        	<a class="drop" href="#"><span>Appendix</span></a>
        	<ul>
        		<li>
        			
          <a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        		<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			SiteManagement.do?function=view&amp;appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7&amp;initialCheck=true&amp;pageType=jobsites
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerId%>&amp;fromPage=Appendix">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			<a href="#"><span>Contract Document</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
        	
        		<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
        	
        	
        		
				<!-- <li><a href="ReportScheduleWindow.do?report_type=redline&typeid=3970&fromPage=NetMedX">Redline Report</a></li>
				<li><a href="report_type=detailsummary&typeid=3970&fromPage=NetMedX">Detailed Summary</a></li>
				<li><a href="ReportScheduleWindow.do?report_type=CSVSummary&typeid=3970&fromPage=NetMedX">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="ReportGeneration.do?firsttime=true">Update Report</a></li></ul></li></ul></li>
        	 -->
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>	
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>" style="width: 120px">Search</a>
  		</li>
  		
	</ul>
</li>

</div>
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tbody><tr>
    <td width="100%" valign="top">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		   <td colspan = "7" height="1"><h2>&nbsp;&nbsp;&nbsp;&nbsp;<%= context%>:&nbsp;Partner&nbsp;<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/></h2></td>      
	      </tbody></table>
   </td>
      
    <td width="35" valign="top"><img width="35" height="60" alt="" src="images/content_head_02.jpg" id="content_head_02"></td>
	<td width="155" valign="top">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tbody><tr>
		          <td width="155" height="39" colspan="2" class="headerrow">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tbody><tr>
			                <td width="150">
			                	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				                   	<tbody><tr>
							            <td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('II');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" title="Scheduled" id="Image10" alt="Month" src="http://localhost:8080/Ilex/images/Icon_Scheduled.gif"></a></td>
	  									<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IO');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" title="Scheduled OverDue" id="Image10" alt="Month" src="images/Icon_Schedule_Overdued.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('ION');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" title="OnSite" id="Image10" alt="Month" src="images/Icon_Onsite.gif"></a></td>
										<td><a class="imgstyle" href="#"><img width="31" height="30" border="0" onclick="setStatus('IOF');" onmouseout="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onmouseover="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" title="Offsite" id="Image10" alt="Month" src="images/Icon_Ofsite.gif"></a></td>
				                    </tr>
			                  	</tbody>
			                  	</table>
			                </td>
			              </tr>
			          </tbody></table>
		          </td>
		        </tr>
        		<tr>
          		<td width="140" height="21" background="images/content_head_04.jpg">
          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              		<tbody><tr>
              		<td width="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li id="closeNow" name="closeNow"><a onclick="ShowDiv();" title="Open View" href="javascript:void(0)">My Views <img border="0" onclick="ShowDiv();" title="Open View" src="images/showFilter.gif"></a><a class="imgstyle" href="javascript:void(0)"><img border="0" onclick="hideDiv();" title="Close View" src="images/offFilter2.gif"></a></li>
									</div>								
	                			</div>		
						<span>
								<div class="divstyle" id="filter" style="">
								<!--  code -->
								<table width="250" cellpadding="0" class="divtable">
		                    <tr>
			                      <td width="50%" valign="top">
			                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
			                          <tr>
	                           	 <td colspan="3" class="filtersCaption">Status View</td>
	                          </tr>
	                          <tr>	
	                          	  <td></td>
		                          	  <td class="tabNormalText">
											<logic:present name ="jobAppStatusList" scope="request">
										    	<logic:iterate id = "list" name = "jobAppStatusList"> 
										    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
										    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
	
													    	<%
												    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
															 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																	addStatusRow++;
																	addStatusSpace = true;
													    		}
													    	%>
													    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
											 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
														<bean:write name ="list" property = "statusid"/>
											 		</html:multibox>   
												 	<bean:write name ="list" property = "statusdesc"/><br> 
												 	<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present>		
											<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
											</table>	
										</td>
		    					</tr>
	                        </table>

			                      
			                      
			                      
			                     <%--  <table width="100%" cellpadding="0" cellspacing="0" border="0">
			                          <tr>
		                       	 <td colspan="3" class="filtersCaption">Status View</td>
		                    </tr>
		                    <tr>	
		                      	  <td></td>
		                          	  <td class="tabNormalText">
											<logic:present name ="jobAppStatusList" scope="request">
										    	<logic:iterate id = "list" name = "jobAppStatusList"> 
										    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
										    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
		
													    	<%
												    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
															 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																	addStatusRow++;
																	addStatusSpace = true;
													    		}
													    	%>
													    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
											 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
														<bean:write name ="list" property = "statusid"/>
											 		</html:multibox>   
												 	<bean:write name ="list" property = "statusdesc"/><br> 
												 	<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present>		
											<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
											</table>	
										</td>
		    			    </tr>
		                    </table> --%>
								<!--   -->
	        			<!-- <table width="250" cellpadding="0" class="divtable">
	                    <tbody><tr>
		                      <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		                          <tbody><tr>
                           	 <td class="filtersCaption" colspan="3">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										
									    	 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('AI');" value="AI" name="jobSelectedStatus">   
											 	Inwork<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IS');" value="IS" name="jobSelectedStatus">   
											 	To Be Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('II');" checked="checked" value="II" name="jobSelectedStatus">   
											 	Scheduled<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IO');" checked="checked" value="IO" name="jobSelectedStatus">   
											 	Scheduled Overdue<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('ION');" checked="checked" value="ION" name="jobSelectedStatus">   
											 	Onsite<br> 
											 	
											 
									    		
									    		

												    	
												    	&nbsp;&nbsp;&nbsp;&nbsp; 
										 		<input type="checkbox" onclick="javascript: checkingStatus('IOF');" checked="checked" value="IOF" name="jobSelectedStatus">   
											 	Offsite<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('F');" value="F" name="jobSelectedStatus">   
											 	Complete<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('O');" value="O" name="jobSelectedStatus">   
											 	Closed<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('H');" value="H" name="jobSelectedStatus">   
											 	Hold<br> 
											 	
											 
									    		
									    		

												    	
												    	 
										 		<input type="checkbox" onclick="javascript: checkingStatus('C');" value="C" name="jobSelectedStatus">   
											 	Cancelled<br> 
											 	
											
												
										<img width="100px" class="imagefilter" src="images/hrfilterLine.gif">
										<table cellpadding="0">
											<tbody><tr><td class="tabNormalText"><input type="radio" value="week" name="jobMonthWeekCheck">This Week</td></tr>
											<tr><td class="tabNormalText"><input type="radio" value="month" name="jobMonthWeekCheck">This Month</td></tr>	
											<tr><td class="tabNormalText"><input type="radio" checked="checked" value="clear" name="jobMonthWeekCheck">Clear</td></tr>	
										</tbody></table>	
									</td>
	    					</tr>
                        </tbody></table> -->
                        
                        
                        
                        </td>
                      <td><img width="1px" height="120" class="imagefilter" src="images/filterLine.gif"></td>
                      <td width="50%" valign="top">
                      
                      
                      
                 <%--      
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td class="filtersCaption" colspan="3">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					         <span id="OwnerSpanId"> 
					   			
							    	
							    		
							    		
								    		
							    			 
							    		<input type="checkbox" onclick="javascript: checkingOwner('<%=ownerId%>');" checked="checked" value="7" name="jobSelectedOwners">   
										  Me
										  <br> 
									
									</span>
								
								
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img height="34" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="changeFilter();" class="divbutton" src="images/showBtn.gif"><br></a></td></tr>
                              <tr><td colspan="3"><img width="2" src="images/spacer.gif"></td></tr>
                              <tr><td align="right" colspan="3"><a href="#"><img width="47" height="15" border="0" onclick="resetFilter();" class="divbutton" src="images/reset.gif"><br></a></td></tr>                              
                              <tr><td colspan="3"><img height="8" src="images/spacer.gif"></td></tr>
                              </tbody></table>
                               --%>
                              
                        <!--  add code  -->
                        
                        <%-- <table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="3" class="filtersCaption">User View</td></tr>
										<tr>
											<td class="tabNormalText">
												<span id="OwnerSpanId"> 
													<logic:present name="jobOwnerList" scope="request">
														<logic:iterate id="olist" name="jobOwnerList">
															<bean:define id="jobOwnerName" name="olist" property="ownerName" type="java.lang.String" />
															<bean:define id="jobOwnerId" name="olist" property="ownerId" type="java.lang.String" />
															<%checkowner = "javascript: checkingOwner('" + jobOwnerId + "');";
															if ((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																addOwnerRow++; addOwnerSpace = true;
															}
															if (addOwnerRow == 0) {%>
															<br />
															<span class="ownerSpan">&nbsp;<bean:message bundle="pm" key="msa.tabular.other" /> <br />
																<%}%>
																<%if (addOwnerSpace) {%>
																&nbsp;&nbsp;&nbsp;&nbsp;
																<%}%>
																<html:multibox property="jobSelectedOwners" onclick="<%=checkowner%>">
																	<bean:write name="olist" property="ownerId" />
																</html:multibox> 
																<bean:write name="olist" property="ownerName" /> 
																<br />
														</logic:iterate>
													</logic:present>
												</span>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick="changeFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" width="2" /></td></tr>
										<tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick="resetFilter();" /><br></a></td></tr>
										<tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
									</table>  --%>
                        
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
	                          <tr>
	                            <td colspan="3" class="filtersCaption">User View</td>
	                          </tr>
	                          <tr>
						         <td class="tabNormalText">
						          <span id = "OwnerSpanId">
						   			<logic:present name ="jobOwnerList" scope="request">
								    	<logic:iterate id = "olist" name = "jobOwnerList">
								    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
								    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
									    		<% 
		
												checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
												if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
													addOwnerRow++;
													addOwnerSpace = true;
												}	
									    		if(addOwnerRow == 0) { %> 
										    		<br/>
										    		<span class="ownerSpan">
													&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
													<br/>								
												<% } %>
								    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
								    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
												<bean:write name ="olist" property = "ownerId"/>
									 		</html:multibox>   
											  <bean:write name ="olist" property = "ownerName"/>
											  <br/> 
										</logic:iterate>
										</span>
									</logic:present>
									</span>
							    </td>
	                          </tr>
	                         
	                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
	                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
	                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
	                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
	                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
	                              </table></td>
	                        </tr>
	  			</table>			
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <!--  End  -->      
                              
                              
                              </td>
                        </tr>
  			</tbody></table>											
		</div>
	</span>
				</td>
	
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      </td>
 </tr>
 </tbody>
 </table>	

	<!--  End  -->
	<%-- <table width  "100%" border = "0" cellspacing = "0" cellpadding = "0">
	 <tr>
	    <td valign="top" width = "100%">
		      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
						<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Appendix</center></a></td>
						<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
						<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>" style="width: 120px"><center>Search</center></a></td>
						<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
					</tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
				          		<div id="breadCrumb">&nbsp;</div>
				        </td>
			        </tr>
			        <tr>
			        	<td colspan = "7" height="1"><h2>&nbsp;&nbsp;&nbsp;&nbsp;<%= context%>:&nbsp;Partner&nbsp;<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/></h2></td>
			        </tr> 
		      </table>
	    </td>
	    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
		<td valign="top" width="155">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
	    		<tr>
	      		<td height="21" background="images/content_head_04.jpg" width="140">
	      			<table width="100%" cellpadding="0" cellspacing="0" border="0">
		          		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
		           			 <td nowrap="nowrap" colspan="2">
		                			<div id="featured1">
			                			<div id="featured">
												<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
										</div>								
		                			</div>		
							<span>
							<div id="filter" class="divstyle">
		        			<table width="250" cellpadding="0" class="divtable">
		                    <tr>
			                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
			                          <tr>
		                       	 <td colspan="3" class="filtersCaption">Status View</td>
		                    </tr>
		                    <tr>	
		                      	  <td></td>
		                          	  <td class="tabNormalText">
											<logic:present name ="jobAppStatusList" scope="request">
										    	<logic:iterate id = "list" name = "jobAppStatusList"> 
										    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
										    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
		
													    	<%
												    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
															 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																	addStatusRow++;
																	addStatusSpace = true;
													    		}
													    	%>
													    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
											 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
														<bean:write name ="list" property = "statusid"/>
											 		</html:multibox>   
												 	<bean:write name ="list" property = "statusdesc"/><br> 
												 	<%addStatusSpace = false;%>
												</logic:iterate>
											</logic:present>		
											<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
											<table cellpadding="0">
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
												<tr><td class="tabNormalText"><html:radio name ="PartnerSOWAssumptionForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
											</table>	
										</td>
		    			    </tr>
		                    </table></td>
		                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
		                    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                      <tr>
		                        <td colspan="3" class="filtersCaption">User View</td>
		                      </tr>
		                      <tr>
						        <td class="tabNormalText">
						         <span id = "OwnerSpanId">
						   			<logic:present name ="jobOwnerList" scope="request">
								    	<logic:iterate id = "olist" name = "jobOwnerList">
								    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
								    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
									    		<% 
		
												checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
												if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
													addOwnerRow++;
													addOwnerSpace = true;
												}	
									    		if(addOwnerRow == 0) { %> 
										    		<br/>
										    		<span class="ownerSpan">
													&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
													<br/>								
												<% } %>
								    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
								    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
													<bean:write name ="olist" property = "ownerId"/>
										 		</html:multibox>   
												  <bean:write name ="olist" property = "ownerName"/>
												  <br/> 
											</logic:iterate>
											</span>
										</logic:present>
										</span>
								    </td>
		                          </tr>
		                          <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
		                          <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
		                          <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
		                          <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
		                          <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
		                        </table></td>
		                        </tr>
		  			            </table>											
				                </div>
			                    </span>
					            </td>
		                    </tr>
	                </table></td>
            </tr>
	        </table>
	    </td>
	 </tr>
	</table> --%>
	<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
	</logic:equal>

</logic:equal>
<logic:equal name ="PartnerSOWAssumptionForm" property="fromType" value="pm_act">
<%@ include  file="/JobMenuScript.inc" %>
</logic:equal>
<logic:equal name ="PartnerSOWAssumptionForm" property="fromType" value="am_act">
<table>
		<tr>
				<td>
						<h2>
	 					&nbsp;&nbsp;&nbsp;&nbsp;<%= context%>:&nbsp;Partner&nbsp;<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/>
						</h2>
				</td>
		</tr>
</table>
</logic:equal>


<TABLE>
         <TD>
            <TABLE cellspacing = 1 cellpadding = 0 class = "dbsummtable3" width = "800" border = 0>
              <TBODY>
	            <logic:present name = "PartnerSOWAssumptionForm" property = "addFlag">
		  		<tr><td>&nbsp;</td>
              	 <td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "PartnerSOWAssumptionForm" property = "addFlag" value = "10000">
			    		<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/>&nbsp;<bean:message bundle = "pm" key = "common.sa.add.success"/>
			    	</logic:equal>

	            	<logic:equal name = "PartnerSOWAssumptionForm" property = "addFlag" value = "10001">
			    		<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/>&nbsp;<bean:message bundle = "pm" key = "common.sa.update.success"/>
			    	</logic:equal>
		    	
			    	<logic:equal name = "PartnerSOWAssumptionForm" property = "addFlag" value = "-90001">
			    		<bean:message bundle = "pm" key = "common.sa.add.failure"/>&nbsp;<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "PartnerSOWAssumptionForm" property = "addFlag" value = "-90002">
			    		<bean:message bundle = "pm" key = "common.sa.update.failure"/>&nbsp;<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/>
			    	</logic:equal>
			    	</td>
			       </tr>	
			     </logic:present>
			     
			     <logic:present name = "deleteFlag" scope = "request">	
			      <tr><td>&nbsp;</td>
	              	 <td colspan = "5" class = "message" height = "30">
				     	<logic:equal name = "deleteFlag" value = "0">
				     		<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/>&nbsp;<bean:message bundle = "pm" key = "common.sa.delete.success"/>	
				     	</logic:equal>
				    	
				     	<logic:notEqual name = "deleteFlag" value = "0">
					     	<bean:message bundle = "pm" key = "common.sa.delete.failure"/>&nbsp;<bean:write name = "PartnerSOWAssumptionForm" property = "dataType"/>
				     	</logic:notEqual>
					    </td>
				    </tr>
			     </logic:present>
	            <tr>
					<TD></TD>
					
				</tr>
          
            <% if( listSize > 0 ){ %>    
              <TR>
                <TD></TD>
                <TD class = Ntryb height = 20 align = "middle">Activities</TD>
                <TD class = Ntryb  height = 20 align = "middle">Type</TD>
                <TD class = Ntryb  height = 20 align = "middle">Qty</TD>
                <TD class = Ntryb  height = 20 align = "middle">Status</TD>
                <TD class = Ntryb  height = 20 align = "middle" width = "150">&nbsp;</TD>
			  </TR>
          
              <logic:present name = "partnerSOWassumptionList">
              		<%
					String temp_str = "";
					String temp_str1 = "";
					String temp_str2 = "";
              		int i = 1;
              		int j = 1;

					String bgcolor = "";
					String bgcolorno = ""; 
					String bgcolortext1 = ""; 
					String tempCategory = "";
					String bgcolordata = "";
					%>
				
					<logic:iterate id = "psalist" name = "partnerSOWassumptionList" >
					<%
					temp_str = "";
					//temp_str = "javascript:del( "+poid+" );"; 
					if( ( i%2 )== 0 ) {
						bgcolor = "dbvalueodd";
						bgcolorno = "Nnumberodd";
						bgcolortext1 = "Ntexto";
						bgcolordata ="Ntextleftodd1";
					} 
					else
					{
						bgcolor = "dbvalueeven";
						bgcolorno="Nnumbereven";
						bgcolortext1="Ntexte";
						bgcolordata ="Ntextlefteven1";
						
					}
				%>
							
				<!-- Added By Amit id of TR for more/back-->
						<bean:define id = "actId" name = "psalist" property = "actId" type = "java.lang.String"/>
						<bean:define id = "actCat" name = "psalist" property = "actCategory" type = "java.lang.String"/>
						<bean:define id = "SOWAssumptionId" name = "psalist" property = "SOWAssumptionId" type = "java.lang.String"/>
					<%if( !tempCategory.equals(actCat) ){  %>
						<TR height="20">
							<TD></TD>
                			<td class = "formCaption" height = "20" colspan = "6">
							<bean:write name = "psalist" property = "actCategory" />
							</td>
						</TR>
					<% } %>
					<% tempCategory = actCat; %>
							
			<TR id = "account" height="20">                
                <TD width = "20">
                	<logic:equal name = "psalist" property = "SOWAssumptionId" value="0">
                		&nbsp;&nbsp;&nbsp;
                	</logic:equal>
                	<logic:notEqual name = "psalist" property = "SOWAssumptionId" value="0">
	                	<A href = "javascript:toggleMenuSection( 'j<%= i %>' );">
						<IMG id = img_j<%= i %> alt=+ src = "images/Expand.gif" border = 0></A> 
					</logic:notEqual>
				</TD>
				
                <TD class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
					<bean:write name = "psalist" property = "actName"/>
				</TD>	
				
				<TD class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
					<bean:write name = "psalist" property = "actType"/>
				</TD>
				
				<TD class = "<%= bgcolorno %>" align = "left"  valign = middle>
					<bean:write name = "psalist" property = "actQty"/>
				</TD>
				
				<TD class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
					<bean:write name = "psalist" property = "actStatus"/>
				</TD>
				
				<TD class="<%= bgcolortext1 %>" align = "right">
					<logic:equal name = "psalist" property = "SOWAssumptionId" value="0">
                		<A id='aa_j<%= actId%>' href = "PartnerSOWAssumptionAction.do?fromId=<bean:write name = "PartnerSOWAssumptionForm" property = "fromId"/>&viewType=<bean:write name = "PartnerSOWAssumptionForm" property = "viewType"/>&fromType=<bean:write name = "PartnerSOWAssumptionForm" property = "fromType"/>&ownerId=<bean:write name = "psalist" property = "ownerId"/>&actId=<bean:write name = "psalist" property = "actId"/>&viewjobtype=<bean:write name = "PartnerSOWAssumptionForm" property = "viewJobType"/>&fromId=<bean:write name = "PartnerSOWAssumptionForm" property = "fromId" />&fromPage=<bean:write name = "PartnerSOWAssumptionForm" property = "fromPage" />">[&nbsp;Add</A>&nbsp;|&nbsp;<A id='ba_j<%= actId%>' href = "PartnerSOWAssumptionList.do?fromId=<bean:write name = "PartnerSOWAssumptionForm" property = "fromId"/>&viewType=<bean:write name = "PartnerSOWAssumptionForm" property = "viewType"/>&fromType=<bean:write name = "PartnerSOWAssumptionForm" property = "fromType"/>&action=getdefault&actId=<bean:write name = "psalist" property = "actId"/>&SOWAssumptionId=<bean:write name = "psalist" property = "SOWAssumptionId"/>">Default from&nbsp;<bean:write name = "PartnerSOWAssumptionForm" property = "contextType"/>&nbsp;]</A> 
                	</logic:equal>
                	<logic:notEqual name = "psalist" property = "SOWAssumptionId" value="0">
	                	<A id='aa_j<%= actId%>' href = "PartnerSOWAssumptionAction.do?fromId=<bean:write name = "PartnerSOWAssumptionForm" property = "fromId"/>&viewType=<bean:write name = "PartnerSOWAssumptionForm" property = "viewType"/>&fromType=<bean:write name = "PartnerSOWAssumptionForm" property = "fromType"/>&actId=<bean:write name = "psalist" property = "actId"/>&viewjobtype=<bean:write name = "PartnerSOWAssumptionForm" property = "viewJobType"/>&ownerId=<bean:write name = "psalist" property = "ownerId"/>&fromId=<bean:write name = "PartnerSOWAssumptionForm" property = "fromId" />&fromPage=<bean:write name = "PartnerSOWAssumptionForm" property = "fromPage" />">[&nbsp;Update</A>&nbsp;|&nbsp;<A id='ba_j<%= actId%>' href = "javascript:del(<%= SOWAssumptionId%>)">Delete</A>&nbsp;|&nbsp;<A id='ca_j<%= actId%>' href = "PartnerSOWAssumptionList.do?fromId=<bean:write name = "PartnerSOWAssumptionForm" property = "fromId"/>&viewType=<bean:write name = "PartnerSOWAssumptionForm" property = "viewType"/>&fromType=<bean:write name = "PartnerSOWAssumptionForm" property = "fromType"/>&action=getdefault&actId=<bean:write name = "psalist" property = "actId"/>&SOWAssumptionId=<bean:write name = "psalist" property = "SOWAssumptionId"/>">Default from <bean:write name = "PartnerSOWAssumptionForm" property = "contextType"/>&nbsp;]</A> 
					</logic:notEqual>
				</TD>
                </TR>
				<logic:notEqual name = "psalist" property = "SOWAssumptionId" value="0">
					<TR id = div_j<%= i %> style = "DISPLAY: none">
	                <TD>&nbsp;</TD>
	                <TD colSpan=6>
	                  <TABLE cellSpacing = 0 cellPadding = 0>
	                    <TBODY>
	                    <TR>
	                      <TD width = 20>&nbsp;&nbsp;&nbsp;&nbsp;</TD>
	                      <TD>
	                        <TABLE height = "100%" cellSpacing = 0 cellPadding = 0 width = "800" border = "0">
	                          <TBODY>
		                        <TR height = 20>
								    <TD class = "<%= bgcolortext1 %>" width = "800" colSpan="6" height="100">
								      <span><html:textarea name="psalist" property="SOWAssumptionValue" rows="10" cols="150" readonly="true" styleClass="<%= bgcolordata %>"/></span>
								  <!-- 	<bean:write name = "psalist" property = "SOWAssumptionValue"/> -->
								  		
								    </TD>
								</TR>
							</TBODY></TABLE></TD></TR></TBODY></TABLE></TD>
						</TR>
					</logic:notEqual>
			<% i++; %>
				</logic:iterate>
			</logic:present>
			<% } else { %>
					<tr>
		    			<TD></TD>
		    			<td  colspan = "6" class = "message" height = "30">
		    				No Activity defined. 
		    			</td>
		    		</tr>
		
			<% } %>
  			   </tbody> </table> </td> </table>
</body>
</html:form>
<script>
function del(SOWAssumptionId) 
{	
	if(document.forms[0].viewType.value == 'sow')
		var convel = confirm( "Are you sure you want to delete the SOW?" );
	else
	 if(document.forms[0].viewType.value == 'assumption')
		var convel = confirm( "Are you sure you want to delete the Assumptions?" );	
		
		if( convel )
		{
			document.forms[0].action = "PartnerSOWAssumptionList.do?fromId=<bean:write name = "PartnerSOWAssumptionForm" property = "fromId"/>&viewType=<bean:write name = "PartnerSOWAssumptionForm" property = "viewType"/>&fromType=<bean:write name = "PartnerSOWAssumptionForm" property = "fromType"/>&SOWAssumptionId="+SOWAssumptionId+"&action=delete&viewjobtype=<bean:write name = "PartnerSOWAssumptionForm" property = "viewJobType"/>&ownerId=<bean:write name = "PartnerSOWAssumptionForm" property = "ownerId"/>";
			document.forms[0].submit();
			return true;	
		}
}

</script>
<script>
function changeFilter()
{
	document.forms[0].action="PartnerSOWAssumptionList.do?showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="PartnerSOWAssumptionList.do?resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].fromId.value;
 	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>

</html:html>


