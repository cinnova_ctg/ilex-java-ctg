<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>



<html:html>

<% 
	
	String setvalue = "";
	String resource_label;
	int size=0;
	int offset1=0;
	int offset2=0;
	int resource=0;
	int columns=0;
	if(request.getAttribute( "size" ).toString()!=null)
	{
		size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
	}
	

	if(request.getAttribute( "restype" ).toString()!=null)
	{
		if(request.getAttribute( "restype" ).equals("Material"))
		{
			resource=1;
		}
		else if(request.getAttribute( "restype" ).equals("Labor"))
		{
			resource=2;
		}
		else if(request.getAttribute( "restype" ).equals("Freight"))
		{
			resource=3;
		}
		else if(request.getAttribute( "restype" ).equals("Travel"))
		{
			resource=4;
		}
	}

	switch(resource){
	case 1:
	{
		
		resource_label="common.searchwindow.materialname";
		
		break;
	}
	case 2:
	{
		
		resource_label="common.searchwindow.laborname";
		
		break;
	}
	case 3:
	{
		
		resource_label="common.searchwindow.freightname";
		
		break;
	}
	case 4:
	{
		
		resource_label="common.searchwindow.travelname";
		
		break;
	}
		default:
		{
			
			resource_label="common.searchwindow.materialname";
			
			
		}

	}

	
	
%>
	<head>
		<title><bean:message bundle = "pm" key = "common.searchwindow.searchcriteria"/></title>
		<%@ include file="../Header.inc" %>
		<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
		<LINK href="styles/content.css" rel="stylesheet"	type="text/css">

	

<script>



</script>
</head>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" >
<html:form action = "/OpenSearchWindowAction" >

<html:hidden property = "type" />
<html:hidden property = "activity_Id" />
<html:hidden property = "from" />
<html:hidden property = "clr" />
<html:hidden property = "offset1" />
<html:hidden property = "offset2" />


<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr> 
		  		<td colspan = "7" class = "labeleboldwhite" height = "30">
		  			<bean:message bundle = "pm" key = "common.searchwindow.searchcriteria"/>
			  	</td>
				</tr>
</table>
<table  border="0" cellspacing="1" cellpadding="1" >
  <tr>
  	<td  width="2" height="0"></td>
  	<td><table border="0" cellspacing="1" cellpadding="1" > 
		  	<tr>
		  		<td class = "colDark" width = "100" >
		  			<bean:message bundle = "pm" key = "common.searchwindow.category"/>
		  		</td>
		  		<td class = "colLight" >
				 	<html:select property = "category" size = "1" styleClass = "select" onchange = "javascript:return refresh();">
						<html:optionsCollection name = "OpenSearchWindowForm" property = "categorycombo" value = "value" label = "label"/>    
					</html:select>
				</td>
		  			
		  		<td class = "colDark" width = "100">
		  			<bean:message bundle = "pm" key = "common.searchwindow.subcategory"/>
		  		</td>
		  		<td class = "colLight" >
				 	<html:select property = "subcategory" size = "1" styleClass = "select"  onchange = "javascript:return refresh();">
						<html:optionsCollection name = "OpenSearchWindowForm" property = "subcategorycombo" value = "value" label = "label"/>    
					</html:select>
				</td>
		  			
		  		<td class = "colDark" width = "100">
		  			<bean:message bundle = "pm" key = "common.searchwindow.subsubcategory"/>
		  		</td>
		  		<td class = "colLight" >
				 	<html:select property = "subsubcategory"  size = "1" styleClass = "select">
						<html:optionsCollection name = "OpenSearchWindowForm" property = "subsubcategorycombo" value = "value" label = "label"/>    
					</html:select>
				</td>
		  	</tr>
		  
		
			<tr>
				
				<td class = "colDark" width = "100">
				 	<bean:message bundle = "pm" key = "common.searchwindow.keyword"/>
				 	
				</td>
				<td class = "colLight" >
				 	<html:text property = "keyword" styleClass = "textbox" size="15"/>
				</td>
				
				<td class = "colDark"  colspan = "4">
				 	<html:submit property = "search" styleClass = "button_C">
				 		<bean:message bundle = "pm" key = "common.searchwindow.search"/>
				 	</html:submit>
				</td>
			</tr>
			
			<tr></tr>
		</table>
	</td>
  </tr>

		

<table  border="0" cellspacing="1" cellpadding="1"  >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" > 
			<logic:equal name = "OpenSearchWindowForm" property = "searchclick" value = "true">
			<tr>
    			<td  class="labeleboldwhite" height="30" colspan="7"><bean:message bundle="pm" key="common.searchwindow.basicsearchresults" /></td>
  			</tr>
  			
			<tr>
				<td class = "Ntryb">&nbsp;</td>
				<td class = "Ntryb">
					<bean:message bundle = "pm" key="<%=resource_label %>"/>
				</td>
				
				<%if(request.getAttribute( "restype" ).equals("Material"))
				{ %>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.manpartnumber"/>
				</td>
				<%} %>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.unitcost"/>
				</td>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.cnspartnumber"/>
				</td>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.sellablequantity"/>
				</td>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.minimumquantity"/>
				</td>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.alternateidentifier"/>
				</td>
				
			</tr>
			
			
			<%int i=1;
		  	int j = 0;
			String bgcolor = ""; 
			String bgcolor1 = "";
			%>
			
			<logic:iterate id = "resourcelist" name = "OpenSearchWindowForm" property = "resourcelist">
			
		 	<%if((i%2)==0)
				{
					bgcolor="Ntexte"; 
					bgcolor1="Ntexte"; 	
				} 
			  else
				{
					bgcolor="Ntexto";
					bgcolor1="Ntexto";
				}
		
				
				if( size == 1 ) 
				{ 
					setvalue = "javascript:assignresource( '' );";
				}
				else
				{
					setvalue = "javascript:assignresource( '"+j+"' );";
				}%>
			
			
				
			
				<bean:define id = "resource_Id" name = "resourcelist" property = "resource_id" type = "java.lang.String"/>
				<tr>
					
					<td class="<%=bgcolor1 %>" nowrap>
						<html:radio property = "resource_id" value = "<%= resource_Id %>" onclick = "<%= setvalue %>"/>
					</td>
					
					<td  class="<%=bgcolor1 %>" nowrap>
						<bean:write name = "resourcelist" property = "resource_name" />
						<html:hidden name = "resourcelist" property = "resource_name" />
					</td>
					
					<%if(request.getAttribute( "restype" ).equals("Material"))
					{ %>
					<td  class="<%=bgcolor1 %>">
						<bean:write name = "resourcelist" property = "manufacturer_part_number" />
						<html:hidden name = "resourcelist" property = "manufacturer_part_number" />
					</td>
					<%} %>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "unit_cost" />
						<html:hidden name = "resourcelist" property = "unit_cost" />
					</td>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "cns_part_number" />
						<html:hidden name = "resourcelist" property = "cns_part_number" />
					</td>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "sellable_quantity" />
						<html:hidden name = "resourcelist" property = "sellable_quantity" />
					</td>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "minimum_quantity" />
						<html:hidden name = "resourcelist" property = "minimum_quantity" />
					</td>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "alternate_identifier" />
						<html:hidden name = "resourcelist" property = "alternate_identifier" />
					</td>
				</tr>
				<%i++;j++; %>
			</logic:iterate>
			<%if(i==1)
			{ %>
				<tr>
					<td>&nbsp;</td>
					<td  class="message" height="30" colspan="7"><bean:message bundle="pm" key="common.searchwindow.norecord" /></td>
				</tr>
			<%} %>
			
				
			
			</logic:equal>
		

 		</table> 
 	  </td>
   </tr>
   
	
  	
  	
</html:form>
</table>

<script>

function refresh()
{
	
	document.forms[0].submit();
	return true;
}

function assignresource( temp )
{
	var address = '';
	
	
	if( temp == '' )
	{
		if(document.forms[0].type.value=='Material')
		{
			
			window.opener.document.forms[0].newmaterialnamecombo.value = document.forms[0].resource_id.value;
			
			window.opener.document.forms[0].newmaterialname.value = document.forms[0].resource_name.value;
			window.opener.document.forms[0].newmaterialid.checked = true;
			window.opener.refresh();
		}
		if(document.forms[0].type.value=='Labor')
		{
			
			if(document.forms[0].clr.value=='yes')
			{
				var off1=document.forms[0].offset1.value;
				var off2=document.forms[0].offset2.value;
				var str=off1+"["+off2+"]";
				var str1="";
				eval('window.opener.document.forms[0].resourceid'+str).value = document.forms[0].resource_id.value;
				eval('window.opener.document.forms[0].resourcename'+str).value = document.forms[0].resource_name.value;
				
			}
			else
			{
				window.opener.document.forms[0].newlabornamecombo.value = document.forms[0].resource_id.value;
				
				window.opener.document.forms[0].newlaborname.value = document.forms[0].resource_name.value;
				window.opener.document.forms[0].newlaborid.checked = true;
				window.opener.document.forms[0].newcnspartnumber.value = document.forms[0].cns_part_number.value;
				
				window.opener.refresh();
			}
			
			
			
			
		}
		if(document.forms[0].type.value=='Freight')
		{
			
			window.opener.document.forms[0].newfreightnamecombo.value = document.forms[0].resource_id.value;
			
			window.opener.document.forms[0].newfreightname.value = document.forms[0].resource_name.value;
			window.opener.document.forms[0].newfreightid.checked = true;
			window.opener.refresh();
		}
		if(document.forms[0].type.value=='Travel')
		{
			
			window.opener.document.forms[0].newtravelnamecombo.value = document.forms[0].resource_id.value;
			
			window.opener.document.forms[0].newtravelname.value = document.forms[0].resource_name.value;
			window.opener.document.forms[0].newtravelid.checked = true;
			window.opener.refresh();
		}
		
	
	}
	
	else
	{
		if(document.forms[0].type.value=='Material')
		{
			
			window.opener.document.forms[0].newmaterialnamecombo.value = document.forms[0].resource_id[temp].value;
			window.opener.document.forms[0].newmaterialname.value = document.forms[0].resource_name[temp].value;
			window.opener.document.forms[0].newmaterialid.checked = true;
			window.opener.refresh();
		}
		
		
		if(document.forms[0].type.value=='Labor')
		{
			
			
			if(document.forms[0].clr.value=='yes')
			{
				var off1=document.forms[0].offset1.value;
				var off2=document.forms[0].offset2.value;
				var str=off1+"["+off2+"]";
				var str1="";
				eval('window.opener.document.forms[0].resourceid'+str).value = document.forms[0].resource_id[temp].value;
				eval('window.opener.document.forms[0].resourcename'+str).value = document.forms[0].resource_name[temp].value;
				
			}
			else
			{
				
				window.opener.document.forms[0].newlabornamecombo.value = document.forms[0].resource_id[temp].value;
				window.opener.document.forms[0].newlaborname.value = document.forms[0].resource_name[temp].value;
				window.opener.document.forms[0].newcnspartnumber.value = document.forms[0].cns_part_number[temp].value;
				
				window.opener.document.forms[0].newlaborid.checked = true;
				
				
				window.opener.refresh();
			}
			
		}
		
		if(document.forms[0].type.value=='Freight')
		{
			
			window.opener.document.forms[0].newfreightnamecombo.value = document.forms[0].resource_id[temp].value;
			window.opener.document.forms[0].newfreightname.value = document.forms[0].resource_name[temp].value;
			window.opener.document.forms[0].newfreightid.checked = true;
			window.opener.refresh();
		}
		
		if(document.forms[0].type.value=='Travel')
		{
			
			window.opener.document.forms[0].newtravelnamecombo.value = document.forms[0].resource_id[temp].value;
			window.opener.document.forms[0].newtravelname.value = document.forms[0].resource_name[temp].value;
			window.opener.document.forms[0].newtravelid.checked = true;
			window.opener.refresh();
		}
			
	}
	 
	window.close();
	
}



</script>




</body>








</html:html>

