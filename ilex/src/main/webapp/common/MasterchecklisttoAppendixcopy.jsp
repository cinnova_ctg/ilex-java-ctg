<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating and  updating  MSAs.
*
-->



<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<% int master_check_list_size = ( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() );
String valstr = null;
String addQuestion="javascript: addQuestion();";
%>


<html:html>

<head>

	<title><bean:message bundle = "pm" key = "msa.tabular.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<%
	String backgroundclass = null;
	String backgroundclass1 = null;
	boolean csschooser = true;
	int i = 0;
	String[] option1 =null;
	int j=0;

%>


<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<!--  <tr>
    	<td class = "toprow"> 
	    	<table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
	        	<tr align = "center"> 
		    		<td class = "toprow1" width = "150"><a href = "AppendixHeader.do?appendixid=<bean:write name = "ChecklistForm" property = "appendix_Id" />&function=view" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "drop"><bean:message bundle = "pm" key = "common.mastertoappendixcopy.backtoappendixdashboard"/></a></td>		
		    		<td class = "toprow1" width = "650">&nbsp;&nbsp;</td>
	        	</tr>
	      	</table>
    	</td>
	</tr>-->
</table>







<table>
<html:form action = "/MasterchecklisttoAppendixcopyAction">
<html:hidden property = "newrowsadded" value = "0"/>
<html:hidden property = "intialchecklistsize" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
	<tr>
		<td>
			<table  border = "0" cellspacing = "1" cellpadding = "1" width = "850">
    			<logic:present name = "updateflag" scope = "request">
		    		<tr> 
    					
    					<td colspan = "5" class = "message" height = "30">
			    			<logic:equal name = "updateflag" value = "0">
			    				<bean:message bundle = "pm" key = "common.mastertoappendixcopy.updatesuccess"/>		
			    			</logic:equal>
		    			
							<logic:equal name = "updateflag" value = "-9001">
								<bean:message bundle = "pm" key = "common.mastertoappendixcopy.updatefailure1"/>	
							</logic:equal>
						
							<logic:equal name = "updateflag" value = "-9002">
								<bean:message bundle = "pm" key = "common.mastertoappendixcopy.updatefailure2"/>	
							</logic:equal>
						</td>
					</tr>
						
		    	</logic:present>
		    	
		    	<logic:present name = "addflag" scope = "request">
					<tr> 
						<td colspan = "5" class = "message" height = "30">
			    			<logic:equal name = "addflag" value = "0">
			    			<bean:message bundle = "pm" key = "common.qcchecklistques.addsuccess"/>				
			    			</logic:equal>
						
							<logic:equal name = "addflag" value = "-9001">
								<bean:message bundle = "pm" key = "common.qcchecklistques.addfailure1"/>	
							</logic:equal>
						
							<logic:equal name = "addflag" value = "-9002">
								<bean:message bundle = "pm" key = "common.qcchecklistques.addfailure2"/>	
							</logic:equal>
						</td>
					</tr>			
	 			</logic:present>
  
		    	
    			<tr>
    				<td colspan="5">
    					<table id = "dynatable" border = "0" width = "848">
    								
    			
		    			<tr>
		    				<td width = "20">&nbsp;</td>
		    				<td class = "labeleboldwhite" colspan = "3" height = "30">
		    					<bean:message bundle = "pm" key = "common.mastertoappendixcopy.header"/>
		    				 </td>
		  					<td></td>
		  				</tr>
		  
						<tr> 
		    				<td width = "20">&nbsp;</td>
		    				<td class = "tryb"  width = "150"><bean:message bundle = "pm" key = "common.mastertoappendixcopy.groupname"/></td>
		    
							<td class = "tryb" width = "350"><bean:message bundle = "pm" key = "common.mastertoappendixcopy.itemname"/></td>
							<td class = "tryb" width = "150"><bean:message bundle = "pm" key = "common.mastertoappendixcopy.options"/></td>
							<td width = "178"></td>
		  				</tr>
					
						<logic:iterate id = "master_checklist" name = "master_checklist" scope = "request">
							<%	
								if ( csschooser == true ) 
								{
									backgroundclass = "hyperodd";
									backgroundclass1 = "readonlytextodd";
									csschooser = false;
									
								}
						
								else
								{
									backgroundclass = "hypereven";
									backgroundclass1 = "readonlytexteven";
									csschooser = true;	
								}
								if( master_check_list_size > 0 )
									valstr = "javascript: document.forms[0].check['"+i+"'].checked = true";
								else
									valstr = "javascript: document.forms[0].check.checked = true";
							%>
							
							
							
							<bean:define id = "master_checklist_id" name = "master_checklist" property = "checklistid" type = "java.lang.String" />
							<bean:define id="citype" name = "master_checklist" property = "checklistidtype" type="java.lang.String"/>
							<bean:define id="opt" name = "master_checklist" property = "option" type="java.lang.String"/>
							<bean:define id = "opt_type" name = "master_checklist" property = "optiontype" type = "java.lang.String" />
							
							<tr>
								<td>
									<html:multibox styleId='<%="check"+i%>' property = "check">
										<bean:write   name = "master_checklist" property = "checklistidtype"/>
									</html:multibox>
								</td>
								
								
								<html:hidden styleId='<%="checklistidtype"+i%>' name="master_checklist" property="checklistidtype" />
								<td class = "<%= backgroundclass %>">
									
									<html:text  styleId='<%="groupname"+i%>' styleClass="<%= backgroundclass1 %>"  size="20" name = "master_checklist" property = "groupname" readonly="true"/>
								</td>
								
								<td class = "<%= backgroundclass %>">
									<html:textarea  styleId = '<%= "itemname"+i%>' styleClass = "textbox"   name = "master_checklist" property = "itemname" rows="1" cols="55" onchange = "<%= valstr %>"  />
								</td>
								
								<td class = "<%= backgroundclass %>">
									
									<%if(opt!=null)
									{
										option1 = opt.split( "-" );
										if(opt_type.equals( "S" ))
										{%>
										<html:radio styleId = '<%="option"+i%>' property = "option" value = "option1[0]" disabled = "true"/><%=option1[0] %>
										<%}
										else
										{ %>
										<html:checkbox styleId = '<%= "option"+i%>' property = "option" value = "option1[0]" disabled = "true"/><%=option1[0] %>
									<%  } 
									}%>
									
									
								</td>
								<td></td>
								</tr>
								
								<%if(opt!=null)
									{
										for(j=1;j<option1.length;j++)
										{%>
										<tr>
											<td>&nbsp;
											</td>
											<td class = "<%= backgroundclass %>">&nbsp;
											</td>
											<td class = "<%= backgroundclass %>">&nbsp;
											</td>
											<td class = "<%= backgroundclass %>">
											<%if( opt_type.equals( "S" ) )
											{ %>
											<html:radio property = "option" value = "option1[j]" disabled = "true"/><%= option1[j] %>
											<%}
											else
											{ %>
											<html:checkbox property = "option" value = "option1[j]" disabled="true"/><%= option1[j] %>
											<%} %>
											</td>
											<td></td>
									
										</tr>
							
											
										<%}
									} 
									i++;
									%>
								
								
						</logic:iterate>
					  </table>
					</td>
				</tr>
				
    			
    			<tr>
    				<td colspan="5" width="848">
    					<table  border = "0" width="840">
	
							<tr>	
								<td width = "20">&nbsp;</td>
					  			<td class = "buttonrow" width = "540" colspan = "3">
					  				<html:submit property = "submit" styleClass = "button" onclick = "return validate();"><bean:message bundle = "pm" key = "common.mastertoappendixcopy.save"/></html:submit>
									<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "pm" key = "msa.tabular.cancel"/></html:reset>
									<html:submit property = "addMore" styleClass = "button" onclick = "return addQuestion();" ><bean:message bundle = "pm" key = "common.mastertoappendixcopy.addmore"/> </html:submit>
								</td>
								<td width = "178"></td>
							</tr>
						</table>
					</td>
				</tr>
			
			
			</table>
		</td>
	</tr>

		<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'msatabularpage'/>
		 </jsp:include>	

</table>

<html:hidden property = "appendix_Id" />


</html:form>
</table>

</body>


<script>

function addQuestion()
{
	
	document.forms[0].action = "AddQcChecklistQues.do";
	
	return true;

}

function validate()
{
	var checkflag = 'false';
	if( <%= master_check_list_size %> != 0 )
	{
		if( <%= master_check_list_size %> == 1 )
		{
			
			if( document.forms[0].check.checked )
			{
				/*if(document.forms[0].groupname.value=="" )
				{
					alert( "<bean:message bundle = "pm" key = "common.mastertoappendixcopy.cannotleftblank"/>");
					
					document.forms[0].groupname.focus();
					return false;
				}*/
				if(document.forms[0].itemname.value=="")
				{
					alert( "<bean:message bundle = "pm" key = "common.mastertoappendixcopy.cannotleftblank"/>");
					
					document.forms[0].itemname.focus();
					return false;
				}
				
			}
			if( !document.forms[0].check.checked )
			{
				alert( "<bean:message bundle = "pm" key = "common.mastertoappendixcopy.selectchecklist"/>" );
				return false;
			}
		}
		else
		{
			for( var i = 0; i < document.forms[0].check.length; i++ )
			{
				if( document.forms[0].check[i].checked )
				{
					//checkflag = 'true';
					
					/*if(document.forms[0].groupname[i].value=="" )
					{
						alert( "<bean:message bundle = "pm" key = "common.mastertoappendixcopy.cannotleftblank"/>");
						document.forms[0].groupname[i].focus();
						
						return false;
					}*/
					if(document.forms[0].itemname[i].value=="")
					{
						alert( "<bean:message bundle = "pm" key = "common.mastertoappendixcopy.cannotleftblank"/>");
						document.forms[0].itemname[i].focus();
						
						return false;
					}
					
					
				}
				
				if( document.forms[0].check[i].checked )
				{
					checkflag = 'true';
				}
				
				
			}
			
			if( checkflag == 'false' )
			{
				alert( "<bean:message bundle = "pm" key = "common.mastertoappendixcopy.selectchecklisttoupdate"/>" );
				return false;
			}
		
		}
	}
	return true;
}


function Backaction(appendixid)
{

	document.forms[0].action = "AppendixHeader.do?appendixid="+appendixid+"&function=view&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	//document.forms[0].submit();
	return true;
}

/*function moreAttach(size){

	var arg= moreAttach.arguments;
	var i = (dynatable.rows.length-2) ;
	var k = (dynatable.rows.length-2)-size ;

	if (arg[0]==1){
		if (i>=1){
		 return;
		 }		
	}
	if (arg[0]!=1)
	
	
	i = (dynatable.rows.length-2);
	k = (dynatable.rows.length-2)-size ;
    var cls='';
	if(dynatable.rows.length%2==0)	cls= 'hyperodd';
	if(dynatable.rows.length%2==1)	cls= 'hypereven';
	
	var oRow=dynatable.insertRow();
	var oCell=oRow.insertCell(0);
	
	oCell.innerHTML="<input type='checkbox' id='check"+i+"' name='check' value='C"+k+"' >";
	

	oCell=oRow.insertCell(1);
	oCell.innerHTML="<input type='text' id='groupname"+i+"' name='groupname' size=20 maxlength=100 class='textbox'>"+" "+"<input type='hidden' id='checklistidtype"+i+"' name='checklistidtype' size=20 maxlength=100 class='textbox' value='C"+k+"'>";
	
	oCell.className=cls;

	oCell=oRow.insertCell(2);
	oCell.innerHTML="<textarea id='itemname"+i+"' name='itemname'  rows='1' cols='55' class='textbox' />";
	oCell.className=cls;

	oCell=oRow.insertCell(3);
	oCell.innerHTML="<input type='radio' id='option"+i+"' name='option' value='S'>Single &nbsp;<input type='radio' id='option"+i+"' name='option' value='M'>Multiple";
	oCell.className=cls;
	
	
	document.forms[0].newrowsadded.value=eval(document.forms[0].newrowsadded.value)+1;
	
}*/	// END OF FUNCTION moreAttach()




</script>


</html:html>
  