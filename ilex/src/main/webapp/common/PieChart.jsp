<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>


<html:html>
<head>
<title>ILEX</title>

</head>

<body bgcolor=#ECECEC>
<form name = "chart" method = "POST" action = "MindChartServlet">
<table>
<logic:present name = "GraphForm" property = "TITLECHART">
<tr>
	<td>
		<input type = "hidden" name = "TITLECHART" value = "<bean:write name="GraphForm" property="TITLECHART"/>"/>
		
	</td>
</tr>				
</logic:present>


<logic:present name= "GraphForm" property = "LEGEND">
<tr>
	<td>
		<input type="hidden" name = "LEGEND" value = "<bean:write name="GraphForm" property="LEGEND"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property = "SERIE_1">
<tr>
	<td>
		<input type="hidden" name = "SERIE_1" value = "<bean:write name="GraphForm" property="SERIE_1"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name = "GraphForm" property = "SERIE_TYPE_1">
<tr>
	<td>
		<input type = "hidden" name = "SERIE_TYPE_1" value = "<bean:write name = "GraphForm" property = "SERIE_TYPE_1"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name = "GraphForm" property="PIE_STYLE_1">
<tr>
	<td>
		<input type = "hidden" name = "PIE_STYLE_1" value="<bean:write name="GraphForm" property="PIE_STYLE_1"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIE_STYLE_2">
<tr>
	<td>
		<input type="hidden" name="PIE_STYLE_2" value="<bean:write name="GraphForm" property="PIE_STYLE_2"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIE_STYLE_3">
<tr>
	<td>
		<input type="hidden" name="PIE_STYLE_3" value="<bean:write name = "GraphForm" property = "PIE_STYLE_3"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIE_STYLE_4">
<tr>
	<td>
		<input type="hidden" name="PIE_STYLE_4" value="<bean:write name="GraphForm" property="PIE_STYLE_4"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIE_NAME_1">
<tr>
	<td>
		<input type="hidden" name="PIE_NAME_1" value="<bean:write name="GraphForm" property="PIE_NAME_1"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIE_NAME_2">
<tr>
	<td>
		<input type="hidden" name="PIE_NAME_2" value="<bean:write name="GraphForm" property="PIE_NAME_2"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIE_NAME_3">
<tr>
	<td>
		<input type="hidden" name="PIE_NAME_3" value="<bean:write name="GraphForm" property="PIE_NAME_3"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIE_NAME_4">
<tr>
	<td>
		<input type="hidden" name="PIE_NAME_4" value="<bean:write name="GraphForm" property="PIE_NAME_4"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIE_LABEL_FORMAT">
<tr>
	<td>
		<input type="hidden" name="PIE_LABEL_FORMAT" value="<bean:write name="GraphForm" property="PIE_LABEL_FORMAT"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="SERIE_DATA_1">
<tr>
	<td>
		<input type="hidden" name="SERIE_DATA_1" value="<bean:write name="GraphForm" property="SERIE_DATA_1"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="SERIE_FONT_1">
<tr>
	<td>
		<input type="hidden" name="SERIE_FONT_1" value="<bean:write name="GraphForm" property="SERIE_FONT_1"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="SERIE_LABELS_1">
<tr>
	<td>
		<input type="hidden" name="SERIE_LABELS_1" value="<bean:write name="GraphForm" property="SERIE_LABELS_1"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="SERIE_TOGETHER_1">
<tr>
	<td>
		<input type="hidden" name="SERIE_TOGETHER_1" value="<bean:write name="GraphForm" property="SERIE_TOGETHER_1"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="CHART_BORDER">
<tr>
	<td>
		<input type="hidden" name="CHART_BORDER" value="<bean:write name="GraphForm" property="CHART_BORDER"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="CHART_FILL">
<tr>
	<td>
		<input type="hidden" name="CHART_FILL" value="<bean:write name="GraphForm" property="CHART_FILL"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="LEGEND_BORDER">
<tr>
	<td>
		<input type="hidden" name="LEGEND_BORDER" value="<bean:write name="GraphForm" property="LEGEND_BORDER"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="LEGEND_VERTICAL">
<tr>
	<td>
		<input type="hidden" name="LEGEND_VERTICAL" value="<bean:write name="GraphForm" property="LEGEND_VERTICAL"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="LEGEND_POSITION">
<tr>
	<td>
		<input type="hidden" name="LEGEND_POSITION" value="<bean:write name="GraphForm" property="LEGEND_POSITION"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="LEGEND_FILL">
<tr>
	<td>
		<input type="hidden" name="LEGEND_FILL" value="<bean:write name="GraphForm" property="LEGEND_FILL"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="PIECHART_3D">
<tr>
	<td>
		<input type="hidden" name="PIECHART_3D" value="<bean:write name="GraphForm" property="PIECHART_3D"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="WIDTH">
<tr>
	<td>
		<input type="hidden" name="WIDTH" value="<bean:write name="GraphForm" property="WIDTH"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="HEIGHT">
<tr>
	<td>
		<input type="hidden" name="HEIGHT" value="<bean:write name="GraphForm" property="HEIGHT"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="LENGTH">
<tr>
	<td>
		<input type="hidden" name="LENGTH" value="<bean:write name="GraphForm" property="LENGTH"/>"/>
	</td>
</tr>				
</logic:present>

<logic:present name="GraphForm" property="LEGEND_TITLE">
<tr>
	<td>
		<input type="hidden" name="LEGEND_TITLE" value="<bean:write name="GraphForm" property="LEGEND_TITLE"/>"/>
	</td>
</tr>				
</logic:present>

</body>
<script>

chart.submit();
</script>
</html:html>