<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:define id = "checkforamflag" name = "AssumptionForm" property = "type"  type = "java.lang.String"/>	
<link rel = "stylesheet" href="styles/content.css" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>

<html:html>
<head>
	<title><bean:message bundle = "pm" key = "common.assumption.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	
<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
<%
 Job_Id = "";
 MSA_Id = "";
String from  = "";
String addendum_id = "";
 Appendix_Id = "";
String chkaddendum = "";

if( request.getAttribute( "Job_Id" ) != null )
	Job_Id = ( String ) request.getAttribute( "Job_Id" ).toString();  

if( request.getAttribute( "MSA_Id" ) != null )
	MSA_Id = ( String ) request.getAttribute( "MSA_Id" ).toString();  

if( request.getAttribute( "from" ) != null )
	from = ( String ) request.getAttribute( "from" ).toString();  

if( request.getAttribute( "addendum_id" ) != null )
	addendum_id = ( String ) request.getAttribute( "addendum_id" ).toString();  

if( request.getAttribute( "Appendix_Id" ) != null )
	Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" ).toString();  

if( request.getAttribute( "Appendix_Id" ) != null )
	Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" ).toString();  

if( request.getAttribute( "chkaddendum" ) != null )
	chkaddendum = request.getAttribute( "chkaddendum" ).toString();  

if( request.getAttribute( "from" ) != null ){
	from = request.getAttribute( "from" ).toString();
	request.setAttribute( "from" , from );
}
%>


	
<%@ include  file="/ActivityMenu.inc" %>
<%@ include file="/CommonResources.inc" %>	
	
</head>


<%  
	if( request.getAttribute( "jobid" ) != null ){
		jobid = request.getAttribute( "jobid" ).toString();
		request.setAttribute( "jobid" , jobid );
	}

	int assumption_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString()); 
	String menuid = (String ) request.getAttribute( "menuid" );
%> 


<%
	String backgroundclass = "Ntexto";
	int i = 0;
	boolean csschooser = true;
	String schdayscheck = null;
	String readonlyclass = "Nreadonlytextodd";	
	String alternate = "colDarkRap";
%>


<body onload ="leftAdjLayers();"> 
<html:form action = "/ManageAssumptionAction">
<html:hidden property = "id"/>
<html:hidden property = "type"/>
<html:hidden property = "addendum_id" />
<html:hidden property = "ref" />
<html:hidden property = "name" />
<html:hidden property = "fromflag" />
<html:hidden property = "dashboardid" />
<html:hidden property = "resourceStatus" />
<html:hidden property = "resourceStatusList" />
<html:hidden property = "activity_Id" />
<html:hidden property = "dashboardid" />
<html:hidden property = "chkaddendum" />
<html:hidden property = "chkdetailactivity" />
<html:hidden property = "appendixName" />
<html:hidden property = "activityName" />
<html:hidden property = "jobName" />
<html:hidden property = "msaName" />
<html:hidden property = "msaId" />
<html:hidden property = "appendixId" />
<html:hidden property = "jobId" />
<html:hidden property = "resourceListPage" />
<html:hidden property = "resourceType" />
<html:hidden property = "resourceListType" />


<%if(request.getAttribute("ActivityManager") == null && !checkforamflag.equals("Activity") && !checkforamflag.equals("pm_resM") && !checkforamflag.equals("pm_resL") && !checkforamflag.equals("pm_resF") && !checkforamflag.equals("pm_resT")){%>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<tr>
    	<td class = "Ntoprow"> 
	    	<table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
	        	<tr align = "center"> 
	        	
	        		<logic:equal name = "AssumptionForm" property = "type" value = "PRM">
		    			<td class = "Ntoprow1" width = "220"><a href = "ActivityHeader.do?function=view&activityid=<%= menuid %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.assumption.backtoactivitydashboard"/></a></td>		
		    		</logic:equal>
			  		<logic:equal name = "AssumptionForm" property = "type" value = "am">
		    			<td class = "Ntoprow1" width = "220"><a href = "ActivityLibraryDetailAction.do?Activitylibrary_Id=<%= menuid %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.assumption.backtoactivity"/></a></td>		
		    		</logic:equal>
		    		
		    		<logic:equal name = "AssumptionForm" property = "type" value = "Activity">
		    			<!-- <td class = "toprow1" width = "150"><a href = "ActivityDetailAction.do?addendum_id=<bean:write name = "AssumptionForm" property = "addendum_id" />&ref=<bean:write name = "AssumptionForm" property = "ref" />&Activity_Id=<%= menuid %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "drop"><bean:message bundle = "pm" key = "common.assumption.backtopmactivity"/></a></td>	-->	
		    			<td class = "Ntoprow1" width = "150">
		    				<a href= "javascript:history.go(-1);" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.sow.backtopmactivity"/></a></td>
		    			
		    		</logic:equal>
			  		
			  		<logic:equal name = "AssumptionForm" property = "type" value = "M">
		    			<td class = "Ntoprow1" width = "220"><a href = "MaterialResourceDetailAction.do?Material_Id=<%= menuid %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.assumption.backtomaterial"/></a></td>		
		    		</logic:equal>
		    		
		    		<logic:equal name = "AssumptionForm" property = "type" value = "L">
		    			<td class = "Ntoprow1" width = "220"><a href = "LaborResourceDetailAction.do?Labor_Id=<%= menuid %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.assumption.backtolabor"/></a></td>		
		    		</logic:equal>
		    		
		    		<logic:equal name = "AssumptionForm" property = "type" value = "F">
		    			<td class = "Ntoprow1" width = "220"><a href = "FreightResourceDetailAction.do?Freight_Id=<%= menuid %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.assumption.backtofreight"/></a></td>		
		    		</logic:equal>
		    			    		
		    		<logic:equal name = "AssumptionForm" property = "type" value = "T">
		    			<td class = "Ntoprow1" width = "220"><a href = "TravelResourceDetailAction.do?Travel_Id=<%= menuid %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.assumption.backtotravel"/></a></td>		
		    		</logic:equal>
		    			    		
		    			<td class = "Ntoprow1" width = "700">&nbsp;&nbsp;</td>
	        	</tr>
	        	
	      	</table>
    	</td>
	</tr>
</table>
<%}%>
 <%if(request.getAttribute("ActivityManager") != null && request.getAttribute("ActivityManager").equals("AM")) { %>
 		<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
			<tr>
		    	<td class = "Ntoprow"> 
			    	<table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
			        <tr>
		       			<td width ="900" class ="Ntoprow1">&nbsp;</td>
		       		</tr>
		        <tr>
		        	<td background="images/content_head_04.jpg" height="21" >
			         <div id="breadCrumb">
						 <a href="#" class="bgNone">Activity Manager</a>
			         	 <a href="ActivitySummaryAction.do?ref=View&MSA_Id=<bean:write name = "AssumptionForm" property = "msaId"/>" background="none;"><bean:write name = "AssumptionForm" property = "msaName"/></a>
			         	 <a href="ManageActivityAction.do?ref=View&type=AM&MSA_Id=<bean:write name = "AssumptionForm" property = "msaId"/>" background="none;">View Activities</a>
			         	 <a href = "ActivityLibraryDetailAction.do?Activitylibrary_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>" background="none;"><bean:write name = "AssumptionForm" property = "activityName"/></a>
			         	 <a href = "AMResourceLibraryListAction.do?ref=view&Activitylibrary_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&MSA_Id=<bean:write name = "AssumptionForm" property = "msaId"/>" background="none;"><bean:write name = "AssumptionForm" property = "name"/></a>
						 <a><span class="breadCrumb1">Manage Assumption</span></a>
					</div>
			    	</td>
				</tr>
			      	</table>
		    	</td>
			</tr>
		</table>
 <%} %>
<logic:equal name ="AssumptionForm" property ="resourceListPage" value ="resourcelistpage">
<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
	<c:if test="${requestScope.jobType eq 'inscopejob'}">
		<%@ include  file="/Menu_ProjectJob.inc" %>
	</c:if>
	<c:if test="${requestScope.jobType eq 'Default'}">
		<%@ include  file="/Menu_DefaultJob.inc" %>
	</c:if>
	<c:if test="${requestScope.jobType eq 'Addendum'}">
		<%@ include  file="/Menu_Addendum.inc" %>
	</c:if>
</c:if>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
    		<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
				<c:if test="${requestScope.jobType ne 'inscopejob' && requestScope.jobType ne 'Default' && requestScope.jobType ne  'Addendum'}">
				 <tr>
		          <!-- Sub Navigation goes into this td -->
		          	  <%if( from != null && from != ""){%>
		          	  <!-- from jobdashboard:start -->
				          <td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>All</center></a></td>
				          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=L&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Labor</center></a></td>
 						  <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=M&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Material</center></a></td>
				          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=T&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Travel</center></a></td>
				          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=F&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Freight</center></a></td>
			          <!-- from jobdashboard:end-->
			          <%}else{ %>
			          <!-- from project manager:start -->
						          <td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>All</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=L&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Labor</center></a></td>
 								  <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=M&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Material</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=T&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Travel</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=F&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Freight</center></a></td>
					          <!-- from project manager:start -->
			          <%} %>
			          <td  id="pop5" width="600" class="headerrow" height="19">&nbsp;</td>
			      <!-- End of Sub Navigation -->
		         <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <%if( from != null && from != ""){%>
			             	<div id="breadCrumb"><a class ="bgNone" href="AppendixHeader.do?function=view&fromPage=appendix&viewjobtype=ION&&msaId=<bean:write name = "AssumptionForm" property = "msaId" />&appendixid=<bean:write name = "AssumptionForm" property = "appendixId" />"><bean:write name = "AssumptionForm" property = "appendixName"/></a>
			          				<a href="JobDashboardAction.do?function=view&jobid=<bean:write name = "AssumptionForm" property = "jobId" />&appendixid=<bean:write name = "AssumptionForm" property = "appendixId" />"><bean:write name = "AssumptionForm" property = "jobName"/></a>
			          		</div>
						<%}else{%>
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "AssumptionForm" property = "msaId" />">
			          		<bean:write name = "AssumptionForm" property = "msaName"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "AssumptionForm" property = "appendixId" />">
			   				<bean:write name = "AssumptionForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "AssumptionForm" property = "jobId" />">
			       			<bean:write name = "AssumptionForm" property = "jobName"/></A>
			       			<!-- <A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=<bean:write name = "AssumptionForm" property = "resourceType" />&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id" />"> -->
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=A&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id" />">
			       			<bean:write name = "AssumptionForm" property = "activityName"/></A>
			       		<%} %>
			       		</td>	
			          	</div>  
						    
	 				</tr>
	 			</c:if>
	 			<c:if test="${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Default' || requestScope.jobType eq 'Addendum'}">
					<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
											 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
											<a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
											  <a><span class="breadCrumb1">Assumptions 
											  </a>
					    </td>
					</tr>
			        <tr><td height="8" ></td></tr>	
			     </c:if>
			   </c:if>
			   <c:if test="${requestScope.appendixStatus eq 'pmAppendix'}">
			   		 <tr>
		          <!-- Sub Navigation goes into this td -->
		          	  <%if( from != null && from != ""){%>
		          	  <!-- from jobdashboard:start -->
				          <td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>All</center></a></td>
				          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=L&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Labor</center></a></td>
 						  <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=M&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Material</center></a></td>
				          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=T&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Travel</center></a></td>
				          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=F&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&from=<%=from%>&jobid=<%=jobid%>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Freight</center></a></td>
			          <!-- from jobdashboard:end-->
			          <%}else{ %>
			          <!-- from project manager:start -->
						          <td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=A&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>All</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=L&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Labor</center></a></td>
 								  <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=M&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Material</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=T&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Travel</center></a></td>
						          <td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "ResourceListAction.do?resourceListType=F&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id"/>&ref=<%=refForResource %>" class = "menufont" style="width: 120px"><center>Freight</center></a></td>
					          <!-- from project manager:start -->
			          <%} %>
			          <td  id="pop5" width="600" class="headerrow" height="19">&nbsp;</td>
			      <!-- End of Sub Navigation -->
		         <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <%if( from != null && from != ""){%>
			             	<div id="breadCrumb"><a class ="bgNone" href="AppendixHeader.do?function=view&fromPage=appendix&viewjobtype=ION&&msaId=<bean:write name = "AssumptionForm" property = "msaId" />&appendixid=<bean:write name = "AssumptionForm" property = "appendixId" />"><bean:write name = "AssumptionForm" property = "appendixName"/></a>
			          				<a href="JobDashboardAction.do?function=view&jobid=<bean:write name = "AssumptionForm" property = "jobId" />&appendixid=<bean:write name = "AssumptionForm" property = "appendixId" />"><bean:write name = "AssumptionForm" property = "jobName"/></a>
			          		</div>
						<%}else{%>
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "AssumptionForm" property = "msaId" />">
			          		<bean:write name = "AssumptionForm" property = "msaName"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "AssumptionForm" property = "appendixId" />">
			   				<bean:write name = "AssumptionForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "AssumptionForm" property = "jobId" />">
			       			<bean:write name = "AssumptionForm" property = "jobName"/></A>
			       			<!-- <A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=<bean:write name = "AssumptionForm" property = "resourceType" />&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id" />"> -->
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=A&Activity_Id=<bean:write name = "AssumptionForm" property = "activity_Id" />">
			       			<bean:write name = "AssumptionForm" property = "activityName"/></A>
			       		<%} %>
			       		</td>	
			          	</div>  
						    
	 				</tr>
			   </c:if>
	 				<tr>
					   <td colspan="7"><h2><bean:message bundle = "pm" key = "pm.resource.assumption.label"/><bean:write name = "AssumptionForm" property = "name" /></h2></td>  	    
				    </tr>
	 				
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" >
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>

<logic:equal name ="AssumptionForm" property ="type" value ="Activity">
<logic:equal name ="AssumptionForm" property ="ref" value ="detailactivity">


<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        <%if(  jobtype.equals( "Default" ) ||  jobtype.equals( "Addendum" ) ) { %>
						<%if(activityStatus.equals("Approved")){%>
							<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditapprovedactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
						<%}%>
						<%if(activityStatus.equals("Draft")){%>
				        				<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
						<%}%>
		        	<%}else{%>
						<%if(activityType.equals("Overhead")){%>
					  		<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditoverheadactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
				        	<%}else{%>
						   	<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
				        	<%}%>
		        	<%}%>
		        	<td id = "pop2" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><CENTER>Manage</CENTER></a></td>
		        	<td id = "pop3" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><CENTER>Resources</CENTER></a></td>
					<td id = "pop4" width = "840" class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        
			        </tr>
			        <tr>
				         <td background="images/content_head_04.jpg" height="21" colspan="23">
				          	<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
					          	<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "AssumptionForm" property = "msaId" />">
				  				<bean:write name = "AssumptionForm" property = "msaName"/></a>
				  				<A href="JobUpdateAction.do?ref=<%=refForJob %>&Appendix_Id=<bean:write name = "AssumptionForm" property = "appendixId" />">
				  				<bean:write name = "AssumptionForm" property = "appendixName"/></A>
				  				<A href="ActivityUpdateAction.do?ref=<%=chkaddendum %>&Job_Id=<bean:write name = "AssumptionForm" property = "jobId" />">
				  				<bean:write name = "AssumptionForm" property = "jobName"/></A>
				          </div>
		          		</td>
  					</tr>
  					
	 				<tr>
	 					<td colspan ="7"><h2><bean:message bundle = "pm" key = "common.addcommentpage.activity"/>&nbsp;<bean:message bundle = "pm" key = "common.assumption.Assumption"/>:&nbsp;<bean:write name = "AssumptionForm" property = "activityName" /></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>

<logic:notEqual name ="AssumptionForm" property ="ref" value ="detailactivity">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19"><a href= "javascript:history.go(-1);" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.sow.backtopmactivity"/></a></td></tr>
					<tr><td background="images/content_head_04.jpg" height="21" width="100%">&nbsp;</td></tr>
					<tr>
	 					<td colspan ="7"><h2><bean:message bundle = "pm" key = "common.addcommentpage.activity"/>&nbsp;<bean:message bundle = "pm" key = "common.assumption.Assumption"/>:&nbsp;<bean:write name = "AssumptionForm" property = "activityName" /></h2></td>
	 				</tr>
					
					
</table>
</logic:notEqual>

</logic:equal>

<logic:equal name ="AssumptionForm" property ="type" value ="Activity">
	<logic:equal name ="AssumptionForm" property ="ref" value ="detailactivity">
		<%@ include  file="/ActivityMenuScript.inc" %>
	</logic:equal>
</logic:equal>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<tr>
		<td>&nbsp;</td>
	  	<td>
	  		<table border = "0" cellspacing = "1" cellpadding = "0"  width="450">
				<logic:present name = "deleteflag" scope = "request">
    				<logic:equal name = "deleteflag" value = "0">
    					<tr> <td>&nbsp;</td>
    						<td  colspan = "5" class = "message" height = "30">
    							<bean:message bundle = "pm" key = "common.assumption.delete.success"/>
    						</td>
    					</tr>
    					
    				</logic:equal>
    			
    				<logic:notEqual name = "deleteflag" value = "0">
    					<tr> <td>&nbsp; </td>
    						<td colspan = "5" class = "message" height = "30">
    							<bean:message bundle = "pm" key = "common.assumption.delete.failure"/>
    						</td>
    					</tr>
    				</logic:notEqual>
    			</logic:present>
    			
    			<logic:present name = "addflag" scope = "request">
	    			<logic:equal name = "addflag" value = "0">
	    				<tr> <td>&nbsp; </td>
			    			<td  colspan = "5" class = "message" height = "30">
			    				<bean:message bundle = "pm" key = "common.assumption.add.success"/>
			    			</td>
	    				</tr>
	    			</logic:equal>
	    			
	    			<logic:notEqual name = "addflag" value = "0">
    					<tr> <td>&nbsp; </td>
    						<td colspan = "5" class = "message" height = "30">
    							<bean:message bundle = "pm" key = "common.assumption.add.failure"/>
    						</td>
    					</tr>
    				</logic:notEqual>
    			</logic:present>
    			
    			<logic:present name = "updateflag" scope = "request">
			    	<logic:equal name = "updateflag" value = "0">
						<tr> <td>&nbsp; </td>
							<td  colspan = "5" class = "message" height = "30">
								<bean:message bundle = "pm" key = "common.assumption.update.success"/>
							</td>
						</tr>
		    		</logic:equal>
		    		
		    		<logic:notEqual name = "updateflag" value = "0">
						<tr> <td>&nbsp; </td>
							<td  colspan = "5" class = "message" height = "30">
								<bean:message bundle = "pm" key = "common.assumption.update.failure"/>
							</td>
						</tr>
		    		</logic:notEqual>
    			</logic:present>
    			
				<% if(assumption_size != 0) { %>
				<tr> 
	    			<logic:equal name = "AssumptionForm" property = "type" value = "am">
	    				<td>
			 				<img src = "images/spacer.gif" width = "2" height = "0">
 						</td>
	    				<td class = "Nlabeleboldwhite"  height = "30">
	    					<bean:message bundle = "pm" key = "common.assumption.activity.label"/>
	    				</td>
	    			</logic:equal>
		    	</tr>
		    	<%} %>
		    	<tr>
		    			
	    			<logic:equal name = "AssumptionForm" property = "type" value = "M">
	    				<td>
			 				<img src = "images/spacer.gif" width = "2" height = "0">
 						</td>
	    				<td class = "Nlabeleboldwhite"  height = "30">
	    					<bean:message bundle = "pm" key = "common.assumption.material.label"/> <bean:write name = "AssumptionForm" property = "name" />
	    				</td>
	    			</logic:equal>
	    			
	    			<logic:equal name = "AssumptionForm" property = "type" value = "L">
	    				<td>
			 				<img src = "images/spacer.gif" width = "2" height = "0">
 						</td>
	    				<td class = "Nlabeleboldwhite"  height = "30">
	    					<bean:message bundle = "pm" key = "common.assumption.labor.label"/> <bean:write name = "AssumptionForm" property = "name" />
	    				</td>
	    			</logic:equal>
	    		
	    					
	    			
	    			<logic:equal name = "AssumptionForm" property = "type" value = "F">
	    				<td>
			 				<img src = "images/spacer.gif" width = "2" height = "0">
 						</td>
	    				<td class = "Nlabeleboldwhite"  height = "30">
	    					<bean:message bundle = "pm" key = "common.assumption.freight.label"/> <bean:write name = "AssumptionForm" property = "name" />
	    				</td>
	    			</logic:equal>
	    			<logic:equal name = "AssumptionForm" property = "type" value = "T">
	    				<td>
			 				<img src = "images/spacer.gif" width = "2" height = "0">
 						</td>
	    				<td class = "Nlabeleboldwhite"  height = "30">
	    					<bean:message bundle = "pm" key = "common.assumption.travel.label"/> <bean:write name = "AssumptionForm" property = "name" />
	    				</td>
	    			</logic:equal>
				</tr> 
				<%  	
				if( checkforamflag.equals( "am"  ) || checkforamflag.equals( "Activity"  ) )
				{ if( assumption_size == 0 ) {
				%>
					<tr> 
			    		<td colspan = "5" class = "message" height = "30">
    						<bean:message bundle = "pm" key = "common.assumption.noassumption"/>
    					</td>
				</tr>
				
				<%}} else { %>
					<tr> 
			    		<td class = "formCaption" colspan = "3">
			    			<bean:message bundle = "pm" key = "common.assumption.Assumption"/>
			    		</td>
				</tr>
				
				<%}%>
				
				<tr>
					<td class = "<%= backgroundclass %>" colspan = "2" width="20px">
			   			<html:select property="defaultAssumption" styleId="myId" onchange="putValueToTextarea();" styleClass = "select">
			   			 	<html:optionsCollection  property = "defaultAssumptionList" value = "value" label = "label"/>
			   			</html:select>
			   		</td>
				</tr>

  
<logic:present name = "assumptionlist" scope = "request" > 
	<logic:iterate id = "ms" name = "assumptionlist">
	
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass = "colLight";
				csschooser = false;
				alternate = "colLight";
			}
	
			else
			{
				csschooser = true;	
				backgroundclass = "colLight";
				alternate = "colLight";
			}
		%>
				<tr>
					<% if( !( checkforamflag.equals( "am"  ) || checkforamflag.equals( "Activity"  ) ) )
					{  %>	
					<td class = "colDark">
						<html:multibox property = "check">
							<bean:write name = "ms" property = "assumption_Id"/>
						</html:multibox>
    				</td>
    				
    				<html:hidden  name = "ms" property = "assumption_Id"/>
    				<% }else {%>
						
					
		<%  }
		 
			if( assumption_size == 1 )
			{
				schdayscheck = "javascript: document.forms[0].check.checked = true;";	
			}
			else if( assumption_size > 1 )
			{
				schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;";
			}
		%>				
					<% if( !( checkforamflag.equals( "am"  ) || checkforamflag.equals( "Activity"  ) ) )
					{  %>
						<td class = "<%= backgroundclass %>" colspan = "2">
			   				<html:textarea styleId = '<%= "assumption"+i%>' name = "ms"  property = "assumption" rows = "10" cols = "120" styleClass = "textarea" onclick = "<%= schdayscheck %>" />
			   			
			   			</td>
			   		<% } else {%>
			   		<td class = "<%=alternate%>" colspan="3">
			   			<bean:write name = "ms" property = "assumption" />   				
			   		</td>
			   			<%} %>
		   		</tr>
		   	 <% i++;  %>
	</logic:iterate>
</logic:present>

		<%	
		  	if( assumption_size != 0 )
		  	{
		  		if( assumption_size % 2 == 1 )
		  		{
		  			backgroundclass = "colLight";
		  			i = 2;
				}
		  		else
		  		{
		  			backgroundclass = "colLight";
		  			i = 1;
		  		}
		  	}
		  	else
			{
				backgroundclass = "colLight";
				i = 1;
			}
	    %>	
				
				
				<% if( !( checkforamflag.equals( "am"  ) || checkforamflag.equals( "Activity"  ) ) )
					{  %>
				<tr> 
		    		<td class = "colDark"> 
		    			<html:checkbox property = "newassumption_Id" styleId="asdf" />
		   			</td>
		   			
		   			<td class = "<%= backgroundclass %>"colspan = "2">
		   				<html:textarea styleId = "newassumption" property = "newassumption" rows = "10" cols = "120" styleClass = "textarea" onclick = "document.forms[0].newassumption_Id.checked = true;" />
		   			</td>
				</tr>
				

				<%}  if( assumption_size > 0 )
				{
					if( !( checkforamflag.equals( "am"  ) || checkforamflag.equals( "Activity"  ) ) )
					{  
					
				%>				
					<tr>
			  			<td class = "colLight" align="center" colspan="3">
			  				<html:submit property = "function" styleClass = "button_c" onclick = "return validate( 'update' );">
			  					<bean:message bundle = "pm" key = "common.assumption.submit"/>
			  				</html:submit>
						
							<!-- <% if( request.getAttribute( "from" ) != null && request.getAttribute( "from" ) != "" ) { %>
 		  						<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
 		  					<%} %> -->
						
			  				<html:submit property = "function" styleClass = "button_c" onclick = "return validate( 'delete' );">
			  					<bean:message bundle = "pm" key = "common.assumption.delete"/>
			  				</html:submit>
							<html:reset property = "cancel" styleClass = "button_c">
								<bean:message bundle = "pm" key = "common.assumption.cancel"/>
							</html:reset>
						</td>
					</tr>
				<%
				}}
				else
				{
					if( !( checkforamflag.equals( "am"  ) || checkforamflag.equals( "Activity"  ) ) )
					{ 
				%>
				<tr>
		  			<td class = "colLight" align="center" colspan="3">
		  				<html:submit property = "function" styleClass = "button_c" onclick = "return validate( 'update' );">
		  					<bean:message bundle = "pm" key = "common.assumption.submit"/>
		  				</html:submit>
		  				
		  				<!-- <% if( request.getAttribute( "from" ) != null && request.getAttribute( "from" ) != "") { %>
 		  					<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
 		  				<%} %> -->
						<html:reset property = "cancel" styleClass = "button_c">
							<bean:message bundle = "pm" key = "common.assumption.cancel"/>
						</html:reset>
					</td>
			   </tr>
			    <% 
				}}
				%>			
				
					
			</table>
		</td>
	</tr>
</table>
<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
	<c:if test = "${requestScope.jobType eq 'Addendum'}">
	
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
		<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/DefaultJobMenuScript.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
</c:if>
</html:form>
</body>

<script>

function putValueToTextarea(){
	var value = document.getElementById("myId").value;
	if(document.getElementById("myId").value=="Not listed"){
		value = "";
	}
	document.getElementById("newassumption").value = value;
	document.getElementById("asdf").checked = true;
	//document.getElementById("asdf").check = true;
}

function validate( temp )
{
	var submitflag = 'false';
	if( <%= assumption_size %> != 0 )
	{
		if( <%= assumption_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newassumption_Id.checked ) )
			{
				if( temp == 'update' )
				{
					alert( "<bean:message bundle = "AM" key = "am.detail.selectassumptionupdate"/>" );
					return false;
				}
				
				else
				{
					alert( "<bean:message bundle = "AM" key = "am.detail.selectassumptiondelete"/>" );
					return false;
				}
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newassumption_Id.checked ) )
		  	{
		  		if( temp == 'update' )
				{
			  		alert( "<bean:message bundle = "AM" key = "am.detail.selectassumptionupdate"/>" );
					return false;
				}
				else
				{
					alert( "<bean:message bundle = "AM" key = "am.detail.selectassumptiondelete"/>" );
					return false;
				}
		  	}
		}
	}
	
	else
	{
		if( !document.forms[0].newassumption_Id.checked )
		{
			alert( "<bean:message bundle = "AM" key = "am.detail.newassumption"/>" );
			return false;
		}
	}
	
	
	if( <%= assumption_size %> != 0 )
	{	
		if( <%= assumption_size %> == 1 )
		{
			if( document.forms[0].check.checked ) 
	  		{
	  			if( document.forms[0].assumption.value == "" )
		  		{
		  			alert( "<bean:message bundle = "AM" key = "am.detail.enterassumptionname"/>" );
 					document.forms[0].assumption.focus();
 					return false;
		  		}
		  		else
			  	{
			  		var temp = document.forms[0].assumption.value;
			  		//if( temp.length > 4000 )
			  		//{	
			  		//	alert( "<bean:message bundle = "pm" key = "common.assumption.maxlength"/>" );
			  		//	document.forms[0].assumption.focus();
 					//	return false;
			  		//}
			  	}
	 		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			if( document.forms[0].assumption[i].value == "" )
		  			{
		  				alert( "<bean:message bundle = "AM" key = "am.detail.enterassumptionname"/>" );
		  				document.forms[0].assumption[i].focus();
 						return false;
		  			}
		  			else
			  		{
			  			var temp = document.forms[0].assumption[i].value;
			  		//	if( temp.length > 4000 )
			  		//	{
			  				
			  		//		alert( "<bean:message bundle = "pm" key = "common.assumption.maxlength"/>" );
			  		//		document.forms[0].assumption[i].focus();
 					//		return false;
			  		//	}
			  		}
		 			
		 		}
		 	}
		 }
 	}

 	if( document.forms[0].newassumption_Id.checked )
 	{
 		if( document.forms[0].newassumption.value == "" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.detail.enterassumptionname"/>" );
 			document.forms[0].newassumption.focus();
 			return false;
 		}
 		
 		else
		{
			var temp = document.forms[0].newassumption.value;
		//	if( temp.length > 4000 )
		//	{	
		//	  alert( "<bean:message bundle = "pm" key = "common.assumption.maxlength"/>" );
		//	  document.forms[0].newassumption.focus();
 		//	  return false;
		//	}
		}		
 	}
	
	return true;
}

function Backactionjob()
{
	if( document.forms[0].type.value == 'pm_resM' )
	{
		document.forms[0].action = "MaterialDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "AssumptionForm" property = "addendum_id" />&ref=<bean:write name = "AssumptionForm" property = "ref" />&Material_Id=<%= menuid %>";
	}
	
	if( document.forms[0].type.value == 'pm_resL' )
	{
		document.forms[0].action = "LaborDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "AssumptionForm" property = "addendum_id" />&ref=<bean:write name = "AssumptionForm" property = "ref" />&Labor_Id=<%= menuid %>";
	}
	
	if( document.forms[0].type.value == 'pm_resF' )
	{
		document.forms[0].action = "FreightDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "AssumptionForm" property = "addendum_id" />&ref=<bean:write name = "AssumptionForm" property = "ref" />&Freight_Id=<%= menuid %>";
	}
	
	if( document.forms[0].type.value == 'pm_resT' )
	{
		document.forms[0].action = "TravelDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "AssumptionForm" property = "addendum_id" />&ref=<bean:write name = "AssumptionForm" property = "ref" />&Travel_Id=<%= menuid %>";
	}
	document.forms[0].submit();
	return true;
}

</script>
<script>
function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;

}
function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}


function leftAdjLayers(){
try{

	if(document.forms[0].type.value=='Activity' && document.forms[0].ref.value =='detailactivity'){
				var layers, leftAdj=0;
				leftAdj =document.body.clientWidth;
		 		leftAdj = leftAdj - 280;
				document.getElementById("filter").style.left=leftAdj;
		}	
	else if (document.forms[0].type.value=='pm_resM' ||document.forms[0].type.value=='pm_resL'|| document.forms[0].type.value=='pm_resF' ||document.forms[0].type.value=='pm_resT' ){
				var layers, leftAdj=0;
				leftAdj =document.body.clientWidth;
		 		leftAdj = leftAdj - 280;
				document.getElementById("filter").style.left=leftAdj;
		}	
	else
	{}		
		
}catch(e)
	{}		
}
</script>
</html:html>