<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><bean:message bundle = "pm" key = "exception.pdfgeneration"/></title>
	<%@ include file = "../Header.inc" %>

	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<%
String Type = "";
String View = "";
String Id = "";
String from_type = "";
String jobId = "";

if( request.getParameter( "Type" ) != null )
	Type = request.getParameter( "Type" );

if( request.getParameter( "View" ) != null )
	View = request.getParameter( "View" );

if( request.getParameter( "Id" ) != null )
	Id = request.getParameter( "Id" );

if( request.getParameter( "from_type" ) != null )
	from_type = request.getParameter( "from_type" );

if( request.getParameter( "jobId" ) != null )
	jobId = request.getParameter( "jobId" );

%>
<Script language = "JavaScript">
	function generatePDF(){
		document.forms[0].action = "ViewAction.do?View=<%=View%>&Type=<%=Type%>&from_type=<%=from_type%>&Id=<%=Id%>&jobId=<%=jobId%>&temp=generatePDF";
		document.forms[0].submit();
		return true;
	}
</Script>

</head>

<body>
<html:form action="/ViewAction">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr><td background="images/content_head_04.jpg" height="21">&nbsp;</td></tr>
				<tr><td><h2>Generate PDF From ILEX</h2></td></tr>
</table>

	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			 <td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "90%"> 
  			 			<logic:present name = "DocumentIdNotFoundException" scope = "request">
  			 				  <tr><td  colspan = "3" class = "message" height = "30"><bean:message bundle = "pm" key = "exception.documentid.notfound"/></td></tr>
  			 			</logic:present>
  			 			<logic:present name = "CouldNotCheckDocumentException" scope = "request">
  			 				<tr><td  colspan = "3" class = "message" height = "30"><bean:message bundle = "pm" key = "exception.couldcotcheckdocument"/></td></tr>
  			 			</logic:present>
  			 			<logic:present name = "DocumentNotExistsException" scope = "request">
  			 				<tr><td  colspan = "3" class = "message" height = "30"><bean:message bundle = "pm" key = "exception.document.notexists"/></td></tr>	
  			 			</logic:present>
  			 			<logic:present name = "Exception" scope = "request">
  			 				<tr><td  colspan = "3" class = "message" height = "30"><bean:message bundle = "pm" key = "exception.general"/></td></tr>	
  			 			</logic:present>
			    		<tr>
			    			<td  colspan = "3" class = "ColLight" height = "30" align = "center">
			    				<input type="button" class="button_c" value="Back" onclick="history.go(-1);"> 
			    				<input type="button" class="button_c" value="Generate PDF From ILEX" onclick="generatePDF();">
			    			</td>
			    		</tr>
			    		
				 </table>
			  </td>
			 </tr>
</table>
</html:form>
</body>
</html:html>