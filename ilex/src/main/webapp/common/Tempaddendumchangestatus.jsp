<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ page language = "java" %>


<%
	String flag = "";
	String Id = "";
	String ref = "";
	String type = "";
	String addendumId = "";
	
	if( request.getAttribute( "copyaddendumflag" ) != null )
	{
		flag = ( String ) request.getAttribute( "copyaddendumflag" );
	}

	if( request.getAttribute( "Appendix_Id" ) != null )
	{
		Id = ( String ) request.getAttribute( "Appendix_Id" );
	}

	if( request.getAttribute( "addendumId" ) != null )
	{
		addendumId = ( String ) request.getAttribute( "addendumId" );
	}

	if( request.getAttribute( "Job_Id" ) != null )
	{
		Id = ( String ) request.getAttribute( "Job_Id" );
	}

	if( request.getAttribute( "Activity_Id" ) != null )
	{
		Id = ( String ) request.getAttribute( "Activity_Id" );
	}

	if( request.getAttribute( "Resource_Id" ) != null )
	{
		Id = ( String ) request.getAttribute( "Resource_Id" );
	}

	if( request.getAttribute( "type" ) != null )
	{
		type = ( String ) request.getAttribute( "type" );
	}

	if( request.getAttribute( "ref" ) != null )
	{
		ref = ( String ) request.getAttribute( "ref" );
	}
%>
	<% if( type.equals( "Job" ) )
		{ 
	%>
		
		<jsp:forward page = "/JobDetailAction.do">
			<jsp:param name = "copyaddendumflag" value = "<%= flag %>" />
			<jsp:param name = "Job_Id" value = "<%= Id %>" />
			<jsp:param name = "ref" value = "<%= ref %>" />
	  		<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' />
	   		<jsp:param name = "changestatusflag" value = '<%= ( String ) request.getAttribute( "changestatusflag" ) %>' />
	   </jsp:forward>

	<%} 
	 if( type.equals( "Activity" ) )
		{ 
	%>
		<jsp:forward page = "/ActivityDetailAction.do">
			<jsp:param name = "copyaddendumflag" value = "<%= flag %>" />
			<jsp:param name = "Activity_Id" value = "<%= Id %>" />
			<jsp:param name = "ref" value = "<%= ref %>" />
			<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' />
			
			<jsp:param name = "changestatusflag" value = '<%= ( String ) request.getAttribute( "changestatusflag" ) %>' />
	   </jsp:forward>
	<%
		}
		if( type.equals( "pm_resM" ) )
		{
	%>
		<jsp:forward page = "/MaterialDetailAction.do">
			<jsp:param name = "copyaddendumflag" value = "<%= flag %>" />
			<jsp:param name = "Material_Id" value = "<%= Id %>" />
			<jsp:param name = "ref" value = "<%= ref %>" />
			<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' />
	   </jsp:forward>
	
	<%
		}
		if( type.equals( "pm_resL" ) )
		{
	%>
		<jsp:forward page = "/LaborDetailAction.do">
			<jsp:param name = "copyaddendumflag" value = "<%= flag %>" />
			<jsp:param name = "Labor_Id" value = "<%= Id %>" />
			<jsp:param name = "ref" value = "<%= ref %>" />
			<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' />
	   </jsp:forward>
	
	<%
		}
		if( type.equals( "pm_resF" ) )
		{
	%>
		<jsp:forward page = "/FreightDetailAction.do">
			<jsp:param name = "copyaddendumflag" value = "<%= flag %>" />
			<jsp:param name = "Freight_Id" value = "<%= Id %>" />
			<jsp:param name = "ref" value = "<%= ref %>" />
			<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' />
	   </jsp:forward>
	
	<%
		}
		if( type.equals( "pm_resT" ) )
		{
	%>
		<jsp:forward page = "/TravelDetailAction.do">
			<jsp:param name = "copyaddendumflag" value = "<%= flag %>" />
			<jsp:param name = "Travel_Id" value = "<%= Id %>" />
			<jsp:param name = "ref" value = "<%= ref %>" />
			<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' />
	   </jsp:forward>
	
	<%
		}
		if( type.equals( "PM" ) )
		{
	%>
		<jsp:forward page = "/JobUpdateAction.do">
			<jsp:param name = "addendumflag" value = "<%= flag %>" />
			<jsp:param name = "ref" value = "View" />
			<jsp:param name = "Appendix_Id" value = "<%= Id %>" />
	   </jsp:forward>
	<%
		}
		if( type.equals( "PRM" ) )
		{
	%>
		<jsp:forward page = "/AppendixHeader.do">
			<jsp:param name = "addendumflag" value = "<%= flag %>" />
			<jsp:param name = "function" value = "view" />
			<jsp:param name = "appendixid" value = "<%= Id %>" />
	   </jsp:forward>
	<%
		}
		if( type.equals( "pm" ) )
		{
	%>
		<jsp:forward page = "/ActivityUpdateAction.do">
			<jsp:param name = "addendumflag" value = "<%= flag %>" />
			<jsp:param name = "ref" value = "View" />
			<jsp:param name = "Job_Id" value = "<%= Id %>" />
	   </jsp:forward>
	<%
		}
		if( type.equals( "AddAddendumActivity" ) )
		{
	%>
	   <jsp:forward page = "/CopyAddendumActivityAction.do">
			<jsp:param name = "Appendix_Id" value = "<%= Id %>" />
			<jsp:param name = "addendumflag" value = "<%= flag %>" />
			<jsp:param name = "from" value = "PRM" />
			<jsp:param name = "Job_Id" value = "<%= addendumId %>" />
	   </jsp:forward>
	<%
		}
	%>
	

	