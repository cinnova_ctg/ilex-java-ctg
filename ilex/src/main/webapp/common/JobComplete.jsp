<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<c:if test="${empty requestScope.throughJobDashboard}">
	<%@ include file="/Header.inc" %>
</c:if>
<link href="/Ilex/styles/style.css" rel="stylesheet" type="text/css" />
<link href="/Ilex/styles/content.css" rel="stylesheet" type="text/css" />
<link href="/Ilex/styles/summary.css" rel="stylesheet" type="text/css" />
<link href="/Ilex/docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="/Ilex/docm/styles/content.css" rel="stylesheet" type="text/css" />
<link href="/Ilex/docm/styles/summary.css" rel="stylesheet" type="text/css" />
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<title>Job Edit Page</title>


<style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
.Ntextoleftalignwrappaleyellowborder4 { 
	font-size: 10px; 
	font-family: 'Verdana', Arial, Helvetica, 'sans-serif';
	font-weight:bold;
	padding: 2px 1px ( left, right );
	color: #000000; 
	background: #FAF8CC;
	border-color : #FFFFFF #BDBFC1 #849DC0 #849DC0;
	border-style : solid;
	border-top-width : 0px;
	border-right-width : 0px;
	border-bottom-width : 0px;
	border-left-width :0px;
	padding-left:0	
	}
	
	.Ntextoleftalignwrappaleyellowborder2 { 
	font-size: 10px; 
	font-family: 'Verdana', Arial, Helvetica, 'sans-serif';
	font-weight:normal;
	padding: 2px 1px ( left, right );
	color: #000000; 
	background: #FAF8CC;
	border-color : #FFFFFF #BDBFC1 #849DC0 #849DC0;
	border-style : solid;
	border-top-width : 0px;
	border-right-width : 0px;
	border-bottom-width : 0px;
	border-left-width :0px;
	padding-left:0	
	}
</style>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<link rel="stylesheet" href="styles/style.css" type="text/css">
<link rel="stylesheet" href="styles/style_common.css" type="text/css">

<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/AddendumMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ProjectJobMenu.inc" %>
</head>
<c:if test="${empty requestScope.throughJobDashboard}">
<%@ include  file="/NMenu.inc" %>
</c:if>
<body>
		<html:form action="JobCompleteAction">
		<html:hidden property = "jobId"/>
		<html:hidden property = "type"/>
		<html:hidden property = "status"/>
		<bean:define id = "job_status" name ="JobCompleteForm" property ="jobStatus"></bean:define> 
		
		<bean:define id = "revenueStatus" name ="JobCompleteForm" property ="revenue"></bean:define>
		<bean:define id = "expenseStatus" name ="JobCompleteForm" property ="expense"></bean:define>
				
		<bean:define id = "jobOverAllStatus" name ="JobCompleteForm" property ="overAllStatus"></bean:define> 	
		<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr><td valign="top" width = "100%">
		 <c:if test="${empty requestScope.throughJobDashboard}">
			 <TABLE height=18 cellSpacing="0" cellPadding="0" width="100%" border=0>
	        	<TBODY>
	        		<TR>
				         <TD class= Ntoprow1 id=pop1 width=120><A class=menufont style="width: 120px" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#"><center>Job</center></A></TD>
				         <TD class=Ntoprow1 id=pop2 width=120><A class=menufont  style="width: 120px" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#"><center>Status Report</center></A></TD>			         
		 				 <td id ="pop3" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
	         		</TR>
	         		<tr>    
						 <tr><td background="images/content_head_04.jpg" height="21" width ="100%" colspan="4">
						 	  <div id="breadCrumb"> 
						 	  	<a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<bean:write name = "JobCompleteForm" property = "msaId" />&app=null"   class="bgNone"><bean:write name = "JobCompleteForm" property = "msaName" /></a>
								<a href="AppendixHeader.do?function=view&appendixid=<bean:write name="JobCompleteForm" property="appendixId"/>&viewjobtype=<bean:write name = "JobCompleteForm" property = "viewjobtype" />&ownerId=<bean:write name = "JobCompleteForm" property = "ownerId" />&msaId=<bean:write name = "JobCompleteForm" property = "msaId" />" ><bean:write name="JobCompleteForm" property="appendixName"/></a>
								<a href ="JobDashboardAction.do?function=view&jobid=<bean:write name = "JobCompleteForm" property = "jobId" />&appendixid=<bean:write name = "JobCompleteForm" property = "appendixId"/>"><bean:write name = "JobCompleteForm" property = "jobName" /></a>
								<a><span class="breadCrumb1">Process CheckList</a>
							 </div>	
						</td>
					</tr>
					<logic:present name="changestatusflag">
						<tr><td colspan = "4" height = "30" class="message">&nbsp;&nbsp;<bean:message bundle = "PRM" key ="prm.job.status.complete"/></td></tr>
					</logic:present>
					<tr><td colspan = "4" height = "30" width="100%"><h2><bean:message bundle = "PRM" key ="job.complete.check"/></h2></td></tr>
	         	</TBODY>
	         </TABLE>
         </c:if>
         <c:if test="${not empty requestScope.throughJobDashboard}">
         	<table>
         		<logic:present name="changestatusflag">
					<tr><td colspan = "4" height = "30" class="message">&nbsp;&nbsp;<bean:message bundle = "PRM" key ="prm.job.status.complete"/></td></tr>
				</logic:present>
				<tr><td colspan = "4"  class = "texthd" style="padding-left: 0px;" width="100%"><h4 style="font-size: 12px;padding-left: 0px;font-color:blue">Complete Job: </h4></td></tr>
         	</table>
         </c:if>
         </td></tr></table>
         <c:if test="${empty requestScope.throughJobDashboard}">  
         	<%@ include  file="/ProjectJobMenuScript.inc" %>
         </c:if>
      
               
                  
			<TABLE  cellSpacing=0 cellPadding=0 width="100%" border =0>
			<tr><td style="padding-left: 10px;" >
			<TABLE cellSpacing=1 cellPadding=1 border=0 class="Ntextoleftalignwrappaleyellowborder2">
					<tr>
						<td class="Ntextoleftalignwrappaleyellowborder4" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item
						</td>
						<td class="Ntextoleftalignwrappaleyellowborder4">Status
						</td>
					</tr>
					<tr><td  class="Ntextoleftalignwrappaleyellowborder4"><bean:message bundle = "PRM" key = "prm.job.planned/onsite/offsitecheck"/></td>
						<logic:equal name = "JobCompleteForm" property ="scheduleCheck" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.failed"/></td>
						</logic:equal>
						<logic:notEqual name = "JobCompleteForm" property ="scheduleCheck" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.passed"/></td>
						</logic:notEqual>
					</tr>
					<logic:equal name ="JobCompleteForm" property = "scheduleCheck" value ="Failed">
							<tr><td  class="Ntextoleftalignwrappaleyellowborder2" colspan="2" width="700"><bean:message bundle = "PRM" key = "prm.job.planned/onsite/offsitecheck.failed"/></td></tr>
					</logic:equal>	
					<tr><td class="Ntextoleftalignwrappaleyellowborder4"><bean:message bundle = "PRM" key = "prm.job.Installtion.Notes"/></td>
						<logic:equal name = "JobCompleteForm" property ="installNotesCheck" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.failed"/></td>
						</logic:equal>
						<logic:notEqual name = "JobCompleteForm" property ="installNotesCheck" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.passed"/></td>
						</logic:notEqual>
					</tr>
					<logic:equal name ="JobCompleteForm" property = "installNotesCheck" value ="Failed">
							<tr><td  class="Ntextoleftalignwrappaleyellowborder2" colspan="2" width="700"><bean:message bundle = "PRM" key = "prm.job.Installtion.Notes.failed"/></td></tr>
					</logic:equal>	
					<tr><td class="Ntextoleftalignwrappaleyellowborder4"><bean:message bundle = "PRM" key = "prm.job.customer.reference"/></td>
						<logic:equal name = "JobCompleteForm" property ="customerReferenceCheck" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.failed"/></td>
						</logic:equal>
						<logic:notEqual name = "JobCompleteForm" property ="customerReferenceCheck" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.passed"/></td>
						</logic:notEqual>
					</tr>
					<logic:equal name ="JobCompleteForm" property = "customerReferenceCheck" value ="Failed">
							<tr><td  class="Ntextoleftalignwrappaleyellowborder2" colspan="2" width="700"><bean:message bundle = "PRM" key = "prm.job.customer.reference.failed"/></td></tr>
					</logic:equal>
					<tr><td class="Ntextoleftalignwrappaleyellowborder4"><bean:message bundle = "PRM" key = "prm.job.outstanding.delieverables"/></td>
						<logic:equal name = "JobCompleteForm" property ="outStandingDelieverableCheck" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.failed"/></td>
						</logic:equal>
						<logic:notEqual name = "JobCompleteForm" property ="outStandingDelieverableCheck" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.passed"/></td>
						</logic:notEqual>
					</tr>
					<logic:equal name ="JobCompleteForm" property = "outStandingDelieverableCheck" value ="Failed">
							<tr><td colspan = "2" class="Ntextoleftalignwrappaleyellowborder2" width="700"><bean:message bundle = "PRM" key = "prm.job.outstanding.delieverables.failed"/></td></tr>
					</logic:equal>
						
					<tr><td class="Ntextoleftalignwrappaleyellowborder4"><bean:message bundle = "PRM" key = "prm.job.financial.information"/></td>
						<td class="Ntextoleftalignwrappaleyellowborder2">
							<c:choose>
								<c:when test="${not empty requestScope.financialFlag and requestScope.financialFlag eq 'Y'}">
									<bean:message bundle = "PRM" key = "prm.job.passed"/>
								</c:when>
								<c:otherwise>
									<%if(revenueStatus.equals("Passed") || expenseStatus.equals("Passed")){ %>
										<bean:message bundle = "PRM" key = "prm.job.passed"/>
									<%}else{%>
										<bean:message bundle = "PRM" key = "prm.job.failed"/>
									<%}%>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr><td class="Ntextoleftalignwrappaleyellowborder4">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.job.revenue"/></td>
						<logic:equal name = "JobCompleteForm" property ="revenue" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.failed"/></td>
						</logic:equal>
						<logic:notEqual name = "JobCompleteForm" property ="revenue" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.passed" /></td>
						</logic:notEqual>
					</tr>
					<logic:equal name ="JobCompleteForm" property = "revenue" value ="Failed">
							<tr><td  class="Ntextoleftalignwrappaleyellowborder2" colspan="2" width="700"><bean:message bundle = "PRM" key = "prm.job.revenue.failed"/></td></tr>
					</logic:equal>	
						
					<tr><td class="Ntextoleftalignwrappaleyellowborder4">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message bundle = "PRM" key = "prm.job.expenses"/></td>
						<logic:equal name = "JobCompleteForm" property ="expense" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.failed"/></td>
						</logic:equal>
						<logic:notEqual name = "JobCompleteForm" property ="expense" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.passed"/></td>
						</logic:notEqual>
					</tr>
					<logic:equal name ="JobCompleteForm" property = "expense" value ="Failed">
							<tr><td  class="Ntextoleftalignwrappaleyellowborder2" colspan="2" width="700"><bean:message bundle = "PRM" key = "prm.job.expenses.failed"/></td></tr>
					</logic:equal>	
						
					<tr><td class="Ntextoleftalignwrappaleyellowborder4"><bean:message bundle = "PRM" key = "prm.job.resource.allocation"/></td>
						<logic:equal name = "JobCompleteForm" property ="resourceAllocation" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.failed"/></td>
						</logic:equal>
						<logic:notEqual name = "JobCompleteForm" property ="resourceAllocation" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.passed"/></td>
						</logic:notEqual>
					</tr>  
					<logic:equal name ="JobCompleteForm" property = "resourceAllocation" value ="Failed">
							<tr><td  class="Ntextoleftalignwrappaleyellowborder2" colspan="2" width="700"><bean:message bundle = "PRM" key = "prm.job.resource.allocation.failed"/></td></tr>
					</logic:equal>	
					
					
					<tr><td class="Ntextoleftalignwrappaleyellowborder4"><bean:message bundle = "PRM" key = "prm.job.site.contact.info"/></td>
						<logic:equal name = "JobCompleteForm" property ="siteContactInfo" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.failed"/></td>
						</logic:equal>
						<logic:notEqual name = "JobCompleteForm" property ="siteContactInfo" value = "Failed">
							<td  class="Ntextoleftalignwrappaleyellowborder2"><bean:message bundle = "PRM" key = "prm.job.passed"/></td>
						</logic:notEqual>
					</tr>  
					<logic:equal name ="JobCompleteForm" property = "siteContactInfo" value ="Failed">
							<tr><td  class="Ntextoleftalignwrappaleyellowborder2" colspan="2" width="700"><bean:message bundle = "PRM" key = "prm.job.site.contact.info.failed"/></td></tr>
					</logic:equal>	
					
					
					
						
					<logic:equal name ="JobCompleteForm" property = "overAllStatus" value ="Passed">
						<tr><td class = "Ntextoleftalignwrappaleyellowborder2" colspan="2" align="left">
						<c:if test="${empty requestScope.throughJobDashboard}">  
							<html:submit property="next" styleClass = "button_c"><bean:message bundle = "PRM" key = "prm.job.next"/></html:submit>
						</c:if>
						<c:if test="${not empty requestScope.throughJobDashboard}">  
							<html:button property="next" styleClass = "button_c" onclick="setupCustomFieldData();">Complete</html:button>						
						</c:if>
						</td></tr>	
					</logic:equal>
					<c:if test="${not empty requestScope.throughJobDashboard}">
						<tr><td class = "Ntextoleftalignwrappaleyellowborder2" colspan="2" align="left">  
							<logic:notEqual name ="JobCompleteForm" property = "overAllStatus" value ="Passed">
								<html:button property="next" styleClass = "button_c" onclick="setupCustomFieldData();" disabled="true">Complete</html:button>						
							</logic:notEqual>
						</td></tr>		
					</c:if>
					</TABLE></td></tr></TABLE>
		
		<logic:present name = "verified" scope = "request">
			<logic:equal name ="JobCompleteForm" property = "overAllStatus" value ="Passed">
				<script>
					//popUpChecklistTab();
				</script> 
			</logic:equal>
		</logic:present>
			
		</html:form>	
		<div id="popupScreen" style="display: none; cursor: default; border: 1px solid #aaaaaa">
			       <div style="border: 0px;">
			       	<div class="headerpaleyellow" style="background-color: #faf8cc;">
			       	<div style="float: left;padding:5px;" class="PCSTD0001" id="pageTitle"></div>
			       	<div style="text-align: right;padding:5px;cursor: pointer"><img
			                                              height="15" width="15" alt="Close"
			                                              src="images/delete.gif"
			                                              id="close" onclick="closeWindow();">
			           </div>
			       	
			       		
			       	</div>	                 
			       </div>
				       <div id="popupBody"
				                 style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;text-align: left;">
				                 <img
				                           src="images/waiting_tree.gif" />
				       </div>
  		 </div>
  		 
  		
</body>
<!-- load jQuery 1.3.2 -->
<script type="text/javascript" src="javascript/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="javascript/jquery.blockUI.js"></script>
<script>
	function submitJobForCompletion()
	{		
		var url = "./JobCompleteAction.do?jobid=<c:out value='${JobCompleteForm.jobId}'/>&jobStatus=<c:out value='${JobCompleteForm.jobStatus}'/>&status=<c:out value='${JobCompleteForm.status}'/>&Type=prj_job&next=next&throughJobDashboard=Y";
		document.forms[0].action = url;
		document.forms[0].submit();
	}
	
	function setupCustomFieldData() {
		var msaName="${JobCompleteForm.msaName}";
		var jobStatus='${JobCompleteForm.jobStatus}';
		if(msaName!="XO Communications")
			{
				$.blockUI({
					message :jQuery('#popupScreen'),
					css : {
						width : 'auto',
						height : 'auto',
						position : 'absolute',
						color : 'black'
					},
					overlayCSS : {
						backgroundColor : '#f0f0f0',
						opacity : 0.3
					},
					centerX : true,
					centerY : true,
					fadeIn : 0,
					fadeOut : 0
				});
				
				jQuery("#pageTitle").html(
						"Customer Required Data");
				var url='./CustomerRequireDataAction.do?jobid=<c:out value='${JobCompleteForm.jobId}'/>&jobStatus=<c:out value='${JobCompleteForm.jobStatus}'/>&status=<c:out value='${JobCompleteForm.status}'/>&appendixId=<c:out value='${JobCompleteForm.appendixId}'/>&msaId=<c:out value='${JobCompleteForm.msaId}'/>';
				jQuery("#popupBody").load(url,function(){
					jQuery('div.blockMsg').css('left','5%');
				});
				
			}
		else{
			submitJobForCompletion();
			}
	}
	function closeWindow() {
		jQuery.unblockUI();
		jQuery("#popupBody").empty().html(
				'<img src="images/waiting_tree.gif" />');
		jQuery("#pageTitle").innerHTML = '';
	}
</script>
</html>