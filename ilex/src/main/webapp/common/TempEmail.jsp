<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ page language = "java" %>


<%
	String act_id = "";
	String job_id = "";
	String from_type = ""; 
	String id = "";
	String type = "";
	String docMError = "";

	if( request.getAttribute( "job_id" ) != null )
	{
		job_id = ( String ) request.getAttribute( "job_id" );
	}

	if( request.getAttribute( "act_id" ) != null )   
	{
		act_id = ( String ) request.getAttribute( "act_id" );
		
	}

	if( request.getAttribute( "id" ) != null )
	{
		id = ( String ) request.getAttribute( "id" );
	}

	if( request.getAttribute( "from_type" ) != null )
	{
		from_type = ( String ) request.getAttribute( "from_type" );
	}

	if( request.getAttribute( "type" ) != null )
	{
		type = ( String ) request.getAttribute( "type" );
	}
	if(act_id.indexOf("~") >= 0) act_id = act_id.substring(0, act_id.indexOf("~")); 
	
	if(request.getAttribute("docMError")!=null){
		docMError = ( String ) request.getAttribute( "docMError" );
	}
	
%>

<% 	if( from_type.equals( "POWODashboard" ) )
	{%>	
	<jsp:forward page="/POWOAction.do">
			<jsp:param name = "jobid" value = "<%= job_id %>" /> 
			<jsp:param name = "docMError" value = "<%= docMError %>" /> 
		</jsp:forward>
	<%}  
	if( type.equals( "prm_appendix" ) )
	{%>	
	<jsp:forward page="/AppendixHeader.do">
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "appendixid" value = "<%= id %>" /> 
		</jsp:forward>
	<%}

	if( type.equals( "prm_job" ) )
	{%>	
	<jsp:forward page="/JobHeader.do">
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "jobid" value = "<%= id %>" /> 
		</jsp:forward>
	<%}

	if( type.equals( "prm_activity" ) )
	{%>	
	<jsp:forward page="/ActivityHeader.do">
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "activityid" value = "<%= id %>" /> 
		</jsp:forward>
	<%}
	%>


