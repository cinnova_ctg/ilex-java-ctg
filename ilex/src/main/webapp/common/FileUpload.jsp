<!DOCTYPE HTML>
<!--

THIS FILE WILL BE USED AS AN INTERFACE FOR FILE UPLOAD
-->

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:define id = "typeupload" name = "UploadForm" property = "type" />
<bean:define id = "forward_Id" name = "UploadForm" property = "id1" />
<html:html>
<head>

<% 
String path = "";
String appendix_Id = "";
int addRow = -1;
boolean addSpace = false;
int rowHeight = 120;
int ownerListSize = 0;
int i=0;
int j=2;
String checkowner=null;

if(request.getAttribute("ownerListSize") != null)
	ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());


if(request.getAttribute("path")!=null)
{
	path = ""+request.getAttribute("path");
	appendix_Id = ""+request.getAttribute("appendix_Id");
}
%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkstatus = null;
int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


%>


<title>ILEX</title>
<%@ include file = "/Header.inc" %>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<link rel = "stylesheet" href = "styles/style.css" type = "text/css">

	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css">  
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>    
  	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
  	<%@ include file = "/NMenu.inc" %>

</head>
  <%@ include  file="/MSAMenu.inc" %> 
  <%@ include  file="/AppendixMenu.inc" %>            
  <%@ include  file="/JobMenu.inc" %> 
  <%@ include file="/DefaultJobMenu.inc" %>
  <%@ include  file="/DashboardStatusScript.inc" %>	
  
  <%
	
	String addendumStatus ="";
	String job_Status ="";
	String jobViewtype ="";
	
	if(request.getAttribute("Job_Id")!=null)
		Job_Id= request.getAttribute("Job_Id").toString();
		
	if(request.getAttribute("job_Status")!=null)
		job_Status= request.getAttribute("job_Status").toString();
		
	if(request.getAttribute("jobViewtype")!=null)
		jobViewtype= request.getAttribute("jobViewtype").toString();	
	
%>

<script>
function addendumSendEmail() {
	document.forms[0].action = "EmailAction.do?Type=PRMAppendix&Appendix_Id=<%=Appendix_Id%>&jobId=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;	
}	

function addendumManageStatus(v) {
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_job&id=<%=Job_Id%>";
	document.forms[0].submit();
	return true;	
}

function addendumViewContractDocument(id, docType) {
	if(docType=='html')   document.forms[0].target = "_blank";
	else   document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+id+"&View="+docType+"&Type=Appendix&from_type=PRM&Id=<%=Appendix_Id%>";
	document.forms[0].submit();
}

function addendumSendStatusReport() {
	document.forms[0].action = "EmailAction.do?Type=prm_job&Type1=prm_job&&id=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;
}

function addendumViewComments() {
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}

function addendumAddComment() {
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Appendix_Id=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}


</script>
</head>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad ="leftAdjLayers();OnLoad();">
<html:form action = "/UploadAction" target = "ilexmain" enctype = "multipart/form-data">
<html:hidden property = "id1" />
<html:hidden property = "MSA_Id"/>
<html:hidden property = "msaname"/>
<html:hidden property = "type" />
<html:hidden property = "ref" />
<html:hidden property = "ref1" />
<html:hidden property = "addendum_id" />
<html:hidden property = "appendix_Id" />
<html:hidden property = "path" />
<html:hidden property="nettype" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property="otherCheck"/> 
<html:hidden property="go"/>
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property="jobOwnerOtherCheck"/> 


<logic:equal name = "UploadForm" property = "type" value = "MSA">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        	<% if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) || uploadstatus.equals( "U" ) )  
							{boolean check = false;
								if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) ) 
								{check = true;
									if( msastatus.equals( "Signed" ) )
									{
								%>
									<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegestatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}else{
								%>
								<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}}if( uploadstatus.equals( "U" ) &&  !check )
								{%>
									<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegeupload"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}
							} 
							else 
							{ %>
	                 			 <td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
							<% 
							} %>
							<td id = "pop2"  width = "120"  class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="4" width="350">&nbsp;</td>
			        		
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
						<td colspan="7"><h2>MSA&nbsp;<bean:message bundle = "pm" key = "common.fileupload.new.msa"/><bean:write name = "UploadForm" property = "msaname"/></h2></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "selectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<% 
																checkowner = "javascript: checking('"+ownerId+"');"; 
																 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "UploadForm" property = "type" value = "Appendix">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        		<% if( appendixStatus.equals( "Signed" ) || appendixStatus.equals( "Transmitted" ) )
						{
							if( appendixStatus.equals( "Signed" ) )
							{ %>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noeditappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
							<%}
							else
							{%>
								<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
							<%}
						}
						else
						{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%} %>	
		
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
						<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>	
			        
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "UploadForm" property = "MSA_Id"/>"><bean:write name = "UploadForm" property = "msaname"/></a></div>   				
					    </td>
	 				</tr>
	 				<tr>
						<td colspan="7"><h2>Appendix&nbsp;<bean:message bundle = "pm" key = "common.fileupload.new.appendixheader"/>&nbsp;<bean:write name = "UploadForm" property = "appendixName"/></h2></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<% 
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "UploadForm"  property = "type" value = "Job">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        <%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>
			        </tr>
			        <tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "UploadForm" property = "MSA_Id"/>&Appendix_Id=<bean:write name = "UploadForm" property = "appendix_Id"/>"><bean:write name = "UploadForm" property = "msaname"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "UploadForm" property = "appendix_Id"/>&Name=<bean:write name = "UploadForm" property = "appendixName"/>&ref=<%=chkaddendum%>"><bean:write name = "UploadForm" property = "appendixName"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
						<td colspan="7"><h2>Job&nbsp;<bean:message bundle = "pm" key = "common.fileupload.new.appendixheader"/>&nbsp;<bean:write name = "UploadForm" property = "jobName"/></h2></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "UploadForm" property = "type" value = "Appendixdashboard">
<logic:notEqual name = "UploadForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr> 
		          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage</center></a></td>
		          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
		     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name = "UploadForm" property = "appendix_Id" />&ownerId=<bean:write name = "UploadForm" property = "ownerId" />&msaId=<bean:write name="UploadForm" property="MSA_Id"/>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
		          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
		        </tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
						<td colspan="4"><h2><bean:message bundle = "pm" key = "common.fileupload.appendixheader"/>&nbsp;<bean:write name = "UploadForm" property = "appendixName"/></h2></td>
					</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					          <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
<%@ include  file="/AppedixDashboardMenuScript.inc" %>
</logic:notEqual>

<logic:equal name = "UploadForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr>
		        	<td colspan = "7" height="1"><h2><bean:message bundle = "pm" key = "common.fileupload.appendixheader"/>&nbsp;<bean:write name = "UploadForm" property = "appendixName"/></h2></td>
		        </tr> 
	      </table>
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        <tr>
	          <td width="155" height="39"  class="headerrow" colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" border="0">
		              <tr>
		                <td width="150">
		                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
			                   	<tr>
						            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
			                    </tr>
		                  	</table>
		                </td>
		              </tr>
		          </table>
	          </td>
	        </tr>
	        <tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>	
	                			<span>
									<div id="filter" class="divstyle">
					        			<table width="250" cellpadding="0" class="divtable">
						                     <tr>
					                            <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
															 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																		<bean:write name ="list" property = "statusid"/>
															 		</html:multibox>   
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="UploadForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table></td>
		                      			    	<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
			                      			    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												         <span id = "OwnerSpanId">
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
																	
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
															 			</html:multibox>   
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>											
						            </div>
				                </span>
							 </td>
	              		</tr>
            		</table>
            	</td>
        	</tr>
	    </table>
	</td>
 </tr>
</table>
<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
</logic:equal>




</logic:equal>
<logic:equal name = "UploadForm" property = "type" value = "Jobdashboard">
<table border="0" cellspacing="1" cellpadding="0" width="100%">
	<tr>
        	<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
	<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
	<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
	</tr>
	<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
	         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
				 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
				 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
				<a ><span class="breadCrumb1">Upload</a>
	    </td>
	</tr>
       <tr><td height="5" >&nbsp;</td></tr>	
</table>
</logic:equal>
   		<logic:equal name = "UploadForm" property = "type" value = "MSA">
   			<%@ include  file="/MSAMenuScript.inc" %>
   		</logic:equal>
   		
   		<logic:equal name = "UploadForm" property = "type" value = "Appendix">
   			<%@ include  file="/AppendixMenuScript.inc" %>
   		</logic:equal>
		
		<logic:equal name = "UploadForm" property = "type" value = "Job">
   			<%@ include  file="/JobMenuScript.inc" %>
   		</logic:equal>
		
		
	<table width = "100%" border = "0" cellspacing="1" cellpadding = "1" align = "center" valign  = "top">
		<tr><td width="2" height="0"></td>
		    <td>
		    	<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present> 	
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
		   		<table id = "dynatable" border = "0" cellspacing = "1" cellpadding = "1" width = "450"> 
		   		
		   			<logic:notEqual name = "UploadForm" property = "type" value = "MSA">
			   			<tr>
			   				<td class = "colDark">
			   					<bean:message bundle = "pm" key = "common.fileupload.document"/>
		    				</td>
		    				
		    				<td class = "colLight">
		    					<html:file property = "filename" styleClass = "button" />
		    				</td>
		    			</tr>
		    			
		    			<tr>	
		    				<td class = "colDark">
			   					<bean:message bundle = "pm" key = "common.fileupload.remarks"/>
		    				</td>
		    				
		    				
		    				<td class = "colLight">
		    					<html:text property = "remarks" size = "50" maxlength = "100" styleClass = "text"></html:text>
		    				</td>
		    		
		    			</tr>
	    			</logic:notEqual>
	    				
	    			<logic:equal name = "UploadForm" property = "type" value = "MSA">
	    				<tr>
	    					<td class = "colDark">
			   					<bean:message bundle = "pm" key = "common.fileupload.msaname"/>
		    				</td>
		    				<td class = "colLight">
		    					<html:file property = "filename" styleClass = "button" />
		    				</td>
		    			</tr>
		    			<tr>
		    				<td class = "colDark">
			   					<bean:message bundle = "pm" key = "common.fileupload.remarks"/>
		    				</td>
		    				<td class = "colLight">
		    					<html:text property = "remarks" size = "50" maxlength = "100" styleClass = "text"></html:text>
		    				</td>
		    			</tr>

	    				<tr>
	    					<td class = "colDark">
	    						<bean:message bundle = "pm" key = "common.fileupload.formaldocdate"/>
	    					</td>
	    					<td class = "colLight">
	    						<html:text styleClass = "Ntextbox" size = "10" property = "formaldocdate"  readonly = "true"/>
    							<img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].formaldocdate ,  document.forms[0].formaldocdate , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;">
    						</td>
	    				</tr>
	    				
	    				<tr>
	    					<td class = "colDark">
	    						<bean:message bundle = "pm" key = "common.fileupload.status"/>
	    					</td>
	    					
	    					<td class = "colLight">
	    						<html:select property = "status" styleClass = "select">
									<html:optionsCollection property = "statuslist" value = "value" label = "label"/> 
								</html:select>	
    						</td>
	    				</tr>
	    			</logic:equal>
	    			<tr>
	    				<td class = "colLight" align="center" colspan="2"> 
						<html:submit property = "upload" styleClass = "button_c" onclick = "javascript:return validate();window.close();"><bean:message bundle = "pm" key = "common.fileupload.upload"/></html:submit>
						<logic:equal name = "UploadForm" property = "type" value = "Appendixdashboard">
		    				<html:button property="back" styleClass="button_c" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
		    			</logic:equal>
		    		
		    				<logic:equal name  ="UploadForm" property ="path" value ="appendixdashboard">
			    				<html:button property="back" styleClass="button_c" onclick = "return Backactionappendixjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    			</logic:equal>
			    			
	    				<logic:notEqual name  ="UploadForm" property ="path" value ="appendixdashboard">
								    		
			    			<logic:equal name = "UploadForm" property = "type" value = "Jobdashboard">
			    				<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    			</logic:equal>
		    			
		    			</logic:notEqual>
			   				
			   			</td>				
   					</tr>	
   			
   		
			   	</table>
			</td>
		</tr> 
	</table>
	<logic:equal name = "UploadForm" property = "type" value = "Jobdashboard">
		<c:if test = "${requestScope.jobType eq 'Addendum'}">	
			<%@ include  file="/AddendumMenuScript.inc" %>
		</c:if>
			<c:if test = "${requestScope.jobType eq 'Default'}">
			<%@ include  file="/DefaultJobMenuScript.inc" %>
		</c:if>
		<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
			<%@ include  file="/ProjectJobMenuScript.inc" %>
		</c:if>	
	</logic:equal>
</html:form>
</table>
</body>

<script>

function moreattach()
{
	var i = dynatable.rows.length-1;
	var oRow = dynatable.insertRow();

	if(dynatable.rows.length%2 == 0 )
	{
		oRow.style.background= "#EEF3F1";
	}
	else
	{
		oRow.style.background= "#ECF5FF";
	}
	var temprow="";
	var oCell=oRow.insertCell(0);
	oCell.innerHTML="&nbsp";
	oCell.align="center";

	oCell=oRow.insertCell(1);
	temprow="<input type=text name = remarks size = 15 maxlength = 100 class = Ntextbox >";
	oCell.innerHTML=temprow;
	oCell.align="left";
	
	
	oCell=oRow.insertCell(2);
	temprow="<input type=file  name=filename class = Ntextbox >";
	oCell.innerHTML=temprow;
	oCell.align="left";
}

function validate()
{
	
	if( document.forms[0].remarks.value == "" )
	{
		alert( "<bean:message bundle = "pm" key = "common.fileupload.enterremark"/>" );
		document.forms[0].remarks.focus();
		return false;
	}
	
	if( document.forms[0].filename.value == "" )
	{
		alert( "<bean:message bundle = "pm" key = "common.fileupload.selectfile"/>" );
		document.forms[0].filename.focus();
		return false;
	}
	
	if( <%= typeupload.equals( "MSA" ) %>  )
	{
		if( document.forms[0].formaldocdate.value == "" )
		{
			alert( "<bean:message bundle = "pm" key = "common.fileupload.formaldocdateenter"/>" );
			document.forms[0].formaldocdate.focus();
			return false;
		}
		
		if( document.forms[0].status.value == '0' ) 
		{
			alert( "<bean:message bundle = "pm" key = "common.fileupload.statusenter"/>" );
			document.forms[0].status.focus();
			return false;
		}
	}
	
	
	return true;
}

function Backactionappendixjob()
{
	if(document.forms[0].nettype.value == 'dispatch')
		document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	else
		document.forms[0].action = "AppendixHeader.do?function=view&appendixid=<bean:write name ="UploadForm" property="appendix_Id"/>&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}

function Backactionappendix()
{
	document.forms[0].action = "AppendixHeader.do?function=view&appendixid=<%= forward_Id %>&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
function Backactionjob()
{
	document.forms[0].action = "JobDashboardAction.do?jobid=<%= forward_Id %>" ;
	document.forms[0].submit();
	return true;
}
</script>
<script>
function OnLoad()
{
if(document.forms[0].type.value =='prm_appendix' || document.forms[0].type.value =='Jobdashboard'){}
else{
		<%if(request.getAttribute("opendiv") != null){%>
			document.getElementById("filter").style.visibility="visible";
		<%}%>
}
}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function leftAdjLayers(){
if(document.forms[0].type.value =='Jobdashboard'){}
else{	
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 280;
		document.getElementById("filter").style.left=leftAdj;
		
}
}
function show()
{
	
	document.forms[0].go.value="true";
	
	if(document.forms[0].type.value=='MSA')
		document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<bean:write name='UploadForm' property ='id1'/>&Status=<bean:write name = 'UploadForm' property = 'status' />&clickShow=true";
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<bean:write name='UploadForm' property ='id1'/>&clickShow=true";
		
		
	document.forms[0].submit();
	return true;
}

function initialState()
{
	document.forms[0].go.value="";
	if(document.forms[0].type.value=='MSA') {
		document.forms[0].action="UploadAction.do?ref=FromMSA&Id=<bean:write name='UploadForm' property ='id1'/>&Status=<bean:write name = 'UploadForm' property = 'status' />&firstCall=true&home=home&clickShow=true";
	}
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action="UploadAction.do?ref=FromAppendix&Id=<bean:write name='UploadForm' property ='id1'/>&firstCall=true&home=home&clickShow=true";
		
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{
	if(document.forms[0].type.value=='MSA'){
		if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				document.forms[0].selectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				if(document.forms[0].selectedOwners[j].checked) {
				 	document.forms[0].selectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{	
	
			for(i=0;i<document.forms[0].selectedOwners.length;i++)
			{
					if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
					{
						document.forms[0].otherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="UploadAction.do?ref=FromMSA&Id=<bean:write name='UploadForm' property ='id1'/>&Status=<bean:write name = 'UploadForm' property = 'status' />&opendiv=0";
		 				document.forms[0].submit();
					}
			}
	}
	}
	if(document.forms[0].type.value=='Appendix'){
			if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
						{
						if(checkboxvalue == '%'){
						for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
						{
							document.forms[0].appendixSelectedOwners[j].checked=false;
						}
						}
				else
				{
					for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
					{
						if(document.forms[0].appendixSelectedOwners[j].checked) {
						 	document.forms[0].appendixSelectedOwners[1].checked = false;
						 	break;
						}
					}		
				}
				}		
		
		else
		{
			for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
			{
					if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
					{
						document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="UploadAction.do?ref=FromAppendix&Id=<bean:write name='UploadForm' property ='id1'/>&opendiv=0";
		 				document.forms[0].submit();
		
					}
			}
			}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="UploadAction.do?ref=FromMSA&Id=<bean:write name='UploadForm' property ='id1'/>&Status=<bean:write name = 'UploadForm' property = 'status' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<bean:write name='UploadForm' property ='id1'/>&Status=<bean:write name = 'UploadForm' property = 'status' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="UploadAction.do?ref=FromAppendix&Id=<bean:write name='UploadForm' property ='id1'/>&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
document.forms[0].go.value="true";
//alert('in month_Appendix()');
document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<bean:write name='UploadForm' property ='id1'/>&week=0&clickShow=true";
document.forms[0].submit();
return true;
}

</script>
<script>

function changeFilter()
{
	document.forms[0].action="UploadAction.do?ref=FromAppendixDashboard&Id=<bean:write name='UploadForm' property ='id1'/>&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="UploadAction.do?ref=FromAppendixDashboard&Id=<bean:write name='UploadForm' property ='id1'/>&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].id1.value;	
 	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>

</html:html>


