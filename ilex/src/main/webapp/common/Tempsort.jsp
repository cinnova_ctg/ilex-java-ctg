

<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ page language = "java" %>


<%
	String  rdofilter = null;
	String id = ( String ) request.getAttribute( "id" );
	String type = ( String ) request.getAttribute( "type" );
	String querystring = ( String ) request.getAttribute( "querystring" );

	
	
	String page1 = null;
	String viewjobtype = null;
	String ref1 = null;

	String appendix_id = ( String ) request.getAttribute( "appendixid" );
	String job_id = ( String ) request.getAttribute( "jobid" );
	String activity_id = ( String ) request.getAttribute( "activityid" );
	String currentstatus = "";
	String nettype = "";
	String ownerId = "";
	String from = null;
	

	//Start :Added By Amit For Edit Site Master
	String sitestate = "";
	String sitename = "";
	String sitenumber = "";
	String sitecity = "";
	String stateid = "";
	String flag = "";
	String siteSearchName="";
	String siteSearchNumber="";
	String siteSearchCity="";
	String siteSearchState="";
	String siteSearchZip="";
	String siteSearchCountry="";
	String siteSearchGroup="";
	String siteSearchEndCustomer="";
	String siteSearchStatus="";
	
	String lines_per_page= "";     
	String current_page_no = "";   
	String total_page_size = "";                              
	String org_page_no = "";
	String org_lines_per_page = "";
    String siteNameForColumn = ""; 
	String siteNumberForColumn = "";
	String siteAddressForColumn = "";
	String siteCityForColumn= "";
	String siteStateForColumn = "";
	String siteZipForColumn = "";
	String siteCountryForColumn = "";
	String pageType= "";
	String site_msaid = "";
	
	String invoice_Flag = (String) request.getAttribute("invoice_Flag");
	String partner_name = (String) request.getAttribute("partner_name");
	String powo_number = (String) request.getAttribute("powo_number");
	String from_date = (String) request.getAttribute("from_date");
	String to_date   = (String) request.getAttribute("to_date");
	String invoiceno = (String) request.getAttribute("invoiceno");
	
	if(request.getAttribute("sitestate")!=null)
	{
		sitestate  = (String)request.getAttribute("sitestate");
	}
	if(request.getAttribute("stateid")!=null)
	{
		stateid  = (String)request.getAttribute("stateid");
	}
	if(request.getAttribute("sitename")!=null)
	{
		sitename  = (String)request.getAttribute("sitename");
	}
	if(request.getAttribute("sitenumber")!=null)
	{
		sitenumber = (String)request.getAttribute("sitenumber");
	}

	if(request.getAttribute("sitecity")!=null)
	{
		sitecity  = (String)request.getAttribute("sitecity");
	}
	
	if(request.getAttribute("flag")!=null)
	{
		flag  = (String)request.getAttribute("flag");
	}
	//End

	//partner
	String name="";
	String keyword="";
	String partnerSearchType="";
	String mcsaVers="";
	String zipcode="";
	String radius="";
	String mainOffice="";
	String fieldOffice="";
	String techii="";
	String rating="";
	String techinicaians="";
	String certification="";
	String corecomp="";
	String checkBox1="";
	String checkBox2="";
	String jobId="";
	String typeSearch="";
	String formType="";
	if(request.getAttribute("searchNmae")!=null) 
		name=(String)request.getAttribute("searchNmae");
	if(request.getAttribute("keyword")!=null) 
		keyword=(String)request.getAttribute("keyword");

	if(request.getAttribute("partnerType")!=null) 
		partnerSearchType=(String)request.getAttribute("partnerType");

	if(request.getAttribute("mcsaVers")!=null) 
		mcsaVers=(String)request.getAttribute("mcsaVers");
	
	if(request.getAttribute("zipcode")!=null) 
		zipcode=(String)request.getAttribute("zipcode");	

	if(request.getAttribute("radius")!=null) 
		radius=(String)request.getAttribute("radius");
	

	if(request.getAttribute("mainOffice")!=null) 
		mainOffice=(String)request.getAttribute("mainOffice");

	if(request.getAttribute("fieldOffice")!=null) 
		fieldOffice=(String)request.getAttribute("fieldOffice");

	if(request.getAttribute("Techii")!=null) 
		techii=(String)request.getAttribute("Techii");

	if(request.getAttribute("rating")!=null) 
		rating=(String)request.getAttribute("rating");

	if(request.getAttribute("techinicians")!=null) 
		techinicaians=(String)request.getAttribute("techinicians");

	if(request.getAttribute("certification")!=null) 
		certification=(String)request.getAttribute("certification");

	if(request.getAttribute("corecomp")!=null) 
		corecomp=(String)request.getAttribute("corecomp");

	if(request.getAttribute("checkbox1")!=null) 
		checkBox1=(String)request.getAttribute("checkbox1");

	if(request.getAttribute("checkbox2")!=null) 
		checkBox2=(String)request.getAttribute("checkbox2");

	if(request.getAttribute("jobId")!=null && !(request.getAttribute("jobId").equals(""))) { 
		jobId=(String)request.getAttribute("jobId");
		typeSearch="Add";
		formType="powo";
	} else {
		typeSearch="v";
		formType="pvs";
		jobId="";
	}
	//partner End
	
	if(request.getAttribute("from")!=null)
	{
		from  = (String)request.getAttribute("from");
	}
	if(request.getAttribute("rdofilter")!=null)
	{
		rdofilter  = (String)request.getAttribute("rdofilter");
	}

	if( request.getAttribute( "nettype" ) != null )
		nettype = ( String )request.getAttribute("nettype");

	if( request.getAttribute( "ownerId" ) != null )
		ownerId = ( String )request.getAttribute("ownerId");
	
	if( request.getAttribute( "currentstatus" ) != null )
		currentstatus = ( String )request.getAttribute("currentstatus");
	else
		currentstatus="%";
	
	if( request.getAttribute( "Appendix_Id" ) != null )
		appendix_id = ( String ) request.getAttribute( "Appendix_Id" );


	
	if(request.getAttribute("invoice_Flag") != null)
		invoice_Flag=(String) request.getAttribute("invoice_Flag");
	if(request.getAttribute("partner_name") != null)
		partner_name=(String) request.getAttribute("partner_name");
	if(request.getAttribute("powo_number") != null)
		powo_number=(String) request.getAttribute("powo_number");
	if(request.getAttribute("from_date") != null)
		from_date=(String) request.getAttribute("from_date");
	if(request.getAttribute("to_date") != null)
		to_date=(String) request.getAttribute("to_date");
	if(request.getAttribute("invoiceno") != null)
		invoiceno=(String) request.getAttribute("invoiceno");
	
	
	if(request.getAttribute("siteSearchName") != null)
		siteSearchName=(String) request.getAttribute("siteSearchName");
	
	if(request.getAttribute("siteSearchNumber") != null)
		siteSearchNumber=(String) request.getAttribute("siteSearchNumber");

	if(request.getAttribute("siteSearchCity") != null)
		siteSearchCity=(String) request.getAttribute("siteSearchCity");

	if(request.getAttribute("siteSearchState") != null)
		siteSearchState=(String) request.getAttribute("siteSearchState");

	if(request.getAttribute("siteSearchZip") != null)
		siteSearchZip=(String) request.getAttribute("siteSearchZip");

	if(request.getAttribute("siteSearchCountry") != null)
		siteSearchCountry=(String) request.getAttribute("siteSearchCountry");

	if(request.getAttribute("siteSearchGroup") != null)
		siteSearchGroup=(String) request.getAttribute("siteSearchGroup");

	if(request.getAttribute("siteSearchEndCustomer") != null)
		siteSearchEndCustomer=(String) request.getAttribute("siteSearchEndCustomer");
	
	if(request.getAttribute("siteSearchStatus") != null)
		siteSearchStatus=(String) request.getAttribute("siteSearchStatus");
	
	

	if(request.getAttribute("lines_per_page") != null)
		lines_per_page=(String) request.getAttribute("lines_per_page");

	if(request.getAttribute("org_lines_per_page") != null)
		org_lines_per_page=(String) request.getAttribute("org_lines_per_page");          
	
	if(request.getAttribute("siteNameForColumn")!=null)
		siteNameForColumn = (String)request.getAttribute("siteNameForColumn");
	
	if(request.getAttribute("siteNumberForColumn")!=null)
		siteNumberForColumn = (String)request.getAttribute("siteNumberForColumn");
	
	if(request.getAttribute("siteAddressForColumn")!=null)
		siteAddressForColumn = (String)request.getAttribute("siteAddressForColumn");
	
	if(request.getAttribute("siteCityForColumn")!=null)
		siteCityForColumn = (String)request.getAttribute("siteCityForColumn");
	
	if(request.getAttribute("siteStateForColumn")!=null)
		siteStateForColumn = (String)request.getAttribute("siteStateForColumn");
	
	if(request.getAttribute("siteZipForColumn")!=null)
		siteZipForColumn = (String)request.getAttribute("siteZipForColumn");
	
	if(request.getAttribute("siteNameForColumn")!=null)
		siteCountryForColumn = (String)request.getAttribute("siteCountryForColumn");

	if(request.getAttribute("pageType")!=null)
		pageType = (String)request.getAttribute("pageType");

	if(request.getParameter("site_msaid") != null)
		site_msaid=(String)request.getParameter("site_msaid");
	
	

//Added by amit


if(request.getAttribute("page")!=null)
	{
	page1  = (String)request.getAttribute("page");
	}
if(request.getAttribute("ref1")!=null)
	{
		ref1  = (String)request.getAttribute("ref1");
	}
if(request.getAttribute("viewjobtype")!=null)
	{
		viewjobtype  = (String)request.getAttribute("viewjobtype");
	}

//End
	if( type.contains( "pm_res" ) )
	{
		if( type.charAt( 6 ) == 'M' )
		{
	%>
			<jsp:forward page="/ResourceListAction.do">
				<jsp:param name = "resourceListType" value = "M" />
				<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
				<jsp:param name = "Activity_Id" value = "<%= id %>" />
				<jsp:param name = "querystring" value = "<%= querystring %>" />  
				<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' /> 
			</jsp:forward>
	<%
		}
	
		else if( type.charAt( 6 ) == 'L' )
		{
	%>
			<jsp:forward page="/ResourceListAction.do">
				<jsp:param name = "resourceListType" value = "L" />
				<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
				<jsp:param name = "Activity_Id" value = "<%= id %>" />
				<jsp:param name = "querystring" value = "<%= querystring %>" />   
				<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' /> 
			</jsp:forward>
	<%
		}
	
		else if( type.charAt( 6 ) == 'F' )
		{
	%>
			<jsp:forward page="/ResourceListAction.do">
				<jsp:param name = "resourceListType" value = "F" />
				<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
				<jsp:param name = "Activity_Id" value = "<%= id %>" />
				<jsp:param name = "querystring" value = "<%= querystring %>" />   
				<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' /> 
			</jsp:forward>
	<%
		}
	
		else
		{
	%>
			<jsp:forward page="/ResourceListAction.do">
				<jsp:param name = "resourceListType" value = "T" />
				<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
				<jsp:param name = "Activity_Id" value = "<%= id %>" />
				<jsp:param name = "querystring" value = "<%= querystring %>" />   
				<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' /> 
			</jsp:forward>
	<%
		}
	}

	if( type.equals( "Activity" ) )
	{
	%>
		<jsp:forward page="/ActivityUpdateAction.do">
			<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
			<jsp:param name = "Job_Id" value = "<%= id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" />   
		</jsp:forward>
	<%
		}


	if( type.equals( "Job" ) )
	{
	%>
		<jsp:forward page="/JobUpdateAction.do">
			<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
			<jsp:param name = "Appendix_Id" value = "<%= id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" /> 
			<jsp:param name = "addendum_id" value = '<%= ( String ) request.getAttribute( "addendum_id" ) %>' />   
		</jsp:forward>
	<%
		}	

	if( type.equals( "prj_job" ) )
	{

		
		// Start :added By Amit: To hold Sort
		if(session!=null)
		{
			if(session.getAttribute("HoldingSort")!=null)
			{
				if(!session.getAttribute("HoldingSort").equals(querystring))
				{
					session.setAttribute("HoldingSort",querystring);
				}
			}
			else
			{
				session.setAttribute("HoldingSort",querystring);
			}
		}
		//End :added By Amit: To hold Sort
	%>
		<jsp:forward page="/AppendixHeader.do" >

			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "appendixid" value = "<%= appendix_id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" /> 
			<jsp:param name = "viewjobtype" value = "<%=currentstatus %>" />
		</jsp:forward>
	<%
		}	

	if( type.equals( "prj_netmedx" ) )
	{

		// Start :added By Amit: To hold Sort
		if(session!=null)
		{
			if(session.getAttribute("HoldingSortNetMedx")!=null)
			{
				if(!session.getAttribute("HoldingSortNetMedx").equals(querystring))
				{
					session.setAttribute("HoldingSortNetMedx",querystring);
				}
			}
			else
			{
				session.setAttribute("HoldingSortNetMedx",querystring);
			}
		}
		//End :added By Amit: To hold Sort
	%>
		<jsp:forward page="/NetMedXDashboardAction.do" >
			<jsp:param name = "appendixid" value = "<%= appendix_id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" /> 
			<jsp:param name = "viewjobtype" value = "<%=currentstatus %>" />
			<jsp:param name = "nettype" value = "<%=nettype %>" />
			<jsp:param name = "ownerId" value = "<%=ownerId %>" />
				
		</jsp:forward>	
		
	<%
		}	


	if( type.equals( "prj_jobnew" ) )
	{
	%>
		<jsp:forward page="/AppendixHeaderNew.do" >
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "appendixid" value = "<%= appendix_id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
		
	<%
		}	

	if( type.equals( "prj_activity" ) )
	{
	%>
		<jsp:forward page="/JobHeader.do" >
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "jobid" value = "<%= job_id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
		
	<%
		}

	if( type.equals( "prj_materialresource" )||type.equals( "prj_laborresource" )||type.equals( "prj_freightresource" )||type.equals( "prj_travelresource" ) )
	{
	%>
		<jsp:forward page="/ActivityHeader.do" >
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "activityid" value = "<%= activity_id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
		
	<%
		}
	//Start :Added By Amit for Sort on Appendix Dashboard
	if(type.equals( "MSA" )||type.equals( "All" )||type.equals( "Appendix" ))


	{%>
	
	<jsp:forward page="/InvoiceJobDetail.do" >
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "appendixid" value = "<%= appendix_id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" />
			<jsp:param name = "rdofilter" value = "<%= rdofilter %>" /> 
			<jsp:param name = "invoice_Flag" value = "<%=invoice_Flag%>"/>
			<jsp:param name = "partner_name" value = "<%=partner_name%>"/>
			<jsp:param name = "powo_number" value = "<%=powo_number%>"/>
			<jsp:param name = "from_date" value = "<%=from_date%>"/>
			<jsp:param name = "to_date" value = "<%=to_date%>"/>
			<jsp:param name = "invoiceno" value = "<%=invoiceno%>"/>
			
		</jsp:forward>

	<%}	
	//End
	//Start : Added By Amit For Masters
	if(type.equals( "siteManage"))
		{%>
		
		<jsp:forward page="/SiteManagement.do" >
				
				<jsp:param name = "siteSearchName"    value = "<%= siteSearchName %>" />
				<jsp:param name = "siteSearchNumber"  value = "<%= siteSearchNumber %>" />	
				<jsp:param name = "siteSearchCity"  value = "<%= siteSearchCity %>" />	
				<jsp:param name = "siteSearchState"  value = "<%= siteSearchState %>" />	
				<jsp:param name = "siteSearchZip"  value = "<%= siteSearchZip %>" />	
				<jsp:param name = "siteSearchCountry"  value = "<%= siteSearchCountry %>" />
				<jsp:param name = "siteSearchGroup"  value = "<%= siteSearchGroup %>" />
				<jsp:param name = "siteSearchEndCustomer"  value = "<%= siteSearchEndCustomer %>" />
				<jsp:param name ="siteSearchStatus" value="<%=siteSearchStatus%>"/>
				<jsp:param name = "lines_per_page"    value = "<%= lines_per_page %>" />	
				<jsp:param name = "org_lines_per_page"  value = "<%= org_lines_per_page %>" />
				<jsp:param name = "siteNameForColumn"   value = "<%=siteNameForColumn%>"/>
				<jsp:param name = "siteNumberForColumn" value = "<%=siteNumberForColumn%>"/>				
				<jsp:param name = "siteAddressForColumn"  value = "<%=siteAddressForColumn%>"/>
				<jsp:param name = "siteCityForColumn"     value = "<%=siteCityForColumn%>"/>
				<jsp:param name = "siteStateForColumn"    value = "<%=siteStateForColumn%>"/>
				<jsp:param name = "siteZipForColumn"      value = "<%=siteZipForColumn%>"/>
				<jsp:param name = "siteCountryForColumn"  value = "<%=siteCountryForColumn%>"/>
				
				
				<jsp:param name = "pageType" value ="<%=pageType%>"/> 	     
				<jsp:param name = "appendixid" value = "<%= appendix_id %>" />
				<jsp:param name = "querystring" value = "<%= querystring %>" />  
				<jsp:param name = "msaid" value = "<%=site_msaid%>"/>                                     
				
				
			</jsp:forward>

		<%}
	if( type.equals( "Masters" ) )
	{  
		if( stateid.equals("" ) )
			stateid = "A";
		if( sitename.equals("" ) )
			sitename = "A";
		if( sitenumber.equals("" ) )
			sitenumber = "A";
		if( sitecity.equals("" ) )
			sitecity = "A";
		if( id.equals( "" ) )
				id = "A";
	%>	
	<jsp:forward page="/EditSiteAction.do" >
			<jsp:param name = "querystring" value = "<%= querystring %>" />
			<jsp:param name = "statesearchcombo" value = "<%= stateid %>" />
			<jsp:param name = "sitenamesearch" value = "<%= sitename %>" />
			<jsp:param name = "sitenumbersearch" value = "<%= sitenumber %>" />
			<jsp:param name = "sitecitysearch" value = "<%= sitecity %>" />
			<jsp:param name = "flag" value = "<%= flag %>" />
			<jsp:param name = "msasearchcombo" value = "<%= id %>" />
	</jsp:forward>

	<%} 	
	if( type.equals( "invoice_job" ) )
	{ 
	%>
		<jsp:forward page="/InvoiceJobDetail.do" >
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "appendixid" value = "<%= appendix_id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
		
	<%
	}	
	
	if( type.equals( "prj_powo" ) )
	{


	//Start :added By Amit: To hold Sort
	if(session!=null)
	{
		if(session.getAttribute("HoldingSortPOWODb")!=null)
		{
			if(!session.getAttribute("HoldingSortPOWODb").equals(querystring))
			{
				session.setAttribute("HoldingSortPOWODb",querystring);
			}
		}
		else
		{
			session.setAttribute("HoldingSortPOWODb",querystring);
		}
	}
	//End :added By Amit: To hold Sort
%>	
		<jsp:forward page="/POWOAction.do" >
			<jsp:param name = "jobid" value = "<%= id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
<% }  
	if( type.equals( "siteHistory" ) )
	{%>	
	<jsp:forward page="/SiteHistoryAction.do" >
			<jsp:param name = "jobid" value = "<%= job_id %>" />
			<jsp:param name = "querystring" value = "<%= querystring %>" />
			<jsp:param name = "from" value = "<%= from %>" />
			<jsp:param name = "Appendix_Id" value = "<%= appendix_id %>" />

			<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
			<jsp:param name = "ref1" value = "<%= ref1 %>" />
			<jsp:param name = "page" value = "<%= page1 %>" />
			<jsp:param name = "ownerId" value = "<%= ownerId %>" />
	</jsp:forward>

<%} if( type.equals( "bulkJobCreation" ) )
{
%>
	<jsp:forward page="/BulkJobCreation.do" >
			<jsp:param name = "ref" value = "siteSearch" />
			<jsp:param name = "querystring" value = "<%= querystring %>" />
			<jsp:param name = "appendix_Id" value = "<%= appendix_id %>" />
			<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
			<jsp:param name = "ownerId" value = "<%= ownerId %>" />
			
			<jsp:param name = "sitenamesearch" value = "<%= sitename %>" />
			<jsp:param name = "sitestatesearch" value = "<%= stateid %>" />
			<jsp:param name = "sitecitysearch" value = "<%= sitecity %>" />
	</jsp:forward>

<%} if(type.equals( "partnerSearch" )) {%>
	<jsp:forward page="/Partner_Search.do" >
			<jsp:param name = "ref" value = "search" />
			<jsp:param name = "sort" value = "sort" />
			<jsp:param name = "querystring" value = "<%= querystring %>" />
			<jsp:param name = "startname" value = "<%= name %>" />
			<jsp:param name = "keyword" value = "<%= keyword %>" />
			<jsp:param name = "partnertype" value = "<%= partnerSearchType %>" />
			
			<jsp:param name = "mcsavers" value = "<%= mcsaVers %>" />
			<jsp:param name = "zipcode" value = "<%= zipcode %>" />
			<jsp:param name = "radius" value = "<%= radius %>" />
			<jsp:param name = "mainoffice" value = "<%= mainOffice %>" />
			<jsp:param name = "fieldoffice" value = "<%= fieldOffice %>" />
			<jsp:param name = "techhi" value = "<%= techii %>" />
			<jsp:param name = "rating" value = "<%= rating %>" />
			<jsp:param name = "technician" value = "<%= techinicaians %>" />
			<jsp:param name = "certification" value = "<%= certification %>" />
			<jsp:param name = "corecomp" value = "<%= corecomp %>" />
			<jsp:param name = "checkbox1" value = "<%= checkBox1 %>" />
			<jsp:param name = "checkbox2" value = "<%= checkBox2 %>" />
			<jsp:param name = "typeid" value = "<%= jobId %>" />
			<jsp:param name = "type" value = "<%= typeSearch %>" />
			<jsp:param name = "formtype" value = "<%= formType %>" />
	</jsp:forward>
	
<%} %>		
