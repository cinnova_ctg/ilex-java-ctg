<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ page language = "java" %>


<%
	String resource_Id ="";
	String appendix_id = "";
	String job_id = "";
	String MSA_Id = ""; 
	String viewjobtype ="A" ;

	String id = "";
	String type = ( String ) request.getAttribute( "type" );
	String activity_id = "";
	String Activitylibrary_Id= "";
	String appendixName = "";
	String from = "";
		
	if( request.getAttribute( "viewjobtype" ) != null )
		viewjobtype = ( String ) request.getAttribute( "viewjobtype" );

	if( request.getAttribute( "MSA_Id" ) != null )
		MSA_Id = ( String ) request.getAttribute( "MSA_Id" );


	if( request.getAttribute( "resource_Id" ) != null )
		resource_Id = ( String ) request.getAttribute( "resource_Id" );

	
	if( request.getAttribute( "appendixid" ) != null )
	{
		appendix_id = ( String ) request.getAttribute( "appendixid" );
	}

	if( request.getAttribute( "jobid" ) != null )
	{
		job_id = ( String ) request.getAttribute( "jobid" );
	}

	if( request.getAttribute( "Appendix_Id" ) != null )   //use in revision identifier
	{
		appendix_id = ( String ) request.getAttribute( "Appendix_Id" );
	}
	

	if( request.getAttribute( "Job_Id" ) != null )
	{
		id = ( String ) request.getAttribute( "Job_Id" );
	}

	if( request.getAttribute( "Activity_Id" ) != null )
	{
		id = ( String ) request.getAttribute( "Activity_Id" );
		
	}

	if( request.getAttribute( "Activitylibrary_Id" ) != null )
	{
		Activitylibrary_Id = ( String ) request.getAttribute( "Activitylibrary_Id" );
	}

	if( request.getAttribute( "activityid" ) != null )
	{
		activity_id = ( String ) request.getAttribute( "activityid" );
	}

	if(request.getAttribute("appendixId") != null) {
		appendix_id = request.getAttribute("appendixId").toString();
	}

	if(request.getAttribute("appendixName") != null) {
		appendixName = request.getAttribute("appendixName").toString();
	}

	if(request.getAttribute("from") != null) {
		from = request.getAttribute("from").toString();
	}
	
%>
	<% 	if( type.equals( "Appendix" ) )     //Only called for revision identifier page
	{%>	

	<jsp:forward page="/AppendixRevisionIdentifierAction.do">
			<jsp:param name = "ref" value = "View" /> 
			<jsp:param name = "Appendix_Id" value = "<%= appendix_id %>" /> 
		</jsp:forward>
	<%} %>

<% 	if( type.equals( "Job" ) )
	{%>	

	<jsp:forward page = "/JobDetailAction.do">
			<jsp:param name = "ref" value = '<%= (String ) request.getAttribute( "ref") %>' /> 
			<jsp:param name = "Job_Id" value = '<%= id %>' /> 
			<jsp:param name = "changestatusflag" value = '<%= ( String ) request.getAttribute( "changestatusflag" ) %>' />
		</jsp:forward>
	<%} %>


<% 	if( type.equals( "Activity" ) )
	{%>	

	<jsp:forward page="/ActivityDetailAction.do">
			<jsp:param name = "ref" value = '<%= ( String ) request.getAttribute( "ref" ) %>' /> 
			<jsp:param name = "Activity_Id" value = '<%= id %>'/> 
			<jsp:param name = "changestatusflag" value = '<%= ( String ) request.getAttribute( "changestatusflag" ) %>' />
		</jsp:forward>
	<%} %>
   <% if( type.equals( "prj_appendix" ) )
	{		
	%>	

	<jsp:forward page="/AppendixHeader.do">
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "appendixid" value = "<%= appendix_id %>" /> 
			<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
	</jsp:forward>
	<%} %>
	
<% 	if( type.equals( "prj_job" ) )
	{%>	
	<jsp:forward page="/JobHeader.do">
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "jobid" value = "<%= job_id %>" /> 
		</jsp:forward>
	<%} %>
	
<% 	if( type.equals( "prj_act" ) )
	{%>	
	<jsp:forward page="/ActivityHeader.do">
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "activityid" value = "<%= activity_id %>" /> 
		</jsp:forward>
	<%} %>
	

<!-- project manager email appendix functionality  -->

<% 	if( type.equals("PRMAppendix")  )
	{%>	
	
	<jsp:forward page="/AppendixHeader.do">
			<jsp:param name = "function" value = "view" /> 
			<jsp:param name = "appendixid" value = "<%= appendix_id %>" /> 
			<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" />
		</jsp:forward>
	<%
	} %>
	
<!-- project manager email appendix functionality  -->	

<!-- PVS manager  -->
<% 	if( type.equals("searchpage")  )
	{%>	
	
	<jsp:forward page="/Partner_Search.do">
			<jsp:param name = "ref" value = "search" /> 
			<jsp:param name = "type" value = "v" /> 
		</jsp:forward>
	<%
	} %>

<!-- PVS manager  -->
<!-- Customer Labor Rates functionality  -->

<% 	if( type.equals("AM")||type.equals("AM_EDIT")||type.equals("PM")  )
	{%>	
	<jsp:forward page="/CustomerLaborRates.do">
			<jsp:param name = "type" value = "<%= type %>"  /> 
			<jsp:param name = "MSA_Id" value = "<%= MSA_Id %>" /> 
			<jsp:param name = "Activitylibrary_Id" value = "<%= Activitylibrary_Id %>" /> 
			<jsp:param name = "appendixId" value = "<%= appendix_id %>" />
			<jsp:param name = "appendixName" value = "<%=appendixName%>" />
			<jsp:param name = "from" value = "<%=from %>" />
			<jsp:param name = "refclr" value = "add" /> 
			 
	</jsp:forward>
	<%
	} %>
	
<!-- Customer Labor Rates functionality  -->	
<%

	

	if( type.charAt( 6 ) == 'M' )
	{ 
%>
		<jsp:forward page="/MaterialDetailAction.do">
			<jsp:param name = "ref" value = "View" /> 
			<jsp:param name = "Material_Id" value = "<%= resource_Id %>" /> 
		</jsp:forward>
<%
	}

	else if( type.charAt( 6 ) == 'L' )
	{
%>
		<jsp:forward page="/LaborDetailAction.do">
			<jsp:param name = "ref" value = "View" /> 
			<jsp:param name = "Labor_Id" value = "<%= resource_Id %>" /> 
		</jsp:forward>
<%
	}

	else if( type.charAt( 6 ) == 'F' )
	{
%>
		<jsp:forward page="/FreightDetailAction.do">
			<jsp:param name = "ref" value = "View" /> 
			<jsp:param name = "Freight_Id" value = "<%= resource_Id %>" /> 
		</jsp:forward>
<%
	}

	else
	{
%>
		<jsp:forward page="/TravelDetailAction.do">
				<jsp:param name = "ref" value = "View" /> 
				<jsp:param name = "Travel_Id" value = "<%= resource_Id %>" /> 
		</jsp:forward>
<%
	}
%>



