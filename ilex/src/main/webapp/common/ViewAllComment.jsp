<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:size id = "commentsize" name = "all_comment" scope = "request"/> 
<%
	
	int addRow = -1;
	boolean addSpace = false;
	int rowHeight = 120;
	int ownerListSize = 0;
	int j=2;
	String checkowner=null;
	
	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());


	int i = 0;
	boolean csschooser = true;
	String cssclass = "";
	String nameclass = "";
	String commentbyclass = "";
%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkstatus = null;


int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


%>
<html:html>
<head>
	<title><bean:message bundle = "pm" key = "common.viewcomment.title"/></title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css">  
	<%@ include  file="/NMenu.inc" %>
	<meta http-equiv="X-UA-Compatible" content="IE=9"/>

<%@ include  file="/MSAMenu.inc" %>
<%@ include  file="/AppendixMenu.inc" %>
<%@ include  file="/JobMenu.inc" %>
<%@ include file="/DefaultJobMenu.inc" %>
<%@ include  file="/ActivityMenu.inc" %>
<%@ include  file="/MaterialMenu.inc" %>
<%@ include  file="/LaborMenu.inc" %>
<%@ include  file="/FreightMenu.inc" %>
<%@ include  file="/TravelMenu.inc" %>
<%@ include  file="/CommonResources.inc" %>
<%@ include  file="/DashboardStatusScript.inc" %>

<%@ include  file="/ProjectJobMenu.inc" %>

<%
	
	String addendumStatus ="";
	String job_Status ="";
	String jobViewtype ="";
	
	if(request.getAttribute("Job_Id")!=null)
		Job_Id= request.getAttribute("Job_Id").toString();
		
	if(request.getAttribute("job_Status")!=null)
		job_Status= request.getAttribute("job_Status").toString();
		
	if(request.getAttribute("jobViewtype")!=null)
		jobViewtype= request.getAttribute("jobViewtype").toString();	
	
%>

<script>
function addendumSendEmail() {
	document.forms[0].action = "EmailAction.do?Type=PRMAppendix&Appendix_Id=<%=Appendix_Id%>&jobId=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;	
}	

function addendumManageStatus(v) {
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_job&id=<%=Job_Id%>";
	document.forms[0].submit();
	return true;	
}

function addendumViewContractDocument(id, docType) {
	if(docType=='html')   document.forms[0].target = "_blank";
	else   document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+id+"&View="+docType+"&Type=Appendix&from_type=PRM&Id=<%=Appendix_Id%>";
	document.forms[0].submit();
}

function addendumSendStatusReport() {
	document.forms[0].action = "EmailAction.do?Type=prm_job&Type1=prm_job&&id=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;
}

function addendumViewComments() {
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}

function addendumAddComment() {
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Appendix_Id=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}


</script>
<script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	<%-- //str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>'; --%>
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script>		 
	
</head>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad ="leftAdjLayers();">
<html:form action  ="/MenuFunctionViewCommentAction">
<html:hidden property="msaId"/> 
<html:hidden property = "type" />
<html:hidden property = "name" />
<html:hidden property="otherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<html:hidden property="forwardId"/>
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property="ref"/> 
<html:hidden property="jobOwnerOtherCheck"/>

<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "MSA">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        	<%-- <%if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )){%>
							 <%if(msastatus.equals( "Cancelled" )){%>
					        	<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
					        <%}else{ %>
								<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>		        
					        <%}%>
						  <%}else{%>
	                 			 <td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
							<%}%>
							<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
					<%@ include file = "/Menu_MSA_Appendix_Msa.inc" %>		
			        
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7"><h2>MSA&nbsp;<bean:message bundle = "pm" key = "commom.viewcomment.formheader"/>&nbsp;<bean:write name  ="MenuFunctionViewCommentForm" property ="name"/></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "selectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "Appendix">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
	<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>		       
			        <%-- <tr>
			        <%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )){%>
			        	<%if(appendixStatus.equals( "Cancelled" )){%>
			        		<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
			        	<%}else{ %>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
			        	<%}%>
					<%}else{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%}%>	
	
					<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
			        </tr> --%>
			     <%@ include file = "/Menu_MSA_Appendix_Contact.inc" %>   
			        
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="MenuFunctionViewCommentForm" property ="msaId"/>&firstCall=true"><bean:write name  ="MenuFunctionViewCommentForm" property ="msaName"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7"><h2>Appendix&nbsp;<bean:message bundle = "pm" key = "commom.viewcomment.formheader"/>&nbsp;<bean:write name  ="MenuFunctionViewCommentForm" property ="name"/></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<% 
																checkowner = "javascript: checking('"+ownerId+"');"; 
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name  ="MenuFunctionViewCommentForm" property ="type" value ="Job">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        <%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>
			        </tr>
			        <tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "msaId"/>&Appendix_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "appendixId"/>"><bean:write name = "MenuFunctionViewCommentForm" property = "msaName"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "appendixId"/>&Name=<bean:write name = "MenuFunctionViewCommentForm" property = "appendixName"/>&ref=<%=chkaddendum%>"><bean:write name = "MenuFunctionViewCommentForm" property = "appendixName"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
	 					<td colspan="7"><h2>Job&nbsp;<bean:message bundle = "pm" key = "commom.viewcomment.formheader"/>&nbsp;<bean:write name  ="MenuFunctionViewCommentForm" property ="name"/></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name  ="MenuFunctionViewCommentForm" property ="type" value ="Jobdashboard">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
    	<td valign="top" width = "100%">
    		<table cellpadding="0" cellspacing="0" border="0" width = "100%">
				<tr>
			        	<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
				<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
				<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
				</tr>
				<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
				         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
							 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
							 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
								<a ><span class="breadCrumb1">Comments</a>
				    </td>
				</tr>
			       <tr><td height="5" >&nbsp;</td></tr>	
			 </table>
		</td>
	</tr>
</table>
</logic:equal>
<logic:equal name  ="MenuFunctionViewCommentForm" property ="type" value ="Activity">
<logic:equal name  ="MenuFunctionViewCommentForm" property ="ref" value ="detailactivity">

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        <%if(  jobtype.equals( "Default" ) ||  jobtype.equals( "Addendum" ) ) { %>
						<%if(activityStatus.equals("Approved")){%>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditapprovedactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
						<%}%>
						<%if(activityStatus.equals("Draft")){%>
				        				<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
						<%}%>
		        	<%}else{%>
						<%if(activityType.equals("Overhead")){%>
					  		<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditoverheadactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
				        	<%}else{%>
						   	<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
				        	<%}%>
		        	<%}%>
		        	<td id = "pop2" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><CENTER>Manage</CENTER></a></td>
		        	<td id = "pop3" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><CENTER>Resources</CENTER></a></td>
					<td id = "pop4" width = "840" class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			      </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
						       	<a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
					    	 	<a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="MenuFunctionViewCommentForm" property ="msaId"/>&firstCall=true"><bean:write name  ="MenuFunctionViewCommentForm" property ="msaName"/></a>
					    		<a href ="JobUpdateAction.do?ref=<%=refForJob %>&Appendix_Id=<bean:write name  ="MenuFunctionViewCommentForm" property ="appendixId"/>"><bean:write name  ="MenuFunctionViewCommentForm" property ="appendixName"/></a>
					    		<a href ="ActivityUpdateAction.do?ref=<%=chkaddendum%>&Job_Id=<bean:write name  ="MenuFunctionViewCommentForm" property ="jobId"/>"><bean:write name  ="MenuFunctionViewCommentForm" property ="jobName"/></a>
					         </div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7"><h2><bean:message bundle = "pm" key = "common.viewcomment.activity"/> <bean:message bundle = "pm" key = "commom.viewcomment.formheader"/><bean:write name  ="MenuFunctionViewCommentForm" property ="name"/> </h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>

<logic:notEqual name  ="MenuFunctionViewCommentForm" property ="ref" value ="detailactivity">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19"><a href= "javascript:history.go(-1);" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont"><bean:message bundle = "pm" key = "common.sow.backtopmactivity"/></a></td></tr>
					<tr><td background="images/content_head_04.jpg" height="21" width="100%">&nbsp;</td></tr>
					<tr>
	 					<td colspan="7"><h2><bean:message bundle = "pm" key = "common.viewcomment.activity"/> <bean:message bundle = "pm" key = "commom.viewcomment.formheader"/><bean:write name  ="MenuFunctionViewCommentForm" property ="name"/> </h2></td>
	 				</tr>
					
					
</table>
</logic:notEqual>

</logic:equal>
<logic:equal name  ="MenuFunctionViewCommentForm" property ="type" value ="pm_resM">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			         <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "MaterialEditAction.do?&ref=<%=chkaddendum%>&Material_Id=<%=Material_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "msaId" />">
			          		<bean:write name = "MenuFunctionViewCommentForm" property = "msaName"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "appendixId" />">
			   				<bean:write name = "MenuFunctionViewCommentForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "jobId" />">
			       			<bean:write name = "MenuFunctionViewCommentForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=M&Activity_Id=<%=Activity_Id%>">
			       			<bean:write name = "MenuFunctionViewCommentForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
	 				</tr>
	 				<tr>
						<td colspan="7"><h2><bean:message bundle = "pm" key = "pm.material.comments.label"/><bean:message bundle = "pm" key = "common.viewcomment.for"/> <bean:write name  ="MenuFunctionViewCommentForm" property ="name"/></h2></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>


</logic:equal>
<logic:equal name  ="MenuFunctionViewCommentForm" property ="type" value ="pm_resL">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "LaborEditAction.do?&ref=<%=chkaddendum%>&Labor_Id=<%=Labor_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "msaId" />">
			          		<bean:write name = "MenuFunctionViewCommentForm" property = "msaName"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "appendixId" />">
			   				<bean:write name = "MenuFunctionViewCommentForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "jobId" />">
			       			<bean:write name = "MenuFunctionViewCommentForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=L&Activity_Id=<%=Activity_Id%>">
			       			<bean:write name = "MenuFunctionViewCommentForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
	 				</tr>
			        
	 				<tr>
						<td colspan="7"><h2><bean:message bundle = "pm" key = "pm.labor.comments.label"/><bean:write name  ="MenuFunctionViewCommentForm" property ="name"/></h2></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>


</logic:equal>
<logic:equal name  ="MenuFunctionViewCommentForm" property ="type" value ="pm_resF">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "FreightEditAction.do?&ref=<%=chkaddendum%>&Freight_Id=<%=Freight_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "msaId" />">
			          		<bean:write name = "MenuFunctionViewCommentForm" property = "msaName"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "appendixId" />">
			   				<bean:write name = "MenuFunctionViewCommentForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "jobId" />">
			       			<bean:write name = "MenuFunctionViewCommentForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=F&Activity_Id=<%=Activity_Id%>">
			       			<bean:write name = "MenuFunctionViewCommentForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
	 				</tr>
			        
	 				<tr>
						<td colspan="7"><h2><bean:message bundle = "pm" key = "pm.freight.comments.label"/><bean:message bundle = "pm" key = "common.viewcomment.labour"/></h2></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>


</logic:equal>
<logic:equal name  ="MenuFunctionViewCommentForm" property ="type" value ="pm_resT">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "TravelEditAction.do?&ref=<%=chkaddendum%>&Travel_Id=<%=Travel_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "msaId" />">
			          		<bean:write name = "MenuFunctionViewCommentForm" property = "msaName"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "appendixId" />">
			   				<bean:write name = "MenuFunctionViewCommentForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MenuFunctionViewCommentForm" property = "jobId" />">
			       			<bean:write name = "MenuFunctionViewCommentForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=T&Activity_Id=<%=Activity_Id%>">
			       			<bean:write name = "MenuFunctionViewCommentForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
	 				</tr>
			        
	 				<tr>
						<td colspan="7"><h2><bean:message bundle = "pm" key = "pm.travel.comments.label"/><bean:write name  ="MenuFunctionViewCommentForm" property ="name"/></h2></td>
					</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>

</logic:equal>
<logic:equal name  ="MenuFunctionViewCommentForm" property ="type" value ="prm_appendix">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AppendixDashboardMenuScript.inc" %>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
			       <tr>
						<td colspan="4"><h2><bean:message bundle = "pm" key = "commom.viewcomment.formheader"/><bean:message bundle = "pm" key = "common.viewcomment.for"/> <bean:write name  ="MenuFunctionViewCommentForm" property ="name"/> <bean:message bundle = "pm" key = "common.viewcomment.appendix"/></h2></td>
					</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobAppStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobAppStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="MenuFunctionViewCommentForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					           <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
	
	
</logic:equal>
<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "MSA">
<%@ include  file="/MSAMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "Appendix">
<%@ include  file="/AppendixMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "Job">
<%@ include  file="/JobMenuScript.inc" %>
</logic:equal>

<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "Activity">
	<logic:equal name = "MenuFunctionViewCommentForm" property = "ref" value = "detailactivity">
		<%@include  file = "/ActivityMenuScript.inc"%>
	</logic:equal>
</logic:equal>
<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "pm_resM">
<%@ include  file="/MaterialMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "pm_resL">
<%@ include  file="/LaborMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "pm_resF">
<%@ include  file="/FreightMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "pm_resT">
<%@ include  file="/TravelMenuScript.inc" %>
</logic:equal>


<logic:equal name = "MenuFunctionViewCommentForm" property = "type" value = "Jobdashboard">
		<c:if test = "${requestScope.jobType eq 'Addendum'}">	
			<%@ include  file="/AddendumMenuScript.inc" %>
		</c:if>
			<c:if test = "${requestScope.jobType eq 'Default'}">
			<%@ include  file="/DefaultJobMenuScript.inc" %>
		</c:if>
		<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
			<%@ include  file="/ProjectJobMenuScript.inc" %>
		</c:if>	
	</logic:equal>
<table  cellspacing = "1" cellpadding = "1" width = "500" border ="0"> 
		
		
		<tr>
			<td>&nbsp;</td>
			<td class = "colCaptions" align="center" width ="150"><bean:message bundle = "pm" key = "common.viewcomment.commentdate"/></td>
			<td class = "colCaptions" align="center" width ="200"><bean:message bundle = "pm" key = "common.viewcomment.comments"/></td>
			<td class = "colCaptions" align="center" width ="150"><bean:message bundle = "pm" key = "common.viewcomment.commentby"/></td>
		</tr>   
							<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							 	
		<logic:iterate id = "comment" name = "all_comment" scope = "request">
			  
			<%		i++;
					if( csschooser == true )
					{
						cssclass = "rowDark";
						nameclass = "rowDark";
						commentbyclass = "rowDark";
						csschooser = false;
					}
					else
					{
						cssclass = "rowLight";
						nameclass = "rowLight";
						commentbyclass = "rowLight";
						csschooser = true; 
					}
			%>
			
			<tr>
			<td>&nbsp;</td>
				<td class = "<%= cssclass %>"><bean:write name = "comment" property = "commentdate"/></td>	
				<td class = "<%= cssclass %>" style="word-break:break-all;word-wrap:break-word;"><bean:write name = "comment" property = "comment" filter="false"/></td>
				 <td class = "<%= commentbyclass %>"><bean:write name = "comment" property = "commentby"/></td>			  
	 		</tr>
	 		
	 	</logic:iterate>
			<tr><td><img src = "images/spacer.gif" width = "1" height = "5"></td></tr>
		</table>
		
</html:form>   
</body>
<script>

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}


function show()
{
	
	document.forms[0].go.value="true";
	
	if(document.forms[0].type.value=='MSA')
		document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&clickShow=true";
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'msaId' />&Appendix_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&clickShow=true";
		
		
	document.forms[0].submit();
	return true;
}

function initialState()
{
document.forms[0].go.value="";
	if(document.forms[0].type.value=='MSA')
		document.forms[0].action="MenuFunctionViewCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&firstCall=true&home=home&clickShow=true";
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action="MenuFunctionViewCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'msaId' />&Appendix_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&firstCall=true&home=home&clickShow=true";
		
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{
	if(document.forms[0].type.value=='MSA'){
		
	if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				document.forms[0].selectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].selectedOwners.length;j++)
			{
				if(document.forms[0].selectedOwners[j].checked) {
				 	document.forms[0].selectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{	
		for(i=0;i<document.forms[0].selectedOwners.length;i++)
			{
					if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
					{
						document.forms[0].otherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="MenuFunctionViewCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&opendiv=0";
		 				document.forms[0].submit();
					}
			}
	}
}
	if(document.forms[0].type.value=='Appendix'){
			if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
					{
					if(checkboxvalue == '%'){
					for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
					{
						document.forms[0].appendixSelectedOwners[j].checked=false;
					}
					}
			else
			{
				for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
				{
					if(document.forms[0].appendixSelectedOwners[j].checked) {
					 	document.forms[0].appendixSelectedOwners[1].checked = false;
					 	break;
					}
				}		
			}
			}		
	
	else
	{
			for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
			{
					if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
					{
						document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="MenuFunctionViewCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'msaId' />&Appendix_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&opendiv=0";
		 				document.forms[0].submit();
		
					}
			}
	}
	}
}

function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="MenuFunctionViewCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="MenuFunctionViewCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'msaId' />&Appendix_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
//alert('in month_Appendix()');
document.forms[0].go.value="true";
document.forms[0].action="MenuFunctionViewCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'msaId' />&Appendix_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="MenuFunctionViewCommentAction.do?Type=prm_appendix&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'msaId' />&Appendix_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="MenuFunctionViewCommentAction.do?Type=prm_appendix&MSA_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'msaId' />&Appendix_Id=<bean:write name = 'MenuFunctionViewCommentForm' property = 'forwardId' />&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].forwardId.value;
 	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>


</html:html>
