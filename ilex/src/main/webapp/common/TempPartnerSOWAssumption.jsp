<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ page language = "java" %>


<%
	String fromId = "";
	String appendixId = "";
	String fromType = "";
	String viewType = "";
	String viewjobtype = "";
	String ownerId = "";
	String nettype = "";
	String addFlag = "";
	
	if(request.getAttribute("fromId") != null)
		fromId = (String) request.getAttribute("fromId");

	if(request.getAttribute("appendixId") != null)
		appendixId = (String) request.getAttribute("appendixId");
				
	if(request.getAttribute("fromType") != null)
		fromType = (String) request.getAttribute("fromType");
	
	if(request.getAttribute("viewType") != null)
		viewType = (String) request.getAttribute("viewType");
	
	if(request.getAttribute("viewjobtype") != null)
		viewjobtype = (String) request.getAttribute("viewjobtype");
	
	if(request.getAttribute("ownerId") != null)
		ownerId = (String) request.getAttribute("ownerId");
	
	if(request.getAttribute("nettype") != null)
		nettype = (String) request.getAttribute("nettype");
	
	if(request.getAttribute("addFlag") != null)
		addFlag = (String) request.getAttribute("addFlag");

	//System.out.println("addFlag:::::::::::::::"+addFlag);
%>
<% if(fromType.equals("prm_app")) {%>	
	<jsp:forward page="/AppendixHeader.do">
		<jsp:param name = "function" value = "view" /> 
		<jsp:param name = "addFlag" value = "<%= addFlag %>" /> 
		<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" /> 
		<jsp:param name = "ownerId" value = "<%= ownerId %>" />  
		<jsp:param name = "appendixid" value = "<%= appendixId %>" />  
		<jsp:param name = "nettype" value = "<%= nettype %>" />  
	</jsp:forward>
<% } else { %>	
	<jsp:forward page="/PartnerSOWAssumptionList.do">
		<jsp:param name = "addFlag" value = "<%= addFlag %>" /> 
		<jsp:param name = "fromId" value = "<%= fromId %>" /> 
		<jsp:param name = "fromType" value = "<%= fromType %>" />
		<jsp:param name = "viewType" value = "<%= viewType %>" /> 
		<jsp:param name = "viewjobtype" value = "<%= viewjobtype %>" /> 
		<jsp:param name = "ownerId" value = "<%= ownerId %>" />  
		<jsp:param name = "nettype" value = "<%= nettype %>" />  
	</jsp:forward>

<% } %>






