<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<bean:define id = "codes" name = "codelist" scope = "request"/>

<html:html>
	<head>
		<title><bean:message bundle = "pm" key = "msa.sort.title"/></title>
		<%@ include file="../Header.inc" %>
		<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	

<script>

function refresh() 
{

	

	if( document.forms[0].selectedparam.length == 0 )
	{
		alert( "<bean:message bundle = "pm" key = "msa.sort.warning"/>" );  
		return false;
	}
	
	for( var i = 0 ; i < document.forms[0].selectedparam.length ; i++ )
	{
		document.forms[0].selectedparam.options[i].selected = true;
	}
	
	var i = document.forms[0].sortparam.length;
	
	if( i > 0 )
	{
		
		while( i > 0 )
		{
			if( document.forms[0].sortparam.options[i-1].selected )
			{ 
				document.forms[0].sortparam.options.remove( i -1 );
				//return false;
			}
			i--;
		}
	}
	//alert(window.showModelessDialog);
	// if (!window.open()) {
		 
	//	 alert("insdie of function ... ");
		 var url='SortAction.do?sort=Sort'
				document.forms[0].action=url;
				document.forms[0].target = "ilexmain";
				document.forms[0].submit();
				window.close();
				return false;	 
	// }	
	return true;
}

function remove()
{
	var temp = document.forms[0].selectedparam.length;	
	while( temp > 0 ) 
	{
		document.forms[0].selectedparam.options.remove(0);
		temp--;
	}	
}

function CopyAll() 
{
	var temp = document.forms[0].selectedparam.length;	
	for( var i = 0 ; i < document.forms[0].sortparam.length ; i++ )
	{
		document.forms[0].selectedparam.options.add( new Option ( document.forms[0].sortparam.options[i].text , document.forms[0].sortparam.options[i].value ) , temp );
		temp++;
	}//for
	
	var temp = document.forms[0].sortparam.length;	
	while( temp > 0 ) 
	{
		document.forms[0].sortparam.options.remove( 0 );
		temp--;
	}
	
	return true;
}

function CopySelected() 
{
	var temp = document.forms[0].sortparam.length;	
	for( var i = 0 ; i < document.forms[0].sortparam.length ; i++ )
	{
		if( document.forms[0].sortparam.options[i].selected ) 
		{
			document.forms[0].selectedparam.options.add( new Option( document.forms[0].sortparam.options[i].text , document.forms[0].sortparam.options[i].value ) , temp );
			temp++;
			
		}//if
	}//for
		
		
	var i = document.forms[0].sortparam.length;	
	while( i > 0 ) 
	{
		if( document.forms[0].sortparam.options[i-1].selected ) 
		{
			document.forms[0].sortparam.options.remove( i - 1 );
		}//if
		i--;
	}
	return true;
}//function

function SendAll() 
{
	var temp = document.forms[0].sortparam.length;	
	for( var i = 0; i < document.forms[0].selectedparam.length ; i++ )
	{
		document.forms[0].sortparam.options.add( new Option( document.forms[0].selectedparam.options[i].text,document.forms[0].selectedparam.options[i].value ) , temp );
		temp++;
	}//for
	
	var temp = document.forms[0].selectedparam.length;	
	while( temp > 0 ) 
	{
		document.forms[0].selectedparam.options.remove( 0 );
		temp--;
	}
	return true;
}


function SendSelected() 
{
	var temp = document.forms[0].sortparam.length;	
	for( var i = 0; i<document.forms[0].selectedparam.length; i++ )
	{
		if( document.forms[0].selectedparam.options[i].selected ) 
		{
				document.forms[0].sortparam.options.add( new Option( document.forms[0].selectedparam.options[i].text , document.forms[0].selectedparam.options[i].value) , temp );
				temp++;	
		}//if
	}//for
	
	var i = document.forms[0].selectedparam.length;	
	while( i > 0 ) 
	{
		if( document.forms[0].selectedparam.options[i-1].selected ) 
		{
			document.forms[0].selectedparam.options.remove( i-1 );
		}//if
		i--;
	}//while
	
	return true;
}//function


</script>
</head>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" class = "buttonrow">
  <html:form action = "/SortAction" target = "ilexmain">
<table width = "80%" border = "0" cellspacing="1" cellpadding = "1" align = "center" valign  = "top">
	  <tr> 
	  		<td colspan = "3" class = "labelgraybold" height = "30"><bean:message bundle = "pm" key = "msa.sort.selectcolumns"/></td>
	  </tr> 
  	  
  	  <tr>
  			<td colspan = "2" align = "left" width = "25%" class = "labelgrays"><bean:message bundle = "pm" key = "msa.sort.columnsname"/></td>
  			<td class = "labelgrays"><bean:message bundle = "pm" key = "msa.sort.sortingorder"/></td>
  	  </tr>
  
		
  
  <tr>
    <td class = "buttonrow">
	 	<html:select property = "sortparam" size = "10"  styleClass = "sort">
			<html:optionsCollection name = "codes" property = "sortparameter" value = "value" label = "label"/>    
		</html:select>
	</td>
	
	<td class = "buttonrow" >
		<br>
			<html:button property = "singleforward" styleClass = "button" onclick = "return CopySelected();"><bean:message bundle = "pm" key = "msa.sort.singleforward"/></html:button><br>
			<html:button property = "doubleforward" styleClass = "button" onclick = "return CopyAll();"><bean:message bundle = "pm" key = "msa.sort.doubleforward"/></html:button><br><br>
			<html:button property = "singlebackward" styleClass = "button" onclick = "return SendSelected();"><bean:message bundle = "pm" key = "msa.sort.singlebackward"/></html:button><br>
			<html:button property = "doublebackward" styleClass = "button" onclick = "return SendAll();"><bean:message bundle = "pm" key = "msa.sort.doublebackward"/></html:button><br>
    </td>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
	
	<td class = "buttonrow">
		<html:select property = "selectedparam" size = "10" multiple = "true" styleClass = "sort">
		</html:select>
	</td>
  </tr>
  
  <html:hidden property = "type" />
  <html:hidden property = "id" />
  <html:hidden property = "id1" />
  <html:hidden property = "ref" />
  <html:hidden property = "addendum_id" />
  <html:hidden property="siteid"/>
  <!-- for filter sorting -->
  <html:hidden property = "currentstatus" />
  <html:hidden property = "rdofilter" />
  <html:hidden property = "nettype" />
  <html:hidden property = "ownerId" />
  <html:hidden property = "from" /> 
  <html:hidden property = "ref1" />
  <html:hidden property = "viewjobtype" /> 
  <html:hidden property = "page" />
  
  <!-- Start : Added By Amit -->
  <html:hidden property ="sitename"/>
  <html:hidden property ="sitenumber"/>
  <html:hidden property ="sitecity"/>
  <html:hidden property ="flag"/>
  <html:hidden property="invoice_Flag"/>
  <html:hidden property="partner_name"/>
  <html:hidden property="powo_number"/>
  <html:hidden property="from_date"/>
  <html:hidden property="to_date"/>
  <html:hidden property="invoiceno"/>
  <html:hidden property="siteSearchName"/>
  <html:hidden property="siteSearchNumber"/>
  <html:hidden property="siteSearchCity"/>
  <html:hidden property="siteSearchState"/>
  <html:hidden property="siteSearchZip"/>
  <html:hidden property="siteSearchCountry"/>
  <html:hidden property="siteSearchGroup"/>
  <html:hidden property="siteSearchEndCustomer"/>
  <html:hidden property="siteSearchStatus"/>
  <html:hidden property="lines_per_page"/>
  <html:hidden property="org_lines_per_page"/>
  <html:hidden property="siteNameForColumn"/>
  <html:hidden property="siteNumberForColumn"/>
  <html:hidden property="siteAddressForColumn"/>
  <html:hidden property="siteCityForColumn"/>
  <html:hidden property="siteStateForColumn"/>
  <html:hidden property="siteZipForColumn"/>
  <html:hidden property="siteCountryForColumn"/>
  <html:hidden property="pageType"/>
  <html:hidden property="site_msaid"/>
  <html:hidden property="orgTopName"/>
  <html:hidden property="action"/>
  <html:hidden property="startdate"/>
  <html:hidden property="enddate"/>
  <html:hidden property="division"/>
  <html:hidden property="partnerType"/>
  

  <html:hidden property="name"/>
  <html:hidden property="keyword"/>
  <html:hidden property="partnersearchType"/>
  <html:hidden property="mvsaVers"/>
  <html:hidden property="zip"/>
  <html:hidden property="radius"/>
  <html:hidden property="mainoffice"/>
  <html:hidden property="fieldOffice"/>
  <html:hidden property="techhi"/>
  <html:hidden property="rating"/>
  <html:hidden property="techinician"/>
  <html:hidden property="certification"/>
  <html:hidden property="corecomp"/>
  <html:hidden property="chekBox1"/>
  <html:hidden property="chekBox2"/>
  <html:hidden property="jobId"/>
  <html:hidden property="division"/>
  <html:hidden property="selectedPartnersList"/>
  <html:hidden property="coreCompetencyList"/>
  <html:hidden property="techniciansList"/>
  <html:hidden property="securityCertiftn"/>
  <html:hidden property="selectedCountryName"/>
  <html:hidden property="selectedCityName"/>

  
  
            
  <!-- End -->

 
  <!-- for filter sorting -->
   <tr> 
    <td colspan = "3"  class = "labelgrays">
	    <bean:message bundle = "pm" key = "msa.sort.Orderby"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<html:radio property = "sortcriteria" value = "ASC" />&nbsp;&nbsp;<bean:message bundle = "pm" key = "msa.sort.Ascending"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<html:radio property = "sortcriteria" value = "DESC" />&nbsp;&nbsp;<bean:message bundle="pm" key = "msa.sort.Descending"/>  
	</td>
  </tr>

	<tr>
		<td class = "buttonrow" colspan = "3">&nbsp;</td>
	</tr>
  	
  	<tr> 
   		<td colspan = "3"> 
			<html:submit property = "sort" styleClass = "button" onclick = "return refresh();">Sort</html:submit>
			<html:reset property = "cancel" styleClass = "button">Cancel</html:reset>
			<html:button property = "close" styleClass = "button" onclick = "javascript:window.close();">Close</html:button>
		</td>
 </tr>

</table>

</html:form>

</body>
</html:html>

