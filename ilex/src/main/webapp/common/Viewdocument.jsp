<!DOCTYPE HTML>
<%
	String remarkclass = "";
	String commentbyclass = "";
%>


<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html:html>

<head>

	<title><bean:message bundle = "pm" key = "msa.tabular.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css">  
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>    
  	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
  	
  
  	<%@ include file = "/NMenu.inc" %>

	<%@ include  file="/MSAMenu.inc" %>   
	<%@ include  file="/AppendixMenu.inc" %> 
		<%@ include  file="/JobMenu.inc" %>
	<%@ include file="/DefaultJobMenu.inc" %>
	<%@ include  file="/ProjectJobMenu.inc" %>
	<%@ include  file="/DashboardStatusScript.inc" %>

<%
	String backgroundclass = null;
	boolean csschooser = true;
	int i = 0;
	String id = "";
	String ref = "";
	String ref1 = "";
	int addRow = -1;
	boolean addSpace = false;
	int rowHeight = 120;
	int ownerListSize = 0;
	int j=2;
	String checkowner=null;

	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());

	int filelist_size =( int ) Integer.parseInt( request.getAttribute( "filelistlength" ).toString()); 

%>
<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkstatus = null;
int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());

%>
<%
	
	String addendumStatus ="";
	String job_Status ="";
	String jobViewtype ="";
	
	if(request.getAttribute("Job_Id")!=null)
		Job_Id= request.getAttribute("Job_Id").toString();
		
	if(request.getAttribute("job_Status")!=null)
		job_Status= request.getAttribute("job_Status").toString();
		
	if(request.getAttribute("jobViewtype")!=null)
		jobViewtype= request.getAttribute("jobViewtype").toString();	
	
%>

<script>
function addendumSendEmail() {
	document.forms[0].action = "EmailAction.do?Type=PRMAppendix&Appendix_Id=<%=Appendix_Id%>&jobId=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;	
}	

function addendumManageStatus(v) {
	document.forms[0].action = "ManageStatusAction.do?ref="+v+"&type=prm_job&id=<%=Job_Id%>";
	document.forms[0].submit();
	return true;	
}

function addendumViewContractDocument(id, docType) {
	if(docType=='html')   document.forms[0].target = "_blank";
	else   document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+id+"&View="+docType+"&Type=Appendix&from_type=PRM&Id=<%=Appendix_Id%>";
	document.forms[0].submit();
}

function addendumSendStatusReport() {
	document.forms[0].action = "EmailAction.do?Type=prm_job&Type1=prm_job&&id=<%=Job_Id%>&addendumflag=true";
	document.forms[0].submit();
	return true;
}

function addendumViewComments() {
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}

function addendumAddComment() {
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=jobdashboard&addendum_id=<%=Job_Id%>&ref=view&Type=Jobdashboard&Appendix_Id=<%=Appendix_Id%>&Job_Id=<%=Job_Id%>';
	document.forms[0].submit();
	return true;	 
}


</script>
</head>
<html:form action = "ViewuploadedocumentAction"> 
<body onLoad ="leftAdjLayers();OnLoad();">
<html:hidden property = "ref"/>
<html:hidden property = "ref1"/>
<html:hidden property = "id"/>
<html:hidden property = "addendum_id" />
<html:hidden property = "appendix_Id" />
<html:hidden property = "path" />
<html:hidden property = "nettype" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "type" />
<html:hidden property= "otherCheck"/> 
<html:hidden property= "go"/>
<html:hidden property= "appendixOtherCheck"/>
<html:hidden property= "MSA_Id"/>
<html:hidden property= "jobId"/>
<html:hidden property="jobOwnerOtherCheck"/> 

<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewmsadocument">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        	<% if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) || uploadstatus.equals( "U" ) )  
							{boolean check = false;
								if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) ) 
								{check = true;
									if( msastatus.equals( "Signed" ) )
									{
								%>
									<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegestatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}else{
								%>
								<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}}if( uploadstatus.equals( "U" ) &&  !check )
								{%>
									<td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegeupload"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}
							} 
							else 
							{ %>
	                 			 <td id = "pop1"  width = "120"  class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
							<% 
							} %>
							<td id = "pop2"  width = "120"  class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td>
			        
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan ="7"><h2>MSA&nbsp;<bean:message bundle = "pm" key = "common.fileupload.history"/>&nbsp;<bean:write name = "ViewDocumentForm" property ="name"/></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "selectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');";
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>">  
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>

<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewappendixdocument">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
				        	<% if( appendixStatus.equals( "Signed" ) || appendixStatus.equals( "Transmitted" ) )
						{
							if( appendixStatus.equals( "Signed" ) )
							{ %>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noeditappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
							<%}
							else
							{%>
								<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
							<%}
						}
						else
						{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%} %>	
		
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
						<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>	
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "ViewDocumentForm" property = "MSA_Id"/>&firstCall=true"><bean:write name = "ViewDocumentForm" property = "msaname"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7"><h2>Appendix&nbsp;<bean:message bundle = "pm" key = "common.fileupload.history"/>&nbsp;<bean:write name = "ViewDocumentForm" property ="name"/></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');";
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick =  "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewjobdocument">
 
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        	<%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>       
			        </tr>
			        <tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
						        <div id="breadCrumb">
									          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
									    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "ViewDocumentForm" property = "MSA_Id"/>&Appendix_Id=<bean:write name = "ViewDocumentForm" property = "appendix_Id"/>"><bean:write name = "ViewDocumentForm" property = "msaname"/></a>
									    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "ViewDocumentForm" property = "appendix_Id"/>&Name=<bean:write name = "ViewDocumentForm" property = "appendixName"/>&ref=<%=chkaddendum%>"><bean:write name = "ViewDocumentForm" property = "appendixName"/></a>
								</div>
						</td>
					</tr>
					<tr>
	 					<td colspan="7"><h2>Job&nbsp;<bean:message bundle = "pm" key = "common.fileupload.history"/>&nbsp;<bean:write name = "ViewDocumentForm" property ="name"/></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewjobdashboarddocument">

<table border="0" cellspacing="1" cellpadding="0" width="100%">
		<tr>
	        	<td id = "pop1"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><center>Job</center></a></td>
		<td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Status Report</center></a></td>
		<td id ="pop3" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td>
		</tr>
		<tr><td background="images/content_head_04.jpg" height="21" colspan="7">
		         <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
					 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${requestScope.appendixId }'/>"><c:out value='${requestScope.appendixName }'/></a>
					 <a href="JobDashboardAction.do?appendix_Id=<c:out value='${requestScope.appendixId }'/>&Job_Id=<c:out value='${requestScope.Job_Id }'/>"><c:out value='${requestScope.jobName }'/></a>
					 <a ><span class="breadCrumb1">Email</a>
		    </td>
		</tr>
	       <tr><td height="5" >&nbsp;</td></tr>	
	</table>
</logic:equal>
<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewappendixdashboarddocument">
<logic:notEqual name = "ViewDocumentForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr> 
		          <td  id="pop1" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage</center></a></td>
		          <td  id="pop2" width="120" class="Ntoprow1" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
		     	  <td  id="pop5" width="120" class="Ntoprow1" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<bean:write name = "ViewDocumentForm" property = "appendix_Id" />&ownerId=<bean:write name = "ViewDocumentForm" property = "ownerId" />&msaId=<bean:write name="ViewDocumentForm" property="MSA_Id"/>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
		          <td  id="pop6" width="650"  class="Ntoprow1">&nbsp;</td>
		        </tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        <tr>
						<td colspan="4">
							<h2><bean:message bundle = "pm" key = "common.fileupload.appendix"/>&nbsp;<bean:message bundle = "pm" key = "common.fileupload.history"/>&nbsp;<bean:write name = "ViewDocumentForm" property ="name"/></h2>
						</td>
					</tr>
	      </table>
   </td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					          <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
<%@ include  file="/AppedixDashboardMenuScript.inc" %>
</logic:notEqual>

<logic:equal name = "ViewDocumentForm"  property = "fromPage" value = "NetMedX">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/NetMedXDashboardMenu.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
	      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
		        <tr>
					<td class=Ntoprow1 id=pop1 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
					<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
					<td class=Ntoprow1 id =pop3 width =120><a class=menufont href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerId %>" style="width: 120px"><center>Search</center></a></td>
					<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
				</tr>
		        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
			          		<div id="breadCrumb">&nbsp;</div>
			        </td>
		        </tr>
		        <tr>
		        	<td colspan = "7" height="1"><h2>NetMedX&nbsp;<bean:message bundle = "pm" key = "common.fileupload.history"/>&nbsp;<bean:write name = "ViewDocumentForm" property ="name"/></h2></td>
		        </tr> 
	      </table>
    </td>
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
	        <tr>
	          <td width="155" height="39"  class="headerrow" colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" border="0">
		              <tr>
		                <td width="150">
		                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
			                   	<tr>
						            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
									<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
			                    </tr>
		                  	</table>
		                </td>
		              </tr>
		          </table>
	          </td>
	        </tr>
	        <tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
	              		<tr>
		              		 <td width ="70%" colspan="2">&nbsp;</td>
	               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>	
	                			<span>
									<div id="filter" class="divstyle">
					        			<table width="250" cellpadding="0" class="divtable">
						                     <tr>
					                            <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					                      		    <tr>
			                           	 			  <td colspan="3" class="filtersCaption">Status View</td>
			                          			    </tr>
						                          	<tr>	
						                          	  <td></td>
						                          	  <td class="tabNormalText">
															<logic:present name ="jobStatusList" scope="request">
														    	<logic:iterate id = "list" name = "jobStatusList"> 
														    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
														    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>
															    	<%
														    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
																	 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																			addStatusRow++;
																			addStatusSpace = true;
															    		}
															    	%>
															    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
															 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
																		<bean:write name ="list" property = "statusid"/>
															 		</html:multibox>   
																 	<bean:write name ="list" property = "statusdesc"/><br> 
																 	<%addStatusSpace = false;%>
																</logic:iterate>
															</logic:present>		
															<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
															<table cellpadding="0">
																<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
																<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
																<tr><td class="tabNormalText"><html:radio name ="ViewDocumentForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
															</table>	
												      </td>
							    					</tr>
			                        		    </table></td>
		                      			    	<td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
			                      			    <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						                          <tr>
						                            <td colspan="3" class="filtersCaption">User View</td>
						                          </tr>
						                          <tr>
												        <td class="tabNormalText">
												         <span id = "OwnerSpanId">
												   			<logic:present name ="jobOwnerList" scope="request">
															    	<logic:iterate id = "olist" name = "jobOwnerList">
															    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
															    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
															    		<% 
								
																		checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
																		if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
																			addOwnerRow++;
																			addOwnerSpace = true;
																		}	
															    		if(addOwnerRow == 0) { %> 
																    		<br/>
																    		<span class="ownerSpan">
																			&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																			<br/>								
																		<% } %>
														    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
														    			<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
																			<bean:write name ="olist" property = "ownerId"/>
															 			</html:multibox>   
																	  	<bean:write name ="olist" property = "ownerName"/>
																	  	<br/> 
																	</logic:iterate>
																	</span>
															</logic:present>
															</span>
													    </td>
						                          </tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
			                        			</table></td>
					                   	     </tr>
				  			            </table>											
						            </div>
				                </span>
							 </td>
	              		</tr>
            		</table>
            	</td>
        	</tr>
	    </table>
	</td>
 </tr>
</table>
<%@ include  file="/NetMedXDashboardMenuScript.inc" %>
</logic:equal>








</logic:equal>
<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewmsadocument">
	<%@ include  file="/MSAMenuScript.inc" %>
</logic:equal>
<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewappendixdocument">
	<%@ include  file="/AppendixMenuScript.inc" %>
</logic:equal>
<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewjobdocument">
	<%@ include  file="/JobMenuScript.inc" %>
</logic:equal>


<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
	<tr>
		<td width="2" height="0"></td>
		<td>
			<table border = "0" cellspacing = "1" cellpadding = "1" width = "500">
    			
    			<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present> 	
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
				</logic:present>
    			
    			
    			<logic:present name = "retval" scope = "request">
			    	<tr>
	    				<td  colspan = "5" class = "message" height = "30">
				    	 <logic:equal name = "retval" value = "0">
				    		<bean:message bundle = "pm" key = "common.fileupload.delete.success"/>
				    	</logic:equal>
	    	
				    	<logic:equal name = "retval" value = "-9001">
		    				<bean:message bundle = "pm" key = "common.fileupload.failure"/>
	    				</logic:equal>
	    				</td>
	    		      </tr>
			    </logic:present>
    			<% if(filelist_size == 0) { %>
    				<tr>
	    				<td  colspan = "5" class = "message" height = "30">
    						<bean:message bundle = "pm" key = "common.fileupload.nofile"/>
    					</td>
 			
	    		    </tr>
    				
	    		    <tr >
		   				 <td colspan="5" height="30" class = "colLight" align="center">
	    				  <logic:equal name = "ViewDocumentForm" property ="type" value = "Appendixdashboard">
		    			 	<html:button property="back" styleClass="button_c" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
		    			 </logic:equal>
		    		
		    			<logic:equal name = "ViewDocumentForm" property ="type" value = "Jobdashboard">
		    			 
		    			 				<logic:equal name ="ViewDocumentForm" property ="path" value ="appendixdashboard">
					    					<html:button property="back" styleClass="button_c" onclick = "return Backactionappendixjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
						    			</logic:equal>
						    			
							    		<logic:notEqual name ="ViewDocumentForm" property ="path" value ="appendixdashboard">
						    				<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
						    			</logic:notEqual>
						    			
		    			</logic:equal>
	    			
   	   					 </td>
		   			 </tr>
    			<% } 
    			else { %>
    			<tr> 
						<logic:equal name = "ViewDocumentForm" property ="type" value = "Jobdashboard">
						<td height = "30" colspan = "6"><h1>
							<bean:message bundle = "pm" key = "common.fileupload.jobname"/>
							<bean:message bundle = "pm" key = "common.fileupload.history"/>
							<bean:write name = "ViewDocumentForm" property ="name"/>
							</h1>
						</td>
						</logic:equal>
				</tr>
  
				<tr> 
					<td class="colCaptions">&nbsp;</td>
					<td class = "colCaptions"><bean:message bundle = "pm" key = "common.fileupload.uploaddate"/></td>
					<td class = "colCaptions"><bean:message bundle = "pm" key = "common.fileupload.filename"/></td>
					<td class = "colCaptions"><bean:message bundle = "pm" key = "common.fileupload.uploadremarks"/></td>
					<td class = "colCaptions"><bean:message bundle = "pm" key = "common.fileupload.uploadby"/></td>
					
				</tr>
  				<% } %>
			 	<logic:iterate id = "ms" name = "uploadeddocs" scope = "request">
					<bean:define id = "rem" name = "ms" property = "file_remarks" type = "java.lang.String"/> 
					<%	
						if ( csschooser == true ) 
						{
							backgroundclass = "rowDark";
							csschooser = false;
							remarkclass = "rowDark";
							commentbyclass = "rowDark";
						}
				
						else
						{
							csschooser = true;	
							backgroundclass = "rowLight";
							remarkclass = "rowLight";
							commentbyclass = "rowLight";
						}
					%>
					 <bean:define id = "fileid" name = "ms" property = "file_id" />
					  
					<tr>
						<td class="<%= remarkclass %>">
							<html:checkbox  property = "check" value = "<%= (String)fileid %>" styleClass="checkbox"/>
						</td>			
						<td class = "<%= remarkclass %>">
							<bean:write name = "ms" property = "file_uploaded_date"/>		
						</td>
			
						<td class = "<%= commentbyclass %>" height = "20"> 
							<a href = "ViewuploadedocumentAction.do?ref=downloaddocument&file_id=<bean:write name = "ms" property = "file_id" />"><bean:write name = "ms" property = "file_name" /></a>
						</td>
						
						<td class = "<%= remarkclass %>">
							<bean:write name = "ms" property = "file_remarks"  />
						</td>	
						
						<td class = "<%= commentbyclass %>">
							<bean:write name = "ms" property = "file_uploaded_by"/>		
						</td>
			
						
  					</tr>
  		
	  			 <% i++;  %>
	  		</logic:iterate>
	  	<%if(filelist_size != 0) { %>
	  	<tr >
	  		
		    <td colspan="6" class = "colLight" align="center"> 
			   <html:submit property = "delete" styleClass = "button_c" onclick = "return validate();">
			    <bean:message bundle = "pm" key = "common.fileupload.delete"/>
			   </html:submit> 
			 	<logic:equal name ="ViewDocumentForm" property ="type" value ="Appendixdashboard">
		    			<html:button property="back" styleClass="button_c" onclick = "return Backactionappendix();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
		    			
		    		</logic:equal>
		    		
		    		<logic:equal name ="ViewDocumentForm" property ="type" value ="Jobdashboard">
		    				<logic:equal name ="ViewDocumentForm" property ="path" value ="appendixdashboard">
		    					<html:button property="back" styleClass="button_c" onclick = "return Backactionappendixjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    			</logic:equal>
			    			
			    		<logic:notEqual name ="ViewDocumentForm" property ="path" value ="appendixdashboard">
			    			<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			    		</logic:notEqual>
			 		</logic:equal>
	    	</td>
	  	</tr>
	  	<% } %>
	  	
	  	
	  		
			<jsp:include page = '/Footer.jsp'>
				      <jsp:param name = 'colspan' value = '37'/>
				      <jsp:param name = 'helpid' value = 'viewdocument'/>
		   	</jsp:include> 
	  	</table>
	  </td>
	 </tr>
</table>
</body>
<logic:equal name = "ViewDocumentForm" property = "ref" value = "Viewjobdashboarddocument">
<c:if test = "${requestScope.jobType eq 'Addendum'}">
	
		<%@ include  file="/AddendumMenuScript.inc" %>
	</c:if>
		<c:if test = "${requestScope.jobType eq 'Default'}">
		<%@ include  file="/DefaultJobMenuScript.inc" %>
	</c:if>
	<c:if test = "${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
		<%@ include  file="/ProjectJobMenuScript.inc" %>
	</c:if>
</logic:equal>
</html:form>
<script>
function validate()
{
var submitflag = 'false';
if(<%= filelist_size %> != 0)
	{
		if(<%= filelist_size %> == 1) {
			if(document.forms[0].check.checked)	{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++) {
		  		if((document.forms[0].check[i].checked)) {
		  			submitflag = 'true';
		  		}
		  	}
		}
		
		if(submitflag == 'false') {
		  	alert( "<bean:message bundle = "pm" key = "common.fileupload.deletefile"/>" );
			return false;
		  	}
	}
	if(<%= filelist_size %> == 0)
	{
		return false;
	}


	document.forms[0].action="ViewuploadedocumentAction.do?Id=<bean:write name ='ViewDocumentForm' property ='id'/>"
	document.forms[0].submit(); 
return true;
}

function Backactionappendixjob()
{
	if(document.forms[0].nettype.value == 'dispatch')
		document.forms[0].action = "AppendixHeader.do?function=view&appendixid=-1&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	else
		document.forms[0].action = "AppendixHeader.do?function=view&appendixid=<bean:write name ='ViewDocumentForm' property ='appendix_Id'/>&nettype="+document.forms[0].nettype.value+"&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}

function Backactionappendix()
{
	document.forms[0].action = "AppendixHeader.do?function=view&appendixid=<bean:write name ='ViewDocumentForm' property ='id'/>&viewjobtype="+document.forms[0].viewjobtype.value+"&ownerId="+document.forms[0].ownerId.value;
	document.forms[0].submit();
	return true;
}
function Backactionjob()
{
	document.forms[0].action = "JobDashboardAction.do?jobid=<bean:write name ='ViewDocumentForm' property ='id'/>" ;
	document.forms[0].submit();
	return true;
}
</script>

<script>
function OnLoad()
{
if(document.forms[0].type.value =='prm_appendix' || document.forms[0].type.value =='Jobdashboard'){}
else{
		<%if(request.getAttribute("opendiv") != null){%>
		document.getElementById("filter").style.visibility="visible";
		<%}%>
}
}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function show()
{
	
	document.forms[0].go.value="true";
	
	if(document.forms[0].ref.value=='Viewmsadocument')
		document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&clickShow=true";
		
	if(document.forms[0].ref.value=='Viewappendixdocument')
		document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&clickShow=true";
		
		
	document.forms[0].submit();
	return true;
}

function initialState()
{
document.forms[0].go.value="";
	if(document.forms[0].ref.value=='Viewmsadocument') {
		document.forms[0].action="ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&firstCall=true&home=home&clickShow=true";
		}
		
	if(document.forms[0].ref.value=='Viewappendixdocument')
		document.forms[0].action="ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&firstCall=true&home=home&clickShow=true";
		
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{
	if(document.forms[0].ref.value=='Viewmsadocument'){
			if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
		{
			if(checkboxvalue == '%'){
				for(j=2;j<document.forms[0].selectedOwners.length;j++)
				{
					document.forms[0].selectedOwners[j].checked=false;
				}
			}
			else
			{
				for(j=2;j<document.forms[0].selectedOwners.length;j++)
				{
					if(document.forms[0].selectedOwners[j].checked) {
					 	document.forms[0].selectedOwners[1].checked = false;
					 	break;
					}
				}		
			}		
		}
		else
		{	
	
			for(i=0;i<document.forms[0].selectedOwners.length;i++)
			{
					if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
					{
						document.forms[0].otherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&opendiv=0";
		 				document.forms[0].submit();
					}
			}
		}
	}
	if(document.forms[0].ref.value=='Viewappendixdocument'){
			if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
					{
					if(checkboxvalue == '%'){
					for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
					{
						document.forms[0].appendixSelectedOwners[j].checked=false;
					}
					}
			else
			{
				for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
				{
					if(document.forms[0].appendixSelectedOwners[j].checked) {
					 	document.forms[0].appendixSelectedOwners[1].checked = false;
					 	break;
					}
				}		
			}
			}		
	
	else
	{
			for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
			{
					if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
					{
						document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&opendiv=0";
		 				document.forms[0].submit();
		
					}
			}
	}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
//alert('in month_Appendix()');
document.forms[0].go.value="true";
document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
</script>
<script>
function changeFilter()
{
	document.forms[0].action="ViewuploadedocumentAction.do?ref=Viewappendixdashboarddocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="ViewuploadedocumentAction.do?ref=Viewappendixdashboarddocument&Id=<bean:write name ='ViewDocumentForm' property ='id'/>&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].appendix_Id.value;	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>


</html:html>
  
